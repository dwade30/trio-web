﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCheckListingRegular.
	/// </summary>
	partial class rptCheckListingRegular
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCheckListingRegular));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblSort = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeductions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVacUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSick = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVoid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupDeductions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupVacUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupSickUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalDeduction = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalVacUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalSickUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotGross = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblSort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSick)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateField)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVoid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupTaxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupVacUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupSickUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTaxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDeduction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalVacUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSickUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGross)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDeductions,
				this.txtVacUsed,
				this.txtSick,
				this.txtHours,
				this.txtAmount,
				this.txtRegular,
				this.txtOther,
				this.txtTaxes,
				this.txtEmployee,
				this.txtDateField,
				this.txtType,
				this.txtVoid,
				this.txtCheckNumber,
				this.txtGross
			});
			this.Detail.Height = 0.28125F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalAmount,
				this.txtTotalRegular,
				this.txtTotalOther,
				this.txtTotalTaxes,
				this.txtTotalDeduction,
				this.txtTotalVacUsed,
				this.txtTotalSickUsed,
				this.txtTotalHours,
				this.Field15,
				this.Line13,
				this.txtTotGross
			});
			this.ReportFooter.Height = 0.4375F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblSort,
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.Label3,
				this.txtTime,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.lblRegular,
				this.lblOther,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Line12,
				this.lblGross
			});
			this.PageHeader.Height = 1.447917F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroupAmount,
				this.txtGroupRegular,
				this.txtGroupOther,
				this.txtGroupTaxes,
				this.txtGroupDeductions,
				this.txtGroupVacUsed,
				this.txtGroupSickUsed,
				this.txtGroupHours,
				this.Line14,
				this.Field16,
				this.txtGroupGross
			});
			this.GroupFooter1.Height = 0.6770833F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblSort
			// 
			this.lblSort.Height = 0.2083333F;
			this.lblSort.HyperLink = null;
			this.lblSort.Left = 0.0625F;
			this.lblSort.Name = "lblSort";
			this.lblSort.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 8.5pt; font-weight: bold; text-align: center";
			this.lblSort.Text = null;
			this.lblSort.Top = 0.25F;
			this.lblSort.Width = 9.78125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 10pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PAYROLL CHECK LISTING";
			this.Label1.Top = 0.04166667F;
			this.Label1.Width = 9.78125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.08333334F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.2083333F;
			this.txtDate.Left = 8.40625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2083333F;
			this.txtPage.Left = 8.40625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2083333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label3.Text = "Complete File";
			this.Label3.Top = 0.4583333F;
			this.Label3.Width = 9.78125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.2083333F;
			this.Field1.Left = 0.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'";
			this.Field1.Text = "Employee";
			this.Field1.Top = 1.208333F;
			this.Field1.Width = 0.9375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.2333333F;
			this.Field2.Left = 2.066667F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Field2.Text = "Date";
			this.Field2.Top = 1.2F;
			this.Field2.Width = 0.8333333F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.2083333F;
			this.Field3.Left = 2.90625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'";
			this.Field3.Text = "Type";
			this.Field3.Top = 1.208333F;
			this.Field3.Width = 0.40625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.2083333F;
			this.Field4.Left = 3.3125F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'";
			this.Field4.Text = "Void";
			this.Field4.Top = 1.208333F;
			this.Field4.Width = 0.375F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.2083333F;
			this.Field5.Left = 3.8125F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'";
			this.Field5.Text = "Check #";
			this.Field5.Top = 1.208333F;
			this.Field5.Width = 0.65625F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.2083333F;
			this.Field6.Left = 4.34375F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field6.Text = "Amount";
			this.Field6.Top = 1.208333F;
			this.Field6.Width = 0.65625F;
			// 
			// lblRegular
			// 
			this.lblRegular.Height = 0.2083333F;
			this.lblRegular.Left = 5.03125F;
			this.lblRegular.Name = "lblRegular";
			this.lblRegular.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblRegular.Text = "Regular";
			this.lblRegular.Top = 1.208333F;
			this.lblRegular.Width = 0.6875F;
			// 
			// lblOther
			// 
			this.lblOther.Height = 0.2083333F;
			this.lblOther.Left = 5.71875F;
			this.lblOther.Name = "lblOther";
			this.lblOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblOther.Text = "Other";
			this.lblOther.Top = 1.208333F;
			this.lblOther.Width = 0.6875F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.2083333F;
			this.Field9.Left = 6.40625F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field9.Text = "Taxes";
			this.Field9.Top = 1.208333F;
			this.Field9.Width = 0.6875F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.2083333F;
			this.Field10.Left = 7.09375F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field10.Text = "Deducts";
			this.Field10.Top = 1.208333F;
			this.Field10.Width = 0.6875F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.2083333F;
			this.Field11.Left = 7.78125F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field11.Text = "Vac used";
			this.Field11.Top = 1.208333F;
			this.Field11.Width = 0.6875F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.2083333F;
			this.Field12.Left = 8.46875F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field12.Text = "Sick Used";
			this.Field12.Top = 1.208333F;
			this.Field12.Width = 0.6875F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.2083333F;
			this.Field13.Left = 9.15625F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field13.Text = "Hours";
			this.Field13.Top = 1.208333F;
			this.Field13.Width = 0.6875F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.2083333F;
			this.Field14.Left = 5.0625F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Field14.Text = "- - - W a g e s - - -";
			this.Field14.Top = 1F;
			this.Field14.Width = 1.34375F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 1.416667F;
			this.Line12.Width = 9.875F;
			this.Line12.X1 = 0F;
			this.Line12.X2 = 9.875F;
			this.Line12.Y1 = 1.416667F;
			this.Line12.Y2 = 1.416667F;
			// 
			// lblGross
			// 
			this.lblGross.Height = 0.2083333F;
			this.lblGross.Left = 5.71875F;
			this.lblGross.Name = "lblGross";
			this.lblGross.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblGross.Text = "Gross";
			this.lblGross.Top = 1.208333F;
			this.lblGross.Visible = false;
			this.lblGross.Width = 0.6875F;
			// 
			// txtDeductions
			// 
			this.txtDeductions.Height = 0.2083333F;
			this.txtDeductions.Left = 7.09375F;
			this.txtDeductions.Name = "txtDeductions";
			this.txtDeductions.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDeductions.Text = "Deductions";
			this.txtDeductions.Top = 0.04166667F;
			this.txtDeductions.Width = 0.6875F;
			// 
			// txtVacUsed
			// 
			this.txtVacUsed.Height = 0.2083333F;
			this.txtVacUsed.Left = 7.78125F;
			this.txtVacUsed.Name = "txtVacUsed";
			this.txtVacUsed.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtVacUsed.Text = "Vac used";
			this.txtVacUsed.Top = 0.04166667F;
			this.txtVacUsed.Width = 0.6875F;
			// 
			// txtSick
			// 
			this.txtSick.Height = 0.2083333F;
			this.txtSick.Left = 8.46875F;
			this.txtSick.Name = "txtSick";
			this.txtSick.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSick.Text = "Sick Used";
			this.txtSick.Top = 0.04166667F;
			this.txtSick.Width = 0.6875F;
			// 
			// txtHours
			// 
			this.txtHours.Height = 0.2083333F;
			this.txtHours.Left = 9.15625F;
			this.txtHours.Name = "txtHours";
			this.txtHours.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtHours.Text = "Hours";
			this.txtHours.Top = 0.04166667F;
			this.txtHours.Width = 0.6875F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.2083333F;
			this.txtAmount.Left = 4.34375F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAmount.Text = "Amount";
			this.txtAmount.Top = 0.04166667F;
			this.txtAmount.Width = 0.6875F;
			// 
			// txtRegular
			// 
			this.txtRegular.Height = 0.2083333F;
			this.txtRegular.Left = 5.03125F;
			this.txtRegular.Name = "txtRegular";
			this.txtRegular.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtRegular.Text = "Regular";
			this.txtRegular.Top = 0.04166667F;
			this.txtRegular.Width = 0.6875F;
			// 
			// txtOther
			// 
			this.txtOther.Height = 0.2083333F;
			this.txtOther.Left = 5.71875F;
			this.txtOther.Name = "txtOther";
			this.txtOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOther.Text = "Other";
			this.txtOther.Top = 0.04166667F;
			this.txtOther.Width = 0.6875F;
			// 
			// txtTaxes
			// 
			this.txtTaxes.Height = 0.2083333F;
			this.txtTaxes.Left = 6.40625F;
			this.txtTaxes.Name = "txtTaxes";
			this.txtTaxes.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTaxes.Text = "Taxes";
			this.txtTaxes.Top = 0.04166667F;
			this.txtTaxes.Width = 0.6875F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.2083333F;
			this.txtEmployee.Left = 0.0625F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'";
			this.txtEmployee.Text = "Employee";
			this.txtEmployee.Top = 0.04166667F;
			this.txtEmployee.Width = 1.96875F;
			// 
			// txtDateField
			// 
			this.txtDateField.Height = 0.2083333F;
			this.txtDateField.Left = 2.09375F;
			this.txtDateField.Name = "txtDateField";
			this.txtDateField.Style = "font-family: \'Tahoma\'";
			this.txtDateField.Text = "Date";
			this.txtDateField.Top = 0.04166667F;
			this.txtDateField.Width = 0.8125F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.2083333F;
			this.txtType.Left = 2.90625F;
			this.txtType.Name = "txtType";
			this.txtType.Style = "font-family: \'Tahoma\'";
			this.txtType.Text = "Type";
			this.txtType.Top = 0.04166667F;
			this.txtType.Width = 0.40625F;
			// 
			// txtVoid
			// 
			this.txtVoid.Height = 0.2083333F;
			this.txtVoid.Left = 3.3125F;
			this.txtVoid.Name = "txtVoid";
			this.txtVoid.Style = "font-family: \'Tahoma\'";
			this.txtVoid.Text = "Void";
			this.txtVoid.Top = 0.04166667F;
			this.txtVoid.Width = 0.375F;
			// 
			// txtCheckNumber
			// 
			this.txtCheckNumber.Height = 0.2083333F;
			this.txtCheckNumber.Left = 3.8125F;
			this.txtCheckNumber.Name = "txtCheckNumber";
			this.txtCheckNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCheckNumber.Text = "Check #";
			this.txtCheckNumber.Top = 0.04166667F;
			this.txtCheckNumber.Width = 0.5625F;
			// 
			// txtGross
			// 
			this.txtGross.Height = 0.2083333F;
			this.txtGross.Left = 4.989583F;
			this.txtGross.Name = "txtGross";
			this.txtGross.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGross.Text = "Gross";
			this.txtGross.Top = 0.04166667F;
			this.txtGross.Visible = false;
			this.txtGross.Width = 1.416667F;
			// 
			// txtGroupAmount
			// 
			this.txtGroupAmount.Height = 0.1666667F;
			this.txtGroupAmount.Left = 4.366667F;
			this.txtGroupAmount.Name = "txtGroupAmount";
			this.txtGroupAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupAmount.Text = "Field15";
			this.txtGroupAmount.Top = 0.06666667F;
			this.txtGroupAmount.Width = 0.6666667F;
			// 
			// txtGroupRegular
			// 
			this.txtGroupRegular.Height = 0.1666667F;
			this.txtGroupRegular.Left = 5F;
			this.txtGroupRegular.Name = "txtGroupRegular";
			this.txtGroupRegular.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupRegular.Text = "Field15";
			this.txtGroupRegular.Top = 0.06666667F;
			this.txtGroupRegular.Width = 0.7333333F;
			// 
			// txtGroupOther
			// 
			this.txtGroupOther.Height = 0.1666667F;
			this.txtGroupOther.Left = 5.766667F;
			this.txtGroupOther.Name = "txtGroupOther";
			this.txtGroupOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupOther.Text = "Field15";
			this.txtGroupOther.Top = 0.06666667F;
			this.txtGroupOther.Width = 0.6333333F;
			// 
			// txtGroupTaxes
			// 
			this.txtGroupTaxes.Height = 0.1666667F;
			this.txtGroupTaxes.Left = 6.433333F;
			this.txtGroupTaxes.Name = "txtGroupTaxes";
			this.txtGroupTaxes.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupTaxes.Text = "Field15";
			this.txtGroupTaxes.Top = 0.06666667F;
			this.txtGroupTaxes.Width = 0.6666667F;
			// 
			// txtGroupDeductions
			// 
			this.txtGroupDeductions.Height = 0.1666667F;
			this.txtGroupDeductions.Left = 7.133333F;
			this.txtGroupDeductions.Name = "txtGroupDeductions";
			this.txtGroupDeductions.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupDeductions.Text = "Field15";
			this.txtGroupDeductions.Top = 0.06666667F;
			this.txtGroupDeductions.Width = 0.6333333F;
			// 
			// txtGroupVacUsed
			// 
			this.txtGroupVacUsed.Height = 0.1666667F;
			this.txtGroupVacUsed.Left = 7.766667F;
			this.txtGroupVacUsed.Name = "txtGroupVacUsed";
			this.txtGroupVacUsed.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupVacUsed.Text = "Field15";
			this.txtGroupVacUsed.Top = 0.06666667F;
			this.txtGroupVacUsed.Width = 0.7F;
			// 
			// txtGroupSickUsed
			// 
			this.txtGroupSickUsed.Height = 0.1666667F;
			this.txtGroupSickUsed.Left = 8.5F;
			this.txtGroupSickUsed.Name = "txtGroupSickUsed";
			this.txtGroupSickUsed.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupSickUsed.Text = "Field15";
			this.txtGroupSickUsed.Top = 0.06666667F;
			this.txtGroupSickUsed.Width = 0.6666667F;
			// 
			// txtGroupHours
			// 
			this.txtGroupHours.Height = 0.1666667F;
			this.txtGroupHours.Left = 9.2F;
			this.txtGroupHours.Name = "txtGroupHours";
			this.txtGroupHours.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupHours.Text = "Field15";
			this.txtGroupHours.Top = 0.06666667F;
			this.txtGroupHours.Width = 0.6333333F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 3.166667F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 0.06666667F;
			this.Line14.Width = 6.7F;
			this.Line14.X1 = 3.166667F;
			this.Line14.X2 = 9.866667F;
			this.Line14.Y1 = 0.06666667F;
			this.Line14.Y2 = 0.06666667F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.2F;
			this.Field16.Left = 3.166667F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field16.Text = "Group Totals";
			this.Field16.Top = 0.06666667F;
			this.Field16.Width = 1F;
			// 
			// txtGroupGross
			// 
			this.txtGroupGross.Height = 0.1666667F;
			this.txtGroupGross.Left = 5.041667F;
			this.txtGroupGross.Name = "txtGroupGross";
			this.txtGroupGross.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGroupGross.Text = "Field15";
			this.txtGroupGross.Top = 0.0625F;
			this.txtGroupGross.Visible = false;
			this.txtGroupGross.Width = 1.364583F;
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.1666667F;
			this.txtTotalAmount.Left = 4.375F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalAmount.Text = "Field15";
			this.txtTotalAmount.Top = 0.2083333F;
			this.txtTotalAmount.Width = 0.65625F;
			// 
			// txtTotalRegular
			// 
			this.txtTotalRegular.Height = 0.1666667F;
			this.txtTotalRegular.Left = 5.03125F;
			this.txtTotalRegular.Name = "txtTotalRegular";
			this.txtTotalRegular.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalRegular.Text = "Field15";
			this.txtTotalRegular.Top = 0.2083333F;
			this.txtTotalRegular.Width = 0.71875F;
			// 
			// txtTotalOther
			// 
			this.txtTotalOther.Height = 0.1666667F;
			this.txtTotalOther.Left = 5.75F;
			this.txtTotalOther.Name = "txtTotalOther";
			this.txtTotalOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalOther.Text = "Field15";
			this.txtTotalOther.Top = 0.2083333F;
			this.txtTotalOther.Width = 0.65625F;
			// 
			// txtTotalTaxes
			// 
			this.txtTotalTaxes.Height = 0.1666667F;
			this.txtTotalTaxes.Left = 6.4375F;
			this.txtTotalTaxes.Name = "txtTotalTaxes";
			this.txtTotalTaxes.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalTaxes.Text = "Field15";
			this.txtTotalTaxes.Top = 0.2083333F;
			this.txtTotalTaxes.Width = 0.65625F;
			// 
			// txtTotalDeduction
			// 
			this.txtTotalDeduction.Height = 0.1666667F;
			this.txtTotalDeduction.Left = 7.125F;
			this.txtTotalDeduction.Name = "txtTotalDeduction";
			this.txtTotalDeduction.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalDeduction.Text = "Field15";
			this.txtTotalDeduction.Top = 0.2083333F;
			this.txtTotalDeduction.Width = 0.65625F;
			// 
			// txtTotalVacUsed
			// 
			this.txtTotalVacUsed.Height = 0.1666667F;
			this.txtTotalVacUsed.Left = 7.8125F;
			this.txtTotalVacUsed.Name = "txtTotalVacUsed";
			this.txtTotalVacUsed.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalVacUsed.Text = "Field15";
			this.txtTotalVacUsed.Top = 0.2083333F;
			this.txtTotalVacUsed.Width = 0.6875F;
			// 
			// txtTotalSickUsed
			// 
			this.txtTotalSickUsed.Height = 0.1666667F;
			this.txtTotalSickUsed.Left = 8.46875F;
			this.txtTotalSickUsed.Name = "txtTotalSickUsed";
			this.txtTotalSickUsed.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalSickUsed.Text = "Field15";
			this.txtTotalSickUsed.Top = 0.2083333F;
			this.txtTotalSickUsed.Width = 0.65625F;
			// 
			// txtTotalHours
			// 
			this.txtTotalHours.Height = 0.1666667F;
			this.txtTotalHours.Left = 9.1875F;
			this.txtTotalHours.Name = "txtTotalHours";
			this.txtTotalHours.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalHours.Text = "Field15";
			this.txtTotalHours.Top = 0.2083333F;
			this.txtTotalHours.Width = 0.65625F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.2083333F;
			this.Field15.Left = 3.15625F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field15.Text = "Grand Totals";
			this.Field15.Top = 0.2083333F;
			this.Field15.Width = 1F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 3.125F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 0.1666667F;
			this.Line13.Width = 6.71875F;
			this.Line13.X1 = 3.125F;
			this.Line13.X2 = 9.84375F;
			this.Line13.Y1 = 0.1666667F;
			this.Line13.Y2 = 0.1666667F;
			// 
			// txtTotGross
			// 
			this.txtTotGross.Height = 0.1666667F;
			this.txtTotGross.Left = 5.041667F;
			this.txtTotGross.Name = "txtTotGross";
			this.txtTotGross.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotGross.Text = "Field15";
			this.txtTotGross.Top = 0.2083333F;
			this.txtTotGross.Visible = false;
			this.txtTotGross.Width = 1.364583F;
			// 
			// rptCheckListingRegular
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEndedAndCanceled += ActiveReport_ReportEndedAndCanceled;
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.916667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblSort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSick)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateField)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVoid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupTaxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupVacUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupSickUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTaxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDeduction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalVacUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSickUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotGross)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVacUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSick;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateField;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVoid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGross;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDeduction;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalVacUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSickUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotGross;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSort;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblGross;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupVacUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupSickUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupHours;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupGross;
	}
}
