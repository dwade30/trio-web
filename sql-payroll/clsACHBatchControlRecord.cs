﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsACHBatchControlRecord
	{
		//=========================================================
		private string strLastError = "";
		private bool boolBalanced;
		private int intEntryAddendaCount;
		private double dblHash;
		private double dblTotalDebit;
		private double dblTotalCredit;
		private string strCompanyID = "";
		private string strMessageAuthenticationCode = "";
		private string strOriginatingDFI = "";
		private int intBatchNumber;
		// vbPorter upgrade warning: strACHCompanyIDPrefix As object	OnWrite(string)
		private object strACHCompanyIDPrefix;

		public string RecordCode
		{
			get
			{
				string RecordCode = "";
				RecordCode = "8";
				return RecordCode;
			}
		}

		public string LastError
		{
			get
			{
				string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public bool Balanced
		{
			get
			{
				bool Balanced = false;
				Balanced = boolBalanced;
				return Balanced;
			}
			set
			{
				boolBalanced = value;
			}
		}

		public int EntryAddendaCount
		{
			get
			{
				int EntryAddendaCount = 0;
				EntryAddendaCount = intEntryAddendaCount;
				return EntryAddendaCount;
			}
			set
			{
				intEntryAddendaCount = value;
			}
		}

		public double Hash
		{
			get
			{
				double Hash = 0;
				Hash = dblHash;
				return Hash;
			}
			set
			{
				dblHash = value;
			}
		}

		public double TotalDebit
		{
			get
			{
				double TotalDebit = 0;
				TotalDebit = dblTotalDebit;
				return TotalDebit;
			}
			set
			{
				dblTotalDebit = value;
			}
		}

		public double TotalCredit
		{
			get
			{
				double TotalCredit = 0;
				TotalCredit = dblTotalCredit;
				return TotalCredit;
			}
			set
			{
				dblTotalCredit = value;
			}
		}

		public string CompanyID
		{
			get
			{
				string CompanyID = "";
				CompanyID = strCompanyID;
				return CompanyID;
			}
			set
			{
				strCompanyID = value;
			}
		}

		public string MessageAuthenticationCode
		{
			get
			{
				string MessageAuthenticationCode = "";
				MessageAuthenticationCode = strMessageAuthenticationCode;
				return MessageAuthenticationCode;
			}
			set
			{
				strMessageAuthenticationCode = value;
			}
		}

		public string OriginatingDFI
		{
			get
			{
				string OriginatingDFI = "";
				OriginatingDFI = strOriginatingDFI;
				return OriginatingDFI;
			}
			set
			{
				strOriginatingDFI = value;
			}
		}

		public int BatchNumber
		{
			get
			{
				int BatchNumber = 0;
				BatchNumber = intBatchNumber;
				return BatchNumber;
			}
			set
			{
				intBatchNumber = value;
			}
		}

		public string ACHCompanyIDPrefix
		{
			get
			{
				string ACHCompanyIDPrefix = "";
				ACHCompanyIDPrefix = FCConvert.ToString(strACHCompanyIDPrefix);
				return ACHCompanyIDPrefix;
			}
			set
			{
				strACHCompanyIDPrefix = value;
			}
		}

		public string ServiceClassCode
		{
			get
			{
				string ServiceClassCode = "";
				if (boolBalanced)
				{
					ServiceClassCode = "200";
				}
				else
				{
					ServiceClassCode = "220";
				}
				return ServiceClassCode;
			}
		}

		public clsACHBatchControlRecord() : base()
		{
			boolBalanced = false;
			strACHCompanyIDPrefix = "1";
			intBatchNumber = 0;
			strLastError = "";
			dblTotalDebit = 0;
			dblTotalCredit = 0;
			dblHash = 0;
			intEntryAddendaCount = 0;
			strCompanyID = "";
		}

		public string OutputLine()
		{
			string OutputLine = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strLine;
				strLastError = "";
				strLine = RecordCode;
				strLine += ServiceClassCode;
				strLine += Strings.Right(Strings.StrDup(6, "0") + FCConvert.ToString(intEntryAddendaCount), 6);
				strLine += Strings.Right(Strings.StrDup(10, "0") + FCConvert.ToString(dblHash), 10);
				strLine += Strings.Right(Strings.StrDup(12, "0") + FCConvert.ToString(dblTotalDebit * 100), 12);
				strLine += Strings.Right(Strings.StrDup(12, "0") + FCConvert.ToString(dblTotalCredit * 100), 12);
				strLine += Strings.Right(strACHCompanyIDPrefix + strCompanyID, 10);
				strLine += Strings.Left(strMessageAuthenticationCode + Strings.StrDup(19, " "), 19);
				strLine += Strings.StrDup(6, " ");
				// reserved
				strLine += Strings.Left(strOriginatingDFI + Strings.StrDup(8, "0"), 8);
				strLine += Strings.Right(Strings.StrDup(7, "0") + FCConvert.ToString(intBatchNumber), 7);
				OutputLine = strLine;
				return OutputLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = "Could not build batch header record." + "\r\n" + fecherFoundation.Information.Err(ex).Description;
			}
			return OutputLine;
		}
	}
}
