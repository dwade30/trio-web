//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using SharedApplication.Extensions;

namespace TWPY0000
{
	public class cCheckService
	{
		//=========================================================
		private cBankService bankService = new cBankService();
		cPayRunController prc = new cPayRunController();
		cPayrollBankController payBankController = new cPayrollBankController();
		cBankService budBankController = new cBankService();
		cEarnedTimeService earnServ = new cEarnedTimeService();
		cCustomCheckController customCheckController = new cCustomCheckController();
		cEmployeeService empServ = new cEmployeeService();

		public void FillEmployeeChecksReport(ref cEmployeeChecksReport checksReport, ref cGenericCollection selectedEmployees)
		{
			if (!(checksReport == null))
			{
				cPayRun temp;
				checksReport.Payrun = prc.GetPayRun(checksReport.PayDate, checksReport.PayrunNumber);
				//App.DoEvents();
				if (checksReport.CheckFormatID > 0)
				{
					checksReport.checkFormat = customCheckController.GetFormat(checksReport.CheckFormatID);
				}
				// Call GetEmployeeChecks(checksReport.PayDate, checksReport.PayrunNumber, checksReport.employeeChecks, selectedEmployees)
				GetEmployeeChecks(checksReport, ref selectedEmployees);
				if (checksReport.Payrun.BankID > 0)
				{
					checksReport.BankID = checksReport.Payrun.BankID;
					if (!modGlobalConstants.Statics.gboolBD)
					{
						cPayrollBank pBank;
						pBank = payBankController.GetBank(checksReport.BankID);
						checksReport.BankName = pBank.Name;
					}
					else
					{
						cBank bBank;
						bBank = budBankController.GetBankByNumber(checksReport.BankID);
						checksReport.BankName = bBank.Name;
					}
				}
			}
		}

		private void GetEmployeeChecks(cEmployeeChecksReport checksReport, ref cGenericCollection selectedEmployees)
		{
            try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsChecks = null; ;
                clsDRWrapper rsPay = null;
				clsDRWrapper rsYTDDeductions = null;
				clsDRWrapper rsEMatch = null;
				clsDRWrapper rsDeductions = null;
				clsDRWrapper rsBanks = new clsDRWrapper();
				clsDRWrapper rsStates = new clsDRWrapper();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsDeductionCodes = new clsDRWrapper();
				double dblTotDeductions = 0;
				double dblTotalYTDDeductions = 0;
				clsDRWrapper rsDirectDeposits = new clsDRWrapper();
				clsDRWrapper rsVacSick = null;
				clsDRWrapper rsBenefits = null;
				int[] arPayCodes = new int[100 + 1];
                string[] arPayDescriptions = new string[100 + 1];
				string[] arPayTypes = new string[100 + 1];
                int x;
                int intNumPays;
				int intRow = 0;
				cEmployeeCheck eCheck;
                double dblTotHours = 0;
				cTax tempTax;
				cCheckDeposit tempDeposit;
				cPayrollBank tempBank;
				double dblNet;
				double dblDirectDeposit = 0;
				cVacSickItem tempEarnedTime;
				cEarnedTimeType earnType;
				DateTime dtPayDateToUse;
				int intPayRun;
				cGenericCollection eChecks;
				eChecks = checksReport.employeeChecks;
				intPayRun = checksReport.PayrunNumber;
				dtPayDateToUse = checksReport.PayDate;
				eChecks.ClearList();
				cPayRun pRun;
				pRun = prc.GetPayRun(dtPayDateToUse, intPayRun);
				rsLoad.OpenRecordset("select * from tblpaycategories", "payroll");
				intNumPays = rsLoad.RecordCount();
				while (!rsLoad.EndOfFile())
				{
					//App.DoEvents();
					arPayDescriptions[rsLoad.Get_Fields("categorynumber")] = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("description")));
					arPayCodes[rsLoad.Get_Fields("categorynumber")] = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
					arPayTypes[rsLoad.Get_Fields("categorynumber")] = FCConvert.ToString(rsLoad.Get_Fields("Type"));
					rsLoad.MoveNext();
				}
                rsDeductionCodes = GetDeductionCodes();
                rsBanks.OpenRecordset("select * from tblbanks order by id", "Payroll");
				rsStates.OpenRecordset("Select ID,state from states order by id", "Payroll");
				rsChecks = GetEmpCheckTotals(dtPayDateToUse, intPayRun);
				rsPay = GetEmpPayDistributions(dtPayDateToUse, intPayRun, checksReport.checkFormat.ShowDistributionRate);
				rsDeductions = GetEmployeeDeductions(dtPayDateToUse, intPayRun);
				cGenericCollection earnTypes;
				earnTypes = earnServ.GetCodeTypes();
				cPayStubItem payDist;
				double dblTotGross;
				dblTotGross = 0;
				dblNet = 0;
				cEmployee checkEmployee;
				bool boolUseEmployee = false;
				cEmployee tempEmployee;
				int lngCheckID;
				lngCheckID = 0;
				while (!rsChecks.EndOfFile())
				{
					//App.DoEvents();
					boolUseEmployee = false;
					if (!(selectedEmployees == null))
					{
						selectedEmployees.MoveFirst();
						while (selectedEmployees.IsCurrent())
						{
							//App.DoEvents();
							tempEmployee = (cEmployee)selectedEmployees.GetCurrentItem();
							if (tempEmployee.EmployeeNumber == rsChecks.Get_Fields_String("Employeenumber"))
							{
								boolUseEmployee = true;
								break;
							}
							selectedEmployees.MoveNext();
						}
					}
					else
					{
						boolUseEmployee = true;
					}
					if (boolUseEmployee)
					{
						dblTotGross = 0;
						dblNet = 0;
						eCheck = new cEmployeeCheck();
						lngCheckID += 1;
						eCheck.ID = lngCheckID;
						eCheck.Selected = true;
						eCheck.EmployeeNumber = rsChecks.Get_Fields_String("employeenumber");
						checkEmployee = empServ.GetEmployeeByNumber(eCheck.EmployeeNumber);
						eCheck.Employee = checkEmployee;
						eCheck.PayDate = dtPayDateToUse;
						eCheck.CheckDate = dtPayDateToUse;
						eCheck.CheckNumber = rsChecks.Get_Fields_Int32("CheckNumber");
						eCheck.PayRun.PayDate = pRun.PayDate;
						eCheck.PayRun.PayRunID = pRun.PayRunID;
						eCheck.PayRun.PeriodStart = pRun.PeriodStart;
						eCheck.PayRun.PeriodEnd = pRun.PeriodEnd;
						eCheck.DepartmentDivision = rsChecks.Get_Fields_String("DeptDiv");
                        eCheck.EmployeeName = checkEmployee.FullName;
						eCheck.SSN = checkEmployee.SSN;
						eCheck.YearToDateGross = rsChecks.Get_Fields_Double("ytdgross");
						if (FCConvert.ToBoolean(rsChecks.Get_Fields_Boolean("PrintPayRate")))
						{
							eCheck.BasePayrate = rsChecks.Get_Fields_Double("BasePayRate").RoundToMoney();
						}
						else
						{
							eCheck.BasePayrate = 0;
						}
						dblDirectDeposit = 0;
						rsEMatch = GetEmployersMatch(dtPayDateToUse, intPayRun, eCheck.EmployeeNumber, rsChecks.Get_Fields_String("masterrecord"));
						if (!rsEMatch.EndOfFile())
						{
							eCheck.BenefitsTotal = Conversion.Val(rsEMatch.Get_Fields("CurrentMatchAmount"));
							eCheck.YearToDateBenefitsTotal = Conversion.Val(rsEMatch.Get_Fields("YTDMatch"));
						}
						else
						{
							eCheck.YearToDateBenefitsTotal = 0;
							eCheck.BenefitsTotal = 0;
						}
						if (rsPay.FindFirst("employeenumber = '" + eCheck.EmployeeNumber + "' and masterrecord = '" + rsChecks.Get_Fields_String("Masterrecord") + "'"))
						{
							while (rsPay.Get_Fields_String("employeenumber") == eCheck.EmployeeNumber && (rsChecks.Get_Fields("masterrecord") == rsPay.Get_Fields("masterrecord")))
							{
								intRow = 0;
								//App.DoEvents();
								for (x = 1; x <= intNumPays; x++)
								{
									//App.DoEvents();
									if (arPayCodes[x] == Conversion.Val(rsPay.Get_Fields("distpaycategory")))
									{
										intRow = x;
										break;
									}
								}
								// x
								payDist = new cPayStubItem();
								payDist.Description = arPayDescriptions[intRow];
								payDist.Amount = rsPay.Get_Fields_Double("DGrossPay").RoundToMoney();
								payDist.PayRate = rsPay.Get_Fields_Double("distpayrate").RoundToMoney();
								dblTotGross += payDist.Amount;
								if (fecherFoundation.Strings.UCase(arPayTypes[intRow]) != "DOLLARS")
								{
									payDist.IsDollars = false;
                                    var dHours = rsPay.Get_Fields_Double("Distributionhours").RoundPlaces(2);
									if (fecherFoundation.Strings.UCase(arPayTypes[intRow]) != "NON-PAY (HOURS)")
                                    {
                                        dblTotHours += dHours;
                                        payDist.Hours = dHours;
                                    }
									else
									{
										if (dHours < 0)
                                        {
                                            payDist.Hours = dHours * -1;
										}
										else
                                        {
                                            payDist.Hours = dHours;
										}
									}
								}
								else
								{
									payDist.IsDollars = true;
								}
								eCheck.PayItems.AddItem(payDist);
								rsPay.MoveNext();
								if (rsPay.EndOfFile())
									break;
							}
							eCheck.CurrentGross = dblTotGross;
						}
						dblTotalYTDDeductions = 0;
						cPayStubItem dedItem;
						if (rsDeductions.FindFirst("employeenumber = '" + eCheck.EmployeeNumber + "' and masterrecord = '" + rsChecks.Get_Fields_String("masterrecord") + "' "))
						{
							while (rsDeductions.Get_Fields_String("employeenumber") == eCheck.EmployeeNumber && (rsDeductions.Get_Fields_String("masterrecord") == rsChecks.Get_Fields_String("masterrecord") || (rsDeductions.Get_Fields_String("masterrecord").Trim() == "" && rsChecks.Get_Fields_String("masterrecord").ToUpper() == "P0")))
							{
								if ((rsDeductions.Get_Fields_Double("deductionamount") > 0) || (rsDeductions.Get_Fields_Double("ytddeduction") > 0))
								{
                                    dedItem = new cPayStubItem();
									if (rsDeductionCodes.FindFirst("id = " + rsDeductions.Get_Fields_Int32("DeductionCode")))
									{
										dedItem.Description = rsDeductionCodes.Get_Fields_String("description");
									}

                                    dedItem.Amount = rsDeductions.Get_Fields_Double("DeductionAmount").RoundToMoney();
									dedItem.total = rsDeductions.Get_Fields_Double("YTDDeduction").RoundToMoney();
									dblTotDeductions += rsDeductions.Get_Fields_Double("deductionamount");
									dblTotalYTDDeductions += rsDeductions.Get_Fields_Double("YTDDeduction");
									eCheck.DEDUCTIONS.AddItem(dedItem);
								}
								rsDeductions.MoveNext();
								if (rsDeductions.EndOfFile())
									break;
							}
						}
						dblTotalYTDDeductions = 0;
						rsYTDDeductions = GetEmpYTDDeductions(dtPayDateToUse, intPayRun, eCheck.EmployeeNumber);
						while (!rsYTDDeductions.EndOfFile())
						{
							dblTotalYTDDeductions += rsYTDDeductions.Get_Fields_Double("YTDDeduction");
							rsYTDDeductions.MoveNext();
						}
						eCheck.YearToDateDeductions = dblTotalYTDDeductions;
						cPayStubItem benItem;
						cPayStubItem tempBen;
						rsBenefits = GetEmployeeBenefits(dtPayDateToUse, intPayRun, eCheck.EmployeeNumber, rsChecks.Get_Fields("Masterrecord"));
						while (!rsBenefits.EndOfFile())
						{
							//App.DoEvents();
							if (rsBenefits.Get_Fields_Double("CurrentMatchAmount") != 0 || rsBenefits.Get_Fields_Double("YTDMatch") != 0)
							{
								benItem = new cPayStubItem();
								if (rsDeductionCodes.FindFirst("id = " + rsBenefits.Get_Fields_Int32("matchdeductionnumber")))
								{
									benItem.Description = FCConvert.ToString(rsDeductionCodes.Get_Fields_String("Description"));
								}
								benItem.Amount = rsBenefits.Get_Fields_Double("CurrentMatchamount").RoundToMoney();
								benItem.total = rsBenefits.Get_Fields_Double("YTDMatch").RoundToMoney();
								benItem.ID = rsBenefits.Get_Fields_Int32("matchdeductionnumber");
								eCheck.Benefits.MoveFirst();
								while (eCheck.Benefits.IsCurrent())
								{
									//App.DoEvents();
									tempBen = (cPayStubItem)eCheck.Benefits.GetCurrentItem();
									if (string.Compare(tempBen.Description, benItem.Description) > 0)
									{
										break;
									}
									eCheck.Benefits.MoveNext();
								}
								eCheck.Benefits.InsertItemBefore(benItem);
							}
							rsBenefits.MoveNext();
						}
						tempTax = new cTax();
						tempTax.Name = "fica";
                        tempTax.Tax = rsChecks.Get_Fields_Double("ficataxwh");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "medicare";
						tempTax.Tax = rsChecks.Get_Fields_Double("medicaretaxwh");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "state";
						tempTax.Tax = rsChecks.Get_Fields_Double("statetaxwh");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "federal";
						tempTax.Tax = rsChecks.Get_Fields_Double("federaltaxwh");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "ytdfederal";
						tempTax.Tax = rsChecks.Get_Fields_Double("YTDFedTax");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "ytdfica";
						tempTax.Tax = rsChecks.Get_Fields_Double("YTDFICA");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "ytdmedicare";
						tempTax.Tax = rsChecks.Get_Fields_Double("YTDMedicare");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "ytdstate";
						tempTax.Tax = rsChecks.Get_Fields_Double("YTDStateTax");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "ytdfederalgross";
						tempTax.Tax = rsChecks.Get_Fields_Double("YTDFederalTaxGross");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "ytdstategross";
						tempTax.Tax = rsChecks.Get_Fields_Double("YTDStateTaxGross");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "ytdficagross";
						tempTax.Tax = rsChecks.Get_Fields_Double("YTDFicaGross");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						tempTax = new cTax();
						tempTax.Name = "ytdmedicaregross";
						tempTax.Tax = rsChecks.Get_Fields_Double("YTDMedicareGross");
						eCheck.EmployeeTaxes.Add(tempTax.Name, tempTax);
						eCheck.YearToDateNet = (rsChecks.Get_Fields_Double("ytdgross") - rsChecks.Get_Fields_Double("ytdfedtax") - rsChecks.Get_Fields_Double("ytdfica") - rsChecks.Get_Fields_Double("ytdstatetax") - rsChecks.Get_Fields_Double("YTDMedicare") - dblTotalYTDDeductions).RoundToMoney();
                        dblNet = rsChecks.Get_Fields_Double("netpay");
						eCheck.CheckingAmount = rsChecks.Get_Fields_Double("ddepositchkamount");
						eCheck.SavingsAmount = rsChecks.Get_Fields_Double("ddepositsavamount");
						dblDirectDeposit = eCheck.CheckingAmount + eCheck.SavingsAmount;
						eCheck.Net = dblNet;
						eCheck.CheckAmount = eCheck.Net - dblDirectDeposit;
						if (eCheck.CheckAmount < 0)
						{
							eCheck.CheckAmount = 0;
						}
						if (eCheck.CheckAmount < 0.01)
						{
							eCheck.NonNegotiable = true;
						}
						if (checksReport.AllNonNegotiable)
						{
							eCheck.NonNegotiable = true;
						}
						rsDirectDeposits = GetEmployeeDirectDeposits(dtPayDateToUse, intPayRun, eCheck.EmployeeNumber);
						while (!rsDirectDeposits.EndOfFile())
						{
							//App.DoEvents();
							tempDeposit = new cCheckDeposit();
							tempDeposit.Amount = rsDirectDeposits.Get_Fields("ddepositamount");
							tempDeposit.BankNumber = FCConvert.ToInt32(rsDirectDeposits.Get_Fields("ddbanknumber"));
							tempBank = payBankController.GetBank(tempDeposit.BankNumber);
							tempDeposit.Bank = tempBank.Name;
							eCheck.Deposits.AddItem(tempDeposit);
							rsDirectDeposits.MoveNext();
						}
						rsVacSick = GetEmployeeEarnedTime(dtPayDateToUse, intPayRun, eCheck.EmployeeNumber);
						rsVacSick.MoveFirst();
						while (!rsVacSick.EndOfFile())
						{
							//App.DoEvents();
							tempEarnedTime = new cVacSickItem();
							tempEarnedTime.Accrued = rsVacSick.Get_Fields_Double("accruedCurrent");
							tempEarnedTime.Used = rsVacSick.Get_Fields_Double("usedCurrent");
							tempEarnedTime.Balance = rsVacSick.Get_Fields_Double("usedbalance");
							tempEarnedTime.CalendarAccrued = rsVacSick.Get_Fields_Double("accruedcalendar");
							tempEarnedTime.CalendarUsed = rsVacSick.Get_Fields_Double("usedcalendar");
							tempEarnedTime.ID = rsVacSick.Get_Fields_Int32("id");
							tempEarnedTime.TypeID = rsVacSick.Get_Fields_Int32("TypeID");
							earnTypes.MoveFirst();
							while (earnTypes.IsCurrent())
							{
                                earnType = (cEarnedTimeType)earnTypes.GetCurrentItem();
								if (earnType.ID == tempEarnedTime.TypeID)
								{
									tempEarnedTime.Name = earnType.Description;
									break;
								}
								earnTypes.MoveNext();
							}
							eCheck.EarnedTime.AddItem(tempEarnedTime);
							rsVacSick.MoveNext();
						}
						eChecks.AddItem(eCheck);
					}
					rsChecks.MoveNext();
				}

                rsChecks.Dispose();
                rsPay.Dispose();
                rsYTDDeductions?.Dispose();
                rsEMatch?.Dispose();
                rsDeductions.Dispose();
                rsBanks.Dispose();
                rsStates.Dispose();
                rsLoad.Dispose();
                rsDeductionCodes.Dispose();
                rsDirectDeposits.Dispose();
                rsVacSick?.Dispose();
                rsBenefits?.Dispose();
				return;
			}
			catch (Exception ex)
			{
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private clsDRWrapper GetDeductionCodes()
		{
			clsDRWrapper GetDeductionCodes = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from tbldeductionsetup", "payroll");
			GetDeductionCodes = rsLoad;
			return GetDeductionCodes;
		}

		private clsDRWrapper GetEmpCheckTotals(DateTime dtPayDate, int intPayrunToUse)
		{
			clsDRWrapper GetEmpCheckTotals = null;
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strPayQuery1;
			string strPayQuery1a;
			string strPayQuery2;
			string strPayQuery3;
			string strPayQuery4;
			string strPayQuery5;
			string strPayQuery6;
            DateTime dtStart;
			string strWhere;
			string strOrderBy;
			strWhere = " checknumber > 0 ";
			strOrderBy = " payrunid,tblcheckdetail.employeenumber,lastname,firstname,tblcheckdetail.masterrecord";
			strSQL = "select employeenumber,masterrecord,sum( ddamount) as DDepositChkAmount from tblcheckdetail where checkvoid = 0 and paydate = '" + dtPayDate + "' and payrunid = " + intPayrunToUse + "  and (isnull(DDACCOUNTTYPE ,'') = '22' or isnull(ddaccounttype, '')  = '  ') group by employeenumber,masterrecord";
			strPayQuery1 = "(" + strSQL + ") as CheckPayQuery1 ";
			strSQL = "select employeenumber,masterrecord as masterrecorda,sum( ddamount) as DDepositSavAmount from tblcheckdetail where checkvoid = 0 and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayrunToUse) + " and isnull(DDACCOUNTTYPE ,'') = '32' group by employeenumber,masterrecord";
			strPayQuery1a = "(" + strSQL + ") as CheckPayQuery1a ";
			// now combine these two as one so we can right join wiht the rest
			dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
			strSQL = "select employeenumber,sum(distgrosspay) as YTDGross from tblcheckdetail where checkvoid = 0  and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber HAVING SUM( distgrosspay) > 0";
			strPayQuery2 = "(" + strSQL + ") as CheckPayQuery2 ";
			strSQL = "select employeenumber,sum( FederalTaxWH) as YTDFedTax from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber ";
			strPayQuery3 = "(" + strSQL + ") as CheckPayQuery3 ";
			strSQL = "select employeenumber,sum( ficataxwh) as YTDFica, sum(medicaretaxwh) as YTDMedicare, sum (medicaretaxgross) as YTDMedicareGross, sum(FicaTaxGross) as YTDFicaGross, sum(FederalTaxGross) as YTDFederalTaxGross, sum(StateTaxGross) as YTDStateTaxGross from tblcheckdetail where totalrecord = 1 and  checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber ";
			strPayQuery4 = "(" + strSQL + ") as CheckPayQuery4 ";
			strSQL = "select employeenumber,sum( statetaxwh) as ytdstatetax from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber ";
			strPayQuery5 = "(" + strSQL + ") as CheckPayQuery5 ";
			strSQL = "select MasterRecord, CheckPayQuery5.EmployeeNumber as CheckPayQuery5EmployeeNumber, DDepositSavAmount, DDepositChkAmount, YTDGross, YTDFedTax, YTDFICA, ytdmedicare, YTDStateTax, YTDFederalTaxGross, YTDStateTaxGross, YTDMedicareGross, YTDFicaGross  from " + strPayQuery1a + " right join (" + strPayQuery1 + " right join (" + strPayQuery2 + " inner join (" + strPayQuery3 + " inner join (" + strPayQuery4 + " inner join " + strPayQuery5 + " on (checkpayquery4.employeenumber = checkpayquery5.employeenumber)) on (checkpayquery3.employeenumber = checkpayquery4.employeenumber))";
			strSQL += " on (checkpayquery2.employeenumber = checkpayquery3.employeenumber)) on (checkpayquery1.employeenumber = checkpayquery2.employeenumber)) on (checkpayquery1a.employeenumber = checkpayquery1.employeenumber) and (checkpayquery1a.masterrecorda = checkpayquery1.masterrecord";
			strPayQuery6 = "(" + strSQL + ")) as CheckPayQuery6 ";
			strSQL = "select * from " + strPayQuery6 + " inner join (tblCheckDetail inner join tblemployeemaster on (tblCheckDetail.employeenumber = tblemployeemaster.employeenumber)) on (CheckPayQuery5EmployeeNumber = tblCheckDetail.employeenumber) AND (checkpayquery6.masterrecord = tblcheckdetail.masterrecord) where tblCheckDetail.paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) + " and totalrecord = 1 and " + strWhere + "  order by " + strOrderBy;
			rsLoad.OpenRecordset(strSQL, "Payroll");
			GetEmpCheckTotals = rsLoad;
			return GetEmpCheckTotals;
		}

		private clsDRWrapper GetEmpPayDistributions(DateTime dtPayDate, int intPayrunToUse, bool boolShowDistRate)
		{
			clsDRWrapper GetEmpPayDistributions = null;
			string strSQL = "";
			clsDRWrapper rsPay = new clsDRWrapper();
			string strWhere;
			strWhere = " checknumber > 0 ";
			if (!boolShowDistRate)
			{
				strSQL = "select employeenumber,masterrecord,distpaycategory,sum(distgrosspay) as dgrosspay,sum(disthours) as distributionhours ,0 as distpayrate from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) + " and " + strWhere + " and distributionrecord = 1 group by employeenumber,masterrecord,distpaycategory order by employeenumber,masterrecord,distpaycategory";
			}
			else
			{
				strSQL = "select employeenumber,masterrecord,distpaycategory,sum(distgrosspay) as dgrosspay,sum(disthours) as distributionhours, distpayrate from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) + " and " + strWhere + " and distributionrecord = 1 group by employeenumber,masterrecord,distpaycategory,distpayrate order by employeenumber,masterrecord,distpaycategory";
			}
			rsPay.OpenRecordset(strSQL, "payroll");
			GetEmpPayDistributions = rsPay;
			return GetEmpPayDistributions;
		}

		private clsDRWrapper GetEmpYTDDeductions(DateTime dtPayDate, int intPayrunToUse, string strEmployeeNumber)
		{
			clsDRWrapper GetEmpYTDDeductions = null;
			string strSQL;
			clsDRWrapper clsYTDDeductions = new clsDRWrapper();
            DateTime dtStart;
			dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
			strSQL = "select sum(dedamount) as YTDDeduction,deddeductionnumber as deductioncode from tblcheckdetail where checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by deddeductionnumber ";
			clsYTDDeductions.OpenRecordset(strSQL + " order by deddeductionnumber ", "Payroll");
			GetEmpYTDDeductions = clsYTDDeductions;
			return GetEmpYTDDeductions;
		}

		private clsDRWrapper GetEmployersMatch(DateTime dtPayDate, int intPayrunToUse, string strEmployeeNumber, string strMasterRecord)
		{
			clsDRWrapper GetEmployersMatch = null;
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strWhere;
			string strDed2;
            DateTime dtStart;
			dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
			strWhere = " checknumber > 0 ";
			strSQL = "select employeenumber,masterrecord,sum(matchamount) as CurrentMatchAmount from tblcheckdetail where employeenumber = '" + strEmployeeNumber + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) + " and " + strWhere + " and  matchrecord = 1 group by employeenumber,masterrecord";
			strDed2 = "(" + strSQL + ") AS CheckDedQuery2 ";
			strSQL = "select * from " + strDed2 + " right join (select sum(matchamount) as YTDMatch,employeenumber from tblcheckdetail where checkvoid = 0 and employeenumber = '" + strEmployeeNumber + "' and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber) as CheckDedQuery1 ON  (CheckDedQuery2.employeenumber = CheckDedQuery1.employeenumber) ORDER BY CheckDedQuery1.employeenumber, CheckDedQuery2.masterrecord;";
			rsLoad.OpenRecordset(strSQL, "Payroll");
			GetEmployersMatch = rsLoad;
			return GetEmployersMatch;
		}

		private clsDRWrapper GetEmployeeDeductions(DateTime dtPayDate, int intPayrunToUse)
		{
			clsDRWrapper GetEmployeeDeductions = null;
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strWhere;
			string strDed2;
			string strDed1;
            DateTime dtStart;
			dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
			strWhere = " checknumber > 0 ";
			strSQL = "select sum(dedamount) as YTDDeduction,deddeductionnumber as Query3deductioncode,employeenumber as Query1EmployeeNumber from tblcheckdetail where checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' group by employeenumber,deddeductionnumber";
			strDed1 = "(" + strSQL + ") as CheckDedQuery1 ";
			strSQL = "select employeenumber,masterrecord,deddeductionnumber,sum(dedamount) as deductionamount,empdeductionid from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) + " and " + strWhere + " and  deductionrecord = 1 group by employeenumber,masterrecord,deddeductionnumber,empdeductionid";
			strDed2 = "(" + strSQL + ") as CheckDedQuery2 ";
			strSQL = "select * from (select * from " + strDed2 + " right join " + strDed1 + " on (Query1EmployeeNumber = checkdedquery2.employeenumber) and (checkdedquery2.deddeductionnumber = Query3deductioncode)) as checkdedquery3  LEFT join tblemployeedeductions on (checkdedquery3.empdeductionid = tblemployeedeductions.ID) order by Query1EmployeeNumber,masterrecord DESC,recordnumber";
			rsLoad.OpenRecordset(strSQL, "Payroll");
			GetEmployeeDeductions = rsLoad;
			return GetEmployeeDeductions;
		}

		private clsDRWrapper GetEmployeeBenefits(DateTime dtPayDate, int intPayrunToUse, string strEmployeeNumber, string strMasterRecord)
		{
			clsDRWrapper GetEmployeeBenefits = null;
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strDed2;
			string strWhere;
            DateTime dtStart;
			dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(dtPayDate.Year));
			strWhere = " checknumber > 0 and employeenumber = '" + strEmployeeNumber + "' and masterrecord = '" + strMasterRecord + "' ";
			strSQL = "select matchdeductionnumber, sum(matchamount) as CurrentMatchAmount from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPayDate) + "' and PayRunID = " + FCConvert.ToString(intPayrunToUse) + " and " + strWhere + " and  matchrecord = 1 group by  matchdeductionnumber";
			strDed2 = "(" + strSQL + ") AS CheckDedQuery2 ";
			strSQL = "select checkdedquery1.matchdeductionnumber, CurrentMatchAmount, YTDMatch from  " + strDed2 + " right join (select sum(matchamount) as YTDMatch, matchdeductionnumber from tblcheckdetail where matchrecord = 1 and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtPayDate) + "' and employeenumber = '" + strEmployeeNumber + "' group by  matchdeductionnumber) as CheckDedQuery1 ON   (checkdedquery2.matchdeductionnumber = checkdedquery1.matchdeductionnumber) order by matchdeductionnumber";
			rsLoad.OpenRecordset(strSQL, "Payroll");
			GetEmployeeBenefits = rsLoad;
			return GetEmployeeBenefits;
		}

		private clsDRWrapper GetEmployeeDirectDeposits(DateTime dtPayDate, int intPayrunToUse, string strEmployee)
		{
			clsDRWrapper GetEmployeeDirectDeposits = null;
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			strSQL = "select tblcheckdetail.employeenumber as employeenumber,masterrecord,ddbanknumber, isnull(ddamount, 0) as DDepositAmount from tblcheckdetail inner join tbldirectdeposit on ( tblcheckdetail.ddbanknumber = tbldirectdeposit.bank) and  (tblcheckdetail.ddaccountnumber = tbldirectdeposit.account) and (tblcheckdetail.employeenumber = tbldirectdeposit.employeenumber) where checkvoid = 0 and tblcheckdetail.employeenumber = '" + strEmployee + "' and paydate = '" + FCConvert.ToString(dtPayDate) + "' and payrunid = " + FCConvert.ToString(intPayrunToUse) + "  and isnull(ddamount, 0) > 0 order by masterrecord,rownumber";
			rsLoad.OpenRecordset(strSQL, "Payroll");
			GetEmployeeDirectDeposits = rsLoad;
			return GetEmployeeDirectDeposits;
		}

		private clsDRWrapper GetEmployeeEarnedTime(DateTime dtPayDate, int intPayrunToUse, string strEmployee)
		{
			clsDRWrapper GetEmployeeEarnedTime = null;
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			strSQL = "select id,accruedcurrent, usedCurrent,accruedCalendar,usedCalendar,usedbalance,employeenumber,typeid from tblvacationsick where selected = 1 and employeenumber = '" + strEmployee + "'";
			rsLoad.OpenRecordset(strSQL, "Payroll");
			GetEmployeeEarnedTime = rsLoad;
			return GetEmployeeEarnedTime;
		}
	}
}
