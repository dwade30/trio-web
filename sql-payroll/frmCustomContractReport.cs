//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCustomContractReport : BaseForm
	{
		public frmCustomContractReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomContractReport InstancePtr
		{
			get
			{
				return (frmCustomContractReport)Sys.GetInstance(typeof(frmCustomContractReport));
			}
		}

		protected frmCustomContractReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmCustomContractReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomContractReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomContractReport properties;
			//frmCustomContractReport.FillStyle	= 0;
			//frmCustomContractReport.ScaleWidth	= 9300;
			//frmCustomContractReport.ScaleHeight	= 7710;
			//frmCustomContractReport.LinkTopic	= "Form2";
			//frmCustomContractReport.LockControls	= -1  'True;
			//frmCustomContractReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupcmbStatus();
			SetupEmployee();
		}

		private void SetupcmbStatus()
		{
			cmbStatus.Clear();
			cmbStatus.AddItem("All");
			cmbStatus.AddItem("Outstanding");
			cmbStatus.AddItem("Paid");
			cmbStatus.SelectedIndex = 0;
		}

		private void SetupEmployee()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp = "";
			int lngRow;
			string strList = "";
			txtEmployee.Rows = 1;
			txtEmployee.Cols = 1;
			txtEmployee.RowData(0, "");
			strList = "";
			rsLoad.OpenRecordset("select distinct tblemployeemaster.employeenumber,FIRSTNAME,lastname,middlename,desig from tblemployeemaster inner join contracts on (contracts.employeenumber = tblemployeemaster.employeenumber) order by tblemployeemaster.employeenumber", "twpy0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsLoad.Get_Fields("firstname") + " " + rsLoad.Get_Fields("middlename")) + " " + rsLoad.Get_Fields("lastname") + " " + rsLoad.Get_Fields("desig"));
				strList += rsLoad.Get_Fields("employeenumber") + "\t" + strTemp + "|";
				rsLoad.MoveNext();
			}
			if (strList != string.Empty)
			{
				strList = Strings.Mid(strList, 1, strList.Length - 1);
			}
			txtEmployee.ColComboList(0, strList);
			txtEmployee.Row = 0;
			txtEmployee.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void frmCustomContractReport_Resize(object sender, System.EventArgs e)
		{
			//txtEmployee.Height = txtEmployee.RowHeight(0) + 15;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			PrintReport();
		}

		private void PrintReport()
		{
			string strSQL;
			string strWhere;
			string strWAnd;
			// vbPorter upgrade warning: intStatus As int	OnWriteFCConvert.ToInt32(
			int intStatus;
			// vbPorter upgrade warning: dtAsOf As DateTime	OnWrite(string, int)
			DateTime dtAsOf;
			if (Information.IsDate(t2kAsOf.Text))
			{
				dtAsOf = FCConvert.ToDateTime(t2kAsOf.Text);
			}
			else
			{
				dtAsOf = DateTime.FromOADate(0);
			}
			strWAnd = "Where ";
			strWhere = "";
			intStatus = cmbStatus.SelectedIndex;
			if (fecherFoundation.Strings.Trim(txtEmployee.TextMatrix(txtEmployee.Row, 0)) != string.Empty)
			{
				strWhere += strWAnd + "contracts.employeenumber = '" + txtEmployee.TextMatrix(txtEmployee.Row, 0) + "' ";
				strWAnd = " and ";
			}
			if (Conversion.Val(txtSequence.Text) > 0)
			{
				strWhere += strWAnd + "seqnumber = " + FCConvert.ToString(Conversion.Val(txtSequence.Text));
				strWAnd = " and ";
			}
			if (Conversion.Val(txtGroup.Text) > 0)
			{
				strWhere += strWAnd + "groupID = '" + FCConvert.ToString(Conversion.Val(txtGroup.Text)) + "' ";
				strWAnd = " and ";
			}
			if (Information.IsDate(t2kStartStart.Text))
			{
				if (Information.IsDate(t2kStartEnd.Text))
				{
					strWhere += strWAnd + "startdate >= '" + t2kStartStart.Text + "' and startdate <= '" + t2kStartEnd.Text + "'";
				}
				else
				{
					strWhere += strWAnd + "startdate >= '" + t2kStartStart.Text + "'";
				}
				strWAnd = " and ";
			}
			else if (Information.IsDate(t2kStartEnd.Text))
			{
				strWhere += strWAnd + "startdate <= #" + t2kStartEnd.Text + "#";
				strWAnd = " and ";
			}
			if (Information.IsDate(t2kEndStart.Text))
			{
				if (Information.IsDate(t2kEndEnd.Text))
				{
					strWhere += strWAnd + "enddate >= #" + t2kEndStart.Text + "# and enddate <= #" + t2kEndEnd.Text + "#";
				}
				else
				{
					strWhere += strWAnd + "enddate >= #" + t2kEndStart.Text + "#";
				}
				strWAnd = " and ";
			}
			else if (Information.IsDate(t2kEndEnd.Text))
			{
				strWhere += strWAnd + "enddate <= #" + t2kEndEnd.Text + "#";
				strWAnd = " and ";
			}
			strSQL = "select contracts.*,lastname,firstname,middlename,desig from tblemployeemaster inner join contracts on (tblemployeemaster.employeenumber = contracts.employeenumber) " + strWhere + " order by lastname,firstname,middlename,ID";
			if (cmbType.Text == "Contract Summary")
			{
				// contract summary
				rptCustomContract.InstancePtr.Init(true, ref strSQL, ref intStatus, false, dtAsOf);
			}
			else if (cmbType.Text == "Contract Detail")
			{
				rptCustomContract.InstancePtr.Init(false, ref strSQL, ref intStatus, false, dtAsOf);
				// contract detail
			}
			else
			{
				// distribution detail
				rptCustomContract.InstancePtr.Init(false, ref strSQL, ref intStatus, true, dtAsOf);
			}
		}

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            mnuPrint_Click(cmdPrint, EventArgs.Empty);
        }
    }
}
