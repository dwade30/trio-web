//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Payroll.Enums;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt941Wage.
	/// </summary>
	public partial class rpt941Wage : BaseSectionReport
	{
		public rpt941Wage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "941 Taxable Wage";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt941Wage InstancePtr
		{
			get
			{
				return (rpt941Wage)Sys.GetInstance(typeof(rpt941Wage));
			}
		}

		protected rpt941Wage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsEmployee?.Dispose();
                rsData = null;
                rsEmployee = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt941Wage	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intpage;
		private int intCounter;
		private clsDRWrapper rsData = new clsDRWrapper();
		private clsDRWrapper rsEmployee = new clsDRWrapper();
		private double dblTotalStateGross;
		private double dblTotalFedGross;
		private double dblTotalFICAGross;
		private double dblTotalMedGross;
		private double dblTotalWage;
		private double dblFedTaxWH;
		private double dblStateTaxWH;
		private double dblFICATaxWH;
		private double dblEmployerFicaTax;
		private double dblEmployerMedicareTax;
		private double dblMedTaxWH;
		private double dblMedicareRate;
		private double dblFICARate;
		private double dblEmployerFicaRate;
		private string strBox1Matches = string.Empty;
		int intQuarter;
		int lngYear;
		DateTime dtStartDate;
		DateTime dtEndDate;
        private SSNViewType ssnViewPermission = SSNViewType.None;
        private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			DateTime dtTempDate;
			string strOrderBy = "";
			strBox1Matches = modCoreysSweeterCode.GetBox1Matches();
			// quarterly
			if (intQuarter == 0)
			{
				//FC:FINAL:DSE:#i2421 Use date parameter by reference
				DateTime tmpArg = DateTime.FromOADate(0);
				frmSelectDateInfo.InstancePtr.Init3("Please select the quarter and year to report on.", ref lngYear, ref intQuarter, -1, ref tmpArg, -1, false);
			}
			if (intQuarter != -1)
			{
				modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, intQuarter, lngYear);
				lblDate.Text = "QTD through " + Strings.Format(dtEndDate, "MM/dd/yyyy");
			}
			string strSQL;
			strSQL = "SELECT tblCheckDetail.EmployeeNumber, Sum(tblCheckDetail.StateTaxGross) AS SumOfStateTaxGross, Sum(tblCheckDetail.FederalTaxGross) AS SumOfFederalTaxGross, Sum(tblCheckDetail.FICATaxGross) AS SumOfFICATaxGross, Sum(tblCheckDetail.MedicareTaxGross) AS SumOfMedicareTaxGross, tblCheckDetail.TotalRecord, SumOfGrossPay, Sum(tblCheckDetail.FICATaxWH) AS SumOfFICATaxWH,sum(tblcheckdetail.EmployerFicaTax) as SumOfEmployerFicaTax,sum(tblcheckdetail.EmployerMedicareTax) as SumOfEMployerMedicareTax, Sum(tblCheckDetail.MedicareTaxWH) AS SumOfMedicareTaxWH, Sum(tblCheckDetail.StateTaxWH) AS SumOfStateTaxWH, Sum(tblCheckDetail.FederalTaxWH) AS SumOfFederalTaxWH, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName FROM tblCheckDetail INNER JOIN (tblEmployeeMaster inner join (select employeenumber as employeenum,sum(grosspay) as SumOfGrossPay from tblcheckdetail where (((tblCheckDetail.PayDate) >= '";
			strSQL += FCConvert.ToString(dtStartDate) + "' And (tblCheckDetail.PayDate) <= '" + FCConvert.ToString(dtEndDate) + "')  AND CheckVoid = 0) group by employeenumber) as tbl1 on ";
			strSQL += "(tbl1.employeenum = tblemployeemaster.employeenumber)) ON (tblCheckDetail.EmployeeNumber = tblEmployeeMaster.EmployeeNumber) Where (((tblCheckDetail.PayDate) >= '" + FCConvert.ToString(dtStartDate) + "' And (tblCheckDetail.PayDate) <= '" + FCConvert.ToString(dtEndDate) + "') And ((tblCheckDetail.TotalRecord) = 1) AND CheckVoid = 0) GROUP BY tblCheckDetail.EmployeeNumber, tblCheckDetail.TotalRecord, tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName ,sumofgrosspay ORDER BY tblEmployeeMaster.LastName, tblEmployeeMaster.FirstName";
			rsData.OpenRecordset(strSQL, "twpy0000.vb1");			
			rsEmployee.OpenRecordset("Select EmployeeNumber, SSN, FirstName, LastName from tblEmployeeMaster", "twpy0000.vb1");
            switch (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(PayrollPermissionItem
                .ViewSocialSecurityNumbers.ToInteger()))
            {
                case "F":
                    ssnViewPermission = SSNViewType.Full;
                    break;
                case "P":
                    ssnViewPermission = SSNViewType.Masked;
                    break;
                default:
                    ssnViewPermission = SSNViewType.None;
                    break;
            }
        }

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modDavesSweetCode.UpdateReportStatus("WAGE941");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				rsEmployee.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber"));
				if (rsEmployee.NoMatch)
				{
					dblTotalFedGross = dblTotalFedGross;
				}
				else
				{					
                    var strTempSSN = Strings.Format(rsEmployee.Get_Fields_String("SSN"), "000-00-0000"); ;
                    switch (ssnViewPermission)
                    {
                        case SSNViewType.Full:
                            txtSSN.Text = strTempSSN;
                            break;
                        case SSNViewType.Masked:
                            txtSSN.Text = "***-**-" + "-" + strTempSSN.Right(4);
                            break;
                        default:
                            txtSSN.Text = "***-**-****";
                            break;
                    }
                    txtName.Text = rsEmployee.Get_Fields_String("LastName") + ", " + rsEmployee.Get_Fields_String("FirstName");
				}
				double dblBox1Matches;
				dblBox1Matches = 0;
				dblBox1Matches = modCoreysSweeterCode.GetQTDBox1Matches(strBox1Matches, rsData.Get_Fields("employeenumber"), dtEndDate, dtStartDate);
				txtStateGross.Text = Strings.Format(rsData.Get_Fields("SumOfStateTaxGross") + dblBox1Matches, "#,##0.00");
				txtFedGross.Text = Strings.Format(rsData.Get_Fields("SumOfFederalTaxGross") + dblBox1Matches, "#,##0.00");
				txtFICAGross.Text = Strings.Format(rsData.Get_Fields("SumOfFICATaxGross"), "#,##0.00");
				txtMedGross.Text = Strings.Format(rsData.Get_Fields("SumOfMedicareTaxGross"), "#,##0.00");
				if (txtFedGross.Text == string.Empty)
				{
					txtFedGross.Text = "0.00";
				}
				else
				{
					dblTotalFedGross += FCConvert.ToDouble(txtFedGross.Text);
				}
				if (txtStateGross.Text == string.Empty)
				{
					txtStateGross.Text = "0.00";
				}
				else
				{
					dblTotalStateGross += FCConvert.ToDouble(txtStateGross.Text);
				}
				if (txtFICAGross.Text == string.Empty)
				{
					txtFICAGross.Text = "0.00";
				}
				else
				{
					dblTotalFICAGross += FCConvert.ToDouble(txtFICAGross.Text);
				}
				if (txtMedGross.Text == string.Empty)
				{
					txtMedGross.Text = "0.00";
				}
				else
				{
					dblTotalMedGross += FCConvert.ToDouble(txtMedGross.Text);
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("SumOfGrossPay"))) == string.Empty)
				{
				}
				else
				{
					dblTotalWage += FCConvert.ToDouble(Strings.Format(rsData.Get_Fields("SumOfGrossPay"), "#,##0.00"));
				}
				// NEED TO TAKE THE VAL OF THESE VALUES AS IN SOME CASES THEY ARE ""
				dblFedTaxWH += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields("SumOfFederalTaxWH")), "#,##0.00"));
				dblFICATaxWH += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields("SumOfFICATaxWH")), "#,##0.00"));
				dblEmployerFicaTax += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields("SumOfEmployerFicaTax")), "#,##0.00"));
				dblMedTaxWH += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields("SumOfMedicareTaxWH")), "#,##0.00"));
				dblStateTaxWH += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields("SumOfStateTaxWH")), "#,##0.00"));
				dblEmployerMedicareTax += FCConvert.ToDouble(Strings.Format(Conversion.Val(rsData.Get_Fields("SumOfEmployerMedicareTax")), "#,##0.00"));
				// HERE FOR TESTING MATTHEW 10/30/2003
				// txtMedGross = Format(rsData.Fields("SumOfStateTaxWH"), "#,##0.00")
				if (!rsData.EndOfFile())
					rsData.MoveNext();
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Init(int intQ, int lngY, string strSeq = "")
		{
			// Call frmSelectDateInfo.Init("Please select the pay date you wish to have reported for.", , , , datLowDate)
			intQuarter = intQ;
			lngYear = lngY;
			// frmReportViewer.Init Me
			modCoreysSweeterCode.CheckDefaultPrint(this, boolAllowEmail: false);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtFederalTotal.Text = Strings.Format(dblTotalFedGross, "Currency");
			txtStateTotal.Text = Strings.Format(dblTotalStateGross, "Currency");
			txtFICATotal.Text = Strings.Format(dblTotalFICAGross, "Currency");
			txtMedTotal.Text = Strings.Format(dblTotalMedGross, "Currency");
			txtTotalWage.Text = Strings.Format(dblTotalWage, "Currency");
			txtFedTax.Text = Strings.Format(dblFedTaxWH, "Currency");
			txtSSWage.Text = Strings.Format(dblTotalFICAGross, "Currency");
			txtMCWage.Text = Strings.Format(dblTotalMedGross, "Currency");
			rsData.OpenRecordset("SELECT * FROM tblStandardLimits");
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				dblMedicareRate = rsData.Get_Fields_Double("PercentMedicare");
				dblFICARate = rsData.Get_Fields_Double("PercentSSN");
				dblEmployerFicaRate = rsData.Get_Fields_Double("EmployerFicaRate");
			}
			// txtSSPercent = Format(dblFICARate * 2, "0.0000") & "%"
			txtSSPercent.Text = Strings.Format(dblFICARate + dblEmployerFicaRate, "0.0000") + "%";
			txtMCPercent.Text = Strings.Format(dblMedicareRate * 2, "0.0000") + "%";
			// txtSSTax = Format(txtSSWage * ((dblFICARate * 2) / 100), "Currency")
			txtSSTax.Text = Strings.Format((dblTotalFICAGross * (dblFICARate / 100)) + (dblTotalFICAGross * (dblEmployerFicaRate / 100)), "Currency");
			txtMCTax.Text = Strings.Format(double.Parse(FCConvert.ToString(FCConvert.ToDouble(txtMCWage.Text)), System.Globalization.NumberStyles.Currency) * ((dblMedicareRate * 2) / 100), "Currency");
			txtCalcSSMCTaxWH.Text = Strings.Format(double.Parse(Convert.ToString(FCConvert.ToDouble(Strings.Format(txtSSTax.Text, "Standard"))), System.Globalization.NumberStyles.Currency) + double.Parse(Convert.ToString(FCConvert.ToDouble(Strings.Format(txtMCTax.Text, "Standard"))), System.Globalization.NumberStyles.Currency), "Currency");
			txtSSMCTax.Text = Strings.Format(dblMedTaxWH + dblEmployerMedicareTax + dblFICATaxWH + dblEmployerFicaTax, "Currency");
			txtTotNetTax.Text = Strings.Format(double.Parse(Convert.ToString(FCConvert.ToDouble(Strings.Format(txtFedTax.Text, "Standard"))), System.Globalization.NumberStyles.Currency) + double.Parse(Convert.ToString(FCConvert.ToDouble(Strings.Format(txtSSMCTax.Text, "Standard"))), System.Globalization.NumberStyles.Currency), "Currency");
			txtStateTaxGross.Text = txtStateTotal.Text;
			txtStateIncomeWH.Text = Strings.Format(dblStateTaxWH, "Currency");
		}

		
	}
}
