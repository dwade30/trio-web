//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmployeeMatch.
	/// </summary>
	public partial class srptEmployeeMatch : FCSectionReport
	{
		public srptEmployeeMatch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "sub Employee Match Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptEmployeeMatch InstancePtr
		{
			get
			{
				return (srptEmployeeMatch)Sys.GetInstance(typeof(srptEmployeeMatch));
			}
		}

		protected srptEmployeeMatch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsProcessing?.Dispose();
                rsProcessing = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptEmployeeMatch	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// private local variables
		private clsDRWrapper rsProcessing = new clsDRWrapper();
		private string strEmployee = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: txtAmount As object	OnWrite(string)
			// vbPorter upgrade warning: txtAmountDP As object	OnWrite(string)
			// vbPorter upgrade warning: txtMax As object	OnWrite(string)
			// vbPorter upgrade warning: txtMaxDH As object	OnWrite(string)
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				string strType = "";
				string strAmount = "";
				if (rsProcessing.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				// txtDedCode = GetDeductionNumber(rsProcessing.Fields("DeductionCode"))
				txtDedCode.Text = rsProcessing.Get_Fields_String("description");
				txtAmount.Text = Strings.Format(rsProcessing.Get_Fields("Amount"), "#,##0.00");
				txtAmountDP.Text = Strings.Format(rsProcessing.Get_Fields_String("AmountType"), "$#,##0.00");
				txtCurrent.Text = Strings.Format(modCoreysSweeterCode.GetCurrentMatchTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate), "$#,##0.00");
				txtMTD.Text = Strings.Format(modCoreysSweeterCode.GetMTDMatchTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate), "$#,##0.00");
				txtCalender.Text = Strings.Format(modCoreysSweeterCode.GetCYTDMatchTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate), "$#,##0.00");
				txtFiscal.Text = Strings.Format(modCoreysSweeterCode.GetFYTDMatchTotal(rsProcessing.Get_Fields("deductioncode"), strEmployee, modGlobalVariables.Statics.gdatCurrentPayDate), "$#,##0.00");
				txtAccount.Text = rsProcessing.Get_Fields_String("Account");
				txtMax.Text = Strings.Format(rsProcessing.Get_Fields_Double("MaxAmount"), "$#,##0.00");
				txtMaxDH.Text = (FCConvert.ToInt32(rsProcessing.Get_Fields_String("DollarsHours")) == 1 ? "Dollars" : "Hours");
				rsProcessing.MoveNext();
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Call rsProcessing.OpenRecordset("Select * from tblEmployersMatch Where EmployeeNumber = '" & gstrEmployeeToGet & "' Order by RecordNumber", "TWPY0000.vb1")
			strEmployee = FCConvert.ToString(this.UserData);
			rsProcessing.OpenRecordset("Select * from tblEmployersMatch Where EmployeeNumber = '" + this.UserData + "' Order by RecordNumber", "TWPY0000.vb1");
		}

		
	}
}
