//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class cUC1Controller
	{
		//=========================================================
		public void LoadReport(ref cUC1Report cReport, string strSequence)
		{
			DateTime dtStartDate = DateTime.Now;
			DateTime dtEndDate = DateTime.Now;
			string strSQL;
			double dblWage = 0;
			double dblGross = 0;
			// vbPorter upgrade warning: dblExcess As double	OnWrite(int, Decimal)
			double dblExcess = 0;
			double dblQTDDed = 0;
			double dblYTDDed = 0;
			double dblLimit;
			string strFirstName = "";
			string strMiddleName = "";
			string strLastName = "";
			string strDesig = "";
			string strSSN = "";
			bool boolSeasonal = false;
			//clsDRWrapper rsUDed = new clsDRWrapper();
			string strQTDWhere;
			string strYTDWhere;
			clsDRWrapper rsUnemployment = new clsDRWrapper();
			string strSex = "";
			clsDRWrapper clsMonth1 = new clsDRWrapper();
			clsDRWrapper clsMonth2 = new clsDRWrapper();
			clsDRWrapper clsMonth3 = new clsDRWrapper();
			bool boolFemale = false;
			bool[] boolMonth = new bool[3 + 1];
			bool boolSkip = false;
			modCoreysSweeterCode.GetDateRangeForYearQuarter(ref dtStartDate, ref dtEndDate, cReport.Quarter, cReport.YearCovered);
			FillSummary(cReport.Summary);
			cReport.Summary.ReportYear = FCConvert.ToString(cReport.YearCovered);
			cReport.Summary.ExcessWages = 0;
			cReport.Summary.TaxableWages = 0;
			cReport.Summary.UCWages = 0;
			cReport.Summary.StartDate = dtStartDate;
			cReport.Summary.EndDate = dtEndDate;
			cReport.Summary.EmployeeCountMonth1 = 0;
			cReport.Summary.EmployeeCountMonth2 = 0;
			cReport.Summary.EmployeeCountMonth3 = 0;
			cReport.Summary.FemaleCountMonth1 = 0;
			cReport.Summary.FemaleCountMonth2 = 0;
			cReport.Summary.FemaleCountMonth3 = 0;
			cReport.Summary.UCRate = cReport.UCRate;
			dblLimit = cReport.Limit;
			strSQL = GetDetailSQL(dtStartDate, dtEndDate, strSequence);
			strYTDWhere = GetYTDWhere(cReport.Quarter, cReport.YearCovered);
			strQTDWhere = GetQTDWhere(cReport.Quarter, cReport.YearCovered);
			rsUnemployment.OpenRecordset("Select * from tblMiscUpdate Order by EmployeeNumber", "Payroll");
			clsMonth1.OpenRecordset(GetMonthCountSQL(Convert.ToDateTime(cReport.Get_EmploymentMonthStartDate(1)), Convert.ToDateTime(cReport.Get_EmploymentMonthEndDate(1))), "Payroll");
			clsMonth2.OpenRecordset(GetMonthCountSQL(Convert.ToDateTime(cReport.Get_EmploymentMonthStartDate(2)), Convert.ToDateTime(cReport.Get_EmploymentMonthEndDate(2))), "Payroll");
			clsMonth3.OpenRecordset(GetMonthCountSQL(Convert.ToDateTime(cReport.Get_EmploymentMonthStartDate(3)), Convert.ToDateTime(cReport.Get_EmploymentMonthEndDate(3))), "Payroll");
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset(strSQL, "Payroll");
			while (!rsLoad.EndOfFile())
			{
				boolSkip = false;
				strFirstName = fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("firstname")));
				strLastName = fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields("lastname")));
				strMiddleName = fecherFoundation.Strings.UCase(Strings.Left(rsLoad.Get_Fields("Middlename") + " ", 1));
				strSSN = Strings.Format(rsLoad.Get_Fields("ssn"), "000000000");
				strDesig = fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("Desig")));
				if (fecherFoundation.Strings.UCase(rsLoad.Get_Fields_String("seasonal")) != "S")
				{
					boolSeasonal = false;
				}
				else
				{
					boolSeasonal = true;
				}
				dblWage = Conversion.Val(Strings.Format(rsLoad.Get_Fields("qtdGrossPay"), "0.00"));
				dblGross = Conversion.Val(Strings.Format(rsLoad.Get_Fields("ytdgrosspay"), "0.00"));
				if (rsUnemployment.FindFirst("EmployeeNumber='" + rsLoad.Get_Fields("EmployeeNumber") + "'"))
				{
					if (FCConvert.ToString(rsUnemployment.Get_Fields("Unemployment")) == "N")
					{
						dblGross = 0;
						dblWage = 0;
						dblExcess = 0;
						boolSkip = true;
					}
				}
				if (!boolSkip)
				{
					dblYTDDed = GetUCWages(rsLoad.Get_Fields("employeenumber"), FCConvert.ToDateTime("1/1/" + cReport.YearCovered), dtEndDate);
					if (dblYTDDed > 0)
					{
						dblQTDDed = GetUCWages(rsLoad.Get_Fields("employeenumber"), dtStartDate, dtEndDate);
					}
					else
					{
						dblQTDDed = 0;
					}
					dblWage -= dblQTDDed;
					if (dblWage < 0)
						dblWage = 0;
					dblGross -= dblYTDDed;
					if (dblGross < 0)
						dblGross = 0;
					dblExcess = FCConvert.ToDouble(modDavesSweetCode.CalculateExcess(FCConvert.ToDecimal(dblGross), FCConvert.ToDecimal(dblWage), FCConvert.ToDecimal(dblLimit)));
				}
				cReport.Summary.UCWages += dblWage;
				cReport.Summary.ExcessWages += dblExcess;
				strSex = fecherFoundation.Strings.UCase(Strings.Left(rsLoad.Get_Fields("sex") + " ", 1));
				if (strSex == "F")
				{
					boolFemale = true;
				}
				else
				{
					boolFemale = false;
				}
				boolMonth[0] = false;
				boolMonth[1] = false;
				boolMonth[2] = false;
				if (!(clsMonth1.EndOfFile() && clsMonth1.BeginningOfFile()))
				{
					if (clsMonth1.FindFirst("employeenumber = '" + rsLoad.Get_Fields("employeenumber") + "'"))
					{
						if (dblWage > 0)
						{
							cReport.Summary.EmployeeCountMonth1 += 1;
							boolMonth[0] = true;
							if (strSex == "F")
								cReport.Summary.FemaleCountMonth1 += 1;
						}
					}
					else
					{
						clsMonth1.MoveFirst();
					}
				}
				if (!(clsMonth2.EndOfFile() && clsMonth2.BeginningOfFile()))
				{
					if (clsMonth2.FindFirst("employeenumber = '" + rsLoad.Get_Fields("employeenumber") + "'"))
					{
						if (dblWage > 0)
						{
							cReport.Summary.EmployeeCountMonth2 += 1;
							boolMonth[1] = true;
							if (strSex == "F")
								cReport.Summary.FemaleCountMonth2 += 1;
						}
					}
					else
					{
						clsMonth2.MoveFirst();
					}
				}
				if (!(clsMonth3.EndOfFile() && clsMonth3.BeginningOfFile()))
				{
					if (clsMonth3.FindFirst("employeenumber = '" + rsLoad.Get_Fields("employeenumber") + "'"))
					{
						if (dblWage > 0)
						{
							cReport.Summary.EmployeeCountMonth3 += 1;
							boolMonth[2] = true;
							if (strSex == "F")
								cReport.Summary.FemaleCountMonth3 += 1;
						}
					}
					else
					{
						clsMonth3.MoveFirst();
					}
				}
				cReport.Details.CreateDetail(strFirstName, strMiddleName, strLastName, strDesig, strSSN, dblWage, boolSeasonal, boolFemale, boolMonth[0], boolMonth[1], boolMonth[2], dblExcess, dblWage - dblExcess);
				rsLoad.MoveNext();
			}
			cReport.Summary.TaxableWages = FCConvert.ToDouble(Strings.Format(cReport.Summary.UCWages - cReport.Summary.ExcessWages, "0.00"));
			cReport.Summary.UCDue = FCConvert.ToDouble(Strings.Format(cReport.UCRate * cReport.Summary.TaxableWages, "0.00"));
			cReport.Summary.CSSFAssess = FCConvert.ToDouble(Strings.Format(cReport.CSSFRate * cReport.Summary.TaxableWages, "0.00"));
            cReport.Summary.UPAFAssess = FCConvert.ToDouble(Strings.Format(cReport.UPAFRate * cReport.Summary.TaxableWages, "0.00"));
			cReport.Summary.TotalContCSSFUPAFDue = cReport.Summary.CSSFAssess + cReport.Summary.UPAFAssess + cReport.Summary.UCDue;
			rsUnemployment.Dispose();
			clsMonth1.Dispose();
			clsMonth2.Dispose();
			clsMonth3.Dispose();
		}

		private void FillSummary(cUC1Summary Summary)
		{
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                string strSQL;
                strSQL = "select * from tblemployerinfo";
                rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
                if (!rsLoad.EndOfFile())
                {
                    Summary.FederalID = rsLoad.Get_Fields_String("FederalEmployerID");
                    Summary.Name = rsLoad.Get_Fields_String("EmployerName");
                    Summary.UCAccountID = rsLoad.Get_Fields_String("StateUCAccount");
                }
            }
        }

		private string GetMonthCountSQL(DateTime dtStartDate, DateTime dtEndDate)
		{
			string GetMonthCountSQL = "";
			string strSQL;
			strSQL = "select employeenumber from tblcheckdetail where (not checkvoid = 1) AND (isnull(DistU,'N') <> 'N' AND DistU <> '') and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' group by employeenumber";
			GetMonthCountSQL = strSQL;
			return GetMonthCountSQL;
		}

		private string GetQTDWhere(int intQuarter, int intYear)
		{
			string GetQTDWhere = "";
			string strQTDWhere = "";
			switch (intQuarter)
			{
				case 1:
					{
						strQTDWhere = " where paydate between '01/01/" + FCConvert.ToString(intYear) + "' and '3/31/" + FCConvert.ToString(intYear) + "' ";
						break;
					}
				case 2:
					{
						strQTDWhere = " where paydate between '04/01/" + FCConvert.ToString(intYear) + "' and '6/30/" + FCConvert.ToString(intYear) + "' ";
						break;
					}
				case 3:
					{
						strQTDWhere = " where paydate between '07/01/" + FCConvert.ToString(intYear) + "' and '9/30/" + FCConvert.ToString(intYear) + "' ";
						break;
					}
				case 4:
					{
						strQTDWhere = " where paydate between'10/1/" + FCConvert.ToString(intYear) + "' and '12/31/" + FCConvert.ToString(intYear) + "' ";
						break;
					}
			}
			//end switch
			strQTDWhere += " and deductionrecord = 1 and uexempt = 1 and not checkvoid = 1 ";
			GetQTDWhere = strQTDWhere;
			return GetQTDWhere;
		}

		private string GetYTDWhere(int intQuarter, int intYear)
		{
			string GetYTDWhere = "";
			string strYTDWhere = "";
			switch (intQuarter)
			{
				case 1:
					{
						strYTDWhere = " where paydate between '01/01/" + FCConvert.ToString(intYear) + "' and '3/31/" + FCConvert.ToString(intYear) + "' ";
						break;
					}
				case 2:
					{
						strYTDWhere = " where paydate between '01/01/" + FCConvert.ToString(intYear) + "' and '6/30/" + FCConvert.ToString(intYear) + "' ";
						break;
					}
				case 3:
					{
						strYTDWhere = " where paydate between '01/01/" + FCConvert.ToString(intYear) + "' and '9/30/" + FCConvert.ToString(intYear) + "' ";
						break;
					}
				case 4:
					{
						strYTDWhere = " where paydate between '01/01/" + FCConvert.ToString(intYear) + "' and '12/31/" + FCConvert.ToString(intYear) + "' ";
						break;
					}
			}
			//end switch
			strYTDWhere += " and deductionrecord = 1 and uexempt = 1 and not checkvoid = 1";
			GetYTDWhere = strYTDWhere;
			return GetYTDWhere;
		}
		// vbPorter upgrade warning: dtStart As DateTime	OnWrite(string, DateTime)
		private double GetUCWages(string strEmployeeNumber, DateTime dtStart, DateTime dtEnd)
		{
			double GetUCWages = 0;
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                double dblTotDed = 0;
                rsLoad.OpenRecordset(
                    "select sum(dedamount) as totded from tblcheckdetail where paydate between '" +
                    FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(dtEnd) +
                    "' and deductionrecord = 1 and uexempt = 1 and not checkvoid = 1 and employeenumber = '" +
                    strEmployeeNumber + "'", "Payroll");
                if (!rsLoad.EndOfFile())
                {
                    dblTotDed = Conversion.Val(rsLoad.Get_Fields("totded"));
                }
                else
                {
                    dblTotDed = 0;
                }

                GetUCWages = dblTotDed;
            }

            return GetUCWages;
		}

		private string GetDetailSQL(DateTime dtStartDate, DateTime dtEndDate, string strSequence)
		{
			string GetDetailSQL = "";
			string strSQL;
			string strUnEmpQuery1;
			string strQuery1;
			string strQuery2;
			string strQuery3;
			string strQuery4;
			int lngSeqStart = 0;
			int lngSeqEnd = 0;
			string[] strAry = null;
			string strRange = "";
			int lngYearCovered;
			string strUnEmpQuery2;
			lngYearCovered = dtStartDate.Year;
			if (strSequence != "")
			{
				strAry = Strings.Split(strSequence, ",", -1, CompareConstants.vbTextCompare);
				if (Information.UBound(strAry, 1) < 1)
				{
					// all or single
					lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				}
				else
				{
					// range
					lngSeqStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					lngSeqEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
				}
			}
			if (lngSeqStart >= 0)
			{
				strRange = " where seqnumber between " + FCConvert.ToString(lngSeqStart) + " and " + FCConvert.ToString(lngSeqEnd);
			}
			else
			{
				strRange = "";
			}
			strSQL = "select employeenumber as enum,sum(distGrossPay) as qtdGrossPay from tblCheckDetail where DistributionRecord = 1 AND Paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and not msrsadjustrecord = 1 and CheckVoid = 0 and (distu <> 'N' and distu <> '') group by employeenumber having ( sum(distgrosspay) > 0 )";
			strUnEmpQuery1 = "(" + strSQL + ") as UnEmpQuery1 ";
			strSQL = "select employeenumber as employeenum,sum(distGrossPay) as YTDGrossPay from tblcheckdetail where DistributionRecord = 1 AND paydate between '" + "1/1/" + FCConvert.ToString(lngYearCovered) + "' and '" + FCConvert.ToString(dtEndDate) + "' and  CheckVoid = 0 and (distu <> 'N' and distu <> '') and not msrsadjustrecord = 1 group by employeenumber ";
			strUnEmpQuery2 = "(" + strSQL + ") as UnEmpQuery2 ";
			strQuery1 = "(Select * from " + strUnEmpQuery2 + " left join " + strUnEmpQuery1 + " on (unempquery1.enum = unempquery2.employeenum)) as C1Query1";
			strQuery2 = "(select employeenumber,sum(grosspay) as qtdTotalGross from tblcheckdetail where paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and  checkvoid = 0 and not msrsadjustrecord = 1 group by employeenumber having sum(grosspay) > 0  ) as C1Query2 ";
			// corey 06/04/2004   'added a where clause so we don't get people with no state tax and no gross pay
			strQuery3 = "(select * from " + strQuery2 + " left join " + strQuery1 + " on (c1query1.employeeNUM = c1query2.employeenumBER) WHERE qtdgrosspay > 0 ) as C1Query3";
			strQuery4 = "(select * from " + strQuery3 + " left join (select employeenumber as e4num,unemployment as seasonal from tblMiscUpdate) as C1Query4 on (c1query4.e4num = c1query3.employeenumber)) as C1Query5 ";
			strSQL = "select *,TBLEMPLOYEEMASTER.employeenumber as employeenumber from tblemployeemaster inner join " + strQuery4 + " on (tblemployeemaster.employeenumber = c1query5.employeenumber)   " + strRange + " order by tblemployeemaster.lastname,tblemployeemaster.firstname";
			GetDetailSQL = strSQL;
			return GetDetailSQL;
		}

		public cUC1BarCodeInfo GetBarCodeInfoFromSummary(cUC1Summary cSum)
		{
			cUC1BarCodeInfo GetBarCodeInfoFromSummary = null;
			cUC1BarCodeInfo bcInfo = new cUC1BarCodeInfo();
			bcInfo.CSSFAssess = cSum.CSSFAssess;
			bcInfo.EmployeeCountMonth1 = cSum.EmployeeCountMonth1;
			bcInfo.EmployeeCountMonth2 = cSum.EmployeeCountMonth2;
			bcInfo.EmployeeCountMonth3 = cSum.EmployeeCountMonth3;
			bcInfo.EndDate = cSum.EndDate;
			bcInfo.ExcessWages = cSum.ExcessWages;
			bcInfo.FederalID = cSum.FederalID;
			bcInfo.FemaleCountMonth1 = cSum.FemaleCountMonth1;
			bcInfo.FemaleCountMonth2 = cSum.FemaleCountMonth2;
			bcInfo.FemaleCountMonth3 = cSum.FemaleCountMonth3;
			bcInfo.Name = cSum.Name;
			bcInfo.PayrollProcessor = cSum.PayrollProcessor;
			bcInfo.PreparerEIN = cSum.PreparerEIN;
			bcInfo.ReportYear = cSum.ReportYear;
			bcInfo.StartDate = cSum.StartDate;
			bcInfo.StateCode = cSum.StateCode;
			bcInfo.TaxableWages = cSum.TaxableWages;
			bcInfo.TotalContCSSFUPAFDue = cSum.TotalContCSSFUPAFDue;
			bcInfo.UCAccountID = cSum.UCAccountID;
			bcInfo.UCDue = cSum.UCDue;
			bcInfo.UCWages = cSum.UCWages;
			GetBarCodeInfoFromSummary = bcInfo;
			return GetBarCodeInfoFromSummary;
		}
	}
}
