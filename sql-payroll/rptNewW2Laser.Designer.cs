﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Laser.
	/// </summary>
	partial class rptW2Laser
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW2Laser));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtFederalEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployeesName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFederalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFICATax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMedTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAllocationTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFICAWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMedWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDependentCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNonQualifiedPlans = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEmployersStateEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStateWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLocalityName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12aAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12bAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12cAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt12dAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtThirdParty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatutoryEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLastName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDesig = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeesName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFICATax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMedTaxes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllocationTips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFICAWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMedWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSTips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDependentCare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonQualifiedPlans)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployersStateEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateWages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocalityName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12a)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12b)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12c)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12d)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12aAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12bAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12cAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12dAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThirdParty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatutoryEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFederalEIN,
            this.txtEmployersName,
            this.txtSSN,
            this.txtEmployeesName,
            this.txtTotalWages,
            this.txtFederalTax,
            this.txtFICATax,
            this.txtMedTaxes,
            this.txtAllocationTips,
            this.txtFICAWages,
            this.txtMedWages,
            this.txtSSTips,
            this.txtDependentCare,
            this.txtNonQualifiedPlans,
            this.txtOther,
            this.txtState,
            this.txtEmployersStateEIN,
            this.txtStateWages,
            this.txtStateTax,
            this.txtLocalityName,
            this.txt12a,
            this.txt12b,
            this.txt12c,
            this.txt12d,
            this.txt12aAmount,
            this.txt12bAmount,
            this.txt12cAmount,
            this.txt12dAmount,
            this.txtPen,
            this.txtThirdParty,
            this.txtStatutoryEmployee,
            this.txtLastName,
            this.txtDesig});
            this.Detail.Height = 5.5F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtFederalEIN
            // 
            this.txtFederalEIN.Height = 0.1666667F;
            this.txtFederalEIN.Left = 0.062F;
            this.txtFederalEIN.Name = "txtFederalEIN";
            this.txtFederalEIN.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtFederalEIN.Text = null;
            this.txtFederalEIN.Top = 0.9670001F;
            this.txtFederalEIN.Width = 2.9375F;
            // 
            // txtEmployersName
            // 
            this.txtEmployersName.Height = 0.625F;
            this.txtEmployersName.Left = 0.062F;
            this.txtEmployersName.Name = "txtEmployersName";
            this.txtEmployersName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtEmployersName.Text = null;
            this.txtEmployersName.Top = 1.342F;
            this.txtEmployersName.Width = 2.9375F;
            // 
            // txtSSN
            // 
            this.txtSSN.Height = 0.1666667F;
            this.txtSSN.Left = 1.7495F;
            this.txtSSN.Name = "txtSSN";
            this.txtSSN.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtSSN.Text = null;
            this.txtSSN.Top = 0.6406112F;
            this.txtSSN.Width = 1.6875F;
            // 
            // txtEmployeesName
            // 
            this.txtEmployeesName.Height = 1.136F;
            this.txtEmployeesName.Left = 0.062F;
            this.txtEmployeesName.Name = "txtEmployeesName";
            this.txtEmployeesName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtEmployeesName.Text = null;
            this.txtEmployeesName.Top = 2.644F;
            this.txtEmployeesName.Width = 2.9375F;
            // 
            // txtTotalWages
            // 
            this.txtTotalWages.Height = 0.1666667F;
            this.txtTotalWages.Left = 4.40575F;
            this.txtTotalWages.Name = "txtTotalWages";
            this.txtTotalWages.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtTotalWages.Text = null;
            this.txtTotalWages.Top = 0.9670001F;
            this.txtTotalWages.Width = 1.15625F;
            // 
            // txtFederalTax
            // 
            this.txtFederalTax.Height = 0.1666667F;
            this.txtFederalTax.Left = 6.312F;
            this.txtFederalTax.Name = "txtFederalTax";
            this.txtFederalTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtFederalTax.Text = null;
            this.txtFederalTax.Top = 0.9670001F;
            this.txtFederalTax.Width = 1.15625F;
            // 
            // txtFICATax
            // 
            this.txtFICATax.Height = 0.1666667F;
            this.txtFICATax.Left = 6.312F;
            this.txtFICATax.Name = "txtFICATax";
            this.txtFICATax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtFICATax.Text = null;
            this.txtFICATax.Top = 1.300333F;
            this.txtFICATax.Width = 1.15625F;
            // 
            // txtMedTaxes
            // 
            this.txtMedTaxes.Height = 0.1666667F;
            this.txtMedTaxes.Left = 6.312F;
            this.txtMedTaxes.Name = "txtMedTaxes";
            this.txtMedTaxes.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtMedTaxes.Text = null;
            this.txtMedTaxes.Top = 1.633666F;
            this.txtMedTaxes.Width = 1.15625F;
            // 
            // txtAllocationTips
            // 
            this.txtAllocationTips.Height = 0.1666667F;
            this.txtAllocationTips.Left = 6.312F;
            this.txtAllocationTips.Name = "txtAllocationTips";
            this.txtAllocationTips.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtAllocationTips.Text = null;
            this.txtAllocationTips.Top = 1.967F;
            this.txtAllocationTips.Width = 1.15625F;
            // 
            // txtFICAWages
            // 
            this.txtFICAWages.Height = 0.1666667F;
            this.txtFICAWages.Left = 4.40575F;
            this.txtFICAWages.Name = "txtFICAWages";
            this.txtFICAWages.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtFICAWages.Text = null;
            this.txtFICAWages.Top = 1.300333F;
            this.txtFICAWages.Width = 1.15625F;
            // 
            // txtMedWages
            // 
            this.txtMedWages.Height = 0.1666667F;
            this.txtMedWages.Left = 4.40575F;
            this.txtMedWages.Name = "txtMedWages";
            this.txtMedWages.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtMedWages.Text = null;
            this.txtMedWages.Top = 1.633666F;
            this.txtMedWages.Width = 1.15625F;
            // 
            // txtSSTips
            // 
            this.txtSSTips.Height = 0.1666667F;
            this.txtSSTips.Left = 4.40575F;
            this.txtSSTips.Name = "txtSSTips";
            this.txtSSTips.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtSSTips.Text = null;
            this.txtSSTips.Top = 1.967F;
            this.txtSSTips.Width = 1.15625F;
            // 
            // txtDependentCare
            // 
            this.txtDependentCare.Height = 0.1666667F;
            this.txtDependentCare.Left = 6.312F;
            this.txtDependentCare.Name = "txtDependentCare";
            this.txtDependentCare.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtDependentCare.Text = null;
            this.txtDependentCare.Top = 2.300333F;
            this.txtDependentCare.Width = 1.15625F;
            // 
            // txtNonQualifiedPlans
            // 
            this.txtNonQualifiedPlans.Height = 0.1666667F;
            this.txtNonQualifiedPlans.Left = 4.40575F;
            this.txtNonQualifiedPlans.Name = "txtNonQualifiedPlans";
            this.txtNonQualifiedPlans.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtNonQualifiedPlans.Text = null;
            this.txtNonQualifiedPlans.Top = 2.633666F;
            this.txtNonQualifiedPlans.Width = 1.15625F;
            // 
            // txtOther
            // 
            this.txtOther.Height = 0.5416667F;
            this.txtOther.Left = 4.437F;
            this.txtOther.Name = "txtOther";
            this.txtOther.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.txtOther.Text = null;
            this.txtOther.Top = 3.300333F;
            this.txtOther.Width = 1.15625F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1666667F;
            this.txtState.Left = 0.062F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtState.Text = null;
            this.txtState.Top = 4.134F;
            this.txtState.Width = 0.28125F;
            // 
            // txtEmployersStateEIN
            // 
            this.txtEmployersStateEIN.Height = 0.1666667F;
            this.txtEmployersStateEIN.Left = 0.468F;
            this.txtEmployersStateEIN.Name = "txtEmployersStateEIN";
            this.txtEmployersStateEIN.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtEmployersStateEIN.Top = 4.134F;
            this.txtEmployersStateEIN.Width = 1.28125F;
            // 
            // txtStateWages
            // 
            this.txtStateWages.Height = 0.1666667F;
            this.txtStateWages.Left = 1.84325F;
            this.txtStateWages.Name = "txtStateWages";
            this.txtStateWages.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtStateWages.Text = null;
            this.txtStateWages.Top = 4.133669F;
            this.txtStateWages.Width = 1.28125F;
            // 
            // txtStateTax
            // 
            this.txtStateTax.Height = 0.1666667F;
            this.txtStateTax.Left = 3.28075F;
            this.txtStateTax.Name = "txtStateTax";
            this.txtStateTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtStateTax.Text = null;
            this.txtStateTax.Top = 4.133669F;
            this.txtStateTax.Width = 1.125F;
            // 
            // txtLocalityName
            // 
            this.txtLocalityName.Height = 0.1666667F;
            this.txtLocalityName.Left = 6.90575F;
            this.txtLocalityName.Name = "txtLocalityName";
            this.txtLocalityName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtLocalityName.Text = null;
            this.txtLocalityName.Top = 4.133669F;
            this.txtLocalityName.Width = 0.625F;
            // 
            // txt12a
            // 
            this.txt12a.Height = 0.1666667F;
            this.txt12a.Left = 5.96825F;
            this.txt12a.Name = "txt12a";
            this.txt12a.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.txt12a.Text = null;
            this.txt12a.Top = 2.633666F;
            this.txt12a.Width = 0.4375F;
            // 
            // txt12b
            // 
            this.txt12b.Height = 0.1666667F;
            this.txt12b.Left = 5.96825F;
            this.txt12b.Name = "txt12b";
            this.txt12b.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.txt12b.Text = null;
            this.txt12b.Top = 2.967F;
            this.txt12b.Width = 0.4375F;
            // 
            // txt12c
            // 
            this.txt12c.Height = 0.1666667F;
            this.txt12c.Left = 5.96825F;
            this.txt12c.Name = "txt12c";
            this.txt12c.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.txt12c.Text = null;
            this.txt12c.Top = 3.300333F;
            this.txt12c.Width = 0.4375F;
            // 
            // txt12d
            // 
            this.txt12d.Height = 0.1666667F;
            this.txt12d.Left = 5.96825F;
            this.txt12d.Name = "txt12d";
            this.txt12d.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.txt12d.Text = null;
            this.txt12d.Top = 3.633666F;
            this.txt12d.Width = 0.4375F;
            // 
            // txt12aAmount
            // 
            this.txt12aAmount.Height = 0.1666667F;
            this.txt12aAmount.Left = 6.59325F;
            this.txt12aAmount.Name = "txt12aAmount";
            this.txt12aAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txt12aAmount.Text = null;
            this.txt12aAmount.Top = 2.633666F;
            this.txt12aAmount.Width = 0.90625F;
            // 
            // txt12bAmount
            // 
            this.txt12bAmount.Height = 0.1666667F;
            this.txt12bAmount.Left = 6.59325F;
            this.txt12bAmount.Name = "txt12bAmount";
            this.txt12bAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txt12bAmount.Text = null;
            this.txt12bAmount.Top = 2.967F;
            this.txt12bAmount.Width = 0.90625F;
            // 
            // txt12cAmount
            // 
            this.txt12cAmount.Height = 0.1666667F;
            this.txt12cAmount.Left = 6.59325F;
            this.txt12cAmount.Name = "txt12cAmount";
            this.txt12cAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txt12cAmount.Text = null;
            this.txt12cAmount.Top = 3.300333F;
            this.txt12cAmount.Width = 0.90625F;
            // 
            // txt12dAmount
            // 
            this.txt12dAmount.Height = 0.1666667F;
            this.txt12dAmount.Left = 6.59325F;
            this.txt12dAmount.Name = "txt12dAmount";
            this.txt12dAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txt12dAmount.Text = null;
            this.txt12dAmount.Top = 3.633666F;
            this.txt12dAmount.Width = 0.90625F;
            // 
            // txtPen
            // 
            this.txtPen.Height = 0.1666667F;
            this.txtPen.Left = 4.891F;
            this.txtPen.Name = "txtPen";
            this.txtPen.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
            this.txtPen.Text = "X";
            this.txtPen.Top = 3.03F;
            this.txtPen.Visible = false;
            this.txtPen.Width = 0.15625F;
            // 
            // txtThirdParty
            // 
            this.txtThirdParty.Height = 0.1666667F;
            this.txtThirdParty.Left = 5.391F;
            this.txtThirdParty.Name = "txtThirdParty";
            this.txtThirdParty.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
            this.txtThirdParty.Text = "X";
            this.txtThirdParty.Top = 3.03F;
            this.txtThirdParty.Visible = false;
            this.txtThirdParty.Width = 0.15625F;
            // 
            // txtStatutoryEmployee
            // 
            this.txtStatutoryEmployee.Height = 0.1666667F;
            this.txtStatutoryEmployee.Left = 4.42225F;
            this.txtStatutoryEmployee.Name = "txtStatutoryEmployee";
            this.txtStatutoryEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
            this.txtStatutoryEmployee.Text = "X";
            this.txtStatutoryEmployee.Top = 3.03F;
            this.txtStatutoryEmployee.Visible = false;
            this.txtStatutoryEmployee.Width = 0.15625F;
            // 
            // txtLastName
            // 
            this.txtLastName.Height = 0.1666667F;
            this.txtLastName.Left = 1.8995F;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtLastName.Text = null;
            this.txtLastName.Top = 2.642F;
            this.txtLastName.Width = 1.6F;
            // 
            // txtDesig
            // 
            this.txtDesig.Height = 0.1875F;
            this.txtDesig.Left = 3.78075F;
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.Style = "text-align: center";
            this.txtDesig.Text = "Jr";
            this.txtDesig.Top = 2.633666F;
            this.txtDesig.Width = 0.2708333F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptW2Laser
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.947917F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeesName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFederalTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFICATax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMedTaxes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllocationTips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFICAWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMedWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSTips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDependentCare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonQualifiedPlans)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployersStateEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateWages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocalityName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12a)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12b)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12c)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12d)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12aAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12bAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12cAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12dAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThirdParty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatutoryEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeesName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllocationTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDependentCare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNonQualifiedPlans;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersStateEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocalityName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12aAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12bAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12cAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12dAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtThirdParty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatutoryEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLastName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesig;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
