﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cCodeDescription
	{
		//=========================================================
		private int lngRecordID;
		private string strCode = string.Empty;
		private string strDescription = string.Empty;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Code
		{
			set
			{
				strCode = value;
			}
			get
			{
				string Code = "";
				Code = strCode;
				return Code;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}
	}
}
