//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayTotalsDetail.
	/// </summary>
	public partial class rptPayTotalsDetail : BaseSectionReport
	{
		public rptPayTotalsDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Pay Totals Detail Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPayTotalsDetail InstancePtr
		{
			get
			{
				return (rptPayTotalsDetail)Sys.GetInstance(typeof(rptPayTotalsDetail));
			}
		}

		protected rptPayTotalsDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				employeeService?.Dispose();
                employeeService = null;
				employeeDict?.Clear();
                employeeDict = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPayTotalsDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		int intPageNumber;
		double dblNetPay;
		double dblGross;
		double dblFed;
		double dblState;
		double dblFica;
		double dblMed;
		double dblFedWage;
		double dblFicaWage;
		double dblMedWage;
		double dblStateWage;
		double dblTotalTotal;
		double dblEMatch;
		double dblDistHours;
		double dblDeductions;

		clsDRWrapper rsData = new clsDRWrapper();
		string strEmployeeNumber;
		private Dictionary<string, cEmployee> employeeDict = new Dictionary<string, cEmployee>();
		private cEmployeeService employeeService = new cEmployeeService();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			double dblTemp = 0;
			double dblTotal = 0;
			NextRecord:
			;
			while (!rsData.EndOfFile() && !(employeeDict == null))
			{
				//Application.DoEvents();
				if (!employeeDict.ContainsKey(rsData.Get_Fields("EmployeeNumber")))
				{
					rsData.MoveNext();
				}
				else
				{
					break;
				}
			}
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			if (rsData.EndOfFile())
			{
				// Or Not strEmployeeNumber = vbNullString Then
				eArgs.EOF = true;
				return;
			}
			dblTotal = 0;
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length > 6)
			{
				txtEmployee.Text = rsData.Get_Fields("EmployeeNumber") + Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))), 6) + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
			}
			else
			{
				txtEmployee.Text = rsData.Get_Fields("EmployeeNumber") + Strings.StrDup(6 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"))).Length, " ") + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
			}
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDTAX, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFederal.Text = Strings.Format(dblTemp, "#,##0.00");
			dblFed += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICATAX, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFICA.Text = Strings.Format(dblTemp, "#,##0.00");
			dblFica += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICARETAX, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMedicare.Text = Strings.Format(dblTemp, "#,##0.00");
			dblMed += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATETAX, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtState.Text = Strings.Format(dblTemp, "#,##0.00");
			dblState += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFEDGROSS, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFederalWage.Text = Strings.Format(dblTemp, "#,##0.00");
			dblFedWage += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEFICAGROSS, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtFICAWage.Text = Strings.Format(dblTemp, "#,##0.00");
			dblFicaWage += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPEMEDICAREGROSS, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtMedWage.Text = Strings.Format(dblTemp, "#,##0.00");
			dblMedWage += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPESTATEGROSS, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtStateWage.Text = Strings.Format(dblTemp, "#,##0.00");
			dblStateWage += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDTaxWHGross(modPYConstants.CNSTPAYTOTALTYPETOTALGROSS, rsData.Get_Fields("employeenumber"), modGlobalVariables.Statics.gdatCurrentPayDate);
			txtTotalGrossPay.Text = Strings.Format(dblTemp, "#,##0.00");
			dblTotal = dblTemp;
			dblTotalTotal += dblTotal;
			txtTotalPaid.Text = Strings.Format(dblTotal, "#,##0.00");
			TryAgain:
			;
			if (!rsData.EndOfFile())
			{
				rsData.MoveNext();
			}
			txtFedTotal.Text = Strings.Format(dblFed, "#,##0.00");
			txtStateTotal.Text = Strings.Format(dblState, "#,##0.00");
			txtFICATotal.Text = Strings.Format(dblFica, "#,##0.00");
			txtMedTotal.Text = Strings.Format(dblMed, "#,##0.00");
			txtFedWageTotal.Text = Strings.Format(dblFedWage, "#,##0.00");
			txtStateWageTotal.Text = Strings.Format(dblStateWage, "#,##0.00");
			txtFICAWageTotal.Text = Strings.Format(dblFicaWage, "#,##0.00");
			txtMedWageTotal.Text = Strings.Format(dblMedWage, "#,##0.00");
			txtTotalGrossPayTotal.Text = Strings.Format(dblTotalTotal, "#,##0.00");
			txtTotalPaidTotal.Text = Strings.Format(dblTotalTotal, "#,##0.00");
			if (!rsData.EndOfFile())
				strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
			eArgs.EOF = false;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPageNumber += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strOrderBy = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			intCounter = 2;
			intPageNumber = 0;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtTitle.Text = "Pay Totals Detail Report";
			strEmployeeNumber = string.Empty;
			txtFedTotal.Text = "0";
			txtStateTotal.Text = "0";
			txtFICATotal.Text = "0";
			txtMedTotal.Text = "0";
			txtFedWageTotal.Text = "0";
			txtStateWageTotal.Text = "0";
			txtFICAWageTotal.Text = "0";
			txtMedWageTotal.Text = "0";
			txtTotalGrossPayTotal.Text = "0";
			txtTotalPaidTotal.Text = "0";
			dblTotalTotal = 0;
			dblFed = 0;
			dblFedWage = 0;
			dblFica = 0;
			dblFicaWage = 0;
			dblMed = 0;
			dblMedWage = 0;
			dblState = 0;
			dblStateWage = 0;
			txtPayDate.Text = string.Empty;
			rsData.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 0)
			{
				// employee name
				strOrderBy = " order by lastname,firstname,tblemployeemaster.employeenumber ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 1)
			{
				// employee number
				strOrderBy = " order by tblemployeemaster.employeenumber ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 2)
			{
				// sequence
				strOrderBy = " order by seqnumber,lastname,firstname,tblemployeemaster.employeenumber ";
			}
			else if (Conversion.Val(rsData.Get_Fields("reportsequence")) == 3)
			{
				// dept/div
				strOrderBy = " order by DEPTDIV,lastname,firstname,tblemployeemaster.employeenumber ";
			}
			employeeDict = employeeService.GetEmployeesPermissableAsDictionary(modGlobalConstants.Statics.clsSecurityClass.Get_UserID(), "EmployeeNumber", false);
			rsData.OpenRecordset("select * from tblemployeemaster " + strOrderBy, "twpy0000.vb1");
			if (!rsData.EndOfFile())
				strEmployeeNumber = FCConvert.ToString(rsData.Get_Fields("EmployeeNumber"));
		}
	}
}
