//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmStandardLimits : BaseForm
	{
		public frmStandardLimits()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.txtUnemploymentRate.AllowOnlyNumericInput();
            this.txtUnemploymentBase.AllowOnlyNumericInput();
            this.txtFutaActual.AllowOnlyNumericInput();
            this.txtFutaBase.AllowOnlyNumericInput();
            this.txtFutaRate.AllowOnlyNumericInput();
            this.txtMaxCredit.AllowOnlyNumericInput();
            this.txtCSSFRate.AllowOnlyNumericInput();
            this.txtUPAFRate.AllowOnlyNumericInput();
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmStandardLimits InstancePtr
		{
			get
			{
				return (frmStandardLimits)Sys.GetInstance(typeof(frmStandardLimits));
			}
		}

		protected frmStandardLimits _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 01,2001
		//
		// NOTES:
		//
		// MODIFIED BY: DAN C. SOLTESZ ON 6/17/2001
		// **************************************************
		// private local variables
		private clsDRWrapper rsLimits = new clsDRWrapper();
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, DialogResult)
		private DialogResult intDataChanged;
		private clsHistory clsHistoryClass = new clsHistory();
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		
		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Delete the field length information from the database
				if (MessageBox.Show("This action will delete Field length infomation. Continue?", "Payroll Standard Length", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsLimits.Execute("Delete from tblStandardLimits", "Payroll");
					cmdNew_Click();
					MessageBox.Show("Delete of record was successful", "Payroll Standard Limits", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// clear out the controls but do nothing to the database
				// vbPorter upgrade warning: ControlName As Control	OnWrite(string)
				Control ControlName = new Control();
				foreach (Control ControlName_foreach in this.GetAllControls())
				{
					ControlName = ControlName_foreach;
					if (ControlName is FCTextBox)
					{
						(ControlName as FCTextBox).Text = string.Empty;
					}
					ControlName = null;
				}
				// form is now dirty
				intDataChanged = (DialogResult)1;
            }
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int x;
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Standard Limits / Rates", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				// get all of the records from the database
				rsLimits.OpenRecordset("Select * from tblStandardLimits", "TWPY0000.vb1");
				if (rsLimits.EndOfFile())
				{
					rsLimits.AddNew();
				}
				else
				{
					rsLimits.Edit();
				}
				
				rsLimits.SetData("FutaRate", Conversion.Val(txtFutaRate.Text));
				rsLimits.SetData("FutaMaxCredit", Conversion.Val(txtMaxCredit.Text));
				rsLimits.SetData("FutaActual", Conversion.Val(txtFutaActual.Text));
				rsLimits.SetData("FutaBase", Conversion.Val(txtFutaBase.Text));
				rsLimits.SetData("UnemploymentRate", Conversion.Val(txtUnemploymentRate.Text));
				rsLimits.SetData("UnemploymentBase", Conversion.Val(txtUnemploymentBase.Text));
				rsLimits.Set_Fields("CSSFRate", FCConvert.ToString(Conversion.Val(txtCSSFRate.Text)));
                rsLimits.Set_Fields("UPAFRate", txtUPAFRate.Text);
				rsLimits.Set_Fields("schoolunemploymentrate", FCConvert.ToString(Conversion.Val(txtUPAFRate.Text)));
				rsLimits.SetData("FTD940", cbo940.Text);
				rsLimits.SetData("FTD941", cbo941.Text);
				rsLimits.Update();
				// all changes have been updated
				intDataChanged = 0;
				MessageBox.Show("Save was successful", "Payroll Standard Limits", MessageBoxButtons.OK, MessageBoxIcon.Information);
				clsHistoryClass.Compare();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmStandardLimits_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// If FormExist(Me) Then Exit Sub
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmStandardLimits_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the key press is ESC then close out this form
				if (KeyAscii == Keys.Escape)
					Close();
				if (KeyAscii == Keys.Return)
				{
					if (this.ActiveControl is FCButton)
					{
					}
					else
					{
						Support.SendKeys("{TAB}", false);
					}
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmStandardLimits_Load(object sender, System.EventArgs e)
		{
            try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int counter;
				// vsElasticLight1.Enabled = True
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);

				rsLimits.DefaultDB = "TWPY0000.vb1";
				// Get the data from the database
				ShowData();
				clsHistoryClass.SetGridIDColumn("PY");
				intDataChanged = 0;
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
				FillControlInformationClass();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void ShowData()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ShowData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				rsLimits.OpenRecordset("Select * from tblStandardLimits", "TWPY0000.vb1");
				if (!rsLimits.EndOfFile())
				{
                    txtFutaRate.Text = FCConvert.ToString(Conversion.Val(rsLimits.Get_Fields("FutaRate") + " "));
					txtMaxCredit.Text = FCConvert.ToString(Conversion.Val(rsLimits.Get_Fields("FutaMaxCredit") + " "));
					txtFutaActual.Text = FCConvert.ToString(Conversion.Val(rsLimits.Get_Fields("FutaActual") + " "));
					txtFutaBase.Text = FCConvert.ToString(Conversion.Val(rsLimits.Get_Fields("FutaBase") + " "));
                    txtUnemploymentRate.Text = FCConvert.ToString(Conversion.Val(rsLimits.Get_Fields("UnemploymentRate") + " "));
					txtUnemploymentBase.Text = FCConvert.ToString(Conversion.Val(rsLimits.Get_Fields("UnemploymentBase") + " "));
					txtCSSFRate.Text = FCConvert.ToString(Conversion.Val(rsLimits.Get_Fields("CSSFRate") + ""));
					txtUPAFRate.Text = FCConvert.ToString(Conversion.Val(rsLimits.Get_Fields("UPAFRate")));
					// txtMaxGrossPay = Val(rsLimits.Fields("MaxGrossPay") & " ")
					if (fecherFoundation.Strings.Trim(rsLimits.Get_Fields("FTD940") + " ") != string.Empty)
					{
						if (fecherFoundation.Strings.Trim(rsLimits.Get_Fields("FTD940") + " ") == "M" || fecherFoundation.Strings.Trim(rsLimits.Get_Fields("FTD940") + " ") == "Monthly")
						{
							cbo940.SelectedIndex = 1;
						}
						else if (fecherFoundation.Strings.Trim(rsLimits.Get_Fields("FTD940") + " ") == "Q" || fecherFoundation.Strings.Trim(rsLimits.Get_Fields("FTD940") + " ") == "Quarterly")
						{
							cbo940.SelectedIndex = 2;
						}
						else
						{
							cbo940.SelectedIndex = 0;
						}
					}
					if (fecherFoundation.Strings.Trim(rsLimits.Get_Fields("FTD941") + " ") != string.Empty)
					{
						if (fecherFoundation.Strings.Trim(rsLimits.Get_Fields("FTD941") + " ") == "M" || fecherFoundation.Strings.Trim(rsLimits.Get_Fields("FTD941") + " ") == "Monthly")
						{
							cbo941.SelectedIndex = 1;
						}
						else
						{
							cbo941.SelectedIndex = 0;
						}
					}
				}
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				SaveChanges();
				if (intDataChanged == (DialogResult)2)
				{
					e.Cancel = true;
					return;
				}
				// MDIParent.Show
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}


		private void txtFutaActual_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtFutaActual_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtFutaActual.SelectionStart = 0;
				txtFutaActual.SelectionLength = txtFutaActual.Text.Length;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtFutaActual_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtFutaActual_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// allow the decimal point and backspace
				if (KeyAscii == Keys.Back || KeyAscii == Keys.Delete)
					return;
				// Do not allow the entry of a character
				if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
				{
					KeyAscii = (Keys)0;
				}
				else
				{
					intDataChanged = (DialogResult)1;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtFutaBase_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtFutaBase_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtFutaBase.SelectionStart = 0;
				txtFutaBase.SelectionLength = txtFutaBase.Text.Length;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtFutaBase_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtFutaBase_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// allow the decimal point and backspace
				if (KeyAscii == Keys.Back)
					return;
				// Do not allow the entry of a character
				if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
				{
					KeyAscii = (Keys)0;
				}
				else
				{
					intDataChanged = (DialogResult)1;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtFutaRate_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtFutaRate_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtFutaRate.SelectionStart = 0;
				txtFutaRate.SelectionLength = txtFutaRate.Text.Length;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtFutaRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtFutaRate_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// allow the decimal point and backspace
				if (KeyAscii == Keys.Delete || KeyAscii == Keys.Back)
					return;
				// Do not allow the entry of a character
				if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
				{
					KeyAscii = (Keys)0;
				}
				else
				{
					intDataChanged = (DialogResult)1;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtMaxCredit_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtMaxCredit_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtMaxCredit.SelectionStart = 0;
				txtMaxCredit.SelectionLength = txtMaxCredit.Text.Length;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtMaxCredit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtMaxCredit_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// allow the decimal point and backspace
				if (KeyAscii == Keys.Back || KeyAscii == Keys.Delete)
					return;
				// Do not allow the entry of a character
				if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
				{
					KeyAscii = (Keys)0;
				}
				else
				{
					intDataChanged = (DialogResult)1;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		
		private void txtUnemploymentBase_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtUnemploymentBase_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtUnemploymentBase.SelectionStart = 0;
				txtUnemploymentBase.SelectionLength = txtUnemploymentBase.Text.Length;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtUnemploymentBase_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtUnemploymentBase_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// allow the decimal point and backspace
				if (KeyAscii == Keys.Back)
					return;
				// Do not allow the entry of a character
				if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
				{
					KeyAscii = (Keys)0;
				}
				else
				{
					intDataChanged = (DialogResult)1;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtUnemploymentRate_Enter(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtUnemploymentRate_GotFocus";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				txtUnemploymentRate.SelectionStart = 0;
				txtUnemploymentRate.SelectionLength = txtUnemploymentRate.Text.Length;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void txtUnemploymentRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "txtUnemploymentRate_KeyPress";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// allow the decimal point and backspace
				if (KeyAscii == Keys.Delete || KeyAscii == Keys.Back)
					return;
				// Do not allow the entry of a character
				if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
				{
					KeyAscii = (Keys)0;
				}
				else
				{
					intDataChanged = (DialogResult)1;
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFutaRate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "FUTA Rate";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMaxCredit";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "FUTA Max Credit";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFutaActual";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "FUTA Actual";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFutaBase";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "FUTA Base";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUPAFRate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "UPAF Rate";
            intTotalNumberOfControls += 1;
            Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtCSSFRate";
            clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
            clsControlInfo[intTotalNumberOfControls].DataDescription = "CSSF Rate";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUnemploymentBase";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State Unemployment Base";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cbo940";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reporting - FTD 940";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cbo941";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reporting - FTD 941";
			intTotalNumberOfControls += 1;
		}
    }
}
