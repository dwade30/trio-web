﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMSRSPayrollSummary.
	/// </summary>
	partial class rptMSRSPayrollSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMSRSPayrollSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPaidDatesEnding = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPreparersName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTelephone = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPlan1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerRate1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEarnableComp1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmpCont1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCostCredit1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmplCont1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPlan2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerRate2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEarnableComp2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmpCont2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCostCredit2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmplCont2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPlan3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerRate3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEarnableComp3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmpCont3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCostCredit3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmplCont3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPlan4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerRate4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEarnableComp4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmpCont4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCostCredit4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmplCont4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPlan5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerRate5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEarnableComp5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmpCont5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCostCredit5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmplCont5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPlan6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployerRate6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEarnableComp6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmpCont6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCostCredit6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmplCont6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtEmpContTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCostCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmplContTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotalCont1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalCont2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalCont3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalCont4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalCont5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalCont6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalContTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtActivePremium = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRetireePremium = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSupplementalPremiums = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDependentPremiums = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPremiumTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtCreditDebit13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtCreditDebit14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotalRemittance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line79 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line80 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line81 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line82 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line83 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line84 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line85 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line86 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtCMDM1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCMDM2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExplanation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidDatesEnding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPreparersName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpContTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCreditTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplContTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalContTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActivePremium)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRetireePremium)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSupplementalPremiums)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentPremiums)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPremiumTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCreditDebit13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCreditDebit14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalRemittance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCMDM1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCMDM2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExplanation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Line1,
				this.Label9,
				this.Label10,
				this.Line2,
				this.Line3,
				this.Line4,
				this.Line5,
				this.Label11,
				this.Label12,
				this.Line6,
				this.Line7,
				this.Label13,
				this.txtPaidDatesEnding,
				this.Line8,
				this.Line9,
				this.Label15,
				this.txtDate,
				this.txtEmployerCode,
				this.txtEmployerName,
				this.Label19,
				this.txtPreparersName,
				this.Label21,
				this.txtTelephone,
				this.Label23,
				this.Line10,
				this.Line11,
				this.Line12,
				this.Label24,
				this.Label25,
				this.Line14,
				this.Label26,
				this.Line15,
				this.Label27,
				this.Line16,
				this.Label28,
				this.Label29,
				this.Line18,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33,
				this.Label34,
				this.Label35,
				this.Line19,
				this.txtPlan1,
				this.txtEmployerRate1,
				this.txtEarnableComp1,
				this.txtEmpCont1,
				this.txtCostCredit1,
				this.txtEmplCont1,
				this.Line20,
				this.txtPlan2,
				this.txtEmployerRate2,
				this.txtEarnableComp2,
				this.txtEmpCont2,
				this.txtCostCredit2,
				this.txtEmplCont2,
				this.Line21,
				this.txtPlan3,
				this.txtEmployerRate3,
				this.txtEarnableComp3,
				this.txtEmpCont3,
				this.txtCostCredit3,
				this.txtEmplCont3,
				this.Line22,
				this.txtPlan4,
				this.txtEmployerRate4,
				this.txtEarnableComp4,
				this.txtEmpCont4,
				this.txtCostCredit4,
				this.txtEmplCont4,
				this.Line23,
				this.txtPlan5,
				this.txtEmployerRate5,
				this.txtEarnableComp5,
				this.txtEmpCont5,
				this.txtCostCredit5,
				this.txtEmplCont5,
				this.Line24,
				this.txtPlan6,
				this.txtEmployerRate6,
				this.txtEarnableComp6,
				this.txtEmpCont6,
				this.txtCostCredit6,
				this.txtEmplCont6,
				this.Line25,
				this.txtEmpContTotal,
				this.txtCostCreditTotal,
				this.txtEmplContTotal,
				this.Line17,
				this.Line13,
				this.Label75,
				this.Label76,
				this.Line26,
				this.txtTotalCont1,
				this.txtTotalCont2,
				this.txtTotalCont3,
				this.txtTotalCont4,
				this.txtTotalCont5,
				this.txtTotalCont6,
				this.txtTotalContTotal,
				this.Line27,
				this.Line28,
				this.Line29,
				this.Line30,
				this.Line31,
				this.Line32,
				this.Line33,
				this.Line34,
				this.Line35,
				this.Line36,
				this.Label84,
				this.Line37,
				this.Label85,
				this.Label86,
				this.Line38,
				this.Line39,
				this.Line40,
				this.Line41,
				this.Label87,
				this.Line42,
				this.Label88,
				this.Line43,
				this.Label89,
				this.Label90,
				this.Line44,
				this.txtActivePremium,
				this.txtRetireePremium,
				this.txtSupplementalPremiums,
				this.txtDependentPremiums,
				this.Line45,
				this.Line46,
				this.Label95,
				this.Label96,
				this.Label97,
				this.Label98,
				this.Label99,
				this.Line47,
				this.Line49,
				this.txtPremiumTotal,
				this.Line50,
				this.Line51,
				this.Label101,
				this.Line52,
				this.Line48,
				this.Line53,
				this.txtC,
				this.Line54,
				this.Line55,
				this.Label103,
				this.Line56,
				this.Line57,
				this.Label104,
				this.Label105,
				this.Line58,
				this.Line59,
				this.Line60,
				this.Line61,
				this.Line62,
				this.Line63,
				this.Label106,
				this.Label107,
				this.Label108,
				this.Label109,
				this.Line64,
				this.Line65,
				this.txtCreditDebit13,
				this.Line66,
				this.Line67,
				this.txtCreditDebit14,
				this.Line68,
				this.Line69,
				this.Label112,
				this.Label113,
				this.Label114,
				this.Label115,
				this.Label116,
				this.Line70,
				this.Line71,
				this.Line72,
				this.Line73,
				this.txtD,
				this.Line74,
				this.Label118,
				this.Label119,
				this.Label120,
				this.Line75,
				this.Line76,
				this.Line77,
				this.Line78,
				this.txtTotalRemittance,
				this.Line79,
				this.Label122,
				this.Line80,
				this.Line81,
				this.Label123,
				this.Label124,
				this.Line82,
				this.Line83,
				this.Label125,
				this.Label126,
				this.Label127,
				this.Line84,
				this.Line85,
				this.Line86,
				this.txtCMDM1,
				this.txtCMDM2
			});
			this.Detail.Height = 10.20833F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtExplanation
			});
			this.ReportFooter.Height = 0.2395833F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.6875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 8.5pt; text-align: center";
			this.Label1.Text = "MAINE PUBLIC EMPLOYEE RETIREMENT SYSTEM";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.75F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.6875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 8.5pt; text-align: center";
			this.Label2.Text = "46 State House Station";
			this.Label2.Top = 0.125F;
			this.Label2.Width = 2.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; text-align: center";
			this.Label3.Text = "Augusta, ME 04333-0046";
			this.Label3.Top = 0.25F;
			this.Label3.Width = 2.75F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; text-align: center";
			this.Label4.Text = "Telephone:  (207) 512-3100";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 2.75F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.6875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 8.5pt; text-align: center";
			this.Label5.Text = "Toll-free:  800-451-9800";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 2.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.6875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 8.5pt; text-align: center";
			this.Label6.Text = "TTY:  (207) 512-3102";
			this.Label6.Top = 0.75F;
			this.Label6.Width = 2.75F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.21875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.25F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 12pt; font-weight: bold; text-align: right";
			this.Label7.Text = "MONTHLY PAYROLL SUMMARY REPORT";
			this.Label7.Top = 0.3125F;
			this.Label7.Width = 3.6875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.21875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 12pt; font-weight: bold; text-align: right";
			this.Label8.Text = "For PLDs in the Consolidated Plan";
			this.Label8.Top = 0.53125F;
			this.Label8.Width = 3.6875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 8F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 8F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 9pt";
			this.Label9.Text = "Attach your \"Monthly Payroll Detail Report\" to this cover page. Payroll reports a" + "nd remittances must be submitted no later than the 15th";
			this.Label9.Top = 1.0625F;
			this.Label9.Width = 7.75F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 9pt";
			this.Label10.Text = "day following the end of the month.";
			this.Label10.Top = 1.21875F;
			this.Label10.Width = 7.75F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.40625F;
			this.Line2.Width = 7.875F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 7.9375F;
			this.Line2.Y1 = 1.40625F;
			this.Line2.Y2 = 1.40625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.0625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.65625F;
			this.Line3.Width = 7.875F;
			this.Line3.X1 = 0.0625F;
			this.Line3.X2 = 7.9375F;
			this.Line3.Y1 = 1.65625F;
			this.Line3.Y2 = 1.65625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.0625F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.90625F;
			this.Line4.Width = 7.875F;
			this.Line4.X1 = 0.0625F;
			this.Line4.X2 = 7.9375F;
			this.Line4.Y1 = 1.90625F;
			this.Line4.Y2 = 1.90625F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0.0625F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.15625F;
			this.Line5.Width = 7.875F;
			this.Line5.X1 = 0.0625F;
			this.Line5.X2 = 7.9375F;
			this.Line5.Y1 = 2.15625F;
			this.Line5.Y2 = 2.15625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.0625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "";
			this.Label11.Text = "Employer Code:";
			this.Label11.Top = 1.46875F;
			this.Label11.Width = 0.9375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.8125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "";
			this.Label12.Text = "Employer Name:";
			this.Label12.Top = 1.46875F;
			this.Label12.Width = 1.0625F;
			// 
			// Line6
			// 
			this.Line6.Height = 0.25F;
			this.Line6.Left = 1.8125F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1.40625F;
			this.Line6.Width = 0F;
			this.Line6.X1 = 1.8125F;
			this.Line6.X2 = 1.8125F;
			this.Line6.Y1 = 1.40625F;
			this.Line6.Y2 = 1.65625F;
			// 
			// Line7
			// 
			this.Line7.Height = 0.75F;
			this.Line7.Left = 7.9375F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 1.40625F;
			this.Line7.Width = 0F;
			this.Line7.X1 = 7.9375F;
			this.Line7.X2 = 7.9375F;
			this.Line7.Y1 = 1.40625F;
			this.Line7.Y2 = 2.15625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.0625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "";
			this.Label13.Text = "Paid Dates Ending:";
			this.Label13.Top = 1.71875F;
			this.Label13.Width = 1.125F;
			// 
			// txtPaidDatesEnding
			// 
			this.txtPaidDatesEnding.Height = 0.19F;
			this.txtPaidDatesEnding.HyperLink = null;
			this.txtPaidDatesEnding.Left = 1.1875F;
			this.txtPaidDatesEnding.Name = "txtPaidDatesEnding";
			this.txtPaidDatesEnding.Style = "";
			this.txtPaidDatesEnding.Text = null;
			this.txtPaidDatesEnding.Top = 1.71875F;
			this.txtPaidDatesEnding.Width = 3.625F;
			// 
			// Line8
			// 
			this.Line8.Height = 0.25F;
			this.Line8.Left = 4.8125F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 1.65625F;
			this.Line8.Width = 0F;
			this.Line8.X1 = 4.8125F;
			this.Line8.X2 = 4.8125F;
			this.Line8.Y1 = 1.65625F;
			this.Line8.Y2 = 1.90625F;
			// 
			// Line9
			// 
			this.Line9.Height = 0.25F;
			this.Line9.Left = 4.8125F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 1.90625F;
			this.Line9.Width = 0F;
			this.Line9.X1 = 4.8125F;
			this.Line9.X2 = 4.8125F;
			this.Line9.Y1 = 1.90625F;
			this.Line9.Y2 = 2.15625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4.875F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "";
			this.Label15.Text = "Date Report Completed:";
			this.Label15.Top = 1.71875F;
			this.Label15.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "";
			this.txtDate.Text = null;
			this.txtDate.Top = 1.71875F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtEmployerCode
			// 
			this.txtEmployerCode.Height = 0.19F;
			this.txtEmployerCode.HyperLink = null;
			this.txtEmployerCode.Left = 1F;
			this.txtEmployerCode.Name = "txtEmployerCode";
			this.txtEmployerCode.Style = "";
			this.txtEmployerCode.Text = null;
			this.txtEmployerCode.Top = 1.46875F;
			this.txtEmployerCode.Width = 0.75F;
			// 
			// txtEmployerName
			// 
			this.txtEmployerName.Height = 0.19F;
			this.txtEmployerName.HyperLink = null;
			this.txtEmployerName.Left = 2.875F;
			this.txtEmployerName.Name = "txtEmployerName";
			this.txtEmployerName.Style = "";
			this.txtEmployerName.Text = null;
			this.txtEmployerName.Top = 1.46875F;
			this.txtEmployerName.Width = 4.9375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.0625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "";
			this.Label19.Text = "Person Preparing Report:";
			this.Label19.Top = 1.96875F;
			this.Label19.Width = 1.4375F;
			// 
			// txtPreparersName
			// 
			this.txtPreparersName.Height = 0.19F;
			this.txtPreparersName.HyperLink = null;
			this.txtPreparersName.Left = 1.5F;
			this.txtPreparersName.Name = "txtPreparersName";
			this.txtPreparersName.Style = "";
			this.txtPreparersName.Text = null;
			this.txtPreparersName.Top = 1.96875F;
			this.txtPreparersName.Width = 3.25F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "";
			this.Label21.Text = "Telephone Number:";
			this.Label21.Top = 1.96875F;
			this.Label21.Width = 1.4375F;
			// 
			// txtTelephone
			// 
			this.txtTelephone.Height = 0.19F;
			this.txtTelephone.HyperLink = null;
			this.txtTelephone.Left = 6.375F;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Style = "";
			this.txtTelephone.Text = null;
			this.txtTelephone.Top = 1.96875F;
			this.txtTelephone.Width = 1.4375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.0625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "";
			this.Label23.Text = "RETIREMENT CONTRIBUTION DATA  -  (MSRS Members Only)";
			this.Label23.Top = 2.25F;
			this.Label23.Width = 3.625F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 0.0625F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 2.40625F;
			this.Line10.Width = 2.0625F;
			this.Line10.X1 = 0.0625F;
			this.Line10.X2 = 2.125F;
			this.Line10.Y1 = 2.40625F;
			this.Line10.Y2 = 2.40625F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 0.0625F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 2.625F;
			this.Line11.Width = 5.8125F;
			this.Line11.X1 = 0.0625F;
			this.Line11.X2 = 5.875F;
			this.Line11.Y1 = 2.625F;
			this.Line11.Y2 = 2.625F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0.0625F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 2.96875F;
			this.Line12.Width = 5.8125F;
			this.Line12.X1 = 0.0625F;
			this.Line12.X2 = 5.875F;
			this.Line12.Y1 = 2.96875F;
			this.Line12.Y2 = 2.96875F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.0625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 8.5pt; text-align: left";
			this.Label24.Text = "Plan Code";
			this.Label24.Top = 2.65625F;
			this.Label24.Width = 0.625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.28125F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.6875F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-size: 8.5pt; text-align: left";
			this.Label25.Text = "Employer Rate";
			this.Label25.Top = 2.65625F;
			this.Label25.Width = 0.625F;
			// 
			// Line14
			// 
			this.Line14.Height = 1.875F;
			this.Line14.Left = 1.25F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 2.625F;
			this.Line14.Width = 0F;
			this.Line14.X1 = 1.25F;
			this.Line14.X2 = 1.25F;
			this.Line14.Y1 = 2.625F;
			this.Line14.Y2 = 4.5F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.28125F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 1.3125F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 8.5pt; text-align: left";
			this.Label26.Text = "Earnable Compensation";
			this.Label26.Top = 2.65625F;
			this.Label26.Width = 0.8125F;
			// 
			// Line15
			// 
			this.Line15.Height = 2.125F;
			this.Line15.Left = 2.3125F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 2.625F;
			this.Line15.Width = 0F;
			this.Line15.X1 = 2.3125F;
			this.Line15.X2 = 2.3125F;
			this.Line15.Y1 = 2.625F;
			this.Line15.Y2 = 4.75F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.28125F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 2.375F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 8.5pt; text-align: left";
			this.Label27.Text = "Employer Contribution (2x3)";
			this.Label27.Top = 2.65625F;
			this.Label27.Width = 1.1875F;
			// 
			// Line16
			// 
			this.Line16.Height = 2.125F;
			this.Line16.Left = 3.5625F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 2.625F;
			this.Line16.Width = 0F;
			this.Line16.X1 = 3.5625F;
			this.Line16.X2 = 3.5625F;
			this.Line16.Y1 = 2.625F;
			this.Line16.Y2 = 4.75F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.28125F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 3.625F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 8.5pt; text-align: left";
			this.Label28.Text = "IUUAL Cost or (Credit)*";
			this.Label28.Top = 2.65625F;
			this.Label28.Width = 1.1875F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.28125F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 4.8125F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 8.5pt; text-align: left";
			this.Label29.Text = "Employee Contributions";
			this.Label29.Top = 2.65625F;
			this.Label29.Width = 0.9375F;
			// 
			// Line18
			// 
			this.Line18.Height = 2.125F;
			this.Line18.Left = 5.875F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 2.625F;
			this.Line18.Width = 0F;
			this.Line18.X1 = 5.875F;
			this.Line18.X2 = 5.875F;
			this.Line18.Y1 = 2.625F;
			this.Line18.Y2 = 4.75F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.19F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.1875F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "text-align: center";
			this.Label30.Text = "1";
			this.Label30.Top = 2.4375F;
			this.Label30.Width = 0.25F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.19F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.8125F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "text-align: center";
			this.Label31.Text = "2";
			this.Label31.Top = 2.4375F;
			this.Label31.Width = 0.25F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.19F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 1.625F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "text-align: center";
			this.Label32.Text = "3";
			this.Label32.Top = 2.4375F;
			this.Label32.Width = 0.25F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 2.8125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "text-align: center";
			this.Label33.Text = "4";
			this.Label33.Top = 2.4375F;
			this.Label33.Width = 0.25F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 4F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "text-align: center";
			this.Label34.Text = "5";
			this.Label34.Top = 2.4375F;
			this.Label34.Width = 0.25F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.19F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 5.1875F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "text-align: center";
			this.Label35.Text = "6";
			this.Label35.Top = 2.4375F;
			this.Label35.Width = 0.25F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 0.0625F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 3.21875F;
			this.Line19.Width = 5.8125F;
			this.Line19.X1 = 0.0625F;
			this.Line19.X2 = 5.875F;
			this.Line19.Y1 = 3.21875F;
			this.Line19.Y2 = 3.21875F;
			// 
			// txtPlan1
			// 
			this.txtPlan1.Height = 0.19F;
			this.txtPlan1.HyperLink = null;
			this.txtPlan1.Left = 0.0625F;
			this.txtPlan1.Name = "txtPlan1";
			this.txtPlan1.Style = "text-align: left";
			this.txtPlan1.Text = null;
			this.txtPlan1.Top = 3.03125F;
			this.txtPlan1.Width = 0.5F;
			// 
			// txtEmployerRate1
			// 
			this.txtEmployerRate1.Height = 0.19F;
			this.txtEmployerRate1.HyperLink = null;
			this.txtEmployerRate1.Left = 0.625F;
			this.txtEmployerRate1.Name = "txtEmployerRate1";
			this.txtEmployerRate1.Style = "text-align: right";
			this.txtEmployerRate1.Text = null;
			this.txtEmployerRate1.Top = 3.03125F;
			this.txtEmployerRate1.Width = 0.5625F;
			// 
			// txtEarnableComp1
			// 
			this.txtEarnableComp1.Height = 0.19F;
			this.txtEarnableComp1.HyperLink = null;
			this.txtEarnableComp1.Left = 1.3125F;
			this.txtEarnableComp1.Name = "txtEarnableComp1";
			this.txtEarnableComp1.Style = "text-align: right";
			this.txtEarnableComp1.Text = null;
			this.txtEarnableComp1.Top = 3.03125F;
			this.txtEarnableComp1.Width = 0.9375F;
			// 
			// txtEmpCont1
			// 
			this.txtEmpCont1.Height = 0.19F;
			this.txtEmpCont1.HyperLink = null;
			this.txtEmpCont1.Left = 2.375F;
			this.txtEmpCont1.Name = "txtEmpCont1";
			this.txtEmpCont1.Style = "text-align: right";
			this.txtEmpCont1.Text = null;
			this.txtEmpCont1.Top = 3.03125F;
			this.txtEmpCont1.Width = 1.125F;
			// 
			// txtCostCredit1
			// 
			this.txtCostCredit1.Height = 0.19F;
			this.txtCostCredit1.HyperLink = null;
			this.txtCostCredit1.Left = 3.625F;
			this.txtCostCredit1.Name = "txtCostCredit1";
			this.txtCostCredit1.Style = "text-align: right";
			this.txtCostCredit1.Text = null;
			this.txtCostCredit1.Top = 3.03125F;
			this.txtCostCredit1.Width = 1.0625F;
			// 
			// txtEmplCont1
			// 
			this.txtEmplCont1.Height = 0.19F;
			this.txtEmplCont1.HyperLink = null;
			this.txtEmplCont1.Left = 4.75F;
			this.txtEmplCont1.Name = "txtEmplCont1";
			this.txtEmplCont1.Style = "text-align: right";
			this.txtEmplCont1.Text = null;
			this.txtEmplCont1.Top = 3.03125F;
			this.txtEmplCont1.Width = 1.0625F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 0.0625F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 3.46875F;
			this.Line20.Width = 5.8125F;
			this.Line20.X1 = 0.0625F;
			this.Line20.X2 = 5.875F;
			this.Line20.Y1 = 3.46875F;
			this.Line20.Y2 = 3.46875F;
			// 
			// txtPlan2
			// 
			this.txtPlan2.Height = 0.19F;
			this.txtPlan2.HyperLink = null;
			this.txtPlan2.Left = 0.0625F;
			this.txtPlan2.Name = "txtPlan2";
			this.txtPlan2.Style = "text-align: left";
			this.txtPlan2.Text = null;
			this.txtPlan2.Top = 3.28125F;
			this.txtPlan2.Width = 0.5F;
			// 
			// txtEmployerRate2
			// 
			this.txtEmployerRate2.Height = 0.19F;
			this.txtEmployerRate2.HyperLink = null;
			this.txtEmployerRate2.Left = 0.625F;
			this.txtEmployerRate2.Name = "txtEmployerRate2";
			this.txtEmployerRate2.Style = "text-align: right";
			this.txtEmployerRate2.Text = null;
			this.txtEmployerRate2.Top = 3.28125F;
			this.txtEmployerRate2.Width = 0.5625F;
			// 
			// txtEarnableComp2
			// 
			this.txtEarnableComp2.Height = 0.19F;
			this.txtEarnableComp2.HyperLink = null;
			this.txtEarnableComp2.Left = 1.3125F;
			this.txtEarnableComp2.Name = "txtEarnableComp2";
			this.txtEarnableComp2.Style = "text-align: right";
			this.txtEarnableComp2.Text = null;
			this.txtEarnableComp2.Top = 3.28125F;
			this.txtEarnableComp2.Width = 0.9375F;
			// 
			// txtEmpCont2
			// 
			this.txtEmpCont2.Height = 0.19F;
			this.txtEmpCont2.HyperLink = null;
			this.txtEmpCont2.Left = 2.375F;
			this.txtEmpCont2.Name = "txtEmpCont2";
			this.txtEmpCont2.Style = "text-align: right";
			this.txtEmpCont2.Text = null;
			this.txtEmpCont2.Top = 3.28125F;
			this.txtEmpCont2.Width = 1.125F;
			// 
			// txtCostCredit2
			// 
			this.txtCostCredit2.Height = 0.19F;
			this.txtCostCredit2.HyperLink = null;
			this.txtCostCredit2.Left = 3.625F;
			this.txtCostCredit2.Name = "txtCostCredit2";
			this.txtCostCredit2.Style = "text-align: right";
			this.txtCostCredit2.Text = null;
			this.txtCostCredit2.Top = 3.28125F;
			this.txtCostCredit2.Width = 1.0625F;
			// 
			// txtEmplCont2
			// 
			this.txtEmplCont2.Height = 0.19F;
			this.txtEmplCont2.HyperLink = null;
			this.txtEmplCont2.Left = 4.75F;
			this.txtEmplCont2.Name = "txtEmplCont2";
			this.txtEmplCont2.Style = "text-align: right";
			this.txtEmplCont2.Text = null;
			this.txtEmplCont2.Top = 3.28125F;
			this.txtEmplCont2.Width = 1.0625F;
			// 
			// Line21
			// 
			this.Line21.Height = 0F;
			this.Line21.Left = 0.0625F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 3.71875F;
			this.Line21.Width = 5.8125F;
			this.Line21.X1 = 0.0625F;
			this.Line21.X2 = 5.875F;
			this.Line21.Y1 = 3.71875F;
			this.Line21.Y2 = 3.71875F;
			// 
			// txtPlan3
			// 
			this.txtPlan3.Height = 0.19F;
			this.txtPlan3.HyperLink = null;
			this.txtPlan3.Left = 0.0625F;
			this.txtPlan3.Name = "txtPlan3";
			this.txtPlan3.Style = "text-align: left";
			this.txtPlan3.Text = null;
			this.txtPlan3.Top = 3.53125F;
			this.txtPlan3.Width = 0.5F;
			// 
			// txtEmployerRate3
			// 
			this.txtEmployerRate3.Height = 0.19F;
			this.txtEmployerRate3.HyperLink = null;
			this.txtEmployerRate3.Left = 0.625F;
			this.txtEmployerRate3.Name = "txtEmployerRate3";
			this.txtEmployerRate3.Style = "text-align: right";
			this.txtEmployerRate3.Text = null;
			this.txtEmployerRate3.Top = 3.53125F;
			this.txtEmployerRate3.Width = 0.5625F;
			// 
			// txtEarnableComp3
			// 
			this.txtEarnableComp3.Height = 0.19F;
			this.txtEarnableComp3.HyperLink = null;
			this.txtEarnableComp3.Left = 1.3125F;
			this.txtEarnableComp3.Name = "txtEarnableComp3";
			this.txtEarnableComp3.Style = "text-align: right";
			this.txtEarnableComp3.Text = null;
			this.txtEarnableComp3.Top = 3.53125F;
			this.txtEarnableComp3.Width = 0.9375F;
			// 
			// txtEmpCont3
			// 
			this.txtEmpCont3.Height = 0.19F;
			this.txtEmpCont3.HyperLink = null;
			this.txtEmpCont3.Left = 2.375F;
			this.txtEmpCont3.Name = "txtEmpCont3";
			this.txtEmpCont3.Style = "text-align: right";
			this.txtEmpCont3.Text = null;
			this.txtEmpCont3.Top = 3.53125F;
			this.txtEmpCont3.Width = 1.125F;
			// 
			// txtCostCredit3
			// 
			this.txtCostCredit3.Height = 0.19F;
			this.txtCostCredit3.HyperLink = null;
			this.txtCostCredit3.Left = 3.625F;
			this.txtCostCredit3.Name = "txtCostCredit3";
			this.txtCostCredit3.Style = "text-align: right";
			this.txtCostCredit3.Text = null;
			this.txtCostCredit3.Top = 3.53125F;
			this.txtCostCredit3.Width = 1.0625F;
			// 
			// txtEmplCont3
			// 
			this.txtEmplCont3.Height = 0.19F;
			this.txtEmplCont3.HyperLink = null;
			this.txtEmplCont3.Left = 4.75F;
			this.txtEmplCont3.Name = "txtEmplCont3";
			this.txtEmplCont3.Style = "text-align: right";
			this.txtEmplCont3.Text = null;
			this.txtEmplCont3.Top = 3.53125F;
			this.txtEmplCont3.Width = 1.0625F;
			// 
			// Line22
			// 
			this.Line22.Height = 0F;
			this.Line22.Left = 0.0625F;
			this.Line22.LineWeight = 1F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 3.96875F;
			this.Line22.Width = 5.8125F;
			this.Line22.X1 = 0.0625F;
			this.Line22.X2 = 5.875F;
			this.Line22.Y1 = 3.96875F;
			this.Line22.Y2 = 3.96875F;
			// 
			// txtPlan4
			// 
			this.txtPlan4.Height = 0.19F;
			this.txtPlan4.HyperLink = null;
			this.txtPlan4.Left = 0.0625F;
			this.txtPlan4.Name = "txtPlan4";
			this.txtPlan4.Style = "text-align: left";
			this.txtPlan4.Text = null;
			this.txtPlan4.Top = 3.78125F;
			this.txtPlan4.Width = 0.5F;
			// 
			// txtEmployerRate4
			// 
			this.txtEmployerRate4.Height = 0.19F;
			this.txtEmployerRate4.HyperLink = null;
			this.txtEmployerRate4.Left = 0.625F;
			this.txtEmployerRate4.Name = "txtEmployerRate4";
			this.txtEmployerRate4.Style = "text-align: right";
			this.txtEmployerRate4.Text = null;
			this.txtEmployerRate4.Top = 3.78125F;
			this.txtEmployerRate4.Width = 0.5625F;
			// 
			// txtEarnableComp4
			// 
			this.txtEarnableComp4.Height = 0.19F;
			this.txtEarnableComp4.HyperLink = null;
			this.txtEarnableComp4.Left = 1.3125F;
			this.txtEarnableComp4.Name = "txtEarnableComp4";
			this.txtEarnableComp4.Style = "text-align: right";
			this.txtEarnableComp4.Text = null;
			this.txtEarnableComp4.Top = 3.78125F;
			this.txtEarnableComp4.Width = 0.9375F;
			// 
			// txtEmpCont4
			// 
			this.txtEmpCont4.Height = 0.19F;
			this.txtEmpCont4.HyperLink = null;
			this.txtEmpCont4.Left = 2.375F;
			this.txtEmpCont4.Name = "txtEmpCont4";
			this.txtEmpCont4.Style = "text-align: right";
			this.txtEmpCont4.Text = null;
			this.txtEmpCont4.Top = 3.78125F;
			this.txtEmpCont4.Width = 1.125F;
			// 
			// txtCostCredit4
			// 
			this.txtCostCredit4.Height = 0.19F;
			this.txtCostCredit4.HyperLink = null;
			this.txtCostCredit4.Left = 3.625F;
			this.txtCostCredit4.Name = "txtCostCredit4";
			this.txtCostCredit4.Style = "text-align: right";
			this.txtCostCredit4.Text = null;
			this.txtCostCredit4.Top = 3.78125F;
			this.txtCostCredit4.Width = 1.0625F;
			// 
			// txtEmplCont4
			// 
			this.txtEmplCont4.Height = 0.19F;
			this.txtEmplCont4.HyperLink = null;
			this.txtEmplCont4.Left = 4.75F;
			this.txtEmplCont4.Name = "txtEmplCont4";
			this.txtEmplCont4.Style = "text-align: right";
			this.txtEmplCont4.Text = null;
			this.txtEmplCont4.Top = 3.78125F;
			this.txtEmplCont4.Width = 1.0625F;
			// 
			// Line23
			// 
			this.Line23.Height = 0F;
			this.Line23.Left = 0.0625F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 4.25F;
			this.Line23.Width = 5.8125F;
			this.Line23.X1 = 0.0625F;
			this.Line23.X2 = 5.875F;
			this.Line23.Y1 = 4.25F;
			this.Line23.Y2 = 4.25F;
			// 
			// txtPlan5
			// 
			this.txtPlan5.Height = 0.19F;
			this.txtPlan5.HyperLink = null;
			this.txtPlan5.Left = 0.0625F;
			this.txtPlan5.Name = "txtPlan5";
			this.txtPlan5.Style = "text-align: left";
			this.txtPlan5.Text = null;
			this.txtPlan5.Top = 4.0625F;
			this.txtPlan5.Width = 0.5F;
			// 
			// txtEmployerRate5
			// 
			this.txtEmployerRate5.Height = 0.19F;
			this.txtEmployerRate5.HyperLink = null;
			this.txtEmployerRate5.Left = 0.625F;
			this.txtEmployerRate5.Name = "txtEmployerRate5";
			this.txtEmployerRate5.Style = "text-align: right";
			this.txtEmployerRate5.Text = null;
			this.txtEmployerRate5.Top = 4.0625F;
			this.txtEmployerRate5.Width = 0.5625F;
			// 
			// txtEarnableComp5
			// 
			this.txtEarnableComp5.Height = 0.19F;
			this.txtEarnableComp5.HyperLink = null;
			this.txtEarnableComp5.Left = 1.3125F;
			this.txtEarnableComp5.Name = "txtEarnableComp5";
			this.txtEarnableComp5.Style = "text-align: right";
			this.txtEarnableComp5.Text = null;
			this.txtEarnableComp5.Top = 4.0625F;
			this.txtEarnableComp5.Width = 0.9375F;
			// 
			// txtEmpCont5
			// 
			this.txtEmpCont5.Height = 0.19F;
			this.txtEmpCont5.HyperLink = null;
			this.txtEmpCont5.Left = 2.375F;
			this.txtEmpCont5.Name = "txtEmpCont5";
			this.txtEmpCont5.Style = "text-align: right";
			this.txtEmpCont5.Text = null;
			this.txtEmpCont5.Top = 4.0625F;
			this.txtEmpCont5.Width = 1.125F;
			// 
			// txtCostCredit5
			// 
			this.txtCostCredit5.Height = 0.19F;
			this.txtCostCredit5.HyperLink = null;
			this.txtCostCredit5.Left = 3.625F;
			this.txtCostCredit5.Name = "txtCostCredit5";
			this.txtCostCredit5.Style = "text-align: right";
			this.txtCostCredit5.Text = null;
			this.txtCostCredit5.Top = 4.0625F;
			this.txtCostCredit5.Width = 1.0625F;
			// 
			// txtEmplCont5
			// 
			this.txtEmplCont5.Height = 0.19F;
			this.txtEmplCont5.HyperLink = null;
			this.txtEmplCont5.Left = 4.75F;
			this.txtEmplCont5.Name = "txtEmplCont5";
			this.txtEmplCont5.Style = "text-align: right";
			this.txtEmplCont5.Text = null;
			this.txtEmplCont5.Top = 4.0625F;
			this.txtEmplCont5.Width = 1.0625F;
			// 
			// Line24
			// 
			this.Line24.Height = 0F;
			this.Line24.Left = 0.0625F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 4.5F;
			this.Line24.Width = 5.8125F;
			this.Line24.X1 = 0.0625F;
			this.Line24.X2 = 5.875F;
			this.Line24.Y1 = 4.5F;
			this.Line24.Y2 = 4.5F;
			// 
			// txtPlan6
			// 
			this.txtPlan6.Height = 0.19F;
			this.txtPlan6.HyperLink = null;
			this.txtPlan6.Left = 0.0625F;
			this.txtPlan6.Name = "txtPlan6";
			this.txtPlan6.Style = "text-align: left";
			this.txtPlan6.Text = null;
			this.txtPlan6.Top = 4.3125F;
			this.txtPlan6.Width = 0.5F;
			// 
			// txtEmployerRate6
			// 
			this.txtEmployerRate6.Height = 0.19F;
			this.txtEmployerRate6.HyperLink = null;
			this.txtEmployerRate6.Left = 0.625F;
			this.txtEmployerRate6.Name = "txtEmployerRate6";
			this.txtEmployerRate6.Style = "text-align: right";
			this.txtEmployerRate6.Text = null;
			this.txtEmployerRate6.Top = 4.3125F;
			this.txtEmployerRate6.Width = 0.5625F;
			// 
			// txtEarnableComp6
			// 
			this.txtEarnableComp6.Height = 0.19F;
			this.txtEarnableComp6.HyperLink = null;
			this.txtEarnableComp6.Left = 1.3125F;
			this.txtEarnableComp6.Name = "txtEarnableComp6";
			this.txtEarnableComp6.Style = "text-align: right";
			this.txtEarnableComp6.Text = null;
			this.txtEarnableComp6.Top = 4.3125F;
			this.txtEarnableComp6.Width = 0.9375F;
			// 
			// txtEmpCont6
			// 
			this.txtEmpCont6.Height = 0.19F;
			this.txtEmpCont6.HyperLink = null;
			this.txtEmpCont6.Left = 2.375F;
			this.txtEmpCont6.Name = "txtEmpCont6";
			this.txtEmpCont6.Style = "text-align: right";
			this.txtEmpCont6.Text = null;
			this.txtEmpCont6.Top = 4.3125F;
			this.txtEmpCont6.Width = 1.125F;
			// 
			// txtCostCredit6
			// 
			this.txtCostCredit6.Height = 0.19F;
			this.txtCostCredit6.HyperLink = null;
			this.txtCostCredit6.Left = 3.625F;
			this.txtCostCredit6.Name = "txtCostCredit6";
			this.txtCostCredit6.Style = "text-align: right";
			this.txtCostCredit6.Text = null;
			this.txtCostCredit6.Top = 4.3125F;
			this.txtCostCredit6.Width = 1.0625F;
			// 
			// txtEmplCont6
			// 
			this.txtEmplCont6.Height = 0.19F;
			this.txtEmplCont6.HyperLink = null;
			this.txtEmplCont6.Left = 4.75F;
			this.txtEmplCont6.Name = "txtEmplCont6";
			this.txtEmplCont6.Style = "text-align: right";
			this.txtEmplCont6.Text = null;
			this.txtEmplCont6.Top = 4.3125F;
			this.txtEmplCont6.Width = 1.0625F;
			// 
			// Line25
			// 
			this.Line25.Height = 0F;
			this.Line25.Left = 2.3125F;
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 4.75F;
			this.Line25.Width = 3.5625F;
			this.Line25.X1 = 2.3125F;
			this.Line25.X2 = 5.875F;
			this.Line25.Y1 = 4.75F;
			this.Line25.Y2 = 4.75F;
			// 
			// txtEmpContTotal
			// 
			this.txtEmpContTotal.Height = 0.19F;
			this.txtEmpContTotal.HyperLink = null;
			this.txtEmpContTotal.Left = 2.375F;
			this.txtEmpContTotal.Name = "txtEmpContTotal";
			this.txtEmpContTotal.Style = "text-align: right";
			this.txtEmpContTotal.Text = null;
			this.txtEmpContTotal.Top = 4.5625F;
			this.txtEmpContTotal.Width = 1.125F;
			// 
			// txtCostCreditTotal
			// 
			this.txtCostCreditTotal.Height = 0.19F;
			this.txtCostCreditTotal.HyperLink = null;
			this.txtCostCreditTotal.Left = 3.625F;
			this.txtCostCreditTotal.Name = "txtCostCreditTotal";
			this.txtCostCreditTotal.Style = "text-align: right";
			this.txtCostCreditTotal.Text = null;
			this.txtCostCreditTotal.Top = 4.5625F;
			this.txtCostCreditTotal.Width = 1.0625F;
			// 
			// txtEmplContTotal
			// 
			this.txtEmplContTotal.Height = 0.19F;
			this.txtEmplContTotal.HyperLink = null;
			this.txtEmplContTotal.Left = 4.75F;
			this.txtEmplContTotal.Name = "txtEmplContTotal";
			this.txtEmplContTotal.Style = "text-align: right";
			this.txtEmplContTotal.Text = null;
			this.txtEmplContTotal.Top = 4.5625F;
			this.txtEmplContTotal.Width = 1.0625F;
			// 
			// Line17
			// 
			this.Line17.Height = 2.125F;
			this.Line17.Left = 4.75F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 2.625F;
			this.Line17.Width = 0F;
			this.Line17.X1 = 4.75F;
			this.Line17.X2 = 4.75F;
			this.Line17.Y1 = 2.625F;
			this.Line17.Y2 = 4.75F;
			// 
			// Line13
			// 
			this.Line13.Height = 1.875F;
			this.Line13.Left = 0.625F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 2.625F;
			this.Line13.Width = 0F;
			this.Line13.X1 = 0.625F;
			this.Line13.X2 = 0.625F;
			this.Line13.Y1 = 2.625F;
			this.Line13.Y2 = 4.5F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.19F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 1.5625F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label75.Text = "Totals";
			this.Label75.Top = 4.5625F;
			this.Label75.Width = 0.5625F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.28125F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 6.5625F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "font-size: 8.5pt; text-align: left";
			this.Label76.Text = "Total Contributions (4 + 5 + 6)";
			this.Label76.Top = 2.65625F;
			this.Label76.Width = 1.125F;
			// 
			// Line26
			// 
			this.Line26.Height = 2.125F;
			this.Line26.Left = 7.8125F;
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 2.625F;
			this.Line26.Width = 0F;
			this.Line26.X1 = 7.8125F;
			this.Line26.X2 = 7.8125F;
			this.Line26.Y1 = 2.625F;
			this.Line26.Y2 = 4.75F;
			// 
			// txtTotalCont1
			// 
			this.txtTotalCont1.Height = 0.19F;
			this.txtTotalCont1.HyperLink = null;
			this.txtTotalCont1.Left = 6.5F;
			this.txtTotalCont1.Name = "txtTotalCont1";
			this.txtTotalCont1.Style = "text-align: right";
			this.txtTotalCont1.Text = null;
			this.txtTotalCont1.Top = 3.03125F;
			this.txtTotalCont1.Width = 1.25F;
			// 
			// txtTotalCont2
			// 
			this.txtTotalCont2.Height = 0.19F;
			this.txtTotalCont2.HyperLink = null;
			this.txtTotalCont2.Left = 6.5F;
			this.txtTotalCont2.Name = "txtTotalCont2";
			this.txtTotalCont2.Style = "text-align: right";
			this.txtTotalCont2.Text = null;
			this.txtTotalCont2.Top = 3.28125F;
			this.txtTotalCont2.Width = 1.25F;
			// 
			// txtTotalCont3
			// 
			this.txtTotalCont3.Height = 0.19F;
			this.txtTotalCont3.HyperLink = null;
			this.txtTotalCont3.Left = 6.5F;
			this.txtTotalCont3.Name = "txtTotalCont3";
			this.txtTotalCont3.Style = "text-align: right";
			this.txtTotalCont3.Text = null;
			this.txtTotalCont3.Top = 3.53125F;
			this.txtTotalCont3.Width = 1.25F;
			// 
			// txtTotalCont4
			// 
			this.txtTotalCont4.Height = 0.19F;
			this.txtTotalCont4.HyperLink = null;
			this.txtTotalCont4.Left = 6.5F;
			this.txtTotalCont4.Name = "txtTotalCont4";
			this.txtTotalCont4.Style = "text-align: right";
			this.txtTotalCont4.Text = null;
			this.txtTotalCont4.Top = 3.78125F;
			this.txtTotalCont4.Width = 1.25F;
			// 
			// txtTotalCont5
			// 
			this.txtTotalCont5.Height = 0.19F;
			this.txtTotalCont5.HyperLink = null;
			this.txtTotalCont5.Left = 6.5F;
			this.txtTotalCont5.Name = "txtTotalCont5";
			this.txtTotalCont5.Style = "text-align: right";
			this.txtTotalCont5.Text = null;
			this.txtTotalCont5.Top = 4.0625F;
			this.txtTotalCont5.Width = 1.25F;
			// 
			// txtTotalCont6
			// 
			this.txtTotalCont6.Height = 0.19F;
			this.txtTotalCont6.HyperLink = null;
			this.txtTotalCont6.Left = 6.5F;
			this.txtTotalCont6.Name = "txtTotalCont6";
			this.txtTotalCont6.Style = "text-align: right";
			this.txtTotalCont6.Text = null;
			this.txtTotalCont6.Top = 4.3125F;
			this.txtTotalCont6.Width = 1.25F;
			// 
			// txtTotalContTotal
			// 
			this.txtTotalContTotal.Height = 0.19F;
			this.txtTotalContTotal.HyperLink = null;
			this.txtTotalContTotal.Left = 6.5F;
			this.txtTotalContTotal.Name = "txtTotalContTotal";
			this.txtTotalContTotal.Style = "text-align: right";
			this.txtTotalContTotal.Text = null;
			this.txtTotalContTotal.Top = 4.5625F;
			this.txtTotalContTotal.Width = 1.25F;
			// 
			// Line27
			// 
			this.Line27.Height = 2.125F;
			this.Line27.Left = 6.5F;
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 2.625F;
			this.Line27.Width = 0F;
			this.Line27.X1 = 6.5F;
			this.Line27.X2 = 6.5F;
			this.Line27.Y1 = 2.625F;
			this.Line27.Y2 = 4.75F;
			// 
			// Line28
			// 
			this.Line28.Height = 0F;
			this.Line28.Left = 6.5F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 2.625F;
			this.Line28.Width = 1.3125F;
			this.Line28.X1 = 6.5F;
			this.Line28.X2 = 7.8125F;
			this.Line28.Y1 = 2.625F;
			this.Line28.Y2 = 2.625F;
			// 
			// Line29
			// 
			this.Line29.Height = 0F;
			this.Line29.Left = 6.1875F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 4.75F;
			this.Line29.Width = 1.625F;
			this.Line29.X1 = 6.1875F;
			this.Line29.X2 = 7.8125F;
			this.Line29.Y1 = 4.75F;
			this.Line29.Y2 = 4.75F;
			// 
			// Line30
			// 
			this.Line30.Height = 0F;
			this.Line30.Left = 6.1875F;
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 4.5F;
			this.Line30.Width = 1.625F;
			this.Line30.X1 = 6.1875F;
			this.Line30.X2 = 7.8125F;
			this.Line30.Y1 = 4.5F;
			this.Line30.Y2 = 4.5F;
			// 
			// Line31
			// 
			this.Line31.Height = 0F;
			this.Line31.Left = 6.5F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 4.25F;
			this.Line31.Width = 1.3125F;
			this.Line31.X1 = 6.5F;
			this.Line31.X2 = 7.8125F;
			this.Line31.Y1 = 4.25F;
			this.Line31.Y2 = 4.25F;
			// 
			// Line32
			// 
			this.Line32.Height = 0F;
			this.Line32.Left = 6.5F;
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 3.96875F;
			this.Line32.Width = 1.3125F;
			this.Line32.X1 = 6.5F;
			this.Line32.X2 = 7.8125F;
			this.Line32.Y1 = 3.96875F;
			this.Line32.Y2 = 3.96875F;
			// 
			// Line33
			// 
			this.Line33.Height = 0F;
			this.Line33.Left = 6.5F;
			this.Line33.LineWeight = 1F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 3.71875F;
			this.Line33.Width = 1.3125F;
			this.Line33.X1 = 6.5F;
			this.Line33.X2 = 7.8125F;
			this.Line33.Y1 = 3.71875F;
			this.Line33.Y2 = 3.71875F;
			// 
			// Line34
			// 
			this.Line34.Height = 0F;
			this.Line34.Left = 6.5F;
			this.Line34.LineWeight = 1F;
			this.Line34.Name = "Line34";
			this.Line34.Top = 3.46875F;
			this.Line34.Width = 1.3125F;
			this.Line34.X1 = 6.5F;
			this.Line34.X2 = 7.8125F;
			this.Line34.Y1 = 3.46875F;
			this.Line34.Y2 = 3.46875F;
			// 
			// Line35
			// 
			this.Line35.Height = 0F;
			this.Line35.Left = 6.5F;
			this.Line35.LineWeight = 1F;
			this.Line35.Name = "Line35";
			this.Line35.Top = 3.21875F;
			this.Line35.Width = 1.3125F;
			this.Line35.X1 = 6.5F;
			this.Line35.X2 = 7.8125F;
			this.Line35.Y1 = 3.21875F;
			this.Line35.Y2 = 3.21875F;
			// 
			// Line36
			// 
			this.Line36.Height = 0F;
			this.Line36.Left = 6.5F;
			this.Line36.LineWeight = 1F;
			this.Line36.Name = "Line36";
			this.Line36.Top = 2.96875F;
			this.Line36.Width = 1.3125F;
			this.Line36.X1 = 6.5F;
			this.Line36.X2 = 7.8125F;
			this.Line36.Y1 = 2.96875F;
			this.Line36.Y2 = 2.96875F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.19F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 6.1875F;
			this.Label84.Name = "Label84";
			this.Label84.Style = "text-align: center";
			this.Label84.Text = "A";
			this.Label84.Top = 4.5625F;
			this.Label84.Width = 0.25F;
			// 
			// Line37
			// 
			this.Line37.Height = 0.25F;
			this.Line37.Left = 6.1875F;
			this.Line37.LineWeight = 1F;
			this.Line37.Name = "Line37";
			this.Line37.Top = 4.5F;
			this.Line37.Width = 0F;
			this.Line37.X1 = 6.1875F;
			this.Line37.X2 = 6.1875F;
			this.Line37.Y1 = 4.5F;
			this.Line37.Y2 = 4.75F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.19F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 0.0625F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "font-size: 8.5pt";
			this.Label85.Text = "*A credit in Column 5 cannot exceed the amount on the same line in Column 4.";
			this.Label85.Top = 4.84375F;
			this.Label85.Width = 4.5625F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.19F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 0.0625F;
			this.Label86.Name = "Label86";
			this.Label86.Style = "";
			this.Label86.Text = "GROUP LIFE INSURANCE PREMIUM DATA";
			this.Label86.Top = 5.3125F;
			this.Label86.Width = 2.4375F;
			// 
			// Line38
			// 
			this.Line38.Height = 0F;
			this.Line38.Left = 0.0625F;
			this.Line38.LineWeight = 1F;
			this.Line38.Name = "Line38";
			this.Line38.Top = 5.46875F;
			this.Line38.Width = 2.3125F;
			this.Line38.X1 = 0.0625F;
			this.Line38.X2 = 2.375F;
			this.Line38.Y1 = 5.46875F;
			this.Line38.Y2 = 5.46875F;
			// 
			// Line39
			// 
			this.Line39.Height = 0F;
			this.Line39.Left = 1.25F;
			this.Line39.LineWeight = 1F;
			this.Line39.Name = "Line39";
			this.Line39.Top = 5.75F;
			this.Line39.Width = 4.625F;
			this.Line39.X1 = 1.25F;
			this.Line39.X2 = 5.875F;
			this.Line39.Y1 = 5.75F;
			this.Line39.Y2 = 5.75F;
			// 
			// Line40
			// 
			this.Line40.Height = 0F;
			this.Line40.Left = 1.25F;
			this.Line40.LineWeight = 1F;
			this.Line40.Name = "Line40";
			this.Line40.Top = 6.09375F;
			this.Line40.Width = 4.625F;
			this.Line40.X1 = 1.25F;
			this.Line40.X2 = 5.875F;
			this.Line40.Y1 = 6.09375F;
			this.Line40.Y2 = 6.09375F;
			// 
			// Line41
			// 
			this.Line41.Height = 0.59375F;
			this.Line41.Left = 1.25F;
			this.Line41.LineWeight = 1F;
			this.Line41.Name = "Line41";
			this.Line41.Top = 5.75F;
			this.Line41.Width = 0F;
			this.Line41.X1 = 1.25F;
			this.Line41.X2 = 1.25F;
			this.Line41.Y1 = 5.75F;
			this.Line41.Y2 = 6.34375F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.28125F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 1.3125F;
			this.Label87.Name = "Label87";
			this.Label87.Style = "font-size: 8.5pt; text-align: left";
			this.Label87.Text = "Basic Premiums for Actives";
			this.Label87.Top = 5.78125F;
			this.Label87.Width = 0.9375F;
			// 
			// Line42
			// 
			this.Line42.Height = 0.59375F;
			this.Line42.Left = 2.3125F;
			this.Line42.LineWeight = 1F;
			this.Line42.Name = "Line42";
			this.Line42.Top = 5.75F;
			this.Line42.Width = 0F;
			this.Line42.X1 = 2.3125F;
			this.Line42.X2 = 2.3125F;
			this.Line42.Y1 = 5.75F;
			this.Line42.Y2 = 6.34375F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.28125F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 2.375F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "font-size: 8.5pt; text-align: left";
			this.Label88.Text = "Basic Premiums for Retirees";
			this.Label88.Top = 5.78125F;
			this.Label88.Width = 1.1875F;
			// 
			// Line43
			// 
			this.Line43.Height = 0.59375F;
			this.Line43.Left = 3.5625F;
			this.Line43.LineWeight = 1F;
			this.Line43.Name = "Line43";
			this.Line43.Top = 5.75F;
			this.Line43.Width = 0F;
			this.Line43.X1 = 3.5625F;
			this.Line43.X2 = 3.5625F;
			this.Line43.Y1 = 5.75F;
			this.Line43.Y2 = 6.34375F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.28125F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 3.625F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "font-size: 8.5pt; text-align: left";
			this.Label89.Text = "Supplemental Premiums";
			this.Label89.Top = 5.78125F;
			this.Label89.Width = 1.1875F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.28125F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 4.8125F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "font-size: 8.5pt; text-align: left";
			this.Label90.Text = "Dependent Premiums";
			this.Label90.Top = 5.78125F;
			this.Label90.Width = 0.9375F;
			// 
			// Line44
			// 
			this.Line44.Height = 0.59375F;
			this.Line44.Left = 5.875F;
			this.Line44.LineWeight = 1F;
			this.Line44.Name = "Line44";
			this.Line44.Top = 5.75F;
			this.Line44.Width = 0F;
			this.Line44.X1 = 5.875F;
			this.Line44.X2 = 5.875F;
			this.Line44.Y1 = 5.75F;
			this.Line44.Y2 = 6.34375F;
			// 
			// txtActivePremium
			// 
			this.txtActivePremium.Height = 0.19F;
			this.txtActivePremium.HyperLink = null;
			this.txtActivePremium.Left = 1.3125F;
			this.txtActivePremium.Name = "txtActivePremium";
			this.txtActivePremium.Style = "text-align: right";
			this.txtActivePremium.Text = null;
			this.txtActivePremium.Top = 6.15625F;
			this.txtActivePremium.Width = 0.9375F;
			// 
			// txtRetireePremium
			// 
			this.txtRetireePremium.Height = 0.19F;
			this.txtRetireePremium.HyperLink = null;
			this.txtRetireePremium.Left = 2.375F;
			this.txtRetireePremium.Name = "txtRetireePremium";
			this.txtRetireePremium.Style = "text-align: right";
			this.txtRetireePremium.Text = null;
			this.txtRetireePremium.Top = 6.15625F;
			this.txtRetireePremium.Width = 1.125F;
			// 
			// txtSupplementalPremiums
			// 
			this.txtSupplementalPremiums.Height = 0.19F;
			this.txtSupplementalPremiums.HyperLink = null;
			this.txtSupplementalPremiums.Left = 3.625F;
			this.txtSupplementalPremiums.Name = "txtSupplementalPremiums";
			this.txtSupplementalPremiums.Style = "text-align: right";
			this.txtSupplementalPremiums.Text = null;
			this.txtSupplementalPremiums.Top = 6.15625F;
			this.txtSupplementalPremiums.Width = 1.0625F;
			// 
			// txtDependentPremiums
			// 
			this.txtDependentPremiums.Height = 0.19F;
			this.txtDependentPremiums.HyperLink = null;
			this.txtDependentPremiums.Left = 4.75F;
			this.txtDependentPremiums.Name = "txtDependentPremiums";
			this.txtDependentPremiums.Style = "text-align: right";
			this.txtDependentPremiums.Text = null;
			this.txtDependentPremiums.Top = 6.15625F;
			this.txtDependentPremiums.Width = 1.0625F;
			// 
			// Line45
			// 
			this.Line45.Height = 0.59375F;
			this.Line45.Left = 4.75F;
			this.Line45.LineWeight = 1F;
			this.Line45.Name = "Line45";
			this.Line45.Top = 5.75F;
			this.Line45.Width = 0F;
			this.Line45.X1 = 4.75F;
			this.Line45.X2 = 4.75F;
			this.Line45.Y1 = 5.75F;
			this.Line45.Y2 = 6.34375F;
			// 
			// Line46
			// 
			this.Line46.Height = 0F;
			this.Line46.Left = 1.25F;
			this.Line46.LineWeight = 1F;
			this.Line46.Name = "Line46";
			this.Line46.Top = 6.34375F;
			this.Line46.Width = 4.625F;
			this.Line46.X1 = 1.25F;
			this.Line46.X2 = 5.875F;
			this.Line46.Y1 = 6.34375F;
			this.Line46.Y2 = 6.34375F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.19F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 1.625F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "text-align: center";
			this.Label95.Text = "8";
			this.Label95.Top = 5.5625F;
			this.Label95.Width = 0.25F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.19F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 2.25F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "text-align: center";
			this.Label96.Text = "13";
			this.Label96.Top = 7.125F;
			this.Label96.Width = 0.25F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.19F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 4.5625F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "text-align: center";
			this.Label97.Text = "14";
			this.Label97.Top = 7.125F;
			this.Label97.Width = 0.25F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.19F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 5.1875F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "text-align: center";
			this.Label98.Text = "11";
			this.Label98.Top = 5.5625F;
			this.Label98.Width = 0.25F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.28125F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 6.5625F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-size: 8.5pt; text-align: left";
			this.Label99.Text = "Premium Total        (8 + 9 + 10 + 11)";
			this.Label99.Top = 5.78125F;
			this.Label99.Width = 1.125F;
			// 
			// Line47
			// 
			this.Line47.Height = 0.59375F;
			this.Line47.Left = 7.8125F;
			this.Line47.LineWeight = 1F;
			this.Line47.Name = "Line47";
			this.Line47.Top = 5.75F;
			this.Line47.Width = 0F;
			this.Line47.X1 = 7.8125F;
			this.Line47.X2 = 7.8125F;
			this.Line47.Y1 = 5.75F;
			this.Line47.Y2 = 6.34375F;
			// 
			// Line49
			// 
			this.Line49.Height = 0F;
			this.Line49.Left = 6.5F;
			this.Line49.LineWeight = 1F;
			this.Line49.Name = "Line49";
			this.Line49.Top = 5.75F;
			this.Line49.Width = 1.3125F;
			this.Line49.X1 = 6.5F;
			this.Line49.X2 = 7.8125F;
			this.Line49.Y1 = 5.75F;
			this.Line49.Y2 = 5.75F;
			// 
			// txtPremiumTotal
			// 
			this.txtPremiumTotal.Height = 0.19F;
			this.txtPremiumTotal.HyperLink = null;
			this.txtPremiumTotal.Left = 6.5F;
			this.txtPremiumTotal.Name = "txtPremiumTotal";
			this.txtPremiumTotal.Style = "text-align: right";
			this.txtPremiumTotal.Text = null;
			this.txtPremiumTotal.Top = 6.15625F;
			this.txtPremiumTotal.Width = 1.25F;
			// 
			// Line50
			// 
			this.Line50.Height = 0F;
			this.Line50.Left = 6.1875F;
			this.Line50.LineWeight = 1F;
			this.Line50.Name = "Line50";
			this.Line50.Top = 6.34375F;
			this.Line50.Width = 1.625F;
			this.Line50.X1 = 6.1875F;
			this.Line50.X2 = 7.8125F;
			this.Line50.Y1 = 6.34375F;
			this.Line50.Y2 = 6.34375F;
			// 
			// Line51
			// 
			this.Line51.Height = 0F;
			this.Line51.Left = 6.1875F;
			this.Line51.LineWeight = 1F;
			this.Line51.Name = "Line51";
			this.Line51.Top = 6.09375F;
			this.Line51.Width = 1.625F;
			this.Line51.X1 = 6.1875F;
			this.Line51.X2 = 7.8125F;
			this.Line51.Y1 = 6.09375F;
			this.Line51.Y2 = 6.09375F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.19F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 6.1875F;
			this.Label101.Name = "Label101";
			this.Label101.Style = "text-align: center";
			this.Label101.Text = "B";
			this.Label101.Top = 6.15625F;
			this.Label101.Width = 0.25F;
			// 
			// Line52
			// 
			this.Line52.Height = 0.25F;
			this.Line52.Left = 6.1875F;
			this.Line52.LineWeight = 1F;
			this.Line52.Name = "Line52";
			this.Line52.Top = 6.09375F;
			this.Line52.Width = 0F;
			this.Line52.X1 = 6.1875F;
			this.Line52.X2 = 6.1875F;
			this.Line52.Y1 = 6.09375F;
			this.Line52.Y2 = 6.34375F;
			// 
			// Line48
			// 
			this.Line48.Height = 0.59375F;
			this.Line48.Left = 6.5F;
			this.Line48.LineWeight = 1F;
			this.Line48.Name = "Line48";
			this.Line48.Top = 5.75F;
			this.Line48.Width = 0F;
			this.Line48.X1 = 6.5F;
			this.Line48.X2 = 6.5F;
			this.Line48.Y1 = 5.75F;
			this.Line48.Y2 = 6.34375F;
			// 
			// Line53
			// 
			this.Line53.Height = 0.25F;
			this.Line53.Left = 7.8125F;
			this.Line53.LineWeight = 1F;
			this.Line53.Name = "Line53";
			this.Line53.Top = 6.53125F;
			this.Line53.Width = 0F;
			this.Line53.X1 = 7.8125F;
			this.Line53.X2 = 7.8125F;
			this.Line53.Y1 = 6.53125F;
			this.Line53.Y2 = 6.78125F;
			// 
			// txtC
			// 
			this.txtC.Height = 0.19F;
			this.txtC.HyperLink = null;
			this.txtC.Left = 6.5F;
			this.txtC.Name = "txtC";
			this.txtC.Style = "text-align: right";
			this.txtC.Text = null;
			this.txtC.Top = 6.59375F;
			this.txtC.Width = 1.25F;
			// 
			// Line54
			// 
			this.Line54.Height = 0F;
			this.Line54.Left = 6.1875F;
			this.Line54.LineWeight = 1F;
			this.Line54.Name = "Line54";
			this.Line54.Top = 6.78125F;
			this.Line54.Width = 1.625F;
			this.Line54.X1 = 6.1875F;
			this.Line54.X2 = 7.8125F;
			this.Line54.Y1 = 6.78125F;
			this.Line54.Y2 = 6.78125F;
			// 
			// Line55
			// 
			this.Line55.Height = 0F;
			this.Line55.Left = 6.1875F;
			this.Line55.LineWeight = 1F;
			this.Line55.Name = "Line55";
			this.Line55.Top = 6.53125F;
			this.Line55.Width = 1.625F;
			this.Line55.X1 = 6.1875F;
			this.Line55.X2 = 7.8125F;
			this.Line55.Y1 = 6.53125F;
			this.Line55.Y2 = 6.53125F;
			// 
			// Label103
			// 
			this.Label103.Height = 0.19F;
			this.Label103.HyperLink = null;
			this.Label103.Left = 6.1875F;
			this.Label103.Name = "Label103";
			this.Label103.Style = "text-align: center";
			this.Label103.Text = "C";
			this.Label103.Top = 6.59375F;
			this.Label103.Width = 0.25F;
			// 
			// Line56
			// 
			this.Line56.Height = 0.25F;
			this.Line56.Left = 6.1875F;
			this.Line56.LineWeight = 1F;
			this.Line56.Name = "Line56";
			this.Line56.Top = 6.53125F;
			this.Line56.Width = 0F;
			this.Line56.X1 = 6.1875F;
			this.Line56.X2 = 6.1875F;
			this.Line56.Y1 = 6.53125F;
			this.Line56.Y2 = 6.78125F;
			// 
			// Line57
			// 
			this.Line57.Height = 0.25F;
			this.Line57.Left = 6.5F;
			this.Line57.LineWeight = 1F;
			this.Line57.Name = "Line57";
			this.Line57.Top = 6.53125F;
			this.Line57.Width = 0F;
			this.Line57.X1 = 6.5F;
			this.Line57.X2 = 6.5F;
			this.Line57.Y1 = 6.53125F;
			this.Line57.Y2 = 6.78125F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.3125F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 4.25F;
			this.Label104.Name = "Label104";
			this.Label104.Style = "font-size: 8.5pt; text-align: center";
			this.Label104.Text = "Total before (Credits)/Debits  (Total A + B)";
			this.Label104.Top = 6.53125F;
			this.Label104.Width = 1.6875F;
			// 
			// Label105
			// 
			this.Label105.Height = 0.19F;
			this.Label105.HyperLink = null;
			this.Label105.Left = 0.0625F;
			this.Label105.Name = "Label105";
			this.Label105.Style = "";
			this.Label105.Text = "(CREDITS)/DEBITS";
			this.Label105.Top = 6.90625F;
			this.Label105.Width = 1.125F;
			// 
			// Line58
			// 
			this.Line58.Height = 0F;
			this.Line58.Left = 0.0625F;
			this.Line58.LineWeight = 1F;
			this.Line58.Name = "Line58";
			this.Line58.Top = 7.0625F;
			this.Line58.Width = 1.0625F;
			this.Line58.X1 = 0.0625F;
			this.Line58.X2 = 1.125F;
			this.Line58.Y1 = 7.0625F;
			this.Line58.Y2 = 7.0625F;
			// 
			// Line59
			// 
			this.Line59.Height = 0F;
			this.Line59.Left = 1.25F;
			this.Line59.LineWeight = 1F;
			this.Line59.Name = "Line59";
			this.Line59.Top = 7.3125F;
			this.Line59.Width = 4.625F;
			this.Line59.X1 = 1.25F;
			this.Line59.X2 = 5.875F;
			this.Line59.Y1 = 7.3125F;
			this.Line59.Y2 = 7.3125F;
			// 
			// Line60
			// 
			this.Line60.Height = 0.59375F;
			this.Line60.Left = 1.25F;
			this.Line60.LineWeight = 1F;
			this.Line60.Name = "Line60";
			this.Line60.Top = 7.3125F;
			this.Line60.Width = 0F;
			this.Line60.X1 = 1.25F;
			this.Line60.X2 = 1.25F;
			this.Line60.Y1 = 7.3125F;
			this.Line60.Y2 = 7.90625F;
			// 
			// Line61
			// 
			this.Line61.Height = 0.84375F;
			this.Line61.Left = 3.5625F;
			this.Line61.LineWeight = 1F;
			this.Line61.Name = "Line61";
			this.Line61.Top = 7.3125F;
			this.Line61.Width = 0F;
			this.Line61.X1 = 3.5625F;
			this.Line61.X2 = 3.5625F;
			this.Line61.Y1 = 7.3125F;
			this.Line61.Y2 = 8.15625F;
			// 
			// Line62
			// 
			this.Line62.Height = 0.84375F;
			this.Line62.Left = 5.875F;
			this.Line62.LineWeight = 1F;
			this.Line62.Name = "Line62";
			this.Line62.Top = 7.3125F;
			this.Line62.Width = 0F;
			this.Line62.X1 = 5.875F;
			this.Line62.X2 = 5.875F;
			this.Line62.Y1 = 7.3125F;
			this.Line62.Y2 = 8.15625F;
			// 
			// Line63
			// 
			this.Line63.Height = 0F;
			this.Line63.Left = 1.25F;
			this.Line63.LineWeight = 1F;
			this.Line63.Name = "Line63";
			this.Line63.Top = 7.90625F;
			this.Line63.Width = 4.625F;
			this.Line63.X1 = 1.25F;
			this.Line63.X2 = 5.875F;
			this.Line63.Y1 = 7.90625F;
			this.Line63.Y2 = 7.90625F;
			// 
			// Label106
			// 
			this.Label106.Height = 0.19F;
			this.Label106.HyperLink = null;
			this.Label106.Left = 2.8125F;
			this.Label106.Name = "Label106";
			this.Label106.Style = "text-align: center";
			this.Label106.Text = "9";
			this.Label106.Top = 5.5625F;
			this.Label106.Width = 0.25F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.19F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 4F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "text-align: center";
			this.Label107.Text = "10";
			this.Label107.Top = 5.5625F;
			this.Label107.Width = 0.25F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.19F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 7F;
			this.Label108.Name = "Label108";
			this.Label108.Style = "text-align: center";
			this.Label108.Text = "12";
			this.Label108.Top = 5.5625F;
			this.Label108.Width = 0.25F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.19F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 7F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "text-align: center";
			this.Label109.Text = "7";
			this.Label109.Top = 2.4375F;
			this.Label109.Width = 0.25F;
			// 
			// Line64
			// 
			this.Line64.Height = 1.875F;
			this.Line64.Left = 0.0625F;
			this.Line64.LineWeight = 1F;
			this.Line64.Name = "Line64";
			this.Line64.Top = 2.625F;
			this.Line64.Width = 0F;
			this.Line64.X1 = 0.0625F;
			this.Line64.X2 = 0.0625F;
			this.Line64.Y1 = 2.625F;
			this.Line64.Y2 = 4.5F;
			// 
			// Line65
			// 
			this.Line65.Height = 0.75F;
			this.Line65.Left = 0.0625F;
			this.Line65.LineWeight = 1F;
			this.Line65.Name = "Line65";
			this.Line65.Top = 1.40625F;
			this.Line65.Width = 0F;
			this.Line65.X1 = 0.0625F;
			this.Line65.X2 = 0.0625F;
			this.Line65.Y1 = 1.40625F;
			this.Line65.Y2 = 2.15625F;
			// 
			// txtCreditDebit13
			// 
			this.txtCreditDebit13.Height = 0.19F;
			this.txtCreditDebit13.HyperLink = null;
			this.txtCreditDebit13.Left = 2.375F;
			this.txtCreditDebit13.Name = "txtCreditDebit13";
			this.txtCreditDebit13.Style = "text-align: right";
			this.txtCreditDebit13.Text = null;
			this.txtCreditDebit13.Top = 7.96875F;
			this.txtCreditDebit13.Width = 1.125F;
			// 
			// Line66
			// 
			this.Line66.Height = 0F;
			this.Line66.Left = 2.3125F;
			this.Line66.LineWeight = 1F;
			this.Line66.Name = "Line66";
			this.Line66.Top = 8.15625F;
			this.Line66.Width = 1.25F;
			this.Line66.X1 = 2.3125F;
			this.Line66.X2 = 3.5625F;
			this.Line66.Y1 = 8.15625F;
			this.Line66.Y2 = 8.15625F;
			// 
			// Line67
			// 
			this.Line67.Height = 0.25F;
			this.Line67.Left = 2.3125F;
			this.Line67.LineWeight = 1F;
			this.Line67.Name = "Line67";
			this.Line67.Top = 7.90625F;
			this.Line67.Width = 0F;
			this.Line67.X1 = 2.3125F;
			this.Line67.X2 = 2.3125F;
			this.Line67.Y1 = 7.90625F;
			this.Line67.Y2 = 8.15625F;
			// 
			// txtCreditDebit14
			// 
			this.txtCreditDebit14.Height = 0.19F;
			this.txtCreditDebit14.HyperLink = null;
			this.txtCreditDebit14.Left = 4.6875F;
			this.txtCreditDebit14.Name = "txtCreditDebit14";
			this.txtCreditDebit14.Style = "text-align: right";
			this.txtCreditDebit14.Text = null;
			this.txtCreditDebit14.Top = 7.96875F;
			this.txtCreditDebit14.Width = 1.125F;
			// 
			// Line68
			// 
			this.Line68.Height = 0F;
			this.Line68.Left = 4.625F;
			this.Line68.LineWeight = 1F;
			this.Line68.Name = "Line68";
			this.Line68.Top = 8.15625F;
			this.Line68.Width = 1.25F;
			this.Line68.X1 = 4.625F;
			this.Line68.X2 = 5.875F;
			this.Line68.Y1 = 8.15625F;
			this.Line68.Y2 = 8.15625F;
			// 
			// Line69
			// 
			this.Line69.Height = 0.25F;
			this.Line69.Left = 4.625F;
			this.Line69.LineWeight = 1F;
			this.Line69.Name = "Line69";
			this.Line69.Top = 7.90625F;
			this.Line69.Width = 0F;
			this.Line69.X1 = 4.625F;
			this.Line69.X2 = 4.625F;
			this.Line69.Y1 = 7.90625F;
			this.Line69.Y2 = 8.15625F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.19F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 1.75F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-size: 8.5pt; text-align: left";
			this.Label112.Text = "(Credit) or Debit Taken";
			this.Label112.Top = 7.34375F;
			this.Label112.Width = 1.3125F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.19F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 4.0625F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-size: 8.5pt; text-align: left";
			this.Label113.Text = "(Credit) or Debit Taken";
			this.Label113.Top = 7.34375F;
			this.Label113.Width = 1.3125F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.19F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 1.3125F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "font-size: 8.5pt; text-align: left";
			this.Label114.Text = "Enter (CM) or DM #";
			this.Label114.Top = 7.65625F;
			this.Label114.Width = 1.1875F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.19F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 3.625F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-size: 8.5pt; text-align: left";
			this.Label115.Text = "Enter (CM) or DM #";
			this.Label115.Top = 7.65625F;
			this.Label115.Width = 1.1875F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.19F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 7F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "text-align: center";
			this.Label116.Text = "15";
			this.Label116.Top = 7.125F;
			this.Label116.Width = 0.25F;
			// 
			// Line70
			// 
			this.Line70.Height = 0F;
			this.Line70.Left = 6.5F;
			this.Line70.LineWeight = 1F;
			this.Line70.Name = "Line70";
			this.Line70.Top = 7.3125F;
			this.Line70.Width = 1.3125F;
			this.Line70.X1 = 6.5F;
			this.Line70.X2 = 7.8125F;
			this.Line70.Y1 = 7.3125F;
			this.Line70.Y2 = 7.3125F;
			// 
			// Line71
			// 
			this.Line71.Height = 0.84375F;
			this.Line71.Left = 6.5F;
			this.Line71.LineWeight = 1F;
			this.Line71.Name = "Line71";
			this.Line71.Top = 7.3125F;
			this.Line71.Width = 0F;
			this.Line71.X1 = 6.5F;
			this.Line71.X2 = 6.5F;
			this.Line71.Y1 = 7.3125F;
			this.Line71.Y2 = 8.15625F;
			// 
			// Line72
			// 
			this.Line72.Height = 0.84375F;
			this.Line72.Left = 7.8125F;
			this.Line72.LineWeight = 1F;
			this.Line72.Name = "Line72";
			this.Line72.Top = 7.3125F;
			this.Line72.Width = 0F;
			this.Line72.X1 = 7.8125F;
			this.Line72.X2 = 7.8125F;
			this.Line72.Y1 = 7.3125F;
			this.Line72.Y2 = 8.15625F;
			// 
			// Line73
			// 
			this.Line73.Height = 0F;
			this.Line73.Left = 6.1875F;
			this.Line73.LineWeight = 1F;
			this.Line73.Name = "Line73";
			this.Line73.Top = 7.90625F;
			this.Line73.Width = 1.625F;
			this.Line73.X1 = 6.1875F;
			this.Line73.X2 = 7.8125F;
			this.Line73.Y1 = 7.90625F;
			this.Line73.Y2 = 7.90625F;
			// 
			// txtD
			// 
			this.txtD.Height = 0.19F;
			this.txtD.HyperLink = null;
			this.txtD.Left = 6.5625F;
			this.txtD.Name = "txtD";
			this.txtD.Style = "text-align: right";
			this.txtD.Text = null;
			this.txtD.Top = 7.96875F;
			this.txtD.Width = 1.1875F;
			// 
			// Line74
			// 
			this.Line74.Height = 0F;
			this.Line74.Left = 6.1875F;
			this.Line74.LineWeight = 1F;
			this.Line74.Name = "Line74";
			this.Line74.Top = 8.15625F;
			this.Line74.Width = 1.625F;
			this.Line74.X1 = 6.1875F;
			this.Line74.X2 = 7.8125F;
			this.Line74.Y1 = 8.15625F;
			this.Line74.Y2 = 8.15625F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.19F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 6.5625F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-size: 8.5pt; text-align: left";
			this.Label118.Text = "Total (Credits)/Debits";
			this.Label118.Top = 7.34375F;
			this.Label118.Width = 1.1875F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.19F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 6.5625F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "font-size: 8.5pt; text-align: left";
			this.Label119.Text = "(13 + 14)";
			this.Label119.Top = 7.65625F;
			this.Label119.Width = 1.1875F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.19F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 6.1875F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "text-align: center";
			this.Label120.Text = "D";
			this.Label120.Top = 7.96875F;
			this.Label120.Width = 0.25F;
			// 
			// Line75
			// 
			this.Line75.Height = 0.25F;
			this.Line75.Left = 6.1875F;
			this.Line75.LineWeight = 1F;
			this.Line75.Name = "Line75";
			this.Line75.Top = 7.90625F;
			this.Line75.Width = 0F;
			this.Line75.X1 = 6.1875F;
			this.Line75.X2 = 6.1875F;
			this.Line75.Y1 = 7.90625F;
			this.Line75.Y2 = 8.15625F;
			// 
			// Line76
			// 
			this.Line76.Height = 0.25F;
			this.Line76.Left = 6.5F;
			this.Line76.LineWeight = 1F;
			this.Line76.Name = "Line76";
			this.Line76.Top = 8.34375F;
			this.Line76.Width = 0F;
			this.Line76.X1 = 6.5F;
			this.Line76.X2 = 6.5F;
			this.Line76.Y1 = 8.34375F;
			this.Line76.Y2 = 8.59375F;
			// 
			// Line77
			// 
			this.Line77.Height = 0.25F;
			this.Line77.Left = 7.8125F;
			this.Line77.LineWeight = 1F;
			this.Line77.Name = "Line77";
			this.Line77.Top = 8.34375F;
			this.Line77.Width = 0F;
			this.Line77.X1 = 7.8125F;
			this.Line77.X2 = 7.8125F;
			this.Line77.Y1 = 8.34375F;
			this.Line77.Y2 = 8.59375F;
			// 
			// Line78
			// 
			this.Line78.Height = 0F;
			this.Line78.Left = 6.5F;
			this.Line78.LineWeight = 1F;
			this.Line78.Name = "Line78";
			this.Line78.Top = 8.34375F;
			this.Line78.Width = 1.3125F;
			this.Line78.X1 = 6.5F;
			this.Line78.X2 = 7.8125F;
			this.Line78.Y1 = 8.34375F;
			this.Line78.Y2 = 8.34375F;
			// 
			// txtTotalRemittance
			// 
			this.txtTotalRemittance.Height = 0.19F;
			this.txtTotalRemittance.HyperLink = null;
			this.txtTotalRemittance.Left = 6.5625F;
			this.txtTotalRemittance.Name = "txtTotalRemittance";
			this.txtTotalRemittance.Style = "text-align: right";
			this.txtTotalRemittance.Text = null;
			this.txtTotalRemittance.Top = 8.40625F;
			this.txtTotalRemittance.Width = 1.1875F;
			// 
			// Line79
			// 
			this.Line79.Height = 0F;
			this.Line79.Left = 6.5F;
			this.Line79.LineWeight = 1F;
			this.Line79.Name = "Line79";
			this.Line79.Top = 8.59375F;
			this.Line79.Width = 1.3125F;
			this.Line79.X1 = 6.5F;
			this.Line79.X2 = 7.8125F;
			this.Line79.Y1 = 8.59375F;
			this.Line79.Y2 = 8.59375F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.3125F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 5.3125F;
			this.Label122.Name = "Label122";
			this.Label122.Style = "font-size: 8.5pt; text-align: center";
			this.Label122.Text = "Total Remittance (Total C + Total D)";
			this.Label122.Top = 8.3125F;
			this.Label122.Width = 1.0625F;
			// 
			// Line80
			// 
			this.Line80.Height = 0F;
			this.Line80.Left = 0.0625F;
			this.Line80.LineWeight = 1F;
			this.Line80.Name = "Line80";
			this.Line80.Top = 8.8125F;
			this.Line80.Width = 7.75F;
			this.Line80.X1 = 0.0625F;
			this.Line80.X2 = 7.8125F;
			this.Line80.Y1 = 8.8125F;
			this.Line80.Y2 = 8.8125F;
			// 
			// Line81
			// 
			this.Line81.Height = 0F;
			this.Line81.Left = 0.0625F;
			this.Line81.LineWeight = 1F;
			this.Line81.Name = "Line81";
			this.Line81.Top = 9.90625F;
			this.Line81.Width = 7.75F;
			this.Line81.X1 = 0.0625F;
			this.Line81.X2 = 7.8125F;
			this.Line81.Y1 = 9.90625F;
			this.Line81.Y2 = 9.90625F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.25F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 6.875F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-size: 7pt; text-align: right";
			this.Label123.Text = "Form #AC-1176  Rev. 2/08";
			this.Label123.Top = 9.9375F;
			this.Label123.Width = 0.9375F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.3125F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 0.0625F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-style: italic";
			this.Label124.Text = "My signature certifies that I am the person responsible for the content of this r" + "eport, that I have reviewed the Summary and Detail Reports, and that they are co" + "mplete and accurate.";
			this.Label124.Top = 8.875F;
			this.Label124.Width = 7.8125F;
			// 
			// Line82
			// 
			this.Line82.Height = 1.09375F;
			this.Line82.Left = 7.8125F;
			this.Line82.LineWeight = 1F;
			this.Line82.Name = "Line82";
			this.Line82.Top = 8.8125F;
			this.Line82.Width = 0F;
			this.Line82.X1 = 7.8125F;
			this.Line82.X2 = 7.8125F;
			this.Line82.Y1 = 8.8125F;
			this.Line82.Y2 = 9.90625F;
			// 
			// Line83
			// 
			this.Line83.Height = 1.09375F;
			this.Line83.Left = 0.0625F;
			this.Line83.LineWeight = 1F;
			this.Line83.Name = "Line83";
			this.Line83.Top = 8.8125F;
			this.Line83.Width = 0F;
			this.Line83.X1 = 0.0625F;
			this.Line83.X2 = 0.0625F;
			this.Line83.Y1 = 8.8125F;
			this.Line83.Y2 = 9.90625F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.19F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 0.1875F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-size: 9pt; text-align: left";
			this.Label125.Text = "Signature:";
			this.Label125.Top = 9.375F;
			this.Label125.Width = 0.8125F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.19F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 0.1875F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-size: 9pt; text-align: left";
			this.Label126.Text = "Title:";
			this.Label126.Top = 9.71875F;
			this.Label126.Width = 0.8125F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.19F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 4.6875F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-size: 9pt; text-align: left";
			this.Label127.Text = "Date:";
			this.Label127.Top = 9.375F;
			this.Label127.Width = 0.375F;
			// 
			// Line84
			// 
			this.Line84.Height = 0F;
			this.Line84.Left = 1.0625F;
			this.Line84.LineWeight = 1F;
			this.Line84.Name = "Line84";
			this.Line84.Top = 9.53125F;
			this.Line84.Width = 3.375F;
			this.Line84.X1 = 1.0625F;
			this.Line84.X2 = 4.4375F;
			this.Line84.Y1 = 9.53125F;
			this.Line84.Y2 = 9.53125F;
			// 
			// Line85
			// 
			this.Line85.Height = 0F;
			this.Line85.Left = 1.0625F;
			this.Line85.LineWeight = 1F;
			this.Line85.Name = "Line85";
			this.Line85.Top = 9.84375F;
			this.Line85.Width = 3.375F;
			this.Line85.X1 = 1.0625F;
			this.Line85.X2 = 4.4375F;
			this.Line85.Y1 = 9.84375F;
			this.Line85.Y2 = 9.84375F;
			// 
			// Line86
			// 
			this.Line86.Height = 0F;
			this.Line86.Left = 5.1875F;
			this.Line86.LineWeight = 1F;
			this.Line86.Name = "Line86";
			this.Line86.Top = 9.53125F;
			this.Line86.Width = 2.5625F;
			this.Line86.X1 = 5.1875F;
			this.Line86.X2 = 7.75F;
			this.Line86.Y1 = 9.53125F;
			this.Line86.Y2 = 9.53125F;
			// 
			// txtCMDM1
			// 
			this.txtCMDM1.Height = 0.19F;
			this.txtCMDM1.HyperLink = null;
			this.txtCMDM1.Left = 2.5F;
			this.txtCMDM1.Name = "txtCMDM1";
			this.txtCMDM1.Style = "text-align: left";
			this.txtCMDM1.Text = null;
			this.txtCMDM1.Top = 7.65625F;
			this.txtCMDM1.Width = 1.0625F;
			// 
			// txtCMDM2
			// 
			this.txtCMDM2.Height = 0.19F;
			this.txtCMDM2.HyperLink = null;
			this.txtCMDM2.Left = 4.8125F;
			this.txtCMDM2.Name = "txtCMDM2";
			this.txtCMDM2.Style = "text-align: left";
			this.txtCMDM2.Text = null;
			this.txtCMDM2.Top = 7.65625F;
			this.txtCMDM2.Width = 1.0625F;
			// 
			// txtExplanation
			// 
			this.txtExplanation.Height = 0.1666667F;
			this.txtExplanation.Left = 0.08333334F;
			this.txtExplanation.Name = "txtExplanation";
			this.txtExplanation.Text = null;
			this.txtExplanation.Top = 0F;
			this.txtExplanation.Width = 7.75F;
			// 
			// rptMSRSPayrollSummary
			//
			// 
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidDatesEnding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPreparersName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlan6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEarnableComp6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpCont6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCredit6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplCont6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpContTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCostCreditTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmplContTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCont6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalContTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActivePremium)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRetireePremium)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSupplementalPremiums)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentPremiums)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPremiumTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCreditDebit13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCreditDebit14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalRemittance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCMDM1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCMDM2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExplanation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPaidDatesEnding;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPreparersName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPlan1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerRate1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEarnableComp1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmpCont1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCostCredit1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmplCont1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPlan2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerRate2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEarnableComp2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmpCont2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCostCredit2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmplCont2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPlan3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerRate3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEarnableComp3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmpCont3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCostCredit3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmplCont3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPlan4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerRate4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEarnableComp4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmpCont4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCostCredit4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmplCont4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPlan5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerRate5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEarnableComp5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmpCont5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCostCredit5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmplCont5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPlan6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmployerRate6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEarnableComp6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmpCont6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCostCredit6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmplCont6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmpContTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCostCreditTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtEmplContTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalCont1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalCont2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalCont3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalCont4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalCont5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalCont6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalContTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtActivePremium;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRetireePremium;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSupplementalPremiums;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDependentPremiums;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPremiumTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line52;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtC;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line54;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line61;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line63;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line64;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line65;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCreditDebit13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line66;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line67;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCreditDebit14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line68;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line70;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line71;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line72;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line73;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtD;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line74;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line75;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line76;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line77;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line78;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTotalRemittance;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line79;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line80;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line81;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line82;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line83;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line84;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line85;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line86;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCMDM1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCMDM2;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExplanation;
	}
}
