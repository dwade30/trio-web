namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptFedStateFilingStatus.
	/// </summary>
	partial class rptFedStateFilingStatus
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptFedStateFilingStatus));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkFederal = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkState = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFederal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.chkFederal,
				this.chkState
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtCaption,
				this.txtDate,
				this.Line1,
				this.Label6,
				this.lblPage,
				this.txtTime,
				this.Label7,
				this.Label8
			});
			this.PageHeader.Height = 1F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1875F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: center";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.0625F;
			this.txtCaption.Width = 7.8125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.75F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.0625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7.9375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.2F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.06666667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label6.Text = "Name";
			this.Label6.Top = 0.8F;
			this.Label6.Width = 0.5666667F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.75F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.lblPage.Text = "Label5";
			this.lblPage.Top = 0.25F;
			this.lblPage.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.OutputFormat = resources.GetString("txtTime.OutputFormat");
			this.txtTime.Style = "font-family: \'Courier\'; font-size: 8pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.2F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label7.Text = "Federal";
			this.Label7.Top = 0.8F;
			this.Label7.Width = 0.5666667F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.2F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.733333F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label8.Text = "State";
			this.Label8.Top = 0.8F;
			this.Label8.Width = 0.5666667F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.2F;
			this.txtName.Left = 0.1F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 3.633333F;
			// 
			// chkFederal
			// 
			this.chkFederal.Height = 0.1333333F;
			this.chkFederal.Left = 4.2F;
			this.chkFederal.Name = "chkFederal";
			this.chkFederal.Style = "";
			this.chkFederal.Text = "CheckBox";
			this.chkFederal.Top = 0.03333334F;
			this.chkFederal.Width = 0.1666667F;
			// 
			// chkState
			// 
			this.chkState.Height = 0.1333333F;
			this.chkState.Left = 4.866667F;
			this.chkState.Name = "chkState";
			this.chkState.Style = "";
			this.chkState.Text = "CheckBox";
			this.chkState.Top = 0.03333334F;
			this.chkState.Width = 0.1666667F;
			// 
			// rptFedStateFilingStatus
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.2222222F;
			this.PageSettings.Margins.Right = 0.2222222F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFederal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkFederal;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkState;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}