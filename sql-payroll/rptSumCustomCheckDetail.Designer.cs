﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptSumCustomCheckDetail.
	/// </summary>
	partial class rptSumCustomCheckDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSumCustomCheckDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblEmp = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField1Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField2Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField3Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField4Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField5Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField6Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField7Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtField8Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEmp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField1Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField2Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField3Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField4Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField5Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField6Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField7Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField8Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployee,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8
			});
			this.Detail.Height = 0.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtField1Total,
				this.txtField2Total,
				this.txtField3Total,
				this.txtField4Total,
				this.txtField5Total,
				this.txtField6Total,
				this.txtField7Total,
				this.txtField8Total,
				this.Line2,
				this.Label9
			});
			this.ReportFooter.Height = 0.5F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCaption,
				this.txtMuniName,
				this.txtCaption2,
				this.txtDate,
				this.lblEmp,
				this.Label1,
				this.Line1,
				this.txtTime,
				this.lblPage,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8
			});
			this.PageHeader.Height = 1.03125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1666667F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.08333334F;
			this.txtCaption.Width = 9.875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.08333334F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtCaption2
			// 
			this.txtCaption2.Height = 0.2083333F;
			this.txtCaption2.Left = 0.0625F;
			this.txtCaption2.Name = "txtCaption2";
			this.txtCaption2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.txtCaption2.Text = null;
			this.txtCaption2.Top = 0.25F;
			this.txtCaption2.Width = 9.875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 8.875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.08333334F;
			this.txtDate.Width = 1.0625F;
			// 
			// lblEmp
			// 
			this.lblEmp.Height = 0.1666667F;
			this.lblEmp.HyperLink = null;
			this.lblEmp.Left = 0.09375F;
			this.lblEmp.Name = "lblEmp";
			this.lblEmp.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.lblEmp.Text = "Employee";
			this.lblEmp.Top = 0.8333333F;
			this.lblEmp.Width = 1.28125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label1.Text = null;
			this.Label1.Top = 0.8333333F;
			this.Label1.Width = 0.8125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 9.90625F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 9.90625F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.2083333F;
			this.lblPage.Left = 8.875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.25F;
			this.lblPage.Width = 1.0625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 3F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label2.Text = null;
			this.Label2.Top = 0.8333333F;
			this.Label2.Width = 0.8125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.9375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label3.Text = null;
			this.Label3.Top = 0.8333333F;
			this.Label3.Width = 0.8125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.90625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label4.Text = null;
			this.Label4.Top = 0.8333333F;
			this.Label4.Width = 0.8125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.90625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label5.Text = null;
			this.Label5.Top = 0.8333333F;
			this.Label5.Width = 0.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label6.Text = null;
			this.Label6.Top = 0.8333333F;
			this.Label6.Width = 0.8125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 7.875F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label7.Text = null;
			this.Label7.Top = 0.8333333F;
			this.Label7.Width = 0.8125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1666667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 8.875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label8.Text = null;
			this.Label8.Top = 0.8333333F;
			this.Label8.Width = 0.8125F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.2083333F;
			this.txtEmployee.Left = 0.09375F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtEmployee.Text = null;
			this.txtEmployee.Top = 0F;
			this.txtEmployee.Width = 1.875F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.2083333F;
			this.Field1.Left = 2.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Field1.Text = null;
			this.Field1.Top = 0F;
			this.Field1.Width = 0.8125F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.2083333F;
			this.Field2.Left = 3F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Field2.Text = null;
			this.Field2.Top = 0F;
			this.Field2.Width = 0.8125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.2083333F;
			this.Field3.Left = 3.9375F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Field3.Text = null;
			this.Field3.Top = 0F;
			this.Field3.Width = 0.8125F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.2083333F;
			this.Field4.Left = 4.9375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Field4.Text = null;
			this.Field4.Top = 0F;
			this.Field4.Width = 0.8125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.2083333F;
			this.Field5.Left = 5.90625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Field5.Text = null;
			this.Field5.Top = 0F;
			this.Field5.Width = 0.8125F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.2083333F;
			this.Field6.Left = 6.90625F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Field6.Text = null;
			this.Field6.Top = 0F;
			this.Field6.Width = 0.8125F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.2083333F;
			this.Field7.Left = 7.90625F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Field7.Text = null;
			this.Field7.Top = 0F;
			this.Field7.Width = 0.8125F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.2083333F;
			this.Field8.Left = 8.90625F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center; ddo-char-set: 0";
			this.Field8.Text = null;
			this.Field8.Top = 0F;
			this.Field8.Width = 0.8125F;
			// 
			// txtField1Total
			// 
			this.txtField1Total.Height = 0.2083333F;
			this.txtField1Total.Left = 2.0625F;
			this.txtField1Total.Name = "txtField1Total";
			this.txtField1Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtField1Total.Text = null;
			this.txtField1Total.Top = 0.125F;
			this.txtField1Total.Width = 0.8125F;
			// 
			// txtField2Total
			// 
			this.txtField2Total.Height = 0.2083333F;
			this.txtField2Total.Left = 3F;
			this.txtField2Total.Name = "txtField2Total";
			this.txtField2Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtField2Total.Text = null;
			this.txtField2Total.Top = 0.125F;
			this.txtField2Total.Width = 0.8125F;
			// 
			// txtField3Total
			// 
			this.txtField3Total.Height = 0.2083333F;
			this.txtField3Total.Left = 3.9375F;
			this.txtField3Total.Name = "txtField3Total";
			this.txtField3Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtField3Total.Text = null;
			this.txtField3Total.Top = 0.125F;
			this.txtField3Total.Width = 0.8125F;
			// 
			// txtField4Total
			// 
			this.txtField4Total.Height = 0.2083333F;
			this.txtField4Total.Left = 4.9375F;
			this.txtField4Total.Name = "txtField4Total";
			this.txtField4Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtField4Total.Text = null;
			this.txtField4Total.Top = 0.125F;
			this.txtField4Total.Width = 0.8125F;
			// 
			// txtField5Total
			// 
			this.txtField5Total.Height = 0.2083333F;
			this.txtField5Total.Left = 5.90625F;
			this.txtField5Total.Name = "txtField5Total";
			this.txtField5Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtField5Total.Text = null;
			this.txtField5Total.Top = 0.125F;
			this.txtField5Total.Width = 0.8125F;
			// 
			// txtField6Total
			// 
			this.txtField6Total.Height = 0.2083333F;
			this.txtField6Total.Left = 6.90625F;
			this.txtField6Total.Name = "txtField6Total";
			this.txtField6Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtField6Total.Text = null;
			this.txtField6Total.Top = 0.125F;
			this.txtField6Total.Width = 0.8125F;
			// 
			// txtField7Total
			// 
			this.txtField7Total.Height = 0.2083333F;
			this.txtField7Total.Left = 7.90625F;
			this.txtField7Total.Name = "txtField7Total";
			this.txtField7Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtField7Total.Text = null;
			this.txtField7Total.Top = 0.125F;
			this.txtField7Total.Width = 0.8125F;
			// 
			// txtField8Total
			// 
			this.txtField8Total.Height = 0.2083333F;
			this.txtField8Total.Left = 8.90625F;
			this.txtField8Total.Name = "txtField8Total";
			this.txtField8Total.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtField8Total.Text = null;
			this.txtField8Total.Top = 0.125F;
			this.txtField8Total.Width = 0.8125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.90625F;
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.08333334F;
			this.Line2.Width = 7.90625F;
			this.Line2.X1 = 1.90625F;
			this.Line2.X2 = 9.8125F;
			this.Line2.Y1 = 0.08333334F;
			this.Line2.Y2 = 0.08333334F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.65625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label9.Text = "Totals";
			this.Label9.Top = 0.125F;
			this.Label9.Width = 1.28125F;
			// 
			// rptSumCustomCheckDetail
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEmp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField1Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField2Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField3Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField4Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField5Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField6Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField7Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtField8Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField1Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField2Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField3Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField4Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField5Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField6Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField7Total;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtField8Total;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEmp;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
