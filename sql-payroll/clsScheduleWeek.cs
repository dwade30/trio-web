//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsScheduleWeek
	{
		//=========================================================
		private clsScheduleDay[] WorkWeekList = new clsScheduleDay[7 + 1];
		private int lngScheduleID;
		private int lngWeekID;
		private int intOrderNo;
		private string strName = "";
		private bool boolNotUsed;
		private string strEmployeeNumber = string.Empty;
		private double dblAlwaysPayHours;
		private clsPayTotalsByCat PayTots = new clsPayTotalsByCat();
		private modSchedule.OverTimeOrder intOvertimeSetting;
		private double dblOvertimeStart;

		public void InitPayTots()
		{
			PayTots.LoadCats();
		}

		public double OvertimeStart
		{
			set
			{
				dblOvertimeStart = value;
			}
			get
			{
				double OvertimeStart = 0;
				OvertimeStart = dblOvertimeStart;
				return OvertimeStart;
			}
		}

		public string EmployeeNumber
		{
			set
			{
				strEmployeeNumber = value;
			}
			get
			{
				string EmployeeNumber = "";
				EmployeeNumber = strEmployeeNumber;
				return EmployeeNumber;
			}
		}

		public double AlwaysPayHours
		{
			set
			{
				dblAlwaysPayHours = value;
			}
			get
			{
				double AlwaysPayHours = 0;
				AlwaysPayHours = dblAlwaysPayHours;
				return AlwaysPayHours;
			}
		}

		public bool SaveEmployeeWeek()
		{
			bool SaveEmployeeWeek = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int X;
				bool boolReturn;
				SaveEmployeeWeek = false;
				boolReturn = true;
				for (X = 1; X <= 7; X++)
				{
					if (!(WorkWeekList[X] == null))
					{
						boolReturn = boolReturn && WorkWeekList[X].SaveEmployeeDay();
					}
				}
				// X
				SaveEmployeeWeek = boolReturn;
				return SaveEmployeeWeek;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveEmployeeWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveEmployeeWeek;
		}

		public void MarkWeekAsUnworked()
		{
			int X;
			for (X = 1; X <= 7; X++)
			{
				if (!(WorkWeekList[X] == null))
				{
					WorkWeekList[X].MarkAllShiftsAsUnworked();
				}
			}
			// X
		}

		public bool LoadEmployeeWeek(DateTime dtDate)
		{
			bool LoadEmployeeWeek = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadEmployeeWeek = false;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL = "";
				DateTime dtStart;
				DateTime dtEnd;
				int X;
				DateTime dtTemp;
				int intTemp;
				int intCurWeek = 0;
				int intWeek = 0;
				int intWorkWeek = 0;
				int intShiftWeek = 0;
				clsScheduleWeek tWeek;
				clsSchedule tSched = new clsSchedule();
				dtTemp = modGlobalVariables.Statics.gdatCurrentWeekEndingDate;
				// get the beginning and ending dates for the week containing dtdate
				dtStart = modSchedule.GetWeekBeginningDate(dtDate);
				rsLoad.OpenRecordset("select * from employeeshifts where employeenumber = '" + strEmployeeNumber + "' and shiftdate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("d", 6, dtStart)) + "'", "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					// already exists
					boolNotUsed = false;
					intWeek = modSchedule.WeekOfYear(dtDate);
					intCurWeek = modSchedule.WeekOfYear(dtTemp);
					for (X = 1; X <= 7; X++)
					{
						WorkWeekList[X].ClearDay();
						WorkWeekList[X].DayOfWeek = X;
						dtTemp = fecherFoundation.DateAndTime.DateAdd("d", X - 1, dtStart);
						WorkWeekList[X].LoadEmployeeDay(dtTemp, ref strEmployeeNumber);
					}
					// X
					rsLoad.OpenRecordset("select scheduleid,currentscheduleweek from tblemployeemaster where employeenumber = '" + strEmployeeNumber + "'", "twpy0000.vb1");
					if (!rsLoad.EndOfFile())
					{
						lngScheduleID = FCConvert.ToInt32(rsLoad.Get_Fields("scheduleid"));
						intWorkWeek = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("currentscheduleweek"))));
					}
					intShiftWeek = intWorkWeek;
					tSched.ScheduleID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("scheduleid"))));
					tSched.LoadSchedule(true);
					intShiftWeek = tSched.GetRotationWeekFromAnyWeek(intCurWeek, intShiftWeek, intWeek);
					intOrderNo = intShiftWeek;
				}
				else
				{
					// must create content
					boolNotUsed = false;
					intWeek = modSchedule.WeekOfYear(dtDate);
					intCurWeek = modSchedule.WeekOfYear(dtTemp);
					if (intWeek >= intCurWeek)
					{
						// don't create shifts for weeks that are already past
						rsLoad.OpenRecordset("select scheduleid,currentscheduleweek from tblemployeemaster where employeenumber = '" + strEmployeeNumber + "'", "twpy0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							intWorkWeek = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("currentscheduleweek"))));
						}
						else
						{
							intWorkWeek = 0;
						}
						intShiftWeek = intWorkWeek;
						tSched.ScheduleID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("scheduleid"))));
						lngScheduleID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("scheduleid"))));
						tSched.LoadSchedule(true);
						// intTemp = tSched.HighestWeek
						// If intTemp > 0 Then
						// intShiftWeek = ((intWeek - intCurWeek) Mod intTemp) + intShiftWeek
						intShiftWeek = tSched.GetRotationWeekFromAnyWeek(intCurWeek, intShiftWeek, intWeek);
						// End If
						intOrderNo = intShiftWeek;
						tWeek = tSched.GetWeekByNumber(intShiftWeek);
						clsHolidaysList tHolidayList = new clsHolidaysList();
						clsHoliday tHoliday;
						tHolidayList.LoadHolidays();
						for (X = 1; X <= 7; X++)
						{
							WorkWeekList[X].ClearDay();
							WorkWeekList[X].DayOfWeek = X;
							if (!(tWeek == null))
							{
								WorkWeekList[X] = tWeek.GetDay(X);
								WorkWeekList[X].MarkAllShiftsAsWorked();
								WorkWeekList[X].EmployeeNumber = strEmployeeNumber;
								// clear the id's since we loaded from default table but are creating new employee shift records
								WorkWeekList[X].ClearShiftIDs();
							}
							WorkWeekList[X].WorkDate = fecherFoundation.DateAndTime.DateAdd("d", X - 1, dtStart);
							if (tHolidayList.IsAHoliday(WorkWeekList[X].WorkDate))
							{
								tHoliday = tHolidayList.GetHolidayByDate(WorkWeekList[X].WorkDate);
								rsLoad.OpenRecordset("select * from shifttypes where shifttype = " + FCConvert.ToString(FCConvert.ToInt32(modSchedule.ScheduleShiftType.GiftHoliday)), "twpy0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									if (X > 1 && X < 7)
									{
										WorkWeekList[X].AddShift(FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("id"))), "08:00 AM", "04:00 PM", "08:00 AM", "04:00 PM", 0, 0, "");
									}
									else
									{
										DateTime tDate;
										if (X == 1)
										{
											tDate = fecherFoundation.DateAndTime.DateAdd("d", -1, WorkWeekList[X].WorkDate);
										}
										else
										{
											tDate = fecherFoundation.DateAndTime.DateAdd("d", 1, WorkWeekList[X].WorkDate);
										}
										clsScheduleDay tDay = new clsScheduleDay();
										tDay.LoadEmployeeDay(tDate, ref strEmployeeNumber);
										if (tDay.HasUnworkedOrSick())
										{
											WorkWeekList[X].AddShift(rsLoad.Get_Fields("id"), "08:00 AM", "04:00 PM", "Unworked", "Unworked", 0, 0, "");
										}
										else
										{
											WorkWeekList[X].AddShift(rsLoad.Get_Fields("id"), "08:00 AM", "04:00 PM", "08:00 AM", "04:00 PM", 0, 0, "");
										}
									}
								}
							}
						}
						// X
					}
					else
					{
						for (X = 1; X <= 7; X++)
						{
							WorkWeekList[X].ClearDay();
							WorkWeekList[X].DayOfWeek = X;
							WorkWeekList[X].WorkDate = fecherFoundation.DateAndTime.DateAdd("d", X - 1, dtStart);
						}
						// X
					}
				}
				LoadEmployeeWeek = true;
				return LoadEmployeeWeek;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadEmployeeWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadEmployeeWeek;
		}

		public bool LoadDefaultWeek(int lngID)
		{
			bool LoadDefaultWeek = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadDefaultWeek = false;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				int X;
				intOvertimeSetting = modSchedule.OverTimeOrder.ScheduledFirst;
				strSQL = "Select * from ScheduleWeeks where id = " + FCConvert.ToString(lngID);
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					lngWeekID = lngID;
					lngScheduleID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ScheduleID"))));
					intOrderNo = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("OrderNo"))));
					dblOvertimeStart = Conversion.Val(rsLoad.Get_Fields_Double("OvertimeStart"));
					dblAlwaysPayHours = Conversion.Val(rsLoad.Get_Fields_Double("AlwaysPayHours"));
					boolNotUsed = false;
					strName = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					for (X = 1; X <= 7; X++)
					{
						WorkWeekList[X].ClearDay();
						WorkWeekList[X].DayOfWeek = X;
						WorkWeekList[X].LoadDefaultDay(lngScheduleID, lngWeekID, X);
					}
					// X
					LoadDefaultWeek = true;
				}
				else
				{
					for (X = 1; X <= 7; X++)
					{
						WorkWeekList[X].DayOfWeek = X;
						WorkWeekList[X].ClearDay();
					}
					// X
					dblAlwaysPayHours = 0;
					LoadDefaultWeek = false;
					dblOvertimeStart = 0;
					return LoadDefaultWeek;
				}
				LoadDefaultWeek = true;
				return LoadDefaultWeek;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadDefaultWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadDefaultWeek;
		}

		public void LoadSettings()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			intOvertimeSetting = modSchedule.OverTimeOrder.ScheduledFirst;
			rsLoad.OpenRecordset("select * from scheduleweeks where scheduleid = " + FCConvert.ToString(lngScheduleID) + " and orderno = " + FCConvert.ToString(intOrderNo), "twpy0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				lngWeekID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
				dblAlwaysPayHours = Conversion.Val(rsLoad.Get_Fields_Double("AlwaysPayHours"));
				dblOvertimeStart = Conversion.Val(rsLoad.Get_Fields_Double("OvertimeStart"));
			}
			else
			{
				dblAlwaysPayHours = 0;
				lngWeekID = 0;
				dblOvertimeStart = 0;
			}
		}

		public void DeleteDefaultWeek()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int X;
				for (X = 1; X <= 7; X++)
				{
					WorkWeekList[X].DeleteDefaultDay();
					WorkWeekList[X].ClearDay();
					boolNotUsed = true;
				}
				// X
				string strSQL;
				clsDRWrapper rsSave = new clsDRWrapper();
				strSQL = "delete from scheduleweeks where WEEKid = " + FCConvert.ToString(lngWeekID);
				rsSave.Execute(strSQL, "twpy0000.vb1");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteDefaultWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool SaveDefaultWeek()
		{
			bool SaveDefaultWeek = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				bool boolReturn;
				boolReturn = true;
				SaveDefaultWeek = false;
				if (!boolNotUsed)
				{
					rsSave.OpenRecordset("select * from scheduleweeks where weekid = " + FCConvert.ToString(lngWeekID), "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("ScheduleID", lngScheduleID);
					rsSave.Set_Fields("OrderNo", intOrderNo);
					rsSave.Set_Fields("Name", strName);
					rsSave.Set_Fields("AlwaysPayHours", dblAlwaysPayHours);
					rsSave.Set_Fields("Overtimestart", dblOvertimeStart);
					rsSave.Update();
					lngWeekID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("WeekID"));
					int X;
					for (X = 1; X <= 7; X++)
					{
						WorkWeekList[X].WeekID = lngWeekID;
						WorkWeekList[X].WorkDate = DateTime.FromOADate(0);
						WorkWeekList[X].DayOfWeek = X;
						WorkWeekList[X].ScheduleID = lngScheduleID;
						boolReturn = boolReturn && WorkWeekList[X].SaveDefaultDay();
					}
					// X
				}
				else
				{
					if (lngWeekID > 0)
					{
						DeleteDefaultWeek();
					}
				}
				SaveDefaultWeek = boolReturn;
				return SaveDefaultWeek;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveDefaultWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveDefaultWeek;
		}

		public clsScheduleDay GetDay(int intDayNo)
		{
			clsScheduleDay GetDay = null;
			int X;
			clsScheduleDay tWeek;
			tWeek = null;
			GetDay = tWeek;
			for (X = 1; X <= 7; X++)
			{
				if (WorkWeekList[X].DayOfWeek == intDayNo)
				{
					tWeek = WorkWeekList[X];
					break;
				}
			}
			// X
			GetDay = tWeek;
			return GetDay;
		}
		// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
		public clsScheduleDay GetDayByDate(DateTime dtDate)
		{
			clsScheduleDay GetDayByDate = null;
			int X;
			// vbPorter upgrade warning: intDay As int	OnWrite(long, int)
			int intDay;
			DateTime tDate;
			clsScheduleDay tReturn;
			tReturn = null;
			GetDayByDate = null;
			tDate = modSchedule.GetWeekBeginningDate(dtDate);
			intDay = fecherFoundation.DateAndTime.DateDiff("d", tDate, dtDate);
			intDay += 1;
			for (X = 1; X <= 7; X++)
			{
				if (WorkWeekList[X].DayOfWeek == intDay)
				{
					tReturn = WorkWeekList[X];
					break;
				}
			}
			// X
			GetDayByDate = tReturn;
			return GetDayByDate;
		}

		public int ScheduleID
		{
			set
			{
				lngScheduleID = value;
			}
			get
			{
				int ScheduleID = 0;
				ScheduleID = lngScheduleID;
				return ScheduleID;
			}
		}

		public int WeekID
		{
			set
			{
				lngWeekID = value;
			}
			get
			{
				int WeekID = 0;
				WeekID = lngWeekID;
				return WeekID;
			}
		}

		public int OrderNumber
		{
			set
			{
				intOrderNo = value;
			}
			get
			{
				int OrderNumber = 0;
				OrderNumber = intOrderNo;
				return OrderNumber;
			}
		}

		public string WeekName
		{
			set
			{
				strName = value;
			}
			get
			{
				string WeekName = "";
				WeekName = strName;
				return WeekName;
			}
		}

		public bool Unused
		{
			set
			{
				boolNotUsed = value;
			}
			get
			{
				bool Unused = false;
				Unused = boolNotUsed;
				return Unused;
			}
		}

		public clsScheduleWeek() : base()
		{
			int X;
			clsScheduleDay tDay;
			for (X = 0; X <= 7; X++)
			{
				tDay = new clsScheduleDay();
				tDay.DayOfWeek = X;
				tDay.ScheduleID = lngScheduleID;
				tDay.WeekID = lngWeekID;
				WorkWeekList[X] = tDay;
				PayTots = new clsPayTotalsByCat();
			}
			// X
		}

		public void ReCalcWeek()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				DateTime dtTemp;
				int X;
				clsScheduleDay tDay;
				clsShiftTypeList ShiftList = new clsShiftTypeList();
				clsWorkShift tShift;
				clsShiftType tSType;
				clsCalcShifts tCalc = new clsCalcShifts();
				clsHolidaysList tHolidays = new clsHolidaysList();
				bool boolUnworkedSick = false;
				double dblScheduledRegular;
				tHolidays.LoadHolidays();
				tHolidays.MoveFirst();
				PayTots.ClearCats();
				tSType = null;
				ShiftList.LoadTypes();
				dblScheduledRegular = 0;
				switch (intOvertimeSetting)
				{
					case modSchedule.OverTimeOrder.ScheduledFirst:
						{
							for (X = 1; X <= 7; X++)
							{
								PayTots.ClearDayTotals();
								tDay = WorkWeekList[X];
								if (!(tDay == null))
								{
									tDay.MoveFirst();
									while (tDay.GetCurrentIndex() >= 0)
									{
										tShift = tDay.GetCurrentShift();
										if (tShift.ShiftType > 0)
										{
											if (!tShift.UnScheduled)
											{
												if (!(tSType == null))
												{
													if (tSType.ShiftID != tShift.ShiftType)
													{
														tSType = ShiftList.GetShiftTypeByType(tShift.ShiftType);
													}
												}
												else
												{
													tSType = ShiftList.GetShiftTypeByType(tShift.ShiftType);
												}
												if (!(tSType == null))
												{
													if (tSType.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.GiftHoliday))
													{
														boolUnworkedSick = false;
														if (tDay.HasUnworkedOrSick())
														{
															boolUnworkedSick = true;
														}
														else
														{
															if (X > 1)
															{
																if (WorkWeekList[X - 1].HasUnworkedOrSick())
																{
																	boolUnworkedSick = true;
																}
																else if (X < 7)
																{
																	if (WorkWeekList[X + 1].HasUnworkedOrSick())
																	{
																		boolUnworkedSick = true;
																	}
																}
																else
																{
																	if (tCalc.IsUnworkedOrSick(fecherFoundation.DateAndTime.DateAdd("d", 1, tDay.WorkDate), ref strEmployeeNumber))
																	{
																		boolUnworkedSick = true;
																	}
																}
															}
															else
															{
																if (WorkWeekList[X + 1].HasUnworkedOrSick())
																{
																	boolUnworkedSick = true;
																}
																else
																{
																	if (tCalc.IsUnworkedOrSick(fecherFoundation.DateAndTime.DateAdd("d", -1, tDay.WorkDate), ref strEmployeeNumber))
																	{
																		boolUnworkedSick = true;
																	}
																}
															}
														}
														if (boolUnworkedSick)
														{
															tShift.ActualEnd = "Unworked";
															tShift.ActualStart = "Unworked";
														}
													}
													if (!tCalc.CalcShift(tShift, ref tSType, ref ShiftList, ref PayTots, ref dblOvertimeStart, tDay.WorkDate, ref tHolidays, ref dblScheduledRegular))
													{
														return;
													}
												}
											}
										}
										tDay.MoveNext();
									}
								}
							}
							// X
							for (X = 1; X <= 7; X++)
							{
								PayTots.ClearDayTotals();
								tDay = WorkWeekList[X];
								if (!(tDay == null))
								{
									tDay.MoveFirst();
									while (tDay.GetCurrentIndex() >= 0)
									{
										tShift = tDay.GetCurrentShift();
										if (tShift.ShiftType > 0)
										{
											if (tShift.UnScheduled)
											{
												if (!(tSType == null))
												{
													if (tSType.ShiftID != tShift.ShiftType)
													{
														tSType = ShiftList.GetShiftTypeByType(tShift.ShiftType);
													}
												}
												else
												{
													tSType = ShiftList.GetShiftTypeByType(tShift.ShiftType);
												}
												if (!(tSType == null))
												{
													if (tSType.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.GiftHoliday))
													{
														boolUnworkedSick = false;
														if (tDay.HasUnworkedOrSick())
														{
															boolUnworkedSick = true;
														}
														else
														{
															if (X > 1)
															{
																if (WorkWeekList[X - 1].HasUnworkedOrSick())
																{
																	boolUnworkedSick = true;
																}
																else if (X < 7)
																{
																	if (WorkWeekList[X + 1].HasUnworkedOrSick())
																	{
																		boolUnworkedSick = true;
																	}
																}
																else
																{
																	if (tCalc.IsUnworkedOrSick(fecherFoundation.DateAndTime.DateAdd("d", 1, tDay.WorkDate), ref strEmployeeNumber))
																	{
																		boolUnworkedSick = true;
																	}
																}
															}
															else
															{
																if (WorkWeekList[X + 1].HasUnworkedOrSick())
																{
																	boolUnworkedSick = true;
																}
																else
																{
																	if (tCalc.IsUnworkedOrSick(fecherFoundation.DateAndTime.DateAdd("d", -1, tDay.WorkDate), ref strEmployeeNumber))
																	{
																		boolUnworkedSick = true;
																	}
																}
															}
														}
														if (boolUnworkedSick)
														{
															tShift.ActualEnd = "Unworked";
															tShift.ActualStart = "Unworked";
														}
													}
													if (tDay.WorkDate.ToOADate() != 0)
													{
														if (!tCalc.CalcShift(tShift, ref tSType, ref ShiftList, ref PayTots, ref dblOvertimeStart, tDay.WorkDate, ref tHolidays, ref dblScheduledRegular))
														{
															return;
														}
													}
												}
											}
										}
										tDay.MoveNext();
									}
								}
							}
							// X
							break;
						}
					case modSchedule.OverTimeOrder.InOrderWorked:
						{
							for (X = 1; X <= 7; X++)
							{
								tDay = WorkWeekList[X];
								if (!(tDay == null))
								{
									tDay.MoveFirst();
									while (tDay.GetCurrentIndex() >= 0)
									{
										tShift = tDay.GetCurrentShift();
										if (tShift.ShiftType > 0)
										{
											if (!(tSType == null))
											{
												if (tSType.ShiftID != tShift.ShiftType)
												{
													tSType = ShiftList.GetShiftTypeByType(tShift.ShiftType);
												}
											}
											else
											{
												tSType = ShiftList.GetShiftTypeByType(tShift.ShiftType);
											}
											if (!(tSType == null))
											{
												if (tSType.ShiftType == FCConvert.ToInt32(modSchedule.ScheduleShiftType.GiftHoliday))
												{
													boolUnworkedSick = false;
													if (tDay.HasUnworkedOrSick())
													{
														boolUnworkedSick = true;
													}
													else
													{
														if (X > 1)
														{
															if (WorkWeekList[X - 1].HasUnworkedOrSick())
															{
																boolUnworkedSick = true;
															}
															else if (X < 7)
															{
																if (WorkWeekList[X + 1].HasUnworkedOrSick())
																{
																	boolUnworkedSick = true;
																}
															}
															else
															{
																if (tCalc.IsUnworkedOrSick(fecherFoundation.DateAndTime.DateAdd("d", 1, tDay.WorkDate), ref strEmployeeNumber))
																{
																	boolUnworkedSick = true;
																}
															}
														}
														else
														{
															if (WorkWeekList[X + 1].HasUnworkedOrSick())
															{
																boolUnworkedSick = true;
															}
															else
															{
																if (tCalc.IsUnworkedOrSick(fecherFoundation.DateAndTime.DateAdd("d", -1, tDay.WorkDate), ref strEmployeeNumber))
																{
																	boolUnworkedSick = true;
																}
															}
														}
													}
													if (boolUnworkedSick)
													{
														tShift.ActualEnd = "Unworked";
														tShift.ActualStart = "Unworked";
													}
												}
												if (!tCalc.CalcShift(tShift, ref tSType, ref ShiftList, ref PayTots, ref dblOvertimeStart, tDay.WorkDate, ref tHolidays, ref dblScheduledRegular))
												{
													return;
												}
											}
										}
										tDay.MoveNext();
									}
								}
							}
							// X
							break;
						}
				}
				//end switch
				if (dblAlwaysPayHours > 0)
				{
					double dblTemp = 0;
					int lngTemp = 0;
					double dblSick = 0;
					double dblVacation = 0;
					clsPayCatTotal tCat;
					lngTemp = PayTots.RegularIndex;
					// if no regular at all, this is a vacation or sick week and shouldn't force regular hours
					dblSick = 0;
					dblVacation = 0;
					if (PayTots.SickIndex > 0)
					{
						tCat = PayTots.Get_CategoryTotals(PayTots.SickIndex);
						if (!(tCat == null))
						{
							dblSick = tCat.ActualHours;
						}
					}
					if (PayTots.VacationIndex > 0)
					{
						tCat = PayTots.Get_CategoryTotals(PayTots.VacationIndex);
						if (!(tCat == null))
						{
							dblVacation = tCat.ActualHours;
						}
					}
					tCat = PayTots.Get_CategoryTotals(lngTemp);
					if (!(tCat == null))
					{
						if (tCat.ActualHours > 0)
						{
							if (dblScheduledRegular > tCat.ActualHours)
							{
								dblScheduledRegular = tCat.ActualHours;
							}
							dblTemp = dblAlwaysPayHours - (dblScheduledRegular + dblSick + dblVacation);
							if (dblScheduledRegular + dblTemp < 0)
							{
								dblTemp = -dblScheduledRegular;
							}
							tCat = new clsPayCatTotal();
							tCat.AmountPaid += dblTemp;
							tCat.PayCat = PayTots.RegularIndex;
							// tCat.BreakdownAmountPaid("") = tCat.BreakdownAmountPaid("") + dblTemp
							tCat.Set_BreakdownAmountPaid("", dblTemp);
							PayTots.AddToCat(tCat);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReCalcWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public clsPayTotalsByCat GetPayTotals()
		{
			clsPayTotalsByCat GetPayTotals = null;
			GetPayTotals = PayTots;
			return GetPayTotals;
		}
	}
}
