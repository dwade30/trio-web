namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptBankInfoLabels.
	/// </summary>
	partial class rptBankInfoLabels
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBankInfoLabels));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtLabel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLabel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLabel3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLabel1,
				this.txtLabel2,
				this.txtLabel3
			});
			this.Detail.Height = 1.041667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtLabel1
			// 
			this.txtLabel1.Height = 0.9375F;
			this.txtLabel1.Left = 0.1875F;
			this.txtLabel1.Name = "txtLabel1";
			this.txtLabel1.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtLabel1.Text = null;
			this.txtLabel1.Top = 0.0625F;
			this.txtLabel1.Width = 2.5F;
			// 
			// txtLabel2
			// 
			this.txtLabel2.Height = 0.9375F;
			this.txtLabel2.Left = 2.75F;
			this.txtLabel2.Name = "txtLabel2";
			this.txtLabel2.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtLabel2.Text = null;
			this.txtLabel2.Top = 0.0625F;
			this.txtLabel2.Width = 2.5F;
			// 
			// txtLabel3
			// 
			this.txtLabel3.Height = 0.9375F;
			this.txtLabel3.Left = 5.3125F;
			this.txtLabel3.Name = "txtLabel3";
			this.txtLabel3.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtLabel3.Text = null;
			this.txtLabel3.Top = 0.0625F;
			this.txtLabel3.Width = 2.5F;
			// 
			// rptBankInfoLabels
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.4861111F;
			this.PageSettings.Margins.Right = 0.4861111F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtLabel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLabel1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLabel2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLabel3;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}