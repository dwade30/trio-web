﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptICMA.
	/// </summary>
	partial class rptICMA
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptICMA));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFileName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRecordTypeCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRecordTypeDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlanNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFundID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFundID = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSourceCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSource = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPlan = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIRS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTaxYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTaxYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPOFF = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFileName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecordTypeCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecordTypeDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlanNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFundID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSourceCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIRS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPOFF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtRecordTypeCode,
				this.txtRecordTypeDescription,
				this.txtPlanNumber,
				this.Label2,
				this.txtName,
				this.txtSSN,
				this.txtFundID,
				this.lblFundID,
				this.txtSourceCode,
				this.lblSource,
				this.txtAmount,
				this.lblAmount,
				this.lblPlan,
				this.lblIRS,
				this.txtTaxYear,
				this.lblTaxYear,
				this.txtPOFF
			});
			this.Detail.Height = 1.239583F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtTime,
				this.txtPage,
				this.txtFileName
			});
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.625F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0.375F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.270833F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size" + ": 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "ICMA Electronic File(s)";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.958333F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2.270833F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.0625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtFileName
			// 
			this.txtFileName.Height = 0.1875F;
			this.txtFileName.Left = 2.270833F;
			this.txtFileName.Name = "txtFileName";
			this.txtFileName.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtFileName.Text = null;
			this.txtFileName.Top = 0.21875F;
			this.txtFileName.Width = 2.9375F;
			// 
			// txtRecordTypeCode
			// 
			this.txtRecordTypeCode.Height = 0.1666667F;
			this.txtRecordTypeCode.Left = 0.08333334F;
			this.txtRecordTypeCode.Name = "txtRecordTypeCode";
			this.txtRecordTypeCode.Text = null;
			this.txtRecordTypeCode.Top = 0.07291666F;
			this.txtRecordTypeCode.Width = 0.3020833F;
			// 
			// txtRecordTypeDescription
			// 
			this.txtRecordTypeDescription.Height = 0.1666667F;
			this.txtRecordTypeDescription.Left = 0.5833333F;
			this.txtRecordTypeDescription.Name = "txtRecordTypeDescription";
			this.txtRecordTypeDescription.Text = null;
			this.txtRecordTypeDescription.Top = 0.07291666F;
			this.txtRecordTypeDescription.Width = 1.552083F;
			// 
			// txtPlanNumber
			// 
			this.txtPlanNumber.Height = 0.1666667F;
			this.txtPlanNumber.Left = 3.416667F;
			this.txtPlanNumber.Name = "txtPlanNumber";
			this.txtPlanNumber.Text = null;
			this.txtPlanNumber.Top = 0.07291666F;
			this.txtPlanNumber.Width = 1.552083F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.354167F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Plan Number";
			this.Label2.Top = 0.07291666F;
			this.Label2.Width = 0.96875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1979167F;
			this.txtName.Left = 0.5833333F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0.4166667F;
			this.txtName.Width = 3.489583F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1979167F;
			this.txtSSN.Left = 0.5833333F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = "Field1";
			this.txtSSN.Top = 0.6145833F;
			this.txtSSN.Width = 1.989583F;
			// 
			// txtFundID
			// 
			this.txtFundID.Height = 0.1979167F;
			this.txtFundID.Left = 5.458333F;
			this.txtFundID.Name = "txtFundID";
			this.txtFundID.Text = null;
			this.txtFundID.Top = 0.6145833F;
			this.txtFundID.Width = 1.333333F;
			// 
			// lblFundID
			// 
			this.lblFundID.Height = 0.1979167F;
			this.lblFundID.HyperLink = null;
			this.lblFundID.Left = 4.71875F;
			this.lblFundID.Name = "lblFundID";
			this.lblFundID.Style = "font-weight: bold";
			this.lblFundID.Text = "Fund ID";
			this.lblFundID.Top = 0.6145833F;
			this.lblFundID.Width = 0.6666667F;
			// 
			// txtSourceCode
			// 
			this.txtSourceCode.Height = 0.1979167F;
			this.txtSourceCode.Left = 5.458333F;
			this.txtSourceCode.Name = "txtSourceCode";
			this.txtSourceCode.Text = null;
			this.txtSourceCode.Top = 0.8125F;
			this.txtSourceCode.Width = 0.5833333F;
			// 
			// lblSource
			// 
			this.lblSource.Height = 0.1979167F;
			this.lblSource.HyperLink = null;
			this.lblSource.Left = 4.71875F;
			this.lblSource.Name = "lblSource";
			this.lblSource.Style = "font-weight: bold";
			this.lblSource.Text = "Source";
			this.lblSource.Top = 0.8125F;
			this.lblSource.Width = 0.6666667F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1979167F;
			this.txtAmount.Left = 5.458333F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0.4166667F;
			this.txtAmount.Width = 1.333333F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1979167F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 4.71875F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-weight: bold";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.4166667F;
			this.lblAmount.Width = 0.6666667F;
			// 
			// lblPlan
			// 
			this.lblPlan.Height = 0.1979167F;
			this.lblPlan.HyperLink = null;
			this.lblPlan.Left = 0F;
			this.lblPlan.Name = "lblPlan";
			this.lblPlan.Style = "font-weight: bold";
			this.lblPlan.Text = "Plan";
			this.lblPlan.Top = 0.4166667F;
			this.lblPlan.Visible = false;
			this.lblPlan.Width = 0.5833333F;
			// 
			// lblIRS
			// 
			this.lblIRS.Height = 0.1979167F;
			this.lblIRS.HyperLink = null;
			this.lblIRS.Left = 0F;
			this.lblIRS.Name = "lblIRS";
			this.lblIRS.Style = "font-weight: bold";
			this.lblIRS.Text = "IRS #";
			this.lblIRS.Top = 0.6145833F;
			this.lblIRS.Visible = false;
			this.lblIRS.Width = 0.5833333F;
			// 
			// txtTaxYear
			// 
			this.txtTaxYear.Height = 0.1979167F;
			this.txtTaxYear.Left = 5.458333F;
			this.txtTaxYear.Name = "txtTaxYear";
			this.txtTaxYear.Text = null;
			this.txtTaxYear.Top = 1F;
			this.txtTaxYear.Width = 0.5833333F;
			// 
			// lblTaxYear
			// 
			this.lblTaxYear.Height = 0.1979167F;
			this.lblTaxYear.HyperLink = null;
			this.lblTaxYear.Left = 4.71875F;
			this.lblTaxYear.Name = "lblTaxYear";
			this.lblTaxYear.Style = "font-weight: bold";
			this.lblTaxYear.Text = "Tax Year";
			this.lblTaxYear.Top = 1F;
			this.lblTaxYear.Width = 0.75F;
			// 
			// txtPOFF
			// 
			this.txtPOFF.Height = 0.1979167F;
			this.txtPOFF.Left = 6.875F;
			this.txtPOFF.Name = "txtPOFF";
			this.txtPOFF.Text = "POFF";
			this.txtPOFF.Top = 0.4166667F;
			this.txtPOFF.Visible = false;
			this.txtPOFF.Width = 0.5F;
			// 
			// rptICMA
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFileName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecordTypeCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecordTypeDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlanNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFundID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSourceCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIRS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPOFF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRecordTypeCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRecordTypeDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlanNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFundID;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFundID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSourceCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSource;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPlan;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIRS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPOFF;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFileName;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
