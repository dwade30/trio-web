//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmPurgeTerminatedEmployees : BaseForm
	{
		public frmPurgeTerminatedEmployees()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPurgeTerminatedEmployees InstancePtr
		{
			get
			{
				return (frmPurgeTerminatedEmployees)Sys.GetInstance(typeof(frmPurgeTerminatedEmployees));
			}
		}

		protected frmPurgeTerminatedEmployees _InstancePtr = null;
		//=========================================================
		private int ID;
		private int EmployeeNumber;
		private int EmployeeName;
		private int DATETERMINATED;
		private int CHECKBOX;

		private void frmPurgeTerminatedEmployees_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmPurgeTerminatedEmployees_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeTerminatedEmployees properties;
			//frmPurgeTerminatedEmployees.ScaleWidth	= 8145;
			//frmPurgeTerminatedEmployees.ScaleHeight	= 5595;
			//frmPurgeTerminatedEmployees.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				modGlobalFunctions.SetTRIOColors(this);
				SetGridProperties();
				bool boolTerminate;
				clsDRWrapper rsDeductions = new clsDRWrapper();
				clsDRWrapper rsMatch = new clsDRWrapper();
				clsDRWrapper rsPayTotals = new clsDRWrapper();
				clsDRWrapper rsEmployees = new clsDRWrapper();
				// vbPorter upgrade warning: dtStart As DateTime	OnWrite(string, DateTime)
				DateTime dtStart;
				DateTime dtTemp;
				dtStart = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year));
				dtTemp = modCoreysSweeterCode.GetPrecedingFirstFiscalDay(modGlobalVariables.Statics.gdatCurrentPayDate);
				if (fecherFoundation.DateAndTime.DateDiff("d", dtStart, dtTemp) < 0)
				{
					dtStart = dtTemp;
				}
				rsEmployees.OpenRecordset("Select * from tblEmployeeMaster Where Status = 'Terminated' or status = 'Resigned' Order by EmployeeNumber");
				while (!rsEmployees.EndOfFile())
				{
					rsPayTotals.OpenRecordset("Select * from tblcheckdetail where EmployeeNumber = '" + rsEmployees.Get_Fields("EmployeeNumber") + "' and checkvoid = 0 and paydate between '" + FCConvert.ToString(dtStart) + "' and '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' ", "twpy0000.vb1");
					rsPayTotals.MoveFirst();
					if (rsPayTotals.EndOfFile())
					{
						AddTerminatedEmployee(rsEmployees);
					}
					NextEmployee:
					;
					rsEmployees.MoveNext();
					boolTerminate = false;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// SET THE COLUMN NUMBERS
				ID = 0;
				CHECKBOX = 1;
				EmployeeNumber = 2;
				EmployeeName = 3;
				DATETERMINATED = 4;
				vsData.Cols = 5;
				vsData.Rows = 1;
				modGlobalRoutines.CenterGridCaptions(ref vsData);
				vsData.ColWidth(ID, 0);
				vsData.ColAlignment(ID, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.TextMatrix(0, ID, "ID");
				vsData.ColHidden(ID, true);
				vsData.ColWidth(CHECKBOX, 700);
				vsData.ColAlignment(CHECKBOX, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.TextMatrix(0, CHECKBOX, "Select");
				vsData.ColDataType(CHECKBOX, FCGrid.DataTypeSettings.flexDTBoolean);
				vsData.ColWidth(EmployeeNumber, 1600);
				vsData.ColAlignment(EmployeeNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.TextMatrix(0, EmployeeNumber, "Employee Number");
				vsData.ColWidth(EmployeeName, 3600);
				vsData.ColAlignment(EmployeeName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.TextMatrix(0, EmployeeName, "Employee Name");
				vsData.ColWidth(DATETERMINATED, 1700);
				vsData.ColAlignment(DATETERMINATED, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsData.TextMatrix(0, DATETERMINATED, "Date Terminated");
				vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPurgeTerminatedEmployees_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmPurgeTerminatedEmployees_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPurgeTerminatedEmployees_Resize(object sender, System.EventArgs e)
		{
			vsData.ColWidth(CHECKBOX, FCConvert.ToInt32(vsData.WidthOriginal * 0.07));
			vsData.ColWidth(EmployeeNumber, FCConvert.ToInt32(vsData.WidthOriginal * 0.2));
			vsData.ColWidth(EmployeeName, FCConvert.ToInt32(vsData.WidthOriginal * 0.5));
			vsData.ColWidth(DATETERMINATED, FCConvert.ToInt32(vsData.WidthOriginal * 0.2));
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptTerminatedEmployees.InstancePtr, boolAllowEmail: true, strAttachmentName: "Terminated Employees");
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			bool boolDeductionTableCheck;
			bool boolMatchTableCheck;
			bool boolPayTotalsTableCheck;
			bool boolDDTableCheck;
			bool boolMiscUpdateTableCheck;
			bool boolDistributionTableCheck;
			bool boolVacSickTableCheck;
			bool boolCheckDetailTableCheck;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsTable = new clsDRWrapper();
			if (vsData.Rows == 1)
			{
				MessageBox.Show("There are no employees to Purge. Employees must be marked as terminated or resigned and" + "\r" + "have values equal to zero on all fields in the Employee Deduction, Employers Match, " + "\r" + "and Pay Totals screen in order to be purged.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
				return;
			}
			else
			{
				if (MessageBox.Show("All current and history data will be lost for these employees. " + "\r" + "Are you sure you wish to purge the selected employees?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
					{
						if (FCConvert.ToBoolean(FCConvert.ToInt32(vsData.TextMatrix(intCounter, CHECKBOX))) == true)
						{
							// Delete all of the records for this employee
							rsData.Execute("Delete from tblEmployersMatch where EmployeeNumber = '" + vsData.TextMatrix(intCounter, EmployeeNumber) + "'", "Payroll");
							rsData.Execute("Delete from tblEmployeeDeductions where EmployeeNumber = '" + vsData.TextMatrix(intCounter, EmployeeNumber) + "'", "Payroll");
							rsData.Execute("Delete from tblEmployeeMaster where EmployeeNumber = '" + vsData.TextMatrix(intCounter, EmployeeNumber) + "'", "Payroll");
							rsData.Execute("Delete from tblDirectDeposit where EmployeeNumber = '" + vsData.TextMatrix(intCounter, EmployeeNumber) + "'", "Payroll");
							rsData.Execute("Delete from tblMiscUpdate where EmployeeNumber = '" + vsData.TextMatrix(intCounter, EmployeeNumber) + "'", "Payroll");
							rsData.Execute("Delete from tblPayrollDistribution where EmployeeNumber = '" + vsData.TextMatrix(intCounter, EmployeeNumber) + "'", "Payroll");
							rsData.Execute("Delete from tblVacationSick where EmployeeNumber = '" + vsData.TextMatrix(intCounter, EmployeeNumber) + "'", "Payroll");
							rsData.Execute("Delete from tblCheckDetail where EmployeeNumber = '" + vsData.TextMatrix(intCounter, EmployeeNumber) + "'", "Payroll");
						}
					}
					MessageBox.Show("Purge of selected employee information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			Close();
		}

		public void AddTerminatedEmployee(clsDRWrapper rsEmployee)
		{
			vsData.Rows += 1;
			vsData.TextMatrix(vsData.Rows - 1, CHECKBOX, FCConvert.ToString(true));
			vsData.TextMatrix(vsData.Rows - 1, ID, rsEmployee.Get_Fields("EmployeeNumber"));
			vsData.TextMatrix(vsData.Rows - 1, EmployeeNumber, rsEmployee.Get_Fields("EmployeeNumber"));
			vsData.TextMatrix(vsData.Rows - 1, EmployeeName, rsEmployee.Get_Fields_String("FirstName") + " " + rsEmployee.Get_Fields_String("MiddleName") + " " + rsEmployee.Get_Fields_String("LastName"));
            //FC:FINAL:AM:#2669 - format the datetime
            //vsData.TextMatrix(vsData.Rows - 1, DATETERMINATED, rsEmployee.Get_Fields("DateTerminated"));
            vsData.TextMatrix(vsData.Rows - 1, DATETERMINATED, Strings.Format(rsEmployee.Get_Fields("DateTerminated"), "MM/dd/yyyy"));
            vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsData.Rows - 1, 2, vsData.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void vsData_RowColChange(object sender, System.EventArgs e)
		{
			if (vsData.Col == CHECKBOX)
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}
	}
}
