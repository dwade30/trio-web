﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public partial class frmStartupWait : BaseForm
	{
		public frmStartupWait()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmStartupWait InstancePtr
		{
			get
			{
				return (frmStartupWait)Sys.GetInstance(typeof(frmStartupWait));
			}
		}

		protected frmStartupWait _InstancePtr = null;
		//=========================================================
		public void Init(ref string strMessage)
		{
			Label2.Text = strMessage;
			this.Show(FCForm.FormShowEnum.Modeless);
		}

		public void UpdateMessage(ref string strMessage)
		{
			Label2.Text = strMessage;
			Label2.Refresh();
		}

		private void frmStartupWait_Activated(object sender, System.EventArgs e)
		{
			this.BringToFront();
			this.HeightOriginal = FCConvert.ToInt32(FCGlobal.Screen.HeightOriginal / 4);
			this.WidthOriginal = FCConvert.ToInt32(FCGlobal.Screen.WidthOriginal / 4);
		}

		private void frmStartupWait_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmStartupWait properties;
			//frmStartupWait.ScaleWidth	= 4680;
			//frmStartupWait.ScaleHeight	= 3585;
			//frmStartupWait.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
