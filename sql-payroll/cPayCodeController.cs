//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWPY0000
{
	public class cPayCodeController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public Dictionary<object, object> GetPayCodesAsDictionary()
		{
			Dictionary<object, object> GetPayCodesAsDictionary = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Dictionary<object, object> dictPayCodes = new Dictionary<object, object>();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cPayCode pCode;
				rsLoad.OpenRecordset("Select * from tblPayCodes order by PayCode", "Payroll");
				while (!rsLoad.EndOfFile())
				{
					//App.DoEvents();
					pCode = new cPayCode();
					pCode.Description = FCConvert.ToString(rsLoad.Get_Fields("Description"));
					pCode.ID = FCConvert.ToInt32(rsLoad.Get_Fields("ID"));
					pCode.LastUpdate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("LastUpdate"));
					pCode.LastUserID = FCConvert.ToString(rsLoad.Get_Fields("LastUserID"));
					pCode.PayCode = FCConvert.ToString(rsLoad.Get_Fields_String("PayCode"));
					pCode.Rate = Conversion.Val(rsLoad.Get_Fields("Rate"));
					dictPayCodes.Add(FCConvert.ToInt32(pCode.PayCode), pCode);
					rsLoad.MoveNext();
				}
				GetPayCodesAsDictionary = dictPayCodes;
				return GetPayCodesAsDictionary;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
			return GetPayCodesAsDictionary;
		}
	}
}
