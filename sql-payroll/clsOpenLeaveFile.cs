//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public class clsOpenLeaveFile
	{
		//=========================================================
		private FCCollection rList = new FCCollection();
		private int intCurrentIndex;

		private void ClearList()
		{
			if (!(rList == null))
			{
				foreach (clsLeaveRecord tRec in rList)
				{
					rList.Remove(1);
				}
				// tRec
			}
		}

		public int MovePrevious()
		{
			int MovePrevious = 0;
			int intReturn;
			intReturn = -1;
			MovePrevious = -1;
			if (intCurrentIndex == -1)
				return MovePrevious;
			if (!FCUtils.IsEmpty(rList))
			{
				if (intCurrentIndex < 1)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex > 0)
					{
						intCurrentIndex -= 1;
						if (intCurrentIndex < 1)
						{
							intCurrentIndex = -1;
							break;
						}
						else if (rList[intCurrentIndex] == null)
						{
						}
						else if (rList[intCurrentIndex].Unused)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MovePrevious = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MovePrevious = -1;
			}
			return MovePrevious;
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(rList))
			{
				if (intCurrentIndex > rList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= rList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > rList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (rList[intCurrentIndex] == null)
						{
						}
						else if (rList[intCurrentIndex].Unused)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public void MoveFirst()
		{
			if (!(rList == null))
			{
				if (!FCUtils.IsEmpty(rList))
				{
					if (rList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentIndex;
				return GetCurrentIndex;
			}
		}

		public clsLeaveRecord GetCurrentRecord()
		{
			clsLeaveRecord GetCurrentRecord = null;
			clsLeaveRecord tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(rList))
			{
				if (intCurrentIndex > 0)
				{
					if (!(rList[intCurrentIndex] == null))
					{
						if (!rList[intCurrentIndex].Unused)
						{
							tRec = rList[intCurrentIndex];
						}
					}
				}
			}
			GetCurrentRecord = tRec;
			return GetCurrentRecord;
		}

		public clsLeaveRecord GetRecordByIndex(ref int intIndex)
		{
			clsLeaveRecord GetRecordByIndex = null;
			clsLeaveRecord tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(rList) && intIndex > 0)
			{
				if (!(rList[intIndex] == null))
				{
					if (!rList[intIndex].Unused)
					{
						intCurrentIndex = intIndex;
						tRec = rList[intIndex];
					}
				}
			}
			GetRecordByIndex = tRec;
			return GetRecordByIndex;
		}

		public int AddRecord(ref string strName, ref string strID, ref string strSSN, ref string strHomeWG1, ref string strHomeWG2, ref string strHomeWG3, ref double dblHomeRate, ref string strPayClass, ref string strPayDes, ref double dblHours, ref double dblDollars, ref double dblEffRate, ref string strWorkedWG1, ref string strWorkedWG2, ref string strWorkedWG3, ref string strStartDate, ref string strEndDate, ref string strWG1Code, ref string strWG2Code, ref string strWG3Code, ref string strWorkedWG1Code, ref string strWorkedWG2Code, ref string strWorkedWG3Code)
		{
			int AddRecord = 0;
			clsLeaveRecord tRec = new clsLeaveRecord();
			tRec.EmpName = strName;
			tRec.ID = strID;
			tRec.SSN = strSSN;
			tRec.HomeWG1 = strHomeWG1;
			tRec.HomeWG2 = strHomeWG2;
			tRec.HomeWG3 = strHomeWG3;
			tRec.HomeRate = dblHomeRate;
			tRec.PayClass = strPayClass;
			tRec.PayDes = strPayDes;
			tRec.Hours = dblHours;
			tRec.Dollars = dblDollars;
			tRec.EffectiveRate = dblEffRate;
			tRec.WorkedWG1 = strWorkedWG1;
			tRec.WorkedWG2 = strWorkedWG2;
			tRec.WorkedWG3 = strWorkedWG3;
			tRec.StartDate = strStartDate;
			tRec.EndDate = strEndDate;
			tRec.WG1Code = strWG1Code;
			tRec.WG2Code = strWG2Code;
			tRec.WG3Code = strWG3Code;
			tRec.WorkedWG1Code = strWorkedWG1Code;
			tRec.WorkedWG2Code = strWorkedWG2Code;
			tRec.WorkedWG3Code = strWorkedWG3Code;
			AddRecord = InsertRecord(ref tRec);
			return AddRecord;
		}

		public int InsertRecord(ref clsLeaveRecord tRec)
		{
			int InsertRecord = 0;
			int intReturn;
			rList.Add(tRec);
			intCurrentIndex = rList.Count;
			intReturn = rList.Count;
			InsertRecord = intReturn;
			return InsertRecord;
		}

		public bool OpenSimpleFile(string strFile)
		{
			bool OpenSimpleFile = false;
			FCFileSystem fso = new FCFileSystem();
			string strPath;
			string strFileName;
			StreamReader ts = null;
			bool boolFileOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strRecord = "";
				clsLeaveRecord tRec;
				int intCurrPos = 0;
				// vbPorter upgrade warning: intFirstPos As int	OnWriteFCConvert.ToInt32(
				int intFirstPos = 0;
				// vbPorter upgrade warning: intSecondPos As int	OnWriteFCConvert.ToInt32(
				int intSecondPos = 0;
				// vbPorter upgrade warning: intpos As int	OnWriteFCConvert.ToInt32(
				int intpos = 0;
				string[] strDataArray = null;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				OpenSimpleFile = false;
				boolFileOpen = false;
				strPath = Path.GetDirectoryName(strFile);
				strFileName = Path.GetFileName(strFile);
				if (FCFileSystem.FileExists(strFile))
				{
					ts = FCFileSystem.OpenText(strFile);
					boolFileOpen = true;
				}
				else
				{
					MessageBox.Show("Could not find file " + strFile, "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return OpenSimpleFile;
				}
				ClearList();
				ts.ReadLine();
				// skip column headers
				while (!ts.EndOfStream)
				{
					//App.DoEvents();
					strRecord = ts.ReadLine();
					strRecord = Strings.Replace(strRecord, ",", "^", 1, -1, CompareConstants.vbTextCompare);
					intCurrPos = 1;
					// now return to commas if between quotes
					while (intCurrPos < strRecord.Length)
					{
						intFirstPos = Strings.InStr(intCurrPos, strRecord, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbTextCompare);
						if (intFirstPos < 1)
							break;
						intSecondPos = Strings.InStr(intFirstPos + 1, strRecord, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbTextCompare);
						intpos = Strings.InStr(intFirstPos, strRecord, "^", CompareConstants.vbTextCompare);
						while (intpos > 0 && intpos < intSecondPos)
						{
							Strings.MidSet(ref strRecord, intpos, 1, ",");
							intpos = Strings.InStr(intpos, strRecord, "^", CompareConstants.vbTextCompare);
						}
						intCurrPos = intSecondPos + 1;
					}
					// now get rid of the quotes
					strRecord = Strings.Replace(strRecord, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
					// now split it
					strDataArray = Strings.Split(strRecord, "^", -1, CompareConstants.vbTextCompare);
					tRec = new clsLeaveRecord();
					for (x = 0; x <= (Information.UBound(strDataArray, 1)); x++)
					{
						//App.DoEvents();
						switch (x)
						{
							case 0:
								{
									tRec.ID = strDataArray[x];
									if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "penobscot county")
									{
										tRec.ID = Strings.Right("0000" + FCConvert.ToString(Conversion.Val(strDataArray[x])), 4);
									}
									break;
								}
							case 1:
								{
									tRec.EmpName = strDataArray[x];
									break;
								}
							case 2:
								{
									tRec.SSN = strDataArray[x];
									break;
								}
							case 3:
								{
									tRec.PayDes = strDataArray[x];
									break;
								}
							case 4:
								{
									tRec.StartDate = strDataArray[x];
									break;
								}
							case 5:
								{
									tRec.EffectiveRate = Conversion.Val(strDataArray[x]);
									break;
								}
							case 6:
								{
									tRec.Hours = Conversion.Val(strDataArray[x]);
									break;
								}
							case 7:
								{
									tRec.WG1Code = strDataArray[x];
									break;
								}
							case 8:
								{
									tRec.WG2Code = strDataArray[x];
									break;
								}
						}
						//end switch
					}
					// x
					InsertRecord(ref tRec);
				}
				ts.Close();
				boolFileOpen = false;
				OpenSimpleFile = true;
				return OpenSimpleFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolFileOpen)
					ts.Close();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In OpenFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return OpenSimpleFile;
		}

		public bool OpenFile(string strFile)
		{
			bool OpenFile = false;
			FCFileSystem fso = new FCFileSystem();
			string strPath;
			string strFileName;
			StreamReader ts = null;
			bool boolFileOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strRecord = "";
				int intCurrPos = 0;
				// vbPorter upgrade warning: intFirstPos As int	OnWriteFCConvert.ToInt32(
				int intFirstPos = 0;
				// vbPorter upgrade warning: intSecondPos As int	OnWriteFCConvert.ToInt32(
				int intSecondPos = 0;
				string[] strDataArray = null;
				// vbPorter upgrade warning: intpos As int	OnWriteFCConvert.ToInt32(
				int intpos = 0;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				clsLeaveRecord tRec;
				// vbPorter upgrade warning: lngTemp As int	OnWrite(long)
				int lngTemp = 0;
				int lngTemp2;
				bool boolAsked;
				boolAsked = false;
				OpenFile = false;
				boolFileOpen = false;
				strPath = Path.GetDirectoryName(strFile);
				strFileName = Path.GetFileName(strFile);
				if (FCFileSystem.FileExists(strFile))
				{
					ts = FCFileSystem.OpenText(strFile);
					boolFileOpen = true;
				}
				else
				{
					MessageBox.Show("Could not find file " + strFile, "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return OpenFile;
				}
				ClearList();
				ts.ReadLine();
				// skip the column headers
				while (!ts.EndOfStream)
				{
					//App.DoEvents();
					strRecord = ts.ReadLine();
					strRecord = Strings.Replace(strRecord, ",", "^", 1, -1, CompareConstants.vbTextCompare);
					intCurrPos = 1;
					// now return to commas if between quotes
					while (intCurrPos < strRecord.Length)
					{
						intFirstPos = Strings.InStr(intCurrPos, strRecord, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbTextCompare);
						if (intFirstPos < 1)
							break;
						intSecondPos = Strings.InStr(intFirstPos + 1, strRecord, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbTextCompare);
						intpos = Strings.InStr(intFirstPos, strRecord, "^", CompareConstants.vbTextCompare);
						while (intpos > 0 && intpos < intSecondPos)
						{
							Strings.MidSet(ref strRecord, intpos, 1, ",");
							intpos = Strings.InStr(intpos, strRecord, "^", CompareConstants.vbTextCompare);
						}
						intCurrPos = intSecondPos + 1;
					}
					// now get rid of the quotes
					strRecord = Strings.Replace(strRecord, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
					// now split it
					strDataArray = Strings.Split(strRecord, "^", -1, CompareConstants.vbTextCompare);
					tRec = new clsLeaveRecord();
					for (x = 0; x <= (Information.UBound(strDataArray, 1)); x++)
					{
						switch (x)
						{
							case 0:
								{
									tRec.EmpName = strDataArray[x];
									break;
								}
							case 1:
								{
									tRec.ID = strDataArray[x];
									if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "penobscot county")
									{
										tRec.ID = Strings.Right("0000" + FCConvert.ToString(Conversion.Val(strDataArray[x])), 4);
									}
									break;
								}
							case 2:
								{
									tRec.SSN = strDataArray[x];
									break;
								}
							case 3:
								{
									tRec.HomeWG1 = strDataArray[x];
									break;
								}
							case 4:
								{
									tRec.HomeWG2 = strDataArray[x];
									break;
								}
							case 5:
								{
									tRec.HomeWG3 = strDataArray[x];
									break;
								}
							case 6:
								{
									tRec.HomeRate = Conversion.Val(strDataArray[x]);
									break;
								}
							case 7:
								{
									tRec.PayClass = strDataArray[x];
									break;
								}
							case 8:
								{
									tRec.PayDes = strDataArray[x];
									break;
								}
							case 9:
								{
									tRec.Hours = Conversion.Val(strDataArray[x]);
									break;
								}
							case 10:
								{
									tRec.Dollars = Conversion.Val(strDataArray[x]);
									break;
								}
							case 11:
								{
									tRec.EffectiveRate = Conversion.Val(strDataArray[x]);
									break;
								}
							case 12:
								{
									tRec.WorkedWG1 = strDataArray[x];
									break;
								}
							case 13:
								{
									tRec.WorkedWG2 = strDataArray[x];
									break;
								}
							case 14:
								{
									tRec.WorkedWG3 = strDataArray[x];
									break;
								}
							case 15:
								{
									tRec.StartDate = strDataArray[x];
									break;
								}
							case 16:
								{
									tRec.EndDate = strDataArray[x];
									break;
								}
							case 17:
								{
									tRec.WG1Code = strDataArray[x];
									break;
								}
							case 18:
								{
									tRec.WG2Code = strDataArray[x];
									break;
								}
							case 19:
								{
									tRec.WG3Code = strDataArray[x];
									break;
								}
							case 20:
								{
									tRec.WorkedWG1Code = strDataArray[x];
									break;
								}
							case 21:
								{
									tRec.WorkedWG2Code = strDataArray[x];
									break;
								}
							case 22:
								{
									tRec.WorkedWG3Code = strDataArray[x];
									break;
								}
						}
						//end switch
					}
					// x
					if (Information.IsDate(tRec.StartDate))
					{
						lngTemp = FCConvert.ToInt32((fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(tRec.StartDate), modGlobalVariables.Statics.gdatCurrentWeekEndingDate)));
						if (!boolAsked)
						{
							if (lngTemp < 0 || lngTemp > 7)
							{
								boolAsked = true;
								if (MessageBox.Show("Date in file is more than a week before or is after the current week ending date." + "\r\n" + "Do you want to continue?", "Incorrect Date", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
								{
									ts.Close();
									boolFileOpen = false;
									return OpenFile;
								}
								else
								{
									modGlobalFunctions.AddCYAEntry_6("PY", "Continued time card import", "Current Date " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentWeekEndingDate), "Import date " + tRec.StartDate);
								}
							}
						}
					}
					InsertRecord(ref tRec);
				}
				ts.Close();
				boolFileOpen = false;
				OpenFile = true;
				return OpenFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolFileOpen)
					ts.Close();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In OpenFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return OpenFile;
		}

		private bool SaveDistListToDB(ref clsDistList DistList)
		{
			bool SaveDistListToDB = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDistributionLine CurDist;
				rsSave.OpenRecordset("select * from tblpayrolldistribution where employeenumber = '" + DistList.EmployeeNumber + "'", "twpy0000.vb1");
				if (!(DistList == null))
				{
					if (DistList.MoveFirst())
					{
						do
						{
							CurDist = DistList.GetCurrentDist();
							if (rsSave.FindFirstRecord("ID", CurDist.ID))
							{
								rsSave.Edit();
								rsSave.Set_Fields("gross", CurDist.Gross);
								rsSave.Set_Fields("hoursweek", CurDist.HoursWeek);
								rsSave.Update();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("AccountCode", CurDist.AccountCode);
								if (fecherFoundation.Strings.Trim(CurDist.AccountNumber) != "")
								{
									rsSave.Set_Fields("AccountNumber", CurDist.AccountNumber);
								}
								rsSave.Set_Fields("BaseRate", CurDist.BaseRate);
								rsSave.Set_Fields("CD", CurDist.CD);
								rsSave.Set_Fields("contractid", CurDist.ContractID);
								rsSave.Set_Fields("defaulthours", CurDist.DefaultHours);
								rsSave.Set_Fields("distu", CurDist.DistU);
								rsSave.Set_Fields("employeenumber", CurDist.EmployeeNumber);
								rsSave.Set_Fields("factor", CurDist.Factor);
								rsSave.Set_Fields("grantfunded", CurDist.GrantFunded);
								rsSave.Set_Fields("gross", CurDist.Gross);
								rsSave.Set_Fields("hoursweek", CurDist.HoursWeek);
								rsSave.Set_Fields("msrs", CurDist.MSRS);
								rsSave.Set_Fields("msrsid", CurDist.MSRSID);
								rsSave.Set_Fields("numberweeks", CurDist.NumberWeeks);
								rsSave.Set_Fields("CAT", CurDist.PayCategory);
								rsSave.Set_Fields("project", CurDist.Project);
								rsSave.Set_Fields("recordnumber", CurDist.RecordNumber);
								rsSave.Set_Fields("statuscode", CurDist.StatusCode);
								rsSave.Set_Fields("taxcode", CurDist.TaxCode);
								rsSave.Set_Fields("wc", CurDist.WC);
								rsSave.Set_Fields("weekstaxwithheld", CurDist.WeeksTaxWithheld);
								rsSave.Set_Fields("workcomp", CurDist.WorkComp);
								rsSave.Update();
								CurDist.ID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							}
						}
						while (DistList.MoveNext() > 0);
					}
					SaveDistListToDB = true;
				}
				return SaveDistListToDB;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "Trying to save distribution line in SaveDistToDB", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveDistListToDB;
		}

		public bool ProcessSimpleFile()
		{
			bool ProcessSimpleFile = false;
			ProcessSimpleFile = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strCurrentEmployee;
				clsLeaveRecord tRec;
				clsDRWrapper rsSave = new clsDRWrapper();
				// Dim rsDist As New clsDRWrapper
				string strAccount = "";
				string defaultWorkComp = "";
				string defaultMSRS = "";
				string defaultStatus = "";
				string defaultWC = "";
				double dblBaseRate;
				double dblBaseRateToUse = 0;
				string defaultDistU = "";
				int lngLastNum = 0;
				clsTimeClockToPayrollCategory tCat = new clsTimeClockToPayrollCategory();
				int lngCat = 0;
				double dblPaid = 0;
				clsDRWrapper rsPayCat = new clsDRWrapper();
				// vbPorter upgrade warning: lngTemp As int	OnWrite(long)
				int lngTemp = 0;
				bool boolAsked;
				double dblForceHours;
				bool boolForceHours;
				double dblTotalHoursForForce = 0;
				double dblHoursToForce;
				int lngForceID;
				clsPayCats CatList = new clsPayCats();
				clsDistList DistList = new clsDistList();
				clsDistributionLine CurDist;
				clsPayCat tempCat;
				double dblOTLimit = 0;
				double dblAdjust;
				double dblShouldHave = 0;
				boolAsked = false;
				strCurrentEmployee = "";
				MoveFirst();
				MovePrevious();
				frmWait.InstancePtr.Init("Importing Employee ");
				dblForceHours = 42;
				boolForceHours = false;
				dblAdjust = 0;
				while (MoveNext() > 0)
				{
					//App.DoEvents();
					tRec = GetCurrentRecord();
					if (!(tRec == null))
					{
						if (tRec.ID == "0696")
						{
							dblAdjust = dblAdjust;
						}
						if (strCurrentEmployee != tRec.ID)
						{
							// If strCurrentEmployee <> "" Then
							if (strCurrentEmployee != "")
							{
								// must save the distlist
								if (boolForceHours)
								{
									// dblAdjust = (dblForceHours - dblOTLimit) + (dblTotalHoursForForce - dblOTLimit)
									dblShouldHave = dblForceHours + (dblTotalHoursForForce - dblOTLimit);
									dblAdjust = dblShouldHave - dblTotalHoursForForce;
									// adjust reg hours first
									if (dblAdjust != 0)
									{
										DistList.MoveFirst();
										do
										{
											//App.DoEvents();
											CurDist = DistList.GetCurrentDist();
											if (CatList.FindID(CurDist.PayCategory))
											{
												tempCat = CatList.GetCurrentCat();
												if (!tempCat.IsHoliday && !tempCat.IsOT && !tempCat.IsOther && !tempCat.IsSick && !tempCat.IsVacation)
												{
													if (CurDist.HoursWeek > 0)
													{
														if (CurDist.HoursWeek + dblAdjust > 0)
														{
															CurDist.HoursWeek += dblAdjust;
															dblAdjust = 0;
														}
														else
														{
															dblAdjust += CurDist.HoursWeek;
															CurDist.HoursWeek = 0;
														}
													}
												}
											}
										}
										while (DistList.MoveNext() > 0 && dblAdjust != 0);
										// adjust sick hours and vacation hours next
										if (dblAdjust != 0)
										{
											DistList.MoveFirst();
											do
											{
												//App.DoEvents();
												CurDist = DistList.GetCurrentDist();
												if (CatList.FindID(CurDist.PayCategory))
												{
													tempCat = CatList.GetCurrentCat();
													if (!tempCat.IsHoliday && !tempCat.IsOT)
													{
														if (CurDist.HoursWeek + dblAdjust > 0)
														{
															CurDist.HoursWeek += dblAdjust;
															dblAdjust = 0;
														}
														else
														{
															dblAdjust += CurDist.HoursWeek;
															CurDist.HoursWeek = 0;
														}
													}
												}
											}
											while (DistList.MoveNext() > 0 && dblAdjust != 0);
										}
									}
								}
								if (DistList.MoveFirst())
								{
									SaveDistListToDB(ref DistList);
								}
							}
							// If strCurrentEmployee <> "" And boolForceHours And lngForceID > 0 Then
							// If dblTotalHoursForForce <> dblForceHours Then
							// dblHoursToForce = dblForceHours - dblTotalHoursForForce
							// Call rsSave.Execute("update tblpayrolldistribution set hoursweek = hoursweek + " & dblHoursToForce & "  WHERE ID = " & lngForceID, "twpy0000.vb1")
							// Call rsSave.Execute("update tblpayrolldistribution set hoursweek = 0 where hoursweek < 0 and ID = " & lngForceID, "twpy0000.vb1")
							// End If
							// End If
							dblTotalHoursForForce = 0;
							lngForceID = 0;
							boolForceHours = false;
							frmWait.InstancePtr.lblMessage.Text = "Importing Employee " + tRec.ID;
							frmWait.InstancePtr.lblMessage.Refresh();
							//App.DoEvents();
							strCurrentEmployee = tRec.ID;
							rsSave.Execute("update tblpayrolldistribution set hoursweek = 0 where employeenumber = '" + tRec.ID + "'", "twpy0000.vb1");
							// Call rsSave.OpenRecordset("select * from tblpayrolldistribution where employeenumber = '" & tRec.ID & "'", "twpy0000.vb1")
							rsPayCat.OpenRecordset("select * from tblpaycategories order by ID", "twpy0000.vb1");
							DistList.LoadEmployee(tRec.ID);
							lngLastNum = 0;
							rsSave.OpenRecordset("select alwayspayhours, overtimestart from tblemployeemaster inner join scheduleweeks on (tblemployeemaster.scheduleid = scheduleweeks.scheduleid) and (tblemployeemaster.currentscheduleweek = scheduleweeks.orderno) where employeenumber = '" + tRec.ID + "'", "twpy0000.vb1");
							if (!rsSave.EndOfFile())
							{
								if (Conversion.Val(rsSave.Get_Fields("alwayspayhours")) > 0 && Conversion.Val(rsSave.Get_Fields("overtimestart")) > 0)
								{
									boolForceHours = true;
									dblOTLimit = Conversion.Val(rsSave.Get_Fields("overtimestart"));
									dblForceHours = Conversion.Val(rsSave.Get_Fields("alwayspayhours"));
								}
								else
								{
									boolForceHours = false;
									dblOTLimit = 0;
								}
							}
							else
							{
								boolForceHours = false;
								lngForceID = 0;
								dblOTLimit = 0;
							}
							if (DistList.MoveFirst())
							{
								DistList.MoveFirst();
								if (DistList.IsCurrent())
								{
									CurDist = DistList.GetCurrentDist();
									defaultWorkComp = CurDist.WorkComp;
									defaultMSRS = CurDist.MSRS;
									defaultStatus = CurDist.StatusCode;
									defaultWC = CurDist.WC;
									dblBaseRate = CurDist.BaseRate;
									defaultDistU = CurDist.DistU;
									DistList.MoveLast();
									CurDist = DistList.GetCurrentDist();
									lngLastNum = CurDist.RecordNumber;
								}
								else
								{
									lngLastNum = 0;
								}
							}
							// End If
						}
						if (Information.IsDate(tRec.StartDate))
						{
							lngTemp = FCConvert.ToInt32((fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(tRec.StartDate), modGlobalVariables.Statics.gdatCurrentWeekEndingDate)));
							if (!boolAsked)
							{
								if (lngTemp < 0 || lngTemp > 7)
								{
									frmWait.InstancePtr.Unload();
									boolAsked = true;
									if (MessageBox.Show("Date in file is more than a week before or is after the current week ending date." + "\r\n" + "Do you want to continue?", "Incorrect Date", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
									{
										return ProcessSimpleFile;
									}
									else
									{
										modGlobalFunctions.AddCYAEntry_6("PY", "Continued time card import", "Current Date " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentWeekEndingDate), "Import date " + tRec.StartDate);
										frmWait.InstancePtr.Init("Importing Employee " + tRec.ID);
									}
								}
							}
							if (tRec.Hours > 0)
							{
								lngCat = tCat.GetPayrollCategory(tRec.PayDes);
								if (lngCat > 0)
								{
									strAccount = tRec.WG1Code;
									if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName) == "penobscot county")
									{
										strAccount = "E " + fecherFoundation.Strings.Trim(Strings.Left(tRec.WG1Code + "   ", 3));
										if (CatList.FindID(lngCat))
										{
											tempCat = CatList.GetCurrentCat();
											if (!tempCat.IsHoliday && !tempCat.IsOT)
											{
												dblTotalHoursForForce += Conversion.Val(tRec.Hours);
											}
											else if (tempCat.IsOT)
											{
												tRec.EffectiveRate = FCConvert.ToDouble(Strings.Format(tRec.EffectiveRate / 1.5, "0.00"));
											}
										}
										// Select Case LCase(tRec.PayDes)
										// Case "ovt", "hol"
										// Case Else
										// dblTotalHoursForForce = dblTotalHoursForForce + Val(tRec.Hours)
										// End Select
									}
									if (fecherFoundation.Strings.Trim(strAccount) == "")
									{
										// first occurrence
										strAccount = "";
										dblPaid = tRec.Hours;
										// If tRec.EffectiveRate > 0 Then
										dblBaseRateToUse = tRec.EffectiveRate;
										// Else
										// dblBaseRateToUse = dblBaseRate
										// End If
										if (!SaveToDist(ref rsPayCat, ref DistList, dblPaid, ref lngCat, strAccount, strCurrentEmployee, ref defaultWorkComp, ref defaultMSRS, ref defaultStatus, ref defaultWC, ref dblBaseRateToUse, ref defaultDistU, ref lngLastNum))
										{
											MessageBox.Show("Error while updating employee '" + strCurrentEmployee + "'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
											frmWait.InstancePtr.Unload();
											return ProcessSimpleFile;
										}
									}
									else
									{
										// specific account
										dblPaid = tRec.Hours;
										// If tRec.EffectiveRate > 0 Then
										dblBaseRateToUse = tRec.EffectiveRate;
										// Else
										// dblBaseRateToUse = dblBaseRate
										// End If
										if (!SaveToDist(ref rsPayCat, ref DistList, dblPaid, ref lngCat, strAccount, strCurrentEmployee, ref defaultWorkComp, ref defaultMSRS, ref defaultStatus, ref defaultWC, ref dblBaseRateToUse, ref defaultDistU, ref lngLastNum))
										{
											MessageBox.Show("Error while updating employee '" + strCurrentEmployee + "'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
											frmWait.InstancePtr.Unload();
											return ProcessSimpleFile;
										}
									}
								}
							}
						}
						else
						{
							MessageBox.Show("Invalid date for employee " + tRec.ID + "\r\n" + "Cannot continue", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							frmWait.InstancePtr.Unload();
							return ProcessSimpleFile;
						}
					}
				}
				// save last one
				if (strCurrentEmployee != "")
				{
					// must save the distlist
					if (boolForceHours)
					{
						dblAdjust = (dblForceHours - dblOTLimit) + (dblTotalHoursForForce - dblOTLimit);
						// adjust reg hours first
						if (dblAdjust != 0)
						{
							DistList.MoveFirst();
							do
							{
								//App.DoEvents();
								CurDist = DistList.GetCurrentDist();
								if (CatList.FindID(CurDist.PayCategory))
								{
									tempCat = CatList.GetCurrentCat();
									if (!tempCat.IsHoliday && !tempCat.IsOT && !tempCat.IsOther && !tempCat.IsSick && !tempCat.IsVacation)
									{
										if (CurDist.HoursWeek > 0)
										{
											if (CurDist.HoursWeek + dblAdjust > 0)
											{
												CurDist.HoursWeek += dblAdjust;
												dblAdjust = 0;
											}
											else
											{
												dblAdjust += CurDist.HoursWeek;
												CurDist.HoursWeek = 0;
											}
										}
									}
								}
							}
							while (DistList.MoveNext() > 0 && dblAdjust != 0);
							// adjust sick hours and vacation hours next
							if (dblAdjust != 0)
							{
								DistList.MoveFirst();
								do
								{
									//App.DoEvents();
									CurDist = DistList.GetCurrentDist();
									if (CatList.FindID(CurDist.PayCategory))
									{
										tempCat = CatList.GetCurrentCat();
										if (!tempCat.IsHoliday && !tempCat.IsOT)
										{
											if (CurDist.HoursWeek + dblAdjust > 0)
											{
												CurDist.HoursWeek += dblAdjust;
												dblAdjust = 0;
											}
											else
											{
												dblAdjust += CurDist.HoursWeek;
												CurDist.HoursWeek = 0;
											}
										}
									}
								}
								while (DistList.MoveNext() > 0 && dblAdjust != 0);
							}
						}
					}
					if (DistList.MoveFirst())
					{
						SaveDistListToDB(ref DistList);
					}
				}
				frmWait.InstancePtr.Unload();
				ProcessSimpleFile = true;
				return ProcessSimpleFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ProcessSimpleFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ProcessSimpleFile;
		}

		public bool ProcessLeaveFile()
		{
			bool ProcessLeaveFile = false;
			ProcessLeaveFile = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsScheduleWeek tSchedWeeks = new clsScheduleWeek();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL = "";
				string strCurrentEmployee;
				clsLeaveRecord tRec = new clsLeaveRecord();
				clsScheduleDay tDay;
				clsTimeClockToPayrollShift tConv = new clsTimeClockToPayrollShift();
				clsShiftTypeList tShiftTypes = new clsShiftTypeList();
				int lngShiftType = 0;
				clsShiftType tSType;
				clsWorkShift tWork;
				// vbPorter upgrade warning: dblTemp As double	OnWrite(long, string, double)
				double dblTemp = 0;
				// vbPorter upgrade warning: dtStartTemp As DateTime	OnWrite(string)
				DateTime dtStartTemp;
				// vbPorter upgrade warning: dtEndTemp As DateTime	OnWrite(string, DateTime)
				DateTime dtEndTemp;
				double dblHours = 0;
				clsDRWrapper rsPayCat = new clsDRWrapper();
				rsPayCat.OpenRecordset("select * from tblpaycategories order by ID", "twpy0000.vb1");
				// strSQL = "Select employeenumber,ssn,scheduleid from tblemployeemaster order by employeenumber"
				// Call rsLoad.OpenRecordset(strSQL, "twpy0000.vb1")
				tShiftTypes.LoadTypes();
				strCurrentEmployee = "";
				MoveFirst();
				MovePrevious();
				frmWait.InstancePtr.Init("Importing Employee");
				while (MoveNext() > 0)
				{
					//App.DoEvents();
					tRec = GetCurrentRecord();
					if (!(tRec == null))
					{
						if (strCurrentEmployee != tRec.ID)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Employee " + tRec.ID;
							frmWait.InstancePtr.lblMessage.Refresh();
							//App.DoEvents();
							if (strCurrentEmployee != "")
							{
								tSchedWeeks.ReCalcWeek();
								tSchedWeeks.SaveEmployeeWeek();
								if (!SaveEmployeeTotals(strCurrentEmployee, ref rsPayCat, ref tSchedWeeks))
								{
									frmWait.InstancePtr.Unload();
									return ProcessLeaveFile;
								}
							}
							tSchedWeeks.EmployeeNumber = tRec.ID;
							if (Information.IsDate(tRec.StartDate))
							{
								tSchedWeeks.LoadEmployeeWeek(fecherFoundation.DateAndTime.DateValue(tRec.StartDate));
								tSchedWeeks.LoadSettings();
								tSchedWeeks.InitPayTots();
								tSchedWeeks.MarkWeekAsUnworked();
								tSchedWeeks.ReCalcWeek();
							}
							else
							{
								frmWait.InstancePtr.Unload();
								MessageBox.Show("Invalid date for employee " + tRec.ID + "\r\n" + "Cannot continue", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return ProcessLeaveFile;
							}
							strCurrentEmployee = tRec.ID;
						}
						if (Information.IsDate(tRec.StartDate))
						{
							tDay = tSchedWeeks.GetDayByDate(Convert.ToDateTime(tRec.StartDate));
							if (!(tDay == null))
							{
								lngShiftType = tConv.GetPayrollShift(tRec.WG2Code);
								if (lngShiftType > 0)
								{
									tSType = tShiftTypes.GetShiftTypeByType(lngShiftType);
									if (!(tSType == null))
									{
										tWork = tDay.FindShift(lngShiftType);
										if (!(tWork == null))
										{
											if (Information.IsDate(tWork.ActualStart) && Information.IsDate(tWork.ActualEnd))
											{
												dblTemp = fecherFoundation.DateAndTime.DateDiff("n", FCConvert.ToDateTime(tWork.ActualStart), FCConvert.ToDateTime(tWork.ActualEnd));
												dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 60, "0.00"));
												if (!(dblTemp < 0))
												{
													dblTemp -= tWork.LunchTime;
													dblHours = dblTemp;
													dtStartTemp = FCConvert.ToDateTime(Strings.Format(tRec.StartDate, "MM/dd/yyyy") + " " + tWork.ActualStart);
													dtEndTemp = FCConvert.ToDateTime(Strings.Format(tRec.EndDate, "MM/dd/yyyy") + " " + tWork.ActualEnd);
												}
												else
												{
													// two day span
													// dtEndTemp = Format(DateAdd("d", 1, tRec.StartDate), "MM/dd/yyyy") & " 12:00 AM"
													dtStartTemp = FCConvert.ToDateTime(Strings.Format(tRec.StartDate, "MM/dd/yyyy") + " " + tWork.ActualStart);
													// dblHours = Format(DateDiff("n", dtStartTemp, dtEndTemp) / 60, "0.00")
													// dtStartTemp = dtEndTemp
													dtEndTemp = FCConvert.ToDateTime(Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", 1, FCConvert.ToDateTime(tRec.StartDate)), "MM/dd/yyyy") + " " + tWork.ActualEnd);
													dblTemp = FCConvert.ToDouble(Strings.Format(fecherFoundation.DateAndTime.DateDiff("n", dtStartTemp, dtEndTemp) / 60, "0.00"));
													dblHours = dblTemp;
													dblHours -= tWork.LunchTime;
												}
												if (dblHours != tRec.Hours)
												{
													if (dblHours < tRec.Hours)
													{
														dtEndTemp = fecherFoundation.DateAndTime.DateAdd("n", (tRec.Hours - dblHours) * 60, dtEndTemp);
													}
													else
													{
														dtEndTemp = fecherFoundation.DateAndTime.DateAdd("n", (tRec.Hours - dblHours) * 60, dtEndTemp);
													}
													tWork.ActualEnd = Strings.Format(dtEndTemp, "hh:mm tt");
												}
												else
												{
													// no change, leave alone
												}
											}
											else
											{
												// must add times
												if (Information.IsDate(tWork.ScheduledStart))
												{
													tWork.ActualStart = tWork.ScheduledStart;
													dtEndTemp = fecherFoundation.DateAndTime.DateAdd("n", (tRec.Hours + tWork.LunchTime) * 60, FCConvert.ToDateTime(Strings.Format(tRec.StartDate, "MM/dd/yyyy") + " " + tWork.ActualStart));
													tWork.ActualEnd = Strings.Format(dtEndTemp, "hh:mm tt");
												}
												else
												{
													// just use default of 8 am since the time is unknown
													tWork.ActualStart = "8:00 AM";
													tWork.ActualEnd = Strings.Format(fecherFoundation.DateAndTime.DateAdd("n", (tRec.Hours + tWork.LunchTime) * 60, FCConvert.ToDateTime(Strings.Format(tRec.StartDate, "MM/dd/yyyy") + " 8:00 AM")), "hh:mm tt");
												}
											}
										}
										else
										{
											// must add new shift
											tDay.AddShift(lngShiftType, "Unscheduled", "Unscheduled", "8:00 AM", Strings.Format(fecherFoundation.DateAndTime.DateAdd("n", (tRec.Hours) * 60, FCConvert.ToDateTime(Strings.Format(tRec.StartDate, "MM/dd/yyyy") + " 8:00 AM")), "hh:mm tt"), 0, 0, "");
										}
									}
								}
							}
							else
							{
								MessageBox.Show("Date is not within pay week for employee " + tRec.ID + "\r\n" + "Cannot continue", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								frmWait.InstancePtr.Unload();
								return ProcessLeaveFile;
							}
						}
						else
						{
							MessageBox.Show("Invalid date for employee " + tRec.ID + "\r\n" + "Cannot continue", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							frmWait.InstancePtr.Unload();
							return ProcessLeaveFile;
						}
					}
				}
				if (!(tRec == null))
				{
					tSchedWeeks.ReCalcWeek();
					tSchedWeeks.SaveEmployeeWeek();
					if (!SaveEmployeeTotals(strCurrentEmployee, ref rsPayCat, ref tSchedWeeks))
					{
						frmWait.InstancePtr.Unload();
						return ProcessLeaveFile;
					}
				}
				frmWait.InstancePtr.Unload();
				ProcessLeaveFile = true;
				return ProcessLeaveFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ProcessLeaveFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ProcessLeaveFile;
		}

		private bool SaveEmployeeTotals(string strCurrentEmployee, ref clsDRWrapper rsPayCat, ref clsScheduleWeek wkWorkWeek)
		{
			bool SaveEmployeeTotals = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveEmployeeTotals = false;
				clsPayTotalsByCat PayTots;
				PayTots = wkWorkWeek.GetPayTotals();
				clsPayCatTotal tCat;
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngLastNum;
				int lngCat = 0;
				string defaultWorkComp = "";
				string defaultMSRS = "";
				string defaultStatus = "";
				string defaultWC = "";
				double dblBaseRate = 0;
				string defaultDistU = "";
				string strAccount = "";
				clsPayCatTotal tBreak;
				int x;
				double dblPaid = 0;
				double dblActualHours;
				string strType = "";
				// loop through employees
				// strCurrentEmployee = ""
				// MoveFirst
				// MovePrevious
				rsSave.Execute("update tblpayrolldistribution set hoursweek = 0 where employeenumber = '" + strCurrentEmployee + "'", "twpy0000.vb1");
				rsSave.OpenRecordset("Select * from tblpayrolldistribution where employeenumber = '" + strCurrentEmployee + "' order by recordnumber", "twpy0000.vb1");
				lngLastNum = 0;
				if (!rsSave.EndOfFile())
				{
					defaultWorkComp = FCConvert.ToString(rsSave.Get_Fields("workcomp"));
					defaultMSRS = FCConvert.ToString(rsSave.Get_Fields("msrs"));
					defaultStatus = FCConvert.ToString(rsSave.Get_Fields("statuscode"));
					defaultWC = FCConvert.ToString(rsSave.Get_Fields("wc"));
					dblBaseRate = Conversion.Val(rsSave.Get_Fields("baserate"));
					defaultDistU = FCConvert.ToString(rsSave.Get_Fields("distu"));
					rsSave.MoveLast();
					lngLastNum = FCConvert.ToInt32(rsSave.Get_Fields("recordnumber"));
				}
				while (!rsPayCat.EndOfFile())
				{
					//App.DoEvents();
					tCat = PayTots.Get_CategoryTotals(rsPayCat.Get_Fields("ID"));
					if (!(tCat == null))
					{
						if (tCat.ActualHours > 0 || tCat.AmountPaid > 0)
						{
							if (tCat.BreakDownCount < 1)
							{
								lngCat = tCat.PayCat;
								strAccount = "";
								dblPaid = tCat.AmountPaid;
								dblActualHours = tCat.ActualHours;
								strType = rsPayCat.Get_Fields("type");
								//if (!SaveToDist(ref rsPayCat, ref rsSave, dblPaid, ref lngCat, strAccount, strCurrentEmployee, ref defaultWorkComp, ref defaultMSRS, ref defaultStatus, ref defaultWC, ref dblBaseRate, ref defaultDistU, ref lngLastNum))
								{
									MessageBox.Show("Error while updating employee '" + strCurrentEmployee + "'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return SaveEmployeeTotals;
								}
							}
							else
							{
								strType = rsPayCat.Get_Fields("type");
								for (x = 1; x <= tCat.BreakDownCount; x++)
								{
									//App.DoEvents();
									tBreak = tCat.Get_BreakdownByIndex(x);
									if (!(tBreak == null))
									{
										lngCat = tCat.PayCat;
										// .TextMatrix(lngRow, CNSTGRIDTOTALSCOLDESC) = .Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDTOTALSCOLCAT) & " " & tBreak.Account
										strAccount = tBreak.Account;
										dblPaid = tBreak.AmountPaid;
										dblActualHours = tBreak.ActualHours;
										//if (!SaveToDist(ref rsPayCat, ref rsSave, dblPaid, ref lngCat, strAccount, strCurrentEmployee, ref defaultWorkComp, ref defaultMSRS, ref defaultStatus, ref defaultWC, ref dblBaseRate, ref defaultDistU, ref lngLastNum))
										{
											MessageBox.Show("Error while updating employee '" + strCurrentEmployee + "'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
											return SaveEmployeeTotals;
										}
									}
								}
								// x
							}
						}
					}
					rsPayCat.MoveNext();
				}
				SaveEmployeeTotals = true;
				return SaveEmployeeTotals;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveEmployeeTotals", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveEmployeeTotals;
		}
		// vbPorter upgrade warning: DistList As clsDistList	OnWrite(clsDistList, clsDRWrapper)
		private bool SaveToDist(ref clsDRWrapper rsPayCat, ref clsDistList DistList, double dblPaid, ref int lngCat, string strAccount, string strEmployeeNumber, ref string defaultWorkComp, ref string defaultMSRS, ref string defaultStatus, ref string defaultWC, ref double dblBaseRate, ref string defaultDistU, ref int lngLastNum)
		{
			bool SaveToDist = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveToDist = false;
				bool boolFound;
				bool boolSoFar = false;
				string dAccount = "";
				clsDistributionLine CurDist = new clsDistributionLine();
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				boolFound = false;
				if (DistList.FindFirstCat(lngCat))
				{
					if (fecherFoundation.Strings.Trim(strAccount) == "")
					{
						boolFound = true;
					}
					else
					{
						// dAccount = Trim(rsSave.Fields("accountnumber"))
						dAccount = fecherFoundation.Strings.Trim(DistList.GetCurrentDist().AccountNumber);
						CurDist = DistList.GetCurrentDist();
						if (dAccount.Length > 0)
						{
							boolSoFar = true;
							if (fecherFoundation.Strings.UCase(Strings.Left(strAccount, 1)) == fecherFoundation.Strings.UCase(Strings.Left(dAccount, 1)))
							{
								for (x = 3; x <= (strAccount.Length); x++)
								{
									if (dAccount.Length >= x)
									{
										if (fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != fecherFoundation.Strings.UCase(Strings.Mid(dAccount, x, 1)) && fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != "X")
										{
											boolSoFar = false;
										}
									}
									else
									{
										boolSoFar = false;
									}
								}
								// x
								if (boolSoFar)
								{
									boolFound = true;
								}
								else
								{
									while (DistList.FindNextCat(lngCat) && !boolFound)
									{
										CurDist = DistList.GetCurrentDist();
										dAccount = fecherFoundation.Strings.Trim(CurDist.AccountNumber);
										if (dAccount.Length > 0)
										{
											boolSoFar = true;
											if (fecherFoundation.Strings.UCase(Strings.Left(strAccount, 1)) == fecherFoundation.Strings.UCase(Strings.Left(dAccount, 1)))
											{
												for (x = 3; x <= (strAccount.Length); x++)
												{
													if (dAccount.Length >= x)
													{
														if (fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != fecherFoundation.Strings.UCase(Strings.Mid(dAccount, x, 1)) && fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != "X")
														{
															boolSoFar = false;
														}
													}
													else
													{
														boolSoFar = false;
													}
												}
												// x
												if (boolSoFar)
												{
													boolFound = true;
													break;
												}
											}
										}
									}
								}
							}
						}
					}
				}
				if (boolFound)
				{
					CurDist.HoursWeek += dblPaid;
					CurDist.Gross = FCConvert.ToDouble(Strings.Format(CurDist.HoursWeek * Conversion.Val(CurDist.Factor) * Conversion.Val(CurDist.BaseRate), "0.00"));
				}
				else
				{
					// must make new one
					lngLastNum += 1;
					CurDist = DistList.AddNew(lngLastNum);
					CurDist.PayCategory = lngCat;
					// If rsPayCat.FindFirstRecord("ID", lngCat) Then
					CurDist.Factor = Conversion.Val(rsPayCat.Get_Fields("multi"));
					CurDist.TaxCode = rsPayCat.Get_Fields("taxstatus");
					CurDist.WC = FCConvert.ToString(rsPayCat.Get_Fields_Boolean("workerscomp"));
					CurDist.AccountCode = 2;
					CurDist.CD = 1;
					CurDist.NumberWeeks = 0;
					CurDist.DefaultHours = 0;
					CurDist.StatusCode = "";
					CurDist.GrantFunded = FCConvert.ToString(0);
					CurDist.MSRSID = -3;
					CurDist.Project = 0;
					CurDist.WeeksTaxWithheld = 0;
					CurDist.ContractID = 0;
					CurDist.WorkComp = defaultWorkComp;
					CurDist.MSRS = defaultMSRS;
					CurDist.StatusCode = defaultStatus;
					CurDist.WC = defaultWC;
					CurDist.BaseRate = dblBaseRate;
					CurDist.DistU = defaultDistU;
					CurDist.HoursWeek = dblPaid;
					CurDist.Gross = FCConvert.ToDouble(Strings.Format(dblPaid * Conversion.Val(CurDist.Factor) * Conversion.Val(CurDist.BaseRate), "0.00"));
					if (fecherFoundation.Strings.UCase(rsPayCat.Get_Fields("type")) == "DOLLARS")
					{
						CurDist.BaseRate = 1;
						CurDist.Gross = dblPaid;
					}
					else if (fecherFoundation.Strings.UCase(rsPayCat.Get_Fields("type")) == "NON-PAY (HOURS)")
					{
						CurDist.BaseRate = 0;
						CurDist.Gross = 0;
					}
					frmWait.InstancePtr.Hide();
					MessageBox.Show("Created new distribution line for employee " + strEmployeeNumber + " pay category " + rsPayCat.Get_Fields_String("description") + "\r\n" + "Check to make sure all entries for this line are correct", "New Line", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// End If
					frmWait.InstancePtr.Show();
					// rsSave.Update
				}
				SaveToDist = true;
				return SaveToDist;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveToDist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveToDist;
		}
	}
}
