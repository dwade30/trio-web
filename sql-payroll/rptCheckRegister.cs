//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCheckRegister.
	/// </summary>
	public partial class rptCheckRegister : BaseSectionReport
	{
		public rptCheckRegister()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Check Register Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCheckRegister InstancePtr
		{
			get
			{
				return (rptCheckRegister)Sys.GetInstance(typeof(rptCheckRegister));
			}
		}

		protected rptCheckRegister _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsCheckInfo?.Dispose();
				rsTACheckInfo?.Dispose();
				rsDDCheckInfo?.Dispose();
				rsVoidedCheckInfo?.Dispose();
                rsTACheckInfo = null;
                rsCheckInfo = null;
                rsDDCheckInfo = null;
                rsVoidedCheckInfo = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCheckRegister	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		clsDRWrapper rsTACheckInfo = new clsDRWrapper();
		clsDRWrapper rsDDCheckInfo = new clsDRWrapper();
		clsDRWrapper rsVoidedCheckInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curTotalCheckAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTotalCheckAmount;
		// vbPorter upgrade warning: curTotalDDAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTotalDDAmount;
		// vbPorter upgrade warning: curTotalEmployee As Decimal	OnWrite(int, Decimal)
		Decimal curTotalEmployee;
		// vbPorter upgrade warning: curTotalTA As Decimal	OnWrite(int, Decimal)
		Decimal curTotalTA;
		// vbPorter upgrade warning: curTotalDD As Decimal	OnWrite(int, Decimal)
		Decimal curTotalDD;
		int intTotalEmployee;
		int intTotalVoid;
		int intTotalDD;
		int intTotalTA;
		int intWarrant;
		int intBank;
		string strCheckType = "";
		string strTypeHolder;
		bool blnACH;
		DateTime datTestDate;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("CheckType");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string intVendor = "";
			Decimal curAmount;
			string strCheckNumber = "";
			// CHECK TO SEE IF THIS IS THE FIRST RECORD
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				CheckRecord:
				;
				if (strCheckType == "C")
				{
					if (rsCheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						strCheckType = "D";
						goto CheckRecord;
					}
				}
				else if (strCheckType == "D")
				{
					if (rsDDCheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						strCheckType = "T";
						goto CheckRecord;
					}
				}
				else if (strCheckType == "T")
				{
					if (rsTACheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						strCheckType = "V";
						goto CheckRecord;
					}
				}
				else
				{
					if (rsVoidedCheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
				//FC:FINAL:DDU:#i2423 - fixed accesibility to CheckRecord Label as in original
				if (strCheckType == "C")
				{
					rsCheckInfo.MoveNext();
					if (rsCheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						strCheckType = "D";
						eArgs.EOF = CheckRecord();
					}
				}
				else if (strCheckType == "D")
				{
					rsDDCheckInfo.MoveNext();
					if (rsDDCheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						strCheckType = "T";
						eArgs.EOF = CheckRecord();
					}
				}
				else if (strCheckType == "T")
				{
					rsTACheckInfo.MoveNext();
					if (rsTACheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						strCheckType = "V";
						eArgs.EOF = CheckRecord();
					}
				}
				else
				{
					rsVoidedCheckInfo.MoveNext();
					if (rsVoidedCheckInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["CheckType"].Value = strCheckType;
			}
		}

		private bool CheckRecord()
		{
		CheckRecord:
			;
			if (strCheckType == "C")
			{
				if (rsCheckInfo.EndOfFile() != true)
				{
					return false;
				}
				else
				{
					strCheckType = "D";
					goto CheckRecord;
				}
			}
			else if (strCheckType == "D")
			{
				if (rsDDCheckInfo.EndOfFile() != true)
				{
					return false;
				}
				else
				{
					strCheckType = "T";
					goto CheckRecord;
				}
			}
			else if (strCheckType == "T")
			{
				if (rsTACheckInfo.EndOfFile() != true)
				{
					return false;
				}
				else
				{
					strCheckType = "V";
					goto CheckRecord;
				}
			}
			else
			{
				if (rsVoidedCheckInfo.EndOfFile() != true)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// NEED TO CHECK AND SEE IF THERE IS BUDGETARY
			if (modGlobalVariables.Statics.gboolBudgetary)
			{
				//if (Information.IsDate(datTestDate) && fecherFoundation.Strings.Trim(datTestDate.ToString()) != "12:00:00 AM")
				if (Information.IsDate(datTestDate) && fecherFoundation.Strings.Trim(datTestDate.ToString()) != Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
				{
                    using (clsDRWrapper rsTest = new clsDRWrapper())
                    {
                        rsTest.OpenRecordset(
                            "SELECT * FROM CheckRecMaster WHERE CheckDate = '" + FCConvert.ToString(datTestDate) +
                            "' AND Type = '2' AND Status = '1'", "TWBD0000.vb1");
                        if (rsTest.EndOfFile() != true && rsTest.BeginningOfFile() != true)
                        {
                            // do nothing
                        }
                        else
                        {
                            MessageBox.Show(
                                "There might have been a problem bringing your payroll information over into the Budgetary Check Rec table.  Please call TRIO Software.",
                                "Error Occurred", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
            using (clsDRWrapper rsACHInfo = new clsDRWrapper())
            {
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                PageCounter = 0;
                Label2.Text = modGlobalConstants.Statics.MuniName;
                Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
                strCheckType = "C";
                strTypeHolder = "C";
                if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
                {
                    rsCheckInfo.OpenRecordset(
                        "SELECT * FROM tblCheckDetail WHERE checkvoid = 0 AND TotalRecord = 1 AND PayDate = '" +
                        frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") +
                        "' ORDER BY CheckNumber");
                    rsTACheckInfo.OpenRecordset(
                        "SELECT SUM(TrustAmount) as TotalTAAmount, CheckNumber, PayDate, TrustRecipientID FROM tblCheckDetail WHERE CheckVoid = 0 AND TrustRecord = 1 AND PayDate = '" +
                        frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") +
                        "' GROUP BY CheckNumber, TrustRecipientID, PayDate ORDER BY CheckNumber");
                }
                else
                {
                    rsCheckInfo.OpenRecordset(
                        "SELECT * FROM tblCheckDetail WHERE checkvoid = 0 AND TotalRecord = 1 AND PayDate = '" +
                        frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") +
                        "' AND PayRunID = " + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayRunID") +
                        " ORDER BY CheckNumber");
                    rsTACheckInfo.OpenRecordset(
                        "SELECT SUM(TrustAmount) as TotalTAAmount, CheckNumber, PayDate, TrustRecipientID FROM tblCheckDetail WHERE CheckVoid = 0 AND TrustRecord = 1 AND PayDate = '" +
                        frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") +
                        "' AND PayRunID = " + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayRunID") +
                        " GROUP BY CheckNumber, TrustRecipientID, PayDate ORDER BY CheckNumber");
                }

                rsACHInfo.OpenRecordset("SELECT * FROM tblDefaultInformation");
                if (FCConvert.ToBoolean(rsACHInfo.Get_Fields_Boolean("ACHClearingHouse")))
                {
                    blnACH = true;
                    if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
                    {
                        rsDDCheckInfo.OpenRecordset(
                            "SELECT SUM(DDAmount) as TotalDDAmount, CheckNumber, PayDate FROM tblCheckDetail WHERE checkvoid = 0 AND BankRecord = 1 AND PayDate = '" +
                            frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") +
                            "' GROUP BY CheckNumber, PayDate ORDER BY CheckNumber");
                    }
                    else
                    {
                        rsDDCheckInfo.OpenRecordset(
                            "SELECT SUM(DDAmount) as TotalDDAmount, CheckNumber, PayDate FROM tblCheckDetail WHERE checkvoid = 0 AND BankRecord = 1 AND PayDate = '" +
                            frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") +
                            "' AND PayRunID = " +
                            frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayRunID") +
                            " GROUP BY CheckNumber, PayDate ORDER BY CheckNumber");
                    }
                }
                else
                {
                    blnACH = false;
                    if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
                    {
                        rsDDCheckInfo.OpenRecordset(
                            "SELECT SUM(DDAmount) as TotalDDAmount, CheckNumber, PayDate, DDBankNumber FROM tblCheckDetail WHERE checkvoid = 0 AND BankRecord = 1 AND PayDate = '" +
                            frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") +
                            "' GROUP BY CheckNumber, DDBankNumber, PayDate ORDER BY CheckNumber");
                    }
                    else
                    {
                        rsDDCheckInfo.OpenRecordset(
                            "SELECT SUM(DDAmount) as TotalDDAmount, CheckNumber, PayDate, DDBankNumber FROM tblCheckDetail WHERE checkvoid = 0 AND BankRecord = 1 AND PayDate = '" +
                            frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") +
                            "' AND PayRunID = " +
                            frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayRunID") +
                            " GROUP BY CheckNumber, DDBankNumber, PayDate ORDER BY CheckNumber");
                    }
                }

                if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
                {
                    rsVoidedCheckInfo.OpenRecordset("SELECT * FROM tblCheckHistory WHERE CheckDate = '" +
                                                    frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime(
                                                        "PayDate") + "' ORDER BY CheckNumber");
                }
                else
                {
                    rsVoidedCheckInfo.OpenRecordset("SELECT * FROM tblCheckHistory WHERE CheckDate = '" +
                                                    frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime(
                                                        "PayDate") + "' AND CheckRun = " +
                                                    frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields(
                                                        "PayRunID") + " ORDER BY CheckNumber");
                }

                if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
                {
                    lblPayDate.Text = "Pay Date: " +
                                      Strings.Format(
                                          frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayDate"),
                                          "MM/dd/yyyy") + "  (ALL Pay Runs)";
                }
                else
                {
                    lblPayDate.Text = "Pay Date: " +
                                      Strings.Format(
                                          frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayDate"),
                                          "MM/dd/yyyy");
                }

                //Application.DoEvents();
                if (frmCheckRegister.InstancePtr.cmbReprint.Text == "Reprint Register")
                {
                    lblReprint.Visible = true;
                }
                else
                {
                    lblReprint.Visible = false;
                }

                curTotalCheckAmount = 0;
                curTotalDDAmount = 0;
                curTotalEmployee = 0;
                curTotalTA = 0;
                curTotalDD = 0;
                intTotalEmployee = 0;
                intTotalVoid = 0;
                intTotalDD = 0;
                intTotalTA = 0;
                blnFirstRecord = true;
                if (Conversion.Val(modBudgetaryAccounting.GetBankVariable("PayrollBank")) != 0)
                {
                    intBank = FCConvert.ToInt32(modBudgetaryAccounting.GetBankVariable("PayrollBank"));
                }
                else
                {
                    intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(
                        fecherFoundation.Strings.Trim(Strings.Left(frmCheckRegister.InstancePtr.cboBanks.Text, 2)))));
                }

                if (intBank == 0)
                    intBank = 1;
            }
        }

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
            using (clsDRWrapper rsTest = new clsDRWrapper())
            {
                if (frmCheckRegister.InstancePtr.cmbReprint.Text == "Initial Run")
                {
                    if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
                    {
                        modGlobalRoutines.UpdatePayrollStepTable("CheckRegister", "", "",
                            frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate"),
                            frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayRunID"));
                    }
                    else
                    {
                        if (frmCheckRegister.InstancePtr.rsMultipleWarrants.EndOfFile() != true)
                        {
                            modGlobalRoutines.UpdatePayrollStepTable("CheckRegister", "", "",
                                frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate"),
                                frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayRunID"));
                            modDavesSweetCode.Statics.blnReportCompleted = true;
                        }
                        else
                        {
                            modDavesSweetCode.Statics.blnReportCompleted = true;
                        }
                    }
                }
                else if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
                {
                    modDavesSweetCode.Statics.blnReportCompleted = true;
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			if (strCheckType == "C")
			{
				ShowEmployeeCheckInfo();
			}
			else if (strCheckType == "T")
			{
				ShowTACheckInfo();
			}
			else if (strCheckType == "D")
			{
				ShowDDCheckInfo();
			}
			else
			{
				if (!rsVoidedCheckInfo.EndOfFile())
					ShowVoidedCheckInfo();
			}
		}

		private void ShowEmployeeCheckInfo()
		{
			// vbPorter upgrade warning: fldDirectDeposit As object	OnWrite(string)
			// vbPorter upgrade warning: fldCheckAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldNetPay As object	OnWrite(string)
			// vbPorter upgrade warning: fldDate As object	OnWrite(string)
			// vbPorter upgrade warning: fldPayee As object	OnWrite(string)
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			clsDRWrapper rsDirectDeposit = new clsDRWrapper();
			// vbPorter upgrade warning: curtempDDAmount As Decimal	OnWrite(Decimal, int)
			Decimal curtempDDAmount;
			int intPayrun = 0;
			DateTime dtDate;
			dtDate = frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate");
			if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
			{
				intPayrun = FCConvert.ToInt16(rsCheckInfo.Get_Fields("payrunid"));
				// rsDirectDeposit.OpenRecordset "SELECT SUM(DDAmount) as TotalDDAmount FROM tblCheckDetail WHERE CheckVoid = 0 AND EmployeeNumber = '" & rsCheckInfo.Fields("EmployeeNumber") & "' AND MasterRecord = '" & rsCheckInfo.Fields("MasterRecord") & "' AND BankRecord = 1 AND PayDate = '" & dtDate & "'"
				rsDirectDeposit.OpenRecordset("SELECT SUM(DDAmount) as TotalDDAmount FROM tblCheckDetail WHERE CheckVoid = 0 AND EmployeeNumber = '" + rsCheckInfo.Get_Fields("EmployeeNumber") + "' AND MasterRecord = '" + rsCheckInfo.Get_Fields_String("MasterRecord") + "' AND BankRecord = 1 AND PayDate = '" + FCConvert.ToString(dtDate) + "' AND PayRunID = " + FCConvert.ToString(intPayrun));
			}
			else
			{
				intPayrun = FCConvert.ToInt16(frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayRunID"));
				rsDirectDeposit.OpenRecordset("SELECT SUM(DDAmount) as TotalDDAmount FROM tblCheckDetail WHERE CheckVoid = 0 AND EmployeeNumber = '" + rsCheckInfo.Get_Fields("EmployeeNumber") + "' AND MasterRecord = '" + rsCheckInfo.Get_Fields_String("MasterRecord") + "' AND BankRecord = 1 AND PayDate = '" + FCConvert.ToString(dtDate) + "' AND PayRunID = " + FCConvert.ToString(intPayrun));
			}
			if (rsDirectDeposit.EndOfFile() != true && rsDirectDeposit.BeginningOfFile() != true)
			{
				curtempDDAmount = FCConvert.ToDecimal(Conversion.Val(rsDirectDeposit.Get_Fields("TotalDDAmount")));
			}
			else
			{
				curtempDDAmount = 0;
			}
			intTotalEmployee += 1;
			fldCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields("CheckNumber"));
			fldDirectDeposit.Text = Strings.Format(curtempDDAmount, "#,##0.00");
			fldCheckAmount.Text = Strings.Format(rsCheckInfo.Get_Fields_Decimal("NetPay") - curtempDDAmount, "#,##0.00");
			fldNetPay.Text = Strings.Format(Conversion.Val(rsCheckInfo.Get_Fields_Decimal("NetPay")), "#,##0.00");
			curTotalDDAmount += curtempDDAmount;
			curTotalCheckAmount += FCConvert.ToDecimal(rsCheckInfo.Get_Fields_Decimal("NetPay") - curtempDDAmount);
			fldDate.Text = Strings.Format(rsCheckInfo.Get_Fields("PayDate"), "MM/dd/yy");
			// fldPayee = GetFormat(rsCheckInfo.Fields("EmployeeNumber"), 4) & "  " & rsCheckInfo.Fields("EmployeeName")
			fldPayee.Text = rsCheckInfo.Get_Fields_String("EmployeeNumber") + "  " + rsCheckInfo.Get_Fields_String("EmployeeName");
			if (lblReprint.Visible == false)
			{
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					if (rsCheckInfo.Get_Fields_Decimal("NetPay") - curtempDDAmount > 0)
					{
						if (!Information.IsDate(datTestDate))
						{
							datTestDate = rsCheckInfo.Get_Fields_DateTime("PayDate");
						}
						rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + rsCheckInfo.Get_Fields("CheckNumber") + " AND Type = '2' AND  BANKNUMBER = " + FCConvert.ToString(intBank) + " and checkdate = '" + rsCheckInfo.Get_Fields_DateTime("PayDate") + "'", "TWBD0000.vb1");
						if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
						{
							rsCheckRec.Edit();
						}
						else
						{
							rsCheckRec.AddNew();
						}
						rsCheckRec.Set_Fields("CheckNumber", rsCheckInfo.Get_Fields("CheckNumber"));
						rsCheckRec.Set_Fields("Type", "2");
						rsCheckRec.Set_Fields("CheckDate", rsCheckInfo.Get_Fields_DateTime("PayDate"));
						rsCheckRec.Set_Fields("Name", rsCheckInfo.Get_Fields_String("EmployeeName"));
						rsCheckRec.Set_Fields("Amount", rsCheckInfo.Get_Fields_Decimal("NetPay") - curtempDDAmount);
						rsCheckRec.Set_Fields("Status", "1");
						rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
						rsCheckRec.Set_Fields("BankNumber", intBank);
						rsCheckRec.Update(true);
						// NOW WE WANT TO SHOW THE AMOUNT THAT WAS DD AND SHOW IT AS VOIDED
						if (curtempDDAmount != 0)
						{
							if (!Information.IsDate(datTestDate))
							{
								datTestDate = rsCheckInfo.Get_Fields_DateTime("PayDate");
							}
							rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + rsCheckInfo.Get_Fields("CheckNumber") + " AND Type = '7' and banknumber = " + FCConvert.ToString(intBank) + " and checkdate = '" + rsCheckInfo.Get_Fields_DateTime("PayDate") + "'", "TWBD0000.vb1");
							if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
							{
								rsCheckRec.Edit();
							}
							else
							{
								rsCheckRec.AddNew();
							}
							rsCheckRec.Set_Fields("CheckNumber", rsCheckInfo.Get_Fields("CheckNumber"));
							rsCheckRec.Set_Fields("Type", "7");
							rsCheckRec.Set_Fields("CheckDate", rsCheckInfo.Get_Fields_DateTime("PayDate"));
							rsCheckRec.Set_Fields("Name", rsCheckInfo.Get_Fields_String("EmployeeName") + "(DD)");
							rsCheckRec.Set_Fields("Amount", curtempDDAmount);
							rsCheckRec.Set_Fields("Status", "V");
							rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
							rsCheckRec.Set_Fields("BankNumber", intBank);
							rsCheckRec.Update(true);
						}
					}
					else
					{
						if (!Information.IsDate(datTestDate))
						{
							datTestDate = rsCheckInfo.Get_Fields_DateTime("PayDate");
						}
						rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + rsCheckInfo.Get_Fields("CheckNumber") + " AND Type = '2' and banknumber = " + FCConvert.ToString(intBank) + " and checkdate = '" + rsCheckInfo.Get_Fields_DateTime("PayDate") + "'", "TWBD0000.vb1");
						if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
						{
							rsCheckRec.Edit();
						}
						else
						{
							rsCheckRec.AddNew();
						}
						rsCheckRec.Set_Fields("CheckNumber", rsCheckInfo.Get_Fields("CheckNumber"));
						rsCheckRec.Set_Fields("Type", "7");
						rsCheckRec.Set_Fields("CheckDate", rsCheckInfo.Get_Fields_DateTime("PayDate"));
						rsCheckRec.Set_Fields("Name", rsCheckInfo.Get_Fields_String("EmployeeName") + "(DD)");
						rsCheckRec.Set_Fields("Amount", curtempDDAmount);
						rsCheckRec.Set_Fields("Status", "V");
						rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
						rsCheckRec.Set_Fields("BankNumber", intBank);
						rsCheckRec.Update(true);
					}
				}
			}
			rsCheckRec.Dispose();
			rsDirectDeposit.Dispose();
		}

		private void ShowTACheckInfo()
		{
			// vbPorter upgrade warning: fldDirectDeposit As object	OnWrite(string)
			// vbPorter upgrade warning: fldCheckAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldNetPay As object	OnWrite(string)
			// vbPorter upgrade warning: fldDate As object	OnWrite(string)
			// vbPorter upgrade warning: fldPayee As object	OnWrite(string)
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			clsDRWrapper rsTARecipient = new clsDRWrapper();
			Decimal curtempDDAmount;
			rsTARecipient.OpenRecordset("SELECT * FROM tblRecipients WHERE ID = " + rsTACheckInfo.Get_Fields_Int32("TrustRecipientID"));
			intTotalTA += 1;
			fldCheck.Text = FCConvert.ToString(rsTACheckInfo.Get_Fields("CheckNumber"));
			fldDirectDeposit.Text = Strings.Format(0, "#,##0.00");
			fldCheckAmount.Text = Strings.Format(rsTACheckInfo.Get_Fields("TotalTAAmount"), "#,##0.00");
			fldNetPay.Text = Strings.Format(rsTACheckInfo.Get_Fields("TotalTAAmount"), "#,##0.00");
			curTotalCheckAmount += FCConvert.ToDecimal(rsTACheckInfo.Get_Fields("TotalTAAmount"));
			fldDate.Text = Strings.Format(rsTACheckInfo.Get_Fields("PayDate"), "MM/dd/yy");
			if (rsTARecipient.EndOfFile() != true && rsTARecipient.BeginningOfFile() != true)
			{
				// fldPayee = "T & A " & GetFormat(rsTARecipient.Fields("RecptNumber"), 4) & "  " & rsTARecipient.Fields("Name")
				fldPayee.Text = "T & A " + rsTARecipient.Get_Fields_Int32("RecptNumber") + "  " + rsTARecipient.Get_Fields_String("Name");
			}
			else
			{
				fldPayee.Text = "T & A UNKNOWN";
			}
			if (lblReprint.Visible == false)
			{
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + rsTACheckInfo.Get_Fields("CheckNumber") + " AND Type = '2' and banknumber = " + FCConvert.ToString(intBank), "TWBD0000.vb1");
					if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
					{
						rsCheckRec.Edit();
					}
					else
					{
						rsCheckRec.AddNew();
					}
					rsCheckRec.Set_Fields("CheckNumber", rsTACheckInfo.Get_Fields("CheckNumber"));
					rsCheckRec.Set_Fields("Type", "2");
					rsCheckRec.Set_Fields("CheckDate", rsTACheckInfo.Get_Fields_DateTime("PayDate"));
					if (FCConvert.ToBoolean(rsTARecipient.Get_Fields_Boolean("EFT")))
					{
						rsCheckRec.Set_Fields("Name", rsTARecipient.Get_Fields_String("Name") + " (EFT)");
					}
					else
					{
						rsCheckRec.Set_Fields("Name", rsTARecipient.Get_Fields_String("Name"));
					}
					rsCheckRec.Set_Fields("Amount", rsTACheckInfo.Get_Fields("TotalTAAmount"));
					rsCheckRec.Set_Fields("Status", "1");
					rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
					rsCheckRec.Set_Fields("BankNumber", intBank);
					rsCheckRec.Update(true);
				}
			}
			rsCheckRec.Dispose();
			rsTARecipient.Dispose();
		}

		private void ShowDDCheckInfo()
		{
			// vbPorter upgrade warning: fldDirectDeposit As object	OnWrite(string)
			// vbPorter upgrade warning: fldCheckAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldNetPay As object	OnWrite(string)
			// vbPorter upgrade warning: fldDate As object	OnWrite(string)
			// vbPorter upgrade warning: fldPayee As object	OnWrite(string)
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			clsDRWrapper rsDDBank = new clsDRWrapper();
			Decimal curtempDDAmount;
			string strType;
			bool boolAddingRec = false;
			strType = string.Empty;
			if (blnACH)
			{
				rsDDBank.OpenRecordset("SELECT * FROM tblBanks WHERE ACHBank = 1");
				strType = "ACH";
			}
			else
			{
				rsDDBank.OpenRecordset("SELECT * FROM tblBanks WHERE ID = " + FCConvert.ToString(Conversion.Val(rsDDCheckInfo.Get_Fields("DDBankNumber"))));
				if (FCConvert.ToBoolean(rsDDBank.Get_Fields_Boolean("EFT")))
					strType = "EFT";
			}
			intTotalDD += 1;
			fldCheck.Text = FCConvert.ToString(rsDDCheckInfo.Get_Fields("CheckNumber"));
			fldDirectDeposit.Text = Strings.Format(0, "#,##0.00");
			fldCheckAmount.Text = Strings.Format(rsDDCheckInfo.Get_Fields("TotalDDAmount"), "#,##0.00");
			fldNetPay.Text = Strings.Format(rsDDCheckInfo.Get_Fields("TotalDDAmount"), "#,##0.00");
			curTotalCheckAmount += FCConvert.ToDecimal(rsDDCheckInfo.Get_Fields("TotalDDAmount"));
			fldDate.Text = Strings.Format(rsDDCheckInfo.Get_Fields("PayDate"), "MM/dd/yy");
			if (rsDDBank.EndOfFile() != true && rsDDBank.BeginningOfFile() != true)
			{
				fldPayee.Text = "D / D " + rsDDBank.Get_Fields_Int32("RecordNumber") + "  " + rsDDBank.Get_Fields_String("Name");
			}
			else
			{
				fldPayee.Text = "D / D UNKNOWN";
			}
			if (lblReprint.Visible == false)
			{
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					if (strType == string.Empty)
					{
						if (FCConvert.ToInt32(rsDDCheckInfo.Get_Fields("CheckNumber")) == 0)
						{
							// this case will be when they have chosen to NOT have DD Checks to Bank and
							// the bank is NOT set as EFT or ACH Matthew 1/28/2005
							return;
						}
						else
						{
							rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = -1987456778", "TWBD0000.vb1");
						}
					}
					else
					{
						rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + rsDDCheckInfo.Get_Fields("CheckNumber") + " AND Type = '2' and banknumber = " + FCConvert.ToString(intBank), "TWBD0000.vb1");
					}
					if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
					{
						rsCheckRec.Edit();
						boolAddingRec = false;
					}
					else
					{
						rsCheckRec.AddNew();
						boolAddingRec = true;
					}
					// *******************************************************************************
					// MATTHEW CALL ID 79152 10/28/2005
					// Select Case strType
					// Case "ACH"
					// rsCheckRec.Fields("Type") = "7" 'other debit
					// rsCheckRec.Fields("CheckNumber") = 0
					// MATTHEW 4/27/2005 CALL ID 64927
					// rsCheckRec.Fields("Status") = "V"
					// Case "EFT"
					// rsCheckRec.Fields("Type") = "7" 'other debit
					// rsCheckRec.Fields("CheckNumber") = 0
					// MATTHEW 4/27/2005 CALL ID 64927
					// rsCheckRec.Fields("Status") = "V"
					// Case Else
					// *******************************************************************************
					rsCheckRec.Set_Fields("Type", "2");
					rsCheckRec.Set_Fields("CheckNumber", rsDDCheckInfo.Get_Fields("CheckNumber"));
					// MATTHEW 4/27/2005 CALL ID 64927
					rsCheckRec.Set_Fields("Status", "1");
					// End Select
					rsCheckRec.Set_Fields("CheckDate", rsDDCheckInfo.Get_Fields_DateTime("PayDate"));
					if (rsDDBank.EndOfFile())
					{
						// rscheckrec.Fields("name") = ""
					}
					else
					{
						rsCheckRec.Set_Fields("Name", rsDDBank.Get_Fields_String("Name"));
					}
					// MATTHEW 11/1/2005 CALL ID 80846
					if (boolAddingRec)
					{
						if (fecherFoundation.Strings.Trim(strType) != string.Empty)
						{
							rsCheckRec.Set_Fields("Name", rsCheckRec.Get_Fields_String("Name") + " (" + strType + ")");
						}
						else
						{
							rsCheckRec.Set_Fields("Name", rsCheckRec.Get_Fields_String("Name"));
						}
					}
					rsCheckRec.Set_Fields("Amount", rsDDCheckInfo.Get_Fields("TotalDDAmount"));
					// COMMENTED OUT AND PLACED IN THE CASE STATEMENT ABOVE
					// MATTHEW 4/27/2005 CALL ID 64927
					// rsCheckRec.Fields("Status") = "1"
					rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
					rsCheckRec.Set_Fields("BankNumber", intBank);
					rsCheckRec.Update(true);
				}
			}
			rsCheckRec.Dispose();
			rsDDBank.Dispose();
		}

		private void ShowVoidedCheckInfo()
		{
			// vbPorter upgrade warning: fldDirectDeposit As object	OnWrite(string)
			// vbPorter upgrade warning: fldCheckAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldNetPay As object	OnWrite(string)
			// vbPorter upgrade warning: fldDate As object	OnWrite(string)
			// vbPorter upgrade warning: fldPayee As object	OnWrite(string)
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			clsDRWrapper rsDirectDeposit = new clsDRWrapper();
			// vbPorter upgrade warning: curtempDDAmount As Decimal	OnWrite(Decimal, int)
			Decimal curtempDDAmount;
			if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
			{
				rsDirectDeposit.OpenRecordset("SELECT SUM(DDAmount) as TotalDDAmount FROM tblCheckDetail WHERE CheckVoid = 0 AND EmployeeNumber = '" + rsVoidedCheckInfo.Get_Fields("EmployeeNumber") + "' AND BankRecord = 1 AND PayDate = '" + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") + "'");
			}
			else
			{
				rsDirectDeposit.OpenRecordset("SELECT SUM(DDAmount) as TotalDDAmount FROM tblCheckDetail WHERE CheckVoid = 0 AND EmployeeNumber = '" + rsVoidedCheckInfo.Get_Fields("EmployeeNumber") + "' AND BankRecord = 1 AND PayDate = '" + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_DateTime("PayDate") + "' AND PayRunID = " + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("PayRunID"));
			}
			if (rsDirectDeposit.EndOfFile() != true && rsDirectDeposit.BeginningOfFile() != true)
			{
				curtempDDAmount = FCConvert.ToDecimal(Conversion.Val(rsDirectDeposit.Get_Fields("TotalDDAmount")));
			}
			else
			{
				curtempDDAmount = 0;
			}
			intTotalVoid += 1;
			fldCheck.Text = FCConvert.ToString(rsVoidedCheckInfo.Get_Fields("CheckNumber"));
			fldDirectDeposit.Text = Strings.Format(curtempDDAmount, "#,##0.00");
			if (rsVoidedCheckInfo.Get_Fields("Amount") - curtempDDAmount > 0)
			{
				fldCheckAmount.Text = Strings.Format(rsVoidedCheckInfo.Get_Fields("Amount") - curtempDDAmount, "#,##0.00");
				curTotalCheckAmount += FCConvert.ToDecimal(rsVoidedCheckInfo.Get_Fields("Amount") - curtempDDAmount);
			}
			else
			{
				fldCheckAmount.Text = "0.00";
			}
			if (Conversion.Val(rsVoidedCheckInfo.Get_Fields("amount")) == 0 && curtempDDAmount > 0)
			{
				fldNetPay.Text = fldDirectDeposit.Text;
			}
			else
			{
				fldNetPay.Text = Strings.Format(rsVoidedCheckInfo.Get_Fields("Amount"), "#,##0.00");
			}
			curTotalDDAmount += curtempDDAmount;
			fldDate.Text = Strings.Format(rsVoidedCheckInfo.Get_Fields("CheckDate"), "MM/dd/yy");
			// fldPayee = GetFormat(rsVoidedCheckInfo.Fields("EmployeeNumber"), 4) & "  " & rsVoidedCheckInfo.Fields("EmployeeName")
			fldPayee.Text = rsVoidedCheckInfo.Get_Fields_String("EmployeeNumber") + "  " + rsVoidedCheckInfo.Get_Fields_String("EmployeeName");
			if (lblReprint.Visible == false)
			{
				if (modGlobalVariables.Statics.gboolBudgetary)
				{
					rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + rsVoidedCheckInfo.Get_Fields("CheckNumber") + " AND Type = '2' and banknumber = " + FCConvert.ToString(intBank), "TWBD0000.vb1");
					if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
					{
						rsCheckRec.Edit();
					}
					else
					{
						rsCheckRec.AddNew();
						rsCheckRec.Set_Fields("Amount", rsVoidedCheckInfo.Get_Fields("Amount"));
					}
					rsCheckRec.Set_Fields("CheckNumber", rsVoidedCheckInfo.Get_Fields("CheckNumber"));
					rsCheckRec.Set_Fields("Type", "2");
					rsCheckRec.Set_Fields("CheckDate", rsVoidedCheckInfo.Get_Fields_DateTime("CheckDate"));
					rsCheckRec.Set_Fields("Name", rsVoidedCheckInfo.Get_Fields_String("EmployeeName"));
					rsCheckRec.Set_Fields("Status", "V");
					rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
					rsCheckRec.Set_Fields("BankNumber", intBank);
					rsCheckRec.Update(true);
				}
			}
			rsCheckRec.Dispose();
			rsDirectDeposit.Dispose();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldRegularTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldEmployeeTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldTATotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldDDTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldGrandTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldRegularCount As object	OnWrite
			// vbPorter upgrade warning: fldTACount As object	OnWrite
			// vbPorter upgrade warning: fldDDCount As object	OnWrite
			// vbPorter upgrade warning: fldVoidedCount As object	OnWrite
			// vbPorter upgrade warning: fldTotalCount As object	OnWrite
			fldRegularTotal.Text = Strings.Format(curTotalEmployee, "#,##0.00");
			fldEmployeeTotal.Text = Strings.Format(curTotalEmployee + curTotalDD, "#,##0.00");
			fldTATotal.Text = Strings.Format(curTotalTA, "#,##0.00");
			fldDDTotal.Text = Strings.Format(curTotalDD, "#,##0.00");
			fldGrandTotal.Text = Strings.Format(curTotalEmployee + curTotalTA + curTotalDD, "#,##0.00");
			fldRegularCount.Text = FCConvert.ToString(intTotalEmployee);
			fldTACount.Text = FCConvert.ToString(intTotalTA);
			fldDDCount.Text = FCConvert.ToString(intTotalDD);
			fldVoidedCount.Text = FCConvert.ToString(intTotalVoid);
			fldTotalCount.Text = FCConvert.ToString(intTotalEmployee + intTotalTA + intTotalDD + intTotalVoid);
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotalDD As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalCheckAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalNetPay As object	OnWrite(string)
			fldTotalDD.Text = Strings.Format(curTotalDDAmount, "#,##0.00");
			fldTotalCheckAmount.Text = Strings.Format(curTotalCheckAmount, "#,##0.00");
			fldTotalNetPay.Text = Strings.Format(curTotalDDAmount + curTotalCheckAmount, "#,##0.00");
			if (strTypeHolder == "C")
			{
				curTotalEmployee = curTotalCheckAmount;
			}
			else if (strTypeHolder == "T")
			{
				curTotalTA = curTotalCheckAmount;
			}
			else if (strTypeHolder == "D")
			{
				curTotalDD = curTotalCheckAmount;
			}
			curTotalDDAmount = 0;
			curTotalCheckAmount = 0;
			strTypeHolder = strCheckType;
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lblCheckType As object	OnWrite(string)
			if (strCheckType == "C")
			{
				lblCheckType.Text = "Employee Checks";
			}
			else if (strCheckType == "T")
			{
				lblCheckType.Text = "Trust & Agency Checks";
			}
			else if (strCheckType == "D")
			{
				lblCheckType.Text = "Direct Deposit Checks";
			}
			else
			{
				lblCheckType.Text = "Voided Checks";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		
	}
}
