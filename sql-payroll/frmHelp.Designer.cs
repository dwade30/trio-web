//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmHelp.
	/// </summary>
	partial class frmHelp
	{
		public FCGrid vsData;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblTitle;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.vsData = new fecherFoundation.FCGrid();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 595);
			this.BottomPanel.Size = new System.Drawing.Size(571, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsData);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Size = new System.Drawing.Size(571, 535);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(571, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(145, 30);
			this.HeaderText.Text = "Payroll Help";
			// 
			// vsData
			// 
			this.vsData.AllowSelection = false;
			this.vsData.AllowUserToResizeColumns = false;
			this.vsData.AllowUserToResizeRows = false;
			this.vsData.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsData.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsData.BackColorBkg = System.Drawing.Color.Empty;
			this.vsData.BackColorFixed = System.Drawing.Color.Empty;
			this.vsData.BackColorSel = System.Drawing.Color.Empty;
			this.vsData.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsData.Cols = 10;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsData.ColumnHeadersHeight = 30;
			this.vsData.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsData.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsData.DragIcon = null;
			this.vsData.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsData.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsData.FrozenCols = 0;
			this.vsData.GridColor = System.Drawing.Color.Empty;
			this.vsData.GridColorFixed = System.Drawing.Color.Empty;
			this.vsData.Location = new System.Drawing.Point(30, 65);
			this.vsData.Name = "vsData";
			this.vsData.OutlineCol = 0;
			this.vsData.ReadOnly = true;
			this.vsData.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsData.RowHeightMin = 0;
			this.vsData.Rows = 50;
			this.vsData.ScrollTipText = null;
			this.vsData.ShowColumnVisibilityMenu = false;
			this.vsData.Size = new System.Drawing.Size(511, 155);
			this.vsData.StandardTab = true;
			this.vsData.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsData.TabIndex = 3;
			// 
			// txtDescription
			// 
			this.txtDescription.MaxLength = 25000;
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Menu;
			this.txtDescription.LinkItem = null;
			this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDescription.LinkTopic = null;
			this.txtDescription.Location = new System.Drawing.Point(30, 239);
			this.txtDescription.LockedOriginal = true;
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.ReadOnly = true;
			this.txtDescription.ScrollBars = Wisej.Web.ScrollBars.Vertical;
			this.txtDescription.Size = new System.Drawing.Size(511, 40);
			this.txtDescription.TabIndex = 1;
			this.txtDescription.TabStop = false;
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(30, 30);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(511, 13);
			this.lblTitle.TabIndex = 0;
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// frmHelp
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(571, 370);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmHelp";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Payroll Help";
			this.Load += new System.EventHandler(this.frmHelp_Load);
			this.Activated += new System.EventHandler(this.frmHelp_Activated);
			this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmHelp_KeyUp);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
