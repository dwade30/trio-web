//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptMSRSPayrollSummary.
	/// </summary>
	public partial class rptMSRSPayrollSummary : BaseSectionReport
	{
		public rptMSRSPayrollSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "MSRS Payroll Summary";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMSRSPayrollSummary InstancePtr
		{
			get
			{
				return (rptMSRSPayrollSummary)Sys.GetInstance(typeof(rptMSRSPayrollSummary));
			}
		}

		protected rptMSRSPayrollSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMSRSPayrollSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		int lngReportType;
		bool boolRange;
		string strRangeStart = "";
		string strRangeEnd = "";
		string strExplanation = "";
		bool boolElectronicMSRS;
		// vbPorter upgrade warning: clsMSRSObject As clsMSRS	OnWrite(object)
		clsMSRS clsMSRSObject;
		// vbPorter upgrade warning: MSRSObject As object	OnWrite(clsMSRS)	OnRead(clsMSRS)
		public void Init(int lngTypeOfReport, bool boolUseElectronic/*= false*/, clsMSRS MSRSObject, List<string> batchReports = null)
		{
			lngReportType = lngTypeOfReport;
			boolElectronicMSRS = boolUseElectronic;
			if (boolElectronicMSRS)
			{
				clsMSRSObject = MSRSObject;
			}
			// frmReportViewer.Init Me, , 1
			if (!modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modCoreysSweeterCode.CheckDefaultPrint(this, "", 1, boolAllowEmail: true, strAttachmentName: "MainePERS", boolDontAllowCloseTilReportDone: true);
			}
			else
			{
				this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
				modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//GrapeCity.ActiveReports.Document.Section.Page pg = new GrapeCity.ActiveReports.Document.Section.Page();
			// vbPorter upgrade warning: intPageIndex As int	OnWrite(int, double)
			int intPageIndex;
			int X;
			string strCopy = "";
			float lngXCoord = 0;
			float lngYCoord = 0;
			if (boolElectronicMSRS)
			{
				clsMSRSObject.SetSummaryRecord();
			}
			intPageIndex = this.Document.Pages.Count;
			//FC:FINAL:DSE Endless loop if modifying collection during foreach loop
			//foreach (GrapeCity.ActiveReports.Document.Section.Page pg in this.Document.Pages)
			//{
			//	this.Document.Pages.Insert(intPageIndex, pg);
			//	intPageIndex += 1;
			//}
			int nrPages = this.Document.Pages.Count;
			for (int i = 0; i < nrPages; i++)
			{
				GrapeCity.ActiveReports.Document.Section.Page pg = (GrapeCity.ActiveReports.Document.Section.Page)Document.Pages[i].Clone();
				this.Document.Pages.Insert(intPageIndex, pg);
				intPageIndex += 1;
			}
			// pg
			intPageIndex = FCConvert.ToInt16(this.Document.Pages.Count / 2 - 1);
			X = 0;
			foreach (GrapeCity.ActiveReports.Document.Section.Page pg in this.Document.Pages)
			{
				if (X <= intPageIndex)
                {
					// first report
					strCopy = "MainePERS COPY";
					pg.Font = new Font(pg.Font.Name, 10);
					pg.Font = new Font(pg.Font, FontStyle.Bold);
					pg.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
					lngXCoord = 1530 / 1440f;
					lngYCoord = 14850 / 1440f;
					pg.DrawText(strCopy, lngXCoord, lngYCoord, 1800 / 1440f, 270 / 1440f);
				}
				else
				{
					// second report
					strCopy = "EMPLOYER COPY";
					pg.Font = new Font(pg.Font.Name, 10);
					pg.Font = new Font(pg.Font, FontStyle.Bold);
					pg.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
					lngXCoord = 1530 / 1440f;
					lngYCoord = 14850 / 1440f;
					pg.DrawText(strCopy, lngXCoord, lngYCoord, 1800 / 1440f, 270 / 1440f);
				}
				X += 1;
			}
			// pg
			//this.Document.Pages.Commit();
			//this.Refresh();
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			string strTemp = "";
			int X;
			double dblPremiumTotal;
			double dblContributionTotal;
			double dblTotalC;
			double dblTotalD;
			double dblTotalRemittance;
			int intCurrentCode;
			double[] dblEmpRate = new double[6 + 1];
			double[] dblEarnComp = new double[6 + 1];
			// vbPorter upgrade warning: dblEmployerCont As double	OnWrite(string)
			double[] dblEmployerCont = new double[6 + 1];
			double[] dblCostOrCredit = new double[6 + 1];
			double[] dblEmployeeCont = new double[6 + 1];
			double[] dblTotcontributions = new double[6 + 1];
			double dblTotEmployerCont = 0;
			double dblTotCostOrCredit = 0;
			double dblTotEmployeeCont = 0;
			double dblTotEarnableCompensation;
			// vbPorter upgrade warning: dblTotEarnableCompensationForStatusY As double	OnWrite(int, string)
			double dblTotEarnableCompensationForStatusY;
			double dblBasicActives = 0;
			double dblBasicRetirees = 0;
			double dblSupplemental = 0;
			double dblDependent = 0;
			double dblDepARate = 0;
			double dblDepBRate = 0;
			double[] dblDebitOrCredit = new double[2 + 1];
			string[] strDebitOrCredit = new string[2 + 1];
			int intWeeks = 0;
			double dblMTDWithheld;
			string strMSRSCode = "";
			// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string)
			DateTime dtStartDate = DateTime.FromOADate(0);
			DateTime dtEndDate = DateTime.FromOADate(0);
			clsDRWrapper clsMSRS = new clsDRWrapper();
			clsDRWrapper clsEmpCont = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			bool boolAutoCalcCredit = false;
			double dblPayBack;
			// vbPorter upgrade warning: dblTACorrection As double	OnWrite(string)
			double dblTACorrection = 0;
			dblPremiumTotal = 0;
			dblContributionTotal = 0;
			dblTotalC = 0;
			dblTotalD = 0;
			dblDebitOrCredit[1] = 0;
			dblDebitOrCredit[2] = 0;
			strDebitOrCredit[1] = "";
			strDebitOrCredit[2] = "";
			dblTotalRemittance = 0;
			dblTotEarnableCompensation = 0;
			dblTotEarnableCompensationForStatusY = 0;
			dblPayBack = 0;
			// corey 6/30/2005 call id
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				lngReportType = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("MSRSReportType"))));
			}
			boolRange = false;
			strSQL = "SELECT sum(paybackamount) as totpayback from tblmsrstempdetailreport";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				dblPayBack = Conversion.Val(clsLoad.Get_Fields("totpayback"));
			}
			strSQL = "select * from tblMSRSTable where reporttype = " + FCConvert.ToString(lngReportType);
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalExcessPayback = dblPayBack;
			modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalAdjustmentsGrantFunded = 0;
			modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalAdjustmentsGrantFundedEmployer = 0;
			modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalGrantFunded = 0;
			modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalGrantFundedEmployerContributions = 0;
			if (!clsLoad.EndOfFile())
			{
				boolRange = FCConvert.ToBoolean(clsLoad.Get_Fields("range"));
				strRangeStart = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("rangestart")));
				strRangeEnd = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("rangeend")));
				txtEmployerCode.Text = clsLoad.Get_Fields_String("EmployerCode");
				txtEmployerName.Text = clsLoad.Get_Fields_String("EmployerName");
				intWeeks = 0;
				for (X = 1; X <= 5; X++)
				{
					if (!clsLoad.IsFieldNull("PaidDatesEnding" + FCConvert.ToString(X)) && Convert.ToDateTime(clsLoad.Get_Fields_DateTime("PaidDatesEnding" + FCConvert.ToString(X))).ToOADate() != 0)
					{
						intWeeks += 1;
						strTemp += Strings.Format(clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X)), "MM/dd/yyyy") + "  ";
						if (X == 1)
							dtStartDate = (DateTime)clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X));
						dtEndDate = (DateTime)clsLoad.Get_Fields("paiddatesending" + FCConvert.ToString(X));
					}
				}
				// X
				dtStartDate = FCConvert.ToDateTime(FCConvert.ToString(dtStartDate.Month) + "/1/" + FCConvert.ToString(dtStartDate.Year));
				strTemp = fecherFoundation.Strings.Trim(strTemp);
				txtPaidDatesEnding.Text = strTemp;
				txtPreparersName.Text = clsLoad.Get_Fields_String("PreparersName");
				txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				// strTemp = Format(Val(.Fields("telephone")), "0000000000")
				// txtTelephone.Caption = "(" + Mid(strTemp, 1, 3) + ") " & Mid(strTemp, 4, 3) & "-" & Mid(strTemp, 7)
				txtTelephone.Text = clsLoad.Get_Fields("telephone");
				dblBasicActives = Conversion.Val(clsLoad.Get_Fields("premiumactive"));
				// dblBasicRetirees = Val(.Fields("premiumRetiree"))
				dblSupplemental = Conversion.Val(clsLoad.Get_Fields("premiumsupplemental"));
				dblDependent = Conversion.Val(clsLoad.Get_Fields_Double("PremiumDependent"));
				dblDepARate = Conversion.Val(clsLoad.Get_Fields_Double("DepARate"));
				dblDepBRate = Conversion.Val(clsLoad.Get_Fields_Double("DepBRate"));
				CalculatePremiums(ref dblBasicActives, ref dblBasicRetirees, ref dblSupplemental, ref dblDependent, ref dblDepARate, ref dblDepBRate, ref dtStartDate, ref dtEndDate, ref intWeeks);
				if (dblBasicRetirees == 0)
				{
					dblBasicRetirees = Conversion.Val(clsLoad.Get_Fields("premiumRetiree"));
				}
				txtActivePremium.Text = Strings.Format(dblBasicActives, "#,###,##0.00");
				txtRetireePremium.Text = Strings.Format(dblBasicRetirees, "#,###,##0.00");
				txtSupplementalPremiums.Text = Strings.Format(dblSupplemental, "#,###,##0.00");
				txtDependentPremiums.Text = Strings.Format(dblDependent, "#,###,##0.00");
				dblPremiumTotal = Conversion.Val(dblSupplemental + dblBasicActives + dblBasicRetirees + dblDependent);
				txtPremiumTotal.Text = Strings.Format(dblPremiumTotal, "#,###,##0.00");
				dblDebitOrCredit[1] = Conversion.Val(clsLoad.Get_Fields("creditordebittaken1"));
				dblDebitOrCredit[2] = Conversion.Val(clsLoad.Get_Fields("creditordebittaken2"));
				strDebitOrCredit[1] = FCConvert.ToString(clsLoad.Get_Fields("creditordebitno1"));
				strDebitOrCredit[2] = FCConvert.ToString(clsLoad.Get_Fields("creditordebitno2"));
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("AutoAddEmployerCredit")))
				{
					boolAutoCalcCredit = true;
				}
				else
				{
					boolAutoCalcCredit = false;
				}
				modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalActive = dblBasicActives;
				modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalDependent = dblDependent;
				modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalRetiree = dblBasicRetirees;
				modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalSupplemental = dblSupplemental;
				modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalLifeInsurancePremiums = dblPremiumTotal;
			}
			else
			{
				MessageBox.Show("There is no MainePERS information in the table.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
			}
			// strSQL = "select sum(employeecontributions) as totEmpCont , plancode from (select distinct employeecontributions,employeenumber,plancode from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 ) group by plancode "
			// 04/15/2004 Corey.  Was dropping some of the employee contributions when the contributions for the same employee were the same amount because of the distinct clause
			strSQL = "select sum(employeecontributions) as totEmpCont,plancode from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 group by plancode ";
			clsEmpCont.OpenRecordset(strSQL, "twpy0000.vb1");
			// strSQL = "select sum(earnablecompensation) as totEarnComp, sum(Employeecontributions) as TotEmpCont,plancode from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 and employeecontributions > 0 group by plancode "
			strSQL = "select sum(earnablecompensation) as totEarnCompForStatusY from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 and employeecontributions > 0 and participation = 'Y'";
			clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1");
			if (!clsMSRS.EndOfFile())
			{
				dblTotEarnableCompensationForStatusY = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsMSRS.Get_Fields("totearncompforstatusy")), "0.00"));
			}
			strSQL = "select sum(earnablecompensation) as totEarnComp, sum(Employeecontributions) as TotEmpCont,plancode from tblmsrstempdetailreport where convert(int, isnull(statuscode, 0)) <> 53 and earnablecompensation > 0 and employeecontributions > 0 group by plancode ";
			clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			strSQL = "select * from tblmsrsCodeRate where reporttype = " + FCConvert.ToString(lngReportType);
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			intCurrentCode = 1;
			// Do While Not clsLoad.EndOfFile
			while (!clsMSRS.EndOfFile())
			{
				// If Not clsMSRS.EndOfFile Then
				dblEmpRate[intCurrentCode] = 0;
				dblCostOrCredit[intCurrentCode] = 0;
				if (!clsLoad.EndOfFile())
				{
					// If clsMSRS.FindFirstRecord("plancode", "'" & clsLoad.Fields("code") & "'") Then
					if (clsLoad.FindFirstRecord("code", clsMSRS.Get_Fields("plancode")))
					{
						// dblEarnComp(intCurrentCode) = Val(clsMSRS.Fields("totearncomp"))
						// dblEmployeeCont(intCurrentCode) = Val(clsMSRS.Fields("totempcont"))
						dblEmpRate[intCurrentCode] = Conversion.Val(clsLoad.Get_Fields("rate"));
						if (!boolAutoCalcCredit)
						{
							dblCostOrCredit[intCurrentCode] = Conversion.Val(clsLoad.Get_Fields("costorcredit"));
						}
					}
				}
				dblEarnComp[intCurrentCode] = Conversion.Val(clsMSRS.Get_Fields("totearncomp"));
				// dblEmployeeCont(intCurrentCode) = Val(clsMSRS.Fields("totempcont"))
				if (!(clsEmpCont.EndOfFile() && clsEmpCont.BeginningOfFile()))
				{
					if (clsEmpCont.FindFirstRecord("plancode", clsMSRS.Get_Fields("plancode")))
					{
						dblEmployeeCont[intCurrentCode] = Conversion.Val(clsEmpCont.Get_Fields("totempcont"));
					}
					else
					{
						dblEmployeeCont[intCurrentCode] = 0;
					}
				}
				else
				{
					dblEmployeeCont[intCurrentCode] = 0;
				}
				// dblEmpRate(intCurrentCode) = Val(.Fields("rate"))
				// dblCostOrCredit(intCurrentCode) = Val(.Fields("costorcredit"))
				dblEmployerCont[intCurrentCode] = FCConvert.ToDouble(Strings.Format(dblEarnComp[intCurrentCode] * dblEmpRate[intCurrentCode], "0.00"));
				if (boolAutoCalcCredit)
				{
					dblCostOrCredit[intCurrentCode] = -dblEmployerCont[intCurrentCode];
				}
				// the credit cannot exceed the amount in employer contribution
				if (-1 * dblCostOrCredit[intCurrentCode] > dblEmployerCont[intCurrentCode])
					dblCostOrCredit[intCurrentCode] = dblEmployerCont[intCurrentCode] * -1;
				dblTotcontributions[intCurrentCode] = dblEmployerCont[intCurrentCode] + dblCostOrCredit[intCurrentCode] + dblEmployeeCont[intCurrentCode];
				dblTotEarnableCompensation += dblEarnComp[intCurrentCode];
				// if participation = y
				switch (intCurrentCode)
				{
					case 1:
						{
							// txtPlan1.Caption = .Fields("code")
							txtPlan1.Text = clsMSRS.Get_Fields("plancode");
							txtEmployerRate1.Text = Strings.Format(dblEmpRate[intCurrentCode], "0.0000");
							txtEarnableComp1.Text = Strings.Format(dblEarnComp[intCurrentCode], "#,###,##0.00");
							txtEmpCont1.Text = Strings.Format(dblEmployerCont[intCurrentCode], "#,###,##0.00");
							txtCostCredit1.Text = Strings.Format(dblCostOrCredit[intCurrentCode], "#,###,##0.00");
							txtEmplCont1.Text = Strings.Format(dblEmployeeCont[intCurrentCode], "#,###,##0.00");
							txtTotalCont1.Text = Strings.Format(dblTotcontributions[intCurrentCode], "#,###,##0.00");
							break;
						}
					case 2:
						{
							// txtPlan2.Caption = .Fields("code")
							txtPlan2.Text = clsMSRS.Get_Fields("plancode");
							txtEmployerRate2.Text = Strings.Format(dblEmpRate[intCurrentCode], "0.0000");
							txtEarnableComp2.Text = Strings.Format(dblEarnComp[intCurrentCode], "#,###,##0.00");
							txtEmpCont2.Text = Strings.Format(dblEmployerCont[intCurrentCode], "#,###,##0.00");
							txtCostCredit2.Text = Strings.Format(dblCostOrCredit[intCurrentCode], "#,###,##0.00");
							txtEmplCont2.Text = Strings.Format(dblEmployeeCont[intCurrentCode], "#,###,##0.00");
							txtTotalCont2.Text = Strings.Format(dblTotcontributions[intCurrentCode], "#,###,##0.00");
							break;
						}
					case 3:
						{
							// txtPlan3.Caption = .Fields("code")
							txtPlan3.Text = clsMSRS.Get_Fields("plancode");
							txtEmployerRate3.Text = Strings.Format(dblEmpRate[intCurrentCode], "0.0000");
							txtEarnableComp3.Text = Strings.Format(dblEarnComp[intCurrentCode], "#,###,##0.00");
							txtEmpCont3.Text = Strings.Format(dblEmployerCont[intCurrentCode], "#,###,##0.00");
							txtCostCredit3.Text = Strings.Format(dblCostOrCredit[intCurrentCode], "#,###,##0.00");
							txtEmplCont3.Text = Strings.Format(dblEmployeeCont[intCurrentCode], "#,###,##0.00");
							txtTotalCont3.Text = Strings.Format(dblTotcontributions[intCurrentCode], "#,###,##0.00");
							break;
						}
					case 4:
						{
							// txtPlan4.Caption = .Fields("code")
							txtPlan4.Text = clsMSRS.Get_Fields("plancode");
							txtEmployerRate4.Text = Strings.Format(dblEmpRate[intCurrentCode], "0.0000");
							txtEarnableComp4.Text = Strings.Format(dblEarnComp[intCurrentCode], "#,###,##0.00");
							txtEmpCont4.Text = Strings.Format(dblEmployerCont[intCurrentCode], "#,###,##0.00");
							txtCostCredit4.Text = Strings.Format(dblCostOrCredit[intCurrentCode], "#,###,##0.00");
							txtEmplCont4.Text = Strings.Format(dblEmployeeCont[intCurrentCode], "#,###,##0.00");
							txtTotalCont4.Text = Strings.Format(dblTotcontributions[intCurrentCode], "#,###,##0.00");
							break;
						}
					case 5:
						{
							// txtPlan5.Caption = .Fields("code")
							txtPlan5.Text = clsMSRS.Get_Fields("plancode");
							txtEmployerRate5.Text = Strings.Format(dblEmpRate[intCurrentCode], "0.0000");
							txtEarnableComp5.Text = Strings.Format(dblEarnComp[intCurrentCode], "#,###,##0.00");
							txtEmpCont5.Text = Strings.Format(dblEmployerCont[intCurrentCode], "#,###,##0.00");
							txtCostCredit5.Text = Strings.Format(dblCostOrCredit[intCurrentCode], "#,###,##0.00");
							txtEmplCont5.Text = Strings.Format(dblEmployeeCont[intCurrentCode], "#,###,##0.00");
							txtTotalCont5.Text = Strings.Format(dblTotcontributions[intCurrentCode], "#,###,##0.00");
							break;
						}
					case 6:
						{
							// txtPlan6.Caption = .Fields("code")
							txtPlan6.Text = clsMSRS.Get_Fields("plancode");
							txtEmployerRate6.Text = Strings.Format(dblEmpRate[intCurrentCode], "0.0000");
							txtEarnableComp6.Text = Strings.Format(dblEarnComp[intCurrentCode], "#,###,##0.00");
							txtEmpCont6.Text = Strings.Format(dblEmployerCont[intCurrentCode], "#,###,##0.00");
							txtCostCredit6.Text = Strings.Format(dblCostOrCredit[intCurrentCode], "#,###,##0.00");
							txtEmplCont6.Text = Strings.Format(dblEmployeeCont[intCurrentCode], "#,###,##0.00");
							txtTotalCont6.Text = Strings.Format(dblTotcontributions[intCurrentCode], "#,###,##0.00");
							break;
						}
					case 7:
						{
							break;
						}
				}
				//end switch
				intCurrentCode += 1;
				// clsLoad.MoveNext
				clsMSRS.MoveNext();
			}
			for (X = 1; X <= 6; X++)
			{
				dblTotEmployerCont += dblEmployerCont[X];
				dblTotCostOrCredit += dblCostOrCredit[X];
				dblTotEmployeeCont += dblEmployeeCont[X];
				dblTotcontributions[X] = dblEmployerCont[X] + dblCostOrCredit[X] + dblEmployeeCont[X];
				dblContributionTotal += dblTotcontributions[X];
			}
			// X
			// MSRSSummaryRec.TotalRetirementContributions = dblContributionTotal
			// 09/22/2005
			modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalRetirementContributions = dblTotEmployeeCont;
			if (dblPayBack > 0)
			{
				dblContributionTotal += dblPayBack;
				txtTotalCont6.Text = Strings.Format(dblPayBack, "#,###,##0.00");
				txtEmplCont6.Text = Strings.Format(dblPayBack, "#,###,##0.00");
				txtCostCredit6.Text = "Payback";
			}
			txtTotalContTotal.Text = Strings.Format(dblContributionTotal, "#,###,##0.00");
			txtEmpContTotal.Text = Strings.Format(dblTotEmployerCont, "#,###,##0.00");
			txtCostCreditTotal.Text = Strings.Format(dblTotCostOrCredit, "#,###,##0.00");
			txtEmplContTotal.Text = Strings.Format(dblTotEmployeeCont, "#,###,##0.00");
			modCoreysSweeterCode.Statics.MSRSSummaryRec.TotalEarnableCompensation = dblTotEarnableCompensation;
			modCoreysSweeterCode.Statics.MSRSSummaryRec.EmployerPaidTotalEarnableCompensation = dblTotEarnableCompensationForStatusY;
			// must account for returned checks making adjustments to t+A checks
			dtEndDate = fecherFoundation.DateAndTime.DateAdd("m", 1, dtStartDate);
			dtEndDate = fecherFoundation.DateAndTime.DateAdd("d", -1, dtEndDate);
			double dblTAmount;
			dblTAmount = 0;
			strSQL = "select trustamount,trustrecipientID from tblcheckdetail where taadjustrecord = 1 and paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and reporttype = " + FCConvert.ToString(lngReportType);
			clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1");
			while (!clsMSRS.EndOfFile())
			{
				rsTemp.OpenRecordset("select * from TBLRECIPIENTS where ID = " + FCConvert.ToString(Conversion.Val(clsMSRS.Get_Fields("trustrecipientID"))) + " and freq = 'Monthly'", "twpy0000.vb1");
				if (!rsTemp.EndOfFile())
				{
					dblTAmount += Conversion.Val(clsMSRS.Get_Fields("trustamount"));
				}
				clsMSRS.MoveNext();
			}
			// strSQL = "select sum(trustamount) as totTrustAmount from tblcheckdetail where taadjustrecord = 1 and paydate between '" & dtStartDate & "' and '" & dtEndDate & "' and reporttype = " & lngReportType
			clsMSRS.OpenRecordset(strSQL, "twpy0000.vb1");
			// If Not clsMSRS.EndOfFile Then
			if (dblTAmount != 0)
			{
				// if they're both debits then we cant do this
				// MATTHEW 8/172005 CALL ID 75088
				// TALKED TO RON AND HE AGREES THAT THIS SHOULD BE A NEGATIVE SO THAT IT
				// WILL REDUCE THE AMOUNT OF REMITTANCE ON THE MSRS REPORT
				// dblTACorrection = Format(-1 * Val(clsMSRS.Fields("tottrustamount")), "0.00")
				dblTACorrection = FCConvert.ToDouble(Strings.Format(dblTAmount, "0.00"));
				if (dblTACorrection != 0)
				{
					if (dblDebitOrCredit[1] + dblTACorrection != 0)
					{
						if (dblDebitOrCredit[1] <= 0)
						{
							dblDebitOrCredit[1] += dblTACorrection;
						}
						else if (dblDebitOrCredit[2] <= 0)
						{
							dblDebitOrCredit[2] += dblTACorrection;
						}
						strExplanation = "Voided Checks   (contribution amounts) " + Strings.Format(dblTACorrection, "#,###,##0.00");
					}
					else
					{
						dblDebitOrCredit[1] = 0;
					}
				}
			}
			txtCMDM1.Text = strDebitOrCredit[1];
			txtCMDM2.Text = strDebitOrCredit[2];
			txtCreditDebit13.Text = Strings.Format(dblDebitOrCredit[1], "0.00");
			txtCreditDebit14.Text = Strings.Format(dblDebitOrCredit[2], "0.00");
			dblTotalD = dblDebitOrCredit[1] + dblDebitOrCredit[2];
			dblTotalC = dblPremiumTotal + dblContributionTotal;
			dblTotalRemittance = dblTotalC + dblTotalD;
			txtTotalRemittance.Text = Strings.Format(dblTotalRemittance, "#,###,##0.00");
			txtC.Text = Strings.Format(dblTotalC, "#,###,##0.00");
			txtD.Text = Strings.Format(dblTotalD, "#,###,##0.00");
			clsEmpCont.Dispose();
			clsMSRS.Dispose();
			rsTemp.Dispose();
		}
		// vbPorter upgrade warning: dblBasicActives As object	OnWrite(double, int)
		// vbPorter upgrade warning: dblBasicRetirees As object	OnWrite(double, int)
		// vbPorter upgrade warning: dblSupplemental As object	OnWrite(double, int)
		// vbPorter upgrade warning: dblDependent As object	OnWrite(double, int)
		// vbPorter upgrade warning: dblDepARate As object	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: dblDepBRate As object	OnWriteFCConvert.ToDouble(
		private void CalculatePremiums(ref double dblBasicActives, ref double dblBasicRetirees, ref double dblSupplemental, ref double dblDependent, ref double dblDepARate, ref double dblDepBRate, ref DateTime dtStartDate, ref DateTime dtEndDate, ref int intWeeks)
		{
			double dblTemp = 0;
			double dblBasicActivesTemp = 0;
			double dblSupplementalTemp = 0;
			double dblDependentTemp = 0;
			clsDRWrapper clsPremium = new clsDRWrapper();
			string strSQL;
			string strSubQuery;
			string strNonPaidQuery;
			double dblMTDWithheld = 0;
			string strMSRSCode = "";
			string strWhere;
			string strComma;
			bool boolRange;
			string strRangeStart = "";
			string strRangeEnd = "";
			double dblBasicRetTemp = 0;
			string strMatchWhere;
			if (FCConvert.ToInt32(dblDepARate) == 0 && FCConvert.ToInt32(dblDepBRate) == 0)
				return;
			dblBasicActives = 0;
			dblDependent = 0;
			dblSupplemental = 0;
			dblBasicRetirees = 0;
			strSQL = "select * from tbldefaultdeductions where type = 'L'";
			clsPremium.OpenRecordset(strSQL, "twpy0000.vb1");
			strWhere = " and reporttype = " + FCConvert.ToString(lngReportType) + " ";
			strMatchWhere = " and reporttype = " + FCConvert.ToString(lngReportType) + " ";
			strComma = "";
			if (!clsPremium.EndOfFile())
			{
				strWhere = " and deddeductionnumber in (";
				strMatchWhere = " and MATCHdeductionnumber in (";
				while (!clsPremium.EndOfFile())
				{
					strWhere += strComma + clsPremium.Get_Fields("deductionid");
					strMatchWhere += strComma + clsPremium.Get_Fields("deductionid");
					strComma = ",";
					clsPremium.MoveNext();
				}
				strWhere += ")";
				strMatchWhere += ")";
			}
			// strSubQuery = "(select employeenumber,sum(dedamount) as Premiums  from tblcheckdetail where deductionrecord = 1  and paydate between '" & dtStartDate & "' and '" & dtEndDate & "'" & strWhere & "  group by employeenumber) as ChkQuery "
			strSubQuery = "(select tblcheckdetail.employeenumber as employeenumber,sum(convert(double, isnull(dedamount, 0)) + convert(double, isnull(MATCHAMOUNT, 0))) as Premiums  from tblcheckdetail inner join (select distinct employeenumber from tblmsrstempdetailreport) as TempDetailQuery on (tblcheckdetail.employeenumber = tempdetailquery.employeenumber) where  paydate between '" + FCConvert.ToString(dtStartDate) + "' and '" + FCConvert.ToString(dtEndDate) + "' and ((deductionrecord = 1  " + strWhere + ") or (matchrecord = 1 " + strMatchWhere + ") ) group by tblcheckdetail.employeenumber) as ChkQuery ";
			strNonPaidQuery = "(select LifeInsuranceCode as lifeinscode,InsuranceContributions as PremiumsSum from tblMSRSNonPaid where include = 1 and status <> 'R' and LEFT(isnull(positioncode, ' '),1) <> 'Y') ";
			// strNonPaidQuery = "(select LifeInsuranceCode as lifeinscode,InsuranceContributions as PremiumsSum,status from tblMSRSNonPaid where include = 1 and LEFT(isnull(positioncode, ' '),1) <> 'Y') "
			// inner join tblmiscupdate with with checkdetail for the regular employees
			// union with the same information from the MSRSNonPaid table
			strSQL = "(select lifeinscode,sum(premiums) as PremiumsSum from tblmiscupdate inner join " + strSubQuery + "  on (chkQuery.employeenumber = tblmiscupdate.employeenumber)  where tblmiscupdate.include = 1 group by tblmiscupdate.lifeinscode) ";
			strSQL += " union " + strNonPaidQuery;
			// too many layers of subqueries for access to handle in one statement
			// Call clsPremium.CreateStoredProcedure("MSRSCalcPremiumsQuery", strSQL, "twpy0000.vb1")
			// select the info we need from it
			// Call clsPremium.OpenRecordset("select lifeinscode,sum(premiumssum) as SumPremiums from msrscalcpremiumsquery group by lifeinscode", "twpy0000.vb1")
			clsPremium.OpenRecordset("select lifeinscode,sum(premiumssum) as SumPremiums from [ " + strSQL + "]. as msrscalcpremiumsquery group by lifeinscode", "twpy0000.vb1");
			while (!clsPremium.EndOfFile())
			{
				dblDependentTemp = 0;
				dblBasicActivesTemp = 0;
				dblSupplementalTemp = 0;
				dblMTDWithheld = Conversion.Val(clsPremium.Get_Fields("sumpremiums"));
				// intWeeks = Val(clsPremium.Fields(""))
				strMSRSCode = FCConvert.ToString(clsPremium.Get_Fields("Lifeinscode"));
				if (fecherFoundation.Strings.UCase(strMSRSCode) == "HA")
				{
					dblDependentTemp = dblDepARate * intWeeks;
					dblBasicActivesTemp = dblMTDWithheld - dblDependentTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "HB")
				{
					dblDependentTemp = dblDepBRate * intWeeks;
					dblBasicActivesTemp = dblMTDWithheld - dblDependentTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S1")
				{
					dblSupplementalTemp = dblMTDWithheld / 2;
					dblBasicActivesTemp = dblMTDWithheld - dblSupplementalTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S2")
				{
					dblBasicActivesTemp = dblMTDWithheld / 3;
					dblSupplementalTemp = dblMTDWithheld - dblBasicActivesTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S3")
				{
					dblBasicActivesTemp = dblMTDWithheld / 4;
					dblSupplementalTemp = dblMTDWithheld - dblBasicActivesTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F1A")
				{
					dblDependentTemp = dblDepARate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicActivesTemp = dblTemp / 2;
					dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F2A")
				{
					dblDependentTemp = dblDepARate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicActivesTemp = dblTemp / 3;
					dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F3A")
				{
					dblDependentTemp = dblDepARate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicActivesTemp = dblTemp / 4;
					dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F1B")
				{
					dblDependentTemp = dblDepBRate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicActivesTemp = dblTemp / 2;
					dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F2B")
				{
					dblDependentTemp = dblDepBRate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicActivesTemp = dblTemp / 3;
					dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F3B")
				{
					dblDependentTemp = dblDepBRate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicActivesTemp = dblTemp / 4;
					dblSupplementalTemp = dblTemp - dblBasicActivesTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "B")
				{
					dblBasicActivesTemp = dblMTDWithheld;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "I")
				{
					dblBasicActivesTemp = dblMTDWithheld;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "R")
				{
					// Matthew 11/23/2004 Need to deal with this code
				}
				else
				{
					dblBasicActivesTemp = dblMTDWithheld;
					MessageBox.Show("Unknown MainePERS Code " + strMSRSCode);
				}
				clsPremium.MoveNext();
				dblDependent += dblDependentTemp;
				dblBasicActives += dblBasicActivesTemp;
				dblSupplemental += dblSupplementalTemp;
			}
			// now retired
			strNonPaidQuery = "select LifeInsuranceCode as lifeinscode,sum(InsuranceContributions) as SumPremiums from tblMSRSNonPaid where include = 1 and status = 'R' and LEFT(isnull(positioncode, ' '),1) <> 'Y' group by lifeinsurancecode";
			clsPremium.OpenRecordset(strNonPaidQuery, "twpy0000.vb1");
			while (!clsPremium.EndOfFile())
			{
				dblDependentTemp = 0;
				dblBasicRetTemp = 0;
				dblSupplementalTemp = 0;
				dblMTDWithheld = Conversion.Val(clsPremium.Get_Fields("sumpremiums"));
				// intWeeks = Val(clsPremium.Fields(""))
				strMSRSCode = FCConvert.ToString(clsPremium.Get_Fields("Lifeinscode"));
				if (fecherFoundation.Strings.UCase(strMSRSCode) == "HA")
				{
					dblDependentTemp = dblDepARate * intWeeks;
					dblBasicRetTemp = dblMTDWithheld - dblDependentTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "HB")
				{
					dblDependentTemp = dblDepBRate * intWeeks;
					dblBasicRetTemp = dblMTDWithheld - dblDependentTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S1")
				{
					dblSupplementalTemp = dblMTDWithheld / 2;
					dblBasicRetTemp = dblMTDWithheld - dblSupplementalTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S2")
				{
					dblBasicRetTemp = dblMTDWithheld / 3;
					dblSupplementalTemp = dblMTDWithheld - dblBasicRetTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "S3")
				{
					dblBasicRetTemp = dblMTDWithheld / 4;
					dblSupplementalTemp = dblMTDWithheld - dblBasicRetTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F1A")
				{
					dblDependentTemp = dblDepARate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicRetTemp = dblTemp / 2;
					dblSupplementalTemp = dblTemp - dblBasicRetTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F2A")
				{
					dblDependentTemp = dblDepARate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicRetTemp = dblTemp / 3;
					dblSupplementalTemp = dblTemp - dblBasicRetTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F3A")
				{
					dblDependentTemp = dblDepARate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicRetTemp = dblTemp / 4;
					dblSupplementalTemp = dblTemp - dblBasicRetTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F1B")
				{
					dblDependentTemp = dblDepBRate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicRetTemp = dblTemp / 2;
					dblSupplementalTemp = dblTemp - dblBasicRetTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F2B")
				{
					dblDependentTemp = dblDepBRate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicRetTemp = dblTemp / 3;
					dblSupplementalTemp = dblTemp - dblBasicRetTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "F3B")
				{
					dblDependentTemp = dblDepBRate * intWeeks;
					dblTemp = dblMTDWithheld - dblDependentTemp;
					dblBasicRetTemp = dblTemp / 4;
					dblSupplementalTemp = dblTemp - dblBasicRetTemp;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "B")
				{
					dblBasicRetTemp = dblMTDWithheld;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "I")
				{
					dblBasicRetTemp = dblMTDWithheld;
				}
				else if (fecherFoundation.Strings.UCase(strMSRSCode) == "R")
				{
					// Matthew 11/23/2004 Need to deal with this code
				}
				else
				{
					dblBasicRetTemp = dblMTDWithheld;
					MessageBox.Show("Unknown MainePERS Code " + strMSRSCode);
				}
				dblDependent += dblDependentTemp;
				dblBasicRetirees += dblBasicRetTemp;
				dblSupplemental += dblSupplementalTemp;
				clsPremium.MoveNext();
			}
			clsPremium.Dispose();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
		}

		
	}
}
