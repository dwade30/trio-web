//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2TF.
	/// </summary>
	public partial class rptW2TF : FCSectionReport
	{
		public rptW2TF()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "W2 Laser";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2TF InstancePtr
		{
			get
			{
				return (rptW2TF)Sys.GetInstance(typeof(rptW2TF));
			}
		}

		protected rptW2TF _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2TF	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsW2Master = new clsDRWrapper();
		clsDRWrapper rsW2Deductions = new clsDRWrapper();
		clsDRWrapper rsW2Matches = new clsDRWrapper();
		clsDRWrapper rsBox10 = new clsDRWrapper();
		double dblLaserLineAdjustment;
		object ControlName;
		clsDRWrapper rsbox12 = new clsDRWrapper();
		clsDRWrapper rsBox14 = new clsDRWrapper();
		bool boolThirdParty;
		// vbPorter upgrade warning: intArrayCounter As int	OnWriteFCConvert.ToInt32(
		int intArrayCounter;
		// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
		int intloop;
		private bool boolPrintTest;

		private struct BoxEntries
		{
			public string Code;
			public double Amount;
			public bool ThirdParty;
		};

		private BoxEntries[] Box1214 = null;
		private double dblHorizAdjust;
		int lngYearToUse;

		public void Init(bool boolTestPrint = false, double dblAdjustment = 0, double dblHorizAdjustment = 0)
		{
			// SHOW THE REPORT INSIDE OF THE VIEWER AND ONCE IT ID
			// CLOSED OUT THEN THE W3 REPORT WILL BE SHOWN.
			dblLaserLineAdjustment = dblAdjustment;
			dblHorizAdjust = dblHorizAdjustment;
			boolPrintTest = boolTestPrint;
			//if (this.Document.Printer.PrintDialog) {
			//modPrinterFunctions.PrintXsForAlignment("The X should be at the top of the form", 10, 1, this.Document.Printer.PrinterName);
			frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName, 1, boolAllowEmail: false);
			if (!boolPrintTest)
			{
				rptW3Laser.InstancePtr.Init();
				if (lngYearToUse > 0)
				{
					frmW3ME.InstancePtr.Init(lngYearToUse, false);
				}
			}
			//} else {
			//	this.Close();
			//}
			// frmReportViewer.Init Me, , 1
			// 
			// If Not boolPrintTest Then
			// rptW3Regular.Init
			// End If
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool boolFound;
			boolThirdParty = false;
			if (!boolPrintTest)
			{
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				else
				{
					if (rsW2Master.EndOfFile())
					{
					}
					else
					{
						txtFederalEIN.Text = rsW2Master.Get_Fields_String("EmployersFederalID");
						txtEmployersName.Text = rsW2Master.Get_Fields_String("EmployersName") + "\r";
						txtEmployersName.Text = txtEmployersName + rsW2Master.Get_Fields_String("Address1") + "\r";
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Master.Get_Fields_String("Address2"))) != string.Empty)
						{
							txtEmployersName.Text = txtEmployersName + rsW2Master.Get_Fields_String("Address2") + "\r";
						}
						txtEmployersName.Text = txtEmployersName + rsW2Master.Get_Fields_String("City") + ", " + rsW2Master.Get_Fields("State") + " " + rsW2Master.Get_Fields("Zip");
						txtLocalityName.Text = rsW2Master.Get_Fields_String("LocalityName");
						txtState.Text = rsW2Master.Get_Fields_String("StateCode");
						txtEmployersStateEIN.Text = rsW2Master.Get_Fields_String("EmployersStateID");
					}
					double dblTemp = 0;
					txtSSN.Text = rsData.Get_Fields_String("SSN");
					// txtEmployeesName = rsData.Fields("EmployeeName") & Chr(13) & Chr(13) & Chr(13) & Chr(13)
					// txtEmployeesName = txtEmployeesName & rsData.Fields("Address") & Chr(13)
					// txtEmployeesName = txtEmployeesName & rsData.Fields("CityStateZip")
					txtEmployeesName.Text = rsData.Get_Fields_String("Address") + "\r\n";
					txtEmployeesName.Text = txtEmployeesName + rsData.Get_Fields_String("CityStateZip");
					txtFirstAndMiddle.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields("firstname") + " " + Strings.Left(rsData.Get_Fields("middlename") + " ", 1));
					txtLastName.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields("lastname") + " " + rsData.Get_Fields("desig"));
					// SET THE WAGES AND TAXES
					txtTotalWages.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FederalWage"));
					txtFederalTax.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FederalTax"));
					txtFICAWages.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FICAWage"));
					txtFICATax.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("FICATax"));
					txtMedWages.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("MedicareWage"));
					txtMedTaxes.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("MedicareTax"));
					// PER RON AND KPORT 12/08/2004
					txtStateWages.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("StateWage"));
					txtStateTax.Text = modGlobalRoutines.CheckForBlank(rsData.Get_Fields_String("StateTax"));
					txtPen.Visible = rsData.Get_Fields_Boolean("W2Retirement");
					txtStatutoryEmployee.Visible = rsData.Get_Fields_Boolean("W2StatutoryEmployee");
					// ********************************************************************
					// SAVE W3 INFORMATION
					modGlobalVariables.Statics.W3Information.dblTotalWages += Conversion.Val(txtTotalWages.Text);
					modGlobalVariables.Statics.W3Information.dblFederalTax += Conversion.Val(txtFederalTax.Text);
					modGlobalVariables.Statics.W3Information.dblFicaWages += Conversion.Val(txtFICAWages.Text);
					modGlobalVariables.Statics.W3Information.dblFicaTax += Conversion.Val(txtFICATax.Text);
					modGlobalVariables.Statics.W3Information.dblMedWages += Conversion.Val(txtMedWages.Text);
					modGlobalVariables.Statics.W3Information.dblMedTaxes += Conversion.Val(txtMedTaxes.Text);
					// PER RON AND KPORT 12/08/2004
					modGlobalVariables.Statics.W3Information.dblStateWages += Conversion.Val(txtStateWages.Text);
					modGlobalVariables.Statics.W3Information.dblStateTax += Conversion.Val(txtStateTax.Text);
					// ********************************************************************
					// SET THE BOX 12 AND BOX 14 INFORMATION
					txtOther.Text = string.Empty;
					txt12a.Text = string.Empty;
					txt12aAmount.Text = string.Empty;
					txt12b.Text = string.Empty;
					txt12bAmount.Text = string.Empty;
					txt12c.Text = string.Empty;
					txt12cAmount.Text = string.Empty;
					txt12d.Text = string.Empty;
					txt12dAmount.Text = string.Empty;
					// If rsData.Fields("EmployeeNumber") = "016" Then
					// Else
					// GoTo Done14
					// End If
					rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box12 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					rsW2Deductions.MoveLast();
					rsW2Deductions.MoveFirst();
					Box1214 = new BoxEntries[rsW2Deductions.RecordCount() + 1];
					for (intArrayCounter = 0; intArrayCounter <= (rsW2Deductions.RecordCount() - 1); intArrayCounter++)
					{
						Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
						Box1214[intArrayCounter].Amount = rsW2Deductions.Get_Fields("SumOfCYTDAmount");
						Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
						rsW2Deductions.MoveNext();
					}
					rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave, tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box12 = 1 and tblw2matches.employeenumber = '" + rsData.Get_Fields("employeenumber") + "' GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave, tblW2Matches.Code HAVING (((tblW2Matches.EmployeeNumber)='" + rsData.Get_Fields("EmployeeNumber") + "'))");
					rsW2Matches.MoveLast();
					rsW2Matches.MoveFirst();
					Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + rsW2Matches.RecordCount() + 1);
					// For intArrayCounter = 0 To rsW2Matches.RecordCount - 1
					// For intLoop = 0 To rsW2Deductions.RecordCount - 1
					// If Box1214(intLoop).Code = rsW2Matches.Fields("Code") Then
					// Box1214(intLoop).Amount = Box1214(intLoop).Amount + rsW2Matches.Fields("SumOfCYTDAmount")
					// End If
					// Next
					// 
					// rsW2Matches.MoveNext
					// Next
					while (!rsW2Matches.EndOfFile())
					{
						boolFound = false;
						for (intloop = 0; intloop <= (Information.UBound(Box1214, 1)); intloop++)
						{
							if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("Code")))
							{
								Box1214[intloop].Amount += rsW2Matches.Get_Fields("SumOfCYTDAmount");
								boolFound = true;
								break;
							}
							else if (Box1214[intloop].Code == "")
							{
								Box1214[intloop].Amount = rsW2Matches.Get_Fields("sumofcytdamount");
								Box1214[intloop].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
								Box1214[intloop].ThirdParty = FCConvert.ToBoolean(rsW2Matches.Get_Fields_Boolean("ThirdPartySave"));
								boolFound = true;
								break;
							}
						}
						// intloop
						rsW2Matches.MoveNext();
					}
					// now show box 12 info
					intCounter = 1;
					boolThirdParty = false;
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1) - 1); intArrayCounter++)
					{
						if (!boolThirdParty)
						{
							if (Box1214[intArrayCounter].ThirdParty)
							{
								boolThirdParty = true;
							}
						}
					}
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1) - 1); intArrayCounter++)
					{
						if (intCounter > 4)
							break;
						if (Box1214[intArrayCounter].Amount == 0)
							goto SkipEntry;
						if (Box1214[intArrayCounter].ThirdParty)
						{
							modGlobalVariables.Statics.W3Information.dblThirdPartySick = FCConvert.ToDouble(Strings.Format(Box1214[intArrayCounter].Amount, "0.00"));
						}
						if (intCounter == 1)
						{
							txt12a.Text = Box1214[intArrayCounter].Code;
							txt12aAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 2)
						{
							txt12b.Text = Box1214[intArrayCounter].Code;
							txt12bAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 3)
						{
							txt12c.Text = Box1214[intArrayCounter].Code;
							txt12cAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else if (intCounter == 4)
						{
							txt12d.Text = Box1214[intArrayCounter].Code;
							txt12dAmount.Text = Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						SkipEntry:
						;
					}
					// now fill box 14
					Box1214 = new BoxEntries[0 + 1];
					rsW2Deductions.OpenRecordset("SELECT tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber, Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box14 = 1 GROUP BY tblW2Deductions.ThirdPartySave, tblW2Deductions.Code, tblW2Deductions.EmployeeNumber HAVING tblW2Deductions.Code <> '' AND tblW2Deductions.EmployeeNumber= '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					rsW2Deductions.MoveLast();
					rsW2Deductions.MoveFirst();
					Box1214 = new BoxEntries[rsW2Deductions.RecordCount() + 1 + 1];
					for (intArrayCounter = 0; intArrayCounter <= (rsW2Deductions.RecordCount() - 1); intArrayCounter++)
					{
						Box1214[intArrayCounter].Code = FCConvert.ToString(rsW2Deductions.Get_Fields("Code"));
						Box1214[intArrayCounter].Amount = rsW2Deductions.Get_Fields("SumOfCYTDAmount");
						Box1214[intArrayCounter].ThirdParty = FCConvert.ToBoolean(rsW2Deductions.Get_Fields_Boolean("ThirdPartySave"));
						rsW2Deductions.MoveNext();
					}
					rsW2Matches.OpenRecordset("SELECT tblW2Matches.EmployeeNumber, Sum(tblW2Matches.CYTDAmount) AS SumOfCYTDAmount, tblW2Matches.ThirdPartySave,tblW2Matches.Code FROM tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box14 = 1 GROUP BY tblW2Matches.EmployeeNumber, tblW2Matches.ThirdPartySave,tblW2Matches.Code HAVING (((tblW2Matches.EmployeeNumber)='" + rsData.Get_Fields("EmployeeNumber") + "'))", "TWPY0000.VB1");
					rsW2Matches.MoveLast();
					rsW2Matches.MoveFirst();
					Array.Resize(ref Box1214, Information.UBound(Box1214, 1) + rsW2Matches.RecordCount() + 1);
					// For intArrayCounter = 0 To rsW2Matches.RecordCount - 1
					// For intloop = 0 To rsW2Deductions.RecordCount - 1
					// If Box1214(intloop).Code = rsW2Matches.Fields("Code") Then
					// Box1214(intloop).Amount = Box1214(intloop).Amount + rsW2Matches.Fields("SumOfCYTDAmount")
					// End If
					// Next
					// 
					// rsW2Matches.MoveNext
					// Next
					while (!rsW2Matches.EndOfFile())
					{
						boolFound = false;
						for (intloop = 0; intloop <= (Information.UBound(Box1214, 1)); intloop++)
						{
							if (Box1214[intloop].Code == FCConvert.ToString(rsW2Matches.Get_Fields("Code")))
							{
								Box1214[intloop].Amount += rsW2Matches.Get_Fields("SumOfCYTDAmount");
								boolFound = true;
								break;
							}
							else if (Box1214[intloop].Code == "")
							{
								Box1214[intloop].Amount = rsW2Matches.Get_Fields("sumofcytdamount");
								Box1214[intloop].Code = FCConvert.ToString(rsW2Matches.Get_Fields("code"));
								Box1214[intloop].ThirdParty = FCConvert.ToBoolean(rsW2Matches.Get_Fields_Boolean("ThirdPartySave"));
								boolFound = true;
								break;
							}
						}
						// intloop
						rsW2Matches.MoveNext();
					}
					intCounter = 1;
					boolThirdParty = false;
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1) - 1); intArrayCounter++)
					{
						if (!boolThirdParty)
						{
							if (Box1214[intArrayCounter].ThirdParty)
							{
								boolThirdParty = true;
							}
						}
					}
					for (intArrayCounter = 0; intArrayCounter <= (Information.UBound(Box1214, 1) - 1); intArrayCounter++)
					{
						if (intCounter > 4)
							break;
						if (Box1214[intArrayCounter].Amount == 0)
							goto SkipMatchEntry;
						if (Box1214[intArrayCounter].ThirdParty)
						{
							modGlobalVariables.Statics.W3Information.dblThirdPartySick = FCConvert.ToDouble(Strings.Format(Box1214[intArrayCounter].Amount, "0.00"));
						}
						if (intCounter == 1)
						{
							txtOther.Text = Box1214[intArrayCounter].Code + " ";
							txtOther.Text = txtOther.Text + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						else
						{
							txtOther.Text = txtOther.Text + "\r" + Box1214[intArrayCounter].Code + " ";
							txtOther.Text = txtOther.Text + Strings.Format(Box1214[intArrayCounter].Amount, "0.00");
							intCounter += 1;
						}
						SkipMatchEntry:
						;
					}
					Done14:
					;
					// NOW FILL BOX 10
					dblTemp = 0;
					rsBox10.OpenRecordset("SELECT Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2Deductions.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					if (!rsBox10.EndOfFile())
					{
						dblTemp = Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
					}
					rsBox10.OpenRecordset("SELECT Sum(tblW2matches.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2matches ON tblW2Box12And14.DeductionNumber = tblW2matches.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2matches.EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "'", "TWPY0000.vb1");
					if (!rsBox10.EndOfFile())
					{
						dblTemp += Conversion.Val(rsBox10.Get_Fields("sumofcytdamount"));
					}
					txtDependentCare.Text = Strings.Format(dblTemp, "0.00");
					rsData.MoveNext();
					eArgs.EOF = false;
				}
				txtThirdParty.Visible = boolThirdParty;
			}
			else
			{
				txt12a.Text = "X";
				txt12aAmount.Text = "999.99";
				txt12b.Text = "X";
				txt12bAmount.Text = "999.99";
				txt12c.Text = "X";
				txt12cAmount.Text = "999.99";
				txt12d.Text = "X";
				txt12dAmount.Text = "999.99";
				txtDependentCare.Text = "999.99";
				txtEmployeesName.Text = "Address" + "\r\n" + "City, ST Zip";
				txtFirstAndMiddle.Text = "First Middle";
				txtLastName.Text = "Last";
				txtEmployersName.Text = "Employer";
				txtEmployersStateEIN.Text = "XXXX";
				txtFederalEIN.Text = "XXXX";
				txtFederalTax.Text = "999.99";
				txtFICATax.Text = "999.99";
				txtFICAWages.Text = "999.99";
				txtLocalityName.Text = "XXX";
				txtMedTaxes.Text = "999.99";
				txtMedWages.Text = "999.99";
				txtSSN.Text = "999-99-9999";
				txtState.Text = "99";
				txtStateTax.Text = "999.99";
				txtStateWages.Text = "999.99";
				txtTotalWages.Text = "999.99";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			string strReportOrder = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_printtoolid = 9950;
			//for(cnt=0; cnt<=this.Toolbar.Tools.Count-1; cnt++) {
			//	if ("Print..."==this.Toolbar.Tools(cnt).Caption) {
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//} // cnt
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			//this.Printer.RenderMode = 1;
			if (!boolPrintTest)
			{
				dblLaserLineAdjustment = modGlobalRoutines.SetW2LineAdjustment(this);
				dblHorizAdjust = FCConvert.ToDouble(modGlobalRoutines.SetW2HorizAdjustment(this));
			}
            else
            {
                foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
                {
                    ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment) / 1440F;
                    ControlName.Left += FCConvert.ToSingle(dblHorizAdjust) / 1440F;
                }
            }
            dblLaserLineAdjustment += 1;
			if (!boolPrintTest)
			{
				rsData.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
				if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 0)
				{
					// employee name
					strReportOrder = "LastName,FirstName,MiddleName";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 1)
				{
					// employee number
					strReportOrder = "EmployeeNumber";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 2)
				{
					strReportOrder = "seqnumber,lastname,firstname";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int32("ReportSequence")) == 3)
				{
					strReportOrder = "deptdiv,lastname,firstname";
				}
				if (FCConvert.ToDouble(modGlobalVariables.Statics.gstrPrintAllW2s) == 0)
				{
					rsData.OpenRecordset("Select * from tblW2EditTable order by " + strReportOrder, "TWPY0000.vb1");
				}
				else
				{
					rsData.OpenRecordset("Select * from tblW2EditTable Where EmployeeNumber = '" + modGlobalVariables.Statics.gstrPrintAllW2s + "' order by " + strReportOrder, "TWPY0000.vb1");
				}
				if (!rsData.EndOfFile())
				{
					lngYearToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(rsData.Get_Fields("Year"))));
				}
				rsW2Master.OpenRecordset("Select * from tblW2Master", "TWPY0000.vb1");
				rsW2Deductions.OpenRecordset("Select * from tblW2Deductions where Code <> ''", "TWPY0000.vb1");
				// Call rsW2Matches.OpenRecordset("Select * from tblW2Matches where Code <> ''", "TWPY0000.vb1")
				// Call rsW2Matches.OpenRecordset("SELECT tblW2Matches.* From tblW2Matches, tblW2Deductions WHERE tblW2Matches.DeductionNumber<>[tblW2Deductions].[DeductionNumber] AND tblW2Matches.Code <> ''", "TWPY0000.vb1")
				rsbox12.OpenRecordset("Select * from tblW2Box12AND14 Where Box12 = 1");
				rsBox14.OpenRecordset("Select * from tblW2Box12AND14 Where Box14 = 1");
			}
		}

		
		private void Detail_Format(object sender, EventArgs e)
		{
			//foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			//{
			//	ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment) / 1440F;
			//	ControlName.Left += FCConvert.ToSingle(dblHorizAdjust) / 1440F;
			//}
		}

		private void rptW2TF_Load(object sender, System.EventArgs e)
		{
			
		}
	}
}
