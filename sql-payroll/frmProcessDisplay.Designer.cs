//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmProcessDisplay.
	/// </summary>
	partial class frmProcessDisplay
	{
		public fecherFoundation.FCButton cmdConfirmation;
		public fecherFoundation.FCCheckBox chkNext;
		public fecherFoundation.FCProgressBar pbrStatus;
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCLabel lblStatus;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintZeroNet;
		public fecherFoundation.FCToolStripMenuItem mnuDistributionListing;
		public fecherFoundation.FCToolStripMenuItem mnuPrintDistributionbyPayCategory;
		public fecherFoundation.FCToolStripMenuItem mnuDetailReport;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdConfirmation = new fecherFoundation.FCButton();
            this.chkNext = new fecherFoundation.FCCheckBox();
            this.pbrStatus = new fecherFoundation.FCProgressBar();
            this.vsGrid = new fecherFoundation.FCGrid();
            this.lblStatus = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintZeroNet = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDistributionListing = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintDistributionbyPayCategory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDetailReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrintSummary = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdConfirmation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintSummary);
            this.BottomPanel.Location = new System.Drawing.Point(0, 603);
            this.BottomPanel.Size = new System.Drawing.Size(730, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdConfirmation);
            this.ClientArea.Controls.Add(this.chkNext);
            this.ClientArea.Controls.Add(this.pbrStatus);
            this.ClientArea.Controls.Add(this.vsGrid);
            this.ClientArea.Controls.Add(this.lblStatus);
            this.ClientArea.Size = new System.Drawing.Size(750, 606);
            this.ClientArea.Controls.SetChildIndex(this.lblStatus, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.pbrStatus, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkNext, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdConfirmation, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(750, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(250, 28);
            this.HeaderText.Text = "Data Edit & Calculation";
            // 
            // cmdConfirmation
            // 
            this.cmdConfirmation.AppearanceKey = "actionButton";
            this.cmdConfirmation.Location = new System.Drawing.Point(337, 20);
            this.cmdConfirmation.Name = "cmdConfirmation";
            this.cmdConfirmation.Size = new System.Drawing.Size(148, 40);
            this.cmdConfirmation.TabIndex = 3;
            this.cmdConfirmation.Text = "Confirmation";
            this.cmdConfirmation.Visible = false;
            this.cmdConfirmation.Click += new System.EventHandler(this.cmdConfirmation_Click);
            // 
            // chkNext
            // 
            this.chkNext.Location = new System.Drawing.Point(30, 25);
            this.chkNext.Name = "chkNext";
            this.chkNext.Size = new System.Drawing.Size(237, 22);
            this.chkNext.TabIndex = 2;
            this.chkNext.Text = "Stop process after each Employee?";
            this.chkNext.Visible = false;
            // 
            // pbrStatus
            // 
            this.pbrStatus.Location = new System.Drawing.Point(30, 45);
            this.pbrStatus.Name = "pbrStatus";
            this.pbrStatus.Size = new System.Drawing.Size(675, 17);
            this.pbrStatus.TabIndex = 4;
            // 
            // vsGrid
            // 
            this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsGrid.Cols = 10;
            this.vsGrid.Location = new System.Drawing.Point(30, 72);
            this.vsGrid.Name = "vsGrid";
            this.vsGrid.Rows = 50;
            this.vsGrid.Size = new System.Drawing.Size(691, 531);
            this.vsGrid.TabIndex = 4;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(30, 30);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(675, 15);
            this.lblStatus.TabIndex = 1;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcess,
            this.mnuSP1,
            this.mnuPrintZeroNet,
            this.mnuDistributionListing,
            this.mnuPrintDistributionbyPayCategory,
            this.mnuDetailReport});
            this.MainMenu1.Name = null;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 0;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "Process                    ";
            this.mnuProcess.Visible = false;
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            this.mnuSP1.Visible = false;
            // 
            // mnuPrintZeroNet
            // 
            this.mnuPrintZeroNet.Index = 2;
            this.mnuPrintZeroNet.Name = "mnuPrintZeroNet";
            this.mnuPrintZeroNet.Text = "Print Zero Net Summary Report";
            this.mnuPrintZeroNet.Click += new System.EventHandler(this.mnuPrintZeroNet_Click);
            // 
            // mnuDistributionListing
            // 
            this.mnuDistributionListing.Index = 3;
            this.mnuDistributionListing.Name = "mnuDistributionListing";
            this.mnuDistributionListing.Text = "Print Distribution Listing";
            this.mnuDistributionListing.Click += new System.EventHandler(this.mnuDistributionListing_Click);
            // 
            // mnuPrintDistributionbyPayCategory
            // 
            this.mnuPrintDistributionbyPayCategory.Index = 4;
            this.mnuPrintDistributionbyPayCategory.Name = "mnuPrintDistributionbyPayCategory";
            this.mnuPrintDistributionbyPayCategory.Text = "Print Distribution Listing by Pay Category";
            this.mnuPrintDistributionbyPayCategory.Click += new System.EventHandler(this.mnuPrintDistributionbyPayCategory_Click);
            // 
            // mnuDetailReport
            // 
            this.mnuDetailReport.Index = 5;
            this.mnuDetailReport.Name = "mnuDetailReport";
            this.mnuDetailReport.Text = "Print Detail Report";
            this.mnuDetailReport.Click += new System.EventHandler(this.mnuDetailReport_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = -1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print Summary Report       ";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrintSummary
            // 
            this.cmdPrintSummary.AppearanceKey = "acceptButton";
            this.cmdPrintSummary.Location = new System.Drawing.Point(270, 30);
            this.cmdPrintSummary.Name = "cmdPrintSummary";
            this.cmdPrintSummary.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintSummary.Size = new System.Drawing.Size(218, 48);
            this.cmdPrintSummary.TabIndex = 0;
            this.cmdPrintSummary.Text = "Print Summary Report";
            this.cmdPrintSummary.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmProcessDisplay
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(750, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmProcessDisplay";
            this.Text = "Data Edit & Calculation";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmProcessDisplay_Load);
            this.Activated += new System.EventHandler(this.frmProcessDisplay_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmProcessDisplay_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdConfirmation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSummary)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPrintSummary;
	}
}