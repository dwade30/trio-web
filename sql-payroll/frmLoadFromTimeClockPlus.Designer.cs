//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmLoadFromTimeClockPlus.
	/// </summary>
	partial class frmLoadFromTimeClockPlus
	{
		public fecherFoundation.FCTextBox txtImport;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtImport = new fecherFoundation.FCTextBox();
            this.cmdBrowse = new fecherFoundation.FCButton();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 260);
            this.BottomPanel.Size = new System.Drawing.Size(486, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.txtImport);
            this.ClientArea.Controls.Add(this.cmdBrowse);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(486, 200);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(486, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            // 
            // txtImport
            // 
            this.txtImport.AutoSize = false;
            this.txtImport.BackColor = System.Drawing.SystemColors.Window;
            this.txtImport.LinkItem = null;
            this.txtImport.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtImport.LinkTopic = null;
            this.txtImport.Location = new System.Drawing.Point(30, 59);
            this.txtImport.Name = "txtImport";
            this.txtImport.Size = new System.Drawing.Size(308, 40);
            this.txtImport.TabIndex = 1;
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.AppearanceKey = "actionButton";
            this.cmdBrowse.Location = new System.Drawing.Point(357, 59);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(100, 40);
            this.cmdBrowse.TabIndex = 0;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(106, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "IMPORT FILE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Process File";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 119);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(138, 48);
            this.cmdProcess.TabIndex = 3;
            this.cmdProcess.Text = "Process File";
            this.cmdProcess.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmLoadFromTimeClockPlus
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(486, 260);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmLoadFromTimeClockPlus";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Import From Time Clock";
            this.Load += new System.EventHandler(this.frmLoadFromTimeClockPlus_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLoadFromTimeClockPlus_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}