namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDeductionSetup.
	/// </summary>
	partial class rptDeductionSetup
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDeductionSetup));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrequencyCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmountType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLimit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLimitCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxStatusCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcctDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPriority = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrequencyCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimitCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxStatusCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcctDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPriority)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtLine,
            this.txtDescription,
            this.txtFrequencyCode,
            this.txtAmount,
            this.txtAmountType,
            this.txtLimit,
            this.txtLimitCode,
            this.txtTaxStatusCode,
            this.txtAccount,
            this.txtAcctDescription,
            this.txtPriority,
				this.txtStatus
			});
            this.Detail.Height = 0.2083333F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCaption,
				this.txtMuniName,
				this.txtCaption2,
				this.txtDate,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Line1,
				this.Label8,
				this.txtTime,
				this.lblPage,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12
			});
			this.PageHeader.Height = 1.208333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtCaption
            // 
            this.txtCaption.Height = 0.1666667F;
            this.txtCaption.Left = 0.0625F;
            this.txtCaption.Name = "txtCaption";
            this.txtCaption.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
            this.txtCaption.Text = "Account";
            this.txtCaption.Top = 0.08333334F;
            this.txtCaption.Width = 9.875F;
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.1666667F;
            this.txtMuniName.Left = 0.0625F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-size: 8pt; ddo-char-set: 1";
            this.txtMuniName.Text = "MuniName";
            this.txtMuniName.Top = 0.08333334F;
            this.txtMuniName.Width = 1.625F;
            // 
            // txtCaption2
            // 
            this.txtCaption2.Height = 0.2083333F;
            this.txtCaption2.Left = 0.0625F;
            this.txtCaption2.Name = "txtCaption2";
            this.txtCaption2.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
            this.txtCaption2.Text = null;
            this.txtCaption2.Top = 0.25F;
            this.txtCaption2.Width = 9.875F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1666667F;
            this.txtDate.Left = 8.875F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.txtDate.Text = "Date";
            this.txtDate.Top = 0.08333334F;
            this.txtDate.Width = 1.0625F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.3125F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label1.Text = "Description";
            this.Label1.Top = 0.8125F;
            this.Label1.Width = 0.8125F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2916667F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 2.0625F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label2.Text = "Freq Code";
            this.Label2.Top = 0.7083333F;
            this.Label2.Width = 0.3125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1666667F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 2.90625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
            this.Label3.Text = "Amount";
            this.Label3.Top = 0.8333333F;
            this.Label3.Width = 0.59375F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.3416668F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 3.59375F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label4.Text = "Amt Type";
            this.Label4.Top = 0.7083333F;
            this.Label4.Width = 0.375F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1666667F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 5F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
            this.Label5.Text = "Limit";
            this.Label5.Top = 0.8333333F;
            this.Label5.Width = 0.6875F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.3416668F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 4.09375F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label6.Text = "Limit Code";
            this.Label6.Top = 0.7083333F;
            this.Label6.Width = 0.53125F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.3416668F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 2.4375F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label7.Text = "Tax Code";
            this.Label7.Top = 0.7083333F;
            this.Label7.Width = 0.40625F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 3F;
            this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
            this.Line1.Width = 10F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 10F;
            this.Line1.Y1 = 1.05F;
            this.Line1.Y2 = 1.05F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label8.Text = "#";
            this.Label8.Top = 0.8125F;
            this.Label8.Width = 0.1875F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.Left = 0.0625F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Courier\'; font-size: 8pt";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.25F;
            this.txtTime.Width = 1.375F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2083333F;
            this.lblPage.Left = 8.875F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.lblPage.Text = "Date";
            this.lblPage.Top = 0.25F;
            this.lblPage.Width = 1.0625F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1666667F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 5.71875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
            this.Label9.Text = "Account #";
            this.Label9.Top = 0.8333333F;
            this.Label9.Width = 0.8125F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2166668F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 6.5625F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
            this.Label10.Text = "Description";
            this.Label10.Top = 0.8333333F;
            this.Label10.Width = 1.1875F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1666667F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 8.8125F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
            this.Label11.Text = "Status";
            this.Label11.Top = 0.8333333F;
            this.Label11.Width = 0.625F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2166668F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 9.46875F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
            this.Label12.Text = "Priority";
            this.Label12.Top = 0.8333333F;
            this.Label12.Width = 0.531F;
            // 
			// txtLine
			// 
			this.txtLine.Height = 0.1875F;
			this.txtLine.Left = 0F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtLine.Text = null;
			this.txtLine.Top = 0F;
			this.txtLine.Width = 0.25F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 0.3125F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 1.6875F;
			// 
			// txtFrequencyCode
			// 
			this.txtFrequencyCode.Height = 0.1875F;
			this.txtFrequencyCode.Left = 2.0625F;
			this.txtFrequencyCode.Name = "txtFrequencyCode";
			this.txtFrequencyCode.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtFrequencyCode.Text = null;
			this.txtFrequencyCode.Top = 0F;
			this.txtFrequencyCode.Width = 0.25F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.2083333F;
			this.txtAmount.Left = 2.875F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.65625F;
			// 
			// txtAmountType
			// 
			this.txtAmountType.Height = 0.2083333F;
			this.txtAmountType.Left = 3.59375F;
			this.txtAmountType.Name = "txtAmountType";
			this.txtAmountType.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtAmountType.Text = null;
			this.txtAmountType.Top = 0F;
			this.txtAmountType.Width = 0.3125F;
			// 
			// txtLimit
			// 
			this.txtLimit.CanGrow = false;
			this.txtLimit.Height = 0.2083333F;
			this.txtLimit.Left = 4.90625F;
			this.txtLimit.Name = "txtLimit";
			this.txtLimit.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtLimit.Text = null;
			this.txtLimit.Top = 0F;
			this.txtLimit.Width = 0.75F;
			// 
			// txtLimitCode
			// 
			this.txtLimitCode.Height = 0.2083333F;
			this.txtLimitCode.Left = 4.09375F;
			this.txtLimitCode.Name = "txtLimitCode";
			this.txtLimitCode.Style = "font-size: 8pt; ddo-char-set: 1";
			this.txtLimitCode.Text = null;
			this.txtLimitCode.Top = 0F;
			this.txtLimitCode.Width = 0.71875F;
			// 
			// txtTaxStatusCode
			// 
			this.txtTaxStatusCode.Height = 0.2083333F;
			this.txtTaxStatusCode.Left = 2.4375F;
			this.txtTaxStatusCode.Name = "txtTaxStatusCode";
			this.txtTaxStatusCode.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.txtTaxStatusCode.Text = null;
			this.txtTaxStatusCode.Top = 0F;
			this.txtTaxStatusCode.Width = 0.375F;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.2083333F;
			this.txtAccount.Left = 5.71875F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.8125F;
			// 
			// txtAcctDescription
			// 
			this.txtAcctDescription.CanGrow = false;
			this.txtAcctDescription.Height = 0.2083333F;
			this.txtAcctDescription.Left = 6.5625F;
			this.txtAcctDescription.Name = "txtAcctDescription";
			this.txtAcctDescription.Style = "font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.txtAcctDescription.Text = null;
			this.txtAcctDescription.Top = 0F;
			this.txtAcctDescription.Width = 2.25F;
			// 
			// txtPriority
			// 
			this.txtPriority.CanGrow = false;
			this.txtPriority.Height = 0.2083333F;
			this.txtPriority.Left = 9.4375F;
			this.txtPriority.Name = "txtPriority";
			this.txtPriority.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtPriority.Text = null;
			this.txtPriority.Top = 0F;
			this.txtPriority.Width = 0.5F;
			// 
			// txtStatus
			// 
			this.txtStatus.CanGrow = false;
			this.txtStatus.Height = 0.2083333F;
			this.txtStatus.Left = 8.84375F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Style = "font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.txtStatus.Text = null;
			this.txtStatus.Top = 0F;
			this.txtStatus.Width = 0.5625F;
			// 
			// rptDeductionSetup
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrequencyCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLimitCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxStatusCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcctDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPriority)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrequencyCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLimit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLimitCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxStatusCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcctDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPriority;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}