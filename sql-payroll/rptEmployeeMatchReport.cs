//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptEmployeeMatchReport.
	/// </summary>
	public partial class rptEmployeeMatchReport : BaseSectionReport
	{
		public rptEmployeeMatchReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Employee Match Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
			vsData = new FCGrid();
			vsData.Rows = 1;
			vsData.Cols = 6;
			vsSummary = new FCGrid();
			vsSummary.Rows = 2;
			vsSummary.Cols = 6;
			vsHeader = new FCGrid();
			vsHeader.Rows = 3;
			vsHeader.Cols = 6;
		}

		public static rptEmployeeMatchReport InstancePtr
		{
			get
			{
				return (rptEmployeeMatchReport)Sys.GetInstance(typeof(rptEmployeeMatchReport));
			}
		}

		protected rptEmployeeMatchReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
                rsData = null;
				vsData?.Dispose();
				vsHeader?.Dispose();
				vsSummary?.Dispose();
                vsData = null;
                vsHeader = null;
                vsSummary = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEmployeeMatchReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rs = new clsDRWrapper();
		int intRow;
		int intCol;
		int intPageNumber;
		bool boolDoubleHeader;
		string strEmployeeNumber = "";
		double dblTotal;
		int intCounter;
		double dblTotalPayTotal;
		double dblFederalTotal;
		double dblFICATotal;
		double dblMedicareTotal;
		double dblStateTotal;
		double dblLocalTotal;
		double dblDeductsTotal;
		double dblNetTotal;
		int intEmployeeTotal;
		int intSetTotals;
		int intDeductionCode;
		bool boolFirstTime;
		string strDescription = "";
		float intDetailHeight;
		float intGridHeight;
		bool boolPrint;
		FCGrid vsData;
		FCGrid vsSummary;
		FCGrid vsHeader;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// On Error Resume Next
			double dblTemp = 0;
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			NextRecord:
			;
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			boolPrint = true;
			this.Detail.Visible = true;
			if (intDeductionCode != FCConvert.ToInt32(rsData.Get_Fields_Int32("DeductionCode")))
			{
				intDeductionCode = FCConvert.ToInt16(rsData.Get_Fields_Int32("DeductionCode"));
				strDescription = FCConvert.ToString(intDeductionCode) + " - " + rsData.Get_Fields("tblDeductionSetup.Description");
				this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields_Int32("DeductionCode"));
				this.Detail.Visible = false;
				boolPrint = false;
				eArgs.EOF = false;
				return;
			}
			// ADD THE DATA TO THE CORRECT CELL IN THE GRID
			strEmployeeNumber = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("tblEmployeeMaster.EmployeeNumber")));
			vsData.TextMatrix(0, 0, fecherFoundation.Strings.Trim(strEmployeeNumber));
			vsData.TextMatrix(0, 1, fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LastName"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("FirstName"))));
			intEmployeeTotal += 1;
			intDeductionCode = FCConvert.ToInt16(rsData.Get_Fields_Int32("DeductionCode"));
			strDescription = FCConvert.ToString(intDeductionCode) + " - " + rsData.Get_Fields("tblDeductionSetup.Description");
			dblTemp = modCoreysSweeterCode.GetCurrentMatchTotal(intDeductionCode, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 2, Strings.Format(dblTemp, "0.00"));
			dblTotalPayTotal += dblTemp;
			dblTemp = modCoreysSweeterCode.GetMTDMatchTotal(intDeductionCode, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 3, Strings.Format(dblTemp, "0.00"));
			dblFederalTotal += dblTemp;
			dblTemp = modCoreysSweeterCode.GetFYTDMatchTotal(intDeductionCode, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 4, Strings.Format(dblTemp, "0.00"));
			dblMedicareTotal += dblTemp;
			dblTemp = modCoreysSweeterCode.GetCYTDMatchTotal(intDeductionCode, strEmployeeNumber, modGlobalVariables.Statics.gdatCurrentPayDate);
			vsData.TextMatrix(0, 5, Strings.Format(dblTemp, "0.00"));
			dblStateTotal += dblTemp;
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page #" + FCConvert.ToString(intPageNumber);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			intSetTotals = 0;
			intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy") + "  " + Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtTitle.Text = modCustomReport.Statics.strCustomTitle;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGlobalVariables.DEFAULTDATABASE);
			intRow = 0;
			boolDoubleHeader = false;
			vsHeader.TextMatrix(1, 0, "");
			vsHeader.TextMatrix(1, 1, "");
			vsHeader.TextMatrix(1, 2, "");
			vsHeader.TextMatrix(1, 3, "");
			vsHeader.TextMatrix(1, 4, "   Year To Date");
			vsHeader.TextMatrix(1, 5, "   Year To Date");
			vsHeader.TextMatrix(2, 0, "Employee");
			vsHeader.ColWidth(0, 700);
			vsHeader.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(2, 1, "Employee");
			vsHeader.ColWidth(1, 2000);
			vsHeader.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsHeader.TextMatrix(2, 2, "Current");
			vsHeader.ColWidth(2, 800);
			vsHeader.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(2, 3, "- MTD -");
			vsHeader.ColWidth(3, 800);
			vsHeader.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(2, 4, "Fiscal");
			vsHeader.ColWidth(4, 800);
			vsHeader.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.TextMatrix(2, 5, "Calendar");
			vsHeader.ColWidth(5, 800);
			vsHeader.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsHeader.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsHeader.MergeRow(0, true);
			vsHeader.MergeRow(1, true);
			vsHeader.MergeRow(2, true);
			vsData.ColWidth(0, vsHeader.ColWidth(0));
			vsData.ColWidth(1, vsHeader.ColWidth(1));
			vsData.ColWidth(2, vsHeader.ColWidth(2));
			vsData.ColWidth(3, vsHeader.ColWidth(3));
			vsData.ColWidth(4, vsHeader.ColWidth(4));
			vsData.ColWidth(5, vsHeader.ColWidth(5));
			vsSummary.ColWidth(0, vsHeader.ColWidth(0));
			vsSummary.ColWidth(1, vsHeader.ColWidth(1));
			vsSummary.ColWidth(2, vsHeader.ColWidth(2));
			vsSummary.ColWidth(3, vsHeader.ColWidth(3));
			vsSummary.ColWidth(4, vsHeader.ColWidth(4));
			vsSummary.ColWidth(5, vsHeader.ColWidth(5));
			vsData.ColAlignment(0, vsHeader.ColAlignment(0));
			vsData.ColAlignment(1, vsHeader.ColAlignment(1));
			vsData.ColAlignment(2, vsHeader.ColAlignment(2));
			vsData.ColAlignment(3, vsHeader.ColAlignment(3));
			vsData.ColAlignment(4, vsHeader.ColAlignment(4));
			vsData.ColAlignment(5, vsHeader.ColAlignment(5));
			vsSummary.ColAlignment(0, vsHeader.ColAlignment(0));
			vsSummary.ColAlignment(1, vsHeader.ColAlignment(1));
			vsSummary.ColAlignment(2, vsHeader.ColAlignment(2));
			vsSummary.ColAlignment(3, vsHeader.ColAlignment(3));
			vsSummary.ColAlignment(4, vsHeader.ColAlignment(4));
			vsSummary.ColAlignment(5, vsHeader.ColAlignment(5));
			// vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsData.Rows - 1, intNumberOfSQLFields) = flexAlignLeftCenter
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			// ADD A LINE SEPERATOR BETWEEN THE CAPTIONS AND THE DATA THAT
			// IS DISPLAYED
			Line1.X1 = vsHeader.Left;
			Line1.X2 = this.PrintWidth;
			Line1.Y1 = vsHeader.Top / 1440F + vsHeader.Height / 1440F + 20 / 1440F;
			Line1.Y2 = vsHeader.Top / 1440F + vsHeader.Height / 1440F + 20 / 1440F;
			GroupHeader1.Height += 100 / 1440F;
			Detail.Height = vsData.Height / 1440F + 30 / 1440F;
			boolFirstTime = true;
			intDetailHeight = this.Detail.Height;
			intGridHeight = this.vsData.Height / 1440F;
			if (!rsData.EndOfFile())
			{
				this.Fields["grpHeader"].Value = FCConvert.ToString(rsData.Get_Fields_Int32("DeductionCode"));
				intDeductionCode = FCConvert.ToInt16(rsData.Get_Fields_Int32("DeductionCode"));
			}
			intRow = 0;
			modPrintToFile.SetPrintProperties(this);
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolPrint)
			{
				this.Detail.Height = intDetailHeight;
				this.vsData.Height = FCConvert.ToInt32(intGridHeight * 1440F);
			}
			else
			{
				// Me.vsData.Height = 0
				// Me.Detail.Height = 0
			}
			// If rsData.EndOfFile Then
			// 
			// Else
			// If boolFirstTime Then
			// boolFirstTime = False
			// Else
			this.Fields["grpHeader"].Value = intDeductionCode.ToString();
			// End If
			// End If
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			vsSummary.TextMatrix(0, 0, "Final");
			vsSummary.TextMatrix(1, 0, "Totals");
			vsSummary.TextMatrix(1, 1, "Number Employees: " + FCConvert.ToString(intEmployeeTotal));
			vsSummary.TextMatrix(1, 2, Strings.Format(dblTotalPayTotal, "0.00"));
			vsSummary.TextMatrix(1, 3, Strings.Format(dblFederalTotal, "0.00"));
			vsSummary.TextMatrix(1, 4, Strings.Format(dblMedicareTotal, "0.00"));
			vsSummary.TextMatrix(1, 5, Strings.Format(dblStateTotal, "0.00"));
			intEmployeeTotal = 0;
			dblTotalPayTotal = 0;
			dblFederalTotal = 0;
			dblFICATotal = 0;
			dblMedicareTotal = 0;
			dblStateTotal = 0;
			dblLocalTotal = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			vsHeader.TextMatrix(0, 0, strDescription);
			vsHeader.TextMatrix(0, 1, strDescription);
			vsHeader.TextMatrix(0, 2, strDescription);
			vsHeader.TextMatrix(0, 3, strDescription);
			vsHeader.TextMatrix(0, 4, strDescription);
			vsHeader.TextMatrix(0, 5, strDescription);
			vsHeader.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void rptEmployeeMatchReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptEmployeeMatchReport properties;
			//rptEmployeeMatchReport.Caption	= "Employee Match Report";
			//rptEmployeeMatchReport.Icon	= "rptEmployeeMatchReport.dsx":0000";
			//rptEmployeeMatchReport.Left	= 0;
			//rptEmployeeMatchReport.Top	= 0;
			//rptEmployeeMatchReport.Width	= 11880;
			//rptEmployeeMatchReport.Height	= 8595;
			//rptEmployeeMatchReport.StartUpPosition	= 3;
			//rptEmployeeMatchReport.SectionData	= "rptEmployeeMatchReport.dsx":058A;
			//End Unmaped Properties
		}

		private void ActiveReports_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
