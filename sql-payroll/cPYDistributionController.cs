//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWPY0000
{
	public class cPYDistributionController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public cGenericCollection GetDistributionsByEmployeeNumber(string strEmployeeNumber)
		{
			cGenericCollection GetDistributionsByEmployeeNumber = null;
			ClearErrors();
			var rsLoad = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();

                cGenericCollection listDist = new cGenericCollection();
                cPayrollDistribution distRec;
                rsLoad.OpenRecordset(
                    "Select * from tblPayrollDistribution where employeenumber = '" + strEmployeeNumber +
                    "' order by recordnumber", "Payroll");
                while (!rsLoad.EndOfFile())
                {
                    //App.DoEvents();
                    distRec = new cPayrollDistribution();
                    FillDistributionRecord(ref distRec, ref rsLoad);
                    listDist.AddItem(distRec);
                    rsLoad.MoveNext();
                }

                GetDistributionsByEmployeeNumber = listDist;
                return GetDistributionsByEmployeeNumber;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
            finally
            {
				rsLoad.Dispose();
                rsLoad = null;
            }
			return GetDistributionsByEmployeeNumber;
		}

		private void FillDistributionRecord(ref cPayrollDistribution distRec, ref clsDRWrapper rsLoad)
		{
			if (!(distRec == null))
			{
				distRec.AccountCode = rsLoad.Get_Fields("accountcode");
				distRec.AccountNumber = rsLoad.Get_Fields("AccountNumber");
				distRec.BaseRate = Conversion.Val(rsLoad.Get_Fields_Decimal("BaseRate"));
				distRec.Category = FCConvert.ToInt32(rsLoad.Get_Fields("Cat"));
				distRec.CD = FCConvert.ToInt32(rsLoad.Get_Fields("cd"));
				distRec.ContractID = FCConvert.ToInt32(rsLoad.Get_Fields("contractid"));
				distRec.DefaultHours = Conversion.Val(rsLoad.Get_Fields_Double("DefaultHours"));
				distRec.Deleted = false;
				distRec.DistU = rsLoad.Get_Fields("distu");
				distRec.EmployeeNumber = rsLoad.Get_Fields("EmployeeNumber");
				distRec.Factor = Conversion.Val(rsLoad.Get_Fields_Double("Factor"));
				distRec.GrantFunded = rsLoad.Get_Fields("grantfunded");
				distRec.Gross = Conversion.Val(rsLoad.Get_Fields_Decimal("Gross"));
				distRec.HoursWeek = Conversion.Val(rsLoad.Get_Fields_Double("HoursWeek"));
				distRec.ID = rsLoad.Get_Fields("ID");
				distRec.MSRS = rsLoad.Get_Fields("MSRS");
				distRec.MSRSID = rsLoad.Get_Fields_Int32("MSRSID");
				distRec.NumberWeeks = rsLoad.Get_Fields_Int32("NumberWeeks");
				distRec.Project = FCConvert.ToInt32(rsLoad.Get_Fields("Project"));
				distRec.RecordNumber = rsLoad.Get_Fields_Int32("RecordNumber");
				distRec.StatusCode = rsLoad.Get_Fields_String("StatusCode");
				distRec.TaxCode = rsLoad.Get_Fields_Int32("TaxCode");
				distRec.WC = rsLoad.Get_Fields_String("WC");
				distRec.WeeksTaxWithheld = rsLoad.Get_Fields_Int16("WeeksTaxWithheld");
				distRec.WorkComp = rsLoad.Get_Fields_String("WorkComp");
				distRec.IsUpdated = false;
			}
		}
	}
}
