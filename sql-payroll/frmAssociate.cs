//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmAssociate : BaseForm
	{
		public frmAssociate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmAssociate InstancePtr
		{
			get
			{
				return (frmAssociate)Sys.GetInstance(typeof(frmAssociate));
			}
		}

		protected frmAssociate _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: DAN C. SOLTESZ
		// DATE:       AUG 13, 2001
		//
		// NOTES:
		//
		//
		// **************************************************
		// private local variables
		private clsDRWrapper rsAss = new clsDRWrapper();
		private clsDRWrapper rsAss2 = new clsDRWrapper();
		private int intCounter;
		private int intDataChanged;
		private clsHistory clsHistoryClass = new clsHistory();
		string strTypeDescription = "";
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			int x;
			// we only want to save data that is checked so lets start fresh
			// Dave 12/14/2006--------------------------------------------
			// Set New Information so we can compare
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillNewValue(this);
			}
			// Thsi function compares old and new values and creates change records for any differences
			modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("Vacation / Sick / Other Codes", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
			// Reset all information pertianing to changes and start again
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			clsReportChanges.Reset();
			// Initialize all the control and old data values
			FillControlInformationClassFromListBox();
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillOldValue(this);
			}
			// ----------------------------------------------------------------
			rsAss2.Execute("Delete from tblAssociateCodes where CodeType = " + FCConvert.ToString(modGlobalVariables.Statics.gintVacationCodeType), "TWPY0000.vb1");
			for (intCounter = 0; intCounter <= lstAss.Items.Count - 1; intCounter++)
			{
				// item is selected so lets save it
				if (lstAss.Selected(intCounter) == true)
				{
					rsAss2.OpenRecordset("Select * from tblAssociateCodes where CodeType = " + FCConvert.ToString(modGlobalVariables.Statics.gintVacationCodeType), "TWPY0000.vb1");
					rsAss2.AddNew();
					rsAss2.SetData("Description", lstAss.Items[intCounter].Text);
					rsAss2.SetData("PayCatID", lstAss.ItemData(intCounter));
					rsAss2.SetData("CodeType", modGlobalVariables.Statics.gintVacationCodeType);
					rsAss2.Update();
				}
			}
			clsHistoryClass.Compare();
			MessageBox.Show("Codes Have Been Associated", "Payroll", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Close();
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdOK, new System.EventArgs());
		}

		private void frmAssociate_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// If FormExist(Me) Then Exit Sub
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmAssociate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmAssociate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmAssociate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAssociate properties;
			//frmAssociate.ScaleWidth	= 3885;
			//frmAssociate.ScaleHeight	= 2475;
			//frmAssociate.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int counter;
				// vsElasticLight1.Enabled = True
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 2);
				modGlobalFunctions.SetTRIOColors(this);
				LoadData();
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a listbox to add which list items you want to keep track of changes for
				FillControlInformationClassFromListBox();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
				clsHistoryClass.SetGridIDColumn("PY");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadData()
		{
			int intCount;
			rsAss.OpenRecordset("Select * from tblPayCategories", "TWPY0000.vb1");
			if (!rsAss.EndOfFile())
			{
				rsAss.MoveLast();
				rsAss.MoveFirst();
			}
			else
			{
				MessageBox.Show("No Pay Codes Setup");
			}
			intCount = 0;
			while (!rsAss.EndOfFile())
			{
				lstAss.AddItem(FCConvert.ToString(rsAss.Get_Fields("Description")));
				lstAss.ItemData(intCount, FCConvert.ToInt32(rsAss.Get_Fields("ID")));
				intCount += 1;
				rsAss.MoveNext();
			}
			// if there is selected data already then we want to show whats selected
			for (intCount = 0; intCount <= frmVacationCodes.InstancePtr.lstAss.Items.Count - 1; intCount++)
			{
				for (intCounter = 0; intCounter <= lstAss.Items.Count - 1; intCounter++)
				{
					if (lstAss.Items[intCounter].Text == frmVacationCodes.InstancePtr.lstAss.Items[intCount].Text)
					{
						lstAss.SetSelected(intCounter, true);
						break;
					}
				}
			}
			clsHistoryClass.Init = this;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdOK_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdOK_Click();
			cmdCancel_Click();
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromListBox()
		{
			int intItems;
			clsDRWrapper rsTypeInfo = new clsDRWrapper();
			rsTypeInfo.OpenRecordset("SELECT * FROM tblCodeTypes WHERE ID = " + FCConvert.ToString(modGlobalVariables.Statics.gintVacationCodeType));
			if (rsTypeInfo.EndOfFile() != true && rsTypeInfo.BeginningOfFile() != true)
			{
				strTypeDescription = FCConvert.ToString(rsTypeInfo.Get_Fields("Description"));
			}
			else
			{
				strTypeDescription = "UNKNOWN";
			}
			for (intItems = 0; intItems <= lstAss.Items.Count - 1; intItems++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                clsControlInfo[intTotalNumberOfControls].ControlName = "lstAss";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ListBox;
				clsControlInfo[intTotalNumberOfControls].ListIndex = intItems;
				clsControlInfo[intTotalNumberOfControls].DataDescription = strTypeDescription + " Associated Pay Categories - " + lstAss.Items[intItems].Text;
				intTotalNumberOfControls += 1;
			}
		}
	}
}
