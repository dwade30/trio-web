//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmCustomRetirementDeductions : BaseForm
	{
		public frmCustomRetirementDeductions()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomRetirementDeductions InstancePtr
		{
			get
			{
				return (frmCustomRetirementDeductions)Sys.GetInstance(typeof(frmCustomRetirementDeductions));
			}
		}

		protected frmCustomRetirementDeductions _InstancePtr = null;
		//=========================================================
		private void frmCustomRetirementDeductions_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmCustomRetirementDeductions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomRetirementDeductions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomRetirementDeductions properties;
			//frmCustomRetirementDeductions.ScaleWidth	= 9030;
			//frmCustomRetirementDeductions.ScaleHeight	= 6945;
			//frmCustomRetirementDeductions.LinkTopic	= "Form1";
			//End Unmaped Properties
			// vsElasticLight1.Enabled = True
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			vsData.TextMatrix(0, 0, "Emp #");
			vsData.TextMatrix(0, 1, "SSN");
			vsData.TextMatrix(0, 2, "Employee Name");
			vsData.TextMatrix(0, 3, "Ded #1");
			vsData.TextMatrix(0, 4, "Ded #2");
			vsData.TextMatrix(0, 5, "Ded #3");
			vsData.TextMatrix(0, 6, "Match #1");
			vsData.TextMatrix(0, 7, "Hours");
			vsData.TextMatrix(0, 8, "Salary");
			vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, 8, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 8, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, 1, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 7, 1, 8, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 7, 1, 8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsData.ColWidth(0, 0);
			// 1000
			vsData.ColWidth(1, 0);
			// 1400
			vsData.ColWidth(2, 0);
			// 2000
			vsData.ColWidth(3, 3000);
			// 1200
			vsData.ColWidth(4, 3000);
			// 1200
			vsData.ColWidth(5, 3000);
			// 1200
			vsData.ColWidth(6, 3000);
			// 1200
			vsData.ColWidth(7, 0);
			// 1200
			vsData.ColWidth(8, 0);
			// 1200
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblDeductionSetup", "TWPY0000.vb1");
			while (!rsData.EndOfFile())
			{
				vsData.ColComboList(3, vsData.ColComboList(3) + "#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")).Length, " ") + rsData.Get_Fields("Description") + "|");
				vsData.ColComboList(4, vsData.ColComboList(4) + "#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")).Length, " ") + rsData.Get_Fields("Description") + "|");
				vsData.ColComboList(5, vsData.ColComboList(5) + "#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")).Length, " ") + rsData.Get_Fields("Description") + "|");
				vsData.ColComboList(6, vsData.ColComboList(6) + "#" + rsData.Get_Fields("ID") + ";" + rsData.Get_Fields_Int32("DeductionNumber") + Strings.StrDup(5 - FCConvert.ToString(rsData.Get_Fields_Int32("DeductionNumber")).Length, " ") + rsData.Get_Fields("Description") + "|");
				rsData.MoveNext();
			}
            //FC:FINAL:AM:#4190 - remove the last "|" from all combolists
			vsData.ColComboList(3, Strings.Left(vsData.ColComboList(4), vsData.ColComboList(4).Length - 1));
            vsData.ColComboList(4, Strings.Left(vsData.ColComboList(4), vsData.ColComboList(4).Length - 1));
            vsData.ColComboList(5, Strings.Left(vsData.ColComboList(4), vsData.ColComboList(4).Length - 1));
            vsData.ColComboList(6, Strings.Left(vsData.ColComboList(4), vsData.ColComboList(4).Length - 1));
            vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(GetSQL()))
			{
				frmReportViewer.InstancePtr.Init(rptRetirementDeductionReport.InstancePtr, boolAllowEmail: false);
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private object GetSQL()
		{
			object GetSQL = null;
			string strWhere;
			int intQuarter;
			vsData.Select(0, 0);
			modGlobalVariables.Statics.gstrCheckListingSQL = "SELECT distinct EmployeeNumber from tblCheckDetail WHERE tblCheckDetail.CheckNumber <> 0 AND tblCheckDetail.EmployeeNumber <> '' ";
			GetSQL = false;
			if (cmbReportOn.Text == "Single Employee")
			{
				if (txtEmployeeNumber.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeNumber.Focus();
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeNumber.Text + "'";
					modGlobalVariables.Statics.gstrCheckListingWhere = " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeNumber.Text + "'";
				}
			}
			else if (cmbReportOn.Text == "Date Range")
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Start Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("End Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
					modGlobalVariables.Statics.gstrCheckListingWhere = " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
				}
			}
			else if (cmbReportOn.Text == "All Files")
			{
			}
			else if (cmbReportOn.Text == "Emp/Date Range")
			{
				if (txtEmployeeBoth.Text == string.Empty)
				{
					MessageBox.Show("Employee Number must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEmployeeBoth.Focus();
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
					modGlobalVariables.Statics.gstrCheckListingWhere = " AND tblCheckDetail.EmployeeNumber = '" + txtEmployeeBoth.Text + "'";
				}
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Start Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return GetSQL;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("End Date Month must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEndDate.Focus();
					return GetSQL;
				}
				else
				{
					modGlobalVariables.Statics.gstrCheckListingSQL += " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
					modGlobalVariables.Statics.gstrCheckListingWhere = " AND tblCheckDetail.PayDate >= '" + txtStartDate.Text + "' AND tblCheckDetail.PayDate <= '" + txtEndDate.Text + "' ";
				}
			}
			else
			{
			}
			strWhere = string.Empty;
			if (vsData.TextMatrix(1, 3) == string.Empty)
			{
			}
			else
			{
				strWhere = "DedDeductionNumber = " + vsData.TextMatrix(1, 3);
			}
			if (vsData.TextMatrix(1, 4) == string.Empty)
			{
			}
			else
			{
				strWhere += " OR DedDeductionNumber = " + vsData.TextMatrix(1, 4);
			}
			if (vsData.TextMatrix(1, 5) == string.Empty)
			{
			}
			else
			{
				strWhere += " OR dedDeductionNumber = " + vsData.TextMatrix(1, 5);
			}
			if (vsData.TextMatrix(1, 6) == string.Empty)
			{
			}
			else
			{
				strWhere += " OR MatchDeductionNumber = " + vsData.TextMatrix(1, 6);
			}
			if (Strings.Left(strWhere, 3) == " OR")
			{
				strWhere = Strings.Mid(strWhere, 5, strWhere.Length - 4);
			}
			if (strWhere != string.Empty)
				strWhere = " AND (" + strWhere + ")";
			modGlobalVariables.Statics.gstrCheckListingSQL += strWhere + " Order by EmployeeNumber";
			GetSQL = true;
			return GetSQL;
		}

		private void optReportOn_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			txtEmployeeNumber.Visible = (Index == 0 || Index == 1 || Index == 2 || Index == 3);
			txtStartDate.Visible = (Index == 4 || Index == 6);
			txtEndDate.Visible = (Index == 4 || Index == 6);
			lblCaption.Visible = Index != 5;
			// lblDash.Visible = Index = 4
			txtEmployeeNumber.Text = string.Empty;
			lblEmployeeBoth.Visible = Index == 6;
			txtEmployeeBoth.Visible = Index == 6;
			switch (Index)
			{
				case 0:
					{
						lblCaption.Text = "Employee Number";
						break;
					}
				case 1:
					{
						lblCaption.Text = "Month of Pay Date";
						break;
					}
				case 2:
					{
						lblCaption.Text = "Qtr. of Pay Date";
						break;
					}
				case 3:
					{
						lblCaption.Text = "Year of Pay Date";
						break;
					}
				case 4:
				case 6:
					{
						lblCaption.Text = "Pay Date Range";
						break;
					}
				case 5:
					{
						break;
					}
			}
			//end switch
		}

		private void optReportOn_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:DSE:#i2231 Set VB6 index
			//int index = cmbReportOn.SelectedIndex;
			int index = -1;
			switch(cmbReportOn.Text)
			{
				case "Single Employee":
					index = 0;
					break;
				case "Emp/Date Range":
					index = 6;
					break;
				case "All Files":
					index = 5;
					break;
				case "Date Range":
					index = 4;
					break;
			}
			optReportOn_CheckedChanged(index, sender, e);
		}

        //FC:FINAL:AM:#i2245 - changed date controls to T2KDateBox
        //private void txtEndDate_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtEndDate.Text.Length == 2 || txtEndDate.Text.Length == 5) && Strings.Right(txtEndDate.Text, 1) != "/")
        //	{
        //		txtEndDate.Text = txtEndDate.Text + "/";
        //		txtEndDate.SelectionStart = txtEndDate.Text.Length;
        //		txtEndDate.SelectionLength = txtEndDate.Text.Length;
        //	}
        //}

        //private void txtEndDate_Enter(object sender, System.EventArgs e)
        //{
        //	txtEndDate.SelectionStart = 0;
        //	txtEndDate.SelectionLength = txtEndDate.Text.Length;
        //}

        //private void txtStartDate_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtStartDate.Text.Length == 2 || txtStartDate.Text.Length == 5) && Strings.Right(txtStartDate.Text, 1) != "/")
        //	{
        //		txtStartDate.Text = txtStartDate.Text + "/";
        //		txtStartDate.SelectionStart = txtStartDate.Text.Length;
        //		txtStartDate.SelectionLength = txtStartDate.Text.Length;
        //	}
        //}

        //private void txtStartDate_Enter(object sender, System.EventArgs e)
        //{
        //	txtStartDate.SelectionStart = 0;
        //	txtStartDate.SelectionLength = txtStartDate.Text.Length;
        //}

        private void vsData_RowColChange(object sender, System.EventArgs e)
		{
			if (vsData.Row == 1)
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

        private void cmdSaveContinue_Click(object sender, EventArgs e)
        {
            mnuSaveContinue_Click(cmdSaveContinue, EventArgs.Empty);
        }
    }
}
