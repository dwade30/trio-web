//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class modMultiPay
	{
		//=========================================================
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, int)
		public static object MultiPayCaption(int intCaption)
		{
			object MultiPayCaption = null;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblMultiPay where ParentID = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID + "'", "Twpy0000.vb1");
			if (!rsData.EndOfFile())
			{
				rsData.FindFirstRecord("ChildID", intCaption);
				MultiPayCaption = rsData.Get_Fields_Int32("ChildEmployeeID");
				Statics.gintNumberChildren = rsData.RecordCount();
			}
			else
			{
				MultiPayCaption = 0;
			}
			return MultiPayCaption;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmEmployeePayTotals, frmEmployersMatch, frmVacationSick, frmEmployeeDeductions, frmPayrollDistribution)
		public static void SetMultiPayCaptions(dynamic FormName)
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			FormName.mnuCaption1.Visible = false;
			FormName.mnuCaption2.Visible = false;
			FormName.mnuCaption3.Visible = false;
			FormName.mnuCaption4.Visible = false;
			FormName.mnuCaption5.Visible = false;
			FormName.mnuCaption6.Visible = false;
			FormName.mnuCaption7.Visible = false;
			FormName.mnuCaption8.Visible = false;
			FormName.mnuCaption9.Visible = false;
			rsData.OpenRecordset("Select * from tblMultiPay where ParentID = '" + modGlobalVariables.Statics.gtypeCurrentEmployee.ParentID + "' order by ChildID");
			if (!rsData.EndOfFile())
			{
				if (rsData.RecordCount() >= 1)
				{
					FormName.mnuCaption1.Visible = true;
					FormName.mnuCaption1.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
				if (rsData.RecordCount() >= 2)
				{
					FormName.mnuCaption2.Visible = true;
					FormName.mnuCaption2.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
				if (rsData.RecordCount() >= 3)
				{
					FormName.mnuCaption3.Visible = true;
					FormName.mnuCaption3.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
				if (rsData.RecordCount() >= 4)
				{
					FormName.mnuCaption4.Visible = true;
					FormName.mnuCaption4.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
				if (rsData.RecordCount() >= 5)
				{
					FormName.mnuCaption5.Visible = true;
					FormName.mnuCaption5.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
				if (rsData.RecordCount() >= 6)
				{
					FormName.mnuCaption6.Visible = true;
					FormName.mnuCaption6.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
				if (rsData.RecordCount() >= 7)
				{
					FormName.mnuCaption7.Visible = true;
					FormName.mnuCaption7.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
				if (rsData.RecordCount() >= 8)
				{
					FormName.mnuCaption8.Visible = true;
					FormName.mnuCaption8.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
				if (rsData.RecordCount() >= 9)
				{
					FormName.mnuCaption9.Visible = true;
					FormName.mnuCaption9.Text = rsData.Get_Fields_String("ChildID") + ". " + rsData.Get_Fields_String("Caption");
					rsData.MoveNext();
				}
			}
		}

		public class StaticVariables
		{
			// Public gtypeCurrentEmployee.ParentID As Integer
			// vbPorter upgrade warning: gintNumberChildren As int	OnWriteFCConvert.ToInt32(
			public int gintNumberChildren;
			public string gstrEmployeeCaption = string.Empty;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
