﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptCurrEmployeeVacSick.
	/// </summary>
	partial class rptCurrEmployeeVacSick
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCurrEmployeeVacSick));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSecondTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPER = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedCurrentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedFiscalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccruedCalendarTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedCurrentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedFiscalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedCalendarTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBalanceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPER)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedFiscalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCalendarTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedFiscalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCalendarTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalanceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployee,
				this.txtCode,
				this.txtRate,
				this.txtAccruedCurrent,
				this.txtAccruedFiscal,
				this.txtAccruedCalendar,
				this.txtUsedCurrent,
				this.txtUsedFiscal,
				this.txtUsedCalendar,
				this.txtBalance,
				this.txtUM,
				this.txtPER
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccruedCurrentTotal,
				this.txtAccruedFiscalTotal,
				this.txtAccruedCalendarTotal,
				this.txtUsedCurrentTotal,
				this.txtUsedFiscalTotal,
				this.txtUsedCalendarTotal,
				this.txtBalanceTotal,
				this.txtEmployeeTotal,
				this.Label22,
				this.Line1
			});
			this.ReportFooter.Height = 0.3541667F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.Label20,
				this.Label9,
				this.Label2,
				this.Label3,
				this.Label4,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.txtTime,
				this.Label12,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.lblBalance,
				this.Label21,
				this.txtSecondTitle,
				this.Label13
			});
			this.PageHeader.Height = 1.041667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label27,
				this.txtDeptDiv
			});
			this.GroupHeader1.Height = 0.3645833F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Visible = false;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1875F;
			this.txtTitle.HyperLink = null;
			this.txtTitle.Left = 1.5625F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = null;
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 4.9375F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.2083333F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 3.6875F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label20.Text = "------------ Accrued  ------------";
			this.Label20.Top = 0.625F;
			this.Label20.Width = 1.8125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label9.Text = "Employee ------------------";
			this.Label9.Top = 0.8125F;
			this.Label9.Width = 1.375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.9375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label2.Text = "Code";
			this.Label2.Top = 0.8125F;
			this.Label2.Visible = false;
			this.Label2.Width = 0.375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.9375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label3.Text = "Rate";
			this.Label3.Top = 0.8125F;
			this.Label3.Width = 0.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label4.Text = "U/M";
			this.Label4.Top = 0.8125F;
			this.Label4.Width = 0.375F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.125F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.6875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.5F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.5F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.125F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.4375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.96875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label12.Text = "PER";
			this.Label12.Top = 0.8333333F;
			this.Label12.Width = 0.5625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.2083333F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.21875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label14.Text = "Fiscal";
			this.Label14.Top = 0.7916667F;
			this.Label14.Width = 0.71875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.5F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label15.Text = "Current";
			this.Label15.Top = 0.8125F;
			this.Label15.Width = 0.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 6.1875F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label16.Text = "Fiscal";
			this.Label16.Top = 0.8125F;
			this.Label16.Width = 0.5F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.9375F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label17.Text = "Calendar";
			this.Label17.Top = 0.8125F;
			this.Label17.Width = 0.5625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 6.6875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label18.Text = "Calendar";
			this.Label18.Top = 0.8125F;
			this.Label18.Width = 0.625F;
			// 
			// lblBalance
			// 
			this.lblBalance.Height = 0.1875F;
			this.lblBalance.HyperLink = null;
			this.lblBalance.Left = 7.3125F;
			this.lblBalance.Name = "lblBalance";
			this.lblBalance.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblBalance.Text = "Balance";
			this.lblBalance.Top = 0.8125F;
			this.lblBalance.Width = 0.625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 5.5F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label21.Text = "-------------- Used --------------";
			this.Label21.Top = 0.625F;
			this.Label21.Width = 1.8125F;
			// 
			// txtSecondTitle
			// 
			this.txtSecondTitle.Height = 0.1875F;
			this.txtSecondTitle.Left = 2.4375F;
			this.txtSecondTitle.Name = "txtSecondTitle";
			this.txtSecondTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtSecondTitle.Text = null;
			this.txtSecondTitle.Top = 0.25F;
			this.txtSecondTitle.Width = 3.125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.2083333F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label13.Text = "Current";
			this.Label13.Top = 0.7916667F;
			this.Label13.Width = 0.5625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.125F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label27.Text = "Dept./Div";
			this.Label27.Top = 0.125F;
			this.Label27.Width = 0.875F;
			// 
			// txtDeptDiv
			// 
			this.txtDeptDiv.Height = 0.1875F;
			this.txtDeptDiv.Left = 1.0625F;
			this.txtDeptDiv.Name = "txtDeptDiv";
			this.txtDeptDiv.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.txtDeptDiv.Text = null;
			this.txtDeptDiv.Top = 0.125F;
			this.txtDeptDiv.Width = 2.3125F;
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.1875F;
			this.txtEmployee.Left = 0.125F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-size: 9pt";
			this.txtEmployee.Text = null;
			this.txtEmployee.Top = 0F;
			this.txtEmployee.Width = 1.75F;
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.1875F;
			this.txtCode.Left = 1.9375F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "font-size: 9pt; text-align: right";
			this.txtCode.Text = null;
			this.txtCode.Top = 0F;
			this.txtCode.Visible = false;
			this.txtCode.Width = 0.375F;
			// 
			// txtRate
			// 
			this.txtRate.Height = 0.1875F;
			this.txtRate.Left = 1.9375F;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "font-size: 9pt; text-align: right";
			this.txtRate.Text = null;
			this.txtRate.Top = 0F;
			this.txtRate.Width = 0.5625F;
			// 
			// txtAccruedCurrent
			// 
			this.txtAccruedCurrent.Height = 0.2083333F;
			this.txtAccruedCurrent.Left = 3.625F;
			this.txtAccruedCurrent.Name = "txtAccruedCurrent";
			this.txtAccruedCurrent.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedCurrent.Text = null;
			this.txtAccruedCurrent.Top = 0F;
			this.txtAccruedCurrent.Width = 0.5625F;
			// 
			// txtAccruedFiscal
			// 
			this.txtAccruedFiscal.Height = 0.2083333F;
			this.txtAccruedFiscal.Left = 4.21875F;
			this.txtAccruedFiscal.Name = "txtAccruedFiscal";
			this.txtAccruedFiscal.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedFiscal.Text = null;
			this.txtAccruedFiscal.Top = 0F;
			this.txtAccruedFiscal.Width = 0.71875F;
			// 
			// txtAccruedCalendar
			// 
			this.txtAccruedCalendar.Height = 0.1875F;
			this.txtAccruedCalendar.Left = 4.9375F;
			this.txtAccruedCalendar.Name = "txtAccruedCalendar";
			this.txtAccruedCalendar.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedCalendar.Text = null;
			this.txtAccruedCalendar.Top = 0F;
			this.txtAccruedCalendar.Width = 0.5625F;
			// 
			// txtUsedCurrent
			// 
			this.txtUsedCurrent.Height = 0.1875F;
			this.txtUsedCurrent.Left = 5.5F;
			this.txtUsedCurrent.Name = "txtUsedCurrent";
			this.txtUsedCurrent.Style = "font-size: 9pt; text-align: right";
			this.txtUsedCurrent.Text = null;
			this.txtUsedCurrent.Top = 0F;
			this.txtUsedCurrent.Width = 0.5625F;
			// 
			// txtUsedFiscal
			// 
			this.txtUsedFiscal.Height = 0.1875F;
			this.txtUsedFiscal.Left = 6.0625F;
			this.txtUsedFiscal.Name = "txtUsedFiscal";
			this.txtUsedFiscal.Style = "font-size: 9pt; text-align: right";
			this.txtUsedFiscal.Text = null;
			this.txtUsedFiscal.Top = 0F;
			this.txtUsedFiscal.Width = 0.625F;
			// 
			// txtUsedCalendar
			// 
			this.txtUsedCalendar.Height = 0.1875F;
			this.txtUsedCalendar.Left = 6.6875F;
			this.txtUsedCalendar.Name = "txtUsedCalendar";
			this.txtUsedCalendar.Style = "font-size: 9pt; text-align: right";
			this.txtUsedCalendar.Text = null;
			this.txtUsedCalendar.Top = 0F;
			this.txtUsedCalendar.Width = 0.625F;
			// 
			// txtBalance
			// 
			this.txtBalance.Height = 0.1875F;
			this.txtBalance.Left = 7.3125F;
			this.txtBalance.Name = "txtBalance";
			this.txtBalance.Style = "font-size: 9pt; text-align: right";
			this.txtBalance.Text = null;
			this.txtBalance.Top = 0F;
			this.txtBalance.Width = 0.625F;
			// 
			// txtUM
			// 
			this.txtUM.Height = 0.2083333F;
			this.txtUM.Left = 2.5F;
			this.txtUM.Name = "txtUM";
			this.txtUM.Style = "font-size: 9pt; text-align: center";
			this.txtUM.Text = null;
			this.txtUM.Top = 0F;
			this.txtUM.Width = 0.375F;
			// 
			// txtPER
			// 
			this.txtPER.Height = 0.2083333F;
			this.txtPER.Left = 2.875F;
			this.txtPER.Name = "txtPER";
			this.txtPER.Style = "font-size: 9pt; text-align: center";
			this.txtPER.Text = null;
			this.txtPER.Top = 0F;
			this.txtPER.Width = 0.65625F;
			// 
			// txtAccruedCurrentTotal
			// 
			this.txtAccruedCurrentTotal.Height = 0.2083333F;
			this.txtAccruedCurrentTotal.Left = 3.65625F;
			this.txtAccruedCurrentTotal.Name = "txtAccruedCurrentTotal";
			this.txtAccruedCurrentTotal.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedCurrentTotal.Text = null;
			this.txtAccruedCurrentTotal.Top = 0.125F;
			this.txtAccruedCurrentTotal.Width = 0.5625F;
			// 
			// txtAccruedFiscalTotal
			// 
			this.txtAccruedFiscalTotal.Height = 0.2083333F;
			this.txtAccruedFiscalTotal.Left = 4.21875F;
			this.txtAccruedFiscalTotal.Name = "txtAccruedFiscalTotal";
			this.txtAccruedFiscalTotal.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedFiscalTotal.Text = null;
			this.txtAccruedFiscalTotal.Top = 0.125F;
			this.txtAccruedFiscalTotal.Width = 0.71875F;
			// 
			// txtAccruedCalendarTotal
			// 
			this.txtAccruedCalendarTotal.Height = 0.1875F;
			this.txtAccruedCalendarTotal.Left = 4.9375F;
			this.txtAccruedCalendarTotal.Name = "txtAccruedCalendarTotal";
			this.txtAccruedCalendarTotal.Style = "font-size: 9pt; text-align: right";
			this.txtAccruedCalendarTotal.Text = null;
			this.txtAccruedCalendarTotal.Top = 0.125F;
			this.txtAccruedCalendarTotal.Width = 0.5625F;
			// 
			// txtUsedCurrentTotal
			// 
			this.txtUsedCurrentTotal.Height = 0.1875F;
			this.txtUsedCurrentTotal.Left = 5.5F;
			this.txtUsedCurrentTotal.Name = "txtUsedCurrentTotal";
			this.txtUsedCurrentTotal.Style = "font-size: 9pt; text-align: right";
			this.txtUsedCurrentTotal.Text = null;
			this.txtUsedCurrentTotal.Top = 0.125F;
			this.txtUsedCurrentTotal.Width = 0.5625F;
			// 
			// txtUsedFiscalTotal
			// 
			this.txtUsedFiscalTotal.Height = 0.1875F;
			this.txtUsedFiscalTotal.Left = 6.0625F;
			this.txtUsedFiscalTotal.Name = "txtUsedFiscalTotal";
			this.txtUsedFiscalTotal.Style = "font-size: 9pt; text-align: right";
			this.txtUsedFiscalTotal.Text = null;
			this.txtUsedFiscalTotal.Top = 0.125F;
			this.txtUsedFiscalTotal.Width = 0.625F;
			// 
			// txtUsedCalendarTotal
			// 
			this.txtUsedCalendarTotal.Height = 0.1875F;
			this.txtUsedCalendarTotal.Left = 6.6875F;
			this.txtUsedCalendarTotal.Name = "txtUsedCalendarTotal";
			this.txtUsedCalendarTotal.Style = "font-size: 9pt; text-align: right";
			this.txtUsedCalendarTotal.Text = null;
			this.txtUsedCalendarTotal.Top = 0.125F;
			this.txtUsedCalendarTotal.Width = 0.625F;
			// 
			// txtBalanceTotal
			// 
			this.txtBalanceTotal.Height = 0.1875F;
			this.txtBalanceTotal.Left = 7.3125F;
			this.txtBalanceTotal.Name = "txtBalanceTotal";
			this.txtBalanceTotal.Style = "font-size: 9pt; text-align: right";
			this.txtBalanceTotal.Text = null;
			this.txtBalanceTotal.Top = 0.125F;
			this.txtBalanceTotal.Width = 0.625F;
			// 
			// txtEmployeeTotal
			// 
			this.txtEmployeeTotal.Height = 0.1875F;
			this.txtEmployeeTotal.Left = 1.9375F;
			this.txtEmployeeTotal.Name = "txtEmployeeTotal";
			this.txtEmployeeTotal.Style = "font-size: 9pt; text-align: right";
			this.txtEmployeeTotal.Text = null;
			this.txtEmployeeTotal.Top = 0.125F;
			this.txtEmployeeTotal.Width = 0.375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label22.Text = "Totals:           Employees";
			this.Label22.Top = 0.125F;
			this.Label22.Width = 1.8125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.125F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 7.8125F;
			this.Line1.X1 = 0.125F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// rptCurrEmployeeVacSick
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPER)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCurrentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedFiscalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccruedCalendarTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCurrentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedFiscalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedCalendarTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBalanceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUM;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPER;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedCurrentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedFiscalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccruedCalendarTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedCurrentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedFiscalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedCalendarTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBalanceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
