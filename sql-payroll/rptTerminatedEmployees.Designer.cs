namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTerminatedEmployees.
	/// </summary>
	partial class rptTerminatedEmployees
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTerminatedEmployees));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.chkDeductions = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkMatches = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkPayTotals = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkTerminate = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMatches)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPayTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTerminate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtNumber,
				this.txtName,
				this.chkDeductions,
				this.chkMatches,
				this.chkPayTotals,
				this.chkTerminate
			});
			this.Detail.Height = 0.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtCaption,
				this.txtDate,
				this.lblAddress1,
				this.Label2,
				this.Line1,
				this.lblAddress2,
				this.lblAddress3,
				this.Label6,
				this.lblPage,
				this.txtTime,
				this.Label7,
				this.Label8
			});
			this.PageHeader.Height = 1.5625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1875F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.0625F;
			this.txtCaption.Width = 7.8125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.75F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.0625F;
			// 
			// lblAddress1
			// 
			this.lblAddress1.Height = 0.1875F;
			this.lblAddress1.HyperLink = null;
			this.lblAddress1.Left = 4.1875F;
			this.lblAddress1.Name = "lblAddress1";
			this.lblAddress1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.lblAddress1.Text = "Deductions";
			this.lblAddress1.Top = 1.375F;
			this.lblAddress1.Visible = false;
			this.lblAddress1.Width = 0.8125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label2.Text = "Number";
			this.Label2.Top = 1.375F;
			this.Label2.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.5625F;
			this.Line1.Width = 7.9375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 1.5625F;
			this.Line1.Y2 = 1.5625F;
			// 
			// lblAddress2
			// 
			this.lblAddress2.Height = 0.1875F;
			this.lblAddress2.HyperLink = null;
			this.lblAddress2.Left = 5F;
			this.lblAddress2.Name = "lblAddress2";
			this.lblAddress2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.lblAddress2.Text = "Matches";
			this.lblAddress2.Top = 1.375F;
			this.lblAddress2.Visible = false;
			this.lblAddress2.Width = 0.8125F;
			// 
			// lblAddress3
			// 
			this.lblAddress3.Height = 0.1875F;
			this.lblAddress3.HyperLink = null;
			this.lblAddress3.Left = 5.8125F;
			this.lblAddress3.Name = "lblAddress3";
			this.lblAddress3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.lblAddress3.Text = "Pay Totals";
			this.lblAddress3.Top = 1.375F;
			this.lblAddress3.Visible = false;
			this.lblAddress3.Width = 0.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.75F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label6.Text = "Name";
			this.Label6.Top = 1.375F;
			this.Label6.Width = 0.5625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.75F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = "Label5";
			this.lblPage.Top = 0.25F;
			this.lblPage.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.OutputFormat = resources.GetString("txtTime.OutputFormat");
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label7.Text = "Terminate";
			this.Label7.Top = 1.375F;
			this.Label7.Visible = false;
			this.Label7.Width = 1.1875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.375F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label8.Text = "Check mark indicates that this table is ready to terminate employee.";
			this.Label8.Top = 0.84375F;
			this.Label8.Visible = false;
			this.Label8.Width = 3.28125F;
			// 
			// txtNumber
			// 
			this.txtNumber.Height = 0.1875F;
			this.txtNumber.Left = 0.0625F;
			this.txtNumber.Name = "txtNumber";
			this.txtNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center; ddo-char-set: 0";
			this.txtNumber.Text = null;
			this.txtNumber.Top = 0.03125F;
			this.txtNumber.Width = 0.5F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.75F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtName.Text = null;
			this.txtName.Top = 0.03125F;
			this.txtName.Width = 3.28125F;
			// 
			// chkDeductions
			// 
			this.chkDeductions.Height = 0.125F;
			this.chkDeductions.Left = 4.5625F;
			this.chkDeductions.Name = "chkDeductions";
			this.chkDeductions.Style = "";
			this.chkDeductions.Text = null;
			this.chkDeductions.Top = 0.03125F;
			this.chkDeductions.Visible = false;
			this.chkDeductions.Width = 0.125F;
			// 
			// chkMatches
			// 
			this.chkMatches.Height = 0.125F;
			this.chkMatches.Left = 5.34375F;
			this.chkMatches.Name = "chkMatches";
			this.chkMatches.Style = "";
			this.chkMatches.Text = null;
			this.chkMatches.Top = 0.03125F;
			this.chkMatches.Visible = false;
			this.chkMatches.Width = 0.125F;
			// 
			// chkPayTotals
			// 
			this.chkPayTotals.Height = 0.125F;
			this.chkPayTotals.Left = 6.15625F;
			this.chkPayTotals.Name = "chkPayTotals";
			this.chkPayTotals.Style = "";
			this.chkPayTotals.Text = null;
			this.chkPayTotals.Top = 0.03125F;
			this.chkPayTotals.Visible = false;
			this.chkPayTotals.Width = 0.125F;
			// 
			// chkTerminate
			// 
			this.chkTerminate.Height = 0.125F;
			this.chkTerminate.Left = 7.1875F;
			this.chkTerminate.Name = "chkTerminate";
			this.chkTerminate.Style = "";
			this.chkTerminate.Text = null;
			this.chkTerminate.Top = 0.03125F;
			this.chkTerminate.Visible = false;
			this.chkTerminate.Width = 0.125F;
			// 
			// rptTerminatedEmployees
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.2222222F;
			this.PageSettings.Margins.Right = 0.2222222F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ReportStart += ActiveReport_ReportStart;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMatches)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPayTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTerminate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkMatches;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkPayTotals;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkTerminate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}