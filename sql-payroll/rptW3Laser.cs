//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW3Laser.
	/// </summary>
	public partial class rptW3Laser : BaseSectionReport
	{
		public rptW3Laser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "W3 Laser";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW3Laser InstancePtr
		{
			get
			{
				return (rptW3Laser)Sys.GetInstance(typeof(rptW3Laser));
			}
		}

		protected rptW3Laser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsW2Master?.Dispose();
                rsW2Master = null;
				rsBox10?.Dispose();
                rsBox10 = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW3Laser	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		clsDRWrapper rsW2Master = new clsDRWrapper();
		double dblLaserLineAdjustment;
		object ControlName;
		bool boolMove;
		bool boolThirdParty;
		bool boolDone;
		double dblDeferred;
		clsDRWrapper rsBox10 = new clsDRWrapper();
		bool boolPrintTest;
		bool boolPrintBlank;

		public bool Init()
		{
			bool Init = false;
			boolPrintTest = false;
			boolPrintBlank = false;
			Init = false;
			if (!boolPrintTest)
			{
				frmW3Information.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			}
			if (!modGlobalVariables.Statics.gboolCancelW3)
			{
				frmReportViewer.InstancePtr.Init(this, "", 1, boolAllowEmail: false, showModal: true);
				Init = true;
			}
			return Init;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolDone)
			{
				eArgs.EOF = true;
				return;
			}
			if (!boolPrintTest && !boolPrintBlank)
			{
				if (rsW2Master.EndOfFile())
				{
				}
				else
				{
					// THIS IS THE CONTACT INFORMATION
					txtFederalEIN.Text = rsW2Master.Get_Fields_String("EmployersFederalID");
					txtEmployer.Text = rsW2Master.Get_Fields_String("EmployersName");
					txtEmployersName.Text = rsW2Master.Get_Fields_String("Address1") + "\r";
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsW2Master.Get_Fields_String("Address2"))) != string.Empty)
					{
						txtEmployersName.Text = txtEmployersName.Text + rsW2Master.Get_Fields_String("Address2") + "\r";
					}
					txtEmployersName.Text = txtEmployersName.Text + rsW2Master.Get_Fields_String("City") + ", " + rsW2Master.Get_Fields("State") + " " + rsW2Master.Get_Fields("Zip");
					//txtLocalityName.Text = rsW2Master.Get_Fields("LocalityName");
					txtState.Text = rsW2Master.Get_Fields_String("StateCode");
					txtEmployersStateEIN.Text = rsW2Master.Get_Fields_String("EmployersStateID");
				}
				// SET THE FIELDS TO HAVE THE DATA FROM THE USER DEFINED DATA
				txtTotalWages.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblTotalWages, "0.00");
				txtFederalTax.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblFederalTax, "0.00");
				txtFICAWages.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblFicaWages, "0.00");
				txtFICATax.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblFicaTax, "0.00");
				txtMedWages.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblMedWages, "0.00");
				txtMedTaxes.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblMedTaxes, "0.00");
				txtStateWages.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblStateWages, "0.00");
				txtStateTax.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblStateTax, "0.00");
				txtThirdPartySick.Text = Strings.Format(modGlobalVariables.Statics.W3Information.dblThirdPartySick, "0.00");
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Deductions INNER JOIN tblW2Box12And14 ON (tblW2Deductions.DeductionNumber = tblW2Box12And14.DeductionNumber) where tblw2box12and14.Box12 = 1 and tblw2deductions.Code = 'D'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Deductions INNER JOIN tblW2Box12And14 ON (tblW2Deductions.DeductionNumber = tblW2Box12And14.DeductionNumber) where tblw2box12and14.Box12 = 1 and tblw2deductions.Code = 'E'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Deductions INNER JOIN tblW2Box12And14 ON (tblW2Deductions.DeductionNumber = tblW2Box12And14.DeductionNumber) where tblw2box12and14.Box12 = 1 and tblw2deductions.Code = 'F'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Deductions INNER JOIN tblW2Box12And14 ON (tblW2Deductions.DeductionNumber = tblW2Box12And14.DeductionNumber) where tblw2box12and14.Box12 = 1 and tblw2deductions.Code = 'G'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Deductions INNER JOIN tblW2Box12And14 ON (tblW2Deductions.DeductionNumber = tblW2Box12And14.DeductionNumber) where tblw2box12and14.Box12 = 1 and tblw2deductions.Code = 'H'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Deductions INNER JOIN tblW2Box12And14 ON (tblW2Deductions.DeductionNumber = tblW2Box12And14.DeductionNumber) where tblw2box12and14.Box12 = 1 and tblw2deductions.Code = 'S'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box12 = 1 and  tblw2matches.Code = 'D'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box12 = 1 and  tblw2matches.Code = 'E'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box12 = 1 and  tblw2matches.Code = 'F'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box12 = 1 and  tblw2matches.Code = 'G'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box12 = 1 and  tblw2matches.Code = 'H'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				rsW2Master.OpenRecordset("Select Sum(CYTDAmount) as TotalAmount from tblW2Matches INNER JOIN tblW2Box12And14 ON tblW2Matches.DeductionNumber = tblW2Box12And14.DeductionNumber WHERE  tblw2box12and14.Box12 = 1 and  tblw2matches.Code = 'S'");
				dblDeferred += Conversion.Val(rsW2Master.Get_Fields("TotalAmount"));
				txtDeferredCompensation.Text = Strings.Format(dblDeferred, "0.00");
				// NOW FILL BOX 10
				rsBox10.OpenRecordset("SELECT Sum(tblW2Deductions.CYTDAmount) AS SumOfCYTDAmount FROM tblW2Box12And14 INNER JOIN tblW2Deductions ON tblW2Box12And14.DeductionNumber = tblW2Deductions.DeductionNumber Where tblW2Box12And14.Box10 = 1 AND tblW2Deductions.Code<>''", "TWPY0000.vb1");
				txtDependentCare.Text = FCConvert.ToString(rsBox10.Get_Fields("SumOfCYTDAmount"));
				rsBox10.OpenRecordset("Select Count(EmployeeNumber) as NumberOfW2s from tblW2EditTable");
				if (!rsBox10.EndOfFile())
				{
					txtTotalW2s.Text = FCConvert.ToString(rsBox10.Get_Fields("NumberOfW2s"));
				}
				rsBox10.OpenRecordset("Select * from tblW3Information");
				if (!rsBox10.EndOfFile())
				{
					txtContactPerson.Text = rsBox10.Get_Fields_String("ContactPerson");
					txtEmail.Text = rsBox10.Get_Fields_String("Email");
					txtPhoneNumber.Text = rsBox10.Get_Fields_String("PhoneNumber");
					txtFaxNumber.Text = rsBox10.Get_Fields_String("FaxNumber");
					// check boxes
					chk941.Visible = false;
					chkCT1.Visible = false;
					chkMilitary.Visible = false;
					chkHshld.Visible = false;
					chk943.Visible = false;
					chkMed.Visible = false;
					chkThirdParty.Visible = false;
					chk944.Visible = false;
					chk501cNonGov.Visible = false;
					chkfedgov.Visible = false;
					chkNoneApply.Visible = false;
					chkstatelocal501c.Visible = false;
					chkstatelocalnon501c.Visible = false;
					switch (rsBox10.Get_Fields_Int32("KindOfPayer"))
					{
						case 0:
							{
								chk941.Visible = true;
								break;
							}
						case 1:
							{
								chkCT1.Visible = true;
								break;
							}
						case 2:
							{
								chkMilitary.Visible = true;
								break;
							}
						case 3:
							{
								chkHshld.Visible = true;
								break;
							}
						case 4:
							{
								chk943.Visible = true;
								break;
							}
						case 5:
							{
								chkMed.Visible = true;
								// Case 6
								// chkThirdParty.Visible = True
								break;
							}
						case 7:
							{
								chk944.Visible = true;
								break;
							}
					}
					//end switch
					if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsBox10.Get_Fields("kindofemployer"))) == "N")
					{
						chkNoneApply.Visible = true;
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsBox10.Get_Fields("kindofemployer"))) == "S")
					{
						chkstatelocalnon501c.Visible = true;
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsBox10.Get_Fields("kindofemployer"))) == "Y")
					{
						chkstatelocal501c.Visible = true;
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsBox10.Get_Fields("kindofemployer"))) == "F")
					{
						chkfedgov.Visible = true;
					}
					else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsBox10.Get_Fields("kindofemployer"))) == "T")
					{
						chk501cNonGov.Visible = true;
					}
					else
					{
						chkstatelocalnon501c.Visible = true;
					}
					if (FCConvert.ToBoolean(rsBox10.Get_Fields_Boolean("ThirdPartySickPay")))
					{
						chkThirdParty.Visible = true;
					}
				}
			}
			else if (boolPrintTest)
			{
				chk941.Visible = true;
				chkCT1.Visible = true;
				chkMilitary.Visible = true;
				chkHshld.Visible = true;
				chk943.Visible = true;
				chkMed.Visible = true;
				chkThirdParty.Visible = true;
				chk944.Visible = true;
				chk501cNonGov.Visible = true;
				chkfedgov.Visible = true;
				chkNoneApply.Visible = true;
				chkstatelocal501c.Visible = true;
				chkstatelocalnon501c.Visible = true;
				txtContactPerson.Text = "Contact Person";
				txtEmail.Text = "Email";
				txtPhoneNumber.Text = "000-0000";
				txtFaxNumber.Text = "000-0000";
				txtAreaCode.Text = "000";
				txtFaxAreaCode.Text = "000";
				txtTotalW2s.Text = "00";
				txtDependentCare.Text = "000.00";
				txtDeferredCompensation.Text = "000.00";
				txtTotalWages.Text = "000.00";
				txtFederalTax.Text = "000.00";
				txtFICAWages.Text = "000.00";
				txtFICATax.Text = "000.00";
				txtMedWages.Text = "000.00";
				txtMedTaxes.Text = "000.00";
				txtStateWages.Text = "000.00";
				txtStateTax.Text = "000.00";
				txtThirdPartySick.Text = "000.00";
				txtFederalEIN.Text = "Federal EIN";
				txtEmployer.Text = "Employers Name";
				txtEmployersName.Text = "Address1" + "\r";
				txtEmployersName.Text = txtEmployersName.Text + "Address2" + "\r";
				txtEmployersName.Text = txtEmployersName.Text + "City, ST 00000";
				//txtLocalityName.Text = "Locality";
				txtState.Text = "ST";
				txtEmployersStateEIN.Text = "State ID";
			}
			else
			{
				chk941.Visible = false;
				chkCT1.Visible = false;
				chkMilitary.Visible = false;
				chkHshld.Visible = false;
				chk943.Visible = false;
				chkMed.Visible = false;
				chkThirdParty.Visible = false;
				chk944.Visible = false;
				chk501cNonGov.Visible = false;
				chkfedgov.Visible = false;
				chkNoneApply.Visible = false;
				chkstatelocal501c.Visible = false;
				chkstatelocalnon501c.Visible = false;
			}
			eArgs.EOF = false;
			boolDone = true;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// SET THE SIZE OF THE REPORT TO FIT ONTO THE SCREEN.
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// Me.PageSettings.PaperHeight = 11 * 1440
			// Me.PageSettings.PaperWidth = 8.5 * 1440
			//this.Printer.RenderMode = 1;
			dblLaserLineAdjustment = modGlobalRoutines.SetW2LineAdjustment(this);
			dblLaserLineAdjustment += 3.5;
			// OPEN THE MASTER RECORDSET THAT WILL HOLD THE T
			rsW2Master.OpenRecordset("Select * from tblW2Master", "TWPY0000.vb1");
			MessageBox.Show("Enter W3 form into printer.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// SET THE FIELDS ADJUSTMENT ACCORDING TO WHAT IS IN THE CUSTOMIZE SCEEN.
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment) / 1440F;
				if (boolMove)
				{
					ControlName.Top += 200 / 1440F;
				}
			}
			boolMove = !boolMove;
		}
	}
}
