//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPY0000
{
	public partial class frmPayCodes : BaseForm
	{
		public frmPayCodes()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPayCodes InstancePtr
		{
			get
			{
				return (frmPayCodes)Sys.GetInstance(typeof(frmPayCodes));
			}
		}

		protected frmPayCodes _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       APRIL 30,2001
		//
		// MODIFIED BY: DAN C. SOLTESZ
		// NOTES: The use of the symbols "." and ".." after the row number in
		// column 0 of the FlexGrid indicates that the row has been altered and
		// needs to be saved. The reason for this is to save time on the save
		// process as now only the rows that were altered or added will be saved.
		// The symbol "." means this record is new and an add needs to be done.
		// The symbol ".." means this record was edited and an update needs to be done.
		//
		// **************************************************
		// private local variables
		private clsDRWrapper rsPayCodes = new clsDRWrapper();
		// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		private int intCounter;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(int, bool)
		private int intDataChanged;
		private clsHistory clsHistoryClass = new clsHistory();
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdDelete_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if there is no active current row then we cannot delete anything
				if (vsPayCodes.Row < 0)
					return;
				if (vsPayCodes.Col < 0)
					return;
				if (Conversion.Val(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 0)) == 1)
				{
					MessageBox.Show("Pay Code 1 (NONE) cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else if (FCConvert.ToInt32(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 1)) == 998)
				{
					MessageBox.Show("Pay Code 998 (Contract Wage) cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else if (FCConvert.ToInt32(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 1)) == 999)
				{
					MessageBox.Show("Pay Code 998 (Contract Wage) cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else
				{
					if (MessageBox.Show("This action will delete record #" + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 1) + ". Continue?", "Payroll Deduction Setup", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// now check to make sure that this code is not used in the distribution table
						if (Conversion.Val(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 4)) > 0)
						{
							rsPayCodes.OpenRecordset("Select CD from tblPayrollDistribution where CD = " + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 4), "Twpy0000.vb1");
							if (!rsPayCodes.EndOfFile())
							{
								MessageBox.Show("Pay Code is currently being used. This code cannot be deleted.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
							// SHOW THAT THIS ENTRY IN THE GRID WAS DELETED IN THE AUDIT HISTORY TABLE
							clsHistoryClass.AddAuditHistoryDeleteEntry(ref vsPayCodes);
							// we don't want to remove this record from the table as there is still
							// a deduction number ??? be we want to clear it out.
							if (FCConvert.ToString(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 4)) != string.Empty)
							{
								if (Conversion.Val(FCConvert.ToInt32(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 4)) != 1) != 1)
								{
									rsPayCodes.Execute("Delete from tblPayCodes Where ID = " + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 4), "Payroll");
								}
							}
						}
						// clear the edit symbol from the row number
						vsPayCodes.TextMatrix(vsPayCodes.Row, 0, Strings.Mid(vsPayCodes.TextMatrix(vsPayCodes.Row, 0), 1, (vsPayCodes.TextMatrix(vsPayCodes.Row, 0).Length < 2 ? 2 : vsPayCodes.TextMatrix(vsPayCodes.Row, 0).Length - 2)));
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Deleted Row " + FCConvert.ToString(vsPayCodes.Row) + " from Update Pay Codes");
						// ------------------------------------------------------------------
						// load the grid with the new data to show the 'clean out' of the current row
						LoadGrid();
						// decrement the counter as to how many records are dirty
						intDataChanged -= 1;
						MessageBox.Show("Record Deleted successfully.", "Payroll Pay Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// validate that there is not changes that need to be saved
				SaveChanges();
				// unload the form
				Close();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll Pay Codes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						cmdSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdNew_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// append a new row to the end of the grid
				vsPayCodes.AddItem(FCConvert.ToString(vsPayCodes.Rows) + "\t" + FCConvert.ToString(vsPayCodes.Rows - 1));
				// increment the counter as to how many records are dirty
				intDataChanged += 1;
				// assign a row number to this new record
				vsPayCodes.TextMatrix(vsPayCodes.Rows - 1, 0, vsPayCodes.TextMatrix(vsPayCodes.Rows - 1, 0) + ".");
				// set focus to this new row
				vsPayCodes.Select(vsPayCodes.Rows - 1, 2);
				// Dave 12/14/2006---------------------------------------------------
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Row to Update Pay Codes");
				// ------------------------------------------------------------------
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdPrint_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// display the Pay code report
				// rptPayCodes.Show
				frmReportViewer.InstancePtr.Init(rptPayCodes.InstancePtr, boolAllowEmail: true, strAttachmentName: "PayCodes");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void cmdRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdRefresh_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// verify there are not 'dirty' record
				SaveChanges();
				// load the grid of current data from the database
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdRefresh_Click()
		{
			cmdRefresh_Click(cmdRefresh, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdSave_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int x;
				vsPayCodes.Select(0, 0);
				// Dave 12/14/2006--------------------------------------------
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Update Pay Codes", "", fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gdatCurrentPayDate.ToString("M/d/yyyy")), fecherFoundation.Strings.Trim(modGlobalVariables.Statics.gintCurrentPayRun.ToString()));
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				// ----------------------------------------------------------------
				for (intCounter = 1; intCounter <= (vsPayCodes.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2))) != string.Empty || fecherFoundation.Strings.Trim(FCConvert.ToString(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3))) != string.Empty)
					{
						if (Conversion.Val(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) != 0)
						{
							if (Strings.Right(FCConvert.ToString(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 2) == "..")
							{
								// record has been edited so we need to update it
								if (NotValidData())
									goto ExitRoutine;
								rsPayCodes.Execute("Update tblPayCodes set PayCode ='" + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "',  Description = '" + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "', Rate = " + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3) + " Where ID = " + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4), "Payroll");
								// clear the edit symbol from the row number
								vsPayCodes.TextMatrix(intCounter, 0, Strings.Mid(vsPayCodes.TextMatrix(intCounter, 0), 1, vsPayCodes.TextMatrix(intCounter, 0).Length - 2));
								goto NextRecord;
							}
							if (Strings.Right(FCConvert.ToString(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 0)), 1) == ".")
							{
								// record has been added so we need to save it
								if (NotValidData())
									goto ExitRoutine;
								rsPayCodes.Execute("Insert into tblPayCodes (PayCode,Description,Rate,LastUserID) VALUES ('" + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "','" + vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'," + FCConvert.ToString(Conversion.Val(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3))) + ",'" + modGlobalVariables.Statics.gstrUser + "')", "Payroll");
								// clear the edit symbol from the row number
								vsPayCodes.TextMatrix(intCounter, 0, Strings.Mid(vsPayCodes.TextMatrix(intCounter, 0), 1, vsPayCodes.TextMatrix(intCounter, 0).Length - 1));
								// get the new ID from the table for the record that was just added and place it into column 3
								rsPayCodes.OpenRecordset("Select Max(ID) as NewID from tblPayCodes", "TWPY0000.vb1");
								vsPayCodes.TextMatrix(intCounter, 4, FCConvert.ToString(rsPayCodes.Get_Fields("NewID")));
							}
						}
					}
					NextRecord:
					;
				}
				MessageBox.Show("Record(s) Saved successfully.", "Payroll Pay Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				clsHistoryClass.Compare();
				LoadGrid();
				// initialize the counter as to have many rows are dirty
				intDataChanged = 0;
				return;
				ExitRoutine:
				;
				MessageBox.Show("Record(s) NOT saved successfully. Incorrect data on grid row #" + FCConvert.ToString(intCounter), "Payroll Pay Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private bool NotValidData()
		{
			bool NotValidData = false;
			// If Trim(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) = vbNullString Then NotValidData = True
			// If Trim(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) = vbNullString Then NotValidData = True
			// If Trim(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)) = vbNullString Then NotValidData = True
			// If Trim(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)) = vbNullString Then NotValidData = True
			// If Trim(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)) = vbNullString Then NotValidData = True
			// If Trim(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)) = vbNullString Then NotValidData = True
			// If Trim(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 7)) = vbNullString Then NotValidData = True
			return NotValidData;
		}

		private void frmPayCodes_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call ForceFormToResize(Me)
				// verify that this form is not already open
				if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
					return;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPayCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPayCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPayCodes properties;
			//frmPayCodes.ScaleWidth	= 5895;
			//frmPayCodes.ScaleHeight	= 3930;
			//frmPayCodes.LinkTopic	= "Form1";
			//frmPayCodes.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				int counter;
				// vsElasticLight1.Enabled = True
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				modGlobalFunctions.SetTRIOColors(this);
				// open the forms global database connection
				// Set dbPayCodes = OpenDatabase(PAYROLLDATABASE, False, False, ";PWD=" & DATABASEPASSWORD)
				rsPayCodes.DefaultDB = "TWPY0000.vb1";
				// SET THE ID COLUMNS FOR EACH GRID
				clsHistoryClass.SetGridIDColumn("PY", "vsPayCodes", 4);
				// set the grid column headers/widths/etc....
				SetGridProperties();
				// Load the grid with data
				LoadGrid();
				if (vsPayCodes.Rows > 2)
					vsPayCodes.Select(2, 2);
				// Dave 12/14/2006---------------------------------------------------
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
				intDataChanged = FCConvert.ToInt16(false);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// get all of the records from the database
				vsPayCodes.Rows = 2;
				rsPayCodes.OpenRecordset("Select * from tblPayCodes Order by PayCode", "TWPY0000.vb1");
				if (!rsPayCodes.EndOfFile())
				{
					// this will make the recordcount property work correctly
					rsPayCodes.MoveLast();
					rsPayCodes.MoveFirst();
					// set the number of rows in the grid
					vsPayCodes.Rows = rsPayCodes.RecordCount() + 1;
					vsPayCodes.FixedRows = 1;
					// fill the grid
					for (intCounter = 1; intCounter <= (rsPayCodes.RecordCount()); intCounter++)
					{
						vsPayCodes.TextMatrix(intCounter, 0, FCConvert.ToString(intCounter));
						// If intCounter - 1 <> Trim(rsPayCodes.Fields("PayCode") & " ") Then
						// rsPayCodes.Execute ("Update tblPayCodes Set PayCode = " & intCounter - 1 & " where ID = " & Trim(rsPayCodes.Fields("ID") & " "))
						// vsPayCodes.TextMatrix(intCounter, 1) = intCounter - 1
						// Else
						vsPayCodes.TextMatrix(intCounter, 1, FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(rsPayCodes.Get_Fields_String("PayCode") + " "))));
						// End If
						// vsPayCodes.TextMatrix(intCounter, 1) = Trim(rsPayCodes.Fields("PayCode") & " ")
						vsPayCodes.TextMatrix(intCounter, 2, fecherFoundation.Strings.Trim(rsPayCodes.Get_Fields("Description") + " "));
						vsPayCodes.TextMatrix(intCounter, 3, fecherFoundation.Strings.Trim(rsPayCodes.Get_Fields("Rate") + " "));
						vsPayCodes.TextMatrix(intCounter, 4, fecherFoundation.Strings.Trim(rsPayCodes.Get_Fields("ID") + " "));
						rsPayCodes.MoveNext();
					}
					vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 1, 3, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// initialize the counter as to have many rows are dirty
					intDataChanged = 0;
					if (vsPayCodes.Rows > 2)
						vsPayCodes.Select(2, 2);
				}
				vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vsPayCodes.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
				clsHistoryClass.Init = this;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void SetGridProperties()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SetGridProperties";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set grid properties
                //FC:FINAL:AM:#4201 - remove WordWrap - in VB6 it's not working due to the handling of the ENTER key
				//vsPayCodes.WordWrap = true;
				vsPayCodes.Cols = 5;
				vsPayCodes.FixedCols = 2;
				vsPayCodes.ColHidden(0, true);
				vsPayCodes.ColHidden(4, true);
				// set column 0 properties
				vsPayCodes.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayCodes.ColWidth(0, 400);
				// set column 1 properties
				vsPayCodes.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayCodes.ColWidth(1, 600);
				vsPayCodes.TextMatrix(0, 1, "Code");
				// set column 2 properties
				vsPayCodes.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayCodes.ColWidth(2, 3800);
				vsPayCodes.TextMatrix(0, 2, "Description");
				// set column 3 properties
				vsPayCodes.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsPayCodes.ColWidth(3, 1000);
				vsPayCodes.TextMatrix(0, 3, "Rate");
				vsPayCodes.ColFormat(3, "###.0000");
				//vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, 0, 3, Color.Blue);
				//vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				// vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, 0, 3) = True
				// vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, 0, 3) = vbWhite
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmPayCodes_Resize(object sender, System.EventArgs e)
		{
			vsPayCodes.ColWidth(0, FCConvert.ToInt32(vsPayCodes.WidthOriginal * 0.05));
			vsPayCodes.ColWidth(1, FCConvert.ToInt32(vsPayCodes.WidthOriginal * 0.1));
			vsPayCodes.ColWidth(2, FCConvert.ToInt32(vsPayCodes.WidthOriginal * 0.6));
			vsPayCodes.ColWidth(3, FCConvert.ToInt32(vsPayCodes.WidthOriginal * 0.25));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				vsPayCodes.Select(0, 0);
				SaveChanges();
				//MDIParent.InstancePtr.Show();
				// set focus back to the menu options of the MDIParent
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			cmdRefresh_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void vsPayCodes_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsPayCodes_AfterEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// if the current row is already marked as needing a save then do not mark it again
				if (Strings.Right(FCConvert.ToString(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsPayCodes.Row, 0)), 1) != ".")
				{
					// increment the counter as to how many rows are dirty
					intDataChanged += 1;
					// Change the row number in the first column to indicate that this record has been edited
					vsPayCodes.TextMatrix(vsPayCodes.Row, 0, vsPayCodes.TextMatrix(vsPayCodes.Row, 0) + "..");
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void AddNewGridRecord()
		{
			cmdNew_Click();
		}

		private void vsPayCodes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsPayCodes.Rows > 2)
			{
				if (vsPayCodes.Row == 2 && (KeyCode == (Keys)38 || KeyCode == (Keys)37))
					KeyCode = 0;
			}
			modGridKeyMove.MoveGridProsition(ref vsPayCodes, 2, 3, KeyCode, 9999, true);
		}

		private void vsPayCodes_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "vsPayCodes_KeyDownEdit";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (vsPayCodes.Rows > 2)
				{
					if (vsPayCodes.Row == 2 && (KeyCode == (Keys)38 || KeyCode == (Keys)37))
						KeyCode = 0;
				}
				modGridKeyMove.MoveGridProsition(ref vsPayCodes, 2, 3, KeyCode, 9999, true);
				// if this is the return key then make it function as a tab
				// If KeyCode = vbKeyReturn Or KeyCode = vbKeyTab Then
				// KeyCode = 0
				// if this is the last column then begin in the 1st column of the next row
				// If vsPayCodes.Col = 3 Then
				// If vsPayCodes.Row < vsPayCodes.Rows - 1 Then
				// vsPayCodes.Row = vsPayCodes.Row + 1
				// vsPayCodes.Col = 2
				// Else
				// if there is no other row then create a new one
				// Call cmdNew_Click
				// End If
				// Else
				// move to the next column
				// SendKeys "{RIGHT}"
				// End If
				// End If
				// 
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void vsPayCodes_RowColChange(object sender, System.EventArgs e)
		{
			vsPayCodes.Editable = (FCConvert.ToInt32(vsPayCodes.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsPayCodes.Row, vsPayCodes.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND) ? FCGrid.EditableSettings.flexEDKbdMouse: FCGrid.EditableSettings.flexEDNone;
			if (vsPayCodes.Row == 0)
			{
				vsPayCodes.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				if (FCConvert.ToDouble(vsPayCodes.TextMatrix(vsPayCodes.Row, 1)) == 998 || FCConvert.ToDouble(vsPayCodes.TextMatrix(vsPayCodes.Row, 1)) == 999)
				{
					vsPayCodes.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					vsPayCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// vbPorter upgrade warning: intRows As int	OnWriteFCConvert.ToInt32(
			int intRows;
			int intCols;
			intTotalNumberOfControls = 0;
			for (intRows = 1; intRows <= (vsPayCodes.Rows - 1); intRows++)
			{
				for (intCols = 1; intCols <= 3; intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
                    clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
                    clsControlInfo[intTotalNumberOfControls].ControlName = "vsPayCodes";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					clsControlInfo[intTotalNumberOfControls].DataDescription = "Row " + FCConvert.ToString(intRows) + " " + vsPayCodes.TextMatrix(0, intCols);
					intTotalNumberOfControls += 1;
				}
			}
		}
	}
}
