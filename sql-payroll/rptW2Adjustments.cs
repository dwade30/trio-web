//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Adjustments.
	/// </summary>
	public partial class rptW2Adjustments : BaseSectionReport
	{
		public rptW2Adjustments()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptW2Adjustments InstancePtr
		{
			get
			{
				return (rptW2Adjustments)Sys.GetInstance(typeof(rptW2Adjustments));
			}
		}

		protected rptW2Adjustments _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsRpt?.Dispose();
                clsRpt = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptW2Adjustments	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsRpt = new clsDRWrapper();
		double dblTotFed;
		double dblTotFica;
		double dblTotState;
		double dblTotMedicare;

		public void Init()
		{
			string strSQL;
			int lngYear = 0;
			string strOrderBy = "";
			clsRpt.OpenRecordset("select top 1 * from tblw2EditTable", "twpy0000.vb1");
			if (!clsRpt.EndOfFile())
			{
				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsRpt.Get_Fields("year"))));
			}
			else
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			clsRpt.OpenRecordset("select * from tbldefaultinformation", "twpy0000.vb1");
			if (Conversion.Val(clsRpt.Get_Fields("reportsequence")) == 0)
			{
				strOrderBy = " order by lastname,firstname ";
			}
			else if (Conversion.Val(clsRpt.Get_Fields("reportsequence")) == 2)
			{
				strOrderBy = " order by seqnumber,tblemployeemaster.employeenumber ";
			}
			else if (Conversion.Val(clsRpt.Get_Fields("reportsequence")) == 3)
			{
				strOrderBy = " order by deptdiv,tblemployeemaster.employeenumber ";
			}
			else
			{
				// number
				strOrderBy = " order by tblemployeemaster.employeenumber ";
			}
			strSQL = "select * from (select employeenumber,sum(federaltaxgross) as fedgross,sum(ficataxgross) as ficagross,sum(statetaxgross) as stategross,sum(medicaretaxgross) as medicaregross  from tblcheckdetail where w2adjustment = 1 and paydate = '12/31/" + FCConvert.ToString(lngYear) + "' group by employeenumber) as tbl1 inner join tblemployeemaster on (tbl1.employeenumber = tblemployeemaster.employeenumber)  " + strOrderBy;
			clsRpt.OpenRecordset(strSQL, "twpy0000.vb1");
			if (clsRpt.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			dblTotFed = 0;
			dblTotFica = 0;
			dblTotState = 0;
			dblTotMedicare = 0;
			lblMuni.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "W2Adjustments");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsRpt.EndOfFile();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsRpt.EndOfFile())
			{
				double dblFederal = 0;
				double dblFica = 0;
				double dblState = 0;
				double dblMedicare = 0;
				dblFederal = 0;
				dblFica = 0;
				dblState = 0;
				dblMedicare = 0;
				txtEmployeeNumber.Text = clsRpt.Get_Fields("tblemployeemaster.employeenumber");
				txtName.Text = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(clsRpt.Get_Fields("firstname") + " " + clsRpt.Get_Fields("middlename")) + " " + clsRpt.Get_Fields("lastname") + " " + clsRpt.Get_Fields("desig"));
				dblFederal = Conversion.Val(clsRpt.Get_Fields("FedGross"));
				dblFica = Conversion.Val(clsRpt.Get_Fields("ficagross"));
				dblState = Conversion.Val(clsRpt.Get_Fields("stategross"));
				dblMedicare = Conversion.Val(clsRpt.Get_Fields("medicaregross"));
				dblTotFed += dblFederal;
				dblTotFica += dblFica;
				dblTotState += dblState;
				dblTotMedicare += dblMedicare;
				txtFederal.Text = Strings.Format(dblFederal, "#,###,###,##0.00");
				txtFica.Text = Strings.Format(dblFica, "#,###,###,##0.00");
				txtState.Text = Strings.Format(dblState, "#,###,###,##0.00");
				txtMedicare.Text = Strings.Format(dblMedicare, "#,###,###,##0.00");
				clsRpt.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotFed.Text = Strings.Format(dblTotFed, "#,###,###,##0.00");
			txtTotFica.Text = Strings.Format(dblTotFica, "#,###,###,##0.00");
			txtTotState.Text = Strings.Format(dblTotState, "#,###,###,##0.00");
			txtTotMedicare.Text = Strings.Format(dblTotMedicare, "#,###,###,##0.00");
		}

		
	}
}
