﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public partial class frmError : BaseForm
	{
		public frmError()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Label1 = new System.Collections.Generic.List<FCLabel>();
			this.Label1.AddControlArrayElement(Label1_0, 0);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_4, 4);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmError InstancePtr
		{
			get
			{
				return (frmError)Sys.GetInstance(typeof(frmError));
			}
		}

		protected frmError _InstancePtr = null;
		//=========================================================
		private void cmdContinue_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			// End
			Close();
		}

		private void frmError_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmError properties;
			//frmError.Appearance	= 0;
			//frmError.ScaleWidth	= 6135;
			//frmError.ScaleHeight	= 5145;
			//frmError.LinkTopic	= "Form2";
			//End Unmaped Properties
			frmError.InstancePtr.LeftOriginal = FCConvert.ToInt32((FCGlobal.Screen.WidthOriginal / 2) - (frmError.InstancePtr.WidthOriginal / 2.0));
			frmError.InstancePtr.TopOriginal = FCConvert.ToInt32((FCGlobal.Screen.HeightOriginal / 2) - (frmError.InstancePtr.HeightOriginal / 2.0));
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}
	}
}
