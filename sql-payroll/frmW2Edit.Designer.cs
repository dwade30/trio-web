//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmW2Edit.
	/// </summary>
	partial class frmW2Edit
	{
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCProgressBar pbrStatus;
		public fecherFoundation.FCLabel lblStatus;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vsData = new fecherFoundation.FCGrid();
            this.pbrStatus = new fecherFoundation.FCProgressBar();
            this.lblStatus = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdAddMissingEmployee = new fecherFoundation.FCButton();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddMissingEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 447);
            this.BottomPanel.Size = new System.Drawing.Size(656, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsData);
            this.ClientArea.Controls.Add(this.pbrStatus);
            this.ClientArea.Controls.Add(this.lblStatus);
            this.ClientArea.Size = new System.Drawing.Size(656, 387);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddMissingEmployee);
            this.TopPanel.Size = new System.Drawing.Size(656, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddMissingEmployee, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(273, 30);
            this.HeaderText.Text = "Update W-2 Information";
            // 
            // vsData
            // 
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.Cols = 23;
            this.vsData.ColumnHeadersVisible = false;
            this.vsData.FixedRows = 0;
            this.vsData.Location = new System.Drawing.Point(30, 104);
            this.vsData.Name = "vsData";
            this.vsData.Rows = 50;
            this.vsData.Size = new System.Drawing.Size(598, 276);
            this.vsData.DoubleClick += new System.EventHandler(this.vsData_DblClick);
            // 
            // pbrStatus
            // 
            this.pbrStatus.Location = new System.Drawing.Point(30, 69);
            this.pbrStatus.Name = "pbrStatus";
            this.pbrStatus.Size = new System.Drawing.Size(598, 15);
            this.pbrStatus.TabIndex = 1;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(30, 30);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(598, 19);
            this.lblStatus.TabIndex = 2;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAdd,
            this.mnuSP1,
            this.mnuProcess,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = 0;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Text = "Add Missing Employee";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            this.mnuSP1.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 2;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcess.Text = "Process";
            this.mnuProcess.Visible = false;
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 3;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            this.mnuSP2.Visible = false;
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddMissingEmployee
            // 
            this.cmdAddMissingEmployee.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddMissingEmployee.Location = new System.Drawing.Point(468, 29);
            this.cmdAddMissingEmployee.Name = "cmdAddMissingEmployee";
            this.cmdAddMissingEmployee.Size = new System.Drawing.Size(160, 24);
            this.cmdAddMissingEmployee.TabIndex = 1;
            this.cmdAddMissingEmployee.Text = "Add Missing Employee";
            this.cmdAddMissingEmployee.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(277, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(100, 48);
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Visible = false;
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // frmW2Edit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(656, 555);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmW2Edit";
            this.Text = "Update W-2 Information";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmW2Edit_Load);
            this.Activated += new System.EventHandler(this.frmW2Edit_Activated);
            this.Resize += new System.EventHandler(this.frmW2Edit_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmW2Edit_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmW2Edit_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddMissingEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdAddMissingEmployee;
		private FCButton cmdProcess;
	}
}