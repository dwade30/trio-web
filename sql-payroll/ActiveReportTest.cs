﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for ActiveReportTest.
	/// </summary>
	public partial class ActiveReportTest : FCSectionReport
	{
		public ActiveReportTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static ActiveReportTest InstancePtr
		{
			get
			{
				return (ActiveReportTest)Sys.GetInstance(typeof(ActiveReportTest));
			}
		}

		protected ActiveReportTest _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	ActiveReportTest	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReportTest_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//ActiveReportTest properties;
			//ActiveReportTest.Caption	= "ActiveReport1";
			//ActiveReportTest.Left	= 0;
			//ActiveReportTest.Top	= 0;
			//ActiveReportTest.Width	= 19200;
			//ActiveReportTest.Height	= 8385;
			//ActiveReportTest.StartUpPosition	= 3;
			//ActiveReportTest.SectionData	= "ActiveReportTest.dsx":0000;
			//End Unmaped Properties
		}
	}
}
