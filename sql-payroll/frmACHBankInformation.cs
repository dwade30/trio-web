//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPY0000
{
	public partial class frmACHBankInformation : BaseForm
	{
		public frmACHBankInformation()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.lblDateSelect = new System.Collections.Generic.List<FCLabel>();
			this.lblDateSelect.AddControlArrayElement(lblDateSelect_3, 0);
			this.lblDateSelect.AddControlArrayElement(lblDateSelect_4, 1);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmACHBankInformation InstancePtr
		{
			get
			{
				return (frmACHBankInformation)Sys.GetInstance(typeof(frmACHBankInformation));
			}
		}

		protected frmACHBankInformation _InstancePtr = null;
		//=========================================================
		bool boolDontRespondToClick;
		int lngTotalAmount;
		double dblTotalAmount;
		double lngHashNumber;
		// LONG ISN'T BIG ENOUGH
		double lngHash8;
		// LONG ISN'T BIG ENOUGHT
		int lngNumBatches;
		int lngRecordBlockCount;
		bool boolSetupMode;
		// tells what to do when f12 is hit
		// vbPorter upgrade warning: dtPayDateChosen As DateTime	OnWrite(DateTime, int)
		DateTime dtPayDateChosen;
		bool boolCreateFile;
		bool boolInitRun;
		int intPayRunChosen;
		bool boolBalanced;
		clsDRWrapper clsDDBanks = new clsDRWrapper();
		double dblTotalDebits;
		// Dim intACHEmployerIDPrefix      As Integer
		string strACHEmployerIDPrefix;
		bool boolPreNote;
		bool boolFileStarted;
		bool boolPrenoteFile;
		bool boolOnlyPrenoteEntries;
		bool boolMadePrenoteFile;

		private void cboPayDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillPayRunCombo();
		}

		private void cboPayDateYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// FILL THE PAY DATE COMBO WITH ALL PAY DATES THAT HAVE BEEN PAID FROM THE CHECK DETAIL TABLE.
			FillPayDateCombo();
		}

		private void cmdPrenote_Click(object sender, System.EventArgs e)
		{
			// CREATE THE ACH FILE USING THE PARAMETER THAT THIS IS A PRENOTE FILE SO USE ALL EMPLOYEES
			boolPreNote = true;
			boolPrenoteFile = true;
			if (cmbPrenote.Text == "Only DD entries marked Prenote")
			{
				boolOnlyPrenoteEntries = true;
			}
			else
			{
				boolOnlyPrenoteEntries = false;
			}
			if (CreateACHFile())
			{
				boolMadePrenoteFile = true;
			}
		}

		private void frmACHBankInformation_Activated(object sender, System.EventArgs e)
		{
			// FORCE THE FORM TO BE RESIZED SO THAT THE RESIZER WILL TAKE EFFECT.
			// Call ForceFormToResize(Me)
		}

		private void frmACHBankInformation_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmACHBankInformation_Load(object sender, System.EventArgs e)
		{
			boolSetupMode = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			cmbAccountType.Clear();
			cmbAccountType.AddItem("Checking");
			cmbAccountType.ItemData(0, 27);
			cmbAccountType.AddItem("Savings");
			cmbAccountType.ItemData(1, 37);
			LoadInfo();
			boolMadePrenoteFile = false;
			strACHEmployerIDPrefix = "1";
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
			if (!rsData.EndOfFile())
			{
				// GET IF THERE IS TO BE A PREFIX ADDED TO THE EMPLOYER NUMBER ON THE ACH
				// ELECTRONIC FILE.
				if (Conversion.Val(rsData.Get_Fields_Int16("ACHEmployerIDPrefix")) == 0)
				{
					if (!fecherFoundation.FCUtils.IsNull(rsData.Get_Fields("ACHEmployeridPrefix")))
					{
						strACHEmployerIDPrefix = " ";
					}
				}
				else if (Conversion.Val(rsData.Get_Fields_Int16("ACHEmployerIDPrefix")) == 1)
				{
					strACHEmployerIDPrefix = "1";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int16("ACHEmployerIDPrefix")) == 9)
				{
					strACHEmployerIDPrefix = "9";
				}
			}
		}

		private void mnuCreate_Click(object sender, System.EventArgs e)
		{
			// CREATE THE ACH FILE FOR THE PAYDATE AND PAY RUN SELECTED
			CreateACHFile();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			// THIS WILL TELL ME THAT THE FORM HAS BEEN CANCELED OUT SO THAT I DO NOT SHOW
			// THE ACH REPORT FROM THE FUNCTION THAT STARTED THIS CALL.
			modGlobalVariables.Statics.gboolCancelACHForm = true;
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillYearCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			boolDontRespondToClick = true;
			// There are two different ways
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet || modGlobalVariables.Statics.gstrMQYProcessing != "NONE")
			{
				strSQL = "select year(paydate) as payyear from tblcheckdetail where BankRecord = 1 and checknumber > 0 and year(paydate) = " + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate.Year) + " group by year(paydate) order by year(paydate) desc";
			}
			else
			{
				strSQL = "Select year(paydate) as payyear from tblcheckdetail where BankRecord = 1 and checknumber > 0 group by year(paydate) order by  year(paydate) desc";
			}
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			cboPayDateYear.Clear();
			while (!clsLoad.EndOfFile())
			{
				cboPayDateYear.AddItem(FCConvert.ToString(clsLoad.Get_Fields("PayYear")));
				clsLoad.MoveNext();
			}
			boolDontRespondToClick = false;
			if (cboPayDateYear.Items.Count > 0)
			{
				cboPayDateYear.SelectedIndex = 0;
			}
			else
			{
				cmdCreate.Visible = false;
				mnuSaveExit.Enabled = false;
				cmdSave.Visible = false;
			}
			if (clsLoad.EndOfFile() && clsLoad.BeginningOfFile())
			{
				MessageBox.Show("There are no direct deposit records");
			}
		}

		private void FillPayDateCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			// vbPorter upgrade warning: lngYear As int	OnWrite(string)
			int lngYear;
			if (boolDontRespondToClick)
				return;
			boolDontRespondToClick = true;
			if (cboPayDateYear.Items.Count < 1 || cboPayDateYear.SelectedIndex < 0)
				return;
			lngYear = FCConvert.ToInt32(cboPayDateYear.Items[cboPayDateYear.SelectedIndex].ToString());
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet || modGlobalVariables.Statics.gstrMQYProcessing != "NONE")
			{
				strSQL = "select paydate from tblcheckdetail where paydate = '" + FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate) + "' and BankRecord = 1 group by paydate order by paydate desc";
			}
			else
			{
				strSQL = "Select paydate from tblcheckdetail where paydate between '1/1/" + FCConvert.ToString(lngYear) + "' and '12/31/" + FCConvert.ToString(lngYear) + "' and BankRecord = 1 group by paydate order by paydate desc";
			}
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			cboPayDate.Clear();
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("There are no direct deposit records");
			}
			while (!clsLoad.EndOfFile())
			{
				cboPayDate.AddItem(Strings.Format(clsLoad.Get_Fields("Paydate"), "MM/dd/yyyy"));
				clsLoad.MoveNext();
			}
			boolDontRespondToClick = false;
			if (cboPayDate.Items.Count > 0)
			{
				cboPayDate.SelectedIndex = 0;
			}
			else
			{
				cmdCreate.Visible = false;
				mnuSaveExit.Enabled = false;
				cmdSave.Visible = false;
			}
		}

		private void FillPayRunCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			// vbPorter upgrade warning: dtPDate As DateTime	OnWrite(string)
			DateTime dtPDate;
			if (boolDontRespondToClick)
				return;
			if (cboPayDate.Items.Count < 1 || cboPayDate.SelectedIndex < 0)
			{
				cmdCreate.Visible = false;
				mnuSaveExit.Enabled = false;
				cmdSave.Visible = false;
				return;
			}
			dtPDate = FCConvert.ToDateTime(cboPayDate.Items[cboPayDate.SelectedIndex].ToString());
			strSQL = "Select PayRunID from tblcheckdetail where paydate = '" + FCConvert.ToString(dtPDate) + "' and BankRecord = 1 group by payrunid order by payrunid";
			clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
			cboPayRunID.Clear();
			while (!clsLoad.EndOfFile())
			{
				cboPayRunID.AddItem(FCConvert.ToString(clsLoad.Get_Fields("payrunid")));
				clsLoad.MoveNext();
			}
			if (cboPayRunID.Items.Count > 0)
			{
				cboPayRunID.SelectedIndex = 0;
                //FC:FINAL:AM:#3224 - menu is hidden in VB6
				//cmdCreate.Visible = true;
				mnuSaveExit.Enabled = true;
				cmdSave.Visible = true;
				T2KEffectiveEntryDate.Text = Strings.Format(dtPDate, "MM/dd/yyyy");
				if (boolInitRun)
				{
					cboPayRunID.Enabled = false;
					cboPayDate.Enabled = false;
					cboPayDateYear.Enabled = false;
				}
			}
			else
			{
				cmdCreate.Visible = false;
				mnuSaveExit.Enabled = false;
				cmdSave.Visible = false;
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				clsSave.OpenRecordset("select * from tblACHInformation", "twpy0000.vb1");
				if (clsSave.EndOfFile())
				{
					clsSave.AddNew();
				}
				else
				{
					clsSave.Edit();
				}
				strTemp = fecherFoundation.Strings.Trim(txtBankName.Text);
				if (strTemp == string.Empty)
				{
					MessageBox.Show("You must provide a bank name");
					txtBankName.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("EmployerAccountType", cmbAccountType.ItemData(cmbAccountType.SelectedIndex));
				// pad with spaces to the correct length
				strTemp += Strings.StrDup(modCoreysSweeterCode.ACHNameLen, " ");
				strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHNameLen);
				clsSave.Set_Fields("achbankname", strTemp);
				strTemp = fecherFoundation.Strings.Trim(txtACHRT.Text);
				if (strTemp.Length != modCoreysSweeterCode.ACHRTLen)
				{
					MessageBox.Show("You must provide an ACH RT of length " + FCConvert.ToString(modCoreysSweeterCode.ACHRTLen), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtACHRT.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("achbankrt", strTemp);
				strTemp = fecherFoundation.Strings.Trim(txtEmployerName.Text);
				if (strTemp == string.Empty)
				{
					MessageBox.Show("You must provide an employer name");
					txtEmployerName.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				strTemp += Strings.StrDup(modCoreysSweeterCode.ACHEmployerNameLen, " ");
				strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHEmployerNameLen);
				clsSave.Set_Fields("employername", strTemp);
				if (fecherFoundation.Strings.Trim(txtImmediateOriginName.Text) == string.Empty)
				{
					clsSave.Set_Fields("ImmediateOriginName", strTemp);
				}
				else
				{
					strTemp = fecherFoundation.Strings.Trim(txtImmediateOriginName.Text);
					strTemp += Strings.StrDup(modCoreysSweeterCode.ACHNameLen, " ");
					strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHNameLen);
					clsSave.Set_Fields("ImmediateOriginName", strTemp);
				}
				strTemp = fecherFoundation.Strings.Trim(txtEmpRT.Text);
				if (strTemp.Length != modCoreysSweeterCode.ACHEmployerRTLen)
				{
					MessageBox.Show("You must provide an Employer RT of length " + FCConvert.ToString(modCoreysSweeterCode.ACHEmployerRTLen), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEmpRT.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("employerrt", strTemp);
				if (fecherFoundation.Strings.Trim(txtImmediateOriginRT.Text) == string.Empty)
				{
					clsSave.Set_Fields("ImmediateOriginRT", strTemp);
				}
				else
				{
					strTemp = fecherFoundation.Strings.Trim(txtImmediateOriginRT.Text);
					if (strTemp.Length < 10)
					{
						strTemp += Strings.StrDup(modCoreysSweeterCode.ACHRTLen, " ");
						strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHRTLen);
					}
					clsSave.Set_Fields("ImmediateOriginRT", strTemp);
				}
				if (fecherFoundation.Strings.Trim(txtODFI.Text) == string.Empty)
				{
					clsSave.Set_Fields("odfinum", strTemp);
				}
				else
				{
					strTemp = fecherFoundation.Strings.Trim(txtODFI.Text);
					strTemp += Strings.StrDup(modCoreysSweeterCode.ACHRTLen, " ");
					strTemp = Strings.Mid(strTemp, 1, modCoreysSweeterCode.ACHRTLen);
					clsSave.Set_Fields("ODFINum", strTemp);
				}
				strTemp = fecherFoundation.Strings.Trim(txtEmpAccount.Text);
				if (strTemp.Length < 1)
				{
					// If Len(strTemp) <> ACHEmployerAccountLen Then
					MessageBox.Show("You must provide an employer account", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// MsgBox "You must provide an Employer Account # of length " & ACHEmployerAccountLen, vbExclamation
					txtEmpAccount.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("EmployerAccount", strTemp);
				strTemp = fecherFoundation.Strings.Trim(txtEmployerID.Text);
				if (strTemp.Length != modCoreysSweeterCode.ACHEmployerIDLen)
				{
					MessageBox.Show("You must provide an Employer ID of length " + FCConvert.ToString(modCoreysSweeterCode.ACHEmployerIDLen), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEmployerID.Focus();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return SaveInfo;
				}
				clsSave.Set_Fields("employerid", strTemp);
				if (!Information.IsDate(clsSave.Get_Fields("effectiveentrydate")))
				{
					clsSave.Set_Fields("EffectiveEntryDate", Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy"));
				}
				else
				{
					if (Convert.ToDateTime(clsSave.Get_Fields("effectiveentrydate")).ToOADate() == 0)
					{
						clsSave.Set_Fields("effectiveentrydate", Strings.Format(modGlobalVariables.Statics.gdatCurrentPayDate, "MM/dd/yyyy"));
					}
				}
				clsSave.Update();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// THIS WILL L
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsLoad.OpenRecordset("select * from tblachinformation", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					txtBankName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("achbankname")));
					txtACHRT.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("achbankrt")));
					txtEmployerName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employername")));
					txtEmpRT.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employerrt")));
					txtEmpAccount.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employeraccount")));
					txtEmployerID.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employerid")));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("immediateoriginname"))) != string.Empty)
					{
						txtImmediateOriginName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("immediateoriginname")));
					}
					else
					{
						txtImmediateOriginName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employername")));
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("immediateoriginrt"))) != string.Empty)
					{
						txtImmediateOriginRT.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("immediateoriginrt")));
					}
					else
					{
						txtImmediateOriginRT.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("employerrt")));
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("ODFINum"))) != string.Empty)
					{
						txtODFI.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("odfinum")));
					}
					else
					{
						txtODFI.Text = txtImmediateOriginRT.Text;
					}
					int intTemp = 0;
					intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("employerAccountType"))));
					if (intTemp < 20)
					{
						intTemp = 27;
					}
					if (intTemp == 37)
					{
						cmbAccountType.SelectedIndex = 1;
					}
					else
					{
						cmbAccountType.SelectedIndex = 0;
					}
				}
				else
				{
					clsLoad.OpenRecordset("select * from GLOBALVARIABLES", "SystemSettings");
					txtEmployerName.Text = clsLoad.Get_Fields("citytown") + " of " + clsLoad.Get_Fields("muniname");
					cmbAccountType.SelectedIndex = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			boolPreNote = false;
			boolPrenoteFile = false;
			txtFileName.Text = Strings.Replace(txtFileName.Text, "/", "", 1, -1, CompareConstants.vbTextCompare);
			txtFileName.Text = fecherFoundation.Strings.Trim(txtFileName.Text);
			if (boolSetupMode)
			{
				if (SaveInfo())
				{
					MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
					mnuSelect_Click();
				}
			}
			else
			{
				if (fecherFoundation.Strings.Trim(txtFileName.Text) == string.Empty)
				{
					MessageBox.Show("You must specify a file name");
					return;
				}
				if (!boolMadePrenoteFile)
				{
					clsLoad.OpenRecordset("select * from tbldirectdeposit where prenote = 1", "twpy0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						MessageBox.Show("There are entries that are marked as prenote" + "\r\n" + "Make sure to create a prenote file if you have not already done so", "Prenote Entries Exist", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				if (CreateACHFile())
				{
					Close();
                    //FC:FINAL:AM:#3225 - reset the flag
                    modGlobalVariables.Statics.gboolCancelACHForm = false;
                }
			}
		}

		private void mnuSelect_Click(object sender, System.EventArgs e)
		{
			mnuSaveExit.Enabled = false;
			cmdSave.Visible = false;
			boolSetupMode = false;
			framACH.Visible = true;
			framACH.BringToFront();
			FillYearCombo();
		}

		public void mnuSelect_Click()
		{
			mnuSelect_Click(mnuSelect, new System.EventArgs());
		}

		private bool CreateACHFile()
		{
			bool CreateACHFile = false;
			// MADE ALL STRINGS TO THE ACH FILE UPPER CASE BECAUSE OF CALL ID 69990
			// MATTHEW 6/3/2005
			modCoreysSweeterCode.ACHInfo clsAchInfo = new modCoreysSweeterCode.ACHInfo();
			clsDRWrapper clsLoad = new clsDRWrapper();
			FCFileSystem fso = new FCFileSystem();
			StreamWriter tsStream = null;
			int x;
			string strReturn;
			string strSQL = "";
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string, DateTime)
			DateTime dtDate;
			// vbPorter upgrade warning: intPayRun As int	OnWrite(int, string)
			int intPayRun = 0;
			int lngRecNum;
			int lngNumBlocks;
			string strACHFileName = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CreateACHFile = false;
				clsLoad.OpenRecordset("select * from tblachinformation", "twpy0000.vb1");
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("No ACH information.  Cannot create file.", "Missing Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CreateACHFile;
				}
				if (!boolPrenoteFile && ((cboPayDate.Items.Count < 1 || cboPayDate.SelectedIndex < 0) || (cboPayDateYear.Items.Count < 1 || cboPayDateYear.SelectedIndex < 0) || (cboPayRunID.Items.Count < 1 || cboPayRunID.SelectedIndex < 0)))
				{
					MessageBox.Show("You do not have a Pay Date and Payrun ID selected to create the file for.", "No Pay Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateACHFile;
				}
				if (!Information.IsDate(T2KEffectiveEntryDate.Text))
				{
					if (boolPreNote)
					{
						T2KEffectiveEntryDate.Text = FCConvert.ToString(DateTime.Today);
					}
					else
					{
						MessageBox.Show("You must enter a valid Effective Entry Date", "Invalid Entry Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return CreateACHFile;
					}
				}
				else
				{
					dtDate = FCConvert.ToDateTime(T2KEffectiveEntryDate.Text);
					if (dtDate.ToOADate() == 0)
					{
						MessageBox.Show("You must enter a valid Effective Entry Date", "Invalid Entry Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return CreateACHFile;
					}
				}
				clsDDBanks.OpenRecordset("select * from tblbanks order by ID", "twpy0000.vb1");
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (boolPrenoteFile)
				{
					dtDate = DateTime.Today;
					intPayRun = 1;
				}
				else
				{
					dtDate = FCConvert.ToDateTime(cboPayDate.Items[cboPayDate.SelectedIndex].ToString());
					intPayRun = FCConvert.ToInt32(cboPayRunID.Items[cboPayRunID.SelectedIndex].ToString());
				}
				dtPayDateChosen = dtDate;
				intPayRunChosen = intPayRun;
				clsLoad.Edit();
				clsLoad.Set_Fields("effectiveentrydate", Strings.Format(T2KEffectiveEntryDate.Text, "MM/dd/yyyy"));
				clsLoad.Set_Fields("ACHFileName", fecherFoundation.Strings.Trim(txtFileName.Text));
				clsLoad.Update();
				clsAchInfo.ACHBankName = FCConvert.ToString(clsLoad.Get_Fields("achbankname"));
				clsAchInfo.ACHBankRT = FCConvert.ToString(clsLoad.Get_Fields("achbankrt"));
				clsAchInfo.EffectiveDate = FCConvert.ToDateTime(T2KEffectiveEntryDate.Text);
				clsAchInfo.EmployerAccount = FCConvert.ToString(clsLoad.Get_Fields("employeraccount"));
				clsAchInfo.EmployerID = FCConvert.ToString(clsLoad.Get_Fields("employerid"));
				clsAchInfo.EmployerName = FCConvert.ToString(clsLoad.Get_Fields("employername"));
				clsAchInfo.EmployerRT = FCConvert.ToString(clsLoad.Get_Fields("employerrt"));
				clsAchInfo.ImmediateOriginName = FCConvert.ToString(clsLoad.Get_Fields("immediateoriginname"));
				clsAchInfo.ImmediateOriginRT = FCConvert.ToString(clsLoad.Get_Fields("immediateoriginrt"));
				clsAchInfo.ODFI = FCConvert.ToString(clsLoad.Get_Fields("odfinum"));
				if (!boolCreateFile)
				{
					CreateACHFile = true;
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return CreateACHFile;
				}
				boolBalanced = false;
				clsLoad.OpenRecordset("select achbalanced from tbldefaultinformation", "twpy0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("achbalanced")))
					{
						boolBalanced = true;
					}
				}
				strACHFileName = txtFileName.Text;
				if (Strings.InStr(1, strACHFileName, ".", CompareConstants.vbTextCompare) < 1)
				{
					// didn't provide an extension so provide one
					strACHFileName += ".txt";
				}
				// see if the fileexists yet
				if (FCFileSystem.FileExists(strACHFileName))
				{
					FCFileSystem.DeleteFile(strACHFileName);
				}
				// prepare to create file
				tsStream = FCFileSystem.CreateTextFile(strACHFileName);
				boolFileStarted = true;
				lngNumBatches = 0;
				lngRecordBlockCount = 0;
				// get and write file header
				strReturn = CreateACHFileHeader(clsAchInfo);
				tsStream.WriteLine(fecherFoundation.Strings.UCase(strReturn));
				lngRecordBlockCount += 1;
				// get and write batch header
				strReturn = CreateACHBatchHeader(clsAchInfo);
				tsStream.WriteLine(fecherFoundation.Strings.UCase(strReturn));
				lngRecordBlockCount += 1;
				// now get individual employees
				lngTotalAmount = 0;
				dblTotalAmount = 0;
				dblTotalDebits = 0;
				lngHashNumber = 0;
				lngHash8 = 0;
				if (boolPrenoteFile)
				{
					// strSQL = "SELECT tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName + ' ' + tblEmployeeMaster.MiddleName + ' ' + tblEmployeeMaster.LastName + ' ' + tblEmployeeMaster.Desig as EmployeeName, tblDirectDeposit.Account as DDAccountNumber, tblDirectDeposit.Amount, tblDirectDeposit.Type, tblDirectDeposit.AccountType as DDAccountType, tblBanks.BankDepositID as DDBankNumber FROM tblBanks INNER JOIN (tblEmployeeMaster INNER JOIN tblDirectDeposit ON tblEmployeeMaster.EmployeeNumber = tblDirectDeposit.EmployeeNumber) ON tblBanks.ID = tblDirectDeposit.Bank"
					// 02/02/2006 call #87531
					if (!boolOnlyPrenoteEntries)
					{
						strSQL = "SELECT tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName + ' ' + tblEmployeeMaster.MiddleName + ' ' + tblEmployeeMaster.LastName + ' ' + tblEmployeeMaster.Desig as EmployeeName, tblDirectDeposit.Account as DDAccountNumber, tblDirectDeposit.Amount, tblDirectDeposit.Type, tblDirectDeposit.AccountType as DDAccountType,prenote, tblBanks.ID as DDBankNumber FROM tblBanks INNER JOIN (tblEmployeeMaster INNER JOIN tblDirectDeposit ON tblEmployeeMaster.EmployeeNumber = tblDirectDeposit.EmployeeNumber) ON tblBanks.ID = tblDirectDeposit.Bank";
					}
					else
					{
						strSQL = "SELECT tblEmployeeMaster.EmployeeNumber, tblEmployeeMaster.FirstName + ' ' + tblEmployeeMaster.MiddleName + ' ' + tblEmployeeMaster.LastName + ' ' + tblEmployeeMaster.Desig as EmployeeName, tblDirectDeposit.Account as DDAccountNumber, tblDirectDeposit.Amount, tblDirectDeposit.Type, tblDirectDeposit.AccountType as DDAccountType,prenote, tblBanks.ID as DDBankNumber FROM tblBanks INNER JOIN (tblEmployeeMaster INNER JOIN tblDirectDeposit ON tblEmployeeMaster.EmployeeNumber = tblDirectDeposit.EmployeeNumber) ON tblBanks.ID = tblDirectDeposit.Bank where prenote = 1";
					}
				}
				else
				{
					strSQL = "Select employeenumber,employeename,ddbanknumber,ddaccountnumber,DDAccountType,sum(ddamount) as TotAmount from tblcheckdetail where paydate = '" + Strings.Format(dtDate, "MM/dd/yyyy") + "' and payrunid = " + FCConvert.ToString(intPayRun) + " and BankRecord = 1 and checknumber > 0 AND checkvoid = 0 group by employeenumber,employeename,ddbanknumber,ddaccountnumber,ddAccountType order by employeenumber";
				}
				lngRecNum = 0;
				clsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					strReturn = FCConvert.ToString(CreateACHEmployeeDetail(ref clsAchInfo, clsLoad, lngRecNum + 1));
					if (strReturn == string.Empty)
					{
						tsStream.Close();
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return CreateACHFile;
					}
					tsStream.WriteLine(fecherFoundation.Strings.UCase(strReturn));
					lngRecordBlockCount += 1;
					lngRecNum += 1;
					clsLoad.MoveNext();
				}
				// offset record
				if (boolBalanced)
				{
					strReturn = CreateOffsetRecord(ref clsAchInfo, lngRecNum + 1);
					tsStream.WriteLine(fecherFoundation.Strings.UCase(strReturn));
					lngRecordBlockCount += 1;
					lngRecNum += 1;
				}
				// batch control
				// Matthew 07/03/03
				// strreturn = CreateACHBatchControlRecord(clsAchInfo, lngRecNum, lngHashNumber, lngTotalAmount, lngNumBatches)
				strReturn = FCConvert.ToString(CreateACHBatchControlRecord(ref clsAchInfo, ref lngRecNum, ref lngHashNumber, ref dblTotalAmount, ref lngNumBatches, ref dblTotalDebits));
				tsStream.WriteLine(fecherFoundation.Strings.UCase(strReturn));
				lngRecordBlockCount += 1;
				// file control
				lngRecordBlockCount += 1;
				// add one for the file control record itself
				lngNumBlocks = FCConvert.ToInt32(lngRecordBlockCount / 10);
				if (lngRecordBlockCount % 10 > 0)
				{
					lngNumBlocks += 1;
				}
				// Matthew 07/03/03
				// strreturn = CreateACHFileControl(lngNumBatches, lngNumBlocks, lngRecNum, lngHash8, lngTotalAmount)
				strReturn = FCConvert.ToString(CreateACHFileControl(lngNumBatches, lngNumBlocks, lngRecNum, lngHash8, dblTotalAmount, dblTotalDebits));
				tsStream.WriteLine(fecherFoundation.Strings.UCase(strReturn));
				// fillers
				strReturn = CreateACHBlockFillers((10 - (lngRecordBlockCount % 10)) % 10);
				if (strReturn != string.Empty)
				{
					tsStream.Write(fecherFoundation.Strings.UCase(strReturn));
				}
				tsStream.Close();
				boolFileStarted = false;
				CreateACHFile = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (boolPreNote)
				{
					MessageBox.Show("ACH Prenote file created successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("ACH file created.", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
                //FC:FINAL:AM:#2736 - download the file
                FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, strACHFileName));
                boolPreNote = false;
				return CreateACHFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (boolFileStarted)
				{
					tsStream.Close();
					boolFileStarted = false;
				}
				if (fecherFoundation.Information.Err(ex).Number == 52)
				{
					MessageBox.Show("Bad file name" + "\r\n" + "Make sure there are no illegal characters in the file name such as / or *", "Bad File Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateACHFile;
				}
				else if (fecherFoundation.Information.Err(ex).Number == 70)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateACHFile Line " + fecherFoundation.Information.Erl() + "\r\n" + "File: " + strACHFileName + "\r\n" + "Location: " + Environment.CurrentDirectory, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateACHFile Line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return CreateACHFile;
		}

		private string CreateACHFileHeader(modCoreysSweeterCode.ACHInfo clsAchInfo)
		{
			string CreateACHFileHeader = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// create the file header for ACH file
				string strReturn;
				strReturn = "";
				CreateACHFileHeader = "";
				strReturn = "101";
				// file header and priority code
				strReturn += " " + clsAchInfo.ACHBankRT;
				// strreturn = strreturn & "1" & clsAchInfo.EmployerID
				// corey 08/25/04   format calls for "1" and employerid. Not sure why is was changed to blank and ACHBankRT
				// Fleet bank can't use file with achbankrt in this field
				// strReturn = strReturn & " " & clsAchInfo.ACHBankRT
				// MATTHEW CHANGED FOR FLEET BANK IN ACCORDANCE WITH ACH RULE PRINT OUT 10/19/2004
				// strReturn = strReturn & "1" & clsAchInfo.EmployerID
				// MATTHEW CHANGED FOR FRYE ISLAND ON 5/10/2005 DUE TO CALL ID 69990
				// If optImmediateOrigin(0) Then
				// strReturn = strReturn & " " & clsAchInfo.EmployerRT
				// Else
				// strReturn = strReturn & "1" & clsAchInfo.EmployerID
				// End If
				if (clsAchInfo.ImmediateOriginRT.Length < 10)
				{
					strReturn += " " + clsAchInfo.ImmediateOriginRT;
				}
				else
				{
					strReturn += clsAchInfo.ImmediateOriginRT;
				}
				strReturn += Strings.Mid(FCConvert.ToString(DateTime.Today.Year), 3) + Strings.Format(DateTime.Today.Month, "00") + Strings.Format(DateTime.Today.Day, "00");
				// creation date
				strReturn += Strings.Format(DateTime.Now, "HHmm");
				// time of creation
				strReturn += "A";
				// ID modifier
				strReturn += "094";
				// record size
				strReturn += "10";
				// block size
				strReturn += "1";
				// format code
				strReturn += clsAchInfo.ACHBankName;
				// strReturn = strReturn & clsAchInfo.EmployerName
				strReturn += clsAchInfo.ImmediateOriginName;
				strReturn += Strings.StrDup(8, " ");
				CreateACHFileHeader = strReturn;
				return CreateACHFileHeader;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateACHFileHeader");
			}
			return CreateACHFileHeader;
		}

		private string CreateACHBatchHeader(modCoreysSweeterCode.ACHInfo clsAchInfo, int intBatchNumber = 1)
		{
			string CreateACHBatchHeader = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn;
				string strTemp;
				strReturn = "";
				CreateACHBatchHeader = "";
				strReturn = "5";
				// batchheader record
				if (boolBalanced)
				{
					strReturn += "200";
					// service class code
				}
				else
				{
					strReturn += "220";
				}
				strReturn = strReturn;
				strTemp = Strings.Left(clsAchInfo.EmployerName, 16);
				strReturn += strTemp;
				strReturn += Strings.StrDup(20, " ");
				// MATTHEW 12/09/2004
				// strReturn = strReturn & "1" & clsAchInfo.EmployerID
				strReturn += strACHEmployerIDPrefix + clsAchInfo.EmployerID;
				strReturn += "PPD";
				// entry class code
				// corey 08/25/04  Fleet bank wants payroll capitalized
				strReturn += "PAYROLL" + Strings.StrDup(3, " ");
				// strReturn = strReturn & String(6, " ")
				strReturn += Strings.Format(Strings.Mid(FCConvert.ToString(DateTime.Today.Year), 2), "00") + Strings.Format(DateTime.Today.Month, "00") + Strings.Format(DateTime.Today.Day, "00");
				strReturn += Strings.Format(Strings.Mid(FCConvert.ToString(clsAchInfo.EffectiveDate.Year), 2), "00") + Strings.Format(clsAchInfo.EffectiveDate.Month, "00") + Strings.Format(clsAchInfo.EffectiveDate.Day, "00");
				strReturn += Strings.StrDup(3, " ");
				strReturn += "1";
				// originator status code
				// strReturn = strReturn & Left(clsAchInfo.ACHBankRT, 8)
				strReturn += Strings.Left(clsAchInfo.ODFI, 8);
				strReturn += Strings.Format(intBatchNumber, "0000000");
				lngNumBatches += 1;
				CreateACHBatchHeader = strReturn;
				return CreateACHBatchHeader;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Create ACHBatchHeader", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateACHBatchHeader;
		}
		// vbPorter upgrade warning: clsEmployee As object	OnWrite(clsDRWrapper)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object CreateACHEmployeeDetail(ref modCoreysSweeterCode.ACHInfo clsAchInfo, clsDRWrapper clsEmployee, int lngRecordNumber)
		{
			object CreateACHEmployeeDetail = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strTemp = "";
				string strReturn;
				int lngTemp = 0;
				// vbPorter upgrade warning: dblTemp As double	OnWrite(int, string)
				double dblTemp = 0;
				strReturn = "";
				CreateACHEmployeeDetail = "";
				strReturn = "6";
				// employeedetail
				if (fecherFoundation.Strings.Trim(Strings.Left(clsEmployee.Get_Fields("DDaccounttype"), 2)) == string.Empty)
				{
					if (!boolPreNote)
					{
						strReturn += "22";
					}
					else
					{
						strReturn += "23";
					}
				}
				else
				{
					if (!boolPreNote)
					{
						strReturn += Strings.Left(clsEmployee.Get_Fields("DDaccounttype"), 1) + FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTRECORDREGULAR);
						// 22 is check. 23 is prenote checking 32 is savings 33 is prenote savings
					}
					else
					{
						strReturn += Strings.Left(clsEmployee.Get_Fields("DDaccounttype"), 1) + FCConvert.ToString(modCoreysSweeterCode.CNSTACHACCOUNTRECORDPRENOTE);
					}
				}
				if (clsDDBanks.FindFirstRecord("ID", Conversion.Val(clsEmployee.Get_Fields("ddbanknumber"))))
				{
					strTemp = FCConvert.ToString(clsDDBanks.Get_Fields("BANKDEPOSITID"));
				}
				else
				{
				}
				if (fecherFoundation.Strings.Trim(Strings.Replace(strTemp, "0", "", 1, -1, CompareConstants.vbTextCompare)) == string.Empty)
				{
					MessageBox.Show("No DFI number entered for bank " + clsDDBanks.Get_Fields("recordnumber") + " " + clsDDBanks.Get_Fields("name") + "\r\n" + "Cannot continue", "Bad DFI Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CreateACHEmployeeDetail;
				}
				strReturn += Strings.Format(strTemp, "000000000");
				// banknumber
				lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Strings.Format(strTemp, "000000000"), 1, 8))));
				// COREY 11/8/2004   NEED TO use double to handle big enough number
				// If (EleventyBillion - lngHashNumber) < lngTemp Then
				// lngHashNumber = lngTemp - (EleventyBillion - lngHashNumber)
				// Else
				lngHashNumber += lngTemp;
				// End If
				strTemp = Strings.StrDup(17, " ");
				Strings.MidSet(ref strTemp, 1, clsEmployee.Get_Fields("ddaccountnumber"));
				// account number
				strReturn += strTemp;
				// Matthew 07/03/03
				// lngTotalAmount = lngTotalAmount + Int(Val(.Fields("totamount")) * 100)
				// MATTHEW 9/23/2005 CALL ID 78020
				if (chkTestFile.CheckState == CheckState.Checked | boolPreNote)
				{
					dblTemp = 0;
				}
				else
				{
					dblTemp = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsEmployee.Get_Fields("totamount")), "0.00"));
				}
				dblTotalAmount += dblTemp;
				strTemp = Strings.Format(dblTemp * 100, "0000000000");
				// amount
				strReturn += strTemp;
				// MATTHEW 11/22/2005 CALL ID 81943
				// strReturn = strReturn & String(15, " ") 'blank
				strReturn += Strings.Mid(clsEmployee.Get_Fields("EmployeeNumber") + Strings.StrDup(15, " "), 1, 15);
				strTemp = Strings.Mid(clsEmployee.Get_Fields("employeename") + Strings.StrDup(22, " "), 1, 22);
				strReturn += strTemp;
				// name
				strReturn += Strings.StrDup(2, " ");
				// blank
				strReturn += "0";
				// addenda record indicator
				if (clsAchInfo.EmployerName == clsAchInfo.ImmediateOriginName)
				{
					strReturn += Strings.Mid(clsAchInfo.ACHBankRT, 1, 8);
				}
				else
				{
					strReturn += Strings.Mid(clsAchInfo.ImmediateOriginRT, 1, 8);
				}
				strReturn += Strings.Format(lngRecordNumber, "0000000");
				CreateACHEmployeeDetail = strReturn;
				return CreateACHEmployeeDetail;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateACHEmployeeDetail", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateACHEmployeeDetail;
		}

		private string CreateOffsetRecord(ref modCoreysSweeterCode.ACHInfo clsAchInfo, int lngRecordNumber)
		{
			string CreateOffsetRecord = "";
			string strReturn;
			// vbPorter upgrade warning: strTemp As string	OnWrite(int, string)
			string strTemp = "";
			int lngTemp;
			strReturn = "6";
			// offset
			if (!boolPrenoteFile)
			{
				strReturn += FCConvert.ToString(cmbAccountType.ItemData(cmbAccountType.SelectedIndex));
			}
			else
			{
				strTemp = FCConvert.ToString(cmbAccountType.ItemData(cmbAccountType.SelectedIndex));
				strTemp = Strings.Left(strTemp, 1) + "8";
				strReturn += strTemp;
			}
			strReturn += clsAchInfo.EmployerRT;
			strTemp = Strings.Mid(clsAchInfo.EmployerAccount + Strings.StrDup(modCoreysSweeterCode.ACHEmployerAccountLen, " "), 1, modCoreysSweeterCode.ACHEmployerAccountLen);
			lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Strings.Format(clsAchInfo.EmployerRT, "000000000"), 1, 8))));
			lngHashNumber += lngTemp;
			strReturn += strTemp;
			// employer account
			// Matthew 07/03/03
			// strTemp = Format(lngTotalAmount, "0000000000")  'total
			// MATTHEW 6/30/2004
			// strTemp = Format(dblTotalAmount, "0000000000")  'total
			strTemp = Strings.Format(dblTotalAmount * 100, "0000000000");
			// total
			dblTotalDebits += dblTotalAmount;
			strReturn += strTemp;
			// *****************************************************************************************
			// MATTHEW 12/16/2005 CALL ID 83800
			// strReturn = strReturn & String(15, " ") 'blank
			strReturn += Strings.Left(clsAchInfo.EmployerID + Strings.StrDup(15, " "), 15);
			// Employer ID
			// *****************************************************************************************
			strReturn += Strings.Mid(clsAchInfo.EmployerName + Strings.StrDup(22, " "), 1, 22);
			// employer name
			strReturn += Strings.StrDup(2, " ");
			strReturn += "0";
			// addenda record indicator
			// strReturn = strReturn & Mid(clsAchInfo.ACHBankRT, 1, 8)
			strReturn += Strings.Mid(clsAchInfo.ODFI, 1, 8);
			strReturn += Strings.Format(lngRecordNumber, "0000000");
			// next # after last employee detail
			CreateOffsetRecord = strReturn;
			return CreateOffsetRecord;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object CreateACHBatchControlRecord(ref modCoreysSweeterCode.ACHInfo clsAchInfo, ref int lngRecCount, ref double lngHash, ref double lngTotCredits, ref int lngBatchCount, ref double dblTotDebits)
		{
			object CreateACHBatchControlRecord = null;
			string strReturn;
			CreateACHBatchControlRecord = "";
			strReturn = "8";
			// batch control record
			if (boolBalanced)
			{
				strReturn += "200";
				// service class code
			}
			else
			{
				strReturn += "220";
			}
			strReturn += Strings.Format(lngRecCount, "000000");
			// entry count for this batch
			strReturn += Strings.Right(Strings.Format(lngHash, "0000000000"), 10);
			// hash
			lngHash8 += lngHash;
			// corey 11/08/2004
			// strReturn = strReturn & String(12, "0") '?? unused ??
			strReturn += Strings.Format(dblTotDebits * 100, "000000000000");
			strReturn += Strings.Format(lngTotCredits * 100, "000000000000");
			// MATTHEW 12/09/2004
			// strReturn = strReturn & "1" & clsAchInfo.EmployerID
			strReturn += strACHEmployerIDPrefix + clsAchInfo.EmployerID;
			strReturn += Strings.StrDup(19, " ");
			// blank
			strReturn += Strings.StrDup(6, " ");
			// blank
			// strReturn = strReturn & Mid(clsAchInfo.ACHBankRT, 1, 8) 'ach bank r/t first 8 chars
			strReturn += Strings.Left(clsAchInfo.ODFI, 8);
			strReturn += Strings.Format(lngBatchCount, "0000000");
			CreateACHBatchControlRecord = strReturn;
			return CreateACHBatchControlRecord;
		}
		// vbPorter upgrade warning: lngBatchCount As object	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngBlockCount As object	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngEntryCount As object	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngTotCredits As object	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		private object CreateACHFileControl(double lngBatchCount, double lngBlockCount, double lngEntryCount, double lng8Hash, double lngTotCredits, double lngTotalDebits)
		{
			object CreateACHFileControl = null;
			// vbPorter upgrade warning: strReturn As object	OnWrite(string)
			object strReturn;
			CreateACHFileControl = "";
			strReturn = "9";
			// file control
			strReturn = strReturn + Strings.Format(lngBatchCount, "000000");
			// how many batches
			strReturn = strReturn + Strings.Format(lngBlockCount, "000000");
			// how many blocks
			strReturn = strReturn + Strings.Format(lngEntryCount, "00000000");
			// how many "6" records
			strReturn = strReturn + Strings.Right(Strings.Format(lng8Hash, "0000000000"), 10);
			// sum of batch control hashes
			// *********************************************************************************************
			// mattthew 12/09/2004 call id 58646
			// strReturn = strReturn & String(12, "0")
			strReturn = strReturn + Strings.Format(lngTotalDebits * 100, "000000000000");
			// total debits
			// *********************************************************************************************
			strReturn = strReturn + Strings.Format(lngTotCredits * 100, "000000000000");
			// total credits
			strReturn = strReturn + Strings.StrDup(39, " ");
			// blank
			CreateACHFileControl = strReturn;
			return CreateACHFileControl;
		}
		// vbPorter upgrade warning: intNumRecsToFill As object	OnWrite
		private string CreateACHBlockFillers(int intNumRecsToFill)
		{
			string CreateACHBlockFillers = "";
			int x;
			CreateACHBlockFillers = "";
			for (x = 1; x <= FCConvert.ToInt32(intNumRecsToFill); x++)
			{
				CreateACHBlockFillers = CreateACHBlockFillers + Strings.StrDup(94, "9") + Constants.vbNewLine;
			}
			// x
			return CreateACHBlockFillers;
		}

		public string Init(bool boolCreateACHFile, bool boolFirstRun = true)
		{
			string Init = "";
			Init = "0,0";
			dtPayDateChosen = DateTime.FromOADate(0);
			intPayRunChosen = 0;
			boolInitRun = boolFirstRun;
			boolCreateFile = boolCreateACHFile;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = Strings.Format(dtPayDateChosen, "MM/dd/yyyy") + "," + FCConvert.ToString(intPayRunChosen);
			return Init;
		}

        private void frmACHBankInformation_FormClosing(object sender, FormClosingEventArgs e)
        {
            //FC:FINAL:AM:#3225 - don't create reports when closing from the close button
            modGlobalVariables.Statics.gboolCancelACHForm = true;

        }
    }
}
