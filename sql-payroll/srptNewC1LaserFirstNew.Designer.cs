﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptNewC1LaserFirst.
	/// </summary>
	partial class srptNewC1LaserFirst
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptNewC1LaserFirst));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Barcode1 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtWithholdingAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUCEmployerAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtQuarterNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine4FirstMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4SecMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4ThirdMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5FirstMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5SecMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5ThirdMo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonalCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriodStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriodEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSeasonEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEMail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumberofPayees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLicense = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine9b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine9d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtFileBefore = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCEmployerAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarterNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4FirstMo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4SecMo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4ThirdMo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5FirstMo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5SecMo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5ThirdMo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumberofPayees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicense)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFileBefore)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label38,
				this.Label122,
				this.Shape3,
				this.Label43,
				this.Label13,
				this.Label12,
				this.Barcode1,
				this.Label1,
				this.Label2,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.txtWithholdingAccount,
				this.txtUCEmployerAccount,
				this.txtName,
				this.txtLine1,
				this.Label9,
				this.Label10,
				this.txtLine2,
				this.txtLine3,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.txtLine6,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.txtQuarterNum,
				this.Label26,
				this.Label27,
				this.txtLine7,
				this.txtLine8,
				this.txtLine10,
				this.txtLine11,
				this.Label29,
				this.Label30,
				this.txtLine9,
				this.Label33,
				this.Label34,
				this.Label35,
				this.Label36,
				this.Line7,
				this.Label37,
				this.Label39,
				this.Label40,
				this.Label41,
				this.Label42,
				this.Label45,
				this.Label46,
				this.Label47,
				this.Label48,
				this.Label49,
				this.Label50,
				this.Label51,
				this.Label52,
				this.Label53,
				this.Label54,
				this.Label55,
				this.Label56,
				this.Label57,
				this.Label58,
				this.Label59,
				this.Label60,
				this.Label61,
				this.Label62,
				this.Label63,
				this.Label64,
				this.Label66,
				this.Label67,
				this.Line11,
				this.Label73,
				this.Label74,
				this.Label75,
				this.Label76,
				this.txtYear,
				this.txtLine4FirstMo,
				this.txtLine4SecMo,
				this.txtLine4ThirdMo,
				this.txtLine5FirstMo,
				this.txtLine5SecMo,
				this.txtLine5ThirdMo,
				this.txtSeasonalCode,
				this.txtPeriodStart,
				this.txtPeriodEnd,
				this.txtPhone,
				this.Field25,
				this.txtSeasonStart,
				this.txtSeasonEnd,
				this.Field28,
				this.txtEIN,
				this.txtEMail,
				this.txtTitle,
				this.Label77,
				this.txtAddress,
				this.txtCity,
				this.txtState,
				this.txtZip,
				this.Label80,
				this.Label81,
				this.txtNumberofPayees,
				this.Label107,
				this.Label108,
				this.txtDate,
				this.Line21,
				this.Line23,
				this.Label109,
				this.txtLicense,
				this.Line25,
				this.Line28,
				this.txtLine9b,
				this.Field29,
				this.txtLine9d,
				this.Label110,
				this.Label111,
				this.Line2,
				this.Label112,
				this.Label113,
				this.Line3,
				this.Label115,
				this.Label117,
				this.Field33,
				this.Line1,
				this.Label118,
				this.Label119,
				this.Line29,
				this.Line30,
				this.Label120,
				this.Line31,
				this.Label121,
				this.Label123,
				this.Line32,
				this.txtFileBefore,
				this.Label124,
				this.Label125,
				this.Label126,
				this.Label127,
				this.Label128,
				this.Label129,
				this.Shape17,
				this.Shape18,
				this.Shape19,
				this.Label130
			});
			this.Detail.Height = 10.16667F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// Label38
			// 
			this.Label38.Height = 0.5F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 6.1875F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Arial Narrow\'; font-size: 8.5pt; text-align: left; vertical-align: " + "top";
			this.Label38.Text = "Maine Revenue Services P.O. Box 1064              Augusta, ME  04332-1064";
			this.Label38.Top = 8.9375F;
			this.Label38.Width = 1.239583F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1875F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 2.25F;
			this.Label122.MultiLine = false;
			this.Label122.Name = "Label122";
			this.Label122.Style = "font-family: \'Arial\'; font-size: 9pt; font-weight: bold; text-align: center; text" + "-decoration: underline; vertical-align: top";
			this.Label122.Text = "For Paid Preparers Only";
			this.Label122.Top = 8.229167F;
			this.Label122.Width = 3.1875F;
			// 
			// Shape3
			// 
			this.Shape3.Height = 0.3958333F;
			this.Shape3.Left = 3.65F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 9.583333F;
			this.Shape3.Visible = false;
			this.Shape3.Width = 0.9125F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1666667F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 3.7125F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Arial\'; font-size: 8.5pt; text-align: left; vertical-align: top";
			this.Label43.Text = "Office use only";
			this.Label43.Top = 9.729167F;
			this.Label43.Visible = false;
			this.Label43.Width = 0.8333333F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.260417F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Arial\'; font-size: 9pt; font-weight: bold; text-align: left; vertic" + "al-align: middle";
			this.Label13.Text = "Part Two - Unemployment Contributions Report";
			this.Label13.Top = 3.416667F;
			this.Label13.Width = 3.166667F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Arial\'; font-size: 9pt; font-weight: bold; text-align: left; text-d" + "ecoration: underline; vertical-align: middle";
			this.Label12.Text = "Part One - Income Tax Witholding";
			this.Label12.Top = 2.083333F;
			this.Label12.Width = 2.375F;
			// 
			// Barcode1
			// 
			//this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F);
			this.Barcode1.Height = 0.625F;
			this.Barcode1.Left = 5.666667F;
			this.Barcode1.Name = "Barcode1";
			this.Barcode1.QuietZoneBottom = 0F;
			this.Barcode1.QuietZoneLeft = 0F;
			this.Barcode1.QuietZoneRight = 0F;
			this.Barcode1.QuietZoneTop = 0F;
			this.Barcode1.Top = 0F;
			this.Barcode1.Width = 1.583333F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 7.166667F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Courier New\'; font-size: 12pt; font-weight: bold; text-align: right" + "";
			this.Label1.Text = "15";
			this.Label1.Top = 0.1840278F;
			this.Label1.Width = 0.3333333F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 9pt; font-weight: bold; text-align: center";
			this.Label2.Text = "Maine Revenue Services";
			this.Label2.Top = 0F;
			this.Label2.Width = 2.333333F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.208333F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: top";
			this.Label4.Text = "Maine Department of Labor";
			this.Label4.Top = 0.1666667F;
			this.Label4.Width = 2.666667F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 1.791667F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Arial\'; font-size: 9pt; font-weight: bold; text-align: center; vert" + "ical-align: top";
			this.Label5.Text = "COMBINED FILING FOR INCOME TAX WITHHOLDING";
			this.Label5.Top = 0.3333333F;
			this.Label5.Width = 3.666667F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.3333333F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.02083333F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label6.Text = "Withholding Account No:";
			this.Label6.Top = 1.309028F;
			this.Label6.Width = 0.9583333F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.2708333F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.02083333F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label7.Text = "UC Employer Account No:";
			this.Label7.Top = 1.815972F;
			this.Label7.Width = 0.9791667F;
			// 
			// txtWithholdingAccount
			// 
			this.txtWithholdingAccount.Height = 0.1666667F;
			this.txtWithholdingAccount.Left = 0.8854167F;
			this.txtWithholdingAccount.Name = "txtWithholdingAccount";
			this.txtWithholdingAccount.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: middle";
			this.txtWithholdingAccount.Text = "99 999999999";
			this.txtWithholdingAccount.Top = 1.354167F;
			this.txtWithholdingAccount.Width = 1.125F;
			// 
			// txtUCEmployerAccount
			// 
			this.txtUCEmployerAccount.Height = 0.1666667F;
			this.txtUCEmployerAccount.Left = 0.8854167F;
			this.txtUCEmployerAccount.Name = "txtUCEmployerAccount";
			this.txtUCEmployerAccount.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: middle";
			this.txtUCEmployerAccount.Text = "9999999999";
			this.txtUCEmployerAccount.Top = 1.847222F;
			this.txtUCEmployerAccount.Width = 1.125F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 0.6F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top";
			this.txtName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			this.txtName.Top = 0.8472222F;
			this.txtName.Width = 4.25F;
			// 
			// txtLine1
			// 
			this.txtLine1.Height = 0.1666667F;
			this.txtLine1.Left = 5.447917F;
			this.txtLine1.Name = "txtLine1";
			this.txtLine1.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine1.Text = "9999999999999.99";
			this.txtLine1.Top = 2.479167F;
			this.txtLine1.Width = 1.75F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Arial\'; font-size: 9pt; text-align: left; vertical-align: middle";
			this.Label9.Text = "FORM 941/C1-ME";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.2083333F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.895833F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label10.Text = "Period Covered:";
			this.Label10.Top = 1.517361F;
			this.Label10.Width = 1.020833F;
			// 
			// txtLine2
			// 
			this.txtLine2.Height = 0.1666667F;
			this.txtLine2.Left = 5.447917F;
			this.txtLine2.Name = "txtLine2";
			this.txtLine2.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine2.Text = "9999999999999.99";
			this.txtLine2.Top = 2.84375F;
			this.txtLine2.Width = 1.75F;
			// 
			// txtLine3
			// 
			this.txtLine3.Height = 0.1666667F;
			this.txtLine3.Left = 5.447917F;
			this.txtLine3.Name = "txtLine3";
			this.txtLine3.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine3.Text = "9999999999999.99";
			this.txtLine3.Top = 3.177083F;
			this.txtLine3.Width = 1.75F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.25F;
			this.Label14.MultiLine = false;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Arial\'; font-size: 9pt; font-weight: bold; text-align: center; text" + "-decoration: underline; vertical-align: top";
			this.Label14.Text = "Part Three - Calculate the Total Amount Due";
			this.Label14.Top = 6.59375F;
			this.Label14.Width = 3.1875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.3541667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.2291667F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label15.Text = "Maine income tax withheld this quarter (from Schedule 2/C1, line 19b)   (Semi-wee" + "kly filers complete Schedule 1/C1 on reverse side)";
			this.Label15.Top = 2.416667F;
			this.Label15.Width = 3.166667F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.25F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.2291667F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label16.Text = "Less any semi-weekly payments (From Schedule 1, line 13 on reverse side.)  (See i" + "nstructions for Schedule 1/C1 on page 7)";
			this.Label16.Top = 2.75F;
			this.Label16.Width = 3.333333F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1666667F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.2291667F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label17.Text = "Income tax withholding due (line 1 minus line 2)";
			this.Label17.Top = 3.145833F;
			this.Label17.Width = 2.25F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.3333333F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.2291667F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Arial\'; font-size: 6pt; text-align: left; vertical-align: middle";
			this.Label18.Text = resources.GetString("Label18.Text");
			this.Label18.Top = 3.666667F;
			this.Label18.Width = 3.833333F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1666667F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.2291667F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label19.Text = "Number of female employees included on line 4.  If none, enter zero (0)";
			this.Label19.Top = 4.3125F;
			this.Label19.Width = 3.583333F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1666667F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.2291667F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label20.Text = "Total Unemployment Compensation Gross Wages Paid this quarter";
			this.Label20.Top = 4.53125F;
			this.Label20.Width = 3.583333F;
			// 
			// txtLine6
			// 
			this.txtLine6.Height = 0.1666667F;
			this.txtLine6.Left = 5.447917F;
			this.txtLine6.Name = "txtLine6";
			this.txtLine6.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: t" + "op";
			this.txtLine6.Text = "9999999999999.99";
			this.txtLine6.Top = 4.65625F;
			this.txtLine6.Width = 1.75F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.25F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label21.Text = "Taxable wages paid in this quarter (line 6 minus line 7)";
			this.Label21.Top = 5.302083F;
			this.Label21.Width = 3.5625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.25F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label22.Text = "Total Contributions and CSSF assessment due (line 9b plus line 9d)";
			this.Label22.Top = 6.302083F;
			this.Label22.Width = 3.5625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1666667F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.2291667F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label23.Text = "Amount Due with this return (line 3 plus line 10)";
			this.Label23.Top = 6.979167F;
			this.Label23.Width = 3.583333F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1666667F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 4.958333F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 10pt; text-align: left; vertical-align: top";
			this.Label24.Text = "QUARTER #";
			this.Label24.Top = 0.9861111F;
			this.Label24.Width = 0.9375F;
			// 
			// txtQuarterNum
			// 
			this.txtQuarterNum.Height = 0.1666667F;
			this.txtQuarterNum.HyperLink = null;
			this.txtQuarterNum.Left = 5.909722F;
			this.txtQuarterNum.Name = "txtQuarterNum";
			this.txtQuarterNum.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: to" + "p";
			this.txtQuarterNum.Text = "9";
			this.txtQuarterNum.Top = 0.9861111F;
			this.txtQuarterNum.Width = 0.25F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1666667F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.0625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label26.Text = "Signature";
			this.Label26.Top = 7.645833F;
			this.Label26.Width = 0.5416667F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.25F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label27.Text = "UC Contribution rate";
			this.Label27.Top = 5.625F;
			this.Label27.Width = 1.0625F;
			// 
			// txtLine7
			// 
			this.txtLine7.Height = 0.1666667F;
			this.txtLine7.Left = 5.447917F;
			this.txtLine7.Name = "txtLine7";
			this.txtLine7.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine7.Text = "9999999999999.99";
			this.txtLine7.Top = 4.989583F;
			this.txtLine7.Width = 1.75F;
			// 
			// txtLine8
			// 
			this.txtLine8.Height = 0.1666667F;
			this.txtLine8.Left = 5.447917F;
			this.txtLine8.Name = "txtLine8";
			this.txtLine8.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine8.Text = "9999999999999.99";
			this.txtLine8.Top = 5.302083F;
			this.txtLine8.Width = 1.75F;
			// 
			// txtLine10
			// 
			this.txtLine10.Height = 0.1666667F;
			this.txtLine10.Left = 5.447917F;
			this.txtLine10.Name = "txtLine10";
			this.txtLine10.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine10.Text = "9999999999999.99";
			this.txtLine10.Top = 6.302083F;
			this.txtLine10.Width = 1.75F;
			// 
			// txtLine11
			// 
			this.txtLine11.Height = 0.1666667F;
			this.txtLine11.Left = 5.447917F;
			this.txtLine11.Name = "txtLine11";
			this.txtLine11.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: t" + "op";
			this.txtLine11.Text = "9999999999999.99";
			this.txtLine11.Top = 6.979167F;
			this.txtLine11.Width = 1.75F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.125F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0.25F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label29.Text = "DEDUCT EXCESS WAGES (SEE INSTRUCTIONS ON PAGE 6)";
			this.Label29.Top = 4.989583F;
			this.Label29.Width = 3.5F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.125F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.25F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label30.Text = "NOTE: THE TAXABLE WAGE BASE IS $12,000 FOR EACH EMPLOYEE.";
			this.Label30.Top = 5.114583F;
			this.Label30.Width = 3.5F;
			// 
			// txtLine9
			// 
			this.txtLine9.Height = 0.1666667F;
			this.txtLine9.Left = 1.333333F;
			this.txtLine9.Name = "txtLine9";
			this.txtLine9.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine9.Text = ".9999";
			this.txtLine9.Top = 5.677083F;
			this.txtLine9.Width = 0.5625F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0.0625F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label33.Text = "Firm\'s Name (or yours, if self-employed):";
			this.Label33.Top = 8.8125F;
			this.Label33.Width = 1.875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.02083333F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label34.Text = "Title";
			this.Label34.Top = 9.364583F;
			this.Label34.Visible = false;
			this.Label34.Width = 0.375F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 3.885417F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label35.Text = "Date";
			this.Label35.Top = 8.447917F;
			this.Label35.Width = 0.4375F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 5.0625F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label36.Text = "Telephone";
			this.Label36.Top = 8.447917F;
			this.Label36.Width = 0.625F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 1.864583F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 8.958333F;
			this.Line7.Width = 1.989583F;
			this.Line7.X1 = 1.864583F;
			this.Line7.X2 = 3.854167F;
			this.Line7.Y1 = 8.958333F;
			this.Line7.Y2 = 8.958333F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.3125F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 6.25F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Arial Narrow\'; font-size: 8.5pt; font-weight: bold; text-align: lef" + "t; vertical-align: top";
			this.Label37.Text = "If not enclosing a check,";
			this.Label37.Top = 8.6875F;
			this.Label37.Width = 1.1875F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1875F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 6.340278F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Arial Narrow\'; font-size: 8.5pt; font-weight: bold; text-align: lef" + "t; vertical-align: top";
			this.Label39.Text = "MAIL RETURN TO:";
			this.Label39.Top = 8.8125F;
			this.Label39.Width = 1.125F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1875F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 0.0625F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label40.Text = "Address:";
			this.Label40.Top = 9.125F;
			this.Label40.Width = 1.3125F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1666667F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0.3125F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label41.Text = "Paid preparer EIN:";
			this.Label41.Top = 9.638889F;
			this.Label41.Width = 1.083333F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1666667F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 0.1875F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label42.Text = "Seasonal Code";
			this.Label42.Top = 3.322917F;
			this.Label42.Visible = false;
			this.Label42.Width = 0.75F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1666667F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0.0625F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label45.Text = "1.";
			this.Label45.Top = 2.354167F;
			this.Label45.Width = 0.25F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1666667F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 0.0625F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label46.Text = "2.";
			this.Label46.Top = 2.6875F;
			this.Label46.Width = 0.25F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1666667F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 0.0625F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label47.Text = "3.";
			this.Label47.Top = 3.083333F;
			this.Label47.Width = 0.25F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1666667F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 0.0625F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label48.Text = "4.";
			this.Label48.Top = 3.666667F;
			this.Label48.Width = 0.25F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1666667F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 0.0625F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label49.Text = "5.";
			this.Label49.Top = 4.3125F;
			this.Label49.Width = 0.25F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1666667F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 0.0625F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label50.Text = "6.";
			this.Label50.Top = 4.53125F;
			this.Label50.Width = 0.25F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 0.0625F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label51.Text = "7.";
			this.Label51.Top = 4.989583F;
			this.Label51.Width = 0.25F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1875F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 0.0625F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label52.Text = "8.";
			this.Label52.Top = 5.302083F;
			this.Label52.Width = 0.25F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 0.0625F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label53.Text = "9a.";
			this.Label53.Top = 5.625F;
			this.Label53.Width = 0.25F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.1875F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 0.0625F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label54.Text = "10.";
			this.Label54.Top = 6.291667F;
			this.Label54.Width = 0.25F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1666667F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 0.0625F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label55.Text = "11.";
			this.Label55.Top = 7F;
			this.Label55.Width = 0.25F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1666667F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 4.447917F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label56.Text = "1.";
			this.Label56.Top = 2.479167F;
			this.Label56.Width = 0.25F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1666667F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 4.447917F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label57.Text = "2.";
			this.Label57.Top = 2.8125F;
			this.Label57.Width = 0.25F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1666667F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 4.447917F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label58.Text = "3.";
			this.Label58.Top = 3.145833F;
			this.Label58.Width = 0.25F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1666667F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 4.385417F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label59.Text = "6.";
			this.Label59.Top = 4.65625F;
			this.Label59.Width = 0.25F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.1666667F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 4.385417F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label60.Text = "7.";
			this.Label60.Top = 4.989583F;
			this.Label60.Width = 0.25F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.1666667F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 4.385417F;
			this.Label61.Name = "Label61";
			this.Label61.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label61.Text = "8.";
			this.Label61.Top = 5.302083F;
			this.Label61.Width = 0.25F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.1666667F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 4.385417F;
			this.Label62.Name = "Label62";
			this.Label62.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "middle";
			this.Label62.Text = "9d.";
			this.Label62.Top = 5.958333F;
			this.Label62.Width = 0.25F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.1666667F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 4.385417F;
			this.Label63.Name = "Label63";
			this.Label63.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label63.Text = "10.";
			this.Label63.Top = 6.302083F;
			this.Label63.Width = 0.25F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.1666667F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 0.1875F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: middle";
			this.Label64.Text = "Seasonal Period";
			this.Label64.Top = 3.489583F;
			this.Label64.Visible = false;
			this.Label64.Width = 0.8333333F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1666667F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 4.46875F;
			this.Label66.Name = "Label66";
			this.Label66.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label66.Text = "4.";
			this.Label66.Top = 3.979167F;
			this.Label66.Width = 0.1666667F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.1666667F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 4.381945F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "middle";
			this.Label67.Text = "5.";
			this.Label67.Top = 4.298611F;
			this.Label67.Width = 0.25F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 5.791667F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 8.125F;
			this.Line11.Width = 1.510417F;
			this.Line11.X1 = 5.791667F;
			this.Line11.X2 = 7.302083F;
			this.Line11.Y1 = 8.125F;
			this.Line11.Y2 = 8.125F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1666667F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 4.625F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: right; vertical-align: middle";
			this.Label73.Text = "Contact person email:";
			this.Label73.Top = 7.965278F;
			this.Label73.Width = 1.166667F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 4.875F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label74.Text = "1st Month";
			this.Label74.Top = 3.729167F;
			this.Label74.Width = 0.75F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1875F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 5.770833F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label75.Text = "2nd Month";
			this.Label75.Top = 3.729167F;
			this.Label75.Width = 0.75F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.1875F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 6.791667F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label76.Text = "3rd Month";
			this.Label76.Top = 3.729167F;
			this.Label76.Width = 0.625F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.3125F;
			this.txtYear.HyperLink = null;
			this.txtYear.Left = 0.9270833F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Arial\'; font-size: 18pt; font-weight: bold";
			this.txtYear.Text = "2014";
			this.txtYear.Top = 0.3125F;
			this.txtYear.Width = 0.625F;
			// 
			// txtLine4FirstMo
			// 
			this.txtLine4FirstMo.Height = 0.1666667F;
			this.txtLine4FirstMo.Left = 4.708333F;
			this.txtLine4FirstMo.Name = "txtLine4FirstMo";
			this.txtLine4FirstMo.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine4FirstMo.Text = "999999";
			this.txtLine4FirstMo.Top = 3.979167F;
			this.txtLine4FirstMo.Width = 0.8333333F;
			// 
			// txtLine4SecMo
			// 
			this.txtLine4SecMo.Height = 0.1666667F;
			this.txtLine4SecMo.Left = 5.614583F;
			this.txtLine4SecMo.Name = "txtLine4SecMo";
			this.txtLine4SecMo.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine4SecMo.Text = "999999";
			this.txtLine4SecMo.Top = 3.979167F;
			this.txtLine4SecMo.Width = 0.8333333F;
			// 
			// txtLine4ThirdMo
			// 
			this.txtLine4ThirdMo.Height = 0.1666667F;
			this.txtLine4ThirdMo.Left = 6.569445F;
			this.txtLine4ThirdMo.Name = "txtLine4ThirdMo";
			this.txtLine4ThirdMo.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine4ThirdMo.Text = "999999";
			this.txtLine4ThirdMo.Top = 3.979167F;
			this.txtLine4ThirdMo.Width = 0.75F;
			// 
			// txtLine5FirstMo
			// 
			this.txtLine5FirstMo.Height = 0.1666667F;
			this.txtLine5FirstMo.Left = 4.708333F;
			this.txtLine5FirstMo.Name = "txtLine5FirstMo";
			this.txtLine5FirstMo.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine5FirstMo.Text = "999999";
			this.txtLine5FirstMo.Top = 4.298611F;
			this.txtLine5FirstMo.Width = 0.8333333F;
			// 
			// txtLine5SecMo
			// 
			this.txtLine5SecMo.Height = 0.1666667F;
			this.txtLine5SecMo.Left = 5.614583F;
			this.txtLine5SecMo.Name = "txtLine5SecMo";
			this.txtLine5SecMo.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine5SecMo.Text = "999999";
			this.txtLine5SecMo.Top = 4.298611F;
			this.txtLine5SecMo.Width = 0.8333333F;
			// 
			// txtLine5ThirdMo
			// 
			this.txtLine5ThirdMo.Height = 0.1666667F;
			this.txtLine5ThirdMo.Left = 6.569445F;
			this.txtLine5ThirdMo.Name = "txtLine5ThirdMo";
			this.txtLine5ThirdMo.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine5ThirdMo.Text = "999999";
			this.txtLine5ThirdMo.Top = 4.298611F;
			this.txtLine5ThirdMo.Width = 0.75F;
			// 
			// txtSeasonalCode
			// 
			this.txtSeasonalCode.Height = 0.1666667F;
			this.txtSeasonalCode.Left = 1.069444F;
			this.txtSeasonalCode.Name = "txtSeasonalCode";
			this.txtSeasonalCode.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" + "ddle";
			this.txtSeasonalCode.Text = "99";
			this.txtSeasonalCode.Top = 3.298611F;
			this.txtSeasonalCode.Visible = false;
			this.txtSeasonalCode.Width = 0.25F;
			// 
			// txtPeriodStart
			// 
			this.txtPeriodStart.Height = 0.1666667F;
			this.txtPeriodStart.Left = 5.048611F;
			this.txtPeriodStart.Name = "txtPeriodStart";
			this.txtPeriodStart.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" + "ddle";
			this.txtPeriodStart.Text = "99 99 9999";
			this.txtPeriodStart.Top = 1.489583F;
			this.txtPeriodStart.Width = 1F;
			// 
			// txtPeriodEnd
			// 
			this.txtPeriodEnd.Height = 0.1666667F;
			this.txtPeriodEnd.Left = 6.381945F;
			this.txtPeriodEnd.Name = "txtPeriodEnd";
			this.txtPeriodEnd.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" + "ddle";
			this.txtPeriodEnd.Text = "99 99 9999";
			this.txtPeriodEnd.Top = 1.493056F;
			this.txtPeriodEnd.Width = 1F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1875F;
			this.txtPhone.Left = 3.666667F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: to" + "p";
			this.txtPhone.Text = "999 999 9999";
			this.txtPhone.Top = 7.9375F;
			this.txtPhone.Width = 1.125F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1666667F;
			this.Field25.Left = 6.126389F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; vertical-align:" + " top";
			this.Field25.Text = "to";
			this.Field25.Top = 1.166667F;
			this.Field25.Visible = false;
			this.Field25.Width = 0.25F;
			// 
			// txtSeasonStart
			// 
			this.txtSeasonStart.Height = 0.1666667F;
			this.txtSeasonStart.Left = 1.069444F;
			this.txtSeasonStart.Name = "txtSeasonStart";
			this.txtSeasonStart.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: bo" + "ttom";
			this.txtSeasonStart.Text = "99-99-99";
			this.txtSeasonStart.Top = 3.489583F;
			this.txtSeasonStart.Visible = false;
			this.txtSeasonStart.Width = 0.8333333F;
			// 
			// txtSeasonEnd
			// 
			this.txtSeasonEnd.Height = 0.1666667F;
			this.txtSeasonEnd.Left = 2.1625F;
			this.txtSeasonEnd.Name = "txtSeasonEnd";
			this.txtSeasonEnd.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: bo" + "ttom";
			this.txtSeasonEnd.Text = "99-99-99";
			this.txtSeasonEnd.Top = 3.489583F;
			this.txtSeasonEnd.Visible = false;
			this.txtSeasonEnd.Width = 0.9166667F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.1666667F;
			this.Field28.Left = 1.9375F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; vertical-align:" + " top";
			this.Field28.Text = "-";
			this.Field28.Top = 3.489583F;
			this.Field28.Visible = false;
			this.Field28.Width = 0.1875F;
			// 
			// txtEIN
			// 
			this.txtEIN.Height = 0.1666667F;
			this.txtEIN.Left = 1.430556F;
			this.txtEIN.Name = "txtEIN";
			this.txtEIN.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: to" + "p";
			this.txtEIN.Text = "99 9999999";
			this.txtEIN.Top = 9.638889F;
			this.txtEIN.Width = 1.75F;
			// 
			// txtEMail
			// 
			this.txtEMail.Height = 0.1875F;
			this.txtEMail.Left = 5.8F;
			this.txtEMail.Name = "txtEMail";
			this.txtEMail.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: to" + "p";
			this.txtEMail.Text = "XXXXXXXXXXXXXXXX";
			this.txtEMail.Top = 7.9375F;
			this.txtEMail.Visible = false;
			this.txtEMail.Width = 1.55F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1875F;
			this.txtTitle.Left = 0.4270833F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: to" + "p";
			this.txtTitle.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXX";
			this.txtTitle.Top = 9.364583F;
			this.txtTitle.Visible = false;
			this.txtTitle.Width = 2.375F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1666667F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 1.958333F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "font-family: \'Arial\'; font-size: 9pt; font-weight: bold; text-align: center; vert" + "ical-align: top";
			this.Label77.Text = "AND UNEMPLOYMENT CONTRIBUTIONS";
			this.Label77.Top = 0.5F;
			this.Label77.Width = 3.333333F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1666667F;
			this.txtAddress.Left = 0.2020833F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top";
			this.txtAddress.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			this.txtAddress.Top = 0.9791667F;
			this.txtAddress.Visible = false;
			this.txtAddress.Width = 3.5F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.1666667F;
			this.txtCity.Left = 0.6F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top";
			this.txtCity.Text = "XXXXXXXXXXXXXXXXXXXXXX";
			this.txtCity.Top = 1F;
			this.txtCity.Visible = false;
			this.txtCity.Width = 2.583333F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1666667F;
			this.txtState.Left = 2.4F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top";
			this.txtState.Text = "ME";
			this.txtState.Top = 1F;
			this.txtState.Visible = false;
			this.txtState.Width = 0.25F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.1666667F;
			this.txtZip.Left = 2.8F;
			this.txtZip.Name = "txtZip";
			this.txtZip.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top";
			this.txtZip.Text = "99999";
			this.txtZip.Top = 1F;
			this.txtZip.Visible = false;
			this.txtZip.Width = 0.5F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.1666667F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 0.01041667F;
			this.Label80.Name = "Label80";
			this.Label80.Style = "font-family: \'Courier New\'";
			this.Label80.Text = "TSC";
			this.Label80.Top = 9.986111F;
			this.Label80.Width = 0.4166667F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1666667F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 4.385417F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "top";
			this.Label81.Text = "11.";
			this.Label81.Top = 6.979167F;
			this.Label81.Width = 0.25F;
			// 
			// txtNumberofPayees
			// 
			this.txtNumberofPayees.Height = 0.1666667F;
			this.txtNumberofPayees.Left = 6.583333F;
			this.txtNumberofPayees.Name = "txtNumberofPayees";
			this.txtNumberofPayees.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
			this.txtNumberofPayees.Text = "99999";
			this.txtNumberofPayees.Top = 2.145833F;
			this.txtNumberofPayees.Visible = false;
			this.txtNumberofPayees.Width = 0.5F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.3333333F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 4.958333F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "font-family: \'Arial Narrow\'; font-size: 7pt";
			this.Label107.Text = "Number of payees subject to Maine income tax withholding";
			this.Label107.Top = 2.083333F;
			this.Label107.Visible = false;
			this.Label107.Width = 1.1875F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.1666667F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 4.6875F;
			this.Label108.Name = "Label108";
			this.Label108.Style = "font-size: 7pt; text-align: right; vertical-align: middle";
			this.Label108.Text = "A";
			this.Label108.Top = 2.076389F;
			this.Label108.Visible = false;
			this.Label108.Width = 0.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.9375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: to" + "p";
			this.txtDate.Text = "99 99 9999";
			this.txtDate.Top = 7.625F;
			this.txtDate.Width = 1.3125F;
			// 
			// Line21
			// 
			this.Line21.Height = 0F;
			this.Line21.Left = 4.125F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 8.625F;
			this.Line21.Width = 0.9444447F;
			this.Line21.X1 = 4.125F;
			this.Line21.X2 = 5.069445F;
			this.Line21.Y1 = 8.625F;
			this.Line21.Y2 = 8.625F;
			// 
			// Line23
			// 
			this.Line23.Height = 0F;
			this.Line23.Left = 0.5208333F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 9.3125F;
			this.Line23.Width = 3.333333F;
			this.Line23.X1 = 0.5208333F;
			this.Line23.X2 = 3.854167F;
			this.Line23.Y1 = 9.3125F;
			this.Line23.Y2 = 9.3125F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.2916667F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 4.5625F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label109.Text = "Maine Payroll Processor License Number";
			this.Label109.Top = 9.583333F;
			this.Label109.Width = 1.239583F;
			// 
			// txtLicense
			// 
			this.txtLicense.Height = 0.1666667F;
			this.txtLicense.Left = 5.805555F;
			this.txtLicense.Name = "txtLicense";
			this.txtLicense.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: to" + "p";
			this.txtLicense.Text = "9999999";
			this.txtLicense.Top = 9.638889F;
			this.txtLicense.Width = 1.125F;
			// 
			// Line25
			// 
			this.Line25.Height = 0F;
			this.Line25.Left = 5.625F;
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 8.625F;
			this.Line25.Width = 1.739583F;
			this.Line25.X1 = 5.625F;
			this.Line25.X2 = 7.364583F;
			this.Line25.Y1 = 8.625F;
			this.Line25.Y2 = 8.625F;
			// 
			// Line28
			// 
			this.Line28.Height = 0F;
			this.Line28.Left = 2.260417F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 3.572917F;
			this.Line28.Width = 2.84375F;
			this.Line28.X1 = 2.260417F;
			this.Line28.X2 = 5.104167F;
			this.Line28.Y1 = 3.572917F;
			this.Line28.Y2 = 3.572917F;
			// 
			// txtLine9b
			// 
			this.txtLine9b.Height = 0.1666667F;
			this.txtLine9b.Left = 5.447917F;
			this.txtLine9b.Name = "txtLine9b";
			this.txtLine9b.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine9b.Text = "9999999999999.99";
			this.txtLine9b.Top = 5.625F;
			this.txtLine9b.Width = 1.75F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1666667F;
			this.Field29.Left = 0.6666667F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Courier New\'; font-size: 8.5pt; font-weight: bold; text-align: righ" + "t; vertical-align: middle";
			this.Field29.Text = ".0006";
			this.Field29.Top = 5.958333F;
			this.Field29.Width = 0.5625F;
			// 
			// txtLine9d
			// 
			this.txtLine9d.Height = 0.1666667F;
			this.txtLine9d.Left = 5.447917F;
			this.txtLine9d.Name = "txtLine9d";
			this.txtLine9d.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; vertical-align: m" + "iddle";
			this.txtLine9d.Text = "9999999999999.99";
			this.txtLine9d.Top = 5.96875F;
			this.txtLine9d.Width = 1.75F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1666667F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 4.385417F;
			this.Label110.Name = "Label110";
			this.Label110.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; vertical-align: " + "middle";
			this.Label110.Text = "9b.";
			this.Label110.Top = 5.625F;
			this.Label110.Width = 0.25F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1875F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 2.5F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label111.Text = "UC Contributions due (line 8 times line 9a)";
			this.Label111.Top = 5.625F;
			this.Label111.Width = 2F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 6.5625F;
			this.Line2.Visible = false;
			this.Line2.Width = 7.5F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 6.5625F;
			this.Line2.Y2 = 6.5625F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1875F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 0.25F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label112.Text = "CSSF rate";
			this.Label112.Top = 5.958333F;
			this.Label112.Width = 0.5625F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1875F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 0.0625F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label113.Text = "9c.";
			this.Label113.Top = 5.958333F;
			this.Label113.Width = 0.25F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.6020833F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 7.791667F;
			this.Line3.Width = 4.15F;
			this.Line3.X1 = 0.6020833F;
			this.Line3.X2 = 4.752083F;
			this.Line3.Y1 = 7.791667F;
			this.Line3.Y2 = 7.791667F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1875F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 1.979167F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: center; vert" + "ical-align: top";
			this.Label115.Text = "See Instructions for Electronic Filing and Payment Requirements and Options";
			this.Label115.Top = 7.201389F;
			this.Label115.Width = 3.75F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 2.4375F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label117.Text = "CSSF Assessment due (line 8 times line 9c)";
			this.Label117.Top = 5.958333F;
			this.Label117.Width = 2.0625F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.1666667F;
			this.Field33.Left = 0.25F;
			this.Field33.Name = "Field33";
			this.Field33.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: middle";
			this.Field33.Text = "Note: The CSSF Assessment does not apply to direct reimbursable employers. See in" + "structions.";
			this.Field33.Top = 6.09375F;
			this.Field33.Width = 4.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3.409722F;
			this.Line1.Visible = false;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 3.409722F;
			this.Line1.Y2 = 3.409722F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1875F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 0.1041667F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: center; vert" + "ical-align: top";
			this.Label118.Text = "Under penalties of perjury, I certify that the information contained on this retu" + "rn, report and attachment(s) is true and correct.";
			this.Label118.Top = 7.354167F;
			this.Label118.Width = 6F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.1666667F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 5.375F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: right; vertical-align: middle";
			this.Label119.Text = "Date:";
			this.Label119.Top = 7.645833F;
			this.Label119.Width = 0.3541667F;
			// 
			// Line29
			// 
			this.Line29.Height = 0F;
			this.Line29.Left = 5.777778F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 7.791667F;
			this.Line29.Width = 1.524306F;
			this.Line29.X1 = 5.777778F;
			this.Line29.X2 = 7.302083F;
			this.Line29.Y1 = 7.791667F;
			this.Line29.Y2 = 7.791667F;
			// 
			// Line30
			// 
			this.Line30.Height = 0F;
			this.Line30.Left = 0.6041667F;
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 8.125F;
			this.Line30.Width = 2.447917F;
			this.Line30.X1 = 0.6041667F;
			this.Line30.X2 = 3.052083F;
			this.Line30.Y1 = 8.125F;
			this.Line30.Y2 = 8.125F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1666667F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 0.0625F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label120.Text = "Print Name:";
			this.Label120.Top = 7.96875F;
			this.Label120.Width = 0.6041667F;
			// 
			// Line31
			// 
			this.Line31.Height = 0F;
			this.Line31.Left = 3.604167F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 8.125F;
			this.Line31.Width = 1.197917F;
			this.Line31.X1 = 3.604167F;
			this.Line31.X2 = 4.802083F;
			this.Line31.Y1 = 8.125F;
			this.Line31.Y2 = 8.125F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1666667F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 3.0625F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label121.Text = "Telephone:";
			this.Label121.Top = 7.965278F;
			this.Label121.Width = 0.6041667F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1875F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 0.0625F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: middle";
			this.Label123.Text = "Paid Preparer\'s Signature:";
			this.Label123.Top = 8.447917F;
			this.Label123.Width = 1.25F;
			// 
			// Line32
			// 
			this.Line32.Height = 0F;
			this.Line32.Left = 1.3125F;
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 8.625F;
			this.Line32.Width = 2.541667F;
			this.Line32.X1 = 1.3125F;
			this.Line32.X2 = 3.854167F;
			this.Line32.Y1 = 8.625F;
			this.Line32.Y2 = 8.625F;
			// 
			// txtFileBefore
			// 
			this.txtFileBefore.Height = 0.1666667F;
			this.txtFileBefore.Left = 5.048611F;
			this.txtFileBefore.Name = "txtFileBefore";
			this.txtFileBefore.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; vertical-align: mi" + "ddle";
			this.txtFileBefore.Text = "99 99 9999";
			this.txtFileBefore.Top = 1.815972F;
			this.txtFileBefore.Width = 1F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.2083333F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 3.895833F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-family: \'Arial\'; font-size: 8.5pt; font-weight: bold; text-align: left; vert" + "ical-align: top";
			this.Label124.Text = "File On or Before:";
			this.Label124.Top = 1.815972F;
			this.Label124.Width = 1.020833F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.3125F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 3.9375F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-family: \'Arial Narrow\'; font-size: 8.5pt; font-weight: bold; text-align: cen" + "ter; vertical-align: top";
			this.Label125.Text = "If enclosing a check, make check payable to:";
			this.Label125.Top = 8.6875F;
			this.Label125.Width = 2.375F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.5F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 4.625F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-family: \'Arial Narrow\'; font-size: 8.5pt; text-align: left; vertical-align: " + "top";
			this.Label126.Text = "Maine Revenue Services P.O. Box 1065              Augusta, ME  04332-1065";
			this.Label126.Top = 9.0625F;
			this.Label126.Width = 1.239583F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1875F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 4.5F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-family: \'Arial Narrow\'; font-size: 8.5pt; font-weight: bold; text-align: lef" + "t; vertical-align: top";
			this.Label127.Text = "and MAIL WITH RETURN TO:";
			this.Label127.Top = 8.9375F;
			this.Label127.Width = 1.4375F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1875F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 4.625F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-family: \'Arial Narrow\'; font-size: 8.5pt; text-align: left; vertical-align: " + "top";
			this.Label128.Text = "Treasurer, State of Maine";
			this.Label128.Top = 8.8125F;
			this.Label128.Width = 1.1875F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.2083333F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 0.02083333F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "font-family: \'Arial\'; font-size: 7pt; font-weight: bold; text-align: left; vertic" + "al-align: top";
			this.Label129.Text = "Name:";
			this.Label129.Top = 0.8541667F;
			this.Label129.Width = 0.3958333F;
			// 
			// Shape17
			// 
			this.Shape17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Shape17.Height = 0.125F;
			this.Shape17.Left = 0.01388889F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 0.01388889F;
			this.Shape17.Width = 0.125F;
			// 
			// Shape18
			// 
			this.Shape18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Shape18.Height = 0.125F;
			this.Shape18.Left = 0F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 9.777778F;
			this.Shape18.Width = 0.125F;
			// 
			// Shape19
			// 
			this.Shape19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Shape19.Height = 0.125F;
			this.Shape19.Left = 7.361111F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 9.777778F;
			this.Shape19.Width = 0.125F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1666667F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 0.2291667F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-family: \'Arial\'; font-size: 7pt; text-align: left; vertical-align: top";
			this.Label130.Text = "(from Schedule 2/C1, line 19a)";
			this.Label130.Top = 4.697917F;
			this.Label130.Width = 3.583333F;
			// 
			// srptNewC1LaserFirst
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUCEmployerAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuarterNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4FirstMo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4SecMo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4ThirdMo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5FirstMo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5SecMo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5ThirdMo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeasonEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumberofPayees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicense)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFileBefore)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWithholdingAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUCEmployerAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtQuarterNum;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4FirstMo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4SecMo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4ThirdMo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5FirstMo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5SecMo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5ThirdMo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeasonalCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodStart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodEnd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeasonStart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSeasonEnd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEMail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumberofPayees;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicense;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine9b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine9d;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFileBefore;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
	}
}
