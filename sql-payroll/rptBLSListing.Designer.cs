﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptBLSListing.
	/// </summary>
	partial class rptBLSListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBLSListing));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployee});
            this.Detail.Height = 0.1666667F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // txtEmployee
            // 
            this.txtEmployee.Height = 0.1666667F;
            this.txtEmployee.Left = 0.34375F;
            this.txtEmployee.Name = "txtEmployee";
            this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtEmployee.Text = "Field1";
            this.txtEmployee.Top = 0F;
            this.txtEmployee.Width = 4.46875F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGrandTotal,
            this.Field5});
            this.ReportFooter.Height = 0.4375F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.Height = 0.1666667F;
            this.txtGrandTotal.Left = 2.233333F;
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.txtGrandTotal.Text = "Field1";
            this.txtGrandTotal.Top = 0.2333333F;
            this.txtGrandTotal.Width = 0.9333333F;
            // 
            // Field5
            // 
            this.Field5.Height = 0.2F;
            this.Field5.Left = 0.3F;
            this.Field5.Name = "Field5";
            this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Field5.Text = "Grand Total of all Employees:";
            this.Field5.Top = 0.2333333F;
            this.Field5.Width = 1.8F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtTime,
            this.txtPage});
            this.PageHeader.Height = 0.6770833F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.2083333F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
            this.Label1.Text = "Payroll -  BLS Worksite ID Report";
            this.Label1.Top = 0F;
            this.Label1.Width = 6.9375F;
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.1875F;
            this.txtMuniName.Left = 0F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.txtMuniName.Text = null;
            this.txtMuniName.Top = 0F;
            this.txtMuniName.Width = 1.5625F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.2083333F;
            this.txtDate.Left = 5.65625F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1.3125F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1666667F;
            this.txtTime.Left = 0F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.2083333F;
            this.txtTime.Width = 1.3125F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1666667F;
            this.txtPage.Left = 5.65625F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.2083333F;
            this.txtPage.Width = 1.3125F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroup,
            this.Field1,
            this.Line1});
            this.GroupHeader1.DataField = "Binder";
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(GroupHeader1_Format);
            // 
            // txtGroup
            // 
            this.txtGroup.Height = 0.1666667F;
            this.txtGroup.Left = 0.8666667F;
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtGroup.Text = "Field1";
            this.txtGroup.Top = 0.03333334F;
            this.txtGroup.Width = 2.933333F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.1666667F;
            this.Field1.Left = 0.1333333F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Field1.Text = "BLS Code:";
            this.Field1.Top = 0.03333334F;
            this.Field1.Width = 0.7F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.1333333F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2333333F;
            this.Line1.Width = 6.8F;
            this.Line1.X1 = 0.1333333F;
            this.Line1.X2 = 6.933333F;
            this.Line1.Y1 = 0.2333333F;
            this.Line1.Y2 = 0.2333333F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotal,
            this.Field3,
            this.Line2});
            this.GroupFooter1.Height = 0.5520833F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
            // 
            // txtTotal
            // 
            this.txtTotal.Height = 0.1666667F;
            this.txtTotal.Left = 1.466667F;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.txtTotal.Text = "Field1";
            this.txtTotal.Top = 0.06666667F;
            this.txtTotal.Width = 0.9333333F;
            // 
            // Field3
            // 
            this.Field3.Height = 0.2F;
            this.Field3.Left = 0.3F;
            this.Field3.Name = "Field3";
            this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Field3.Text = "Total Employees:";
            this.Field3.Top = 0.06666667F;
            this.Field3.Width = 1.1F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.3F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.03333334F;
            this.Line2.Width = 2.1F;
            this.Line2.X1 = 0.3F;
            this.Line2.X2 = 2.4F;
            this.Line2.Y1 = 0.03333334F;
            this.Line2.Y2 = 0.03333334F;
            // 
            // rptBLSListing
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.75F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.979167F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.rptBLSListing_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
