﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsErrorHandlers
	{
		//=========================================================
		public void SetErrorHandler()
		{
			// these variables are needed because once the program runs the 'On Error'
			// statement inside this proceedure then all of the information from the
			// real error is lost.
			// vbPorter upgrade warning: ErrorDescription As object	OnWrite(string)
			// vbPorter upgrade warning: ErrorNumber As object	OnWrite
			object ErrorDescription, ErrorNumber;
			string ErrorSource;
			ErrorNumber = fecherFoundation.Information.Err().Number;
			ErrorDescription = fecherFoundation.Information.Err().Description;
			ErrorSource = fecherFoundation.Information.Err().Source;
			// vbPorter upgrade warning: strStack As object	OnWrite(string())
			string[] strStack;
			string strCallStack = "";
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			frmError.InstancePtr.RichTextBox1.Text = string.Empty;
			strStack = Strings.Split(modErrorHandler.Statics.gstrStack, ";;", -1, CompareConstants.vbBinaryCompare);
			for (intCounter = 1; intCounter <= (Information.UBound(strStack, 1)); intCounter++)
			{
				frmError.InstancePtr.RichTextBox1.Text = frmError.InstancePtr.RichTextBox1.Text + ((object[])strStack)[intCounter] + "\r\n";
			}
			frmError.InstancePtr.Label1[0].Text = "Error has occured in:   " + fecherFoundation.Strings.Trim(ErrorSource);
			frmError.InstancePtr.Label1[1].Text = "Active routine is:          " + (strStack)[Information.UBound(strStack, 1)];
			frmError.InstancePtr.Label1[2].Text = "Error description is:       " + fecherFoundation.Strings.Trim(FCConvert.ToString(ErrorDescription));
			frmError.InstancePtr.Label1[3].Text = "Error number is:            " + fecherFoundation.Strings.Trim(FCConvert.ToString(ErrorNumber));
			// frmError.Label1(4) = "Line number is:                    " & Erl
			frmError.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}
	}
}
