//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWPY0000
{
	public partial class frmContractWages : BaseForm
	{
		public frmContractWages()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmContractWages InstancePtr
		{
			get
			{
				return (frmContractWages)Sys.GetInstance(typeof(frmContractWages));
			}
		}

		protected frmContractWages _InstancePtr = null;
		//=========================================================
		private int intGridColumn;
		private bool boolASC;
		private bool boolGridSort;
		private bool pboolLoad;
		// vbPorter upgrade warning: intDataChanged As int	OnWrite(DialogResult, int)
		private DialogResult intDataChanged;
		private bool boolCantEdit;

		private void frmContractWages_Activated(object sender, System.EventArgs e)
		{
			// Call ForceFormToResize(Me)
		}

		private void frmContractWages_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// if the key press is ESC then close out this form
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmContractWages_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmContractWages properties;
			//frmContractWages.ScaleWidth	= 9105;
			//frmContractWages.ScaleHeight	= 6615;
			//frmContractWages.LinkTopic	= "Form1";
			//frmContractWages.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				boolCantEdit = false;
				// vsElasticLight1.Enabled = True
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this);
				boolGridSort = false;
				LoadProcessGrid();
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.EMPLOYEEEDITSAVE)) != "F")
				{
					boolCantEdit = true;
					mnuSave.Enabled = false;
					mnuSaveExit.Enabled = false;
					vsGrid.Col = -1;
					vsGrid.Row = -1;
					vsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void LoadProcessGrid()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "LoadProcessGrid";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsDistributions = new clsDRWrapper();
				vsGrid.Clear();
				vsGrid.Rows = 1;
				vsGrid.Cols = 7;
				vsGrid.FixedRows = 1;
				vsGrid.FixedCols = 3;
				//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsGrid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsGrid.ColDataType(6, FCGrid.DataTypeSettings.flexDTBoolean);
				vsGrid.TextMatrix(0, 1, "Emp #");
				vsGrid.TextMatrix(0, 2, "Employee Name");
				vsGrid.TextMatrix(0, 3, "Contract Amount");
				vsGrid.TextMatrix(0, 4, "# Pay Periods");
				vsGrid.TextMatrix(0, 5, "Actual Periods");
				vsGrid.TextMatrix(0, 6, "Pay Off");
				vsGrid.ColHidden(0, true);
				// .ColHidden(6) = True
				// get all of those that have a regular pay category already assigned
				rsDistributions.OpenRecordset("Select * from tblPayrollDistribution where CAT = 1");
				rsData.OpenRecordset("Select * from tblEmployeeMaster where Status <> 'Terminated' and status <> 'Resigned' Order by EmployeeNumber", modGlobalVariables.DEFAULTDATABASE);
				while (!rsData.EndOfFile())
				{
					vsGrid.Rows += 1;
					vsGrid.TextMatrix(vsGrid.Rows - 1, 1, FCConvert.ToString(rsData.Get_Fields("EmployeeNumber")));
					vsGrid.TextMatrix(vsGrid.Rows - 1, 2, rsData.Get_Fields_String("LastName") + ", " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("Desig"));
					if (rsDistributions.FindFirstRecord("EmployeeNumber", rsData.Get_Fields("EmployeeNumber")))
					{
						vsGrid.TextMatrix(vsGrid.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields_Double("ContractAmount")));
						vsGrid.TextMatrix(vsGrid.Rows - 1, 4, FCConvert.ToString(rsData.Get_Fields_Double("ContractPeriods")));
						vsGrid.TextMatrix(vsGrid.Rows - 1, 5, FCConvert.ToString(rsData.Get_Fields_Double("ContractActualPeriods")));
					}
					else
					{
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsGrid.Rows - 1, 3, vsGrid.Rows - 1, 6, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					rsData.MoveNext();
				}
				vsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				if (vsGrid.Rows >= 2)
				{
					vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vsGrid.Rows - 1, 5, FCGrid.AlignmentSettings.flexAlignRightCenter);
					// if there are records in the grid then edit the grid and set focus to the first row.
					vsGrid.Select(1, 3);
					vsGrid.EditCell();
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmContractWages_Resize(object sender, System.EventArgs e)
		{
			// Adjust the widths of the columns to be a
			// percentage of the grid with itself
			vsGrid.ColWidth(0, vsGrid.WidthOriginal * 0);
			vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.1));
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.31));
			vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
			vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
			if (pboolLoad)
			{
				vsGrid.ColWidth(5, vsGrid.WidthOriginal * 0);
				vsGrid.ColWidth(6, vsGrid.WidthOriginal * 0);
			}
			else
			{
				vsGrid.ColWidth(5, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
				vsGrid.ColWidth(6, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.1));
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			SaveChanges();
			if (intDataChanged == (DialogResult)2 && !boolCantEdit)
			{
				e.Cancel = true;
				return;
			}
			//MDIParent.InstancePtr.Show();
			// set focus back to the menu options of the MDIParent
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0 && !boolCantEdit)
				{
					intDataChanged = MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intDataChanged == DialogResult.Yes)
					{
						// save all changes
						mnuSave_Click();
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			// vbPorter upgrade warning: intCounter2 As int	OnWriteFCConvert.ToInt32(
			int intCounter2;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsDistributions = new clsDRWrapper();
			clsDRWrapper rsPeriods = new clsDRWrapper();
			string strEmployeesChanged = "";
			int int998PayCode = 0;
			int int999PayCode = 0;
			rsData.OpenRecordset("Select * from tblPayCodes where PayCode = '998'");
			if (rsData.EndOfFile())
			{
				int998PayCode = 1;
			}
			else
			{
				int998PayCode = FCConvert.ToInt16(rsData.Get_Fields("ID"));
			}
			rsData.OpenRecordset("Select * from tblPayCodes where PayCode = '999'");
			if (rsData.EndOfFile())
			{
				int999PayCode = 1;
			}
			else
			{
				int999PayCode = FCConvert.ToInt16(rsData.Get_Fields("ID"));
			}
			vsGrid.Select(0, 0);
			if (pboolLoad)
			{
				rsData.OpenRecordset("Select * from tblEmployeeMaster");
				for (intCounter = 1; intCounter <= (vsGrid.Rows - 1); intCounter++)
				{
					NextEmployee:
					;
					if (vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 3) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						// THERE IS A CHANCE THAT THEY COULD DEFINE A DISTRIBUTION CAT 1, ENTER IN A CONTRACT
						// AMOUNT, THEN REMOVE THAT DISTRIBUTION. NEED TO CLEAR OUT THOSE VALUES IN THE MASTER
						// TABLE IF THEIR IS NO DISTRIBUTION WITH CAT = 1
						if (rsData.FindFirstRecord("EmployeeNumber", vsGrid.TextMatrix(intCounter, 1)))
						{
							rsData.Edit();
							rsData.Set_Fields("ContractAmount", 0);
							rsData.Set_Fields("ContractPeriods", 0);
							rsData.Set_Fields("ContractActualPeriods", 0);
							rsData.Update();
						}
					}
					else
					{
						// GET THE EMPLOYEE MASTER RECORDS
						if (rsData.FindFirstRecord("EmployeeNumber", vsGrid.TextMatrix(intCounter, 1)))
						{
							// ONLY WANT TO UPDATE THE DISTRIBUTION IF ANY OF THE THREE FIELDS HAVE CHANGED.
							if (rsData.Get_Fields_Double("ContractAmount") != Conversion.Val(vsGrid.TextMatrix(intCounter, 3)) || rsData.Get_Fields_Double("ContractPeriods") != Conversion.Val(vsGrid.TextMatrix(intCounter, 4)) || rsData.Get_Fields_Double("ContractActualPeriods") != Conversion.Val(vsGrid.TextMatrix(intCounter, 5)) && Conversion.Val(vsGrid.TextMatrix(intCounter, 3)) != 0)
							{
								strEmployeesChanged += ";" + vsGrid.TextMatrix(intCounter, 1);
								rsData.Edit();
								rsData.Set_Fields("ContractAmount", FCConvert.ToString(Conversion.Val(vsGrid.TextMatrix(intCounter, 3))));
								rsData.Set_Fields("ContractPeriods", FCConvert.ToString(Conversion.Val(vsGrid.TextMatrix(intCounter, 4))));
								rsData.Set_Fields("ContractActualPeriods", FCConvert.ToString(Conversion.Val(vsGrid.TextMatrix(intCounter, 5))));
								rsData.Update();
								// NOW UPDATE THE FIRST ROW IN THE DISTRIBUTION GRID
								if (Conversion.Val(vsGrid.TextMatrix(intCounter, 3)) != 0)
								{
									rsDistributions.OpenRecordset("Select * from tblPayrollDistribution where CAT = 1 and EmployeeNumber = '" + vsGrid.TextMatrix(intCounter, 1) + "' Order by RecordNumber");
									if (!rsDistributions.EndOfFile())
									{
										rsDistributions.Edit();
										if (Conversion.Val(vsGrid.TextMatrix(intCounter, 4)) == 0)
										{
											rsDistributions.Set_Fields("BaseRate", 0);
											// THIS IS THE TO
											rsDistributions.Set_Fields("Gross", 0);
										}
										else
										{
											rsDistributions.Set_Fields("BaseRate", Strings.Format(Conversion.Val(vsGrid.TextMatrix(intCounter, 3)) / Conversion.Val(vsGrid.TextMatrix(intCounter, 4)), "0.0000"));
											rsDistributions.Set_Fields("Gross", Strings.Format(Conversion.Val(vsGrid.TextMatrix(intCounter, 3)) / Conversion.Val(vsGrid.TextMatrix(intCounter, 4)), "0.0000"));
										}
										rsDistributions.Set_Fields("DefaultHours", 1);
										rsDistributions.Set_Fields("HoursWeek", 1);
										rsDistributions.Set_Fields("CD", int998PayCode);
										rsDistributions.Update();
									}
								}
							}
						}
					}
				}
			}
			else
			{
				// NOW EXECUTE THE PAY OFF ROUTINE AND CREATE THE S(X) CHECKS
				rsPeriods.OpenRecordset("Select * from tblEmployeeMaster");
				for (intCounter = 1; intCounter <= (vsGrid.Rows - 1); intCounter++)
				{
					if (vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 3) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
					}
					else
					{
						if (Conversion.Val(vsGrid.TextMatrix(intCounter, 6)) != 0)
						{
							// CREATE THE PAY OFF ROWS
							int intCheckNumber;
							int intExistingS1s;
							// vbPorter upgrade warning: intChecks As int	OnWrite(string)
							int intChecks = 0;
							// vbPorter upgrade warning: intMainCheckAmount As double	OnWrite(string)
							double intMainCheckAmount = 0;
							double intLastCheckAmount = 0;
							clsDRWrapper rsDistributionCheck = new clsDRWrapper();
							clsDRWrapper rsRegularDistribution = new clsDRWrapper();
							clsDRWrapper rsMaxRecordNumber = new clsDRWrapper();
							//DAO.Field FieldName = new DAO.Field();
							int intMaxRecordNumber = 0;
							// GE
							rsMaxRecordNumber.OpenRecordset("Select Max(RecordNumber) as MaxRecordNumber from tblPayrollDistribution where EmployeeNumber = '" + vsGrid.TextMatrix(intCounter, 1) + "'");
							if (rsMaxRecordNumber.EndOfFile())
							{
								intMaxRecordNumber = 0;
							}
							else
							{
								intMaxRecordNumber = rsMaxRecordNumber.Get_Fields("MaxRecordNumber") + 1;
							}
							intChecks = FCConvert.ToInt32(vsGrid.TextMatrix(intCounter, 5));
							if (Conversion.Val(vsGrid.TextMatrix(intCounter, 3)) == 0 || Conversion.Val(vsGrid.TextMatrix(intCounter, 4)) == 0)
								goto NextCheck;
							intMainCheckAmount = FCConvert.ToDouble(Strings.Format(Conversion.Val(vsGrid.TextMatrix(intCounter, 3)) / Conversion.Val(vsGrid.TextMatrix(intCounter, 4)), "0.0000"));
							intLastCheckAmount = Conversion.Val(vsGrid.TextMatrix(intCounter, 3)) - FCConvert.ToDouble(Strings.Format(intMainCheckAmount * (Conversion.Val(vsGrid.TextMatrix(intCounter, 4)) - 1), "0.0000"));
							for (intCheckNumber = intChecks; intCheckNumber <= FCConvert.ToInt32(vsGrid.TextMatrix(intCounter, 4)) - 1; intCheckNumber++)
							{
								for (intExistingS1s = 4; intExistingS1s <= 13; intExistingS1s++)
								{
									rsDistributionCheck.OpenRecordset("Select * from tblPayrollDistribution where EmployeeNumber = '" + vsGrid.TextMatrix(intCounter, 1) + "' AND AccountCode = '" + FCConvert.ToString(intExistingS1s) + "'");
									if (rsDistributionCheck.EndOfFile())
										break;
								}
								if (intExistingS1s <= 13)
								{
									strEmployeesChanged += ";" + vsGrid.TextMatrix(intCounter, 1);
									rsDistributionCheck.OpenRecordset("Select * from tblPayrollDistribution where EmployeeNumber = '99999999A'");
									rsDistributionCheck.AddNew();
									rsRegularDistribution.OpenRecordset("Select * from tblPayrollDistribution where CAT = 1 and EmployeeNumber = '" + vsGrid.TextMatrix(intCounter, 1) + "' Order by RecordNumber");
									for (intCounter2 = 0; intCounter2 <= (rsRegularDistribution.FieldsCount - 1); intCounter2++)
									{
										if (rsRegularDistribution.Get_FieldsIndexName(intCounter2) == "ID")
										{
											// dop nothing
										}
										else if (rsRegularDistribution.Get_FieldsIndexName(intCounter2) == "RecordNumber")
										{
											rsDistributionCheck.Set_Fields(rsRegularDistribution.Get_FieldsIndexName(intCounter2), intMaxRecordNumber);
											intMaxRecordNumber += 1;
										}
										else if (rsRegularDistribution.Get_FieldsIndexName(intCounter2) == "CD")
										{
											rsDistributionCheck.Set_Fields("CD", int999PayCode);
										}
										else
										{
											rsDistributionCheck.Set_Fields(rsRegularDistribution.Get_FieldsIndexName(intCounter2), rsRegularDistribution.Get_Fields(rsRegularDistribution.Get_FieldsIndexName(intCounter2)));
										}
									}
									// For Each FieldName In rsRegularDistribution.AllFields
									// If FieldName.Name = "ID" Then
									// ElseIf FieldName.Name = "RecordNumber" Then
									// rsDistributionCheck.Fields(FieldName.Name) = intMaxRecordNumber
									// intMaxRecordNumber = intMaxRecordNumber + 1
									// ElseIf FieldName.Name = "CD" Then
									// rsDistributionCheck.Fields("CD") = int999PayCode
									// Else
									// rsDistributionCheck.Fields(FieldName.Name) = rsRegularDistribution.Fields(FieldName.Name)
									// End If
									// Next
									if (intCheckNumber == FCConvert.ToDouble(vsGrid.TextMatrix(intCounter, 5)))
									{
										rsDistributionCheck.Set_Fields("BaseRate", intLastCheckAmount);
									}
									else
									{
										rsDistributionCheck.Set_Fields("BaseRate", intMainCheckAmount);
									}
									rsDistributionCheck.Set_Fields("AccountCode", intExistingS1s);
									rsDistributionCheck.Set_Fields("DefaultHours", 1);
									rsDistributionCheck.Set_Fields("HoursWeek", 1);
									rsDistributionCheck.Update();
								}
								else
								{
									MessageBox.Show("Ten S(x) checks have already been created for employee: " + vsGrid.TextMatrix(intCounter, 2) + ". No other pay out checks could be created.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									goto NextCheck;
								}
							}
						}
					}
					NextCheck:
					;
					// still need to save the actual pay periods.
					if (rsPeriods.FindFirstRecord("EmployeeNumber", vsGrid.TextMatrix(intCounter, 1)))
					{
						rsPeriods.Edit();
						rsPeriods.Set_Fields("ContractActualPeriods", FCConvert.ToString(Conversion.Val(vsGrid.TextMatrix(intCounter, 5))));
						rsPeriods.Update();
					}
				}
			}
			intDataChanged = 0;
			MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			mnuExit_Click();
		}

		private void vsGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsGrid.MouseRow == 0)
			{
				vsGrid.Select(1, 0, vsGrid.Rows - 1, vsGrid.Cols - 1);
				vsGrid.ColSel = vsGrid.MouseCol;
				vsGrid.Col = vsGrid.MouseCol;
				if (boolGridSort)
				{
					vsGrid.Sort = (FCGrid.SortSettings)1;
				}
				else
				{
					vsGrid.Sort = (FCGrid.SortSettings)2;
				}
				boolGridSort = !boolGridSort;
				vsGrid.Select(0, 0);
			}
		}

		private void vsGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (vsGrid.Col == 3 || vsGrid.Col == 4 || vsGrid.Col == 5)
			{
				if (KeyAscii == 8)
					return;
				if ((KeyAscii < FCConvert.ToInt32(Keys.D0) || KeyAscii > FCConvert.ToInt32(Keys.D9)) && KeyAscii != 46)
				{
					KeyAscii = 0;
				}
			}
			intDataChanged = (DialogResult)1;
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsGrid_RowColChange(object sender, System.EventArgs e)
		{
			if (vsGrid.Col < 0 || vsGrid.Row < 0)
				return;
			if (vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsGrid.Row, vsGrid.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
			{
				vsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else if (!boolCantEdit)
			{
				vsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsGrid.EditCell();
			}
		}

		public void Init(bool boolLoad)
		{
			pboolLoad = boolLoad;
			this.Show(App.MainForm);
		}

		private void vsGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsGrid.Col == 4 || vsGrid.Col == 5)
			{
				if (vsGrid.Row >= 1)
				{
					if (Conversion.Val(vsGrid.TextMatrix(vsGrid.Row, vsGrid.Col)) > 52 || Conversion.Val(vsGrid.EditText) > 52)
					{
						MessageBox.Show("Periods cannot be greater then 52.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsGrid.TextMatrix(vsGrid.Row, vsGrid.Col, string.Empty);
						e.Cancel = true;
					}
				}
			}
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSave_Click(cmdSave, EventArgs.Empty);
        }
    }
}
