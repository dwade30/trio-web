//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmPurgeDatabaseData.
	/// </summary>
	partial class frmPurgeDatabaseData
	{
		public fecherFoundation.FCComboBox cboYear;
		public fecherFoundation.FCButton cmdPurge;
		public fecherFoundation.FCTextBox txtWarning2;
		public fecherFoundation.FCTextBox txtWarning;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPurge;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cboYear = new fecherFoundation.FCComboBox();
            this.cmdPurge = new fecherFoundation.FCButton();
            this.txtWarning2 = new fecherFoundation.FCTextBox();
            this.txtWarning = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPurge = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 360);
            this.BottomPanel.Size = new System.Drawing.Size(421, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdPurge);
            this.ClientArea.Controls.Add(this.cboYear);
            this.ClientArea.Controls.Add(this.txtWarning2);
            this.ClientArea.Controls.Add(this.txtWarning);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(421, 300);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(421, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(403, 30);
            this.HeaderText.Text = "Purge Payroll Database Information";
            // 
            // cboYear
            // 
            this.cboYear.AutoSize = false;
            this.cboYear.BackColor = System.Drawing.SystemColors.Window;
            this.cboYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(30, 123);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(132, 40);
            this.cboYear.TabIndex = 3;
            // 
            // cmdPurge
            // 
            this.cmdPurge.AppearanceKey = "acceptButton";
            this.cmdPurge.Location = new System.Drawing.Point(30, 243);
            this.cmdPurge.Name = "cmdPurge";
            this.cmdPurge.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPurge.Size = new System.Drawing.Size(132, 48);
            this.cmdPurge.TabIndex = 1;
            this.cmdPurge.Text = "Purge Data";
            this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
            // 
            // txtWarning2
            // 
            this.txtWarning2.Appearance = 0;
            this.txtWarning2.AutoSize = false;
            this.txtWarning2.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtWarning2.ForeColor = System.Drawing.Color.FromArgb(0, 0, 255);
            this.txtWarning2.LinkItem = null;
            this.txtWarning2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtWarning2.LinkTopic = null;
            this.txtWarning2.Location = new System.Drawing.Point(30, 183);
            this.txtWarning2.Multiline = true;
            this.txtWarning2.Name = "txtWarning2";
            this.txtWarning2.Size = new System.Drawing.Size(363, 40);
            this.txtWarning2.TabIndex = 4;
            this.txtWarning2.Text = "Text1";
            this.txtWarning2.Visible = false;
            // 
            // txtWarning
            // 
            this.txtWarning.Appearance = 0;
            this.txtWarning.AutoSize = false;
            this.txtWarning.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtWarning.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.txtWarning.LinkItem = null;
            this.txtWarning.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtWarning.LinkTopic = null;
            this.txtWarning.Location = new System.Drawing.Point(30, 30);
            this.txtWarning.Multiline = true;
            this.txtWarning.Name = "txtWarning";
            this.txtWarning.Size = new System.Drawing.Size(363, 40);
            this.txtWarning.TabIndex = 5;
            this.txtWarning.Text = "Text1";
            this.txtWarning.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 90);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(363, 19);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "CHOOSE THE FIRST YEAR OF DATA THAT YOU WANT TO KEEP";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPurge,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPurge
            // 
            this.mnuPurge.Index = 0;
            this.mnuPurge.Name = "mnuPurge";
            this.mnuPurge.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPurge.Text = "Purge Data";
            this.mnuPurge.Click += new System.EventHandler(this.mnuPurge_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmPurgeDatabaseData
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(421, 468);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPurgeDatabaseData";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Purge Payroll Database Information";
            this.Load += new System.EventHandler(this.frmPurgeDatabaseData_Load);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmPurgeDatabaseData_KeyUp);
            this.Resize += new System.EventHandler(this.frmPurgeDatabaseData_Resize);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}