﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public partial class frmUpdateDeductions : BaseForm
	{
		public frmUpdateDeductions()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.txtAmount.AllowOnlyNumericInput(true);
			this.txtMaxAmount.AllowOnlyNumericInput(true);
        }

		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmUpdateDeductions InstancePtr
		{
			get
			{
				return (frmUpdateDeductions)Sys.GetInstance(typeof(frmUpdateDeductions));
			}
		}

		protected frmUpdateDeductions _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cUpdateDeductionsView theView;
		private bool boolRefreshing;
		const int CNSTColEmployeeNumber = 0;
		const int CNSTColEmployeeName = 1;

		private void cmdUpdate_Click(object sender, System.EventArgs e)
		{
			UpdateDeductions();
		}

		private void frmUpdateDeductions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmUpdateDeductions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUpdateDeductions properties;
			//frmUpdateDeductions.FillStyle	= 0;
			//frmUpdateDeductions.ScaleWidth	= 9300;
			//frmUpdateDeductions.ScaleHeight	= 7635;
			//frmUpdateDeductions.LinkTopic	= "Form2";
			//frmUpdateDeductions.LockControls	= -1  'True;
			//frmUpdateDeductions.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			theView = new cUpdateDeductionsView();
			//FC:FINAL:DSE:#i2260 Add events to the object
			theView.AmountTypesChanged += theView_AmountTypesChanged;
			theView.CodeOnesChanged += theView_CodeOnesChanged;
			theView.CodeTwosChanged += theView_CodeTwosChanged;
			theView.CurrentDeductionChanged += theView_CurrentDeductionChanged;
			theView.DeductionsChanged += theView_DeductionsChanged;
			theView.DepartmentsChanged += theView_DepartmentsChanged;
			theView.FilteredEmployeesChanged += theView_FilteredEmployeesChanged;
			theView.FrequenciesChanged += theView_FrequenciesChanged;
			theView.FullTimesChanged += theView_FullTimesChanged;
			theView.GroupIDsChanged += theView_GroupIDsChanged;
			theView.LimitTypesChanged += theView_LimitTypesChanged;
			theView.PercentTypesChanged += theView_PercentTypesChanged;
			theView.SequencesChanged += theView_SequencesChanged;
			GridEmployees.TextMatrix(0, CNSTColEmployeeName, "Employee Name");
			theView.InitializeValues(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void theView_FrequenciesChanged()
		{
			ShowFrequencies();
		}

		private void cmbCodeOne_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				intindex = cmbCodeOne.SelectedIndex;
				theView.CurrentCodeOneIndex = intindex + 1;
				boolRefreshing = false;
			}
		}

		private void cmbCodeTwo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				intindex = cmbCodeTwo.SelectedIndex;
				theView.CurrentCodeTwoIndex = intindex + 1;
				boolRefreshing = false;
			}
		}

		private void cmbDepartment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				intindex = cmbDepartment.SelectedIndex;
				theView.CurrentDepartmentIndex = intindex + 1;
				boolRefreshing = false;
			}
		}

		private void ShowDeductions()
		{
			cGenericCollection listDeds;
			// vbPorter upgrade warning: strTemp As string	OnWrite(int, string)
			string strTemp = "";
			boolRefreshing = true;
			listDeds = theView.Deductions;
			cmbDeductionType.Clear();
			if (!(listDeds == null))
			{
				listDeds.MoveFirst();
				cDeductionSetup empDed;
				while (listDeds.IsCurrent())
				{
					empDed = (cDeductionSetup)listDeds.GetCurrentItem();
					strTemp = FCConvert.ToString(empDed.DeductionNumber);
					if (strTemp.Length < 5)
					{
						strTemp = Strings.Left(strTemp + "    ", 4);
					}
					cmbDeductionType.AddItem(strTemp + "  " + empDed.Description);
					cmbDeductionType.ItemData(cmbDeductionType.NewIndex, empDed.ID);
					listDeds.MoveNext();
				}
			}
			boolRefreshing = false;
		}

		private void ShowDepartments()
		{
			cGenericCollection listDepts;
			boolRefreshing = true;
			listDepts = theView.Departments;
			cmbDepartment.Clear();
			if (!(listDepts == null))
			{
				listDepts.MoveFirst();
				string strTemp = "";
				while (listDepts.IsCurrent())
				{
					strTemp = FCConvert.ToString(listDepts.GetCurrentItem());
					cmbDepartment.AddItem(strTemp);
					listDepts.MoveNext();
				}
			}
			boolRefreshing = false;
		}

		private void ShowCodeOnes()
		{
			cGenericCollection listCodeOnes;
			boolRefreshing = true;
			listCodeOnes = theView.CodeOnes;
			listCodeOnes.MoveFirst();
			string strTemp = "";
			while (listCodeOnes.IsCurrent())
			{
				strTemp = FCConvert.ToString(listCodeOnes.GetCurrentItem());
				cmbCodeOne.AddItem(strTemp);
				listCodeOnes.MoveNext();
			}
			boolRefreshing = false;
		}

		private void ShowCodeTwos()
		{
			cGenericCollection listCodeTwos;
			boolRefreshing = true;
			listCodeTwos = theView.CodeTwos;
			listCodeTwos.MoveFirst();
			string strTemp = "";
			while (listCodeTwos.IsCurrent())
			{
				strTemp = FCConvert.ToString(listCodeTwos.GetCurrentItem());
				cmbCodeTwo.AddItem(strTemp);
				listCodeTwos.MoveNext();
			}
			boolRefreshing = false;
		}

		private void ShowSequences()
		{
			cGenericCollection listSequences;
			boolRefreshing = true;
			listSequences = theView.Sequences;
			listSequences.MoveFirst();
			string strTemp = "";
			while (listSequences.IsCurrent())
			{
				strTemp = FCConvert.ToString(listSequences.GetCurrentItem());
				cmbSequence.AddItem(strTemp);
				listSequences.MoveNext();
			}
			boolRefreshing = false;
		}

		private void ShowGroups()
		{
			cGenericCollection listGroups;
			boolRefreshing = true;
			listGroups = theView.Groups;
			listGroups.MoveFirst();
			string strTemp = "";
			while (listGroups.IsCurrent())
			{
				strTemp = FCConvert.ToString(listGroups.GetCurrentItem());
				cmbGroupIDs.AddItem(strTemp);
				listGroups.MoveNext();
			}
			boolRefreshing = false;
		}

		private void ShowPartFullTimes()
		{
			cGenericCollection listpftimes;
			boolRefreshing = true;
			listpftimes = theView.FullTimes;
			listpftimes.MoveFirst();
			string strTemp = "";
			while (listpftimes.IsCurrent())
			{
				strTemp = FCConvert.ToString(listpftimes.GetCurrentItem());
				cmbFullTime.AddItem(strTemp);
				listpftimes.MoveNext();
			}
			boolRefreshing = false;
		}

		private void frmUpdateDeductions_Resize(object sender, System.EventArgs e)
		{
			ResizeGridEmployees();
		}

		private void cmbFullTime_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				theView.CurrentFullTimePartTime = (cmbFullTime.SelectedIndex + 1);
				boolRefreshing = false;
			}
		}

		private void cmbGroupIDs_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				theView.CurrentGroupIDIndex = (cmbGroupIDs.SelectedIndex + 1);
				boolRefreshing = false;
			}
		}

		private void cmbDeductionType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				int lngID = 0;
				intindex = cmbDeductionType.SelectedIndex;
				if (intindex >= 0)
				{
					lngID = cmbDeductionType.ItemData(intindex);
					theView.SetCurrentDeduction(lngID);
				}
				boolRefreshing = false;
			}
		}

		private void cmbSequence_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefreshing)
			{
				boolRefreshing = true;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				int lngID;
				intindex = cmbSequence.SelectedIndex;
				theView.CurrentSequenceIndex = intindex + 1;
				boolRefreshing = false;
			}
		}

		private void theView_AmountTypesChanged()
		{
			ShowAmountTypes();
		}

		private void theView_CodeOnesChanged()
		{
			ShowCodeOnes();
		}

		private void theView_CodeTwosChanged()
		{
			ShowCodeTwos();
		}

		private void theView_CurrentDeductionChanged()
		{
			int lngID;
			int lngOldID = 0;
			lngID = theView.CurrentDeduction;
			if (cmbDeductionType.SelectedIndex >= 0)
			{
				lngOldID = cmbDeductionType.ItemData(cmbDeductionType.SelectedIndex);
			}
			else
			{
				lngOldID = 0;
			}
			if (lngOldID != lngID)
			{
				boolRefreshing = true;
				if (lngID > 0)
				{
					int x;
					for (x = 0; x <= cmbDeductionType.Items.Count - 1; x++)
					{
						if (cmbDeductionType.ItemData(x) == lngID)
						{
							cmbDeductionType.SelectedIndex = x;
							break;
						}
					}
					// x
				}
				else
				{
					cmbDeductionType.SelectedIndex = -1;
				}
				boolRefreshing = false;
			}
		}

		private void theView_DepartmentsChanged()
		{
			ShowDepartments();
		}

		private void theView_FilteredEmployeesChanged()
		{
			ShowEmployees();
		}

		private void theView_FullTimesChanged()
		{
			ShowPartFullTimes();
		}

		private void theView_GroupIDsChanged()
		{
			ShowGroups();
		}

		private void theView_DeductionsChanged()
		{
			ShowDeductions();
		}

		private void theView_LimitTypesChanged()
		{
			ShowLimitTypes();
		}

		private void theView_PercentTypesChanged()
		{
			ShowPercentTypes();
		}

		private void theView_SequencesChanged()
		{
			ShowSequences();
		}

		private void txtAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= 0 && KeyAscii <= (Keys)30)
			{
			}
			else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Decimal))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMaxAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= 0 && KeyAscii <= (Keys)30)
			{
			}
			else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Decimal))
			{
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void ShowEmployees()
		{
			GridEmployees.Rows = 1;
			if (theView.FilteredEmployees.ItemCount() > 0)
			{
				theView.FilteredEmployees.MoveFirst();
				cEmployee emp;
				int lngRow;
				while (theView.FilteredEmployees.IsCurrent())
				{
					emp = (cEmployee)theView.FilteredEmployees.GetCurrentItem();
					GridEmployees.Rows += 1;
					lngRow = GridEmployees.Rows - 1;
					GridEmployees.TextMatrix(lngRow, CNSTColEmployeeNumber, emp.EmployeeNumber);
					GridEmployees.TextMatrix(lngRow, CNSTColEmployeeName, emp.FullName);
					theView.FilteredEmployees.MoveNext();
				}
			}
		}

		private void ResizeGridEmployees()
		{
			int lngGridWidth;
			lngGridWidth = GridEmployees.WidthOriginal;
			GridEmployees.ColWidth(CNSTColEmployeeNumber, FCConvert.ToInt32(0.2 * lngGridWidth));
		}

		private void ShowAmountTypes()
		{
			cGenericCollection listAmountTypes;
			string strTemp = "";
			listAmountTypes = theView.AmountTypes;
			cmbAmountType.Clear();
			if (!(listAmountTypes == null))
			{
				listAmountTypes.MoveFirst();
				cCodeDescription cd;
				while (listAmountTypes.IsCurrent())
				{
					cd = (cCodeDescription)listAmountTypes.GetCurrentItem();
					cmbAmountType.AddItem(cd.Description);
					cmbAmountType.ItemData(cmbAmountType.NewIndex, cd.ID);
					listAmountTypes.MoveNext();
				}
			}
		}

		private void ShowFrequencies()
		{
			cGenericCollection listFrequencies;
			cCodeDescription cd;
			listFrequencies = theView.Frequencies;
			cmbFrequency.Clear();
			if (!(listFrequencies == null))
			{
				listFrequencies.MoveFirst();
				while (listFrequencies.IsCurrent())
				{
					cd = (cCodeDescription)listFrequencies.GetCurrentItem();
					cmbFrequency.AddItem(cd.Description);
					cmbFrequency.ItemData(cmbFrequency.NewIndex, cd.ID);
					listFrequencies.MoveNext();
				}
			}
		}

		private void ShowPercentTypes()
		{
			cGenericCollection listPercentTypes;
			cCodeDescription cd;
			listPercentTypes = theView.PercentTypes;
			cmbPercentType.Clear();
			if (!(listPercentTypes == null))
			{
				listPercentTypes.MoveFirst();
				while (listPercentTypes.IsCurrent())
				{
					cd = (cCodeDescription)listPercentTypes.GetCurrentItem();
					cmbPercentType.AddItem(cd.Description);
					cmbPercentType.ItemData(cmbPercentType.NewIndex, cd.ID);
					listPercentTypes.MoveNext();
				}
			}
		}

		private void ShowLimitTypes()
		{
			cGenericCollection listLimitTypes;
			cCodeDescription cd;
			listLimitTypes = theView.LimitTypes;
			cmbMaxType.Clear();
			if (!(listLimitTypes == null))
			{
				listLimitTypes.MoveFirst();
				while (listLimitTypes.IsCurrent())
				{
					cd = (cCodeDescription)listLimitTypes.GetCurrentItem();
					cmbMaxType.AddItem(cd.Description);
					cmbMaxType.ItemData(cmbMaxType.NewIndex, cd.ID);
					listLimitTypes.MoveNext();
				}
			}
		}

		private bool UpdateDeductions()
		{
			bool UpdateDeductions = false;
			bool boolUseAmount = false;
			double dblAmount = 0;
			string strAmountType = "";
			string strPercentType = "";
			bool boolUseLimit = false;
			// vbPorter upgrade warning: dblLimit As double	OnWrite(string)
			double dblLimit = 0;
			string strLimitType = "";
			bool boolUseFrequency = false;
			int lngFrequency = 0;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.Trim(txtAmount.Text) != "")
				{
					if (!Information.IsNumeric(txtAmount.Text))
					{
						MessageBox.Show("Amount must be blank or a valid number", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateDeductions;
					}
					if (cmbAmountType.SelectedIndex >= 0)
					{
						if (cmbAmountType.ItemData(cmbAmountType.SelectedIndex) == 2)
						{
							if (cmbPercentType.SelectedIndex < 0)
							{
								MessageBox.Show("You must select a percentage type the percent is specifying", "Invalid Percent Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return UpdateDeductions;
							}
							else
							{
								strPercentType = cmbPercentType.Text;
							}
						}
						boolUseAmount = true;
						dblAmount = FCConvert.ToDouble(txtAmount.Text);
						switch (cmbAmountType.ItemData(cmbAmountType.SelectedIndex))
						{
							case 1:
								{
									strAmountType = "Dollars";
									break;
								}
							case 2:
								{
									strAmountType = "Percent";
									break;
								}
							case 3:
								{
									strAmountType = "Levy";
									break;
								}
						}
						//end switch
					}
					else
					{
						MessageBox.Show("You must select the type the amount is specifying", "Invalid Amount Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateDeductions;
					}
				}
				if (cmbFrequency.SelectedIndex > 0)
				{
					boolUseFrequency = true;
					lngFrequency = cmbFrequency.ItemData(cmbFrequency.SelectedIndex);
				}
				if (fecherFoundation.Strings.Trim(txtMaxAmount.Text) != "")
				{
					if (!Information.IsNumeric(txtMaxAmount.Text))
					{
						MessageBox.Show("Limit must be blank or a valid number", "Invalid Limit", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateDeductions;
					}
					if (cmbMaxType.SelectedIndex >= 0)
					{
						switch (cmbMaxType.ItemData(cmbMaxType.SelectedIndex))
						{
							case 1:
								{
									strLimitType = "Life";
									break;
								}
							case 2:
								{
									strLimitType = "Fiscal";
									break;
								}
							case 3:
								{
									strLimitType = "Calendar";
									break;
								}
							case 4:
								{
									strLimitType = "Quarter";
									break;
								}
							case 5:
								{
									strLimitType = "Month";
									break;
								}
							case 6:
								{
									strLimitType = "Period";
									break;
								}
							case 7:
								{
									strLimitType = "% Gross";
									break;
								}
						}
						//end switch
					}
					else
					{
						MessageBox.Show("You must select the type the limit is specifying", "Invalid Limit Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return UpdateDeductions;
					}
					boolUseLimit = true;
					dblLimit = FCConvert.ToDouble(txtMaxAmount.Text);
				}
				theView.UpdateDeductions(boolUseAmount, dblAmount, strAmountType, strPercentType, boolUseLimit, dblLimit, strLimitType, boolUseFrequency, lngFrequency);
				MessageBox.Show("Update Successful", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				UpdateDeductions = true;
				return UpdateDeductions;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateDeductions;
		}
	}
}
