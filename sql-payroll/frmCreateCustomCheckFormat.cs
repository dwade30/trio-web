//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWPY0000
{
	public partial class frmCreateCustomCheckFormat : BaseForm
	{
		public frmCreateCustomCheckFormat()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            lblField = new System.Collections.Generic.List<FCLabel>();
            lblField.AddControlArrayElement(lblField_0, 0);
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCreateCustomCheckFormat InstancePtr
		{
			get
			{
				return (frmCreateCustomCheckFormat)Sys.GetInstance(typeof(frmCreateCustomCheckFormat));
			}
		}

		protected frmCreateCustomCheckFormat _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           11/17/03
		// This form will be used by towns to setup custom checks in
		// case the pre defined checks that TRIO has created doesn't
		// quite match the format of the town checks
		// ********************************************************
		string strCheckField = "";
		int KeyCol;
		int DescriptionCol;
		int IncludeCol;
		int XPositionCol;
		int YPositionCol;
		int XPositionDefailtCol;
		int YPositionDefailtCol;
		double dblProportion;
		bool blnUnload;
		bool blnEdit;
		bool boolFirstTime;
		bool boolLoading;
		bool boolDataChanged;

		private void cboCheckPosition_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void chkDoubleStub_CheckedChanged(object sender, System.EventArgs e)
		{
			LoadCheckPosition();
			boolDataChanged = true;
		}

		private void chkTwoStubs_CheckedChanged(object sender, System.EventArgs e)
		{
			LoadCheckPosition();
			boolDataChanged = true;
		}

		private void cmbCustomType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			FCFileSystem fs = new FCFileSystem();
			int lngFormatID;
			int counter;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			blnEdit = true;
			LoadCustomCheckInfo();
			LoadCustomCheckFields(lngFormatID);
			blnEdit = false;
			clsLoad.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToString(clsLoad.Get_Fields_String("ScannedCheckFile")) != "")
				{
					if (FCFileSystem.FileExists(FCConvert.ToString(clsLoad.Get_Fields_String("ScannedCheckFile"))))
					{
						imgCheck.Image = FCUtils.LoadPicture(clsLoad.Get_Fields_String("ScannedCheckFile"));
					}
					else
					{
						imgCheck.Image = ImageList1.Images[0];
					}
				}
				else
				{
					imgCheck.Image = ImageList1.Images[0];
				}
			}
			else
			{
				imgCheck.Image = ImageList1.Images[0];
			}
			if (boolFirstTime)
			{
				clsLoad.OpenRecordset("SELECT COUNT(Description) as TotalCount FROM CustomCheckFields where formatid = " + FCConvert.ToString(lngFormatID));
				if (!clsLoad.EndOfFile())
				{
					for (counter = 1; counter <= clsLoad.Get_Fields("TotalCount") - 1; counter++)
					{
						lblField.Load(counter);
					}
					dblProportion = imgCheck.WidthOriginal / 6120.0;
					SetupFieldsFirstTime();
				}
				boolFirstTime = false;
			}
			else
			{
				SetupFields();
			}
			boolDataChanged = false;
		}

		private void frmCreateCustomCheckFormat_Activated(object sender, System.EventArgs e)
		{
			int lngFormatID = 0;
			if (boolLoading)
			{
				// have to do this so the font will resize correctly when the form first comes up
				// otherwise the font will be wrong until you change from laser to dot matrix or change formats
				boolLoading = false;
				//App.DoEvents();
				if (cmbCustomType.SelectedIndex >= 0)
				{
					lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
					LoadCheckPosition();
					LoadCustomCheckFields(lngFormatID);
					SetupFields();
				}
				boolDataChanged = false;
			}
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void ResizevsCheckFields()
		{
			int lngGridWidth = 0;
			lngGridWidth = vsCheckFields.WidthOriginal;
			vsCheckFields.ColWidth(DescriptionCol, FCConvert.ToInt32(0.5 * lngGridWidth));
			vsCheckFields.ColWidth(IncludeCol, FCConvert.ToInt32(0.1 * lngGridWidth));
			vsCheckFields.ColWidth(XPositionCol, FCConvert.ToInt32(0.18 * lngGridWidth));
			vsCheckFields.ColWidth(YPositionCol, FCConvert.ToInt32(0.18 * lngGridWidth));
			// .ColWidth(XPositionDefailtCol) = 0.157 * lngGridWidth
		}

		private void frmCreateCustomCheckFormat_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreateCustomCheckFormat properties;
			//frmCreateCustomCheckFormat.FillStyle	= 0;
			//frmCreateCustomCheckFormat.ScaleWidth	= 9045;
			//frmCreateCustomCheckFormat.ScaleHeight	= 7530;
			//frmCreateCustomCheckFormat.LinkTopic	= "Form2";
			//frmCreateCustomCheckFormat.LockControls	= -1  'True;
			//frmCreateCustomCheckFormat.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			clsDRWrapper rsCheckItems = new clsDRWrapper();
			FCFileSystem fs = new FCFileSystem();
			int lngDefaultID;
			boolLoading = true;
			boolFirstTime = true;
			KeyCol = 0;
			DescriptionCol = 1;
			IncludeCol = 2;
			XPositionCol = 3;
			YPositionCol = 4;
			XPositionDefailtCol = 5;
			YPositionDefailtCol = 6;
			vsCheckFields.ColHidden(KeyCol, true);
			vsCheckFields.ColHidden(XPositionDefailtCol, true);
			vsCheckFields.ColHidden(YPositionDefailtCol, true);
			vsCheckFields.ColDataType(XPositionCol, FCGrid.DataTypeSettings.flexDTDouble);
			vsCheckFields.ColDataType(YPositionCol, FCGrid.DataTypeSettings.flexDTDouble);
			vsCheckFields.ColDataType(XPositionDefailtCol, FCGrid.DataTypeSettings.flexDTDouble);
			vsCheckFields.ColDataType(YPositionDefailtCol, FCGrid.DataTypeSettings.flexDTDouble);
			vsCheckFields.ColDataType(IncludeCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsCheckFields.TextMatrix(0, DescriptionCol, "Check Field");
			vsCheckFields.TextMatrix(0, IncludeCol, "Show");
			vsCheckFields.TextMatrix(0, XPositionCol, "Horz Pos");
			vsCheckFields.TextMatrix(0, YPositionCol, "Vert Pos");
			vsCheckFields.TextMatrix(0, XPositionDefailtCol, "Def Horz Pos");
			vsCheckFields.TextMatrix(0, YPositionDefailtCol, "Def Vert Pos");
			vsCheckFields.ColAlignment(IncludeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// show a border between the titles and the data input section of the grid
			vsCheckFields.Select(0, 0, 0, vsCheckFields.Cols - 1);
			vsCheckFields.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			rsCheckItems.OpenRecordset("SELECT * FROM CustomChecks where rtrim(upper(description)) = 'DEFAULT'");
			lngDefaultID = FCConvert.ToInt32(rsCheckItems.Get_Fields("id"));
			LoadcmbCustomType();
			cmbCustomType.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			boolDataChanged = false;
		}

		private void frmCreateCustomCheckFormat_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCreateCustomCheckFormat_Resize(object sender, System.EventArgs e)
		{
			dblProportion = imgCheck.WidthOriginal / 6120.0;
			ResizevsCheckFields();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			int lngFormatID;
			clsDRWrapper clsSave = new clsDRWrapper();
			if (cmbCustomType.SelectedIndex < 0)
			{
				MessageBox.Show("You must select a format before you can delete one", "No Format Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbCustomType.SelectedIndex == 0 || fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(cmbCustomType.Text)) == "DEFAULT")
			{
				// these should be the same. Default should be item 0
				MessageBox.Show("You cannot delete the default format", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (MessageBox.Show("This will delete this format." + "\r\n" + "Do you wish to continue?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}
			// delete it
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			clsSave.Execute("delete from customcheckfields where formatid = " + FCConvert.ToString(lngFormatID), "Budgetary");
			clsSave.Execute("delete from customchecks where id = " + FCConvert.ToString(lngFormatID), "Budgetary");
			// now reload cmbcustomtype
			LoadcmbCustomType();
			// select it
			cmbCustomType.SelectedIndex = 0;
		}

		private void mnuFileLoadImage_Click(object sender, System.EventArgs e)
		{
			string strCurDir;
			string strFileName = "";
			// vbPorter upgrade warning: Ans As int	OnWrite(DialogResult)
			DialogResult Ans = 0;
			clsDRWrapper rsImageInfo = new clsDRWrapper();
			FCFileSystem fs = new FCFileSystem();
			string strDestinationPath;
			int lngFormatID;
			strCurDir = Environment.CurrentDirectory;
			PickAgain:
			;
			fecherFoundation.Information.Err().Clear();
			// dlg1.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			//- dlg1.CancelError = true;
			/*? On Error Resume Next  */
			dlg1.Filter = "*.jpg, *.gif, *.bmp";
			dlg1.InitDir = App.Path;
			dlg1.ShowOpen();
			if (fecherFoundation.Information.Err().Number == 0)
			{
				strFileName = dlg1.FileName;
				if (fecherFoundation.Strings.UCase(Strings.Right(strFileName, 3)) != "JPG" && fecherFoundation.Strings.UCase(Strings.Right(strFileName, 3)) != "GIF" && fecherFoundation.Strings.UCase(Strings.Right(strFileName, 3)) != "BMP")
				{
					Ans = MessageBox.Show("You must select a picture file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (Ans == DialogResult.Yes)
					{
						goto PickAgain;
					}
					else
					{
						return;
					}
				}
			}
			else
			{
				return;
			}
			/*? On Error GoTo 0 */
			FCFileSystem.ChDrive(strCurDir);
			Environment.CurrentDirectory = strCurDir;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			rsImageInfo.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (rsImageInfo.EndOfFile() != true && rsImageInfo.BeginningOfFile() != true)
			{
				rsImageInfo.Edit();
			}
			else
			{
				rsImageInfo.AddNew();
			}
			strDestinationPath = strCurDir;
			if (Strings.Right(strDestinationPath, 1) != "\\")
			{
				strDestinationPath += "\\CheckIcons";
			}
			else
			{
				strDestinationPath += "CheckIcons";
			}
			if (!Directory.Exists(strDestinationPath))
			{
                Directory.CreateDirectory(strDestinationPath);
			}
			File.Copy(strFileName, strDestinationPath + "\\" + Strings.Right(strFileName, strFileName.Length - Strings.InStrRev(strFileName, "\\", -1, CompareConstants.vbTextCompare/*?*/)), true);
			rsImageInfo.Set_Fields("ScannedCheckFile", strDestinationPath + "\\" + Strings.Right(strFileName, strFileName.Length - Strings.InStrRev(strFileName, "\\", -1, CompareConstants.vbTextCompare/*?*/)));
			rsImageInfo.Update(true);
			imgCheck.Image = FCUtils.LoadPicture(strDestinationPath + "\\" + Strings.Right(strFileName, strFileName.Length - Strings.InStrRev(strFileName, "\\", -1, CompareConstants.vbTextCompare/*?*/)));
			//App.DoEvents();
			imgCheck.Visible = true;
			this.Refresh();
		}

		private void mnuFileResetDefaults_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsCheckFields.Rows - 1); counter++)
			{
				vsCheckFields.TextMatrix(counter, XPositionCol, vsCheckFields.TextMatrix(counter, XPositionDefailtCol));
				vsCheckFields.TextMatrix(counter, YPositionCol, vsCheckFields.TextMatrix(counter, YPositionDefailtCol));
			}
			SetupFields();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		public void mnuFileSave_Click()
		{
			mnuFileSave_Click(mnuFileSave, new System.EventArgs());
		}

		private void mnuFileUnloadImage_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsImageInfo = new clsDRWrapper();
			int lngFormatID;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			rsImageInfo.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (rsImageInfo.EndOfFile() != true && rsImageInfo.BeginningOfFile() != true)
			{
				rsImageInfo.Edit();
			}
			else
			{
				return;
			}
			rsImageInfo.Set_Fields("ScannedCheckFile", "");
			rsImageInfo.Update();
			imgCheck.Image = ImageList1.Images[0];
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			string strNewFormat = "";
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngFormatID = 0;
			int lngCurrentID = 0;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// creates a new type that is the same as the current selected one
			Retry:
			;
			object temp = strNewFormat;
			if (frmInput.InstancePtr.Init(ref temp, "New Format", "Enter a name for the new format", 2160, false, modGlobalConstants.InputDTypes.idtString))
			{
				strNewFormat = temp.ToString();
				strNewFormat = fecherFoundation.Strings.Trim(strNewFormat);
				if (strNewFormat != string.Empty)
				{
					if (fecherFoundation.Strings.UCase(strNewFormat) == "DEFAULT")
					{
						MessageBox.Show("There can be only one default format." + "\r\n" + "Please choose another name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
						goto Retry;
					}
					lngCurrentID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
					clsLoad.OpenRecordset("select * from customchecks where id = " + FCConvert.ToString(lngCurrentID));
					clsSave.OpenRecordset("select * from customchecks where id = -1");
					clsSave.AddNew();
					clsSave.Set_Fields("description", strNewFormat);
					if (cmbMailer.Text == "Laser")
					{
						clsSave.Set_Fields("CheckType", "L");
					}
					else if (cmbMailer.Text == "Mailer")
					{
						clsSave.Set_Fields("CheckType", "M");
					}
					else
					{
						clsSave.Set_Fields("CheckType", "D");
					}
					clsSave.Set_Fields("scannedcheckfile", clsLoad.Get_Fields("scannedcheckfile"));
					clsSave.Set_Fields("CheckPosition", Strings.Left(cboCheckPosition.Text, 1));
					clsSave.Set_Fields("UseDoubleStub", (chkDoubleStub.CheckState == Wisej.Web.CheckState.Checked && cmbMailer.Text == "Laser"));
					clsSave.Update();
					lngFormatID = FCConvert.ToInt32(clsSave.Get_Fields("ID"));
					// add this to the combobox
					cmbCustomType.AddItem(strNewFormat);
					cmbCustomType.ItemData(cmbCustomType.NewIndex, lngFormatID);
					// now copy and save current info
					clsLoad.OpenRecordset("select * from customcheckfields where formatid = " + FCConvert.ToString(lngCurrentID));
					clsSave.OpenRecordset("select * from customcheckfields where formatid = -1");
					while (!clsLoad.EndOfFile())
					{
						clsSave.AddNew();
						clsSave.Set_Fields("formatid", lngFormatID);
						clsSave.Set_Fields("description", clsLoad.Get_Fields_String("description"));
						clsSave.Set_Fields("controlname", clsLoad.Get_Fields("controlname"));
						clsSave.Set_Fields("checktype", clsLoad.Get_Fields("checktype"));
						clsSave.Set_Fields("defaultxposition", clsLoad.Get_Fields("defaultxposition"));
						clsSave.Set_Fields("defaultyposition", clsLoad.Get_Fields("defaultyposition"));
						clsSave.Set_Fields("fieldheight", FCConvert.ToInt16(Conversion.Val(clsLoad.Get_Fields("fieldheight"))));
						clsSave.Set_Fields("fieldwidth", FCConvert.ToInt16(Conversion.Val(clsLoad.Get_Fields("fieldwidth"))));
						for (x = 1; x <= (vsCheckFields.Rows - 1); x++)
						{
							if (vsCheckFields.TextMatrix(x, KeyCol) == FCConvert.ToString(clsLoad.Get_Fields("id")))
							{
								clsSave.Set_Fields("Include", FCConvert.CBool(vsCheckFields.TextMatrix(x, IncludeCol)));
								clsSave.Set_Fields("XPosition", vsCheckFields.TextMatrix(x, XPositionCol));
								clsSave.Set_Fields("YPosition", vsCheckFields.TextMatrix(x, YPositionCol));
							}
						}
						// x
						clsSave.Update();
						clsLoad.MoveNext();
					}
					//App.DoEvents();
					// now load it since the id numbers won't match for the customcheckfields records
					for (x = 0; x <= cmbCustomType.Items.Count - 1; x++)
					{
						if (cmbCustomType.ItemData(x) == lngFormatID)
						{
							cmbCustomType.SelectedIndex = x;
							return;
						}
					}
					// x
				}
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// Unload rptSampleCheck
			// rptSampleCheck.PrintReport (False)
			if (cmbCustomType.SelectedIndex < 0)
				return;
			modCustomChecks.Statics.intCustomCheckFormatToUse = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			if (boolDataChanged)
			{
				mnuFileSave_Click();
			}
			rptNewSampleCheck.InstancePtr.Init(modCustomChecks.Statics.intCustomCheckFormatToUse, this.Modal);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void LoadCheckPosition()
		{
			string strTemp;
			strTemp = cboCheckPosition.Text + " ";
			strTemp = Strings.Left(strTemp, 1);
			if (cmbMailer.Text == "Laser" && chkDoubleStub.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkShowPayRate.Visible = true;
			}
			else
			{
				chkShowPayRate.Visible = false;
			}

            var temp = fecherFoundation.Strings.UCase(strTemp);

            if (cmbMailer.Text == "Laser" && chkDoubleStub.CheckState != Wisej.Web.CheckState.Checked )
			{
				cboCheckPosition.Clear();
				cboCheckPosition.AddItem("Top Section");
				cboCheckPosition.AddItem("Middle Section");
				cboCheckPosition.AddItem("Bottom Section");
				switch (temp)
                {
                    case "T":
                        cboCheckPosition.SelectedIndex = 0;

                        break;
                    case "M":
                        cboCheckPosition.SelectedIndex = 1;

                        break;
                    case "B":
                        cboCheckPosition.SelectedIndex = 2;

                        break;
                    default:
                        cboCheckPosition.SelectedIndex = 0;

                        break;
                }
			}
			else
            {
                if (cmbMailer.Text == "Mailer")
                {
                    cboCheckPosition.Clear();
                    cboCheckPosition.AddItem("Middle Section");
                    cboCheckPosition.SelectedIndex = 0;
                }
                else
                {
                    cboCheckPosition.Clear();
                    cboCheckPosition.AddItem("Top Section");
                    cboCheckPosition.AddItem("Bottom Section");
                    switch (temp)
                    {
                        case "T":
                            cboCheckPosition.SelectedIndex = 0;

                            break;
                        case "B":
                        case "M":
                            cboCheckPosition.SelectedIndex = 1;

                            break;
                        default:
                            cboCheckPosition.SelectedIndex = 0;

                            break;
                    }
                }
            }
        }

		private void LoadCustomCheckFields(int lngFormatID)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			// vbPorter upgrade warning: intEmployeeRow As int	OnWriteFCConvert.ToInt32(
			int intEmployeeRow;
			intEmployeeRow = -1;
			// If optLaser Then
			// rsInfo.OpenRecordset "SELECT * FROM CustomCheckFields WHERE formatid = " & lngFormatID & " and (CheckType = 'L' OR CheckType = 'B') ORDER BY Description"
			// Else
			// rsInfo.OpenRecordset "SELECT * FROM CustomCheckFields WHERE formatid = " & lngFormatID & " and (CheckType = 'D' OR CheckType = 'B') ORDER BY Description"
			// End If
			rsInfo.OpenRecordset("Select * from customcheckfields where formatid = " + FCConvert.ToString(lngFormatID) + " order by description");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				vsCheckFields.Rows = 1;
				do
				{
					vsCheckFields.AddItem("");
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields("ID")));
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields("Description")));
					if (FCConvert.ToString(rsInfo.Get_Fields("Description")) == "Employee Number")
						intEmployeeRow = (vsCheckFields.Rows - 1);
					if (FCConvert.ToBoolean(rsInfo.Get_Fields("Include")))
					{
						vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, IncludeCol, FCConvert.ToString(true));
					}
					else
					{
						vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, IncludeCol, FCConvert.ToString(false));
					}
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, XPositionCol, FCConvert.ToString(rsInfo.Get_Fields_Double("XPosition")));
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, YPositionCol, FCConvert.ToString(rsInfo.Get_Fields_Double("YPosition")));
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, XPositionDefailtCol, FCConvert.ToString(rsInfo.Get_Fields_Double("DefaultXPosition")));
					vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, YPositionDefailtCol, FCConvert.ToString(rsInfo.Get_Fields_Double("DefaultYPosition")));
					if (cmbMailer.Text == "Laser" && FCConvert.ToString(rsInfo.Get_Fields("CheckType")) == "D")
					{
						//FC:FINAL:DDU:#i2402 - changed from hidding the row with removing it out
						//vsCheckFields.RowHidden(vsCheckFields.Rows - 1, true);
						vsCheckFields.RemoveItem(vsCheckFields.Rows - 1);
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				// CHANGE MADE ON 8/10/2004 BY MATTHEW
				// THIS WILL MOVE THE EMPLOYEE NAME FIELD OPTION TO THE BOTTOM IF THIS
				// MODULE IS PAYROLL
				if (fecherFoundation.Strings.UCase(App.EXEName) == "TWPY0000")
				{
					if (intEmployeeRow >= 0)
					{
						// vbPorter upgrade warning: inta As int	OnWriteFCConvert.ToInt32(
						int inta;
						vsCheckFields.AddItem("");
						for (inta = 0; inta <= (vsCheckFields.Cols - 1); inta++)
						{
							vsCheckFields.TextMatrix(vsCheckFields.Rows - 1, inta, vsCheckFields.TextMatrix(intEmployeeRow, inta));
						}
						vsCheckFields.RemoveItem(intEmployeeRow);
					}
				}
			}
            rsInfo.DisposeOf();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void optDotMatrix_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			int lngFormatID;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			chkDoubleStub.Visible = false;
			chkTwoStubs.Visible = true;
			chkShowPayRate.Visible = false;
			LoadCheckPosition();
			LoadCustomCheckFields(lngFormatID);
			if (!blnEdit)
			{
				SetupFields();
			}
			boolDataChanged = true;
		}

		private void optLaser_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			int lngFormatID;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			chkDoubleStub.Visible = true;
			chkTwoStubs.Visible = false;
			if (chkDoubleStub.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkShowPayRate.Visible = true;
			}
			else
			{
				chkShowPayRate.Visible = false;
			}
			LoadCheckPosition();
			LoadCustomCheckFields(lngFormatID);
			if (!blnEdit)
			{
				SetupFields();
			}
		}

		private void LoadCustomCheckInfo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngFormatID;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			rsInfo.OpenRecordset("select * from tblcheckformat", "twpy0000.vb1");
			if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("LogoOnNonNegotiableOnly")))
			{
				chkNonNegotiable.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkNonNegotiable.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			rsInfo.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
                var checkPosition = rsInfo.Get_Fields_String("CheckPosition");

                if (FCConvert.ToString(rsInfo.Get_Fields("CheckType")) == "L")
				{
					cmbMailer.Text = "Laser";
					chkDoubleStub.Visible = true;
					chkTwoStubs.Visible = false;
					chkTwoStubs.CheckState = Wisej.Web.CheckState.Unchecked;
					if (rsInfo.Get_Fields_Boolean("UseDoubleStub") == true)
					{
						chkDoubleStub.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDoubleStub.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					txtVoidAfter.Text = FCConvert.ToString(rsInfo.Get_Fields_String("VoidAfterMessage"));
					LoadCheckPosition();
					if (chkDoubleStub.CheckState == Wisej.Web.CheckState.Unchecked)
                    {
                        switch (checkPosition)
                        {
                            case "T":
                                cboCheckPosition.SelectedIndex = 0;

                                break;
                            case "M":
                                cboCheckPosition.SelectedIndex = 1;

                                break;
                            case "B":
                                cboCheckPosition.SelectedIndex = 2;

                                break;
                        }

                        chkShowPayRate.Visible = false;
                    }
					else
					{
						chkShowPayRate.Visible = true;
						if (fecherFoundation.Strings.UCase(checkPosition) == "T")
						{
							cboCheckPosition.SelectedIndex = 0;
						}
						else if (fecherFoundation.Strings.UCase(checkPosition) == "B")
						{
							cboCheckPosition.SelectedIndex = 1;
						}
						else
						{
							cboCheckPosition.SelectedIndex = 0;
						}
					}
				}
				else if (rsInfo.Get_Fields("CheckType") == "M")
				{
					cmbMailer.Text = "Mailer";
					chkDoubleStub.Visible = false;
					chkTwoStubs.Visible = false;
					chkShowPayRate.Visible = false;
					chkTwoStubs.CheckState = Wisej.Web.CheckState.Unchecked;
					chkDoubleStub.CheckState = Wisej.Web.CheckState.Unchecked;
					txtVoidAfter.Text = FCConvert.ToString(rsInfo.Get_Fields_String("VoidAfterMessage"));
					LoadCheckPosition();
					cboCheckPosition.SelectedIndex = 0;
				}
				else
				{
					cmbMailer.Text = "Dot Matrix";
					chkShowPayRate.Visible = false;
					LoadCheckPosition();
					if (rsInfo.Get_Fields_Boolean("UseTwoStubs") == true)
					{
						chkTwoStubs.CheckState = Wisej.Web.CheckState.Checked;
						chkTwoStubs.Visible = true;
						switch (checkPosition)
                        {
                            case "T":
                                cboCheckPosition.SelectedIndex = 0;

                                break;
                            case "M":
                                cboCheckPosition.SelectedIndex = 1;

                                break;
                            case "B":
                                cboCheckPosition.SelectedIndex = 2;

                                break;
                        }
					}
					else
					{
						chkTwoStubs.CheckState = Wisej.Web.CheckState.Unchecked;
						chkTwoStubs.Visible = true;
						switch (checkPosition)
                        {
                            case "T":
                                cboCheckPosition.SelectedIndex = 0;

                                break;
                            case "B":
                                cboCheckPosition.SelectedIndex = 1;

                                break;
                        }
					}
				}
				if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("ShowIndividualPayrates")))
				{
					chkShowPayRate.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkShowPayRate.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				LoadCheckPosition();
				cboCheckPosition.SelectedIndex = 0;
			}
		}

		private void SaveInfo()
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int lngFormatID;
			cboCheckPosition.Focus();
			if (cmbCustomType.SelectedIndex < 0)
			{
				MessageBox.Show("You must select a format to save", "No Format Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			if (cboCheckPosition.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a check position before you may continue.", "Invalid Check Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cboCheckPosition.Focus();
				return;
			}
            
            clsDRWrapper rsInfo = new clsDRWrapper();

			rsInfo.OpenRecordset("SELECT * FROM CustomChecks where id = " + FCConvert.ToString(lngFormatID));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				rsInfo.Edit();
			}
			else
			{
				rsInfo.AddNew();
			}
			switch (cmbMailer.Text)
            {
                case "Laser":
                    rsInfo.Set_Fields("CheckType", "L");

                    break;
                case "Mailer":
                    rsInfo.Set_Fields("CheckType", "M");

                    break;
                default:
                    rsInfo.Set_Fields("CheckType", "L");

                    break;
            }
			rsInfo.Set_Fields("CheckPosition", Strings.Left(cboCheckPosition.Text, 1));
			rsInfo.Set_Fields("UseDoubleStub", (chkDoubleStub.CheckState == Wisej.Web.CheckState.Checked && cmbMailer.Text == "Laser"));
			rsInfo.Set_Fields("UseTwoStubs", false);
			rsInfo.Set_Fields("VoidAfterMessage", fecherFoundation.Strings.Trim(txtVoidAfter.Text));
			rsInfo.Set_Fields("ShowIndividualPayrates", chkShowPayRate.CheckState == Wisej.Web.CheckState.Checked);
			rsInfo.Update();
			for (counter = 1; counter <= (vsCheckFields.Rows - 1); counter++)
			{
				rsInfo.OpenRecordset("SELECT * FROM CustomCheckFields WHERE formatid = " + FCConvert.ToString(lngFormatID) + " and ID = " + vsCheckFields.TextMatrix(counter, KeyCol));
				rsInfo.Edit();
				rsInfo.Set_Fields("Include", FCConvert.CBool(vsCheckFields.TextMatrix(counter, IncludeCol)));
				rsInfo.Set_Fields("XPosition", vsCheckFields.TextMatrix(counter, XPositionCol));
				rsInfo.Set_Fields("YPosition", vsCheckFields.TextMatrix(counter, YPositionCol));
				rsInfo.Update(false);
			}
			rsInfo.OpenRecordset("select * from tblCheckFormat", "twpy0000.vb1");
			rsInfo.Edit();
			if (chkNonNegotiable.CheckState == Wisej.Web.CheckState.Checked)
			{
				rsInfo.Set_Fields("LogoOnNonNegotiableOnly", true);
			}
			else
			{
				rsInfo.Set_Fields("LogoOnNonNegotiableOnly", false);
			}
			rsInfo.Update();
            rsInfo.DisposeOf();
			boolDataChanged = false;
			MessageBox.Show("Check format was saved successfully.", "Save Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
			if (blnUnload)
			{
				mnuProcessQuit_Click();
			}
		}

		private void optMailer_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			int lngFormatID;
			if (cmbCustomType.SelectedIndex < 0)
				return;
			lngFormatID = cmbCustomType.ItemData(cmbCustomType.SelectedIndex);
			chkDoubleStub.Visible = false;
			chkTwoStubs.Visible = false;
			chkShowPayRate.Visible = false;
			LoadCheckPosition();
			LoadCustomCheckFields(lngFormatID);
			if (!blnEdit)
			{
				SetupFields();
			}
		}

		private void vsCheckFields_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void vsCheckFields_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (vsCheckFields.Row > 0)
			{
				boolDataChanged = true;
			}
		}

		private void vsCheckFields_RowColChange(object sender, System.EventArgs e)
		{
			if (vsCheckFields.Col != IncludeCol)
			{
				vsCheckFields.EditCell();
			}
			SetupFields();
		}

		private void vsCheckFields_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				Support.SendKeys("{TAB}", false);
			}
		}

		private void SetupFields()
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			dblProportion = FCConvert.ToDouble(imgCheck.WidthOriginal / 6120);
			for (counter = 1; counter <= (vsCheckFields.Rows - 1); counter++)
			{
				lblField[(counter - 1)].Visible = FCConvert.CBool(vsCheckFields.TextMatrix(counter, IncludeCol)) && !vsCheckFields.RowHidden(counter);
				lblField[(counter - 1)].TopOriginal = FCConvert.ToInt32(imgCheck.TopOriginal + (Conversion.Val(vsCheckFields.TextMatrix(counter, YPositionCol)) * 120 * dblProportion));
				lblField[(counter - 1)].LeftOriginal = FCConvert.ToInt32(imgCheck.LeftOriginal + (Conversion.Val(vsCheckFields.TextMatrix(counter, XPositionCol)) * 77 * dblProportion));
				lblField[(counter - 1)].Text = vsCheckFields.TextMatrix(counter, DescriptionCol);
				lblField[(counter - 1)].ForeColor = Color.Blue;
				lblField[(counter - 1)].BringToFront();
				lblField[(counter - 1)].Font = FCUtils.FontChangeSize(lblField[(counter - 1)].Font, FCConvert.ToSingle(Strings.Format(6 * dblProportion, "0.0")));
			}
            //FC:FINAL:AM:#i2095 - make all labels visible
            imgCheck.SendToBack();
		}

		private void SetupFieldsFirstTime()
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			clsDRWrapper rsFieldInfo = new clsDRWrapper();
			dblProportion = imgCheck.WidthOriginal / 6120.0;
			for (counter = 1; counter <= (vsCheckFields.Rows - 1); counter++)
			{
				lblField[(counter - 1)].Visible = FCConvert.CBool(vsCheckFields.TextMatrix(counter, IncludeCol)) && !vsCheckFields.RowHidden(counter);
				lblField[(counter - 1)].TopOriginal = FCConvert.ToInt32(imgCheck.TopOriginal + (Conversion.Val(vsCheckFields.TextMatrix(counter, YPositionCol)) * 120 * dblProportion));
				lblField[(counter - 1)].LeftOriginal = FCConvert.ToInt32(imgCheck.LeftOriginal + (Conversion.Val(vsCheckFields.TextMatrix(counter, XPositionCol)) * 77 * dblProportion));
				lblField[(counter - 1)].Text = vsCheckFields.TextMatrix(counter, DescriptionCol);
				rsFieldInfo.OpenRecordset("SELECT * FROM CustomCheckFields WHERE Description = '" + vsCheckFields.TextMatrix(counter, DescriptionCol) + "'");
				if (rsFieldInfo.EndOfFile() != true && rsFieldInfo.BeginningOfFile() != true)
				{
					if (Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldHeight")) > 0 && Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldWidth")) > 0)
					{
						lblField[(counter - 1)].HeightOriginal = FCConvert.ToInt32(Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldHeight")) * (imgCheck.HeightOriginal / 5025.0) * dblProportion);
						lblField[(counter - 1)].WidthOriginal = FCConvert.ToInt32(Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldWidth")) * (imgCheck.WidthOriginal / 10800.0) * dblProportion);
						lblField[(counter - 1)].BorderStyle = FCConvert.ToInt32(BorderStyle.Solid);
					}
				}
				lblField[(counter - 1)].Font = FCUtils.FontChangeSize(lblField[(counter - 1)].Font, FCConvert.ToSingle(Strings.Format(6 * dblProportion, "0.0")));
				lblField[(counter - 1)].ForeColor = Color.Blue;
				lblField[(counter - 1)].BringToFront();
			}
            //FC:FINAL:AM:#i2095 - make all labels visible
            imgCheck.SendToBack();
        }

		private void vsCheckFields_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			double dblProportion;
			boolDataChanged = true;
			if (fecherFoundation.Strings.Trim(vsCheckFields.EditText) == "")
			{
				vsCheckFields.EditText = "0";
			}
			if (!Information.IsNumeric(vsCheckFields.EditText))
			{
				MessageBox.Show("You may only enter a number in these fields.", "Invalid Position Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
				return;
			}
			dblProportion = FCConvert.ToDouble(imgCheck.WidthOriginal / 6120);
			if (vsCheckFields.Col == YPositionCol)
			{
				if (vsCheckFields.TextMatrix(vsCheckFields.Row, DescriptionCol) == "MICR Line")
				{
					// do nothing
				}
				else
				{
					if (imgCheck.TopOriginal + (Conversion.Val(vsCheckFields.EditText) * 120 * dblProportion) > imgCheck.TopOriginal + imgCheck.HeightOriginal || imgCheck.TopOriginal + (Conversion.Val(vsCheckFields.EditText) * 120 * dblProportion) + lblField[(vsCheckFields.Row - 1)].HeightOriginal < imgCheck.TopOriginal)
					{
						MessageBox.Show("Using this value would place the field off the check.", "Invalid Position Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
						return;
					}
				}
			}
			else if (vsCheckFields.Col == XPositionCol)
			{
				if (imgCheck.LeftOriginal + (Conversion.Val(vsCheckFields.EditText) * 77 * dblProportion) > imgCheck.LeftOriginal + imgCheck.WidthOriginal || imgCheck.LeftOriginal + (Conversion.Val(vsCheckFields.EditText) * 77 * dblProportion) + lblField[(vsCheckFields.Row - 1)].WidthOriginal < imgCheck.LeftOriginal)
				{
					MessageBox.Show("Using this value would place the field off the check.", "Invalid Position Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
			}
			SetupFields();
		}

		private void SetCustomFormColors()
		{
			lblField[0].ForeColor = Color.Blue;
		}

		private void vsCheckFields_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsCheckFields.Row > 0)
			{
				if (FCConvert.CBool(vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol)) == false)
				{
					vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol, FCConvert.ToString(true));
				}
				else
				{
					vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol, FCConvert.ToString(false));
				}
			}
			SetupFields();
		}

		private void vsCheckFields_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 32)
			{
				KeyAscii = 0;
				if (FCConvert.CBool(vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol)) == false)
				{
					vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol, FCConvert.ToString(true));
				}
				else
				{
					vsCheckFields.TextMatrix(vsCheckFields.Row, IncludeCol, FCConvert.ToString(false));
				}
			}
			SetupFields();
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void LoadcmbCustomType()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbCustomType.Clear();
			cmbCustomType.AddItem("Default");
			clsLoad.OpenRecordset("select * from customchecks where rtrim(upper(description)) = 'DEFAULT'");
			cmbCustomType.ItemData(0, FCConvert.ToInt32(clsLoad.Get_Fields("id")));
			clsLoad.OpenRecordset("select * from customchecks where upper(rtrim(Description)) <> 'DEFAULT' order by description");
			while (!clsLoad.EndOfFile())
			{
				cmbCustomType.AddItem(clsLoad.Get_Fields_String("description"));
				cmbCustomType.ItemData(cmbCustomType.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields("id")));
				clsLoad.MoveNext();
			}
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuFileSave_Click(cmdSave, EventArgs.Empty);
        }

		private void cmbMailer_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbMailer.Text == "Mailer")
			{
				optMailer_CheckedChanged(sender, e);
			}
			if (cmbMailer.Text == "Laser")
			{
				optLaser_CheckedChanged(sender, e);
			}
		}

	}
}
