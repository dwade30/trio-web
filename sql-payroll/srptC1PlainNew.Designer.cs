﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptC1Plain.
	/// </summary>
	partial class srptC1Plain
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptC1Plain));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Barcode1 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
            this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWithholdingAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQuarterNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine3a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine3b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line87 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMaineLicense = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNumberofPayees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMagnetic = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line194 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line195 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line196 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line197 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line198 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line199 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPeriodMonthEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodDayEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodYearEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodMonthStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodDayStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodYearStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDueMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDueDay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDueYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line200 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line201 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDueDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine2b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLine2c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtOverpaymentCheckbox = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPayeeStatementsSent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEnclosingAmendedw3me = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label139 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label140 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label141 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line202 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line203 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label142 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label143 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label144 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAmendedReturn = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label145 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label146 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label147 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCloseAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label148 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Barcode2 = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
            this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtAmount43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentSub3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentSub1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDatePaid1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDatePaid22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentSub2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDatePaid43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtLine6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWAccount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line101 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line103 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line104 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line106 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line108 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line109 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line111 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line112 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line113 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line114 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line122 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line123 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line124 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line126 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line127 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line128 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line130 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line131 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line133 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line134 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line136 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line137 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line143 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line145 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line146 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line147 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line148 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line150 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line151 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line152 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line154 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line155 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line157 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line158 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line160 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line166 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMonthStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDayStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtYearStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMonthEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDayEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtYearEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line167 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line168 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line169 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line170 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line171 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line172 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line173 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line174 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line175 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDatePaid34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid58 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDatePaid63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line176 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line177 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line178 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line179 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line180 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line181 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line182 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line183 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line184 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line185 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line186 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line187 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line188 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line189 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line190 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line191 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line192 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line193 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPeriodStart2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriodEnd2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarterNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine3a)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine3b)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaineLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberofPayees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMagnetic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodMonthEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodDayEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodYearEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodMonthStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodDayStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodYearStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine2b)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine2c)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverpaymentCheckbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayeeStatementsSent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEnclosingAmendedw3me)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmendedReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCloseAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWAccount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Barcode1,
            this.Label73,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.txtName,
            this.txtAddress1,
            this.txtWithholdingAccount,
            this.txtQuarterNum,
            this.txtLine1,
            this.txtLine2,
            this.txtLine3a,
            this.txtDate,
            this.txtPhone,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Label32,
            this.Label37,
            this.Label40,
            this.Label41,
            this.Line76,
            this.Label42,
            this.Label43,
            this.Label44,
            this.Label45,
            this.Label46,
            this.Label47,
            this.Label48,
            this.Label52,
            this.Label53,
            this.txtEmail,
            this.txtEIN,
            this.Label54,
            this.Label58,
            this.Label60,
            this.Label67,
            this.Label68,
            this.txtCity,
            this.txtPeriodStart,
            this.txtPeriodEnd,
            this.Field25,
            this.txtState,
            this.txtZip,
            this.txtLine3b,
            this.Label76,
            this.Line87,
            this.Label83,
            this.Label74,
            this.Label106,
            this.txtMaineLicense,
            this.txtNumberofPayees,
            this.Label109,
            this.txtMagnetic,
            this.Label110,
            this.Label111,
            this.Label112,
            this.Line94,
            this.Line96,
            this.Line97,
            this.Label113,
            this.Label115,
            this.Line194,
            this.Label116,
            this.Label117,
            this.Line195,
            this.Label118,
            this.Line196,
            this.Label119,
            this.Line197,
            this.Label120,
            this.Line198,
            this.Label121,
            this.Line199,
            this.Label123,
            this.Label124,
            this.Label125,
            this.txtPeriodMonthEnd,
            this.txtPeriodDayEnd,
            this.txtPeriodYearEnd,
            this.txtPeriodMonthStart,
            this.txtPeriodDayStart,
            this.txtPeriodYearStart,
            this.Label126,
            this.txtDueMonth,
            this.txtDueDay,
            this.txtDueYear,
            this.Line200,
            this.Line201,
            this.Label127,
            this.Label128,
            this.Label129,
            this.Label130,
            this.Label131,
            this.txtDueDate,
            this.txtLine2b,
            this.txtLine2c,
            this.Label132,
            this.Label133,
            this.txtOverpaymentCheckbox,
            this.txtPayeeStatementsSent,
            this.txtEnclosingAmendedw3me,
            this.Label134,
            this.Label135,
            this.Label136,
            this.Label137,
            this.Label138,
            this.Label139,
            this.Label140,
            this.Label141,
            this.Line202,
            this.Line203,
            this.Label142,
            this.Label143,
            this.Label144,
            this.txtAmendedReturn,
            this.Label145,
            this.Label146,
            this.Label147,
            this.txtCloseAccount,
            this.Label148});
            this.Detail.Height = 9.96875F;
            this.Detail.Name = "Detail";
            this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // Barcode1
            // 
            this.Barcode1.CaptionPosition = GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below;
            this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F);
            this.Barcode1.Height = 0.625F;
            this.Barcode1.Left = 5.666667F;
            this.Barcode1.Name = "Barcode1";
            this.Barcode1.QuietZoneBottom = 0F;
            this.Barcode1.QuietZoneLeft = 0F;
            this.Barcode1.QuietZoneRight = 0F;
            this.Barcode1.QuietZoneTop = 0F;
            this.Barcode1.Text = "2106200";
            this.Barcode1.Top = 0F;
            this.Barcode1.Width = 1.583333F;
            // 
            // Label73
            // 
            this.Label73.Height = 0.1666667F;
            this.Label73.HyperLink = null;
            this.Label73.Left = 7.25F;
            this.Label73.Name = "Label73";
            this.Label73.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 12pt; font-weight: bold;" +
    " text-align: right; vertical-align: top";
            this.Label73.Text = "15";
            this.Label73.Top = 0.1597222F;
            this.Label73.Width = 0.25F;
            // 
            // Shape17
            // 
            this.Shape17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape17.Height = 0.1388889F;
            this.Shape17.Left = 0.01388889F;
            this.Shape17.Name = "Shape17";
            this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape17.Top = 0.01388889F;
            this.Shape17.Width = 0.1388889F;
            // 
            // Shape18
            // 
            this.Shape18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape18.Height = 0.1388889F;
            this.Shape18.Left = 0.01388889F;
            this.Shape18.Name = "Shape18";
            this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape18.Top = 9.802083F;
            this.Shape18.Width = 0.1388889F;
            // 
            // Shape19
            // 
            this.Shape19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape19.Height = 0.1388889F;
            this.Shape19.Left = 7.357639F;
            this.Shape19.Name = "Shape19";
            this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape19.Top = 9.802083F;
            this.Shape19.Width = 0.1388889F;
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.Height = 0.1666667F;
            this.txtName.Left = 0.104F;
            this.txtName.MultiLine = false;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; white-space: nowra" +
    "p; ddo-wrap-mode: nowrap";
            this.txtName.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName.Top = 1.833F;
            this.txtName.Width = 3.427083F;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Height = 0.1666667F;
            this.txtAddress1.Left = 0.104F;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtAddress1.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtAddress1.Top = 2.323F;
            this.txtAddress1.Width = 3.40625F;
            // 
            // txtWithholdingAccount
            // 
            this.txtWithholdingAccount.Height = 0.1666667F;
            this.txtWithholdingAccount.Left = 1.823F;
            this.txtWithholdingAccount.Name = "txtWithholdingAccount";
            this.txtWithholdingAccount.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left";
            this.txtWithholdingAccount.Text = "99 999999999";
            this.txtWithholdingAccount.Top = 1.511F;
            this.txtWithholdingAccount.Width = 1.833333F;
            // 
            // txtQuarterNum
            // 
            this.txtQuarterNum.Height = 0.1666667F;
            this.txtQuarterNum.Left = 4.156F;
            this.txtQuarterNum.Name = "txtQuarterNum";
            this.txtQuarterNum.Style = "font-family: \\000027Courier\\000020New\\000027; text-align: left";
            this.txtQuarterNum.Text = "9";
            this.txtQuarterNum.Top = 0.854F;
            this.txtQuarterNum.Width = 0.25F;
            // 
            // txtLine1
            // 
            this.txtLine1.Height = 0.1666667F;
            this.txtLine1.Left = 5.823F;
            this.txtLine1.Name = "txtLine1";
            this.txtLine1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtLine1.Text = "9999999999.99";
            this.txtLine1.Top = 1.667F;
            this.txtLine1.Width = 1.5F;
            // 
            // txtLine2
            // 
            this.txtLine2.Height = 0.1666667F;
            this.txtLine2.Left = 5.823F;
            this.txtLine2.Name = "txtLine2";
            this.txtLine2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtLine2.Text = "9999999999.99";
            this.txtLine2.Top = 2.1667F;
            this.txtLine2.Width = 1.5F;
            // 
            // txtLine3a
            // 
            this.txtLine3a.Height = 0.1666667F;
            this.txtLine3a.Left = 5.823F;
            this.txtLine3a.Name = "txtLine3a";
            this.txtLine3a.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtLine3a.Text = "9999999999.99";
            this.txtLine3a.Top = 3.5F;
            this.txtLine3a.Width = 1.5F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1666667F;
            this.txtDate.Left = 5.84375F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDate.Text = "99 99 99";
            this.txtDate.Top = 7.475695F;
            this.txtDate.Width = 0.75F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.1666667F;
            this.txtPhone.Left = 3.416667F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtPhone.Text = "999 999 9999";
            this.txtPhone.Top = 7.822917F;
            this.txtPhone.Width = 1.25F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.25F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 1.916667F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
            this.Label26.Text = "MAINE REVENUE SERVICES";
            this.Label26.Top = 0.125F;
            this.Label26.Width = 3F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1875F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 1.916667F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.Label27.Text = "EMPLOYER\'S RETURN";
            this.Label27.Top = 0.375F;
            this.Label27.Width = 3F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.25F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 0.8541667F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: \\000027Arial\\000027; font-size: 16pt; font-weight: bold";
            this.Label28.Text = "2021";
            this.Label28.Top = 0.5F;
            this.Label28.Width = 0.6666667F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.1875F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 1.75F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.Label29.Text = "OF MAINE INCOME TAX WITHHOLDING";
            this.Label29.Top = 0.5625F;
            this.Label29.Width = 3.416667F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.1666667F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 0.08333334F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-size: 8.5pt";
            this.Label30.Text = "Under penalties of perjury, I certify that the information contained on this retu" +
    "rn, report and attachment(s) is true and correct.";
            this.Label30.Top = 7.194445F;
            this.Label30.Width = 6.666667F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.1666667F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 3.2125F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-weight: bold";
            this.Label31.Text = "QUARTER #";
            this.Label31.Top = 0.8333333F;
            this.Label31.Width = 0.8333333F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.1666667F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 5.5F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-size: 7pt; vertical-align: middle";
            this.Label32.Text = "Date";
            this.Label32.Top = 7.506945F;
            this.Label32.Width = 0.4166667F;
            // 
            // Label37
            // 
            this.Label37.Height = 0.1666667F;
            this.Label37.HyperLink = null;
            this.Label37.Left = 2.010417F;
            this.Label37.Name = "Label37";
            this.Label37.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; text-decorati" +
    "on: underline; vertical-align: top";
            this.Label37.Text = "Treasurer, State of Maine";
            this.Label37.Top = 9.354167F;
            this.Label37.Width = 1.166667F;
            // 
            // Label40
            // 
            this.Label40.Height = 0.1666667F;
            this.Label40.HyperLink = null;
            this.Label40.Left = 3.089583F;
            this.Label40.Name = "Label40";
            this.Label40.Style = "font-size: 7pt; vertical-align: middle";
            this.Label40.Text = "Telephone";
            this.Label40.Top = 7.854167F;
            this.Label40.Width = 0.5833333F;
            // 
            // Label41
            // 
            this.Label41.Height = 0.1666667F;
            this.Label41.HyperLink = null;
            this.Label41.Left = 0.1041667F;
            this.Label41.Name = "Label41";
            this.Label41.Style = "font-size: 7pt; vertical-align: middle";
            this.Label41.Text = "Signature";
            this.Label41.Top = 7.510417F;
            this.Label41.Width = 0.5833333F;
            // 
            // Line76
            // 
            this.Line76.Height = 0F;
            this.Line76.Left = 0.5416667F;
            this.Line76.LineWeight = 1F;
            this.Line76.Name = "Line76";
            this.Line76.Top = 7.635417F;
            this.Line76.Width = 4.458333F;
            this.Line76.X1 = 0.5416667F;
            this.Line76.X2 = 5F;
            this.Line76.Y1 = 7.635417F;
            this.Line76.Y2 = 7.635417F;
            // 
            // Label42
            // 
            this.Label42.Height = 0.1666667F;
            this.Label42.HyperLink = null;
            this.Label42.Left = 2.510584F;
            this.Label42.Name = "Label42";
            this.Label42.Style = "font-size: 7pt; vertical-align: middle";
            this.Label42.Text = "State";
            this.Label42.Top = 2.923583F;
            this.Label42.Width = 0.3333333F;
            // 
            // Label43
            // 
            this.Label43.Height = 0.281F;
            this.Label43.HyperLink = null;
            this.Label43.Left = 4.042F;
            this.Label43.Name = "Label43";
            this.Label43.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label43.Text = "Total Maine income tax withheld for this quarter";
            this.Label43.Top = 1.55F;
            this.Label43.Width = 1.229167F;
            // 
            // Label44
            // 
            this.Label44.Height = 0.6770833F;
            this.Label44.HyperLink = null;
            this.Label44.Left = 4.041667F;
            this.Label44.Name = "Label44";
            this.Label44.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label44.Text = "Payments made (semiweekly payments from Schedule 1, line 5 plus, if amended, any " +
    "payments made with, or after filing, the original return)";
            this.Label44.Top = 1.845833F;
            this.Label44.Width = 1.3125F;
            // 
            // Label45
            // 
            this.Label45.Height = 0.225F;
            this.Label45.HyperLink = null;
            this.Label45.Left = 4.042F;
            this.Label45.Name = "Label45";
            this.Label45.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label45.Text = "Amount due with this return (see instructions)";
            this.Label45.Top = 3.476F;
            this.Label45.Width = 1.34375F;
            // 
            // Label46
            // 
            this.Label46.Height = 0.1666667F;
            this.Label46.HyperLink = null;
            this.Label46.Left = 3.792001F;
            this.Label46.Name = "Label46";
            this.Label46.Style = "font-size: 7pt; text-align: right; vertical-align: bottom";
            this.Label46.Text = "1.";
            this.Label46.Top = 1.55F;
            this.Label46.Width = 0.25F;
            // 
            // Label47
            // 
            this.Label47.Height = 0.1666667F;
            this.Label47.HyperLink = null;
            this.Label47.Left = 3.791667F;
            this.Label47.Name = "Label47";
            this.Label47.Style = "font-size: 7pt; text-align: right; vertical-align: bottom";
            this.Label47.Text = "2a.";
            this.Label47.Top = 1.845833F;
            this.Label47.Width = 0.25F;
            // 
            // Label48
            // 
            this.Label48.Height = 0.1666667F;
            this.Label48.HyperLink = null;
            this.Label48.Left = 3.792001F;
            this.Label48.Name = "Label48";
            this.Label48.Style = "font-size: 7pt; text-align: right; vertical-align: bottom";
            this.Label48.Text = "3a.";
            this.Label48.Top = 3.476F;
            this.Label48.Width = 0.25F;
            // 
            // Label52
            // 
            this.Label52.Height = 0.1666667F;
            this.Label52.HyperLink = null;
            this.Label52.Left = 0.104F;
            this.Label52.Name = "Label52";
            this.Label52.Style = "font-size: 7pt";
            this.Label52.Text = "Name";
            this.Label52.Top = 2.0205F;
            this.Label52.Width = 1.583333F;
            // 
            // Label53
            // 
            this.Label53.Height = 0.1666667F;
            this.Label53.HyperLink = null;
            this.Label53.Left = 4.688F;
            this.Label53.Name = "Label53";
            this.Label53.Style = "font-size: 7pt; vertical-align: middle";
            this.Label53.Text = "Contact Person E-mail";
            this.Label53.Top = 7.84375F;
            this.Label53.Width = 1.103667F;
            // 
            // txtEmail
            // 
            this.txtEmail.CanGrow = false;
            this.txtEmail.Height = 0.1666667F;
            this.txtEmail.Left = 5.84375F;
            this.txtEmail.MultiLine = false;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "white-space: nowrap; ddo-wrap-mode: nowrap";
            this.txtEmail.Text = "XXXXXXXXXXXXXXXXXX";
            this.txtEmail.Top = 7.822917F;
            this.txtEmail.Width = 1.625F;
            // 
            // txtEIN
            // 
            this.txtEIN.Height = 0.1666667F;
            this.txtEIN.Left = 4.989583F;
            this.txtEIN.Name = "txtEIN";
            this.txtEIN.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left";
            this.txtEIN.Text = "99 9999999";
            this.txtEIN.Top = 8.743055F;
            this.txtEIN.Width = 1.583333F;
            // 
            // Label54
            // 
            this.Label54.Height = 0.1666667F;
            this.Label54.HyperLink = null;
            this.Label54.Left = 4.0625F;
            this.Label54.Name = "Label54";
            this.Label54.Style = "font-size: 7pt; vertical-align: middle";
            this.Label54.Text = "Paid Preparer EIN:";
            this.Label54.Top = 8.743055F;
            this.Label54.Width = 0.9166667F;
            // 
            // Label58
            // 
            this.Label58.Height = 0.1666667F;
            this.Label58.HyperLink = null;
            this.Label58.Left = 0.75F;
            this.Label58.Name = "Label58";
            this.Label58.Style = "font-size: 9pt; font-weight: bold";
            this.Label58.Text = "FORM 941ME";
            this.Label58.Top = 0.1875F;
            this.Label58.Width = 1F;
            // 
            // Label60
            // 
            this.Label60.Height = 0.2708333F;
            this.Label60.HyperLink = null;
            this.Label60.Left = 0.062F;
            this.Label60.Name = "Label60";
            this.Label60.Style = "font-family: \\000027Arial\\000027; font-size: 8.5pt; text-align: left";
            this.Label60.Text = resources.GetString("Label60.Text");
            this.Label60.Top = 4.577F;
            this.Label60.Width = 7.333333F;
            // 
            // Label67
            // 
            this.Label67.Height = 0.1666667F;
            this.Label67.HyperLink = null;
            this.Label67.Left = 0.1041667F;
            this.Label67.Name = "Label67";
            this.Label67.Style = "font-size: 7pt";
            this.Label67.Text = "Withholding Account Number:";
            this.Label67.Top = 1.53125F;
            this.Label67.Width = 1.416667F;
            // 
            // Label68
            // 
            this.Label68.Height = 0.1666667F;
            this.Label68.HyperLink = null;
            this.Label68.Left = 4.729167F;
            this.Label68.Name = "Label68";
            this.Label68.Style = "font-size: 7pt";
            this.Label68.Text = "Quarter Period Covered:";
            this.Label68.Top = 0.7395833F;
            this.Label68.Width = 1.166667F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1666667F;
            this.txtCity.Left = 0.104F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtCity.Text = "XXXXXXXXXXXXXXXX";
            this.txtCity.Top = 2.7F;
            this.txtCity.Width = 1.458333F;
            // 
            // txtPeriodStart
            // 
            this.txtPeriodStart.Height = 0.1666667F;
            this.txtPeriodStart.Left = 4.979167F;
            this.txtPeriodStart.Name = "txtPeriodStart";
            this.txtPeriodStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodStart.Text = "99 99 9999";
            this.txtPeriodStart.Top = 1.006944F;
            this.txtPeriodStart.Visible = false;
            this.txtPeriodStart.Width = 1.03125F;
            // 
            // txtPeriodEnd
            // 
            this.txtPeriodEnd.Height = 0.1666667F;
            this.txtPeriodEnd.Left = 6.340278F;
            this.txtPeriodEnd.Name = "txtPeriodEnd";
            this.txtPeriodEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodEnd.Text = "99 99  9999";
            this.txtPeriodEnd.Top = 0.7569444F;
            this.txtPeriodEnd.Visible = false;
            this.txtPeriodEnd.Width = 0.9895833F;
            // 
            // Field25
            // 
            this.Field25.Height = 0.1666667F;
            this.Field25.Left = 6.0625F;
            this.Field25.Name = "Field25";
            this.Field25.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: right" +
    "; vertical-align: top";
            this.Field25.Text = "-";
            this.Field25.Top = 1.020833F;
            this.Field25.Width = 0.25F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1666667F;
            this.txtState.Left = 2.510584F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtState.Text = "XX";
            this.txtState.Top = 2.7F;
            this.txtState.Width = 0.25F;
            // 
            // txtZip
            // 
            this.txtZip.Height = 0.1666667F;
            this.txtZip.Left = 2.937667F;
            this.txtZip.Name = "txtZip";
            this.txtZip.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtZip.Text = "99999";
            this.txtZip.Top = 2.7F;
            this.txtZip.Width = 0.5F;
            // 
            // txtLine3b
            // 
            this.txtLine3b.Height = 0.1666667F;
            this.txtLine3b.Left = 5.823F;
            this.txtLine3b.Name = "txtLine3b";
            this.txtLine3b.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtLine3b.Text = "9999999999.99";
            this.txtLine3b.Top = 3.8334F;
            this.txtLine3b.Width = 1.5F;
            // 
            // Label76
            // 
            this.Label76.Height = 0.3020833F;
            this.Label76.HyperLink = null;
            this.Label76.Left = 4.042F;
            this.Label76.Name = "Label76";
            this.Label76.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label76.Text = "Overpayment to be refunded  (see instructions)";
            this.Label76.Top = 3.797F;
            this.Label76.Width = 1.364583F;
            // 
            // Line87
            // 
            this.Line87.Height = 0F;
            this.Line87.Left = 0F;
            this.Line87.LineWeight = 1F;
            this.Line87.Name = "Line87";
            this.Line87.Top = 9.59375F;
            this.Line87.Visible = false;
            this.Line87.Width = 7.5F;
            this.Line87.X1 = 0F;
            this.Line87.X2 = 7.5F;
            this.Line87.Y1 = 9.59375F;
            this.Line87.Y2 = 9.59375F;
            // 
            // Label83
            // 
            this.Label83.Height = 0.1666667F;
            this.Label83.HyperLink = null;
            this.Label83.Left = 3.792001F;
            this.Label83.Name = "Label83";
            this.Label83.Style = "font-size: 7pt; text-align: right; vertical-align: bottom";
            this.Label83.Text = "3b.";
            this.Label83.Top = 3.797F;
            this.Label83.Width = 0.25F;
            // 
            // Label74
            // 
            this.Label74.Height = 0.1666667F;
            this.Label74.HyperLink = null;
            this.Label74.Left = 0.4375F;
            this.Label74.Name = "Label74";
            this.Label74.Style = "font-family: \\000027Courier\\000020New\\000027";
            this.Label74.Text = "TSC";
            this.Label74.Top = 9.791667F;
            this.Label74.Visible = false;
            this.Label74.Width = 0.4166667F;
            // 
            // Label106
            // 
            this.Label106.Height = 0.1666667F;
            this.Label106.HyperLink = null;
            this.Label106.Left = 4.0625F;
            this.Label106.Name = "Label106";
            this.Label106.Style = "font-size: 7pt; vertical-align: middle";
            this.Label106.Text = "Maine Payroll Processor License Number:";
            this.Label106.Top = 9.083333F;
            this.Label106.Width = 1.916667F;
            // 
            // txtMaineLicense
            // 
            this.txtMaineLicense.Height = 0.1666667F;
            this.txtMaineLicense.Left = 6.013889F;
            this.txtMaineLicense.Name = "txtMaineLicense";
            this.txtMaineLicense.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left";
            this.txtMaineLicense.Text = "XXXXXXXXX";
            this.txtMaineLicense.Top = 9.079861F;
            this.txtMaineLicense.Width = 1.208333F;
            // 
            // txtNumberofPayees
            // 
            this.txtNumberofPayees.Height = 0.1666667F;
            this.txtNumberofPayees.Left = 6.84375F;
            this.txtNumberofPayees.Name = "txtNumberofPayees";
            this.txtNumberofPayees.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtNumberofPayees.Text = "99999";
            this.txtNumberofPayees.Top = 1.03125F;
            this.txtNumberofPayees.Visible = false;
            this.txtNumberofPayees.Width = 0.5F;
            // 
            // Label109
            // 
            this.Label109.Height = 0.1666667F;
            this.Label109.HyperLink = null;
            this.Label109.Left = 6.0625F;
            this.Label109.Name = "Label109";
            this.Label109.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; text-align: right;" +
    " vertical-align: middle";
            this.Label109.Text = "A";
            this.Label109.Top = 1.03125F;
            this.Label109.Visible = false;
            this.Label109.Width = 0.25F;
            // 
            // txtMagnetic
            // 
            this.txtMagnetic.Height = 0.1666667F;
            this.txtMagnetic.Left = 3.5F;
            this.txtMagnetic.Name = "txtMagnetic";
            this.txtMagnetic.Style = "font-family: \\000027Courier\\000020New\\000027; text-align: left";
            this.txtMagnetic.Text = "X";
            this.txtMagnetic.Top = 3.1667F;
            this.txtMagnetic.Width = 0.1666667F;
            // 
            // Label110
            // 
            this.Label110.Height = 0.3645833F;
            this.Label110.HyperLink = null;
            this.Label110.Left = 0.355F;
            this.Label110.Name = "Label110";
            this.Label110.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label110.Text = "Check here if MRS granted a waiver allowing you to exclude non-wage withholding f" +
    "rom Schedule 2. See instructions";
            this.Label110.Top = 3.106F;
            this.Label110.Width = 2.635417F;
            // 
            // Label111
            // 
            this.Label111.Height = 0.1666667F;
            this.Label111.HyperLink = null;
            this.Label111.Left = 0.042F;
            this.Label111.Name = "Label111";
            this.Label111.Style = "font-size: 7pt; text-align: right; vertical-align: bottom";
            this.Label111.Text = "A";
            this.Label111.Top = 3.146F;
            this.Label111.Width = 0.25F;
            // 
            // Label112
            // 
            this.Label112.Height = 0.1666667F;
            this.Label112.HyperLink = null;
            this.Label112.Left = 3.187F;
            this.Label112.Name = "Label112";
            this.Label112.Style = "font-size: 7pt; text-align: left; vertical-align: middle";
            this.Label112.Text = "A";
            this.Label112.Top = 3.222F;
            this.Label112.Width = 0.25F;
            // 
            // Line94
            // 
            this.Line94.Height = 0F;
            this.Line94.Left = 5.760417F;
            this.Line94.LineWeight = 1F;
            this.Line94.Name = "Line94";
            this.Line94.Top = 7.635417F;
            this.Line94.Width = 1.708333F;
            this.Line94.X1 = 5.760417F;
            this.Line94.X2 = 7.46875F;
            this.Line94.Y1 = 7.635417F;
            this.Line94.Y2 = 7.635417F;
            // 
            // Line96
            // 
            this.Line96.Height = 0F;
            this.Line96.Left = 3.604167F;
            this.Line96.LineWeight = 1F;
            this.Line96.Name = "Line96";
            this.Line96.Top = 7.982639F;
            this.Line96.Width = 1.0625F;
            this.Line96.X1 = 3.604167F;
            this.Line96.X2 = 4.666667F;
            this.Line96.Y1 = 7.982639F;
            this.Line96.Y2 = 7.982639F;
            // 
            // Line97
            // 
            this.Line97.Height = 0F;
            this.Line97.Left = 5.760417F;
            this.Line97.LineWeight = 1F;
            this.Line97.Name = "Line97";
            this.Line97.Top = 7.982639F;
            this.Line97.Width = 1.708333F;
            this.Line97.X1 = 5.760417F;
            this.Line97.X2 = 7.46875F;
            this.Line97.Y1 = 7.982639F;
            this.Line97.Y2 = 7.982639F;
            // 
            // Label113
            // 
            this.Label113.Height = 0.1666667F;
            this.Label113.HyperLink = null;
            this.Label113.Left = 0.062F;
            this.Label113.Name = "Label113";
            this.Label113.Style = "font-size: 7pt; font-weight: bold";
            this.Label113.Text = "Note:";
            this.Label113.Top = 4.577F;
            this.Label113.Visible = false;
            this.Label113.Width = 0.4166667F;
            // 
            // Label115
            // 
            this.Label115.Height = 0.1666667F;
            this.Label115.HyperLink = null;
            this.Label115.Left = 0.1041667F;
            this.Label115.Name = "Label115";
            this.Label115.Style = "font-size: 7pt; vertical-align: middle";
            this.Label115.Text = "Print Name:";
            this.Label115.Top = 7.84375F;
            this.Label115.Width = 0.5833333F;
            // 
            // Line194
            // 
            this.Line194.Height = 0F;
            this.Line194.Left = 0.6666667F;
            this.Line194.LineWeight = 1F;
            this.Line194.Name = "Line194";
            this.Line194.Top = 7.982639F;
            this.Line194.Width = 2.3125F;
            this.Line194.X1 = 0.6666667F;
            this.Line194.X2 = 2.979167F;
            this.Line194.Y1 = 7.982639F;
            this.Line194.Y2 = 7.982639F;
            // 
            // Label116
            // 
            this.Label116.Height = 0.2187497F;
            this.Label116.HyperLink = null;
            this.Label116.Left = 2.770833F;
            this.Label116.Name = "Label116";
            this.Label116.Style = "font-family: \\000027Tahoma\\000027; font-size: 10pt; font-weight: bold; text-align" +
    ": center";
            this.Label116.Text = "For Paid Preparers Only";
            this.Label116.Top = 8.03125F;
            this.Label116.Width = 2.125F;
            // 
            // Label117
            // 
            this.Label117.Height = 0.1666667F;
            this.Label117.HyperLink = null;
            this.Label117.Left = 0.1041667F;
            this.Label117.Name = "Label117";
            this.Label117.Style = "font-size: 7pt; vertical-align: middle";
            this.Label117.Text = "Address:";
            this.Label117.Top = 9.045139F;
            this.Label117.Width = 0.5833333F;
            // 
            // Line195
            // 
            this.Line195.Height = 0F;
            this.Line195.Left = 0.5416667F;
            this.Line195.LineWeight = 1F;
            this.Line195.Name = "Line195";
            this.Line195.Top = 9.177083F;
            this.Line195.Width = 3.375F;
            this.Line195.X1 = 0.5416667F;
            this.Line195.X2 = 3.916667F;
            this.Line195.Y1 = 9.177083F;
            this.Line195.Y2 = 9.177083F;
            // 
            // Label118
            // 
            this.Label118.Height = 0.1666667F;
            this.Label118.HyperLink = null;
            this.Label118.Left = 0.1041667F;
            this.Label118.Name = "Label118";
            this.Label118.Style = "font-size: 7pt; vertical-align: middle";
            this.Label118.Text = "Paid Preparers\'s Signature:";
            this.Label118.Top = 8.385417F;
            this.Label118.Width = 1.270833F;
            // 
            // Line196
            // 
            this.Line196.Height = 0F;
            this.Line196.Left = 1.354167F;
            this.Line196.LineWeight = 1F;
            this.Line196.Name = "Line196";
            this.Line196.Top = 8.53125F;
            this.Line196.Width = 2.375F;
            this.Line196.X1 = 1.354167F;
            this.Line196.X2 = 3.729167F;
            this.Line196.Y1 = 8.53125F;
            this.Line196.Y2 = 8.53125F;
            // 
            // Label119
            // 
            this.Label119.Height = 0.1666667F;
            this.Label119.HyperLink = null;
            this.Label119.Left = 3.8125F;
            this.Label119.Name = "Label119";
            this.Label119.Style = "font-size: 7pt; vertical-align: middle";
            this.Label119.Text = "Date:";
            this.Label119.Top = 8.385417F;
            this.Label119.Width = 0.3333333F;
            // 
            // Line197
            // 
            this.Line197.Height = 0F;
            this.Line197.Left = 4.104167F;
            this.Line197.LineWeight = 1F;
            this.Line197.Name = "Line197";
            this.Line197.Top = 8.53125F;
            this.Line197.Width = 0.9375F;
            this.Line197.X1 = 4.104167F;
            this.Line197.X2 = 5.041667F;
            this.Line197.Y1 = 8.53125F;
            this.Line197.Y2 = 8.53125F;
            // 
            // Label120
            // 
            this.Label120.Height = 0.1666667F;
            this.Label120.HyperLink = null;
            this.Label120.Left = 5.0625F;
            this.Label120.Name = "Label120";
            this.Label120.Style = "font-size: 7pt; vertical-align: middle";
            this.Label120.Text = "Telephone:";
            this.Label120.Top = 8.385417F;
            this.Label120.Width = 0.5833333F;
            // 
            // Line198
            // 
            this.Line198.Height = 0F;
            this.Line198.Left = 5.65625F;
            this.Line198.LineWeight = 1F;
            this.Line198.Name = "Line198";
            this.Line198.Top = 8.53125F;
            this.Line198.Width = 1.8125F;
            this.Line198.X1 = 5.65625F;
            this.Line198.X2 = 7.46875F;
            this.Line198.Y1 = 8.53125F;
            this.Line198.Y2 = 8.53125F;
            // 
            // Label121
            // 
            this.Label121.Height = 0.1666667F;
            this.Label121.HyperLink = null;
            this.Label121.Left = 0.1041667F;
            this.Label121.Name = "Label121";
            this.Label121.Style = "font-size: 7pt; vertical-align: middle";
            this.Label121.Text = "Firm\'s Name (or yours, if self-employed):";
            this.Label121.Top = 8.75F;
            this.Label121.Width = 1.833333F;
            // 
            // Line199
            // 
            this.Line199.Height = 0F;
            this.Line199.Left = 1.916667F;
            this.Line199.LineWeight = 1F;
            this.Line199.Name = "Line199";
            this.Line199.Top = 8.871528F;
            this.Line199.Width = 2F;
            this.Line199.X1 = 1.916667F;
            this.Line199.X2 = 3.916667F;
            this.Line199.Y1 = 8.871528F;
            this.Line199.Y2 = 8.871528F;
            // 
            // Label123
            // 
            this.Label123.Height = 0.1666667F;
            this.Label123.HyperLink = null;
            this.Label123.Left = 1.65625F;
            this.Label123.Name = "Label123";
            this.Label123.Style = "font-size: 7pt; font-weight: bold";
            this.Label123.Text = "If enclosing a check, make check payable to:";
            this.Label123.Top = 9.239583F;
            this.Label123.Width = 2.25F;
            // 
            // Label124
            // 
            this.Label124.Height = 0.3541667F;
            this.Label124.HyperLink = null;
            this.Label124.Left = 1.885417F;
            this.Label124.Name = "Label124";
            this.Label124.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label124.Text = "MAINE REVENUE SERVICES        P.O. Box 1065                  Augusta, ME 04332-10" +
    "65";
            this.Label124.Top = 9.614583F;
            this.Label124.Width = 1.40625F;
            // 
            // Label125
            // 
            this.Label125.Height = 0.1666667F;
            this.Label125.HyperLink = null;
            this.Label125.Left = 1.895833F;
            this.Label125.Name = "Label125";
            this.Label125.Style = "font-size: 7pt; font-weight: bold";
            this.Label125.Text = "and MAIL WITH RETURN TO:";
            this.Label125.Top = 9.486111F;
            this.Label125.Width = 1.541667F;
            // 
            // txtPeriodMonthEnd
            // 
            this.txtPeriodMonthEnd.Height = 0.1666667F;
            this.txtPeriodMonthEnd.Left = 6.423611F;
            this.txtPeriodMonthEnd.Name = "txtPeriodMonthEnd";
            this.txtPeriodMonthEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodMonthEnd.Text = "99";
            this.txtPeriodMonthEnd.Top = 1.006944F;
            this.txtPeriodMonthEnd.Width = 0.25F;
            // 
            // txtPeriodDayEnd
            // 
            this.txtPeriodDayEnd.Height = 0.1666667F;
            this.txtPeriodDayEnd.Left = 6.677083F;
            this.txtPeriodDayEnd.Name = "txtPeriodDayEnd";
            this.txtPeriodDayEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodDayEnd.Text = "99";
            this.txtPeriodDayEnd.Top = 1.006944F;
            this.txtPeriodDayEnd.Width = 0.25F;
            // 
            // txtPeriodYearEnd
            // 
            this.txtPeriodYearEnd.Height = 0.1666667F;
            this.txtPeriodYearEnd.Left = 6.927083F;
            this.txtPeriodYearEnd.Name = "txtPeriodYearEnd";
            this.txtPeriodYearEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodYearEnd.Text = "9999";
            this.txtPeriodYearEnd.Top = 1.006944F;
            this.txtPeriodYearEnd.Width = 0.5F;
            // 
            // txtPeriodMonthStart
            // 
            this.txtPeriodMonthStart.Height = 0.1666667F;
            this.txtPeriodMonthStart.Left = 4.979167F;
            this.txtPeriodMonthStart.Name = "txtPeriodMonthStart";
            this.txtPeriodMonthStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodMonthStart.Text = "99";
            this.txtPeriodMonthStart.Top = 1.006944F;
            this.txtPeriodMonthStart.Width = 0.25F;
            // 
            // txtPeriodDayStart
            // 
            this.txtPeriodDayStart.Height = 0.1666667F;
            this.txtPeriodDayStart.Left = 5.263889F;
            this.txtPeriodDayStart.Name = "txtPeriodDayStart";
            this.txtPeriodDayStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodDayStart.Text = "99";
            this.txtPeriodDayStart.Top = 1.006944F;
            this.txtPeriodDayStart.Width = 0.25F;
            // 
            // txtPeriodYearStart
            // 
            this.txtPeriodYearStart.Height = 0.1666667F;
            this.txtPeriodYearStart.Left = 5.652778F;
            this.txtPeriodYearStart.Name = "txtPeriodYearStart";
            this.txtPeriodYearStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtPeriodYearStart.Text = "9999";
            this.txtPeriodYearStart.Top = 1.006944F;
            this.txtPeriodYearStart.Width = 0.4375F;
            // 
            // Label126
            // 
            this.Label126.Height = 0.1666667F;
            this.Label126.HyperLink = null;
            this.Label126.Left = 0.1666667F;
            this.Label126.Name = "Label126";
            this.Label126.Style = "font-size: 7pt";
            this.Label126.Text = "Due on or Before:";
            this.Label126.Top = 0.78125F;
            this.Label126.Width = 0.9166667F;
            // 
            // txtDueMonth
            // 
            this.txtDueMonth.Height = 0.1666667F;
            this.txtDueMonth.Left = 0.3958333F;
            this.txtDueMonth.Name = "txtDueMonth";
            this.txtDueMonth.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDueMonth.Text = "99";
            this.txtDueMonth.Top = 1.027778F;
            this.txtDueMonth.Width = 0.25F;
            // 
            // txtDueDay
            // 
            this.txtDueDay.Height = 0.1666667F;
            this.txtDueDay.Left = 0.7083333F;
            this.txtDueDay.Name = "txtDueDay";
            this.txtDueDay.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDueDay.Text = "99";
            this.txtDueDay.Top = 1.027778F;
            this.txtDueDay.Width = 0.25F;
            // 
            // txtDueYear
            // 
            this.txtDueYear.Height = 0.1666667F;
            this.txtDueYear.Left = 1.097222F;
            this.txtDueYear.Name = "txtDueYear";
            this.txtDueYear.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDueYear.Text = "9999";
            this.txtDueYear.Top = 1.027778F;
            this.txtDueYear.Width = 0.4375F;
            // 
            // Line200
            // 
            this.Line200.Height = 0F;
            this.Line200.Left = 0.01041667F;
            this.Line200.LineWeight = 1F;
            this.Line200.Name = "Line200";
            this.Line200.Top = 1.1875F;
            this.Line200.Visible = false;
            this.Line200.Width = 7.5F;
            this.Line200.X1 = 0.01041667F;
            this.Line200.X2 = 7.510417F;
            this.Line200.Y1 = 1.1875F;
            this.Line200.Y2 = 1.1875F;
            // 
            // Line201
            // 
            this.Line201.Height = 0F;
            this.Line201.Left = 0.125F;
            this.Line201.LineWeight = 1F;
            this.Line201.Name = "Line201";
            this.Line201.Top = 6.895833F;
            this.Line201.Width = 7.25F;
            this.Line201.X1 = 0.125F;
            this.Line201.X2 = 7.375F;
            this.Line201.Y1 = 6.895833F;
            this.Line201.Y2 = 6.895833F;
            // 
            // Label127
            // 
            this.Label127.Height = 0.1666667F;
            this.Label127.HyperLink = null;
            this.Label127.Left = 0.104F;
            this.Label127.Name = "Label127";
            this.Label127.Style = "font-size: 7pt";
            this.Label127.Text = "Address";
            this.Label127.Top = 2.5105F;
            this.Label127.Width = 1.583333F;
            // 
            // Label128
            // 
            this.Label128.Height = 0.1666667F;
            this.Label128.HyperLink = null;
            this.Label128.Left = 0.104F;
            this.Label128.Name = "Label128";
            this.Label128.Style = "font-size: 7pt";
            this.Label128.Text = "City";
            this.Label128.Top = 2.91F;
            this.Label128.Width = 1.020833F;
            // 
            // Label129
            // 
            this.Label129.Height = 0.1666667F;
            this.Label129.HyperLink = null;
            this.Label129.Left = 4.916667F;
            this.Label129.Name = "Label129";
            this.Label129.Style = "font-size: 7pt; font-weight: bold";
            this.Label129.Text = "If not enclosing a check,";
            this.Label129.Top = 9.302083F;
            this.Label129.Width = 2.25F;
            // 
            // Label130
            // 
            this.Label130.Height = 0.1666667F;
            this.Label130.HyperLink = null;
            this.Label130.Left = 5F;
            this.Label130.Name = "Label130";
            this.Label130.Style = "font-size: 7pt; font-weight: bold";
            this.Label130.Text = "MAIL RETURN TO:";
            this.Label130.Top = 9.416667F;
            this.Label130.Width = 1.541667F;
            // 
            // Label131
            // 
            this.Label131.Height = 0.3541667F;
            this.Label131.HyperLink = null;
            this.Label131.Left = 4.854167F;
            this.Label131.Name = "Label131";
            this.Label131.Style = "font-family: \\000027Arial\\000027; font-size: 7pt; text-align: left; vertical-alig" +
    "n: top";
            this.Label131.Text = "MAINE REVENUE SERVICES        P.O. Box 1064                  Augusta, ME 04332-10" +
    "64";
            this.Label131.Top = 9.614583F;
            this.Label131.Width = 1.40625F;
            // 
            // txtDueDate
            // 
            this.txtDueDate.Height = 0.1666667F;
            this.txtDueDate.Left = 0.2916667F;
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDueDate.Text = "99 99 9999";
            this.txtDueDate.Top = 0.96875F;
            this.txtDueDate.Visible = false;
            this.txtDueDate.Width = 1.03125F;
            // 
            // txtLine2b
            // 
            this.txtLine2b.Height = 0.1666667F;
            this.txtLine2b.Left = 5.823F;
            this.txtLine2b.Name = "txtLine2b";
            this.txtLine2b.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtLine2b.Text = "9999999999.99";
            this.txtLine2b.Top = 2.667F;
            this.txtLine2b.Width = 1.5F;
            // 
            // txtLine2c
            // 
            this.txtLine2c.Height = 0.1666667F;
            this.txtLine2c.Left = 5.823F;
            this.txtLine2c.Name = "txtLine2c";
            this.txtLine2c.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtLine2c.Text = "9999999999.99";
            this.txtLine2c.Top = 3.1667F;
            this.txtLine2c.Width = 1.5F;
            // 
            // Label132
            // 
            this.Label132.Height = 0.2916667F;
            this.Label132.HyperLink = null;
            this.Label132.Left = 0.062F;
            this.Label132.Name = "Label132";
            this.Label132.Style = "font-size: 8.5pt";
            this.Label132.Text = resources.GetString("Label132.Text");
            this.Label132.Top = 4.212417F;
            this.Label132.Width = 7.354167F;
            // 
            // Label133
            // 
            this.Label133.Height = 0.1666667F;
            this.Label133.HyperLink = null;
            this.Label133.Left = 0.062F;
            this.Label133.Name = "Label133";
            this.Label133.Style = "font-size: 8.5pt";
            this.Label133.Text = "4. By checking the box(es) below, I certify that:";
            this.Label133.Top = 4.899917F;
            this.Label133.Width = 2.729167F;
            // 
            // txtOverpaymentCheckbox
            // 
            this.txtOverpaymentCheckbox.Height = 0.1666667F;
            this.txtOverpaymentCheckbox.Left = 0.111F;
            this.txtOverpaymentCheckbox.Name = "txtOverpaymentCheckbox";
            this.txtOverpaymentCheckbox.Style = "font-family: \\000027Courier\\000020New\\000027; text-align: left";
            this.txtOverpaymentCheckbox.Text = "X";
            this.txtOverpaymentCheckbox.Top = 5.356F;
            this.txtOverpaymentCheckbox.Width = 0.1666667F;
            // 
            // txtPayeeStatementsSent
            // 
            this.txtPayeeStatementsSent.Height = 0.1666667F;
            this.txtPayeeStatementsSent.Left = 0.111F;
            this.txtPayeeStatementsSent.Name = "txtPayeeStatementsSent";
            this.txtPayeeStatementsSent.Style = "font-family: \\000027Courier\\000020New\\000027; text-align: left";
            this.txtPayeeStatementsSent.Text = "X";
            this.txtPayeeStatementsSent.Top = 5.689333F;
            this.txtPayeeStatementsSent.Width = 0.1666667F;
            // 
            // txtEnclosingAmendedw3me
            // 
            this.txtEnclosingAmendedw3me.Height = 0.1666667F;
            this.txtEnclosingAmendedw3me.Left = 0.111F;
            this.txtEnclosingAmendedw3me.Name = "txtEnclosingAmendedw3me";
            this.txtEnclosingAmendedw3me.Style = "font-family: \\000027Courier\\000020New\\000027; text-align: left";
            this.txtEnclosingAmendedw3me.Text = "X";
            this.txtEnclosingAmendedw3me.Top = 6.023F;
            this.txtEnclosingAmendedw3me.Width = 0.1666667F;
            // 
            // Label134
            // 
            this.Label134.Height = 0.5416667F;
            this.Label134.HyperLink = null;
            this.Label134.Left = 0.4995F;
            this.Label134.Name = "Label134";
            this.Label134.Style = "font-size: 8.5pt";
            this.Label134.Text = resources.GetString("Label134.Text");
            this.Label134.Top = 5.066584F;
            this.Label134.Width = 6.979167F;
            // 
            // Label135
            // 
            this.Label135.Height = 0.2916667F;
            this.Label135.HyperLink = null;
            this.Label135.Left = 0.499F;
            this.Label135.Name = "Label135";
            this.Label135.Style = "font-size: 8.5pt";
            this.Label135.Text = resources.GetString("Label135.Text");
            this.Label135.Top = 5.692F;
            this.Label135.Width = 6.916667F;
            // 
            // Label136
            // 
            this.Label136.Height = 0.1666667F;
            this.Label136.HyperLink = null;
            this.Label136.Left = 0.4995F;
            this.Label136.Name = "Label136";
            this.Label136.Style = "font-size: 8.5pt";
            this.Label136.Text = "I am enclosing an amended Form W-3ME (Reconciliation of Maine Income Tax Withheld" +
    ") to reflect changes made on this form.";
            this.Label136.Top = 6.066584F;
            this.Label136.Width = 6.916667F;
            // 
            // Label137
            // 
            this.Label137.Height = 0.1770833F;
            this.Label137.HyperLink = null;
            this.Label137.Left = 0.08333334F;
            this.Label137.Name = "Label137";
            this.Label137.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label137.Text = "Explanation of adjustments:";
            this.Label137.Top = 6.5F;
            this.Label137.Width = 1.385417F;
            // 
            // Label138
            // 
            this.Label138.Height = 0.4375003F;
            this.Label138.HyperLink = null;
            this.Label138.Left = 4.041667F;
            this.Label138.Name = "Label138";
            this.Label138.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label138.Text = "If amended, overpayment on original return or as previously adjusted";
            this.Label138.Top = 2.5125F;
            this.Label138.Width = 1.229167F;
            // 
            // Label139
            // 
            this.Label139.Height = 0.1666667F;
            this.Label139.HyperLink = null;
            this.Label139.Left = 3.791667F;
            this.Label139.Name = "Label139";
            this.Label139.Style = "font-size: 7pt; text-align: right; vertical-align: bottom";
            this.Label139.Text = "2b.";
            this.Label139.Top = 2.5125F;
            this.Label139.Width = 0.25F;
            // 
            // Label140
            // 
            this.Label140.Height = 0.1670001F;
            this.Label140.HyperLink = null;
            this.Label140.Left = 4.042F;
            this.Label140.Name = "Label140";
            this.Label140.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label140.Text = "Line 2a minus line 2b.";
            this.Label140.Top = 3.19F;
            this.Label140.Width = 1.229167F;
            // 
            // Label141
            // 
            this.Label141.Height = 0.1666667F;
            this.Label141.HyperLink = null;
            this.Label141.Left = 3.792001F;
            this.Label141.Name = "Label141";
            this.Label141.Style = "font-size: 7pt; text-align: right; vertical-align: bottom";
            this.Label141.Text = "2c.";
            this.Label141.Top = 3.19F;
            this.Label141.Width = 0.25F;
            // 
            // Line202
            // 
            this.Line202.Height = 0F;
            this.Line202.Left = 1.4375F;
            this.Line202.LineWeight = 1F;
            this.Line202.Name = "Line202";
            this.Line202.Top = 6.625F;
            this.Line202.Width = 5.9375F;
            this.Line202.X1 = 1.4375F;
            this.Line202.X2 = 7.375F;
            this.Line202.Y1 = 6.625F;
            this.Line202.Y2 = 6.625F;
            // 
            // Line203
            // 
            this.Line203.Height = 0F;
            this.Line203.Left = 0.125F;
            this.Line203.LineWeight = 1F;
            this.Line203.Name = "Line203";
            this.Line203.Top = 7.1875F;
            this.Line203.Width = 7.25F;
            this.Line203.X1 = 0.125F;
            this.Line203.X2 = 7.375F;
            this.Line203.Y1 = 7.1875F;
            this.Line203.Y2 = 7.1875F;
            // 
            // Label142
            // 
            this.Label142.Height = 0.1666667F;
            this.Label142.HyperLink = null;
            this.Label142.Left = 0.354F;
            this.Label142.Name = "Label142";
            this.Label142.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label142.Text = "Check here if this is an amended return. See instructions";
            this.Label142.Top = 3.584F;
            this.Label142.Width = 2.635417F;
            // 
            // Label143
            // 
            this.Label143.Height = 0.1666667F;
            this.Label143.HyperLink = null;
            this.Label143.Left = 0.04166667F;
            this.Label143.Name = "Label143";
            this.Label143.Style = "font-size: 7pt; text-align: right; vertical-align: middle";
            this.Label143.Text = "B";
            this.Label143.Top = 3.604167F;
            this.Label143.Width = 0.25F;
            // 
            // Label144
            // 
            this.Label144.Height = 0.1666667F;
            this.Label144.HyperLink = null;
            this.Label144.Left = 3.187F;
            this.Label144.Name = "Label144";
            this.Label144.Style = "font-size: 7pt; text-align: left; vertical-align: middle";
            this.Label144.Text = "B";
            this.Label144.Top = 3.546F;
            this.Label144.Width = 0.25F;
            // 
            // txtAmendedReturn
            // 
            this.txtAmendedReturn.Height = 0.1666667F;
            this.txtAmendedReturn.Left = 3.5F;
            this.txtAmendedReturn.Name = "txtAmendedReturn";
            this.txtAmendedReturn.Style = "font-family: \\000027Courier\\000020New\\000027; text-align: left";
            this.txtAmendedReturn.Text = "X";
            this.txtAmendedReturn.Top = 3.5F;
            this.txtAmendedReturn.Width = 0.1666667F;
            // 
            // Label145
            // 
            this.Label145.Height = 0.3541667F;
            this.Label145.HyperLink = null;
            this.Label145.Left = 0.354F;
            this.Label145.Name = "Label145";
            this.Label145.Style = "font-family: \\000027Arial\\000020Narrow\\000027; font-size: 7pt; vertical-align: mi" +
    "ddle";
            this.Label145.Text = "Check here to close your withholding account.";
            this.Label145.Top = 3.834F;
            this.Label145.Width = 2.635417F;
            // 
            // Label146
            // 
            this.Label146.Height = 0.1666667F;
            this.Label146.HyperLink = null;
            this.Label146.Left = 0.042F;
            this.Label146.Name = "Label146";
            this.Label146.Style = "font-size: 7pt; text-align: right; vertical-align: middle";
            this.Label146.Text = "C";
            this.Label146.Top = 3.925F;
            this.Label146.Width = 0.25F;
            // 
            // Label147
            // 
            this.Label147.Height = 0.1666667F;
            this.Label147.HyperLink = null;
            this.Label147.Left = 3.187F;
            this.Label147.Name = "Label147";
            this.Label147.Style = "font-size: 7pt; text-align: left; vertical-align: middle";
            this.Label147.Text = "C";
            this.Label147.Top = 3.931F;
            this.Label147.Width = 0.25F;
            // 
            // txtCloseAccount
            // 
            this.txtCloseAccount.Height = 0.1666667F;
            this.txtCloseAccount.Left = 3.5F;
            this.txtCloseAccount.Name = "txtCloseAccount";
            this.txtCloseAccount.Style = "font-family: \\000027Courier\\000020New\\000027; text-align: left";
            this.txtCloseAccount.Text = "X";
            this.txtCloseAccount.Top = 3.834F;
            this.txtCloseAccount.Width = 0.1666667F;
            // 
            // Label148
            // 
            this.Label148.Height = 0.1666667F;
            this.Label148.HyperLink = null;
            this.Label148.Left = 2.937667F;
            this.Label148.Name = "Label148";
            this.Label148.Style = "font-size: 7pt; vertical-align: middle";
            this.Label148.Text = "ZIP Code";
            this.Label148.Top = 2.923583F;
            this.Label148.Width = 0.5208333F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Barcode2,
            this.Label82,
            this.Shape20,
            this.Shape21,
            this.Shape22,
            this.Shape13,
            this.Shape15,
            this.Shape14,
            this.txtAmount43,
            this.txtAmount44,
            this.txtAmount45,
            this.txtAmount46,
            this.txtAmount47,
            this.txtAmount48,
            this.txtAmount49,
            this.txtAmount50,
            this.txtAmount51,
            this.txtAmount52,
            this.txtAmount53,
            this.txtAmount54,
            this.txtPaymentSub3,
            this.txtAmount1,
            this.txtAmount2,
            this.txtAmount3,
            this.txtAmount4,
            this.txtAmount5,
            this.txtAmount6,
            this.txtAmount7,
            this.txtAmount8,
            this.txtAmount9,
            this.txtAmount10,
            this.txtAmount11,
            this.txtAmount12,
            this.txtPaymentSub1,
            this.Label24,
            this.Label25,
            this.Label1,
            this.Label3,
            this.txtDatePaid1,
            this.txtDatePaid2,
            this.txtDatePaid3,
            this.txtDatePaid4,
            this.txtDatePaid5,
            this.txtDatePaid6,
            this.txtDatePaid7,
            this.txtDatePaid8,
            this.txtDatePaid9,
            this.txtDatePaid10,
            this.txtDatePaid11,
            this.txtDatePaid12,
            this.Label85,
            this.Label88,
            this.Label90,
            this.txtDatePaid22,
            this.txtDatePaid23,
            this.txtDatePaid24,
            this.txtDatePaid25,
            this.txtDatePaid26,
            this.txtDatePaid27,
            this.txtDatePaid28,
            this.txtDatePaid29,
            this.txtDatePaid30,
            this.txtDatePaid31,
            this.txtDatePaid32,
            this.txtDatePaid33,
            this.txtAmount22,
            this.txtAmount23,
            this.txtAmount24,
            this.txtAmount25,
            this.txtAmount26,
            this.txtAmount27,
            this.txtAmount28,
            this.txtAmount29,
            this.txtAmount30,
            this.txtAmount31,
            this.txtAmount32,
            this.txtAmount33,
            this.txtPaymentSub2,
            this.Label91,
            this.Label92,
            this.Label94,
            this.txtDatePaid43,
            this.txtDatePaid44,
            this.txtDatePaid45,
            this.txtDatePaid46,
            this.txtDatePaid47,
            this.txtDatePaid48,
            this.txtDatePaid49,
            this.txtDatePaid50,
            this.txtDatePaid51,
            this.txtDatePaid52,
            this.txtDatePaid53,
            this.txtDatePaid54,
            this.Label95,
            this.txtLine6,
            this.Label101,
            this.txtName2,
            this.txtWAccount2,
            this.Label102,
            this.Label103,
            this.Label104,
            this.Field36,
            this.Label4,
            this.Line100,
            this.Line101,
            this.Line102,
            this.Line103,
            this.Line104,
            this.Line106,
            this.Line108,
            this.Line109,
            this.Line111,
            this.Line112,
            this.Line113,
            this.Line114,
            this.Line122,
            this.Line123,
            this.Line124,
            this.Line126,
            this.Line127,
            this.Line128,
            this.Line130,
            this.Line131,
            this.Line133,
            this.Line134,
            this.Line136,
            this.Line137,
            this.Line143,
            this.Line145,
            this.Line146,
            this.Line147,
            this.Line148,
            this.Line150,
            this.Line151,
            this.Line152,
            this.Line154,
            this.Line155,
            this.Line157,
            this.Line158,
            this.Line160,
            this.Line166,
            this.Label114,
            this.txtMonthStart,
            this.txtDayStart,
            this.txtYearStart,
            this.txtMonthEnd,
            this.txtDayEnd,
            this.txtYearEnd,
            this.txtAmount13,
            this.txtDatePaid13,
            this.txtAmount14,
            this.txtDatePaid14,
            this.txtAmount15,
            this.txtDatePaid15,
            this.txtAmount16,
            this.txtDatePaid16,
            this.txtAmount17,
            this.txtDatePaid17,
            this.txtAmount18,
            this.txtDatePaid18,
            this.txtAmount19,
            this.txtDatePaid19,
            this.txtAmount20,
            this.txtDatePaid20,
            this.txtAmount21,
            this.txtDatePaid21,
            this.Line167,
            this.Line168,
            this.Line169,
            this.Line170,
            this.Line171,
            this.Line172,
            this.Line173,
            this.Line174,
            this.Line175,
            this.Line99,
            this.txtDatePaid34,
            this.txtAmount34,
            this.txtDatePaid35,
            this.txtAmount35,
            this.txtDatePaid36,
            this.txtAmount36,
            this.txtDatePaid37,
            this.txtAmount37,
            this.txtDatePaid38,
            this.txtAmount38,
            this.txtDatePaid39,
            this.txtAmount39,
            this.txtDatePaid40,
            this.txtAmount40,
            this.txtDatePaid41,
            this.txtAmount41,
            this.txtDatePaid42,
            this.txtAmount42,
            this.txtAmount55,
            this.txtDatePaid55,
            this.txtAmount56,
            this.txtDatePaid56,
            this.txtAmount57,
            this.txtDatePaid57,
            this.txtAmount58,
            this.txtDatePaid58,
            this.txtAmount59,
            this.txtDatePaid59,
            this.txtAmount60,
            this.txtDatePaid60,
            this.txtAmount61,
            this.txtDatePaid61,
            this.txtAmount62,
            this.txtDatePaid62,
            this.txtAmount63,
            this.txtDatePaid63,
            this.Line176,
            this.Line177,
            this.Line178,
            this.Line179,
            this.Line180,
            this.Line181,
            this.Line182,
            this.Line183,
            this.Line184,
            this.Line185,
            this.Line186,
            this.Line187,
            this.Line188,
            this.Line189,
            this.Line190,
            this.Line191,
            this.Line192,
            this.Line193,
            this.txtPeriodStart2,
            this.txtPeriodEnd2});
            this.ReportFooter.Height = 10.02083F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // Barcode2
            // 
            this.Barcode2.CaptionPosition = GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below;
            this.Barcode2.Font = new System.Drawing.Font("Courier New", 8F);
            this.Barcode2.Height = 0.6875F;
            this.Barcode2.Left = 5.636F;
            this.Barcode2.Name = "Barcode2";
            this.Barcode2.QuietZoneBottom = 0F;
            this.Barcode2.QuietZoneLeft = 0F;
            this.Barcode2.QuietZoneRight = 0F;
            this.Barcode2.QuietZoneTop = 0F;
            this.Barcode2.Text = "2106204";
            this.Barcode2.Top = 0F;
            this.Barcode2.Width = 1.583333F;
            // 
            // Label82
            // 
            this.Label82.Height = 0.1666667F;
            this.Label82.HyperLink = null;
            this.Label82.Left = 7.25F;
            this.Label82.Name = "Label82";
            this.Label82.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 12pt; font-weight: bold;" +
    " text-align: right; vertical-align: top";
            this.Label82.Text = "15";
            this.Label82.Top = 0.1666667F;
            this.Label82.Width = 0.25F;
            // 
            // Shape20
            // 
            this.Shape20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape20.Height = 0.1388889F;
            this.Shape20.Left = 0.01388889F;
            this.Shape20.Name = "Shape20";
            this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape20.Top = 0.01388889F;
            this.Shape20.Width = 0.1388889F;
            // 
            // Shape21
            // 
            this.Shape21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape21.Height = 0.1388889F;
            this.Shape21.Left = 0.01388889F;
            this.Shape21.Name = "Shape21";
            this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape21.Top = 9.840278F;
            this.Shape21.Width = 0.1388889F;
            // 
            // Shape22
            // 
            this.Shape22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape22.Height = 0.1388889F;
            this.Shape22.Left = 7.347222F;
            this.Shape22.Name = "Shape22";
            this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape22.Top = 9.840278F;
            this.Shape22.Width = 0.1388888F;
            // 
            // Shape13
            // 
            this.Shape13.Height = 5.5F;
            this.Shape13.Left = 0.014F;
            this.Shape13.Name = "Shape13";
            this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape13.Top = 2.535F;
            this.Shape13.Visible = false;
            this.Shape13.Width = 2.291667F;
            // 
            // Shape15
            // 
            this.Shape15.Height = 5.5F;
            this.Shape15.Left = 5.191083F;
            this.Shape15.Name = "Shape15";
            this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape15.Top = 2.535F;
            this.Shape15.Visible = false;
            this.Shape15.Width = 2.298611F;
            // 
            // Shape14
            // 
            this.Shape14.Height = 5.5F;
            this.Shape14.Left = 2.593861F;
            this.Shape14.Name = "Shape14";
            this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape14.Top = 2.535F;
            this.Shape14.Visible = false;
            this.Shape14.Width = 2.298611F;
            // 
            // txtAmount43
            // 
            this.txtAmount43.CanGrow = false;
            this.txtAmount43.Height = 0.1666667F;
            this.txtAmount43.Left = 6.549195F;
            this.txtAmount43.Name = "txtAmount43";
            this.txtAmount43.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount43.Text = "999999.99";
            this.txtAmount43.Top = 2.525833F;
            this.txtAmount43.Width = 0.8333333F;
            // 
            // txtAmount44
            // 
            this.txtAmount44.CanGrow = false;
            this.txtAmount44.Height = 0.1666667F;
            this.txtAmount44.Left = 6.552667F;
            this.txtAmount44.Name = "txtAmount44";
            this.txtAmount44.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount44.Text = "999999.99";
            this.txtAmount44.Top = 2.775833F;
            this.txtAmount44.Width = 0.8333333F;
            // 
            // txtAmount45
            // 
            this.txtAmount45.CanGrow = false;
            this.txtAmount45.Height = 0.1666667F;
            this.txtAmount45.Left = 6.552667F;
            this.txtAmount45.Name = "txtAmount45";
            this.txtAmount45.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount45.Text = "999999.99";
            this.txtAmount45.Top = 3.025833F;
            this.txtAmount45.Width = 0.8333333F;
            // 
            // txtAmount46
            // 
            this.txtAmount46.CanGrow = false;
            this.txtAmount46.Height = 0.1666667F;
            this.txtAmount46.Left = 6.552667F;
            this.txtAmount46.Name = "txtAmount46";
            this.txtAmount46.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount46.Text = "999999.99";
            this.txtAmount46.Top = 3.275833F;
            this.txtAmount46.Width = 0.8333333F;
            // 
            // txtAmount47
            // 
            this.txtAmount47.CanGrow = false;
            this.txtAmount47.Height = 0.1666667F;
            this.txtAmount47.Left = 6.552667F;
            this.txtAmount47.Name = "txtAmount47";
            this.txtAmount47.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount47.Text = "999999.99";
            this.txtAmount47.Top = 3.525833F;
            this.txtAmount47.Width = 0.8333333F;
            // 
            // txtAmount48
            // 
            this.txtAmount48.CanGrow = false;
            this.txtAmount48.Height = 0.1666667F;
            this.txtAmount48.Left = 6.552667F;
            this.txtAmount48.Name = "txtAmount48";
            this.txtAmount48.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount48.Text = "999999.99";
            this.txtAmount48.Top = 3.775833F;
            this.txtAmount48.Width = 0.8333333F;
            // 
            // txtAmount49
            // 
            this.txtAmount49.CanGrow = false;
            this.txtAmount49.Height = 0.1666667F;
            this.txtAmount49.Left = 6.552667F;
            this.txtAmount49.Name = "txtAmount49";
            this.txtAmount49.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount49.Text = "999999.99";
            this.txtAmount49.Top = 4.025832F;
            this.txtAmount49.Width = 0.8333333F;
            // 
            // txtAmount50
            // 
            this.txtAmount50.CanGrow = false;
            this.txtAmount50.Height = 0.1666667F;
            this.txtAmount50.Left = 6.552667F;
            this.txtAmount50.Name = "txtAmount50";
            this.txtAmount50.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount50.Text = "999999.99";
            this.txtAmount50.Top = 4.275832F;
            this.txtAmount50.Width = 0.8333333F;
            // 
            // txtAmount51
            // 
            this.txtAmount51.CanGrow = false;
            this.txtAmount51.Height = 0.1666667F;
            this.txtAmount51.Left = 6.552667F;
            this.txtAmount51.Name = "txtAmount51";
            this.txtAmount51.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount51.Text = "999999.99";
            this.txtAmount51.Top = 4.525832F;
            this.txtAmount51.Width = 0.8333333F;
            // 
            // txtAmount52
            // 
            this.txtAmount52.CanGrow = false;
            this.txtAmount52.Height = 0.1666667F;
            this.txtAmount52.Left = 6.552667F;
            this.txtAmount52.Name = "txtAmount52";
            this.txtAmount52.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount52.Text = "999999.99";
            this.txtAmount52.Top = 4.775832F;
            this.txtAmount52.Width = 0.8333333F;
            // 
            // txtAmount53
            // 
            this.txtAmount53.CanGrow = false;
            this.txtAmount53.Height = 0.1666667F;
            this.txtAmount53.Left = 6.552667F;
            this.txtAmount53.Name = "txtAmount53";
            this.txtAmount53.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount53.Text = "999999.99";
            this.txtAmount53.Top = 5.025832F;
            this.txtAmount53.Width = 0.8333333F;
            // 
            // txtAmount54
            // 
            this.txtAmount54.CanGrow = false;
            this.txtAmount54.Height = 0.1666667F;
            this.txtAmount54.Left = 6.552667F;
            this.txtAmount54.Name = "txtAmount54";
            this.txtAmount54.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount54.Text = "999999.99";
            this.txtAmount54.Top = 5.275832F;
            this.txtAmount54.Width = 0.8333333F;
            // 
            // txtPaymentSub3
            // 
            this.txtPaymentSub3.CanGrow = false;
            this.txtPaymentSub3.Height = 0.1666667F;
            this.txtPaymentSub3.Left = 6.552667F;
            this.txtPaymentSub3.Name = "txtPaymentSub3";
            this.txtPaymentSub3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtPaymentSub3.Text = "999999.99";
            this.txtPaymentSub3.Top = 7.775832F;
            this.txtPaymentSub3.Width = 0.8333333F;
            // 
            // txtAmount1
            // 
            this.txtAmount1.CanGrow = false;
            this.txtAmount1.Height = 0.1666667F;
            this.txtAmount1.Left = 1.368639F;
            this.txtAmount1.Name = "txtAmount1";
            this.txtAmount1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount1.Text = "999999.99";
            this.txtAmount1.Top = 2.525833F;
            this.txtAmount1.Width = 0.8333333F;
            // 
            // txtAmount2
            // 
            this.txtAmount2.CanGrow = false;
            this.txtAmount2.Height = 0.1666667F;
            this.txtAmount2.Left = 1.365166F;
            this.txtAmount2.Name = "txtAmount2";
            this.txtAmount2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount2.Text = "999999.99";
            this.txtAmount2.Top = 2.775833F;
            this.txtAmount2.Width = 0.8333333F;
            // 
            // txtAmount3
            // 
            this.txtAmount3.CanGrow = false;
            this.txtAmount3.Height = 0.1666667F;
            this.txtAmount3.Left = 1.365166F;
            this.txtAmount3.Name = "txtAmount3";
            this.txtAmount3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount3.Text = "999999.99";
            this.txtAmount3.Top = 3.025833F;
            this.txtAmount3.Width = 0.8333333F;
            // 
            // txtAmount4
            // 
            this.txtAmount4.CanGrow = false;
            this.txtAmount4.Height = 0.1666667F;
            this.txtAmount4.Left = 1.365166F;
            this.txtAmount4.Name = "txtAmount4";
            this.txtAmount4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount4.Text = "999999.99";
            this.txtAmount4.Top = 3.275833F;
            this.txtAmount4.Width = 0.8333333F;
            // 
            // txtAmount5
            // 
            this.txtAmount5.CanGrow = false;
            this.txtAmount5.Height = 0.1666667F;
            this.txtAmount5.Left = 1.365166F;
            this.txtAmount5.Name = "txtAmount5";
            this.txtAmount5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount5.Text = "999999.99";
            this.txtAmount5.Top = 3.525833F;
            this.txtAmount5.Width = 0.8333333F;
            // 
            // txtAmount6
            // 
            this.txtAmount6.CanGrow = false;
            this.txtAmount6.Height = 0.1666667F;
            this.txtAmount6.Left = 1.365166F;
            this.txtAmount6.Name = "txtAmount6";
            this.txtAmount6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount6.Text = "999999.99";
            this.txtAmount6.Top = 3.775833F;
            this.txtAmount6.Width = 0.8333333F;
            // 
            // txtAmount7
            // 
            this.txtAmount7.CanGrow = false;
            this.txtAmount7.Height = 0.1666667F;
            this.txtAmount7.Left = 1.365166F;
            this.txtAmount7.Name = "txtAmount7";
            this.txtAmount7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount7.Text = "999999.99";
            this.txtAmount7.Top = 4.025832F;
            this.txtAmount7.Width = 0.8333333F;
            // 
            // txtAmount8
            // 
            this.txtAmount8.CanGrow = false;
            this.txtAmount8.Height = 0.1666667F;
            this.txtAmount8.Left = 1.365166F;
            this.txtAmount8.Name = "txtAmount8";
            this.txtAmount8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount8.Text = "999999.99";
            this.txtAmount8.Top = 4.275832F;
            this.txtAmount8.Width = 0.8333333F;
            // 
            // txtAmount9
            // 
            this.txtAmount9.CanGrow = false;
            this.txtAmount9.Height = 0.1666667F;
            this.txtAmount9.Left = 1.365166F;
            this.txtAmount9.Name = "txtAmount9";
            this.txtAmount9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount9.Text = "999999.99";
            this.txtAmount9.Top = 4.525832F;
            this.txtAmount9.Width = 0.8333333F;
            // 
            // txtAmount10
            // 
            this.txtAmount10.CanGrow = false;
            this.txtAmount10.Height = 0.1666667F;
            this.txtAmount10.Left = 1.365166F;
            this.txtAmount10.Name = "txtAmount10";
            this.txtAmount10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount10.Text = "999999.99";
            this.txtAmount10.Top = 4.775832F;
            this.txtAmount10.Width = 0.8333333F;
            // 
            // txtAmount11
            // 
            this.txtAmount11.CanGrow = false;
            this.txtAmount11.Height = 0.1666667F;
            this.txtAmount11.Left = 1.365166F;
            this.txtAmount11.Name = "txtAmount11";
            this.txtAmount11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount11.Text = "999999.99";
            this.txtAmount11.Top = 5.025832F;
            this.txtAmount11.Width = 0.8333333F;
            // 
            // txtAmount12
            // 
            this.txtAmount12.CanGrow = false;
            this.txtAmount12.Height = 0.1666667F;
            this.txtAmount12.Left = 1.365166F;
            this.txtAmount12.Name = "txtAmount12";
            this.txtAmount12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount12.Text = "999999.99";
            this.txtAmount12.Top = 5.275832F;
            this.txtAmount12.Width = 0.8333333F;
            // 
            // txtPaymentSub1
            // 
            this.txtPaymentSub1.CanGrow = false;
            this.txtPaymentSub1.Height = 0.1666667F;
            this.txtPaymentSub1.Left = 1.365166F;
            this.txtPaymentSub1.Name = "txtPaymentSub1";
            this.txtPaymentSub1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtPaymentSub1.Text = "999999.99";
            this.txtPaymentSub1.Top = 7.775832F;
            this.txtPaymentSub1.Width = 0.8333333F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.1666667F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 0.2916667F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-weight: bold; text-align: center; text-decoration: underline";
            this.Label24.Text = "Reconciliation of Semiweekly Payments of Income Tax Withholding";
            this.Label24.Top = 1.625111F;
            this.Label24.Width = 6.916667F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.1666667F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 1.479F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-size: 8.5pt; text-align: center";
            this.Label25.Text = "For employers or non-payroll filers required to remit withholding taxes on a semi" +
    "weekly basis.";
            this.Label25.Top = 1.834F;
            this.Label25.Width = 5.041667F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.25F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.07349996F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: center";
            this.Label1.Text = "Date Wages or Non-wages Paid";
            this.Label1.Top = 2.213333F;
            this.Label1.Width = 0.8125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.25F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 1.365583F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: center";
            this.Label3.Text = "Amount of Withholding Paid";
            this.Label3.Top = 2.213333F;
            this.Label3.Width = 0.8640001F;
            // 
            // txtDatePaid1
            // 
            this.txtDatePaid1.CanGrow = false;
            this.txtDatePaid1.Height = 0.1666667F;
            this.txtDatePaid1.Left = 0.136F;
            this.txtDatePaid1.Name = "txtDatePaid1";
            this.txtDatePaid1.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid1.Text = "99 99 99";
            this.txtDatePaid1.Top = 2.53F;
            this.txtDatePaid1.Width = 0.75F;
            // 
            // txtDatePaid2
            // 
            this.txtDatePaid2.CanGrow = false;
            this.txtDatePaid2.Height = 0.1666667F;
            this.txtDatePaid2.Left = 0.136F;
            this.txtDatePaid2.Name = "txtDatePaid2";
            this.txtDatePaid2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid2.Text = "99 99 99";
            this.txtDatePaid2.Top = 2.78F;
            this.txtDatePaid2.Width = 0.75F;
            // 
            // txtDatePaid3
            // 
            this.txtDatePaid3.CanGrow = false;
            this.txtDatePaid3.Height = 0.1666667F;
            this.txtDatePaid3.Left = 0.136F;
            this.txtDatePaid3.Name = "txtDatePaid3";
            this.txtDatePaid3.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid3.Text = "99 99 99";
            this.txtDatePaid3.Top = 3.03F;
            this.txtDatePaid3.Width = 0.75F;
            // 
            // txtDatePaid4
            // 
            this.txtDatePaid4.CanGrow = false;
            this.txtDatePaid4.Height = 0.1666667F;
            this.txtDatePaid4.Left = 0.136F;
            this.txtDatePaid4.Name = "txtDatePaid4";
            this.txtDatePaid4.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid4.Text = "99 99 99";
            this.txtDatePaid4.Top = 3.28F;
            this.txtDatePaid4.Width = 0.75F;
            // 
            // txtDatePaid5
            // 
            this.txtDatePaid5.CanGrow = false;
            this.txtDatePaid5.Height = 0.1666667F;
            this.txtDatePaid5.Left = 0.136F;
            this.txtDatePaid5.Name = "txtDatePaid5";
            this.txtDatePaid5.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid5.Text = "99 99 99";
            this.txtDatePaid5.Top = 3.53F;
            this.txtDatePaid5.Width = 0.75F;
            // 
            // txtDatePaid6
            // 
            this.txtDatePaid6.CanGrow = false;
            this.txtDatePaid6.Height = 0.1666667F;
            this.txtDatePaid6.Left = 0.136F;
            this.txtDatePaid6.Name = "txtDatePaid6";
            this.txtDatePaid6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid6.Text = "99 99 99";
            this.txtDatePaid6.Top = 3.78F;
            this.txtDatePaid6.Width = 0.75F;
            // 
            // txtDatePaid7
            // 
            this.txtDatePaid7.CanGrow = false;
            this.txtDatePaid7.Height = 0.1666667F;
            this.txtDatePaid7.Left = 0.136F;
            this.txtDatePaid7.Name = "txtDatePaid7";
            this.txtDatePaid7.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid7.Text = "99 99 99";
            this.txtDatePaid7.Top = 4.03F;
            this.txtDatePaid7.Width = 0.75F;
            // 
            // txtDatePaid8
            // 
            this.txtDatePaid8.CanGrow = false;
            this.txtDatePaid8.Height = 0.1666667F;
            this.txtDatePaid8.Left = 0.136F;
            this.txtDatePaid8.Name = "txtDatePaid8";
            this.txtDatePaid8.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid8.Text = "99 99 99";
            this.txtDatePaid8.Top = 4.28F;
            this.txtDatePaid8.Width = 0.75F;
            // 
            // txtDatePaid9
            // 
            this.txtDatePaid9.CanGrow = false;
            this.txtDatePaid9.Height = 0.1666667F;
            this.txtDatePaid9.Left = 0.136F;
            this.txtDatePaid9.Name = "txtDatePaid9";
            this.txtDatePaid9.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid9.Text = "99 99 99";
            this.txtDatePaid9.Top = 4.53F;
            this.txtDatePaid9.Width = 0.75F;
            // 
            // txtDatePaid10
            // 
            this.txtDatePaid10.CanGrow = false;
            this.txtDatePaid10.Height = 0.1666667F;
            this.txtDatePaid10.Left = 0.136F;
            this.txtDatePaid10.Name = "txtDatePaid10";
            this.txtDatePaid10.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid10.Text = "99 99 99";
            this.txtDatePaid10.Top = 4.78F;
            this.txtDatePaid10.Width = 0.75F;
            // 
            // txtDatePaid11
            // 
            this.txtDatePaid11.CanGrow = false;
            this.txtDatePaid11.Height = 0.1666667F;
            this.txtDatePaid11.Left = 0.136F;
            this.txtDatePaid11.Name = "txtDatePaid11";
            this.txtDatePaid11.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid11.Text = "99 99 99";
            this.txtDatePaid11.Top = 5.03F;
            this.txtDatePaid11.Width = 0.75F;
            // 
            // txtDatePaid12
            // 
            this.txtDatePaid12.CanGrow = false;
            this.txtDatePaid12.Height = 0.1666667F;
            this.txtDatePaid12.Left = 0.136F;
            this.txtDatePaid12.Name = "txtDatePaid12";
            this.txtDatePaid12.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid12.Text = "99 99 99";
            this.txtDatePaid12.Top = 5.28F;
            this.txtDatePaid12.Width = 0.75F;
            // 
            // Label85
            // 
            this.Label85.Height = 0.2395833F;
            this.Label85.HyperLink = null;
            this.Label85.Left = 0.1876111F;
            this.Label85.Name = "Label85";
            this.Label85.Style = "background-color: rgb(255,255,255); font-size: 8.5pt; font-weight: bold; text-ali" +
    "gn: center; vertical-align: middle";
            this.Label85.Text = "Subtotal A";
            this.Label85.Top = 7.764166F;
            this.Label85.Width = 0.75F;
            // 
            // Label88
            // 
            this.Label88.Height = 0.25F;
            this.Label88.HyperLink = null;
            this.Label88.Left = 2.646416F;
            this.Label88.Name = "Label88";
            this.Label88.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: center";
            this.Label88.Text = "Date Wages or Non-wages Paid";
            this.Label88.Top = 2.213333F;
            this.Label88.Width = 0.875F;
            // 
            // Label90
            // 
            this.Label90.Height = 0.25F;
            this.Label90.HyperLink = null;
            this.Label90.Left = 4.021417F;
            this.Label90.Name = "Label90";
            this.Label90.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: center";
            this.Label90.Text = "Amount of Withholding Paid";
            this.Label90.Top = 2.213333F;
            this.Label90.Width = 0.7671663F;
            // 
            // txtDatePaid22
            // 
            this.txtDatePaid22.CanGrow = false;
            this.txtDatePaid22.Height = 0.1666667F;
            this.txtDatePaid22.Left = 2.708916F;
            this.txtDatePaid22.Name = "txtDatePaid22";
            this.txtDatePaid22.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid22.Text = "99 99 99";
            this.txtDatePaid22.Top = 2.525833F;
            this.txtDatePaid22.Width = 0.75F;
            // 
            // txtDatePaid23
            // 
            this.txtDatePaid23.CanGrow = false;
            this.txtDatePaid23.Height = 0.1666667F;
            this.txtDatePaid23.Left = 2.708916F;
            this.txtDatePaid23.Name = "txtDatePaid23";
            this.txtDatePaid23.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid23.Text = "99 99 99";
            this.txtDatePaid23.Top = 2.775833F;
            this.txtDatePaid23.Width = 0.75F;
            // 
            // txtDatePaid24
            // 
            this.txtDatePaid24.CanGrow = false;
            this.txtDatePaid24.Height = 0.1666667F;
            this.txtDatePaid24.Left = 2.708916F;
            this.txtDatePaid24.Name = "txtDatePaid24";
            this.txtDatePaid24.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid24.Text = "99 99 99";
            this.txtDatePaid24.Top = 3.025833F;
            this.txtDatePaid24.Width = 0.75F;
            // 
            // txtDatePaid25
            // 
            this.txtDatePaid25.CanGrow = false;
            this.txtDatePaid25.Height = 0.1666667F;
            this.txtDatePaid25.Left = 2.708916F;
            this.txtDatePaid25.Name = "txtDatePaid25";
            this.txtDatePaid25.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid25.Text = "99 99 99";
            this.txtDatePaid25.Top = 3.275833F;
            this.txtDatePaid25.Width = 0.75F;
            // 
            // txtDatePaid26
            // 
            this.txtDatePaid26.CanGrow = false;
            this.txtDatePaid26.Height = 0.1666667F;
            this.txtDatePaid26.Left = 2.708916F;
            this.txtDatePaid26.Name = "txtDatePaid26";
            this.txtDatePaid26.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid26.Text = "99 99 99";
            this.txtDatePaid26.Top = 3.525833F;
            this.txtDatePaid26.Width = 0.75F;
            // 
            // txtDatePaid27
            // 
            this.txtDatePaid27.CanGrow = false;
            this.txtDatePaid27.Height = 0.1666667F;
            this.txtDatePaid27.Left = 2.708916F;
            this.txtDatePaid27.Name = "txtDatePaid27";
            this.txtDatePaid27.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid27.Text = "99 99 99";
            this.txtDatePaid27.Top = 3.775833F;
            this.txtDatePaid27.Width = 0.75F;
            // 
            // txtDatePaid28
            // 
            this.txtDatePaid28.CanGrow = false;
            this.txtDatePaid28.Height = 0.1666667F;
            this.txtDatePaid28.Left = 2.708916F;
            this.txtDatePaid28.Name = "txtDatePaid28";
            this.txtDatePaid28.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid28.Text = "99 99 99";
            this.txtDatePaid28.Top = 4.025832F;
            this.txtDatePaid28.Width = 0.75F;
            // 
            // txtDatePaid29
            // 
            this.txtDatePaid29.CanGrow = false;
            this.txtDatePaid29.Height = 0.1666667F;
            this.txtDatePaid29.Left = 2.708916F;
            this.txtDatePaid29.Name = "txtDatePaid29";
            this.txtDatePaid29.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid29.Text = "99 99 99";
            this.txtDatePaid29.Top = 4.275832F;
            this.txtDatePaid29.Width = 0.75F;
            // 
            // txtDatePaid30
            // 
            this.txtDatePaid30.CanGrow = false;
            this.txtDatePaid30.Height = 0.1666667F;
            this.txtDatePaid30.Left = 2.708916F;
            this.txtDatePaid30.Name = "txtDatePaid30";
            this.txtDatePaid30.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid30.Text = "99 99 99";
            this.txtDatePaid30.Top = 4.525832F;
            this.txtDatePaid30.Width = 0.75F;
            // 
            // txtDatePaid31
            // 
            this.txtDatePaid31.CanGrow = false;
            this.txtDatePaid31.Height = 0.1666667F;
            this.txtDatePaid31.Left = 2.708916F;
            this.txtDatePaid31.Name = "txtDatePaid31";
            this.txtDatePaid31.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid31.Text = "99 99 99";
            this.txtDatePaid31.Top = 4.775832F;
            this.txtDatePaid31.Width = 0.75F;
            // 
            // txtDatePaid32
            // 
            this.txtDatePaid32.CanGrow = false;
            this.txtDatePaid32.Height = 0.1666667F;
            this.txtDatePaid32.Left = 2.708916F;
            this.txtDatePaid32.Name = "txtDatePaid32";
            this.txtDatePaid32.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid32.Text = "99 99 99";
            this.txtDatePaid32.Top = 5.025832F;
            this.txtDatePaid32.Width = 0.75F;
            // 
            // txtDatePaid33
            // 
            this.txtDatePaid33.CanGrow = false;
            this.txtDatePaid33.Height = 0.1666667F;
            this.txtDatePaid33.Left = 2.708916F;
            this.txtDatePaid33.Name = "txtDatePaid33";
            this.txtDatePaid33.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid33.Text = "99 99 99";
            this.txtDatePaid33.Top = 5.275832F;
            this.txtDatePaid33.Width = 0.75F;
            // 
            // txtAmount22
            // 
            this.txtAmount22.CanGrow = false;
            this.txtAmount22.Height = 0.1666667F;
            this.txtAmount22.Left = 3.955444F;
            this.txtAmount22.Name = "txtAmount22";
            this.txtAmount22.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount22.Text = "999999.99";
            this.txtAmount22.Top = 2.525833F;
            this.txtAmount22.Width = 0.8333333F;
            // 
            // txtAmount23
            // 
            this.txtAmount23.CanGrow = false;
            this.txtAmount23.Height = 0.1666667F;
            this.txtAmount23.Left = 3.955444F;
            this.txtAmount23.Name = "txtAmount23";
            this.txtAmount23.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount23.Text = "999999.99";
            this.txtAmount23.Top = 2.775833F;
            this.txtAmount23.Width = 0.8333333F;
            // 
            // txtAmount24
            // 
            this.txtAmount24.CanGrow = false;
            this.txtAmount24.Height = 0.1666667F;
            this.txtAmount24.Left = 3.955444F;
            this.txtAmount24.Name = "txtAmount24";
            this.txtAmount24.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount24.Text = "999999.99";
            this.txtAmount24.Top = 3.025833F;
            this.txtAmount24.Width = 0.8333333F;
            // 
            // txtAmount25
            // 
            this.txtAmount25.CanGrow = false;
            this.txtAmount25.Height = 0.1666667F;
            this.txtAmount25.Left = 3.955444F;
            this.txtAmount25.Name = "txtAmount25";
            this.txtAmount25.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount25.Text = "999999.99";
            this.txtAmount25.Top = 3.275833F;
            this.txtAmount25.Width = 0.8333333F;
            // 
            // txtAmount26
            // 
            this.txtAmount26.CanGrow = false;
            this.txtAmount26.Height = 0.1666667F;
            this.txtAmount26.Left = 3.955444F;
            this.txtAmount26.Name = "txtAmount26";
            this.txtAmount26.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount26.Text = "999999.99";
            this.txtAmount26.Top = 3.525833F;
            this.txtAmount26.Width = 0.8333333F;
            // 
            // txtAmount27
            // 
            this.txtAmount27.CanGrow = false;
            this.txtAmount27.Height = 0.1666667F;
            this.txtAmount27.Left = 3.955444F;
            this.txtAmount27.Name = "txtAmount27";
            this.txtAmount27.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount27.Text = "999999.99";
            this.txtAmount27.Top = 3.775833F;
            this.txtAmount27.Width = 0.8333333F;
            // 
            // txtAmount28
            // 
            this.txtAmount28.CanGrow = false;
            this.txtAmount28.Height = 0.1666667F;
            this.txtAmount28.Left = 3.955444F;
            this.txtAmount28.Name = "txtAmount28";
            this.txtAmount28.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount28.Text = "999999.99";
            this.txtAmount28.Top = 4.025832F;
            this.txtAmount28.Width = 0.8333333F;
            // 
            // txtAmount29
            // 
            this.txtAmount29.CanGrow = false;
            this.txtAmount29.Height = 0.1666667F;
            this.txtAmount29.Left = 3.955444F;
            this.txtAmount29.Name = "txtAmount29";
            this.txtAmount29.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount29.Text = "999999.99";
            this.txtAmount29.Top = 4.275832F;
            this.txtAmount29.Width = 0.8333333F;
            // 
            // txtAmount30
            // 
            this.txtAmount30.CanGrow = false;
            this.txtAmount30.Height = 0.1666667F;
            this.txtAmount30.Left = 3.955444F;
            this.txtAmount30.Name = "txtAmount30";
            this.txtAmount30.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount30.Text = "999999.99";
            this.txtAmount30.Top = 4.525832F;
            this.txtAmount30.Width = 0.8333333F;
            // 
            // txtAmount31
            // 
            this.txtAmount31.CanGrow = false;
            this.txtAmount31.Height = 0.1666667F;
            this.txtAmount31.Left = 3.955444F;
            this.txtAmount31.Name = "txtAmount31";
            this.txtAmount31.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount31.Text = "999999.99";
            this.txtAmount31.Top = 4.775832F;
            this.txtAmount31.Width = 0.8333333F;
            // 
            // txtAmount32
            // 
            this.txtAmount32.CanGrow = false;
            this.txtAmount32.Height = 0.1666667F;
            this.txtAmount32.Left = 3.955444F;
            this.txtAmount32.Name = "txtAmount32";
            this.txtAmount32.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount32.Text = "999999.99";
            this.txtAmount32.Top = 5.025832F;
            this.txtAmount32.Width = 0.8333333F;
            // 
            // txtAmount33
            // 
            this.txtAmount33.CanGrow = false;
            this.txtAmount33.Height = 0.1666667F;
            this.txtAmount33.Left = 3.955444F;
            this.txtAmount33.Name = "txtAmount33";
            this.txtAmount33.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount33.Text = "999999.99";
            this.txtAmount33.Top = 5.275832F;
            this.txtAmount33.Width = 0.8333333F;
            // 
            // txtPaymentSub2
            // 
            this.txtPaymentSub2.CanGrow = false;
            this.txtPaymentSub2.Height = 0.1666667F;
            this.txtPaymentSub2.Left = 3.958916F;
            this.txtPaymentSub2.Name = "txtPaymentSub2";
            this.txtPaymentSub2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtPaymentSub2.Text = "999999.99";
            this.txtPaymentSub2.Top = 7.775832F;
            this.txtPaymentSub2.Width = 0.8333333F;
            // 
            // Label91
            // 
            this.Label91.Height = 0.2395833F;
            this.Label91.HyperLink = null;
            this.Label91.Left = 2.770944F;
            this.Label91.Name = "Label91";
            this.Label91.Style = "font-size: 8.5pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label91.Text = "Subtotal B";
            this.Label91.Top = 7.764166F;
            this.Label91.Width = 0.75F;
            // 
            // Label92
            // 
            this.Label92.Height = 0.25F;
            this.Label92.HyperLink = null;
            this.Label92.Left = 5.261F;
            this.Label92.Name = "Label92";
            this.Label92.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: center";
            this.Label92.Text = "Date Wages or Non-wages Paid";
            this.Label92.Top = 2.213333F;
            this.Label92.Width = 0.8125F;
            // 
            // Label94
            // 
            this.Label94.Height = 0.25F;
            this.Label94.HyperLink = null;
            this.Label94.Left = 6.542583F;
            this.Label94.Name = "Label94";
            this.Label94.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: center";
            this.Label94.Text = "Amount of Withholding Paid";
            this.Label94.Top = 2.213333F;
            this.Label94.Width = 0.816F;
            // 
            // txtDatePaid43
            // 
            this.txtDatePaid43.CanGrow = false;
            this.txtDatePaid43.Height = 0.1666667F;
            this.txtDatePaid43.Left = 5.302667F;
            this.txtDatePaid43.Name = "txtDatePaid43";
            this.txtDatePaid43.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid43.Text = "99 99 99";
            this.txtDatePaid43.Top = 2.525833F;
            this.txtDatePaid43.Width = 0.75F;
            // 
            // txtDatePaid44
            // 
            this.txtDatePaid44.CanGrow = false;
            this.txtDatePaid44.Height = 0.1666667F;
            this.txtDatePaid44.Left = 5.302667F;
            this.txtDatePaid44.Name = "txtDatePaid44";
            this.txtDatePaid44.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid44.Text = "99 99 99";
            this.txtDatePaid44.Top = 2.775833F;
            this.txtDatePaid44.Width = 0.75F;
            // 
            // txtDatePaid45
            // 
            this.txtDatePaid45.CanGrow = false;
            this.txtDatePaid45.Height = 0.1666667F;
            this.txtDatePaid45.Left = 5.302667F;
            this.txtDatePaid45.Name = "txtDatePaid45";
            this.txtDatePaid45.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid45.Text = "99 99 99";
            this.txtDatePaid45.Top = 3.025833F;
            this.txtDatePaid45.Width = 0.75F;
            // 
            // txtDatePaid46
            // 
            this.txtDatePaid46.CanGrow = false;
            this.txtDatePaid46.Height = 0.1666667F;
            this.txtDatePaid46.Left = 5.302667F;
            this.txtDatePaid46.Name = "txtDatePaid46";
            this.txtDatePaid46.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid46.Text = "99 99 99";
            this.txtDatePaid46.Top = 3.275833F;
            this.txtDatePaid46.Width = 0.75F;
            // 
            // txtDatePaid47
            // 
            this.txtDatePaid47.CanGrow = false;
            this.txtDatePaid47.Height = 0.1666667F;
            this.txtDatePaid47.Left = 5.302667F;
            this.txtDatePaid47.Name = "txtDatePaid47";
            this.txtDatePaid47.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid47.Text = "99 99 99";
            this.txtDatePaid47.Top = 3.525833F;
            this.txtDatePaid47.Width = 0.75F;
            // 
            // txtDatePaid48
            // 
            this.txtDatePaid48.CanGrow = false;
            this.txtDatePaid48.Height = 0.1666667F;
            this.txtDatePaid48.Left = 5.302667F;
            this.txtDatePaid48.Name = "txtDatePaid48";
            this.txtDatePaid48.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid48.Text = "99 99 99";
            this.txtDatePaid48.Top = 3.775833F;
            this.txtDatePaid48.Width = 0.75F;
            // 
            // txtDatePaid49
            // 
            this.txtDatePaid49.CanGrow = false;
            this.txtDatePaid49.Height = 0.1666667F;
            this.txtDatePaid49.Left = 5.302667F;
            this.txtDatePaid49.Name = "txtDatePaid49";
            this.txtDatePaid49.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid49.Text = "99 99 99";
            this.txtDatePaid49.Top = 4.025832F;
            this.txtDatePaid49.Width = 0.75F;
            // 
            // txtDatePaid50
            // 
            this.txtDatePaid50.CanGrow = false;
            this.txtDatePaid50.Height = 0.1666667F;
            this.txtDatePaid50.Left = 5.302667F;
            this.txtDatePaid50.Name = "txtDatePaid50";
            this.txtDatePaid50.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid50.Text = "99 99 99";
            this.txtDatePaid50.Top = 4.275832F;
            this.txtDatePaid50.Width = 0.75F;
            // 
            // txtDatePaid51
            // 
            this.txtDatePaid51.CanGrow = false;
            this.txtDatePaid51.Height = 0.1666667F;
            this.txtDatePaid51.Left = 5.302667F;
            this.txtDatePaid51.Name = "txtDatePaid51";
            this.txtDatePaid51.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid51.Text = "99 99 99";
            this.txtDatePaid51.Top = 4.525832F;
            this.txtDatePaid51.Width = 0.75F;
            // 
            // txtDatePaid52
            // 
            this.txtDatePaid52.CanGrow = false;
            this.txtDatePaid52.Height = 0.1666667F;
            this.txtDatePaid52.Left = 5.302667F;
            this.txtDatePaid52.Name = "txtDatePaid52";
            this.txtDatePaid52.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid52.Text = "99 99 99";
            this.txtDatePaid52.Top = 4.775832F;
            this.txtDatePaid52.Width = 0.75F;
            // 
            // txtDatePaid53
            // 
            this.txtDatePaid53.CanGrow = false;
            this.txtDatePaid53.Height = 0.1666667F;
            this.txtDatePaid53.Left = 5.302667F;
            this.txtDatePaid53.Name = "txtDatePaid53";
            this.txtDatePaid53.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid53.Text = "99 99 99";
            this.txtDatePaid53.Top = 5.025832F;
            this.txtDatePaid53.Width = 0.75F;
            // 
            // txtDatePaid54
            // 
            this.txtDatePaid54.CanGrow = false;
            this.txtDatePaid54.Height = 0.1666667F;
            this.txtDatePaid54.Left = 5.302667F;
            this.txtDatePaid54.Name = "txtDatePaid54";
            this.txtDatePaid54.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid54.Text = "99 99 99";
            this.txtDatePaid54.Top = 5.275832F;
            this.txtDatePaid54.Width = 0.75F;
            // 
            // Label95
            // 
            this.Label95.Height = 0.2395833F;
            this.Label95.HyperLink = null;
            this.Label95.Left = 5.395945F;
            this.Label95.Name = "Label95";
            this.Label95.Style = "background-color: rgb(255,255,255); font-size: 8.5pt; font-weight: bold; text-ali" +
    "gn: center; vertical-align: middle";
            this.Label95.Text = "Subtotal C";
            this.Label95.Top = 7.764166F;
            this.Label95.Width = 0.75F;
            // 
            // txtLine6
            // 
            this.txtLine6.Height = 0.1666667F;
            this.txtLine6.Left = 5.542F;
            this.txtLine6.Name = "txtLine6";
            this.txtLine6.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtLine6.Text = "99999999999.99";
            this.txtLine6.Top = 8.474F;
            this.txtLine6.Width = 1.6875F;
            // 
            // Label101
            // 
            this.Label101.Height = 0.1666667F;
            this.Label101.HyperLink = null;
            this.Label101.Left = 0.13575F;
            this.Label101.Name = "Label101";
            this.Label101.Style = "font-family: \\000027Tahoma\\000027; font-size: 7pt; text-align: left; vertical-ali" +
    "gn: middle";
            this.Label101.Text = "5. Total payment amount (Enter on Form 941ME, line2)";
            this.Label101.Top = 8.504001F;
            this.Label101.Width = 2.5625F;
            // 
            // txtName2
            // 
            this.txtName2.Height = 0.1666667F;
            this.txtName2.Left = 1.111F;
            this.txtName2.Name = "txtName2";
            this.txtName2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt";
            this.txtName2.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            this.txtName2.Top = 0.365F;
            this.txtName2.Width = 2.8625F;
            // 
            // txtWAccount2
            // 
            this.txtWAccount2.Height = 0.1666667F;
            this.txtWAccount2.Left = 1.115F;
            this.txtWAccount2.Name = "txtWAccount2";
            this.txtWAccount2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left";
            this.txtWAccount2.Text = "99 999999999";
            this.txtWAccount2.Top = 0.6620001F;
            this.txtWAccount2.Width = 1.833333F;
            // 
            // Label102
            // 
            this.Label102.Height = 0.1666667F;
            this.Label102.HyperLink = null;
            this.Label102.Left = 0.125F;
            this.Label102.Name = "Label102";
            this.Label102.Style = "font-size: 7pt";
            this.Label102.Text = "Name:";
            this.Label102.Top = 0.3855278F;
            this.Label102.Width = 0.5F;
            // 
            // Label103
            // 
            this.Label103.Height = 0.25F;
            this.Label103.HyperLink = null;
            this.Label103.Left = 0.125F;
            this.Label103.Name = "Label103";
            this.Label103.Style = "font-size: 7pt";
            this.Label103.Text = "Withholding Account No:";
            this.Label103.Top = 0.629F;
            this.Label103.Width = 0.8333333F;
            // 
            // Label104
            // 
            this.Label104.Height = 0.1666667F;
            this.Label104.HyperLink = null;
            this.Label104.Left = 0.125F;
            this.Label104.Name = "Label104";
            this.Label104.Style = "font-size: 7pt";
            this.Label104.Text = "Period Covered:";
            this.Label104.Top = 1.034833F;
            this.Label104.Width = 0.9166667F;
            // 
            // Field36
            // 
            this.Field36.Height = 0.1666667F;
            this.Field36.Left = 2.229F;
            this.Field36.Name = "Field36";
            this.Field36.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 8.5pt; text-align: cente" +
    "r; vertical-align: top";
            this.Field36.Text = "-";
            this.Field36.Top = 0.9540001F;
            this.Field36.Visible = false;
            this.Field36.Width = 0.25F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2291667F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.25F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \\000027Arial\\000027; font-size: 10pt; font-weight: bold; text-align:" +
    " left";
            this.Label4.Text = "SCHEDULE 1 (Form 941ME) ";
            this.Label4.Top = 0.014F;
            this.Label4.Width = 3.354167F;
            // 
            // Line100
            // 
            this.Line100.Height = 0F;
            this.Line100.Left = 0.014F;
            this.Line100.LineWeight = 1F;
            this.Line100.Name = "Line100";
            this.Line100.Top = 2.768333F;
            this.Line100.Visible = false;
            this.Line100.Width = 2.291667F;
            this.Line100.X1 = 0.014F;
            this.Line100.X2 = 2.305667F;
            this.Line100.Y1 = 2.768333F;
            this.Line100.Y2 = 2.768333F;
            // 
            // Line101
            // 
            this.Line101.Height = 0F;
            this.Line101.Left = 0.014F;
            this.Line101.LineWeight = 1F;
            this.Line101.Name = "Line101";
            this.Line101.Top = 3.018333F;
            this.Line101.Visible = false;
            this.Line101.Width = 2.291667F;
            this.Line101.X1 = 0.014F;
            this.Line101.X2 = 2.305667F;
            this.Line101.Y1 = 3.018333F;
            this.Line101.Y2 = 3.018333F;
            // 
            // Line102
            // 
            this.Line102.Height = 0F;
            this.Line102.Left = 0.014F;
            this.Line102.LineWeight = 1F;
            this.Line102.Name = "Line102";
            this.Line102.Top = 3.268333F;
            this.Line102.Visible = false;
            this.Line102.Width = 2.291667F;
            this.Line102.X1 = 0.014F;
            this.Line102.X2 = 2.305667F;
            this.Line102.Y1 = 3.268333F;
            this.Line102.Y2 = 3.268333F;
            // 
            // Line103
            // 
            this.Line103.Height = 0F;
            this.Line103.Left = 0.014F;
            this.Line103.LineWeight = 1F;
            this.Line103.Name = "Line103";
            this.Line103.Top = 3.518333F;
            this.Line103.Visible = false;
            this.Line103.Width = 2.291667F;
            this.Line103.X1 = 0.014F;
            this.Line103.X2 = 2.305667F;
            this.Line103.Y1 = 3.518333F;
            this.Line103.Y2 = 3.518333F;
            // 
            // Line104
            // 
            this.Line104.Height = 0F;
            this.Line104.Left = 0.014F;
            this.Line104.LineWeight = 1F;
            this.Line104.Name = "Line104";
            this.Line104.Top = 3.768333F;
            this.Line104.Visible = false;
            this.Line104.Width = 2.291667F;
            this.Line104.X1 = 0.014F;
            this.Line104.X2 = 2.305667F;
            this.Line104.Y1 = 3.768333F;
            this.Line104.Y2 = 3.768333F;
            // 
            // Line106
            // 
            this.Line106.Height = 0F;
            this.Line106.Left = 0.014F;
            this.Line106.LineWeight = 1F;
            this.Line106.Name = "Line106";
            this.Line106.Top = 4.018333F;
            this.Line106.Visible = false;
            this.Line106.Width = 2.291667F;
            this.Line106.X1 = 0.014F;
            this.Line106.X2 = 2.305667F;
            this.Line106.Y1 = 4.018333F;
            this.Line106.Y2 = 4.018333F;
            // 
            // Line108
            // 
            this.Line108.Height = 0F;
            this.Line108.Left = 0.014F;
            this.Line108.LineWeight = 1F;
            this.Line108.Name = "Line108";
            this.Line108.Top = 4.268333F;
            this.Line108.Visible = false;
            this.Line108.Width = 2.291667F;
            this.Line108.X1 = 0.014F;
            this.Line108.X2 = 2.305667F;
            this.Line108.Y1 = 4.268333F;
            this.Line108.Y2 = 4.268333F;
            // 
            // Line109
            // 
            this.Line109.Height = 0F;
            this.Line109.Left = 0.014F;
            this.Line109.LineWeight = 1F;
            this.Line109.Name = "Line109";
            this.Line109.Top = 4.518333F;
            this.Line109.Visible = false;
            this.Line109.Width = 2.291667F;
            this.Line109.X1 = 0.014F;
            this.Line109.X2 = 2.305667F;
            this.Line109.Y1 = 4.518333F;
            this.Line109.Y2 = 4.518333F;
            // 
            // Line111
            // 
            this.Line111.Height = 0F;
            this.Line111.Left = 0.014F;
            this.Line111.LineWeight = 1F;
            this.Line111.Name = "Line111";
            this.Line111.Top = 4.768333F;
            this.Line111.Visible = false;
            this.Line111.Width = 2.291667F;
            this.Line111.X1 = 0.014F;
            this.Line111.X2 = 2.305667F;
            this.Line111.Y1 = 4.768333F;
            this.Line111.Y2 = 4.768333F;
            // 
            // Line112
            // 
            this.Line112.Height = 0F;
            this.Line112.Left = 0.014F;
            this.Line112.LineWeight = 1F;
            this.Line112.Name = "Line112";
            this.Line112.Top = 5.018333F;
            this.Line112.Visible = false;
            this.Line112.Width = 2.291667F;
            this.Line112.X1 = 0.014F;
            this.Line112.X2 = 2.305667F;
            this.Line112.Y1 = 5.018333F;
            this.Line112.Y2 = 5.018333F;
            // 
            // Line113
            // 
            this.Line113.Height = 0F;
            this.Line113.Left = 0.014F;
            this.Line113.LineWeight = 1F;
            this.Line113.Name = "Line113";
            this.Line113.Top = 5.268333F;
            this.Line113.Visible = false;
            this.Line113.Width = 2.291667F;
            this.Line113.X1 = 0.014F;
            this.Line113.X2 = 2.305667F;
            this.Line113.Y1 = 5.268333F;
            this.Line113.Y2 = 5.268333F;
            // 
            // Line114
            // 
            this.Line114.Height = 0F;
            this.Line114.Left = 0.014F;
            this.Line114.LineWeight = 1F;
            this.Line114.Name = "Line114";
            this.Line114.Top = 5.518333F;
            this.Line114.Visible = false;
            this.Line114.Width = 2.291667F;
            this.Line114.X1 = 0.014F;
            this.Line114.X2 = 2.305667F;
            this.Line114.Y1 = 5.518333F;
            this.Line114.Y2 = 5.518333F;
            // 
            // Line122
            // 
            this.Line122.Height = 5.5F;
            this.Line122.Left = 3.73275F;
            this.Line122.LineWeight = 1F;
            this.Line122.Name = "Line122";
            this.Line122.Top = 2.535F;
            this.Line122.Visible = false;
            this.Line122.Width = 0F;
            this.Line122.X1 = 3.73275F;
            this.Line122.X2 = 3.73275F;
            this.Line122.Y1 = 2.535F;
            this.Line122.Y2 = 8.035F;
            // 
            // Line123
            // 
            this.Line123.Height = 0F;
            this.Line123.Left = 2.593861F;
            this.Line123.LineWeight = 1F;
            this.Line123.Name = "Line123";
            this.Line123.Top = 2.764166F;
            this.Line123.Visible = false;
            this.Line123.Width = 2.302084F;
            this.Line123.X1 = 2.593861F;
            this.Line123.X2 = 4.895945F;
            this.Line123.Y1 = 2.764166F;
            this.Line123.Y2 = 2.764166F;
            // 
            // Line124
            // 
            this.Line124.Height = 0F;
            this.Line124.Left = 2.593861F;
            this.Line124.LineWeight = 1F;
            this.Line124.Name = "Line124";
            this.Line124.Top = 3.014166F;
            this.Line124.Visible = false;
            this.Line124.Width = 2.302084F;
            this.Line124.X1 = 2.593861F;
            this.Line124.X2 = 4.895945F;
            this.Line124.Y1 = 3.014166F;
            this.Line124.Y2 = 3.014166F;
            // 
            // Line126
            // 
            this.Line126.Height = 0F;
            this.Line126.Left = 2.593861F;
            this.Line126.LineWeight = 1F;
            this.Line126.Name = "Line126";
            this.Line126.Top = 3.264166F;
            this.Line126.Visible = false;
            this.Line126.Width = 2.302084F;
            this.Line126.X1 = 2.593861F;
            this.Line126.X2 = 4.895945F;
            this.Line126.Y1 = 3.264166F;
            this.Line126.Y2 = 3.264166F;
            // 
            // Line127
            // 
            this.Line127.Height = 0F;
            this.Line127.Left = 2.593861F;
            this.Line127.LineWeight = 1F;
            this.Line127.Name = "Line127";
            this.Line127.Top = 3.514166F;
            this.Line127.Visible = false;
            this.Line127.Width = 2.302084F;
            this.Line127.X1 = 2.593861F;
            this.Line127.X2 = 4.895945F;
            this.Line127.Y1 = 3.514166F;
            this.Line127.Y2 = 3.514166F;
            // 
            // Line128
            // 
            this.Line128.Height = 0F;
            this.Line128.Left = 2.593861F;
            this.Line128.LineWeight = 1F;
            this.Line128.Name = "Line128";
            this.Line128.Top = 3.764166F;
            this.Line128.Visible = false;
            this.Line128.Width = 2.302084F;
            this.Line128.X1 = 2.593861F;
            this.Line128.X2 = 4.895945F;
            this.Line128.Y1 = 3.764166F;
            this.Line128.Y2 = 3.764166F;
            // 
            // Line130
            // 
            this.Line130.Height = 0F;
            this.Line130.Left = 2.593861F;
            this.Line130.LineWeight = 1F;
            this.Line130.Name = "Line130";
            this.Line130.Top = 4.014166F;
            this.Line130.Visible = false;
            this.Line130.Width = 2.302084F;
            this.Line130.X1 = 2.593861F;
            this.Line130.X2 = 4.895945F;
            this.Line130.Y1 = 4.014166F;
            this.Line130.Y2 = 4.014166F;
            // 
            // Line131
            // 
            this.Line131.Height = 0F;
            this.Line131.Left = 2.593861F;
            this.Line131.LineWeight = 1F;
            this.Line131.Name = "Line131";
            this.Line131.Top = 4.514166F;
            this.Line131.Visible = false;
            this.Line131.Width = 2.302084F;
            this.Line131.X1 = 2.593861F;
            this.Line131.X2 = 4.895945F;
            this.Line131.Y1 = 4.514166F;
            this.Line131.Y2 = 4.514166F;
            // 
            // Line133
            // 
            this.Line133.Height = 0F;
            this.Line133.Left = 2.593861F;
            this.Line133.LineWeight = 1F;
            this.Line133.Name = "Line133";
            this.Line133.Top = 4.764166F;
            this.Line133.Visible = false;
            this.Line133.Width = 2.302084F;
            this.Line133.X1 = 2.593861F;
            this.Line133.X2 = 4.895945F;
            this.Line133.Y1 = 4.764166F;
            this.Line133.Y2 = 4.764166F;
            // 
            // Line134
            // 
            this.Line134.Height = 0F;
            this.Line134.Left = 2.593861F;
            this.Line134.LineWeight = 1F;
            this.Line134.Name = "Line134";
            this.Line134.Top = 5.014166F;
            this.Line134.Visible = false;
            this.Line134.Width = 2.302084F;
            this.Line134.X1 = 2.593861F;
            this.Line134.X2 = 4.895945F;
            this.Line134.Y1 = 5.014166F;
            this.Line134.Y2 = 5.014166F;
            // 
            // Line136
            // 
            this.Line136.Height = 0F;
            this.Line136.Left = 2.593861F;
            this.Line136.LineWeight = 1F;
            this.Line136.Name = "Line136";
            this.Line136.Top = 5.264166F;
            this.Line136.Visible = false;
            this.Line136.Width = 2.302084F;
            this.Line136.X1 = 2.593861F;
            this.Line136.X2 = 4.895945F;
            this.Line136.Y1 = 5.264166F;
            this.Line136.Y2 = 5.264166F;
            // 
            // Line137
            // 
            this.Line137.Height = 0F;
            this.Line137.Left = 2.593861F;
            this.Line137.LineWeight = 1F;
            this.Line137.Name = "Line137";
            this.Line137.Top = 5.514166F;
            this.Line137.Visible = false;
            this.Line137.Width = 2.302084F;
            this.Line137.X1 = 2.593861F;
            this.Line137.X2 = 4.895945F;
            this.Line137.Y1 = 5.514166F;
            this.Line137.Y2 = 5.514166F;
            // 
            // Line143
            // 
            this.Line143.Height = 0F;
            this.Line143.Left = 2.593861F;
            this.Line143.LineWeight = 1F;
            this.Line143.Name = "Line143";
            this.Line143.Top = 4.264166F;
            this.Line143.Visible = false;
            this.Line143.Width = 2.302084F;
            this.Line143.X1 = 2.593861F;
            this.Line143.X2 = 4.895945F;
            this.Line143.Y1 = 4.264166F;
            this.Line143.Y2 = 4.264166F;
            // 
            // Line145
            // 
            this.Line145.Height = 5.5F;
            this.Line145.Left = 6.329973F;
            this.Line145.LineWeight = 1F;
            this.Line145.Name = "Line145";
            this.Line145.Top = 2.535F;
            this.Line145.Visible = false;
            this.Line145.Width = 0F;
            this.Line145.X1 = 6.329973F;
            this.Line145.X2 = 6.329973F;
            this.Line145.Y1 = 2.535F;
            this.Line145.Y2 = 8.035F;
            // 
            // Line146
            // 
            this.Line146.Height = 0F;
            this.Line146.Left = 5.198028F;
            this.Line146.LineWeight = 1F;
            this.Line146.Name = "Line146";
            this.Line146.Top = 2.764166F;
            this.Line146.Visible = false;
            this.Line146.Width = 2.291667F;
            this.Line146.X1 = 5.198028F;
            this.Line146.X2 = 7.489695F;
            this.Line146.Y1 = 2.764166F;
            this.Line146.Y2 = 2.764166F;
            // 
            // Line147
            // 
            this.Line147.Height = 0F;
            this.Line147.Left = 5.198028F;
            this.Line147.LineWeight = 1F;
            this.Line147.Name = "Line147";
            this.Line147.Top = 3.014166F;
            this.Line147.Visible = false;
            this.Line147.Width = 2.291667F;
            this.Line147.X1 = 5.198028F;
            this.Line147.X2 = 7.489695F;
            this.Line147.Y1 = 3.014166F;
            this.Line147.Y2 = 3.014166F;
            // 
            // Line148
            // 
            this.Line148.Height = 0F;
            this.Line148.Left = 5.198028F;
            this.Line148.LineWeight = 1F;
            this.Line148.Name = "Line148";
            this.Line148.Top = 3.264166F;
            this.Line148.Visible = false;
            this.Line148.Width = 2.291667F;
            this.Line148.X1 = 5.198028F;
            this.Line148.X2 = 7.489695F;
            this.Line148.Y1 = 3.264166F;
            this.Line148.Y2 = 3.264166F;
            // 
            // Line150
            // 
            this.Line150.Height = 0F;
            this.Line150.Left = 5.198028F;
            this.Line150.LineWeight = 1F;
            this.Line150.Name = "Line150";
            this.Line150.Top = 3.514166F;
            this.Line150.Visible = false;
            this.Line150.Width = 2.291667F;
            this.Line150.X1 = 5.198028F;
            this.Line150.X2 = 7.489695F;
            this.Line150.Y1 = 3.514166F;
            this.Line150.Y2 = 3.514166F;
            // 
            // Line151
            // 
            this.Line151.Height = 0F;
            this.Line151.Left = 5.198028F;
            this.Line151.LineWeight = 1F;
            this.Line151.Name = "Line151";
            this.Line151.Top = 3.764166F;
            this.Line151.Visible = false;
            this.Line151.Width = 2.291667F;
            this.Line151.X1 = 5.198028F;
            this.Line151.X2 = 7.489695F;
            this.Line151.Y1 = 3.764166F;
            this.Line151.Y2 = 3.764166F;
            // 
            // Line152
            // 
            this.Line152.Height = 0F;
            this.Line152.Left = 5.198028F;
            this.Line152.LineWeight = 1F;
            this.Line152.Name = "Line152";
            this.Line152.Top = 4.014166F;
            this.Line152.Visible = false;
            this.Line152.Width = 2.291667F;
            this.Line152.X1 = 5.198028F;
            this.Line152.X2 = 7.489695F;
            this.Line152.Y1 = 4.014166F;
            this.Line152.Y2 = 4.014166F;
            // 
            // Line154
            // 
            this.Line154.Height = 0F;
            this.Line154.Left = 5.198028F;
            this.Line154.LineWeight = 1F;
            this.Line154.Name = "Line154";
            this.Line154.Top = 4.514166F;
            this.Line154.Visible = false;
            this.Line154.Width = 2.291667F;
            this.Line154.X1 = 5.198028F;
            this.Line154.X2 = 7.489695F;
            this.Line154.Y1 = 4.514166F;
            this.Line154.Y2 = 4.514166F;
            // 
            // Line155
            // 
            this.Line155.Height = 0F;
            this.Line155.Left = 5.198028F;
            this.Line155.LineWeight = 1F;
            this.Line155.Name = "Line155";
            this.Line155.Top = 4.764166F;
            this.Line155.Visible = false;
            this.Line155.Width = 2.291667F;
            this.Line155.X1 = 5.198028F;
            this.Line155.X2 = 7.489695F;
            this.Line155.Y1 = 4.764166F;
            this.Line155.Y2 = 4.764166F;
            // 
            // Line157
            // 
            this.Line157.Height = 0F;
            this.Line157.Left = 5.198028F;
            this.Line157.LineWeight = 1F;
            this.Line157.Name = "Line157";
            this.Line157.Top = 5.014166F;
            this.Line157.Visible = false;
            this.Line157.Width = 2.291667F;
            this.Line157.X1 = 5.198028F;
            this.Line157.X2 = 7.489695F;
            this.Line157.Y1 = 5.014166F;
            this.Line157.Y2 = 5.014166F;
            // 
            // Line158
            // 
            this.Line158.Height = 0F;
            this.Line158.Left = 5.198028F;
            this.Line158.LineWeight = 1F;
            this.Line158.Name = "Line158";
            this.Line158.Top = 5.264166F;
            this.Line158.Visible = false;
            this.Line158.Width = 2.291667F;
            this.Line158.X1 = 5.198028F;
            this.Line158.X2 = 7.489695F;
            this.Line158.Y1 = 5.264166F;
            this.Line158.Y2 = 5.264166F;
            // 
            // Line160
            // 
            this.Line160.Height = 0F;
            this.Line160.Left = 5.198028F;
            this.Line160.LineWeight = 1F;
            this.Line160.Name = "Line160";
            this.Line160.Top = 5.514166F;
            this.Line160.Visible = false;
            this.Line160.Width = 2.291667F;
            this.Line160.X1 = 5.198028F;
            this.Line160.X2 = 7.489695F;
            this.Line160.Y1 = 5.514166F;
            this.Line160.Y2 = 5.514166F;
            // 
            // Line166
            // 
            this.Line166.Height = 0F;
            this.Line166.Left = 5.198028F;
            this.Line166.LineWeight = 1F;
            this.Line166.Name = "Line166";
            this.Line166.Top = 4.264166F;
            this.Line166.Visible = false;
            this.Line166.Width = 2.291667F;
            this.Line166.X1 = 5.198028F;
            this.Line166.X2 = 7.489695F;
            this.Line166.Y1 = 4.264166F;
            this.Line166.Y2 = 4.264166F;
            // 
            // Label114
            // 
            this.Label114.Height = 0.1666667F;
            this.Label114.HyperLink = null;
            this.Label114.Left = 2.822917F;
            this.Label114.Name = "Label114";
            this.Label114.Style = "font-weight: bold; text-align: center; text-decoration: underline";
            this.Label114.Text = "Schedule1";
            this.Label114.Top = 1.472333F;
            this.Label114.Width = 1.854167F;
            // 
            // txtMonthStart
            // 
            this.txtMonthStart.Height = 0.1666667F;
            this.txtMonthStart.Left = 3.062333F;
            this.txtMonthStart.Name = "txtMonthStart";
            this.txtMonthStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtMonthStart.Text = "99";
            this.txtMonthStart.Top = 0.9540001F;
            this.txtMonthStart.Visible = false;
            this.txtMonthStart.Width = 0.25F;
            // 
            // txtDayStart
            // 
            this.txtDayStart.Height = 0.1666667F;
            this.txtDayStart.Left = 3.458166F;
            this.txtDayStart.Name = "txtDayStart";
            this.txtDayStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDayStart.Text = "99";
            this.txtDayStart.Top = 0.9540001F;
            this.txtDayStart.Visible = false;
            this.txtDayStart.Width = 0.25F;
            // 
            // txtYearStart
            // 
            this.txtYearStart.Height = 0.1666667F;
            this.txtYearStart.Left = 3.864416F;
            this.txtYearStart.Name = "txtYearStart";
            this.txtYearStart.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtYearStart.Text = "99";
            this.txtYearStart.Top = 0.9540001F;
            this.txtYearStart.Visible = false;
            this.txtYearStart.Width = 0.2499998F;
            // 
            // txtMonthEnd
            // 
            this.txtMonthEnd.Height = 0.1666667F;
            this.txtMonthEnd.Left = 4.464416F;
            this.txtMonthEnd.Name = "txtMonthEnd";
            this.txtMonthEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtMonthEnd.Text = "99";
            this.txtMonthEnd.Top = 0.9540001F;
            this.txtMonthEnd.Visible = false;
            this.txtMonthEnd.Width = 0.25F;
            // 
            // txtDayEnd
            // 
            this.txtDayEnd.Height = 0.1666667F;
            this.txtDayEnd.Left = 4.864417F;
            this.txtDayEnd.Name = "txtDayEnd";
            this.txtDayEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtDayEnd.Text = "99";
            this.txtDayEnd.Top = 0.9540001F;
            this.txtDayEnd.Visible = false;
            this.txtDayEnd.Width = 0.25F;
            // 
            // txtYearEnd
            // 
            this.txtYearEnd.Height = 0.1666667F;
            this.txtYearEnd.Left = 5.26025F;
            this.txtYearEnd.Name = "txtYearEnd";
            this.txtYearEnd.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left; " +
    "vertical-align: middle";
            this.txtYearEnd.Text = "99";
            this.txtYearEnd.Top = 0.9540001F;
            this.txtYearEnd.Visible = false;
            this.txtYearEnd.Width = 0.25F;
            // 
            // txtAmount13
            // 
            this.txtAmount13.CanGrow = false;
            this.txtAmount13.Height = 0.1666667F;
            this.txtAmount13.Left = 1.365166F;
            this.txtAmount13.Name = "txtAmount13";
            this.txtAmount13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount13.Text = "999999.99";
            this.txtAmount13.Top = 5.525832F;
            this.txtAmount13.Width = 0.8333333F;
            // 
            // txtDatePaid13
            // 
            this.txtDatePaid13.CanGrow = false;
            this.txtDatePaid13.Height = 0.1666667F;
            this.txtDatePaid13.Left = 0.136F;
            this.txtDatePaid13.Name = "txtDatePaid13";
            this.txtDatePaid13.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid13.Text = "99 99 99";
            this.txtDatePaid13.Top = 5.53F;
            this.txtDatePaid13.Width = 0.75F;
            // 
            // txtAmount14
            // 
            this.txtAmount14.CanGrow = false;
            this.txtAmount14.Height = 0.1666667F;
            this.txtAmount14.Left = 1.365166F;
            this.txtAmount14.Name = "txtAmount14";
            this.txtAmount14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount14.Text = "999999.99";
            this.txtAmount14.Top = 5.78F;
            this.txtAmount14.Width = 0.8333333F;
            // 
            // txtDatePaid14
            // 
            this.txtDatePaid14.CanGrow = false;
            this.txtDatePaid14.Height = 0.1666667F;
            this.txtDatePaid14.Left = 0.136F;
            this.txtDatePaid14.Name = "txtDatePaid14";
            this.txtDatePaid14.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid14.Text = "99 99 99";
            this.txtDatePaid14.Top = 5.78F;
            this.txtDatePaid14.Width = 0.75F;
            // 
            // txtAmount15
            // 
            this.txtAmount15.CanGrow = false;
            this.txtAmount15.Height = 0.1666667F;
            this.txtAmount15.Left = 1.365166F;
            this.txtAmount15.Name = "txtAmount15";
            this.txtAmount15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount15.Text = "999999.99";
            this.txtAmount15.Top = 6.03F;
            this.txtAmount15.Width = 0.8333333F;
            // 
            // txtDatePaid15
            // 
            this.txtDatePaid15.CanGrow = false;
            this.txtDatePaid15.Height = 0.1666667F;
            this.txtDatePaid15.Left = 0.136F;
            this.txtDatePaid15.Name = "txtDatePaid15";
            this.txtDatePaid15.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid15.Text = "99 99 99";
            this.txtDatePaid15.Top = 6.03F;
            this.txtDatePaid15.Width = 0.75F;
            // 
            // txtAmount16
            // 
            this.txtAmount16.CanGrow = false;
            this.txtAmount16.Height = 0.1666667F;
            this.txtAmount16.Left = 1.365166F;
            this.txtAmount16.Name = "txtAmount16";
            this.txtAmount16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount16.Text = "999999.99";
            this.txtAmount16.Top = 6.28F;
            this.txtAmount16.Width = 0.8333333F;
            // 
            // txtDatePaid16
            // 
            this.txtDatePaid16.CanGrow = false;
            this.txtDatePaid16.Height = 0.1666667F;
            this.txtDatePaid16.Left = 0.136F;
            this.txtDatePaid16.Name = "txtDatePaid16";
            this.txtDatePaid16.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid16.Text = "99 99 99";
            this.txtDatePaid16.Top = 6.28F;
            this.txtDatePaid16.Width = 0.75F;
            // 
            // txtAmount17
            // 
            this.txtAmount17.CanGrow = false;
            this.txtAmount17.Height = 0.1666667F;
            this.txtAmount17.Left = 1.365166F;
            this.txtAmount17.Name = "txtAmount17";
            this.txtAmount17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount17.Text = "999999.99";
            this.txtAmount17.Top = 6.53F;
            this.txtAmount17.Width = 0.8333333F;
            // 
            // txtDatePaid17
            // 
            this.txtDatePaid17.CanGrow = false;
            this.txtDatePaid17.Height = 0.1666667F;
            this.txtDatePaid17.Left = 0.136F;
            this.txtDatePaid17.Name = "txtDatePaid17";
            this.txtDatePaid17.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid17.Text = "99 99 99";
            this.txtDatePaid17.Top = 6.53F;
            this.txtDatePaid17.Width = 0.75F;
            // 
            // txtAmount18
            // 
            this.txtAmount18.CanGrow = false;
            this.txtAmount18.Height = 0.1666667F;
            this.txtAmount18.Left = 1.365166F;
            this.txtAmount18.Name = "txtAmount18";
            this.txtAmount18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount18.Text = "999999.99";
            this.txtAmount18.Top = 6.78F;
            this.txtAmount18.Width = 0.8333333F;
            // 
            // txtDatePaid18
            // 
            this.txtDatePaid18.CanGrow = false;
            this.txtDatePaid18.Height = 0.1666667F;
            this.txtDatePaid18.Left = 0.136F;
            this.txtDatePaid18.Name = "txtDatePaid18";
            this.txtDatePaid18.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid18.Text = "99 99 99";
            this.txtDatePaid18.Top = 6.78F;
            this.txtDatePaid18.Width = 0.75F;
            // 
            // txtAmount19
            // 
            this.txtAmount19.CanGrow = false;
            this.txtAmount19.Height = 0.1666667F;
            this.txtAmount19.Left = 1.365166F;
            this.txtAmount19.Name = "txtAmount19";
            this.txtAmount19.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount19.Text = "999999.99";
            this.txtAmount19.Top = 7.03F;
            this.txtAmount19.Width = 0.8333333F;
            // 
            // txtDatePaid19
            // 
            this.txtDatePaid19.CanGrow = false;
            this.txtDatePaid19.Height = 0.1666667F;
            this.txtDatePaid19.Left = 0.136F;
            this.txtDatePaid19.Name = "txtDatePaid19";
            this.txtDatePaid19.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid19.Text = "99 99 99";
            this.txtDatePaid19.Top = 7.03F;
            this.txtDatePaid19.Width = 0.75F;
            // 
            // txtAmount20
            // 
            this.txtAmount20.CanGrow = false;
            this.txtAmount20.Height = 0.1666667F;
            this.txtAmount20.Left = 1.365166F;
            this.txtAmount20.Name = "txtAmount20";
            this.txtAmount20.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount20.Text = "999999.99";
            this.txtAmount20.Top = 7.28F;
            this.txtAmount20.Width = 0.8333333F;
            // 
            // txtDatePaid20
            // 
            this.txtDatePaid20.CanGrow = false;
            this.txtDatePaid20.Height = 0.1666667F;
            this.txtDatePaid20.Left = 0.136F;
            this.txtDatePaid20.Name = "txtDatePaid20";
            this.txtDatePaid20.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid20.Text = "99 99 99";
            this.txtDatePaid20.Top = 7.28F;
            this.txtDatePaid20.Width = 0.75F;
            // 
            // txtAmount21
            // 
            this.txtAmount21.CanGrow = false;
            this.txtAmount21.Height = 0.1666667F;
            this.txtAmount21.Left = 1.365166F;
            this.txtAmount21.Name = "txtAmount21";
            this.txtAmount21.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount21.Text = "999999.99";
            this.txtAmount21.Top = 7.53F;
            this.txtAmount21.Width = 0.8333333F;
            // 
            // txtDatePaid21
            // 
            this.txtDatePaid21.CanGrow = false;
            this.txtDatePaid21.Height = 0.1666667F;
            this.txtDatePaid21.Left = 0.1568333F;
            this.txtDatePaid21.Name = "txtDatePaid21";
            this.txtDatePaid21.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid21.Text = "99 99 99";
            this.txtDatePaid21.Top = 7.525832F;
            this.txtDatePaid21.Width = 0.75F;
            // 
            // Line167
            // 
            this.Line167.Height = 0F;
            this.Line167.Left = 0.014F;
            this.Line167.LineWeight = 1F;
            this.Line167.Name = "Line167";
            this.Line167.Top = 5.768333F;
            this.Line167.Visible = false;
            this.Line167.Width = 2.291667F;
            this.Line167.X1 = 0.014F;
            this.Line167.X2 = 2.305667F;
            this.Line167.Y1 = 5.768333F;
            this.Line167.Y2 = 5.768333F;
            // 
            // Line168
            // 
            this.Line168.Height = 0F;
            this.Line168.Left = 0.01052778F;
            this.Line168.LineWeight = 1F;
            this.Line168.Name = "Line168";
            this.Line168.Top = 6.018333F;
            this.Line168.Visible = false;
            this.Line168.Width = 2.291666F;
            this.Line168.X1 = 0.01052778F;
            this.Line168.X2 = 2.302194F;
            this.Line168.Y1 = 6.018333F;
            this.Line168.Y2 = 6.018333F;
            // 
            // Line169
            // 
            this.Line169.Height = 0F;
            this.Line169.Left = 0.01052778F;
            this.Line169.LineWeight = 1F;
            this.Line169.Name = "Line169";
            this.Line169.Top = 6.268333F;
            this.Line169.Visible = false;
            this.Line169.Width = 2.291666F;
            this.Line169.X1 = 0.01052778F;
            this.Line169.X2 = 2.302194F;
            this.Line169.Y1 = 6.268333F;
            this.Line169.Y2 = 6.268333F;
            // 
            // Line170
            // 
            this.Line170.Height = 0F;
            this.Line170.Left = 0.01052778F;
            this.Line170.LineWeight = 1F;
            this.Line170.Name = "Line170";
            this.Line170.Top = 6.518333F;
            this.Line170.Visible = false;
            this.Line170.Width = 2.291666F;
            this.Line170.X1 = 0.01052778F;
            this.Line170.X2 = 2.302194F;
            this.Line170.Y1 = 6.518333F;
            this.Line170.Y2 = 6.518333F;
            // 
            // Line171
            // 
            this.Line171.Height = 0F;
            this.Line171.Left = 0.01052778F;
            this.Line171.LineWeight = 1F;
            this.Line171.Name = "Line171";
            this.Line171.Top = 6.768333F;
            this.Line171.Visible = false;
            this.Line171.Width = 2.291666F;
            this.Line171.X1 = 0.01052778F;
            this.Line171.X2 = 2.302194F;
            this.Line171.Y1 = 6.768333F;
            this.Line171.Y2 = 6.768333F;
            // 
            // Line172
            // 
            this.Line172.Height = 0F;
            this.Line172.Left = 0.01052778F;
            this.Line172.LineWeight = 1F;
            this.Line172.Name = "Line172";
            this.Line172.Top = 7.018333F;
            this.Line172.Visible = false;
            this.Line172.Width = 2.291666F;
            this.Line172.X1 = 0.01052778F;
            this.Line172.X2 = 2.302194F;
            this.Line172.Y1 = 7.018333F;
            this.Line172.Y2 = 7.018333F;
            // 
            // Line173
            // 
            this.Line173.Height = 0F;
            this.Line173.Left = 0.01052778F;
            this.Line173.LineWeight = 1F;
            this.Line173.Name = "Line173";
            this.Line173.Top = 7.268333F;
            this.Line173.Visible = false;
            this.Line173.Width = 2.291666F;
            this.Line173.X1 = 0.01052778F;
            this.Line173.X2 = 2.302194F;
            this.Line173.Y1 = 7.268333F;
            this.Line173.Y2 = 7.268333F;
            // 
            // Line174
            // 
            this.Line174.Height = 0F;
            this.Line174.Left = 0.01052778F;
            this.Line174.LineWeight = 1F;
            this.Line174.Name = "Line174";
            this.Line174.Top = 7.518334F;
            this.Line174.Visible = false;
            this.Line174.Width = 2.291666F;
            this.Line174.X1 = 0.01052778F;
            this.Line174.X2 = 2.302194F;
            this.Line174.Y1 = 7.518334F;
            this.Line174.Y2 = 7.518334F;
            // 
            // Line175
            // 
            this.Line175.Height = 0F;
            this.Line175.Left = 0.01052778F;
            this.Line175.LineWeight = 1F;
            this.Line175.Name = "Line175";
            this.Line175.Top = 7.768334F;
            this.Line175.Visible = false;
            this.Line175.Width = 2.291666F;
            this.Line175.X1 = 0.01052778F;
            this.Line175.X2 = 2.302194F;
            this.Line175.Y1 = 7.768334F;
            this.Line175.Y2 = 7.768334F;
            // 
            // Line99
            // 
            this.Line99.Height = 5.5F;
            this.Line99.Left = 1.125111F;
            this.Line99.LineWeight = 1F;
            this.Line99.Name = "Line99";
            this.Line99.Top = 2.535F;
            this.Line99.Visible = false;
            this.Line99.Width = 0F;
            this.Line99.X1 = 1.125111F;
            this.Line99.X2 = 1.125111F;
            this.Line99.Y1 = 2.535F;
            this.Line99.Y2 = 8.035F;
            // 
            // txtDatePaid34
            // 
            this.txtDatePaid34.CanGrow = false;
            this.txtDatePaid34.Height = 0.1666667F;
            this.txtDatePaid34.Left = 2.708916F;
            this.txtDatePaid34.Name = "txtDatePaid34";
            this.txtDatePaid34.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid34.Text = "99 99 99";
            this.txtDatePaid34.Top = 5.525832F;
            this.txtDatePaid34.Width = 0.75F;
            // 
            // txtAmount34
            // 
            this.txtAmount34.CanGrow = false;
            this.txtAmount34.Height = 0.1666667F;
            this.txtAmount34.Left = 3.958916F;
            this.txtAmount34.Name = "txtAmount34";
            this.txtAmount34.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount34.Text = "999999.99";
            this.txtAmount34.Top = 5.525832F;
            this.txtAmount34.Width = 0.8333333F;
            // 
            // txtDatePaid35
            // 
            this.txtDatePaid35.CanGrow = false;
            this.txtDatePaid35.Height = 0.1666667F;
            this.txtDatePaid35.Left = 2.708916F;
            this.txtDatePaid35.Name = "txtDatePaid35";
            this.txtDatePaid35.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid35.Text = "99 99 99";
            this.txtDatePaid35.Top = 5.775832F;
            this.txtDatePaid35.Width = 0.75F;
            // 
            // txtAmount35
            // 
            this.txtAmount35.CanGrow = false;
            this.txtAmount35.Height = 0.1666667F;
            this.txtAmount35.Left = 3.958916F;
            this.txtAmount35.Name = "txtAmount35";
            this.txtAmount35.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount35.Text = "999999.99";
            this.txtAmount35.Top = 5.775832F;
            this.txtAmount35.Width = 0.8333333F;
            // 
            // txtDatePaid36
            // 
            this.txtDatePaid36.CanGrow = false;
            this.txtDatePaid36.Height = 0.1666667F;
            this.txtDatePaid36.Left = 2.708916F;
            this.txtDatePaid36.Name = "txtDatePaid36";
            this.txtDatePaid36.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid36.Text = "99 99 99";
            this.txtDatePaid36.Top = 6.025832F;
            this.txtDatePaid36.Width = 0.75F;
            // 
            // txtAmount36
            // 
            this.txtAmount36.CanGrow = false;
            this.txtAmount36.Height = 0.1666667F;
            this.txtAmount36.Left = 3.958916F;
            this.txtAmount36.Name = "txtAmount36";
            this.txtAmount36.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount36.Text = "999999.99";
            this.txtAmount36.Top = 6.025832F;
            this.txtAmount36.Width = 0.8333333F;
            // 
            // txtDatePaid37
            // 
            this.txtDatePaid37.CanGrow = false;
            this.txtDatePaid37.Height = 0.1666667F;
            this.txtDatePaid37.Left = 2.708916F;
            this.txtDatePaid37.Name = "txtDatePaid37";
            this.txtDatePaid37.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid37.Text = "99 99 99";
            this.txtDatePaid37.Top = 6.275832F;
            this.txtDatePaid37.Width = 0.75F;
            // 
            // txtAmount37
            // 
            this.txtAmount37.CanGrow = false;
            this.txtAmount37.Height = 0.1666667F;
            this.txtAmount37.Left = 3.958916F;
            this.txtAmount37.Name = "txtAmount37";
            this.txtAmount37.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount37.Text = "999999.99";
            this.txtAmount37.Top = 6.275832F;
            this.txtAmount37.Width = 0.8333333F;
            // 
            // txtDatePaid38
            // 
            this.txtDatePaid38.CanGrow = false;
            this.txtDatePaid38.Height = 0.1666667F;
            this.txtDatePaid38.Left = 2.708916F;
            this.txtDatePaid38.Name = "txtDatePaid38";
            this.txtDatePaid38.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid38.Text = "99 99 99";
            this.txtDatePaid38.Top = 6.525832F;
            this.txtDatePaid38.Width = 0.75F;
            // 
            // txtAmount38
            // 
            this.txtAmount38.CanGrow = false;
            this.txtAmount38.Height = 0.1666667F;
            this.txtAmount38.Left = 3.958916F;
            this.txtAmount38.Name = "txtAmount38";
            this.txtAmount38.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount38.Text = "999999.99";
            this.txtAmount38.Top = 6.525832F;
            this.txtAmount38.Width = 0.8333333F;
            // 
            // txtDatePaid39
            // 
            this.txtDatePaid39.CanGrow = false;
            this.txtDatePaid39.Height = 0.1666667F;
            this.txtDatePaid39.Left = 2.708916F;
            this.txtDatePaid39.Name = "txtDatePaid39";
            this.txtDatePaid39.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid39.Text = "99 99 99";
            this.txtDatePaid39.Top = 6.775832F;
            this.txtDatePaid39.Width = 0.75F;
            // 
            // txtAmount39
            // 
            this.txtAmount39.CanGrow = false;
            this.txtAmount39.Height = 0.1666667F;
            this.txtAmount39.Left = 3.958916F;
            this.txtAmount39.Name = "txtAmount39";
            this.txtAmount39.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount39.Text = "999999.99";
            this.txtAmount39.Top = 6.775832F;
            this.txtAmount39.Width = 0.8333333F;
            // 
            // txtDatePaid40
            // 
            this.txtDatePaid40.CanGrow = false;
            this.txtDatePaid40.Height = 0.1666667F;
            this.txtDatePaid40.Left = 2.708916F;
            this.txtDatePaid40.Name = "txtDatePaid40";
            this.txtDatePaid40.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid40.Text = "99 99 99";
            this.txtDatePaid40.Top = 7.025832F;
            this.txtDatePaid40.Width = 0.75F;
            // 
            // txtAmount40
            // 
            this.txtAmount40.CanGrow = false;
            this.txtAmount40.Height = 0.1666667F;
            this.txtAmount40.Left = 3.958916F;
            this.txtAmount40.Name = "txtAmount40";
            this.txtAmount40.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount40.Text = "999999.99";
            this.txtAmount40.Top = 7.025832F;
            this.txtAmount40.Width = 0.8333333F;
            // 
            // txtDatePaid41
            // 
            this.txtDatePaid41.CanGrow = false;
            this.txtDatePaid41.Height = 0.1666667F;
            this.txtDatePaid41.Left = 2.708916F;
            this.txtDatePaid41.Name = "txtDatePaid41";
            this.txtDatePaid41.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid41.Text = "99 99 99";
            this.txtDatePaid41.Top = 7.275832F;
            this.txtDatePaid41.Width = 0.75F;
            // 
            // txtAmount41
            // 
            this.txtAmount41.CanGrow = false;
            this.txtAmount41.Height = 0.1666667F;
            this.txtAmount41.Left = 3.958916F;
            this.txtAmount41.Name = "txtAmount41";
            this.txtAmount41.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount41.Text = "999999.99";
            this.txtAmount41.Top = 7.275832F;
            this.txtAmount41.Width = 0.8333333F;
            // 
            // txtDatePaid42
            // 
            this.txtDatePaid42.CanGrow = false;
            this.txtDatePaid42.Height = 0.1666667F;
            this.txtDatePaid42.Left = 2.708916F;
            this.txtDatePaid42.Name = "txtDatePaid42";
            this.txtDatePaid42.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid42.Text = "99 99 99";
            this.txtDatePaid42.Top = 7.525832F;
            this.txtDatePaid42.Width = 0.75F;
            // 
            // txtAmount42
            // 
            this.txtAmount42.CanGrow = false;
            this.txtAmount42.Height = 0.1666667F;
            this.txtAmount42.Left = 3.958916F;
            this.txtAmount42.Name = "txtAmount42";
            this.txtAmount42.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount42.Text = "999999.99";
            this.txtAmount42.Top = 7.525832F;
            this.txtAmount42.Width = 0.8333333F;
            // 
            // txtAmount55
            // 
            this.txtAmount55.CanGrow = false;
            this.txtAmount55.Height = 0.1666667F;
            this.txtAmount55.Left = 6.552667F;
            this.txtAmount55.Name = "txtAmount55";
            this.txtAmount55.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount55.Text = "999999.99";
            this.txtAmount55.Top = 5.525832F;
            this.txtAmount55.Width = 0.8333333F;
            // 
            // txtDatePaid55
            // 
            this.txtDatePaid55.CanGrow = false;
            this.txtDatePaid55.Height = 0.1666667F;
            this.txtDatePaid55.Left = 5.302667F;
            this.txtDatePaid55.Name = "txtDatePaid55";
            this.txtDatePaid55.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid55.Text = "99 99 99";
            this.txtDatePaid55.Top = 5.525832F;
            this.txtDatePaid55.Width = 0.75F;
            // 
            // txtAmount56
            // 
            this.txtAmount56.CanGrow = false;
            this.txtAmount56.Height = 0.1666667F;
            this.txtAmount56.Left = 6.552667F;
            this.txtAmount56.Name = "txtAmount56";
            this.txtAmount56.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount56.Text = "999999.99";
            this.txtAmount56.Top = 5.775832F;
            this.txtAmount56.Width = 0.8333333F;
            // 
            // txtDatePaid56
            // 
            this.txtDatePaid56.CanGrow = false;
            this.txtDatePaid56.Height = 0.1666667F;
            this.txtDatePaid56.Left = 5.302667F;
            this.txtDatePaid56.Name = "txtDatePaid56";
            this.txtDatePaid56.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid56.Text = "99 99 99";
            this.txtDatePaid56.Top = 5.775832F;
            this.txtDatePaid56.Width = 0.75F;
            // 
            // txtAmount57
            // 
            this.txtAmount57.CanGrow = false;
            this.txtAmount57.Height = 0.1666667F;
            this.txtAmount57.Left = 6.552667F;
            this.txtAmount57.Name = "txtAmount57";
            this.txtAmount57.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount57.Text = "999999.99";
            this.txtAmount57.Top = 6.025832F;
            this.txtAmount57.Width = 0.8333333F;
            // 
            // txtDatePaid57
            // 
            this.txtDatePaid57.CanGrow = false;
            this.txtDatePaid57.Height = 0.1666667F;
            this.txtDatePaid57.Left = 5.302667F;
            this.txtDatePaid57.Name = "txtDatePaid57";
            this.txtDatePaid57.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid57.Text = "99 99 99";
            this.txtDatePaid57.Top = 6.025832F;
            this.txtDatePaid57.Width = 0.75F;
            // 
            // txtAmount58
            // 
            this.txtAmount58.CanGrow = false;
            this.txtAmount58.Height = 0.1666667F;
            this.txtAmount58.Left = 6.552667F;
            this.txtAmount58.Name = "txtAmount58";
            this.txtAmount58.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount58.Text = "999999.99";
            this.txtAmount58.Top = 6.275832F;
            this.txtAmount58.Width = 0.8333333F;
            // 
            // txtDatePaid58
            // 
            this.txtDatePaid58.CanGrow = false;
            this.txtDatePaid58.Height = 0.1666667F;
            this.txtDatePaid58.Left = 5.302667F;
            this.txtDatePaid58.Name = "txtDatePaid58";
            this.txtDatePaid58.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid58.Text = "99 99 99";
            this.txtDatePaid58.Top = 6.275832F;
            this.txtDatePaid58.Width = 0.75F;
            // 
            // txtAmount59
            // 
            this.txtAmount59.CanGrow = false;
            this.txtAmount59.Height = 0.1666667F;
            this.txtAmount59.Left = 6.552667F;
            this.txtAmount59.Name = "txtAmount59";
            this.txtAmount59.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount59.Text = "999999.99";
            this.txtAmount59.Top = 6.525832F;
            this.txtAmount59.Width = 0.8333333F;
            // 
            // txtDatePaid59
            // 
            this.txtDatePaid59.CanGrow = false;
            this.txtDatePaid59.Height = 0.1666667F;
            this.txtDatePaid59.Left = 5.302667F;
            this.txtDatePaid59.Name = "txtDatePaid59";
            this.txtDatePaid59.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid59.Text = "99 99 99";
            this.txtDatePaid59.Top = 6.525832F;
            this.txtDatePaid59.Width = 0.75F;
            // 
            // txtAmount60
            // 
            this.txtAmount60.CanGrow = false;
            this.txtAmount60.Height = 0.1666667F;
            this.txtAmount60.Left = 6.552667F;
            this.txtAmount60.Name = "txtAmount60";
            this.txtAmount60.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount60.Text = "999999.99";
            this.txtAmount60.Top = 6.775832F;
            this.txtAmount60.Width = 0.8333333F;
            // 
            // txtDatePaid60
            // 
            this.txtDatePaid60.CanGrow = false;
            this.txtDatePaid60.Height = 0.1666667F;
            this.txtDatePaid60.Left = 5.302667F;
            this.txtDatePaid60.Name = "txtDatePaid60";
            this.txtDatePaid60.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid60.Text = "99 99 99";
            this.txtDatePaid60.Top = 6.775832F;
            this.txtDatePaid60.Width = 0.75F;
            // 
            // txtAmount61
            // 
            this.txtAmount61.CanGrow = false;
            this.txtAmount61.Height = 0.1666667F;
            this.txtAmount61.Left = 6.552667F;
            this.txtAmount61.Name = "txtAmount61";
            this.txtAmount61.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount61.Text = "999999.99";
            this.txtAmount61.Top = 7.025832F;
            this.txtAmount61.Width = 0.8333333F;
            // 
            // txtDatePaid61
            // 
            this.txtDatePaid61.CanGrow = false;
            this.txtDatePaid61.Height = 0.1666667F;
            this.txtDatePaid61.Left = 5.302667F;
            this.txtDatePaid61.Name = "txtDatePaid61";
            this.txtDatePaid61.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid61.Text = "99 99 99";
            this.txtDatePaid61.Top = 7.025832F;
            this.txtDatePaid61.Width = 0.75F;
            // 
            // txtAmount62
            // 
            this.txtAmount62.CanGrow = false;
            this.txtAmount62.Height = 0.1666667F;
            this.txtAmount62.Left = 6.552667F;
            this.txtAmount62.Name = "txtAmount62";
            this.txtAmount62.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount62.Text = "999999.99";
            this.txtAmount62.Top = 7.275832F;
            this.txtAmount62.Width = 0.8333333F;
            // 
            // txtDatePaid62
            // 
            this.txtDatePaid62.CanGrow = false;
            this.txtDatePaid62.Height = 0.1666667F;
            this.txtDatePaid62.Left = 5.302667F;
            this.txtDatePaid62.Name = "txtDatePaid62";
            this.txtDatePaid62.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid62.Text = "99 99 99";
            this.txtDatePaid62.Top = 7.275832F;
            this.txtDatePaid62.Width = 0.75F;
            // 
            // txtAmount63
            // 
            this.txtAmount63.CanGrow = false;
            this.txtAmount63.Height = 0.1666667F;
            this.txtAmount63.Left = 6.552667F;
            this.txtAmount63.Name = "txtAmount63";
            this.txtAmount63.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: right";
            this.txtAmount63.Text = "999999.99";
            this.txtAmount63.Top = 7.525832F;
            this.txtAmount63.Width = 0.8333333F;
            // 
            // txtDatePaid63
            // 
            this.txtDatePaid63.CanGrow = false;
            this.txtDatePaid63.Height = 0.1666667F;
            this.txtDatePaid63.Left = 5.302667F;
            this.txtDatePaid63.Name = "txtDatePaid63";
            this.txtDatePaid63.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: center" +
    "";
            this.txtDatePaid63.Text = "99 99 99";
            this.txtDatePaid63.Top = 7.525832F;
            this.txtDatePaid63.Width = 0.75F;
            // 
            // Line176
            // 
            this.Line176.Height = 0F;
            this.Line176.Left = 2.593861F;
            this.Line176.LineWeight = 1F;
            this.Line176.Name = "Line176";
            this.Line176.Top = 5.764166F;
            this.Line176.Visible = false;
            this.Line176.Width = 2.291667F;
            this.Line176.X1 = 2.593861F;
            this.Line176.X2 = 4.885528F;
            this.Line176.Y1 = 5.764166F;
            this.Line176.Y2 = 5.764166F;
            // 
            // Line177
            // 
            this.Line177.Height = 0F;
            this.Line177.Left = 2.593861F;
            this.Line177.LineWeight = 1F;
            this.Line177.Name = "Line177";
            this.Line177.Top = 6.014166F;
            this.Line177.Visible = false;
            this.Line177.Width = 2.291667F;
            this.Line177.X1 = 2.593861F;
            this.Line177.X2 = 4.885528F;
            this.Line177.Y1 = 6.014166F;
            this.Line177.Y2 = 6.014166F;
            // 
            // Line178
            // 
            this.Line178.Height = 0F;
            this.Line178.Left = 2.593861F;
            this.Line178.LineWeight = 1F;
            this.Line178.Name = "Line178";
            this.Line178.Top = 6.264166F;
            this.Line178.Visible = false;
            this.Line178.Width = 2.291667F;
            this.Line178.X1 = 2.593861F;
            this.Line178.X2 = 4.885528F;
            this.Line178.Y1 = 6.264166F;
            this.Line178.Y2 = 6.264166F;
            // 
            // Line179
            // 
            this.Line179.Height = 0F;
            this.Line179.Left = 2.593861F;
            this.Line179.LineWeight = 1F;
            this.Line179.Name = "Line179";
            this.Line179.Top = 6.514166F;
            this.Line179.Visible = false;
            this.Line179.Width = 2.291667F;
            this.Line179.X1 = 2.593861F;
            this.Line179.X2 = 4.885528F;
            this.Line179.Y1 = 6.514166F;
            this.Line179.Y2 = 6.514166F;
            // 
            // Line180
            // 
            this.Line180.Height = 0F;
            this.Line180.Left = 2.593861F;
            this.Line180.LineWeight = 1F;
            this.Line180.Name = "Line180";
            this.Line180.Top = 6.764166F;
            this.Line180.Visible = false;
            this.Line180.Width = 2.291667F;
            this.Line180.X1 = 2.593861F;
            this.Line180.X2 = 4.885528F;
            this.Line180.Y1 = 6.764166F;
            this.Line180.Y2 = 6.764166F;
            // 
            // Line181
            // 
            this.Line181.Height = 0F;
            this.Line181.Left = 2.593861F;
            this.Line181.LineWeight = 1F;
            this.Line181.Name = "Line181";
            this.Line181.Top = 7.014166F;
            this.Line181.Visible = false;
            this.Line181.Width = 2.291667F;
            this.Line181.X1 = 2.593861F;
            this.Line181.X2 = 4.885528F;
            this.Line181.Y1 = 7.014166F;
            this.Line181.Y2 = 7.014166F;
            // 
            // Line182
            // 
            this.Line182.Height = 0F;
            this.Line182.Left = 2.593861F;
            this.Line182.LineWeight = 1F;
            this.Line182.Name = "Line182";
            this.Line182.Top = 7.264166F;
            this.Line182.Visible = false;
            this.Line182.Width = 2.291667F;
            this.Line182.X1 = 2.593861F;
            this.Line182.X2 = 4.885528F;
            this.Line182.Y1 = 7.264166F;
            this.Line182.Y2 = 7.264166F;
            // 
            // Line183
            // 
            this.Line183.Height = 0F;
            this.Line183.Left = 2.593861F;
            this.Line183.LineWeight = 1F;
            this.Line183.Name = "Line183";
            this.Line183.Top = 7.514166F;
            this.Line183.Visible = false;
            this.Line183.Width = 2.291667F;
            this.Line183.X1 = 2.593861F;
            this.Line183.X2 = 4.885528F;
            this.Line183.Y1 = 7.514166F;
            this.Line183.Y2 = 7.514166F;
            // 
            // Line184
            // 
            this.Line184.Height = 0F;
            this.Line184.Left = 2.593861F;
            this.Line184.LineWeight = 1F;
            this.Line184.Name = "Line184";
            this.Line184.Top = 7.764166F;
            this.Line184.Visible = false;
            this.Line184.Width = 2.291667F;
            this.Line184.X1 = 2.593861F;
            this.Line184.X2 = 4.885528F;
            this.Line184.Y1 = 7.764166F;
            this.Line184.Y2 = 7.764166F;
            // 
            // Line185
            // 
            this.Line185.Height = 0F;
            this.Line185.Left = 5.187611F;
            this.Line185.LineWeight = 1F;
            this.Line185.Name = "Line185";
            this.Line185.Top = 5.764166F;
            this.Line185.Visible = false;
            this.Line185.Width = 2.291666F;
            this.Line185.X1 = 5.187611F;
            this.Line185.X2 = 7.479277F;
            this.Line185.Y1 = 5.764166F;
            this.Line185.Y2 = 5.764166F;
            // 
            // Line186
            // 
            this.Line186.Height = 0F;
            this.Line186.Left = 5.187611F;
            this.Line186.LineWeight = 1F;
            this.Line186.Name = "Line186";
            this.Line186.Top = 6.014166F;
            this.Line186.Visible = false;
            this.Line186.Width = 2.291666F;
            this.Line186.X1 = 5.187611F;
            this.Line186.X2 = 7.479277F;
            this.Line186.Y1 = 6.014166F;
            this.Line186.Y2 = 6.014166F;
            // 
            // Line187
            // 
            this.Line187.Height = 0F;
            this.Line187.Left = 5.187611F;
            this.Line187.LineWeight = 1F;
            this.Line187.Name = "Line187";
            this.Line187.Top = 6.264166F;
            this.Line187.Visible = false;
            this.Line187.Width = 2.291666F;
            this.Line187.X1 = 5.187611F;
            this.Line187.X2 = 7.479277F;
            this.Line187.Y1 = 6.264166F;
            this.Line187.Y2 = 6.264166F;
            // 
            // Line188
            // 
            this.Line188.Height = 0F;
            this.Line188.Left = 5.187611F;
            this.Line188.LineWeight = 1F;
            this.Line188.Name = "Line188";
            this.Line188.Top = 6.514166F;
            this.Line188.Visible = false;
            this.Line188.Width = 2.291666F;
            this.Line188.X1 = 5.187611F;
            this.Line188.X2 = 7.479277F;
            this.Line188.Y1 = 6.514166F;
            this.Line188.Y2 = 6.514166F;
            // 
            // Line189
            // 
            this.Line189.Height = 0F;
            this.Line189.Left = 5.187611F;
            this.Line189.LineWeight = 1F;
            this.Line189.Name = "Line189";
            this.Line189.Top = 6.764166F;
            this.Line189.Visible = false;
            this.Line189.Width = 2.291666F;
            this.Line189.X1 = 5.187611F;
            this.Line189.X2 = 7.479277F;
            this.Line189.Y1 = 6.764166F;
            this.Line189.Y2 = 6.764166F;
            // 
            // Line190
            // 
            this.Line190.Height = 0F;
            this.Line190.Left = 5.187611F;
            this.Line190.LineWeight = 1F;
            this.Line190.Name = "Line190";
            this.Line190.Top = 7.014166F;
            this.Line190.Visible = false;
            this.Line190.Width = 2.291666F;
            this.Line190.X1 = 5.187611F;
            this.Line190.X2 = 7.479277F;
            this.Line190.Y1 = 7.014166F;
            this.Line190.Y2 = 7.014166F;
            // 
            // Line191
            // 
            this.Line191.Height = 0F;
            this.Line191.Left = 5.187611F;
            this.Line191.LineWeight = 1F;
            this.Line191.Name = "Line191";
            this.Line191.Top = 7.264166F;
            this.Line191.Visible = false;
            this.Line191.Width = 2.291666F;
            this.Line191.X1 = 5.187611F;
            this.Line191.X2 = 7.479277F;
            this.Line191.Y1 = 7.264166F;
            this.Line191.Y2 = 7.264166F;
            // 
            // Line192
            // 
            this.Line192.Height = 0F;
            this.Line192.Left = 5.187611F;
            this.Line192.LineWeight = 1F;
            this.Line192.Name = "Line192";
            this.Line192.Top = 7.514166F;
            this.Line192.Visible = false;
            this.Line192.Width = 2.291666F;
            this.Line192.X1 = 5.187611F;
            this.Line192.X2 = 7.479277F;
            this.Line192.Y1 = 7.514166F;
            this.Line192.Y2 = 7.514166F;
            // 
            // Line193
            // 
            this.Line193.Height = 0F;
            this.Line193.Left = 5.187611F;
            this.Line193.LineWeight = 1F;
            this.Line193.Name = "Line193";
            this.Line193.Top = 7.764166F;
            this.Line193.Visible = false;
            this.Line193.Width = 2.291666F;
            this.Line193.X1 = 5.187611F;
            this.Line193.X2 = 7.479277F;
            this.Line193.Y1 = 7.764166F;
            this.Line193.Y2 = 7.764166F;
            // 
            // txtPeriodStart2
            // 
            this.txtPeriodStart2.Height = 0.1666667F;
            this.txtPeriodStart2.Left = 1.114583F;
            this.txtPeriodStart2.Name = "txtPeriodStart2";
            this.txtPeriodStart2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left";
            this.txtPeriodStart2.Text = "99 99   9999";
            this.txtPeriodStart2.Top = 0.996F;
            this.txtPeriodStart2.Width = 1.15625F;
            // 
            // txtPeriodEnd2
            // 
            this.txtPeriodEnd2.Height = 0.1666667F;
            this.txtPeriodEnd2.Left = 2.569277F;
            this.txtPeriodEnd2.Name = "txtPeriodEnd2";
            this.txtPeriodEnd2.Style = "font-family: \\000027Courier\\000020New\\000027; font-size: 10pt; text-align: left";
            this.txtPeriodEnd2.Text = "99 99 9999";
            this.txtPeriodEnd2.Top = 0.995667F;
            this.txtPeriodEnd2.Width = 1.15625F;
            // 
            // srptC1Plain
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWithholdingAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuarterNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine3a)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine3b)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaineLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberofPayees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMagnetic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodMonthEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodDayEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodYearEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodMonthStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodDayStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodYearStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine2b)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine2c)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverpaymentCheckbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayeeStatementsSent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEnclosingAmendedw3me)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmendedReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCloseAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentSub2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWAccount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDatePaid63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode Barcode2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWithholdingAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuarterNum;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3a;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line76;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEIN;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3b;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line87;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMaineLicense;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumberofPayees;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMagnetic;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line94;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line96;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line97;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line194;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line195;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line196;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line197;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line198;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line199;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodMonthEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodDayEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodYearEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodMonthStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodDayStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodYearStart;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDay;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueYear;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line200;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line201;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2b;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2c;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverpaymentCheckbox;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayeeStatementsSent;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEnclosingAmendedw3me;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label138;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label139;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label140;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label141;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line202;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line203;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label142;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label143;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label144;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmendedReturn;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label145;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label146;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label147;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCloseAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label148;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount54;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaymentSub3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaymentSub1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaymentSub2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid51;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid54;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWAccount2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field36;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line100;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line101;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line102;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line103;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line104;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line106;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line108;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line109;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line111;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line112;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line113;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line114;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line122;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line123;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line124;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line126;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line127;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line128;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line130;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line131;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line133;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line134;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line136;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line137;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line143;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line145;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line146;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line147;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line148;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line150;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line151;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line152;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line154;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line155;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line157;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line158;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line160;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line166;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonthStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDayStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearStart;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonthEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDayEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearEnd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid21;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line167;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line168;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line169;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line170;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line171;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line172;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line173;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line174;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line175;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line99;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid55;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid56;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid57;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid58;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid59;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid60;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid61;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount63;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid63;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line176;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line177;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line178;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line179;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line180;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line181;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line182;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line183;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line184;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line185;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line186;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line187;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line188;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line189;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line190;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line191;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line192;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line193;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodStart2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriodEnd2;
    }
}
