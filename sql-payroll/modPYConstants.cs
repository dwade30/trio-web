﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class modPYConstants
	{
		//=========================================================
		public const int CNSTPAYTOTALTYPEFEDTAX = 1;
		public const int CNSTPAYTOTALTYPEFICATAX = 2;
		public const int CNSTPAYTOTALTYPEMEDICARETAX = 3;
		public const int CNSTPAYTOTALTYPESTATETAX = 4;
		public const int CNSTPAYTOTALTYPEFEDGROSS = 6;
		public const int CNSTPAYTOTALTYPEFICAGROSS = 7;
		public const int CNSTPAYTOTALTYPEMEDICAREGROSS = 8;
		public const int CNSTPAYTOTALTYPESTATEGROSS = 9;
		public const int CNSTPAYTOTALTYPETOTALGROSS = 11;
		public const int CNSTPAYTOTALTYPETOTALNET = 13;
		public const int CNSTPAYTOTALTYPEUNEMPLOYMENT = 14;
		public const int CNSTPAYTOTALTYPEMSRSGROSS = 15;
		public const int CNSTPAYTOTALTYPEEMPLOYERFICATAX = 16;
		public const int CNSTPAYTOTALTYPEEMPLOYERMEDICARETAX = 17;
	}
}
