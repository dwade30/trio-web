//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptTempCheckListingDistribution.
	/// </summary>
	public partial class rptTempCheckListingDistribution : BaseSectionReport
	{
		public rptTempCheckListingDistribution()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Distribution Record Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptTempCheckListingDistribution InstancePtr
		{
			get
			{
				return (rptTempCheckListingDistribution)Sys.GetInstance(typeof(rptTempCheckListingDistribution));
			}
		}

		protected rptTempCheckListingDistribution _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMain?.Dispose();
				rsTax?.Dispose();
				rsPayCat?.Dispose();
                rsMain = null;
                rsTax = null;
                rsPayCat = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTempCheckListingDistribution	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsMain = new clsDRWrapper();
		private clsDRWrapper rsTax = new clsDRWrapper();
		private clsDRWrapper rsPayCat = new clsDRWrapper();
		
		private double dblHours;
		private double dblGrossPay;
		private string strDistributionID = "";
		private bool boolFinished;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "CheckListingDistribution", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: txtDistribution As object	OnWrite(string)
			// vbPorter upgrade warning: txtTax As object	OnWrite(string, object)
			// vbPorter upgrade warning: txtPayRate As object	OnWrite(string)
			// vbPorter upgrade warning: txtAccount As object	OnWrite(string, object)
			// vbPorter upgrade warning: txtHours As object	OnWrite(string)
			// vbPorter upgrade warning: txtDistGrossPay As object	OnWrite(string)
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ResumeCode:
				;
				NextRecord:
				;
				if (rsMain.EndOfFile())
				{
					if (boolFinished)
					{
						eArgs.EOF = true;
						return;
					}
					else
					{
						boolFinished = true;
						goto FinalTotals;
					}
				}
				if (LineTotals.Visible)
				{
					// NEED TO S
					switch (modGlobalVariables.Statics.gintDistributionLisitingField)
					{
						case 0:
							{
								// Name
								strDistributionID = rsMain.Get_Fields_String("FirstName") + rsMain.Get_Fields_String("LastName");
								break;
							}
						case 1:
							{
								// Number
								strDistributionID = FCConvert.ToString(rsMain.Get_Fields("EmployeeNumber"));
								break;
							}
						case 2:
							{
								// Sequence
								strDistributionID = FCConvert.ToString(rsMain.Get_Fields("SeqNumber"));
								break;
							}
						case 3:
							{
								// Dept/Div
								strDistributionID = FCConvert.ToString(rsMain.Get_Fields("DeptDiv"));
								break;
							}
						case 4:
							{
								// Distribution number
								strDistributionID = FCConvert.ToString(rsMain.Get_Fields_Int32("DistPayCategory"));
								break;
							}
					}
					//end switch
					LineTotals.Visible = false;
					txtEmployee.Text = string.Empty;
					txtDateField.Text = string.Empty;
					txtPayRunID.Text = string.Empty;
					txtDistribution.Text = string.Empty;
					txtTax.Text = string.Empty;
					txtPayRate.Text = string.Empty;
					txtAccount.Text = string.Empty;
					txtHours.Text = string.Empty;
					txtDistGrossPay.Text = string.Empty;
					eArgs.EOF = false;
					return;
				}
				switch (modGlobalVariables.Statics.gintDistributionLisitingField)
				{
					case 0:
						{
							// Name
							if (strDistributionID != rsMain.Get_Fields_String("FirstName") + rsMain.Get_Fields_String("LastName"))
								goto FinalTotals;
							break;
						}
					case 1:
						{
							// Number
							if (strDistributionID != FCConvert.ToString(rsMain.Get_Fields("EmployeeNumber")))
								goto FinalTotals;
							break;
						}
					case 2:
						{
							// Sequence
							if (strDistributionID != FCConvert.ToString(rsMain.Get_Fields("SeqNumber")))
								goto FinalTotals;
							break;
						}
					case 3:
						{
							// Dept/Div
							if (strDistributionID != FCConvert.ToString(rsMain.Get_Fields("DeptDiv")))
								goto FinalTotals;
							break;
						}
					case 4:
						{
							// Distribution number
							if (strDistributionID != FCConvert.ToString(rsMain.Get_Fields_Int32("DistPayCategory")))
								goto FinalTotals;
							break;
						}
				}
				//end switch
				if (FCConvert.ToString(rsMain.Get_Fields("employeenumber")).Length < 6)
				{
					txtEmployee.Text = rsMain.Get_Fields("EmployeeNumber") + Strings.StrDup(5 - fecherFoundation.Strings.Trim(FCConvert.ToString(rsMain.Get_Fields("EmployeeNumber"))).Length, " ") + rsMain.Get_Fields_String("EmployeeName");
				}
				else
				{
					txtEmployee.Text = rsMain.Get_Fields("employeenumber") + " " + rsMain.Get_Fields("employeename");
				}
				txtDateField.Text = FCConvert.ToString(modGlobalVariables.Statics.gdatCurrentPayDate);
				txtPayRunID.Text = FCConvert.ToString(modGlobalVariables.Statics.gintCurrentPayRun);
				rsPayCat.FindFirstRecord("ID", rsMain.Get_Fields_Int32("DistPayCategory"));
				if (rsPayCat.NoMatch)
				{
					txtDistribution.Text = string.Empty;
					txtTax.Text = string.Empty;
				}
				else
				{
					txtDistribution.Text = FCConvert.ToString(rsPayCat.Get_Fields_Int32("CategoryNumber")) + " - " + rsPayCat.Get_Fields("Description");
					rsTax.FindFirstRecord("ID", rsPayCat.Get_Fields_Int32("TaxStatus"));
					if (rsTax.NoMatch)
					{
						txtTax.Text = string.Empty;
					}
					else
					{
						txtTax.Text = rsTax.Get_Fields_String("TaxStatusCode");
					}
				}
				txtPayRate.Text = Strings.Format(rsMain.Get_Fields("DistPayRate"), "#,##0.00");
				txtAccount.Text = rsMain.Get_Fields_String("DistAccountNumber");
				txtHours.Text = Strings.Format(rsMain.Get_Fields("DistHours"), "0.00");
				txtDistGrossPay.Text = Strings.Format(rsMain.Get_Fields("DistGrossPay"), "#,##0.00");
				dblHours += FCConvert.ToDouble(Strings.Format(rsMain.Get_Fields("DistHours"), "0.00"));
				dblGrossPay += FCConvert.ToDouble(Strings.Format(rsMain.Get_Fields("DistGrossPay"), "0.00"));
				//lngDistributionID = rsMain.Get_Fields("DistPayCategory");
				if (!rsMain.EndOfFile())
					rsMain.MoveNext();
				eArgs.EOF = false;
				return;
				FinalTotals:
				;
				LineTotals.Visible = true;
				txtEmployee.Text = string.Empty;
				txtDateField.Text = string.Empty;
				txtPayRunID.Text = string.Empty;
				txtDistribution.Text = string.Empty;
				txtTax.Text = string.Empty;
				txtPayRate.Text = string.Empty;
				txtAccount.Text = string.Empty;
				txtHours.Text = Strings.Format(dblHours, "0.00");
				txtDistGrossPay.Text = Strings.Format(dblGrossPay, "#,##0.00");
				dblHours = 0;
				dblGrossPay = 0;
				eArgs.EOF = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FetchData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: intPageNumber As object	OnWrite
			object intPageNumber;
			// - "AutoDim"
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			intPageNumber = 1;
			rsMain.OpenRecordset(modGlobalVariables.Statics.gstrCheckListingSQL, modGlobalVariables.DEFAULTDATABASE);
			rsPayCat.OpenRecordset("Select * from tblPayCategories", "TWPY0000.vb1");
			rsTax.OpenRecordset("Select * from tblTaxStatusCodes", "TWPY0000.vb1");
			lblSort.Text = modGlobalVariables.Statics.gstrCheckListingSort;
			if (!rsMain.EndOfFile())
			{
				switch (modGlobalVariables.Statics.gintDistributionLisitingField)
				{
					case 0:
						{
							// Name
							strDistributionID = rsMain.Get_Fields_String("FirstName") + rsMain.Get_Fields_String("LastName");
							break;
						}
					case 1:
						{
							// Number
							strDistributionID = FCConvert.ToString(rsMain.Get_Fields("EmployeeNumber"));
							break;
						}
					case 2:
						{
							// Sequence
							strDistributionID = FCConvert.ToString(rsMain.Get_Fields("SeqNumber"));
							break;
						}
					case 3:
						{
							// Dept/Div
							strDistributionID = FCConvert.ToString(rsMain.Get_Fields("DeptDiv"));
							break;
						}
					case 4:
						{
							// Distribution number
							strDistributionID = FCConvert.ToString(rsMain.Get_Fields_Int32("DistPayCategory"));
							break;
						}
				}
				//end switch
			}
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniname As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		
	}
}
