//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rpt941.
	/// </summary>
	public partial class rpt941 : BaseSectionReport
	{
		public rpt941()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "FTD FICA & WITHHOLDINGS";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt941 InstancePtr
		{
			get
			{
				return (rpt941)Sys.GetInstance(typeof(rpt941));
			}
		}

		protected rpt941 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsFTD?.Dispose();
                rsFTD = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt941	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intpage;
		private clsDRWrapper rsFTD = new clsDRWrapper();
		private int intCounter;
		private double FedSum;
		private double FicaMedSum;
		private double FedGrossSum;
		private double FicaGrossSum;
		private double MedGrossSum;
		private double StateGrossSum;
		private double StateTaxSum;
		private double total;
		private string strFieldName = "";
		// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
		int intMonth;
		int lngYear;
		DateTime datPayDate;
		// vbPorter upgrade warning: datLowDate As DateTime	OnWrite(DateTime, string)
		DateTime datLowDate;
		DateTime datHighDate;
		int intPayRunID;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
			{
				modDavesSweetCode.Statics.blnReportCompleted = true;
			}
			else
			{
				modDavesSweetCode.UpdateReportStatus("FTD941");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intpage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intpage);
			txtTime.Text = FCConvert.ToString(fecherFoundation.DateAndTime.TimeOfDay);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				double dblBox1MatchAmount;
				string strMatchList;
				strMatchList = modCoreysSweeterCode.GetBox1Matches();
				dblBox1MatchAmount = 0;
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns || intPayRunID == 0)
				{
					rsFTD.OpenRecordset("SELECT Sum(FederalTaxWH) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'", "TWPY0000.vb1");
				}
				else
				{
					rsFTD.OpenRecordset("SELECT Sum(FederalTaxWH) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID), "TWPY0000.vb1");
				}
				FedSum = Conversion.Val(rsFTD.Get_Fields_Decimal("Total"));
				txtFedWithheld.Text = Strings.Format(FedSum, "Currency");
				txtFedDue.Text = Strings.Format(FedSum, "Currency");
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns || intPayRunID == 0)
				{
					rsFTD.OpenRecordset("SELECT Sum(FICATaxWH) as TotalFICA,sum(EmployerFicaTax) as TotEmployerFica,sum(EmployerMedicareTax) as TotEmployerMedicare, Sum(MedicareTaxWH) as TotalMedicare FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'", "TWPY0000.vb1");
				}
				else
				{
					rsFTD.OpenRecordset("SELECT Sum(FICATaxWH) as TotalFICA,sum(EmployerFicaTax) as TotEmployerFica,sum(EmployerMedicareTax) as TotEmployerMedicare, Sum(MedicareTaxWH) as TotalMedicare FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID), "TWPY0000.vb1");
				}
				FicaMedSum = Conversion.Val(rsFTD.Get_Fields("TotalFICA")) + Conversion.Val(rsFTD.Get_Fields("TotalMedicare"));
				double dblEmployerFica;
				double dblEMployeeFica;
				double dblEmployeeMed;
				double dblEmployerMed;
				double dblTemp;
				dblEMployeeFica = Conversion.Val(rsFTD.Get_Fields("TotalFica"));
				dblEmployerFica = Conversion.Val(rsFTD.Get_Fields("totemployerfica"));
				dblEmployeeMed = Conversion.Val(rsFTD.Get_Fields("TotalMedicare"));
				dblEmployerMed = Conversion.Val(rsFTD.Get_Fields("totemployermedicare"));
				total = dblEMployeeFica + dblEmployerFica + (dblEmployeeMed) + dblEmployerMed;
				dblTemp = dblEMployeeFica + dblEmployeeMed;
				txtFicaMedWithheld.Text = Strings.Format(dblTemp, "Currency");
				txtFicaMedDue.Text = Strings.Format(total, "Currency");
				total = FedSum + FicaMedSum;
				txtTotalWithheld.Text = Strings.Format(total, "Currency");
				// total = FedSum + FicaMedSum * 2
				total = FedSum + dblEMployeeFica + dblEmployerFica + (dblEmployeeMed * 2);
				txtTotalDue.Text = Strings.Format(total, "Currency");
				txtDue.Text = Strings.Format(total, "Currency");
				// **************************************************************************************************
				// MATTHEW 6/14/2005 THIS WAS ADDED FOR CALL ID 68387
				// 
				// GET THE FEDERAL TAXABLE GROSS
				dblBox1MatchAmount = modCoreysSweeterCode.GetQTDBox1Matches(strMatchList, "", datHighDate, datLowDate);
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns || intPayRunID == 0)
				{
					rsFTD.OpenRecordset("SELECT Sum(FederalTaxGross) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'", "TWPY0000.vb1");
				}
				else
				{
					rsFTD.OpenRecordset("SELECT Sum(FederalTaxGross) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID), "TWPY0000.vb1");
				}
				txtFederalTaxableGross.Text = Strings.Format(Conversion.Val(rsFTD.Get_Fields_Decimal("Total")) + dblBox1MatchAmount, "Currency");
				// GET THE FICA TAXABLE GROSS
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns || intPayRunID == 0)
				{
					rsFTD.OpenRecordset("SELECT Sum(FICATaxGross) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'", "TWPY0000.vb1");
				}
				else
				{
					rsFTD.OpenRecordset("SELECT Sum(FICATaxGross) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID), "TWPY0000.vb1");
				}
				txtFICATaxableGross.Text = Strings.Format(rsFTD.Get_Fields_Decimal("Total"), "Currency");
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns || intPayRunID == 0)
				{
					rsFTD.OpenRecordset("SELECT Sum(MedicareTaxGross) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'", "TWPY0000.vb1");
				}
				else
				{
					rsFTD.OpenRecordset("SELECT Sum(MedicareTaxGross) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID), "TWPY0000.vb1");
				}
				txtMedicareTaxableGross.Text = Strings.Format(rsFTD.Get_Fields_Decimal("Total"), "Currency");
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns || intPayRunID == 0)
				{
					rsFTD.OpenRecordset("SELECT Sum(StateTaxGross) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'", "TWPY0000.vb1");
				}
				else
				{
					rsFTD.OpenRecordset("SELECT Sum(StateTaxGross) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID), "TWPY0000.vb1");
				}
				txtStateTaxableGross.Text = Strings.Format(Conversion.Val(rsFTD.Get_Fields_Decimal("Total")) + dblBox1MatchAmount, "Currency");
				if (modGlobalConstants.Statics.gboolPrintALLPayRuns || intPayRunID == 0)
				{
					rsFTD.OpenRecordset("SELECT Sum(StateTaxWH) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "'", "TWPY0000.vb1");
				}
				else
				{
					rsFTD.OpenRecordset("SELECT Sum(StateTaxWH) as Total FROM tblCheckDetail WHERE (checkvoid = 0) and TotalRecord = 1 AND PayDate >= '" + FCConvert.ToString(datLowDate) + "' AND PayDate <= '" + FCConvert.ToString(datHighDate) + "' AND PayRunID = " + FCConvert.ToString(intPayRunID), "TWPY0000.vb1");
				}
				txtStateTaxWH.Text = Strings.Format(rsFTD.Get_Fields_Decimal("Total"), "Currency");
				// **************************************************************************************************
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void Init(List<string> batchReports = null)
		{
			intMonth = 0;
			lngYear = 0;
			rsFTD.OpenRecordset("SELECT FTD941 from tblStandardLimits", "TWPY0000.vb1");
			if (FCConvert.ToString(rsFTD.Get_Fields("FTD941")) == "Weekly")
			{
				lblMQY.Text = "WEEKLY";
				if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
				{
					datLowDate = DateTime.Today;
					modGlobalVariables.Statics.gboolCancelSelected = false;
					frmSelectDateInfo.InstancePtr.Init2("Please select the pay date you wish to have reported for.", -1, -1, -1, ref datLowDate, ref intPayRunID, false);
					if (modGlobalVariables.Statics.gboolCancelSelected)
					{
						if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
						{
							modDavesSweetCode.Statics.blnReportCompleted = true;
						}
						this.Close();
						return;
					}
					if (datLowDate.ToOADate() != 0)
					{
						if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
						{
							lblDate.Text = "Pay Date: " + Strings.Format(datLowDate, "MM/dd/yyyy") + "  (ALL Pay Runs)";
						}
						else
						{
							lblDate.Text = "Pay Date: " + Strings.Format(datLowDate, "MM/dd/yyyy");
						}
						lngYear = datLowDate.Year;
						datHighDate = datLowDate;
					}
					else
					{
						if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
						{
							modDavesSweetCode.Statics.blnReportCompleted = true;
						}
						this.Close();
						return;
					}
				}
				else
				{
					datPayDate = modGlobalVariables.Statics.gdatCurrentPayDate;
					lngYear = datPayDate.Year;
					datLowDate = datPayDate;
					datHighDate = datPayDate;
					intPayRunID = modGlobalVariables.Statics.gintCurrentPayRun;
					if (modGlobalConstants.Statics.gboolPrintALLPayRuns)
					{
						lblDate.Text = "Pay Date: " + Strings.Format(datLowDate, "MM/dd/yyyy") + "  (ALL Pay Runs)";
					}
					else
					{
						lblDate.Text = "Pay Date: " + Strings.Format(datLowDate, "MM/dd/yyyy");
					}
				}
			}
			else
			{
				lblMQY.Text = "MONTHLY";
				if (modGlobalVariables.Statics.gstrMQYProcessing == "NONE")
				{
					//FC:FINAL:DSE:#i2421 Use date parameter by reference
					DateTime tmpArg = DateTime.FromOADate(0);
					frmSelectDateInfo.InstancePtr.Init5("Please select the month and year to report on.", ref lngYear, -1, ref intMonth, ref tmpArg, -1, false);
					if (modGlobalVariables.Statics.gboolCancelSelected)
					{
						if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
						{
							modDavesSweetCode.Statics.blnReportCompleted = true;
						}
						this.Close();
						return;
					}
					if (intMonth != -1)
					{
						datLowDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
						datHighDate = modDavesSweetCode.FindLastDay(fecherFoundation.Strings.Trim(datLowDate.ToString()));
						lblDate.Text = "Month: " + modBudgetaryAccounting.MonthCalc(intMonth) + " " + FCConvert.ToString(lngYear);
					}
					else
					{
						if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
						{
							modDavesSweetCode.Statics.blnReportCompleted = true;
						}
						this.Close();
						return;
					}
				}
				else
				{
					intMonth = modGlobalVariables.Statics.gdatCurrentPayDate.Month;
					lngYear = modGlobalVariables.Statics.gdatCurrentPayDate.Year;
					datLowDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
					datHighDate = modDavesSweetCode.FindLastDay(fecherFoundation.Strings.Trim(datLowDate.ToString()));
					lblDate.Text = "Month: " + modBudgetaryAccounting.MonthCalc(intMonth) + " " + FCConvert.ToString(lngYear);
				}
			}
			if (lngYear != -1)
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet)
				{
					this.Document.Printer.PrinterName = modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName;
					modDuplexPrinting.DuplexPrintReport(this, modGlobalVariables.Statics.gtypeFullSetReports.strFullSetName, batchReports: batchReports);
					// rpt941.PrintReport False
				}
				else
				{
					// frmReportViewer.Init Me
					modCoreysSweeterCode.CheckDefaultPrint(this, boolAllowEmail: true, strAttachmentName: "Fed941");
				}
			}
			else
			{
				if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == true)
				{
					modDavesSweetCode.Statics.blnReportCompleted = true;
				}
				this.Close();
			}
		}

		
	}
}
