﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptPayTotalsDetail.
	/// </summary>
	partial class rptPayTotalsDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPayTotalsDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedicare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederalWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICAWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateWage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalGrossPay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtFedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICATotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtFedWageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateWageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICAWageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedWageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalGrossPayTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPaidTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalGrossPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedWageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalGrossPayTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPaidTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployee,
            this.txtFederal,
            this.txtFICA,
            this.txtMedicare,
            this.txtState,
            this.txtFederalWage,
            this.txtFICAWage,
            this.txtMedWage,
            this.txtStateWage,
            this.txtTotalGrossPay,
            this.txtTotalPaid});
			this.Detail.Height = 0.2708333F;
			this.Detail.Name = "Detail";
			// 
			// txtEmployee
			// 
			this.txtEmployee.Height = 0.2083333F;
			this.txtEmployee.Left = 0.03125F;
			this.txtEmployee.Name = "txtEmployee";
			this.txtEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEmployee.Text = "Employee";
			this.txtEmployee.Top = 0.04166667F;
			this.txtEmployee.Width = 1.8125F;
			// 
			// txtFederal
			// 
			this.txtFederal.Height = 0.2083333F;
			this.txtFederal.Left = 1.90625F;
			this.txtFederal.Name = "txtFederal";
			this.txtFederal.OutputFormat = resources.GetString("txtFederal.OutputFormat");
			this.txtFederal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFederal.Text = "Federal";
			this.txtFederal.Top = 0.04166667F;
			this.txtFederal.Width = 0.6875F;
			// 
			// txtFICA
			// 
			this.txtFICA.Height = 0.2083333F;
			this.txtFICA.Left = 2.59375F;
			this.txtFICA.Name = "txtFICA";
			this.txtFICA.OutputFormat = resources.GetString("txtFICA.OutputFormat");
			this.txtFICA.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFICA.Text = "FICA";
			this.txtFICA.Top = 0.04166667F;
			this.txtFICA.Width = 0.65625F;
			// 
			// txtMedicare
			// 
			this.txtMedicare.Height = 0.2083333F;
			this.txtMedicare.Left = 3.25F;
			this.txtMedicare.Name = "txtMedicare";
			this.txtMedicare.OutputFormat = resources.GetString("txtMedicare.OutputFormat");
			this.txtMedicare.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedicare.Text = "Medicare";
			this.txtMedicare.Top = 0.04166667F;
			this.txtMedicare.Width = 0.6875F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.2083333F;
			this.txtState.Left = 3.9375F;
			this.txtState.Name = "txtState";
			this.txtState.OutputFormat = resources.GetString("txtState.OutputFormat");
			this.txtState.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtState.Text = "State";
			this.txtState.Top = 0.04166667F;
			this.txtState.Width = 0.6875F;
			// 
			// txtFederalWage
			// 
			this.txtFederalWage.Height = 0.2083333F;
			this.txtFederalWage.Left = 4.625F;
			this.txtFederalWage.Name = "txtFederalWage";
			this.txtFederalWage.OutputFormat = resources.GetString("txtFederalWage.OutputFormat");
			this.txtFederalWage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFederalWage.Text = "Federal";
			this.txtFederalWage.Top = 0.04166667F;
			this.txtFederalWage.Width = 0.6875F;
			// 
			// txtFICAWage
			// 
			this.txtFICAWage.Height = 0.2083333F;
			this.txtFICAWage.Left = 5.3125F;
			this.txtFICAWage.Name = "txtFICAWage";
			this.txtFICAWage.OutputFormat = resources.GetString("txtFICAWage.OutputFormat");
			this.txtFICAWage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFICAWage.Text = "FICA";
			this.txtFICAWage.Top = 0.04166667F;
			this.txtFICAWage.Width = 0.65625F;
			// 
			// txtMedWage
			// 
			this.txtMedWage.Height = 0.2083333F;
			this.txtMedWage.Left = 5.9375F;
			this.txtMedWage.Name = "txtMedWage";
			this.txtMedWage.OutputFormat = resources.GetString("txtMedWage.OutputFormat");
			this.txtMedWage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedWage.Text = "Medicare";
			this.txtMedWage.Top = 0.04166667F;
			this.txtMedWage.Width = 0.65625F;
			// 
			// txtStateWage
			// 
			this.txtStateWage.Height = 0.2083333F;
			this.txtStateWage.Left = 6.59375F;
			this.txtStateWage.Name = "txtStateWage";
			this.txtStateWage.OutputFormat = resources.GetString("txtStateWage.OutputFormat");
			this.txtStateWage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtStateWage.Text = "State";
			this.txtStateWage.Top = 0.04166667F;
			this.txtStateWage.Width = 0.65625F;
			// 
			// txtTotalGrossPay
			// 
			this.txtTotalGrossPay.Height = 0.2083333F;
			this.txtTotalGrossPay.Left = 7.3125F;
			this.txtTotalGrossPay.Name = "txtTotalGrossPay";
			this.txtTotalGrossPay.OutputFormat = resources.GetString("txtTotalGrossPay.OutputFormat");
			this.txtTotalGrossPay.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalGrossPay.Text = "TotalGrossPay";
			this.txtTotalGrossPay.Top = 0.04166667F;
			this.txtTotalGrossPay.Width = 0.96875F;
			// 
			// txtTotalPaid
			// 
			this.txtTotalPaid.Height = 0.2083333F;
			this.txtTotalPaid.Left = 9.0625F;
			this.txtTotalPaid.Name = "txtTotalPaid";
			this.txtTotalPaid.OutputFormat = resources.GetString("txtTotalPaid.OutputFormat");
			this.txtTotalPaid.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalPaid.Text = "TotalPaid";
			this.txtTotalPaid.Top = 0.04166667F;
			this.txtTotalPaid.Width = 0.875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFedTotal,
            this.txtStateTotal,
            this.txtFICATotal,
            this.txtMedTotal,
            this.Field23,
            this.Line2,
            this.txtFedWageTotal,
            this.txtStateWageTotal,
            this.txtFICAWageTotal,
            this.txtMedWageTotal,
            this.txtTotalGrossPayTotal,
            this.txtTotalPaidTotal});
			this.ReportFooter.Height = 0.7708333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// txtFedTotal
			// 
			this.txtFedTotal.Height = 0.2083333F;
			this.txtFedTotal.Left = 1.875F;
			this.txtFedTotal.Name = "txtFedTotal";
			this.txtFedTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFedTotal.Text = null;
			this.txtFedTotal.Top = 0.1666667F;
			this.txtFedTotal.Width = 0.75F;
			// 
			// txtStateTotal
			// 
			this.txtStateTotal.Height = 0.2083333F;
			this.txtStateTotal.Left = 3.875F;
			this.txtStateTotal.Name = "txtStateTotal";
			this.txtStateTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtStateTotal.Text = null;
			this.txtStateTotal.Top = 0.4166667F;
			this.txtStateTotal.Width = 0.8125F;
			// 
			// txtFICATotal
			// 
			this.txtFICATotal.Height = 0.2083333F;
			this.txtFICATotal.Left = 2.5F;
			this.txtFICATotal.Name = "txtFICATotal";
			this.txtFICATotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFICATotal.Text = null;
			this.txtFICATotal.Top = 0.4166667F;
			this.txtFICATotal.Width = 0.8125F;
			// 
			// txtMedTotal
			// 
			this.txtMedTotal.Height = 0.2083333F;
			this.txtMedTotal.Left = 3.15625F;
			this.txtMedTotal.Name = "txtMedTotal";
			this.txtMedTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedTotal.Text = null;
			this.txtMedTotal.Top = 0.1666667F;
			this.txtMedTotal.Width = 0.8125F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1666667F;
			this.Field23.Left = 0.0625F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field23.Text = "Totals:";
			this.Field23.Top = 0.2083333F;
			this.Field23.Width = 0.59375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.1875F;
			this.Line2.LineWeight = 2F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.1666667F;
			this.Line2.Width = 8.75F;
			this.Line2.X1 = 1.1875F;
			this.Line2.X2 = 9.9375F;
			this.Line2.Y1 = 0.1666667F;
			this.Line2.Y2 = 0.1666667F;
			// 
			// txtFedWageTotal
			// 
			this.txtFedWageTotal.Height = 0.2083333F;
			this.txtFedWageTotal.Left = 4.59375F;
			this.txtFedWageTotal.Name = "txtFedWageTotal";
			this.txtFedWageTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFedWageTotal.Text = null;
			this.txtFedWageTotal.Top = 0.1666667F;
			this.txtFedWageTotal.Width = 0.78125F;
			// 
			// txtStateWageTotal
			// 
			this.txtStateWageTotal.Height = 0.2083333F;
			this.txtStateWageTotal.Left = 6.5F;
			this.txtStateWageTotal.Name = "txtStateWageTotal";
			this.txtStateWageTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtStateWageTotal.Text = null;
			this.txtStateWageTotal.Top = 0.4166667F;
			this.txtStateWageTotal.Width = 0.75F;
			// 
			// txtFICAWageTotal
			// 
			this.txtFICAWageTotal.Height = 0.2083333F;
			this.txtFICAWageTotal.Left = 5.15625F;
			this.txtFICAWageTotal.Name = "txtFICAWageTotal";
			this.txtFICAWageTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtFICAWageTotal.Text = null;
			this.txtFICAWageTotal.Top = 0.4166667F;
			this.txtFICAWageTotal.Width = 0.78125F;
			// 
			// txtMedWageTotal
			// 
			this.txtMedWageTotal.Height = 0.2083333F;
			this.txtMedWageTotal.Left = 5.75F;
			this.txtMedWageTotal.Name = "txtMedWageTotal";
			this.txtMedWageTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtMedWageTotal.Text = null;
			this.txtMedWageTotal.Top = 0.1666667F;
			this.txtMedWageTotal.Width = 0.84375F;
			// 
			// txtTotalGrossPayTotal
			// 
			this.txtTotalGrossPayTotal.Height = 0.2083333F;
			this.txtTotalGrossPayTotal.Left = 7.28125F;
			this.txtTotalGrossPayTotal.Name = "txtTotalGrossPayTotal";
			this.txtTotalGrossPayTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalGrossPayTotal.Text = null;
			this.txtTotalGrossPayTotal.Top = 0.1666667F;
			this.txtTotalGrossPayTotal.Width = 1F;
			// 
			// txtTotalPaidTotal
			// 
			this.txtTotalPaidTotal.Height = 0.2083333F;
			this.txtTotalPaidTotal.Left = 8.96875F;
			this.txtTotalPaidTotal.Name = "txtTotalPaidTotal";
			this.txtTotalPaidTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalPaidTotal.Text = null;
			this.txtTotalPaidTotal.Top = 0.1666667F;
			this.txtTotalPaidTotal.Width = 0.96875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.Field3,
            this.Field4,
            this.Field5,
            this.Field17,
            this.Field18,
            this.Line1,
            this.txtTitle,
            this.txtDate,
            this.txtPage,
            this.txtMuniName,
            this.txtTime,
            this.txtPayDate,
            this.Field26,
            this.Field27,
            this.Field28,
            this.Field29,
            this.Field30,
            this.Field32,
            this.Field34});
			this.PageHeader.Height = 1.041667F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 0.09375F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field1.Text = "Employee";
			this.Field1.Top = 0.8125F;
			this.Field1.Width = 0.6875F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.2083333F;
			this.Field3.Left = 2.03125F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field3.Text = "Federal";
			this.Field3.Top = 0.7916667F;
			this.Field3.Width = 0.5625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.2083333F;
			this.Field4.Left = 2.6875F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field4.Text = "FICA";
			this.Field4.Top = 0.7916667F;
			this.Field4.Width = 0.5625F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.2083333F;
			this.Field5.Left = 3.375F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field5.Text = "Medicare";
			this.Field5.Top = 0.7916667F;
			this.Field5.Width = 0.5625F;
			// 
			// Field17
			// 
			this.Field17.CanGrow = false;
			this.Field17.Height = 0.2083333F;
			this.Field17.Left = 2.03125F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field17.Text = "- - -  - - - - - -   T   A   X   E   S  - - -  - - - - - -";
			this.Field17.Top = 0.5833333F;
			this.Field17.Width = 2.59375F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.2083333F;
			this.Field18.Left = 4.0625F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field18.Text = "State";
			this.Field18.Top = 0.7916667F;
			this.Field18.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 2F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.03125F;
			this.Line1.Width = 9.90625F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 9.96875F;
			this.Line1.Y1 = 1.03125F;
			this.Line1.Y2 = 1.03125F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.2083333F;
			this.txtTitle.Left = 0.0625F;
			this.txtTitle.MultiLine = false;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Title";
			this.txtTitle.Top = 0.04166667F;
			this.txtTitle.Width = 9.78125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 7.6875F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 2.1875F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.2083333F;
			this.txtPage.Left = 8.75F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = "Page";
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.MultiLine = false;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.0625F;
			// 
			// txtPayDate
			// 
			this.txtPayDate.Height = 0.2083333F;
			this.txtPayDate.Left = 0.0625F;
			this.txtPayDate.MultiLine = false;
			this.txtPayDate.Name = "txtPayDate";
			this.txtPayDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtPayDate.Text = "Title";
			this.txtPayDate.Top = 0.25F;
			this.txtPayDate.Width = 9.78125F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.2083333F;
			this.Field26.Left = 4.78125F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field26.Text = "Federal";
			this.Field26.Top = 0.7916667F;
			this.Field26.Width = 0.5625F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.2083333F;
			this.Field27.Left = 5.375F;
			this.Field27.Name = "Field27";
			this.Field27.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field27.Text = "FICA";
			this.Field27.Top = 0.7916667F;
			this.Field27.Width = 0.5625F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.2083333F;
			this.Field28.Left = 6.03125F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field28.Text = "Medicare";
			this.Field28.Top = 0.7916667F;
			this.Field28.Width = 0.5625F;
			// 
			// Field29
			// 
			this.Field29.CanGrow = false;
			this.Field29.Height = 0.2083333F;
			this.Field29.Left = 4.84375F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field29.Text = "- - - - - - - - - - -   W A G E S  - - -  - - - - - - - -";
			this.Field29.Top = 0.5833333F;
			this.Field29.Width = 2.40625F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.2083333F;
			this.Field30.Left = 6.6875F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field30.Text = "State";
			this.Field30.Top = 0.7916667F;
			this.Field30.Width = 0.5625F;
			// 
			// Field32
			// 
			this.Field32.Height = 0.2083333F;
			this.Field32.Left = 7.28125F;
			this.Field32.Name = "Field32";
			this.Field32.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field32.Text = "Total Gross Pay";
			this.Field32.Top = 0.7916667F;
			this.Field32.Width = 1F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.2083333F;
			this.Field34.Left = 9.0625F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Field34.Text = "Total Paid";
			this.Field34.Top = 0.7916667F;
			this.Field34.Width = 0.875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptPayTotalsDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 8.5F;
			this.PageSettings.PaperWidth = 11F;
			this.PrintWidth = 9.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalGrossPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFedWageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalGrossPayTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPaidTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedicare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateWage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalGrossPay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPaid;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFedWageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateWageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAWageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedWageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalGrossPayTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPaidTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
