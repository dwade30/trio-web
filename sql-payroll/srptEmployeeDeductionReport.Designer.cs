﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for srptEmployeeDeductionReport.
	/// </summary>
	partial class srptEmployeeDeductionReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptEmployeeDeductionReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtEmployerMatchTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDeductionHeading = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOrder = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmployeeNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumEmployees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDedCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDedMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDedQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDedFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDedCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDedLTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumberOfEmployees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalMTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalQTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalFiscal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCalendar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalLTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerMatchTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductionHeading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOrder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLast)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumEmployees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedLTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumberOfEmployees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalMTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalQTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFiscal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEmployeeNumber,
				this.txtName,
				this.txtCurrent,
				this.txtMTD,
				this.txtQTD,
				this.txtFiscal,
				this.txtCalendar,
				this.txtLTD,
				this.txtLast
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			//
			// 
			this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.txtNumberOfEmployees,
				this.txtTotalCurrent,
				this.txtTotalMTD,
				this.txtTotalQTD,
				this.txtTotalFiscal,
				this.txtTotalCalendar,
				this.txtTotalLTD,
				this.Line4
			});
			this.ReportFooter.Height = 0.4479167F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line2,
				this.Line3,
				this.txtEmployerMatchTitle,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.txtDeductionHeading,
				this.Label9,
				this.lblOrder
			});
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Height = 0.7291667F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtNumEmployees,
				this.txtDedCurrent,
				this.txtDedMTD,
				this.txtDedQTD,
				this.txtDedFiscal,
				this.txtDedCalendar,
				this.txtDedLTD,
				this.Line5
			});
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.5F;
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Tag = "headerfields";
			this.Line2.Top = 0.09375F;
			this.Line2.Visible = false;
			this.Line2.Width = 2F;
			this.Line2.X1 = 0.5F;
			this.Line2.X2 = 2.5F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.875F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Tag = "headerfields";
			this.Line3.Top = 0.09375F;
			this.Line3.Visible = false;
			this.Line3.Width = 2F;
			this.Line3.X1 = 4.875F;
			this.Line3.X2 = 6.875F;
			this.Line3.Y1 = 0.09375F;
			this.Line3.Y2 = 0.09375F;
			// 
			// txtEmployerMatchTitle
			// 
			this.txtEmployerMatchTitle.Height = 0.19F;
			this.txtEmployerMatchTitle.Left = 2.5625F;
			this.txtEmployerMatchTitle.Name = "txtEmployerMatchTitle";
			this.txtEmployerMatchTitle.Style = "font-weight: bold; text-align: center";
			this.txtEmployerMatchTitle.Tag = "headerfields";
			this.txtEmployerMatchTitle.Text = "Employer Match ";
			this.txtEmployerMatchTitle.Top = 0F;
			this.txtEmployerMatchTitle.Visible = false;
			this.txtEmployerMatchTitle.Width = 2.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.06666667F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 9pt";
			this.Label1.Tag = "headerfields";
			this.Label1.Text = "Employee";
			this.Label1.Top = 0.5F;
			this.Label1.Width = 0.8F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.5F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 9pt; text-align: right";
			this.Label2.Tag = "headerfields";
			this.Label2.Text = "Current";
			this.Label2.Top = 0.5F;
			this.Label2.Width = 0.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.25F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 9pt; text-align: right";
			this.Label3.Tag = "headerfields";
			this.Label3.Text = "MTD";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 0.75F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 9pt; text-align: right";
			this.Label4.Tag = "headerfields";
			this.Label4.Text = "QTD";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 0.75F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.75F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 9pt; text-align: right";
			this.Label5.Tag = "headerfields";
			this.Label5.Text = "Fiscal";
			this.Label5.Top = 0.5F;
			this.Label5.Width = 0.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.5F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 9pt; text-align: right";
			this.Label6.Tag = "headerfields";
			this.Label6.Text = "Calendar";
			this.Label6.Top = 0.5F;
			this.Label6.Width = 0.8125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.3125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 9pt; text-align: right";
			this.Label7.Tag = "headerfields";
			this.Label7.Text = "LTD";
			this.Label7.Top = 0.5F;
			this.Label7.Width = 0.75F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 7.0625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 9pt; text-align: center";
			this.Label8.Tag = "headerfields";
			this.Label8.Text = "Limit";
			this.Label8.Top = 0.5F;
			this.Label8.Width = 0.4375F;
			// 
			// txtDeductionHeading
			// 
			this.txtDeductionHeading.Height = 0.1666667F;
			this.txtDeductionHeading.Left = 2.566667F;
			this.txtDeductionHeading.Name = "txtDeductionHeading";
			this.txtDeductionHeading.Style = "font-weight: bold; text-align: center";
			this.txtDeductionHeading.Tag = "headerfields";
			this.txtDeductionHeading.Text = null;
			this.txtDeductionHeading.Top = 0.2F;
			this.txtDeductionHeading.Width = 2.233333F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 7.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 9pt; text-align: center";
			this.Label9.Tag = "headerfields";
			this.Label9.Text = "Hit";
			this.Label9.Top = 0.3125F;
			this.Label9.Width = 0.4375F;
			// 
			// lblOrder
			// 
			this.lblOrder.Height = 0.1666667F;
			this.lblOrder.HyperLink = null;
			this.lblOrder.Left = 0.03125F;
			this.lblOrder.Name = "lblOrder";
			this.lblOrder.Style = "font-size: 9pt";
			this.lblOrder.Tag = "headerfields";
			this.lblOrder.Text = "Employee";
			this.lblOrder.Top = 0.5F;
			this.lblOrder.Visible = false;
			this.lblOrder.Width = 0.625F;
			// 
			// txtEmployeeNumber
			// 
			this.txtEmployeeNumber.Height = 0.1666667F;
			this.txtEmployeeNumber.Left = 0F;
			this.txtEmployeeNumber.Name = "txtEmployeeNumber";
			this.txtEmployeeNumber.Style = "font-size: 9pt";
			this.txtEmployeeNumber.Text = null;
			this.txtEmployeeNumber.Top = 0F;
			this.txtEmployeeNumber.Visible = false;
			this.txtEmployeeNumber.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 0.03333334F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 9pt";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.4F;
			// 
			// txtCurrent
			// 
			this.txtCurrent.Height = 0.19F;
			this.txtCurrent.Left = 2.5F;
			this.txtCurrent.Name = "txtCurrent";
			this.txtCurrent.Style = "font-size: 9pt; text-align: right";
			this.txtCurrent.Text = null;
			this.txtCurrent.Top = 0F;
			this.txtCurrent.Width = 0.75F;
			// 
			// txtMTD
			// 
			this.txtMTD.Height = 0.19F;
			this.txtMTD.Left = 3.25F;
			this.txtMTD.Name = "txtMTD";
			this.txtMTD.Style = "font-size: 9pt; text-align: right";
			this.txtMTD.Text = null;
			this.txtMTD.Top = 0F;
			this.txtMTD.Width = 0.75F;
			// 
			// txtQTD
			// 
			this.txtQTD.Height = 0.19F;
			this.txtQTD.Left = 4F;
			this.txtQTD.Name = "txtQTD";
			this.txtQTD.Style = "font-size: 9pt; text-align: right";
			this.txtQTD.Text = null;
			this.txtQTD.Top = 0F;
			this.txtQTD.Width = 0.75F;
			// 
			// txtFiscal
			// 
			this.txtFiscal.Height = 0.19F;
			this.txtFiscal.Left = 4.75F;
			this.txtFiscal.Name = "txtFiscal";
			this.txtFiscal.Style = "font-size: 9pt; text-align: right";
			this.txtFiscal.Text = null;
			this.txtFiscal.Top = 0F;
			this.txtFiscal.Width = 0.75F;
			// 
			// txtCalendar
			// 
			this.txtCalendar.Height = 0.19F;
			this.txtCalendar.Left = 5.5F;
			this.txtCalendar.Name = "txtCalendar";
			this.txtCalendar.Style = "font-size: 9pt; text-align: right";
			this.txtCalendar.Text = null;
			this.txtCalendar.Top = 0F;
			this.txtCalendar.Width = 0.8125F;
			// 
			// txtLTD
			// 
			this.txtLTD.Height = 0.19F;
			this.txtLTD.Left = 6.3125F;
			this.txtLTD.Name = "txtLTD";
			this.txtLTD.Style = "font-size: 9pt; text-align: right";
			this.txtLTD.Text = null;
			this.txtLTD.Top = 0F;
			this.txtLTD.Width = 0.75F;
			// 
			// txtLast
			// 
			this.txtLast.Height = 0.19F;
			this.txtLast.Left = 7.125F;
			this.txtLast.Name = "txtLast";
			this.txtLast.Style = "font-size: 9pt; text-align: center";
			this.txtLast.Text = null;
			this.txtLast.Top = 0F;
			this.txtLast.Width = 0.3125F;
			// 
			// txtNumEmployees
			// 
			this.txtNumEmployees.Height = 0.125F;
			this.txtNumEmployees.Left = 0.71875F;
			this.txtNumEmployees.Name = "txtNumEmployees";
			this.txtNumEmployees.Style = "font-size: 9pt";
			this.txtNumEmployees.Text = null;
			this.txtNumEmployees.Top = 0.08333334F;
			this.txtNumEmployees.Width = 1.71875F;
			// 
			// txtDedCurrent
			// 
			this.txtDedCurrent.Height = 0.19F;
			this.txtDedCurrent.Left = 2.5F;
			this.txtDedCurrent.Name = "txtDedCurrent";
			this.txtDedCurrent.Style = "font-size: 9pt; text-align: right";
			this.txtDedCurrent.Text = null;
			this.txtDedCurrent.Top = 0.0625F;
			this.txtDedCurrent.Width = 0.75F;
			// 
			// txtDedMTD
			// 
			this.txtDedMTD.Height = 0.19F;
			this.txtDedMTD.Left = 3.25F;
			this.txtDedMTD.Name = "txtDedMTD";
			this.txtDedMTD.Style = "font-size: 9pt; text-align: right";
			this.txtDedMTD.Text = null;
			this.txtDedMTD.Top = 0.0625F;
			this.txtDedMTD.Width = 0.75F;
			// 
			// txtDedQTD
			// 
			this.txtDedQTD.Height = 0.19F;
			this.txtDedQTD.Left = 4F;
			this.txtDedQTD.Name = "txtDedQTD";
			this.txtDedQTD.Style = "font-size: 9pt; text-align: right";
			this.txtDedQTD.Text = null;
			this.txtDedQTD.Top = 0.0625F;
			this.txtDedQTD.Width = 0.75F;
			// 
			// txtDedFiscal
			// 
			this.txtDedFiscal.Height = 0.19F;
			this.txtDedFiscal.Left = 4.75F;
			this.txtDedFiscal.Name = "txtDedFiscal";
			this.txtDedFiscal.Style = "font-size: 9pt; text-align: right";
			this.txtDedFiscal.Text = null;
			this.txtDedFiscal.Top = 0.0625F;
			this.txtDedFiscal.Width = 0.75F;
			// 
			// txtDedCalendar
			// 
			this.txtDedCalendar.Height = 0.19F;
			this.txtDedCalendar.Left = 5.5F;
			this.txtDedCalendar.Name = "txtDedCalendar";
			this.txtDedCalendar.Style = "font-size: 9pt; text-align: right";
			this.txtDedCalendar.Text = null;
			this.txtDedCalendar.Top = 0.0625F;
			this.txtDedCalendar.Width = 0.8125F;
			// 
			// txtDedLTD
			// 
			this.txtDedLTD.Height = 0.19F;
			this.txtDedLTD.Left = 6.3125F;
			this.txtDedLTD.Name = "txtDedLTD";
			this.txtDedLTD.Style = "font-size: 9pt; text-align: right";
			this.txtDedLTD.Text = null;
			this.txtDedLTD.Top = 0.0625F;
			this.txtDedLTD.Width = 0.75F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.03125F;
			this.Line5.Width = 7.4375F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.4375F;
			this.Line5.Y1 = 0.03125F;
			this.Line5.Y2 = 0.03125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.19F;
			this.Field1.Left = 0F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 9pt";
			this.Field1.Text = "Totals";
			this.Field1.Top = 0.25F;
			this.Field1.Width = 0.5F;
			// 
			// txtNumberOfEmployees
			// 
			this.txtNumberOfEmployees.Height = 0.1666667F;
			this.txtNumberOfEmployees.Left = 0.71875F;
			this.txtNumberOfEmployees.Name = "txtNumberOfEmployees";
			this.txtNumberOfEmployees.Style = "font-size: 9pt";
			this.txtNumberOfEmployees.Text = null;
			this.txtNumberOfEmployees.Top = 0.25F;
			this.txtNumberOfEmployees.Width = 1.71875F;
			// 
			// txtTotalCurrent
			// 
			this.txtTotalCurrent.Height = 0.19F;
			this.txtTotalCurrent.Left = 2.5F;
			this.txtTotalCurrent.Name = "txtTotalCurrent";
			this.txtTotalCurrent.Style = "font-size: 9pt; text-align: right";
			this.txtTotalCurrent.Text = null;
			this.txtTotalCurrent.Top = 0.25F;
			this.txtTotalCurrent.Width = 0.75F;
			// 
			// txtTotalMTD
			// 
			this.txtTotalMTD.Height = 0.19F;
			this.txtTotalMTD.Left = 3.25F;
			this.txtTotalMTD.Name = "txtTotalMTD";
			this.txtTotalMTD.Style = "font-size: 9pt; text-align: right";
			this.txtTotalMTD.Text = null;
			this.txtTotalMTD.Top = 0.25F;
			this.txtTotalMTD.Width = 0.75F;
			// 
			// txtTotalQTD
			// 
			this.txtTotalQTD.Height = 0.19F;
			this.txtTotalQTD.Left = 4F;
			this.txtTotalQTD.Name = "txtTotalQTD";
			this.txtTotalQTD.Style = "font-size: 9pt; text-align: right";
			this.txtTotalQTD.Text = null;
			this.txtTotalQTD.Top = 0.25F;
			this.txtTotalQTD.Width = 0.75F;
			// 
			// txtTotalFiscal
			// 
			this.txtTotalFiscal.Height = 0.19F;
			this.txtTotalFiscal.Left = 4.75F;
			this.txtTotalFiscal.Name = "txtTotalFiscal";
			this.txtTotalFiscal.Style = "font-size: 9pt; text-align: right";
			this.txtTotalFiscal.Text = null;
			this.txtTotalFiscal.Top = 0.25F;
			this.txtTotalFiscal.Width = 0.75F;
			// 
			// txtTotalCalendar
			// 
			this.txtTotalCalendar.Height = 0.19F;
			this.txtTotalCalendar.Left = 5.5F;
			this.txtTotalCalendar.Name = "txtTotalCalendar";
			this.txtTotalCalendar.Style = "font-size: 9pt; text-align: right";
			this.txtTotalCalendar.Text = null;
			this.txtTotalCalendar.Top = 0.25F;
			this.txtTotalCalendar.Width = 0.8125F;
			// 
			// txtTotalLTD
			// 
			this.txtTotalLTD.Height = 0.19F;
			this.txtTotalLTD.Left = 6.3125F;
			this.txtTotalLTD.Name = "txtTotalLTD";
			this.txtTotalLTD.Style = "font-size: 9pt; text-align: right";
			this.txtTotalLTD.Text = null;
			this.txtTotalLTD.Top = 0.25F;
			this.txtTotalLTD.Width = 0.75F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 2F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.21875F;
			this.Line4.Width = 7.4375F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.4375F;
			this.Line4.Y1 = 0.21875F;
			this.Line4.Y2 = 0.21875F;
			// 
			// srptEmployeeDeductionReport
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtEmployerMatchTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeductionHeading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOrder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeeNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLast)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumEmployees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDedLTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumberOfEmployees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalMTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalQTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFiscal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeeNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLast;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumberOfEmployees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLTD;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployerMatchTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeductionHeading;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOrder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumEmployees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedMTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedQTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedFiscal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedCalendar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDedLTD;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
	}
}
