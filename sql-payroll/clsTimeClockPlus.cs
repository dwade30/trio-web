//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;


namespace TWPY0000
{
	public class clsTimeClockPlus
	{
		//=========================================================
		private FCCollection rList = new FCCollection();
		private int intCurrentIndex;
		private string strMsg = "";
		private string strNewLine = string.Empty;

		public string Messages
		{
			get
			{
				string Messages = "";
				Messages = strMsg;
				return Messages;
			}
		}

		private void ClearList()
		{
			if (!(rList == null))
			{
				foreach (clsTimeClockPlusImportRecord tRec in rList)
				{
					rList.Remove(1);
				}
				// tRec
			}
		}

		public int MovePrevious()
		{
			int MovePrevious = 0;
			int intReturn;
			intReturn = -1;
			MovePrevious = -1;
			if (intCurrentIndex == -1)
				return MovePrevious;
			if (!FCUtils.IsEmpty(rList))
			{
				if (intCurrentIndex < 1)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex > 0)
					{
						intCurrentIndex -= 1;
						if (intCurrentIndex < 1)
						{
							intCurrentIndex = -1;
							break;
						}
						else if (rList[intCurrentIndex] == null)
						{
							// ElseIf rList(intCurrentIndex).Unused Then
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MovePrevious = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MovePrevious = -1;
			}
			return MovePrevious;
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(rList))
			{
				if (intCurrentIndex > rList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= rList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > rList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (rList[intCurrentIndex] == null)
						{
							// ElseIf rList[intCurrentIndex].Unused Then
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public void MoveFirst()
		{
			if (!(rList == null))
			{
				if (!FCUtils.IsEmpty(rList))
				{
					if (rList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentIndex;
				return GetCurrentIndex;
			}
		}

		public clsTimeClockPlusImportRecord GetCurrentRecord()
		{
			clsTimeClockPlusImportRecord GetCurrentRecord = null;
			clsTimeClockPlusImportRecord tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(rList))
			{
				if (intCurrentIndex > 0)
				{
					if (!(rList[intCurrentIndex] == null))
					{
						// If Not rList[intCurrentIndex].Unused Then
						tRec = rList[intCurrentIndex];
						// End If
					}
				}
			}
			GetCurrentRecord = tRec;
			return GetCurrentRecord;
		}

		public clsTimeClockPlusImportRecord GetRecordByIndex(ref int intIndex)
		{
			clsTimeClockPlusImportRecord GetRecordByIndex = null;
			clsTimeClockPlusImportRecord tRec;
			tRec = null;
			if (!FCUtils.IsEmpty(rList) && intIndex > 0)
			{
				if (!(rList[intIndex] == null))
				{
					// If Not rList[intindex].Unused Then
					intCurrentIndex = intIndex;
					tRec = rList[intIndex];
					// End If
				}
			}
			GetRecordByIndex = tRec;
			return GetRecordByIndex;
		}

		public int AddRecord(ref string strEmpNum, ref string strCustomEmpNum, ref string strExport, ref string strLast, ref string strSSN, ref string strDept, ref bool boolSalary, ref string strJobCode, ref string strPayType, ref string strExpense, ref string strJobDesc, ref double dblPayrate, ref double dblHours)
		{
			int AddRecord = 0;
			clsTimeClockPlusImportRecord tRec = new clsTimeClockPlusImportRecord();
			tRec.CustomEmployeeNumber = strCustomEmpNum;
			tRec.Department = strDept;
			tRec.EmployeeNum = strEmpNum;
			tRec.ExpenseCode = strExpense;
			tRec.ExportCode = strExport;
			tRec.Hours = dblHours;
			tRec.JobCode = strJobCode;
			tRec.JobCodeDescription = strJobDesc;
			tRec.LastName = strLast;
			tRec.PayRate = dblPayrate;
			tRec.PayType = strPayType;
			tRec.Salary = boolSalary;
			tRec.SSN = strSSN;
			AddRecord = InsertRecord(ref tRec);
			return AddRecord;
		}

		public int InsertRecord(ref clsTimeClockPlusImportRecord tRec)
		{
			int InsertRecord = 0;
			int intReturn;
			rList.Add(tRec);
			intCurrentIndex = rList.Count;
			intReturn = rList.Count;
			InsertRecord = intReturn;
			return InsertRecord;
		}

		private bool SaveDistListToDB(ref clsDistList DistList)
		{
			bool SaveDistListToDB = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDistributionLine CurDist;
				rsSave.OpenRecordset("select * from tblpayrolldistribution where employeenumber = '" + DistList.EmployeeNumber + "'", "twpy0000.vb1");
				if (!(DistList == null))
				{
					if (DistList.MoveFirst())
					{
						do
						{
							CurDist = DistList.GetCurrentDist();
							if (rsSave.FindFirstRecord("ID", CurDist.ID))
							{
								rsSave.Edit();
								rsSave.Set_Fields("gross", CurDist.Gross);
								rsSave.Set_Fields("hoursweek", CurDist.HoursWeek);
								rsSave.Update();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("AccountCode", CurDist.AccountCode);
								if (fecherFoundation.Strings.Trim(CurDist.AccountNumber) != "")
								{
									rsSave.Set_Fields("AccountNumber", CurDist.AccountNumber);
								}
								rsSave.Set_Fields("BaseRate", CurDist.BaseRate);
								rsSave.Set_Fields("CD", CurDist.CD);
								rsSave.Set_Fields("contractid", CurDist.ContractID);
								rsSave.Set_Fields("defaulthours", CurDist.DefaultHours);
								rsSave.Set_Fields("distu", CurDist.DistU);
								rsSave.Set_Fields("employeenumber", CurDist.EmployeeNumber);
								rsSave.Set_Fields("factor", CurDist.Factor);
								rsSave.Set_Fields("grantfunded", CurDist.GrantFunded);
								rsSave.Set_Fields("gross", CurDist.Gross);
								rsSave.Set_Fields("hoursweek", CurDist.HoursWeek);
								rsSave.Set_Fields("msrs", CurDist.MSRS);
								rsSave.Set_Fields("msrsid", CurDist.MSRSID);
								rsSave.Set_Fields("numberweeks", CurDist.NumberWeeks);
								rsSave.Set_Fields("CAT", CurDist.PayCategory);
								rsSave.Set_Fields("project", CurDist.Project);
								rsSave.Set_Fields("recordnumber", CurDist.RecordNumber);
								rsSave.Set_Fields("statuscode", CurDist.StatusCode);
								rsSave.Set_Fields("taxcode", CurDist.TaxCode);
								rsSave.Set_Fields("wc", CurDist.WC);
								rsSave.Set_Fields("weekstaxwithheld", CurDist.WeeksTaxWithheld);
								rsSave.Set_Fields("workcomp", CurDist.WorkComp);
								rsSave.Update();
								CurDist.ID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
							}
						}
						while (DistList.MoveNext() > 0);
					}
					SaveDistListToDB = true;
				}
				return SaveDistListToDB;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "Trying to save distribution line in SaveDistToDB", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveDistListToDB;
		}

		public bool LoadFile(string strFile)
		{
			bool LoadFile = false;
			bool boolFileOpen = false;
			FCFileSystem fso = new FCFileSystem();
			string strPath;
			string strFileName;
			StreamReader ts = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strRecord = "";
				clsTimeClockPlusImportRecord tRec;
				int intCurrPos = 0;
				// vbPorter upgrade warning: intFirstPos As int	OnWriteFCConvert.ToInt32(
				int intFirstPos = 0;
				// vbPorter upgrade warning: intSecondPos As int	OnWriteFCConvert.ToInt32(
				int intSecondPos = 0;
				// vbPorter upgrade warning: intpos As int	OnWriteFCConvert.ToInt32(
				int intpos = 0;
				string[] strDataArray = null;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				LoadFile = false;
				strPath = Path.GetDirectoryName(strFile);
				strFileName = Path.GetFileName(strFile);
				if (FCFileSystem.FileExists(strFile))
				{
					ts = FCFileSystem.OpenText(strFile);
					boolFileOpen = true;
				}
				else
				{
					MessageBox.Show("Could not find file " + strFile, "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return LoadFile;
				}
				ClearList();
				while (!ts.EndOfStream)
				{
					strRecord = ts.ReadLine();
					strRecord = Strings.Replace(strRecord, ",", "^", 1, -1, CompareConstants.vbTextCompare);
					intCurrPos = 1;
					// now return to commas if between quotes
					while (intCurrPos < strRecord.Length)
					{
						intFirstPos = Strings.InStr(intCurrPos, strRecord, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbTextCompare);
						if (intFirstPos < 1)
							break;
						intSecondPos = Strings.InStr(intFirstPos + 1, strRecord, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbTextCompare);
						intpos = Strings.InStr(intFirstPos, strRecord, "^", CompareConstants.vbTextCompare);
						while (intpos > 0 && intpos < intSecondPos)
						{
							Strings.MidSet(ref strRecord, intpos, 1, ",");
							intpos = Strings.InStr(intpos, strRecord, "^", CompareConstants.vbTextCompare);
						}
						intCurrPos = intSecondPos + 1;
					}
					// now get rid of the quotes
					strRecord = Strings.Replace(strRecord, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
					// now split it
					strDataArray = Strings.Split(strRecord, "^", -1, CompareConstants.vbTextCompare);
					tRec = new clsTimeClockPlusImportRecord();
					for (x = 0; x <= (Information.UBound(strDataArray, 1)); x++)
					{
						switch (x)
						{
							case 0:
								{
									tRec.EmployeeNum = strDataArray[x];
									break;
								}
							case 1:
								{
									tRec.CustomEmployeeNumber = strDataArray[x];
									break;
								}
							case 2:
								{
									tRec.ExportCode = strDataArray[x];
									break;
								}
							case 3:
								{
									tRec.LastName = strDataArray[x];
									break;
								}
							case 4:
								{
									tRec.SSN = strDataArray[x];
									break;
								}
							case 5:
								{
									tRec.Department = strDataArray[x];
									break;
								}
							case 6:
								{
									if (strDataArray[x] == "1" || fecherFoundation.Strings.LCase(strDataArray[x]) == "y")
									{
										tRec.Salary = true;
									}
									else
									{
										tRec.Salary = false;
									}
									break;
								}
							case 7:
								{
									tRec.JobCode = strDataArray[x];
									break;
								}
							case 8:
								{
									tRec.PayType = strDataArray[x];
									break;
								}
							case 9:
								{
									tRec.ExpenseCode = strDataArray[x];
									break;
								}
							case 10:
								{
									tRec.JobCodeDescription = strDataArray[x];
									break;
								}
							case 11:
								{
									tRec.PayRate = Conversion.Val(strDataArray[x]);
									break;
								}
							case 12:
								{
									tRec.Hours = Conversion.Val(strDataArray[x]);
									break;
								}
						}
						//end switch
					}
					// x
					InsertRecord(ref tRec);
				}
				ts.Close();
				boolFileOpen = false;
				LoadFile = true;
				return LoadFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolFileOpen)
					ts.Close();
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadFile;
		}

		public bool ProcessImport()
		{
			bool ProcessImport = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsTimeClockPlusImportRecord tRec = new clsTimeClockPlusImportRecord();
				clsDistList DistList = new clsDistList();
				clsDistributionLine CurDist;
				clsDRWrapper rsSave = new clsDRWrapper();
				string strCurrentEmployee;
				clsDRWrapper rsPayCat = new clsDRWrapper();
				string strAccount = "";
				string defaultWorkComp = "";
				string defaultMSRS = "";
				string defaultStatus = "";
				string defaultWC = "";
				double dblBaseRate = 0;
				double dblBaseRateToUse = 0;
				string defaultDistU = "";
				int lngLastNum = 0;
				clsTimeClockPlusToPayCategory tCat = new clsTimeClockPlusToPayCategory();
				int lngCat = 0;
				double dblPaid = 0;
				clsDRWrapper rsLoad = new clsDRWrapper();
				bool boolEmployeeFound = false;
				int lngCurrentEmployee = 0;

				strMsg = "";
				strNewLine = "";
				MoveFirst();
				MovePrevious();
				strCurrentEmployee = "";
				frmWait.InstancePtr.Init("Importing Employee ");
				rsLoad.OpenRecordset("select employeenumber, Convert(int,employeenumber) as empnum from tblemployeemaster", "twpy0000.vb1");
				rsPayCat.OpenRecordset("select * from tblpaycategories order by ID", "twpy0000.vb1");
				while (MoveNext() > 0)
				{
					//App.DoEvents();
					tRec = GetCurrentRecord();
					if (!(tRec == null))
					{
						if (lngCurrentEmployee != tRec.CustomEmployeeNumber.ToIntegerValue())
						{
							// new employee
							if (lngCurrentEmployee != 0)
							{
								// save
								if (DistList.MoveFirst())
								{
									SaveDistListToDB(ref DistList);
								}
							}
							lngCurrentEmployee = tRec.CustomEmployeeNumber.ToIntegerValue();
							frmWait.InstancePtr.lblMessage.Text = "Importing Employee " + strCurrentEmployee;
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            //App.DoEvents();
                            if (rsLoad.FindFirst("empnum = " + lngCurrentEmployee))
							{
								boolEmployeeFound = true;
								strCurrentEmployee = rsLoad.Get_Fields_String("employeenumber");
							}
							else
							{
								boolEmployeeFound = false;
								strMsg += strNewLine + "Employee number " + lngCurrentEmployee + " not found.";
								strNewLine = "\r\n";
							}
							if (boolEmployeeFound)
							{
								rsSave.Execute("update tblpayrolldistribution set hoursweek = 0,GROSS = 0 where employeenumber = '" + strCurrentEmployee + "'", "twpy0000.vb1");
								DistList.LoadEmployee(strCurrentEmployee);
								lngLastNum = 0;
								if (DistList.MoveFirst())
								{
									DistList.MoveFirst();
									if (DistList.IsCurrent())
									{
										CurDist = DistList.GetCurrentDist();
										defaultWorkComp = CurDist.WorkComp;
										defaultMSRS = CurDist.MSRS;
										defaultStatus = CurDist.StatusCode;
										defaultWC = CurDist.WC;
										dblBaseRate = CurDist.BaseRate;
										defaultDistU = CurDist.DistU;
										DistList.MoveLast();
										CurDist = DistList.GetCurrentDist();
										lngLastNum = CurDist.RecordNumber;
									}
									else
									{
										lngLastNum = 0;
									}
								}
							}
						}
						if (boolEmployeeFound)
						{
							if (tRec.Hours > 0)
							{
								lngCat = tCat.GetPayrollCategory(tRec.PayType);
								if (lngCat > 0)
								{
									strAccount = tRec.ExpenseCode;
									if (fecherFoundation.Strings.Trim(strAccount) == "")
									{
										// first occurrence
										strAccount = "";
										dblPaid = tRec.Hours;
										dblBaseRateToUse = dblBaseRate;

										if (!SaveToDist(ref rsPayCat, ref DistList, dblPaid, ref lngCat, strAccount, strCurrentEmployee, ref defaultWorkComp, ref defaultMSRS, ref defaultStatus, ref defaultWC, ref dblBaseRateToUse, ref defaultDistU, ref lngLastNum))
										{
											frmWait.InstancePtr.Unload();
											MessageBox.Show("Error while updating employee '" + strCurrentEmployee + "'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
											return ProcessImport;
										}
									}
									else
									{
										// specific account
										dblPaid = tRec.Hours;
										dblBaseRateToUse = dblBaseRate;
										if (!SaveToDist(ref rsPayCat, ref DistList, dblPaid, ref lngCat, strAccount, strCurrentEmployee, ref defaultWorkComp, ref defaultMSRS, ref defaultStatus, ref defaultWC, ref dblBaseRateToUse, ref defaultDistU, ref lngLastNum))
										{
											frmWait.InstancePtr.Unload();
											MessageBox.Show("Error while updating employee '" + strCurrentEmployee + "'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
											return ProcessImport;
										}
									}
								}
							}
						}
					}
				}
				if (DistList.MoveFirst())
				{
					SaveDistListToDB(ref DistList);
				}
				frmWait.InstancePtr.Unload();
				ProcessImport = true;
				if (strMsg != "")
				{
					// "Created new distribution line for employee " & strEmployeeNumber & " pay category " & rsPayCat.Fields("description") & vbNewLine & "Check to make sure all entries for this line are correct", vbExclamation, "New Line"
					strMsg = "Check to make sure all entries for the following line(s) are correct and any errors are corrected" + "\r\n" + "\r\n" + strMsg;
				}
				return ProcessImport;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ProcessImport", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ProcessImport;
		}

		private bool SaveToDist(ref clsDRWrapper rsPayCat, ref clsDistList DistList, double dblPaid, ref int lngCat, string strAccount, string strEmployeeNumber, ref string defaultWorkComp, ref string defaultMSRS, ref string defaultStatus, ref string defaultWC, ref double dblBaseRate, ref string defaultDistU, ref int lngLastNum)
		{
			bool SaveToDist = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveToDist = false;
				bool boolFound;
				bool boolSoFar = false;
				string dAccount = "";
				clsDistributionLine CurDist = new clsDistributionLine();
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				boolFound = false;
				if (DistList.FindFirstCat(lngCat))
				{
					if (fecherFoundation.Strings.Trim(strAccount) == "")
					{
						boolFound = true;
						CurDist = DistList.GetCurrentDist();
					}
					else
					{
						boolSoFar = false;
						// dAccount = Trim(rsSave.Fields("accountnumber"))
						dAccount = fecherFoundation.Strings.Trim(DistList.GetCurrentDist().AccountNumber);
						CurDist = DistList.GetCurrentDist();
						if (dAccount.Length > 0)
						{
							if (fecherFoundation.Strings.UCase(Strings.Left(strAccount, 1)) == fecherFoundation.Strings.UCase(Strings.Left(dAccount, 1)))
							{
								boolSoFar = true;
								for (x = 1; x <= (strAccount.Length); x++)
								{
									if (dAccount.Length >= x)
									{
										if (fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != fecherFoundation.Strings.UCase(Strings.Mid(dAccount, x, 1)) && fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != "X")
										{
											boolSoFar = false;
											break;
										}
									}
									else
									{
										boolSoFar = false;
										break;
									}
								}
								// x
							}
							// if first letter matches
						}
						// if len(daccount) > 0
						if (boolSoFar)
						{
							boolFound = true;
						}
						else
						{
							while (DistList.FindNextCat(lngCat) && !boolFound)
							{
								CurDist = DistList.GetCurrentDist();
								dAccount = fecherFoundation.Strings.Trim(CurDist.AccountNumber);
								if (dAccount.Length > 0)
								{
									boolSoFar = true;
									// If UCase(Left(strAccount, 1)) = UCase(Left(dAccount, 1)) Then
									for (x = 1; x <= (strAccount.Length); x++)
									{
										if (dAccount.Length >= x)
										{
											if (fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != fecherFoundation.Strings.UCase(Strings.Mid(dAccount, x, 1)) && fecherFoundation.Strings.UCase(Strings.Mid(strAccount, x, 1)) != "X")
											{
												boolSoFar = false;
												break;
											}
										}
										else
										{
											boolSoFar = false;
											break;
										}
									}
									// x
									if (boolSoFar)
									{
										boolFound = true;
										break;
									}
									// End If
								}
							}
						}
						// End If
						// Else
						// If CurDist.AccountNumberMatch <> "" And CurDist.AccountNumberMatch = strAccount Then
						// boolFound = True
						// Else
						// Do While DistList.FindNextCat(lngCat) And Not boolFound
						// Set CurDist = DistList.GetCurrentDist
						// If CurDist.AccountNumberMatch = strAccount Then
						// boolFound = True
						// Exit Do
						// End If
						// Loop
						// End If
						// End If 'if first letter matches
						// End If 'if len daccount > 0
					}
					// if straccount <> ""
				}
				// if find first cat
				if (boolFound)
				{
					CurDist.HoursWeek += dblPaid;
					CurDist.Gross = FCConvert.ToDouble(Strings.Format(CurDist.HoursWeek * Conversion.Val(CurDist.Factor) * Conversion.Val(CurDist.BaseRate), "0.00"));
				}
				else
				{
					// must make new one
					lngLastNum += 1;
					CurDist = DistList.AddNew(lngLastNum);
					CurDist.PayCategory = lngCat;
					if (rsPayCat.FindFirstRecord("ID", lngCat))
					{
						CurDist.AccountNumberMatch = strAccount;
						CurDist.AccountNumber = strAccount;
						CurDist.Factor = Conversion.Val(rsPayCat.Get_Fields("multi"));
						CurDist.TaxCode = rsPayCat.Get_Fields("taxstatus");
						CurDist.WC = FCConvert.ToString(rsPayCat.Get_Fields_Boolean("workerscomp"));
						CurDist.AccountCode = 2;
						CurDist.CD = 1;
						CurDist.NumberWeeks = 0;
						CurDist.DefaultHours = 0;
						CurDist.StatusCode = "";
						CurDist.GrantFunded = FCConvert.ToString(0);
						CurDist.MSRSID = -3;
						CurDist.Project = 0;
						CurDist.WeeksTaxWithheld = 0;
						CurDist.ContractID = 0;
						CurDist.WorkComp = defaultWorkComp;
						CurDist.MSRS = defaultMSRS;
						CurDist.StatusCode = defaultStatus;
						CurDist.WC = defaultWC;
						CurDist.BaseRate = dblBaseRate;
						CurDist.DistU = defaultDistU;
						CurDist.HoursWeek = dblPaid;
						CurDist.Gross = FCConvert.ToDouble(Strings.Format(dblPaid * Conversion.Val(CurDist.Factor) * Conversion.Val(CurDist.BaseRate), "0.00"));
						if (fecherFoundation.Strings.UCase(rsPayCat.Get_Fields("type")) == "DOLLARS")
						{
							CurDist.BaseRate = 1;
							CurDist.Gross = dblPaid;
						}
						else if (fecherFoundation.Strings.UCase(rsPayCat.Get_Fields("type")) == "NON-PAY (HOURS)")
						{
							CurDist.BaseRate = 0;
							CurDist.Gross = 0;
						}
						frmWait.InstancePtr.Hide();
						// MsgBox "Created new distribution line for employee " & strEmployeeNumber & " pay category " & rsPayCat.Fields("description") & vbNewLine & "Check to make sure all entries for this line are correct", vbExclamation, "New Line"
						strMsg += strNewLine + "Created new distribution line for employee " + strEmployeeNumber + " pay category " + rsPayCat.Get_Fields_String("description");
						strNewLine = "\r\n";
					}
					frmWait.InstancePtr.Show();
					// rsSave.Update
				}
				SaveToDist = true;
				return SaveToDist;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveToDist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveToDist;
		}
	}
}
