﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDataEntryForms.
	/// </summary>
	partial class rptDataEntryForms
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDataEntryForms));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPeriods = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label911 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label911)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmployee,
            this.txtHours,
            this.txtRate,
            this.txtPeriods,
            this.Shape2,
            this.Shape3,
            this.Shape4,
            this.Shape5,
            this.Shape6,
            this.Shape7,
            this.Shape8,
            this.Shape9,
            this.Shape10,
            this.Shape11,
            this.Shape12,
            this.Shape13,
            this.Shape14,
            this.Shape15,
            this.Shape16,
            this.Shape17,
            this.Shape18,
            this.Shape19,
            this.Shape20,
            this.Shape21,
            this.Shape22});
            this.Detail.Height = 0.5F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // txtEmployee
            // 
            this.txtEmployee.Height = 0.1875F;
            this.txtEmployee.Left = 0.125F;
            this.txtEmployee.Name = "txtEmployee";
            this.txtEmployee.Style = "font-family: \'Arial\'; font-size: 9pt";
            this.txtEmployee.Text = null;
            this.txtEmployee.Top = 0.125F;
            this.txtEmployee.Width = 1.5625F;
            // 
            // txtHours
            // 
            this.txtHours.Height = 0.1875F;
            this.txtHours.Left = 1.8125F;
            this.txtHours.Name = "txtHours";
            this.txtHours.Style = "font-family: \'Arial\'; font-size: 9pt; text-align: right";
            this.txtHours.Text = null;
            this.txtHours.Top = 0.0625F;
            this.txtHours.Width = 0.75F;
            // 
            // txtRate
            // 
            this.txtRate.Height = 0.1875F;
            this.txtRate.Left = 1.8125F;
            this.txtRate.Name = "txtRate";
            this.txtRate.Style = "font-family: \'Arial\'; font-size: 9pt; text-align: right";
            this.txtRate.Text = null;
            this.txtRate.Top = 0.25F;
            this.txtRate.Width = 0.75F;
            // 
            // txtPeriods
            // 
            this.txtPeriods.Height = 0.1875F;
            this.txtPeriods.Left = 2.75F;
            this.txtPeriods.Name = "txtPeriods";
            this.txtPeriods.Style = "font-family: \'Arial\'; font-size: 9pt";
            this.txtPeriods.Text = null;
            this.txtPeriods.Top = 0.0625F;
            this.txtPeriods.Width = 0.375F;
            // 
            // Shape2
            // 
            this.Shape2.Height = 0.5F;
            this.Shape2.Left = 3.34375F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape2.Top = 0F;
            this.Shape2.Width = 0.3125F;
            // 
            // Shape3
            // 
            this.Shape3.Height = 0.5F;
            this.Shape3.Left = 3.65625F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape3.Top = 0F;
            this.Shape3.Width = 0.3125F;
            // 
            // Shape4
            // 
            this.Shape4.Height = 0.5F;
            this.Shape4.Left = 3.96875F;
            this.Shape4.Name = "Shape4";
            this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape4.Top = 0F;
            this.Shape4.Width = 0.3125F;
            // 
            // Shape5
            // 
            this.Shape5.Height = 0.5F;
            this.Shape5.Left = 4.28125F;
            this.Shape5.Name = "Shape5";
            this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape5.Top = 0F;
            this.Shape5.Width = 0.3125F;
            // 
            // Shape6
            // 
            this.Shape6.Height = 0.5F;
            this.Shape6.Left = 4.59375F;
            this.Shape6.Name = "Shape6";
            this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape6.Top = 0F;
            this.Shape6.Width = 0.3125F;
            // 
            // Shape7
            // 
            this.Shape7.Height = 0.5F;
            this.Shape7.Left = 4.90625F;
            this.Shape7.Name = "Shape7";
            this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape7.Top = 0F;
            this.Shape7.Width = 0.3125F;
            // 
            // Shape8
            // 
            this.Shape8.Height = 0.5F;
            this.Shape8.Left = 5.21875F;
            this.Shape8.Name = "Shape8";
            this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape8.Top = 0F;
            this.Shape8.Width = 0.3125F;
            // 
            // Shape9
            // 
            this.Shape9.Height = 0.5F;
            this.Shape9.Left = 5.53125F;
            this.Shape9.Name = "Shape9";
            this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape9.Top = 0F;
            this.Shape9.Width = 0.3125F;
            // 
            // Shape10
            // 
            this.Shape10.Height = 0.5F;
            this.Shape10.Left = 5.84375F;
            this.Shape10.Name = "Shape10";
            this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape10.Top = 0F;
            this.Shape10.Width = 0.3125F;
            // 
            // Shape11
            // 
            this.Shape11.Height = 0.5F;
            this.Shape11.Left = 6.15625F;
            this.Shape11.Name = "Shape11";
            this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape11.Top = 0F;
            this.Shape11.Width = 0.3125F;
            // 
            // Shape12
            // 
            this.Shape12.Height = 0.5F;
            this.Shape12.Left = 6.46875F;
            this.Shape12.Name = "Shape12";
            this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape12.Top = 0F;
            this.Shape12.Width = 0.3125F;
            // 
            // Shape13
            // 
            this.Shape13.Height = 0.5F;
            this.Shape13.Left = 6.78125F;
            this.Shape13.Name = "Shape13";
            this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape13.Top = 0F;
            this.Shape13.Width = 0.3125F;
            // 
            // Shape14
            // 
            this.Shape14.Height = 0.5F;
            this.Shape14.Left = 7.09375F;
            this.Shape14.Name = "Shape14";
            this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape14.Top = 0F;
            this.Shape14.Width = 0.3125F;
            // 
            // Shape15
            // 
            this.Shape15.Height = 0.5F;
            this.Shape15.Left = 7.40625F;
            this.Shape15.Name = "Shape15";
            this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape15.Top = 0F;
            this.Shape15.Width = 0.3125F;
            // 
            // Shape16
            // 
            this.Shape16.Height = 0.5F;
            this.Shape16.Left = 7.71875F;
            this.Shape16.Name = "Shape16";
            this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape16.Top = 0F;
            this.Shape16.Width = 0.3125F;
            // 
            // Shape17
            // 
            this.Shape17.Height = 0.5F;
            this.Shape17.Left = 8.03125F;
            this.Shape17.Name = "Shape17";
            this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape17.Top = 0F;
            this.Shape17.Width = 0.3125F;
            // 
            // Shape18
            // 
            this.Shape18.Height = 0.5F;
            this.Shape18.Left = 8.34375F;
            this.Shape18.Name = "Shape18";
            this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape18.Top = 0F;
            this.Shape18.Width = 0.3125F;
            // 
            // Shape19
            // 
            this.Shape19.Height = 0.5F;
            this.Shape19.Left = 8.65625F;
            this.Shape19.Name = "Shape19";
            this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape19.Top = 0F;
            this.Shape19.Width = 0.3125F;
            // 
            // Shape20
            // 
            this.Shape20.Height = 0.5F;
            this.Shape20.Left = 8.96875F;
            this.Shape20.Name = "Shape20";
            this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape20.Top = 0F;
            this.Shape20.Width = 0.3125F;
            // 
            // Shape21
            // 
            this.Shape21.Height = 0.5F;
            this.Shape21.Left = 9.28125F;
            this.Shape21.Name = "Shape21";
            this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape21.Top = 0F;
            this.Shape21.Width = 0.3125F;
            // 
            // Shape22
            // 
            this.Shape22.Height = 0.5F;
            this.Shape22.Left = 9.59375F;
            this.Shape22.Name = "Shape22";
            this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape22.Top = 0F;
            this.Shape22.Width = 0.3125F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape1,
            this.Label911,
            this.Label2,
            this.Label3,
            this.Label9,
            this.Label1,
            this.txtMuniName,
            this.txtDate,
            this.txtPage,
            this.txtTime,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Label23,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29});
            this.PageHeader.Height = 1.90625F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Shape1
            // 
            this.Shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Shape1.Height = 1.375F;
            this.Shape1.Left = 0.125F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape1.Top = 0.53125F;
            this.Shape1.Width = 9.78125F;
            // 
            // Label911
            // 
            this.Label911.Height = 0.1875F;
            this.Label911.HyperLink = null;
            this.Label911.Left = 0.1875F;
            this.Label911.Name = "Label911";
            this.Label911.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label911.Text = "Employee ----------------------------";
            this.Label911.Top = 1.71875F;
            this.Label911.Width = 1.75F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.3125F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 1.9375F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label2.Text = "Sch Hrs Pay Rate";
            this.Label2.Top = 1.59375F;
            this.Label2.Width = 0.5625F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.3125F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 2.5625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label3.Text = "Periods  Tax/Ded";
            this.Label3.Top = 1.59375F;
            this.Label3.Width = 0.625F;
            // 
            // Label9
            // 
            this.Label9.Height = 1.413F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 3.364F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label9.Text = "Regular";
            this.Label9.Top = 0.493F;
            this.Label9.Width = 0.313F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.0625F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "background-color: rgb(255,255,255); font-weight: bold; text-align: center";
            this.Label1.Text = "Data Entry Form";
            this.Label1.Top = 0.0625F;
            this.Label1.Width = 9.8125F;
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.1875F;
            this.txtMuniName.Left = 0.125F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtMuniName.Text = null;
            this.txtMuniName.Top = 0.0625F;
            this.txtMuniName.Width = 1.4375F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 8.46875F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.0625F;
            this.txtDate.Width = 1.4375F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.Left = 8.46875F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.25F;
            this.txtPage.Width = 1.4375F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.Left = 0.125F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.25F;
            this.txtTime.Width = 1.4375F;
            // 
            // Label10
            // 
            this.Label10.Height = 1.413F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 3.6765F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label10.Text = "Regular";
            this.Label10.Top = 0.493F;
            this.Label10.Width = 0.313F;
            // 
            // Label11
            // 
            this.Label11.Height = 1.413F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 3.989F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label11.Text = "Regular";
            this.Label11.Top = 0.493F;
            this.Label11.Width = 0.313F;
            // 
            // Label12
            // 
            this.Label12.Height = 1.413F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 4.3015F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label12.Text = "Regular";
            this.Label12.Top = 0.493F;
            this.Label12.Width = 0.313F;
            // 
            // Label13
            // 
            this.Label13.Height = 1.413F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 4.614F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label13.Text = "Regular";
            this.Label13.Top = 0.493F;
            this.Label13.Width = 0.313F;
            // 
            // Label14
            // 
            this.Label14.Height = 1.413F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 4.9265F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label14.Text = "Regular";
            this.Label14.Top = 0.493F;
            this.Label14.Width = 0.313F;
            // 
            // Label15
            // 
            this.Label15.Height = 1.413F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 5.239F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label15.Text = "Regular";
            this.Label15.Top = 0.493F;
            this.Label15.Width = 0.313F;
            // 
            // Label16
            // 
            this.Label16.Height = 1.413F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 5.5515F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label16.Text = "Regular";
            this.Label16.Top = 0.493F;
            this.Label16.Width = 0.313F;
            // 
            // Label17
            // 
            this.Label17.Height = 1.413F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 5.864F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label17.Text = "Regular";
            this.Label17.Top = 0.493F;
            this.Label17.Width = 0.313F;
            // 
            // Label18
            // 
            this.Label18.Height = 1.413F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 6.1765F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label18.Text = "Regular";
            this.Label18.Top = 0.493F;
            this.Label18.Width = 0.313F;
            // 
            // Label19
            // 
            this.Label19.Height = 1.413F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 6.489F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label19.Text = "Regular";
            this.Label19.Top = 0.493F;
            this.Label19.Width = 0.313F;
            // 
            // Label20
            // 
            this.Label20.Height = 1.413F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 6.8015F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label20.Text = "Regular";
            this.Label20.Top = 0.493F;
            this.Label20.Width = 0.313F;
            // 
            // Label21
            // 
            this.Label21.Height = 1.413F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 7.114F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label21.Text = "Regular";
            this.Label21.Top = 0.493F;
            this.Label21.Width = 0.313F;
            // 
            // Label22
            // 
            this.Label22.Height = 1.413F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 7.4265F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label22.Text = "Regular";
            this.Label22.Top = 0.493F;
            this.Label22.Width = 0.313F;
            // 
            // Label23
            // 
            this.Label23.Height = 1.413F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 7.739F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label23.Text = "Regular";
            this.Label23.Top = 0.493F;
            this.Label23.Width = 0.313F;
            // 
            // Label24
            // 
            this.Label24.Height = 1.413F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 8.0515F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label24.Text = "Regular";
            this.Label24.Top = 0.493F;
            this.Label24.Width = 0.313F;
            // 
            // Label25
            // 
            this.Label25.Height = 1.413F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 8.364F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; text-justify: auto; vertical-align: bottom; ddo-font-vertical: true";
            this.Label25.Text = "Regular";
            this.Label25.Top = 0.493F;
            this.Label25.Width = 0.313F;
            // 
            // Label26
            // 
            this.Label26.Height = 1.413F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 8.6765F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label26.Text = "Regular";
            this.Label26.Top = 0.493F;
            this.Label26.Width = 0.313F;
            // 
            // Label27
            // 
            this.Label27.Height = 1.413F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 8.989F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label27.Text = "Regular";
            this.Label27.Top = 0.493F;
            this.Label27.Width = 0.313F;
            // 
            // Label28
            // 
            this.Label28.Height = 1.413F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 9.3015F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label28.Text = "Regular";
            this.Label28.Top = 0.493F;
            this.Label28.Width = 0.313F;
            // 
            // Label29
            // 
            this.Label29.Height = 1.413F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 9.614F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \'Microsoft Sans Serif\'; font-size: 8.5pt; font-weight: bold; text-al" +
    "ign: right; vertical-align: bottom; ddo-font-vertical: true";
            this.Label29.Text = "Regular";
            this.Label29.Top = 0.493F;
            this.Label29.Width = 0.313F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.01041667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptDataEntryForms
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 9.927083F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label911)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHours;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriods;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label911;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
