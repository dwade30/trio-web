﻿namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptW2Narrow2Up.
	/// </summary>
	partial class rptW2Narrow2Up
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptW2Narrow2Up));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtFederalEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeesName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICATax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAllocationTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICAWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSTips = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDependentCare = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNonQualifiedPlans = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployersStateEIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateWages = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederalEIN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployersName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployeesName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalWages2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFederalTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICATax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedTaxes2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAllocationTips2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFICAWages2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedWages2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSTips2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDependentCare2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNonQualifiedPlans2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOther2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmployersStateEIN2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateWages2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12aAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12bAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12cAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12dAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12a2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12b2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12c2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12d2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12a2Amount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12b2Amount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12c2Amount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt12d2Amount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPen2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatutoryEmployee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatutoryEmployee2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFirstAndMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLastName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeesName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTaxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllocationTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTips)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentCare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNonQualifiedPlans)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersStateEIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeesName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWages2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTaxes2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllocationTips2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWages2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWages2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTips2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentCare2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNonQualifiedPlans2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersStateEIN2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWages2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12aAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12bAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12cAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12dAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12a2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12b2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12c2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12d2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12a2Amount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12b2Amount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12c2Amount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12d2Amount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatutoryEmployee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatutoryEmployee2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstAndMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLastName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFederalEIN,
            this.txtEmployersName,
            this.txtSSN,
            this.txtEmployeesName,
            this.txtTotalWages,
            this.txtFederalTax,
            this.txtFICATax,
            this.txtMedTaxes,
            this.txtAllocationTips,
            this.txtFICAWages,
            this.txtMedWages,
            this.txtSSTips,
            this.txtDependentCare,
            this.txtNonQualifiedPlans,
            this.txtOther,
            this.txtState,
            this.txtEmployersStateEIN,
            this.txtStateWages,
            this.txtStateTax,
            this.txtFederalEIN2,
            this.txtEmployersName2,
            this.txtSSN2,
            this.txtEmployeesName2,
            this.txtTotalWages2,
            this.txtFederalTax2,
            this.txtFICATax2,
            this.txtMedTaxes2,
            this.txtAllocationTips2,
            this.txtFICAWages2,
            this.txtMedWages2,
            this.txtSSTips2,
            this.txtDependentCare2,
            this.txtNonQualifiedPlans2,
            this.txtOther2,
            this.txtState2,
            this.txtEmployersStateEIN2,
            this.txtStateWages2,
            this.txtStateTax2,
            this.txt12a,
            this.txt12b,
            this.txt12c,
            this.txt12d,
            this.txt12aAmount,
            this.txt12bAmount,
            this.txt12cAmount,
            this.txt12dAmount,
            this.txt12a2,
            this.txt12b2,
            this.txt12c2,
            this.txt12d2,
            this.txt12a2Amount,
            this.txt12b2Amount,
            this.txt12c2Amount,
            this.txt12d2Amount,
            this.txtPen,
            this.txtPen2,
            this.txtStatutoryEmployee,
            this.txtStatutoryEmployee2,
            this.txtFirstAndMiddle,
            this.txtLastName});
			this.Detail.Height = 5.5F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtFederalEIN
			// 
			this.txtFederalEIN.Height = 0.1666667F;
			this.txtFederalEIN.Left = 5.3125F;
			this.txtFederalEIN.Name = "txtFederalEIN";
			this.txtFederalEIN.Style = "font-family: \'Tahoma\'";
			this.txtFederalEIN.Text = null;
			this.txtFederalEIN.Top = 2.291667F;
			this.txtFederalEIN.Width = 1.5F;
			// 
			// txtEmployersName
			// 
			this.txtEmployersName.Height = 0.625F;
			this.txtEmployersName.Left = 5.3125F;
			this.txtEmployersName.Name = "txtEmployersName";
			this.txtEmployersName.Style = "font-family: \'Tahoma\'";
			this.txtEmployersName.Text = null;
			this.txtEmployersName.Top = 1.583333F;
			this.txtEmployersName.Width = 3.1875F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1666667F;
			this.txtSSN.Left = 6.96875F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Style = "font-family: \'Tahoma\'";
			this.txtSSN.Text = null;
			this.txtSSN.Top = 2.291667F;
			this.txtSSN.Width = 1.53125F;
			// 
			// txtEmployeesName
			// 
			this.txtEmployeesName.Height = 0.875F;
			this.txtEmployeesName.Left = 5.3125F;
			this.txtEmployeesName.Name = "txtEmployeesName";
			this.txtEmployeesName.Style = "font-family: \'Tahoma\'";
			this.txtEmployeesName.Text = null;
			this.txtEmployeesName.Top = 2.583333F;
			this.txtEmployeesName.Width = 3.1875F;
			// 
			// txtTotalWages
			// 
			this.txtTotalWages.Height = 0.1666667F;
			this.txtTotalWages.Left = 7.125F;
			this.txtTotalWages.Name = "txtTotalWages";
			this.txtTotalWages.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalWages.Text = null;
			this.txtTotalWages.Top = 0.5833333F;
			this.txtTotalWages.Width = 1.25F;
			// 
			// txtFederalTax
			// 
			this.txtFederalTax.Height = 0.1666667F;
			this.txtFederalTax.Left = 9F;
			this.txtFederalTax.Name = "txtFederalTax";
			this.txtFederalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFederalTax.Text = null;
			this.txtFederalTax.Top = 0.5833333F;
			this.txtFederalTax.Width = 1.15625F;
			// 
			// txtFICATax
			// 
			this.txtFICATax.Height = 0.1666667F;
			this.txtFICATax.Left = 9F;
			this.txtFICATax.Name = "txtFICATax";
			this.txtFICATax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFICATax.Text = null;
			this.txtFICATax.Top = 0.7916667F;
			this.txtFICATax.Width = 1.15625F;
			// 
			// txtMedTaxes
			// 
			this.txtMedTaxes.Height = 0.1666667F;
			this.txtMedTaxes.Left = 9F;
			this.txtMedTaxes.Name = "txtMedTaxes";
			this.txtMedTaxes.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMedTaxes.Text = null;
			this.txtMedTaxes.Top = 1F;
			this.txtMedTaxes.Width = 1.15625F;
			// 
			// txtAllocationTips
			// 
			this.txtAllocationTips.Height = 0.1666667F;
			this.txtAllocationTips.Left = 5.3125F;
			this.txtAllocationTips.Name = "txtAllocationTips";
			this.txtAllocationTips.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAllocationTips.Text = null;
			this.txtAllocationTips.Top = 1.041667F;
			this.txtAllocationTips.Width = 1.25F;
			// 
			// txtFICAWages
			// 
			this.txtFICAWages.Height = 0.1666667F;
			this.txtFICAWages.Left = 7.125F;
			this.txtFICAWages.Name = "txtFICAWages";
			this.txtFICAWages.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFICAWages.Text = null;
			this.txtFICAWages.Top = 0.7916667F;
			this.txtFICAWages.Width = 1.25F;
			// 
			// txtMedWages
			// 
			this.txtMedWages.Height = 0.1666667F;
			this.txtMedWages.Left = 7.125F;
			this.txtMedWages.Name = "txtMedWages";
			this.txtMedWages.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMedWages.Text = null;
			this.txtMedWages.Top = 1.041667F;
			this.txtMedWages.Width = 1.25F;
			// 
			// txtSSTips
			// 
			this.txtSSTips.Height = 0.1666667F;
			this.txtSSTips.Left = 5.3125F;
			this.txtSSTips.Name = "txtSSTips";
			this.txtSSTips.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSSTips.Text = null;
			this.txtSSTips.Top = 0.7916667F;
			this.txtSSTips.Width = 1.25F;
			// 
			// txtDependentCare
			// 
			this.txtDependentCare.Height = 0.1666667F;
			this.txtDependentCare.Left = 7.125F;
			this.txtDependentCare.Name = "txtDependentCare";
			this.txtDependentCare.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDependentCare.Text = null;
			this.txtDependentCare.Top = 1.291667F;
			this.txtDependentCare.Width = 1.25F;
			// 
			// txtNonQualifiedPlans
			// 
			this.txtNonQualifiedPlans.Height = 0.1666667F;
			this.txtNonQualifiedPlans.Left = 9F;
			this.txtNonQualifiedPlans.Name = "txtNonQualifiedPlans";
			this.txtNonQualifiedPlans.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtNonQualifiedPlans.Text = null;
			this.txtNonQualifiedPlans.Top = 1.208333F;
			this.txtNonQualifiedPlans.Width = 1.15625F;
			// 
			// txtOther
			// 
			this.txtOther.Height = 0.7083333F;
			this.txtOther.Left = 8.59375F;
			this.txtOther.Name = "txtOther";
			this.txtOther.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtOther.Text = null;
			this.txtOther.Top = 2.75F;
			this.txtOther.Width = 1.625F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1666667F;
			this.txtState.Left = 6.03125F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Tahoma\'";
			this.txtState.Text = null;
			this.txtState.Top = 3.583333F;
			this.txtState.Width = 0.3125F;
			// 
			// txtEmployersStateEIN
			// 
			this.txtEmployersStateEIN.Height = 0.1666667F;
			this.txtEmployersStateEIN.Left = 6.40625F;
			this.txtEmployersStateEIN.Name = "txtEmployersStateEIN";
			this.txtEmployersStateEIN.Style = "font-family: \'Tahoma\'";
			this.txtEmployersStateEIN.Text = null;
			this.txtEmployersStateEIN.Top = 3.583333F;
			this.txtEmployersStateEIN.Width = 1.28125F;
			// 
			// txtStateWages
			// 
			this.txtStateWages.Height = 0.1666667F;
			this.txtStateWages.Left = 7.71875F;
			this.txtStateWages.Name = "txtStateWages";
			this.txtStateWages.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtStateWages.Text = null;
			this.txtStateWages.Top = 3.583333F;
			this.txtStateWages.Width = 1.28125F;
			// 
			// txtStateTax
			// 
			this.txtStateTax.Height = 0.1666667F;
			this.txtStateTax.Left = 9.0625F;
			this.txtStateTax.Name = "txtStateTax";
			this.txtStateTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtStateTax.Text = null;
			this.txtStateTax.Top = 3.583333F;
			this.txtStateTax.Width = 1.125F;
			// 
			// txtFederalEIN2
			// 
			this.txtFederalEIN2.Height = 0.1666667F;
			this.txtFederalEIN2.Left = 0.0625F;
			this.txtFederalEIN2.Name = "txtFederalEIN2";
			this.txtFederalEIN2.Style = "font-family: \'Tahoma\'";
			this.txtFederalEIN2.Text = null;
			this.txtFederalEIN2.Top = 2.291667F;
			this.txtFederalEIN2.Width = 1.5F;
			// 
			// txtEmployersName2
			// 
			this.txtEmployersName2.Height = 0.625F;
			this.txtEmployersName2.Left = 0.0625F;
			this.txtEmployersName2.Name = "txtEmployersName2";
			this.txtEmployersName2.Style = "font-family: \'Tahoma\'";
			this.txtEmployersName2.Text = null;
			this.txtEmployersName2.Top = 1.583333F;
			this.txtEmployersName2.Width = 3.1875F;
			// 
			// txtSSN2
			// 
			this.txtSSN2.Height = 0.1666667F;
			this.txtSSN2.Left = 1.71875F;
			this.txtSSN2.Name = "txtSSN2";
			this.txtSSN2.Style = "font-family: \'Tahoma\'";
			this.txtSSN2.Text = null;
			this.txtSSN2.Top = 2.291667F;
			this.txtSSN2.Width = 1.53125F;
			// 
			// txtEmployeesName2
			// 
			this.txtEmployeesName2.Height = 0.625F;
			this.txtEmployeesName2.Left = 0.08333334F;
			this.txtEmployeesName2.Name = "txtEmployeesName2";
			this.txtEmployeesName2.Style = "font-family: \'Tahoma\'";
			this.txtEmployeesName2.Text = null;
			this.txtEmployeesName2.Top = 2.833333F;
			this.txtEmployeesName2.Width = 3.354167F;
			// 
			// txtTotalWages2
			// 
			this.txtTotalWages2.Height = 0.1666667F;
			this.txtTotalWages2.Left = 1.875F;
			this.txtTotalWages2.Name = "txtTotalWages2";
			this.txtTotalWages2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalWages2.Text = null;
			this.txtTotalWages2.Top = 0.5833333F;
			this.txtTotalWages2.Width = 1.25F;
			// 
			// txtFederalTax2
			// 
			this.txtFederalTax2.Height = 0.1666667F;
			this.txtFederalTax2.Left = 3.75F;
			this.txtFederalTax2.Name = "txtFederalTax2";
			this.txtFederalTax2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFederalTax2.Text = null;
			this.txtFederalTax2.Top = 0.5833333F;
			this.txtFederalTax2.Width = 1.15625F;
			// 
			// txtFICATax2
			// 
			this.txtFICATax2.Height = 0.1666667F;
			this.txtFICATax2.Left = 3.75F;
			this.txtFICATax2.Name = "txtFICATax2";
			this.txtFICATax2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFICATax2.Text = null;
			this.txtFICATax2.Top = 0.7916667F;
			this.txtFICATax2.Width = 1.15625F;
			// 
			// txtMedTaxes2
			// 
			this.txtMedTaxes2.Height = 0.1666667F;
			this.txtMedTaxes2.Left = 3.75F;
			this.txtMedTaxes2.Name = "txtMedTaxes2";
			this.txtMedTaxes2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMedTaxes2.Text = null;
			this.txtMedTaxes2.Top = 1F;
			this.txtMedTaxes2.Width = 1.15625F;
			// 
			// txtAllocationTips2
			// 
			this.txtAllocationTips2.Height = 0.1666667F;
			this.txtAllocationTips2.Left = 0.0625F;
			this.txtAllocationTips2.Name = "txtAllocationTips2";
			this.txtAllocationTips2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAllocationTips2.Text = null;
			this.txtAllocationTips2.Top = 1.041667F;
			this.txtAllocationTips2.Width = 1.25F;
			// 
			// txtFICAWages2
			// 
			this.txtFICAWages2.Height = 0.1666667F;
			this.txtFICAWages2.Left = 1.875F;
			this.txtFICAWages2.Name = "txtFICAWages2";
			this.txtFICAWages2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFICAWages2.Text = null;
			this.txtFICAWages2.Top = 0.7916667F;
			this.txtFICAWages2.Width = 1.25F;
			// 
			// txtMedWages2
			// 
			this.txtMedWages2.Height = 0.1666667F;
			this.txtMedWages2.Left = 1.875F;
			this.txtMedWages2.Name = "txtMedWages2";
			this.txtMedWages2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMedWages2.Text = null;
			this.txtMedWages2.Top = 1.041667F;
			this.txtMedWages2.Width = 1.25F;
			// 
			// txtSSTips2
			// 
			this.txtSSTips2.Height = 0.1666667F;
			this.txtSSTips2.Left = 0.0625F;
			this.txtSSTips2.Name = "txtSSTips2";
			this.txtSSTips2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSSTips2.Text = null;
			this.txtSSTips2.Top = 0.7916667F;
			this.txtSSTips2.Width = 1.25F;
			// 
			// txtDependentCare2
			// 
			this.txtDependentCare2.Height = 0.1666667F;
			this.txtDependentCare2.Left = 1.875F;
			this.txtDependentCare2.Name = "txtDependentCare2";
			this.txtDependentCare2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDependentCare2.Text = null;
			this.txtDependentCare2.Top = 1.291667F;
			this.txtDependentCare2.Width = 1.25F;
			// 
			// txtNonQualifiedPlans2
			// 
			this.txtNonQualifiedPlans2.Height = 0.1666667F;
			this.txtNonQualifiedPlans2.Left = 3.75F;
			this.txtNonQualifiedPlans2.Name = "txtNonQualifiedPlans2";
			this.txtNonQualifiedPlans2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtNonQualifiedPlans2.Text = null;
			this.txtNonQualifiedPlans2.Top = 1.208333F;
			this.txtNonQualifiedPlans2.Width = 1.15625F;
			// 
			// txtOther2
			// 
			this.txtOther2.Height = 0.6666667F;
			this.txtOther2.Left = 3.5F;
			this.txtOther2.Name = "txtOther2";
			this.txtOther2.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtOther2.Text = null;
			this.txtOther2.Top = 2.791667F;
			this.txtOther2.Width = 1.46875F;
			// 
			// txtState2
			// 
			this.txtState2.Height = 0.1666667F;
			this.txtState2.Left = 0.84375F;
			this.txtState2.Name = "txtState2";
			this.txtState2.Style = "font-family: \'Tahoma\'";
			this.txtState2.Text = null;
			this.txtState2.Top = 3.583333F;
			this.txtState2.Width = 0.3125F;
			// 
			// txtEmployersStateEIN2
			// 
			this.txtEmployersStateEIN2.Height = 0.1666667F;
			this.txtEmployersStateEIN2.Left = 1.21875F;
			this.txtEmployersStateEIN2.Name = "txtEmployersStateEIN2";
			this.txtEmployersStateEIN2.Style = "font-family: \'Tahoma\'";
			this.txtEmployersStateEIN2.Text = null;
			this.txtEmployersStateEIN2.Top = 3.583333F;
			this.txtEmployersStateEIN2.Width = 1.28125F;
			// 
			// txtStateWages2
			// 
			this.txtStateWages2.Height = 0.1666667F;
			this.txtStateWages2.Left = 2.53125F;
			this.txtStateWages2.Name = "txtStateWages2";
			this.txtStateWages2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtStateWages2.Text = null;
			this.txtStateWages2.Top = 3.583333F;
			this.txtStateWages2.Width = 1.28125F;
			// 
			// txtStateTax2
			// 
			this.txtStateTax2.Height = 0.1666667F;
			this.txtStateTax2.Left = 3.875F;
			this.txtStateTax2.Name = "txtStateTax2";
			this.txtStateTax2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtStateTax2.Text = null;
			this.txtStateTax2.Top = 3.583333F;
			this.txtStateTax2.Width = 1.125F;
			// 
			// txt12a
			// 
			this.txt12a.Height = 0.1666667F;
			this.txt12a.Left = 3.34375F;
			this.txt12a.Name = "txt12a";
			this.txt12a.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txt12a.Text = null;
			this.txt12a.Top = 1.541667F;
			this.txt12a.Width = 0.6875F;
			// 
			// txt12b
			// 
			this.txt12b.Height = 0.1666667F;
			this.txt12b.Left = 3.34375F;
			this.txt12b.Name = "txt12b";
			this.txt12b.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txt12b.Text = null;
			this.txt12b.Top = 1.791667F;
			this.txt12b.Width = 0.6875F;
			// 
			// txt12c
			// 
			this.txt12c.Height = 0.1666667F;
			this.txt12c.Left = 3.34375F;
			this.txt12c.Name = "txt12c";
			this.txt12c.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txt12c.Text = null;
			this.txt12c.Top = 2F;
			this.txt12c.Width = 0.6875F;
			// 
			// txt12d
			// 
			this.txt12d.Height = 0.1666667F;
			this.txt12d.Left = 3.34375F;
			this.txt12d.Name = "txt12d";
			this.txt12d.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txt12d.Text = null;
			this.txt12d.Top = 2.208333F;
			this.txt12d.Width = 0.6875F;
			// 
			// txt12aAmount
			// 
			this.txt12aAmount.Height = 0.1666667F;
			this.txt12aAmount.Left = 4.0625F;
			this.txt12aAmount.Name = "txt12aAmount";
			this.txt12aAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txt12aAmount.Text = null;
			this.txt12aAmount.Top = 1.541667F;
			this.txt12aAmount.Width = 0.84375F;
			// 
			// txt12bAmount
			// 
			this.txt12bAmount.Height = 0.1666667F;
			this.txt12bAmount.Left = 4.0625F;
			this.txt12bAmount.Name = "txt12bAmount";
			this.txt12bAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txt12bAmount.Text = null;
			this.txt12bAmount.Top = 1.791667F;
			this.txt12bAmount.Width = 0.84375F;
			// 
			// txt12cAmount
			// 
			this.txt12cAmount.Height = 0.1666667F;
			this.txt12cAmount.Left = 4.0625F;
			this.txt12cAmount.Name = "txt12cAmount";
			this.txt12cAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txt12cAmount.Text = null;
			this.txt12cAmount.Top = 2F;
			this.txt12cAmount.Width = 0.84375F;
			// 
			// txt12dAmount
			// 
			this.txt12dAmount.Height = 0.1666667F;
			this.txt12dAmount.Left = 4.0625F;
			this.txt12dAmount.Name = "txt12dAmount";
			this.txt12dAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txt12dAmount.Text = null;
			this.txt12dAmount.Top = 2.208333F;
			this.txt12dAmount.Width = 0.84375F;
			// 
			// txt12a2
			// 
			this.txt12a2.Height = 0.1666667F;
			this.txt12a2.Left = 8.59375F;
			this.txt12a2.Name = "txt12a2";
			this.txt12a2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txt12a2.Text = null;
			this.txt12a2.Top = 1.541667F;
			this.txt12a2.Width = 0.65625F;
			// 
			// txt12b2
			// 
			this.txt12b2.Height = 0.1666667F;
			this.txt12b2.Left = 8.59375F;
			this.txt12b2.Name = "txt12b2";
			this.txt12b2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txt12b2.Text = null;
			this.txt12b2.Top = 1.75F;
			this.txt12b2.Width = 0.65625F;
			// 
			// txt12c2
			// 
			this.txt12c2.Height = 0.1666667F;
			this.txt12c2.Left = 8.59375F;
			this.txt12c2.Name = "txt12c2";
			this.txt12c2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txt12c2.Text = null;
			this.txt12c2.Top = 1.958333F;
			this.txt12c2.Width = 0.65625F;
			// 
			// txt12d2
			// 
			this.txt12d2.Height = 0.1666667F;
			this.txt12d2.Left = 8.59375F;
			this.txt12d2.Name = "txt12d2";
			this.txt12d2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.txt12d2.Text = null;
			this.txt12d2.Top = 2.166667F;
			this.txt12d2.Width = 0.65625F;
			// 
			// txt12a2Amount
			// 
			this.txt12a2Amount.Height = 0.1666667F;
			this.txt12a2Amount.Left = 9.28125F;
			this.txt12a2Amount.Name = "txt12a2Amount";
			this.txt12a2Amount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txt12a2Amount.Text = null;
			this.txt12a2Amount.Top = 1.541667F;
			this.txt12a2Amount.Width = 0.875F;
			// 
			// txt12b2Amount
			// 
			this.txt12b2Amount.Height = 0.1666667F;
			this.txt12b2Amount.Left = 9.28125F;
			this.txt12b2Amount.Name = "txt12b2Amount";
			this.txt12b2Amount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txt12b2Amount.Text = null;
			this.txt12b2Amount.Top = 1.75F;
			this.txt12b2Amount.Width = 0.875F;
			// 
			// txt12c2Amount
			// 
			this.txt12c2Amount.Height = 0.1666667F;
			this.txt12c2Amount.Left = 9.28125F;
			this.txt12c2Amount.Name = "txt12c2Amount";
			this.txt12c2Amount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txt12c2Amount.Text = null;
			this.txt12c2Amount.Top = 1.958333F;
			this.txt12c2Amount.Width = 0.875F;
			// 
			// txt12d2Amount
			// 
			this.txt12d2Amount.Height = 0.1666667F;
			this.txt12d2Amount.Left = 9.28125F;
			this.txt12d2Amount.Name = "txt12d2Amount";
			this.txt12d2Amount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txt12d2Amount.Text = null;
			this.txt12d2Amount.Top = 2.166667F;
			this.txt12d2Amount.Width = 0.875F;
			// 
			// txtPen
			// 
			this.txtPen.Height = 0.1666667F;
			this.txtPen.Left = 4.03125F;
			this.txtPen.Name = "txtPen";
			this.txtPen.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.txtPen.Text = "X";
			this.txtPen.Top = 2.541667F;
			this.txtPen.Visible = false;
			this.txtPen.Width = 0.15625F;
			// 
			// txtPen2
			// 
			this.txtPen2.Height = 0.1666667F;
			this.txtPen2.Left = 9.25F;
			this.txtPen2.Name = "txtPen2";
			this.txtPen2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.txtPen2.Text = "X";
			this.txtPen2.Top = 2.5F;
			this.txtPen2.Visible = false;
			this.txtPen2.Width = 0.15625F;
			// 
			// txtStatutoryEmployee
			// 
			this.txtStatutoryEmployee.Height = 0.1666667F;
			this.txtStatutoryEmployee.Left = 3.53125F;
			this.txtStatutoryEmployee.Name = "txtStatutoryEmployee";
			this.txtStatutoryEmployee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.txtStatutoryEmployee.Text = "X";
			this.txtStatutoryEmployee.Top = 2.541667F;
			this.txtStatutoryEmployee.Visible = false;
			this.txtStatutoryEmployee.Width = 0.15625F;
			// 
			// txtStatutoryEmployee2
			// 
			this.txtStatutoryEmployee2.Height = 0.1666667F;
			this.txtStatutoryEmployee2.Left = 8.625F;
			this.txtStatutoryEmployee2.Name = "txtStatutoryEmployee2";
			this.txtStatutoryEmployee2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.txtStatutoryEmployee2.Text = "X";
			this.txtStatutoryEmployee2.Top = 2.5F;
			this.txtStatutoryEmployee2.Visible = false;
			this.txtStatutoryEmployee2.Width = 0.15625F;
			// 
			// txtFirstAndMiddle
			// 
			this.txtFirstAndMiddle.Height = 0.1666667F;
			this.txtFirstAndMiddle.Left = 0.08333334F;
			this.txtFirstAndMiddle.Name = "txtFirstAndMiddle";
			this.txtFirstAndMiddle.Style = "font-family: \'Tahoma\'";
			this.txtFirstAndMiddle.Text = null;
			this.txtFirstAndMiddle.Top = 2.583333F;
			this.txtFirstAndMiddle.Width = 1.583333F;
			// 
			// txtLastName
			// 
			this.txtLastName.Height = 0.1666667F;
			this.txtLastName.Left = 1.75F;
			this.txtLastName.Name = "txtLastName";
			this.txtLastName.Style = "font-family: \'Tahoma\'";
			this.txtLastName.Text = null;
			this.txtLastName.Top = 2.583333F;
			this.txtLastName.Width = 1.666667F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptW2Narrow2Up
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.30208F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeesName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTaxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllocationTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTips)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentCare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNonQualifiedPlans)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersStateEIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalEIN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployeesName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalWages2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFederalTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICATax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedTaxes2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllocationTips2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFICAWages2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedWages2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSTips2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDependentCare2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNonQualifiedPlans2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmployersStateEIN2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateWages2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12aAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12bAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12cAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12dAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12a2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12b2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12c2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12d2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12a2Amount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12b2Amount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12c2Amount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt12d2Amount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatutoryEmployee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatutoryEmployee2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstAndMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLastName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeesName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTaxes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllocationTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSTips;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDependentCare;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNonQualifiedPlans;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersStateEIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateWages;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalEIN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployeesName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalWages2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFederalTax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICATax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedTaxes2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllocationTips2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFICAWages2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedWages2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSTips2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDependentCare2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNonQualifiedPlans2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmployersStateEIN2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateWages2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateTax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12aAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12bAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12cAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12dAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12a2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12b2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12c2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12d2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12a2Amount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12b2Amount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12c2Amount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt12d2Amount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPen2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatutoryEmployee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatutoryEmployee2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirstAndMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLastName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
