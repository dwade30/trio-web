//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	public class clsSchedule
	{
		//=========================================================
		private int lngScheduleID;
		private string strName = string.Empty;
		private modSchedule.SchedulePaidOption intPaidOption;
		private bool boolUnused;
		private clsScheduleWeek[] WeekList = null;
		private int intCurrentWeek;

		public bool AdvanceWeekRotations()
		{
			bool AdvanceWeekRotations = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int intWeek;
				AdvanceWeekRotations = false;
				intWeek = HighestWeek();
				string strSQL;
				clsDRWrapper rsSave = new clsDRWrapper();
				strSQL = "Update tblemployeemaster set currentscheduleweek = currentscheduleweek + 1 where scheduleid = " + FCConvert.ToString(lngScheduleID);
				rsSave.Execute(strSQL, "twpy0000.vb1");
				strSQL = "update tblemployeemaster set currentscheduleweek = 1 where currentscheduleweek > " + FCConvert.ToString(intWeek) + " and scheduleid = " + FCConvert.ToString(lngScheduleID);
				rsSave.Execute(strSQL, "twpy0000.vb1");
				AdvanceWeekRotations = true;
				return AdvanceWeekRotations;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AdvanceWeekRotations", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AdvanceWeekRotations;
		}

		public bool ReverseWeekRotations()
		{
			bool ReverseWeekRotations = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int intWeek;
				intWeek = HighestWeek();
				ReverseWeekRotations = false;
				string strSQL;
				clsDRWrapper rsSave = new clsDRWrapper();
				strSQL = "update tblemployeemaster set currentscheduleweek = currentscheduleweek - 1 where scheduleid = " + FCConvert.ToString(lngScheduleID);
				rsSave.Execute(strSQL, "twpy0000.vb1");
				strSQL = "update tblemployeemaster set currentscheduleweek = " + FCConvert.ToString(intWeek) + " where scheduleid = " + FCConvert.ToString(lngScheduleID) + " and currentscheduleweek < 1";
				rsSave.Execute(strSQL, "twpy0000.vb1");
				ReverseWeekRotations = true;
				return ReverseWeekRotations;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReverseWeekRotations", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ReverseWeekRotations;
		}

		public bool Unused
		{
			set
			{
				boolUnused = value;
			}
			get
			{
				bool Unused = false;
				Unused = boolUnused;
				return Unused;
			}
		}

		public int GetNextWeekInRotation(int intWeek)
		{
			int GetNextWeekInRotation = 0;
			int intReturn;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				intReturn = GetRotationWeekFromAnyWeek(1, intWeek, 2);
				GetNextWeekInRotation = intReturn;
				return GetNextWeekInRotation;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetNexWeekInRotation", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetNextWeekInRotation;
		}

		public int GetRotationWeekFromAnyWeek(int intKnownWeekofYear, int intKnownWeekRotation, int intUnknownWeekofYear)
		{
			int GetRotationWeekFromAnyWeek = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int intReturn = 0;
				int intHWeek;
				intHWeek = HighestWeek();
				if (intHWeek == 0)
				{
					intReturn = 0;
				}
				else if (intHWeek == 1)
				{
					intReturn = 1;
				}
				else
				{
					intReturn = ((intUnknownWeekofYear - intKnownWeekofYear) % intHWeek) + intKnownWeekRotation;
					if (intReturn <= 0)
					{
						if (intReturn == 0)
						{
							intReturn = intHWeek;
						}
						else
						{
							intReturn = intHWeek + intReturn;
						}
					}
				}
				GetRotationWeekFromAnyWeek = intReturn;
				return GetRotationWeekFromAnyWeek;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetRotationWeekFromAnyWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetRotationWeekFromAnyWeek;
		}

		public int HighestWeek()
		{
			int HighestWeek = 0;
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			int intReturn;
			intReturn = 0;
			if (!FCUtils.IsEmpty(WeekList))
			{
				for (X = 0; X <= (Information.UBound(WeekList, 1)); X++)
				{
					if (!(WeekList[X] == null))
					{
						if (!WeekList[X].Unused)
						{
							if (intReturn < WeekList[X].OrderNumber)
							{
								intReturn = WeekList[X].OrderNumber;
							}
						}
					}
				}
				// X
			}
			else
			{
				intReturn = 0;
			}
			HighestWeek = intReturn;
			return HighestWeek;
		}

		public bool AddWeek(int intOrderNo, string strWeekName, int lngWeekID = 0)
		{
			bool AddWeek = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				AddWeek = false;
				clsScheduleWeek tWeek = new clsScheduleWeek();
				tWeek.OrderNumber = intOrderNo;
				tWeek.WeekName = strWeekName;
				tWeek.WeekID = lngWeekID;
				tWeek.ScheduleID = lngScheduleID;
				if (InsertWeek(ref tWeek))
				{
					AddWeek = true;
				}
				AddWeek = true;
				return AddWeek;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddWeek;
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (!FCUtils.IsEmpty(WeekList))
			{
				// intCurrentWeek = intCurrentWeek + 1
				if (intCurrentWeek > Information.UBound(WeekList, 1))
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentWeek <= Information.UBound(WeekList, 1))
					{
						intCurrentWeek += 1;
						if (intCurrentWeek > Information.UBound(WeekList, 1))
						{
							intReturn = -1;
							break;
						}
						else if (WeekList[intCurrentWeek] == null)
						{
						}
						else if (WeekList[intCurrentWeek].Unused)
						{
						}
						else
						{
							intReturn = intCurrentWeek;
							break;
						}
					}
				}
				intCurrentWeek = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentWeek = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public void MoveFirst()
		{
			if (!FCUtils.IsEmpty(WeekList))
			{
				if (Information.UBound(WeekList, 1) >= 0)
				{
					intCurrentWeek = -1;
					MoveNext();
				}
				else
				{
					intCurrentWeek = -1;
				}
			}
			else
			{
				intCurrentWeek = -1;
			}
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentWeek;
				return GetCurrentIndex;
			}
		}

		public clsScheduleWeek GetWeekByID(ref int lngID)
		{
			clsScheduleWeek GetWeekByID = null;
			clsScheduleWeek tWeek;
			tWeek = null;
			if (!FCUtils.IsEmpty(WeekList) && lngID > 0)
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(WeekList, 1)); X++)
				{
					if (!(WeekList[X] == null))
					{
						if (!WeekList[X].Unused)
						{
							if (WeekList[X].WeekID == lngID)
							{
								intCurrentWeek = X;
								tWeek = WeekList[X];
								break;
							}
						}
					}
				}
				// X
			}
			GetWeekByID = tWeek;
			return GetWeekByID;
		}
		// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToDouble(
		public clsScheduleWeek GetWeekByIndex(int intindex)
		{
			clsScheduleWeek GetWeekByIndex = null;
			clsScheduleWeek tWeek;
			tWeek = null;
			if (!FCUtils.IsEmpty(WeekList) && intindex >= 0)
			{
				if (!(WeekList[intindex] == null))
				{
					if (!WeekList[intindex].Unused)
					{
						intCurrentWeek = intindex;
						tWeek = WeekList[intindex];
					}
				}
			}
			GetWeekByIndex = tWeek;
			return GetWeekByIndex;
		}

		public clsScheduleWeek GetWeekByNumber(int intOrder)
		{
			clsScheduleWeek GetWeekByNumber = null;
			clsScheduleWeek tWeek;
			tWeek = null;
			if (!FCUtils.IsEmpty(WeekList) && intOrder > 0)
			{
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				for (X = 0; X <= (Information.UBound(WeekList, 1)); X++)
				{
					if (!(WeekList[X] == null))
					{
						if (!WeekList[X].Unused)
						{
							if (WeekList[X].OrderNumber == intOrder)
							{
								intCurrentWeek = X;
								tWeek = WeekList[X];
								break;
							}
						}
					}
				}
				// X
			}
			GetWeekByNumber = tWeek;
			return GetWeekByNumber;
		}

		public clsScheduleWeek GetCurrentWeek()
		{
			clsScheduleWeek GetCurrentWeek = null;
			clsScheduleWeek tWeek;
			tWeek = null;
			if (!FCUtils.IsEmpty(WeekList))
			{
				if (intCurrentWeek >= 0)
				{
					if (!(WeekList[intCurrentWeek] == null))
					{
						if (!WeekList[intCurrentWeek].Unused)
						{
							tWeek = WeekList[intCurrentWeek];
						}
					}
				}
			}
			GetCurrentWeek = tWeek;
			return GetCurrentWeek;
		}

		public bool InsertWeek(ref clsScheduleWeek tWeek)
		{
			bool InsertWeek = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				InsertWeek = false;
				// vbPorter upgrade warning: intindex As int	OnWriteFCConvert.ToInt32(
				int intindex = 0;
				if (!FCUtils.IsEmpty(WeekList))
				{
					intindex = Information.UBound(WeekList, 1);
					if (intindex < 0)
					{
						intindex = 0;
					}
					Array.Resize(ref WeekList, intindex + 1 + 1);
					intindex = Information.UBound(WeekList, 1);
				}
				else
				{
					WeekList = new clsScheduleWeek[1 + 1];
					intindex = 0;
				}
				tWeek.ScheduleID = lngScheduleID;
				WeekList[intindex] = tWeek;
				intCurrentWeek = intindex;
				InsertWeek = true;
				return InsertWeek;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In InsertWeek", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return InsertWeek;
		}

		public int ScheduleID
		{
			set
			{
				lngScheduleID = value;
			}
			get
			{
				int ScheduleID = 0;
				ScheduleID = lngScheduleID;
				return ScheduleID;
			}
		}

		public string ScheduleName
		{
			set
			{
				strName = value;
			}
			get
			{
				string ScheduleName = "";
				ScheduleName = strName;
				return ScheduleName;
			}
		}

		public modSchedule.SchedulePaidOption PaidOption
		{
			set
			{
				intPaidOption = value;
			}
			get
			{
				modSchedule.SchedulePaidOption PaidOption = (modSchedule.SchedulePaidOption)0;
				PaidOption = intPaidOption;
				return PaidOption;
			}
		}

		public void LoadSchedule(bool boolLoadWeeks)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strSQL = "select * from schedules where id = " + FCConvert.ToString(lngScheduleID);
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					strName = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					intPaidOption = (modSchedule.SchedulePaidOption)rsLoad.Get_Fields_Int16("PaidOption");
					strSQL = "select id,orderno from ScheduleWeeks  where scheduleid = " + FCConvert.ToString(lngScheduleID) + " order by orderno";
					FCUtils.EraseSafe(WeekList);
					intCurrentWeek = -1;
					if (boolLoadWeeks)
					{
						rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
						clsScheduleWeek tWeek;
						while (!rsLoad.EndOfFile())
						{
							tWeek = new clsScheduleWeek();
							tWeek.OrderNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("OrderNo"))));
							tWeek.LoadDefaultWeek(rsLoad.Get_Fields("id"));
							InsertWeek(ref tWeek);
							rsLoad.MoveNext();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void DeleteSchedule()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
				int X;
				if (!FCUtils.IsEmpty(WeekList))
				{
					for (X = 0; X <= (Information.UBound(WeekList, 1)); X++)
					{
						if (!(WeekList[X] == null))
						{
							WeekList[X].DeleteDefaultWeek();
							WeekList[X] = null;
						}
					}
					// X
				}
				string strSQL;
				clsDRWrapper rsSave = new clsDRWrapper();
				strSQL = "Delete from schedules where id = " + FCConvert.ToString(lngScheduleID);
				rsSave.Execute(strSQL, "twpy0000.vb1");
				intCurrentWeek = -1;
				strName = "";
				lngScheduleID = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool SaveSchedule(bool boolSaveWeeks)
		{
			bool SaveSchedule = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				bool boolReturn;
				SaveSchedule = false;
				boolReturn = true;
				if (!boolUnused)
				{
					strSQL = "select * from schedules where id = " + FCConvert.ToString(lngScheduleID);
					rsSave.OpenRecordset(strSQL, "twpy0000.vb1");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("Name", strName);
					rsSave.Set_Fields("PaidOption", intPaidOption);
					rsSave.Update();
					lngScheduleID = FCConvert.ToInt32(rsSave.Get_Fields("ID"));
					if (boolSaveWeeks)
					{
						// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
						int X;
						if (!FCUtils.IsEmpty(WeekList))
						{
							for (X = 0; X <= (Information.UBound(WeekList, 1)); X++)
							{
								if (!(WeekList[X] == null))
								{
									WeekList[X].ScheduleID = lngScheduleID;
									boolReturn = boolReturn && WeekList[X].SaveDefaultWeek();
								}
							}
							// X
						}
					}
				}
				else
				{
					DeleteSchedule();
				}
				SaveSchedule = boolReturn;
				return SaveSchedule;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveSchedule;
		}
	}
}
