﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	partial class frmCustomReport
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCCheckBox chkLandscape;
		public fecherFoundation.FCPanel fraLayout;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraReports;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public Wisej.Web.ImageList ImageList1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSP11;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnuChangeParameters;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomReport));
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.chkLandscape = new fecherFoundation.FCCheckBox();
            this.fraLayout = new fecherFoundation.FCPanel();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.fraReports = new fecherFoundation.FCFrame();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
			this.Line1 = new fecherFoundation.FCLine();
			this.Line2 = new fecherFoundation.FCLine();
			this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP11 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuChangeParameters = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdAddRow = new fecherFoundation.FCButton();
            this.cmdAddColumn = new fecherFoundation.FCButton();
            this.cmdDeleteRow = new fecherFoundation.FCButton();
            this.cmdDeleteColumn = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandscape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraLayout)).BeginInit();
            this.fraLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReports)).BeginInit();
            this.fraReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkLandscape);
            this.ClientArea.Controls.Add(this.fraMessage);
            this.ClientArea.Controls.Add(this.fraLayout);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraReports);
			this.ClientArea.Controls.Add(this.Line2);
			this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Size = new System.Drawing.Size(1078, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Controls.Add(this.cmdAddRow);
            this.TopPanel.Controls.Add(this.cmdAddColumn);
            this.TopPanel.Controls.Add(this.cmdDeleteRow);
            this.TopPanel.Controls.Add(this.cmdDeleteColumn);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(177, 30);
            this.HeaderText.Text = "Custom Report";
            // 
            // cmbReport
            // 
            this.cmbReport.AutoSize = false;
            this.cmbReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbReport.FormattingEnabled = true;
            this.cmbReport.Items.AddRange(new object[] {
            "Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(278, 40);
            this.cmbReport.TabIndex = 11;
            this.cmbReport.ToolTipText = null;
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // chkLandscape
            // 
            this.chkLandscape.Location = new System.Drawing.Point(722, 472);
            this.chkLandscape.Name = "chkLandscape";
            this.chkLandscape.Size = new System.Drawing.Size(146, 27);
            this.chkLandscape.TabIndex = 23;
            this.chkLandscape.Text = "Print Landscape";
            this.chkLandscape.ToolTipText = null;
            this.chkLandscape.CheckedChanged += new System.EventHandler(this.chkLandscape_CheckedChanged);
            // 
            // fraLayout
            // 
            this.fraLayout.AutoScroll = true;
            this.fraLayout.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraLayout.Controls.Add(this.vsLayout);
            this.fraLayout.Controls.Add(this.Image1);
            this.fraLayout.Controls.Add(this.Line1);
            this.fraLayout.Location = new System.Drawing.Point(30, 30);
            this.fraLayout.Name = "fraLayout";
            this.fraLayout.Size = new System.Drawing.Size(1018, 232);
            this.fraLayout.ScrollBars = Wisej.Web.ScrollBars.Horizontal;
            this.fraLayout.TabIndex = 19;
            // 
            // vsLayout
            // 
            this.vsLayout.AllowSelection = false;
            this.vsLayout.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
            this.vsLayout.AllowUserToResizeRows = false;
            this.vsLayout.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left)));
            this.vsLayout.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsLayout.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsLayout.BackColorBkg = System.Drawing.Color.Empty;
            this.vsLayout.BackColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.BackColorSel = System.Drawing.Color.Empty;
            this.vsLayout.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Both;
            this.vsLayout.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsLayout.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsLayout.ColumnHeadersHeight = 30;
            this.vsLayout.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsLayout.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsLayout.DragIcon = null;
            this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLayout.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.FrozenCols = 0;
			
			this.vsLayout.GridColor = System.Drawing.Color.Empty;
            this.vsLayout.GridColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.Location = new System.Drawing.Point(0, 45);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.OutlineCol = 0;
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsLayout.RowHeightMin = 0;
            this.vsLayout.Rows = 1;
            this.vsLayout.ScrollTipText = null;
            this.vsLayout.ShowColumnVisibilityMenu = false;
            this.vsLayout.Size = new System.Drawing.Size(917, 167);
            this.vsLayout.StandardTab = true;
            this.vsLayout.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsLayout.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsLayout.TabIndex = 21;
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            this.vsLayout.ColumnAdded += new Wisej.Web.DataGridViewColumnEventHandler(this.VsLayout_ColumnAdded);
            this.vsLayout.ColumnRemoved += new Wisej.Web.DataGridViewColumnEventHandler(this.VsLayout_ColumnRemoved);
            this.vsLayout.ColumnWidthChanged += new Wisej.Web.DataGridViewColumnEventHandler(this.vsLayout_ColumnWidthChanged);
            this.vsLayout.ColumnStateChanged += new Wisej.Web.DataGridViewColumnStateChangedEventHandler(this.VsLayout_ColumnStateChanged);
            // 
            // Line1
            // 
            this.Line1.LineColor = System.Drawing.Color.Red;
            this.Line1.LineSize = 2;
            this.Line1.Location = new System.Drawing.Point(720, 45);
            this.Line1.Name = "line1";
            this.Line1.Orientation = Wisej.Web.Orientation.Vertical;
            this.Line1.Size = new System.Drawing.Size(2, 167);
			// 
			// Image1
			// 
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(0, 15);
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(984, 18);
            this.Image1.TabIndex = 23;
            this.Image1.ToolTipText = null;
			// 
			// Line2
			// 
			this.Line2.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line2.BorderWidth = ((short)(1));
			this.Line2.LineWidth = 0;
			this.Line2.Location = new System.Drawing.Point(20, 6);
			this.Line2.Name = "Line2";
			this.Line2.Size = new System.Drawing.Size(144, 1);
			this.Line2.Visible = false;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 1440F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// fraFields
			// 
			this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Location = new System.Drawing.Point(30, 272);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(326, 292);
            this.fraFields.TabIndex = 16;
            this.fraFields.Text = "Fields To Display On Report";
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.AllowDrag = true;
            this.lstFields.AllowDrop = true;
            this.lstFields.Appearance = 0;
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.MultiSelect = 0;
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(286, 242);
            this.lstFields.Sorted = false;
            this.lstFields.TabIndex = 17;
            this.lstFields.ToolTipText = null;
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(359, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(138, 48);
            this.cmdPrint.TabIndex = 15;
            this.cmdPrint.Text = "Print Report";
            this.cmdPrint.ToolTipText = null;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Location = new System.Drawing.Point(375, 272);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(326, 292);
            this.fraSort.TabIndex = 13;
            this.fraSort.Text = "Fields To Sort By";
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.AllowDrag = true;
            this.lstSort.AllowDrop = true;
            this.lstSort.Appearance = 0;
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.MultiSelect = 0;
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(286, 242);
            this.lstSort.Sorted = false;
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 14;
            this.lstSort.ToolTipText = null;
            // 
            // fraReports
            // 
            this.fraReports.Controls.Add(this.cboSavedReport);
            this.fraReports.Controls.Add(this.cmbReport);
            this.fraReports.Controls.Add(this.cmdAdd);
            this.fraReports.Location = new System.Drawing.Point(722, 272);
            this.fraReports.Name = "fraReports";
            this.fraReports.Size = new System.Drawing.Size(318, 190);
            this.fraReports.TabIndex = 7;
            this.fraReports.Text = "Report";
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.AutoSize = false;
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboSavedReport.FormattingEnabled = true;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 80);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(278, 40);
            this.cboSavedReport.TabIndex = 10;
            this.cboSavedReport.ToolTipText = null;
            this.cboSavedReport.Visible = false;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(20, 130);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(278, 40);
            this.cmdAdd.TabIndex = 8;
            this.cmdAdd.Text = "Add Custom report to Library";
            this.cmdAdd.ToolTipText = null;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // fraWhere
            // 
            this.fraWhere.AppearanceKey = "groupBoxNoBorders";
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 574);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(671, 345);
            this.fraWhere.TabIndex = 4;
            this.fraWhere.Text = "Select Search Criteria";
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.AllowSelection = false;
            this.vsWhere.AllowUserToResizeColumns = false;
            this.vsWhere.AllowUserToResizeRows = false;
            this.vsWhere.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsWhere.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsWhere.BackColorBkg = System.Drawing.Color.Empty;
            this.vsWhere.BackColorFixed = System.Drawing.Color.Empty;
            this.vsWhere.BackColorSel = System.Drawing.Color.Empty;
            this.vsWhere.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsWhere.Cols = 10;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsWhere.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsWhere.ColumnHeadersHeight = 30;
            this.vsWhere.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.vsWhere.ColumnHeadersVisible = false;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsWhere.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsWhere.DragIcon = null;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsWhere.FrozenCols = 0;
            this.vsWhere.GridColor = System.Drawing.Color.Empty;
            this.vsWhere.GridColorFixed = System.Drawing.Color.Empty;
            this.vsWhere.Location = new System.Drawing.Point(0, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.OutlineCol = 0;
            this.vsWhere.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsWhere.RowHeightMin = 0;
            this.vsWhere.Rows = 0;
            this.vsWhere.ScrollTipText = null;
            this.vsWhere.ShowColumnVisibilityMenu = false;
            this.vsWhere.Size = new System.Drawing.Size(671, 290);
            this.vsWhere.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 5;
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            // 
            // fraMessage
            // 
            this.fraMessage.AppearanceKey = "groupBoxNoBorders";
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.ForeColor = System.Drawing.Color.FromArgb(0, 0, 192);
            this.fraMessage.Location = new System.Drawing.Point(30, 30);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(483, 15);
            this.fraMessage.TabIndex = 0;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(0, 15);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(483, 15);
            this.Label3.TabIndex = 1;
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Label3.ToolTipText = null;
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuSP11,
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn,
            this.mnuChangeParameters,
            this.mnuSP2,
            this.mnuPrint,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuSP11
            // 
            this.mnuSP11.Index = 1;
            this.mnuSP11.Name = "mnuSP11";
            this.mnuSP11.Text = "-";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 2;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row                                ";
            this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 3;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column          ";
            this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 4;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row           ";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 5;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column      ";
            this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // mnuChangeParameters
            // 
            this.mnuChangeParameters.Index = 6;
            this.mnuChangeParameters.Name = "mnuChangeParameters";
            this.mnuChangeParameters.Text = "Change Parameters";
            this.mnuChangeParameters.Visible = false;
            this.mnuChangeParameters.Click += new System.EventHandler(this.mnuChangeParameters_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 7;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 8;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print Report";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 9;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 10;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddRow
            // 
            this.cmdAddRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddRow.AppearanceKey = "toolbarButton";
            this.cmdAddRow.Location = new System.Drawing.Point(652, 29);
            this.cmdAddRow.Name = "cmdAddRow";
            this.cmdAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdAddRow.Size = new System.Drawing.Size(76, 24);
            this.cmdAddRow.TabIndex = 1;
            this.cmdAddRow.Text = "Add Row";
            this.cmdAddRow.ToolTipText = null;
            this.cmdAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // cmdAddColumn
            // 
            this.cmdAddColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddColumn.AppearanceKey = "toolbarButton";
            this.cmdAddColumn.Location = new System.Drawing.Point(734, 29);
            this.cmdAddColumn.Name = "cmdAddColumn";
            this.cmdAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdAddColumn.Size = new System.Drawing.Size(98, 24);
            this.cmdAddColumn.TabIndex = 2;
            this.cmdAddColumn.Text = "Add Column";
            this.cmdAddColumn.ToolTipText = null;
            this.cmdAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // cmdDeleteRow
            // 
            this.cmdDeleteRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteRow.AppearanceKey = "toolbarButton";
            this.cmdDeleteRow.Location = new System.Drawing.Point(838, 29);
            this.cmdDeleteRow.Name = "cmdDeleteRow";
            this.cmdDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdDeleteRow.Size = new System.Drawing.Size(92, 24);
            this.cmdDeleteRow.TabIndex = 3;
            this.cmdDeleteRow.Text = "Delete Row";
            this.cmdDeleteRow.ToolTipText = null;
            this.cmdDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // cmdDeleteColumn
            // 
            this.cmdDeleteColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteColumn.AppearanceKey = "toolbarButton";
            this.cmdDeleteColumn.Location = new System.Drawing.Point(936, 29);
            this.cmdDeleteColumn.Name = "cmdDeleteColumn";
            this.cmdDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdDeleteColumn.Size = new System.Drawing.Size(114, 24);
            this.cmdDeleteColumn.TabIndex = 4;
            this.cmdDeleteColumn.Text = "Delete Column";
            this.cmdDeleteColumn.ToolTipText = null;
            this.cmdDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.AppearanceKey = "toolbarButton";
            this.cmdClear.Location = new System.Drawing.Point(500, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(146, 24);
            this.cmdClear.TabIndex = 5;
            this.cmdClear.Text = "Clear Search Criteria";
            this.cmdClear.ToolTipText = null;
            this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // frmCustomReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomReport";
            this.Text = "Custom Report";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCustomReport_Load);
            this.Activated += new System.EventHandler(this.frmCustomReport_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomReport_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomReport_KeyPress);
            this.Resize += new System.EventHandler(this.frmCustomReport_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandscape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraLayout)).EndInit();
            this.fraLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraReports)).EndInit();
            this.fraReports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdClear;
        private FCButton cmdDeleteColumn;
        private FCButton cmdDeleteRow;
        private FCButton cmdAddColumn;
        private FCButton cmdAddRow;
    }
}
