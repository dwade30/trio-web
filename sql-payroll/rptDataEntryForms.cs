//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPY0000
{
	/// <summary>
	/// Summary description for rptDataEntryForms.
	/// </summary>
	public partial class rptDataEntryForms : BaseSectionReport
	{
		public rptDataEntryForms()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Data Entry Forms";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDataEntryForms InstancePtr
		{
			get
			{
				return (rptDataEntryForms)Sys.GetInstance(typeof(rptDataEntryForms));
			}
		}

		protected rptDataEntryForms _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsTemp?.Dispose();
                rsData = null;
                rsTemp = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDataEntryForms	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		private clsDRWrapper rsTemp = new clsDRWrapper();
		private string strTemp = "";
		int intPageNumber = 0;
		int intRow = 0;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				txtEmployee.Text = fecherFoundation.Strings.Trim(rsData.Get_Fields("EmployeeNumber") + "  " + rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName") + " " + rsData.Get_Fields("middlename") + " " + rsData.Get_Fields_String("Desig"));
				txtPeriods.Text = Strings.Format(rsData.Get_Fields_Double("PeriodTaxes"), "0") + "/" + Strings.Format(rsData.Get_Fields("PeriodDEDs"), "0");
				txtHours.Text = Strings.Format(rsData.Get_Fields_Double("HrsWk"), "0.00");
				rsTemp.OpenRecordset("Select CD,BaseRate,PayCode,Rate from tblPayrollDistribution INNER JOIN tblPayCodes ON tblPayrollDistribution.CD = tblPayCodes.ID where EmployeeNumber = '" + rsData.Get_Fields("EmployeeNumber") + "' and RecordNumber = 1");
				if (!rsTemp.EndOfFile())
				{
					if (FCConvert.ToInt32(rsTemp.Get_Fields_String("PayCode")) == 0)
					{
						txtRate.Text = Strings.Format(rsTemp.Get_Fields_Decimal("BaseRate"), "$0.00");
					}
					else
					{
						txtRate.Text = Strings.Format(rsTemp.Get_Fields("Rate"), "$0.00");
					}
				}
				if (!rsData.EndOfFile())
					rsData.MoveNext();
				eArgs.EOF = false;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: intPageNumber As object	OnWrite
			// vbPorter upgrade warning: txtMuniname As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txttitle As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			// vbPorter upgrade warning: intRow As object	OnWrite
			// NOT REALLY SURE WHY WE ALLOW THIS
			// On Error Resume Next
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			intPageNumber = 0;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			//txtTitle.Text = modCustomReport.strCustomTitle;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			string strControl = "";
			rsData.OpenRecordset("Select Description from tblPayCategories");
			//FC:FINAL:DDU:#i2346 - get just needed Label controls in the list which are needed to set their text
			List<GrapeCity.ActiveReports.SectionReportModel.Label> controls = new List<GrapeCity.ActiveReports.SectionReportModel.Label>();
			for (int i = 0; i < this.PageHeader.Controls.Count; i++)
			{
				if (this.PageHeader.Controls[i].GetType() == typeof(GrapeCity.ActiveReports.SectionReportModel.Label))
				{
					controls.Add((GrapeCity.ActiveReports.SectionReportModel.Label)this.PageHeader.Controls[i]);
				}
			}
			for (intCounter = 9; intCounter <= 29; intCounter++)
			{
				strControl = "Label" + FCConvert.ToString(intCounter);
				foreach (GrapeCity.ActiveReports.SectionReportModel.Label ControlName in controls)
				{
					if (ControlName.Name == strControl)
					{
						if (!rsData.EndOfFile())
						{
							ControlName.Text = FCConvert.ToString(rsData.Get_Fields("Description"));
						}
						else
						{
							ControlName.Text = "";
						}
						break;
					}
				}
				rsData.MoveNext();
			}
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGlobalVariables.DEFAULTDATABASE);
			if (rsData.EndOfFile())
			{
				modGlobalRoutines.UpdatePayrollStepTable("DEForms");
				modDavesSweetCode.Statics.blnReportCompleted = true;
				MessageBox.Show("There are no records to print", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// MATTHEW NOT SURE WHY IT DOES NOT LIKE THE UNLOAD ME BUT IT WAS GIVING AND AUTOMATION ERROR
				// ON THE EXIT SUB OF THIS ROUTINE. CHANGED IT TO ME.STOP AND EVERYTHING SEEMED TO BE FINE.
				// COREY HELPED ON THIS ONE SO IF I'M NOT HERE SEE HIM. 4/13/2004
				// Unload Me
				this.Stop();
				return;
			}
			// NEED TO MAKE SURE THAT ALL OF THE PAY CATS ARE SHOWING ON THIS REPORT
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			// NEED TO MA
			intRow = 0;
			modPrintToFile.SetPrintProperties(this);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.gtypeFullSetReports.boolFullSet == false)
			{
				modGlobalRoutines.UpdatePayrollStepTable("DEForms");
			}
			else
			{
				modDavesSweetCode.Statics.blnDataEntryCompleted = true;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtMuniname As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: intPageNumber As object	OnWrite
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			txtMuniName.Text = modGlobalVariables.Statics.gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPageNumber += 1;
			txtPage.Text = "Page " + intPageNumber;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
		}

		
	}
}
