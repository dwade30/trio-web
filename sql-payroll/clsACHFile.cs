﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsACHFile
	{
		//=========================================================
		private clsACHFileInfo tFileInfo = new clsACHFileInfo();
		private clsACHFileHeaderRecord tFileHeader = new clsACHFileHeaderRecord();
		private clsACHFileControlRecord tfilecontrol = new clsACHFileControlRecord();
		private FCCollection tBatches = new FCCollection();
		private FCCollection tFillers = new FCCollection();

		public clsACHFileHeaderRecord FileHeader()
		{
			clsACHFileHeaderRecord FileHeader = null;
			FileHeader = tFileHeader;
			return FileHeader;
		}

		public clsACHFileControlRecord FileControl()
		{
			clsACHFileControlRecord FileControl = null;
			FileControl = tfilecontrol;
			return FileControl;
		}

		public clsACHFileInfo FileInfo()
		{
			clsACHFileInfo FileInfo = null;
			FileInfo = tFileInfo;
			return FileInfo;
		}

		public FCCollection Batches()
		{
			return tBatches;
		}

		public FCCollection Fillers()
		{
			return tFillers;
		}

		public void AddBatch(ref clsACHBatch tBatch)
		{
			tBatches.Add(tBatch);
			tBatch.BatchNumber = tBatches.Count;
			tBatch.BatchHeader().BatchNumber = tBatch.BatchNumber;
		}

		public void CreateFillers(int intNumFillers)
		{
			int x;
			for (x = tFillers.Count - 1; x >= 0; x--)
			{
				tFillers.Remove(x);
			}
			// x
			clsACHBlockFiller tBlockFill;
			for (x = 1; x <= intNumFillers; x++)
			{
				tBlockFill = new clsACHBlockFiller();
				tFillers.Add(tBlockFill);
			}
			// x
		}

		public void BuildFileControl()
		{
			double dblDebits = 0;
			double dblCredits = 0;
			double dblHash = 0;
			int intEntries = 0;
			int intBlocks;
			int intBlockRecs = 0;
			foreach (clsACHBatch tBatch in tBatches)
			{
				dblDebits += tBatch.Debits;
				dblCredits += tBatch.Credits;
				dblHash += tBatch.Hash;
				intEntries += tBatch.BatchControl().EntryAddendaCount;
				intBlockRecs += tBatch.BlockRecords;
			}
			// tBatch
			tfilecontrol.TotalCredit = dblCredits;
			tfilecontrol.TotalDebit = dblDebits;
			tfilecontrol.Hash = dblHash;
			tfilecontrol.BatchCount = tBatches.Count;
			tfilecontrol.EntryAddendaCount = intEntries;
			intBlockRecs += 2;
			// file header and control
			intBlocks = FCConvert.ToInt16(intBlockRecs / 10);
			if (intBlockRecs % 10 > 0)
			{
				intBlocks += 1;
			}
			tfilecontrol.BlockCount = intBlocks;
			CreateFillers((10 - (intBlockRecs % 10)) % 10);
		}
	}
}
