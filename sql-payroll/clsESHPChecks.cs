﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPY0000
{
	public class clsESHPChecks
	{
		//=========================================================
		private FCCollection lstPayChecks = new FCCollection();
		private int intCurrentIndex;
		private DateTime dtPayDate;
		private int intPayrun;

		public clsESHPChecks() : base()
		{
			intCurrentIndex = -1;
		}

		public DateTime PayDate
		{
			set
			{
				dtPayDate = value;
			}
			get
			{
				DateTime PayDate = System.DateTime.Now;
				PayDate = dtPayDate;
				return PayDate;
			}
		}

		public int Payrun
		{
			set
			{
				intPayrun = value;
			}
			get
			{
				int Payrun = 0;
				Payrun = intPayrun;
				return Payrun;
			}
		}

		public void ClearList()
		{
			if (!(lstPayChecks == null))
			{
				foreach (clsESHPCheckRecord tCheck in lstPayChecks)
				{
					lstPayChecks.Remove(1);
				}
				// tCheck
			}
		}

		public void AddItem(ref clsESHPCheckRecord tItem)
		{
			if (!(tItem == null))
			{
				lstPayChecks.Add(tItem);
			}
		}

		public void MoveFirst()
		{
			if (!(lstPayChecks == null))
			{
				if (!FCUtils.IsEmpty(lstPayChecks))
				{
					if (lstPayChecks.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(lstPayChecks))
			{
				if (intCurrentIndex > lstPayChecks.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= lstPayChecks.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > lstPayChecks.Count)
						{
							intReturn = -1;
							break;
						}
						else if (lstPayChecks[intCurrentIndex] == null)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}

		public clsESHPCheckRecord GetCurrentCheck()
		{
			clsESHPCheckRecord GetCurrentCheck = null;
			clsESHPCheckRecord tCheck;
			tCheck = null;
			if (!FCUtils.IsEmpty(lstPayChecks))
			{
				if (intCurrentIndex > 0)
				{
					if (!(lstPayChecks[intCurrentIndex] == null))
					{
						tCheck = lstPayChecks[intCurrentIndex];
					}
				}
			}
			GetCurrentCheck = tCheck;
			return GetCurrentCheck;
		}

		public clsESHPCheckRecord GetCheckByIndex(ref int intindex)
		{
			clsESHPCheckRecord GetCheckByIndex = null;
			clsESHPCheckRecord tCheck;
			tCheck = null;
			if (!FCUtils.IsEmpty(lstPayChecks) && intindex > 0)
			{
				if (!(lstPayChecks[intindex] == null))
				{
					intCurrentIndex = intindex;
					tCheck = lstPayChecks[intindex];
				}
			}
			GetCheckByIndex = tCheck;
			return GetCheckByIndex;
		}

		public int ItemCount()
		{
			int ItemCount = 0;
			if (!FCUtils.IsEmpty(lstPayChecks))
			{
				ItemCount = lstPayChecks.Count;
			}
			else
			{
				ItemCount = 0;
			}
			return ItemCount;
		}
	}
}
