﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class clsESHPEmployeeTax
	{
		//=========================================================
		private string strSSN = string.Empty;
		private string strDescription = "";
		private string strSortSequence = "";
		private string strFilingStatus = "";
		private int intExemptions;
		private int intAdditionalExemptions;
		private double dblAdditionalAmount;
		private double dblAdditionalPercent;
		private string strStartDate = "";

		public string StartDate
		{
			get
			{
				string StartDate = "";
				StartDate = strStartDate;
				return StartDate;
			}
			set
			{
				strStartDate = value;
			}
		}

		public string Description
		{
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
			set
			{
				strDescription = value;
			}
		}

		public string SortSequence
		{
			get
			{
				string SortSequence = "";
				SortSequence = strSortSequence;
				return SortSequence;
			}
			set
			{
				strSortSequence = value;
			}
		}

		public string FilingStatus
		{
			get
			{
				string FilingStatus = "";
				FilingStatus = strFilingStatus;
				return FilingStatus;
			}
			set
			{
				strFilingStatus = value;
			}
		}

		public int Exemptions
		{
			get
			{
				int Exemptions = 0;
				Exemptions = intExemptions;
				return Exemptions;
			}
			set
			{
				intExemptions = value;
			}
		}

		public int AdditionalExemptions
		{
			get
			{
				int AdditionalExemptions = 0;
				AdditionalExemptions = intAdditionalExemptions;
				return AdditionalExemptions;
			}
			set
			{
				intAdditionalExemptions = value;
			}
		}

		public double AdditionalAmount
		{
			get
			{
				double AdditionalAmount = 0;
				AdditionalAmount = dblAdditionalAmount;
				return AdditionalAmount;
			}
			set
			{
				dblAdditionalAmount = value;
			}
		}

		public double AdditionalPercent
		{
			set
			{
				dblAdditionalPercent = value;
			}
			get
			{
				double AdditionalPercent = 0;
				AdditionalPercent = dblAdditionalPercent;
				return AdditionalPercent;
			}
		}

		public string SocialSecurityNumber
		{
			set
			{
				strSSN = value;
			}
			get
			{
				string SocialSecurityNumber = "";
				SocialSecurityNumber = strSSN;
				return SocialSecurityNumber;
			}
		}
	}
}
