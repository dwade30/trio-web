﻿using System;
using Xunit;

namespace CollectionsTests
{
    public class modCLCalculationsTests
    {
        [Fact]
        public void CalculatesNoInterestWhenInterestDateInFuture()
        {
            Assert.Equal(0, Global.modCLCalculations.CalculateInterest(2000, DateTime.Now.AddYears(1), DateTime.Now, .7, 365));
        }

        [Fact]
        public void CalculatesInterestWhenInterestDateInPast()
        {
            Assert.Equal(1400, Global.modCLCalculations.CalculateInterest(2000, new DateTime(2018, 10, 10), new DateTime(2019,10,10), .7, 365));
        }

        [Fact]
        public void CalculatesInterestForOverPaymentsWhenOverPaid()
        {
            double perDiem = 0;
            Assert.Equal(-1400, Global.modCLCalculations.CalculateInterest(-2000, new DateTime(2018, 10, 10), new DateTime(2019, 10, 10), .7, 365, 0, .7, ref perDiem));
        }
    }
}
