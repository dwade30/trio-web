﻿using System;
using SharedApplication.TaxCollections.Models;
using Xunit;
namespace CollectionsTests
{
    public class PropertyTaxLienTests
    {
        [Fact]
        public void LienSummaryCorrectWhenNoPayments()
        {
            var lien = new PropertyTaxLien()
            {
                Book = "1",
                DateCreated = DateTime.Now,
                Id = 1,
                InterestAppliedThroughDate = DateTime.Now,
                LienSummary =
                {
                    CostsPaid = 0,
                    OriginalCostsCharged = 10,
                    AdjustedCostsCharged = 10,
                    OriginalInterestCharged = 0,
                    AdjustedInterestCharged = 0,
                    InterestPaid =  0,
                    MaturityFee = 0,
                    PreLienInterestPaid = 0,
                    OriginalPreLienInterestCharged = 15,
                    AdjustedPreLienInterestCharged = 15,
                    OriginalPrincipalCharged = 100,
                    AdjustedPrincipalCharged = 100,
                    PrincipalPaid =  0
                },
                RateRecord = new TaxRateInfo(),
                Page = "1",
                RateId = 1                
            };
            Assert.Equal(125,lien.LienSummary.GetNetOwed());
        }

        [Fact]
        public void LienSummaryCorrectForAbatements()
        {
            var lien = new PropertyTaxLien()
            {
                Book = "1",
                DateCreated = DateTime.Now,
                Id = 2,
                InterestAppliedThroughDate = DateTime.Now,
                RateRecord = new TaxRateInfo(),
                Page = "1",
                RateId = 1,
                LienSummary =
                {
                    OriginalPrincipalCharged = 100,
                    AdjustedPrincipalCharged = 100,
                    OriginalCostsCharged = 25,
                    AdjustedCostsCharged = 25,
                    OriginalInterestCharged = 15,
                    AdjustedInterestCharged = 15,
                    MaturityFee = 12,
                    OriginalPreLienInterestCharged = 13,
                    AdjustedPreLienInterestCharged = 13
                }
            };
            var payments = new Payments();
            payments.AddPayment(new PaymentRec()
                {
                    Account = 1,
                    ActualSystemDate = new DateTime(2018,1,1),
                    BillCode = "L",
                    BillId = 2,
                    Id = 1,
                    Year = 20161,
                    Reference = "",
                    Code = "A",
                    Principal = 20,
                    EffectiveInterestDate = new DateTime(2018,1,1),
                    RecordedTransactionDate = new DateTime(2018,1,1),
                    PreLienInterest = 5,
                    LienCost = 11,
                    CurrentInterest = 8
                });
            lien.ApplyPayments(payments);
            Assert.Equal(80,lien.LienSummary.AdjustedPrincipalCharged);
            Assert.Equal(100,lien.LienSummary.OriginalPrincipalCharged);
            Assert.Equal(26,lien.LienSummary.GetNetCosts());
            Assert.Equal(7,lien.LienSummary.AdjustedInterestCharged);
            Assert.Equal(8, lien.LienSummary.AdjustedPreLienInterestCharged);
            Assert.Equal(121,lien.LienSummary.GetNetOwed());
        }

        [Fact]
        public void LienSummaryCorrectWhenPaymentsExist()
        {
            var lien = new PropertyTaxLien()
            {
                Book = "1",
                DateCreated = DateTime.Now,
                Id = 2,
                InterestAppliedThroughDate = DateTime.Now,
                RateRecord = new TaxRateInfo(),
                Page = "1",
                RateId = 1,
                LienSummary =
                {
                    OriginalPrincipalCharged = 100,
                    AdjustedPrincipalCharged = 100,
                    OriginalPreLienInterestCharged = 13,
                    AdjustedPreLienInterestCharged = 13,
                }
            };
            var payments = new Payments();
            payments.AddPayment(
                new PaymentRec()
                {
                    Account = 1,
                    ActualSystemDate = new DateTime(2018, 1, 1),
                    BillId = 2,
                    BillCode = "L",
                    Code = "I",
                    CurrentInterest = -18,
                    ChargedInterestdate = new DateTime(2018, 1, 1),
                    EffectiveInterestDate = new DateTime(2018, 1, 1),
                    RecordedTransactionDate = new DateTime(2018, 1, 1),

                });

            payments.AddPayment(
                new PaymentRec()
                {
                    Account = 1,
                    ActualSystemDate = new DateTime(2018, 1, 1),
                    BillId = 2,
                    BillCode = "L",
                    Code = "P",
                    Principal = 10,
                    PreLienInterest = 13,
                    CurrentInterest = 18,
                    EffectiveInterestDate = new DateTime(2018, 1, 1),
                    RecordedTransactionDate = new DateTime(2018, 1, 1)
                });
            lien.ApplyPayments(payments);
            Assert.Equal(90,lien.LienSummary.GetNetOwed());
            Assert.Equal(18,lien.LienSummary.AdjustedInterestCharged);
        }
    }
}