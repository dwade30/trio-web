﻿using System;
using SharedApplication.TaxCollections.Models;
using Xunit;

namespace CollectionsTests
{
    public class PaymentTests
    {
        [Fact]
        public void GetPaymentsSummaryReturnsZeroSumsWhenNoPayments()
        {
            var payments = new Payments();
            Assert.True(IsZeroPaymentSummary(payments.GetPaymentsSummary()));
        }
        private Boolean IsZeroPaymentSummary(PaymentSummary summary)
        {
            return (summary.Costs == 0) && (summary.Interest == 0) && (summary.Principal == 0);
        }

        [Fact]
        public void GetPaymentsSummaryReturnsCorrectSumsForRegularBills()
        {
            var payments = new Payments();
            payments.AddPayment(new PaymentRec()
            {
                Account = 1,
                ActualSystemDate = new DateTime(2018, 10, 1),
                BillCode = "R",
                BillId = 1,
                BudgetaryAccountNumber = "",
                CashDrawer = "Y",
                Code = "P",
                CurrentInterest = 5M,
                LienCost = 0,
                EffectiveInterestDate = new DateTime(2018, 10, 1),
                Period = "A",
                PreLienInterest = 0,
                Principal = 100M,
                ReceiptNumber = 1,
                RecordedTransactionDate = new DateTime(2018, 10, 1),
                IsReversal = false,
                Year = 20181
            });
            payments.AddPayment(new PaymentRec()
            {
                Account = 1,
                ActualSystemDate = new DateTime(2018, 10, 1),
                BillCode = "R",
                BillId = 1,
                BudgetaryAccountNumber = "",
                Code = "I",
                CurrentInterest = -5M,
                LienCost = 0,
                EffectiveInterestDate = new DateTime(2018, 10, 1),
                Period = "1",
                PreLienInterest = 0,
                Principal = 0,
                ReceiptNumber = 1,
                RecordedTransactionDate = new DateTime(2018, 10, 1),
                IsReversal = false,
                Year = 20181
            });
            payments.AddPayment(new PaymentRec()
            {
                Account = 1,
                ActualSystemDate = new DateTime(2018, 10, 1),
                BillCode = "R",
                BillId = 1,
                BudgetaryAccountNumber = "",
                Code = "I",
                CurrentInterest = -6M,
                LienCost = 0,
                EffectiveInterestDate = new DateTime(2018, 10, 1),
                Period = "1",
                PreLienInterest = 0,
                Principal = 0,
                ReceiptNumber = 1,
                RecordedTransactionDate = new DateTime(2018, 10, 1),
                IsReversal = false,
                Year = 20181
            });
            payments.AddPayment(new PaymentRec()
            {
                Account = 1,
                ActualSystemDate = new DateTime(2018, 10, 1),
                BillCode = "R",
                BillId = 1,
                BudgetaryAccountNumber = "",
                Code = "3",
                CurrentInterest = 0,
                LienCost = 30M,
                EffectiveInterestDate = new DateTime(2018, 10, 1),
                Period = "A",
                PreLienInterest = 0,
                Principal = 0,
                ReceiptNumber = 1,
                RecordedTransactionDate = new DateTime(2018, 10, 1),
                IsReversal = false,
                Year = 20181
            });
            var paymentSummary = payments.GetPaymentsSummary();
            Assert.Equal(100M, paymentSummary.Principal);
            Assert.Equal(-6M, paymentSummary.Interest);
            Assert.Equal(30M, paymentSummary.Costs);
        }

        [Fact]
        public void GetPaymentsSummaryDoesNotIncludeAbatements()
        {
            var payments = new Payments();
            payments.AddPayment(new PaymentRec()
            {
                Account = 1,
                ActualSystemDate = new DateTime(2018, 11, 1),
                BillCode = "R",
                BillId = 1,
                Code = "P",
                CurrentInterest = 0,
                EffectiveInterestDate = new DateTime(2018, 11, 1),
                Id = 1,
                LienCost = 0,
                Period = "A",
                Principal = 100M,
                PreLienInterest = 0,
                RecordedTransactionDate = new DateTime(2018, 11, 1),
                IsReversal = false,
                Year = 20181
            });
            payments.AddPayment(new PaymentRec()
            {
                Account = 1,
                ActualSystemDate = new DateTime(2018, 10, 15),
                RecordedTransactionDate = new DateTime(2018, 10, 15),
                BillCode = "R",
                BillId = 1,
                Code = "A",
                CurrentInterest = 0,
                EffectiveInterestDate = new DateTime(2018, 10, 15),
                Id = 2,
                LienCost = 0,
                Period = "A",
                Principal = 50M,
                PreLienInterest = 0,
                IsReversal = false,
                Year = 20181
            });
            Assert.Equal(100M, payments.GetPaymentsSummary().Principal);
        }
    }
}