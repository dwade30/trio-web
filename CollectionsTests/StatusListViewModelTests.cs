﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TWCL0000;
using Xunit;
using NSubstitute;
using SharedApplication;

namespace CollectionsTests
{
    public class StatusListViewModelTests
    {

        //[Fact]
        //public void SetReportTypeRaisesOnSortOptionsChangedWhenReportChanges()
        //{
        //    var eventWasRaised = false;
        //    var newReportType = "Account";
        //    EventHandler testDelegate = delegate { eventWasRaised = true; };
        //    var reportRepo = Substitute.For<ISavedStatusReportsRepository>();
        //    var billRepo = Substitute.For<IBillingMasterRepository>();
        //    var trioContextFactory = Substitute.For<Itrio
        //    var reportDescriptionList = new List<DescriptionIDPair>() { new DescriptionIDPair() { Description = "FakeReportOne", ID = 1 }, new DescriptionIDPair() { Description = "FakeReportTwo", ID = 2 } };
        //    reportRepo.GetDescriptions(newReportType).Returns(reportDescriptionList);

        //    var statusListViewModel = new StatusListViewModel(PropertyTaxBillType.Real, reportRepo, billRepo);

        //    statusListViewModel.SortOptionsChanged += testDelegate;
        //    statusListViewModel.SetReportType(newReportType);
        //    Assert.True(eventWasRaised);
        //}

        //[Fact]
        //public void SetReportTypeDoesNotRaiseOnSortOptionsChangedWhenReportIsTheSame()
        //{
        //    var eventWasntRaised = true;
        //    var newReportType = "Account";
        //    var sameReportType = newReportType;
        //    EventHandler testDelegate = delegate { eventWasntRaised = false; };
        //    var reportRepo = Substitute.For<ISavedStatusReportsRepository>();
        //    var billRepo = Substitute.For<IBillingMasterRepository>();
        //    var reportDescriptionList = new List<DescriptionIDPair>() { new DescriptionIDPair() { Description = "AFakeReport", ID = 1 }, new DescriptionIDPair() { Description = "AnotherFakeReport", ID = 4 } };
        //    reportRepo.GetDescriptions(Arg.Any<String>()).Returns(reportDescriptionList);
        //    var statusListViewModel = new StatusListViewModel(PropertyTaxBillType.Real, reportRepo, billRepo);
        //    statusListViewModel.SortOptionsChanged += testDelegate;
        //    statusListViewModel.SetReportType(newReportType);
        //    eventWasntRaised = true;
        //    statusListViewModel.SetReportType(sameReportType);
        //    Assert.True(eventWasntRaised);
        //}
    }
}
