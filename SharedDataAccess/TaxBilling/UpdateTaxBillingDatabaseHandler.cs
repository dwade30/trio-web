﻿using SharedApplication.Messaging;
using SharedApplication.TaxBilling;

namespace SharedDataAccess.TaxBilling
{
    public class UpdateTaxBillingDatabaseHandler : CommandHandler<UpdateTaxBillingDatabase,bool>
    {
        protected override bool Handle(UpdateTaxBillingDatabase command)
        {
            var updater = new cTaxBillingDB(command.SqlConfigInfo,command.DataEnvironment);
            return updater.CheckVersion();
        }
    }
}