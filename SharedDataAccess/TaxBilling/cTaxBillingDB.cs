﻿using System.Runtime.Remoting.Messaging;
using SharedApplication.Models;
using SharedApplication.TaxBilling;
using SharedApplication.Versioning;

namespace SharedDataAccess.TaxBilling
{
    public class cTaxBillingDB : cDatabaseVersionUpdater, ITaxBillingDB
    {
        public cTaxBillingDB(SQLConfigInfo sqlInfo, string dataEnvironment)
        {
            defaultDatabaseName = "TaxBilling";
            DataEnvironment = dataEnvironment;
            dbInfo = sqlInfo;
        }

       
        protected override bool PerformChecks(cVersionInfo currentVersion)
        {
            //Example:
            //CheckFunction(TestFunction, currentVersion,new cVersionInfo() {Major = 6, Minor = 0, Revision = 1, Build = 0});
            return true;
        }
    }
}