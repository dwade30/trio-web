﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.RealEstate
{
    public class RealEstateEmptyContext : IRealEstateContext
    {
        public IQueryable<REDefault> Defaults => new List<REDefault>().AsQueryable();
        public IQueryable<Mvrreport> MvrReports => new List<Mvrreport>().AsQueryable();
        public IQueryable<TranCode> TranCodes => new List<TranCode>().AsQueryable();
        public IQueryable<REMaster> REMasters => new List<REMaster>().AsQueryable();
        public IQueryable<ExemptCode> ExemptCodes => new List<ExemptCode>().AsQueryable();
        public IQueryable<BookPage> BookPages => new List<BookPage>().AsQueryable();
        public IQueryable<LandType> LandTypes => new List<LandType>().AsQueryable();
        public IQueryable<PreviousOwner> PreviousOwners => new List<PreviousOwner>().AsQueryable();
        public IQueryable<ReportTitle> ReportTitles => new List<ReportTitle>().AsQueryable();
        public IQueryable<Summrecord> SummationRecords => new List<Summrecord>().AsQueryable();
        public IQueryable<Taxratecalculation> TaxRateCalculations => new List<Taxratecalculation>().AsQueryable();
        public IQueryable<PictureRecord> PictureRecords => new List<PictureRecord>().AsQueryable();
        public IQueryable<REAccountPartyAddressView> REAccountPartyAddressViews => new List<REAccountPartyAddressView>().AsQueryable();
        public IQueryable<Srmaster> SRMasters => new List<Srmaster>().AsQueryable();
    }
}