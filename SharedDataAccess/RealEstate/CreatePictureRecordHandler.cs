﻿using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate
{
    public class CreatePictureRecordHandler : CommandHandler<CreatePictureRecord,int>
    {
        private RealEstateContext reContext;

        public CreatePictureRecordHandler(IRealEstateContext reContext)
        {
            this.reContext = (RealEstateContext)reContext;
        }
        protected override int Handle(CreatePictureRecord command)
        {
            var pictureRecord = new PictureRecord()
            {
                Account = command.Account,
                Card = command.Card,
                Description = command.Description,
                DocumentIdentifier = command.DocumentIdentifier,
                PicNum = command.SequenceNumber,
                PictureLocation = command.FileName
            };
            reContext.PictureRecords.Add(pictureRecord);
            reContext.SaveChanges();
            return pictureRecord.Id;
        }
    }
}