﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;

namespace SharedDataAccess.RealEstate
{
    public class RealEstateAccountExistsHandler : CommandHandler<RealEstateAccountExists,bool>
    {
        private IRealEstateContext reContext;
        public RealEstateAccountExistsHandler(IRealEstateContext reContext)
        {
            this.reContext = reContext;
        }
        protected override bool Handle(RealEstateAccountExists command)
        {
            if (reContext.REMasters.Any(r => r.Rsaccount == command.Account))
            {
                return true;
            }

            return false;
        }
    }
}