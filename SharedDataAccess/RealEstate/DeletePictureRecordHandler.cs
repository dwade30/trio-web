﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;

namespace SharedDataAccess.RealEstate
{
    public class DeletePictureRecordHandler : CommandHandler<DeletePictureRecord>
    {
        private RealEstateContext reContext;
        public DeletePictureRecordHandler(IRealEstateContext reContext)
        {
            this.reContext = (RealEstateContext)reContext;
        }
        protected override void Handle(DeletePictureRecord command)
        {
            reContext.Database.ExecuteSqlCommand("Delete from PictureRecord where id = " + command.Id);
        }
    }
}