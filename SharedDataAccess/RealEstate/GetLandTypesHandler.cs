﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;

namespace SharedDataAccess.RealEstate
{
    public class GetLandTypesHandler : CommandHandler<GetLandTypes,LandTypes>
    {
        private IRealEstateContext realEstateContext;
        public GetLandTypesHandler(IRealEstateContext realEstateContext)
        {
            this.realEstateContext = realEstateContext;
        }
        protected override LandTypes Handle(GetLandTypes command)
        {
            var landTypes = new LandTypes();
            var landQuery = realEstateContext.LandTypes.ToList();
            landTypes.SetRangeTypes(landQuery);
            return landTypes;
        }
    }
}