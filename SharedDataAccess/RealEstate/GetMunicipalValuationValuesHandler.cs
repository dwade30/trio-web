using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Enums;
using SharedApplication.RealEstate.Extensions;
using SharedApplication.RealEstate.Models;
using SharedApplication.RealEstate.MVR;

namespace SharedDataAccess.RealEstate
{
    public class GetMunicipalValuationReturnHandler : CommandHandler<GetMunicipalValuationReturn, MunicipalValuationReturn>
    {
        private const int HomesteadExemptionAmount = 25000;
        private const int vetExempt = 6000;
        private const int wwiVetExempt = 7000;
        private const int paraplegicExempt = 50000;
        private IRealEstateContext realEstateContext;
        private MunicipalValuationReturn ValuationReturn = new MunicipalValuationReturn();
        private GlobalRealEstateSettings realEstateSettings;
        private IPersonalPropertyContext personalPropertyContext;
        private IGlobalActiveModuleSettings moduleSettings;
        private DataEnvironmentSettings environmentSettings;
        private LandTypes landTypes;
        public GetMunicipalValuationReturnHandler(IRealEstateContext realEstateContext, IPersonalPropertyContext personalPropertyContext, IGlobalActiveModuleSettings moduleSettings, GlobalRealEstateSettings realEstateSettings, DataEnvironmentSettings environmentSettings, LandTypes landTypes)
        {
            this.realEstateContext = realEstateContext;
            this.personalPropertyContext = personalPropertyContext;
            this.realEstateSettings = realEstateSettings;
            this.moduleSettings = moduleSettings;
            this.environmentSettings = environmentSettings;
            this.landTypes = landTypes;
        }

        protected override MunicipalValuationReturn Handle(GetMunicipalValuationReturn command)
        {
            LoadMVR(command.TownNumber);
            if (command.CalculateCurrent)
            {
                CalculateMVR();
            }
            return ValuationReturn;
        }

        private void LoadMVR(int townNumber)
        {
            var mvrValues = realEstateContext.MvrReports.Where(m => m.TownNumber == townNumber).ToList();
            var taxratecalculation = realEstateContext.TaxRateCalculations.FirstOrDefault(m => m.TownNumber == townNumber);
            ValuationReturn.TownNumber = townNumber;
            ValuationReturn.CertifiedRatio = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.CertifiedRatio)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.County =
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.County)?.DataValue;
            ValuationReturn.Municipality =
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.Municipality)?.DataValue;
            ValuationReturn.CommitmentDate =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.CommitmentDate)?.DataValue
                    .ToDate()).GetValueOrDefault();
            FillRealEstate(mvrValues);
            FillPersonalProperty(mvrValues);
            FillOtherTaxInformation(mvrValues);
            FillHomestead(mvrValues);
            FillBete(mvrValues);
            FillTIF(mvrValues);
            FillExcise(mvrValues);
            FillElectricalGeneration(mvrValues);
            FillForestTreeGrowth(mvrValues);
            FillFarmAndOpenSpace(mvrValues);
            FillWaterfront(mvrValues);
            FillExemptProperty(mvrValues);
            FillVeteranExemptions(mvrValues);
            FillMunicipalRecords(mvrValues, taxratecalculation);
            FillValuationInformation(mvrValues);
        }

        private void FillRealEstate(List<Mvrreport> mvrValues)
        {
            ValuationReturn.RealEstate.TaxableBuilding =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TaxableBuilding)?.DataValue
                    .ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.RealEstate.TaxableLand = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TaxableLand)?.DataValue.ToIntegerValue()).GetValueOrDefault();
        }

        private void FillPersonalProperty(List<Mvrreport> mvrValues)
        {
            ValuationReturn.PersonalProperty.BusinessEquipment = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FurnitureFixtures)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.PersonalProperty.ProductionMachineryAndEquipment =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MachineryAndEquipment)?.DataValue
                    .ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.PersonalProperty.OtherPersonalProperty =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.OtherPersonalProperty)?.DataValue
                    .ToIntegerValue()).GetValueOrDefault();
        }

        private void FillOtherTaxInformation(List<Mvrreport> mvrValues)
        {
            ValuationReturn.OtherTaxInformation.TaxLevy =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TaxLevy)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.OtherTaxInformation.TaxRate = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TaxRate)?.DataValue.ToDecimalValue()).GetValueOrDefault();
        }

        private void FillHomestead(List<Mvrreport> mvrValues)
        {
            ValuationReturn.Homestead.NumberOfFullHomesteadsGranted =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.HomesteadReimTotFullGranted)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.Homestead.NumberOfFullyExemptedHomesteads =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.HomesteadReimTotFullExemptGranted)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.Homestead.TotalValueOfFullyExemptedHomesteads =
                (mvrValues.FirstOrDefault(m =>
                        m.Code == (int)MunicipalValuationCode.HomesteadReimTotValueFullExemptGranted)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.Homestead.TotalAssessedValueOfAllHomesteadProperty =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.HomesteadReimTotAssessed)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
        }

        private void FillBete(List<Mvrreport> mvrValues)
        {
            ValuationReturn.Bete.ApplicationsApproved =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TotalBETEApproved)?.DataValue
                    .ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.Bete.ApplicationsProcessed = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.NumberBete)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.Bete.TotalBeteInTIF = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TIFBete)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.Bete.TotalBeteQualifiedExempt =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TotalBete)?.DataValue
                    .ToIntegerValue()).GetValueOrDefault();
        }

        private void FillTIF(List<Mvrreport> mvrValues)
        {
            ValuationReturn.TIF.BeteRevenueDeposited = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TIFBeteDepositied)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.TIF.CapturedAssessedValue = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TifAssessed)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.TIF.RevenueDeposited =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TifTaxRevenue)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.TIF.TifIncrease =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TIFIncrease)?.DataValue
                    .ToIntegerValue()).GetValueOrDefault();
        }


        private void FillExcise(List<Mvrreport> mvrValues)
        {
            ValuationReturn.Excise.MVExcise =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExciseCollected)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.Excise.WatercraftExcise =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.WaterCraftExciseCollected)
                    ?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.Excise.Fiscal = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExciseCalendarFiscal)
                           ?.DataValue.ToLower() == "fiscal";
        }

        private void FillElectricalGeneration(List<Mvrreport> mvrValues)
        {
            ValuationReturn.ElectricalGeneration.DistributionAndTransmissionLines =
                (mvrValues.FirstOrDefault(m =>
                        m.Code == (int)MunicipalValuationCode.IndustrialTransmissionDistributionLines)?.DataValue
                    .ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.ElectricalGeneration.GenerationFacilities =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ElectricalGenerationFacilities)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
        }

        private void FillForestTreeGrowth(List<Mvrreport> mvrValues)
        {
            ValuationReturn.ForestTreeGrowth.AveragePerAcreUnitValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthPerAcreUndeveloped)
                    ?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.HardwoodAcreage =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthHardAcreage)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.MixedwoodAcreage =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthMixedAcreage)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.SoftwoodAcreage =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthSoftAcreage)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.NumberOfParcelsClassified =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthNumberParcels)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.TotalAssessedValuation =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthTotalAssessed)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.SoftwoodRate =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthSoftPerAcre)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.MixedWoodRate =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthMixedPerAcre)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.HardwoodRate =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.TreeGrowthHardPerAcre)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();

            ValuationReturn.ForestTreeGrowth.NumberOfAcresFirstClassifiedForTaxYear =
                (mvrValues.FirstOrDefault(m =>
                        m.Code == (int)MunicipalValuationCode.TreeGrowthTotalAcreageClassifiedCurrent)?.DataValue
                    .ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.NumberOfParcelsWithdrawn =
                (mvrValues.FirstOrDefault(m =>
                       m.Code == (int)MunicipalValuationCode.TreeGrowthTotalWithdrawn)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.NumberOfAcresWithdrawn =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.TreeGrowthTotalAcresWithdrawn)?.DataValue.ToDecimalValue())
                .GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.TotalAmountOfPenaltiesAssessed =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.TreeGrowthTotalPenalties)?.DataValue.ToDecimalValue())
                .GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.TotalNumberOfNonCompliancePenalties =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.TreeGrowthNumTotalPenalties)?.DataValue.ToIntegerValue())
                .GetValueOrDefault();
            ValuationReturn.ForestTreeGrowth.TreeGrowthHasBeenTransferredToFarmlandThisYear =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.TreeGrowthTransferredYesNo)?.DataValue.ToBoolean())
                .GetValueOrDefault();
        }

        private void FillFarmAndOpenSpace(List<Mvrreport> mvrValues)
        {
            ValuationReturn.FarmAndOpenSpace.NumberOfParcelsClassifiedAsFarmland =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmParcelsClassified)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.NumberOfAcresClassifiedAsFarmlandForTaxYear =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmAcreageNewClassified)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalNumberOfAcresOfAllFarmland =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmTotalAcreageTillable)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalValuationOfAllFarmland =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmTotalValueTillable)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.NumberOfFarmSoftwoodAcres =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmSoftAcres)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.NumberOfFarmMixedWoodAcres =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmMixedAcres)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.NumberOfFarmHardwoodAcres =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmHardAcres)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalNumberOfFarmWoodlandAcres =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmTotalAcreageWoodland)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalValuationOfFarmWoodland =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmTotalValueWoodland)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.FarmSoftwoodRate =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmSoftPerAcre)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.FarmMixedWoodRate =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmMixedPerAcre)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.FarmHardwoodRate =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmHardPerAcre)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalNumberOfFarmlandParcelsWithdrawn =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmTotalWithdrawn)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalNumberOfFarmlandAcresWithdrawn =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmTotalAcresWithdrawn)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnFarmland =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.FarmTotalPenalties)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.NumberOfOpenSpaceParcelsFirstClassified =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.OpenParcels)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.NumberOfOpenSpaceAcresFirstClassified =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.OpenNewClassified)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalAcresOfOpenSpaceFirstClassified =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.OpenTotalAcreageClassified)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalValuationOfAllOpenSpace =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.OpenTotalValueClassified)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalNumberOfOpenSpaceParcelsWithdrawn =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.OpenParcelsWithdrawn)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalNumberOfOpenSpaceAcresWithdrawn =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.OpenAcresWithdrawn)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnOpenSpace =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.OpenTotalPenalties)?.DataValue.ToDecimalValue()).GetValueOrDefault();
        }

        private void FillWaterfront(List<Mvrreport> mvrValues)
        {
            ValuationReturn.Waterfront.NumberOfParcelsFirstClassified =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.WaterfrontParcels)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.Waterfront.NumberOfAcresFirstClassified =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.WaterfrontFirstClassified)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.Waterfront.TotalAcresOfWaterfront =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.WaterfrontCurrentAcreage)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.Waterfront.TotalValuationOfWaterfront =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.WaterfrontCurrentValuation)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.Waterfront.NumberOfWaterfrontParcelsWithdrawn =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.WaterfrontTotalWithdrawn)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.Waterfront.NumberOfWaterfrontAcresWithdrawn =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.WaterfrontTotalAcresWithdrawn)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.Waterfront.TotalAmountOfPenalties =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.WaterfrontTotalPenalties)?.DataValue.ToDecimalValue()).GetValueOrDefault();

        }

        private void FillExemptProperty(List<Mvrreport> mvrValues)
        {
            ValuationReturn.ExemptProperty.UnitedStates =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptUnitedStates)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.StateOfMaine =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptMaine)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.NewhampshireWater =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNHWater)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.Municipal =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptQuasiMuni)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.MunicipalUtilities =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptHydroOutsidedMuni)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.MunicipalAirport =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptPublicAirport)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.PrivateAirport =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptPrivateAirport)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.MunicipalSewage =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptSewageDisposal)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.Benevolent =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptBenevolent)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.LiteraryAndScientific =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptLiterary)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.VeteranOrganizations =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptVetOrgs)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.VeteranOrgReimbursableExemption =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptVetOrgsAdministrative)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.ChamberOfCommerce =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptChamberCommerce)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.NumberOfParsonages =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumParsonages)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.ExemptValueOfParsonages =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptParsonages)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.TaxableValueOfParsonages =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptTaxableParsonages)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.ReligiousWorship =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptTotalReligious)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.FraternalOrganizations =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptFraternalOrgs)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.HospitalPersonalProperty =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptLeasedHospital)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.LegallyBlind =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptBlind)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.WaterSupplyTransport =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptWaterPipes)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.CertifiedAnimalWasteStorage =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptAnimalWaste)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.PollutionControlFacilities =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptPollution)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.SnowmobileGroomingEquipment =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptSnowGrooming)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.SolarAndWindApplicationsApproved =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptSolarWindApproved)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.SolarAndWindApplicationsProcessed =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptSolarWindProcessed)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.SolarAndWindEquipment =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptSolarWindValue)
                    ?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.ExemptProperty.ExemptOrganizations.Add(
                new ExemptOrganization()
                {
                    Name = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherName1)?.DataValue,
                    ProvisionOfLaw = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherProvision1)?.DataValue,
                    ExemptValue = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherValue1)?.DataValue.ToDecimalValue()).GetValueOrDefault()
                });
            ValuationReturn.ExemptProperty.ExemptOrganizations.Add(
                new ExemptOrganization()
                {
                    Name = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherName2)?.DataValue,
                    ProvisionOfLaw = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherProvision2)?.DataValue,
                    ExemptValue = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherValue2)?.DataValue.ToDecimalValue()).GetValueOrDefault()
                });
            ValuationReturn.ExemptProperty.ExemptOrganizations.Add(
                new ExemptOrganization()
                {
                    Name = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherName3)?.DataValue,
                    ProvisionOfLaw = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherProvision3)?.DataValue,
                    ExemptValue = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherValue3)?.DataValue.ToDecimalValue()).GetValueOrDefault()
                });
            //ValuationReturn.ExemptProperty.ExemptOrganizations.Add(
            //    new ExemptOrganization()
            //    {
            //        Name = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherName4)?.DataValue,
            //        ProvisionOfLaw = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherProvision4)?.DataValue,
            //        ExemptValue = (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherValue4)?.DataValue.ToDecimalValue()).GetValueOrDefault()
            //    });
        }

        private void FillVeteranExemptions(List<Mvrreport> mvrValues)
        {
            ValuationReturn.VeteranExemptions.SurvivingMale.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptSurvivingMaleCount)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.SurvivingMale.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptSurvivingMaleValue)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.ParaplegicRevocableTrusts.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumParaplegicLivingTrust)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.ParaplegicRevocableTrusts.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptParaplegicLivingTrust)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.MiscRevocableTrust.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumOtherLivingTrust)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.MiscRevocableTrust.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherLivingTrust)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.MaineEnlistedWWIVeteran.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumWWIResident)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.MaineEnlistedWWIVeteran.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptWWIResident)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.ParaplegicVeteran.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumParaplegic)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.ParaplegicVeteran.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptParaplegic)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.COOPHousing.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.VetExemptNumCoop)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.COOPHousing.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.VetExemptCoop)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.MiscMaineEnlisted.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumOtherResident)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.MiscMaineEnlisted.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptTotOtherResident)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.MiscNonResident.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumOtherNonResident)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.MiscNonResident.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptOtherNonResident)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.DisabledInLineOfDuty.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumVetLineOfDuty)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.DisabledInLineOfDuty.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptVetLineOfDuty)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.LebanonPanama.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.VetExemptNumLebanonPanama)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.LebanonPanama.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.VetExemptLebanonPanama)?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.EarlyVietnamConflict.Count =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptNumEarlyVietnam)?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.VeteranExemptions.EarlyVietnamConflict.ExemptValue =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ExemptEarlyVietnam)?.DataValue.ToDecimalValue()).GetValueOrDefault();
        }

        private void FillMunicipalRecords(List<Mvrreport> mvrValues, Taxratecalculation taxratecalculation)
        {
            ValuationReturn.MunicipalRecords.HasTaxMaps =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsTaxMapsYesNo)
                    ?.DataValue.ToBoolean()).GetValueOrDefault();
            var tempDate = mvrValues
                .FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsTaxMapsDate)
                ?.DataValue;
            if (!tempDate.IsNullOrWhiteSpace())
            {
                ValuationReturn.MunicipalRecords.DateMapsOriginallyObtained =
                    tempDate.ToDate();
            }
            else
            {
                ValuationReturn.MunicipalRecords.DateMapsOriginallyObtained = null;
            }
            ValuationReturn.MunicipalRecords.NameOfOriginalContractor =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsTaxMapsContractor)
                    ?.DataValue);
            var tempstr = mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsTaxMapType)
                    ?.DataValue;
            if (!String.IsNullOrEmpty(tempstr))
            {
                ValuationReturn.MunicipalRecords.TaxMapType = (MVRTaxMapType)Enum.Parse(typeof(MVRTaxMapType), tempstr);
            }
            else
            {
                ValuationReturn.MunicipalRecords.TaxMapType = MVRTaxMapType.None;
            }

            ValuationReturn.MunicipalRecords.NumberOfParcelsInMunicipality =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsLandParcels)
                    ?.DataValue.ToIntegerValue()).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.TotalTaxableAcreage =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsTaxableAcreage)
                    ?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.ProfessionalRevaluationWasCompleted =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsRevaluationYesNo)
                    ?.DataValue.ToBoolean()).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.RevaluationIncludedLand =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsRevaluationLand)
                    ?.DataValue.ToBoolean()).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.RevaluationIncludedBuildings =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsRevaluationBuildings)
                    ?.DataValue.ToBoolean()).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.RevaluationIncludedPersonalProperty =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsRevaluationPersonalProperty)
                    ?.DataValue.ToBoolean()).GetValueOrDefault();
            tempDate = mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsRevaluationEffectiveDate)
                ?.DataValue;
            if (!tempDate.IsNullOrWhiteSpace())
            {
                ValuationReturn.MunicipalRecords.RevaluationEffectiveDate =
                    tempDate.ToDate();
            }
            else
            {
                ValuationReturn.MunicipalRecords.RevaluationEffectiveDate = null;
            }
            ValuationReturn.MunicipalRecords.RevaluationContractorName =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsRevaluationContractorName)
                    ?.DataValue);
            ValuationReturn.MunicipalRecords.RevaluationCost =
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.MunicipalRecsRevaluationCost)
                    ?.DataValue.ToDecimalValue()).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.SignatureDate = DateTime.Today;

            ValuationReturn.MunicipalRecords.AssessmentFunction = MVRMunicipalityAssessmentFunction.SingleAssessor;
            tempstr = (mvrValues.FirstOrDefault(m =>
		            m.Code == (int) MunicipalValuationCode.MunicipalRecsAssessmentFunctionSingleAssessor)
	            ?.DataValue);
            if (!String.IsNullOrEmpty(tempstr))
            {
	            tempstr = tempstr.Replace(" ", "");
	            MVRMunicipalityAssessmentFunction tempMunicipalityAssessmentFunction;
	            if (Enum.TryParse(tempstr, true, out tempMunicipalityAssessmentFunction))
	            {
		            ValuationReturn.MunicipalRecords.AssessmentFunction = tempMunicipalityAssessmentFunction;
	            }
            }

            ValuationReturn.MunicipalRecords.AssessorOrAgentName =
                (mvrValues.FirstOrDefault(m =>
                        m.Code == (int)MunicipalValuationCode.MunicipalRecsAssessmentFunctionSingleAssessorName)?.DataValue
                    );
            ValuationReturn.MunicipalRecords.AssessorOrAgentEmail =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsEmail)?.DataValue
                );
            tempDate = mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsFiscalStart)
                ?.DataValue;
            if (taxratecalculation != null)
            {
                if (taxratecalculation.FiscalStartDate.HasValue && !taxratecalculation.FiscalStartDate.Value.Equals(DateTime.MinValue))
                {
                    tempDate = taxratecalculation.FiscalStartDate.Value.FormatAndPadShortDate();
                }
            }

            if (!tempDate.IsNullOrWhiteSpace())
            {
                ValuationReturn.MunicipalRecords.FiscalYearStart =
                    tempDate.ToDate();
            }
            else
            {
                ValuationReturn.MunicipalRecords.FiscalYearStart = null;
            }
            tempDate = mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsFiscalEnd)
                ?.DataValue;
            if (taxratecalculation != null)
            {
                if (taxratecalculation.FiscalEndDate.HasValue && !taxratecalculation.FiscalEndDate.Value.Equals(DateTime.MinValue))
                {
                    tempDate = taxratecalculation.FiscalEndDate.Value.FormatAndPadShortDate();
                }
            }
            if (!tempDate.IsNullOrWhiteSpace())
            {
                ValuationReturn.MunicipalRecords.FiscalYearEnd =
                    tempDate.ToDate();
            }
            else
            {
                ValuationReturn.MunicipalRecords.FiscalYearEnd = null;
            }
            ValuationReturn.MunicipalRecords.OverDueInterestRate =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsInterestRate)?.DataValue.ToDecimalValue()
                ).GetValueOrDefault();

            tempDate = mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsDateDue1)
                ?.DataValue;
            if (!tempDate.IsNullOrWhiteSpace())
            {
                ValuationReturn.MunicipalRecords.Period1DueDate =
                    tempDate.ToDate();
            }
            else
            {
                ValuationReturn.MunicipalRecords.Period1DueDate = null;
            }

            tempDate = mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsDateDue2)
                ?.DataValue;
            if (!tempDate.IsNullOrWhiteSpace())
            {
                ValuationReturn.MunicipalRecords.Period2DueDate =
                    tempDate.ToDate();
            }
            else
            {
                ValuationReturn.MunicipalRecords.Period2DueDate = null;
            }

            tempDate = mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsDateDue3)
                ?.DataValue;
            if (!tempDate.IsNullOrWhiteSpace())
            {
                ValuationReturn.MunicipalRecords.Period3DueDate =
                    tempDate.ToDate();
            }
            else
            {
                ValuationReturn.MunicipalRecords.Period3DueDate = null;
            }

            tempDate = mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsDateDue4)
                ?.DataValue;
            if (!tempDate.IsNullOrWhiteSpace())
            {
                ValuationReturn.MunicipalRecords.Period4DueDate =
                    tempDate.ToDate();
            }
            else
            {
                ValuationReturn.MunicipalRecords.Period4DueDate = null;
            }

            ValuationReturn.MunicipalRecords.AssessmentRecordsAreComputerized =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsComputerizedYesNo)?.DataValue.ToBoolean()
                ).GetValueOrDefault(true);
            ValuationReturn.MunicipalRecords.AssessmentSoftware =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsComputerizedContractor)?.DataValue
                ) ?? "TRIO Software";
            ValuationReturn.MunicipalRecords.ImplementedATaxReliefProgram =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsLocalTaxReliefYesNo)?.DataValue.ToBoolean()
                ).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.NumberOfPeopleQualifiedForTaxProgram =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsLocalTaxReliefHowMany)?.DataValue.ToIntegerValue()
                ).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.AmountOfReliefGrantedForTaxProgram =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalRecsLocalTaxReliefHowMuch)?.DataValue.ToDecimalValue()
                ).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.ImplementedElderlyTaxCredit =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalElderlyTaxCreditYesNo)?.DataValue.ToBoolean()
                ).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.NumberOfPeopleQualifiedForElderlyCredit =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalElderlyTaxCreditHowMany)?.DataValue.ToIntegerValue()
                ).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.AmountOfReliefGrantedForElderlyCredit =
                (mvrValues.FirstOrDefault(m =>
                    m.Code == (int)MunicipalValuationCode.MunicipalElderlyTaxCreditHowMuch)?.DataValue.ToDecimalValue()
                ).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.ImplementedSeniorTaxDeferral =
                (mvrValues.FirstOrDefault(m =>
                        m.Code == (int)MunicipalValuationCode.MunicipalSeniorTaxDeferralYesNo)?.DataValue.ToBoolean()
                ).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.NumberOfPeopleQualifiedForSeniorTaxDeferral =
                (mvrValues.FirstOrDefault(m =>
                        m.Code == (int)MunicipalValuationCode.MunicipalSeniorTaxDeferralHowMany)?.DataValue
                    .ToIntegerValue()
                ).GetValueOrDefault();
            ValuationReturn.MunicipalRecords.AmountOfReliefGrantedForSeniorTaxDeferral =
                (mvrValues.FirstOrDefault(m =>
                        m.Code == (int)MunicipalValuationCode.MunicipalSeniorTaxDeferralHowMuch)?.DataValue
                    .ToDecimalValue()
                ).GetValueOrDefault();
        }

        private void FillValuationInformation(List<Mvrreport> mvrValues)
        {
            ValuationReturn.ValuationInformation.New = MakeValuationStats(
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationGridNew)
                    ?.DataValue) ?? "");
            ValuationReturn.ValuationInformation.Converted = MakeValuationStats(
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationGridConverted)
                     ?.DataValue) ?? "");
            ValuationReturn.ValuationInformation.Demolished = MakeValuationStats(
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationGridDemolished)
                    ?.DataValue) ?? "");

            ValuationReturn.ValuationInformation.ValuationIncrease = MakeValuationValues(
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationGridIncrease)
                    ?.DataValue) ?? "");
            ValuationReturn.ValuationInformation.ValuationDecrease = MakeValuationValues(
                (mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationGridLoss)
                    ?.DataValue) ?? "");
            ValuationReturn.ValuationInformation.IndustrialMercantileGrowth.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion2a)?.DataValue);
            ValuationReturn.ValuationInformation.IndustrialMercantileGrowth.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion2b)?.DataValue);
            ValuationReturn.ValuationInformation.IndustrialMercantileGrowth.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion2c)?.DataValue);
            ValuationReturn.ValuationInformation.IndustrialMercantileGrowth.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion2d)?.DataValue);
            ValuationReturn.ValuationInformation.IndustrialMercantileGrowth.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion2e)?.DataValue);
            ValuationReturn.ValuationInformation.IndustrialMercantileGrowth.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion2f)?.DataValue);
            ValuationReturn.ValuationInformation.IndustrialMercantileGrowth.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion2G)?.DataValue);
            ValuationReturn.ValuationInformation.IndustrialMercantileGrowth.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion2h)?.DataValue);

            ValuationReturn.ValuationInformation.ExtremeLosses.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion3A)?.DataValue);
            ValuationReturn.ValuationInformation.ExtremeLosses.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion3B)?.DataValue);
            ValuationReturn.ValuationInformation.ExtremeLosses.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion3C)?.DataValue);
            ValuationReturn.ValuationInformation.ExtremeLosses.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion3d)?.DataValue);
            ValuationReturn.ValuationInformation.ExtremeLosses.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion3E)?.DataValue);
            ValuationReturn.ValuationInformation.ExtremeLosses.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion3F)?.DataValue);
            ValuationReturn.ValuationInformation.ExtremeLosses.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion3G)?.DataValue);
            ValuationReturn.ValuationInformation.ExtremeLosses.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion3H)?.DataValue);

            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4A)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4B)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4C)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4D)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4E)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4F)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4G)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4H)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4I)?.DataValue);
            ValuationReturn.ValuationInformation.GeneralValuationChange.Add(
                mvrValues.FirstOrDefault(m => m.Code == (int)MunicipalValuationCode.ValuationQuestion4J)?.DataValue);
        }

        private ValuationInformationStats MakeValuationStats(string statValue)
        {
            var statArray = statValue.Split(',');
            var stats = new ValuationInformationStats();
            if (statArray.Length > 0)
            {
                stats.OneFamily = statArray[0].ToIntegerValue();
            }

            if (statArray.Length > 1)
            {
                stats.TwoFamily = statArray[1].ToIntegerValue();
            }

            if (statArray.Length > 2)
            {
                stats.ThreeToFourFamily = statArray[2].ToIntegerValue();
            }

            if (statArray.Length > 3)
            {
                stats.FiveFamilyPlus = statArray[3].ToIntegerValue();
            }

            if (statArray.Length > 4)
            {
                stats.MobileHomes = statArray[4].ToIntegerValue();
            }

            if (statArray.Length > 5)
            {
                stats.SeasonalHomes = statArray[5].ToIntegerValue();
            }
            return stats;
        }

        private ValuationInformationValues MakeValuationValues(string statValue)
        {
            var statArray = statValue.Split(',');
            var stats = new ValuationInformationValues();
            if (statArray.Length > 0)
            {
                stats.OneFamily = statArray[0].ToDecimalValue();
            }

            if (statArray.Length > 1)
            {
                stats.TwoFamily = statArray[1].ToDecimalValue();
            }

            if (statArray.Length > 2)
            {
                stats.ThreeToFourFamily = statArray[2].ToDecimalValue();
            }

            if (statArray.Length > 3)
            {
                stats.FiveFamilyPlus = statArray[3].ToDecimalValue();
            }

            if (statArray.Length > 4)
            {
                stats.MobileHomes = statArray[4].ToDecimalValue();
            }

            if (statArray.Length > 5)
            {
                stats.SeasonalHomes = statArray[5].ToDecimalValue();
            }
            return stats;
        }

        private void CalculateMVR()
        {
            LoadDefaults();
            CalculateHomesteadValues();
            CalculateBETESection();
            CalculateWoodland();
            //CalculateMVSection();
            CalculateVetExempts();
            CalculateExemptsForMVR();
            //CalculateIndustrial();
            CalculateTaxableValuation();
            CalculateREValuation();
        }

        private void LoadDefaults()
        {
            ValuationReturn.CertifiedRatio = realEstateSettings.CertifiedRatio * 100;
            if (ValuationReturn.Municipality.IsNullOrWhiteSpace())
            {
                ValuationReturn.Municipality = environmentSettings.ClientName;
            }
        }

        private void CalculateHomesteadValues()
        {
            var homesteadCodes =
                realEstateContext.ExemptCodes.Where(c => c.Category == PropertyExemptionCategory.Homestead.ToInteger()).ToList();
            if (!homesteadCodes.Any())
            {
                ValuationReturn.Homestead.NumberOfFullHomesteadsGranted = 0;
                ValuationReturn.Homestead.NumberOfFullyExemptedHomesteads = 0;
                ValuationReturn.Homestead.TotalAssessedValueOfAllHomesteadProperty = 0;
                ValuationReturn.Homestead.TotalValueOfFullyExemptedHomesteads = 0;
                return;
            }

            var adjustedHomesteadAmount = HomesteadExemptionAmount * realEstateSettings.CertifiedRatio;
            var code1Query = realEstateContext.REMasters.Where(r => r.Rscard == 1 &&
                r.Rsdeleted != true && homesteadCodes.Any(cd => cd.Code == r.RiexemptCd1));
            var code2Query = realEstateContext.REMasters.Where(r => r.Rscard == 1 &&
                r.Rsdeleted != true && homesteadCodes.Any(cd => cd.Code == r.RiexemptCd2));
            var code3Query = realEstateContext.REMasters.Where(r => r.Rscard == 1 &&
                r.Rsdeleted != true && homesteadCodes.Any(cd => cd.Code == r.RiexemptCd3));
            var unionQuery = code1Query.Union(code2Query).Union(code3Query);//.GroupBy(r => new HomesteadSummary(){Count = 1,Assessment = r.LastBldgVal.GetValueOrDefault() + r.LastLandVal.GetValueOrDefault(),HomesteadExempt = }).Select(m => new HomesteadSummary(){Count = m.Count()})
            var masterQuery = realEstateContext.REMasters;
            if (environmentSettings.IsMultiTown)
            {
                masterQuery = masterQuery.Where(r => r.RitranCode.GetValueOrDefault() == ValuationReturn.TownNumber);
            }

			//TODO - This needs to include the Assessment on ALL cards, not just Card 1
            var finalQuery = masterQuery.Join(unionQuery, 
	                                          r => r.Id, 
	                                          codeQuery => codeQuery.Id, 
	                                          (r, codeQuery) => new HomesteadSummary(){
		                                          Assessment = r.LastBldgVal.GetValueOrDefault() + r.LastLandVal.GetValueOrDefault(),
		                                          HomesteadCount = homesteadCodes.Count(h => h.Code == r.RiexemptCd1) + homesteadCodes.Count(h => h.Code == r.RiexemptCd2) + homesteadCodes.Count(h => h.Code == r.RiexemptCd3),
		                                          HomesteadExempt = r.HomesteadValue.GetValueOrDefault()
	                                          });
            var fullQuery = finalQuery.Where(r => r.HomesteadExempt >= adjustedHomesteadAmount).ToList();
            var homesteadSummary = new HomesteadSummary()
            {
	            Assessment = fullQuery.Sum(s => s.Assessment),
	            HomesteadCount = fullQuery.Sum(s => s.HomesteadCount),
	            HomesteadExempt = fullQuery.Sum(s => s.HomesteadExempt)
            };
            var fullyExemptQuery =
                finalQuery.Where(h => h.HomesteadExempt < adjustedHomesteadAmount).ToList();

            ValuationReturn.Homestead.NumberOfFullHomesteadsGranted = homesteadSummary.HomesteadCount;
            ValuationReturn.Homestead.TotalAssessedValueOfAllHomesteadProperty = homesteadSummary.Assessment;
            ValuationReturn.Homestead.NumberOfFullyExemptedHomesteads = fullyExemptQuery.Count;
            ValuationReturn.Homestead.TotalValueOfFullyExemptedHomesteads = fullyExemptQuery.Sum(s => s.HomesteadExempt);
        }

        private void CalculateBETESection()
        {
            if (moduleSettings.PersonalPropertyIsActive || moduleSettings.TaxBillingIsActive)
            {
                var masterQuery = personalPropertyContext.PPMasters.Where(p => p.Deleted != true);
                if (environmentSettings.IsMultiTown)
                {
                    masterQuery = masterQuery.Where(p => p.TranCode.GetValueOrDefault() == ValuationReturn.TownNumber);
                }
                var beteQuery = masterQuery.Join(personalPropertyContext.PPValuations, m => m.Account, v => v.Account, (m, p) => p).AsEnumerable().Select(v => (Exempt1: v.Cat1Exempt, Exempt2: v.Cat2Exempt, Exempt3: v.Cat3Exempt, Exempt4: v.Cat4Exempt, Exempt5: v.Cat5Exempt, Exempt6: v.Cat6Exempt, Exempt7: v.Cat7Exempt, Exempt8: v.Cat8Exempt, Exempt9: v.Cat9Exempt)).ToList();
                if (beteQuery.Any())
                {
                    ValuationReturn.Bete.TotalBeteInTIF = beteQuery.Count();
                    ValuationReturn.Bete.TotalBeteQualifiedExempt = beteQuery.Sum(s =>
                        s.Exempt1.GetValueOrDefault() + s.Exempt2.GetValueOrDefault() + s.Exempt3.GetValueOrDefault() +
                        s.Exempt4.GetValueOrDefault() + s.Exempt5.GetValueOrDefault() + s.Exempt6.GetValueOrDefault() +
                        s.Exempt7.GetValueOrDefault() + s.Exempt8.GetValueOrDefault() + s.Exempt9.GetValueOrDefault());
                }
                else
                {
                    ValuationReturn.Bete.TotalBeteInTIF = 0;
                    ValuationReturn.Bete.TotalBeteQualifiedExempt = 0;
                }
            }
        }

        private void CalculateWoodland()
        {
            var masterQuery = realEstateContext.REMasters.Where(r => r.Rsdeleted != true);
            var fullyExemptCodes = realEstateContext.ExemptCodes.Where(e => e.Amount == 0);
            if (environmentSettings.IsMultiTown)
            {
                masterQuery = masterQuery.Where(r => r.RitranCode.GetValueOrDefault() == ValuationReturn.TownNumber);
            }

            var masterList = masterQuery.OrderBy(r => r.Rsaccount).ThenBy(r => r.Rscard).ToList();
            decimal totalAcres = 0;
            decimal totalSoft = 0;
            decimal totalHard = 0;
            decimal totalMixed = 0;
            decimal totalTreeGrowthValue = 0;
            int lastAccount = 0;
            int parcelCount = 0;
            var canUse = false;
            decimal waterFrontAcres = 0;
            decimal waterFrontValue = 0;
            int waterFrontCount = 0;
            int lastWaterFrontAccount = 0;

            int lastOpenSpaceAccount = 0;
            decimal openSpaceAcres = 0;
            decimal openSpaceValue = 0;
            int openSpaceCount = 0;
            decimal tillableAcres = 0;
            int tillableCount = 0;
            decimal tillableValue = 0;
            int lastTillableAccount = 0;

            decimal farmSoftAcres = 0;
            decimal farmMixedAcres = 0;
            decimal farmHardAcres = 0;
            int farmTreeGrowthCount = 0;
            decimal farmTreeGrowthValue = 0;
            int lastFarmTreeGrowthAccount = 0;

            int lastFarmAccount = 0;
            int farmCount = 0;
            decimal totalTaxableLandAcreage = 0;

            foreach (var parcel in masterList)
            {
                try
                {
                    totalAcres += parcel.Piacres.GetValueOrDefault().ToDecimal();
                    if (parcel.Rscard.GetValueOrDefault() == 1)
                    {
                        canUse = true;
                        if (parcel.RiexemptCd1.GetValueOrDefault() > 0 &&
                            fullyExemptCodes.Any(e => e.Code == parcel.RiexemptCd1.GetValueOrDefault()))
                        {
                            canUse = false;
                        }
                        else if (parcel.RiexemptCd2.GetValueOrDefault() > 0 &&
                                 fullyExemptCodes.Any(e => e.Code == parcel.RiexemptCd2.GetValueOrDefault()))
                        {
                            canUse = false;
                        }
                        else if (parcel.RiexemptCd3.GetValueOrDefault() > 0 && fullyExemptCodes.Any(e => e.Code == parcel.RiexemptCd3.GetValueOrDefault()))
                        {
                            canUse = false;
                        }
                    }

                    if (canUse)
                    {
                        var landPieces = parcel.GetLandPieces();
                        //var hadSome = false;
                        var hadWaterfront = false;
                        var hadTillable = false;
                        var hadTreeGrowth = false;
                        var hadOpenSpace = false;
                        var hadFarmTreeGrowth = false;
                        totalTaxableLandAcreage += parcel.Piacres.GetValueOrDefault().ToDecimal();
                        if (landPieces.Any(l => l.IsTreeGrowth(landTypes) || l.IsOpenSpace(landTypes) ||
                                                    l.IsTillable(landTypes) || l.IsWaterfront(landTypes)))
                        {
                            var summary = realEstateContext.SummationRecords
                                .FirstOrDefault(s => s.SrecordNumber == parcel.Rsaccount.GetValueOrDefault() && s.CardNumber == parcel.Rscard.GetValueOrDefault());

                            if (summary != null)
                            {
                                
                                var summaryPieces = summary.GetLandSummaries();
                                foreach (var summaryPiece in summaryPieces)
                                {

                                    var currentLandType = landTypes.GetType(summaryPiece.LandTypeCode);
                                    if (currentLandType != null)
                                    {
                                        if (!currentLandType.IsFarmTreeGrowth())
                                        {
                                            if (currentLandType.IsSoftwood())
                                            {
                                                totalSoft += summaryPiece.Acres;
                                                totalTreeGrowthValue += summaryPiece.Value;
                                                hadTreeGrowth = true;
                                            }
                                            else if (currentLandType.IsHardWood())
                                            {
                                                totalHard += summaryPiece.Acres;
                                                totalTreeGrowthValue += summaryPiece.Value;
                                                hadTreeGrowth = true;
                                            }
                                            else if (currentLandType.IsMixedwood())
                                            {
                                                totalMixed += summaryPiece.Acres;
                                                totalTreeGrowthValue += summaryPiece.Value;
                                                hadTreeGrowth = true;
                                            }
                                            else if (currentLandType.IsWaterfront())
                                            {
                                                waterFrontAcres += summaryPiece.Acres;
                                                waterFrontValue += summaryPiece.Value;
                                                hadWaterfront = true;
                                            }
                                            else if (currentLandType.IsOpenSpace())
                                            {
                                                openSpaceAcres += summaryPiece.Acres;
                                                openSpaceValue += summaryPiece.Value;
                                                hadOpenSpace = true;
                                            }
                                            else if (currentLandType.IsTillable())
                                            {
                                                tillableAcres += summaryPiece.Acres;
                                                tillableValue += summaryPiece.Value;
                                                hadTillable = true;
                                            }
                                        }
                                        else
                                        {
                                            if (currentLandType.IsSoftwood())
                                            {
                                                farmSoftAcres += summaryPiece.Acres;
                                                farmTreeGrowthValue += summaryPiece.Value;
                                                hadFarmTreeGrowth = true;
                                            }
                                            else if (currentLandType.IsHardWood())
                                            {
                                                farmHardAcres += summaryPiece.Acres;
                                                farmTreeGrowthValue += summaryPiece.Value;
                                                hadFarmTreeGrowth = true;
                                            }
                                            else if (currentLandType.IsMixedwood())
                                            {
                                                farmMixedAcres += summaryPiece.Acres;
                                                farmTreeGrowthValue += summaryPiece.Value;
                                                hadFarmTreeGrowth = true;
                                            }
                                        }
                                    }
                                }

                                if (hadTreeGrowth)
                                {
                                    if (lastAccount != parcel.Rsaccount.GetValueOrDefault())
                                    {
                                        parcelCount++;
                                    }

                                    lastAccount = parcel.Rsaccount.GetValueOrDefault();
                                }

                                if (hadWaterfront)
                                {
                                    if (lastWaterFrontAccount != parcel.Rsaccount.GetValueOrDefault())
                                    {
                                        waterFrontCount++;
                                    }

                                    lastWaterFrontAccount = parcel.Rsaccount.GetValueOrDefault();
                                }

                                if (hadOpenSpace)
                                {
                                    if (lastOpenSpaceAccount != parcel.Rsaccount.GetValueOrDefault())
                                    {
                                        openSpaceCount++;
                                    }

                                    lastOpenSpaceAccount = parcel.Rsaccount.GetValueOrDefault();
                                }

                                if (hadTillable || hadFarmTreeGrowth)
                                {
                                    if (lastFarmAccount != parcel.Rsaccount.GetValueOrDefault())
                                    {
                                        farmCount++;
                                    }

                                    lastFarmAccount = parcel.Rsaccount.GetValueOrDefault();
                                }

                                if (hadTillable)
                                {
                                    if (lastTillableAccount != parcel.Rsaccount.GetValueOrDefault())
                                    {
                                        tillableCount++;
                                    }

                                    lastTillableAccount = parcel.Rsaccount.GetValueOrDefault();
                                }

                                if (hadFarmTreeGrowth)
                                {
                                    if (lastFarmTreeGrowthAccount != parcel.Rsaccount.GetValueOrDefault())
                                    {
                                        farmTreeGrowthCount++;
                                    }

                                    lastFarmTreeGrowthAccount = parcel.Rsaccount.GetValueOrDefault();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                    throw;
                }
            }
            ValuationReturn.MunicipalRecords.TotalTaxableAcreage = totalTaxableLandAcreage;

            ValuationReturn.ForestTreeGrowth.NumberOfParcelsClassified = parcelCount;
            ValuationReturn.ForestTreeGrowth.HardwoodAcreage = totalHard;
            ValuationReturn.ForestTreeGrowth.SoftwoodAcreage = totalSoft;
            ValuationReturn.ForestTreeGrowth.MixedwoodAcreage = totalMixed;
            ValuationReturn.ForestTreeGrowth.TotalAssessedValuation = totalTreeGrowthValue;
            ValuationReturn.FarmAndOpenSpace.NumberOfFarmHardwoodAcres = farmHardAcres;
            ValuationReturn.FarmAndOpenSpace.NumberOfFarmSoftwoodAcres = farmSoftAcres;
            ValuationReturn.FarmAndOpenSpace.NumberOfFarmMixedWoodAcres = farmMixedAcres;
            ValuationReturn.FarmAndOpenSpace.NumberOfParcelsClassifiedAsFarmland = farmCount;
            ValuationReturn.FarmAndOpenSpace.TotalNumberOfAcresOfAllFarmland = tillableAcres;
            ValuationReturn.FarmAndOpenSpace.TotalValuationOfAllFarmland = tillableValue;
            ValuationReturn.FarmAndOpenSpace.TotalNumberOfFarmWoodlandAcres =
                farmSoftAcres + farmHardAcres + farmMixedAcres;
            ValuationReturn.FarmAndOpenSpace.TotalValuationOfFarmWoodland = farmTreeGrowthValue;
            ValuationReturn.FarmAndOpenSpace.TotalValuationOfAllOpenSpace = openSpaceValue;
            ValuationReturn.FarmAndOpenSpace.NumberOfOpenSpaceParcelsFirstClassified = openSpaceCount;
            ValuationReturn.FarmAndOpenSpace.TotalAcresOfOpenSpaceFirstClassified = openSpaceAcres;
        }

        private void CalculateTaxableValuation()
        {
            var ppQuery = personalPropertyContext.PPMasters.Where(p => p.Deleted != true && p.Value > 0 && p.Orcode != "Y");
            if (environmentSettings.IsMultiTown)
            {
                ppQuery = ppQuery.Where(p => p.TranCode == ValuationReturn.TownNumber);
            }
            var ppJoinQuery = ppQuery.Join(personalPropertyContext.PPValuations, master => master.Account,
                    valuation => valuation.Account,
                    (master, valuation) => valuation).ToList();
            int totalBusiness = 0;
            int totalMachinery = 0;
            int totalOther = 0;
            var businessCategories = realEstateSettings.PersonalPropertyCategories.BusinessEquipmentCategories();
            var machineryCategories = realEstateSettings.PersonalPropertyCategories.MachineryAndEquipmentCategories();
            var otherCategories = realEstateSettings.PersonalPropertyCategories.OtherCategories();
            foreach (var valuation in ppJoinQuery)
            {
                foreach (var category in businessCategories)
                {
                    totalBusiness += valuation.CategoryAmount(category);
                }

                foreach (var category in machineryCategories)
                {
                    totalMachinery += valuation.CategoryAmount(category);
                }

                foreach (var category in otherCategories)
                {
                    totalOther += valuation.CategoryAmount(category);
                }
            }

            ValuationReturn.PersonalProperty.BusinessEquipment = totalBusiness;
            ValuationReturn.PersonalProperty.OtherPersonalProperty = totalOther;
            ValuationReturn.PersonalProperty.ProductionMachineryAndEquipment = totalMachinery;
        }

        private void CalculateVetExempts()
        {
            var masterQuery = realEstateContext.REMasters.Where(r => r.Rsdeleted != true && r.Rscard == 1);
            var ratio = realEstateSettings.CertifiedRatio;
            var exemptions = realEstateContext.ExemptCodes.ToList();
            decimal fullVetExempt = vetExempt * ratio;
            decimal fullWWIVetExempt = wwiVetExempt * ratio;
            decimal fullParaplegicExempt = paraplegicExempt * ratio;
            decimal totalExempts = 0;
            decimal totalValue = 0;

            if (environmentSettings.IsMultiTown)
            {
                masterQuery = masterQuery.Where(r => r.RitranCode.GetValueOrDefault() == ValuationReturn.TownNumber);
            }

            var maleWidowerCount = GetVetExemptionCount(
                masterQuery,
                 true, false,
                 exemptions.Where(c => c.Category == PropertyExemptionCategory.VetMaleSurvivor.ToInteger() ||
                                                 c.Category == PropertyExemptionCategory.WWIWidower.ToInteger() ||
                                                 c.Category == PropertyExemptionCategory.WWIIWidower.ToInteger()));
            var maleWidowerAmount = fullVetExempt * maleWidowerCount;

            var revocableTrustParaplegicCount = GetVetExemptionCount(
	            masterQuery,
	            true, true,
	            exemptions.Where(c => c.Category == PropertyExemptionCategory.ParaplegicVet.ToInteger()));
            var revocableTrustParaplegicAmount = fullParaplegicExempt * revocableTrustParaplegicCount;

			var revocableTrustCount = GetVetExemptionCount(
	            masterQuery,
	            true, true,
	            exemptions.Where(c => c.Category == PropertyExemptionCategory.Korean.ToInteger()
	                                    || c.Category == PropertyExemptionCategory.Veteran.ToInteger()
	                                    || c.Category == PropertyExemptionCategory.VietnamConflict.ToInteger()
	                                    || c.Category == PropertyExemptionCategory.WorldWarI.ToInteger()
	                                    || c.Category == PropertyExemptionCategory.WorldWarII.ToInteger()
	                                    || c.Category == PropertyExemptionCategory.WWIWidower.ToInteger()
	                                    || c.Category == PropertyExemptionCategory.WWIIWidower.ToInteger()
	                                    || c.Category == PropertyExemptionCategory.VetCOOPHousing.ToInteger()
                                        || c.Category == PropertyExemptionCategory.VetMaleSurvivor.ToInteger()));
            var revocableTrustAmount = fullVetExempt * revocableTrustCount;

            var paraplegicCount = GetVetExemptionCount(
                masterQuery,
                true, false,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.ParaplegicVet.ToInteger()));
            var paraplegicAmount = fullParaplegicExempt * paraplegicCount;


            var wwiResidentCount = GetVetExemptionCount(
                masterQuery,
                false, false,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.WorldWarI.ToInteger() && c.Enlisted == ExemptEnlistedResidency.MaineResident.ToInteger()));
            var wwiResidentAmount = fullWWIVetExempt * wwiResidentCount;

            var wwiNonResidentCount = GetVetExemptionCount(
                masterQuery,
                false, false,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.WorldWarI.ToInteger() && c.Enlisted == ExemptEnlistedResidency.NonMaineResident.ToInteger()));
            var wwiNonResidentAmount = fullWWIVetExempt * wwiNonResidentCount;

            var coopCount = GetVetExemptionCount(
                masterQuery,
                true, false,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.VetCOOPHousing.ToInteger()));
            var coopAmount = fullVetExempt * coopCount;

            var otherMaineResCount = GetVetExemptionCount(
                masterQuery,
                true, false,
                exemptions.Where(c => c.Category != PropertyExemptionCategory.VetMaleSurvivor.ToInteger()
                                      && c.Category != PropertyExemptionCategory.WWIWidower.ToInteger()
                                      && c.Category != PropertyExemptionCategory.WWIIWidower.ToInteger()
                                      && c.Category != PropertyExemptionCategory.WorldWarI.ToInteger()
                                      && c.Category != PropertyExemptionCategory.ParaplegicVet.ToInteger()
                                      && c.Category != PropertyExemptionCategory.VetCOOPHousing.ToInteger()
                                      && c.Category != PropertyExemptionCategory.DisabledVeteran.ToInteger()
                                      && c.Category != PropertyExemptionCategory.LebanonPanama.ToInteger()
                                      && c.Category != PropertyExemptionCategory.EarlyVietnamConflict.ToInteger()
                                      && c.Enlisted == ExemptEnlistedResidency.MaineResident.ToInteger()
                                      ));
            var otherMaineResAmount = fullVetExempt * otherMaineResCount;

            var otherMaineNonResidentCount = GetVetExemptionCount(
                masterQuery,
                true, false,
                exemptions.Where(c => c.Category != PropertyExemptionCategory.VetMaleSurvivor.ToInteger()
                                      && c.Category != PropertyExemptionCategory.WWIWidower.ToInteger()
                                      && c.Category != PropertyExemptionCategory.WWIIWidower.ToInteger()
                                      && c.Category != PropertyExemptionCategory.WorldWarI.ToInteger()
                                      && c.Category != PropertyExemptionCategory.ParaplegicVet.ToInteger()
                                      && c.Category != PropertyExemptionCategory.VetCOOPHousing.ToInteger()
                                      && c.Category != PropertyExemptionCategory.DisabledVeteran.ToInteger()
                                      && c.Category != PropertyExemptionCategory.LebanonPanama.ToInteger()
                                      && c.Category != PropertyExemptionCategory.EarlyVietnamConflict.ToInteger()
                                      && c.Enlisted == ExemptEnlistedResidency.NonMaineResident.ToInteger()));

            var otherMaineNonResidentAmount = fullVetExempt * otherMaineNonResidentCount;

            var disabledCount = GetVetExemptionCount(
                masterQuery,
                false, false,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.DisabledVeteran.ToInteger()));
            var disabledAmount = fullVetExempt * disabledCount;

            var lebanonPanamaCount = GetVetExemptionCount(
                masterQuery,
                false, false,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.LebanonPanama.ToInteger()));
            var lebanonPanamaAmount = fullVetExempt * lebanonPanamaCount;

            var vietnamCount = GetVetExemptionCount(
                masterQuery,
                false, false,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.EarlyVietnamConflict.ToInteger()));
            var vietnamAmount = fullVetExempt * vietnamCount;

            ValuationReturn.VeteranExemptions.COOPHousing.Count = coopCount;
            ValuationReturn.VeteranExemptions.COOPHousing.ExemptValue = coopAmount;
            ValuationReturn.VeteranExemptions.DisabledInLineOfDuty.Count = disabledCount;
            ValuationReturn.VeteranExemptions.DisabledInLineOfDuty.ExemptValue = disabledAmount;
            ValuationReturn.VeteranExemptions.LebanonPanama.Count = lebanonPanamaCount;
            ValuationReturn.VeteranExemptions.LebanonPanama.ExemptValue = lebanonPanamaAmount;
            ValuationReturn.VeteranExemptions.MaineEnlistedWWIVeteran.Count = wwiResidentCount;
            ValuationReturn.VeteranExemptions.MaineEnlistedWWIVeteran.ExemptValue = wwiResidentAmount;
            ValuationReturn.VeteranExemptions.NonResidentWWIVeteran.Count = wwiNonResidentCount;
            ValuationReturn.VeteranExemptions.NonResidentWWIVeteran.ExemptValue = wwiNonResidentAmount;
            ValuationReturn.VeteranExemptions.MiscMaineEnlisted.Count = otherMaineResCount;
            ValuationReturn.VeteranExemptions.MiscMaineEnlisted.ExemptValue = otherMaineResAmount;
            ValuationReturn.VeteranExemptions.MiscNonResident.Count = otherMaineNonResidentCount;
            ValuationReturn.VeteranExemptions.MiscNonResident.ExemptValue = otherMaineNonResidentAmount;
            ValuationReturn.VeteranExemptions.MiscRevocableTrust.Count = revocableTrustCount;
            ValuationReturn.VeteranExemptions.MiscRevocableTrust.ExemptValue = revocableTrustAmount;
            ValuationReturn.VeteranExemptions.ParaplegicRevocableTrusts.Count = revocableTrustParaplegicCount;
            ValuationReturn.VeteranExemptions.ParaplegicRevocableTrusts.ExemptValue = revocableTrustParaplegicAmount;
            ValuationReturn.VeteranExemptions.ParaplegicVeteran.Count = paraplegicCount;
            ValuationReturn.VeteranExemptions.ParaplegicVeteran.ExemptValue = paraplegicAmount;
            ValuationReturn.VeteranExemptions.SurvivingMale.Count = maleWidowerCount;
            ValuationReturn.VeteranExemptions.SurvivingMale.ExemptValue = maleWidowerAmount;
            ValuationReturn.VeteranExemptions.EarlyVietnamConflict.Count = vietnamCount;
            ValuationReturn.VeteranExemptions.EarlyVietnamConflict.ExemptValue = vietnamAmount;
        }

        private void CalculateExemptsForMVR()
        {
            var masterQuery = realEstateContext.REMasters.Where(r => r.Rsdeleted != true && r.Rscard == 1);
            if (environmentSettings.IsMultiTown)
            {
                masterQuery = masterQuery.Where(r => r.RitranCode.GetValueOrDefault() == ValuationReturn.TownNumber);
            }
            var exemptions = realEstateContext.ExemptCodes.ToList();
            ValuationReturn.ExemptProperty.UnitedStates = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.UnitedStates.ToInteger()));
            ValuationReturn.ExemptProperty.StateOfMaine = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.StateOfMaine.ToInteger()));
            ValuationReturn.ExemptProperty.NewhampshireWater = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.NHWater.ToInteger()));
            ValuationReturn.ExemptProperty.Municipal = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.MunicipalCounty.ToInteger() || c.Category == PropertyExemptionCategory.QuasiMunicipal.ToInteger()));
            ValuationReturn.ExemptProperty.MunicipalUtilities = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.HydroOutsideMuni.ToInteger()));
            ValuationReturn.ExemptProperty.MunicipalAirport = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.MunicipalAirport.ToInteger()));
            ValuationReturn.ExemptProperty.PrivateAirport = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.PrivateAirport.ToInteger()));
            ValuationReturn.ExemptProperty.MunicipalSewage = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.SewageDisposal.ToInteger()));
            ValuationReturn.ExemptProperty.Benevolent = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.Benevolent.ToInteger()));
            ValuationReturn.ExemptProperty.LiteraryAndScientific = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.ScienceAndLiterature.ToInteger()));
            ValuationReturn.ExemptProperty.VeteranOrganizations = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.VeteransOrganization.ToInteger()));
            //ValuationReturn.ExemptProperty.VeteranOrgReimbursableExemption = GetExemptionAmount(masterQuery,
            //    exemptions.Where(c => c.Category == PropertyExemptionCategory..ToInteger()));
            ValuationReturn.ExemptProperty.ChamberOfCommerce = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.ChamberOfCommerce.ToInteger()));
            var parsonageStats = GetParsonageStats(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.Parsonage.ToInteger()));
            ValuationReturn.ExemptProperty.NumberOfParsonages = parsonageStats.count;
            ValuationReturn.ExemptProperty.ExemptValueOfParsonages = parsonageStats.exemptValue;
            ValuationReturn.ExemptProperty.TaxableValueOfParsonages = parsonageStats.valuation;
            ValuationReturn.ExemptProperty.ReligiousWorship = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.Religious.ToInteger()));
            ValuationReturn.ExemptProperty.FraternalOrganizations = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.Fraternal.ToInteger()));
            ValuationReturn.ExemptProperty.HospitalPersonalProperty = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.HospitalLeased.ToInteger()));
            ValuationReturn.ExemptProperty.LegallyBlind = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.Blind.ToInteger()));
            ValuationReturn.ExemptProperty.WaterSupplyTransport = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.WaterPipes.ToInteger()));
            ValuationReturn.ExemptProperty.CertifiedAnimalWasteStorage = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.AnimalWaste.ToInteger()));
            ValuationReturn.ExemptProperty.PollutionControlFacilities = GetExemptionAmount(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.PollutionControl.ToInteger()));
            ValuationReturn.ExemptProperty.SnowmobileGroomingEquipment = GetSnowmobileTrailGroomingExemptAmount();
            var solarAndWindEquipStats = GetSolarAndWindEquipmentStats(masterQuery,
                exemptions.Where(c => c.Category == PropertyExemptionCategory.SolarAndWindEquipment.ToInteger()));
            ValuationReturn.ExemptProperty.SolarAndWindApplicationsApproved = solarAndWindEquipStats.count;
            ValuationReturn.ExemptProperty.SolarAndWindEquipment = solarAndWindEquipStats.exemptValue;
        }

        private void CalculateREValuation()
        {
            var masterQuery = realEstateContext.REMasters.Where(r => r.Rsdeleted != true);
            if (environmentSettings.IsMultiTown)
            {
                masterQuery = masterQuery.Where(r => r.RitranCode.GetValueOrDefault() == ValuationReturn.TownNumber);
            }

            var parcelCount = masterQuery.Where(c => c.Piland1Type > 0 || c.Piland2Type > 0 || c.Piland3Type > 0 || c.Piland4Type > 0 || c.Piland5Type > 0 || c.Piland6Type > 0 || c.Piland7Type > 0).GroupBy(g => g.Rsaccount).Count();
            ValuationReturn.MunicipalRecords.NumberOfParcelsInMunicipality = parcelCount;

            var valuationSums = masterQuery.GroupBy(g => 0).Select(g => new
            {
                landSum = g.Sum(o => o.LastLandVal.GetValueOrDefault()),
                landExemptSum = g.Sum(o => o.LandExempt.GetValueOrDefault()),
                bldgSum = g.Sum(o => o.LastBldgVal.GetValueOrDefault()),
                bldgExemptSum = g.Sum(o => o.BldgExempt.GetValueOrDefault())
            }).FirstOrDefault();

            if (valuationSums != null)
            {
                ValuationReturn.RealEstate.TaxableLand = valuationSums.landSum - valuationSums.landExemptSum;
                ValuationReturn.RealEstate.TaxableBuilding = valuationSums.bldgSum - valuationSums.bldgExemptSum;
            }

            var calculations = realEstateContext.TaxRateCalculations.FirstOrDefault(t =>
                t.TownNumber == ValuationReturn.TownNumber);
            if (calculations != null)
            {
                ValuationReturn.OtherTaxInformation.TaxRate = calculations.TaxRate.GetValueOrDefault().ToDecimal();
                
            }

            var defaults = realEstateContext.Defaults.FirstOrDefault(r => r.TownNumber == ValuationReturn.TownNumber);
            if (defaults != null)
            {
                if (realEstateSettings.ApplyCertifiedRatioToTreeGrowthRates)
                {
                    ValuationReturn.ForestTreeGrowth.HardwoodRate =
                        defaults.HardCost.GetValueOrDefault().ToDecimal() * realEstateSettings.CertifiedRatio;
                    ValuationReturn.ForestTreeGrowth.MixedWoodRate =
                        defaults.MixedCost.GetValueOrDefault().ToDecimal() * realEstateSettings.CertifiedRatio;
                    ValuationReturn.ForestTreeGrowth.SoftwoodRate =
                        defaults.SoftCost.GetValueOrDefault().ToDecimal() * realEstateSettings.CertifiedRatio;
                    ValuationReturn.FarmAndOpenSpace.FarmHardwoodRate = defaults.HardCost.GetValueOrDefault().ToDecimal() * realEstateSettings.CertifiedRatio;
                    ValuationReturn.FarmAndOpenSpace.FarmMixedWoodRate = defaults.MixedCost.GetValueOrDefault().ToDecimal() * realEstateSettings.CertifiedRatio;
                    ValuationReturn.FarmAndOpenSpace.FarmSoftwoodRate = defaults.SoftCost.GetValueOrDefault().ToDecimal() * realEstateSettings.CertifiedRatio;
                }
                else
                {
                    ValuationReturn.ForestTreeGrowth.HardwoodRate =
                        defaults.HardCost.GetValueOrDefault().ToDecimal();
                    ValuationReturn.ForestTreeGrowth.MixedWoodRate =
                        defaults.MixedCost.GetValueOrDefault().ToDecimal();
                    ValuationReturn.ForestTreeGrowth.SoftwoodRate =
                        defaults.SoftCost.GetValueOrDefault().ToDecimal();
                    ValuationReturn.FarmAndOpenSpace.FarmHardwoodRate = defaults.HardCost.GetValueOrDefault().ToDecimal();
                    ValuationReturn.FarmAndOpenSpace.FarmMixedWoodRate = defaults.MixedCost.GetValueOrDefault().ToDecimal();
                    ValuationReturn.FarmAndOpenSpace.FarmSoftwoodRate = defaults.SoftCost.GetValueOrDefault().ToDecimal();

                }
            }

            ValuationReturn.OtherTaxInformation.TaxLevy = ((ValuationReturn.RealEstate.TaxableRealEstate + ValuationReturn.PersonalProperty.TaxablePersonalProperty) *
                ValuationReturn.OtherTaxInformation.TaxRate).RoundToMoney();
        }
        private int GetVetExemptionCount(IQueryable<REMaster> masterQuery, bool filterByLivingTrust, bool isLivingTrust,IEnumerable<ExemptCode> exemptCodes)
        {
            var filteredQuery = masterQuery;
            if (filterByLivingTrust)
            {
                filteredQuery = filteredQuery.Where(r => r.RevocableTrust == isLivingTrust);
            }
           

            int exemptCount = 0;
            foreach (var exemptCode in exemptCodes)
            {
                for (int x = 1; x < 4; x++)
                {
                    switch (x)
                    {
                        case 1:
                            exemptCount += filteredQuery.Count(r => r.RiexemptCd1 == exemptCode.Code);
                            break;
                        case 2:
                            exemptCount += filteredQuery.Count(r => r.RiexemptCd2 == exemptCode.Code);
                            break;
                        case 3:
                            exemptCount += filteredQuery.Count(r => r.RiexemptCd3 == exemptCode.Code);
                            break;
                    }
                }
            }

            return exemptCount;
        }

        private decimal GetExemptionAmount(IQueryable<REMaster> masterQuery, IEnumerable<ExemptCode> exemptCodes)
        {
            decimal totalAmount = 0;

            foreach (var exemptCode in exemptCodes)
            {
                totalAmount += masterQuery.Where(r => r.RiexemptCd1 == exemptCode.Code).Sum(r => r.ExemptVal1).GetValueOrDefault();
                totalAmount += masterQuery.Where(r => r.RiexemptCd2 == exemptCode.Code).Sum(r => r.ExemptVal2).GetValueOrDefault();
                totalAmount += masterQuery.Where(r => r.RiexemptCd3 == exemptCode.Code).Sum(r => r.ExemptVal3).GetValueOrDefault();
            }

            return totalAmount;
        }

        private (int count, decimal exemptValue, decimal valuation) GetParsonageStats(IQueryable<REMaster> masterQuery, IEnumerable<ExemptCode> exemptCodes)
        {
            int totalCount = 0;
            decimal totalExempt = 0;
            decimal totalValuation = 0;
            foreach (var exemptCode in exemptCodes)
            { 
              var codeQuery = masterQuery
                    .Where(r => r.RiexemptCd1 == exemptCode.Code || r.RiexemptCd2 == exemptCode.Code ||
                                r.RiexemptCd3 == exemptCode.Code).Select(r => new Tuple<int,int ,int ,int,int, int,int>
                        (r.RiexemptCd1.GetValueOrDefault(),
                            r.ExemptVal1.GetValueOrDefault(), r.RiexemptCd2.GetValueOrDefault(),
                             r.ExemptVal2.GetValueOrDefault(),r.RiexemptCd3.GetValueOrDefault(),
                            r.ExemptVal3.GetValueOrDefault()
                            , r.LastLandVal.GetValueOrDefault() + r.LastBldgVal.GetValueOrDefault()
                            - r.Rlexemption.GetValueOrDefault())).ToList();

              totalCount += codeQuery.Count();
              totalValuation += codeQuery.Sum(r => r.Item7);
              var code1List = codeQuery.Where(r => r.Item1 == exemptCode.Code);
              totalExempt += code1List.Sum(r => r.Item2);


              var code2List = codeQuery.Where(r => r.Item3 == exemptCode.Code);
              totalExempt += code2List.Sum(r => r.Item4);

              var code3List = codeQuery.Where(r => r.Item5 == exemptCode.Code);
              totalExempt += code3List.Sum(r => r.Item6);
            }

            return (count: totalCount, exemptValue: totalExempt, valuation: totalValuation);
        }

        private decimal GetSnowmobileTrailGroomingExemptAmount()
        {
	        decimal totalExempt = 0;
	        if (moduleSettings.PersonalPropertyIsActive)
	        {
		        var ppExemptCodes = personalPropertyContext.ExemptCodes
			        .Where(e => e.MVRCategory == "Snowmobile Trail Grooming Equipment").ToList();

		        var ppmasterQuery = personalPropertyContext.PPMasters.Where(p => p.Deleted != true);
		        if (environmentSettings.IsMultiTown)
		        {
			        ppmasterQuery =
				        ppmasterQuery.Where(p => p.TranCode.GetValueOrDefault() == ValuationReturn.TownNumber);
		        }

		        foreach (var exemptCode in ppExemptCodes)
		        {
			        totalExempt += ppmasterQuery.Where(p => p.ExemptCode1 == exemptCode.Code).Sum(p => p.Exemption1)
				        .GetValueOrDefault();
			        totalExempt += ppmasterQuery.Where(p => p.ExemptCode2 == exemptCode.Code).Sum(p => p.Exemption2)
				        .GetValueOrDefault();
		        }
	        }

	        return totalExempt;
        }


		private (int count, decimal exemptValue) GetSolarAndWindEquipmentStats(IQueryable<REMaster> masterQuery, IEnumerable<ExemptCode> exemptCodes)
        {
	        int totalCount = 0;
	        decimal totalExempt = 0;

	        foreach (var exemptCode in exemptCodes)
	        {
		        totalExempt += masterQuery.Where(r => r.RiexemptCd1 == exemptCode.Code).Sum(r => r.ExemptVal1)
			        .GetValueOrDefault();
		        totalExempt += masterQuery.Where(r => r.RiexemptCd2 == exemptCode.Code).Sum(r => r.ExemptVal2)
			        .GetValueOrDefault();
		        totalExempt += masterQuery.Where(r => r.RiexemptCd3 == exemptCode.Code).Sum(r => r.ExemptVal3)
			        .GetValueOrDefault();

		        totalCount += masterQuery.Count(r => r.RiexemptCd1 == exemptCode.Code);
		        totalCount += masterQuery.Count(r => r.RiexemptCd2 == exemptCode.Code);
		        totalCount += masterQuery.Count(r => r.RiexemptCd3 == exemptCode.Code);
	        }

	        if (moduleSettings.PersonalPropertyIsActive)
	        {
		        var ppExemptCodes = personalPropertyContext.ExemptCodes
			        .Where(e => e.MVRCategory == "Solar and Wind Energy Equipment").ToList();

		        var ppmasterQuery = personalPropertyContext.PPMasters.Where(p => p.Deleted != true);
		        if (environmentSettings.IsMultiTown)
		        {
			        ppmasterQuery =
				        ppmasterQuery.Where(p => p.TranCode.GetValueOrDefault() == ValuationReturn.TownNumber);
		        }

		        foreach (var exemptCode in ppExemptCodes)
		        {
			        totalExempt += ppmasterQuery.Where(p => p.ExemptCode1 == exemptCode.Code).Sum(p => p.Exemption1)
				        .GetValueOrDefault();
			        totalCount += ppmasterQuery.Count(p => p.ExemptCode1 == exemptCode.Code);
			        totalExempt += ppmasterQuery.Where(p => p.ExemptCode2 == exemptCode.Code).Sum(p => p.Exemption2)
				        .GetValueOrDefault();
			        totalCount += ppmasterQuery.Count(p => p.ExemptCode2 == exemptCode.Code);
		        }
	        }

	        return (count: totalCount, exemptValue: totalExempt);
        }

		//private void CalculateIndustrial()
		//{
		//    var reQuery = realEstateContext.REMasters.Where(r => r.Rsdeleted != true);
		//    var ppQuery = personalPropertyContext.PPMasters.Where(p => p.Deleted != true);
		//    decimal industrialRealEstate = 0;
		//    decimal industrialPersonalProperty = 0;
		//    if (environmentSettings.IsMultiTown)
		//    {
		//        reQuery = reQuery.Where(r => r.RitranCode == ValuationReturn.TownNumber);
		//        ppQuery = ppQuery.Where(p => p.TranCode == ValuationReturn.TownNumber);
		//    }

		//    foreach (var buildingCode in realEstateSettings.IndustrialBuildingCodes)
		//    {
		//        var reSum = reQuery.Where(r => r.RibldgCode == buildingCode)
		//            .Sum(r => r.LastLandVal + r.LastBldgVal - r.Rlexemption);
		//        industrialRealEstate += reSum.GetValueOrDefault();
		//    }

		//    if (realEstateSettings.PersonalPropertyIndustrialCategory > 0)
		//    {
		//        var ppJoinQuery = ppQuery.Join(personalPropertyContext.PPValuations, master => master.Account,
		//            valuation => valuation.Account,
		//            (master, valuation) => valuation);
		//        switch (realEstateSettings.PersonalPropertyIndustrialCategory)
		//        {
		//            case 1:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat1Exempt.GetValueOrDefault());
		//                break;
		//            case 2:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat2Exempt.GetValueOrDefault());
		//                break;
		//            case 3:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat3Exempt.GetValueOrDefault());
		//                break;
		//            case 4:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat4Exempt.GetValueOrDefault());
		//                break;
		//            case 5:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat5Exempt.GetValueOrDefault());
		//                break;
		//            case 6:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat6Exempt.GetValueOrDefault());
		//                break;
		//            case 7:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat7Exempt.GetValueOrDefault());
		//                break;
		//            case 8:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat8Exempt.GetValueOrDefault());
		//                break;
		//            case 9:
		//                industrialPersonalProperty = ppJoinQuery.Sum(p => p.Cat9Exempt.GetValueOrDefault());
		//                break;
		//        }
		//    }


		//}
	}
}