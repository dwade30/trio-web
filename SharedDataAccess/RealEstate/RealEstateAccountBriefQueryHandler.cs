﻿using System.Linq;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace SharedDataAccess.RealEstate
{
    public class RealEstateAccountBriefQueryHandler : IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo>
    {
        private IRealEstateContext reContext;
        private DataEnvironmentSettings dataEnvSettings;
        public RealEstateAccountBriefQueryHandler(IRealEstateContext reContext, DataEnvironmentSettings dataEnvSettings)
        {
            this.reContext = reContext;
            this.dataEnvSettings = dataEnvSettings;
        }
        public RealEstateAccountBriefInfo ExecuteQuery(RealEstateAccountBriefQuery query)
        {
            var account = reContext.REMasters.Where(m => m.Rsaccount == query.Account && m.Rscard == 1).FirstOrDefault();
            var brief = new RealEstateAccountBriefInfo();
            if (account != null)
            {
                brief.DeedName1 = account.DeedName1;
                brief.DeedName2 = account.DeedName2;
                brief.Account = query.Account;
                brief.Deleted = account.Rsdeleted.GetValueOrDefault();
                brief.LocationNumber = account.RslocNumAlph.ToIntegerValue();
                brief.LocationStreet = account.RslocStreet?.Trim();
                brief.MapLot = account.RsmapLot?.Trim();
                brief.PartyId1 = account.OwnerPartyId.GetValueOrDefault();
                brief.PartyId2 = account.SecOwnerPartyId.GetValueOrDefault();
                brief.Reference1 = account.Rsref1?.Trim();
                brief.Reference2 = account.Rsref2?.Trim();
                brief.IsTaxAcquired = account.TaxAcquired.GetValueOrDefault();
                brief.TownId = dataEnvSettings.IsMultiTown ? account.RitranCode.GetValueOrDefault() : 0;
            }

            return brief;
        }
    }
}