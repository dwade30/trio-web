﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedApplication.TaxCollections.Models;
using SharedDataAccess.RealEstate.Configuration;

namespace SharedDataAccess.RealEstate
{
    public class RealEstateContext : DataContext<RealEstateContext>, IRealEstateContext
    {
        public DbSet<REDefault> Defaults { get; set; }
        public DbSet<Mvrreport> MvrReports { get; set; }
        public DbSet<TranCode> TranCodes { get; set; }
        public DbSet<REMaster> REMasters { get; set; }
        public DbSet<ExemptCode> ExemptCodes { get; set; }
        public DbSet<BookPage> BookPages { get; set; }
        public DbSet<LandType> LandTypes { get; set; }
        public DbSet<PictureRecord> PictureRecords { get; set; }
        public DbSet<PreviousOwner> PreviousOwners { get; set; }
        public DbQuery<REAccountPartyAddressView> REAccountPartyAddressViews { get; set; }
        public DbSet<ReportTitle> ReportTitles { get; set; }
        public DbSet<Srmaster> SRMasters { get; set; }
        public DbSet<Summrecord> SummationRecords { get; set; }
        public DbSet<Taxratecalculation> TaxRateCalculations { get; set; }

        IQueryable<REDefault> IRealEstateContext.Defaults => Defaults;
        IQueryable<Mvrreport> IRealEstateContext.MvrReports => MvrReports;
        IQueryable<TranCode> IRealEstateContext.TranCodes => TranCodes;
        IQueryable<REMaster> IRealEstateContext.REMasters => REMasters;
        IQueryable<ExemptCode> IRealEstateContext.ExemptCodes => ExemptCodes;
        IQueryable<BookPage> IRealEstateContext.BookPages => BookPages;
        IQueryable<LandType> IRealEstateContext.LandTypes => LandTypes;
        IQueryable<PreviousOwner> IRealEstateContext.PreviousOwners => PreviousOwners;
        IQueryable<ReportTitle> IRealEstateContext.ReportTitles => ReportTitles;
        IQueryable<PictureRecord> IRealEstateContext.PictureRecords => PictureRecords;
        IQueryable<Srmaster> IRealEstateContext.SRMasters => SRMasters;
        IQueryable<Summrecord> IRealEstateContext.SummationRecords => SummationRecords;
        IQueryable<Taxratecalculation> IRealEstateContext.TaxRateCalculations => TaxRateCalculations;
        IQueryable<REAccountPartyAddressView> IRealEstateContext.REAccountPartyAddressViews =>
            REAccountPartyAddressViews;
        public RealEstateContext(DbContextOptions<RealEstateContext> options) : base(options)
        {    
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new REDefaultConfiguration());
            modelBuilder.ApplyConfiguration(new TranCodeConfiguration());
            modelBuilder.ApplyConfiguration(new REMasterConfiguration());
            modelBuilder.ApplyConfiguration(new REAccountPartyAddressViewConfiguration());
            modelBuilder.ApplyConfiguration(new ExemptCodeConfiguration());
            modelBuilder.ApplyConfiguration(new BookPageConfiguration());
            modelBuilder.ApplyConfiguration(new LandTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PictureRecordConfiguration());
            modelBuilder.ApplyConfiguration(new PreviousOwnerConfiguration());
            modelBuilder.ApplyConfiguration(new MvrreportConfiguration());
            modelBuilder.ApplyConfiguration(new ReportTitleConfiguration());
            modelBuilder.ApplyConfiguration(new SRMasterConfiguration());
            modelBuilder.ApplyConfiguration(new SummRecordConfiguration());
            modelBuilder.ApplyConfiguration(new TaxRateCalculationConfiguration());
        }
    }
}
