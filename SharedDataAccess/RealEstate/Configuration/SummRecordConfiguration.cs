﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class SummRecordConfiguration : IEntityTypeConfiguration<Summrecord>
    {
        public void Configure(EntityTypeBuilder<Summrecord> builder)
        {
            builder.ToTable("Summrecord");

            builder.HasIndex(e => new { e.Sacct, e.CardNumber })
                .HasName("ix_AccountCard");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Comval1).HasColumnName("comval1");

            builder.Property(e => e.Comval2).HasColumnName("comval2");

            builder.Property(e => e.Dwellcode).HasColumnName("dwellcode");

            builder.Property(e => e.Sacct).HasColumnName("SACCT");

            builder.Property(e => e.Sacres).HasColumnName("SACRES");

            builder.Property(e => e.Sage).HasColumnName("SAge");

            builder.Property(e => e.Sbuilt).HasColumnName("SBUILT");

            builder.Property(e => e.Scmocc1).HasColumnName("SCMOCC1");

            builder.Property(e => e.Scmocc2).HasColumnName("SCMOCC2");

            builder.Property(e => e.Scmval1).HasColumnName("SCMVAL1");

            builder.Property(e => e.Scmval2).HasColumnName("SCMVAL2");

            builder.Property(e => e.Scode).HasColumnName("SCODE");

            builder.Property(e => e.Sdate)
                .HasColumnName("SDATE")
                .HasMaxLength(50);

            builder.Property(e => e.Sdwellrcnld).HasColumnName("SDWELLRCNLD");

            builder.Property(e => e.Sexpansion)
                .HasColumnName("SEXPANSION")
                .HasMaxLength(50);

            builder.Property(e => e.Slanda1).HasColumnName("slanda1");

            builder.Property(e => e.Slanda2).HasColumnName("slanda2");

            builder.Property(e => e.Slanda3).HasColumnName("slanda3");

            builder.Property(e => e.Slanda4).HasColumnName("slanda4");

            builder.Property(e => e.Slanda5).HasColumnName("slanda5");

            builder.Property(e => e.Slanda6).HasColumnName("slanda6");

            builder.Property(e => e.Slanda7).HasColumnName("slanda7");

            builder.Property(e => e.Slandc1).HasColumnName("slandc1");

            builder.Property(e => e.Slandc2).HasColumnName("slandc2");

            builder.Property(e => e.Slandc3).HasColumnName("slandc3");

            builder.Property(e => e.Slandc4).HasColumnName("slandc4");

            builder.Property(e => e.Slandc5).HasColumnName("slandc5");

            builder.Property(e => e.Slandc6).HasColumnName("slandc6");

            builder.Property(e => e.Slandc7).HasColumnName("slandc7");

            builder.Property(e => e.Slandv1).HasColumnName("slandv1");

            builder.Property(e => e.Slandv2).HasColumnName("slandv2");

            builder.Property(e => e.Slandv3).HasColumnName("slandv3");

            builder.Property(e => e.Slandv4).HasColumnName("slandv4");

            builder.Property(e => e.Slandv5).HasColumnName("slandv5");

            builder.Property(e => e.Slandv6).HasColumnName("slandv6");

            builder.Property(e => e.Slandv7).HasColumnName("slandv7");

            builder.Property(e => e.Sobc1).HasColumnName("sobc1");

            builder.Property(e => e.Sobc10).HasColumnName("sobc10");

            builder.Property(e => e.Sobc2).HasColumnName("sobc2");

            builder.Property(e => e.Sobc3).HasColumnName("sobc3");

            builder.Property(e => e.Sobc4).HasColumnName("sobc4");

            builder.Property(e => e.Sobc5).HasColumnName("sobc5");

            builder.Property(e => e.Sobc6).HasColumnName("sobc6");

            builder.Property(e => e.Sobc7).HasColumnName("sobc7");

            builder.Property(e => e.Sobc8).HasColumnName("sobc8");

            builder.Property(e => e.Sobc9).HasColumnName("sobc9");

            builder.Property(e => e.Sobv1).HasColumnName("sobv1");

            builder.Property(e => e.Sobv10).HasColumnName("sobv10");

            builder.Property(e => e.Sobv2).HasColumnName("sobv2");

            builder.Property(e => e.Sobv3).HasColumnName("sobv3");

            builder.Property(e => e.Sobv4).HasColumnName("sobv4");

            builder.Property(e => e.Sobv5).HasColumnName("sobv5");

            builder.Property(e => e.Sobv6).HasColumnName("sobv6");

            builder.Property(e => e.Sobv7).HasColumnName("sobv7");

            builder.Property(e => e.Sobv8).HasColumnName("sobv8");

            builder.Property(e => e.Sobv9).HasColumnName("sobv9");

            builder.Property(e => e.Sovbldga).HasColumnName("SOVBLDGA");

            builder.Property(e => e.Sovbldgc).HasColumnName("SOVBLDGC");

            builder.Property(e => e.Sovlanda).HasColumnName("SOVLANDA");

            builder.Property(e => e.Sovlandc).HasColumnName("SOVLANDC");

            builder.Property(e => e.SrecordNumber).HasColumnName("SRecordNumber");

            builder.Property(e => e.Ssfla).HasColumnName("SSFLA");

            builder.Property(e => e.Sstyle).HasColumnName("SSTYLE");

            builder.Property(e => e.Strancode).HasColumnName("STRANCODE");
        }
    }
}