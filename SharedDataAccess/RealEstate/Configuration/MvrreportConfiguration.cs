﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class MvrreportConfiguration : IEntityTypeConfiguration<Mvrreport>
    {
        public void Configure(EntityTypeBuilder<Mvrreport> builder)
        {
            builder.ToTable("MVRReport");
            builder.HasIndex(e => e.Code)
                .HasName("ix_Code");

            builder.Property(e => e.Id).HasColumnName("ID");
        }
    }
}