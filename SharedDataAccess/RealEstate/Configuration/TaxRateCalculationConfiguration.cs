﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class TaxRateCalculationConfiguration : IEntityTypeConfiguration<Taxratecalculation>
    {
        public void Configure(EntityTypeBuilder<Taxratecalculation> builder)
        {
            builder.ToTable("taxratecalculation");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Beteexempt).HasColumnName("BETEExempt");

            builder.Property(e => e.BetereimburseValue).HasColumnName("BETEReimburseValue");

            builder.Property(e => e.Betereimbursement).HasColumnName("BETEReimbursement");

            builder.Property(e => e.CollectionDate).HasColumnType("datetime");

            builder.Property(e => e.CommitmentDate).HasColumnType("datetime");

            builder.Property(e => e.County).HasMaxLength(255);

            builder.Property(e => e.DueDate).HasColumnType("datetime");

            builder.Property(e => e.DueDate2).HasColumnType("datetime");

            builder.Property(e => e.DueDate3).HasColumnType("datetime");

            builder.Property(e => e.DueDate4).HasColumnType("datetime");

            builder.Property(e => e.FiscalEndDate).HasColumnType("datetime");

            builder.Property(e => e.FiscalStartDate).HasColumnType("datetime");

            builder.Property(e => e.InterestDate).HasColumnType("datetime");

            builder.Property(e => e.InterestDate2).HasColumnType("datetime");

            builder.Property(e => e.InterestDate3).HasColumnType("datetime");

            builder.Property(e => e.InterestDate4).HasColumnType("datetime");

            builder.Property(e => e.Reamount).HasColumnName("REAmount");

            builder.Property(e => e.TaxCollector).HasMaxLength(255);

            builder.Property(e => e.Tif).HasColumnName("TIF");

            builder.Property(e => e.Tifbeteval).HasColumnName("TIFBETEVal");

            builder.Property(e => e.TifmuniRetentionPerc).HasColumnName("TIFMuniRetentionPerc");

            builder.Property(e => e.Treasurer).HasMaxLength(255);

            builder.Property(e => e.UseEnhancedBete).HasColumnName("UseEnhancedBETE");
        }
    }
}