﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class PictureRecordConfiguration : IEntityTypeConfiguration<PictureRecord>
    {
        public void Configure(EntityTypeBuilder<PictureRecord> builder)
        {
            builder.ToTable("PictureRecord");

            builder.HasIndex(e => new { e.Account, e.Card, e.PicNum })
                .HasName("ix_AccountCardPic");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.Account).HasColumnName("MRAccountNumber");

            builder.Property(e => e.PictureLocation).HasMaxLength(1024);

            builder.Property(e => e.Card).HasColumnName("RSCard");
        }
    }
}