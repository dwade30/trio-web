﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class REDefaultConfiguration : IEntityTypeConfiguration<REDefault>
    {
        public void Configure(EntityTypeBuilder<REDefault> builder)
        {
            builder.ToTable("Defaults");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.TaxNoticeTitle).HasMaxLength(255);
        }
    }
}