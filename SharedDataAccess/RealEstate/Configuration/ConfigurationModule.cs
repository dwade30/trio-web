﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedDataAccess.Budgetary;

namespace SharedDataAccess.RealEstate.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{

			builder.RegisterType<MasterQueryHandler>()
				.As<IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>>>();
            builder.RegisterType<RealEstateAccountBriefQueryHandler>()
                .As<IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo>>();
            RegisterRealEstateContext(ref builder);
            builder.RegisterType<RealEstateAccountTotalsQueryHandler>()
                .As<IQueryHandler<RealEstateAccountTotalsQuery, RealEstateAccountTotals>>();
            builder.RegisterType<RealEstateBookPageQueryHandler>()
                .As<IQueryHandler<RealEstateBookPageQuery, IEnumerable<BookPage>>>();
            RegisterLandTypes(ref builder);
        }

        private void RegisterRealEstateContext(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var activeModules = f.Resolve<IGlobalActiveModuleSettings>();
                if (activeModules.RealEstateIsActive || activeModules.TaxBillingIsActive || activeModules.TaxCollectionsIsActive)
                {
                    var tcf = f.Resolve<ITrioContextFactory>();
                    return tcf.GetRealEstateContext();
                }
                IRealEstateContext emptyContext = new RealEstateEmptyContext();
                return emptyContext;
            }).As<IRealEstateContext>();
        }

        private void RegisterLandTypes(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var dispatcher = f.Resolve<CommandDispatcher>();
                return dispatcher.Send(new GetLandTypes()).Result;

            }).AsSelf();
        }
    }
}
