﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class REMasterConfiguration : IEntityTypeConfiguration<REMaster>
    {
        public void Configure(EntityTypeBuilder<REMaster> builder)
        {
            builder.ToTable("Master");

            builder.HasIndex(e => e.Rsaccount);

            builder.HasIndex(e => new { e.Rsaccount, e.Rscard })
                .HasName("ix_AccountCard");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AccountId)
                .HasColumnName("AccountID")
                .HasMaxLength(255);

            builder.Property(e => e.CardId)
                .HasColumnName("CardID")
                .HasMaxLength(255);

            builder.Property(e => e.DateCreated).HasColumnType("datetime");

            builder.Property(e => e.DateInspected).HasColumnType("datetime");

            builder.Property(e => e.DeedName1).HasMaxLength(255);

            builder.Property(e => e.DeedName2).HasMaxLength(255);

            builder.Property(e => e.Hialt1BldgCode).HasColumnName("HIAlt1BldgCode");

            builder.Property(e => e.Hialt2BldgCode).HasColumnName("HIAlt2BldgCode");

            builder.Property(e => e.HiupdCode)
                .HasColumnName("HIUpdCode")
                .HasMaxLength(255);

            builder.Property(e => e.HivalBldgCode).HasColumnName("HIValBldgCode");

            builder.Property(e => e.HivalLandCode).HasColumnName("HIValLandCode");

            builder.Property(e => e.HlUpdate)
                .HasColumnName("hlUpdate")
                .HasColumnType("datetime");

            builder.Property(e => e.Hlalt1Bldg).HasColumnName("HLAlt1Bldg");

            builder.Property(e => e.Hlalt2Bldg).HasColumnName("HLAlt2Bldg");

            builder.Property(e => e.HlcompValBldg).HasColumnName("HLCompValBldg");

            builder.Property(e => e.HlcompValLand).HasColumnName("HLCompValLand");

            builder.Property(e => e.Hldolexemption).HasColumnName("HLDOLExemption");

            builder.Property(e => e.HlvalBldg).HasColumnName("HLValBldg");

            builder.Property(e => e.HlvalLand).HasColumnName("HLValLand");

            builder.Property(e => e.OwnerPartyId).HasColumnName("OwnerPartyID");

            builder.Property(e => e.Piacres).HasColumnName("PIAcres");

            builder.Property(e => e.Piland1Inf).HasColumnName("PILand1Inf");

            builder.Property(e => e.Piland1InfCode).HasColumnName("PILand1InfCode");

            builder.Property(e => e.Piland1Type).HasColumnName("PILand1Type");

            builder.Property(e => e.Piland1UnitsA)
                .HasColumnName("PILand1UnitsA")
                .HasMaxLength(255);

            builder.Property(e => e.Piland1UnitsB)
                .HasColumnName("PILand1UnitsB")
                .HasMaxLength(255);

            builder.Property(e => e.Piland2Inf).HasColumnName("PILand2Inf");

            builder.Property(e => e.Piland2InfCode).HasColumnName("PILand2InfCode");

            builder.Property(e => e.Piland2Type).HasColumnName("PILand2Type");

            builder.Property(e => e.Piland2UnitsA)
                .HasColumnName("PILand2UnitsA")
                .HasMaxLength(255);

            builder.Property(e => e.Piland2UnitsB)
                .HasColumnName("PILand2UnitsB")
                .HasMaxLength(255);

            builder.Property(e => e.Piland3Inf).HasColumnName("PILand3Inf");

            builder.Property(e => e.Piland3InfCode).HasColumnName("PILand3InfCode");

            builder.Property(e => e.Piland3Type).HasColumnName("PILand3Type");

            builder.Property(e => e.Piland3UnitsA)
                .HasColumnName("PILand3UnitsA")
                .HasMaxLength(255);

            builder.Property(e => e.Piland3UnitsB)
                .HasColumnName("PILand3UnitsB")
                .HasMaxLength(255);

            builder.Property(e => e.Piland4Inf).HasColumnName("PILand4Inf");

            builder.Property(e => e.Piland4InfCode).HasColumnName("PILand4InfCode");

            builder.Property(e => e.Piland4Type).HasColumnName("PILand4Type");

            builder.Property(e => e.Piland4UnitsA)
                .HasColumnName("PILand4UnitsA")
                .HasMaxLength(255);

            builder.Property(e => e.Piland4UnitsB)
                .HasColumnName("PILand4UnitsB")
                .HasMaxLength(255);

            builder.Property(e => e.Piland5Inf).HasColumnName("PILand5Inf");

            builder.Property(e => e.Piland5InfCode).HasColumnName("PILand5InfCode");

            builder.Property(e => e.Piland5Type).HasColumnName("PILand5Type");

            builder.Property(e => e.Piland5UnitsA)
                .HasColumnName("PILand5UnitsA")
                .HasMaxLength(255);

            builder.Property(e => e.Piland5UnitsB)
                .HasColumnName("PILand5UnitsB")
                .HasMaxLength(255);

            builder.Property(e => e.Piland6Inf).HasColumnName("PILand6Inf");

            builder.Property(e => e.Piland6InfCode).HasColumnName("PILand6InfCode");

            builder.Property(e => e.Piland6Type).HasColumnName("PILand6Type");

            builder.Property(e => e.Piland6UnitsA)
                .HasColumnName("PILand6UnitsA")
                .HasMaxLength(255);

            builder.Property(e => e.Piland6UnitsB)
                .HasColumnName("PILand6UnitsB")
                .HasMaxLength(255);

            builder.Property(e => e.Piland7Inf).HasColumnName("PILand7Inf");

            builder.Property(e => e.Piland7InfCode).HasColumnName("PILand7InfCode");

            builder.Property(e => e.Piland7Type).HasColumnName("PILand7Type");

            builder.Property(e => e.Piland7UnitsA)
                .HasColumnName("PILand7UnitsA")
                .HasMaxLength(255);

            builder.Property(e => e.Piland7UnitsB)
                .HasColumnName("PILand7UnitsB")
                .HasMaxLength(255);

            builder.Property(e => e.Pineighborhood).HasColumnName("PINeighborhood");

            builder.Property(e => e.Piopen1).HasColumnName("PIOpen1");

            builder.Property(e => e.Piopen2).HasColumnName("PIOpen2");

            builder.Property(e => e.PisaleFinancing).HasColumnName("PISaleFinancing");

            builder.Property(e => e.PisalePrice).HasColumnName("PISalePrice");

            builder.Property(e => e.PisaleType).HasColumnName("PISaleType");

            builder.Property(e => e.PisaleValidity).HasColumnName("PISaleValidity");

            builder.Property(e => e.PisaleVerified).HasColumnName("PISaleVerified");

            builder.Property(e => e.PisecZone).HasColumnName("PISecZone");

            builder.Property(e => e.Pistreet).HasColumnName("PIStreet");

            builder.Property(e => e.PistreetCode).HasColumnName("PIStreetCode");

            builder.Property(e => e.Pitopography1).HasColumnName("PITopography1");

            builder.Property(e => e.Pitopography2).HasColumnName("PITopography2");

            builder.Property(e => e.Piutilities1).HasColumnName("PIUtilities1");

            builder.Property(e => e.Piutilities2).HasColumnName("PIUtilities2");

            builder.Property(e => e.Pixcoord)
                .HasColumnName("PIXCoord")
                .HasMaxLength(255);

            builder.Property(e => e.Piycoord)
                .HasColumnName("PIYCoord")
                .HasMaxLength(255);

            builder.Property(e => e.Pizone).HasColumnName("PIZone");

            builder.Property(e => e.ReasonAccountLocked).HasMaxLength(255);

            builder.Property(e => e.RibldgCode).HasColumnName("RIBldgCode");

            builder.Property(e => e.RiexemptCd1).HasColumnName("RIExemptCD1");

            builder.Property(e => e.RiexemptCd2).HasColumnName("RIExemptCD2");

            builder.Property(e => e.RiexemptCd3).HasColumnName("RIExemptCD3");

            builder.Property(e => e.RilandCode).HasColumnName("RILandCode");

            builder.Property(e => e.RitranCode).HasColumnName("RITranCode");

            builder.Property(e => e.RlbldgVal).HasColumnName("RLBldgVal");

            builder.Property(e => e.Rlexemption).HasColumnName("RLExemption");

            builder.Property(e => e.RllandVal).HasColumnName("RLLandVal");

            builder.Property(e => e.Rsaccount).HasColumnName("RSAccount");

            builder.Property(e => e.Rscard).HasColumnName("RSCard");

            builder.Property(e => e.Rsdeleted).HasColumnName("RSDeleted");

            builder.Property(e => e.RsdwellingCode)
                .HasColumnName("RSDwellingCode")
                .HasMaxLength(255);

            builder.Property(e => e.Rshard).HasColumnName("RSHard");

            builder.Property(e => e.RshardValue).HasColumnName("RSHardValue");

            builder.Property(e => e.RslocApt)
                .HasColumnName("RSLocApt")
                .HasMaxLength(255);

            builder.Property(e => e.RslocNumAlph)
                .HasColumnName("RSLocNumAlph")
                .HasMaxLength(255);

            builder.Property(e => e.RslocStreet)
                .HasColumnName("RSLocStreet")
                .HasMaxLength(255);

            builder.Property(e => e.RsmapLot)
                .HasColumnName("RSMapLot")
                .HasMaxLength(255);

            builder.Property(e => e.Rsmixed).HasColumnName("RSMixed");

            builder.Property(e => e.RsmixedValue).HasColumnName("RSMixedValue");

            builder.Property(e => e.Rsother).HasColumnName("RSOther");

            builder.Property(e => e.RsotherValue).HasColumnName("RSOtherValue");

            builder.Property(e => e.RsoutbuildingCode)
                .HasColumnName("RSOutbuildingCode")
                .HasMaxLength(255);

            builder.Property(e => e.RspreviousMaster)
                .HasColumnName("RSPreviousMaster")
                .HasMaxLength(255);

            builder.Property(e => e.RspropertyCode)
                .HasColumnName("RSPropertyCode")
                .HasMaxLength(255);

            builder.Property(e => e.Rsref1)
                .HasColumnName("RSRef1")
                .HasMaxLength(255);

            builder.Property(e => e.Rsref2)
                .HasColumnName("RSRef2")
                .HasMaxLength(255);

            builder.Property(e => e.Rssoft).HasColumnName("RSSoft");

            builder.Property(e => e.RssoftValue).HasColumnName("RSSoftValue");

            builder.Property(e => e.SaleDate).HasColumnType("datetime");

            builder.Property(e => e.SecOwnerPartyId).HasColumnName("SecOwnerPartyID");

            builder.Property(e => e.TADate)
                .HasColumnName("TADate")
                .HasColumnType("datetime");
        }
    }
}