﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class LandTypeConfiguration : IEntityTypeConfiguration<LandType>
    {
        public void Configure(EntityTypeBuilder<LandType> builder)
        {
            builder.ToTable("LandType");

            builder.HasIndex(e => e.Code)
                .HasName("ix_Code");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Description).HasMaxLength(50);

            builder.Property(e => e.ShortDescription).HasMaxLength(50);

        }
    }
}