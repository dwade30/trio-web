﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class BookPageConfiguration : IEntityTypeConfiguration<BookPage>
    {
        public void Configure(EntityTypeBuilder<BookPage> builder)
        {
            builder.ToTable("BookPage");

            builder.HasIndex(e => e.Account)
                .HasName("ix_Account");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Book)
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.Bpdate)
                .HasColumnName("BPDate")
                .HasMaxLength(255);

            builder.Property(e => e.Page)
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.SaleDate).HasColumnType("datetime");
        }
    }
}