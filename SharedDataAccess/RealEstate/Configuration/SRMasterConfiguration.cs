﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class SRMasterConfiguration : IEntityTypeConfiguration<Srmaster>
    {
        public void Configure(EntityTypeBuilder<Srmaster> builder)
        {
            builder.ToTable("SRMaster");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.DateCreated).HasColumnType("datetime");

            builder.Property(e => e.DateInspected).HasColumnType("datetime");

            builder.Property(e => e.Email)
                .HasColumnName("EMail")
                .HasMaxLength(255);

            builder.Property(e => e.Hialt1bldgcode).HasColumnName("HIALT1BLDGCODE");

            builder.Property(e => e.Hialt2bldgcode).HasColumnName("HIALT2BLDGCODE");

            builder.Property(e => e.Hiupdcode)
                .HasColumnName("HIUPDCODE")
                .HasMaxLength(50);

            builder.Property(e => e.Hivalbldgcode).HasColumnName("HIVALBLDGCODE");

            builder.Property(e => e.Hivallandcode).HasColumnName("HIVALLANDCODE");

            builder.Property(e => e.Hlalt1bldg).HasColumnName("HLALT1BLDG");

            builder.Property(e => e.Hlalt2bldg).HasColumnName("HLALT2BLDG");

            builder.Property(e => e.Hlcompvalbldg).HasColumnName("HLCOMPVALBLDG");

            builder.Property(e => e.Hlcompvalland).HasColumnName("HLCOMPVALLAND");

            builder.Property(e => e.Hldolexemption).HasColumnName("HLDOLEXEMPTION");

            builder.Property(e => e.Hlupdate)
                .HasColumnName("hlupdate")
                .HasColumnType("datetime");

            builder.Property(e => e.Hlvalbldg).HasColumnName("HLVALBLDG");

            builder.Property(e => e.Hlvalland).HasColumnName("HLVALLAND");

            builder.Property(e => e.Lastbldgval).HasColumnName("LASTBLDGVAL");

            builder.Property(e => e.Lastlandval).HasColumnName("LASTLANDVAL");

            builder.Property(e => e.PhoneDescription).HasMaxLength(50);

            builder.Property(e => e.Piacres).HasColumnName("PIACRES");

            builder.Property(e => e.Piland1Inf).HasColumnName("PILand1Inf");

            builder.Property(e => e.Piland1infcode).HasColumnName("PILAND1INFCODE");

            builder.Property(e => e.Piland1type).HasColumnName("PILAND1TYPE");

            builder.Property(e => e.Piland1unitsa)
                .HasColumnName("PILAND1UNITSA")
                .HasMaxLength(50);

            builder.Property(e => e.Piland1unitsb)
                .HasColumnName("PILAND1UNITSB")
                .HasMaxLength(50);

            builder.Property(e => e.Piland2Inf).HasColumnName("PILand2Inf");

            builder.Property(e => e.Piland2infcode).HasColumnName("PILAND2INFCODE");

            builder.Property(e => e.Piland2type).HasColumnName("PILAND2TYPE");

            builder.Property(e => e.Piland2unitsa)
                .HasColumnName("PILAND2UNITSA")
                .HasMaxLength(50);

            builder.Property(e => e.Piland2unitsb)
                .HasColumnName("PILAND2UNITSB")
                .HasMaxLength(50);

            builder.Property(e => e.Piland3Inf).HasColumnName("PILand3Inf");

            builder.Property(e => e.Piland3infcode).HasColumnName("PILAND3INFCODE");

            builder.Property(e => e.Piland3type).HasColumnName("PILAND3TYPE");

            builder.Property(e => e.Piland3unitsa)
                .HasColumnName("PILAND3UNITSA")
                .HasMaxLength(50);

            builder.Property(e => e.Piland3unitsb)
                .HasColumnName("PILAND3UNITSB")
                .HasMaxLength(50);

            builder.Property(e => e.Piland4Inf).HasColumnName("PILand4Inf");

            builder.Property(e => e.Piland4infcode).HasColumnName("PILAND4INFCODE");

            builder.Property(e => e.Piland4type).HasColumnName("PILAND4TYPE");

            builder.Property(e => e.Piland4unitsa)
                .HasColumnName("PILAND4UNITSA")
                .HasMaxLength(50);

            builder.Property(e => e.Piland4unitsb)
                .HasColumnName("PILAND4UNITSB")
                .HasMaxLength(50);

            builder.Property(e => e.Piland5Inf).HasColumnName("PILand5Inf");

            builder.Property(e => e.Piland5infcode).HasColumnName("PILAND5INFCODE");

            builder.Property(e => e.Piland5type).HasColumnName("PILAND5TYPE");

            builder.Property(e => e.Piland5unitsa)
                .HasColumnName("PILAND5UNITSA")
                .HasMaxLength(50);

            builder.Property(e => e.Piland5unitsb)
                .HasColumnName("PILAND5UNITSB")
                .HasMaxLength(50);

            builder.Property(e => e.Piland6Inf).HasColumnName("PILand6Inf");

            builder.Property(e => e.Piland6infcode).HasColumnName("PILAND6INFCODE");

            builder.Property(e => e.Piland6type).HasColumnName("PILAND6TYPE");

            builder.Property(e => e.Piland6unitsa)
                .HasColumnName("PILAND6UNITSA")
                .HasMaxLength(50);

            builder.Property(e => e.Piland6unitsb)
                .HasColumnName("PILAND6UNITSB")
                .HasMaxLength(50);

            builder.Property(e => e.Piland7Inf).HasColumnName("PILand7Inf");

            builder.Property(e => e.Piland7infcode).HasColumnName("PILAND7INFCODE");

            builder.Property(e => e.Piland7type).HasColumnName("PILAND7TYPE");

            builder.Property(e => e.Piland7unitsa)
                .HasColumnName("PILAND7UNITSA")
                .HasMaxLength(50);

            builder.Property(e => e.Piland7unitsb)
                .HasColumnName("PILAND7UNITSB")
                .HasMaxLength(50);

            builder.Property(e => e.Pineighborhood).HasColumnName("PINEIGHBORHOOD");

            builder.Property(e => e.Piopen1).HasColumnName("PIOPEN1");

            builder.Property(e => e.Piopen2).HasColumnName("PIOPEN2");

            builder.Property(e => e.Pisaledateday)
                .HasColumnName("PISALEDATEDAY")
                .HasMaxLength(50);

            builder.Property(e => e.Pisaledatemonth)
                .HasColumnName("PISALEDATEMONTH")
                .HasMaxLength(50);

            builder.Property(e => e.Pisaledateyear)
                .HasColumnName("PISALEDATEYEAR")
                .HasMaxLength(50);

            builder.Property(e => e.Pisalefinancing).HasColumnName("PISALEFINANCING");

            builder.Property(e => e.Pisaleprice).HasColumnName("PISALEPRICE");

            builder.Property(e => e.Pisaletype).HasColumnName("PISALETYPE");

            builder.Property(e => e.Pisalevalidity).HasColumnName("PISALEVALIDITY");

            builder.Property(e => e.Pisaleverified).HasColumnName("PISALEVERIFIED");

            builder.Property(e => e.Piseczone).HasColumnName("PISECZONE");

            builder.Property(e => e.Pistreet).HasColumnName("PISTREET");

            builder.Property(e => e.Pistreetcode).HasColumnName("PISTREETCODE");

            builder.Property(e => e.Pitopography1).HasColumnName("PITOPOGRAPHY1");

            builder.Property(e => e.Pitopography2).HasColumnName("PITOPOGRAPHY2");

            builder.Property(e => e.Piutilities1).HasColumnName("PIUTILITIES1");

            builder.Property(e => e.Piutilities2).HasColumnName("PIUTILITIES2");

            builder.Property(e => e.Pixcoord).HasColumnName("PIXCOORD");

            builder.Property(e => e.Piycoord).HasColumnName("PIYCOORD");

            builder.Property(e => e.Pizone).HasColumnName("PIZONE");

            builder.Property(e => e.ReasonAccountLocked).HasMaxLength(255);

            builder.Property(e => e.Ribldgcode).HasColumnName("RIBLDGCODE");

            builder.Property(e => e.Riexemptcd1).HasColumnName("RIEXEMPTCD1");

            builder.Property(e => e.Riexemptcd2).HasColumnName("RIEXEMPTCD2");

            builder.Property(e => e.Riexemptcd3).HasColumnName("RIEXEMPTCD3");

            builder.Property(e => e.Rilandcode).HasColumnName("RILANDCODE");

            builder.Property(e => e.Ritrancode).HasColumnName("RITRANCODE");

            builder.Property(e => e.Rlbldgval).HasColumnName("RLBLDGVAL");

            builder.Property(e => e.Rlexemption).HasColumnName("RLEXEMPTION");

            builder.Property(e => e.Rllandval).HasColumnName("RLLANDVAL");

            builder.Property(e => e.Rsaccount).HasColumnName("RSAccount");

            builder.Property(e => e.Rsaddr1)
                .HasColumnName("RSADDR1")
                .HasMaxLength(50);

            builder.Property(e => e.Rsaddr2)
                .HasColumnName("RSADDR2")
                .HasMaxLength(50);

            builder.Property(e => e.Rsaddr3)
                .HasColumnName("RSADDR3")
                .HasMaxLength(50);

            builder.Property(e => e.Rscard).HasColumnName("RSCard");

            builder.Property(e => e.Rsdeleted).HasColumnName("RSDeleted");

            builder.Property(e => e.Rsdwellingcode)
                .HasColumnName("RSDWELLINGCODE")
                .HasMaxLength(50);

            builder.Property(e => e.Rshard).HasColumnName("RSHard");

            builder.Property(e => e.RshardValue).HasColumnName("RSHardValue");

            builder.Property(e => e.Rslocapt)
                .HasColumnName("RSLOCAPT")
                .HasMaxLength(50);

            builder.Property(e => e.Rslocnumalph)
                .HasColumnName("RSLOCNUMALPH")
                .HasMaxLength(50);

            builder.Property(e => e.Rslocstreet)
                .HasColumnName("RSLOCSTREET")
                .HasMaxLength(50);

            builder.Property(e => e.Rsmaplot)
                .HasColumnName("RSMAPLOT")
                .HasMaxLength(50);

            builder.Property(e => e.Rsmixed).HasColumnName("RSMixed");

            builder.Property(e => e.RsmixedValue).HasColumnName("RSMixedValue");

            builder.Property(e => e.Rsname)
                .HasColumnName("RSNAME")
                .HasMaxLength(50);

            builder.Property(e => e.Rsother).HasColumnName("RSOther");

            builder.Property(e => e.RsotherValue).HasColumnName("RSOtherValue");

            builder.Property(e => e.Rsoutbuildingcode)
                .HasColumnName("RSOUTBUILDINGCODE")
                .HasMaxLength(50);

            builder.Property(e => e.Rspreviousmaster)
                .HasColumnName("RSPREVIOUSMASTER")
                .HasMaxLength(50);

            builder.Property(e => e.Rspropertycode)
                .HasColumnName("RSPROPERTYCODE")
                .HasMaxLength(50);

            builder.Property(e => e.Rsref1)
                .HasColumnName("RSREF1")
                .HasMaxLength(50);

            builder.Property(e => e.Rsref2)
                .HasColumnName("RSREF2")
                .HasMaxLength(50);

            builder.Property(e => e.Rssecowner)
                .HasColumnName("RSSECOWNER")
                .HasMaxLength(50);

            builder.Property(e => e.Rssoft).HasColumnName("RSSoft");

            builder.Property(e => e.RssoftValue).HasColumnName("RSSoftValue");

            builder.Property(e => e.Rsstate)
                .HasColumnName("RSSTATE")
                .HasMaxLength(50);

            builder.Property(e => e.Rstelephone)
                .HasColumnName("RSTELEPHONE")
                .HasMaxLength(50);

            builder.Property(e => e.Rszip)
                .HasColumnName("RSZIP")
                .HasMaxLength(50);

            builder.Property(e => e.Rszip4)
                .HasColumnName("RSZIP4")
                .HasMaxLength(50);

            builder.Property(e => e.SaleDate).HasColumnType("datetime");

            builder.Property(e => e.SaleId).HasColumnName("SaleID");

            builder.Property(e => e.SecondPhone).HasMaxLength(255);

            builder.Property(e => e.SecondPhoneDescription).HasMaxLength(50);

            builder.Property(e => e.Taxacquired).HasColumnName("taxacquired");
        }
    }
}