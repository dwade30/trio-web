﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class PreviousOwnerConfiguration : IEntityTypeConfiguration<PreviousOwner>
    {
        public void Configure(EntityTypeBuilder<PreviousOwner> builder)
        {
            builder.ToTable("PreviousOwner");

            builder.HasIndex(e => e.PartyId1);

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Address1).HasMaxLength(255);

            builder.Property(e => e.Address2).HasMaxLength(255);

            builder.Property(e => e.CardId).HasColumnName("CardID");

            builder.Property(e => e.City).HasMaxLength(255);

            builder.Property(e => e.DateCreated).HasColumnType("datetime");

            builder.Property(e => e.Name).HasMaxLength(255);

            builder.Property(e => e.PartyId1).HasColumnName("PartyID1");

            builder.Property(e => e.PartyId2).HasColumnName("PartyID2");

            builder.Property(e => e.SaleDate).HasColumnType("datetime");

            builder.Property(e => e.SecOwner).HasMaxLength(255);

            builder.Property(e => e.State).HasMaxLength(255);

            builder.Property(e => e.Zip).HasMaxLength(255);

            builder.Property(e => e.Zip4).HasMaxLength(255);
        }
    }
}