﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
    public class TranCodeConfiguration : IEntityTypeConfiguration<TranCode>
    {
        public void Configure(EntityTypeBuilder<TranCode> builder)
        {
            builder.ToTable("tblTranCode");
            builder.Property(p => p.Description).HasMaxLength(50);
            builder.Property(p => p.ShortDescription).HasMaxLength(50);
                        
        }
    }
}
