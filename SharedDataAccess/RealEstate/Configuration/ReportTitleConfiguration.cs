﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Payroll.Models;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate.Configuration
{
	public class ReportTitleConfiguration : IEntityTypeConfiguration<ReportTitle>
	{
		public void Configure(EntityTypeBuilder<ReportTitle> builder)
		{
			builder.ToTable("ReportTitles");
			builder.Property(p => p.Description).HasMaxLength(255);
			builder.Property(p => p.Title).HasMaxLength(50);
			builder.Property(p => p.Id).HasColumnName("ID");
			builder.Property(p => p.UserId).HasColumnName("UserID");
		}
	}
}
