﻿using System;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedApplication.RealEstate.MVR;

namespace SharedDataAccess.RealEstate
{
    public class SaveMunicipalValuationReturnHandler : CommandHandler<SaveMunicipalValuationReturn>
    {
        private RealEstateContext realEstateContext;
        public SaveMunicipalValuationReturnHandler(IRealEstateContext realEstateContext)
        {
            this.realEstateContext = (RealEstateContext)realEstateContext;
        }
        protected override void Handle(SaveMunicipalValuationReturn command)
        {            
            var mvrValues = realEstateContext.MvrReports.Where(m => m.TownNumber == command.MunicipalValuationReturn.TownNumber).ToList();
            var updatedValues = command.MunicipalValuationReturn.ToMvrreports();
            foreach (var mvrItem in updatedValues)
            {
                var savedItem = mvrValues.FirstOrDefault(m => m.Code == mvrItem.Code);
                if (savedItem == null)
                {
                   
                    savedItem = new Mvrreport(){Code = mvrItem.Code,TownNumber =  command.MunicipalValuationReturn.TownNumber, DataValue = mvrItem.DataValue};
                    realEstateContext.MvrReports.Add(savedItem);
                }
                else
                {
                    savedItem.DataValue = mvrItem.DataValue;
                }
            }
            realEstateContext.SaveChanges();
        }
    }
}