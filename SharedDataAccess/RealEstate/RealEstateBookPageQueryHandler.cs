﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate
{
    public class RealEstateBookPageQueryHandler : IQueryHandler<RealEstateBookPageQuery,IEnumerable<BookPage>>
    {
        private IRealEstateContext reContext;
        public RealEstateBookPageQueryHandler(IRealEstateContext reContext)
        {
            this.reContext = reContext;
        }
        public IEnumerable<BookPage> ExecuteQuery(RealEstateBookPageQuery query)
        {
            var bpQuery = reContext.BookPages.Where(b => b.Account == query.Account);
            if (query.OnlyCurrent)
            {
                bpQuery = bpQuery.Where(b => b.Current == true);
            }

            return bpQuery.OrderByDescending(b => b.Line.GetValueOrDefault()).ToList();
        }
    }
}