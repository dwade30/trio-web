﻿using System.Linq;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace SharedDataAccess.RealEstate
{
    public class RealEstateAccountTotalsQueryHandler : IQueryHandler<RealEstateAccountTotalsQuery,RealEstateAccountTotals>
    {
        private IRealEstateContext reContext;

        public RealEstateAccountTotalsQueryHandler(IRealEstateContext reContext)
        {
            this.reContext = reContext;
        }
        public RealEstateAccountTotals ExecuteQuery(RealEstateAccountTotalsQuery query)
        {
            var cards = reContext.REMasters.Where(m => m.Rsaccount == query.Account).OrderBy(m => m.Rscard).ToList();
            if (!cards.Any())
            {
                return new RealEstateAccountTotals();
            }
            var totals = new RealEstateAccountTotals();
            totals.Account = query.Account;
            totals.Acreage = cards.Sum(c => c.Piacres.GetValueOrDefault().ToDecimal());
            totals.Land = cards.Sum(c => c.LastLandVal.GetValueOrDefault());
            totals.Building = cards.Sum(c => c.LastBldgVal.GetValueOrDefault());
            totals.Exemptions = cards.Sum(c => c.Rlexemption.GetValueOrDefault());
            return totals;
        }
    }
}