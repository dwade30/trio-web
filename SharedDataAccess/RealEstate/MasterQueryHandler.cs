﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;

namespace SharedDataAccess.RealEstate
{
	public class MasterQueryHandler : IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>>
	{
		private IRealEstateContext realEstateContext;

		public MasterQueryHandler(ITrioContextFactory contextFactory)
		{
			this.realEstateContext = contextFactory.GetRealEstateContext();
		}

		public IEnumerable<REMaster> ExecuteQuery(REMasterSearchCriteria query)
		{
			IQueryable<REMaster> masterQuery;

			masterQuery = realEstateContext.REMasters;

			if (query.Id > 0)
			{
				masterQuery = masterQuery.Where(x => x.Id == query.Id);
			}

			if (query.AccountNumber > 0)
			{
				masterQuery = masterQuery.Where(x => x.Rsaccount == query.AccountNumber);
			}

			if (query.deletedOption == REMasterSearchCriteria.DeletedSelection.NotDeleted)
			{
				masterQuery = masterQuery.Where(x => (x.Rsdeleted ?? false) == false);
			}
			else if (query.deletedOption == REMasterSearchCriteria.DeletedSelection.OnlyDeleted)
			{
				masterQuery = masterQuery.Where(x => (x.Rsdeleted ?? false));
			}

			return masterQuery.ToList();
		}
	}
}
