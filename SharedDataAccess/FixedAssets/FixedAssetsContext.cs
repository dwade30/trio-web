﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.FixedAssets.Models;
using SharedDataAccess.FixedAssets.Configuration;

namespace SharedDataAccess.FixedAssets
{
    public class FixedAssetsContext : DataContext<FixedAssetsContext>
    {
        public DbSet<Condition> Condition { get; set; }
        public DbSet<Cya> Cya { get; set; }
        public DbSet<Dbversion> Dbversion { get; set; }
        public DbSet<DefaultCounty> DefaultCounties { get; set; }
        public DbSet<DefaultTown> DefaultTowns { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<TblClassCodes> TblClassCodes { get; set; }
        public DbSet<TblCustomReports> TblCustomReports { get; set; }
        public DbSet<TblDepreciation> TblDepreciation { get; set; }
        public DbSet<TblDepreciationMaster> TblDepreciationMaster { get; set; }
        public DbSet<TblDeptDiv> TblDeptDiv { get; set; }
        public DbSet<TblLocations> TblLocations { get; set; }
        public DbSet<TblMaster> TblMaster { get; set; }
        public DbSet<TblReportLayout> TblReportLayout { get; set; }
        public DbSet<TblSettings> TblSettings { get; set; }
        public DbSet<TblVendors> TblVendors { get; set; }
        public FixedAssetsContext(DbContextOptions<FixedAssetsContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new ConditionConfiguration());
			modelBuilder.ApplyConfiguration(new CyaConfiguration());
			modelBuilder.ApplyConfiguration(new DbVersionConfiguration());
			modelBuilder.ApplyConfiguration(new DefaultCountyConfiguration());
			modelBuilder.ApplyConfiguration(new DefaultTownConfiguration());
			modelBuilder.ApplyConfiguration(new StateConfiguration());

			modelBuilder.Entity<TblClassCodes>(entity =>
            {
                entity.ToTable("tblClassCodes");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Life).HasMaxLength(50);
            });

            modelBuilder.Entity<TblCustomReports>(entity =>
            {
                entity.ToTable("tblCustomReports");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LastUpdated).HasColumnType("datetime");

                entity.Property(e => e.Orientation).HasMaxLength(255);

                entity.Property(e => e.ReportName).HasMaxLength(255);

                entity.Property(e => e.Sql).HasColumnName("SQL");

                entity.Property(e => e.Title).HasMaxLength(50);

                entity.Property(e => e.Type).HasMaxLength(255);
            });

            modelBuilder.Entity<TblDepreciation>(entity =>
            {
                entity.ToTable("tblDepreciation");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChangedToSl)
                    .HasColumnName("ChangedToSL")
                    .HasMaxLength(255);

                entity.Property(e => e.DepreciationAmount).HasColumnType("money");

                entity.Property(e => e.DepreciationDate).HasColumnType("datetime");

                entity.Property(e => e.DepreciationDesc).HasMaxLength(250);

                entity.Property(e => e.DepreciationUnits).HasMaxLength(250);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.OldDepreciationDate).HasColumnType("datetime");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");
            });

            modelBuilder.Entity<TblDepreciationMaster>(entity =>
            {
                entity.ToTable("tblDepreciationMaster");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepreciationType).HasMaxLength(255);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblDeptDiv>(entity =>
            {
                entity.ToTable("tblDeptDiv");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Dept).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Div).HasMaxLength(50);
            });

            modelBuilder.Entity<TblLocations>(entity =>
            {
                entity.ToTable("tblLocations");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<TblMaster>(entity =>
            {
                entity.ToTable("tblMaster");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Acct1).HasMaxLength(50);

                entity.Property(e => e.Acct2).HasMaxLength(50);

                entity.Property(e => e.Acct3).HasMaxLength(50);

                entity.Property(e => e.Acct4).HasMaxLength(50);

                entity.Property(e => e.BasisCost).HasColumnType("money");

                entity.Property(e => e.BasisReduction).HasColumnType("money");

                entity.Property(e => e.ChangedToSl)
                    .HasColumnName("ChangedToSL")
                    .HasMaxLength(255);

                entity.Property(e => e.ClassCode).HasMaxLength(50);

                entity.Property(e => e.Condition).HasMaxLength(255);

                entity.Property(e => e.DateLastDepreciation).HasColumnType("datetime");

                entity.Property(e => e.Dbrate).HasColumnName("DBRate");

                entity.Property(e => e.Dept).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.DiscardedDate).HasColumnType("datetime");

                entity.Property(e => e.Div).HasMaxLength(50);

                entity.Property(e => e.LifeUnits).HasMaxLength(50);

                entity.Property(e => e.Location1Desc).HasMaxLength(50);

                entity.Property(e => e.Location2Desc).HasMaxLength(50);

                entity.Property(e => e.Location3Desc).HasMaxLength(50);

                entity.Property(e => e.Location4Desc).HasMaxLength(50);

                entity.Property(e => e.Make).HasMaxLength(50);

                entity.Property(e => e.ManualClassCode).HasMaxLength(50);

                entity.Property(e => e.ManualVendor).HasMaxLength(200);

                entity.Property(e => e.Model).HasMaxLength(50);

                entity.Property(e => e.OldDepreciationDate).HasColumnType("datetime");

                entity.Property(e => e.OutOfServiceDate).HasColumnType("datetime");

                entity.Property(e => e.PurchaseDate).HasColumnType("datetime");

                entity.Property(e => e.SalvageValue).HasColumnType("money");

                entity.Property(e => e.SerialNumber).HasMaxLength(50);

                entity.Property(e => e.TagNumber).HasMaxLength(50);

                entity.Property(e => e.TotalDepreciated).HasColumnType("money");

                entity.Property(e => e.TotalPosted).HasMaxLength(50);

                entity.Property(e => e.Unrate)
                    .HasColumnName("UNRate")
                    .HasMaxLength(50);

                entity.Property(e => e.VendorCode).HasMaxLength(50);

                entity.Property(e => e.WarrantyDescription).HasMaxLength(50);

                entity.Property(e => e.WarrantyExpDate).HasColumnType("datetime");

                entity.Property(e => e.Year).HasMaxLength(50);
            });

            modelBuilder.Entity<TblReportLayout>(entity =>
            {
                entity.ToTable("tblReportLayout");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ColumnId).HasColumnName("ColumnID");

                entity.Property(e => e.DisplayText).HasMaxLength(255);

                entity.Property(e => e.FieldId).HasColumnName("FieldID");

                entity.Property(e => e.LastUpdated).HasColumnType("datetime");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.RowId).HasColumnName("RowID");
            });

            modelBuilder.Entity<TblSettings>(entity =>
            {
                entity.ToTable("tblSettings");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Location1Caption).HasMaxLength(50);

                entity.Property(e => e.Location2Caption).HasMaxLength(50);

                entity.Property(e => e.Location3Caption).HasMaxLength(50);

                entity.Property(e => e.Location4Caption).HasMaxLength(50);

                entity.Property(e => e.Version).HasMaxLength(50);
            });

            modelBuilder.Entity<TblVendors>(entity =>
            {
                entity.ToTable("tblVendors");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description).HasMaxLength(50);
            });
        }


    }
}