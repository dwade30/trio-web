﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.FixedAssets.Models;

namespace SharedDataAccess.FixedAssets.Configuration
{
	public class DefaultTownConfiguration : IEntityTypeConfiguration<DefaultTown>
	{
		public void Configure(EntityTypeBuilder<DefaultTown> builder)
		{
			builder.ToTable("DefaultTowns");

			builder.Property(e => e.Id).HasColumnName("ID");

			builder.Property(e => e.Name).HasMaxLength(200);
		}
	}
}
