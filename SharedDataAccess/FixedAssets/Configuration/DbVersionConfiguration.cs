﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.FixedAssets.Models;

namespace SharedDataAccess.FixedAssets.Configuration
{
	public class DbVersionConfiguration : IEntityTypeConfiguration<Dbversion>
	{
		public void Configure(EntityTypeBuilder<Dbversion> builder)
		{
			builder.ToTable("DBVersion");

			builder.Property(e => e.Id).HasColumnName("ID");
		}
	}
}
