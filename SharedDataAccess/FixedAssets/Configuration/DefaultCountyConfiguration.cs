﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.FixedAssets.Models;

namespace SharedDataAccess.FixedAssets.Configuration
{
	public class DefaultCountyConfiguration : IEntityTypeConfiguration<DefaultCounty>
	{
		public void Configure(EntityTypeBuilder<DefaultCounty> builder)
		{
			builder.ToTable("DefaultCounties");

			builder.Property(e => e.Id).HasColumnName("ID");

			builder.Property(e => e.Name).HasMaxLength(200);
		}
	}
}
