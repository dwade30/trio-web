﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.FixedAssets.Models;

namespace SharedDataAccess.FixedAssets.Configuration
{
	public class StateConfiguration : IEntityTypeConfiguration<State>
	{
		public void Configure(EntityTypeBuilder<State> builder)
		{
			builder.ToTable("States");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.ConcurrencyId).HasColumnName("ConcurrencyID");
			builder.Property(e => e.Description).HasMaxLength(40);
			builder.Property(e => e.LastUpdate).HasColumnType("datetime");
			builder.Property(e => e.LastUserId).HasMaxLength(10);
			builder.Property(e => e.StateAbbreviation).HasMaxLength(2);
			builder.Property(e => e.StateAbbreviation).HasColumnName("State");
		}
	}
}
