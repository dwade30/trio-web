﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.FixedAssets.Models;

namespace SharedDataAccess.FixedAssets.Configuration
{
	public class ConditionConfiguration : IEntityTypeConfiguration<Condition>
	{
		public void Configure(EntityTypeBuilder<Condition> builder)
		{
			builder.Property(e => e.Id).HasColumnName("ID");

			builder.Property(e => e.Condition1)
				.HasColumnName("Condition")
				.HasMaxLength(255);
		}
	}
}
