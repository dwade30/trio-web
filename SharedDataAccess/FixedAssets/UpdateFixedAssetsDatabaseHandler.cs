﻿using System.Data;
using SharedApplication.FixedAssets;
using SharedApplication.Messaging;

namespace SharedDataAccess.FixedAssets
{
    public class UpdateFixedAssetsDatabaseHandler : CommandHandler<UpdateFixedAssetsDatabase,bool>
    {
        protected override bool Handle(UpdateFixedAssetsDatabase command)
        {
           var updater = new cFixedAssetsDB(command.SqlConfigInfo,command.DataEnvironment);
           return updater.CheckVersion();
        }
    }
}