﻿using System;
using SharedApplication.Models;
using SharedApplication.Versioning;

namespace SharedDataAccess.FixedAssets
{
    public class cFixedAssetsDB : cDatabaseVersionUpdater
    {
        public cFixedAssetsDB(SQLConfigInfo sqlConfigInfo,string dataEnvironment)
        {
            defaultDatabaseName = "FixedAssets";
            dbInfo = sqlConfigInfo;
            DataEnvironment = dataEnvironment;
        }

       

        protected override bool PerformChecks(cVersionInfo currentVersion)
        {
            //Example:
           // CheckFunction(TestFunction,currentVersion,new cVersionInfo(){ Major = 6, Minor = 0, Revision =  1, Build = 0});
            return true;
        }

        

    }
}
