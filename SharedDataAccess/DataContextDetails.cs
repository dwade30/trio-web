﻿namespace SharedDataAccess
{
    public class DataContextDetails
    {
        public string DataSource { get; set; } = "";
        public string UserID { get; set; } = "";
        public string Password { get; set; } = "";
        public string DataEnvironment { get; set; } = "";
        public string EnvironmentGroup { get; set; } = "";
    }
}
