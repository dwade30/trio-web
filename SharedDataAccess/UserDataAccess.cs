﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.ClientSettings.Models;
using SharedDataAccess.ClientSettings;
using PasswordHistory = SharedApplication.ClientSettings.Models.PasswordHistory;

namespace SharedDataAccess
{
	public class UserDataAccess : IUserDataAccess
	{
		protected ClientSettingsContext clientSettingsContext;
		private cGlobalSettings settings;
        private IRepository<User> repo;

		public UserDataAccess(ClientSettingsContext passedContext, cGlobalSettings settings)
		{
			clientSettingsContext = passedContext;
            repo = new Repository<User>(passedContext);
            this.settings = settings;
		}

		public bool DuplicateUserIdExists(string strUserId)
		{
			return GetUserByUserId(strUserId) != null;
		}

		public User GetUserByID(int lngUserID)
		{
			if (lngUserID != -1)
            {
                var user = repo.Get(lngUserID);
				//var user = systemSettingsContext.Securities.FirstOrDefault(x => x.ID == lngUserID);
				return user;
			}
			else
			{
				var user = new User();
				user.Id = -1;
				user.UserID = "SuperUser";
				user.UserName = "SuperUser";
				return user;
			}
		}

		public User GetUserByUserId(string strUserId)
		{
			try
            {
                var user = repo.Find(x => x.UserID == strUserId && x.ClientIdentifier == settings.ClientIdentifier).FirstOrDefault();
				//var user = systemSettingsContext.Securities.FirstOrDefault(x => x.UserID == strUserId);
				return user;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				return null;
			}
		}

		public IEnumerable<User> GetUsers()
		{
			try
			{
				// On Error GoTo ErrorHandler
				var users = clientSettingsContext.Users.Where(y => y.ClientIdentifier == settings.ClientIdentifier).OrderBy(x => x.UserName).ToList();
				return users;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				return new List<User>();
			}
		}

		public IEnumerable<PasswordHistory> GetPreviousPasswords(int userId)
		{
			try
			{
				var history = clientSettingsContext.PasswordHistories.Where(x => x.UserId == userId).ToList();
				return history;
			}
			catch (Exception e)
			{
				return new List<PasswordHistory>();
			}
		}

		public bool UpdatePassword(User user, string newPassword, string salt)
		{
			try
			{
				clientSettingsContext.PasswordHistories.Add(new PasswordHistory()
				{
					DateAdded = DateTime.Now,
					Password = user.Password,
					Salt = user.Salt,
					UserId = user.Id
				});

				user.Password = newPassword;
				user.Salt = salt;
				user.DateChanged = DateTime.Now;

				SaveChanges();
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		public bool SaveUser(User user)
		{
			try
			{
				if (user.Id == 0)
				{
					clientSettingsContext.Users.Add(user);
				}

				SaveChanges();
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		public bool SaveChanges()
		{
			try
			{
				clientSettingsContext.SaveChanges();
				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}
