﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.Authorization;
using SharedApplication.SystemSettings.Interfaces;
using SharedApplication.SystemSettings.Models;
using SharedDataAccess.SystemSettings;

namespace SharedDataAccess.Authorization
{
    public class GetUserPermissionsQueryHandler : IQueryHandler<GetUserPermissions,IEnumerable<PermissionItem>>
    {
        private ISystemSettingsContext systemContext;
        public GetUserPermissionsQueryHandler(ITrioContextFactory contextFactory)
        {
            systemContext = contextFactory.GetSystemSettingsContext();
        }
        public IEnumerable<PermissionItem> ExecuteQuery(GetUserPermissions query)
        {
            return systemContext.UserPermissions.Where(p => p.UserID == query.UserId).ToList().ToPermissionItems();
        }

        
    }

    internal static class UserPermissionExtensions
    {
        internal static IEnumerable<PermissionItem> ToPermissionItems(this IEnumerable<UserPermission> permissions)
        {
            if (permissions != null)
            {
                return permissions.Select(p => new PermissionItem()
                    {PermissionValue = p.Permission, FunctionId = p.FunctionID, Category = p.ModuleName}).ToList();
            }
            return new List<PermissionItem>();
        }
    }
}