﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralDocuments;

namespace SharedDataAccess.CentralDocuments.Configuration
{
    public class CentralDocumentConfiguration : IEntityTypeConfiguration<CentralDocument>
    {
        public void Configure(EntityTypeBuilder<CentralDocument> builder)
        {
            builder.ToTable("Documents");

            builder.Property(e => e.AltReference).HasMaxLength(50);

            builder.Property(e => e.DateCreated)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.DocumentIdentifier).HasDefaultValueSql("(newid())");

            builder.Property(e => e.ItemDescription).HasMaxLength(255);

            builder.Property(e => e.ItemName).HasMaxLength(255);

            builder.Property(e => e.MediaType).HasMaxLength(50);

            builder.Property(e => e.DataGroup).HasColumnName("ReferenceGroup").HasMaxLength(50);
            

            builder.Property(e => e.ReferenceType).HasMaxLength(50);
            builder.Ignore(e => e.IsDeleted);
            builder.Ignore(e => e.IsUpdated);
            builder.Ignore(e => e.ItemLength);
        }
    }
}