﻿using Autofac;
using Microsoft.EntityFrameworkCore.Internal;
using SharedApplication.CentralDocuments;

namespace SharedDataAccess.CentralDocuments.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<CentralDocumentsContext>().As<ICentralDocumentContext>().AsSelf();
            RegisterCentralDocumentsContext(ref builder);
        }

        private void RegisterCentralDocumentsContext(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return tcf.GetCentralDocumentsContext();

            }).As<ICentralDocumentContext>().As<CentralDocumentsContext>();
        }
    }
}