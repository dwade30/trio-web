﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralDocuments.Models;

namespace SharedDataAccess.CentralDocuments.Configuration
{
    public class SketchConfiguration : IEntityTypeConfiguration<Sketch>
    {
        public void Configure(EntityTypeBuilder<Sketch> builder)
        {
            builder.ToTable("Sketches");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Calculations).HasMaxLength(1024);

            builder.Property(e => e.DateUpdated)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.MediaType).HasMaxLength(50);
        }
    }
}