﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Models;
using SharedDataAccess.CentralDocuments.Configuration;

namespace SharedDataAccess.CentralDocuments
{
    public class CentralDocumentsContext :DbContext, ICentralDocumentContext
    {
        public DbSet<Sketch> Sketches { get; set; }

        public DbSet<CentralDocument> Documents { get; set; }
        IQueryable<Sketch> ICentralDocumentContext.Sketches{get => Sketches;}
        IQueryable<CentralDocument> ICentralDocumentContext.Documents
        {
            get => Documents;
        }
        IQueryable<SketchInfo> ICentralDocumentContext.SketchInfos
        {
            get => Sketches.Select(s => new SketchInfo()
            {
                 Id = s.Id,  SequenceNumber = s.SequenceNumber,
                MediaType = s.MediaType, Calculations = s.Calculations, DateUpdated = s.DateUpdated,
                SketchIdentifier = s.SketchIdentifier, CardIdentifier = s.CardIdentifier
            });
        }
        public CentralDocumentsContext(DbContextOptions<CentralDocumentsContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new SketchConfiguration());
            modelBuilder.ApplyConfiguration(new CentralDocumentConfiguration());
        }
    }
}