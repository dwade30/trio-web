﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.EntityFrameworkCore;
using SharedApplication.CentralDocuments;
using Microsoft.EntityFrameworkCore.SqlServer;

namespace SharedDataAccess.CentralDocuments
{
    public class CentralDocumentUtility
    {
        private CentralDocumentsContext centralDocumentsContext;

        public CentralDocumentUtility(CentralDocumentsContext centralDocumentsContext)
        {
            this.centralDocumentsContext = centralDocumentsContext;
        }

        public int CreateCentralDocument(CreateCentralDocument command)
        {

                using (var dbCommand = centralDocumentsContext.Database.GetDbConnection().CreateCommand())
                {
                    try
                    {
                        dbCommand.CommandType = CommandType.StoredProcedure;
                        dbCommand.CommandText = "Insert_Document";
                        var param = new SqlParameter();
                        param.ParameterName = "@ID";
                        param.Value = 0;
                        param.Direction = ParameterDirection.Output;
                        dbCommand.Parameters.Add(param);
                        dbCommand.Parameters.Add(new SqlParameter("@AltReference", command.AltReference));
                        dbCommand.Parameters.Add(new SqlParameter("@ItemDescription", command.Description));
                        dbCommand.Parameters.Add(new SqlParameter("@ItemName", command.ItemName));
                        dbCommand.Parameters.Add(new SqlParameter("@MediaType", command.MediaType));
                        dbCommand.Parameters.Add(new SqlParameter("@ReferenceId", command.ReferenceId));
                        dbCommand.Parameters.Add(new SqlParameter("@DataGroup", command.DataGroup));
                        dbCommand.Parameters.Add(new SqlParameter("@ReferenceType", command.ReferenceType));
                        dbCommand.Parameters.Add(new SqlParameter("@ItemData", command.ItemData));
                        dbCommand.Parameters.Add(new SqlParameter("@DocumentIdentifier", command.DocumentIdentifier));
                        dbCommand.Parameters.Add(new SqlParameter("@DateCreated", command.DateCreated));
                        dbCommand.Connection.Open();
                        dbCommand.ExecuteNonQuery();
                        var id =  Convert.ToInt32(dbCommand.Parameters["@ID"].Value);
                        dbCommand.Connection.Close();
                        return id;
                    }
                    catch (Exception ex)
                    {

                    }
                }

            return 0;
        }

        public string MediaTypeFromFileName(string fileName)
        {
            var strTemp = Path.GetExtension(fileName).TrimStart('.');
            var extension = strTemp.ToLower();

            switch (extension)
            {
                case "pdf":
                    return "application/pdf";
                case "jpg":
                case "jpeg":
                    return "image/jpeg";
                case "bmp":
                    return "image/bmp";
                case "png":
                    return "image/png";
                case "gif":
                    return "image/gif";
                case "tif":
                case "tiff":
                    return "image/tif";
                default:
                    return "";
            }
        }
    }
}