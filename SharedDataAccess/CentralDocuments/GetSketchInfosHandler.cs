﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;

namespace SharedDataAccess.CentralDocuments
{
    public class GetSketchInfosHandler : CommandHandler<GetSketchInfos,IEnumerable<SketchInfo>>
    {
        private ICentralDocumentContext docContext;
        public GetSketchInfosHandler(ICentralDocumentContext docContext)
        {
            this.docContext = docContext;
        }
        protected override IEnumerable<SketchInfo> Handle(GetSketchInfos command)
        {
            return docContext.SketchInfos.Where(s => s.CardIdentifier == command.CardIdentifier)
                .OrderBy(s => s.SequenceNumber).ToList();
        }
    }
}