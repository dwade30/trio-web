﻿using System.Linq;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralDocuments
{
    public class MoveSketchesHandler : CommandHandler<MoveSketches>
    {
        private CentralDocumentsContext docContext;

        public MoveSketchesHandler(CentralDocumentsContext docContext)
        {
            this.docContext = docContext;
        }
        protected override void Handle(MoveSketches command)
        {
            var sketches = docContext.Sketches.Where(s => s.CardIdentifier == command.SourceCardIdentifier);
            foreach (var sketch in sketches)
            {
                sketch.CardIdentifier = command.DestinationCardIdentifier;
            }

            docContext.SaveChanges();
        }
    }
}