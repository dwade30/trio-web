﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralDocuments
{
    public class DeleteSketchHandler : CommandHandler<DeleteSketch>
    {
        private CentralDocumentsContext docContext;

        public DeleteSketchHandler(CentralDocumentsContext docContext)
        {
            this.docContext = docContext;
        }
        protected override void Handle(DeleteSketch command)
        {
            docContext.Database.ExecuteSqlCommand("Delete from Sketches where id = " + command.Id);
        }
    }
}