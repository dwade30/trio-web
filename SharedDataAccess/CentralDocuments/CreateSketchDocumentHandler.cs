﻿using SharedApplication.CentralDocuments.Commands;
using SharedApplication.CentralDocuments.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralDocuments
{
    public class CreateSketchDocumentHandler : CommandHandler<CreateSketchDocument,int>
    {
        private CentralDocumentsContext docContext;

        public CreateSketchDocumentHandler(CentralDocumentsContext docContext)
        {
            this.docContext = docContext;
        }
        protected override int Handle(CreateSketchDocument command)
        {
            var sketch = new Sketch()
            {
                //Account =  command.Account,
                //Card =  command.Card,
                Calculations =  command.Calculations,
                DateUpdated = command.DateUpdated,
                MediaType = command.MediaType,
                SequenceNumber = command.SequenceNumber,
                SketchIdentifier = command.SketchIdentifier,
                SketchImage = command.SketchImage,
                SketchData = command.SketchData,
                CardIdentifier = command.CardIdentifier
            };
            docContext.Sketches.Add(sketch);
            docContext.SaveChanges();
            return sketch.Id;
        }
    }
}