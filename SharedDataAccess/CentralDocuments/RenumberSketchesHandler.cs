﻿using System;
using System.Linq;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralDocuments
{
    public class RenumberSketchesHandler : CommandHandler<RenumberSketches>
    {
        private CentralDocumentsContext docContext;

        public RenumberSketchesHandler(CentralDocumentsContext docContext)
        {
            this.docContext = docContext;
        }
        protected override void Handle(RenumberSketches command)
        {
            int sequence = 0;
            var sketches = docContext.Sketches.Where(s => s.CardIdentifier == command.CardId)
                .OrderBy(s => s.SequenceNumber.GetValueOrDefault());
            foreach (var sketch in docContext.Sketches)
            {
                sequence += 1;
                if (sketch.SequenceNumber.GetValueOrDefault() != sequence)
                {
                    sketch.SequenceNumber = sequence;
                }
            }

            docContext.SaveChanges();
        }
    }
}