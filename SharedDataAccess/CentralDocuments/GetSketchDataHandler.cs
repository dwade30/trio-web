﻿using System.Linq;
using SharedApplication;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralDocuments
{
    public class GetSketchDataHandler : CommandHandler<GetSketchData,byte[]>
    {
        private ICentralDocumentContext docContext;
        public GetSketchDataHandler(ICentralDocumentContext docContext)
        {
            this.docContext = docContext;
        }
        protected override byte[] Handle(GetSketchData command)
        {
            return docContext.Sketches.Where(s => s.Id == command.Id).Select(s => s.SketchData).FirstOrDefault();
        }
    }
}