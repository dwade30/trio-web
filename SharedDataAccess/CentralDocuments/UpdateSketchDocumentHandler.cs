﻿using System.Linq;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralDocuments
{
    public class UpdateSketchDocumentHandler : CommandHandler<UpdateSketchDocument>
    {
        private CentralDocumentsContext docContext;

        public UpdateSketchDocumentHandler(CentralDocumentsContext docContext)
        {
            this.docContext = docContext;
        }
        protected override void Handle(UpdateSketchDocument command)
        {
            var sketch = docContext.Sketches.FirstOrDefault(s => s.Id == command.SketchId);
            if (sketch != null)
            {
                //sketch.Account = command.Account;
                sketch.Calculations = command.Calculations;
                //sketch.Card = command.Card;
                sketch.CardIdentifier = command.CardIdentifier;
                sketch.DateUpdated = command.DateUpdated;
                sketch.MediaType = command.MediaType;
                sketch.SequenceNumber = command.SequenceNumber;
                sketch.SketchData = command.SketchData;
                sketch.SketchImage = command.SketchImage;
                docContext.SaveChanges();
            }
        }
    }
}