﻿using System.Linq;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralDocuments
{
    public class GetSketchImageHandler : CommandHandler<GetSketchImage,byte[]>
    {
        private ICentralDocumentContext docContext;
        public GetSketchImageHandler(ICentralDocumentContext docContext)
        {
            this.docContext = docContext;
        }
        protected override byte[] Handle(GetSketchImage command)
        {
            return docContext.Sketches.Where(s => s.Id == command.Id).Select(s => s.SketchImage).FirstOrDefault();

        }
    }
}