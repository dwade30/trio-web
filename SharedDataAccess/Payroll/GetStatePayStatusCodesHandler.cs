﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.Payroll;
using SharedApplication.Payroll.Interfaces;
using SharedApplication.Payroll.Models;

namespace SharedDataAccess.Payroll
{
    public class GetStatePayStatusCodesHandler : CommandHandler<GetStatePayStatusCodes,IEnumerable<TblPayStatus>>
    {
        private IPayrollContext pyContext;
        public GetStatePayStatusCodesHandler(IPayrollContext pyContext)
        {
            this.pyContext = pyContext;
        }
        protected override IEnumerable<TblPayStatus> Handle(GetStatePayStatusCodes command)
        {
            return pyContext.PayStatuses.Where(s => s.Type == "STATE").OrderBy(s => s.StatusCode).ToList();
        }
    }
}