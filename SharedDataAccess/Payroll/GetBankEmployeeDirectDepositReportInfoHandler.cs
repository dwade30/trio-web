﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Payroll.Commands;
using SharedApplication.Payroll.Models;

namespace SharedDataAccess.Payroll
{
	public class GetBankEmployeeDirectDepositReportInfoHandler : CommandHandler<GetBankEmployeeDirectDepositReportInfo, IEnumerable<BankEmployeeDirectDepositReportInfo>>
	{
		private PayrollContext payrollContext;
		public GetBankEmployeeDirectDepositReportInfoHandler(ITrioContextFactory contextFactory)
		{
			payrollContext = contextFactory.GetPayrollContext();
		}
		protected override IEnumerable<BankEmployeeDirectDepositReportInfo> Handle(GetBankEmployeeDirectDepositReportInfo command)
		{
			List<BankEmployeeDirectDepositReportInfo> employeeData = new List<BankEmployeeDirectDepositReportInfo>();

			if (command.UseTempTable)
			{
				employeeData = payrollContext.TempPayProcesses.Include(y => y.EmployeeMaster).Where(x => x.DdbankNumber == command.BankNumber && x.PayDate == command.PayDate && (x.BankRecord ?? false) && (command.PayRunId == 0 || x.PayRunId == command.PayRunId)).Select(z => new BankEmployeeDirectDepositReportInfo
				{
					EmployeeNumber = z.EmployeeNumber.ToIntegerValue(),
					EmployeeName = z.EmployeeName,
					DirectDepositTotal = (z.Ddamount ?? 0).ToDecimal(),
					DirectDepositAccountNumber = z.DdaccountNumber,
					LastName = z.EmployeeMaster.LastName,
					FirstName = z.EmployeeMaster.FirstName,
					MiddleName = z.EmployeeMaster.MiddleName,
					Designation = z.EmployeeMaster.Desig
				}).ToList();
			}
			else
			{
				employeeData = payrollContext.CheckDetails.Include(y => y.EmployeeMaster).Where(x => x.DdbankNumber == command.BankNumber && x.PayDate == command.PayDate && (x.BankRecord ?? false) && (command.PayRunId == 0 || x.PayRunId == command.PayRunId)).Select(z => new BankEmployeeDirectDepositReportInfo
				{
					EmployeeNumber = z.EmployeeNumber.ToIntegerValue(),
					EmployeeName = z.EmployeeName,
					DirectDepositTotal = (z.Ddamount ?? 0).ToDecimal(),
					DirectDepositAccountNumber = z.DdaccountNumber,
					LastName = z.EmployeeMaster.LastName,
					FirstName = z.EmployeeMaster.FirstName,
					MiddleName = z.EmployeeMaster.MiddleName,
					Designation = z.EmployeeMaster.Desig
				}).ToList();
			}
			
			return employeeData.GroupBy(p => new {p.DirectDepositAccountNumber, p.EmployeeNumber, p.EmployeeName, p.LastName, p.FirstName, p.MiddleName, p.Designation })
				.Select(g => new BankEmployeeDirectDepositReportInfo
				{
					DirectDepositAccountNumber = g.Key.DirectDepositAccountNumber,
					EmployeeNumber = g.Key.EmployeeNumber,
					EmployeeName = g.Key.EmployeeName,
					FirstName = g.Key.FirstName,
					LastName = g.Key.LastName,
					MiddleName = g.Key.MiddleName,
					Designation = g.Key.Designation,
					DirectDepositTotal = g.Sum(x => x.DirectDepositTotal)
				}).OrderBy(x => x.LastName).ThenBy(y => y.FirstName).ThenBy(z => z.DirectDepositAccountNumber);
		}
	}
}
