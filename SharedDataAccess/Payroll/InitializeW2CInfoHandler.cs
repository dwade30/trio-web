﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.Payroll.Commands;
using SharedApplication.Payroll.Models;

namespace SharedDataAccess.Payroll
{
	public class InitializeW2CInfoHandler : CommandHandler<InitializeW2CInfo, int>
	{
		private PayrollContext payrollContext;
		private CommandDispatcher commandDispatcher;

		public InitializeW2CInfoHandler(ITrioContextFactory contextFactory, CommandDispatcher commandDispatcher)
		{
			payrollContext = contextFactory.GetPayrollContext();
			this.commandDispatcher = commandDispatcher;
		}
		protected override int Handle(InitializeW2CInfo command)
		{
			var W2Cs = payrollContext.W2Cs.Where(x => x.TaxYear == command.TaxYear).ToList();

			foreach (var W2C in W2Cs)
			{
				if (W2C.Ssn == command.SocialSecurityNumber)
				{
					return W2C.Id;
				}
			}

			var archives = payrollContext.W2Archives.Where(x => x.Year == command.TaxYear).ToList();
			foreach (var archive in archives)
			{
				if (archive.Ssn == command.SocialSecurityNumber)
				{
					string[] strAry = null;
					string address1 = "";
					string address2 = "";

					if (archive.Address != null)
					{
						strAry = archive.Address.Split(new string[] { "   " }, StringSplitOptions.None);
						if (strAry.Length > 1)
						{
							address1 = strAry[0].Trim();
							address2 = strAry[1].Trim();
						}
						else
						{
							address1 = strAry[0].Trim();
						}
					}

					var addedW2C = new W2c
					{
						TaxYear = command.TaxYear,
						DeptDiv = command.DeptDiv,
						SeqNumber = command.SequenceNumber,
						Ssn = command.SocialSecurityNumber,
						EmployeeNumber = command.EmployeeNumber,
						FirstName = archive.FirstName,
						LastName = archive.LastName,
						MiddleName = archive.MiddleName,
						Desig = archive.Desig,
						City = archive.City,
						State = archive.State,
						Zip = archive.Zip,
						Address1 = address1,
						Address2 = address2
					};

					return commandDispatcher.Send(new SaveW2CInfo
					{
						W2CInfo = addedW2C
					}).Result;
				}
			}

			var employeeMasters = payrollContext.EmployeeMasters.ToList();
			var strippedSocialSecurityNumber = command.SocialSecurityNumber.Replace("-", "");
			
			foreach (var employee in employeeMasters)
			{
				if (employee.Ssn == strippedSocialSecurityNumber)
				{
					var addedW2C = new W2c
					{
						TaxYear = command.TaxYear,
						DeptDiv = command.DeptDiv,
						SeqNumber = command.SequenceNumber,
						Ssn = command.SocialSecurityNumber,
						EmployeeNumber = command.EmployeeNumber,
						FirstName = employee.FirstName,
						LastName = employee.LastName,
						MiddleName = employee.MiddleName,
						Desig = employee.Desig,
						City = employee.City,
						State = employee.State,
						Zip = employee.Zip,
						Address1 = employee.Address1,
						Address2 = employee.Address2
					};

					return commandDispatcher.Send(new SaveW2CInfo
					{
						W2CInfo = addedW2C
					}).Result;
				}
			}

			return 0;
		}

		
	}
}
