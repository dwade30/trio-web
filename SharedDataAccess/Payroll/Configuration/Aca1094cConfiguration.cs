﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.Payroll.Models;
namespace Payroll
{
    public class Aca1094cConfiguration : IEntityTypeConfiguration<Aca1094c>
    {
        public void Configure(EntityTypeBuilder<Aca1094c> entity)
        {
            entity.ToTable("ACA1094C");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.ContactTelephone).HasMaxLength(50);

            entity.Property(e => e.DesignatedEntityEin)
                .HasColumnName("DesignatedEntityEIN")
                .HasMaxLength(50);

            entity.Property(e => e.DesignatedEntityName).HasMaxLength(255);

            entity.Property(e => e.EmployerAddress).HasMaxLength(255);

            entity.Property(e => e.EmployerCity).HasMaxLength(255);

            entity.Property(e => e.EmployerContact).HasMaxLength(255);

            entity.Property(e => e.EmployerContactLast).HasMaxLength(255);

            entity.Property(e => e.EmployerContactMiddle).HasMaxLength(255);

            entity.Property(e => e.EmployerContactSuffix).HasMaxLength(255);

            entity.Property(e => e.EmployerEin)
                .HasColumnName("EmployerEIN")
                .HasMaxLength(50);

            entity.Property(e => e.EmployerName).HasMaxLength(255);

            entity.Property(e => e.EmployerPostalCode).HasMaxLength(50);

            entity.Property(e => e.EmployerState).HasMaxLength(50);

            entity.Property(e => e.EntityCity).HasMaxLength(255);

            entity.Property(e => e.EntityContact).HasMaxLength(255);

            entity.Property(e => e.EntityContactTelephone).HasMaxLength(50);

            entity.Property(e => e.EntityPostalCode).HasMaxLength(50);

            entity.Property(e => e.EntityState).HasMaxLength(50);

            entity.Property(e => e.Entityaddress).HasMaxLength(255);

            entity.Property(e => e.Section4980Hrelief).HasColumnName("Section4980HRelief");
        }
    }
}
