﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.Payroll.Models;
namespace Payroll
{
    public class TblEmployeeDeductionHistoryConfiguration : IEntityTypeConfiguration<TblEmployeeDeductionHistory>
    {
        public void Configure(EntityTypeBuilder<TblEmployeeDeductionHistory> entity)
        {
            entity.ToTable("tblEmployeeDeductionHistory");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.FieldName).HasMaxLength(150);

            entity.Property(e => e.LastUpdate).HasColumnType("datetime");

            entity.Property(e => e.LastUserId)
                .HasColumnName("LastUserID")
                .HasMaxLength(25);

            entity.Property(e => e.NewValue).HasMaxLength(200);

            entity.Property(e => e.OldValue).HasMaxLength(200);
        }
    }
}
