﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Payroll.Models;

namespace SharedDataAccess.Payroll.Configuration
{
    public class CategoryTagConfiguration : IEntityTypeConfiguration<CategoryTag>
    {
        public void Configure(EntityTypeBuilder<CategoryTag> builder)
        {
            builder.ToTable("CategoryTags");
            builder.Property(p => p.Tag).HasMaxLength(255);
        }
    }
}