﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.Payroll.Models;
namespace Payroll
{
    public class AuditHistoryConfiguration : IEntityTypeConfiguration<AuditHistory>
    {
        public void Configure(EntityTypeBuilder<AuditHistory> entity)
        {
            entity.ToTable("AuditHistory");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.AuditHistoryDate).HasColumnType("datetime");

            entity.Property(e => e.AuditHistoryTime).HasColumnType("datetime");

            entity.Property(e => e.BoolUseSecurity).HasColumnName("boolUseSecurity");

            entity.Property(e => e.Description1).HasMaxLength(255);

            entity.Property(e => e.Description2).HasMaxLength(255);

            entity.Property(e => e.Description3).HasMaxLength(255);

            entity.Property(e => e.Description4).HasMaxLength(255);

            entity.Property(e => e.Description5).HasMaxLength(255);

            entity.Property(e => e.EmployeeNumber).HasMaxLength(255);

            entity.Property(e => e.UserId)
                .HasColumnName("UserID")
                .HasMaxLength(255);
        }
    }
}
