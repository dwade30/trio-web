﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.Payroll.Models;
namespace Payroll
{
    public class TimeClockCategoryConfiguration : IEntityTypeConfiguration<TimeClockCategory>
    {
        public void Configure(EntityTypeBuilder<TimeClockCategory> entity)
        {
            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.TimeCardCategory).HasMaxLength(255);
        }
    }
}
