﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.Payroll.Models;

namespace Payroll
{
    public class EmployeeMasterConfiguration : IEntityTypeConfiguration<EmployeeMaster>
    {
        public void Configure(EntityTypeBuilder<EmployeeMaster> entity)
        {
            entity.ToTable("tblEmployeeMaster");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Address1).HasMaxLength(50);

            entity.Property(e => e.Address2).HasMaxLength(50);

            entity.Property(e => e.BlsworksiteId)
                .HasColumnName("BLSWorksiteID")
                .HasMaxLength(255);

            entity.Property(e => e.BoolFedFirstCheckOnly).HasColumnName("boolFedFirstCheckOnly");

            entity.Property(e => e.BoolLocalFirstCheckOnly).HasColumnName("boolLocalFirstCheckOnly");

            entity.Property(e => e.BoolStateFirstCheckOnly).HasColumnName("boolStateFirstCheckOnly");

            entity.Property(e => e.Check).HasMaxLength(50);

            entity.Property(e => e.City).HasMaxLength(50);

            entity.Property(e => e.Code1).HasMaxLength(255);

            entity.Property(e => e.Code2).HasMaxLength(255);

            entity.Property(e => e.DateAnniversary).HasMaxLength(50);

            entity.Property(e => e.DateBirth).HasColumnType("datetime");

            entity.Property(e => e.DateHire).HasColumnType("datetime");

            entity.Property(e => e.DateRetired).HasColumnType("datetime");

            entity.Property(e => e.DateTerminated).HasColumnType("datetime");

            entity.Property(e => e.Dddollars).HasColumnName("DDDollars");

            entity.Property(e => e.Ddpercent).HasColumnName("DDPercent");

            entity.Property(e => e.Department).HasMaxLength(255);

            entity.Property(e => e.DeptDiv).HasMaxLength(200);

            entity.Property(e => e.DescriptionCode1).HasMaxLength(15);

            entity.Property(e => e.DescriptionCode2).HasMaxLength(15);

            entity.Property(e => e.Desig).HasMaxLength(50);

            entity.Property(e => e.Eiccode)
                .HasColumnName("EICCode")
                .HasMaxLength(50);

            entity.Property(e => e.Email).HasMaxLength(255);

            entity.Property(e => e.EmployeeNumber).HasMaxLength(50);

            entity.Property(e => e.FedDollarPercent).HasMaxLength(50);

            entity.Property(e => e.FedFilingStatusId).HasColumnName("FedFilingStatusID");

            entity.Property(e => e.FirstName).HasMaxLength(50);

            entity.Property(e => e.FtorPt).HasColumnName("FTorPT");

            entity.Property(e => e.GroupId)
                .HasColumnName("GroupID")
                .HasMaxLength(50);

            entity.Property(e => e.LastName).HasMaxLength(50);

            entity.Property(e => e.LastUpdate).HasColumnType("datetime");

            entity.Property(e => e.LastUserId)
                .HasColumnName("LastUserID")
                .HasMaxLength(25);

            entity.Property(e => e.LocalDollarPercent).HasMaxLength(50);

            entity.Property(e => e.LocalFilingStatusId).HasColumnName("LocalFilingStatusID");

            entity.Property(e => e.MiddleName).HasMaxLength(255);

            entity.Property(e => e.Mqge).HasColumnName("MQGE");

            entity.Property(e => e.Msrstype)
                .HasColumnName("MSRSType")
                .HasMaxLength(255);

            entity.Property(e => e.Phone).HasMaxLength(255);

            entity.Property(e => e.S1checkAsP1deductions).HasColumnName("S1CheckAsP1Deductions");

            entity.Property(e => e.S1checkAsP1directDeposits).HasColumnName("S1CheckAsP1DirectDeposits");

            entity.Property(e => e.S1checkAsP1matches).HasColumnName("S1CheckAsP1Matches");

            entity.Property(e => e.S1checkAsP1vacation).HasColumnName("S1CheckAsP1Vacation");

            entity.Property(e => e.ScheduleId).HasColumnName("ScheduleID");

            entity.Property(e => e.Sex).HasMaxLength(50);

            entity.Property(e => e.Ssn)
                .HasColumnName("SSN")
                .HasMaxLength(50);

            entity.Property(e => e.State).HasMaxLength(50);

            entity.Property(e => e.StateDollarPercent).HasMaxLength(50);

            entity.Property(e => e.StateFilingStatusId).HasColumnName("StateFilingStatusID");

            entity.Property(e => e.Status).HasMaxLength(50);

            entity.Property(e => e.W2defIncome).HasColumnName("W2DefIncome");

            entity.Property(e => e.W2pen).HasColumnName("W2Pen");

            entity.Property(e => e.W2statutoryEmployee).HasColumnName("W2StatutoryEmployee");

            entity.Property(e => e.W4date)
                .HasColumnName("W4Date")
                .HasColumnType("datetime");

            entity.Property(e => e.W4multipleJobs).HasColumnName("W4MultipleJobs");

            entity.Property(e => e.WorkCompCode).HasMaxLength(50);

            entity.Property(e => e.Zip).HasMaxLength(50);

            entity.Property(e => e.Zip4).HasMaxLength(255);
        }
    }
}
