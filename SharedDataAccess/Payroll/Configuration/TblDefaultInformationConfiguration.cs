﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.Payroll.Models;
namespace Payroll
{
    public class TblDefaultInformationConfiguration : IEntityTypeConfiguration<TblDefaultInformation>
    {
        public void Configure(EntityTypeBuilder<TblDefaultInformation> entity)
        {
            entity.ToTable("tblDefaultInformation");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Achbalanced).HasColumnName("ACHBalanced");

            entity.Property(e => e.AchclearingHouse).HasColumnName("ACHClearingHouse");

            entity.Property(e => e.AchemployerIdprefix).HasColumnName("ACHEmployerIDPrefix");

            entity.Property(e => e.BankDd).HasColumnName("BankDD");

            entity.Property(e => e.CheckLogo).HasMaxLength(255);

            entity.Property(e => e.CustomCheckFormat).HasMaxLength(50);

            entity.Property(e => e.Esphost)
                .HasColumnName("ESPHost")
                .HasMaxLength(255);

            entity.Property(e => e.Esporganization).HasColumnName("ESPOrganization");

            entity.Property(e => e.Esppassword)
                .HasColumnName("ESPPassword")
                .HasMaxLength(255);

            entity.Property(e => e.Espport).HasColumnName("ESPPort");

            entity.Property(e => e.EspserverCheckDir)
                .HasColumnName("ESPServerCheckDir")
                .HasMaxLength(255);

            entity.Property(e => e.EspserverDataDir)
                .HasColumnName("ESPServerDataDir")
                .HasMaxLength(255);

            entity.Property(e => e.EspserverExportDir)
                .HasColumnName("ESPServerExportDir")
                .HasMaxLength(255);

            entity.Property(e => e.EspserverW2dir)
                .HasColumnName("ESPServerW2Dir")
                .HasMaxLength(255);

            entity.Property(e => e.Espuser)
                .HasColumnName("ESPUser")
                .HasMaxLength(255);

            entity.Property(e => e.LastUpdate).HasColumnType("datetime");

            entity.Property(e => e.LastUserId)
                .HasColumnName("LastUserID")
                .HasMaxLength(25);

            entity.Property(e => e.Mmaunemployment).HasColumnName("MMAUnemployment");

            entity.Property(e => e.NoMsrs).HasColumnName("NoMSRS");

            entity.Property(e => e.Open1).HasMaxLength(255);

            entity.Property(e => e.Open2).HasMaxLength(255);

            entity.Property(e => e.PrintedDd).HasColumnName("PrintedDD");

            entity.Property(e => e.ReturnAddress1).HasMaxLength(255);

            entity.Property(e => e.ReturnAddress2).HasMaxLength(255);

            entity.Property(e => e.ReturnAddress3).HasMaxLength(255);

            entity.Property(e => e.ReturnAddress4).HasMaxLength(255);

            entity.Property(e => e.UseNewMsrs).HasColumnName("UseNewMSRS");

            entity.Property(e => e.Version)
                .HasMaxLength(50)
                .IsUnicode(false);

            entity.Property(e => e.W2checkType).HasColumnName("W2CheckType");

            entity.Property(e => e.W2masterSaveDone).HasColumnName("W2MasterSaveDone");

            entity.Property(e => e.WeeklyPr).HasColumnName("WeeklyPR");
        }
    }
}
