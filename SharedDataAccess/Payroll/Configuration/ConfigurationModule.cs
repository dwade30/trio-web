﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication.Payroll.Interfaces;

namespace SharedDataAccess.Payroll.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			RegisterPayrollContext(builder);
		}

		private void RegisterPayrollContext(ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var tcf = f.Resolve<ITrioContextFactory>();
				return tcf.GetPayrollContext();
			}).As<IPayrollContext>();
		}
	}
}
