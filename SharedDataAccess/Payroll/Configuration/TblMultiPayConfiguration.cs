﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.Payroll.Models;
namespace Payroll
{
    public class TblMultiPayConfiguration : IEntityTypeConfiguration<TblMultiPay>
    {
        public void Configure(EntityTypeBuilder<TblMultiPay> entity)
        {
            entity.ToTable("tblMultiPay");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.Caption).HasMaxLength(100);

            entity.Property(e => e.ChildEmployeeId).HasColumnName("ChildEmployeeID");

            entity.Property(e => e.ChildId)
                .HasColumnName("ChildID")
                .HasMaxLength(50);

            entity.Property(e => e.LastUpdate).HasColumnType("datetime");

            entity.Property(e => e.LastUserId)
                .HasColumnName("LastUserID")
                .HasMaxLength(25);

            entity.Property(e => e.ParentId)
                .HasColumnName("ParentID")
                .HasMaxLength(50);
        }
    }
}
