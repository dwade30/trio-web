﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.Payroll.Commands;
using SharedApplication.Payroll.Models;

namespace SharedDataAccess.Payroll
{
	public class SaveW2CInfoHandler : CommandHandler<SaveW2CInfo, int>
	{
		private PayrollContext payrollContext;
		public SaveW2CInfoHandler(ITrioContextFactory contextFactory)
		{
			payrollContext = contextFactory.GetPayrollContext();
		}
		protected override int Handle(SaveW2CInfo command)
		{
			var W2C = payrollContext.W2Cs.FirstOrDefault(x => x.Id == command.W2CInfo.Id);

			if (W2C != null)
			{
				W2C.Ssn = command.W2CInfo.Ssn;
				W2C.TaxYear = command.W2CInfo.TaxYear;
				W2C.Address1 = command.W2CInfo.Address1;
				W2C.Address2 = command.W2CInfo.Address2;
				W2C.AllocatedTips = command.W2CInfo.AllocatedTips;
				W2C.City = command.W2CInfo.City;
				W2C.CorrectingSsnorName = command.W2CInfo.CorrectingSsnorName;
				W2C.Current = command.W2CInfo.Current;
				W2C.DependentCare = command.W2CInfo.DependentCare;
				W2C.DeptDiv = command.W2CInfo.DeptDiv;
				W2C.Desig = command.W2CInfo.Desig;
				W2C.PrevDependentCare = command.W2CInfo.PrevDependentCare;
				W2C.PrevDesig = command.W2CInfo.PrevDesig;
				W2C.EmployeeNumber = command.W2CInfo.EmployeeNumber;
				W2C.Eicamount = command.W2CInfo.Eicamount;
				W2C.PrevEicamount = command.W2CInfo.PrevEicamount;
				W2C.PrevW2statutoryEmployee = command.W2CInfo.PrevW2statutoryEmployee;
				W2C.W2statutoryEmployee = command.W2CInfo.W2statutoryEmployee;
				W2C.FirstName = command.W2CInfo.FirstName;
				W2C.FederalTax = command.W2CInfo.FederalTax;
				W2C.FederalWage = command.W2CInfo.FederalWage;
				W2C.FicaTax = command.W2CInfo.FicaTax;
				W2C.FicaWage = command.W2CInfo.FicaWage;
				W2C.PrevFederalTax = command.W2CInfo.PrevFederalTax;
				W2C.PrevFederalWage = command.W2CInfo.PrevFederalWage;
				W2C.PrevFicaTax = command.W2CInfo.PrevFicaTax;
				W2C.PrevFicaWage = command.W2CInfo.PrevFicaWage;
				W2C.PrevFirstName = command.W2CInfo.PrevFirstName;
				W2C.PrevTotalGross = command.W2CInfo.PrevTotalGross;
				W2C.TotalGross = command.W2CInfo.TotalGross;
				W2C.PrevStateId = command.W2CInfo.PrevStateId;
				W2C.PrevStateId2 = command.W2CInfo.PrevStateId2;
				W2C.StateId = command.W2CInfo.StateId;
				W2C.StateId2 = command.W2CInfo.StateId2;
				W2C.LastName = command.W2CInfo.LastName;
				W2C.LocalTax = command.W2CInfo.LocalTax;
				W2C.LocalTax2 = command.W2CInfo.LocalTax2;
				W2C.LocalWage = command.W2CInfo.LocalWage;
				W2C.LocalWage2 = command.W2CInfo.LocalWage2;
				W2C.LocalityName = command.W2CInfo.LocalityName;
				W2C.LocalityName2 = command.W2CInfo.LocalityName2;
				W2C.PrevLastName = command.W2CInfo.PrevLastName;
				W2C.PrevLocalTax = command.W2CInfo.PrevLocalTax;
				W2C.PrevLocalTax2 = command.W2CInfo.PrevLocalTax2;
				W2C.PrevLocalWage = command.W2CInfo.PrevLocalWage;
				W2C.PrevLocalWage2 = command.W2CInfo.PrevLocalWage2;
				W2C.PrevLocalityName = command.W2CInfo.PrevLocalityName;
				W2C.PrevLocalityName2 = command.W2CInfo.PrevLocalityName2;
				W2C.MiddleName = command.W2CInfo.MiddleName;
				W2C.MedicareTax = command.W2CInfo.MedicareTax;
				W2C.MedicareWage = command.W2CInfo.MedicareWage;
				W2C.PrevMedicareTax = command.W2CInfo.PrevMedicareTax;
				W2C.PrevMedicareWage = command.W2CInfo.PrevMedicareWage;
				W2C.PrevMiddleName = command.W2CInfo.PrevMiddleName;
				W2C.NonQualifiedAmounts = command.W2CInfo.NonQualifiedAmounts;
				W2C.PrevNonQualifiedAmounts = command.W2CInfo.PrevNonQualifiedAmounts;
				W2C.SeqNumber = command.W2CInfo.SeqNumber;
				W2C.Sstips = command.W2CInfo.Sstips;
				W2C.State = command.W2CInfo.State;
				W2C.StateName = command.W2CInfo.StateName;
				W2C.StateName2 = command.W2CInfo.StateName2;
				W2C.StateTax = command.W2CInfo.StateTax;
				W2C.StateTax2 = command.W2CInfo.StateTax2;
				W2C.StateWage = command.W2CInfo.StateWage;
				W2C.StateWage2 = command.W2CInfo.StateWage2;
				W2C.PrevSsn = command.W2CInfo.PrevSsn;
				W2C.PrevSstips = command.W2CInfo.PrevSstips;
				W2C.PrevStateName = command.W2CInfo.PrevStateName;
				W2C.PrevStateName2 = command.W2CInfo.PrevStateName2;
				W2C.PrevStateTax = command.W2CInfo.PrevStateTax;
				W2C.PrevStateTax2 = command.W2CInfo.PrevStateTax2;
				W2C.PrevStateWage = command.W2CInfo.PrevStateWage;
				W2C.PrevStateWage2 = command.W2CInfo.PrevStateWage2;
				W2C.W2deferred = command.W2CInfo.W2deferred;
				W2C.W2retirement = command.W2CInfo.W2retirement;
				W2C.PrevW2deferred = command.W2CInfo.PrevW2deferred;
				W2C.PrevW2retirement = command.W2CInfo.PrevW2retirement;
                W2C.PrevFirstName = command.W2CInfo.PrevFirstName;
				payrollContext.SaveChanges();
				return W2C.Id;
			}
			else
			{
				payrollContext.W2Cs.Add(command.W2CInfo);
				payrollContext.SaveChanges();
				return command.W2CInfo.Id;
			}
		}
	}
}
