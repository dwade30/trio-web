﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.Payroll.Commands;
using SharedApplication.Payroll.Interfaces;
using SharedApplication.Payroll.Models;

namespace SharedDataAccess.Payroll
{
    public class GetCategoryTagsHandler : CommandHandler<GetCategoryTags,IEnumerable<CategoryTag>>
    {
        private IPayrollContext payrollContext;

        public GetCategoryTagsHandler(IPayrollContext payrollContext)
        {
            this.payrollContext = payrollContext;
        }
        protected override IEnumerable<CategoryTag> Handle(GetCategoryTags command)
        {
            if (command.PayCategory > 0)
            {
                return payrollContext.CategoryTags.Where(t => t.PayCategory == command.PayCategory).OrderBy(t => t.Tag).ToList();
            }
            else
            {
                return payrollContext.CategoryTags.OrderBy(t => t.PayCategory).ThenBy(t => t.Tag).ToList();
            }
        }
    }
}