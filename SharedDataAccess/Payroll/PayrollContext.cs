﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Payroll;
using SharedApplication.Payroll.Interfaces;
using SharedApplication.Payroll.Models;
using SharedApplication.Payroll.Models.Interfaces;
using SharedDataAccess.Payroll.Configuration;

namespace SharedDataAccess.Payroll
{

    public class PayrollContext : DataContext<PayrollContext>, IPayrollContext
    {
        public DbSet<PayrollPermission> PayrollPermissions { get; set; }
        public DbSet<EmployeeMaster> EmployeeMasters { get; set; }
        public DbSet<W2c> W2Cs { get; set; }
        public DbSet<W2archive> W2Archives { get; set; }
        public DbSet<TempPayProcess> TempPayProcesses { get; set; }
        public DbSet<CheckDetail> CheckDetails { get; set; }
        public DbSet<CategoryTag> CategoryTags { get; set; }
        public DbSet<TblPayStatus> PayStatuses { get; set; }

        IQueryable<PayrollPermission> IPayrollContext.PayrollPermissions => PayrollPermissions;
        IQueryable<EmployeeMaster> IPayrollContext.EmployeeMasters => EmployeeMasters;
        IQueryable<W2c> IPayrollContext.W2Cs => W2Cs;
        IQueryable<W2archive> IPayrollContext.W2Archives => W2Archives;
        IQueryable<TempPayProcess> IPayrollContext.TempPayProcesses => TempPayProcesses;
        IQueryable<CheckDetail> IPayrollContext.CheckDetails => CheckDetails;
        IQueryable<CategoryTag> IPayrollContext.CategoryTags => CategoryTags;
        IQueryable<TblPayStatus> IPayrollContext.PayStatuses => PayStatuses;

        public PayrollContext(DbContextOptions<PayrollContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new PayrollPermissionConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeMasterConfiguration());
            modelBuilder.ApplyConfiguration(new W2cConfiguration());
            modelBuilder.ApplyConfiguration(new W2archiveConfiguration());
            modelBuilder.ApplyConfiguration(new CheckDetailConfiguration());
            modelBuilder.ApplyConfiguration(new TempPayProcessConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryTagConfiguration());
            modelBuilder.ApplyConfiguration(new TblPayStatusConfiguration());

            modelBuilder.Entity<EmployeeMaster>()
	            .HasMany(c => c.CheckDetails)
	            .WithOne(e => e.EmployeeMaster)
	            .HasForeignKey(e => e.EmployeeNumber)
	            .HasPrincipalKey(k => k.EmployeeNumber);

            modelBuilder.Entity<EmployeeMaster>()
	            .HasMany(c => c.TempPayProcesses)
	            .WithOne(e => e.EmployeeMaster)
	            .HasForeignKey(e => e.EmployeeNumber)
	            .HasPrincipalKey(k => k.EmployeeNumber);
        }
    }
}
