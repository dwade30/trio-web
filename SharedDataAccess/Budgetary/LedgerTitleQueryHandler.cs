﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
	public class LedgerTitleQueryHandler : IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>
	{
		private IBudgetaryContext budgetaryContext;

		public LedgerTitleQueryHandler(ITrioContextFactory contextFactory)
		{
			this.budgetaryContext = contextFactory.GetBudgetaryContext();
		}

		public IEnumerable<LedgerTitle> ExecuteQuery(LedgerTitleSearchCriteria query)
		{
			IQueryable<LedgerTitle> ledgerTitleQuery;

			ledgerTitleQuery = budgetaryContext.LedgerTitles;

			if (query.Fund != "")
			{
				ledgerTitleQuery = ledgerTitleQuery.Where(x => x.Fund == query.Fund);
			}

			if (query.Account != "")
			{
				ledgerTitleQuery = ledgerTitleQuery.Where(x => x.Account == query.Account);
			}

			if (query.Suffix != "")
			{
				ledgerTitleQuery = ledgerTitleQuery.Where(x => x.Year == query.Suffix);
			}

			return ledgerTitleQuery.ToList();
		}
	}
}
