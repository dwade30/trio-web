﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace SharedDataAccess.Budgetary
{
	class BudgetaryJournalService : IBudgetaryJournalService
	{
		private CommandDispatcher commandDispatcher;
		private ITrioContextFactory contextFactory;

		public BudgetaryJournalService(CommandDispatcher commandDispatcher, ITrioContextFactory trioContextFactory)
		{
			this.commandDispatcher = commandDispatcher;
			this.contextFactory = trioContextFactory;
		}

		public int CreateGeneralJournal(int period, string description, IEnumerable<JournalEntryInfo> entries)
		{
			var journalNumber = commandDispatcher.Send(new CreateJournal
			{
				Period = period,
				Description = description,
				JournalType = JournalType.GeneralJournal
			}).Result;

			if (journalNumber > 0)
			{
				var budgetaryContext = contextFactory.GetBudgetaryContext();

				foreach (var entry in entries)
				{
					budgetaryContext.JournalEntries.Add(new JournalEntry
					{
						Type = "G",
						Account = entry.BudgetaryAccountNumber,
						Amount = entry.Amount,
						Description = entry.Description,
						Period = entry.Period,
						JournalEntriesDate = entry.Date,
						WarrantNumber = 0,
						Rcb = entry.Rcb,
						Status = entry.Status,
						JournalNumber = journalNumber
					});
				}

				budgetaryContext.SaveChanges();
			}

			return journalNumber;
		}
	}
}
