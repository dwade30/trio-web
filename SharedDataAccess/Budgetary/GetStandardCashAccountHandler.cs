﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.Budgetary
{
	public class GetStandardCashAccountHandler : CommandHandler<GetStandardCashAccount, string>
	{
		private IBudgetaryContext budgetaryContext;
		public GetStandardCashAccountHandler(ITrioContextFactory contextFactory)
		{
			this.budgetaryContext = contextFactory.GetBudgetaryContext();
		}

		protected override string Handle(GetStandardCashAccount command)
		{
			try
			{
				string codeToFind = "";
				switch (command.CashAccountType)
				{
					case CashAccountType.MiscellaneousCash:
						codeToFind = "CM";
						break;
					case CashAccountType.AccountsPayable:
						codeToFind = "CA";
						break;
					case CashAccountType.Payroll:
						codeToFind = "CP";
						break;
				}

				if (codeToFind == "")
				{
					return "";
				}
				else
				{
					var result = budgetaryContext.StandardAccounts.FirstOrDefault(x => x.Code == "CM");
					if (result != null)
					{
						return result.Account;
					}
					return "";
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}
