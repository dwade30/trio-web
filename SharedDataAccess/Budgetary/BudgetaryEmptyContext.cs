﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
    public class BudgetaryEmptyContext : IBudgetaryContext
    {
        public IQueryable<LedgerTitle> LedgerTitles { get => new List<LedgerTitle>().AsQueryable(); }
        public IQueryable<RevTitle> RevTitles { get => new List<RevTitle>().AsQueryable(); }
        public IQueryable<DeptDivTitle> DeptDivTitles { get => new List<DeptDivTitle>().AsQueryable(); }
        public IQueryable<ExpObjTitle> ExpObjTitles { get => new List<ExpObjTitle>().AsQueryable(); }
        public IQueryable<AccountMaster> AccountMasters { get => new List<AccountMaster>().AsQueryable(); }
        public IQueryable<LedgerRange> LedgerRanges { get => new List<LedgerRange>().AsQueryable(); }
        public IQueryable<BudgetarySetting> BudgetarySettings { get => new List<BudgetarySetting>().AsQueryable(); }
        public IQueryable<StandardAccount> StandardAccounts { get => new List<StandardAccount>().AsQueryable(); }
        public IQueryable<JournalLock> JournalLocks { get => new List<JournalLock>().AsQueryable(); }
        public IQueryable<JournalMaster> JournalMasters { get => new List<JournalMaster>().AsQueryable(); }
        public IQueryable<JournalEntry> JournalEntries { get => new List<JournalEntry>().AsQueryable(); }
        public IQueryable<TaxTitle> TaxTitles { get => new List<TaxTitle>().AsQueryable(); }
        public IQueryable<VendorTaxInfo> VendorTaxInfos { get => new List<VendorTaxInfo>().AsQueryable(); }
        public IQueryable<VendorTaxInfoArchive> VendorTaxInfoArchives { get => new List<VendorTaxInfoArchive>().AsQueryable(); }
        public IQueryable<APJournal> APJournals { get => new List<APJournal>().AsQueryable(); }
        public IQueryable<APJournalDetail> APJournalDetails { get => new List<APJournalDetail>().AsQueryable(); }
        public IQueryable<Adjustment> Adjustments { get => new List<Adjustment>().AsQueryable(); }
        public IQueryable<VendorMaster> VendorMasters { get => new List<VendorMaster>().AsQueryable(); }
        public IQueryable<APjournalArchive> APJournalArchives { get => new List<APjournalArchive>().AsQueryable(); }
        public IQueryable<APJournalDetailArchive> APJournalDetailArchives { get => new List<APJournalDetailArchive>().AsQueryable(); }
        public IQueryable<CdjournalArchive> CDJournalArchives { get => new List<CdjournalArchive>().AsQueryable(); }
    }
}