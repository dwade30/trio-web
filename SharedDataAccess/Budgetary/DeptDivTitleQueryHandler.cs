﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
	public class DeptDivTitleQueryHandler : IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>
	{
		private IBudgetaryContext budgetaryContext;

		public DeptDivTitleQueryHandler(ITrioContextFactory contextFactory)
		{
			this.budgetaryContext = contextFactory.GetBudgetaryContext();
		}

		public IEnumerable<DeptDivTitle> ExecuteQuery(DeptDivTitleSearchCriteria query)
		{
			IQueryable<DeptDivTitle> deptDivTitleQuery;

			deptDivTitleQuery = budgetaryContext.DeptDivTitles;

			if (query.Department != "")
			{
				deptDivTitleQuery = deptDivTitleQuery.Where(x => x.Department == query.Department);
			}

			if (query.Division != "")
			{
				deptDivTitleQuery = deptDivTitleQuery.Where(x => x.Division == query.Division);
			}

			return deptDivTitleQuery.ToList();
		}
	}
}
