﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
    public class VendorTaxInfoQueryHandler : IQueryHandler<VendorTaxInfoSearchCriteria, IEnumerable<VendorTaxInfo>>
    {
        private ITrioContextFactory contextFactory;

        public VendorTaxInfoQueryHandler(ITrioContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        public IEnumerable<VendorTaxInfo> ExecuteQuery(VendorTaxInfoSearchCriteria query)
        {
             var budgetaryContext = contextFactory.GetBudgetaryContext();

            IQueryable<VendorTaxInfo> vendorTaxInfoQuery;

            vendorTaxInfoQuery = budgetaryContext.VendorTaxInfos;

            if (query.VendorNumber != 0)
            {
                vendorTaxInfoQuery = vendorTaxInfoQuery.Where(x => x.VendorNumber == query.VendorNumber);
            }

            if (query.AttorneySelection != AttorneySearchOption.All)
            {
                switch (query.AttorneySelection)
                {
                    case AttorneySearchOption.AttorneyOnly:
                        vendorTaxInfoQuery = vendorTaxInfoQuery.Where(x => x.VendorMaster.Attorney ?? false);
                        break;
                    case AttorneySearchOption.NotAttorney:
                        vendorTaxInfoQuery = vendorTaxInfoQuery.Where(x => !x.VendorMaster.Attorney ?? false);
                        break;
                }
            }

            if (query.FormType != TaxFormType.All)
            {
                switch (query.FormType)
                {
                    case TaxFormType.MISC1099:
                        if (query.Category != 0)
                        {
                            vendorTaxInfoQuery = vendorTaxInfoQuery.Where(x => x.TaxTitle.FormName == "MISC" && x.TaxTitle.Category == query.Category);
                        }
                        else
                        {
                            vendorTaxInfoQuery = vendorTaxInfoQuery.Where(x => x.TaxTitle.FormName == "MISC");
                        }
                        break;
                    case TaxFormType.NEC1099:
                        if (query.Category != 0)
                        {
                            vendorTaxInfoQuery = vendorTaxInfoQuery.Where(x => x.TaxTitle.FormName == "NEC" && x.TaxTitle.Category == query.Category);
                        }
                        else
                        {
                            vendorTaxInfoQuery = vendorTaxInfoQuery.Where(x => x.TaxTitle.FormName == "NEC");
                        }
                        break;
                }
            }

            return vendorTaxInfoQuery.ToList();
        }
    }
}
