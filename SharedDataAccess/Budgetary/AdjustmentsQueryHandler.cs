﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{

    public class AdjustmentsQueryHandler : IQueryHandler<AdjustmentsSearchCriteria, IEnumerable<Adjustment>>
    {
        private ITrioContextFactory contextFactory;

        public AdjustmentsQueryHandler(ITrioContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }
        
        public IEnumerable<Adjustment> ExecuteQuery(AdjustmentsSearchCriteria query)
        {

            var budgetaryContext = contextFactory.GetBudgetaryContext();
            IQueryable<Adjustment> adjustmentQuery;

            adjustmentQuery = budgetaryContext.Adjustments;

            if (query.VendorNumber != 0)
            {
                adjustmentQuery = adjustmentQuery.Where(x => x.VendorNumber == query.VendorNumber);
            }

            if (query.FormType != TaxFormType.All)
            {
                var eligibleCodes = budgetaryContext.TaxTitles.Where(x =>
                    x.FormName == (query.FormType == TaxFormType.MISC1099 ? "MISC" : "NEC")).Select(y => y.TaxCode).ToList();

                adjustmentQuery = adjustmentQuery.Where(x => eligibleCodes.Any(z => z == (x.Code ?? 0)));
            }

            return adjustmentQuery.ToList();
        }
    }
}
