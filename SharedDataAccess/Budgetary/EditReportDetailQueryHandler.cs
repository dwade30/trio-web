﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
    public class EditReportDetailQueryHandler : IQueryHandler<EditReportDetailSearchCriteria, IEnumerable<EditReportDetailInfo>>
    {
        private ITrioContextFactory contextFactory;

        public EditReportDetailQueryHandler(ITrioContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }
        
    public IEnumerable<EditReportDetailInfo> ExecuteQuery(EditReportDetailSearchCriteria query)
        {

            var budgetaryContext = contextFactory.GetBudgetaryContext();

            var result = new List<EditReportDetailInfo>();

            var lowDate = new DateTime(query.Year, 1, 1);
            var highDate  = new DateTime(query.Year + 1, 1, 1);

            result.AddRange(budgetaryContext.APJournalDetails.Where(x =>
                x.APJournal.VendorNumber == query.VendorNumber && (query.IncludeAllPayments || x.C1099 != "N") && (x.APJournal.CheckDate >= lowDate && x.APJournal.CheckDate < highDate)).Select(y => new EditReportDetailInfo
            {
                TaxCode = query.Override1099Code != "" ? query.Override1099Code : y.C1099,
                CheckDate = (DateTime)y.APJournal.CheckDate,
                Amount = (y.Amount ?? 0) - (y.Discount ?? 0),
                JournalNumber = y.APJournal.JournalNumber ?? 0,
                BudgetaryAccount = y.Account,
                Description = y.Description
            }));

            result.AddRange(budgetaryContext.APJournalDetailArchives.Where(x =>
                x.APJournalArchive.VendorNumber == query.VendorNumber && (query.IncludeAllPayments || x._1099 != "N") && (x.APJournalArchive.CheckDate >= lowDate && x.APJournalArchive.CheckDate < highDate)).Select(y => new EditReportDetailInfo
            {
                TaxCode = query.Override1099Code != "" ? query.Override1099Code : y._1099,
                CheckDate = (DateTime)y.APJournalArchive.CheckDate,
                Amount = (y.Amount ?? 0) - (y.Discount ?? 0),
                JournalNumber = y.APJournalArchive.JournalNumber ?? 0,
                BudgetaryAccount = y.Account,
                Description = y.Description
            }));

            result.AddRange(budgetaryContext.JournalEntries.Where(x =>
                x.VendorNumber == query.VendorNumber && (query.IncludeAllPayments || x._1099 != "N") && (x.JournalEntriesDate >= lowDate && x.JournalEntriesDate < highDate)).Select(y => new EditReportDetailInfo
            {
                TaxCode = query.Override1099Code != "" ? query.Override1099Code : y._1099,
                CheckDate = (DateTime)y.JournalEntriesDate,
                Amount = y.Amount ?? 0,
                JournalNumber = y.JournalNumber ?? 0,
                BudgetaryAccount = y.Account,
                Description = y.Description
            }));

            result.AddRange(budgetaryContext.CDJournalArchives.Where(x =>
                x.VendorNumber == query.VendorNumber && (query.IncludeAllPayments || x._1099 != "N") && (x.ArchiveDate >= lowDate && x.ArchiveDate < highDate)).Select(y => new EditReportDetailInfo
            {
                TaxCode = query.Override1099Code != "" ? query.Override1099Code : y._1099,
                CheckDate = (DateTime)y.ArchiveDate,
                Amount = y.Amount ?? 0,
                JournalNumber = y.JournalNumber ?? 0,
                BudgetaryAccount = y.Account,
                Description = y.Description
            }));

            return result.ToList().OrderBy(x => x.CheckDate);
        }

        
    }
}
