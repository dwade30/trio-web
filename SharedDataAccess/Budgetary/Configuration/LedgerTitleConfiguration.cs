﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{

	public class LedgerTitleConfiguration : IEntityTypeConfiguration<LedgerTitle>
	{
		public void Configure(EntityTypeBuilder<LedgerTitle> builder)
		{
			builder.ToTable("LedgerTitles");
			builder.Property(e => e.Id)
				.HasColumnName("ID");

			builder.Property(e => e.Account)
				.HasMaxLength(11);

			builder.Property(e => e.ApoverrideCashFund)
				.HasColumnName("APOverrideCashFund")
				.HasMaxLength(255);

			builder.Property(e => e.CashFund)
				.HasMaxLength(255);

			builder.Property(e => e.CroverrideCashFund)
				.HasColumnName("CROverrideCashFund")
				.HasMaxLength(255);

			builder.Property(e => e.Fund)
				.HasMaxLength(2);

			builder.Property(e => e.GlaccountType)
				.HasColumnName("GLAccountType")
				.HasMaxLength(255);

			builder.Property(e => e.LongDescription)
				.HasMaxLength(30);

			builder.Property(e => e.PyoverrideCashFund)
				.HasColumnName("PYOverrideCashFund")
				.HasMaxLength(255);

			builder.Property(e => e.ShortDescription)
				.HasMaxLength(12);

			builder.Property(e => e.Tax)
				.HasMaxLength(255);

			builder.Property(e => e.Year)
				.HasMaxLength(2);
		}
	}
}
