﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class EoyAdjustmentConfiguration : IEntityTypeConfiguration<EoyAdjustment>
    {
        public void Configure(EntityTypeBuilder<EoyAdjustment> builder)
        {
            builder.ToTable("EOYAdjustments");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Account).HasMaxLength(255);

            builder.Property(e => e.Amount).HasColumnType("money");
        }
    }
}