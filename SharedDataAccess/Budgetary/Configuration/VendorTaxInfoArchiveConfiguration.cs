﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class VendorTaxInfoArchiveConfiguration : IEntityTypeConfiguration<VendorTaxInfoArchive>
    {
        public void Configure(EntityTypeBuilder<VendorTaxInfoArchive> builder)
        {
            builder.ToTable("VendorTaxInfoArchive");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.TaxCode).HasColumnName("Class");
            builder.Property(e => e.Amount).HasColumnType("money");
        }
    }
}
