﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class ClassCodeConfiguration : IEntityTypeConfiguration<ClassCode>
    {
        public void Configure(EntityTypeBuilder<ClassCode> builder)
        {
            builder.ToTable("ClassCodes");
            builder.Property(p => p.ClassKey).HasMaxLength(50);
            builder.Property(p => p.Code).HasMaxLength(50).HasColumnName("ClassCode");
        }
    }
}
