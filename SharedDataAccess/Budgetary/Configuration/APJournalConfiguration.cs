﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class APJournalConfiguration : IEntityTypeConfiguration<APJournal>
    {
        public void Configure(EntityTypeBuilder<APJournal> builder)
        {
            builder.ToTable("APJournal");
            builder.Property(p => p.TempVendorName).HasMaxLength(50);
            builder.Property(p => p.TempVendorAddress1).HasMaxLength(35);
            builder.Property(p => p.TempVendorAddress2).HasMaxLength(35);
            builder.Property(p => p.TempVendorAddress3).HasMaxLength(35);
            builder.Property(p => p.TempVendorAddress4).HasMaxLength(35);
            builder.Property(p => p.Frequency).HasMaxLength(3);
            builder.Property(p => p.Description).HasMaxLength(25);
            builder.Property(p => p.Reference).HasMaxLength(15);
            builder.Property(p => p.Amount).HasColumnType("money");
            builder.Property(p => p.CheckNumber).HasMaxLength(6);
            builder.Property(p => p.Status).HasMaxLength(1);
            builder.Property(p => p.Warrant).HasMaxLength(50);
            builder.Property(p => p.Returned).HasMaxLength(50);
            builder.Property(p => p.TotalEncumbrance).HasColumnType("money");
            builder.Property(p => p.AlternateCashAccount).HasMaxLength(255);
            builder.Property(p => p.TempVendorCity).HasMaxLength(255);
            builder.Property(p => p.TempVendorState).HasMaxLength(255);
            builder.Property(p => p.TempVendorZip).HasMaxLength(255);
            builder.Property(p => p.TempVendorZip4).HasMaxLength(255);
            builder.Property(p => p.SchoolOverride).HasMaxLength(255);
            builder.Property(p => p.TownOverride).HasMaxLength(255);
        }
    }
}
