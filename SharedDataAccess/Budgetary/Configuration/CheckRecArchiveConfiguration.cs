﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CheckRecArchiveConfiguration : IEntityTypeConfiguration<CheckRecArchive>
    {
        public void Configure(EntityTypeBuilder<CheckRecArchive> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Amount).HasColumnType("money");

            builder.Property(e => e.ArchiveDate).HasColumnType("datetime");

            builder.Property(e => e.Cdentry).HasColumnName("CDEntry");

            builder.Property(e => e.CheckDate).HasColumnType("datetime");

            builder.Property(e => e.CrperiodCloseoutKey).HasColumnName("CRPeriodCLoseoutKey");

            builder.Property(e => e.EnteredBy).HasMaxLength(255);

            builder.Property(e => e.EnteredDate).HasColumnType("datetime");

            builder.Property(e => e.Name).HasMaxLength(50);

            builder.Property(e => e.PypayDate)
                .HasColumnName("PYPayDate")
                .HasColumnType("datetime");

            builder.Property(e => e.PypayRunId).HasColumnName("PYPayRunID");

            builder.Property(e => e.Status).HasMaxLength(50);

            builder.Property(e => e.StatusDate).HasColumnType("datetime");

            builder.Property(e => e.Type).HasMaxLength(50);
        }
    }
}
