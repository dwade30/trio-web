﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class APJournalDetailArchiveConfiguration :IEntityTypeConfiguration<APJournalDetailArchive>
    {
        public void Configure(EntityTypeBuilder<APJournalDetailArchive> builder)
        {

                builder.ToTable("APJournalDetailArchive");

                builder.Property(e => e.Id).HasColumnName("ID");

                builder.Property(e => e.Account).HasMaxLength(255);

                builder.Property(e => e.Amount).HasColumnType("money");

                builder.Property(e => e.ApjournalArchiveId).HasColumnName("APJournalArchiveID");

                builder.Property(e => e.CorrectedAmount).HasColumnType("money");

                builder.Property(e => e.Description).HasMaxLength(25);

                builder.Property(e => e.Discount).HasColumnType("money");

                builder.Property(e => e.Encumbrance).HasColumnType("money");

                builder.Property(e => e.Project).HasMaxLength(4);

                builder.Property(e => e.PurchaseOrderDetailsId).HasColumnName("PurchaseOrderDetailsID");

                builder.Property(e => e.Rcb)
                    .HasColumnName("RCB")
                    .HasMaxLength(50);

                builder.Property(e => e._1099)
                    .HasColumnName("1099")
                    .HasMaxLength(1);
          
        }
    }
}
