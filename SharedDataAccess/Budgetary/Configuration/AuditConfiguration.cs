﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public  class AuditConfiguration :IEntityTypeConfiguration<Audit>
    {
        public void Configure(EntityTypeBuilder<Audit> builder)
        {
            builder.ToTable("Audit");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.OverrideDate).HasColumnType("datetime");

            builder.Property(e => e.OverrideOperator).HasMaxLength(50);

            builder.Property(e => e.OverrideScreen)
                .HasColumnName("OVerrideScreen")
                .HasMaxLength(50);

            builder.Property(e => e.OverrideTime).HasColumnType("datetime");

            builder.Property(e => e.OverrideType).HasMaxLength(50);
        }
    }
}
