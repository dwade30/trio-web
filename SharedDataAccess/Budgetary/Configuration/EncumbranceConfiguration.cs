﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class EncumbranceConfiguration : IEntityTypeConfiguration<Encumbrance>
    {
        public void Configure(EntityTypeBuilder<Encumbrance> builder)
        {
            builder.ToTable("Encumbrances");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Adjustments).HasColumnType("money");

            builder.Property(e => e.Amount).HasColumnType("money");

            builder.Property(e => e.Description).HasMaxLength(25);

            builder.Property(e => e.EncumbrancesDate).HasColumnType("datetime");

            builder.Property(e => e.Liquidated).HasColumnType("money");

            builder.Property(e => e.Po)
                .HasColumnName("PO")
                .HasMaxLength(15);

            builder.Property(e => e.PostedDate).HasColumnType("datetime");

            builder.Property(e => e.Status).HasMaxLength(1);

            builder.Property(e => e.TempVendorAddress1).HasMaxLength(35);

            builder.Property(e => e.TempVendorAddress2).HasMaxLength(35);

            builder.Property(e => e.TempVendorAddress3).HasMaxLength(35);

            builder.Property(e => e.TempVendorAddress4).HasMaxLength(35);

            builder.Property(e => e.TempVendorCity).HasMaxLength(255);

            builder.Property(e => e.TempVendorName).HasMaxLength(50);

            builder.Property(e => e.TempVendorState).HasMaxLength(255);

            builder.Property(e => e.TempVendorZip).HasMaxLength(255);

            builder.Property(e => e.TempVendorZip4).HasMaxLength(255);
        }
    }
}