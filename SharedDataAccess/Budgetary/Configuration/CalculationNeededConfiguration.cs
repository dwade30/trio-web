﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CalculationNeededConfiguration : IEntityTypeConfiguration<CalculationNeeded>
    {
        public void Configure(EntityTypeBuilder<CalculationNeeded> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");
        }
    }
}
