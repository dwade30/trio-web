﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class DbVersionConfiguration : IEntityTypeConfiguration<Dbversion>
    {
        public void Configure(EntityTypeBuilder<Dbversion> builder)
        {
            builder.ToTable("DBVersion");

            builder.Property(e => e.Id).HasColumnName("ID");
        }
    }
}