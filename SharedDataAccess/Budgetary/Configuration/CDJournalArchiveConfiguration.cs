﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CDJournalArchiveConfiguration : IEntityTypeConfiguration<CdjournalArchive>
    {
        public void Configure(EntityTypeBuilder<CdjournalArchive> builder)
        {
            builder.ToTable("CDJournalArchive");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Account).HasMaxLength(255);

            builder.Property(e => e.Amount).HasColumnType("money");

            builder.Property(e => e.ArchiveDate)
                .HasColumnName("CDJournalArchiveDate")
                .HasColumnType("datetime");

            builder.Property(e => e.CheckNumber).HasMaxLength(255);

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.Po)
                .HasColumnName("PO")
                .HasMaxLength(255);

            builder.Property(e => e.PostedDate).HasColumnType("datetime");

            builder.Property(e => e.Project).HasMaxLength(255);

            builder.Property(e => e.Rcb)
                .HasColumnName("RCB")
                .HasMaxLength(255);

            builder.Property(e => e.Status).HasMaxLength(255);

            builder.Property(e => e.Type).HasMaxLength(255);

            builder.Property(e => e._1099)
                .HasColumnName("1099")
                .HasMaxLength(255);
        }
    }
}
