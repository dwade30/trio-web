﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    class APJournalArchiveConfiguration : IEntityTypeConfiguration<APjournalArchive>
    {
        public void Configure(EntityTypeBuilder<APjournalArchive> builder)
        {
            builder.ToTable("APJournalArchive");

                builder.Property(e => e.Id).HasColumnName("ID");

                builder.Property(e => e.AlternateCashAccount).HasMaxLength(255);

                builder.Property(e => e.Amount).HasColumnType("money");

                builder.Property(e => e.CheckDate).HasColumnType("datetime");

                builder.Property(e => e.CheckNumber).HasMaxLength(6);

                builder.Property(e => e.Description).HasMaxLength(25);

                builder.Property(e => e.Eftcheck).HasColumnName("EFTCheck");

                builder.Property(e => e.Frequency).HasMaxLength(3);

                builder.Property(e => e.Payable).HasColumnType("datetime");

                builder.Property(e => e.PostedDate).HasColumnType("datetime");

                builder.Property(e => e.PurchaseOrderId).HasColumnName("PurchaseOrderID");

                builder.Property(e => e.Reference).HasMaxLength(15);

                builder.Property(e => e.Returned).HasMaxLength(50);

                builder.Property(e => e.SchoolOverride).HasMaxLength(255);

                builder.Property(e => e.Status).HasMaxLength(1);

                builder.Property(e => e.TempVendorAddress1).HasMaxLength(35);

                builder.Property(e => e.TempVendorAddress2).HasMaxLength(35);

                builder.Property(e => e.TempVendorAddress3).HasMaxLength(35);

                builder.Property(e => e.TempVendorAddress4).HasMaxLength(35);

                builder.Property(e => e.TempVendorCity).HasMaxLength(255);

                builder.Property(e => e.TempVendorName).HasMaxLength(50);

                builder.Property(e => e.TempVendorState).HasMaxLength(255);

                builder.Property(e => e.TempVendorZip).HasMaxLength(255);

                builder.Property(e => e.TempVendorZip4).HasMaxLength(255);

                builder.Property(e => e.TotalEncumbrance).HasColumnType("money");

                builder.Property(e => e.TownOverride).HasMaxLength(255);

                builder.Property(e => e.Until).HasColumnType("datetime");

                builder.Property(e => e.Warrant).HasMaxLength(50);
        }
    }
}
