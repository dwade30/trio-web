﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CustomCheckFieldConfiguration : IEntityTypeConfiguration<CustomCheckField>
    {
        public void Configure(EntityTypeBuilder<CustomCheckField> builder)
        {
            builder.ToTable("CustomCheckFields");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.CheckType).HasMaxLength(255);

            builder.Property(e => e.ControlName).HasMaxLength(255);

            builder.Property(e => e.DefaultXposition).HasColumnName("DefaultXPosition");

            builder.Property(e => e.DefaultYposition).HasColumnName("DefaultYPosition");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.FormatId).HasColumnName("FormatID");

            builder.Property(e => e.Xposition).HasColumnName("XPosition");

            builder.Property(e => e.Yposition).HasColumnName("YPosition");
        }
    }
}
