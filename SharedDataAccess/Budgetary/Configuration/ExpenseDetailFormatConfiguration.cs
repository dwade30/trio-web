﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class ExpenseDetailFormatConfiguration : IEntityTypeConfiguration<ExpenseDetailFormat>
    {
        public void Configure(EntityTypeBuilder<ExpenseDetailFormat> builder)
        {
            builder.ToTable("ExpenseDetailFormats");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Description).HasMaxLength(25);

            builder.Property(e => e.DescriptionLength).HasMaxLength(1);

            builder.Property(e => e.Font).HasMaxLength(1);

            builder.Property(e => e.FontName).HasMaxLength(25);

            builder.Property(e => e.PaperWidth).HasMaxLength(1);

            builder.Property(e => e.Printer).HasMaxLength(1);

            builder.Property(e => e.Use).HasMaxLength(1);

            builder.Property(e => e.YtddebitCredit).HasColumnName("YTDDebitCredit");

            builder.Property(e => e.Ytdnet).HasColumnName("YTDNet");
        }
    }
}