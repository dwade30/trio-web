﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class EncumbranceDetailConfiguration : IEntityTypeConfiguration<EncumbranceDetail>
    {
        public void Configure(EntityTypeBuilder<EncumbranceDetail> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Account).HasMaxLength(255);

            builder.Property(e => e.Adjustments).HasColumnType("money");

            builder.Property(e => e.Amount).HasColumnType("money");

            builder.Property(e => e.Description).HasMaxLength(25);

            builder.Property(e => e.EncumbranceId).HasColumnName("EncumbranceID");

            builder.Property(e => e.Liquidated).HasColumnType("money");

            builder.Property(e => e.Project).HasMaxLength(4);
        }
    }
}