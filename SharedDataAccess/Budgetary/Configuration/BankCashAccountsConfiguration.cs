﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class BankCashAccountsConfiguration : IEntityTypeConfiguration<BankCashAccount>
    {
        public void Configure(EntityTypeBuilder<BankCashAccount> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Account).HasMaxLength(255);
        }
    }
}
