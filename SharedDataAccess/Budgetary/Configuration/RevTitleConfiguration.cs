﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
	public class RevTitleConfiguration : IEntityTypeConfiguration<RevTitle>
	{
		public void Configure(EntityTypeBuilder<RevTitle> builder)
		{
			builder.ToTable("RevTitles");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.CloseoutAccount).HasMaxLength(255);
			builder.Property(e => e.Department).HasMaxLength(11);
			builder.Property(e => e.Division).HasMaxLength(10);
			builder.Property(e => e.LongDescription).HasMaxLength(30);
			builder.Property(e => e.Revenue).HasMaxLength(11);
			builder.Property(e => e.ShortDescription).HasMaxLength(12);
		}
	}
}
