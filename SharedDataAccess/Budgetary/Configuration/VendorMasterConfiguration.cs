﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class VendorMasterConfiguration : IEntityTypeConfiguration<VendorMaster>
    {
        public void Configure(EntityTypeBuilder<VendorMaster> builder)
        {
            builder.ToTable("VendorMaster");
            builder.Property(p => p.CheckName).HasMaxLength(50);
            builder.Property(p => p.CheckAddress1).HasMaxLength(35);
            builder.Property(p => p.CheckAddress2).HasMaxLength(35);
            builder.Property(p => p.CheckAddress3).HasMaxLength(35);
            builder.Property(p => p.CheckAddress4).HasMaxLength(35);
            builder.Property(p => p.CorrespondName).HasMaxLength(50);
            builder.Property(p => p.CorrespondAddress1).HasMaxLength(35);
            builder.Property(p => p.CorrespondAddress2).HasMaxLength(35);
            builder.Property(p => p.CorrespondAddress3).HasMaxLength(35);
            builder.Property(p => p.CorrespondAddress4).HasMaxLength(35);
            builder.Property(p => p.Contact).HasMaxLength(30);
            builder.Property(p => p.TelephoneNumber).HasMaxLength(15);
            builder.Property(p => p.Extension).HasMaxLength(7);
            builder.Property(p => p.Status).HasMaxLength(1);
            builder.Property(p => p.Class).HasMaxLength(6);
            builder.Property(p => p.C1099).HasColumnName("1099");
            builder.Property(p => p.TaxNumber).HasMaxLength(255);
            builder.Property(p => p.Message).HasMaxLength(60);
            builder.Property(p => p.CheckMessage).HasMaxLength(60);
            builder.Property(p => p.DataEntryMessage).HasMaxLength(60);
            builder.Property(p => p.CheckNumber).HasMaxLength(10);
            builder.Property(p => p.UpdatedBy).HasMaxLength(50);
            builder.Property(p => p.DefaultDescription).HasMaxLength(50);
            builder.Property(p => p.C1099UseCorrName).HasColumnName("1099UseCorrName");
            builder.Property(p => p.Email).HasMaxLength(255);
            builder.Property(p => p.FaxNumber).HasMaxLength(255);
            builder.Property(p => p.CheckCity).HasMaxLength(255);
            builder.Property(p => p.CheckState).HasMaxLength(255);
            builder.Property(p => p.CheckZip).HasMaxLength(255);
            builder.Property(p => p.CheckZip4).HasMaxLength(255);
            builder.Property(p => p.CorrespondCity).HasMaxLength(255);
            builder.Property(p => p.CorrespondState).HasMaxLength(255);
            builder.Property(p => p.CorrespondZip).HasMaxLength(255);
            builder.Property(p => p.CorrespondZip4).HasMaxLength(255);
            builder.Property(p => p.C1099Code).HasMaxLength(255).HasColumnName("1099Code");
            builder.Property(p => p.DefaultCheckNumber).HasMaxLength(255);
            builder.Property(p => p.RoutingNumber).HasMaxLength(255);
            builder.Property(p => p.BankAccountNumber).HasMaxLength(255);
            builder.Property(p => p.AccountType).HasMaxLength(255);
        }
    }
}
