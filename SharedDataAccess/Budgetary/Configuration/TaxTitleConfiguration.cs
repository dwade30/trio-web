﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class TaxTitleConfiguration : IEntityTypeConfiguration<TaxTitle>
    {
        public void Configure(EntityTypeBuilder<TaxTitle> builder)
        {
            builder.ToTable("TaxTitles");
            builder.Property(p => p.Id).HasColumnName("ID");
            builder.Property(p => p.TaxDescription).HasMaxLength(50);
            builder.Property(p => p.FormName).HasMaxLength(50);
        }
    }
}
