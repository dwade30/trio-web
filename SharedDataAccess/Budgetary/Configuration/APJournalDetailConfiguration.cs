﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    class APJournalDetailConfiguration : IEntityTypeConfiguration<APJournalDetail>
    {
        public void Configure(EntityTypeBuilder<APJournalDetail> builder)
        {
            builder.ToTable("APJournalDetail");
            builder.Property(p => p.Description).HasMaxLength(25);
            builder.Property(p => p.Account).HasMaxLength(255);
            builder.Property(p => p.Amount).HasColumnType("money");
            builder.Property(p => p.Project).HasMaxLength(4);
            builder.Property(p => p.Encumbrance).HasColumnType("money");
            builder.Property(p => p.Discount).HasColumnType("money");
            builder.Property(p => p.C1099).HasMaxLength(1).HasColumnName("1099");
            builder.Property(p => p.RCB).HasMaxLength(50);
            builder.Property(p => p.CorrectedAmount).HasColumnType("money");
        }
    }
}
