﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CustomReportConfiguration : IEntityTypeConfiguration<CustomReport>
    {
        public void Configure(EntityTypeBuilder<CustomReport> builder)
        {
            builder.ToTable("CustomReports");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AcctInfo).HasMaxLength(255);

            builder.Property(e => e.DateRange).HasMaxLength(50);

            builder.Property(e => e.Title).HasMaxLength(50);

            builder.Property(e => e.Ytdactivity).HasColumnName("YTDActivity");

            builder.Property(e => e.YtddebCred).HasColumnName("YTDDebCred");
        }
    }
}