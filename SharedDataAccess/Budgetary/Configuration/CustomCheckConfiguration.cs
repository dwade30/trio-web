﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CustomCheckConfiguration : IEntityTypeConfiguration<CustomCheck>
    {
        public void Configure(EntityTypeBuilder<CustomCheck> builder)
        {
            builder.ToTable("CustomChecks");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.CheckPosition).HasMaxLength(255);

            builder.Property(e => e.CheckType).HasMaxLength(255);

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.ScannedCheckFile).HasMaxLength(255);

            builder.Property(e => e.VoidAfterMessage).HasMaxLength(50);
        }
    }
}
