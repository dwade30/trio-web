﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CustomBudgetReportConfiguration : IEntityTypeConfiguration<CustomBudgetReport>
    {
        public void Configure(EntityTypeBuilder<CustomBudgetReport> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AccountTypes).HasMaxLength(255);

            builder.Property(e => e.BudgetYear).HasMaxLength(255);

            builder.Property(e => e.Comments).HasMaxLength(255);

            builder.Property(e => e.CurrentYear).HasMaxLength(255);

            builder.Property(e => e.CurrentYtd).HasColumnName("CurrentYTD");

            builder.Property(e => e.DeptPageBreak).HasMaxLength(255);

            builder.Property(e => e.DeptTotals).HasMaxLength(255);

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.DivPageBreak).HasMaxLength(255);

            builder.Property(e => e.DivTotals).HasMaxLength(255);

            builder.Property(e => e.ExpLowestDetailLevel).HasMaxLength(255);

            builder.Property(e => e.ExpTotals).HasMaxLength(255);

            builder.Property(e => e.FunctionPageBreak).HasMaxLength(255);

            builder.Property(e => e.HighDept).HasMaxLength(255);

            builder.Property(e => e.IncludeBudgetAdjustments).HasMaxLength(255);

            builder.Property(e => e.IncludeCarryForward).HasMaxLength(255);

            builder.Property(e => e.LowDept).HasMaxLength(255);

            builder.Property(e => e.ObjTotals).HasMaxLength(255);

            builder.Property(e => e.Orientation).HasMaxLength(255);

            builder.Property(e => e.ReportTitle).HasMaxLength(255);

            builder.Property(e => e.ReportingRange).HasMaxLength(255);

            builder.Property(e => e.RevLowestDetailLevel).HasMaxLength(255);

            builder.Property(e => e.TownSchool).HasMaxLength(255);
        }
    }
}
