﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class AccountMasterConfiguration : IEntityTypeConfiguration<AccountMaster>
    {
        public void Configure(EntityTypeBuilder<AccountMaster> builder)
        {
            builder.ToTable("AccountMaster");
            builder.Property(p => p.Account).HasMaxLength(50);
            builder.Property(p => p.AccountType).HasMaxLength(50);
            builder.Property(p => p.FirstAccountField).HasMaxLength(50);
            builder.Property(p => p.SecondAccountField).HasMaxLength(50);
            builder.Property(p => p.ThirdAccountField).HasMaxLength(50);
            builder.Property(p => p.FourthAccountField).HasMaxLength(50);
            builder.Property(p => p.FifthAccountField).HasMaxLength(50);
            builder.Property(p => p.Status).HasMaxLength(50);
            builder.Property(p => p.CurrentBudget).HasColumnType("money");
            builder.Property(p => p.JanBudget).HasColumnType("money");
            builder.Property(p => p.FebBudget).HasColumnType("money");
            builder.Property(p => p.MarBudget).HasColumnType("money");
            builder.Property(p => p.AprBudget).HasColumnType("money");
            builder.Property(p => p.MayBudget).HasColumnType("money");
            builder.Property(p => p.JuneBudget).HasColumnType("money");
            builder.Property(p => p.JulyBudget).HasColumnType("money");
            builder.Property(p => p.AugBudget).HasColumnType("money");
            builder.Property(p => p.SeptBudget).HasColumnType("money");
            builder.Property(p => p.OctBudget).HasColumnType("money");
            builder.Property(p => p.NovBudget).HasColumnType("money");
            builder.Property(p => p.DecBudget).HasColumnType("money");
            builder.Property(p => p.EFM45AccountNumber).HasMaxLength(255);
            builder.Property(p => p.EFM45Title).HasMaxLength(255);
            builder.Property(p => p.PreviousYearEndingBalance).HasColumnType("money");
            builder.Property(p => p.SchoolReportingAccount).HasMaxLength(255);
            builder.Property(p => p.GLAccountType).HasMaxLength(255);
        }
    }
}
