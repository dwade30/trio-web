﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class BudgetConfiguration : IEntityTypeConfiguration<Budget>
    {
        public void Configure(EntityTypeBuilder<Budget> builder)
        {
            builder.ToTable("Budget");
            builder.Property(p => p.Account).HasMaxLength(50);
            builder.Property(p => p.InitialRequest).HasColumnType("money");
            builder.Property(p => p.ManagerRequest).HasColumnType("money");
            builder.Property(p => p.CommitteeRequest).HasColumnType("money");
            builder.Property(p => p.ApprovedAmount).HasColumnType("money");
            builder.Property(p => p.JanBudget).HasColumnType("money");
            builder.Property(p => p.FebBudget).HasColumnType("money");
            builder.Property(p => p.MarBudget).HasColumnType("money");
            builder.Property(p => p.AprBudget).HasColumnType("money");
            builder.Property(p => p.MayBudget).HasColumnType("money");
            builder.Property(p => p.JuneBudget).HasColumnType("money");
            builder.Property(p => p.JulyBudget).HasColumnType("money");
            builder.Property(p => p.AugBudget).HasColumnType("money");
            builder.Property(p => p.SeptBudget).HasColumnType("money");
            builder.Property(p => p.OctBudget).HasColumnType("money");
            builder.Property(p => p.NovBudget).HasColumnType("money");
            builder.Property(p => p.DecBudget).HasColumnType("money");
            builder.Property(p => p.ElectedRequest).HasColumnType("money");
        }
    }
}

