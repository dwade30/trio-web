﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class BudgetarySettingConfiguration : IEntityTypeConfiguration<BudgetarySetting>
    {
        public void Configure(EntityTypeBuilder<BudgetarySetting> builder)
        {
            builder.ToTable("Budgetary");
            builder.Property(p => p.UseProjectNumber).HasMaxLength(1);
            builder.Property(p => p.FiscalStart).HasMaxLength(2);
            builder.Property(p => p.SummaryReportOption).HasMaxLength(1);
            builder.Property(p => p.WarrantPrevOutput).HasMaxLength(1);
            builder.Property(p => p.WarrantPrevSigLine).HasMaxLength(1);
            builder.Property(p => p.CheckRef).HasMaxLength(1);
            builder.Property(p => p.PreviewPrint).HasMaxLength(1);
            builder.Property(p => p.WarrantPrintOption).HasMaxLength(1);
            builder.Property(p => p.APSeq).HasMaxLength(1);
            builder.Property(p => p.EOYStatus).HasMaxLength(1);
            builder.Property(p => p.CashDue).HasMaxLength(2);
            builder.Property(p => p.DueFrom).HasMaxLength(20);
            builder.Property(p => p.DueTo).HasMaxLength(20);
            builder.Property(p => p.Due).HasMaxLength(1);
            builder.Property(p => p.Expenditure).HasMaxLength(8);
            builder.Property(p => p.Revenue).HasMaxLength(6);
            builder.Property(p => p.Ledger).HasMaxLength(6);
            builder.Property(p => p.ValidAccountUse).HasMaxLength(1);
            builder.Property(p => p.CheckRec).HasMaxLength(50);
            builder.Property(p => p.C1099Laser).HasColumnName("1099Laser");
            builder.Property(p => p.C1099LaserAdjustment).HasColumnName("1099LaserAdjustment");
            builder.Property(p => p.ShadeReports).HasMaxLength(50);
            builder.Property(p => p.CheckIcon).HasMaxLength(255);
            builder.Property(p => p.C1099LaserAdjustmentHorizontal).HasColumnName("1099LaserAdjustmentHorizontal");
            builder.Property(p => p.ImmediateOriginODFINumber).HasMaxLength(255);
            builder.Property(p => p.ImmediateOriginName).HasMaxLength(255);
            builder.Property(p => p.ImmediateOriginRoutingNumber).HasMaxLength(255);
            builder.Property(p => p.ImmediateDestinationName).HasMaxLength(255);
            builder.Property(p => p.ImmediateDestinationRoutingNumber).HasMaxLength(255);
            builder.Property(p => p.ACHTownName).HasMaxLength(255);
            builder.Property(p => p.ACHFederalID).HasMaxLength(255);
            builder.Property(p => p.ReturnAddress1).HasMaxLength(255);
            builder.Property(p => p.ReturnAddress2).HasMaxLength(255);
            builder.Property(p => p.ReturnAddress3).HasMaxLength(255);
            builder.Property(p => p.ReturnAddress4).HasMaxLength(255);
            builder.Property(p => p.Version).HasMaxLength(50);
        }
    }
}
