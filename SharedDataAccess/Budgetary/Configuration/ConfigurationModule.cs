﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.CashReceipts;

namespace SharedDataAccess.Budgetary.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			RegisterDeptDivTitleSearchQueryHandler(ref builder);
			RegisterExpObjTitleSearchQueryHandler(ref builder);
			RegisterRevTitleSearchQueryHandler(ref builder);
			RegisterLedgerTitleSearchQueryHandler(ref builder);
            RegisterBudgetaryContext(ref builder);
            RegisterBudgetaryJournalService(ref builder);
            RegisterTaxTitleQueryHandler(ref builder);
            RegisterVendorTaxInfoQueryHandler(ref builder);
            RegisterVendorMasterQueryHandler(ref builder);
            RegisterAdjustmentsQueryHandler(ref builder);
            RegisterEditReportDetailQueryHandler(ref builder);
        }


        private void RegisterBudgetaryContext(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var activeModules = f.Resolve<IGlobalActiveModuleSettings>();
                if (activeModules.BudgetaryIsActive)
                {
                    var tcf = f.Resolve<ITrioContextFactory>();
                    return tcf.GetBudgetaryContext();
                }
                IBudgetaryContext emptyContext = new BudgetaryEmptyContext();
                return emptyContext;
            }).As<IBudgetaryContext>();
        }

        private void RegisterVendorMasterQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<VendorMasterQueryHandler>()
                .As<IQueryHandler<VendorMasterSearchCriteria, IEnumerable<VendorMaster>>>();
        }

        private void RegisterAdjustmentsQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<AdjustmentsQueryHandler>()
                .As<IQueryHandler<AdjustmentsSearchCriteria, IEnumerable<Adjustment>>>();
        }

        private void RegisterEditReportDetailQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<EditReportDetailQueryHandler>()
                .As<IQueryHandler<EditReportDetailSearchCriteria, IEnumerable<EditReportDetailInfo>>>();
        }

        private void RegisterTaxTitleQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<TaxTitleQueryHandler>()
                .As<IQueryHandler<TaxTitleSearchCriteria, IEnumerable<TaxTitle>>>();
        }

        private void RegisterVendorTaxInfoQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<VendorTaxInfoQueryHandler>()
                .As<IQueryHandler<VendorTaxInfoSearchCriteria, IEnumerable<VendorTaxInfo>>>();
        }


		private void RegisterDeptDivTitleSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<DeptDivTitleQueryHandler>()
				.As<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
		}

		private void RegisterExpObjTitleSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<ExpObjTitleQueryHandler>()
				.As<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
		}

		private void RegisterRevTitleSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<RevTitleQueryHandler>()
				.As<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
		}

		private void RegisterLedgerTitleSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<LedgerTitleQueryHandler>()
				.As<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
		}

		private void RegisterBudgetaryJournalService(ref ContainerBuilder builder)
		{
			builder.RegisterType<BudgetaryJournalService>()
				.As<IBudgetaryJournalService>();
		}

	}
}
