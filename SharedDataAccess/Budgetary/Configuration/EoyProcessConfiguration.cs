﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class EoyProcessConfiguration : IEntityTypeConfiguration<Eoyprocess>
    {
        public void Configure(EntityTypeBuilder<Eoyprocess> builder)
        {
            builder.ToTable("EOYProcess");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.EoycompletedDate)
                .HasColumnName("EOYCompletedDate")
                .HasColumnType("datetime");
        }
    }
}
