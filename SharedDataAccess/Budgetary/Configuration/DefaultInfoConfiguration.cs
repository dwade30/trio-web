﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class DefaultInfoConfiguration : IEntityTypeConfiguration<DefaultInfo>
    {
        public void Configure(EntityTypeBuilder<DefaultInfo> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AccountNumber).HasMaxLength(50);

            builder.Property(e => e.Amount).HasColumnType("money");

            builder.Property(e => e.Description).HasMaxLength(50);

            builder.Property(e => e.Project).HasMaxLength(4);

            builder.Property(e => e.Tax).HasMaxLength(1);
        }
    }
}