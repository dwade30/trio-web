﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class LedgerDetailSummaryInfoConfiguration : IEntityTypeConfiguration<LedgerDetailSummaryInfo>
    {
        public void Configure(EntityTypeBuilder<LedgerDetailSummaryInfo> builder)
        {
           
                builder.Property(e => e.Id)
                      .HasColumnName("ID");

                builder.Property(e => e.Account)
                      .HasMaxLength(50);

                builder.Property(e => e.BudgetAdjustments)
                      .HasColumnType("money");

                builder.Property(e => e.CarryForward)
                      .HasColumnType("money");

                builder.Property(e => e.EncumbActivity)
                      .HasColumnType("money");

                builder.Property(e => e.EncumbranceCredits)
                      .HasColumnType("money");

                builder.Property(e => e.EncumbranceDebits)
                      .HasColumnType("money");

                builder.Property(e => e.Fund)
                      .HasMaxLength(50);

                builder.Property(e => e.LedgerAccount)
                      .HasMaxLength(50);

                builder.Property(e => e.OriginalBudget)
                      .HasColumnType("money");

                builder.Property(e => e.PendingCredits)
                      .HasColumnType("money");

                builder.Property(e => e.PendingDebits)
                      .HasColumnType("money");

                builder.Property(e => e.PostedCredits)
                      .HasColumnType("money");

                builder.Property(e => e.PostedDebits)
                      .HasColumnType("money");

                builder.Property(e => e.Suffix)
                      .HasMaxLength(50);
            
        }
    }
}