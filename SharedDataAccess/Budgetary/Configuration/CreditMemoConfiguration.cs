﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CreditMemoConfiguration : IEntityTypeConfiguration<CreditMemo>
    {
        public void Configure(EntityTypeBuilder<CreditMemo> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Adjustments).HasColumnType("money");

            builder.Property(e => e.Amount).HasColumnType("money");

            builder.Property(e => e.CreditMemoDate).HasColumnType("datetime");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.Fund).HasMaxLength(255);

            builder.Property(e => e.Liquidated).HasColumnType("money");

            builder.Property(e => e.MemoNumber).HasMaxLength(255);

            builder.Property(e => e.OriginalAprecord).HasColumnName("OriginalAPRecord");

            builder.Property(e => e.PostedDate).HasColumnType("datetime");

            builder.Property(e => e.Status).HasMaxLength(255);
        }
    }
}
