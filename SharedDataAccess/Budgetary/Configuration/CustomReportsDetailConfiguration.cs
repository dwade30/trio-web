﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CustomReportsDetailConfiguration : IEntityTypeConfiguration<CustomReportsDetail>
    {
        public void Configure(EntityTypeBuilder<CustomReportsDetail> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Account).HasMaxLength(50);

            builder.Property(e => e.DetailSummary).HasMaxLength(50);

            builder.Property(e => e.Title).HasMaxLength(50);

            builder.Property(e => e.Type).HasMaxLength(50);
        }
    }
}