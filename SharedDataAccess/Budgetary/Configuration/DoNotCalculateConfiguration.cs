﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class DoNotCalculateConfiguration : IEntityTypeConfiguration<DoNotCalculate>
    {
        public void Configure(EntityTypeBuilder<DoNotCalculate> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Account).HasMaxLength(255);
        }
    }
}