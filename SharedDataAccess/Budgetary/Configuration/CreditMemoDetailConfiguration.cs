﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class CreditMemoDetailConfiguration : IEntityTypeConfiguration<CreditMemoDetail>
    {
        public void Configure(EntityTypeBuilder<CreditMemoDetail> builder)
        {
            
                builder.Property(e => e.Id).HasColumnName("ID");

                builder.Property(e => e.Account).HasMaxLength(255);

                builder.Property(e => e.Adjustments).HasColumnType("money");

                builder.Property(e => e.Amount).HasColumnType("money");

                builder.Property(e => e.ParentId).HasColumnName("CreditMemoID");

                builder.Property(e => e.Liquidated).HasColumnType("money");

                builder.Property(e => e.Project).HasMaxLength(255);

                builder.Property(e => e._1099)
                    .HasColumnName("1099")
                    .HasMaxLength(255);
        }
    }
}
