﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class DeptDivTitleConfiguration : IEntityTypeConfiguration<DeptDivTitle>
    {
        public void Configure(EntityTypeBuilder<DeptDivTitle> builder)
        {
            builder.ToTable("DeptDivTitles");
            builder.Property(p => p.Department).HasMaxLength(11);
            builder.Property(p => p.Division).HasMaxLength(10);
            builder.Property(p => p.ShortDescription).HasMaxLength(12);
            builder.Property(p => p.LongDescription).HasMaxLength(30);
            builder.Property(p => p.Fund).HasMaxLength(2);
            builder.Property(p => p.CloseoutAccount).HasMaxLength(255);
        }
    }
}

