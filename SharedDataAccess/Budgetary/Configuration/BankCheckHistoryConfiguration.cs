﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class BankCheckHistoryConfiguration : IEntityTypeConfiguration<BankCheckHistory>
    {
        public void Configure(EntityTypeBuilder<BankCheckHistory> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.BankId).HasColumnName("BankID");

            builder.Property(e => e.CheckType).HasMaxLength(255);
        }
    }
}
