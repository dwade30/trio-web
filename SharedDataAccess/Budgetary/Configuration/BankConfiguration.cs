﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class BankConfiguration : IEntityTypeConfiguration<Bank>
    {
        public void Configure(EntityTypeBuilder<Bank> builder)
        {
            builder.ToTable("Banks");
            builder.Property(p => p.Name).HasMaxLength(30);
            builder.Property(p => p.Balance).HasColumnType("money");
            builder.Property(p => p.Fund).HasMaxLength(50);
            builder.Property(p => p.CashAccount).HasMaxLength(255);
            builder.Property(p => p.TownCashAccount).HasMaxLength(255);
            builder.Property(p => p.SchoolCashAccount).HasMaxLength(255);
            builder.Property(p => p.RoutingNumber).HasMaxLength(255);
            builder.Property(p => p.BankAccountNumber).HasMaxLength(255);
            builder.Property(p => p.AccountType).HasMaxLength(255);
        }
    }
}
