﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class ArchiveAPAmountConfiguration :IEntityTypeConfiguration<ArchiveAPAmount>
    {
        public void Configure(EntityTypeBuilder<ArchiveAPAmount> builder)
        {
            builder.ToTable("ArchiveAPAmounts");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Account).HasMaxLength(255);

            builder.Property(e => e.Amount).HasColumnType("money");

            builder.Property(e => e.CashAccount).HasMaxLength(255);
        }
    }
}
