﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary.Configuration
{
    public class ExpObjTitleConfiguration : IEntityTypeConfiguration<ExpObjTitle>
    {
        public void Configure(EntityTypeBuilder<ExpObjTitle> builder)
        {
            builder.ToTable("ExpObjTitles");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Expense).HasMaxLength(11);

            builder.Property(e => e.LongDescription).HasMaxLength(30);

            builder.Property(e => e.Object).HasMaxLength(10);

            builder.Property(e => e.ShortDescription).HasMaxLength(12);

            builder.Property(e => e.Tax).HasMaxLength(1);
        }
    }
}