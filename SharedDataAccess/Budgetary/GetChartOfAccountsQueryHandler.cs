﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Budgetary.Queries;
using SharedApplication.Extensions;

namespace SharedDataAccess.Budgetary
{
    public class GetChartOfAccountsQueryHandler : IQueryHandler<GetChartOfAccounts,ChartOfAccounts>
    {
        private IBudgetaryContext budgetaryContext;
        private IGlobalBudgetaryAccountSettings budgetarySettings;
        public GetChartOfAccountsQueryHandler(ITrioContextFactory contextFactory, IGlobalBudgetaryAccountSettings budgetaryAccountSettings)
        {
            this.budgetaryContext = contextFactory.GetBudgetaryContext();
            budgetarySettings = budgetaryAccountSettings;
        }
        public ChartOfAccounts ExecuteQuery(GetChartOfAccounts query)
        {
            var coa = new ChartOfAccounts();
            var accountMasters = budgetaryContext.AccountMasters.OrderBy(a => a.Account).ToList();

            var deptDivTitles = budgetaryContext.DeptDivTitles.OrderBy(d => d.Department).ThenBy(d => d.Division).ToList();
            var departments = deptDivTitles.ToBudgetaryDepartments();
            coa.AddDepartments(departments);

            var ledgerTitles = budgetaryContext.LedgerTitles.OrderBy(l => l.Fund).ThenBy(l => l.Account)
                .ThenBy(l => l.Year).ToList();
            coa.AddFunds(ledgerTitles.ToBudgetaryFunds());

            (string assetLow, string assetHigh, string liabilityLow, string liabilityHigh, string fundLow, string
                fundHigh) ledgerRanges = ("","","","","","");


            if (!budgetarySettings.UsePUCChartOfAccounts)
            {
                 ledgerRanges = GetLedgerRanges(budgetaryContext.LedgerRanges);

            }

            var budgetaryAccounts = accountMasters.ToBudgetaryAccounts();
            foreach (var account in budgetaryAccounts)
            {
                if (account.IsLedger())
                {
                    LedgerTitle ledgerTitle;
                    if (budgetarySettings.SuffixExistsInLedger())
                    {
                         ledgerTitle = ledgerTitles
                            .FirstOrDefault(l => l.Fund == account.FirstAccountField && l.Account == account.SecondAccountField && l.Year == account.ThirdAccountField);
                    }
                    else
                    {
                         ledgerTitle = ledgerTitles
                            .FirstOrDefault(l => l.Fund == account.FirstAccountField && l.Account == account.SecondAccountField);
                    }

                    if (ledgerTitle != null)
                    {
                        account.Description = ledgerTitle.LongDescription;
                        account.ShortDescription = ledgerTitle.ShortDescription;
                    }

                    if (!budgetarySettings.UsePUCChartOfAccounts)
                    {
                        if (account.SecondAccountField.IsBetween(ledgerRanges.assetLow, ledgerRanges.assetHigh))
                        {
                            account.TypeOfAccount = BudgetaryAccountType.Asset;
                        }
                        else if (account.SecondAccountField.IsBetween(ledgerRanges.liabilityLow,
                            ledgerRanges.liabilityHigh))
                        {
                            account.TypeOfAccount = BudgetaryAccountType.Liability;
                        }
                        else if (account.SecondAccountField.IsBetween(ledgerRanges.fundLow, ledgerRanges.fundHigh))
                        {
                            account.TypeOfAccount = BudgetaryAccountType.FundBalance;
                        }
                    }
                }
                else
                {
                    if (account.AccountType == "E")
                    {
                        account.TypeOfAccount = BudgetaryAccountType.Expense;
                    }
                    else if (account.AccountType == "R")
                    {
                        account.TypeOfAccount = BudgetaryAccountType.Revenue;
                    }
                }
            }

            return coa;
        }

        private (string assetLow, string assetHigh, string liabilityLow, string liabilityHigh, string fundLow, string
            fundHigh) GetLedgerRanges(IEnumerable<LedgerRange> ledgerRanges)
        {
            var assetLow = "";
            var assetHigh = "";
            var LiabilityLow = "";
            var LiabilityHigh = "";
            var FundLow = "";
            var FundHigh = "";
            var assetRange = ledgerRanges
                .FirstOrDefault(l => l.RecordName == "A" && l.Low != "");
            if (assetRange != null)
            {
                assetLow = assetRange.Low;
                assetHigh = assetRange.High;
            }

            var LiabilityRange =
                ledgerRanges.FirstOrDefault(l => l.RecordName == "L" && l.Low != "");
            if (LiabilityRange != null)
            {
                LiabilityLow = LiabilityRange.Low;
                LiabilityHigh = LiabilityRange.High;
            }

            var FundRange =ledgerRanges.FirstOrDefault(l => l.RecordName == "F" && l.Low != "");
            if (FundRange != null)
            {
                FundLow = FundRange.Low;
                FundHigh = FundRange.High;
            }

            return (assetLow: assetLow, assetHigh: assetHigh, liabilityLow: LiabilityLow, liabilityHigh: LiabilityHigh,
                fundLow: FundLow, fundHigh: FundHigh);
        }
    }

    public static class DepartmentDivisionExtensions
    {
        public static IEnumerable<BudgetaryDepartment> ToBudgetaryDepartments(
            this IEnumerable<DeptDivTitle> deptDivTitles)
        {
            var departments = new List<BudgetaryDepartment>();
            if (deptDivTitles == null)
            {
                return departments;
            }

            BudgetaryDepartment currentDepartment;
            var departmentTitles = deptDivTitles.Where(d => d.Division == "" || d.Division.Replace("0", "") == "");
            foreach (var deptDivTitle in departmentTitles)
            {
                departments.Add(new BudgetaryDepartment()
                {
                    BreakdownCode = deptDivTitle.BreakdownCode.GetValueOrDefault(),
                    CloseoutAccount = deptDivTitle.CloseoutAccount,
                    DepartmentCode = deptDivTitle.Department,
                    Description = deptDivTitle.LongDescription,
                    ShortDescription = deptDivTitle.ShortDescription,
                    Fund =  deptDivTitle.Fund,
                    Id =  deptDivTitle.ID
                });
            }

            var divisionTitles = departmentTitles.Where(d => d.Division.Replace("0", "") != "");
            foreach (var divisionTitle in divisionTitles)
            {
                var department = departments.Where(d => d.DepartmentCode == divisionTitle.Department).FirstOrDefault();
                if (department != null)
                {
                    department.AddDivision(new BudgetaryDivision()
                    {
                        Id = divisionTitle.ID,
                        BreakdownCode = divisionTitle.BreakdownCode.GetValueOrDefault(),
                        CloseoutAccount = divisionTitle.CloseoutAccount,
                        Description = divisionTitle.LongDescription,
                        DivisionCode = divisionTitle.Division,
                        ShortDescription = divisionTitle.ShortDescription
                    });
                }
            }

            return departments;
        }
    }

    public static class BudgetaryFundExtensions
    {
        public static IEnumerable<BudgetaryFund> ToBudgetaryFunds(this IEnumerable<LedgerTitle> ledgerTitles)
        {
            if (ledgerTitles == null)
            {
                return new List<BudgetaryFund>();
            }

            return  ledgerTitles.Where(l => l.Account.Replace("0", "") == "").Select(l => new BudgetaryFund(){AccountsPayableOverrideCashFund = l.ApoverrideCashFund, Id = l.Id, Description = l.LongDescription, CashFund = l.CashFund, ShortDescription = l.ShortDescription, CashReceiptingOverrideCashFund = l.CroverrideCashFund, PayrollOverrideCashFund = l.PyoverrideCashFund, UseDueToDueFrom = l.UseDueToFrom.GetValueOrDefault()});

        }
    }

    internal static class AccountMasterExtensions
    {
        public static IEnumerable<BudgetaryAccount> ToBudgetaryAccounts(this IEnumerable<AccountMaster> accountMasters)
        {
            if (accountMasters == null)
            {
                return new List<BudgetaryAccount>();
            }
            return accountMasters.Select(a => a.ToBudgetaryAccount());
        }

        private static BudgetaryAccount ToBudgetaryAccount(this AccountMaster accountMaster)
        {
            var account = new BudgetaryAccount()
            {
                Account = accountMaster.Account,
                AccountType = accountMaster.AccountType,
                FirstAccountField = accountMaster.FirstAccountField,
                SecondAccountField = accountMaster.SecondAccountField
                
            };
            account.SetGLAccountType(accountMaster.GLAccountType);
            return account;
        }
    }
}