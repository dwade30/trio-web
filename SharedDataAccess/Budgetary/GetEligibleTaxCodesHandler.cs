﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Messaging;

namespace SharedDataAccess.Budgetary
{
    public class GetEligibleTaxCodesHandler : CommandHandler<GetEligibleTaxCodes, List<int>>
    {
        private BudgetaryContext budgetaryContext;

        public GetEligibleTaxCodesHandler(ITrioContextFactory contextFactory)
        {
            this.budgetaryContext = contextFactory.GetBudgetaryContext();
        }

        protected override List<int> Handle(GetEligibleTaxCodes command)
        {
            return budgetaryContext.TaxTitles.Where(x =>
                x.FormName == (command.FormType == TaxFormType.MISC1099 ? "MISC" : "NEC")).Select(y => y.TaxCode ?? 0).ToList();
        }
    }
}
