﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Extensions;
using SharedDataAccess.Budgetary.Configuration;

namespace SharedDataAccess.Budgetary
{
    public class BudgetaryContext : DataContext<BudgetaryContext>, IBudgetaryContext
    {
        #region DbSets
        public DbSet<AccountMaster> AccountMasters { get; set; }
        public DbSet<Adjustment> Adjustments { get; set; }
        public DbSet<APJournal> APJournals { get; set; }
        public DbSet<APjournalArchive> APjournalArchives { get; set; }
        public DbSet<APJournalDetail> APJournalDetails { get; set; }
        public DbSet<APJournalDetailArchive> APJournalDetailArchives { get; set; }
        public DbSet<ArchiveAPAmount> ArchiveAPAmounts { get; set; }
        public DbSet<Audit> Audits { get; set; }
        public DbSet<AuditChange> AuditChanges { get; set; }
        public DbSet<AuditChangesArchive> AuditChangesArchives { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<BankCashAccount> BankCashAccounts { get; set; }
        public DbSet<BankCheckHistory> BankCheckHistories { get; set; }
        public DbSet<Budget> Budgets { get; set; }
        public DbSet<BudgetarySetting> BudgetarySettings { get; set; }
        public DbSet<CalculationNeeded> CalculationNeededs { get; set; }
        public DbSet<CdjournalArchive> CDJournalArchives { get; set; }
        public DbSet<CheckRecArchive> CheckRecArchives { get; set; }
        public DbSet<CheckRecMaster> CheckRecMasters { get; set; }
        public DbSet<ClassCode> ClassCodes { get; set; }
        public DbSet<CreditMemo> CreditMemos { get; set; }
        public DbSet<CreditMemoDetail> CreditMemoDetails { get; set; }
        public DbSet<CustomBudgetReport> CustomBudgetReports { get; set; }
        public DbSet<CustomCheckField> CustomCheckFields { get; set; }
        public DbSet<CustomCheck> CustomChecks { get; set; }
        public DbSet<CustomReport> CustomReports { get; set; }
        public DbSet<CustomReportsDetail> CustomReportsDetails { get; set; }
        public DbSet<Cya> Cyas { get; set; }
        public DbSet<Dbversion> Dbversions { get; set; }
        public DbSet<DefaultInfo> DefaultInfos { get; set; }
        public DbSet<DeptDivTitle> DeptDivTitles { get; set; }
        public DbSet<DoNotCalculate> DoNotCalculates { get; set; }
        public DbSet<EncumbranceDetail> Encumbrance { get; set; }
        public DbSet<Encumbrance> Encumbrances { get; set; }
        public DbSet<EoyAdjustment> Eoyadjustments { get; set; }
        public DbSet<Eoyprocess> Eoyprocesses { get; set; }
        public DbSet<ExpenseDetailFormat> ExpenseDetailFormats { get; set; }
        public DbSet<ExpenseDetailInfo> ExpenseDetailInfos { get; set; }
        public DbSet<ExpenseDetailSummaryInfo> ExpenseDetailSummaryInfos { get; set; }
        public DbSet<ExpenseReportInfo> ExpenseReportInfos { get; set; }
        public DbSet<ExpenseSummaryFormat> ExpenseSummaryFormats { get; set; }
        public DbSet<ExpObjTitle> ExpObjTitles { get; set; }
        public DbSet<FundTitle> FundTitles { get; set; }
        public DbSet<GjdefaultDetail> GjdefaultDetails { get; set; }
        public DbSet<GjdefaultMaster> GjdefaultMasters { get; set; }
        public DbSet<VendorMaster> VendorMasters { get; set; }
		public DbSet<RevTitle> RevTitles { get; set; }
		public DbSet<LedgerTitle> LedgerTitles { get; set; }
        public DbSet<LedgerRange> LedgerRanges { get; set; }
        public DbSet<JournalLock> JournalLocks { get; set; }
        public DbSet<JournalMaster> JournalMasters { get; set; }
        public DbSet<JournalEntry> JournalEntries { get; set; }
        public DbSet<StandardAccount> StandardAccounts { get; set; }
        public DbSet<TaxTitle> TaxTitles { get; set; }
        public DbSet<VendorTaxInfo> VendorTaxInfos { get; set; }
        public DbSet<VendorTaxInfoArchive> VendorTaxInfoArchives { get; set; }


        IQueryable<LedgerTitle> IBudgetaryContext.LedgerTitles { get => LedgerTitles; }
		IQueryable<RevTitle> IBudgetaryContext.RevTitles { get => RevTitles; }
		IQueryable<DeptDivTitle> IBudgetaryContext.DeptDivTitles { get => DeptDivTitles; }
		IQueryable<ExpObjTitle> IBudgetaryContext.ExpObjTitles { get => ExpObjTitles; }
		IQueryable<AccountMaster> IBudgetaryContext.AccountMasters { get => AccountMasters; }
		IQueryable<LedgerRange> IBudgetaryContext.LedgerRanges { get => LedgerRanges; }
		IQueryable<BudgetarySetting> IBudgetaryContext.BudgetarySettings { get => BudgetarySettings; }
		IQueryable<StandardAccount> IBudgetaryContext.StandardAccounts { get => StandardAccounts; }
		IQueryable<JournalLock> IBudgetaryContext.JournalLocks { get => JournalLocks; }
		IQueryable<JournalEntry> IBudgetaryContext.JournalEntries { get => JournalEntries; }
		IQueryable<JournalMaster> IBudgetaryContext.JournalMasters { get => JournalMasters; }
        IQueryable<Adjustment> IBudgetaryContext.Adjustments { get => Adjustments; }
        IQueryable<VendorMaster> IBudgetaryContext.VendorMasters { get => VendorMasters; }
        IQueryable<APJournal> IBudgetaryContext.APJournals { get => APJournals; }
        IQueryable<APjournalArchive> IBudgetaryContext.APJournalArchives { get => APjournalArchives; }
        IQueryable<APJournalDetail> IBudgetaryContext.APJournalDetails { get => APJournalDetails; }
        IQueryable<APJournalDetailArchive> IBudgetaryContext.APJournalDetailArchives { get => APJournalDetailArchives; }
        IQueryable<CdjournalArchive> IBudgetaryContext.CDJournalArchives { get => CDJournalArchives; }
        IQueryable<TaxTitle> IBudgetaryContext.TaxTitles { get => TaxTitles; }
        IQueryable<VendorTaxInfo> IBudgetaryContext.VendorTaxInfos { get => VendorTaxInfos; }
        IQueryable<VendorTaxInfoArchive> IBudgetaryContext.VendorTaxInfoArchives { get => VendorTaxInfoArchives; }

        #endregion



        public BudgetaryContext(DbContextOptions<BudgetaryContext> options) : base(options)
        {

        }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.ApplyConfiguration(new AccountMasterConfiguration());
			modelBuilder.ApplyConfiguration(new AdjustmentConfiguration());
			modelBuilder.ApplyConfiguration(new APJournalConfiguration());
			modelBuilder.ApplyConfiguration(new APJournalArchiveConfiguration());
			modelBuilder.ApplyConfiguration(new APJournalDetailArchiveConfiguration());
			modelBuilder.ApplyConfiguration(new APJournalDetailConfiguration());
			modelBuilder.ApplyConfiguration(new ArchiveAPAmountConfiguration());
			modelBuilder.ApplyConfiguration(new AuditConfiguration());
			modelBuilder.ApplyConfiguration(new AuditChangesConfiguration());
			modelBuilder.ApplyConfiguration(new AuditChangesArchiveConfiguration());
			modelBuilder.ApplyConfiguration(new BankCashAccountsConfiguration());
			modelBuilder.ApplyConfiguration(new BankCheckHistoryConfiguration());
			modelBuilder.ApplyConfiguration(new BankConfiguration());
			modelBuilder.ApplyConfiguration(new BudgetConfiguration());
			modelBuilder.ApplyConfiguration(new BudgetarySettingConfiguration());
			modelBuilder.ApplyConfiguration(new CDJournalArchiveConfiguration());
			modelBuilder.ApplyConfiguration(new CheckRecArchiveConfiguration());
			modelBuilder.ApplyConfiguration(new CheckRecMasterConfiguration());
			modelBuilder.ApplyConfiguration(new ClassCodeConfiguration());
			modelBuilder.ApplyConfiguration(new CreditMemoConfiguration());
			modelBuilder.ApplyConfiguration(new DeptDivTitleConfiguration());
			modelBuilder.ApplyConfiguration(new VendorMasterConfiguration());
			modelBuilder.ApplyConfiguration(new CalculationNeededConfiguration());
			modelBuilder.ApplyConfiguration(new CreditMemoDetailConfiguration());
			modelBuilder.ApplyConfiguration(new CustomBudgetReportConfiguration());
			modelBuilder.ApplyConfiguration(new CustomCheckFieldConfiguration());
			modelBuilder.ApplyConfiguration(new CustomCheckConfiguration());
			modelBuilder.ApplyConfiguration(new CustomReportConfiguration());
			modelBuilder.ApplyConfiguration(new CustomReportsDetailConfiguration());
			modelBuilder.ApplyConfiguration(new CyaConfiguration());
			modelBuilder.ApplyConfiguration(new DbVersionConfiguration());
			modelBuilder.ApplyConfiguration(new DefaultInfoConfiguration());
			modelBuilder.ApplyConfiguration(new DoNotCalculateConfiguration());
			modelBuilder.ApplyConfiguration(new EncumbranceDetailConfiguration());
			modelBuilder.ApplyConfiguration(new EncumbranceConfiguration());
			modelBuilder.ApplyConfiguration(new EoyAdjustmentConfiguration());
			modelBuilder.ApplyConfiguration(new EoyProcessConfiguration());
			modelBuilder.ApplyConfiguration(new ExpObjTitleConfiguration());
			modelBuilder.ApplyConfiguration(new ExpenseDetailFormatConfiguration());
			modelBuilder.ApplyConfiguration(new RevTitleConfiguration());
			modelBuilder.ApplyConfiguration(new LedgerTitleConfiguration());
            modelBuilder.ApplyConfiguration(new TaxTitleConfiguration());
            modelBuilder.ApplyConfiguration(new VendorTaxInfoConfiguration());
            modelBuilder.ApplyConfiguration(new VendorTaxInfoArchiveConfiguration());

            BuildExpenseDetailInfo(modelBuilder);
			BuildExpenseDetailSummaryInfo(modelBuilder);
			BuildExpenseReportInfo(modelBuilder);
			BuildExpenseSummaryFormat(modelBuilder);
			BuildFundTitle(modelBuilder);
			BuildGjdefaultDetail(modelBuilder);
			BuildGjdefaultMaster(modelBuilder);
			BuildGraphItems(modelBuilder);
			BuildItemsInChart(modelBuilder);
			BuildJournalEntries(modelBuilder);
			BuildJournalLock(modelBuilder);
			BuildJournalMaster(modelBuilder);
			BuildLastChecksRun(modelBuilder);
			BuildLedgerDetailFormats(modelBuilder);
			BuildLedgerDetailInfo(modelBuilder);

			modelBuilder.ApplyConfiguration(new LedgerDetailSummaryInfoConfiguration());

			BuildLedgerRanges(modelBuilder);

			BuildLedgerReportInfo(modelBuilder);

			BuildLedgerSummaryFormats(modelBuilder);

            modelBuilder.Entity<TaxTitle>()
                .HasMany(c => c.VendorTaxInfos)
                .WithOne(e => e.TaxTitle)
                .HasForeignKey(f => f.TaxCode)
                .HasPrincipalKey(k => (int)k.TaxCode);

            modelBuilder.Entity<TaxTitle>()
                .HasMany(c => c.VendorTaxInfoArchives)
                .WithOne(e => e.TaxTitle)
                .HasForeignKey(f => f.TaxCode)
                .HasPrincipalKey(k => (int)k.TaxCode);

            modelBuilder.Entity<VendorMaster>()
                .HasMany(c => c.VendorTaxInfos)
                .WithOne(e => e.VendorMaster)
                .HasForeignKey(f => f.VendorNumber)
                .HasPrincipalKey(k => k.VendorNumber);

            modelBuilder.Entity<VendorMaster>()
                .HasMany(c => c.VendorTaxInfoArchives)
                .WithOne(e => e.VendorMaster)
                .HasForeignKey(f => f.VendorNumber)
                .HasPrincipalKey(k => k.VendorNumber);

            modelBuilder.Entity<VendorMaster>()
                .HasMany(c => c.APJournals)
                .WithOne(e => e.VendorMaster)
                .HasForeignKey(f => f.VendorNumber)
                .HasPrincipalKey(k => k.VendorNumber);

            modelBuilder.Entity<VendorMaster>()
                .HasMany(c => c.APJournalArchives)
                .WithOne(e => e.VendorMaster)
                .HasForeignKey(f => f.VendorNumber)
                .HasPrincipalKey(k => k.VendorNumber);

            modelBuilder.Entity<VendorMaster>()
                .HasMany(c => c.CDJournalArchives)
                .WithOne(e => e.VendorMaster)
                .HasForeignKey(f => f.VendorNumber)
                .HasPrincipalKey(k => k.VendorNumber);

            modelBuilder.Entity<VendorMaster>()
                .HasMany(c => c.JournalEntries)
                .WithOne(e => e.VendorMaster)
                .HasForeignKey(f => f.VendorNumber)
                .HasPrincipalKey(k => k.VendorNumber);

            modelBuilder.Entity<VendorMaster>()
                .HasMany(c => c.Adjustments)
                .WithOne(e => e.VendorMaster)
                .HasForeignKey(f => f.VendorNumber)
                .HasPrincipalKey(k => k.VendorNumber);

            modelBuilder.Entity<APJournal>()
                .HasMany(c => c.APJournalDetails)
                .WithOne(e => e.APJournal)
                .HasForeignKey(f => f.APJournalID)
                .HasPrincipalKey(k => k.ID);

            modelBuilder.Entity<APjournalArchive>()
                .HasMany(c => c.APJournalDetails)
                .WithOne(e => e.APJournalArchive)
                .HasForeignKey(f => f.ApjournalArchiveId)
                .HasPrincipalKey(k => k.Id);

            modelBuilder.Entity<LinkedDocuments>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Description).HasMaxLength(255);

				entity.Property(e => e.DocReferenceId).HasColumnName("DocReferenceID");

				entity.Property(e => e.DocReferenceType)
					.HasMaxLength(255)
					.IsUnicode(false);

				entity.Property(e => e.FileName).HasMaxLength(255);
			});

			modelBuilder.Entity<PastBudgets>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Account).HasMaxLength(50);

				entity.Property(e => e.ActualSpent).HasColumnType("money");

				entity.Property(e => e.AprActual).HasColumnType("money");

				entity.Property(e => e.AprBudget).HasColumnType("money");

				entity.Property(e => e.AugActual).HasColumnType("money");

				entity.Property(e => e.AugBudget).HasColumnType("money");

				entity.Property(e => e.BudgetAdjustments).HasColumnType("money");

				entity.Property(e => e.CarryForward).HasColumnType("money");

				entity.Property(e => e.DecActual).HasColumnType("money");

				entity.Property(e => e.DecBudget).HasColumnType("money");

				entity.Property(e => e.EndBudget).HasColumnType("money");

				entity.Property(e => e.FebActual).HasColumnType("money");

				entity.Property(e => e.FebBudget).HasColumnType("money");

				entity.Property(e => e.JanActual).HasColumnType("money");

				entity.Property(e => e.JanBudget).HasColumnType("money");

				entity.Property(e => e.JulyActual).HasColumnType("money");

				entity.Property(e => e.JulyBudget).HasColumnType("money");

				entity.Property(e => e.JuneActual).HasColumnType("money");

				entity.Property(e => e.JuneBudget).HasColumnType("money");

				entity.Property(e => e.MarActual).HasColumnType("money");

				entity.Property(e => e.MarBudget).HasColumnType("money");

				entity.Property(e => e.MayActual).HasColumnType("money");

				entity.Property(e => e.MayBudget).HasColumnType("money");

				entity.Property(e => e.NovActual).HasColumnType("money");

				entity.Property(e => e.NovBudget).HasColumnType("money");

				entity.Property(e => e.OctActual).HasColumnType("money");

				entity.Property(e => e.OctBudget).HasColumnType("money");

				entity.Property(e => e.SeptActual).HasColumnType("money");

				entity.Property(e => e.SeptBudget).HasColumnType("money");

				entity.Property(e => e.StartBudget).HasColumnType("money");

				entity.Property(e => e.Ytdadjustments)
					.HasColumnName("YTDAdjustments")
					.HasColumnType("money");

				entity.Property(e => e.YtdbudgetAdjustments)
					.HasColumnName("YTDBudgetAdjustments")
					.HasColumnType("money");

				entity.Property(e => e.YtdcarryForward)
					.HasColumnName("YTDCarryForward")
					.HasColumnType("money");
			});

			modelBuilder.Entity<Polock>(entity =>
			{
				entity.ToTable("POLock");

				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.NextPo).HasColumnName("NextPO");

				entity.Property(e => e.OpId)
					.HasColumnName("OpID")
					.HasMaxLength(255);
			});

			modelBuilder.Entity<ProjectDetailFormats>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Description).HasMaxLength(255);

				entity.Property(e => e.DescriptionLength).HasMaxLength(255);

				entity.Property(e => e.Font).HasMaxLength(255);

				entity.Property(e => e.FontName).HasMaxLength(255);

				entity.Property(e => e.Printer).HasMaxLength(255);

				entity.Property(e => e.YtddebitCredit).HasColumnName("YTDDebitCredit");

				entity.Property(e => e.Ytdnet).HasColumnName("YTDNet");
			});

			modelBuilder.Entity<ProjectMaster>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Fund).HasMaxLength(255);

				entity.Property(e => e.LongDescription).HasMaxLength(255);

				entity.Property(e => e.ProjectCode).HasMaxLength(255);

				entity.Property(e => e.ProjectedCost).HasColumnType("money");

				entity.Property(e => e.ShortDescription).HasMaxLength(255);
			});

			modelBuilder.Entity<ProjectSummaryFormats>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Description).HasMaxLength(255);

				entity.Property(e => e.DescriptionLength).HasMaxLength(255);

				entity.Property(e => e.Font).HasMaxLength(255);

				entity.Property(e => e.FontName).HasMaxLength(255);

				entity.Property(e => e.Osencumbrance).HasColumnName("OSEncumbrance");

				entity.Property(e => e.Printer).HasMaxLength(255);

				entity.Property(e => e.YtddebitCredit).HasColumnName("YTDDebitCredit");

				entity.Property(e => e.Ytdnet).HasColumnName("YTDNet");
			});

			modelBuilder.Entity<PurchaseOrderDetails>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Account).HasMaxLength(255);

				entity.Property(e => e.Description).HasMaxLength(255);

				entity.Property(e => e.Price).HasColumnType("money");

				entity.Property(e => e.PurchaseOrderId).HasColumnName("PurchaseOrderID");

				entity.Property(e => e.Total).HasColumnType("money");
			});

			modelBuilder.Entity<PurchaseOrders>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Department).HasMaxLength(255);

				entity.Property(e => e.Description).HasMaxLength(255);

				entity.Property(e => e.Po).HasColumnName("PO");

				entity.Property(e => e.Podate)
					.HasColumnName("PODate")
					.HasColumnType("datetime");
			});

			modelBuilder.Entity<ReportTitles>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.BegAccount).HasMaxLength(255);

				entity.Property(e => e.BegDeptExp).HasMaxLength(11);

				entity.Property(e => e.Description).HasMaxLength(25);

				entity.Property(e => e.EndAccount).HasMaxLength(255);

				entity.Property(e => e.EndDeptExp).HasMaxLength(11);

				entity.Property(e => e.LowDetail).HasMaxLength(2);

				entity.Property(e => e.MajorSequence).HasMaxLength(1);

				entity.Property(e => e.SelectedAccounts).HasMaxLength(1);

				entity.Property(e => e.SelectedMonths).HasMaxLength(1);

				entity.Property(e => e.SortOrder).HasMaxLength(255);

				entity.Property(e => e.Totals).HasMaxLength(1);

				entity.Property(e => e.Type).HasMaxLength(255);
			});

			modelBuilder.Entity<Reports>(entity =>
			{

				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.CriteriaId).HasColumnName("CriteriaID");

				entity.Property(e => e.FormatId).HasColumnName("FormatID");

				entity.Property(e => e.Title).HasMaxLength(50);

				entity.Property(e => e.Type).HasMaxLength(255);
			});

			modelBuilder.Entity<Reprint>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Type).HasMaxLength(50);
			});

			modelBuilder.Entity<RevenueDetailFormats>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Description).HasMaxLength(25);

				entity.Property(e => e.DescriptionLength).HasMaxLength(1);

				entity.Property(e => e.Font).HasMaxLength(1);

				entity.Property(e => e.FontName).HasMaxLength(25);

				entity.Property(e => e.PaperWidth).HasMaxLength(1);

				entity.Property(e => e.Printer).HasMaxLength(1);

				entity.Property(e => e.Use).HasMaxLength(1);

				entity.Property(e => e.YtddebitCredit).HasColumnName("YTDDebitCredit");

				entity.Property(e => e.Ytdnet).HasColumnName("YTDNet");
			});

			modelBuilder.Entity<RevenueDetailInfo>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Account).HasMaxLength(50);

				entity.Property(e => e.Adjustments).HasColumnType("money");

				entity.Property(e => e.Amount).HasColumnType("money");

				entity.Property(e => e.CheckNumber).HasMaxLength(50);

				entity.Property(e => e.Department).HasMaxLength(50);

				entity.Property(e => e.Description).HasMaxLength(50);

				entity.Property(e => e.Discount).HasColumnType("money");

				entity.Property(e => e.Division).HasMaxLength(50);

				entity.Property(e => e.Encumbrance).HasColumnType("money");

				entity.Property(e => e.Liquidated).HasColumnType("money");

				entity.Property(e => e.PostedDate).HasColumnType("datetime");

				entity.Property(e => e.Project).HasMaxLength(255);

				entity.Property(e => e.Rcb)
					.HasColumnName("RCB")
					.HasMaxLength(50);

				entity.Property(e => e.Revenue).HasMaxLength(50);

				entity.Property(e => e.Status).HasMaxLength(50);

				entity.Property(e => e.TransDate).HasColumnType("datetime");

				entity.Property(e => e.Type).HasMaxLength(50);
			});

			modelBuilder.Entity<RevenueDetailSummaryInfo>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Account).HasMaxLength(50);

				entity.Property(e => e.BudgetAdjustments).HasColumnType("money");

				entity.Property(e => e.CarryForward).HasColumnType("money");

				entity.Property(e => e.Department).HasMaxLength(50);

				entity.Property(e => e.Division).HasMaxLength(50);

				entity.Property(e => e.EncumbActivity).HasColumnType("money");

				entity.Property(e => e.EncumbranceCredits).HasColumnType("money");

				entity.Property(e => e.EncumbranceDebits).HasColumnType("money");

				entity.Property(e => e.OriginalBudget).HasColumnType("money");

				entity.Property(e => e.PendingCredits).HasColumnType("money");

				entity.Property(e => e.PendingDebits).HasColumnType("money");

				entity.Property(e => e.PostedCredits).HasColumnType("money");

				entity.Property(e => e.PostedDebits).HasColumnType("money");

				entity.Property(e => e.Revenue).HasMaxLength(50);
			});

			modelBuilder.Entity<RevenueRanges>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Department).HasMaxLength(11);

				entity.Property(e => e.Description1).HasMaxLength(20);

				entity.Property(e => e.Description10).HasMaxLength(20);

				entity.Property(e => e.Description2).HasMaxLength(20);

				entity.Property(e => e.Description3).HasMaxLength(20);

				entity.Property(e => e.Description4).HasMaxLength(20);

				entity.Property(e => e.Description5).HasMaxLength(20);

				entity.Property(e => e.Description6).HasMaxLength(20);

				entity.Property(e => e.Description7).HasMaxLength(20);

				entity.Property(e => e.Description8).HasMaxLength(20);

				entity.Property(e => e.Description9).HasMaxLength(20);

				entity.Property(e => e.Division).HasMaxLength(10);

				entity.Property(e => e.Range1).HasMaxLength(11);

				entity.Property(e => e.Range10).HasMaxLength(11);

				entity.Property(e => e.Range10Description1).HasMaxLength(20);

				entity.Property(e => e.Range10Description2).HasMaxLength(20);

				entity.Property(e => e.Range10Description3).HasMaxLength(20);

				entity.Property(e => e.Range10Description4).HasMaxLength(20);

				entity.Property(e => e.Range10Description5).HasMaxLength(20);

				entity.Property(e => e.Range10Sub1).HasMaxLength(11);

				entity.Property(e => e.Range10Sub2).HasMaxLength(11);

				entity.Property(e => e.Range10Sub3).HasMaxLength(11);

				entity.Property(e => e.Range10Sub4).HasMaxLength(11);

				entity.Property(e => e.Range10Sub5).HasMaxLength(11);

				entity.Property(e => e.Range1Description1).HasMaxLength(20);

				entity.Property(e => e.Range1Description2).HasMaxLength(20);

				entity.Property(e => e.Range1Description3).HasMaxLength(20);

				entity.Property(e => e.Range1Description4).HasMaxLength(20);

				entity.Property(e => e.Range1Description5).HasMaxLength(20);

				entity.Property(e => e.Range1Sub1).HasMaxLength(11);

				entity.Property(e => e.Range1Sub2).HasMaxLength(11);

				entity.Property(e => e.Range1Sub3).HasMaxLength(11);

				entity.Property(e => e.Range1Sub4).HasMaxLength(11);

				entity.Property(e => e.Range1Sub5).HasMaxLength(11);

				entity.Property(e => e.Range2).HasMaxLength(11);

				entity.Property(e => e.Range2Description1).HasMaxLength(20);

				entity.Property(e => e.Range2Description2).HasMaxLength(20);

				entity.Property(e => e.Range2Description3).HasMaxLength(20);

				entity.Property(e => e.Range2Description4).HasMaxLength(20);

				entity.Property(e => e.Range2Description5).HasMaxLength(20);

				entity.Property(e => e.Range2Sub1).HasMaxLength(11);

				entity.Property(e => e.Range2Sub2).HasMaxLength(11);

				entity.Property(e => e.Range2Sub3).HasMaxLength(11);

				entity.Property(e => e.Range2Sub4).HasMaxLength(11);

				entity.Property(e => e.Range2Sub5).HasMaxLength(11);

				entity.Property(e => e.Range3).HasMaxLength(11);

				entity.Property(e => e.Range3Description1).HasMaxLength(20);

				entity.Property(e => e.Range3Description2).HasMaxLength(20);

				entity.Property(e => e.Range3Description3).HasMaxLength(20);

				entity.Property(e => e.Range3Description4).HasMaxLength(20);

				entity.Property(e => e.Range3Description5).HasMaxLength(20);

				entity.Property(e => e.Range3Sub1).HasMaxLength(11);

				entity.Property(e => e.Range3Sub2).HasMaxLength(11);

				entity.Property(e => e.Range3Sub3).HasMaxLength(11);

				entity.Property(e => e.Range3Sub4).HasMaxLength(11);

				entity.Property(e => e.Range3Sub5).HasMaxLength(11);

				entity.Property(e => e.Range4).HasMaxLength(11);

				entity.Property(e => e.Range4Description1).HasMaxLength(20);

				entity.Property(e => e.Range4Description2).HasMaxLength(20);

				entity.Property(e => e.Range4Description3).HasMaxLength(20);

				entity.Property(e => e.Range4Description4).HasMaxLength(20);

				entity.Property(e => e.Range4Description5).HasMaxLength(20);

				entity.Property(e => e.Range4Sub1).HasMaxLength(11);

				entity.Property(e => e.Range4Sub2).HasMaxLength(11);

				entity.Property(e => e.Range4Sub3).HasMaxLength(11);

				entity.Property(e => e.Range4Sub4).HasMaxLength(11);

				entity.Property(e => e.Range4Sub5).HasMaxLength(11);

				entity.Property(e => e.Range5).HasMaxLength(11);

				entity.Property(e => e.Range5Description1).HasMaxLength(20);

				entity.Property(e => e.Range5Description2).HasMaxLength(20);

				entity.Property(e => e.Range5Description3).HasMaxLength(20);

				entity.Property(e => e.Range5Description4).HasMaxLength(20);

				entity.Property(e => e.Range5Description5).HasMaxLength(20);

				entity.Property(e => e.Range5Sub1).HasMaxLength(11);

				entity.Property(e => e.Range5Sub2).HasMaxLength(11);

				entity.Property(e => e.Range5Sub3).HasMaxLength(11);

				entity.Property(e => e.Range5Sub4).HasMaxLength(11);

				entity.Property(e => e.Range5Sub5).HasMaxLength(11);

				entity.Property(e => e.Range6).HasMaxLength(11);

				entity.Property(e => e.Range6Description1).HasMaxLength(20);

				entity.Property(e => e.Range6Description2).HasMaxLength(20);

				entity.Property(e => e.Range6Description3).HasMaxLength(20);

				entity.Property(e => e.Range6Description4).HasMaxLength(20);

				entity.Property(e => e.Range6Description5).HasMaxLength(20);

				entity.Property(e => e.Range6Sub1).HasMaxLength(11);

				entity.Property(e => e.Range6Sub2).HasMaxLength(11);

				entity.Property(e => e.Range6Sub3).HasMaxLength(11);

				entity.Property(e => e.Range6Sub4).HasMaxLength(11);

				entity.Property(e => e.Range6Sub5).HasMaxLength(11);

				entity.Property(e => e.Range7).HasMaxLength(11);

				entity.Property(e => e.Range7Description1).HasMaxLength(20);

				entity.Property(e => e.Range7Description2).HasMaxLength(20);

				entity.Property(e => e.Range7Description3).HasMaxLength(20);

				entity.Property(e => e.Range7Description4).HasMaxLength(20);

				entity.Property(e => e.Range7Description5).HasMaxLength(20);

				entity.Property(e => e.Range7Sub1).HasMaxLength(11);

				entity.Property(e => e.Range7Sub2).HasMaxLength(11);

				entity.Property(e => e.Range7Sub3).HasMaxLength(11);

				entity.Property(e => e.Range7Sub4).HasMaxLength(11);

				entity.Property(e => e.Range7Sub5).HasMaxLength(11);

				entity.Property(e => e.Range8).HasMaxLength(11);

				entity.Property(e => e.Range8Description1).HasMaxLength(20);

				entity.Property(e => e.Range8Description2).HasMaxLength(20);

				entity.Property(e => e.Range8Description3).HasMaxLength(20);

				entity.Property(e => e.Range8Description4).HasMaxLength(20);

				entity.Property(e => e.Range8Description5).HasMaxLength(20);

				entity.Property(e => e.Range8Sub1).HasMaxLength(11);

				entity.Property(e => e.Range8Sub2).HasMaxLength(11);

				entity.Property(e => e.Range8Sub3).HasMaxLength(11);

				entity.Property(e => e.Range8Sub4).HasMaxLength(11);

				entity.Property(e => e.Range8Sub5).HasMaxLength(11);

				entity.Property(e => e.Range9).HasMaxLength(11);

				entity.Property(e => e.Range9Description1).HasMaxLength(20);

				entity.Property(e => e.Range9Description2).HasMaxLength(20);

				entity.Property(e => e.Range9Description3).HasMaxLength(20);

				entity.Property(e => e.Range9Description4).HasMaxLength(20);

				entity.Property(e => e.Range9Description5).HasMaxLength(20);

				entity.Property(e => e.Range9Sub1).HasMaxLength(11);

				entity.Property(e => e.Range9Sub2).HasMaxLength(11);

				entity.Property(e => e.Range9Sub3).HasMaxLength(11);

				entity.Property(e => e.Range9Sub4).HasMaxLength(11);

				entity.Property(e => e.Range9Sub5).HasMaxLength(11);
			});

			modelBuilder.Entity<RevenueReportInfo>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Account).HasMaxLength(50);

				entity.Property(e => e.AutomaticBudgetAdjustments).HasColumnType("money");

				entity.Property(e => e.BudgetAdjustments).HasColumnType("money");

				entity.Property(e => e.CarryForward).HasColumnType("money");

				entity.Property(e => e.Department).HasMaxLength(50);

				entity.Property(e => e.Division).HasMaxLength(50);

				entity.Property(e => e.EncumbActivity).HasColumnType("money");

				entity.Property(e => e.EncumbranceCredits).HasColumnType("money");

				entity.Property(e => e.EncumbranceDebits).HasColumnType("money");

				entity.Property(e => e.MonthlyBudget).HasColumnType("money");

				entity.Property(e => e.MonthlyBudgetAdjustments).HasColumnType("money");

				entity.Property(e => e.OriginalBudget).HasColumnType("money");

				entity.Property(e => e.PendingCredits).HasColumnType("money");

				entity.Property(e => e.PendingDebits).HasColumnType("money");

				entity.Property(e => e.PendingEncumbActivity).HasColumnType("money");

				entity.Property(e => e.PendingEncumbranceDebits).HasColumnType("money");

				entity.Property(e => e.PostedCredits).HasColumnType("money");

				entity.Property(e => e.PostedDebits).HasColumnType("money");

				entity.Property(e => e.Project).HasMaxLength(255);

				entity.Property(e => e.Revenue).HasMaxLength(50);
			});

			modelBuilder.Entity<RevenueSummaryFormats>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Description).HasMaxLength(25);

				entity.Property(e => e.DescriptionLength).HasMaxLength(1);

				entity.Property(e => e.Font).HasMaxLength(1);

				entity.Property(e => e.FontName).HasMaxLength(25);

				entity.Property(e => e.Mtdbudget).HasColumnName("MTDBudget");

				entity.Property(e => e.Osencumbrance).HasColumnName("OSEncumbrance");

				entity.Property(e => e.PaperWidth).HasMaxLength(1);

				entity.Property(e => e.Printer).HasMaxLength(1);

				entity.Property(e => e.Use).HasMaxLength(1);

				entity.Property(e => e.YtddebitCredit).HasColumnName("YTDDebitCredit");

				entity.Property(e => e.Ytdnet).HasColumnName("YTDNet");
			});



			modelBuilder.Entity<StandardAccount>(entity =>
			{
				entity.ToTable("StandardAccounts");
                entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Account).HasMaxLength(255);

				entity.Property(e => e.Code).HasMaxLength(50);

				entity.Property(e => e.LongDescription).HasMaxLength(30);

				entity.Property(e => e.ShortDescription).HasMaxLength(12);

				entity.Property(e => e.Type).HasMaxLength(255);
			});

			modelBuilder.Entity<TaxCommitment>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.ActivityDate).HasColumnType("datetime");

				entity.Property(e => e.Pp)
					.HasColumnName("PP")
					.HasColumnType("money");

				entity.Property(e => e.Re)
					.HasColumnName("RE")
					.HasColumnType("money");

				entity.Property(e => e.TaxType).HasMaxLength(50);
			});

			modelBuilder.Entity<TaxTitle>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.TaxDescription).HasMaxLength(50);
			});

			modelBuilder.Entity<TblCustomReports>(entity =>
			{
				entity.ToTable("tblCustomReports");

				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.LastUpdated).HasColumnType("datetime");

				entity.Property(e => e.ReportName).HasMaxLength(255);

				entity.Property(e => e.Sql).HasColumnName("SQL");

				entity.Property(e => e.Type).HasMaxLength(255);
			});

			modelBuilder.Entity<TblReportLayout>(entity =>
			{
				entity.ToTable("tblReportLayout");

				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.ColumnId).HasColumnName("ColumnID");

				entity.Property(e => e.DisplayText).HasMaxLength(255);

				entity.Property(e => e.FieldId).HasColumnName("FieldID");

				entity.Property(e => e.LastUpdated).HasColumnType("datetime");

				entity.Property(e => e.ReportId).HasColumnName("ReportID");

				entity.Property(e => e.RowId).HasColumnName("RowID");
			});

			modelBuilder.Entity<Temp>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Account).HasMaxLength(50);

				entity.Property(e => e.AccountType).HasMaxLength(1);

				entity.Property(e => e.FifthAccountField).HasMaxLength(255);

				entity.Property(e => e.FirstAccountField).HasMaxLength(12);

				entity.Property(e => e.FourthAccountField).HasMaxLength(12);

				entity.Property(e => e.GlaccountType)
					.HasColumnName("GLAccountType")
					.HasMaxLength(255);

				entity.Property(e => e.SecondAccountField).HasMaxLength(12);

				entity.Property(e => e.ThirdAccountField).HasMaxLength(12);
			});

			modelBuilder.Entity<TempCheckFile>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Amount).HasColumnType("money");

				entity.Property(e => e.CheckAddress1).HasMaxLength(50);

				entity.Property(e => e.CheckAddress2).HasMaxLength(50);

				entity.Property(e => e.CheckAddress3).HasMaxLength(50);

				entity.Property(e => e.CheckAddress4).HasMaxLength(50);

				entity.Property(e => e.CheckCity).HasMaxLength(255);

				entity.Property(e => e.CheckDate).HasColumnType("datetime");

				entity.Property(e => e.CheckState).HasMaxLength(255);

				entity.Property(e => e.CheckZip).HasMaxLength(255);

				entity.Property(e => e.CheckZip4).HasMaxLength(255);

				entity.Property(e => e.Eftcheck).HasColumnName("EFTCheck");

				entity.Property(e => e.OldCheckNumber).HasMaxLength(255);

				entity.Property(e => e.Status).HasMaxLength(50);

				entity.Property(e => e.VendorName).HasMaxLength(50);
			});

			modelBuilder.Entity<TempJournalEntries>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Account).HasMaxLength(255);

				entity.Property(e => e.Amount).HasColumnType("money");

				entity.Property(e => e.CheckNumber).HasMaxLength(6);

				entity.Property(e => e.Description).HasMaxLength(25);

				entity.Property(e => e.Po)
					.HasColumnName("PO")
					.HasMaxLength(15);

				entity.Property(e => e.PostedDate).HasColumnType("datetime");

				entity.Property(e => e.Project).HasMaxLength(4);

				entity.Property(e => e.Rcb)
					.HasColumnName("RCB")
					.HasMaxLength(1);

				entity.Property(e => e.Status).HasMaxLength(1);

				entity.Property(e => e.TempJournalEntriesDate).HasColumnType("datetime");

				entity.Property(e => e.Type).HasMaxLength(2);

				entity.Property(e => e._1099)
					.HasColumnName("1099")
					.HasMaxLength(50);
			});

			modelBuilder.Entity<VendorClass>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.Class).HasMaxLength(50);
			});

			

			modelBuilder.Entity<WarrantLock>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.OpId)
					.HasColumnName("OpID")
					.HasMaxLength(50);
			});

			modelBuilder.Entity<WarrantMaster>(entity =>
			{
				entity.Property(e => e.Id).HasColumnName("ID");

				entity.Property(e => e.WarrantDate).HasColumnType("datetime");

				entity.Property(e => e.WarrantOperator).HasMaxLength(50);

				entity.Property(e => e.WarrantTime).HasColumnType("datetime");
			});

			modelBuilder.Entity<WarrantMessage>(entity => { entity.Property(e => e.Id).HasColumnName("ID"); });
		}

		private static void BuildLedgerSummaryFormats(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LedgerSummaryFormats>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Description)
                      .HasMaxLength(25);

                entity.Property(e => e.DescriptionLength)
                      .HasMaxLength(1);

                entity.Property(e => e.Font)
                      .HasMaxLength(1);

                entity.Property(e => e.FontName)
                      .HasMaxLength(25);

                entity.Property(e => e.Osencumbrance)
                      .HasColumnName("OSEncumbrance");

                entity.Property(e => e.PaperWidth)
                      .HasMaxLength(1);

                entity.Property(e => e.Printer)
                      .HasMaxLength(1);

                entity.Property(e => e.Use)
                      .HasMaxLength(1);

                entity.Property(e => e.YtddebitCredit)
                      .HasColumnName("YTDDebitCredit");

                entity.Property(e => e.Ytdnet)
                      .HasColumnName("YTDNet");
            });
        }

        private static void BuildLedgerReportInfo(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LedgerReportInfo>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Account)
                      .HasMaxLength(50);

                entity.Property(e => e.AutomaticBudgetAdjustments)
                      .HasColumnType("money");

                entity.Property(e => e.BudgetAdjustments)
                      .HasColumnType("money");

                entity.Property(e => e.CarryForward)
                      .HasColumnType("money");

                entity.Property(e => e.EncumbActivity)
                      .HasColumnType("money");

                entity.Property(e => e.EncumbranceCredits)
                      .HasColumnType("money");

                entity.Property(e => e.EncumbranceDebits)
                      .HasColumnType("money");

                entity.Property(e => e.Fund)
                      .HasMaxLength(50);

                entity.Property(e => e.LedgerAccount)
                      .HasMaxLength(50);

                entity.Property(e => e.OriginalBudget)
                      .HasColumnType("money");

                entity.Property(e => e.PendingCredits)
                      .HasColumnType("money");

                entity.Property(e => e.PendingDebits)
                      .HasColumnType("money");

                entity.Property(e => e.PendingEncumbActivity)
                      .HasColumnType("money");

                entity.Property(e => e.PendingEncumbranceDebits)
                      .HasColumnType("money");

                entity.Property(e => e.PostedCredits)
                      .HasColumnType("money");

                entity.Property(e => e.PostedDebits)
                      .HasColumnType("money");

                entity.Property(e => e.Project)
                      .HasMaxLength(255);

                entity.Property(e => e.Suffix)
                      .HasMaxLength(50);
            });
        }

        private static void BuildLedgerRanges(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LedgerRange>(entity =>
            {
                entity.ToTable("LedgerRanges");
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.High)
                      .HasMaxLength(11);

                entity.Property(e => e.High1)
                      .HasMaxLength(11);

                entity.Property(e => e.High11)
                      .HasMaxLength(11);

                entity.Property(e => e.High12)
                      .HasMaxLength(11);

                entity.Property(e => e.High13)
                      .HasMaxLength(11);

                entity.Property(e => e.High2)
                      .HasMaxLength(11);

                entity.Property(e => e.High21)
                      .HasMaxLength(11);

                entity.Property(e => e.High22)
                      .HasMaxLength(11);

                entity.Property(e => e.High23)
                      .HasMaxLength(11);

                entity.Property(e => e.High3)
                      .HasMaxLength(11);

                entity.Property(e => e.High31)
                      .HasMaxLength(11);

                entity.Property(e => e.High32)
                      .HasMaxLength(11);

                entity.Property(e => e.High33)
                      .HasMaxLength(11);

                entity.Property(e => e.High4)
                      .HasMaxLength(11);

                entity.Property(e => e.High41)
                      .HasMaxLength(11);

                entity.Property(e => e.High42)
                      .HasMaxLength(11);

                entity.Property(e => e.High43)
                      .HasMaxLength(11);

                entity.Property(e => e.Low)
                      .HasMaxLength(11);

                entity.Property(e => e.Low1)
                      .HasMaxLength(11);

                entity.Property(e => e.Low11)
                      .HasMaxLength(11);

                entity.Property(e => e.Low12)
                      .HasMaxLength(11);

                entity.Property(e => e.Low13)
                      .HasMaxLength(11);

                entity.Property(e => e.Low2)
                      .HasMaxLength(11);

                entity.Property(e => e.Low21)
                      .HasMaxLength(11);

                entity.Property(e => e.Low22)
                      .HasMaxLength(11);

                entity.Property(e => e.Low23)
                      .HasMaxLength(11);

                entity.Property(e => e.Low3)
                      .HasMaxLength(11);

                entity.Property(e => e.Low31)
                      .HasMaxLength(11);

                entity.Property(e => e.Low32)
                      .HasMaxLength(11);

                entity.Property(e => e.Low33)
                      .HasMaxLength(11);

                entity.Property(e => e.Low4)
                      .HasMaxLength(11);

                entity.Property(e => e.Low41)
                      .HasMaxLength(11);

                entity.Property(e => e.Low42)
                      .HasMaxLength(11);

                entity.Property(e => e.Low43)
                      .HasMaxLength(11);

                entity.Property(e => e.Name1)
                      .HasMaxLength(15);

                entity.Property(e => e.Name11)
                      .HasMaxLength(15);

                entity.Property(e => e.Name12)
                      .HasMaxLength(15);

                entity.Property(e => e.Name13)
                      .HasMaxLength(15);

                entity.Property(e => e.Name2)
                      .HasMaxLength(15);

                entity.Property(e => e.Name21)
                      .HasMaxLength(15);

                entity.Property(e => e.Name22)
                      .HasMaxLength(15);

                entity.Property(e => e.Name23)
                      .HasMaxLength(15);

                entity.Property(e => e.Name3)
                      .HasMaxLength(15);

                entity.Property(e => e.Name31)
                      .HasMaxLength(15);

                entity.Property(e => e.Name32)
                      .HasMaxLength(15);

                entity.Property(e => e.Name33)
                      .HasMaxLength(15);

                entity.Property(e => e.Name4)
                      .HasMaxLength(15);

                entity.Property(e => e.Name41)
                      .HasMaxLength(15);

                entity.Property(e => e.Name42)
                      .HasMaxLength(15);

                entity.Property(e => e.Name43)
                      .HasMaxLength(15);

                entity.Property(e => e.RecordName)
                      .HasMaxLength(1);
            });
        }

        private static void BuildLedgerDetailInfo(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LedgerDetailInfo>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Account)
                      .HasMaxLength(50);

                entity.Property(e => e.Acct)
                      .HasMaxLength(50);

                entity.Property(e => e.Adjustments)
                      .HasColumnType("money");

                entity.Property(e => e.Amount)
                      .HasColumnType("money");

                entity.Property(e => e.CheckNumber)
                      .HasMaxLength(50);

                entity.Property(e => e.Description)
                      .HasMaxLength(50);

                entity.Property(e => e.Discount)
                      .HasColumnType("money");

                entity.Property(e => e.Encumbrance)
                      .HasColumnType("money");

                entity.Property(e => e.Fund)
                      .HasMaxLength(50);

                entity.Property(e => e.Liquidated)
                      .HasColumnType("money");

                entity.Property(e => e.PostedDate)
                      .HasColumnType("datetime");

                entity.Property(e => e.Project)
                      .HasMaxLength(255);

                entity.Property(e => e.Rcb)
                      .HasColumnName("RCB")
                      .HasMaxLength(50);

                entity.Property(e => e.Status)
                      .HasMaxLength(50);

                entity.Property(e => e.Suffix)
                      .HasMaxLength(50);

                entity.Property(e => e.TransDate)
                      .HasColumnType("datetime");

                entity.Property(e => e.Type)
                      .HasMaxLength(50);
            });
        }

        private static void BuildLedgerDetailFormats(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LedgerDetailFormats>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Description)
                      .HasMaxLength(25);

                entity.Property(e => e.DescriptionLength)
                      .HasMaxLength(1);

                entity.Property(e => e.Font)
                      .HasMaxLength(1);

                entity.Property(e => e.FontName)
                      .HasMaxLength(25);

                entity.Property(e => e.PaperWidth)
                      .HasMaxLength(1);

                entity.Property(e => e.Printer)
                      .HasMaxLength(1);

                entity.Property(e => e.Use)
                      .HasMaxLength(1);

                entity.Property(e => e.YtddebitCredit)
                      .HasColumnName("YTDDebitCredit");

                entity.Property(e => e.Ytdnet)
                      .HasColumnName("YTDNet");
            });
        }

        private static void BuildLastChecksRun(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LastChecksRun>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");
            });
        }

        private static void BuildJournalMaster(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JournalMaster>(entity =>
            {
	            entity.ToTable("JournalMaster");
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.CheckDate)
                      .HasColumnType("datetime");

                entity.Property(e => e.CrperiodCloseoutKey)
                      .HasColumnName("CRPeriodCloseoutKey");

                entity.Property(e => e.Description)
                      .HasMaxLength(255);

                entity.Property(e => e.OobpostingDate)
                      .HasColumnName("OOBPostingDate")
                      .HasColumnType("datetime");

                entity.Property(e => e.OobpostingOpId)
                      .HasColumnName("OOBPostingOpID")
                      .HasMaxLength(50);

                entity.Property(e => e.OobpostingTime)
                      .HasColumnName("OOBPostingTime")
                      .HasColumnType("datetime");

                entity.Property(e => e.PostedOob)
                      .HasColumnName("PostedOOB");

                entity.Property(e => e.Status)
                      .HasMaxLength(1);

                entity.Property(e => e.StatusChangeDate)
                      .HasColumnType("datetime");

                entity.Property(e => e.StatusChangeOpId)
                      .HasColumnName("StatusChangeOpID")
                      .HasMaxLength(50);

                entity.Property(e => e.StatusChangeTime)
                      .HasColumnType("datetime");

                entity.Property(e => e.TotalAmount)
                      .HasColumnType("money");

                entity.Property(e => e.TotalCreditMemo)
                      .HasColumnType("money");

                entity.Property(e => e.Type)
                      .HasMaxLength(2);
            });
        }

        private static void BuildJournalLock(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JournalLock>(entity =>
            {
	            entity.ToTable("JournalLock");
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.OpId)
                      .HasColumnName("OpID")
                      .HasMaxLength(50);
            });
        }

        private static void BuildJournalEntries(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JournalEntry>(entity =>
            {
	            entity.ToTable("JournalEntries");
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Account)
                      .HasMaxLength(255);

                entity.Property(e => e.Amount)
                      .HasColumnType("money");

                entity.Property(e => e.CheckNumber)
                      .HasMaxLength(6);

                entity.Property(e => e.Description)
                      .HasMaxLength(255);

                entity.Property(e => e.JournalEntriesDate)
                      .HasColumnType("datetime");

                entity.Property(e => e.Po)
                      .HasColumnName("PO")
                      .HasMaxLength(15);

                entity.Property(e => e.PostedDate)
                      .HasColumnType("datetime");

                entity.Property(e => e.Project)
                      .HasMaxLength(4);

                entity.Property(e => e.Rcb)
                      .HasColumnName("RCB")
                      .HasMaxLength(1);

                entity.Property(e => e.Status)
                      .HasMaxLength(1);

                entity.Property(e => e.Type)
                      .HasMaxLength(2);

                entity.Property(e => e._1099)
                      .HasColumnName("1099")
                      .HasMaxLength(50);
            });
        }

        private static void BuildItemsInChart(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ItemsInChart>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.GraphItemId)
                      .HasColumnName("GraphItemID");
            });
        }

        private static void BuildGraphItems(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GraphItems>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.AccountSelectionType)
                      .HasMaxLength(50);

                entity.Property(e => e.CashChartId)
                      .HasColumnName("CashChartID");

                entity.Property(e => e.High)
                      .HasMaxLength(50);

                entity.Property(e => e.Low)
                      .HasMaxLength(50);

                entity.Property(e => e.Title)
                      .HasMaxLength(50);

                entity.Property(e => e.Type)
                      .HasMaxLength(50);
            });
        }

        private static void BuildGjdefaultMaster(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GjdefaultMaster>(entity =>
            {
                entity.ToTable("GJDefaultMaster");

                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Description)
                      .HasMaxLength(255);
            });
        }

        private static void BuildGjdefaultDetail(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GjdefaultDetail>(entity =>
            {
                entity.ToTable("GJDefaultDetail");

                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Account)
                      .HasMaxLength(255);

                entity.Property(e => e.Amount)
                      .HasColumnType("money");

                entity.Property(e => e.Description)
                      .HasMaxLength(255);

                entity.Property(e => e.GjdefaultMasterId)
                      .HasColumnName("GJDefaultMasterID");

                entity.Property(e => e.Project)
                      .HasMaxLength(255);

                entity.Property(e => e.Rcb)
                      .HasColumnName("RCB")
                      .HasMaxLength(255);
            });
        }

        private static void BuildFundTitle(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FundTitle>(entity =>
            {
                entity.ToTable("FundTitles");
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Abbreviation)
                      .HasMaxLength(50);

                entity.Property(e => e.LongDescription)
                      .HasMaxLength(50);

                entity.Property(e => e.ShortDescription)
                      .HasMaxLength(50);
            });
        }

        private static void BuildExpenseSummaryFormat(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExpenseSummaryFormat>(entity =>
            {
                entity.ToTable("ExpenseSummaryFormats");
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Description)
                      .HasMaxLength(25);

                entity.Property(e => e.DescriptionLength)
                      .HasMaxLength(1);

                entity.Property(e => e.Font)
                      .HasMaxLength(1);

                entity.Property(e => e.FontName)
                      .HasMaxLength(25);

                entity.Property(e => e.Mtdbudget)
                      .HasColumnName("MTDBudget");

                entity.Property(e => e.Osencumbrance)
                      .HasColumnName("OSEncumbrance");

                entity.Property(e => e.PaperWidth)
                      .HasMaxLength(1);

                entity.Property(e => e.Printer)
                      .HasMaxLength(1);

                entity.Property(e => e.Use)
                      .HasMaxLength(1);

                entity.Property(e => e.YtddebitCredit)
                      .HasColumnName("YTDDebitCredit");

                entity.Property(e => e.Ytdnet)
                      .HasColumnName("YTDNet");
            });
        }

        private static void BuildExpenseReportInfo(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExpenseReportInfo>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Account)
                      .HasMaxLength(50);

                entity.Property(e => e.AutomaticBudgetAdjustments)
                      .HasColumnType("money");

                entity.Property(e => e.BudgetAdjustments)
                      .HasColumnType("money");

                entity.Property(e => e.CarryForward)
                      .HasColumnType("money");

                entity.Property(e => e.Department)
                      .HasMaxLength(50);

                entity.Property(e => e.Division)
                      .HasMaxLength(50);

                entity.Property(e => e.EncumbActivity)
                      .HasColumnType("money");

                entity.Property(e => e.EncumbranceCredits)
                      .HasColumnType("money");

                entity.Property(e => e.EncumbranceDebits)
                      .HasColumnType("money");

                entity.Property(e => e.Expense)
                      .HasMaxLength(50);

                entity.Property(e => e.MonthlyBudget)
                      .HasColumnType("money");

                entity.Property(e => e.MonthlyBudgetAdjustments)
                      .HasColumnType("money");

                entity.Property(e => e.Object)
                      .HasMaxLength(50);

                entity.Property(e => e.OriginalBudget)
                      .HasColumnType("money");

                entity.Property(e => e.PendingCredits)
                      .HasColumnType("money");

                entity.Property(e => e.PendingDebits)
                      .HasColumnType("money");

                entity.Property(e => e.PendingEncumbActivity)
                      .HasColumnType("money");

                entity.Property(e => e.PendingEncumbranceDebits)
                      .HasColumnType("money");

                entity.Property(e => e.PostedCredits)
                      .HasColumnType("money");

                entity.Property(e => e.PostedDebits)
                      .HasColumnType("money");

                entity.Property(e => e.Project)
                      .HasMaxLength(255);
            });
        }

        private static void BuildExpenseDetailSummaryInfo(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExpenseDetailSummaryInfo>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Account)
                      .HasMaxLength(50);

                entity.Property(e => e.BudgetAdjustments)
                      .HasColumnType("money");

                entity.Property(e => e.CarryForward)
                      .HasColumnType("money");

                entity.Property(e => e.Department)
                      .HasMaxLength(50);

                entity.Property(e => e.Division)
                      .HasMaxLength(50);

                entity.Property(e => e.EncumbActivity)
                      .HasColumnType("money");

                entity.Property(e => e.EncumbranceCredits)
                      .HasColumnType("money");

                entity.Property(e => e.EncumbranceDebits)
                      .HasColumnType("money");

                entity.Property(e => e.Expense)
                      .HasMaxLength(50);

                entity.Property(e => e.Object)
                      .HasMaxLength(50);

                entity.Property(e => e.OriginalBudget)
                      .HasColumnType("money");

                entity.Property(e => e.PendingCredits)
                      .HasColumnType("money");

                entity.Property(e => e.PendingDebits)
                      .HasColumnType("money");

                entity.Property(e => e.PostedCredits)
                      .HasColumnType("money");

                entity.Property(e => e.PostedDebits)
                      .HasColumnType("money");
            });
        }

        private static void BuildExpenseDetailInfo(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExpenseDetailInfo>(entity =>
            {
                entity.Property(e => e.Id)
                      .HasColumnName("ID");

                entity.Property(e => e.Account)
                      .HasMaxLength(50);

                entity.Property(e => e.Adjustments)
                      .HasColumnType("money");

                entity.Property(e => e.Amount)
                      .HasColumnType("money");

                entity.Property(e => e.CheckNumber)
                      .HasMaxLength(50);

                entity.Property(e => e.Department)
                      .HasMaxLength(50);

                entity.Property(e => e.Description)
                      .HasMaxLength(50);

                entity.Property(e => e.Discount)
                      .HasColumnType("money");

                entity.Property(e => e.Division)
                      .HasMaxLength(50);

                entity.Property(e => e.Encumbrance)
                      .HasColumnType("money");

                entity.Property(e => e.Expense)
                      .HasMaxLength(50);

                entity.Property(e => e.Liquidated)
                      .HasColumnType("money");

                entity.Property(e => e.Object)
                      .HasMaxLength(50);

                entity.Property(e => e.PostedDate)
                      .HasColumnType("datetime");

                entity.Property(e => e.Project)
                      .HasMaxLength(255);

                entity.Property(e => e.Rcb)
                      .HasColumnName("RCB")
                      .HasMaxLength(50);

                entity.Property(e => e.Status)
                      .HasMaxLength(50);

                entity.Property(e => e.TransDate)
                      .HasColumnType("datetime");

                entity.Property(e => e.Type)
                      .HasMaxLength(50);
            });
        }
    }
}
