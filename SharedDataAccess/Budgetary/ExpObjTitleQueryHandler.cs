﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
	public class ExpObjTitleQueryHandler : IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>
	{
		private IBudgetaryContext budgetaryContext;

		public ExpObjTitleQueryHandler(ITrioContextFactory contextFactory)
		{
			this.budgetaryContext = contextFactory.GetBudgetaryContext();
		}

		public IEnumerable<ExpObjTitle> ExecuteQuery(ExpObjTitleSearchCriteria query)
		{
			IQueryable<ExpObjTitle> expObjTitleQuery;

			expObjTitleQuery = budgetaryContext.ExpObjTitles;

			if (query.Expense != "")
			{
				expObjTitleQuery = expObjTitleQuery.Where(x => x.Expense == query.Expense);
			}

			if (query.Object != "")
			{
				expObjTitleQuery = expObjTitleQuery.Where(x => x.Object == query.Object);
			}

			return expObjTitleQuery.ToList();
		}
	}
}
