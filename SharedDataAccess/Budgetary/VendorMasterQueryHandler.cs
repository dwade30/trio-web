﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
    public class VendorMasterQueryHandler : IQueryHandler<VendorMasterSearchCriteria, IEnumerable<VendorMaster>>
    {
        private ITrioContextFactory contextFactory;

        public VendorMasterQueryHandler(ITrioContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        public IEnumerable<VendorMaster> ExecuteQuery(VendorMasterSearchCriteria query)
        {

            var budgetaryContext = contextFactory.GetBudgetaryContext();
            
            IQueryable<VendorMaster> vendorMasterQuery;

            vendorMasterQuery = budgetaryContext.VendorMasters;

            if (query.VendorNumber != 0)
            {
                vendorMasterQuery = vendorMasterQuery.Where(x => x.VendorNumber == query.VendorNumber);
            }

            if (query.VendorNumbers.Count > 0)
            {
                vendorMasterQuery = vendorMasterQuery.Where(x => query.VendorNumbers.Contains(x.VendorNumber ?? 0));
            }

            return vendorMasterQuery.ToList();
        }
    }
}
