﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Extensions;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.Budgetary
{
	public class CreateJournalHandler : CommandHandler<CreateJournal, int>
	{
		private BudgetaryContext budgetaryContext;
		public CreateJournalHandler(ITrioContextFactory contextFactory)
		{
			this.budgetaryContext = contextFactory.GetBudgetaryContext();
		}

		protected override int Handle(CreateJournal command)
		{
			try
			{
				if (!LockJournal())
				{
					return 0;
				}
				else
				{
					var lastJournal = budgetaryContext.JournalMasters.OrderByDescending(x => x.JournalNumber)
						.FirstOrDefault();
					int journalNumber = 0;

					if (lastJournal != null)
					{
						journalNumber = (lastJournal.JournalNumber ?? 0) + 1;
					}
					else
					{
						journalNumber = 1;
					}

					budgetaryContext.JournalMasters.Add(new JournalMaster
					{
						JournalNumber = journalNumber,
						Status = "E",
						StatusChangeDate = DateTime.Today,
						Description = command.Description,
						Type = command.JournalType.ToJournalTypeString(),
						Period = command.Period
					});
					budgetaryContext.SaveChanges();

					UnlockJournal();
					return journalNumber;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return 0;
			}
		}

		private bool LockJournal()
		{
			bool LockJournal = false;

			var lockFile = budgetaryContext.JournalLocks.FirstOrDefault();

			if (lockFile != null)
			{
				if (!lockFile.MasterLock ?? false)
				{
					lockFile.MasterLock = true;
					lockFile.OpId = "PAYPORT";
					budgetaryContext.SaveChanges();
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				budgetaryContext.JournalLocks.Add(new JournalLock
				{
					MasterLock = true,
					OpId = "PAYPORT"
				});
				budgetaryContext.SaveChanges();
				return true;
			}
		}

		private void UnlockJournal()
		{
			var lockFile = budgetaryContext.JournalLocks.FirstOrDefault();

			if (lockFile != null)
			{
				lockFile.MasterLock = false;
				lockFile.OpId = "";
			}

			budgetaryContext.SaveChanges();
		}
	}
}
