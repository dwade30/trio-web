﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.Budgetary
{
	public class GetBudgetarySettingsHandler : CommandHandler<GetBudgetarySettings, BudgetarySetting>
	{
		private IBudgetaryContext budgetaryContext;
		public GetBudgetarySettingsHandler(ITrioContextFactory contextFactory)
		{
			this.budgetaryContext = contextFactory.GetBudgetaryContext();
		}

		protected override BudgetarySetting Handle(GetBudgetarySettings command)
		{
			try
			{
				var bdSetting = budgetaryContext.BudgetarySettings.FirstOrDefault();
				if (bdSetting != null)
				{
					return bdSetting;
				}
				return new BudgetarySetting();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}
