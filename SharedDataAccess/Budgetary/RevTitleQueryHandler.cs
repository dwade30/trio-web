﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
	public class RevTitleQueryHandler : IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>
	{
		private ITrioContextFactory contextFactory;

        public RevTitleQueryHandler(ITrioContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }

		public IEnumerable<RevTitle> ExecuteQuery(RevTitleSearchCriteria query)
		{
            var budgetaryContext = contextFactory.GetBudgetaryContext();

			IQueryable<RevTitle> ledgerTitleQuery;

			ledgerTitleQuery = budgetaryContext.RevTitles;

			if (query.Department != "")
			{
				ledgerTitleQuery = ledgerTitleQuery.Where(x => x.Department == query.Department);
			}

			if (query.Division != "")
			{
				ledgerTitleQuery = ledgerTitleQuery.Where(x => x.Division == query.Division);
			}

			if (query.Revenue != "")
			{
				ledgerTitleQuery = ledgerTitleQuery.Where(x => x.Revenue == query.Revenue);
			}

			return ledgerTitleQuery.ToList();
		}
	}
}
