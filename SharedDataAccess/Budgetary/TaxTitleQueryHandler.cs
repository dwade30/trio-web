﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Budgetary
{
    public class TaxTitleQueryHandler : IQueryHandler<TaxTitleSearchCriteria, IEnumerable<TaxTitle>>
    {
        private ITrioContextFactory contextFactory;

        public TaxTitleQueryHandler(ITrioContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        public IEnumerable<TaxTitle> ExecuteQuery(TaxTitleSearchCriteria query)
        {
            var budgetaryContext = contextFactory.GetBudgetaryContext();
            IQueryable<TaxTitle> taxTitleQuery;

            taxTitleQuery = budgetaryContext.TaxTitles;

            if (query.FormType != TaxFormType.All)
            {
                switch (query.FormType)
                {
                    case TaxFormType.MISC1099:
                        taxTitleQuery = taxTitleQuery.Where(x => x.FormName == "MISC");
                        break;
                    case TaxFormType.NEC1099:
                        taxTitleQuery = taxTitleQuery.Where(x => x.FormName == "NEC");
                        break;
                }
            }

            if (query.CategoryCode != 0)
            {
                taxTitleQuery = taxTitleQuery.Where(x => x.Category == query.CategoryCode);
            }

            if (query.TaxCode != 0)
            {
                taxTitleQuery = taxTitleQuery.Where(x => x.TaxCode == query.TaxCode);
            }

            return taxTitleQuery.ToList();
        }
    }
}
