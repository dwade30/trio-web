﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Enums;
using SharedApplication.Extensions;

namespace SharedDataAccess.CashReceipts
{
	public class ReceiptSearchReportQueryHandler : IQueryHandler<ReceiptSearchReportSetupInformation, IEnumerable<ReceiptSearchReportResult>>
	{
		private ICashReceiptsContext cashReceiptsContext;
		private IEnumerable<ReceiptType> receiptTypes = new List<ReceiptType>();
		private IEnumerable<DefaultBillType> defaultBillTypes = new List<DefaultBillType>();
		private IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;
		private DataEnvironmentSettings environmentSettings;
		private IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>> defaultBillTypeQueryHandler;
		private IGlobalActiveModuleSettings activeModuleSettings;

		public ReceiptSearchReportQueryHandler(ICashReceiptsContext cashReceiptsContext, IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler, DataEnvironmentSettings environmentSettings, IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>> defaultBillTypeQueryHandler, IGlobalActiveModuleSettings activeModuleSettings)
		{
			this.cashReceiptsContext = cashReceiptsContext;
			this.receiptTypeQueryHandler = receiptTypeQueryHandler;
			this.environmentSettings = environmentSettings;
			this.defaultBillTypeQueryHandler = defaultBillTypeQueryHandler;
			this.activeModuleSettings = activeModuleSettings;

			GetReceiptTypes();
			GetDefaultBillTypes();
		}

		private void GetDefaultBillTypes()
		{
			if (activeModuleSettings.AccountsReceivableIsActive)
			{
				defaultBillTypes = defaultBillTypeQueryHandler.ExecuteQuery(new DefaultBillTypeSearchCriteria()).ToList();
			}
			else
			{
				defaultBillTypes = new List<DefaultBillType>();
			}
		}

		private void GetReceiptTypes()
		{
			receiptTypes = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
			{
				TownKey = environmentSettings.IsMultiTown ? 1 : 0,
				DeletedSearchOption = DeletedOption.All
			}).ToList();
		}

		public IEnumerable<ReceiptSearchReportResult> ExecuteQuery(ReceiptSearchReportSetupInformation query)
		{
			var currentQuery = cashReceiptsContext.Archives;
			var previousQuery = cashReceiptsContext.LastYearArchives;
			
			if (query.ActualDateStart != null)
			{
				currentQuery = currentQuery.Where(x =>
					x.ActualSystemDate.Value.Date >= query.ActualDateStart.Value.Date && x.ActualSystemDate.Value.Date <= query.ActualDateEnd.Value.Date);
				previousQuery = previousQuery.Where(x =>
					x.ActualSystemDate.Value.Date >= query.ActualDateStart.Value.Date && x.ActualSystemDate.Value.Date <= query.ActualDateEnd.Value.Date);
			}

			if (query.ActualTimeStart != null)
			{
				currentQuery = currentQuery.Where(x => 
					TimeSpan.Compare(x.ActualSystemDate.Value.TimeOfDay, query.ActualTimeStart.Value.TimeOfDay) >= 0 && TimeSpan.Compare(x.ActualSystemDate.Value.TimeOfDay, query.ActualTimeEnd.Value.TimeOfDay) <= 0);
				previousQuery = previousQuery.Where(x =>
					TimeSpan.Compare(x.ActualSystemDate.Value.TimeOfDay, query.ActualTimeStart.Value.TimeOfDay) >= 0 && TimeSpan.Compare(x.ActualSystemDate.Value.TimeOfDay, query.ActualTimeEnd.Value.TimeOfDay) <= 0);
			}

			if (query.Bankid > 0)
			{
				currentQuery = currentQuery.Where(x => x.Receipt.CheckMasters.Any(y => y.BankId == query.Bankid));
				previousQuery = previousQuery.Where(x => x.Receipt.CheckMasters.Any(y => y.BankId == query.Bankid));
			}

			if (query.CheckNumber != "")
			{
				currentQuery =
					currentQuery.Where(x => x.Receipt.CheckMasters.Any(y => y.CheckNumber == query.CheckNumber));
				previousQuery =
					previousQuery.Where(x => x.Receipt.CheckMasters.Any(y => y.CheckNumber == query.CheckNumber));
			}

			if (query.CloseOutDateStartId > 0)
			{
				currentQuery = currentQuery.Where(x =>
					x.DailyCloseOut >= query.CloseOutDateStartId && x.DailyCloseOut <= query.CloseOutDateEndId);
				previousQuery = previousQuery.Where(x =>
					x.DailyCloseOut >= query.CloseOutDateStartId && x.DailyCloseOut <= query.CloseOutDateEndId);
			}

			if (query.Comment != "")
			{
				currentQuery = currentQuery.Where(x => x.Comment == query.Comment);
				previousQuery = previousQuery.Where(x => x.Comment == query.Comment);
			}

			if (query.Control1 != "")
			{
				currentQuery = currentQuery.Where(x => x.Control1 == query.Control1);
				previousQuery = previousQuery.Where(x => x.Control1 == query.Control1);
			}

			if (query.Control2 != "")
			{
				currentQuery = currentQuery.Where(x => x.Control2 == query.Control2);
				previousQuery = previousQuery.Where(x => x.Control2 == query.Control2);
			}

			if (query.Control3 != "")
			{
				currentQuery = currentQuery.Where(x => x.Control3 == query.Control3);
				previousQuery = previousQuery.Where(x => x.Control3 == query.Control3);
			}

			if (query.Reference != "")
			{
				currentQuery = currentQuery.Where(x => x.Ref == query.Reference);
				previousQuery = previousQuery.Where(x => x.Ref == query.Reference);
			}

			if (query.GLAccount != "")
			{
				currentQuery = currentQuery.Where(x =>
					x.ConvenienceFeeGLAccount == query.GLAccount || x.Account1 == query.GLAccount ||
					x.Account2 == query.GLAccount || x.Account3 == query.GLAccount || x.Account4 == query.GLAccount ||
					x.Account5 == query.GLAccount || x.Account6 == query.GLAccount);
				previousQuery = previousQuery.Where(x =>
					x.ConvenienceFeeGLAccount == query.GLAccount || x.Account1 == query.GLAccount ||
					x.Account2 == query.GLAccount || x.Account3 == query.GLAccount || x.Account4 == query.GLAccount ||
					x.Account5 == query.GLAccount || x.Account6 == query.GLAccount);
			}

			if (query.MotorVehicleTransactionType != MotorVehicleTransactionTypes.NoSelection)
			{
				
				List<string> commentSearch = new List<string>();

				switch (query.MotorVehicleTransactionType)
				{
					case MotorVehicleTransactionTypes.Correction:
						commentSearch.Add("ECR");
						commentSearch.Add("ECO");
						break;
					case MotorVehicleTransactionTypes.LostPlateStickers:
						commentSearch.Add("LPS");
						break;
					case MotorVehicleTransactionTypes.DuplicateRegistration:
						commentSearch.Add("DPR");
						break;
					case MotorVehicleTransactionTypes.DuplicateStickers:
						commentSearch.Add("DPS");
						break;
					case MotorVehicleTransactionTypes.GrossVehicleWeightChange:
						commentSearch.Add("GVW");
						break;
					case MotorVehicleTransactionTypes.NewRegistration:
						commentSearch.Add("NRR");
						break;
					case MotorVehicleTransactionTypes.NewRegistrationTransfer:
						commentSearch.Add("NRT");
						break;
					case MotorVehicleTransactionTypes.ReRegistration:
						commentSearch.Add("RRR");
						break;
					case MotorVehicleTransactionTypes.ReRegistrationTransfer:
						commentSearch.Add("RRT");
						break;
				}

				if (commentSearch.Count == 0)
				{
					currentQuery = currentQuery.Where(x => x.ReceiptType == 99);
					previousQuery = previousQuery.Where(x => x.ReceiptType == 99);
				}
				else if (commentSearch.Count == 1)
				{
					currentQuery = currentQuery.Where(x => x.ReceiptType == 99 && x.Comment == commentSearch.FirstOrDefault());
					previousQuery = previousQuery.Where(x => x.ReceiptType == 99 && x.Comment == commentSearch.FirstOrDefault());
				}
				else
				{
					currentQuery = currentQuery.Where(x => x.ReceiptType == 99 && commentSearch.Contains(x.Comment));
					previousQuery = previousQuery.Where(x => x.ReceiptType == 99 && commentSearch.Contains(x.Comment));
				}
			}

			if (query.Name != "")
			{
				if (query.NameSearchType == NameSearchOptions.BeginsWith)
				{
					currentQuery = currentQuery.Where(x => x.Name.StartsWith(query.Name));
					previousQuery = previousQuery.Where(x => x.Name.StartsWith(query.Name));
				}
				else
				{
					currentQuery = currentQuery.Where(x => x.Name.Contains(query.Name));
					previousQuery = previousQuery.Where(x => x.Name.Contains(query.Name));
				}
			}

			if (query.PaidBy != "")
			{
				if (query.NameSearchType == NameSearchOptions.BeginsWith)
				{
					currentQuery = currentQuery.Where(x => x.Receipt.PaidBy.StartsWith(query.Name));
					previousQuery = previousQuery.Where(x => x.Receipt.PaidBy.StartsWith(query.Name));
				}
				else
				{
					currentQuery = currentQuery.Where(x => x.Receipt.PaidBy.Contains(query.Name));
					previousQuery = previousQuery.Where(x => x.Receipt.PaidBy.Contains(query.Name));
				}
			}

			if (query.PaymentType != PaymentMethod.None)
			{
				if (query.PaymentType == PaymentMethod.Cash)
				{
					currentQuery = currentQuery.Where(x => x.Receipt.CashAmount != 0);
					previousQuery = previousQuery.Where(x => x.Receipt.CashAmount != 0);
				}
				else if (query.PaymentType == PaymentMethod.Credit)
				{
					currentQuery = currentQuery.Where(x => x.Receipt.CardAmount != 0);
					previousQuery = previousQuery.Where(x => x.Receipt.CardAmount != 0);
				}
				else
				{
					currentQuery = currentQuery.Where(x => x.Receipt.CheckAmount != 0);
					previousQuery = previousQuery.Where(x => x.Receipt.CheckAmount != 0);
				}
			}

			if (query.ReceiptNumberStart > 0)
			{
				currentQuery = currentQuery.Where(x =>
					x.Receipt.ReceiptNumber >= query.ReceiptNumberStart &&
					x.Receipt.ReceiptNumber <= query.ReceiptNumberEnd);
				previousQuery = previousQuery.Where(x =>
					x.Receipt.ReceiptNumber >= query.ReceiptNumberStart &&
					x.Receipt.ReceiptNumber <= query.ReceiptNumberEnd);
			}

			if (query.ReceiptTotal != null)
			{
				if (query.ReceiptTotalSearchType == AmountSearchOptions.GreaterThan)
				{
					currentQuery = currentQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0)).ToDecimal() > query.ReceiptTotal);
					previousQuery = previousQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0)).ToDecimal() > query.ReceiptTotal);
				}
				else if (query.ReceiptTotalSearchType == AmountSearchOptions.LessThan)
				{
					currentQuery = currentQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0)).ToDecimal() < query.ReceiptTotal);
					previousQuery = previousQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0)).ToDecimal() < query.ReceiptTotal);
				}
				else if (query.ReceiptTotalSearchType == AmountSearchOptions.NotEqual)
				{
					currentQuery = currentQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0)).ToDecimal() != query.ReceiptTotal);
					previousQuery = previousQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0)).ToDecimal() != query.ReceiptTotal);
				}
				else
				{
					currentQuery = currentQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0)).ToDecimal() == query.ReceiptTotal);
					previousQuery = previousQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0)).ToDecimal() == query.ReceiptTotal);
				}
			}

			if (query.TenderedTotal != null)
			{
				if (query.ReceiptTotalSearchType == AmountSearchOptions.GreaterThan)
				{
					currentQuery = currentQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0) + (x.Receipt.Change ?? 0)).ToDecimal() > query.TenderedTotal);
					previousQuery = previousQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0) + (x.Receipt.Change ?? 0)).ToDecimal() > query.TenderedTotal);
				}
				else if (query.ReceiptTotalSearchType == AmountSearchOptions.LessThan)
				{
					currentQuery = currentQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0) + (x.Receipt.Change ?? 0)).ToDecimal() < query.TenderedTotal);
					previousQuery = previousQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0) + (x.Receipt.Change ?? 0)).ToDecimal() < query.TenderedTotal);
				}
				else if (query.ReceiptTotalSearchType == AmountSearchOptions.NotEqual)
				{
					currentQuery = currentQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0) + (x.Receipt.Change ?? 0)).ToDecimal() != query.TenderedTotal);
					previousQuery = previousQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0) + (x.Receipt.Change ?? 0)).ToDecimal() != query.TenderedTotal);
				}
				else
				{
					currentQuery = currentQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0) + (x.Receipt.Change ?? 0)).ToDecimal() == query.TenderedTotal);
					previousQuery = previousQuery.Where(x =>
						((x.Receipt.CashAmount ?? 0) + (x.Receipt.CardAmount ?? 0) + (x.Receipt.CheckAmount ?? 0) + (x.Receipt.Change ?? 0)).ToDecimal() == query.TenderedTotal);
				}
			}

			if (query.ReceiptTypeCodeStart != 0)
			{
				currentQuery = currentQuery.Where(x =>
					x.ReceiptType >= query.ReceiptTypeCodeStart &&
					x.ReceiptType <= query.ReceiptTypeCodeEnd);
				previousQuery = previousQuery.Where(x =>
					x.ReceiptType >= query.ReceiptTypeCodeStart &&
					x.ReceiptType <= query.ReceiptTypeCodeEnd);
			}

			if (query.ReceiptTypes.Count() > 0)
			{
				currentQuery = currentQuery.Where(x => query.ReceiptTypes.Contains(x.ReceiptType ?? 0));
				previousQuery = previousQuery.Where(x => query.ReceiptTypes.Contains(x.ReceiptType ?? 0));
			}

			if (query.TellerId != "")
			{
				currentQuery = currentQuery.Where(x => x.TellerID == query.TellerId);
				previousQuery = previousQuery.Where(x => x.TellerId == query.TellerId);
			}

			var currentResults = currentQuery.Select(x => new ReceiptSearchReportResult
			{
				AccountNumber = x.AccountNumber ?? 0,
				Comment = x.Comment ?? "",
				ReceiptType = x.ReceiptType ?? 0,
				Reference = x.Ref ?? "",
				Name = x.Name ?? "",
				Control1 = x.Control1 ?? "",
				Control2 = x.Control2 ?? "",
				Control3 = x.Control3 ?? "",
				ReceiptNumber = x.Receipt.ReceiptNumber ?? 0,
				PaidBy = x.Receipt.PaidBy ?? "",
				RecordedTransactionDate = x.ArchiveDate,
				ARBillType = x.ARBillType ?? 0,
				Account1 = x.Account1 ?? "",
				Account2 = x.Account2 ?? "",
				Account3 = x.Account3 ?? "",
				Account4 = x.Account4 ?? "",
				Account5 = x.Account5 ?? "",
				Account6 = x.Account6 ?? "",
				ActualSystemDate = x.ActualSystemDate,
				Amount1 = (x.Amount1 ?? 0).ToDecimal(),
				Amount2 = (x.Amount2 ?? 0).ToDecimal(),
				Amount3 = (x.Amount3 ?? 0).ToDecimal(),
				Amount4 = (x.Amount4 ?? 0).ToDecimal(),
				Amount5 = (x.Amount5 ?? 0).ToDecimal(),
				Amount6 = (x.Amount6 ?? 0).ToDecimal(),
				CardAmount = (x.Receipt.CardAmount ?? 0).ToDecimal(),
				CashAmount = (x.Receipt.CashAmount ?? 0).ToDecimal(),
				CheckAmount = (x.Receipt.CheckAmount ?? 0).ToDecimal(),
				CardPaidAmount = (x.CardPaidAmount ?? 0),
				CheckPaidAmount = (x.CheckPaidAmount ?? 0),
				CashPaidAmount = (x.CashPaidAmount ?? 0),
				Change = (x.Receipt.Change ?? 0).ToDecimal(),
				ConvenienceFee = (x.Receipt.ConvenienceFee ?? 0),
				ConvenienceFeeGLAccount = x.ConvenienceFeeGLAccount ?? "",
				DailyCloseOut = x.DailyCloseOut ?? 0,
				DefaultAccount = x.DefaultAccount ?? "",
				DefaultCashAccount = x.DefaultCashAccount ?? "",
				ReceiptDate = x.Receipt.ReceiptDate,
				TellerID = x.TellerID ?? "",
				MultipleLineItemsOnReceipt = x.Receipt.Archives.Count > 1,
				Checks = x.Receipt.CheckMasters.Select(y => new ReceiptSearchReportCheckResult
				{
					Amount = (y.Amount ?? 0).ToDecimal(),
					CheckNumber = y.CheckNumber ?? ""
				})
			});
			
			var	previousResults = previousQuery.Select(x => new ReceiptSearchReportResult
			{
				AccountNumber = x.AccountNumber ?? 0,
				Comment = x.Comment ?? "",
				ReceiptType = x.ReceiptType ?? 0,
				Reference = x.Ref ?? "",
				Name = x.Name ?? "",
				Control1 = x.Control1 ?? "",
				Control2 = x.Control2 ?? "",
				Control3 = x.Control3 ?? "",
				ReceiptNumber = x.Receipt.ReceiptNumber ?? 0,
				PaidBy = x.Receipt.PaidBy ?? "",
				RecordedTransactionDate = x.ArchiveDate,
				ARBillType = x.ARBillType ?? 0,
				Account1 = x.Account1 ?? "",
				Account2 = x.Account2 ?? "",
				Account3 = x.Account3 ?? "",
				Account4 = x.Account4 ?? "",
				Account5 = x.Account5 ?? "",
				Account6 = x.Account6 ?? "",
				ActualSystemDate = x.ActualSystemDate,
				Amount1 = (x.Amount1 ?? 0).ToDecimal(),
				Amount2 = (x.Amount2 ?? 0).ToDecimal(),
				Amount3 = (x.Amount3 ?? 0).ToDecimal(),
				Amount4 = (x.Amount4 ?? 0).ToDecimal(),
				Amount5 = (x.Amount5 ?? 0).ToDecimal(),
				Amount6 = (x.Amount6 ?? 0).ToDecimal(),
				CardAmount = (x.Receipt.CardAmount ?? 0).ToDecimal(),
				CashAmount = (x.Receipt.CashAmount ?? 0).ToDecimal(),
				CheckAmount = (x.Receipt.CheckAmount ?? 0).ToDecimal(),
				CardPaidAmount = (x.CardPaidAmount ?? 0),
				CheckPaidAmount = (x.CheckPaidAmount ?? 0),
				CashPaidAmount = (x.CashPaidAmount ?? 0),
				Change = (x.Receipt.Change ?? 0).ToDecimal(),
				ConvenienceFee = (x.Receipt.ConvenienceFee ?? 0),
				ConvenienceFeeGLAccount = x.ConvenienceFeeGLAccount ?? "",
				DailyCloseOut = x.DailyCloseOut ?? 0,
				DefaultAccount = x.DefaultAccount ?? "",
				DefaultCashAccount = x.DefaultCashAccount ?? "",
				ReceiptDate = x.Receipt.LastYearReceiptDate,
				TellerID = x.TellerId ?? "",
				MultipleLineItemsOnReceipt = x.Receipt.Archives.Count > 1,
				Checks = x.Receipt.CheckMasters.Select(y => new ReceiptSearchReportCheckResult
				{
					Amount = (y.Amount ?? 0).ToDecimal(),
					CheckNumber = y.CheckNumber ?? ""
				})
			});

			var allResults = query.TimePeriod == TimePeriodOption.CurrentYear ? currentResults :
				query.TimePeriod == TimePeriodOption.PreviousYears ? previousResults :
				currentResults.Concat(previousResults);
			
			foreach (var sortItem in query.SortOptions)
			{
				switch (sortItem)
				{
					case ReceiptSearchSortByOptions.ActualSystemDate:
						allResults = allResults.AppendOrderBy(x => x.ActualSystemDate);
						break;
					case ReceiptSearchSortByOptions.ActualSystemTime:
						allResults = allResults.AppendOrderBy(x => x.ActualSystemDate.Value.ToUniversalTime());
						break;
					case ReceiptSearchSortByOptions.ReceiptType:
						allResults = allResults.AppendOrderBy(x => x.ReceiptType);
						break;
					case ReceiptSearchSortByOptions.CloseOutDate:
						allResults = allResults.AppendOrderBy(x => x.DailyCloseOut);
						break;
					case ReceiptSearchSortByOptions.Control1:
						allResults = allResults.AppendOrderBy(x => x.Control1);
						break;
					case ReceiptSearchSortByOptions.Control2:
						allResults = allResults.AppendOrderBy(x => x.Control2);
						break;
					case ReceiptSearchSortByOptions.Control3:
						allResults = allResults.AppendOrderBy(x => x.Control3);
						break;
					case ReceiptSearchSortByOptions.Reference:
						allResults = allResults.AppendOrderBy(x => x.Reference);
						break;
					case ReceiptSearchSortByOptions.ReceiptNumber:
						allResults = allResults.AppendOrderBy(x => x.ReceiptNumber);
						break;
					case ReceiptSearchSortByOptions.Name:
						allResults = allResults.AppendOrderBy(x => x.Name);
						break;
					case ReceiptSearchSortByOptions.TellerId:
						allResults = allResults.AppendOrderBy(x => x.TellerID);
						break;
				}
			}

			var returnValue = allResults.ToList();

			foreach (var result in returnValue)
			{
				var receiptTypeInfo = receiptTypes.FirstOrDefault(x => x.TypeCode == result.ReceiptType);

				if (result.ARBillType > 0)
				{
					var titleInfo = defaultBillTypes.FirstOrDefault(x => x.TypeCode == result.ARBillType);

					if (titleInfo != null)
					{
						result.Control1Label = titleInfo.Control1.Trim() == "" ? "Control 1" : titleInfo.Control1.Trim();
						result.Control2Label = titleInfo.Control2.Trim() == "" ? "Control 2" : titleInfo.Control2.Trim();
						result.Control3Label = titleInfo.Control3.Trim() == "" ? "Control 3" : titleInfo.Control3.Trim();
						result.Title1 = receiptTypeInfo.Title1.Trim() != "" ? receiptTypeInfo.Title1 + " - " + result.ARBillType : "";
						result.Title2 = receiptTypeInfo.Title2.Trim() != "" ? receiptTypeInfo.Title2 + " - " + result.ARBillType : "";
						result.Title3 = receiptTypeInfo.Title3.Trim() != "" ? receiptTypeInfo.Title3 + " - " + result.ARBillType : "";
						result.Title4 = receiptTypeInfo.Title4.Trim() != "" ? receiptTypeInfo.Title4 + " - " + result.ARBillType : "";
						result.Title5 = receiptTypeInfo.Title5.Trim() != "" ? receiptTypeInfo.Title5 + " - " + result.ARBillType : "";
						result.Title6 = receiptTypeInfo.Title6.Trim() != "" ? receiptTypeInfo.Title6 + " - " + result.ARBillType : "";
					}
					else
					{
						result.Control1Label = "Control 1";
						result.Control2Label = "Control 2";
						result.Control3Label = "Control 3";
						result.Title1 = "GL Account 1";
						result.Title2 = "GL Account 2";
						result.Title3 = "GL Account 3";
						result.Title4 = "GL Account 4";
						result.Title5 = "GL Account 5";
						result.Title6 = "GL Account 6";
					}
				}
				else
				{
					result.Control1Label = receiptTypeInfo.Control1.Trim() == "" ? "Control 1" : receiptTypeInfo.Control1.Trim();
					result.Control2Label = receiptTypeInfo.Control2.Trim() == "" ? "Control 2" : receiptTypeInfo.Control2.Trim();
					result.Control3Label = receiptTypeInfo.Control3.Trim() == "" ? "Control 3" : receiptTypeInfo.Control3.Trim();
					result.Title1 = receiptTypeInfo.Title1?.Trim() != "" ? receiptTypeInfo.Title1 + ((receiptTypeInfo.Year1 ?? false) ? " - " + (result.Account1.Trim() != "" ? result.Account1.Right(2) : "") : "") : "";
					result.Title2 = receiptTypeInfo.Title2?.Trim() != "" ? receiptTypeInfo.Title2 + ((receiptTypeInfo.Year2 ?? false) ? " - " + (result.Account2.Trim() != "" ? result.Account2.Right(2) : "") : "") : "";
					result.Title3 = receiptTypeInfo.Title3?.Trim() != "" ? receiptTypeInfo.Title3 + ((receiptTypeInfo.Year3 ?? false) ? " - " + (result.Account3.Trim() != "" ? result.Account3.Right(2) : "") : "") : "";
					result.Title4 = receiptTypeInfo.Title4?.Trim() != "" ? receiptTypeInfo.Title4 + ((receiptTypeInfo.Year4 ?? false) ? " - " + (result.Account4.Trim() != "" ? result.Account4.Right(2) : "") : "") : "";
					result.Title5 = receiptTypeInfo.Title5?.Trim() != "" ? receiptTypeInfo.Title5 + ((receiptTypeInfo.Year5 ?? false) ? " - " + (result.Account5.Trim() != "" ? result.Account5.Right(2) : "") : "") : "";
					result.Title6 = receiptTypeInfo.Title6?.Trim() != "" ? receiptTypeInfo.Title6 + ((receiptTypeInfo.Year6 ?? false) ? " - " + (result.Account6.Trim() != "" ? result.Account6.Right(2) : "") : "") : "";
				}
			}

			return returnValue;
		}

	}
}
