﻿using System;
using System.Linq;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Messaging;

namespace SharedDataAccess.CashReceipts
{
    public class UpdateElectronicPaymentLogHandler : CommandHandler<UpdateElectronicPaymentLog>
    {
        private CashReceiptsContext crContext;
        public UpdateElectronicPaymentLogHandler(CashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }
        protected override void Handle(UpdateElectronicPaymentLog command)
        {
            var log = crContext.ElectronicPaymentLogs.FirstOrDefault(l => l.Id == command.LogId);
            if (log != null)
            {
                log.Success = command.PaymentResponse.Success;
                log.StatusCode = command.PaymentResponse.AuthorizationCode;
                log.ErrorDescription = command.PaymentResponse.ErrorDescription;
                log.ReferenceId = command.PaymentResponse.ReferenceNumber;
                log.ResponseData = command.PaymentResponse.ResponseData;
                log.PaymentTime = DateTime.Now;
                crContext.SaveChanges();
            }
        }
    }
}