﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;

namespace SharedDataAccess.CashReceipts
{
	public class CheckMasterQueryHandler : IQueryHandler<CheckMasterSearchCriteria, IEnumerable<CheckMaster>>
	{
		private ICashReceiptsContext cashReceiptsContext;

		public CheckMasterQueryHandler(ICashReceiptsContext cashReceiptsContext)
		{
			this.cashReceiptsContext = cashReceiptsContext;
		}
		public IEnumerable<CheckMaster> ExecuteQuery(CheckMasterSearchCriteria query)
		{
			IQueryable<CheckMaster> checkMasterQuery;

			checkMasterQuery = cashReceiptsContext.CheckMasters;

			if (query.BankId > 0)
			{
				checkMasterQuery = checkMasterQuery.Where(x => x.BankId == query.BankId);
			}

			if (query.CheckNumber != "")
			{
				checkMasterQuery = checkMasterQuery.Where(x => x.CheckNumber == query.CheckNumber);
			}

			if (query.ReceiptId != 0)
			{
				checkMasterQuery = checkMasterQuery.Where(x => x.ReceiptId == query.ReceiptId);
			}
			return checkMasterQuery.ToList();
		}
	}
}
