﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.CashReceipts
{
	public class SaveReceiptSearchReportHandler : CommandHandler<SaveReceiptSearchReport, bool>
	{
		private CashReceiptsContext cashReceiptsContext;

		public SaveReceiptSearchReportHandler(ITrioContextFactory trioContextFactory)
		{
			cashReceiptsContext = trioContextFactory.GetCashReceiptsContext();
		}

		protected override bool Handle(SaveReceiptSearchReport command)
		{
			try
			{
				var report = cashReceiptsContext.SavedStatusReports.FirstOrDefault(x =>
					x.ReportName.ToUpper() == command.reportSetup.ReportName.ToUpper().Trim());

				if (report != null)
				{
					report.WhereSelection = command.reportSetup.WhereSelection;
					report.Ans0 = command.reportSetup.Ans0;
					report.Ans1 = command.reportSetup.Ans1;
					report.Ans2 = command.reportSetup.Ans2;
					report.Ans3 = command.reportSetup.Ans3;
					report.Ans4 = command.reportSetup.Ans4;
					report.Ans5 = command.reportSetup.Ans5;
					report.Ans6 = command.reportSetup.Ans6;
					report.Ans7 = command.reportSetup.Ans7;
					report.Ans8 = command.reportSetup.Ans8;
					report.Ans9 = command.reportSetup.Ans9;
					report.LastUpdated = DateTime.Now;
					report.FieldConstraint0 = command.reportSetup.FieldConstraint0;
					report.FieldConstraint1 = command.reportSetup.FieldConstraint1;
					report.FieldConstraint2 = command.reportSetup.FieldConstraint2;
					report.FieldConstraint3 = command.reportSetup.FieldConstraint3;
					report.FieldConstraint4 = command.reportSetup.FieldConstraint4;
					report.FieldConstraint5 = command.reportSetup.FieldConstraint5;
					report.FieldConstraint6 = command.reportSetup.FieldConstraint6;
					report.FieldConstraint7 = command.reportSetup.FieldConstraint7;
					report.FieldConstraint8 = command.reportSetup.FieldConstraint8;
					report.FieldConstraint9 = command.reportSetup.FieldConstraint9;
					report.FieldConstraint10 = command.reportSetup.FieldConstraint10;
					report.FieldConstraint11 = command.reportSetup.FieldConstraint11;
					report.FieldConstraint12 = command.reportSetup.FieldConstraint12;
					report.FieldConstraint13 = command.reportSetup.FieldConstraint13;
					report.FieldConstraint14 = command.reportSetup.FieldConstraint14;
					report.ReportName = command.reportSetup.ReportName;
					report.SQL = command.reportSetup.SQL;
					report.SortSelection = command.reportSetup.SortSelection;
					report.Type = command.reportSetup.Type;
				}
				else
				{
					cashReceiptsContext.SavedStatusReports.Add(command.reportSetup);
				}

				cashReceiptsContext.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
