﻿using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.CashReceipts
{
    public class CreateNewReceiptHandler : CommandHandler<CreateNewReceipt,int>
    {
        private CashReceiptsContext crContext;
        public CreateNewReceiptHandler(ITrioContextFactory contextFactory)
        {
            this.crContext = contextFactory.GetCashReceiptsContext();
        }
        protected override int Handle(CreateNewReceipt command)
        {
            if (command != null)
            {
                if (command.Receipt != null)
                {
                    crContext.Add(command.Receipt);
                    crContext.SaveChanges();
                    return command.Receipt.Id;
                }
            }

            return 0;
        }
    }
}