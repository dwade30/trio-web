﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication.CashReceipts;
using SharedApplication.Clerk;

namespace SharedDataAccess.CashReceipts
{
	public class CashReceiptsRepositoryFactory : RepositoryFactory, ICashReceiptsRepositoryFactory
	{
		public CashReceiptsRepositoryFactory(DbContext context) : base(context)
		{
		}
	}
}
