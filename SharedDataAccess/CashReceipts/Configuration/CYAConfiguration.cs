﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class CYAConfiguration : IEntityTypeConfiguration<Cya>
	{
		public void Configure(EntityTypeBuilder<Cya> builder)
		{
			builder.ToTable("CYA");
			builder.Property(p => p.Id).HasColumnName("ID");
			builder.Property(p => p.UserID).HasMaxLength(255);
			builder.Property(p => p.Description1).HasMaxLength(255);
			builder.Property(p => p.Description2).HasMaxLength(255);
			builder.Property(p => p.Description3).HasMaxLength(255);
			builder.Property(p => p.Description4).HasMaxLength(255);
			builder.Property(p => p.Description5).HasMaxLength(255);
		}
	}
}
