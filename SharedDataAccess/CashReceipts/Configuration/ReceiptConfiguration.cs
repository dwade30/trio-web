﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class ReceiptConfiguration: IEntityTypeConfiguration<Receipt>
	{
		public void Configure(EntityTypeBuilder<Receipt> builder)
		{
			builder.ToTable("Receipt");
			builder.Property(p => p.Id).HasColumnName("ReceiptKey");
			builder.Property(p => p.TellerID).HasMaxLength(255);
			builder.Property(p => p.PaidBy).HasMaxLength(255);
			builder.Property(p => p.ConvenienceFee).HasColumnType("money");
			builder.HasKey(t => t.Id);
		}
	}
}
