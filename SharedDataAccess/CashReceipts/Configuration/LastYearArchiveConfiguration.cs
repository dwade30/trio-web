﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
    public class LastYearArchiveConfiguration : IEntityTypeConfiguration<LastYearArchive>
    {
        public void Configure(EntityTypeBuilder<LastYearArchive> builder)
        {
            builder.ToTable("LastYearArchive");

            builder.HasIndex(e => e.ReceiptId);
            builder.Property(e => e.ReceiptId).HasColumnName("ReceiptNumber");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.Account1).HasMaxLength(255);

            builder.Property(e => e.Account2).HasMaxLength(255);

            builder.Property(e => e.Account3).HasMaxLength(255);

            builder.Property(e => e.Account4).HasMaxLength(255);

            builder.Property(e => e.Account5).HasMaxLength(255);

            builder.Property(e => e.Account6).HasMaxLength(255);

            builder.Property(e => e.ActualSystemDate).HasColumnType("datetime");

            builder.Property(e => e.ARBillType).HasColumnName("ARBillType");

            builder.Property(e => e.CardPaidAmount).HasColumnType("money");

            builder.Property(e => e.CashPaidAmount).HasColumnType("money");

            builder.Property(e => e.CheckPaidAmount).HasColumnType("money");

            builder.Property(e => e.CollectionCode).HasMaxLength(255);

            builder.Property(e => e.Comment).HasMaxLength(255);

            builder.Property(e => e.Control1).HasMaxLength(255);

            builder.Property(e => e.Control2).HasMaxLength(255);

            builder.Property(e => e.Control3).HasMaxLength(255);

            builder.Property(e => e.ConvenienceFee).HasColumnType("money");

            builder.Property(e => e.ConvenienceFeeGLAccount)
                .HasColumnName("ConvenienceFeeGLAccount")
                .HasMaxLength(255);

            builder.Property(e => e.DefaultAccount).HasMaxLength(255);

            builder.Property(e => e.DefaultCashAccount).HasMaxLength(255);

            builder.Property(e => e.DefaultMiaccount)
                .HasColumnName("DefaultMIAccount")
                .HasMaxLength(255);

            builder.Property(e => e.Eft).HasColumnName("EFT");

            builder.Property(e => e.ArchiveDate).HasColumnName("LastYearArchiveDate").HasColumnType("datetime");

            builder.Property(e => e.Name).HasMaxLength(255);

            builder.Property(e => e.NamePartyId).HasColumnName("NamePartyID");

            builder.Property(e => e.Project1).HasMaxLength(255);

            builder.Property(e => e.Project2).HasMaxLength(255);

            builder.Property(e => e.Project3).HasMaxLength(255);

            builder.Property(e => e.Project4).HasMaxLength(255);

            builder.Property(e => e.Project5).HasMaxLength(255);

            builder.Property(e => e.Project6).HasMaxLength(255);

            builder.Property(e => e.ReceiptTitle).HasMaxLength(255);

            builder.Property(e => e.Ref).HasMaxLength(255);

            builder.Property(e => e.SeperateCreditCardGlaccount)
                .HasColumnName("SeperateCreditCardGLAccount")
                .HasMaxLength(255);

            builder.Property(e => e.TellerId)
                .HasColumnName("TellerID")
                .HasMaxLength(255);
        }
    }
}