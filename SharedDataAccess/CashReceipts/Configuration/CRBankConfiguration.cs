﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
    public class CRBankConfiguration :IEntityTypeConfiguration<CRBank>
    {
        public void Configure(EntityTypeBuilder<CRBank> builder)
        {
            builder.ToTable("Bank");
            builder.Property(p => p.Name).HasMaxLength(255);
            builder.Property(p => p.RoutingTransit).HasMaxLength(255);
        }
    }
}