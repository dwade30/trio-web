﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class CloseOutConfiguration : IEntityTypeConfiguration<CloseOut>
	{
		public void Configure(EntityTypeBuilder<CloseOut> builder)
		{
			builder.ToTable("CloseOut");
			builder.Property(p => p.Type).HasMaxLength(255);
			builder.Property(p => p.TellerId).HasMaxLength(255);
			builder.Property(p => p.Id).HasColumnName("ID");
			builder.Property(p => p.TellerId).HasColumnName("TellerID");
		}
	}
}
