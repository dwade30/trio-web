﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class SavedStatusReportConfiguration : IEntityTypeConfiguration<SavedStatusReport>
	{
		public void Configure(EntityTypeBuilder<SavedStatusReport> builder)
		{
			builder.ToTable("SavedStatusReports");
			builder.Property(p => p.Id).HasColumnName("ID");
			builder.Property(p => p.ReportName).HasMaxLength(255);
			builder.Property(p => p.Type).HasMaxLength(255);
			builder.Property(p => p.WhereSelection).HasMaxLength(255);
			builder.Property(p => p.SortSelection).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint0).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint1).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint2).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint3).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint4).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint5).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint6).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint7).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint8).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint9).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint10).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint11).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint12).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint13).HasMaxLength(255);
			builder.Property(p => p.FieldConstraint14).HasMaxLength(255);
			builder.Property(p => p.Ans0).HasMaxLength(255);
			builder.Property(p => p.Ans1).HasMaxLength(255);
			builder.Property(p => p.Ans2).HasMaxLength(255);
			builder.Property(p => p.Ans3).HasMaxLength(255);
			builder.Property(p => p.Ans4).HasMaxLength(255);
			builder.Property(p => p.Ans5).HasMaxLength(255);
			builder.Property(p => p.Ans6).HasMaxLength(255);
			builder.Property(p => p.Ans7).HasMaxLength(255);
			builder.Property(p => p.Ans8).HasMaxLength(255);
			builder.Property(p => p.Ans9).HasMaxLength(255);

		}
	}
}
