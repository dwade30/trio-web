﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class ArchiveConfiguration : IEntityTypeConfiguration<Archive>
	{
		public void Configure(EntityTypeBuilder<Archive> builder)
		{
			builder.ToTable("Archive");
			builder.Property(p => p.Id).HasColumnName("ID");
			builder.Property(p => p.ReceiptId).HasColumnName("ReceiptNumber");
			builder.Property(p => p.ReceiptTitle).HasMaxLength(255);
			builder.Property(p => p.Account1).HasMaxLength(255);
			builder.Property(p => p.Account2).HasMaxLength(255);
			builder.Property(p => p.Account3).HasMaxLength(255);
			builder.Property(p => p.Account4).HasMaxLength(255);
			builder.Property(p => p.Account5).HasMaxLength(255);
			builder.Property(p => p.Account6).HasMaxLength(255);
			builder.Property(p => p.Comment).HasMaxLength(255);
			builder.Property(p => p.Ref).HasMaxLength(255);
			builder.Property(p => p.Control1).HasMaxLength(255);
			builder.Property(p => p.Control2).HasMaxLength(255);
			builder.Property(p => p.Control3).HasMaxLength(255);
			builder.Property(p => p.TellerID).HasMaxLength(255);
			builder.Property(p => p.Name).HasMaxLength(255);
			builder.Property(p => p.CollectionCode).HasMaxLength(255);
			builder.Property(p => p.DefaultAccount).HasMaxLength(255);
			builder.Property(p => p.DefaultCashAccount).HasMaxLength(255);
			builder.Property(p => p.DefaultMIAccount).HasMaxLength(255);
			builder.Property(p => p.Project1).HasMaxLength(255);
			builder.Property(p => p.Project2).HasMaxLength(255);
			builder.Property(p => p.Project3).HasMaxLength(255);
			builder.Property(p => p.Project4).HasMaxLength(255);
			builder.Property(p => p.Project5).HasMaxLength(255);
			builder.Property(p => p.Project6).HasMaxLength(255);
			builder.Property(p => p.SeperateCreditCardGLAccount).HasMaxLength(255);
			builder.Property(p => p.ConvenienceFeeGLAccount).HasMaxLength(255);
			builder.Property(p => p.ConvenienceFee).HasColumnType("money");
			builder.Property(p => p.CashPaidAmount).HasColumnType("money");
			builder.Property(p => p.CardPaidAmount).HasColumnType("money");
			builder.Property(p => p.CheckPaidAmount).HasColumnType("money");
		}
	}
}
