﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
    public class ServiceCodeConfiguration : IEntityTypeConfiguration<ServiceCode>
    {
        public void Configure(EntityTypeBuilder<ServiceCode> builder)
        {
            builder.ToTable("EPmtAccounts");
            builder.Property(p => p.Id).HasColumnName("ID");
            builder.Property(p => p.Code).HasColumnName("AcctID").HasMaxLength(255);
            builder.Property(p => p.Description).HasColumnName("AcctDescription").HasMaxLength(255);
        }
    }
}