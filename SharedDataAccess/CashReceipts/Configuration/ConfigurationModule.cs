﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.SystemSettings;
using SharedApplication.SystemSettings.Models;
using SharedDataAccess.SystemSettings;
using Module = Autofac.Module;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			RegisterCashReceiptsRepositoryFactory(ref builder);
			RegisterReceiptTypeSearchQueryHandler(ref builder);
			RegisterReceiptSearchQueryHandler(ref builder);
			RegisterLastYearReceiptSearchQueryHandler(ref builder);
			RegisterSavedStatusReportQueryHandler(ref builder);
            RegisterCRBanksQueryHandler(ref builder);
            RegisterCloseOutQueryHandler(ref builder);
			RegisterCRContext(builder);
            RegisterMosesTypesQueryHandler(ref builder);
            RegisterCreditCardTypeQueryHandler(ref builder);
            RegisterCreditCardMasterQueryHandler(ref builder);
            RegisterCheckMasterQueryHandler(ref builder);
            RegisterReceiptSearchReportQueryHandler(ref builder);

            builder.RegisterType<CRReceiptQueryHandler>().As<IQueryHandler<CRReceiptQuery, Receipt>>();
        }

		private void RegisterCloseOutQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<CloseOutQueryHandler>()
				.As<IQueryHandler<CloseOutSearchCriteria, IEnumerable<CloseOut>>>();
		}

		private void RegisterReceiptSearchReportQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<ReceiptSearchReportQueryHandler>()
				.As<IQueryHandler<ReceiptSearchReportSetupInformation, IEnumerable<ReceiptSearchReportResult>>>();
		}

		private void RegisterSavedStatusReportQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<SavedStatusReportsQueryHandler>()
				.As<IQueryHandler<SavedStatusReportsSearchCriteria, IEnumerable<SavedStatusReport>>>();
		}

        private void RegisterMosesTypesQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<MosesTypesQueryHandler>().As<IQueryHandler<MosesTypesQuery, IEnumerable<MOSESType>>>();
        }

        private void RegisterCRBanksQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<CRBanksQueryHandler>().As<IQueryHandler<CRBanksQuery, IEnumerable<CRBank>>>();
        }

        private void RegisterCreditCardTypeQueryHandler(ref ContainerBuilder builder)
        {
	        builder.RegisterType<CreditCardTypeQueryHandler>().As<IQueryHandler<CreditCardTypeSearchCriteria, IEnumerable<CreditCardType>>>();
        }

        private void RegisterCreditCardMasterQueryHandler(ref ContainerBuilder builder)
        {
	        builder.RegisterType<CreditCardMasterQueryHandler>().As<IQueryHandler<CreditCardMasterSearchCriteria, IEnumerable<CreditCardMaster>>>();
        }

        private void RegisterCheckMasterQueryHandler(ref ContainerBuilder builder)
        {
	        builder.RegisterType<CheckMasterQueryHandler>().As<IQueryHandler<CheckMasterSearchCriteria, IEnumerable<CheckMaster>>>();
        }

        private void RegisterReceiptTypeSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<ReceiptTypeQueryHandler>()
				.As<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
		}

		private void RegisterReceiptSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<ReceiptQueryHandler>()
				.As<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
		}
		private void RegisterLastYearReceiptSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<LastYearReceiptQueryHandler>()
				.As<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
		}

		private void RegisterCashReceiptsRepositoryFactory(ref ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var tcf = f.Resolve<ITrioContextFactory>();
				return new CashReceiptsRepositoryFactory(tcf.GetCashReceiptsContext());
			}).As<ICashReceiptsRepositoryFactory>();
		}

        private void RegisterCRContext(ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return tcf.GetCashReceiptsContext();
            }).As<ICashReceiptsContext>().AsSelf();
        }

}
}
