﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class CreditCardTypeConfiguration : IEntityTypeConfiguration<CreditCardType>
	{
		public void Configure(EntityTypeBuilder<CreditCardType> builder)
		{
			builder.ToTable("CCType");
			builder.Property(p => p.Type).HasMaxLength(255);
		}
	}
}
