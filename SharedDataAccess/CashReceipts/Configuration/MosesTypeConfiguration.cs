﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
    public class MosesTypeConfiguration : IEntityTypeConfiguration<MOSESType>
    {
        public void Configure(EntityTypeBuilder<MOSESType> builder)
        {
            builder.ToTable("MOSESTypes");
            builder.Property(p => p.MOSESTypeDescription).HasMaxLength(255);
        }
    }
}