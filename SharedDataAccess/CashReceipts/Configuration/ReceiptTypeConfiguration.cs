﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class ReceiptTypeConfiguration : IEntityTypeConfiguration<ReceiptType>
	{
		public void Configure(EntityTypeBuilder<ReceiptType> builder)
		{
			builder.ToTable("Type");
			builder.Property(p => p.TypeTitle).HasMaxLength(255);
			builder.Property(p => p.Title1).HasMaxLength(255);
			builder.Property(p => p.Title1Abbrev).HasMaxLength(255);
			builder.Property(p => p.Account1).HasMaxLength(255);
			builder.Property(p => p.Title2).HasMaxLength(255);
			builder.Property(p => p.Title2Abbrev).HasMaxLength(255);
			builder.Property(p => p.Account2).HasMaxLength(255);
			builder.Property(p => p.Title3).HasMaxLength(255);
			builder.Property(p => p.Title3Abbrev).HasMaxLength(255);
			builder.Property(p => p.Account3).HasMaxLength(255);
			builder.Property(p => p.Title4).HasMaxLength(255);
			builder.Property(p => p.Title4Abbrev).HasMaxLength(255);
			builder.Property(p => p.Account4).HasMaxLength(255);
			builder.Property(p => p.Title5).HasMaxLength(255);
			builder.Property(p => p.Title5Abbrev).HasMaxLength(255);
			builder.Property(p => p.Account5).HasMaxLength(255);
			builder.Property(p => p.Title6).HasMaxLength(255);
			builder.Property(p => p.Title6Abbrev).HasMaxLength(255);
			builder.Property(p => p.Account6).HasMaxLength(255);
			builder.Property(p => p.Reference).HasMaxLength(255);
			builder.Property(p => p.Control1).HasMaxLength(255);
			builder.Property(p => p.Control2).HasMaxLength(255);
			builder.Property(p => p.Control3).HasMaxLength(255);
			builder.Property(p => p.Project1).HasMaxLength(255);
			builder.Property(p => p.Project2).HasMaxLength(255);
			builder.Property(p => p.Project3).HasMaxLength(255);
			builder.Property(p => p.Project4).HasMaxLength(255);
			builder.Property(p => p.Project5).HasMaxLength(255);
			builder.Property(p => p.Project6).HasMaxLength(255);
			builder.Property(p => p.DefaultAccount).HasMaxLength(255);
			builder.Property(p => p.ConvenienceFeeMethod).HasMaxLength(255);
			builder.Property(p => p.ConvenienceFeeGLAccount).HasMaxLength(255);
			builder.Property(p => p.EPmtAcctID).HasMaxLength(255);
			builder.Property(p => p.ConvenienceFlatAmount).HasColumnType("money");
		}
	}
}
