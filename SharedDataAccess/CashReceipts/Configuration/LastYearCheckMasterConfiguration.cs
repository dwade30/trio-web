﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class LastYearCheckMasterConfiguration : IEntityTypeConfiguration<LastYearCheckMaster>
	{
		public void Configure(EntityTypeBuilder<LastYearCheckMaster> builder)
		{
			builder.ToTable("LastYearCheckMaster");
			builder.Property(p => p.ReceiptId).HasColumnName("ReceiptNumber");
			builder.Property(p => p.CheckNumber).HasMaxLength(50);
			builder.Property(p => p.EPymtRefNumber).HasMaxLength(50);
			builder.Property(p => p.Last4Digits).HasMaxLength(4);
			builder.Property(p => p.AccountType).HasMaxLength(25);
            builder.Property(p => p.BankId).HasColumnName("BankNumber");
        }
	}
}
