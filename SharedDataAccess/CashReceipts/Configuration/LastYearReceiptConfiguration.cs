﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class LastYearReceiptConfiguration : IEntityTypeConfiguration<LastYearReceipt>
	{
		public void Configure(EntityTypeBuilder<LastYearReceipt> builder)
		{
			builder.ToTable("LastYearReceipt");
			builder.Property(e => e.PaidByPartyId).HasColumnName("PaidByPartyID");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(p => p.TellerID).HasMaxLength(255);
			builder.Property(p => p.PaidBy).HasMaxLength(255);
			builder.Property(p => p.ConvenienceFee).HasColumnType("money");
			builder.HasKey(t => t.Id);
		}
	}
}
