﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
    public class RNumLockConfiguration : IEntityTypeConfiguration<RNumLock>
    {
        public void Configure(EntityTypeBuilder<RNumLock> builder)
        {
            builder.ToTable("RNumLock");
            builder.Property(p => p.OpID).HasMaxLength(255);
        }
    }
}