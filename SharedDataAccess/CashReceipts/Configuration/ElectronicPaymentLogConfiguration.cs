﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
    public class ElectronicPaymentLogConfiguration : IEntityTypeConfiguration<ElectronicPaymentLog>
    {
        public void Configure(EntityTypeBuilder<ElectronicPaymentLog> builder)
        {
            builder.ToTable("ElectronicPaymentLog");
            builder.Property(p => p.Id).HasColumnName("ID");
            builder.Property(p => p.ErrorDescription).HasMaxLength(1023);
            builder.Property(p => p.ReferenceId).HasMaxLength(255);
            builder.Property(p => p.ResponseData).HasMaxLength(1023);
            builder.Property(p => p.StatusCode).HasMaxLength(100);
            builder.Property(p => p.MerchantCode).HasMaxLength(100);
            builder.Property(p => p.TellerId).HasMaxLength(100);
        }
    }
}