﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts.Configuration
{
	public class LastYearCreditCardMasterConfiguration : IEntityTypeConfiguration<LastYearCreditCardMaster>
	{
		public void Configure(EntityTypeBuilder<LastYearCreditCardMaster> builder)
		{
			builder.ToTable("LastYearCCMaster");
			builder.Property(p => p.ReceiptId).HasColumnName("ReceiptNumber");
			builder.Property(p => p.EPymtRefNumber).HasMaxLength(50);
			builder.Property(p => p.Last4Digits).HasMaxLength(4);
			builder.Property(p => p.CryptCard).HasMaxLength(255);
			builder.Property(p => p.CryptCardExpiration).HasMaxLength(255);
			builder.Property(p => p.MaskedCreditCardNumber).HasMaxLength(255);
			builder.Property(p => p.AuthCode).HasMaxLength(255);
			builder.Property(p => p.AuthCodeConvenienceFee).HasMaxLength(255);
			builder.Property(p => p.AuthCodeVISANonTax).HasMaxLength(255);
			builder.Property(p => p.AuthCodeNonTaxConvenienceFee).HasMaxLength(255);
			builder.Property(p => p.SecureGUID).HasMaxLength(255);
			builder.Property(p => p.EPmtAcctID).HasMaxLength(255);
			builder.Property(p => p.ConvenienceFee).HasColumnType("money");
			builder.Property(p => p.NonTaxAmount).HasColumnType("money");
			builder.Property(p => p.NonTaxConvenienceFee).HasColumnType("money");
		}
	}

}
