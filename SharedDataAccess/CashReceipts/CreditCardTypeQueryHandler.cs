﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts
{
	public class CreditCardTypeQueryHandler : IQueryHandler<CreditCardTypeSearchCriteria, IEnumerable<CreditCardType>>
	{
		private ICashReceiptsContext cashReceiptsContext;

		public CreditCardTypeQueryHandler(ICashReceiptsContext cashReceiptsContext)
		{
			this.cashReceiptsContext = cashReceiptsContext;
		}
		public IEnumerable<CreditCardType> ExecuteQuery(CreditCardTypeSearchCriteria query)
		{
			IQueryable<CreditCardType> ccTypeQuery;

			if (query.Id != 0)
			{
				ccTypeQuery = cashReceiptsContext.CreditCardTypes.Where(x => x.ID == query.Id);
			}
			else
			{
				ccTypeQuery = cashReceiptsContext.CreditCardTypes.Where(x => x.SystemDefined == query.SystemDefined);
			}
			
			return ccTypeQuery.OrderBy(b => b.Type).ToList();
		}
	}
}
