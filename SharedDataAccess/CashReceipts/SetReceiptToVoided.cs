﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedDataAccess;
using SharedDataAccess.CashReceipts;

namespace SharedApplication.CashReceipts.Commands
{
    public class SetReceiptToVoidedHandler : CommandHandler<SetReceiptToVoided>
    {
        private CashReceiptsContext crContext;
        public SetReceiptToVoidedHandler(ITrioContextFactory factory)
        {
            this.crContext = factory.GetCashReceiptsContext();
        }

        protected override void Handle(SetReceiptToVoided command)
        {
            var receipt = crContext.Receipts.FirstOrDefault(x => x.ReceiptNumber == command.ReceiptNumber);

            if (receipt != null)
            {
                receipt.IsVoided = true;
                crContext.SaveChanges();
            }
        }
    }
}
