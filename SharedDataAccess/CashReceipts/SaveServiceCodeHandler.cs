﻿using System.Linq;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.CashReceipts
{
    public class SaveServiceCodeHandler : CommandHandler<SaveServiceCode>
    {
        private CashReceiptsContext crContext;
        public SaveServiceCodeHandler(CashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }
        protected override void Handle(SaveServiceCode command)
        {
            if (command != null && command.ServiceCode != null)
            {
                if (command.ServiceCode.Id == 0)
                {
                    AddServiceCode(command.ServiceCode);
                }
                else
                {
                    UpdateServiceCode(command.ServiceCode);
                }
            }
        }

        private void AddServiceCode(ServiceCode serviceCode)
        {
            crContext.ServiceCodes.Add(serviceCode);
            crContext.SaveChanges();
        }

        private void UpdateServiceCode(ServiceCode serviceCode)
        {
            var code = crContext.ServiceCodes.FirstOrDefault(s => s.Id == serviceCode.Id);
            if (code != null)
            {
                code.Copy(serviceCode);
                crContext.SaveChanges();
            }
        }
    }
}