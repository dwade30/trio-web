﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralParties.Models;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;

namespace SharedDataAccess.CashReceipts
{
	public class ReceiptTypeQueryHandler : IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>
	{
		private ICashReceiptsContext cashReceiptsContext;

		public ReceiptTypeQueryHandler(ITrioContextFactory contextFactory)
		{
			this.cashReceiptsContext = contextFactory.GetCashReceiptsContext();
		}

		public IEnumerable<ReceiptType> ExecuteQuery(ReceiptTypeSearchCriteria query)
		{
			IQueryable<ReceiptType> receiptTypeQuery;

			receiptTypeQuery = cashReceiptsContext.ReceiptTypes;

			if (query.TownKey > 0)
			{
				receiptTypeQuery = receiptTypeQuery.Where(x => x.TownKey == query.TownKey);
			}

			if (query.ReceiptType > 0)
			{
				receiptTypeQuery = receiptTypeQuery.Where(x => x.TypeCode == query.ReceiptType);
			}

			if (query.DeletedSearchOption == DeletedOption.DeletedOnly)
			{
				receiptTypeQuery = receiptTypeQuery.Where(x => x.Deleted ?? false);
			}
			else if (query.DeletedSearchOption == DeletedOption.NotDeletedOnly)
			{
				receiptTypeQuery = receiptTypeQuery.Where(x => !x.Deleted ?? false);
			}

			return receiptTypeQuery.ToList();
		}
	}
}
