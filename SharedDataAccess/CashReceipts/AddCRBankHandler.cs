﻿using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.CashReceipts
{
    public class AddCRBankHandler : CommandHandler<AddCRBank,int>
    {
        private CashReceiptsContext crContext;
        public AddCRBankHandler(ITrioContextFactory contextFactory)
        {
            crContext = contextFactory.GetCashReceiptsContext();
        }
        protected override int Handle(AddCRBank command)
        {
            crContext.Banks.Add(command.Bank);
            crContext.SaveChanges();
            return command.Bank.ID;
        }
    }
}