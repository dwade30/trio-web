﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.CashReceipts
{
    public class DeleteServiceCodeHandler : CommandHandler<DeleteServiceCode>
    {
        private CashReceiptsContext crContext;
        public DeleteServiceCodeHandler(CashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }
        protected override void Handle(DeleteServiceCode command)
        {
            if (command.Id > 0)
            {
               // crContext.Database.ExecuteSqlCommand("delete from ServiceCodes where id == " + command.Id);
                var serviceCode = crContext.ServiceCodes.FirstOrDefault(s => s.Id == command.Id);
                if (serviceCode != null)
                {
                    crContext.ServiceCodes.Remove(serviceCode);
                    crContext.SaveChanges();
                }
            }
        }
    }
}