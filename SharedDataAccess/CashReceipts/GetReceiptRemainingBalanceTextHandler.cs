﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.AccountGroups;
using SharedApplication.AccountGroups.Commands;
using SharedApplication.AccountsReceivable.Commands;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.Telemetry;
using SharedApplication.UtilityBilling.Commands;

namespace SharedDataAccess.CashReceipts
{
    public class GetReceiptRemainingBalanceTextHandler :CommandHandler<GetReceiptRemainingBalanceText, string>
    {
        private GlobalCashReceiptSettings globalCashReceiptSettings;
        private CommandDispatcher commandDispatcher;
        private ITelemetryService telemetryService;
        private IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>> groupMasterQueryHandler;

		public GetReceiptRemainingBalanceTextHandler(GlobalCashReceiptSettings globalCashReceiptSettings, CommandDispatcher commandDispatcher, ITelemetryService telemetryService, IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>> groupMasterQueryHandler)
        {
            this.globalCashReceiptSettings = globalCashReceiptSettings;
            this.commandDispatcher = commandDispatcher;
            this.telemetryService = telemetryService;
            this.groupMasterQueryHandler = groupMasterQueryHandler;
        }

        protected override string Handle(GetReceiptRemainingBalanceText command)
        {
			try
			{
				if (command.IsBatch)
                {
                    return "";
                }
				else if (command.IsReprint)
                {
                    return "REPRINT";
                }
				else if (!command.IsFirstCopy)
                {
                    return "COPY";
                }
                else
                {
                    List<int> groupNumbersToCalculate = new List<int>();
                    List<AccountInfo> accountsToCalculate = new List<AccountInfo>();
                    
					foreach (var transaction in command.Transactions)
                    {
                        var transactionType = ReceiptTypeToInteger(transaction.TransactionTypeCode);
                        var account = transaction.ToReceiptSummaryItem().Account;
                        var type = AccountType.None;

						switch (transactionType)
                        {
							case 90:
							case 91:
							case 890:
							case 891:
                            {
								type = AccountType.RealEstate;
								break;
                            }

                            case 92:
                            {
								type = AccountType.PersonalProperty;
								break;
                            }

                            case 93:
                            case 94:
                            case 95:
                            case 96:
                            case 893:
                            case 894:
                            case 895:
                            case 896:
							{
								type = AccountType.UtilityBilling;
								break;
                            }

							case 97:
                            {
                                type = AccountType.AccountsReceivable;
                                break;
							}
						}

                        if (type != AccountType.None)
                        {
                            var groupMaster = this.groupMasterQueryHandler.ExecuteQuery(new GroupMasterSearchCriteria
                            {
                                AssociatedWith = new ModuleAssociationSearchCriteria
                                {
                                    AccountNumber = account,
                                    Module = type.ToAbbreviation()
                                }
                            }).FirstOrDefault();

							if (groupMaster != null)
                            {
                                if (!groupNumbersToCalculate.Any(x => x == groupMaster.GroupNumber))
                                {
                                    groupNumbersToCalculate.Add((int)groupMaster.GroupNumber);
                                }
                            }
                            else
                            {
                                if (!accountsToCalculate.Any(x =>
                                    x.Account == account && x.Type == AccountType.RealEstate))
                                {
                                    accountsToCalculate.Add(new AccountInfo
                                    {
                                        Account = account,
                                        Type = type
                                    });
                                }
                            }
						}
                    }

                    decimal remainingBalanceTotal = 0;
                    int numberOfAccounts = 0;
                    List<AccountInfo> accountsAlreadyCalculated = new List<AccountInfo>();

					foreach (var accountToCalculate in accountsToCalculate)
                    {
						if (!accountsAlreadyCalculated.Any(x => x.Account == accountToCalculate.Account && x.Type == accountToCalculate.Type))
                        {
							switch (accountToCalculate.Type.ToAbbreviation().ToLower())
							{
								case "pp":
									var calculatedPPAccount = commandDispatcher.Send(new GetCalculatedTaxAccount(accountToCalculate.Account,
										PropertyTaxBillType.Personal, DateTime.Today)).Result;
									if (calculatedPPAccount != null)
									{
                                        numberOfAccounts++;
                                        remainingBalanceTotal += calculatedPPAccount.AccountSummary.Balance;
                                        accountsAlreadyCalculated.Add(accountToCalculate);
                                    }

									break;
								case "re":
									var calculatedREAccount = commandDispatcher.Send(new GetCalculatedTaxAccount(accountToCalculate.Account,
										PropertyTaxBillType.Real, DateTime.Today)).Result;
									if (calculatedREAccount != null)
									{
                                        numberOfAccounts++;
                                        remainingBalanceTotal += calculatedREAccount.AccountSummary.Balance;
                                        accountsAlreadyCalculated.Add(accountToCalculate);
									}

									break;
								case "ut":
									var calculatedUTAccount = commandDispatcher
										.Send(new GetCalculatedUtilityAccount(accountToCalculate.Account, DateTime.Today)).Result;
									if (calculatedUTAccount != null)
									{
                                        numberOfAccounts++;
                                        remainingBalanceTotal += calculatedUTAccount.AccountSummary.Balance;
                                        accountsAlreadyCalculated.Add(accountToCalculate);
									}
									break;

                                case "ar":
                                    var calculatedARAccount = commandDispatcher
                                        .Send(new GetCalculatedAccountsReceivableAccount(accountToCalculate.Account, DateTime.Today)).Result;
                                    if (calculatedARAccount != null)
                                    {
                                        numberOfAccounts++;
                                        remainingBalanceTotal += calculatedARAccount.AccountSummary.Balance;
                                        accountsAlreadyCalculated.Add(accountToCalculate);
                                    }
                                    break;
							}

							
                        }
					}

                    foreach (var groupToCalculate in groupNumbersToCalculate)
                    {
                        var groupResults =  commandDispatcher.Send(new CalculateAccountGroup(groupToCalculate, DateTime.Today)).Result;

                        if (groupResults != null)
                        {
                            foreach (var accountSummary in groupResults.AccountSummaries)
                            {
                                if (!accountsAlreadyCalculated.Any(x => x.Account == accountSummary.Account && x.Type == ConvertBillingTypeToType(accountSummary.AccountType)))
                                {
                                    numberOfAccounts++;
                                    remainingBalanceTotal += accountSummary.Balance;
									accountsAlreadyCalculated.Add(new AccountInfo
                                    {
										Type = ConvertBillingTypeToType(accountSummary.AccountType),
										Account = accountSummary.Account
									});
                                }
                            }
                        }
                    }

					var result = new string(' ', globalCashReceiptSettings.ReceiptOffset);

					if (remainingBalanceTotal == 0)
                    {
                        return result + "Remaining Balance: 0.00";
                    }
                    else if (numberOfAccounts <= 1)
                    {
                        return result + "Remaining Balance: " + remainingBalanceTotal.FormatAsCurrencyNoSymbol();
                    }
                    else
                    {
                        return result + "Remaining Balance: (" + numberOfAccounts.FormatAsNumber() + ") " + remainingBalanceTotal.FormatAsCurrencyNoSymbol();
                    }
				}
            }
			catch (Exception ex)
			{
				telemetryService.TrackException(ex);
                return "";
			}
        }

		private int ReceiptTypeToInteger(string receiptType)
        {
            if (receiptType.ToIntegerValue() > 0)
            {
                return receiptType.ToIntegerValue();
            }
            switch (receiptType.ToUpper())
            {
                case "RETAX":
                    return 90;
                case "RETAXLIEN":
                    return 91;
                case "PPTAX":
                    return 92;
                case "TARETAXLIEN":
                    return 891;
                case "TARETAX":
                    return 890;
                case "DOG":
                    return 800;
                case "DEA":
                    return 801;
                case "BIR":
                    return 802;
                case "MAR":
                    return 803;
                case "BUR":
                    return 804;
            }
            return 0;
        }

        private AccountType ConvertBillingTypeToType(AccountBillingType billingType)
        {
            return billingType == AccountBillingType.RealEstate
                ? AccountType.RealEstate
                : billingType == AccountBillingType.PersonalProperty
                    ? AccountType.PersonalProperty
                    : AccountType.UtilityBilling;
        }
	}
}
