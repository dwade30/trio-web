﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;

namespace SharedDataAccess.CashReceipts
{
	class SavedStatusReportsQueryHandler : IQueryHandler<SavedStatusReportsSearchCriteria, IEnumerable<SavedStatusReport>>
	{
		private ICashReceiptsContext cashReceiptsContext;

		public SavedStatusReportsQueryHandler(ICashReceiptsContext cashReceiptsContext)
		{
			this.cashReceiptsContext = cashReceiptsContext;
		}
		public IEnumerable<SavedStatusReport> ExecuteQuery(SavedStatusReportsSearchCriteria query)
		{
			IQueryable<SavedStatusReport> savedStatusReportQuery;

			savedStatusReportQuery = cashReceiptsContext.SavedStatusReports;

			if (query.Id > 0)
			{
				savedStatusReportQuery = savedStatusReportQuery.Where(x => x.Id == query.Id);
			}

			return savedStatusReportQuery.AsNoTracking().ToList();
		}
	}
}
