﻿using System.Linq;
using System.Threading;
using SharedApplication;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.CashReceipts
{
    public class GetNewReceiptNumberHandler : CommandHandler<GetNewReceiptNumber,int>
    {
        
        private CashReceiptsContext crContext;
        private cGlobalSettings globalSettings;
        public GetNewReceiptNumberHandler(ITrioContextFactory contextFactory,cGlobalSettings globalSettings)
        {
            this.crContext = contextFactory.GetCashReceiptsContext();
            this.globalSettings = globalSettings;
        }
        protected override int Handle(GetNewReceiptNumber command)
        {
            var nextReceiptNumber = 0;
            var dataEnvironment = globalSettings.DataEnvironment;
            using (Mutex mutex = new Mutex(false, dataEnvironment + "_CRReceiptNumber"))
            {
                if (mutex.WaitOne(2000))
                {
                    var rNumLock = crContext.RNumLocks.FirstOrDefault();
                    if (rNumLock == null)
                    {
                        rNumLock = new RNumLock(){NextReceiptNumber = 1, OpID = "", ReceiptNumberLock = false};
                        crContext.RNumLocks.Add(rNumLock);
                    }

                    nextReceiptNumber = rNumLock.NextReceiptNumber.GetValueOrDefault();
                    rNumLock.NextReceiptNumber += 1;
                    crContext.SaveChanges();
                }
                mutex.ReleaseMutex();
            }

            return nextReceiptNumber;
        }
    }
}