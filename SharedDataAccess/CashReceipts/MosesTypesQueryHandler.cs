﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts
{
    public class MosesTypesQueryHandler : IQueryHandler<MosesTypesQuery,IEnumerable<MOSESType>>
    {
        private ICashReceiptsContext crContext;
        public MosesTypesQueryHandler(ICashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }

        public IEnumerable<MOSESType> ExecuteQuery(MosesTypesQuery query)
        {
            return crContext.MosesTypes.ToList();
        }
    }
}