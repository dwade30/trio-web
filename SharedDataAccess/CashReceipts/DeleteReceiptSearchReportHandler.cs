﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;
using SharedDataAccess.UtilityBilling;

namespace SharedDataAccess.CashReceipts
{
	public class DeleteReceiptSearchReportHandler : CommandHandler<DeleteReceiptSearchReport, bool>
	{
		private CashReceiptsContext cashReceiptsContext;

		public DeleteReceiptSearchReportHandler(ITrioContextFactory trioContextFactory)
		{
			cashReceiptsContext = trioContextFactory.GetCashReceiptsContext();
		}

		protected override bool Handle(DeleteReceiptSearchReport command)
		{
			try
			{
				var report = cashReceiptsContext.SavedStatusReports.FirstOrDefault(x =>
					x.Id == command.Id);

				if (report != null)
				{
					cashReceiptsContext.SavedStatusReports.Remove(report);
					cashReceiptsContext.SaveChanges();
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
