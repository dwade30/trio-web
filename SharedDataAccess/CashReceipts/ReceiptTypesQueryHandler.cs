﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts
{
    public class ReceiptTypesQueryHandler : IQueryHandler<ReceiptTypesQuery,IEnumerable<ReceiptType>>
    {
        private ICashReceiptsContext cashReceiptsContext;
        public ReceiptTypesQueryHandler(ITrioContextFactory contextFactory)
        {
            this.cashReceiptsContext = contextFactory.GetCashReceiptsContext();
        }
        public IEnumerable<ReceiptType> ExecuteQuery(ReceiptTypesQuery query)
        {
            var receiptTypes = cashReceiptsContext.ReceiptTypes.Where(r => r.TownKey == query.TownNumber).OrderBy(r => r.TypeCode);
            return receiptTypes.ToList();

        }
    }
}