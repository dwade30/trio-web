﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedDataAccess.CashReceipts
{
	public class FinalizeMiscReceiptTransactionCommandHandler : CommandHandler<FinalizeTransactions<MiscReceiptTransactionBase>, bool>
	{
		private CashReceiptsContext cashReceiptsContext;
		

		public FinalizeMiscReceiptTransactionCommandHandler(ITrioContextFactory trioContextFactory)
		{
			cashReceiptsContext = trioContextFactory.GetCashReceiptsContext();
		}

		protected override bool Handle(FinalizeTransactions<MiscReceiptTransactionBase> command)
		{
			return true;
		}
	}
}
