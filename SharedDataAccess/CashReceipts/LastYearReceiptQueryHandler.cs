﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts
{
	public class LastYearReceiptQueryHandler : IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>
	{
		private ICashReceiptsContext cashReceiptsContext;

		public LastYearReceiptQueryHandler(ITrioContextFactory contextFactory)
		{
			this.cashReceiptsContext = contextFactory.GetCashReceiptsContext();
		}

		public IEnumerable<LastYearReceipt> ExecuteQuery(LastYearReceiptSearchCriteria query)
		{
			IQueryable<LastYearReceipt> lastYearReceiptQuery;

			lastYearReceiptQuery = cashReceiptsContext.LastYearReceipts;

			if (query.ReceiptId > 0)
			{
				lastYearReceiptQuery = lastYearReceiptQuery.Where(x => x.Id == query.ReceiptId);
			}

			if (query.ReceiptDate != null)
			{
				lastYearReceiptQuery = lastYearReceiptQuery.Where(x => ((DateTime)x.LastYearReceiptDate).Date == ((DateTime)query.ReceiptDate).Date);
			}

			return lastYearReceiptQuery.ToList();
		}
	}

}
