﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedDataAccess.CashReceipts.Configuration;
using SharedDataAccess.TaxCollections;

namespace SharedDataAccess.CashReceipts
{
	public partial class CashReceiptsContext : DbContext, ICashReceiptsContext 
	{
		public DbSet<Archive> Archives { get; set; }
		public DbSet<Receipt> Receipts { get; set; }
		public DbSet<LastYearReceipt> LastYearReceipts { get; set; }
        public DbSet<LastYearArchive> LastYearArchives { get; set; }
		public DbSet<ReceiptType> ReceiptTypes { get; set; }
        public DbSet<CRBank> Banks { get; set; }
        public DbSet<RNumLock> RNumLocks { get; set; }
        public DbSet<MOSESType> MosesTypes { get; set; }
        public DbSet<CreditCardType> CreditCardTypes { get; set; }
        public DbSet<CreditCardMaster> CreditCardMasters { get; set; }
        public DbSet<CheckMaster> CheckMasters { get; set; }
        public DbSet<Cya> CYAs { get; set; }
        public DbSet<CloseOut> CloseOuts { get; set; }
        public DbSet<SavedStatusReport> SavedStatusReports { get; set; }
        public DbSet<LastYearCreditCardMaster> LastYearCreditCardMasters { get; set; }
        public DbSet<LastYearCheckMaster> LastYearCheckMasters { get; set; }
		public DbSet<ElectronicPaymentLog> ElectronicPaymentLogs { get; set; }
		public DbSet<ServiceCode> ServiceCodes { get; set; }
		IQueryable<Receipt> ICashReceiptsContext.Receipts { get => Receipts; }
		IQueryable<LastYearReceipt> ICashReceiptsContext.LastYearReceipts { get => LastYearReceipts; }

		IQueryable<Archive> ICashReceiptsContext.Archives { get => Archives; }
		IQueryable<ReceiptType> ICashReceiptsContext.ReceiptTypes { get => ReceiptTypes; }
        IQueryable<CRBank> ICashReceiptsContext.Banks {get => Banks;}
		IQueryable<CreditCardType> ICashReceiptsContext.CreditCardTypes { get => CreditCardTypes; }
		IQueryable<CreditCardMaster> ICashReceiptsContext.CreditCardMasters { get => CreditCardMasters; }
		IQueryable<CheckMaster> ICashReceiptsContext.CheckMasters { get => CheckMasters; }
		IQueryable<Cya> ICashReceiptsContext.CYAs { get => CYAs; }
		IQueryable<CloseOut> ICashReceiptsContext.CloseOuts { get => CloseOuts; }
		IQueryable<SavedStatusReport> ICashReceiptsContext.SavedStatusReports { get => SavedStatusReports; }
		IQueryable<LastYearArchive> ICashReceiptsContext.LastYearArchives { get => LastYearArchives; }
		IQueryable<LastYearCreditCardMaster> ICashReceiptsContext.LastYearCreditCardMasters { get => LastYearCreditCardMasters; }
		IQueryable<LastYearCheckMaster> ICashReceiptsContext.LastYearCheckMasters { get => LastYearCheckMasters; }

		IQueryable<MOSESType> ICashReceiptsContext.MosesTypes
        {
            get => MosesTypes;
        }
		IQueryable<ElectronicPaymentLog> ICashReceiptsContext.ElectronicPaymentLogs
        {
            get => ElectronicPaymentLogs;
        }
		IQueryable<ServiceCode> ICashReceiptsContext.ServiceCodes
        {
            get => ServiceCodes;
        }
		public CashReceiptsContext()
		{
		}

		public CashReceiptsContext(DbContextOptions<CashReceiptsContext> options) : base(options)
		{
		}

		public CashReceiptsContext(string connectionString)
			: this(new DbContextOptionsBuilder<CashReceiptsContext>()
				.UseSqlServer(connectionString).Options)
		{ }


		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.ApplyConfiguration(new ArchiveConfiguration());
			modelBuilder.ApplyConfiguration(new ReceiptConfiguration());
			modelBuilder.ApplyConfiguration(new ReceiptTypeConfiguration());
			modelBuilder.ApplyConfiguration(new LastYearReceiptConfiguration());
            modelBuilder.ApplyConfiguration(new CRBankConfiguration());
            modelBuilder.ApplyConfiguration(new RNumLockConfiguration());
            modelBuilder.ApplyConfiguration(new MosesTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CreditCardTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CreditCardMasterConfiguration());
            modelBuilder.ApplyConfiguration(new CheckMasterConfiguration());
            modelBuilder.ApplyConfiguration(new CYAConfiguration());
            modelBuilder.ApplyConfiguration(new CloseOutConfiguration());
            modelBuilder.ApplyConfiguration(new SavedStatusReportConfiguration());
            modelBuilder.ApplyConfiguration(new LastYearArchiveConfiguration());
            modelBuilder.ApplyConfiguration(new LastYearCheckMasterConfiguration());
            modelBuilder.ApplyConfiguration(new LastYearCreditCardMasterConfiguration());
            modelBuilder.ApplyConfiguration(new ElectronicPaymentLogConfiguration());
            modelBuilder.ApplyConfiguration(new ServiceCodeConfiguration());

			modelBuilder.Entity<Receipt>()
				.HasMany(c => c.Archives)
				.WithOne(e => e.Receipt)
				.HasForeignKey(f => f.ReceiptId);

            modelBuilder.Entity<Receipt>()
	            .HasMany(c => c.CheckMasters)
	            .WithOne(e => e.Receipt)
	            .HasForeignKey(f => f.ReceiptId);

            modelBuilder.Entity<Receipt>()
	            .HasMany(c => c.CreditCardMasters)
	            .WithOne(e => e.Receipt)
	            .HasForeignKey(f => f.ReceiptId);
			
            modelBuilder.Entity<LastYearReceipt>()
	            .HasMany(c => c.Archives)
	            .WithOne(e => e.Receipt)
	            .HasForeignKey(f => f.ReceiptId)
	            .HasPrincipalKey(k => k.ReceiptKey);

            modelBuilder.Entity<LastYearReceipt>()
	            .HasMany(c => c.CheckMasters)
	            .WithOne(e => e.Receipt)
				.HasForeignKey(f => f.ReceiptId)
	            .HasPrincipalKey(k => k.ReceiptKey);

			modelBuilder.Entity<LastYearReceipt>()
	            .HasMany(c => c.CreditCardMasters)
	            .WithOne(e => e.Receipt)
				.HasForeignKey(f => f.ReceiptId)
	            .HasPrincipalKey(k => k.ReceiptKey);

		}
	}
}
