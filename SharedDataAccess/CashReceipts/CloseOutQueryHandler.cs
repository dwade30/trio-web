﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts
{
	public class CloseOutQueryHandler : IQueryHandler<CloseOutSearchCriteria, IEnumerable<CloseOut>>
	{
		private ICashReceiptsContext cashReceiptsContext;

		public CloseOutQueryHandler(ICashReceiptsContext cashReceiptsContext)
		{
			this.cashReceiptsContext = cashReceiptsContext;
		}
		public IEnumerable<CloseOut> ExecuteQuery(CloseOutSearchCriteria query)
		{
			IQueryable<CloseOut> closeOutQuery;

			closeOutQuery = cashReceiptsContext.CloseOuts;

			if (query.Type == CloseOutType.FullCloseoutOnly)
			{
				closeOutQuery = closeOutQuery.Where(x => x.Type == "R");
			}
			else if (query.Type == CloseOutType.TellerCloseoutOnly)
			{
				closeOutQuery = closeOutQuery.Where(x => x.Type == "T");
			}

			return closeOutQuery.ToList();
		}
	}
}
