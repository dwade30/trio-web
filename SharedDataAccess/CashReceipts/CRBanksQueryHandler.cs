﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts
{
    public class CRBanksQueryHandler : IQueryHandler<CRBanksQuery,IEnumerable<CRBank>>
    {
        private ICashReceiptsContext cashReceiptsContext;

        public CRBanksQueryHandler(ICashReceiptsContext cashReceiptsContext)
        {
            this.cashReceiptsContext = cashReceiptsContext;
        }
        public IEnumerable<CRBank> ExecuteQuery(CRBanksQuery query)
        {
            return cashReceiptsContext.Banks.OrderBy(b => b.Name).ToList();
        }
    }
}