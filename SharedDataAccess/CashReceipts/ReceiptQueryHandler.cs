﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts
{
	public class ReceiptQueryHandler : IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>
	{
        private ITrioContextFactory contextFactory;

		public ReceiptQueryHandler(ITrioContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }

		public IEnumerable<Receipt> ExecuteQuery(ReceiptSearchCriteria query)
		{
			try
			{
                var cashReceiptsContext = contextFactory.GetCashReceiptsContext();

				IQueryable<Receipt> receiptQuery;

				receiptQuery = cashReceiptsContext.Receipts;

				if (query.ReceiptId > 0)
				{
					receiptQuery = receiptQuery.Where(x => x.Id == query.ReceiptId);
				}

				if (query.ReceiptNumber > 0)
				{
					receiptQuery = receiptQuery.Where(x => x.ReceiptNumber == query.ReceiptNumber);
				}

				if (query.ReceiptDate != null)
				{
					receiptQuery = receiptQuery.Where(x => ((DateTime)x.ReceiptDate).Date == ((DateTime)query.ReceiptDate).Date);
				}

                if (query.VoidedCriteria != VoidedSearchOption.All)
                {
                    if (query.VoidedCriteria == VoidedSearchOption.VoidedOnly)
                    {
                        receiptQuery = receiptQuery.Where(x => x.IsVoided ?? false);
					}
					else if (query.VoidedCriteria == VoidedSearchOption.NotVoidedOnly)
                    {
                        receiptQuery = receiptQuery.Where(x => !x.IsVoided ?? false);
					}
                }

				var result = receiptQuery.Include(x => x.Archives).ToList();

				return result;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}

}
