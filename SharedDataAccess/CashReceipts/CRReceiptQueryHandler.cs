﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.Models;

namespace SharedDataAccess.CashReceipts
{
    public class CRReceiptQueryHandler : IQueryHandler<CRReceiptQuery,Receipt>
    {
        private ICashReceiptsContext crContext;

        public CRReceiptQueryHandler(ICashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }
        public Receipt ExecuteQuery(CRReceiptQuery query)
        {
            Receipt receipt;
            if (crContext.Receipts.Any(r => r.Id == query.Id))
            {
                return GetCurrentReceipt(query.Id);
            }
            else if (crContext.LastYearReceipts.Any(l => l.Id == query.Id))
            {
                return GetArchivedReceipt(query.Id);
            }
            else
            {
                return GetEmptyReceipt();
            }
        }

        private Receipt GetEmptyReceipt()
        {
            return new Receipt()
            {
                Archives = new List<Archive>(),
                CashAmount = 0,
                CheckAmount = 0,
                CheckMasters = new List<CheckMaster>(),
                Change = 0,
                ConvenienceFee = 0,
                CreditCardMasters = new List<CreditCardMaster>(),
                CardAmount = 0,
                EFT = false,
                Id = 0,
                ReceiptIdentifier = Guid.Empty,
                MultipleChecks = false,
                ReceiptDate = DateTime.MinValue,
                MultipleCC = false,
                TellerID = "",
                PaidBy = "",
                ReceiptNumber = 0,
                IsEPayment = false,
                PaidByPartyID = 0
            };
        }

        private Receipt GetCurrentReceipt(int id)
        {
            var receipt = crContext.Receipts.FirstOrDefault(r => r.Id == id);
            var archives = crContext.Archives.Where(a => a.ReceiptId == id).ToList();
            receipt.Archives = archives;
            receipt.CreditCardMasters = crContext.CreditCardMasters.Where(c => c.ReceiptId == id).ToList();
            receipt.CheckMasters = crContext.CheckMasters.Where(c => c.ReceiptId == id).ToList();
            return receipt;
        }

        private Receipt GetArchivedReceipt(int id)
        {
            var archivedReceipt = crContext.LastYearReceipts.FirstOrDefault(l => l.Id == id);
            var receipt = new Receipt()
            {
                CashAmount = archivedReceipt.CashAmount.GetValueOrDefault(),
                Id = id,
                CheckAmount = archivedReceipt.CheckAmount.GetValueOrDefault(),
                EFT = archivedReceipt.EFT.GetValueOrDefault(),
                CardAmount = archivedReceipt.CardAmount.GetValueOrDefault(),
                Change =  archivedReceipt.Change.GetValueOrDefault(),
                ConvenienceFee = archivedReceipt.ConvenienceFee.GetValueOrDefault(),
                IsEPayment = archivedReceipt.IsEPayment.GetValueOrDefault(),
                MultipleChecks = archivedReceipt.MultipleChecks.GetValueOrDefault(),
                MultipleCC = archivedReceipt.MultipleCC.GetValueOrDefault(),
                PaidBy = archivedReceipt.PaidBy,
                PaidByPartyID = archivedReceipt.PaidByPartyId.GetValueOrDefault(),
                ReceiptDate = archivedReceipt.LastYearReceiptDate,
                ReceiptNumber = archivedReceipt.ReceiptNumber.GetValueOrDefault(),
                TellerID = archivedReceipt.TellerID
            };
            receipt.Archives =
                crContext.LastYearArchives
                    .Where(l => l.ReceiptId == archivedReceipt.ReceiptKey.GetValueOrDefault()).ToList().ToArchives().ToList();
            receipt.CreditCardMasters = crContext.LastYearCreditCardMasters.Where(c => c.OriginalReceiptKey.GetValueOrDefault() == archivedReceipt.ReceiptKey.GetValueOrDefault()).ToList().ToCreditCardMasters().ToList();
            receipt.CheckMasters = crContext.LastYearCheckMasters.Where(c => c.ReceiptId == archivedReceipt.ReceiptKey.GetValueOrDefault()).ToList().ToCheckMasters().ToList();
            return receipt;
        }

       

        
    }
}