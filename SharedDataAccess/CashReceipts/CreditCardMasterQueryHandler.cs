﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;

namespace SharedDataAccess.CashReceipts
{
	public class CreditCardMasterQueryHandler : IQueryHandler<CreditCardMasterSearchCriteria, IEnumerable<CreditCardMaster>>
	{
		private ICashReceiptsContext cashReceiptsContext;

		public CreditCardMasterQueryHandler(ICashReceiptsContext cashReceiptsContext)
		{
			this.cashReceiptsContext = cashReceiptsContext;
		}
		public IEnumerable<CreditCardMaster> ExecuteQuery(CreditCardMasterSearchCriteria query)
		{
			IQueryable<CreditCardMaster> creditCardMasterQuery;

			creditCardMasterQuery = cashReceiptsContext.CreditCardMasters;

			if (query.ReceiptId != 0)
			{
				creditCardMasterQuery = creditCardMasterQuery.Where(x => x.ReceiptId == query.ReceiptId);
			}

			return creditCardMasterQuery.ToList();
		}
	}
}
