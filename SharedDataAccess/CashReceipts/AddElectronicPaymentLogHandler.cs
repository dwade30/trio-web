﻿using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Messaging;

namespace SharedDataAccess.CashReceipts
{
    public class AddElectronicPaymentLogHandler : CommandHandler<AddElectronicPaymentLog,int>
    {
        private CashReceiptsContext crContext;

        public AddElectronicPaymentLogHandler(CashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }
        protected override int Handle(AddElectronicPaymentLog command)
        {
            var log = new ElectronicPaymentLog()
            {
                Amount =  command.PaymentLog.Amount,
                Success = command.PaymentLog.Success,
                ErrorDescription = command.PaymentLog.ErrorDescription,
                StatusCode =  command.PaymentLog.StatusCode,
                ReferenceId = command.PaymentLog.ReferenceId,
                ResponseData = command.PaymentLog.ResponseData,
                MerchantCode = command.PaymentLog.MerchantCode,
                Fee = command.PaymentLog.Fee,
                TransactionGroupId = command.PaymentLog.TransactionGroupId,
                InTestMode = command.PaymentLog.InTestMode,
                ElectronicPaymentIdentifier = command.PaymentLog.ElectronicPaymentIdentifier,
                TellerId = command.PaymentLog.TellerId,
                PaymentTime = command.PaymentLog.PaymentTime
            };
            crContext.ElectronicPaymentLogs.Add(log);
            crContext.SaveChanges();
            return log.Id;
        }
    }
}