﻿using System.Linq;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralData
{
	public class SaveSettingHandler : CommandHandler<SaveSetting>
	{
		private CentralDataContext centralDataContext;
		private cGlobalSettings globalSettings;
		private UserInformation userInformation;

		public SaveSettingHandler(ITrioContextFactory trioContextFactory, cGlobalSettings globalSettings, UserInformation userInformation)
		{
			centralDataContext = trioContextFactory.GetCentralDataContext();
			this.globalSettings = globalSettings;
			this.userInformation = userInformation;

		}
		protected override void Handle(SaveSetting command)
		{
			string ownerId = command.type == SettingOwnerType.Global ? "" :
				command.type == SettingOwnerType.Machine ? globalSettings.MachineOwnerID :
				userInformation.Id.ToString();
			string ownerType = command.type == SettingOwnerType.Global ? "" :
				command.type == SettingOwnerType.Machine ? "Machine" : "User";

			var setting = centralDataContext.Settings.FirstOrDefault(x =>
				x.Owner == ownerId && x.OwnerType == ownerType && x.SettingType == (command.setting.SettingType ?? "") &&
				x.SettingName == command.setting.SettingName);

			if (setting != null)
			{
				setting.SettingValue = command.setting.SettingValue;
				if (setting.SettingType == null)
				{
					setting.SettingType = "";
				}
			}
			else
			{
				centralDataContext.Settings.Add(new Setting
				{
					Owner = ownerId,
					OwnerType = ownerType,
					SettingType = command.setting.SettingType ?? "", 
					SettingName = command.setting.SettingName,
					SettingValue = command.setting.SettingValue,
				});
			}

			centralDataContext.SaveChanges();
		}
	}
}
