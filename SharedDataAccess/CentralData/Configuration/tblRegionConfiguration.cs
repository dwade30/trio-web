﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData
{
    public class tblRegionConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.ToTable("tblRegions");
            builder.Property(p => p.TownName).HasMaxLength(255);
            builder.Property(p => p.TownAbbreviation).HasMaxLength(50);
            builder.Property(p => p.ResCode).HasMaxLength(255);
            builder.Property(p => p.Fund).HasMaxLength(255);
            builder.Property(p => p.RECommitmentAccount).HasMaxLength(255);
            builder.Property(p => p.PPCommitmentAccount).HasMaxLength(255);
            builder.Property(p => p.RESupplementalAccount).HasMaxLength(255);
            builder.Property(p => p.PPSupplementalAccount).HasMaxLength(255);
        }
    }
}
