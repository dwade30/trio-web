﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData.Configuration
{
	public class RegionalTownBreakdownConfiguration : IEntityTypeConfiguration<RegionalTownBreakdown>
	{
		public void Configure(EntityTypeBuilder<RegionalTownBreakdown> builder)
		{
			builder.ToTable("RegionalTownBreakdown");
		}
	}
}
