﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData.Configuration
{
	public class SettingConfiguration : IEntityTypeConfiguration<Setting>
	{
		public void Configure(EntityTypeBuilder<Setting> builder)
		{
			builder.ToTable("Settings");
			builder.Property(p => p.SettingName).HasMaxLength(255);
			builder.Property(p => p.SettingValue).HasMaxLength(255);
			builder.Property(p => p.SettingType).HasMaxLength(255);
			builder.Property(p => p.Owner).HasMaxLength(255);
			builder.Property(p => p.OwnerType).HasMaxLength(255);
		}
	}
}
