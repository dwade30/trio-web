﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData.Configuration
{
	public class ModuleAssociationConfiguration : IEntityTypeConfiguration<ModuleAssociation>
	{
		public void Configure(EntityTypeBuilder<ModuleAssociation> builder)
		{
			builder.ToTable("ModuleAssociation");
			builder.Property(p => p.Module).HasMaxLength(255);
		}
	}
}
