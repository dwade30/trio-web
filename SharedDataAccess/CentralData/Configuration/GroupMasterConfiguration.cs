﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData.Configuration
{
	public class GroupMasterConfiguration : IEntityTypeConfiguration<GroupMaster>
	{
		public void Configure(EntityTypeBuilder<GroupMaster> builder)
		{
			builder.ToTable("GroupMaster");
		}
	}
}
