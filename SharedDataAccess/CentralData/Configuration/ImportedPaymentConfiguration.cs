﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData.Configuration
{
	public class ImportedPaymentConfiguration : IEntityTypeConfiguration<ImportedPayment>
	{
		public void Configure(EntityTypeBuilder<ImportedPayment> builder)
		{
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.AppliedDateTime).HasColumnType("datetime");
			builder.Property(e => e.BillNumber)
				.IsRequired()
				.HasMaxLength(25)
				.IsUnicode(false);
			builder.Property(e => e.BillType)
				.IsRequired()
				.HasMaxLength(25)
				.IsUnicode(false);
			builder.Property(e => e.PaidBy)
				.IsRequired()
				.HasMaxLength(255)
				.IsUnicode(false);
			builder.Property(e => e.PaymentAmount).HasColumnType("decimal(18, 2)");
			builder.Property(e => e.PaymentDate).HasColumnType("datetime");
			builder.Property(e => e.PaymentIdentifier).HasMaxLength(64);
		}
	}
}
