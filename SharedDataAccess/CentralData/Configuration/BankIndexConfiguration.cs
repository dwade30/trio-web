﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData.Configuration
{
    public class BankIndexConfiguration : IEntityTypeConfiguration<BankIndex>
    {
        public void Configure(EntityTypeBuilder<BankIndex> builder)
        {
            builder.ToTable("BankIndex");
            builder.Property(e => e.Id).HasColumnName("ID");
        }
    }
}