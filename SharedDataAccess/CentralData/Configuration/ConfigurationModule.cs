﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			RegisterRegionSearchQueryHandler(ref builder);
			RegisterSettingSearchQueryHandler(ref builder);
			RegisterGroupMasterQueryHandler(ref builder);
			RegisterModuleAssociationQueryHandler(ref builder);
            RegisterCentralDataContext(ref builder);
        }

        private void RegisterCentralDataContext(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var activeModules = f.Resolve<IGlobalActiveModuleSettings>();
                var tcf = f.Resolve<ITrioContextFactory>();
                return tcf.GetCentralDataContext();
            }).As<ICentralDataContext>();
        }

		private void RegisterRegionSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<RegionQueryHandler>()
				.As<IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
		}

		private void RegisterSettingSearchQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<SettingQueryHandler>()
				.As<IQueryHandler<SettingSearchCriteria, IEnumerable<Setting>>>();
		}

		private void RegisterGroupMasterQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<GroupMasterQueryHandler>()
				.As<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
		}

		private void RegisterModuleAssociationQueryHandler(ref ContainerBuilder builder)
		{
			builder.RegisterType<ModuleAssociationQueryHandler>()
				.As<IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>>();
		}
	}
}
