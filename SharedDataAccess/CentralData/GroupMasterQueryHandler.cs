﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData
{
	public class GroupMasterQueryHandler : IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>
	{
		private ICentralDataContext centralDataContext;

		public GroupMasterQueryHandler(ITrioContextFactory contextFactory)
		{
			this.centralDataContext = contextFactory.GetCentralDataContext();
		}

		public IEnumerable<GroupMaster> ExecuteQuery(GroupMasterSearchCriteria query)
		{
			IQueryable<GroupMaster> groupMasterQuery;

			groupMasterQuery = centralDataContext.GroupMasters;

			if (query.AssociatedWith != null)
			{
				var associatedResult = groupMasterQuery
					.Join(centralDataContext.ModuleAssociations,
						group => group.ID,
						association => association.GroupNumber,
						(group, association) => new { Group = group, Association = association })
					.Where(z => z.Association.Module == query.AssociatedWith.Module && z.Association.Account == query.AssociatedWith.AccountNumber);

				return associatedResult.Select(x => x.Group).ToList();
			}
			else
			{
				if (query.GroupNumber != 0)
				{
					groupMasterQuery = groupMasterQuery.Where(x => x.GroupNumber == query.GroupNumber);
				}

				return groupMasterQuery.ToList();
			}
		}
	}
}
