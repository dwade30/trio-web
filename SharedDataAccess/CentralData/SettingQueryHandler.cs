﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;

namespace SharedDataAccess.CentralData
{
	public class SettingQueryHandler : IQueryHandler<SettingSearchCriteria, IEnumerable<Setting>>
	{
		private ICentralDataContext centralDataContext;
		private cGlobalSettings globalSettings;
		private UserInformation userInformation;

		public SettingQueryHandler(ITrioContextFactory contextFactory, cGlobalSettings globalSettings, UserInformation userInformation)
		{
			this.centralDataContext = contextFactory.GetCentralDataContext();
			this.globalSettings = globalSettings;
			this.userInformation = userInformation;
		}

		public IEnumerable<Setting> ExecuteQuery(SettingSearchCriteria query)
		{
			IQueryable<Setting> settingQuery;

			settingQuery = centralDataContext.Settings.AsNoTracking();
			
			switch (query.OwnerType)
            {
                case SettingOwnerType.Machine:
                    settingQuery = settingQuery.Where(x => x.OwnerType == "Machine");

                    break;
                case SettingOwnerType.User:
                    settingQuery = settingQuery.Where(x => x.OwnerType == "User");

                    break;
                default:
                    settingQuery = settingQuery.Where(x => (x.OwnerType ?? "") == "");

                    break;
            }

			if ((query.Owner ?? "") != "")
			{
				settingQuery = settingQuery.Where(x => x.Owner == query.Owner);
			}
			else
            {
                switch (query.OwnerType)
                {
                    case SettingOwnerType.Machine:
                        settingQuery = settingQuery.Where(x => x.Owner == globalSettings.MachineOwnerID);

                        break;
                    case SettingOwnerType.User:
                        settingQuery = settingQuery.Where(x => x.Owner == userInformation.Id.ToString());

                        break;
                    default:
                        settingQuery = settingQuery.Where(x => (x.Owner ?? "") == "");

                        break;
                }
            }

			if ((query.SettingType ?? "") != "")
			{
				settingQuery = settingQuery.Where(x => x.SettingType == query.SettingType);
			}

			if ((query.SettingName ?? "") != "")
			{
				settingQuery = settingQuery.Where(x => x.SettingName == query.SettingName);
			}

			return settingQuery.ToList();
		}
	}
}
