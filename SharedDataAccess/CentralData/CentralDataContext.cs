﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedDataAccess.CentralData.Configuration;

//using SharedDataAccess.CentralData.Configuration;

namespace SharedDataAccess.CentralData
{
    public class CentralDataContext : DbContext, ICentralDataContext
	{
        public DbSet<BankIndex> DefaultBanks { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<RegionalTownBreakdown> RegionalTownBreakdowns { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<GroupMaster> GroupMasters { get; set; }
        public DbSet<ModuleAssociation> ModuleAssociations { get; set; }
        public DbSet<ImportedPayment> ImportedPayments { get; set; }

        IQueryable<BankIndex> ICentralDataContext.DefaultBanks => DefaultBanks;
        IQueryable<Region> ICentralDataContext.Regions { get => Regions; }
		IQueryable<RegionalTownBreakdown> ICentralDataContext.RegionalTownBreakdowns{get => RegionalTownBreakdowns;}
		IQueryable<Setting> ICentralDataContext.Settings { get => Settings; }
		IQueryable<GroupMaster> ICentralDataContext.GroupMasters { get => GroupMasters; }
		IQueryable<ModuleAssociation> ICentralDataContext.ModuleAssociations { get => ModuleAssociations; }
		IQueryable<ImportedPayment> ICentralDataContext.ImportedPayments { get => ImportedPayments; }

        public CentralDataContext()
        {
        }

		public CentralDataContext(DbContextOptions<CentralDataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new BankIndexConfiguration());
            modelBuilder.ApplyConfiguration(new tblRegionConfiguration());
            modelBuilder.ApplyConfiguration(new RegionalTownBreakdownConfiguration());
            modelBuilder.ApplyConfiguration(new SettingConfiguration());
            modelBuilder.ApplyConfiguration(new GroupMasterConfiguration());
            modelBuilder.ApplyConfiguration(new ModuleAssociationConfiguration());
            modelBuilder.ApplyConfiguration(new ImportedPaymentConfiguration());
        }
    }
}
