﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData
{
	public class ModuleAssociationQueryHandler : IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>
	{
		private ICentralDataContext centralDataContext;

		public ModuleAssociationQueryHandler(ITrioContextFactory contextFactory)
		{
			this.centralDataContext = contextFactory.GetCentralDataContext();
		}

		public IEnumerable<ModuleAssociation> ExecuteQuery(ModuleAssociationSearchCriteria query)
		{
			IQueryable<ModuleAssociation> moduleAssociationQuery;

			moduleAssociationQuery = centralDataContext.ModuleAssociations.Where(x => x.PrimaryAssociate ?? false);

			if (query.Module != null)
			{
				moduleAssociationQuery = moduleAssociationQuery.Where(x => x.Module == query.Module);
			}
			
			if (query.REMasterAccount != 0)
			{
				moduleAssociationQuery = moduleAssociationQuery.Where(x => x.REMasterAcct == query.REMasterAccount);
			}

            if (query.AccountNumber != 0)
            {
                moduleAssociationQuery = moduleAssociationQuery.Where(x => x.Account == query.AccountNumber);
            }

			return moduleAssociationQuery.ToList();
			
		}
	}
}
