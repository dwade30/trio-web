﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.CentralData
{
	public class RegionQueryHandler : IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>
	{
		private ICentralDataContext centralDataContext;

		public RegionQueryHandler(ITrioContextFactory contextFactory)
		{
			this.centralDataContext = contextFactory.GetCentralDataContext();
		}

		public IEnumerable<Region> ExecuteQuery(RegionSearchCriteria query)
		{
			IQueryable<Region> regionQuery;

			regionQuery = centralDataContext.Regions;

			if (query.RegionCode > 0)
			{
				regionQuery = regionQuery.Where(x => x.TownNumber == query.RegionCode);
			}

			return regionQuery.ToList();
		}
	}
}
