﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.AccountGroups;
using SharedApplication.AccountGroups.Commands;
using SharedApplication.CentralData;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.UtilityBilling.Commands;

namespace SharedDataAccess.AccountGroups
{
    public class CalculateAccountGroupHandler: CommandHandler<CalculateAccountGroup,AccountGroup>
    {
        private ICentralDataContext centralDataContext;
        private CommandDispatcher commandDispatcher;
        public CalculateAccountGroupHandler(ICentralDataContext centralDataContext, CommandDispatcher commandDispatcher)
        {
            this.centralDataContext = centralDataContext;
            this.commandDispatcher = commandDispatcher;
        }
        protected override AccountGroup Handle(CalculateAccountGroup command)
        {
            var groupMaster = centralDataContext.GroupMasters.FirstOrDefault(g => g.GroupNumber == command.GroupNumber);
            if (groupMaster == null)
            {
                return new AccountGroup(0, new List<GroupAccountSummary>());
            }

            var accountSummaries = new List<GroupAccountSummary>();
            var associations = centralDataContext.ModuleAssociations.Where(a => a.GroupNumber == groupMaster.ID).ToList();
            foreach (var association in associations)
            {
                switch (association.Module.ToLower())
                {
                    case "pp":
                        var calculatedPPAccount = commandDispatcher.Send(new GetCalculatedTaxAccount(association.Account.Value,
                            PropertyTaxBillType.Personal, command.EffectiveDate)).Result;
                        if (calculatedPPAccount != null)
                        {
                            accountSummaries.Add(new GroupAccountSummary()
                            {
                                Account = association.Account.Value, AccountType = AccountBillingType.PersonalProperty,
                                Balance = calculatedPPAccount.AccountSummary.Balance
                            });
                        }

                        break;
                    case "re":
                        var calculatedREAccount = commandDispatcher.Send(new GetCalculatedTaxAccount(association.Account.Value,
                            PropertyTaxBillType.Real, command.EffectiveDate)).Result;
                        if (calculatedREAccount != null)
                        {
                            accountSummaries.Add(new GroupAccountSummary()
                            {
                                Account = association.Account.Value, AccountType = AccountBillingType.RealEstate,
                                Balance = calculatedREAccount.AccountSummary.Balance
                            });
                        }

                        break;
                    case "ut":
                        var calculatedUTAccount = commandDispatcher
                            .Send(new GetCalculatedUtilityAccount(association.Account.Value, command.EffectiveDate)).Result;
                        if (calculatedUTAccount != null)
                        {
                            accountSummaries.Add(new GroupAccountSummary()
                            {
                                Account = association.Account.Value,
                                AccountType = AccountBillingType.Utility,
                                Balance = calculatedUTAccount.AccountSummary.Balance
                            });
                        }
                        break;
                }
            }
            return new AccountGroup(groupMaster.GroupNumber.Value,accountSummaries);
        }
    }
}