﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.ExpressionVisitors.Internal;
using Microsoft.Extensions.Logging;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CentralData;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralParties;
using SharedApplication.Clerk;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.MotorVehicle;
using SharedApplication.Payroll.Interfaces;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.SystemSettings.Interfaces;
using SharedApplication.TaxCollections;
using SharedApplication.UtilityBilling;
using SharedDataAccess.AccountsReceivable;
using SharedDataAccess.Budgetary;
using SharedDataAccess.CashReceipts;
using SharedDataAccess.CentralData;
using SharedDataAccess.CentralDocuments;
using SharedDataAccess.CentralParties;
using SharedDataAccess.Clerk;
using SharedDataAccess.ClientSettings;
using SharedDataAccess.MotorVehicle;
using SharedDataAccess.Payroll;
using SharedDataAccess.PersonalProperty;
using SharedDataAccess.RealEstate;
using SharedDataAccess.SystemSettings;
using SharedDataAccess.TaxCollections;
using SharedDataAccess.UtilityBilling;
using Microsoft.Extensions.Logging;

namespace SharedDataAccess
{
    public class TrioContextFactory : ITrioContextFactory, ITrioContextInterfaceFactory
    {
	    private cGlobalSettings globalSettings;
		private DataContextDetails dataContextDetails;

        public TrioContextFactory(DataContextDetails contextDetails, cGlobalSettings globalSettings)
        {
            dataContextDetails = contextDetails;
            this.globalSettings = globalSettings;
        }

        public CentralPartyContext GetCentralPartyContext(bool useNoChangeTracking = false)
        {
            var databaseName = GetLiveDatabaseName("CentralParties");
            var optionsBuilder = new DbContextOptionsBuilder<CentralPartyContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new CentralPartyContext(optionsBuilder.Options);
        }


        public void UpdateContextDetails(string environment)
        {
	        dataContextDetails.DataEnvironment = environment;
        }

        public RealEstateContext GetRealEstateContext(bool useNoChangeTracking = false)
        {
            var databaseName = GetFullDatabaseBaseName("RealEstate");
            var optionsBuilder = new DbContextOptionsBuilder<RealEstateContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new RealEstateContext(optionsBuilder.Options);
        }


        public TaxCollectionsContext GetTaxCollectionsContext(bool useNoChangeTracking = false)
        {
            var databaseName = GetLiveDatabaseName("Collections");
            var optionsBuilder = new DbContextOptionsBuilder<TaxCollectionsContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }

            return new TaxCollectionsContext(optionsBuilder.Options);
        }

        
        public BudgetaryContext GetBudgetaryContext(bool useNoChangeTracking = false)
        {
	        var databaseName = GetFullDatabaseBaseName("Budgetary");
	        var optionsBuilder = new DbContextOptionsBuilder<BudgetaryContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new BudgetaryContext(optionsBuilder.Options);
        }

        public MotorVehicleContext GetMotorVehicleContext(bool useNoChangeTracking = false)
        {
	        var databaseName = GetLiveDatabaseName("MotorVehicle");
	        var optionsBuilder = new DbContextOptionsBuilder<MotorVehicleContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new MotorVehicleContext(optionsBuilder.Options);
        }

        public PayrollContext GetPayrollContext(bool useNoChangeTracking = false)
        {
	        var databaseName = GetLiveDatabaseName("Payroll");
	        var optionsBuilder = new DbContextOptionsBuilder<PayrollContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new PayrollContext(optionsBuilder.Options);
        }

        public SystemSettingsContext GetSystemSettingsContext(bool useNoChangeTracking = false)
        {
	        var databaseName = GetLiveDatabaseName("SystemSettings");
	        var optionsBuilder = new DbContextOptionsBuilder<SystemSettingsContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new SystemSettingsContext(optionsBuilder.Options);
        }

		public ClientSettingsContext GetClientSettingsContext(bool useNoChangeTracking = false)
		{
			var databaseName = GetLiveDatabaseName("ClientSettings");
			var optionsBuilder = new DbContextOptionsBuilder<ClientSettingsContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new ClientSettingsContext(optionsBuilder.Options);
		}

        private string GetLiveDatabaseName(string databaseName)
        {
	        if (databaseName.Trim().ToLower() != "clientsettings" && databaseName.Trim().ToLower() != "bluebook")
	        {
		        return "TRIO_" + dataContextDetails.DataEnvironment + "_Live_" + databaseName;
            }
	        else
	        {
		        return "TRIO_ALL_Live_" + databaseName;
            }
            
        }
		private string GetFullDatabaseBaseName(string databaseName)
        {
            return "TRIO_" + dataContextDetails.DataEnvironment + "_" + globalSettings.EnvironmentGroup + "_" + databaseName;
        }

        private string GetConnectionString(string databaseName)
        {
            var stringBuilder = new SqlConnectionStringBuilder();
            stringBuilder.DataSource = dataContextDetails.DataSource;
            stringBuilder.UserID = dataContextDetails.UserID;
            stringBuilder.Password = dataContextDetails.Password;
            stringBuilder.IntegratedSecurity = false;
            stringBuilder.InitialCatalog = databaseName;
            stringBuilder.ColumnEncryptionSetting= SqlConnectionColumnEncryptionSetting.Enabled;
            return stringBuilder.ToString();
        }

        public PersonalPropertyContext GetPersonalPropertyContext(bool useNoChangeTracking = false)
        {
            var databaseName = GetFullDatabaseBaseName("PersonalProperty");
            var optionsBuilder = new DbContextOptionsBuilder<PersonalPropertyContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new PersonalPropertyContext(optionsBuilder.Options);
        }

        public CentralDataContext GetCentralDataContext(bool useNoChangeTracking = false)
        {
            var databaseName = GetFullDatabaseBaseName("CentralData");
            var optionsBuilder = new DbContextOptionsBuilder<CentralDataContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new CentralDataContext(optionsBuilder.Options);
        }

		public UtilityBillingContext GetUtilityBillingContext(bool useNoChangeTracking = false)
		{
			var databaseName = GetLiveDatabaseName("UtilityBilling");
			var optionsBuilder = new DbContextOptionsBuilder<UtilityBillingContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new UtilityBillingContext(optionsBuilder.Options);
		}

		public AccountsReceivableContext GetAccountsReceivableContext(bool useNoChangeTracking = false)
		{
			var databaseName = GetLiveDatabaseName("AccountsReceivable");
			var optionsBuilder = new DbContextOptionsBuilder<AccountsReceivableContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new AccountsReceivableContext(optionsBuilder.Options);
		}

		public CashReceiptsContext GetCashReceiptsContext(bool useNoChangeTracking = false)
		{
			var databaseName = GetLiveDatabaseName("CashReceipts");
			var optionsBuilder = new DbContextOptionsBuilder<CashReceiptsContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new CashReceiptsContext(optionsBuilder.Options);
		}

        public ClerkContext GetClerkContext(bool useNoChangeTracking = false)
        {
            var databaseName = GetLiveDatabaseName("Clerk");
            var optionsBuilder = new DbContextOptionsBuilder<ClerkContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new ClerkContext(optionsBuilder.Options);
        }

        public CentralDocumentsContext GetCentralDocumentsContext(bool useNoChangeTracking = false)
        {
            var databaseName = GetFullDatabaseBaseName("CentralDocuments");
            var optionsBuilder = new DbContextOptionsBuilder<CentralDocumentsContext>();
            if (useNoChangeTracking)
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }
            else
            {
                optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            }
            return new CentralDocumentsContext(optionsBuilder.Options);
        }

        public ITaxCollectionsContext GetTaxCollectionsContextInterface()
        {
            return GetTaxCollectionsContext(true);
        }

        public IRealEstateContext GetRealEstateContextInterface()
        {
            return GetRealEstateContext(true);
        }

        public IPersonalPropertyContext GetPersonalPropertyContextInterface()
        {
            return GetPersonalPropertyContext(true);
        }

        public ISystemSettingsContext GetSystemSettingsContextInterface()
        {
            return GetSystemSettingsContext(true);
        }

        public IClientSettingsContext GetClientSettingsContextInterface()
        {
            return GetClientSettingsContext(true);
        }

        public IUtilityBillingContext GetUtilityBillingContextInterface()
        {
            return GetUtilityBillingContext(true);
        }

        public IAccountsReceivableContext GetAccountsReceivableContextInterface()
        {
            return GetAccountsReceivableContext(true);
        }

        public ICashReceiptsContext GetCashReceiptsContextInterface()
        {
            return GetCashReceiptsContext(true);
        }

        public ICentralDataContext GetCentralDataContextInterface()
        {
            return GetCentralDataContext(true);
        }

        public ICentralDocumentContext GetCentralDocumentContextInterface()
        {
            return GetCentralDocumentsContext(true);
        }

        public IClerkContext GetClerkContextInterface()
        {
            return GetClerkContext(true);
        }

        public ICentralPartyContext GetCentralPartyContextInterface()
        {
            return GetCentralPartyContext(true);
        }

        public IBudgetaryContext GetBudgetaryContextInterface()
        {
            return GetBudgetaryContext(true);
        }

        public IMotorVehicleContext GetMotorVehicleContextInterface()
        {
            return GetMotorVehicleContext(true);
        }

        public IPayrollContext GetPayrollContextInterface()
        {
            return GetPayrollContext(true);
        }
    }

  
}
