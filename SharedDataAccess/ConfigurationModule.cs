﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;


namespace SharedDataAccess
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TrioContextFactory>().As<ITrioContextFactory>().As<ITrioContextInterfaceFactory>();

        }
    }
}
