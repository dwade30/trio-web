﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
	public class InventoryAdjustmentConfiguration : IEntityTypeConfiguration<InventoryAdjustment>
	{
		public void Configure(EntityTypeBuilder<InventoryAdjustment> builder)
		{
			builder.ToTable("InventoryAdjustments");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.PeriodCloseoutId).HasColumnName("PeriodCloseoutID");
			builder.Property(e => e.TellerCloseoutId).HasColumnName("TellerCloseoutID");
			builder.Property(p => p.AdjustmentCode).HasMaxLength(255);
			builder.Property(p => p.InventoryType).HasMaxLength(255);
			builder.Property(p => p.Reason).HasMaxLength(255);
			builder.Property(p => p.OPID).HasMaxLength(255);
			builder.Property(p => p.Low).HasMaxLength(255);
			builder.Property(p => p.High).HasMaxLength(255);
		}
	}
}
