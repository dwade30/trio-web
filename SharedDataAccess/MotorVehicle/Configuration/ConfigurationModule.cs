﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication.CashReceipts;
using SharedApplication.MotorVehicle;
using SharedApplication.MotorVehicle.Interfaces;

namespace SharedDataAccess.MotorVehicle.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			RegisterMotorVehicleContext(builder);
            builder.RegisterType<InventoryService>().As<IInventoryService>();
            builder.RegisterType<StyleCodeService>().As<IStyleCodeService>();
		}

		private void RegisterMotorVehicleContext(ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var tcf = f.Resolve<ITrioContextFactory>();
				return tcf.GetMotorVehicleContext();
			}).As<IMotorVehicleContext>();
		}

	}
}
