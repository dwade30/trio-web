﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class StyleCodeConfiguration : IEntityTypeConfiguration<StyleCode>
    {
        public void Configure(EntityTypeBuilder<StyleCode> builder)
        {
            builder.ToTable("Style");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(p => p.Type).HasMaxLength(255);
            builder.Property(p => p.Code).HasMaxLength(255);
            builder.Property(p => p.Description).HasMaxLength(255);
            builder.Property(p => p.Description).HasColumnName("Style");
        }
    }
}
