﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class TemporaryPlateRegisterConfiguration : IEntityTypeConfiguration<TemporaryPlateRegister>
    {
        public void Configure(EntityTypeBuilder<TemporaryPlateRegister> entity)
        {
            entity.Property(e => e.Class).HasMaxLength(255);

            entity.Property(e => e.Name).HasMaxLength(255);

            entity.Property(e => e.OPID).HasMaxLength(255);

            entity.Property(e => e.PaidDate).HasColumnType("datetime");

            entity.Property(e => e.Plate).HasMaxLength(255);

            entity.Property(e => e.TemporaryPlateNumber).HasMaxLength(255);
        }
    }
}
