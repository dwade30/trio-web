﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class ClassCodeConfiguration : IEntityTypeConfiguration<ClassCode>
    {
        public void Configure(EntityTypeBuilder<ClassCode> builder)
        {
            builder.ToTable("Class");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.RegistrationFee).HasColumnType("money");
            builder.Property(e => e.OtherFeesNew).HasColumnType("money");
            builder.Property(e => e.OtherFeesRenew).HasColumnType("money");
            builder.Property(p => p.SystemCode).HasMaxLength(255);
            builder.Property(p => p.BMVCode).HasMaxLength(255);
            builder.Property(p => p.Description).HasMaxLength(255);
            builder.Property(p => p.ExciseRequired).HasMaxLength(255);
            builder.Property(p => p.HalfRateAllowed).HasMaxLength(255);
            builder.Property(p => p.TwoYear).HasMaxLength(255);
            builder.Property(p => p.LocationNew).HasMaxLength(255);
            builder.Property(p => p.LocationRenewal).HasMaxLength(255);
            builder.Property(p => p.LocationTransfer).HasMaxLength(255);
            builder.Property(p => p.RenewalDate).HasMaxLength(255);
            builder.Property(p => p.TitleRequired).HasMaxLength(255);
            builder.Property(p => p.Emission).HasMaxLength(255);
            builder.Property(p => p.InsuranceRequired).HasMaxLength(255);
        }
    }
}
