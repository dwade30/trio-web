﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class LongTermTitleApplicationsConfiguration : IEntityTypeConfiguration<LongTermTitleApplication>
    {
        public void Configure(EntityTypeBuilder<LongTermTitleApplication> entity)
        {
            entity.Property(e => e.CTANumber).HasMaxLength(255);

            entity.Property(e => e.Class).HasMaxLength(255);

            entity.Property(e => e.CustomerName).HasMaxLength(255);

            entity.Property(e => e.DuplicateReason).HasMaxLength(255);

            entity.Property(e => e.Fee).HasColumnType("decimal(18, 2)");

            entity.Property(e => e.MSRP).HasMaxLength(255);

            entity.Property(e => e.OPID).HasMaxLength(255);

            entity.Property(e => e.Plate).HasMaxLength(255);

            entity.Property(e => e.RegistrationDate).HasColumnType("datetime");

            entity.Property(e => e.RushFee).HasColumnType("decimal(18, 2)");
        }
    }
}
