﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class LongTermTrailerRegistrationsConfiguration : IEntityTypeConfiguration<LongTermTrailerRegistration>
    {
        public void Configure(EntityTypeBuilder<LongTermTrailerRegistration> entity)
        {
            entity.HasIndex(e => e.PartyID1);

            entity.HasIndex(e => e.PartyID2);

            entity.Property(e => e.Address1).HasMaxLength(255);

            entity.Property(e => e.City).HasMaxLength(255);

            entity.Property(e => e.Color).HasMaxLength(255);

            entity.Property(e => e.DateUpdated).HasColumnType("datetime");

            entity.Property(e => e.EffectiveDate).HasColumnType("datetime");

            entity.Property(e => e.ExpireDate).HasColumnType("datetime");

            entity.Property(e => e.InfoMessage).HasMaxLength(255);

            entity.Property(e => e.LegalResAddress).HasMaxLength(255);

            entity.Property(e => e.LegalResCity).HasMaxLength(255);

            entity.Property(e => e.LegalResState).HasMaxLength(255);

            entity.Property(e => e.LegalResZip).HasMaxLength(255);

            entity.Property(e => e.Make).HasMaxLength(255);

            entity.Property(e => e.OldPlate).HasMaxLength(255);

            entity.Property(e => e.Plate).HasMaxLength(255);

            entity.Property(e => e.Reason).HasMaxLength(255);

            entity.Property(e => e.RegistrationType).HasMaxLength(50);

            entity.Property(e => e.State).HasMaxLength(255);

            entity.Property(e => e.Status).HasMaxLength(255);

            entity.Property(e => e.Style).HasMaxLength(255);

            entity.Property(e => e.TitleNumber).HasMaxLength(255);

            entity.Property(e => e.Unit).HasMaxLength(255);

            entity.Property(e => e.VIN).HasMaxLength(255);

            entity.Property(e => e.Year).HasMaxLength(50);

            entity.Property(e => e.Zip).HasMaxLength(255);
        }
    }
}
