﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class BoosterConfiguration : IEntityTypeConfiguration<Booster>
    {
        public void Configure(EntityTypeBuilder<Booster> entity)
        {
            entity.Property(e => e.Class).HasMaxLength(255);

            entity.Property(e => e.DateUpdated).HasColumnType("datetime");

            entity.Property(e => e.EffectiveDate).HasColumnType("datetime");

            entity.Property(e => e.ExpireDate).HasColumnType("datetime");

            entity.Property(e => e.LocalPaid).HasColumnType("money");

            entity.Property(e => e.OpID).HasMaxLength(255);

            entity.Property(e => e.Plate).HasMaxLength(255);

            entity.Property(e => e.StatePaid).HasColumnType("money");

            entity.Property(e => e.TimeUpdated).HasColumnType("datetime");
        }
    }
}
