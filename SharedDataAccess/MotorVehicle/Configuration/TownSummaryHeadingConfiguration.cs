﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class TownSummaryHeadingConfiguration : IEntityTypeConfiguration<TownSummaryHeading>
    {
        public void Configure(EntityTypeBuilder<TownSummaryHeading> builder)
        {
            builder.ToTable("TownSummaryHeadings");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(p => p.Heading).HasMaxLength(255);
        }
    }
}
