﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class ExceptionReportConfiguration : IEntityTypeConfiguration<ExceptionReport>
    {
        public void Configure(EntityTypeBuilder<ExceptionReport> entity)
        {
            entity.Property(e => e.AddOrSubtract).HasMaxLength(255);

            entity.Property(e => e.ApplicantName).HasMaxLength(255);

            entity.Property(e => e.CategoryAdjusted).HasMaxLength(255);

            entity.Property(e => e.ExceptionReportDate).HasColumnType("datetime");

            entity.Property(e => e.Fee).HasColumnType("money");

            entity.Property(e => e.NewClass).HasMaxLength(255);

            entity.Property(e => e.NewPlate).HasMaxLength(255);

            entity.Property(e => e.OPID).HasMaxLength(255);

            entity.Property(e => e.OriginalClass).HasMaxLength(255);

            entity.Property(e => e.OriginalMMYY).HasMaxLength(255);

            entity.Property(e => e.OriginalPlate).HasMaxLength(255);

            entity.Property(e => e.PermitNumber).HasMaxLength(255);

            entity.Property(e => e.PermitType).HasMaxLength(255);

            entity.Property(e => e.Reason).HasMaxLength(255);

            entity.Property(e => e.ReplacementMMYY).HasMaxLength(255);

            entity.Property(e => e.Type).HasMaxLength(255);
        }
    }
}
