﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
    public class GrossVehicleWeightConfiguration : IEntityTypeConfiguration<GrossVehicleWeight>
    {
        public void Configure(EntityTypeBuilder<GrossVehicleWeight> entity)
        {
            entity.Property(e => e.Type).HasMaxLength(255);
        }
    }
}
