﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralData.Models;
using SharedApplication.MotorVehicle.Models;

namespace SharedDataAccess.MotorVehicle.Configuration
{
	public class InventoryConfiguration : IEntityTypeConfiguration<Inventory>
	{
		public void Configure(EntityTypeBuilder<Inventory> builder)
		{
			builder.ToTable("Inventory");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.PeriodCloseoutId).HasColumnName("PeriodCloseoutID");
			builder.Property(e => e.TellerCloseoutId).HasColumnName("TellerCloseoutID");
			builder.Property(p => p.Code).HasMaxLength(255);
			builder.Property(p => p.Prefix).HasMaxLength(255);
			builder.Property(p => p.Suffix).HasMaxLength(255);
			builder.Property(p => p.OPID).HasMaxLength(255);
			builder.Property(p => p.Status).HasMaxLength(255);
			builder.Property(p => p.GroupID).HasMaxLength(255);
			builder.Property(p => p.AssignedBy).HasMaxLength(255);
			builder.Property(p => p.Comment).HasMaxLength(255);
			builder.Property(p => p.FormattedInventory).HasMaxLength(255);

		}
	}
}
