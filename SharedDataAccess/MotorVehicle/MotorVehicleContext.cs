﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication.MotorVehicle;
using SharedApplication.MotorVehicle.Models;
using SharedDataAccess.MotorVehicle.Configuration;


namespace SharedDataAccess.MotorVehicle
{
	public partial class MotorVehicleContext : DbContext, IMotorVehicleContext
	{
		public DbSet<Inventory> Inventories { get; set; }
		public DbSet<InventoryAdjustment> InventoryAdjustments { get; set; }
        public DbSet<ClassCode> ClassCodes { get; set; }
        public DbSet<StyleCode> StyleCodes { get; set; }
        public DbSet<TownSummaryHeading> TownSummaryHeadings { get; set; }
        public virtual DbSet<ActivityMaster> ActivityMasters { get; set; }
        public virtual DbSet<ArchiveMaster> ArchiveMasters { get; set; }
        public virtual DbSet<AuditChange> AuditChanges { get; set; }
        public virtual DbSet<AuditChangesArchive> AuditChangesArchives { get; set; }
        public virtual DbSet<AuditReport> AuditReports { get; set; }
        public virtual DbSet<Booster> Boosters { get; set; }
        public virtual DbSet<CYA> CYAs { get; set; }
        public virtual DbSet<CloseoutInventory> CloseoutInventories { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        public virtual DbSet<MVCounty> Counties { get; set; }
        public virtual DbSet<DBVersion> DBVersions { get; set; }
        public virtual DbSet<Defaultinfo> Defaultinfos { get; set; }
        public virtual DbSet<DoubleCTA> DoubleCTAs { get; set; }
        public virtual DbSet<ExceptionReport> ExceptionReports { get; set; }
        public virtual DbSet<ExciseTax> ExciseTaxes { get; set; }
        public virtual DbSet<FleetLock> FleetLocks { get; set; }
        public virtual DbSet<FleetMaster> FleetMasters { get; set; }
        public virtual DbSet<GiftCertificateRegister> GiftCertificateRegisters { get; set; }
        public virtual DbSet<GrossVehicleWeight> GrossVehicleWeights { get; set; }
        public virtual DbSet<GroupAddressOverride> GroupAddressOverrides { get; set; }
        public virtual DbSet<HeldRegistrationMaster> HeldRegistrationMasters { get; set; }
        public virtual DbSet<HeldTitleUseTax> HeldTitleUseTaxes { get; set; }
        public virtual DbSet<InventoryLock> InventoryLocks { get; set; }
        public virtual DbSet<InventoryMaster> InventoryMasters { get; set; }
        public virtual DbSet<InventoryOnHandAtPeriodCloseout> InventoryOnHandAtPeriodCloseouts { get; set; }
        public virtual DbSet<LastMVRNumber> LastMVRNumbers { get; set; }
        public virtual DbSet<LongTermTitleApplication> LongTermTitleApplications { get; set; }
        public virtual DbSet<LongTermTrailerRegistration> LongTermTrailerRegistrations { get; set; }
        public virtual DbSet<Make> Makes { get; set; }
        public virtual DbSet<MVMaster> Masters { get; set; }
        public virtual DbSet<Operator> Operators { get; set; }
        public virtual DbSet<PendingActivityMaster> PendingActivityMasters { get; set; }
        public virtual DbSet<PendingExceptionReport> PendingExceptionReports { get; set; }
        public virtual DbSet<PendingTitleApplication> PendingTitleApplications { get; set; }
        public virtual DbSet<PeriodCloseout> PeriodCloseouts { get; set; }
        public virtual DbSet<PeriodCloseoutLongTerm> PeriodCloseoutLongTerms { get; set; }
        public virtual DbSet<PrintPriority> PrintPriorities { get; set; }
        public virtual DbSet<ReasonCode> ReasonCodes { get; set; }
        public virtual DbSet<Residence> Residences { get; set; }
        public virtual DbSet<SpecialRegistration> SpecialRegistrations { get; set; }
        public virtual DbSet<SpecialResCode> SpecialResCodes { get; set; }
        public virtual DbSet<SpecialtyPlate> SpecialtyPlates { get; set; }
        public virtual DbSet<MVState> States { get; set; }
        public virtual DbSet<TellerCloseoutTable> TellerCloseoutTables { get; set; }
        public virtual DbSet<TemporaryPlateRegister> TemporaryPlateRegisters { get; set; }
        public virtual DbSet<Title> Titles { get; set; }
        public virtual DbSet<TitleApplication> TitleApplications { get; set; }
        public virtual DbSet<TownDetailSummary> TownDetailSummaries { get; set; }
        public virtual DbSet<UseTax> UseTaxes { get; set; }
        public virtual DbSet<WMV> WMVs { get; set; }
        public virtual DbSet<MVYear> Years { get; set; }
        public virtual DbSet<tblMasterTemp> tblMasterTemps { get; set; }

        IQueryable<Inventory> IMotorVehicleContext.Inventories { get => Inventories; }
		IQueryable<InventoryAdjustment> IMotorVehicleContext.InventoryAdjustments { get => InventoryAdjustments; }
        IQueryable<ClassCode> IMotorVehicleContext.ClassCodes { get => ClassCodes; }
        IQueryable<StyleCode> IMotorVehicleContext.StyleCodes { get => StyleCodes; }
        IQueryable<TownSummaryHeading> IMotorVehicleContext.TownSummaryHeadings { get => TownSummaryHeadings; }

        IQueryable<ActivityMaster> IMotorVehicleContext.ActivityMasters { get => ActivityMasters; }
        IQueryable<ArchiveMaster> IMotorVehicleContext.ArchiveMasters { get => ArchiveMasters; }
        IQueryable<AuditChange> IMotorVehicleContext.AuditChanges { get => AuditChanges; }
        IQueryable<AuditChangesArchive> IMotorVehicleContext.AuditChangesArchives { get => AuditChangesArchives; }
        IQueryable<AuditReport> IMotorVehicleContext.AuditReports { get => AuditReports; }
        IQueryable<Booster> IMotorVehicleContext.Boosters { get => Boosters; }
        IQueryable<CYA> IMotorVehicleContext.CYAs { get => CYAs; }
        IQueryable<CloseoutInventory> IMotorVehicleContext.CloseoutInventories { get => CloseoutInventories; }
        IQueryable<Color> IMotorVehicleContext.Colors { get => Colors; }
        IQueryable<MVCounty> IMotorVehicleContext.Counties { get => Counties; }
        IQueryable<DBVersion> IMotorVehicleContext.DBVersions { get => DBVersions; }
        IQueryable<Defaultinfo> IMotorVehicleContext.Defaultinfos { get => Defaultinfos; }
        IQueryable<DoubleCTA> IMotorVehicleContext.DoubleCTAs { get=>DoubleCTAs; }

        IQueryable<ExceptionReport> IMotorVehicleContext.ExceptionReports { get=>ExceptionReports; }
        IQueryable<ExciseTax> IMotorVehicleContext.ExciseTaxes { get=>ExciseTaxes; }
        IQueryable<FleetLock> IMotorVehicleContext.FleetLocks { get=>FleetLocks; }
        IQueryable<FleetMaster> IMotorVehicleContext.FleetMasters { get=>FleetMasters; }
        IQueryable<GiftCertificateRegister> IMotorVehicleContext.GiftCertificateRegisters { get=>GiftCertificateRegisters; }
        IQueryable<GrossVehicleWeight> IMotorVehicleContext.GrossVehicleWeights { get=>GrossVehicleWeights; }
        IQueryable<GroupAddressOverride> IMotorVehicleContext.GroupAddressOverrides { get=>GroupAddressOverrides; }
        IQueryable<HeldRegistrationMaster> IMotorVehicleContext.HeldRegistrationMasters { get=>HeldRegistrationMasters; }
        IQueryable<HeldTitleUseTax> IMotorVehicleContext.HeldTitleUseTaxes { get=>HeldTitleUseTaxes; }
        IQueryable<InventoryLock> IMotorVehicleContext.InventoryLocks { get=>InventoryLocks; }
        IQueryable<InventoryMaster> IMotorVehicleContext.InventoryMasters { get=>InventoryMasters; }
        IQueryable<InventoryOnHandAtPeriodCloseout> IMotorVehicleContext.InventoryOnHandAtPeriodCloseouts { get=>InventoryOnHandAtPeriodCloseouts; }
        IQueryable<LastMVRNumber> IMotorVehicleContext.LastMVRNumbers { get=>LastMVRNumbers; }
        IQueryable<LongTermTitleApplication> IMotorVehicleContext.LongTermTitleApplications { get=>LongTermTitleApplications; }
        IQueryable<LongTermTrailerRegistration> IMotorVehicleContext.LongTermTrailerRegistrations { get=>LongTermTrailerRegistrations; }
        IQueryable<Make> IMotorVehicleContext.Makes { get=>Makes; }

        IQueryable<MVMaster> IMotorVehicleContext.Masters { get => Masters;}

        IQueryable<Operator> IMotorVehicleContext.Operators { get=>Operators; }
        IQueryable<PendingActivityMaster> IMotorVehicleContext.PendingActivityMasters { get => PendingActivityMasters; }
        IQueryable<PendingExceptionReport> IMotorVehicleContext.PendingExceptionReports { get=>PendingExceptionReports; }
        IQueryable<PendingTitleApplication> IMotorVehicleContext.PendingTitleApplications { get=>PendingTitleApplications; }
        IQueryable<PeriodCloseout> IMotorVehicleContext.PeriodCloseouts { get=>PeriodCloseouts; }
        IQueryable<PeriodCloseoutLongTerm> IMotorVehicleContext.PeriodCloseoutLongTerms { get=>PeriodCloseoutLongTerms; }
        IQueryable<PrintPriority> IMotorVehicleContext.PrintPriorities { get=>PrintPriorities; }
        IQueryable<ReasonCode> IMotorVehicleContext.ReasonCodes { get=>ReasonCodes; }
        IQueryable<Residence> IMotorVehicleContext.Residences { get=>Residences; }
        IQueryable<SpecialRegistration> IMotorVehicleContext.SpecialRegistrations { get=>SpecialRegistrations; }
        IQueryable<SpecialResCode> IMotorVehicleContext.SpecialResCodes { get=>SpecialResCodes; }
        IQueryable<SpecialtyPlate> IMotorVehicleContext.SpecialtyPlates { get=>SpecialtyPlates; }
        IQueryable<MVState> IMotorVehicleContext.States { get=>States; }
        IQueryable<TellerCloseoutTable> IMotorVehicleContext.TellerCloseoutTables { get=>TellerCloseoutTables; }
        IQueryable<TemporaryPlateRegister> IMotorVehicleContext.TemporaryPlateRegisters { get=>TemporaryPlateRegisters; }
        IQueryable<Title> IMotorVehicleContext.Titles { get=>Titles; }
        IQueryable<TitleApplication> IMotorVehicleContext.TitleApplications { get=>TitleApplications; }
        IQueryable<TownDetailSummary> IMotorVehicleContext.TownDetailSummaries { get=>TownDetailSummaries; }
        IQueryable<UseTax> IMotorVehicleContext.UseTaxes { get=>UseTaxes; }
        IQueryable<WMV> IMotorVehicleContext.WMVs { get=>WMVs; }
        IQueryable<MVYear> IMotorVehicleContext.Years { get=>Years; }
        IQueryable<tblMasterTemp> IMotorVehicleContext.tblMasterTemps { get=>tblMasterTemps; }

        public MotorVehicleContext()
		{
		}

		public MotorVehicleContext(DbContextOptions<MotorVehicleContext> options) : base(options)
		{
		}

		public MotorVehicleContext(string connectionString)
			: this(new DbContextOptionsBuilder<MotorVehicleContext>()
				.UseSqlServer(connectionString).Options)
		{
		}


		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.ApplyConfiguration(new InventoryConfiguration());
			modelBuilder.ApplyConfiguration(new InventoryAdjustmentConfiguration());
            modelBuilder.ApplyConfiguration(new ClassCodeConfiguration());
            modelBuilder.ApplyConfiguration(new StyleCodeConfiguration());
            modelBuilder.ApplyConfiguration(new TownSummaryHeadingConfiguration());

            modelBuilder.ApplyConfiguration(new ActivityMasterConfiguration());
            modelBuilder.ApplyConfiguration(new ArchiveMasterConfiguration());
            modelBuilder.ApplyConfiguration(new AuditChangesConfiguration());
            modelBuilder.ApplyConfiguration(new AuditChangesArchiveConfiguration());
            modelBuilder.ApplyConfiguration(new AuditReportsConfiguration());
            modelBuilder.ApplyConfiguration(new BoosterConfiguration());
            modelBuilder.ApplyConfiguration(new CYAConfiguration());
            modelBuilder.ApplyConfiguration(new CloseoutInventoryConfiguration());
            modelBuilder.ApplyConfiguration(new ColorConfiguration());
            modelBuilder.ApplyConfiguration(new CountyConfiguration());
            modelBuilder.ApplyConfiguration(new DefaultinfoConfiguration());
            modelBuilder.ApplyConfiguration(new DoubleCTAConfiguration());
            modelBuilder.ApplyConfiguration(new ExceptionReportConfiguration());
            modelBuilder.ApplyConfiguration(new ExciseTaxConfiguration());
            modelBuilder.ApplyConfiguration(new FleetLockConfiguration());
            modelBuilder.ApplyConfiguration(new FleetMasterConfiguration());
            modelBuilder.ApplyConfiguration(new GiftCertificateRegisterConfiguration());
            modelBuilder.ApplyConfiguration(new GrossVehicleWeightConfiguration());
            modelBuilder.ApplyConfiguration(new GroupAddressOverrideConfiguration());
            modelBuilder.ApplyConfiguration(new HeldRegistrationMasterConfiguration());
            modelBuilder.ApplyConfiguration(new HeldTitleUseTaxConfiguration());
            modelBuilder.ApplyConfiguration(new InventoryLockConfiguration());
            modelBuilder.ApplyConfiguration(new InventoryMasterConfiguration());
            modelBuilder.ApplyConfiguration(new InventoryOnHandAtPeriodCloseoutConfiguration());
            modelBuilder.ApplyConfiguration(new LastMVRNumberConfiguration());
            modelBuilder.ApplyConfiguration(new LongTermTitleApplicationsConfiguration());
            modelBuilder.ApplyConfiguration(new LongTermTrailerRegistrationsConfiguration());
            modelBuilder.ApplyConfiguration(new MakeConfiguration());
            modelBuilder.ApplyConfiguration(new MasterConfiguration());
            modelBuilder.ApplyConfiguration(new OperatorsConfiguration());
            modelBuilder.ApplyConfiguration(new PendingActivityMasterConfiguration());
            modelBuilder.ApplyConfiguration(new PendingExceptionReportConfiguration());
            modelBuilder.ApplyConfiguration(new PendingTitleApplicationsConfiguration());
            modelBuilder.ApplyConfiguration(new PeriodCloseoutConfiguration());
            modelBuilder.ApplyConfiguration(new PeriodCloseoutLongTermConfiguration());
            modelBuilder.ApplyConfiguration(new PrintPriorityConfiguration());
            modelBuilder.ApplyConfiguration(new ReasonCodesConfiguration());
            modelBuilder.ApplyConfiguration(new ResidenceConfiguration());
            modelBuilder.ApplyConfiguration(new SpecialRegistrationConfiguration());
            modelBuilder.ApplyConfiguration(new SpecialResCodeConfiguration());
            modelBuilder.ApplyConfiguration(new SpecialtyPlatesConfiguration());
            modelBuilder.ApplyConfiguration(new StateConfiguration());
            modelBuilder.ApplyConfiguration(new TellerCloseoutTableConfiguration());
            modelBuilder.ApplyConfiguration(new TemporaryPlateRegisterConfiguration());
            modelBuilder.ApplyConfiguration(new TitleConfiguration());
            modelBuilder.ApplyConfiguration(new TitleApplicationsConfiguration());
            modelBuilder.ApplyConfiguration(new TownDetailSummaryConfiguration());
            modelBuilder.ApplyConfiguration(new TransitPlatesConfiguration());
            modelBuilder.ApplyConfiguration(new UseTaxConfiguration());
            modelBuilder.ApplyConfiguration(new WMVConfiguration());
            modelBuilder.ApplyConfiguration(new YearConfiguration());
            modelBuilder.ApplyConfiguration(new tblMasterTempConfiguration());
        }
	}
}
