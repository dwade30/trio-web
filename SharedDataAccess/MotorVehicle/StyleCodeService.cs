﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle.Enums;
using SharedApplication.MotorVehicle.Interfaces;
using SharedApplication.MotorVehicle.Models;
using SharedApplication.Telemetry;

namespace SharedDataAccess.MotorVehicle
{
    public class StyleCodeService : IStyleCodeService
    {
        private MotorVehicleContext motorVehicleContext;
        private ITelemetryService telemetryService;

        public StyleCodeService(ITrioContextFactory contextFactory, ITelemetryService telemetryService)
        {
            motorVehicleContext = contextFactory.GetMotorVehicleContext();
            this.telemetryService = telemetryService;
        }

        public bool InitializeStyleCodes()
        {

            try
            {
                var currentCodes = motorVehicleContext.StyleCodes;
                motorVehicleContext.StyleCodes.RemoveRange(currentCodes);

                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "2D", Description = "2 Door".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "3D", Description = "3 Door/Sedan".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "4D", Description = "4 Door/Sedan".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "AM", Description = "Ambulance".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "BU", Description = "Bus".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "CP", Description = "Coupe".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "CV", Description = "Convertible".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "HR", Description = "Hearse".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "LM", Description = "Limousine".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "LV", Description = "Law Enforcement".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "MV", Description = "Multi-wheeled Vehicle".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "SD", Description = "Sedan".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "SQ", Description = "Search and Rescue".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "SW", Description = "Station Wagon".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "VC", Description = "Van Camper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "VT", Description = "Vanette".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "AU", Code = "PK", Description = "Pick-up".ToUpper() });

                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "2W", Description = "2 Wheel Motorcycle".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "3W", Description = "3 Wheel Tricycle".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "MB", Description = "Motorbike".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "MC", Description = "Motorcycle".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "MK", Description = "Minibike".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "MP", Description = "Moped".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "MS", Description = "Motorscooter".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "MV", Description = "Multi-wheel".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "MC", Code = "MY", Description = "Minicycle".ToUpper() });

                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CO", Code = "00", Description = "Pickup Trucks or Suvs".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CO", Code = "10", Description = "2 Axle-4 Tires".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CO", Code = "20", Description = "2 Axle-6 Tires".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CO", Code = "30", Description = "3 Axle".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CO", Code = "40", Description = "4 Axle".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CO", Code = "50", Description = "5 Axle".ToUpper() });

                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CT", Code = "25", Description = "2 Axle Tractor".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CT", Code = "35", Description = "3 Axle Tractor".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CT", Code = "45", Description = "4 Axle Tractor".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "CT", Code = "55", Description = "5 Axle Tractor".ToUpper() });

                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "BU", Code = "26", Description = "School Type Buses".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "BU", Code = "27", Description = "Other 2 Axle Buses".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "BU", Code = "37", Description = "3 Axle Buses".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "BU", Code = "47", Description = "All Other Buses".ToUpper() });

                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "AD", Description = "Asphalt Distributor".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "AE", Description = "Aerial Platform".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "AI", Description = "Air Compressor".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "BC", Description = "Brush Chipper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "BR", Description = "Beverage Rack".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "CM", Description = "Concrete or Transit Mixer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "CS", Description = "Culvert Steamer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "DR", Description = "Drill Rock".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "FR", Description = "Flatrack Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "FS", Description = "Fertilizer Spreader".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "GD", Description = "Grader".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "GE", Description = "Generator".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "HL", Description = "Hay Bale Loader".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "HM", Description = "Hammer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "LK", Description = "Log Skidder".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "LL", Description = "Carry All".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "MU", Description = "Mulcher".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "RC", Description = "Rock Crusher".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "SA", Description = "Sail Plane".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "SC", Description = "Scraper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "SZ", Description = "Saw".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "TP", Description = "Tar Pot".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "WC", Description = "Wood Chipper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "WD", Description = "Well Driller".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "WE", Description = "Welder".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "WN", Description = "Wind Rower".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "SE", Code = "WS", Description = "Wood Splitter".ToUpper() });

                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "5W", Description = "5th Wheel travel trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "AC", Description = "Auto Carrier".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "BA", Description = "Bulk Agriculture".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "BT", Description = "Boat Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "BZ", Description = "Biohazard".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "CG", Description = "Converter Gear Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "CT", Description = "Camping or Travel Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "DO", Description = "Dolly".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "DT", Description = "Dump Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "DY", Description = "Auxiliary Dolly".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "EB", Description = "Enclosed Body Removable Enclosure".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "EN", Description = "Enclosed Body Non-removable Enclosure".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "FB", Description = "Flatbed/Platform, Trailer/Amusement Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "FD", Description = "Flotation Chassis".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "GA", Description = "Wagon-Type Trailer(Gondola)".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "GN", Description = "Grain Truck/ Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "HD", Description = "Hydraulic Dump".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "HE", Description = "Horse Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "HO", Description = "Hopper(Bottom Dump) Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "HS", Description = "House Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "LB", Description = "Lowboy or Lowbed Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "LP", Description = "Pole or Pipe(logging) Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "LS", Description = "Livestock Rack or Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "LV", Description = "Law Enforcement".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "MT", Description = "Motorcycle Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "OP", Description = "Open Body".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "OT", Description = "Office Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "PT", Description = "Passenger Tram or Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "RF", Description = "Refrigerated Van".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "SB", Description = "Cooking Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "SE", Description = "Semi-Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "SM", Description = "Snowmobile Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "SQ", Description = "Search and Rescue".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "ST", Description = "Stake or Rack Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "TD", Description = "Auto Tow Dolly".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "TE", Description = "Tent Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "TN", Description = "Tank Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "UT", Description = "Utility Trucks/Trailers/Concession Trailer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TL", Code = "VN", Description = "Van Trailer".ToUpper() });

                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "BD", Description = "Bulldozer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "BG", Description = "Buggy, Concrete".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "BH", Description = "Backhoe/Loader".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "BK", Description = "Backhoe".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "CE", Description = "Unpublished Construction Equipment".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "CH", Description = "Coach".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "CI", Description = "Corn Picker".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "CK", Description = "Cotton Picker".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "CO", Description = "Self-propelled combine".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "CR", Description = "Crane".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "CZ", Description = "Cotton Stripper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "DE", Description = "Detasseling Equipment".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "DI", Description = "Potato Digger".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "EX", Description = "Excavator".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "FL", Description = "Fork Lift".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "GC", Description = "Golf Cart".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "HV", Description = "Harvester".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "HY", Description = "Hay Baler".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "LD", Description = "Loader".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "LF", Description = "Lift Boom".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "MF", Description = "Unpublished style of farm Equipment".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "MO", Description = "Mower".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "MR", Description = "Mower/Conditioner".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "PB", Description = "Portable Boom".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "PV", Description = "Paver".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "RO", Description = "Roller".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "SH", Description = "Shovel".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "SI", Description = "Striper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "SO", Description = "Snow Blower".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "SS", Description = "Street Sweeper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "SY", Description = "Sprayer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "TA", Description = "Tree Harvester".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "TC", Description = "Tractor Track Type".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "TF", Description = "Tractor Wheel Type".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TR", Code = "TH", Description = "Trencher".ToUpper() });


                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "3D", Description = "3-Door Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "4D", Description = "4-Door Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "AM", Description = "Ambulance".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "AR", Description = "Armored Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "BA", Description = "Bulk Agriculture".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "BR", Description = "Beverage Rack".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "BU", Description = "Bus".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "BZ", Description = "Biohazard".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "CB", Description = "Chassis and Cab(Utility Truck)".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "CM", Description = "Concrete or Transit Mixer".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "DP", Description = "Dump Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "FB", Description = "Flatbed".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "FR", Description = "Flatrack Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "FT", Description = "Fire Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "GG", Description = "Garbage or Refuse Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "GN", Description = "Grain Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "GR", Description = "Glass Rack".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "HD", Description = "Hydraulic Dump".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "HO", Description = "Hopper Bottom".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "LF", Description = "Lift Boom".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "LG", Description = "Log".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "LL", Description = "Carry All(trail, SUVs)".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "LS", Description = "Livestock Rack".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "LV", Description = "Law Enforcement".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "LW", Description = "Lunch Wagon".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "MH", Description = "Motorized Home".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "MJ", Description = "Multi-Engine".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "PK", Description = "Pick-up".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "PL", Description = "Pallet".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "PM", Description = "Pickup with Mounted Camper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "PR", Description = "Primer Mover".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "PV", Description = "Paver".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "RF", Description = "Refrigerated Van".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "SQ", Description = "Search and Rescue".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "ST", Description = "Stake or Rack".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "TN", Description = "Tanker".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "TR", Description = "Tractor Truck Gasoline".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "TT", Description = "Tow Truck/Wrecker".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "UT", Description = "Utility Truck".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "VC", Description = "Van Camper".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "VN", Description = "Van".ToUpper() });
                motorVehicleContext.StyleCodes.Add(new StyleCode { Type = "TK", Code = "VT", Description = "Vanette".ToUpper() });

                motorVehicleContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                telemetryService.TrackException(e);
                return false;
            }
            
        }

        public IEnumerable<StyleCode> GetValidStyleCodeList(string vehicleClass, string vehicleSubClass, string plate)
        {
            var vehicleType = ConvertVehicleStyleTypeToCode(GetVehicleStyleType(vehicleClass, vehicleSubClass, plate));

            if (vehicleType == "")
            {
                return new List<StyleCode>();
            }
            else if (vehicleType == "ALL")
            {
                return motorVehicleContext.StyleCodes.OrderBy(y => y.Code).ToList();
            }
            else if (vehicleType == "NC")
            {
                return motorVehicleContext.StyleCodes.Where(x => x.Code != "00" && x.Code.ToIntegerValue() == 0).OrderBy(y => y.Code).ToList();
            }
            else
            {
                return motorVehicleContext.StyleCodes.Where(x => x.Code == "00" || x.Code.ToIntegerValue() != 0).OrderBy(y => y.Code).ToList();
            }
        }

        public bool ValidateStyleCode(string vehicleClass, string vehicleSubClass, string plate, string styleCode)
        { 
            var vehicleType = ConvertVehicleStyleTypeToCode(GetVehicleStyleType(vehicleClass, vehicleSubClass, plate));

            if (vehicleType == "")
            {
                return false;
            }
            else if (vehicleType == "NC")
            {
                int temp;
                if (Int32.TryParse(styleCode, out temp))
                {
                    return false;
                }
            }
            else if (vehicleType == "CO")
            {
                int temp;
                if (!Int32.TryParse(styleCode, out temp))
                {
                    return false;
                }
            }

            return motorVehicleContext.StyleCodes.Any(x => x.Code == styleCode);
        }

        private VehicleStyleType GetVehicleStyleType(string vehicleClass, string vehicleSubClass, string plate)
        {
            var classResult = motorVehicleContext.ClassCodes.FirstOrDefault(x => x.BMVCode == vehicleClass && x.SystemCode == vehicleSubClass);

            if (classResult != null)
            {
                switch (classResult.ShortDescription ?? 0)
                {
                    case 40:
                    case 60:
                    case 80:
                        return VehicleStyleType.Commercial;
                    case 30:
                        return VehicleStyleType.All;
                    default:
                        if (vehicleClass == "AP")
                        {
                            return VehicleStyleType.Commercial;
                        }
                        else
                        {
                            return VehicleStyleType.NonCommercial;
                        }
                }
            }
            else
            {
                return VehicleStyleType.Unknown;
            }
        }

        private string ConvertVehicleStyleTypeToCode(VehicleStyleType type)
        {
            switch (type)
            {
                case VehicleStyleType.Commercial:
                    return "CO";
                case VehicleStyleType.NonCommercial:
                    return "NC";
                case VehicleStyleType.All:
                    return "ALL";
                default:
                    return "";
            }
        }
    }
}
