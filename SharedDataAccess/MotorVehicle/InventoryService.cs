﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Enums;
using SharedApplication.MotorVehicle.Interfaces;
using SharedApplication.MotorVehicle.Models;
using SharedDataAccess.MotorVehicle.Configuration;

namespace SharedDataAccess.MotorVehicle
{
    public class InventoryService : IInventoryService
    {
        private MotorVehicleContext motorVehicleContext;
        private bool cancelled = false;

        public InventoryService(ITrioContextFactory contextFactory)
        {
            motorVehicleContext = contextFactory.GetMotorVehicleContext();
        }

        public InventoryAdjustmentValidationResult ValidateInventoryAdjustmentInfo(InventoryAdjustmentInfo info)
        {
            var inventory = motorVehicleContext.Inventories.Where(x =>
                x.Code == info.Code && x.Prefix == info.Prefix && x.Suffix == info.Suffix &&
                (x.Number >= info.Low && x.Number <= info.High)).ToList();

            return info.AdjustmentType == InventoryAdjustmentType.Add ? ValidateAdd(inventory, info.Low, info.High) :
                info.AdjustmentType == InventoryAdjustmentType.Delete ? ValidateDelete(inventory, (info.High - info.Low) + 1) :
                InventoryAdjustmentValidationResult.InvalidRequest;
        }

        private InventoryAdjustmentValidationResult ValidateAdd(List<Inventory> existingInventory, int low, int high)
        {
            if (existingInventory.Any(x => x.Status != "D" && x.Status != "A"))
            {
                return InventoryAdjustmentValidationResult.InventoryExistsVoidedIssued;
            }
            else if (existingInventory.Any(x => x.Status == "A"))
            {
                return InventoryAdjustmentValidationResult.InventoryExistsAvailable;
            }
            else if (low.ToString().Length != high.ToString().Length)
            {
                return InventoryAdjustmentValidationResult.VerifyRange;
            }
            else
            {
                return InventoryAdjustmentValidationResult.NoIssues;
            }
        }

        private InventoryAdjustmentValidationResult ValidateDelete(List<Inventory> existingInventory, int inventoryCount)
        {
            if (existingInventory.Count() < inventoryCount)
            {
                return InventoryAdjustmentValidationResult.InventoryNotFound;
            }
            else if (existingInventory.Any(x => x.Status == "I" || x.Status == "D" || x.Status == "V"))
            {
                return InventoryAdjustmentValidationResult.InventoryExistsVoidedIssued;
            }
            else
            {
                return InventoryAdjustmentValidationResult.NoIssues;
            }
        }

        public event EventHandler CancelUnavailable;

		public bool AdjustInventory(AdjustInventoryConfigurationInfo config)
		{
			try
            {
                cancelled = false;

                var inventory = motorVehicleContext.Inventories.Where(x =>
					x.Code == config.AdjustmentDetails.Code && x.Prefix == config.AdjustmentDetails.Prefix && x.Suffix == config.AdjustmentDetails.Suffix &&
					(x.Number >= config.AdjustmentDetails.Low && x.Number <= config.AdjustmentDetails.High)).OrderBy(y => y.Number).ToList();

				if (config.AdjustmentDetails.AdjustmentType == InventoryAdjustmentType.Add)
				{
					for (int counter = config.AdjustmentDetails.Low; counter <= config.AdjustmentDetails.High; counter++)
					{
						var result = inventory.FirstOrDefault(x =>
							x.Code == config.AdjustmentDetails.Code && x.Prefix == config.AdjustmentDetails.Prefix && x.Suffix == config.AdjustmentDetails.Suffix &&
							x.Number == counter);

						if (result != null)
						{
							result.Status = "A";
							result.OPID = config.OpId;
							result.InventoryDate = DateTime.Today;
							result.Group = config.Group;
							result.FormattedInventory = CreateFormattedInventory(config.AdjustmentDetails.Prefix, counter.ToString(), config.AdjustmentDetails.Suffix, config.AdjustmentDetails.Code);
							result.MVR3 = 0;
						}
						else
						{
							motorVehicleContext.Inventories.Add(new Inventory
							{
								Prefix = config.AdjustmentDetails.Prefix,
								Suffix = config.AdjustmentDetails.Suffix,
								Number = counter,
								FormattedInventory = CreateFormattedInventory(config.AdjustmentDetails.Prefix, counter.ToString(), config.AdjustmentDetails.Suffix, config.AdjustmentDetails.Code),
								Status = "A",
								OPID = config.OpId,
								InventoryDate = DateTime.Today,
								Group = config.Group,
								MVR3 = 0,
								Code = config.AdjustmentDetails.Code,
								TellerCloseoutId = 0
							});
						}

                        if (cancelled)
                            return false;
                    }

					motorVehicleContext.InventoryAdjustments.Add(new InventoryAdjustment
					{
						AdjustmentCode = "R",
						DateOfAdjustment = DateTime.Today,
						QuantityAdjusted = (config.AdjustmentDetails.High - config.AdjustmentDetails.Low) + 1,
						InventoryType = config.AdjustmentDetails.Code,
						Low = CreateFormattedInventory(config.AdjustmentDetails.Prefix, config.AdjustmentDetails.Low.ToString(), config.AdjustmentDetails.Suffix, config.AdjustmentDetails.Code),
						High = CreateFormattedInventory(config.AdjustmentDetails.Prefix, config.AdjustmentDetails.High.ToString(), config.AdjustmentDetails.Suffix, config.AdjustmentDetails.Code),
						OPID = config.OpId,
						Reason = config.Reason,
						PeriodCloseoutId = 0,
						TellerCloseoutId = 0
					});

                    if (cancelled)
                        return false;

                    if (CancelUnavailable != null)
                    {
                        CancelUnavailable(this, new EventArgs());
                    }

					motorVehicleContext.SaveChanges();

					return true;
				}
				else if (config.AdjustmentDetails.AdjustmentType == InventoryAdjustmentType.Delete)
				{
					foreach (var item in inventory)
					{
						item.OPID = config.OpId;
						item.Status = "D";
						item.InventoryDate = DateTime.Today;
						item.TellerCloseoutId = 0;

                        if (cancelled)
                            return false;
					}

					motorVehicleContext.InventoryAdjustments.Add(new InventoryAdjustment
					{
						AdjustmentCode = "A",
						DateOfAdjustment = DateTime.Today,
						QuantityAdjusted = (config.AdjustmentDetails.High - config.AdjustmentDetails.Low) + 1,
						InventoryType = config.AdjustmentDetails.Code,
						Low = CreateFormattedInventory(config.AdjustmentDetails.Prefix, config.AdjustmentDetails.Low.ToString(), config.AdjustmentDetails.Suffix, config.AdjustmentDetails.Code),
						High = CreateFormattedInventory(config.AdjustmentDetails.Prefix, config.AdjustmentDetails.High.ToString(), config.AdjustmentDetails.Suffix, config.AdjustmentDetails.Code),
						OPID = config.OpId,
						Reason = config.Reason,
						PeriodCloseoutId = 0,
						TellerCloseoutId = 0
					});

                    if (cancelled)
                        return false;

                    if (CancelUnavailable != null)
                    {
                        CancelUnavailable(this, new EventArgs());
                    }

					motorVehicleContext.SaveChanges();

					return true;
				}
				else
				{
					return false;
				}
			}
			catch
			{
				return false;
			}
		}

        public void CancelAdjustment()
        {
            cancelled = true;
        }

		private string CreateFormattedInventory(string strPre, string strNumber, string strSuff, string strType)
		{
			string CreateFormattedInventory = "";
			if (strType.Left(2) != "PX" || strPre == "NEW")
			{
				CreateFormattedInventory = strPre + strNumber + strSuff;
			}
			else
			{
				if ((strType.Right(2) == "AC") || (strType.Right(2) == "AF") || (strType.Right(2) == "AW") || strType.Right(2) == "BB" || (strType.Right(2) == "BC") || strType.Right(2) == "BH" || (strType.Right(2) == "CV") || (strType.Right(2) == "LB") || (strType.Right(2) == "LS") || (strType.Right(2) == "TS") || (strType.Right(2) == "WB") || (strType.Right(2) == "LC") || (strType.Right(2) == "EM"))
				{
					// tromvs-97 6.29.18
					CreateFormattedInventory = strPre + strNumber + "-" + strSuff;
				}
				else if ((strType.Right(2) == "SW") || (strType.Right(2) == "AG"))
				{
					if (strSuff.Trim() == "")
					{
						CreateFormattedInventory = strPre + strNumber + strSuff;
					}
					else
					{
						CreateFormattedInventory = strPre + strNumber + "-" + strSuff;
					}
				}
				else if ((strType.Right(2) == "AM") || (strType.Right(2) == "AP") || (strType.Right(2) == "AQ") || (strType.Right(2) == "CI") || (strType.Right(2) == "CM") || (strType.Right(2) == "FM") || (strType.Right(2) == "MC") || (strType.Right(2) == "MM") || (strType.Right(2) == "MP") || (strType.Right(2) == "MQ") || (strType.Right(2) == "SE") || (strType.Right(2) == "SR") || (strType.Right(2) == "TR") || (strType.Right(2) == "TX"))
				{
					// "BU"
					if (strNumber.Trim().Length <= 4)
					{
						CreateFormattedInventory = strPre + strNumber + strSuff;
					}
					else if (strNumber.Trim().Length == 5)
					{
						CreateFormattedInventory = strPre + strNumber.Trim().Left(2) + "-" + strNumber.Trim().Right(3) + strSuff;
					}
					else
					{
						CreateFormattedInventory = strPre + strNumber.Trim().Left(3) + "-" + strNumber.Trim().Right(3) + strSuff;
					}
				}
				else if ((strType.Right(2) == "PC") || (strType.Right(2) == "MH"))
				{
					if (strSuff == "")
					{
						if (strNumber.Trim().Length <= 4)
						{
							CreateFormattedInventory = strPre + strNumber + strSuff;
						}
						else if (strNumber.Trim().Length == 5)
						{
							CreateFormattedInventory = strPre + strNumber.Trim().Left(2) + "-" + strNumber.Trim().Right(3) + strSuff;
						}
						else
						{
							CreateFormattedInventory = strPre + strNumber.Trim().Left(3) + "-" + strNumber.Trim().Right(3) + strSuff;
						}
					}
					else
					{
						CreateFormattedInventory = strPre + strNumber + strSuff;
					}
				}
				else if ((strType.Right(2) == "CO") || (strType.Right(2) == "TT"))
				{
					if (strPre == "")
					{
						if (strNumber.Trim().Length <= 4)
						{
							CreateFormattedInventory = strPre + strNumber + strSuff;
						}
						else if (strNumber.Trim().Length == 5)
						{
							CreateFormattedInventory = strPre + strNumber.Trim().Left(2) + "-" + strNumber.Trim().Right(3) + strSuff;
						}
						else
						{
							CreateFormattedInventory = strPre + strNumber.Trim().Left(3) + "-" + strNumber.Trim().Right(3) + strSuff;
						}
					}
					else
					{
						if (!strPre.IsValidInt32())
						{
							if (strPre.Length > 2)
							{
								CreateFormattedInventory = strPre.Left(2) + "-" + strPre.Right(strPre.Length - 2) + strNumber + strSuff;
							}
							else
							{
								CreateFormattedInventory = strPre + "-" + strNumber + strSuff;
							}
						}
						else
						{
							if (strPre.Length > 3)
							{
								CreateFormattedInventory = strPre.Left(3) + "-" + strPre.Right(strPre.Length - 3) + strNumber + strSuff;
							}
							else
							{
								CreateFormattedInventory = strPre + "-" + strNumber + strSuff;
							}
						}
					}
				}
				else if (strType.Right(2) == "ST")
				{
					if (strPre == "")
					{
						CreateFormattedInventory = strPre + strNumber + strSuff;
					}
					else
					{
						if (strPre.Length == 1)
						{
							CreateFormattedInventory = strPre + strNumber.Trim().Left(2) + "-" + strNumber.Trim().Right(3) + strSuff;
						}
						else
						{
							CreateFormattedInventory = strPre + strNumber.Trim().Left(1) + "-" + strNumber.Trim().Right(3) + strSuff;
						}
					}
				}
				else if (strType.Right(2) == "TL")
				{
					if (strPre == "")
					{
						if (strNumber.Trim().Length <= 4)
						{
							CreateFormattedInventory = strPre + strNumber + strSuff;
						}
						else if (strNumber.Trim().Length == 5)
						{
							CreateFormattedInventory = strPre + strNumber.Trim().Left(2) + "-" + strNumber.Trim().Right(3) + strSuff;
						}
						else if (strNumber.Trim().Length == 7 || (strNumber.Trim().Length == 6 && strSuff != ""))
						{
							CreateFormattedInventory = strPre + strNumber + strSuff;
						}
						else
						{
							CreateFormattedInventory = strPre + strNumber.Trim().Left(3) + "-" + strNumber.Trim().Right(3) + strSuff;
						}
					}
					else
					{
						CreateFormattedInventory = strPre + strNumber + strSuff;
					}
				}
				else if (strType.Right(2) == "VT")
				{
					if (strPre == "")
					{
						CreateFormattedInventory = strPre + strNumber + strSuff;
					}
					else
					{
						if (strPre.Length > 2)
						{
							CreateFormattedInventory = strPre.Left(2) + "-" + strPre.Right(strPre.Length - 2) + strNumber + strSuff;
						}
						else
						{
							CreateFormattedInventory = strPre + "-" + strNumber + strSuff;
						}
					}
				}
				else if (strType.Right(2) == "LT")
				{
					CreateFormattedInventory = strNumber.Left(2) + "-" + strNumber.Right(strNumber.Length - 2) + strSuff;
				}
				else
				{
					CreateFormattedInventory = strPre + strNumber + strSuff;
				}
			}
			return CreateFormattedInventory;
		}


	}
}
