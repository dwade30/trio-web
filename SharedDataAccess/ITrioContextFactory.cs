﻿
using SharedApplication.CentralData;
using SharedDataAccess.AccountsReceivable;
using SharedDataAccess.Budgetary;
using SharedDataAccess.CashReceipts;
using SharedDataAccess.CentralData;
using SharedDataAccess.CentralDocuments;
using SharedDataAccess.CentralParties;
using SharedDataAccess.Clerk;
using SharedDataAccess.ClientSettings;
using SharedDataAccess.MotorVehicle;
using SharedDataAccess.Payroll;
using SharedDataAccess.PersonalProperty;
using SharedDataAccess.RealEstate;
using SharedDataAccess.SystemSettings;
using SharedDataAccess.TaxCollections;
using SharedDataAccess.UtilityBilling;

namespace SharedDataAccess
{
    public interface ITrioContextFactory
    {
        RealEstateContext GetRealEstateContext(bool useNoChangeTracking = false);
        PersonalPropertyContext GetPersonalPropertyContext(bool useNoChangeTracking = false);
		SystemSettingsContext GetSystemSettingsContext(bool useNoChangeTracking = false);
		ClientSettingsContext GetClientSettingsContext(bool useNoChangeTracking = false);
        UtilityBillingContext GetUtilityBillingContext(bool useNoChangeTracking = false);
		TaxCollectionsContext GetTaxCollectionsContext(bool useNoChangeTracking = false);
		AccountsReceivableContext GetAccountsReceivableContext(bool useNoChangeTracking = false);
		CashReceiptsContext GetCashReceiptsContext(bool useNoChangeTracking = false);
		CentralDataContext GetCentralDataContext(bool useNoChangeTracking = false);
        CentralDocumentsContext GetCentralDocumentsContext(bool useNoChangeTracking = false);
        ClerkContext GetClerkContext(bool useNoChangeTracking = false);
        CentralPartyContext GetCentralPartyContext(bool useNoChangeTracking = false);
        BudgetaryContext GetBudgetaryContext(bool useNoChangeTracking = false);
        MotorVehicleContext GetMotorVehicleContext(bool useNoChangeTracking = false);
        PayrollContext GetPayrollContext(bool useNoChangeTracking = false);
        void UpdateContextDetails(string environment);
    }
}