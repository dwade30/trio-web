﻿using System;
using SharedApplication.Models;
using SharedApplication.Versioning;

namespace SharedDataAccess
{
    public class cClientSettingsDB : cDatabaseVersionUpdater
    {

        public cClientSettingsDB(SQLConfigInfo sqlInfo, string dataEnvironment)
        {
            defaultDatabaseName = "ClientSettings";
            dbInfo = sqlInfo;
            DataEnvironment = dataEnvironment;
        }
        protected override bool PerformChecks(cVersionInfo currentVersion)
        {
            CheckFunction(AddDBVersion,currentVersion,new cVersionInfo(){Major = 6,Minor = 0, Revision = 0,Build = 0});
			CheckFunction(AddSiteVersionTable,currentVersion,new cVersionInfo(){Major = 6, Minor = 0, Revision = 1, Build = 0});
            CheckFunction(AddUserTable, currentVersion, new cVersionInfo() { Major = 6, Minor = 0, Revision = 2, Build = 0 });
            CheckFunction(AddClientTable,currentVersion,new cVersionInfo(){Major = 6, Minor = 0, Revision = 3, Build = 0});
            CheckFunction(AddPasswordHistoryTable, currentVersion, new cVersionInfo() { Major = 6, Minor = 0, Revision = 4, Build = 0 });
            CheckFunction(AddScheduledUpdatesTable, currentVersion, new cVersionInfo() { Major = 6, Minor = 0, Revision = 5, Build = 0 });
            return true;
        }

        protected override bool CheckForDatabase()
        {
            if (!base.CheckForDatabase())
            {
                CreateDB();
            }
            
            return base.CheckForDatabase();
        }

        private void CreateDB()
        {
            try
            {
                var sql = "Create Database [" + MakeDBName() + "]";
                var connectionString = base.GetMasterConnectionString();
                ExecuteStatement(sql,connectionString);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                LastErrorMessage = ex.Message;
            }
        }

        private bool AddDBVersion()
        {
            if (!TableExists("DBVersion"))
            {
                var sql = GetTableCreationHeaderSQL("DBVersion");
                sql += "[Build] [int]  NULL,";
                sql += "[Major] [int] NULL,";
                sql += "[Minor] [int] NULL,";
                sql += "[Revision] [int] NULL ";
                sql += GetTablePKSql("DBVersion");
				ExecuteStatement(sql);
			}

            return true;
        }

        private bool AddSiteVersionTable()
        {
            if (!TableExists("SiteVersion"))
            {
                var sql = GetTableCreationHeaderSQL("SiteVersion");
                sql += "[Version] [nvarchar] (50) NULL";
                sql += GetTablePKSql("SiteVersion");
                ExecuteStatement(sql);
            }
            return true;
        }

        private bool AddScheduledUpdatesTable()
        {
            if (!TableExists("ScheduledUpdates"))
            {
                var sql  = GetTableCreationHeaderSQL("ScheduledUpdates");
                sql += "[ScheduledByUserId] [int] NOT NULL,";
                sql += "[ScheduledDateTime] [datetime] NOT NULL, ";
                sql += "[Completed] [bit] NULL,";
                sql += "[UpdateResult] [nvarchar] (255) Null, ";
                sql += GetTablePKSql("ScheduledUpdates");
                ExecuteStatement(sql);
            }

            return true;
        }

        private bool AddUserTable()
        {
            if (!TableExists("Users"))
            {
                var sql = GetTableCreationHeaderSQL("Users");
                sql += "[UserID] [nvarchar] (255) NULL,";
                sql += "[OPID] [nvarchar] (255) NULL,";
                sql += "[UserName] [nvarchar] (255) NULL,";
                sql += "[Frequency] [int] NULL,";
                sql += "[DateChanged] [datetime] NULL, ";
                sql += "[UpdateDate] [datetime] NULL,";
                sql += "[Password] [nvarchar] (Max) NULL,";
                sql += "[DefaultAdvancedSearch] [bit] NULL,";
                sql += "[LockedOut] [bit] Null, ";
                sql += "[Inactive] [bit] NULL,";
                sql += "[FailedAttempts] [int] NULL,";
                sql += "[LockoutDateTime] [datetime] NULL,";
                sql += "[Salt] [nvarchar] (255) Null, ";
                sql += "[CanUpdateSite] [bit] NULL,";
                sql += "[ClientIdentifier] [UniqueIdentifier] Not Null, ";
                sql += "[UserIdentifier] [UniqueIdentifier] Not Null ";
                sql += GetTablePKSql("Users");
                ExecuteStatement(sql);
            }

            return true;
        }

        private bool AddClientTable()
        {
            if (!TableExists("Clients"))
            {
                var sql = GetTableCreationHeaderSQL("Clients");
                sql += "[Name] [nvarchar] (255) NULL,";
                sql += "[ClientIdentifier] [UniqueIdentifier] Not Null ";
                sql += GetTablePKSql("Clients");
                ExecuteStatement(sql);
            }

            return true;
        }

        private bool AddPasswordHistoryTable()
        {
            if (!TableExists("PasswordHistory"))
            {
                var sql = GetTableCreationHeaderSQL("PasswordHistory");
                sql += "[UserId] [int] NOT NULL,";
                sql += "[Password] [nvarchar] (Max) NULL,";
                sql += "[Salt] [nvarchar] (255) NULL,";
                sql += "[DateAdded] [datetime] NULL";
                sql += GetTablePKSql("PasswordHistory");
                ExecuteStatement(sql);
            }

            return true;
        }
    }
}