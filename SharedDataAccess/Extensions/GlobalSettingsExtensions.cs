﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SharedApplication;

namespace SharedDataAccess.Extensions
{
    public static class GlobalSettingsExtensions
    {
        public static DataContextDetails ToDataContextDetails(this cGlobalSettings globalSettings)
        {
            return new DataContextDetails()
            {
                DataEnvironment = globalSettings.DataEnvironment,
                DataSource = globalSettings.DataSource,
                Password = globalSettings.Password,
                UserID = globalSettings.UserName,
                EnvironmentGroup = globalSettings.EnvironmentGroup
            };
        }
    }
}