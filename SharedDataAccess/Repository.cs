﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using SharedApplication;

namespace SharedDataAccess
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext repoContext;
        //protected  DbContext repoContext;
        public Repository(DbContext context)
        {
            repoContext = context;
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return repoContext.Set<TEntity>().Where(predicate).ToList();
        }

        public void Add(TEntity entity)
        {
            if (entity != null)
            {
                repoContext.Set<TEntity>().Add(entity);
                repoContext.SaveChanges();
            }
        }

        //public void Remove(int id)
        //{ //requires forcing entities to be IEntities<int> or IEntities<TIdType>
        //    if (id > 0)
        //    {
        //        var entity = repoContext.Set<TEntity>().Local.First(e => e.Id == id);
        //        if (entity == null)
        //        {
        //            entity = new TEntity(){Id = id};
        //            repoContext.Set<TEntity>().Attach(entity);
        //        }

        //        repoContext.Set<TEntity>().Remove(entity);
        //    }
        //}

        public void Remove(TEntity entity)
        {
            if (entity != null)
            {
                repoContext.Set<TEntity>().Remove(entity);
                repoContext.SaveChanges();
            }
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities != null )
            {
                repoContext.Set<TEntity>().AddRange(entities);
                repoContext.SaveChanges();
            }
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            if (entities != null)
            {
                repoContext.Set<TEntity>().RemoveRange(entities);
                repoContext.SaveChanges();
            }
        }

        public TEntity Get(int id)
        {
            return repoContext.Set<TEntity>().Find(id);
        }


        public IEnumerable<TEntity> GetIncluding(Expression<Func<TEntity,bool>> whereProperty,params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> getQuery = repoContext.Set<TEntity>();
            if (whereProperty != null)
            {
                getQuery = getQuery.Where(whereProperty);
            }

            foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
            {
                getQuery = getQuery.Include<TEntity, object>(includeProperty);
            }

            return getQuery;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return repoContext.Set<TEntity>().ToList();
        }
    }
}
