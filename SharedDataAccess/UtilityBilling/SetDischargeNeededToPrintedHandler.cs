﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class SetDischargeNeededToPrintedHandler : CommandHandler<SetDischargeNeededToPrinted>
	{
		private UtilityBillingContext ubContext;

		public SetDischargeNeededToPrintedHandler(ITrioContextFactory trioContextFactory)
		{
			this.ubContext = trioContextFactory.GetUtilityBillingContext();
		}

		protected override void Handle(SetDischargeNeededToPrinted command)
		{
			var dischargeNeeded = ubContext.DischargeNeededs.FirstOrDefault(dn => dn.LienId == command.LienId);
			if (dischargeNeeded == null)
			{
				dischargeNeeded = new DischargeNeeded()
				{
					Teller = "Admin",
					Batch = false,
					BillId = 0,
					Book = 0,
					Cancelled = false,
					LienId = command.LienId,
					Page = 0,
					DatePaid = command.DatePaid
				};
				ubContext.DischargeNeededs.Add(dischargeNeeded);
			}

			dischargeNeeded.UpdatedDate = command.UpdatedDate;
			dischargeNeeded.Printed = true;
			ubContext.SaveChanges();
		}
	}
}
