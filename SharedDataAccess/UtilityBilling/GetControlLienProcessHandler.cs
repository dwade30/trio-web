﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedDataAccess.UtilityBilling
{
	public class GetControlLienProcessHandler : CommandHandler<GetControlLienProcess, ControlLienProcess>
	{
		private IUtilityBillingContext ubContext;
		public GetControlLienProcessHandler(IUtilityBillingContext ubContext)
		{
			this.ubContext = ubContext;
		}
		protected override ControlLienProcess Handle(GetControlLienProcess command)
		{
			if (command.Service == UtilityType.Water)
			{
				return ubContext.WaterControlLienProcesses.FirstOrDefault();
			}
			else
			{
				return ubContext.SewerControlLienProcesses.FirstOrDefault();
			}
		}
	}
}
