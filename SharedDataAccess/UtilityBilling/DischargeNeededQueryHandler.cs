﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class DischargeNeededQueryHandler : IQueryHandler<DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>>
	{
		private IUtilityBillingContext utilityBillingContext;

		public DischargeNeededQueryHandler(ITrioContextFactory contextFactory)
		{
			this.utilityBillingContext = contextFactory.GetUtilityBillingContext();
		}

		public IEnumerable<DischargeNeeded> ExecuteQuery(DischargeNeededSearchCriteria query)
		{
			IQueryable<DischargeNeeded> dischargeNeededQuery;

			dischargeNeededQuery = utilityBillingContext.DischargeNeededs;

			if (query.Ids != null)
			{
				dischargeNeededQuery = dischargeNeededQuery.Where(x => query.Ids.Contains(x.LienId ?? 0));
			}

			return dischargeNeededQuery.ToList();
		}
	}
}
