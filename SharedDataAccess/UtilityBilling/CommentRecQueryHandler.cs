﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class CommentRecQueryHandler : IQueryHandler<CommentSearchCriteria, IEnumerable<CommentRec>>
	{
		private IUtilityBillingContext utilityBillingContext;

		public CommentRecQueryHandler(ITrioContextFactory contextFactory)
		{
			this.utilityBillingContext = contextFactory.GetUtilityBillingContext();
		}

		public IEnumerable<CommentRec> ExecuteQuery(CommentSearchCriteria query)
		{
			IQueryable<CommentRec> commentQuery;

			commentQuery = utilityBillingContext.CommentRecs;

			if (query.AccountId > 0)
			{
				commentQuery = commentQuery.Where(x => x.AccountId == query.AccountId);
			}

			return commentQuery.ToList();
		}
	}
}
