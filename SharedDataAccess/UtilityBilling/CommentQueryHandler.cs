﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class CommentQueryHandler : IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>
	{
		private IUtilityBillingContext utilityBillingContext;

		public CommentQueryHandler(ITrioContextFactory contextFactory)
		{
			this.utilityBillingContext = contextFactory.GetUtilityBillingContext();
		}

		public IEnumerable<Comment> ExecuteQuery(CommentSearchCriteria query)
		{
			IQueryable<Comment> commentQuery;

			commentQuery = utilityBillingContext.Comments;

			if (query.AccountId > 0)
			{
				commentQuery = commentQuery.Where(x => x.Account == query.AccountId);
			}

			return commentQuery.ToList();
		}
	}
}
