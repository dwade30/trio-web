﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedDataAccess.UtilityBilling
{
	public class CreateBatchRecoverEntryHandler : CommandHandler<CreateBatchRecoverEntry, int>
	{
		private UtilityBillingContext utilityBillingContext;

		public CreateBatchRecoverEntryHandler(ITrioContextFactory trioContextFactory)
		{
			utilityBillingContext = trioContextFactory.GetUtilityBillingContext();
		}

		protected override int Handle(CreateBatchRecoverEntry command)
		{
			try
			{
				utilityBillingContext.BatchRecovers.Add(command.BatchRecoverData);
				utilityBillingContext.SaveChanges();

				return command.BatchRecoverData.Id;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return 0;
			}
		}
	}
}
