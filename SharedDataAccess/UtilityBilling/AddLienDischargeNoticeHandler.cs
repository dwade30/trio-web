﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class AddLienDischargeNoticeHandler : CommandHandler<AddLienDischargeNotice>
	{
		private UtilityBillingContext ubContext;
		public AddLienDischargeNoticeHandler(ITrioContextFactory trioContextFactory)
		{
			this.ubContext = trioContextFactory.GetUtilityBillingContext();
		}
		protected override void Handle(AddLienDischargeNotice command)
		{
			var discharge = ubContext.DischargeNeededs.FirstOrDefault(d => d.LienId == command.LienId);
			if (discharge == null)
			{
				discharge = new DischargeNeeded()
				{
					Batch = false,
					BillId = command.BillId,
					Book = 0,
					DatePaid = command.DatePaid,
					LienId = command.LienId,
					Cancelled = false,
					Page = 0,
					Printed = false,
					Teller = command.Teller,
					UpdatedDate = DateTime.Now
				};
				ubContext.Add(discharge);
				ubContext.SaveChanges();
			}

		}
	}
}
