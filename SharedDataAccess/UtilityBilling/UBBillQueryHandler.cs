﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedDataAccess.UtilityBilling
{
	public class UBBillQueryHandler : IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>>
	{
		private IUtilityBillingContext utilityBillingContext;
		private IBillCalculationService billCalculationService;

		public UBBillQueryHandler(ITrioContextFactory contextFactory, IBillCalculationService billCalculationService)
		{
			this.utilityBillingContext = contextFactory.GetUtilityBillingContext();
			this.billCalculationService = billCalculationService;
		}

		private List<UBPayment> CreatePaymentsList(IEnumerable<PaymentRec> payments, UtilityType service)
		{
			//return payments.Where(z => !(z.LienPayment ?? false) && z.Service == (service == UtilityType.Water ? "W" : "S") && z.Code != "I").Select(y =>
            var ubPayments = payments.Where(z => !(z.LienPayment ?? false) && z.Service == (service == UtilityType.Water ? "W" : "S") ).Select(y =>
				new UBPayment
				{
					Id = y.ID,
					ReceiptId = y.ReceiptNumber,
					ActualSystemDate = y.ActualSystemDate,
					Tax = y.Tax ?? 0,
					Code = y.Code,
					TransactionId = y.TransactionIdentifier,
					CurrentInterest = y.CurrentInterest ?? 0,
					Principal = y.Principal ?? 0,
					Reference = y.Reference,
					Teller = y.Teller,
					PaidBy = y.PaidBy,
					Comments = y.Comments,
					RecordedTransactionDate = y.RecordedTransactionDate,
					GeneralLedger = y.GeneralLedger,
					DailyCloseOut = y.DailyCloseOut,
					BudgetaryAccountNumber = y.BudgetaryAccountNumber,
					CashDrawer = y.CashDrawer,
					BillId = y.BillId ?? 0,
					EffectiveInterestDate = y.EffectiveInterestDate,
					Pending = false,
					ChargedInterestDate = y.CHGINTDate,
					IsReversal = false,
					ReversedPaymentId = 0,
					Cost = y.LienCost ?? 0,
					PreLienInterest = y.PreLienInterest ?? 0,
					ChargedInterestRecord = payments
						.Where(a => a.TransactionIdentifier == y.TransactionIdentifier && a.TransactionIdentifier != null && a.Code == "I" && a.ID != y.ID).Select(
							b => new UBPayment
							{
								Id = b.ID,
								ReceiptId = b.ReceiptNumber,
								ActualSystemDate = b.ActualSystemDate,
								Tax = b.Tax ?? 0,
								Code = b.Code,
								TransactionId = b.TransactionIdentifier,
								CurrentInterest = b.CurrentInterest ?? 0,
								Principal = b.Principal ?? 0,
								Reference = b.Reference,
								Teller = b.Teller,
								PaidBy = b.PaidBy,
								Comments = b.Comments,
								RecordedTransactionDate = b.RecordedTransactionDate,
								GeneralLedger = b.GeneralLedger,
								DailyCloseOut = b.DailyCloseOut,
								BudgetaryAccountNumber = b.BudgetaryAccountNumber,
								CashDrawer = b.CashDrawer,
								BillId = b.BillId ?? 0,
								EffectiveInterestDate = b.EffectiveInterestDate,
								Pending = false,
								ChargedInterestDate = b.CHGINTDate,
								IsReversal = false,
								ReversedPaymentId = 0,
								Cost = b.LienCost ?? 0,
								PreLienInterest = b.PreLienInterest ?? 0
							}).FirstOrDefault()
				}).ToList();

            var idList = ubPayments.Where(p => p.ChargedInterestRecord != null).Select(p => p.ChargedInterestRecord.Id).ToList();
            if (idList.Any())
            {
                foreach (var id in idList)
                {
                    ubPayments.RemoveAll(p => p.Id == id);
                }
            }

            return ubPayments;
        }

		private List<UBPayment> CreatePaymentsListLien(IEnumerable<PaymentRec> payments, UtilityType service)
		{
			//return payments.Where(z => z.Code != "I").Select(y =>
			var ubPayments = payments.Select(y =>
				new UBPayment
				{
					Id = y.ID,
					ReceiptId = y.ReceiptNumber,
					ActualSystemDate = y.ActualSystemDate,
					Tax = y.Tax ?? 0,
					Code = y.Code,
					TransactionId = y.TransactionIdentifier,
					CurrentInterest = y.CurrentInterest ?? 0,
					Principal = y.Principal ?? 0,
					Reference = y.Reference,
					Teller = y.Teller,
					PaidBy = y.PaidBy,
					Comments = y.Comments,
					RecordedTransactionDate = y.RecordedTransactionDate,
					GeneralLedger = y.GeneralLedger,
					DailyCloseOut = y.DailyCloseOut,
					BudgetaryAccountNumber = y.BudgetaryAccountNumber,
					CashDrawer = y.CashDrawer,
					BillId = y.LienId ?? 0,
					EffectiveInterestDate = y.EffectiveInterestDate,
					Pending = false,
					ChargedInterestDate = y.CHGINTDate,
					IsReversal = false,
					ReversedPaymentId = 0,
					Cost = y.LienCost ?? 0,
					PreLienInterest = y.PreLienInterest ?? 0,
					ChargedInterestRecord = payments
						.Where(a => a.TransactionIdentifier == y.TransactionIdentifier && a.Code == "I" && a.TransactionIdentifier!= null && a.ID != y.ID).Select(
							b => new UBPayment
							{
								Id = b.ID,
								ReceiptId = b.ReceiptNumber,
								ActualSystemDate = b.ActualSystemDate,
								Tax = b.Tax ?? 0,
								Code = b.Code,
								TransactionId = b.TransactionIdentifier,
								CurrentInterest = b.CurrentInterest ?? 0,
								Principal = b.Principal ?? 0,
								Reference = b.Reference,
								Teller = b.Teller,
								PaidBy = b.PaidBy,
								Comments = b.Comments,
								RecordedTransactionDate = b.RecordedTransactionDate,
								GeneralLedger = b.GeneralLedger,
								DailyCloseOut = b.DailyCloseOut,
								BudgetaryAccountNumber = b.BudgetaryAccountNumber,
								CashDrawer = b.CashDrawer,
								BillId = b.LienId ?? 0,
								EffectiveInterestDate = b.EffectiveInterestDate,
								Pending = false,
								ChargedInterestDate = b.CHGINTDate,
								IsReversal = false,
								ReversedPaymentId = 0,
								Cost = b.LienCost ?? 0,
								PreLienInterest = b.PreLienInterest ?? 0
							}).FirstOrDefault()
				}).ToList();
            var idList = ubPayments.Where(p => p.ChargedInterestRecord != null).Select(p => p.ChargedInterestRecord.Id).ToList();
            if (idList.Any())
            {
                foreach (var id in idList)
                {
                    ubPayments.RemoveAll(p => p.Id == id);
                }
            }
			return ubPayments;
        }

		private List<UBBillUtilityDetails> CreateUtilityDetails(Bill bill, RateKey rate, IEnumerable<PaymentRec> payments)
		{
			var results = new List<UBBillUtilityDetails>();

			if (bill.Service == "W" || bill.Service == "B")
			{
				results.Add(new UBBillUtilityDetails
				{
					IntPaid = (decimal)(bill.WIntPaid ?? 0),
					TaxOwed = (decimal)(bill.WTaxOwed ?? 0),
					TaxPaid = (decimal)(bill.WTaxPaid ?? 0),
					IntAdded = (decimal)(bill.WIntAdded ?? 0),
					PrincipalPaid = (decimal)(bill.WPrinPaid ?? 0),
					PrincipalOwed = (decimal)(bill.WPrinOwed ?? 0),
					CurrentInterest = 0,
					IntPaidDate = bill.WIntPaidDate,
					BillDate = rate == null ? null : rate.BillDate,
					InterestRate = rate?.WIntRate ?? 0,
					InterestStartDate = rate?.IntStart,
					LastInterestDate = bill.WIntPaidDate ?? (rate == null ? null : rate.IntStart),
					PreviousInterestDate = bill.WPreviousInterestPaidDate,
					SavedPreviousInterestDate = bill.WPreviousInterestPaidDate,
					CurrentInterestDate = null,
					PerDiem = 0,
					RateCreatedDate = rate?.DateCreated ?? null,
					RateDescription = rate?.Description ?? "",
					RateStart = rate?.Start ?? null,
					RateEnd = rate?.End ?? null,
					PendingChargedInterest = 0,
					PendingCurrentInterestAmount = 0,
					PendingPreLienInterestAmount = 0,
					PendingPrincipalAmount = 0,
					PendingTaxAmount = 0,
					BillOwner = bill.WBillOwner ?? false,
					CostAdded = (decimal)(bill.WCostAdded ?? 0),
					CostOwed = (decimal)(bill.WCostOwed ?? 0),
					CostPaid = (decimal)(bill.WCostPaid ?? 0),
					DemandGroupId = bill.WDemandGroupID ?? 0,
					IntOwed = (decimal)(bill.WIntOwed ?? 0),
					IsLiened = (bill.WLienRecordNumber ?? 0) != 0,
					IsLien = false,
					LienId = bill.WLienRecordNumber ?? 0,
					LienMainBill = bill.WCombinationLienKey == bill.ID,
					MaturityFee = 0,
					PendingCostAmount = 0,
					PreLienInterestPaid = 0,
					Service = UtilityType.Water,
					Payments = CreatePaymentsList(payments, UtilityType.Water)
				});
			}

			if (bill.Service == "S" || bill.Service == "B")
			{
				results.Add(new UBBillUtilityDetails
				{
					IntPaid = (decimal)(bill.SIntPaid ?? 0),
					TaxOwed = (decimal)(bill.STaxOwed ?? 0),
					TaxPaid = (decimal)(bill.STaxPaid ?? 0),
					IntAdded = (decimal)(bill.SIntAdded ?? 0),
					PrincipalPaid = (decimal)(bill.SPrinPaid ?? 0),
					PrincipalOwed = (decimal)(bill.SPrinOwed ?? 0),
					CurrentInterest = 0,
					IntPaidDate = bill.SIntPaidDate,
					InterestRate = rate?.SIntRate ?? 0,
					InterestStartDate = rate?.IntStart,
					BillDate = rate == null ? null : rate.BillDate,
					LastInterestDate = bill.SIntPaidDate ?? (rate == null ? null : rate.IntStart),
					PreviousInterestDate = bill.SPreviousInterestPaidDate,
					SavedPreviousInterestDate = bill.SPreviousInterestPaidDate,
					CurrentInterestDate = null,
					PerDiem = 0,
					RateCreatedDate = rate?.DateCreated ?? null,
					RateDescription = rate?.Description ?? "",
					RateStart = rate?.Start ?? null,
					RateEnd = rate?.End ?? null,
					PendingChargedInterest = 0,
					PendingCurrentInterestAmount = 0,
					PendingPreLienInterestAmount = 0,
					PendingPrincipalAmount = 0,
					PendingTaxAmount = 0,
					BillOwner = bill.SBillOwner ?? false,
					CostAdded = (decimal)(bill.SCostAdded ?? 0),
					CostOwed = (decimal)(bill.SCostOwed ?? 0),
					CostPaid = (decimal)(bill.SCostPaid ?? 0),
					DemandGroupId = bill.SDemandGroupID ?? 0,
					IntOwed = (decimal)(bill.SIntOwed ?? 0),
					IsLiened = (bill.SLienRecordNumber ?? 0) != 0,
					IsLien = false,
					LienId = bill.SLienRecordNumber ?? 0,
					LienMainBill = bill.SCombinationLienKey == bill.ID,
					MaturityFee = 0,
					PendingCostAmount = 0,
					PreLienInterestPaid = 0,
					Service = UtilityType.Sewer,
					Payments = CreatePaymentsList(payments, UtilityType.Sewer)
				});
			}

			return results;
		}

		private List<UBBill> CreateLienBillList(List<int> ids)
		{
			var lienQuery = utilityBillingContext.Liens.Where(x => ids.Contains(x.ID));

			var rateKeyInfoResult = lienQuery.GroupJoin(
					utilityBillingContext.RateKeys,
					bill => bill.RateKeyId,
					rate => rate.ID,
					(lien, rate) => new { Lien = lien, Rates = rate.DefaultIfEmpty() })
				.Select(b => new { b.Lien, Rate = b.Rates.FirstOrDefault() });

			return rateKeyInfoResult.Select(x => new UBBill
			{
				Id = x.Lien.ID,
				BillStatus = x.Lien.Status,
				BillNumber = x.Lien.RateKeyId ?? 0,
				BillDate = x.Rate == null ? null : x.Rate.BillDate,
				DueDate = x.Rate == null ? null : x.Rate.DueDate,
				Bname = "",
				Bname2 = "",
				AccountId = 0,
				MapLot = "",
				IsLien = true,
				BilledAddress1 = "",
				BilledAddress2 = "",
				BilledAddress3 = "",
				BilledCity = "",
				BilledState = "",
				BilledZip = "",
				BilledZip4 = "",
				OwnerAddress1 = "",
				OwnerAddress2 = "",
				OwnerAddress3 = "",
				OwnerCity = "",
				OwnerState = "",
				OwnerZip = "",
				OwnerZip4 = "",
				CurrentReading = 0,
				PreviousReading = 0,
				CurrentReadingDate = null,
				PreviousReadingDate = null,
				Book = x.Lien.Book ?? 0,
				Page = x.Lien.Page ?? 0,
				Pending = false,
				Oname = "",
				Oname2 = "",
				FinalBill = false,
				FinalBillDate = null,
				FinalBillStartDate = null,
				FinalBillEndDate = null,
				UtilityDetails = CreateUtilityDetailsLien(x.Lien, x.Rate, x.Lien.Payments)
			}).ToList();
		}

		private List<UBBillUtilityDetails> CreateUtilityDetailsLien(Lien lien, RateKey rate, IEnumerable<PaymentRec> payments)
		{
			return new List<UBBillUtilityDetails>
			{
				new UBBillUtilityDetails
				{
					IntPaid = (decimal)(lien.IntPaid ?? 0),
					TaxOwed = (decimal)(lien.Tax ?? 0),
					TaxPaid = (decimal)(lien.TaxPaid ?? 0),
					IntAdded = (decimal)(lien.IntAdded ?? 0),
					PrincipalPaid = (decimal)(lien.PrinPaid ?? 0),
					PrincipalOwed = (decimal)(lien.Principal ?? 0),
					BillDate = rate == null ? null : rate.BillDate,
					CurrentInterest = 0,
					IntPaidDate = lien.IntPaidDate,
					InterestRate = rate?.SIntRate ?? 0,
					InterestStartDate = rate?.IntStart,
					LastInterestDate = lien.IntPaidDate ?? (rate?.IntStart),
					RateDescription = rate?.Description ?? "",
					PreviousInterestDate = lien.PreviousInterestPaidDate,
					SavedPreviousInterestDate = lien.PreviousInterestPaidDate,
					CurrentInterestDate = null,
					PerDiem = 0,
					RateCreatedDate = rate?.DateCreated,
					RateStart = rate?.Start,
					RateEnd = rate?.End,
					PendingChargedInterest = 0,
					PendingCurrentInterestAmount = 0,
					PendingPreLienInterestAmount = 0,
					PendingPrincipalAmount = 0,
					PendingTaxAmount = 0,
					BillOwner = false,
					CostAdded = 0,
					CostOwed = (decimal)(lien.Costs ?? 0),
					CostPaid = (decimal)(lien.CostPaid ?? 0),
					DemandGroupId = 0,
					IntOwed = (decimal)(lien.Interest ?? 0),
					IsLiened = false,
					IsLien = true,
					LienId = 0,
					LienMainBill = false,
					MaturityFee = (decimal)(lien.MaturityFee ?? 0),
					PendingCostAmount = 0,
					PreLienInterestPaid = (decimal)(lien.PLIPaid ?? 0),
					Service = (lien.Water ?? false) ? UtilityType.Water : UtilityType.Sewer,
					Payments = CreatePaymentsListLien(payments, (lien.Water ?? false) ? UtilityType.Water : UtilityType.Sewer)
				}
			};
		}

		public IEnumerable<UBBill> ExecuteQuery(UBBillSearchCriteria query)
		{
			var billQuery = utilityBillingContext.Bills;
			
			if (query.AccountId > 0)
			{
				billQuery = billQuery.Where(x => x.AccountId == query.AccountId);
			}

			if (query.LienId > 0)
			{
				billQuery = billQuery.Where(x => x.WLienRecordNumber == query.LienId || x.SLienRecordNumber == query.LienId);
			}

			if (query.AccountSelection != AccountStatusSelection.All)
			{
				billQuery = billQuery.Where(x =>
					(query.AccountSelection == AccountStatusSelection.DeletedOnly && x.CustomerParty.Deleted.Value ) ||
					(query.AccountSelection == AccountStatusSelection.NotDeleted && !x.CustomerParty.Deleted.Value));
			}

			if (query.VoidedSelection != VoidedStatusSelection.All)
			{
				if (query.VoidedSelection == VoidedStatusSelection.NotVoided)
				{
					billQuery = billQuery.Where(x => x.BillStatus != "V");
				}
				else
				{
					billQuery = billQuery.Where(x => x.BillStatus == "V");
				}
			}

			var rateKeyInfoResult = billQuery.GroupJoin(
				utilityBillingContext.RateKeys,
				bill => bill.BillNumber,
				rate => rate.ID,
				(bill, rate) => new { Bill = bill, Rates = rate.DefaultIfEmpty() })
				.Select(b => new { b.Bill, Rate = b.Rates.FirstOrDefault() });

			var billsResult = rateKeyInfoResult.Select(x => new UBBill
			{
				Id = x.Bill.ID,
				BillStatus = x.Bill.BillStatus,
				BillNumber = x.Bill.BillNumber ?? 0,
				BillDate = x.Rate == null ? null : x.Rate.BillDate,
				DueDate = x.Rate == null ? null : x.Rate.DueDate,
				Bname = x.Bill.BName,
				Bname2 = x.Bill.BName2 ?? "",
				AccountId = x.Bill.AccountId ?? 0,
				MapLot = x.Bill.MapLot,
				IsLien = false,
				BilledAddress1 = x.Bill.BAddress1 ?? "",
				BilledAddress2 = x.Bill.BAddress2 ?? "",
				BilledAddress3 = x.Bill.BAddress3 ?? "",
				BilledCity = x.Bill.BCity ?? "",
				BilledState = x.Bill.BState ?? "",
				BilledZip = x.Bill.BZip ?? "",
				BilledZip4 = x.Bill.BZip4 ?? "",
				OwnerAddress1 = x.Bill.OAddress1 ?? "",
				OwnerAddress2 = x.Bill.OAddress2 ?? "",
				OwnerAddress3 = x.Bill.OAddress3 ?? "",
				OwnerCity = x.Bill.OCity ?? "",
				OwnerState = x.Bill.OState ?? "",
				OwnerZip = x.Bill.OZip ?? "",
				OwnerZip4 = x.Bill.OZip4 ?? "",
				Pending = false,
				Oname = x.Bill.OName,
				Oname2 = x.Bill.OName2 ?? "",
				FinalBill = x.Bill.Final ?? false,
				FinalBillDate = x.Bill.FinalBillDate,
				FinalBillStartDate = x.Bill.FinalStartDate,
				FinalBillEndDate = x.Bill.FinalEndDate,
				CurrentReading = x.Bill.CurReading ?? 0,
				PreviousReading = x.Bill.PrevReading ?? 0,
				CurrentReadingDate = x.Bill.CurDate,
				PreviousReadingDate = x.Bill.PrevDate,
				Book = 0,
				Page = 0,
				UtilityDetails = CreateUtilityDetails(x.Bill, x.Rate, x.Bill.Payments)
			})
			.Where(z => query.PaidSelection == PaidStatusSelection.All || (query.PaidSelection == PaidStatusSelection.NotPaid && z.UtilityDetails.Any(a => a.BalanceDue > 0)) || (query.PaidSelection == PaidStatusSelection.PaidOnly && !z.UtilityDetails.Any(a => a.BalanceDue > 0)))
			.ToList();

			var lienResult = CreateLienBillList(billsResult.SelectMany(x => x.UtilityDetails).Where(z => z.LienMainBill)
				.Select(y => y.LienId).ToList());

			billsResult.AddRange(lienResult.Where(z =>
				query.PaidSelection == PaidStatusSelection.All ||
				(query.PaidSelection == PaidStatusSelection.NotPaid && z.UtilityDetails.Any(a => a.BalanceDue > 0)) ||
				(query.PaidSelection == PaidStatusSelection.PaidOnly && !z.UtilityDetails.Any(a => a.BalanceDue > 0))));

			if (query.CalculateCurrentInterest)
			{
				foreach (var bill in billsResult.Where(x => x.BillNumber != 0))
				{
					foreach (var utility in bill.UtilityDetails.Where(x => !x.IsLiened))
					{
						var result = billCalculationService.CalculateUtilityTotals(utility, query.EffectiveDate);

						if (result.InterestCalculated)
						{
							utility.LastInterestDate = result.LastInterestDate;
							utility.CurrentInterest = result.CalculatedInterestResult.Interest * -1;
							utility.CurrentInterestDate = result.CalculatedInterestDate;
							utility.PerDiem = (decimal)result.CalculatedInterestResult.PerDiem;
						}
					}
				}
			}

			return billsResult;
		}
	}
}
