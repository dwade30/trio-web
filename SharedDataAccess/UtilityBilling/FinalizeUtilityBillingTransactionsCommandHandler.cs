﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedDataAccess.UtilityBilling
{
	public class FinalizeUtilityBillingTransactionsCommandHandler : CommandHandler<FinalizeTransactions<UtilityBillingTransactionBase>, bool>
	{
		private UtilityBillingContext utilityBillingContext;
		private IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>> customerPartyQueryHandler;
		private CommandDispatcher commandDispatcher;

		public FinalizeUtilityBillingTransactionsCommandHandler(ITrioContextFactory trioContextFactory, IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>> customerPartyQueryHandler, CommandDispatcher commandDispatcher)
		{
			utilityBillingContext = trioContextFactory.GetUtilityBillingContext();
			this.customerPartyQueryHandler = customerPartyQueryHandler;
			this.commandDispatcher = commandDispatcher;
		}

		protected override bool Handle(FinalizeTransactions<UtilityBillingTransactionBase> command)
		{
			var dischargeLiens = new List<(Lien lien, DateTime recordedDate)>();

			try
			{
				foreach (var transaction in command.Transactions)
				{
					var customer = customerPartyQueryHandler.ExecuteQuery(new CustomerPartySearchCriteria
					{
						AccountNumber = transaction.AccountNumber
					}).FirstOrDefault();

					foreach (var bill in transaction.TransactionDetails.Bills)
					{
						var savedBill = new Bill();
						var savedLien = new Lien();

						if (bill.Pending)
						{
							savedBill = new Bill
							{
								AccountId = customer.Id,
								ActualAccountNumber = customer.AccountNumber,
								CreationDate = DateTime.Today,
								LastUpdatedDate = DateTime.Today,
								BillStatus = "D",
								BillNumber = 0,
								BillingYear = 0,
								MeterKey = bill.MeterId,
								Service = bill.ContainsService(UtilityType.Water) ? (bill.ContainsService(UtilityType.Sewer) ? "B" : "W") : "S",
								Book = bill.Book,
								BName = bill.Bname,
								BName2 = bill.Bname2,
								BAddress1 = bill.BilledAddress1,
								BAddress2 = bill.BilledAddress2,
								BAddress3 = bill.BilledAddress3,
								BCity = bill.BilledCity,
								BState = bill.BilledState,
								BZip = bill.BilledZip == null ? "" : bill.BilledZip.Length > 5 ? bill.BilledZip.Left(5) : bill.BilledZip,
								BZip4 = bill.BilledZip == null ? "" : bill.BilledZip.Contains("-") && bill.BilledZip.Length > 6 ? bill.BilledZip.Right(bill.BilledZip.Length - 6) : "",
								OName = bill.Oname,
								OName2 = bill.Oname2,
								OAddress1 = bill.OwnerAddress1,
								OAddress2 = bill.OwnerAddress2,
								OAddress3 = bill.OwnerAddress3,
								OCity = bill.OwnerCity,
								OState = bill.OwnerState,
								OZip = bill.OwnerZip == null ? "" : bill.OwnerZip.Length > 5 ? bill.OwnerZip.Left(5) : bill.OwnerZip,
								OZip4 = bill.OwnerZip == null ? "" : bill.OwnerZip.Contains("-") && bill.OwnerZip.Length > 6 ? bill.OwnerZip.Right(bill.OwnerZip.Length - 6) : ""
							};

							utilityBillingContext.Bills.Add(savedBill);
						}
						else
						{
							savedBill = utilityBillingContext.Bills.FirstOrDefault(x => x.ID == bill.Id);
						}

						foreach (var utility in bill.UtilityDetails)
						{
							if (!bill.Pending)
							{
								if (utility.IsLien)
								{
									savedLien = utilityBillingContext.Liens.Include(y => y.RateKey).FirstOrDefault(x => x.ID == bill.Id);
								}
								else
								{
									savedBill = utilityBillingContext.Bills.FirstOrDefault(x => x.ID == bill.Id);
								}
							}

							foreach (var payment in utility.Payments.Where(x => x.Pending))
							{
								PaymentRec newChargedInterestRecord = null;

								if (payment.ChargedInterestRecord != null)
								{
									newChargedInterestRecord = new PaymentRec
									{
										AccountKey = customer.Id,
										Year = 0,
										BillKey = bill.Id,
										BillNumber = bill.BillNumber,
										ReceiptNumber = command.ReceiptId,
										CHGINTNumber = 0,
										CHGINTDate = payment.ChargedInterestRecord.ChargedInterestDate,
										ActualSystemDate = payment.ChargedInterestRecord.ActualSystemDate,
										EffectiveInterestDate = payment.ChargedInterestRecord.EffectiveInterestDate,
										RecordedTransactionDate = payment.ChargedInterestRecord.RecordedTransactionDate,
										Teller = command.TellerId,
										TransNumber = "",
										TransactionIdentifier = transaction.Id,
										CorrelationIdentifier = transaction.CorrelationIdentifier,
										DailyCloseOut = 0,
										Reference = payment.ChargedInterestRecord.Reference,
										Code = payment.ChargedInterestRecord.Code,
										Principal = payment.ChargedInterestRecord.Principal,
										Tax = payment.ChargedInterestRecord.Tax,
										LienCost = payment.ChargedInterestRecord.Cost,
										CurrentInterest = payment.ChargedInterestRecord.CurrentInterest,
										PreLienInterest = payment.ChargedInterestRecord.PreLienInterest,
										PaidBy = payment.ChargedInterestRecord.PaidBy ?? "",
										Comments = payment.ChargedInterestRecord.Comments ?? "",
										CashDrawer = payment.ChargedInterestRecord.CashDrawer,
										GeneralLedger = payment.ChargedInterestRecord.GeneralLedger,
										BudgetaryAccountNumber = payment.ChargedInterestRecord.BudgetaryAccountNumber,
										LienPayment = utility.IsLien,
										Service = utility.ServiceCode,
										EPmtID = payment.EPaymentId
									};

									if (utility.IsLien)
									{
										newChargedInterestRecord.LienId = bill.Id;
									}
									else
									{
										newChargedInterestRecord.BillId = bill.Id;
									}

									if (payment.Reference != "REVERSE")
									{
										if (utility.IsLien)
										{
											savedLien.IntAdded = savedLien.IntAdded + (payment.ChargedInterestRecord.PreLienInterest + payment.ChargedInterestRecord.CurrentInterest).ToDouble();
										}
										else
										{
											if (utility.Service == UtilityType.Water)
											{
												savedBill.WIntAdded = (savedBill.WIntAdded ?? 0) + (payment.ChargedInterestRecord.PreLienInterest + payment.ChargedInterestRecord.CurrentInterest).ToDouble();
											}
											else
											{
												savedBill.SIntAdded = (savedBill.SIntAdded ?? 0) + (payment.ChargedInterestRecord.PreLienInterest + payment.ChargedInterestRecord.CurrentInterest).ToDouble();
											}
										}
									}
									else
									{
										if (utility.IsLien)
										{
											savedLien.IntPaid = savedLien.IntPaid + (payment.ChargedInterestRecord.CurrentInterest).ToDouble();
											savedLien.PLIPaid = savedLien.PLIPaid + (payment.ChargedInterestRecord.PreLienInterest).ToDouble();
										}
										else
										{
											if (utility.Service == UtilityType.Water)
											{
												savedBill.WIntPaid = savedBill.WIntPaid + (payment.ChargedInterestRecord.PreLienInterest + payment.ChargedInterestRecord.CurrentInterest).ToDouble();
											}
											else
											{
												savedBill.SIntPaid = savedBill.SIntPaid + (payment.ChargedInterestRecord.PreLienInterest + payment.ChargedInterestRecord.CurrentInterest).ToDouble();
											}
										}
									}
								}

								var newPayment = new PaymentRec
								{
									AccountKey = customer.Id,
									BillKey = bill.Id,
									BillNumber = bill.BillNumber,
									ReceiptNumber = command.ReceiptId,
									CHGINTNumber = 0,
									TransactionIdentifier = transaction.Id,
									CorrelationIdentifier = transaction.CorrelationIdentifier,
									ActualSystemDate = payment.ActualSystemDate,
									EffectiveInterestDate = payment.EffectiveInterestDate,
									RecordedTransactionDate = payment.RecordedTransactionDate,
									Teller = command.TellerId,
									Reference = payment.Reference,
									TransNumber = "",
									DailyCloseOut = 0,
									Code = payment.Code,
									Principal = payment.Principal,
									Tax = payment.Tax,
									LienCost = payment.Cost,
									CurrentInterest = payment.CurrentInterest,
									PreLienInterest = payment.PreLienInterest,
									PaidBy = payment.PaidBy,
									Comments = payment.Comments ?? "",
									CashDrawer = payment.CashDrawer,
									GeneralLedger = payment.GeneralLedger,
									BudgetaryAccountNumber = payment.BudgetaryAccountNumber,
									LienPayment = utility.IsLien,
									Service = utility.ServiceCode,
									EPmtID = payment.EPaymentId
								};

								if (utility.IsLien)
								{
									savedLien.Payments.Add(newPayment);
									if (newChargedInterestRecord != null)
									{
										savedLien.Payments.Add(newChargedInterestRecord);
									}

									
									savedLien.IntPaid = savedLien.IntPaid + utility.PendingCurrentInterestAmount.ToDouble();
									savedLien.PLIPaid = savedLien.PLIPaid + utility.PendingPreLienInterestAmount.ToDouble();
									savedLien.PrinPaid = savedLien.PrinPaid + utility.PendingPrincipalAmount.ToDouble();
									savedLien.TaxPaid = savedLien.TaxPaid + utility.PendingTaxAmount.ToDouble();
									savedLien.CostPaid = savedLien.CostPaid + utility.PendingCostAmount.ToDouble();
									savedLien.IntPaidDate = utility.LastInterestDate;
									savedLien.PreviousInterestPaidDate = utility.PreviousInterestDate;

									if (bill.UtilityDetails.Sum(x => x.BalanceDue) == 0)
									{
										dischargeLiens.Add((lien: (Lien)savedLien, recordedDate: transaction.EffectiveDateTime));
									}
								}
								else
								{
									savedBill.Payments.Add(newPayment);
									if (newChargedInterestRecord != null)
									{
										savedBill.Payments.Add(newChargedInterestRecord);
									}

									if (utility.Service == UtilityType.Water)
									{
										savedBill.WIntPaid = savedBill.WIntPaid + (utility.PendingCurrentInterestAmount + utility.PendingPreLienInterestAmount).ToDouble();
										savedBill.WPrinPaid = savedBill.WPrinPaid + utility.PendingPrincipalAmount.ToDouble();
										savedBill.WTaxPaid = savedBill.WTaxPaid + utility.PendingTaxAmount.ToDouble();
										savedBill.WCostPaid = savedBill.WCostPaid + utility.PendingCostAmount.ToDouble();
										savedBill.WIntPaidDate = utility.LastInterestDate;
										savedBill.WPreviousInterestPaidDate = utility.PreviousInterestDate;
									}
									else
									{
										savedBill.SIntPaid = savedBill.SIntPaid + (utility.PendingCurrentInterestAmount + utility.PendingPreLienInterestAmount).ToDouble();
										savedBill.SPrinPaid = savedBill.SPrinPaid + utility.PendingPrincipalAmount.ToDouble();
										savedBill.STaxPaid = savedBill.STaxPaid + utility.PendingTaxAmount.ToDouble();
										savedBill.SCostPaid = savedBill.SCostPaid + utility.PendingCostAmount.ToDouble();
										savedBill.SIntPaidDate = utility.LastInterestDate;
										savedBill.SPreviousInterestPaidDate = utility.PreviousInterestDate;
									}
								}
                            }
						}
					}
				}

				utilityBillingContext.SaveChanges();

				foreach (var billTuple in dischargeLiens)
				{
					var lien = billTuple.lien;
					var recordedDate = billTuple.recordedDate;
					commandDispatcher.Send(new AddLienDischargeNotice(lien.ID, recordedDate, command.TellerId, 0));
					if (command.AllowUIInteraction)
					{
						if (commandDispatcher
							.Send(new GetYesOrNoResponse(
								"Would you like to print the Lien Discharge Notice now?" + "\r\n" + "\r\n" + "Yes  -  Print Notice" + "\r\n" + "No   -  Save to Print Later", "Lien Discharge Notice", false))
							.Result)
						{
							commandDispatcher.Send(new ShowLienDischargeNotice(DateTime.Today, lien));
						}
					}
				}

				return true;
		}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
