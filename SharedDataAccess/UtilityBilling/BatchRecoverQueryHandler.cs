﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class BatchRecoverQueryHandler : IQueryHandler<BatchRecoverSearchCriteria, IEnumerable<BatchRecover>>
	{
		private IUtilityBillingContext utilityBillingContext;

		public BatchRecoverQueryHandler(ITrioContextFactory contextFactory)
		{
			this.utilityBillingContext = contextFactory.GetUtilityBillingContext();
		}

		public IEnumerable<BatchRecover> ExecuteQuery(BatchRecoverSearchCriteria query)
		{
			IQueryable<BatchRecover> batchRecoverQuery;

			batchRecoverQuery = utilityBillingContext.BatchRecovers;

			if (query.TellerId != "")
			{
				batchRecoverQuery = batchRecoverQuery.Where(x => x.TellerId == query.TellerId);
			}

			return batchRecoverQuery.ToList();
		}
	}

}
