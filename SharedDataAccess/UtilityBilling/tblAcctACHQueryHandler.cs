﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class tblAcctACHQueryHandler : IQueryHandler<tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>>
	{
		private IUtilityBillingContext utilityBillingContext;

		public tblAcctACHQueryHandler(ITrioContextFactory contextFactory)
		{
			this.utilityBillingContext = contextFactory.GetUtilityBillingContext();
		}

		public IEnumerable<tblAcctACH> ExecuteQuery(tblAcctACHSearchCriteria query)
		{
			IQueryable<tblAcctACH> tblAcctACHQuery;

			tblAcctACHQuery = utilityBillingContext.tblAcctACHs;

			if (query.AccountId > 0)
			{
				tblAcctACHQuery = tblAcctACHQuery.Where(x => x.AccountKey == query.AccountId);
			}

			return tblAcctACHQuery.ToList();
		}
	}
}
