﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class MasterQueryHandler : IQueryHandler<MasterSearchCriteria, IEnumerable<Master>>
	{
		private IUtilityBillingContext utilityBillingContext;

		public MasterQueryHandler(ITrioContextFactory contextFactory)
		{
			this.utilityBillingContext = contextFactory.GetUtilityBillingContext();
		}

		public IEnumerable<Master> ExecuteQuery(MasterSearchCriteria query)
		{
			IQueryable<Master> masterQuery;

			masterQuery = utilityBillingContext.Masters;

			if (query.Id > 0)
			{
				masterQuery = masterQuery.Where(x => x.ID == query.Id);
			}

			if (query.AccountNumber > 0)
			{
				masterQuery = masterQuery.Where(x => x.AccountNumber == query.AccountNumber);
			}

			if (query.deletedOption == DeletedSelection.NotDeleted)
			{
				masterQuery = masterQuery.Where(x => (x.Deleted ?? false) == false);
			}
			else if (query.deletedOption == DeletedSelection.OnlyDeleted)
			{
				masterQuery = masterQuery.Where(x => (x.Deleted ?? false) == true);
			}

			var result = masterQuery.Include(x => x.MeterTables).ToList();
			return result;
		}
	}
}
