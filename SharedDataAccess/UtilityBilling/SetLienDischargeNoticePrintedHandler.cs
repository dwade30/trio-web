﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;

namespace SharedDataAccess.UtilityBilling
{
	public class SetLienDischargeNoticePrintedHandler : CommandHandler<SetLienDischargeNoticePrinted>
	{
		private UtilityBillingContext ubContext;

		public SetLienDischargeNoticePrintedHandler(ITrioContextFactory trioContextFactory)
		{
			this.ubContext = trioContextFactory.GetUtilityBillingContext();
		}

		protected override void Handle(SetLienDischargeNoticePrinted command)
		{
			var lien = ubContext.Liens.FirstOrDefault(l => l.ID == command.LienId);
			if (lien != null)
			{
				lien.PrintedLDN = true;
			}

			ubContext.SaveChanges();
		}
	}
}
