﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedDataAccess.UtilityBilling
{
	public class SaveControlDischargeNoticeHandler : CommandHandler<SaveControlDischargeNotice>
	{
		private UtilityBillingContext ubContext;

		public SaveControlDischargeNoticeHandler(ITrioContextFactory trioContextFactory)
		{
			this.ubContext = trioContextFactory.GetUtilityBillingContext();
		}

		protected override void Handle(SaveControlDischargeNotice command)
		{
			if (command.DischargeNotice != null)
			{
				ControlDischargeNotice dischargeNotice;

				if (command.Service == UtilityType.Water)
				{
					dischargeNotice = ubContext.WaterControlDischargeNotices.FirstOrDefault();
				}
				else if (command.Service == UtilityType.Sewer)
				{
					dischargeNotice = ubContext.SewerControlDischargeNotices.FirstOrDefault();
				}
				else
				{
					return;
				}
				
				if (dischargeNotice == null)
				{
					dischargeNotice = new ControlDischargeNotice();
					if (command.Service == UtilityType.Water)
					{
						ubContext.WaterControlDischargeNotices.Add((WaterControlDischargeNotice)dischargeNotice);
					}
					else if (command.Service == UtilityType.Sewer)
					{
						ubContext.SewerControlDischargeNotices.Add((SewerControlDischargeNotice)dischargeNotice);
					}
				}

				dischargeNotice.CommissionExpiration = command.DischargeNotice.CommissionExpiration;
				dischargeNotice.County = command.DischargeNotice.County;
				dischargeNotice.Male = command.DischargeNotice.Male;
				dischargeNotice.SignerDesignation = command.DischargeNotice.SignerDesignation;
				dischargeNotice.SignerName = command.DischargeNotice.SignerName;
				dischargeNotice.Treasurer = command.DischargeNotice.Treasurer;
				ubContext.SaveChanges();
			}
		}
	}
}
