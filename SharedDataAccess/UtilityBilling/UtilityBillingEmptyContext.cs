﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
    public class UtilityBillingEmptyContext : IUtilityBillingContext
    {
        public IQueryable<PaymentRec> PaymentRecs
        {
            get => new List<PaymentRec>().AsQueryable();
        }
        public IQueryable<Lien> Liens
        {
            get => new List<Lien>().AsQueryable();
        }
        public IQueryable<Bill> Bills { get => new List<Bill>().AsQueryable(); }
        public IQueryable<RateKey> RateKeys { get => new List<RateKey>().AsQueryable(); }
        public IQueryable<Master> Masters { get => new List<Master>().AsQueryable(); }
        public IQueryable<MeterTable> MeterTables { get => new List<MeterTable>().AsQueryable(); }
        public IQueryable<CustomerParty> CustomerParties { get => new List<CustomerParty>().AsQueryable(); }
        public IQueryable<Comment> Comments { get => new List<Comment>().AsQueryable(); }
        public IQueryable<CommentRec> CommentRecs { get => new List<CommentRec>().AsQueryable(); }
        public IQueryable<tblAcctACH> tblAcctACHs { get => new List<tblAcctACH>().AsQueryable(); }
        public IQueryable<DischargeNeeded> DischargeNeededs { get => new List<DischargeNeeded>().AsQueryable(); }
        public IQueryable<BatchRecover> BatchRecovers { get => new List<BatchRecover>().AsQueryable(); }
        public IQueryable<UtilityBillingSetting> UtilityBillingSettings { get => new List<UtilityBillingSetting>().AsQueryable(); }
        IQueryable<WaterControlDischargeNotice> IUtilityBillingContext.WaterControlDischargeNotices { get => new List<WaterControlDischargeNotice>().AsQueryable(); }
        IQueryable<SewerControlDischargeNotice> IUtilityBillingContext.SewerControlDischargeNotices { get => new List<SewerControlDischargeNotice>().AsQueryable(); }
        IQueryable<WaterControlLienProcess> IUtilityBillingContext.WaterControlLienProcesses { get => new List<WaterControlLienProcess>().AsQueryable(); }
        IQueryable<SewerControlLienProcess> IUtilityBillingContext.SewerControlLienProcesses { get => new List<SewerControlLienProcess>().AsQueryable(); }

    }
}