﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedDataAccess.UtilityBilling
{
	public class GetControlDischargeNoticeHandler : CommandHandler<GetControlDischargeNotice, ControlDischargeNotice>
	{
		private IUtilityBillingContext ubContext;
		public GetControlDischargeNoticeHandler(IUtilityBillingContext ubContext)
		{
			this.ubContext = ubContext;
		}
		protected override ControlDischargeNotice Handle(GetControlDischargeNotice command)
		{
			if (command.Service == UtilityType.Water)
			{
				return ubContext.WaterControlDischargeNotices.FirstOrDefault();
			}
			else
			{
				return ubContext.SewerControlDischargeNotices.FirstOrDefault();
			}
		}
	}
}
