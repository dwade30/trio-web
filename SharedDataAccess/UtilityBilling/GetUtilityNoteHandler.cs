﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Notes;

namespace SharedDataAccess.UtilityBilling
{
    public class GetUtilityNoteHandler : CommandHandler<GetUtilityNote,Comment>
    {
        private IUtilityBillingContext utContext;
        public GetUtilityNoteHandler(IUtilityBillingContext utContext)
        {
            this.utContext = utContext;
        }
        protected override Comment Handle(GetUtilityNote command)
        {
            return utContext.Comments.FirstOrDefault(u => u.Account == command.Account);
        }
    }
}