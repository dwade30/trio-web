﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
	public class CustomerPartyQueryHandler : IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>
	{
		private IUtilityBillingContext utilityBillingContext;

		public CustomerPartyQueryHandler(ITrioContextFactory contextFactory)
		{
			this.utilityBillingContext = contextFactory.GetUtilityBillingContext();
		}

		public IEnumerable<UBCustomerPaymentStatusResult> ExecuteQuery(CustomerPartySearchCriteria query)
		{
			try
			{
				var result = utilityBillingContext.CustomerParties;
				var previousOwnerResult = utilityBillingContext.Bills;

				if ((query.Remote ?? "") != "" || (query.SerialNumber ?? "") != "" || (query.XRef1 ?? "") != "")
				{
					
					var meterQueryResult = result
						.Join(utilityBillingContext.MeterTables,
							cust => cust.ID,
							meter => meter.AccountKey,
							(cust, meter) => new {Customer = cust, Meter = meter});

					var returnVal = new List<UBCustomerPaymentStatusResult>();

					if ((query.Remote ?? "") != "")
					{
						if (query.MatchCriteriaType == SearchCriteriaMatchType.StartsWith)
						{
							meterQueryResult = meterQueryResult.Where(x => x.Meter.Remote.StartsWith(query.Remote));
						}
						else
						{
							meterQueryResult = meterQueryResult.Where(x => x.Meter.Remote.Contains(query.Remote));
						}
					}
					else if ((query.SerialNumber ?? "") != "")
					{
						if (query.MatchCriteriaType == SearchCriteriaMatchType.StartsWith)
						{
							meterQueryResult = meterQueryResult.Where(x => x.Meter.SerialNumber.StartsWith(query.SerialNumber));
						}
						else
						{
							meterQueryResult = meterQueryResult.Where(x => x.Meter.SerialNumber.Contains(query.SerialNumber));
						}
					}
					else
					{
						if (query.MatchCriteriaType == SearchCriteriaMatchType.StartsWith)
						{
							meterQueryResult = meterQueryResult.Where(x => x.Meter.XRef1.StartsWith(query.XRef1));
						}
						else
						{
							meterQueryResult = meterQueryResult.Where(x => x.Meter.XRef1.Contains(query.XRef1));
						}
					}

					return meterQueryResult.Select(x => new UBCustomerPaymentStatusResult
					{
						Id = x.Customer.ID,
						BilledAddress1 = x.Customer.BilledAddress1,
						BilledAddress2 = x.Customer.BilledAddress2,
						BilledAddress3 = x.Customer.BilledAddress3,
						FullNameLF = x.Customer.FullNameLF,
						SecondOwnerFullNameLF = x.Customer.Owner2FullNameLF,
						Billed2FullNameLF = x.Customer.Billed2NameLF,
						AccountNumber = x.Customer.AccountNumber ?? 0,
						BilledState = x.Customer.BilledState,
						BilledCity = x.Customer.BilledCity,
						BilledZip = x.Customer.BilledZip,
						Deleted = x.Customer.Deleted ?? false,
						PartyId = x.Customer.OwnerPartyID ?? 0,
						MapLot = x.Customer.MapLot,
						StreetName = x.Customer.StreetName,
						StreetNumber = (x.Customer.StreetNumber ?? "0").ToIntegerValue(),
						REAccount = x.Customer.REAccount ?? 0,
						BilledFullNameLF = x.Customer.BilledNameLF,
						OwnerZip = x.Customer.Zip,
						OwnerCity = x.Customer.City,
						OwnerState = x.Customer.State,
						OwnerAddress1 = x.Customer.Address1,
						OwnerAddress2 = x.Customer.Address2,
						OwnerAddress3 = x.Customer.Address3,
						PreviousOwner = false
					}).Distinct().ToList();
				}
				else
				{
					if ((query.Name ?? "") != "")
					{
						if (query.MatchCriteriaType == SearchCriteriaMatchType.StartsWith)
						{
							result = result.Where(x =>
								x.FullNameLF.StartsWith(query.Name) || x.Owner2FullNameLF.StartsWith(query.Name) || x.BilledNameLF.StartsWith(query.Name) ||
								 x.Billed2NameLF.StartsWith(query.Name));

							previousOwnerResult = previousOwnerResult.Where(x =>
								query.IncludePreviousOwners && x.BName.StartsWith(query.Name) || x.BName2.StartsWith(query.Name));
						}
						else
						{
							result = result.Where(x =>
								x.FullNameLF.Contains(query.Name) || x.Owner2FullNameLF.Contains(query.Name) ||
								x.BilledNameLF.Contains(query.Name) || x.Billed2NameLF.Contains(query.Name));

							previousOwnerResult = previousOwnerResult.Where(x =>
								query.IncludePreviousOwners && x.BName.Contains(query.Name) || x.BName2.Contains(query.Name));
						}
					}

					if ((query.Address ?? "") != "")
					{
						if (query.MatchCriteriaType == SearchCriteriaMatchType.StartsWith)
						{
							result = result.Where(x =>
								x.Address1.StartsWith(query.Address) || x.BilledAddress1.StartsWith(query.Address));

							previousOwnerResult = previousOwnerResult.Where(x =>
								query.IncludePreviousOwners && x.BAddress1.StartsWith(query.Address));
						}
						else
						{
							result = result.Where(x =>
								x.Address1.Contains(query.Address) || x.BilledAddress1.Contains(query.Address));

							previousOwnerResult = previousOwnerResult.Where(x =>
								query.IncludePreviousOwners && x.BAddress1.Contains(query.Address));
						}
					}

					if ((query.MapLot ?? "") != "")
					{
						if (query.MatchCriteriaType == SearchCriteriaMatchType.StartsWith)
						{
							result = result.Where(x =>
								x.MapLot.StartsWith(query.MapLot));

							previousOwnerResult = previousOwnerResult.Where(x =>
								query.IncludePreviousOwners && x.MapLot.StartsWith(query.MapLot));
						}
						else
						{
							result = result.Where(x =>
								x.MapLot.Contains(query.MapLot));

							previousOwnerResult = previousOwnerResult.Where(x =>
								query.IncludePreviousOwners && x.MapLot.Contains(query.MapLot));
						}
					}

					if ((query.StreetName ?? "") != "")
					{
						if (query.MatchCriteriaType == SearchCriteriaMatchType.StartsWith)
						{
							result = result.Where(x => x.StreetName.StartsWith(query.StreetName));
							previousOwnerResult = null;
						}
						else
						{
							result = result.Where(x => x.StreetName.Contains(query.StreetName));
							previousOwnerResult = null;
						}
					}

					if ((query.StreetNumber ?? "") != "")
					{
						var streetNumberValue = query.StreetNumber.ToIntegerValue();

						if (streetNumberValue > 0)
						{
							result = result.Where(x => x.StreetNumber.Contains(query.StreetNumber));
							previousOwnerResult = null;
						}
					}

					if (query.AccountNumber != 0)
					{
						result = result.Where(x => x.AccountNumber == query.AccountNumber);
						previousOwnerResult = null;
					}

					if (query.REAccount != 0)
					{
						result = result.Where(x => x.REAccount == query.REAccount);
						previousOwnerResult = null;
					}

					var returnVal = result.Select(x => new UBCustomerPaymentStatusResult
					{
						Id = x.ID,
						BilledAddress1 = x.BilledAddress1 ?? "",
						BilledAddress2 = x.BilledAddress2 ?? "",
						BilledAddress3 = x.BilledAddress3 ?? "",
						FullNameLF = x.FullNameLF ?? "",
						SecondOwnerFullNameLF = x.Owner2FullNameLF ?? "",
						Billed2FullNameLF = x.Billed2NameLF ?? "",
						AccountNumber = x.AccountNumber ?? 0,
						BilledState = x.BilledState ?? "",
						BilledCity = x.BilledCity ?? "",
						BilledZip = x.BilledZip ?? "",
						Deleted = x.Deleted ?? false,
						PartyId = x.OwnerPartyID ?? 0,
						MapLot = x.MapLot ?? "",
						StreetName = x.StreetName ?? "",
						StreetNumber = (x.StreetNumber ?? "0").ToIntegerValue(),
						REAccount = x.REAccount ?? 0,
						BilledFullNameLF = x.BilledNameLF ?? "",
						OwnerZip = x.Zip ?? "",
						OwnerCity = x.City ?? "",
						OwnerState = x.State ?? "",
						OwnerAddress1 = x.Address1 ?? "",
						OwnerAddress2 = x.Address2 ?? "",
						OwnerAddress3 = x.Address3 ?? "",
						PreviousOwner = false
					}).ToList();

					if (query.IncludePreviousOwners && previousOwnerResult != null)
					{
						var prevResults = previousOwnerResult
							.Where(z => !returnVal.Any(y => y.AccountNumber == z.ActualAccountNumber)).Select(x =>
								new UBCustomerPaymentStatusResult
								{
									Id = x.AccountId ?? 0,
									BilledAddress1 = x.BAddress1 ?? "",
									BilledAddress2 = x.BAddress2 ?? "",
									BilledAddress3 = x.BAddress3 ?? "",
									FullNameLF = x.OName ?? "",
									SecondOwnerFullNameLF = x.OName2 ?? "",
									Billed2FullNameLF = x.BName2 ?? "",
									AccountNumber = x.ActualAccountNumber ?? 0,
									BilledState = x.BState ?? "",
									BilledCity = x.BCity ?? "",
									BilledZip = x.BZip ?? "",
									OwnerZip = x.OZip ?? "",
									OwnerCity = x.OCity ?? "",
									OwnerState = x.OState ?? "",
									OwnerAddress1 = x.OAddress1 ?? "",
									OwnerAddress2 = x.OAddress2 ?? "",
									OwnerAddress3 = x.OAddress3 ?? "",
									Deleted = false,
									PartyId = 0,
									MapLot = x.MapLot ?? "",
									StreetName = x.Location ?? "",
									StreetNumber = 0,
									REAccount = 0,
									BilledFullNameLF = x.BName ?? "",
									PreviousOwner = true
								}).ToList();

						foreach (var prevResult in prevResults)
						{
							if (!returnVal.Any(x => x.AccountNumber == prevResult.AccountNumber))
							{
								returnVal.Add(prevResult);
							}
						}
					}
					return returnVal;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return new List<UBCustomerPaymentStatusResult>();
			}
		}
	}
}
