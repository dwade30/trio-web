﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedDataAccess.UtilityBilling
{
	public class UndeleteCustomerHandler : CommandHandler<UndeleteCustomer, bool>
	{
		private UtilityBillingContext utilityBillingContext;

		public UndeleteCustomerHandler(ITrioContextFactory trioContextFactory)
		{
			utilityBillingContext = trioContextFactory.GetUtilityBillingContext();
		}

		protected override bool Handle(UndeleteCustomer command)
		{
			try
			{
				var customer = utilityBillingContext.Masters.FirstOrDefault(x =>
					x.AccountNumber == command.Account);

				if (customer != null)
				{
					customer.Deleted = false;
				}

				utilityBillingContext.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
