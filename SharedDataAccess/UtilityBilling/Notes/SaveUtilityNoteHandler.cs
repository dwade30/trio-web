﻿using System.Linq;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Notes;

namespace SharedDataAccess.UtilityBilling.Notes
{
    public class SaveUtilityNoteHandler : CommandHandler<SaveUtilityNote,int>
    {
        private UtilityBillingContext utContext;

        public SaveUtilityNoteHandler(UtilityBillingContext utContext)
        {
            this.utContext = utContext;
        }
        protected override int Handle(SaveUtilityNote command)
        {
            
            var note = utContext.Comments.FirstOrDefault(n => n.Account == command.Account);
            if (note == null)
            {
                note = new Comment();
                note.Account = command.Account;
                utContext.Comments.Add(note);
            }

            note.Priority = command.Priority;
            note.Text = command.Text;
            note.Type = "UT";
            utContext.SaveChanges();

            return note.Id;
        }
    }
}