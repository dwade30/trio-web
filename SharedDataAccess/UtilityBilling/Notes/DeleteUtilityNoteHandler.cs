﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Notes;

namespace SharedDataAccess.UtilityBilling.Notes
{
    public class DeleteUtilityNoteHandler : CommandHandler<DeleteUtilityNote>
    {
        private UtilityBillingContext utContext;

        public DeleteUtilityNoteHandler(UtilityBillingContext utContext)
        {
            this.utContext = utContext;
        }
        protected override void Handle(DeleteUtilityNote command)
        {
            this.utContext.Database.ExecuteSqlCommand("delete from Comments where id = " + command.NoteId);
        }
    }
}