﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class DischargeNeededConfiguration : IEntityTypeConfiguration<DischargeNeeded>
	{
		public void Configure(EntityTypeBuilder<DischargeNeeded> builder)
		{
			builder.ToTable("DischargeNeeded");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.LienId).HasColumnName("LienKey");
			builder.Property(e => e.BillId).HasColumnName("BillKey");
			builder.Property(e => e.Cancelled).HasColumnName("Canceled");
			builder.Property(e => e.Teller).HasMaxLength(255);
		}
	}
}
