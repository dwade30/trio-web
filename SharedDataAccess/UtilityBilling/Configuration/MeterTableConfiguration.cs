﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class MeterTableConfiguration : IEntityTypeConfiguration<MeterTable>
	{
		public void Configure(EntityTypeBuilder<MeterTable> builder)
		{
			builder.ToTable("MeterTable");
			builder.Property(p => p.SerialNumber).HasMaxLength(2);
			builder.Property(p => p.Service).HasMaxLength(1);
			builder.Property(p => p.Combine).HasMaxLength(1);
			builder.Property(p => p.Location).HasMaxLength(255);
			builder.Property(p => p.AdjustDescription).HasMaxLength(50);
			builder.Property(p => p.WaterAccount1).HasMaxLength(50);
			builder.Property(p => p.WaterAccount2).HasMaxLength(50);
			builder.Property(p => p.WaterAccount3).HasMaxLength(50);
			builder.Property(p => p.WaterAccount4).HasMaxLength(50);
			builder.Property(p => p.WaterAccount5).HasMaxLength(50);
			builder.Property(p => p.WaterAdjustAccount).HasMaxLength(50);
			builder.Property(p => p.WaterAdjustDescription).HasMaxLength(50);
			builder.Property(p => p.SewerAccount1).HasMaxLength(50);
			builder.Property(p => p.SewerAccount2).HasMaxLength(50);
			builder.Property(p => p.SewerAccount3).HasMaxLength(50);
			builder.Property(p => p.SewerAccount4).HasMaxLength(50);
			builder.Property(p => p.SewerAccount5).HasMaxLength(50);
			builder.Property(p => p.SewerAdjustAccount).HasMaxLength(50);
			builder.Property(p => p.SewerAdjustDescription).HasMaxLength(50);
			builder.Property(p => p.CurrentCode).HasMaxLength(1);
			builder.Property(p => p.PreviousCode).HasMaxLength(1);
			builder.Property(p => p.Remote).HasMaxLength(50);
			builder.Property(p => p.BillingCode).HasMaxLength(6);
			builder.Property(p => p.Comment).HasMaxLength(255);
			builder.Property(p => p.BillingStatus).HasMaxLength(1);
			builder.Property(p => p.XRef1).HasMaxLength(50);
			builder.Property(p => p.XRef2).HasMaxLength(50);
			builder.Property(p => p.MXU).HasMaxLength(255);
			builder.Property(p => p.Lat).HasMaxLength(255);
			builder.Property(p => p.Long).HasMaxLength(255);
			builder.Property(p => p.ReaderCode).HasMaxLength(10);
		}
	}

}
