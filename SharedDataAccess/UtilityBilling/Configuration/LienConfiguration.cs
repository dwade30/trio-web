﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class LienConfiguration : IEntityTypeConfiguration<Lien>
	{
		public void Configure(EntityTypeBuilder<Lien> builder)
		{
			builder.ToTable("Lien");
			builder.Property(p => p.Status).HasMaxLength(50);
			builder.Property(p => p.RateKeyId).HasColumnName("RateKey");
		}
	}
}
