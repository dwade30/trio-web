﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class CustomerPartyConfiguration : IEntityTypeConfiguration<CustomerParty>
	{
		public void Configure(EntityTypeBuilder<CustomerParty> builder)
		{
			builder.ToTable("CustomerPartyView");
			builder.Property(p => p.SewerAccount).HasMaxLength(255);
			builder.Property(p => p.WaterAccount).HasMaxLength(255);
			builder.Property(p => p.Comment).HasMaxLength(1024);
			builder.Property(p => p.MapLot).HasMaxLength(255);
			builder.Property(p => p.Directions).HasMaxLength(255);
			builder.Property(p => p.BillMessage).HasMaxLength(1024);
			builder.Property(p => p.DataEntry).HasMaxLength(255);
			builder.Property(p => p.BookPage).HasMaxLength(255);
			builder.Property(p => p.Apt).HasMaxLength(255);
			builder.Property(p => p.StreetNumber).HasMaxLength(255);
			builder.Property(p => p.StreetName).HasMaxLength(255);
			builder.Property(p => p.Telephone).HasMaxLength(255);
			builder.Property(p => p.Email).HasMaxLength(255);
			builder.Property(p => p.RefAccountNumber).HasMaxLength(255);
		}
	}

}
