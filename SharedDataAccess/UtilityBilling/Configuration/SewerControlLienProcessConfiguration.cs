﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class SewerControlLienProcessConfiguration : IEntityTypeConfiguration<SewerControlLienProcess>
	{
		public void Configure(EntityTypeBuilder<SewerControlLienProcess> builder)
		{
			builder.ToTable("SControl_LienProcess");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.County).HasMaxLength(255);
			builder.Property(e => e.CollectorName).HasMaxLength(255);
			builder.Property(e => e.Muni).HasMaxLength(255);
			builder.Property(e => e.State).HasMaxLength(255);
			builder.Property(e => e.Signer).HasMaxLength(255);
			builder.Property(e => e.Designation).HasMaxLength(255);
			builder.Property(e => e.MapPreparer).HasMaxLength(255);
			builder.Property(e => e.MapPrepareDate).HasMaxLength(50);
			builder.Property(e => e.SendCopyToNewOwner).HasMaxLength(150);
			builder.Property(e => e.User).HasMaxLength(255);
			builder.Property(e => e.OldCollector).HasMaxLength(255);
			builder.Property(e => e.DefaultSort).HasMaxLength(1);
			builder.Property(e => e.LegalDescription).HasMaxLength(255);
			builder.Property(e => e.MuniTitle).HasMaxLength(255);
			builder.Property(e => e.CollectorTitle).HasMaxLength(50);
		}
	}
}
