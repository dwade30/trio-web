﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class BatchRecoverConfiguration: IEntityTypeConfiguration<BatchRecover>
	{
		public void Configure(EntityTypeBuilder<BatchRecover> builder)
		{
			builder.ToTable("BatchRecover");
			builder.Property(p => p.Id).HasColumnName("ID");
			builder.Property(p => p.TellerId).HasColumnName("TellerID");
			builder.Property(p => p.Reference).HasColumnName("Ref");
			builder.Property(p => p.Principal).HasColumnName("Prin");
			builder.Property(p => p.Interest).HasColumnName("Int");
			builder.Property(p => p.PaymentId).HasColumnName("PaymentKey");
			builder.Property(p => p.ImportId).HasColumnName("ImportID");

			builder.Property(p => p.Service).HasMaxLength(255);
			builder.Property(p => p.Name).HasMaxLength(255);
			builder.Property(p => p.Reference).HasMaxLength(255);
			builder.Property(p => p.PaidBy).HasMaxLength(255);
			builder.Property(p => p.TellerId).HasMaxLength(255);
			
			
		}
	}
}
