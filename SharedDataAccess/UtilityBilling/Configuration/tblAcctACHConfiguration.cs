﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class tblAcctACHConfiguration : IEntityTypeConfiguration<tblAcctACH>
	{
		public void Configure(EntityTypeBuilder<tblAcctACH> builder)
		{
			builder.ToTable("tblAcctACH");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.Service).IsRequired().HasMaxLength(1);
			builder.Property(e => e.ACHBankRT).IsRequired().HasMaxLength(9);
			builder.Property(e => e.ACHAcctNumber).IsRequired().HasMaxLength(50);
			builder.Property(e => e.ACHAcctType).IsRequired().HasMaxLength(2);


		}
	}
}
