﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class RateKeyConfiguration : IEntityTypeConfiguration<RateKey>
	{
		public void Configure(EntityTypeBuilder<RateKey> builder)
		{
			builder.ToTable("RateKeys");
			builder.Property(p => p.Description).HasMaxLength(255);
			builder.Property(p => p.RateType).HasMaxLength(50);
			builder.Property(p => p.ThirtyDayNoticeDateS).HasColumnName("30DNDateS");
			builder.Property(p => p.ThirtyDayNoticeDateW).HasColumnName("30DNDateW");
		}
	}
}
