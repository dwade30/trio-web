﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.TaxCollections
{
    public class BillConfiguration : IEntityTypeConfiguration<Bill>
    {
        public void Configure(EntityTypeBuilder<Bill> builder)
        {
            builder.ToTable("Bill");
			builder.Property(p => p.Service).HasMaxLength(1);
            builder.Property(p => p.BillStatus).HasMaxLength(1);
            builder.Property(p => p.CombinationCode).HasMaxLength(36);
			builder.Property(p => p.CurCode).HasMaxLength(1);
			builder.Property(p => p.PrevCode).HasMaxLength(1);
            builder.Property(p => p.BillingCode).HasMaxLength(5);
			builder.Property(p => p.WaterOverrideAmount).HasColumnType("money");
			builder.Property(p => p.SewerOverrideAmount).HasColumnType("money");
            builder.Property(p => p.WMiscAmount).HasColumnType("money");
            builder.Property(p => p.WAdjustAmount).HasColumnType("money");
            builder.Property(p => p.WDEAdjustAmount).HasColumnType("money");
            builder.Property(p => p.WFlatAmount).HasColumnType("money");
            builder.Property(p => p.WUnitsAmount).HasColumnType("money");
            builder.Property(p => p.WConsumptionAmount).HasColumnType("money");
            builder.Property(p => p.SMiscAmount).HasColumnType("money");
            builder.Property(p => p.SAdjustAmount).HasColumnType("money");
            builder.Property(p => p.SDEAdjustAmount).HasColumnType("money");
            builder.Property(p => p.SFlatAmount).HasColumnType("money");
            builder.Property(p => p.SUnitsAmount).HasColumnType("money");
            builder.Property(p => p.SConsumptionAmount).HasColumnType("money");
			builder.Property(p => p.WTax).HasColumnType("money");
            builder.Property(p => p.STax).HasColumnType("money");
            builder.Property(p => p.TotalSBillAmount).HasColumnType("money");
            builder.Property(p => p.TotalWBillAmount).HasColumnType("money");
			builder.Property(p => p.Location).HasMaxLength(255);
			builder.Property(p => p.BName).HasMaxLength(255);
            builder.Property(p => p.BName2).HasMaxLength(255);
            builder.Property(p => p.BAddress1).HasMaxLength(255);
            builder.Property(p => p.BAddress2).HasMaxLength(255);
            builder.Property(p => p.BAddress3).HasMaxLength(255);
            builder.Property(p => p.BCity).HasMaxLength(255);
            builder.Property(p => p.BState).HasMaxLength(255);
			builder.Property(p => p.BZip).HasMaxLength(255);
            builder.Property(p => p.BZip4).HasMaxLength(255);
			builder.Property(p => p.OName).HasMaxLength(255);
            builder.Property(p => p.OName2).HasMaxLength(255);
            builder.Property(p => p.OAddress1).HasMaxLength(255);
            builder.Property(p => p.OAddress2).HasMaxLength(255);
            builder.Property(p => p.OAddress3).HasMaxLength(255);
            builder.Property(p => p.OCity).HasMaxLength(255);
            builder.Property(p => p.OState).HasMaxLength(255);
            builder.Property(p => p.OZip).HasMaxLength(255);
            builder.Property(p => p.OZip4).HasMaxLength(255);
			builder.Property(p => p.CertifiedMailNumber).HasMaxLength(255);
            builder.Property(p => p.Note).HasMaxLength(255);
            builder.Property(p => p.MapLot).HasMaxLength(255);
            builder.Property(p => p.BookPage).HasMaxLength(50);
            builder.Property(p => p.Telephone).HasMaxLength(255);
            builder.Property(p => p.Email).HasMaxLength(255);
            builder.Property(p => p.BillMessage).HasMaxLength(255);
            builder.Property(p => p.IMPBTrackingNumber).HasMaxLength(30);
            builder.Property(p => p.AccountId).HasColumnName("AccountKey");
            builder.Property(p => p.RateKeyId).HasColumnName("BillingRateKey");
        }
    }
}
