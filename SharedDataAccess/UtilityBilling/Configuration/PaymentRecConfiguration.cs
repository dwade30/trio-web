﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class PaymentRecConfiguration : IEntityTypeConfiguration<PaymentRec>
	{
		public void Configure(EntityTypeBuilder<PaymentRec> builder)
		{
			builder.ToTable("PaymentRec");
			builder.Property(p => p.Teller).HasMaxLength(3);
			builder.Property(p => p.Reference).HasMaxLength(10);
			builder.Property(p => p.Period).HasMaxLength(1);
			builder.Property(p => p.Code).HasMaxLength(1);
			builder.Property(p => p.TransNumber).HasMaxLength(50);
			builder.Property(p => p.PaidBy).HasMaxLength(255);
			builder.Property(p => p.Comments).HasMaxLength(255);
			builder.Property(p => p.CashDrawer).HasMaxLength(1);
			builder.Property(p => p.GeneralLedger).HasMaxLength(1);
			builder.Property(p => p.BudgetaryAccountNumber).HasMaxLength(20);
			builder.Property(p => p.Service).HasMaxLength(1);
			builder.Property(p => p.EPmtID).HasMaxLength(64);
			builder.Property(p => p.Principal).HasColumnType("money");
			builder.Property(p => p.PreLienInterest).HasColumnType("money");
			builder.Property(p => p.CurrentInterest).HasColumnType("money");
			builder.Property(p => p.LienCost).HasColumnType("money");
			builder.Property(p => p.Tax).HasColumnType("money");
			builder.Property(p => p.LienPayment).HasColumnName("Lien");
		}
	}
}
