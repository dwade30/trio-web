﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class WaterControlDischargeNoticeConfiguration : IEntityTypeConfiguration<WaterControlDischargeNotice>
	{
		public void Configure(EntityTypeBuilder<WaterControlDischargeNotice> builder)
		{
			builder.ToTable("WControl_DischargeNotice");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.County).HasMaxLength(255);
			builder.Property(e => e.Treasurer).HasMaxLength(255);
			builder.Property(e => e.SignerDesignation).HasMaxLength(255);
			builder.Property(e => e.SignerName).HasMaxLength(255);
		}
	}
}
