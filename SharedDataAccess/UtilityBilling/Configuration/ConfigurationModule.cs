﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedDataAccess.UtilityBilling.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			RegisterUBContext(builder);
			builder.RegisterType<MasterQueryHandler>()
				.As<IQueryHandler<MasterSearchCriteria, IEnumerable<Master>>>();
			builder.RegisterType<CustomerPartyQueryHandler>()
				.As<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>>();
			builder.RegisterType<CommentQueryHandler>()
				.As<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			builder.RegisterType<CommentRecQueryHandler>()
				.As<IQueryHandler<CommentSearchCriteria, IEnumerable<CommentRec>>>();
			builder.RegisterType<DischargeNeededQueryHandler>()
				.As<IQueryHandler<DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>>>();
			builder.RegisterType<tblAcctACHQueryHandler>()
				.As<IQueryHandler<tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>>>();
			builder.RegisterType<UBBillQueryHandler>()
				.As<IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>>>();
			builder.RegisterType<BatchRecoverQueryHandler>()
				.As<IQueryHandler<BatchRecoverSearchCriteria, IEnumerable<BatchRecover>>>();
        }

		private void RegisterUBContext(ContainerBuilder builder)
		{
			builder.Register(f =>
            {
                var activeModules = f.Resolve<IGlobalActiveModuleSettings>();
                if (activeModules.UtilityBillingIsActive)
                {
                    var tcf = f.Resolve<ITrioContextFactory>();
					var utContext = tcf.GetUtilityBillingContext();
                    if (utContext != null)
                    {
                        if (utContext.Database.CanConnect())
                        {
                            return utContext;
                        }
                    }
                }

                IUtilityBillingContext emptyContext = new UtilityBillingEmptyContext();
                return emptyContext;
            }).As<IUtilityBillingContext>();

            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                var utContext = tcf.GetUtilityBillingContext();
                return utContext;
            }).As<UtilityBillingContext>();
        }
	}
}