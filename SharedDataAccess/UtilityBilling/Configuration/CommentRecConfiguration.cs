﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{

	public class CommentRecConfiguration : IEntityTypeConfiguration<CommentRec>
	{
		public void Configure(EntityTypeBuilder<CommentRec> builder)
		{
			builder.ToTable("CommentRec");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.AccountId).HasColumnName("AccountKey");
			builder.Property(e => e.Text).HasColumnName("Comment").HasMaxLength(255);
		}
	}
}
