﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling.Configuration
{
    public class UtilityBillingSettingConfiguration : IEntityTypeConfiguration<UtilityBillingSetting>
    {
        public void Configure(EntityTypeBuilder<UtilityBillingSetting> builder)
        {
            builder.ToTable("UtilityBilling");
            builder.Property(p => p.Interest).HasMaxLength(4);
            builder.Property(p => p.LienIntMethod).HasMaxLength(1);
            builder.Property(p => p.PayFirst).HasMaxLength(1);
            builder.Property(p => p.Sequence).HasMaxLength(1);
            builder.Property(p => p.BillFormat).HasMaxLength(1);
            builder.Property(p => p.CustomBillFormat).HasMaxLength(8);
            builder.Property(p => p.BillSortOrder).HasMaxLength(1);
            builder.Property(p => p.Service).HasMaxLength(1);
            builder.Property(p => p.RegularIntMethod).HasMaxLength(1);
            builder.Property(p => p.DEMethod).HasMaxLength(1);
            builder.Property(p => p.SewerCostAccount).HasMaxLength(20);
            builder.Property(p => p.SewerIntAccount).HasMaxLength(20);
            builder.Property(p => p.SewerPrincipalAccount).HasMaxLength(20);
            builder.Property(p => p.SewerTaxAccount).HasMaxLength(20);
            builder.Property(p => p.WaterCostAccount).HasMaxLength(20);
            builder.Property(p => p.WaterIntAccount).HasMaxLength(20);
            builder.Property(p => p.WaterPrincipalAccount).HasMaxLength(20);
            builder.Property(p => p.WaterTaxAccount).HasMaxLength(20);
            builder.Property(p => p.Code).HasMaxLength(4);
            builder.Property(p => p.DefaultBillFormat).HasMaxLength(200);
            builder.Property(p => p.ReaderExtractType).HasMaxLength(1);
            builder.Property(p => p.ExtractFileType).HasMaxLength(1);
            builder.Property(p => p.AccountDefaultAccountW).HasMaxLength(50);
            builder.Property(p => p.AccountDefaultAccountS).HasMaxLength(50);
            builder.Property(p => p.AccountDefaultCity).HasMaxLength(255);
            builder.Property(p => p.AccountDefaultState).HasMaxLength(255);
            builder.Property(p => p.AccountDefaultZip).HasMaxLength(255);
            builder.Property(p => p.UTReturnAddress1).HasMaxLength(255);
            builder.Property(p => p.UTReturnAddress2).HasMaxLength(255);
            builder.Property(p => p.UTReturnAddress3).HasMaxLength(255);
            builder.Property(p => p.UTReturnAddress4).HasMaxLength(255);
            builder.Property(p => p.ICMFeeAccount).HasMaxLength(20);
            builder.Property(p => p.RptDesc_PastDue).HasMaxLength(30);
            builder.Property(p => p.RptDesc_Credit).HasMaxLength(30);
            builder.Property(p => p.RptDesc_Regular).HasMaxLength(30);
            builder.Property(p => p.RptDesc_Override).HasMaxLength(30);
            builder.Property(p => p.RptDesc_Interest).HasMaxLength(30);
            builder.Property(p => p.RptDesc_Liens).HasMaxLength(30);
            builder.Property(p => p.RptDesc_Tax).HasMaxLength(30);
            builder.Property(p => p.RptDesc_Misc).HasMaxLength(30);
            builder.Property(p => p.RptDesc_Adj).HasMaxLength(30);
            builder.Property(p => p.InvCldWebSrvcKey).HasMaxLength(50);
            builder.Property(p => p.Version).HasMaxLength(50);
            builder.Property(p => p.FlatInterestAmount).HasColumnType("money");
        }
    }
}