﻿using System;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.UtilityBilling
{
    public class GetUtilityBillingSettingsHandler : CommandHandler<GetUtilityBillingSettings, UtilityBillingSetting>
    {
        private IUtilityBillingContext utilityContext;
        public GetUtilityBillingSettingsHandler(IUtilityBillingContext utilityContext)
        {
            this.utilityContext = utilityContext;
        }

        protected override UtilityBillingSetting Handle(GetUtilityBillingSettings command)
        {
	        try
	        {
		        var utSetting = utilityContext.UtilityBillingSettings.FirstOrDefault();
		        if (utSetting != null)
		        {
			        return utSetting;
		        }
		        return new UtilityBillingSetting();
			}
	        catch (Exception e)
	        {
		        Console.WriteLine(e);
		        throw;
	        }
        }
    }
}