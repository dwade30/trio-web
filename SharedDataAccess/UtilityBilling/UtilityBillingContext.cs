﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication.SystemSettings.Models;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedDataAccess.SystemSettings;
using SharedDataAccess.SystemSettings.Configuration;
using SharedDataAccess.TaxCollections;
using SharedDataAccess.UtilityBilling.Configuration;

namespace SharedDataAccess.UtilityBilling
{

	public class UtilityBillingContext : DataContext<UtilityBillingContext>, IUtilityBillingContext
	{
		public DbSet<PaymentRec> PaymentRecs { get; set; }
		public DbSet<Lien> Liens { get; set; }
		public DbSet<Bill> Bills { get; set; }
		public DbSet<RateKey> RateKeys { get; set; }
		public DbSet<Master> Masters { get; set; }
		public DbSet<MeterTable> MeterTables { get; set; }
		public DbSet<CustomerParty> CustomerParties { get; set; }
		public DbSet<Comment> Comments { get; set; }
		public DbSet<CommentRec> CommentRecs { get; set; }
		public DbSet<tblAcctACH> tblAcctACHs { get; set; }
		public DbSet<DischargeNeeded> DischargeNeededs { get; set; }
		public DbSet<BatchRecover> BatchRecovers { get; set; }
		public DbSet<UtilityBillingSetting> UtilityBillingSettings { get; set; }
		public DbSet<WaterControlDischargeNotice> WaterControlDischargeNotices { get; set; }
		public DbSet<SewerControlDischargeNotice> SewerControlDischargeNotices { get; set; }
		public DbSet<WaterControlLienProcess> WaterControlLienProcesses { get; set; }
		public DbSet<SewerControlLienProcess> SewerControlLienProcesses { get; set; }


		IQueryable<PaymentRec> IUtilityBillingContext.PaymentRecs => PaymentRecs;
        IQueryable<Lien> IUtilityBillingContext.Liens => Liens;
        IQueryable<Bill> IUtilityBillingContext.Bills => Bills;
        IQueryable<RateKey> IUtilityBillingContext.RateKeys => RateKeys;
        IQueryable<Master> IUtilityBillingContext.Masters => Masters;
        IQueryable<MeterTable> IUtilityBillingContext.MeterTables => MeterTables;
        IQueryable<CustomerParty> IUtilityBillingContext.CustomerParties => CustomerParties;
        IQueryable<Comment> IUtilityBillingContext.Comments => Comments;
        IQueryable<CommentRec> IUtilityBillingContext.CommentRecs => CommentRecs;
        IQueryable<tblAcctACH> IUtilityBillingContext.tblAcctACHs => tblAcctACHs;
        IQueryable<DischargeNeeded> IUtilityBillingContext.DischargeNeededs => DischargeNeededs;
        IQueryable<BatchRecover> IUtilityBillingContext.BatchRecovers => BatchRecovers;
		IQueryable<UtilityBillingSetting> IUtilityBillingContext.UtilityBillingSettings => UtilityBillingSettings;
		IQueryable<WaterControlDischargeNotice> IUtilityBillingContext.WaterControlDischargeNotices => WaterControlDischargeNotices;
		IQueryable<SewerControlDischargeNotice> IUtilityBillingContext.SewerControlDischargeNotices => SewerControlDischargeNotices;
		IQueryable<WaterControlLienProcess> IUtilityBillingContext.WaterControlLienProcesses => WaterControlLienProcesses;
		IQueryable<SewerControlLienProcess> IUtilityBillingContext.SewerControlLienProcesses => SewerControlLienProcesses;


		public UtilityBillingContext(DbContextOptions<UtilityBillingContext> options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.ApplyConfiguration(new PaymentRecConfiguration());
			modelBuilder.ApplyConfiguration(new BillConfiguration());
			modelBuilder.ApplyConfiguration(new LienConfiguration());
			modelBuilder.ApplyConfiguration(new RateKeyConfiguration());
            modelBuilder.ApplyConfiguration(new UtilityBillingSettingConfiguration());
			modelBuilder.ApplyConfiguration(new MasterConfiguration());
            modelBuilder.ApplyConfiguration(new MeterTableConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerPartyConfiguration());
            modelBuilder.ApplyConfiguration(new CommentConfiguration());
            modelBuilder.ApplyConfiguration(new CommentRecConfiguration());
            modelBuilder.ApplyConfiguration(new tblAcctACHConfiguration());
            modelBuilder.ApplyConfiguration(new DischargeNeededConfiguration());
            modelBuilder.ApplyConfiguration(new BatchRecoverConfiguration());
			modelBuilder.ApplyConfiguration(new WaterControlDischargeNoticeConfiguration());
            modelBuilder.ApplyConfiguration(new WaterControlLienProcessConfiguration());
            modelBuilder.ApplyConfiguration(new SewerControlDischargeNoticeConfiguration());
            modelBuilder.ApplyConfiguration(new SewerControlLienProcessConfiguration());

			modelBuilder.Entity<RateKey>()
				.HasMany(c => c.Bills)
				.WithOne(e => e.RateKey)
				.HasForeignKey(e => e.RateKeyId);

			modelBuilder.Entity<RateKey>()
				.HasMany(c => c.Liens)
				.WithOne(e => e.RateKey)
				.HasForeignKey(e => e.RateKeyId);

			modelBuilder.Entity<Bill>()
				.HasMany(c => c.Payments)
				.WithOne(e => e.Bill)
				.HasForeignKey(e => e.BillId);

			modelBuilder.Entity<Lien>()
				.HasMany(c => c.Payments)
				.WithOne(e => e.Lien)
				.HasForeignKey(e => e.LienId);

			modelBuilder.Entity<Master>()
				.HasMany(c => c.MeterTables)
				.WithOne(e => e.Master)
				.HasForeignKey(e => e.AccountKey);

			modelBuilder.Entity<Master>()
				.HasMany(c => c.CommentRecs)
				.WithOne(e => e.Master)
				.HasForeignKey(e => e.AccountId);

			modelBuilder.Entity<CustomerParty>()
				.HasMany(c => c.Bills)
				.WithOne(e => e.CustomerParty)
				.HasForeignKey(e => e.AccountId);

			modelBuilder.Entity<CustomerParty>()
				.HasMany(c => c.MeterTables)
				.WithOne(e => e.CustomerParty)
				.HasForeignKey(e => e.AccountKey);
		}
	}
}
