﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Diagnostics;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Messaging;

namespace SharedDataAccess.AccountsReceivable
{
	public class UpdateCommentHandler : CommandHandler<UpdateComment, bool>
	{
		private AccountsReceivableContext accountsReceivableContext;

		public UpdateCommentHandler(ITrioContextFactory trioContextFactory)
		{
			accountsReceivableContext = trioContextFactory.GetAccountsReceivableContext();
		}

		protected override bool Handle(UpdateComment command)
		{
			try
			{
				var comment = accountsReceivableContext.Comments.FirstOrDefault(x =>
					x.Account == command.CustomerId);

				if (comment != null)
				{
					if (command.Comment != null)
					{
						if (command.Comment.Trim() == "")
						{
							accountsReceivableContext.Comments.Remove(comment);
						}
						else
						{
							comment.Text = command.Comment.Trim();
						}
					}

					if (command.Priority >= 0)
					{
						comment.Priority = command.Priority;
					}
				}
				else
				{
					if (command.Comment.Trim() != "")
					{
						accountsReceivableContext.Comments.Add(new Comment
						{
							Account = command.CustomerId,
							Type = "AR",
							Priority = command.Priority >= 0 ? command.Priority : 0,
							Text = command.Comment.Trim()
						});
					}
				}
				accountsReceivableContext.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
