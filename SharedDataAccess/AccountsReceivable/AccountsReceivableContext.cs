﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.CentralData.Models;
using SharedDataAccess.AccountsReceivable.Configuration;

namespace SharedDataAccess.AccountsReceivable
{
    public class AccountsReceivableContext : DataContext<AccountsReceivableContext>, IAccountsReceivableContext
    {
        public DbSet<AuditChanges> AuditChanges { get; set; }
        public DbSet<AuditChangesArchive> AuditChangesArchive { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<CloseOut> CloseOut { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<CustomBillCodes> CustomBillCodes { get; set; }
        public DbSet<CustomBillFields> CustomBillFields { get; set; }
        public DbSet<CustomBills> CustomBills { get; set; }
        public DbSet<CustomerBills> CustomerBills { get; set; }
        public DbSet<CustomerMaster> CustomerMasters { get; set; }
        public DbSet<Customize> Customize { get; set; }
        public DbSet<Cya> Cya { get; set; }
        public DbSet<Dbversion> Dbversion { get; set; }
        public DbSet<DefaultBillType> DefaultBillTypes { get; set; }
        public DbSet<Payees> Payees { get; set; }
        public DbSet<PaymentRec> PaymentRec { get; set; }
        public DbSet<RateKey> RateKeys { get; set; }
        public DbSet<SavedStatusReports> SavedStatusReports { get; set; }
        public DbSet<CustomerParty> CustomerParties { get; set; }


        IQueryable<CustomerParty> IAccountsReceivableContext.CustomerParties { get => CustomerParties; }
		IQueryable<AuditChanges> IAccountsReceivableContext.AuditChanges { get => AuditChanges; }

		IQueryable<AuditChangesArchive> IAccountsReceivableContext.AuditChangesArchive { get => AuditChangesArchive; }

		IQueryable<Bill> IAccountsReceivableContext.Bills { get => Bills; }

		IQueryable<CloseOut> IAccountsReceivableContext.CloseOut { get => CloseOut; }

		IQueryable<Comment> IAccountsReceivableContext.Comments { get => Comments; }

		IQueryable<CustomBillCodes> IAccountsReceivableContext.CustomBillCodes { get => CustomBillCodes; }

		IQueryable<CustomBillFields> IAccountsReceivableContext.CustomBillFields { get => CustomBillFields; }

		IQueryable<CustomBills> IAccountsReceivableContext.CustomBills { get => CustomBills; }

		IQueryable<CustomerBills> IAccountsReceivableContext.CustomerBills { get => CustomerBills; }

		IQueryable<CustomerMaster> IAccountsReceivableContext.CustomerMasters { get => CustomerMasters; }

		IQueryable<Customize> IAccountsReceivableContext.Customize { get => Customize; }

		IQueryable<Cya> IAccountsReceivableContext.Cya { get => Cya; }

		IQueryable<Dbversion> IAccountsReceivableContext.Dbversion { get => Dbversion; }

		IQueryable<DefaultBillType> IAccountsReceivableContext.DefaultBillTypes { get => DefaultBillTypes; }

		IQueryable<Payees> IAccountsReceivableContext.Payees { get => Payees; }

		IQueryable<PaymentRec> IAccountsReceivableContext.PaymentRec { get => PaymentRec; }

		IQueryable<RateKey> IAccountsReceivableContext.RateKeys { get => RateKeys; }

		IQueryable<SavedStatusReports> IAccountsReceivableContext.SavedStatusReports { get => SavedStatusReports; }

		public AccountsReceivableContext(DbContextOptions<AccountsReceivableContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CustomerPartyConfiguration());

			modelBuilder.Entity<AuditChanges>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChangeDescription).HasMaxLength(255);

                entity.Property(e => e.DateUpdated).HasColumnType("datetime");

                entity.Property(e => e.Location).HasMaxLength(255);

                entity.Property(e => e.TimeUpdated).HasColumnType("datetime");

                entity.Property(e => e.UserField1).HasMaxLength(255);

                entity.Property(e => e.UserField2).HasMaxLength(255);

                entity.Property(e => e.UserField3).HasMaxLength(255);

                entity.Property(e => e.UserField4).HasMaxLength(255);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<AuditChangesArchive>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChangeDescription).HasMaxLength(255);

                entity.Property(e => e.DateUpdated).HasColumnType("datetime");

                entity.Property(e => e.Location).HasMaxLength(255);

                entity.Property(e => e.TimeUpdated).HasColumnType("datetime");

                entity.Property(e => e.UserField1).HasMaxLength(255);

                entity.Property(e => e.UserField2).HasMaxLength(255);

                entity.Property(e => e.UserField3).HasMaxLength(255);

                entity.Property(e => e.UserField4).HasMaxLength(255);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Bill>(entity =>
            {
	            entity.ToTable("Bill");
				entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount1).HasColumnType("money");

                entity.Property(e => e.Amount2).HasColumnType("money");

                entity.Property(e => e.Amount3).HasColumnType("money");

                entity.Property(e => e.Amount4).HasColumnType("money");

                entity.Property(e => e.Amount5).HasColumnType("money");

                entity.Property(e => e.Amount6).HasColumnType("money");

                entity.Property(e => e.Baddress1)
                    .HasColumnName("BAddress1")
                    .HasMaxLength(255);

                entity.Property(e => e.Baddress2)
                    .HasColumnName("BAddress2")
                    .HasMaxLength(255);

                entity.Property(e => e.Baddress3)
                    .HasColumnName("BAddress3")
                    .HasMaxLength(255);

                entity.Property(e => e.Bcity)
                    .HasColumnName("BCity")
                    .HasMaxLength(255);

                entity.Property(e => e.BillDate).HasColumnType("datetime");

                entity.Property(e => e.BillStatus).HasMaxLength(1);

                entity.Property(e => e.Bname)
                    .HasColumnName("BName")
                    .HasMaxLength(255);

                entity.Property(e => e.Bname2)
                    .HasColumnName("BName2")
                    .HasMaxLength(255);

                entity.Property(e => e.Bstate)
                    .HasColumnName("BState")
                    .HasMaxLength(255);

                entity.Property(e => e.Bzip)
                    .HasColumnName("BZip")
                    .HasMaxLength(255);

                entity.Property(e => e.Bzip4)
                    .HasColumnName("BZip4")
                    .HasMaxLength(255);

                entity.Property(e => e.Control1).HasMaxLength(50);

                entity.Property(e => e.Control2).HasMaxLength(50);

                entity.Property(e => e.Control3).HasMaxLength(50);

                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.Description1).HasMaxLength(255);

                entity.Property(e => e.Description2).HasMaxLength(255);

                entity.Property(e => e.Description3).HasMaxLength(255);

                entity.Property(e => e.Description4).HasMaxLength(255);

                entity.Property(e => e.Description5).HasMaxLength(255);

                entity.Property(e => e.Description6).HasMaxLength(255);

                entity.Property(e => e.IntAdded).HasColumnType("money");

                entity.Property(e => e.IntPaid).HasColumnType("money");

                entity.Property(e => e.IntPaidDate).HasColumnType("datetime");

                entity.Property(e => e.InvoiceNumber).HasMaxLength(50);

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.PrePaymentAmount).HasColumnType("money");

                entity.Property(e => e.PrinOwed).HasColumnType("money");

                entity.Property(e => e.PrinPaid).HasColumnType("money");

                entity.Property(e => e.ProrateStartDate).HasColumnType("datetime");

                entity.Property(e => e.Reference).HasMaxLength(50);

                entity.Property(e => e.TaxOwed).HasColumnType("money");

                entity.Property(e => e.TaxPaid).HasColumnType("money");
            });

            modelBuilder.Entity<CloseOut>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CloseOutDate).HasColumnType("datetime");

                entity.Property(e => e.TellerId)
                    .HasColumnName("TellerID")
                    .HasMaxLength(255);

                entity.Property(e => e.Type).HasMaxLength(255);
            });

            modelBuilder.Entity<Comment>(entity =>
            {
	            entity.ToTable("Comments");
				entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Text).HasColumnName("Comment").HasMaxLength(255);

                entity.Property(e => e.Type).HasMaxLength(255);
            });

            modelBuilder.Entity<CustomBillCodes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BoolUserDefined).HasColumnName("boolUserDefined");

                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.Dbname)
                    .HasColumnName("DBName")
                    .HasMaxLength(255);

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.ExtraParameters).HasMaxLength(255);

                entity.Property(e => e.FieldId).HasColumnName("FieldID");

                entity.Property(e => e.FieldName).HasMaxLength(255);

                entity.Property(e => e.FormatString).HasMaxLength(50);

                entity.Property(e => e.HelpDescription).HasMaxLength(255);

                entity.Property(e => e.ParametersToolTip).HasMaxLength(255);

                entity.Property(e => e.TableName).HasMaxLength(255);

                entity.Property(e => e.ToolTipText).HasMaxLength(255);
            });

            modelBuilder.Entity<CustomBillFields>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.ExtraParameters).HasMaxLength(255);

                entity.Property(e => e.FieldId).HasColumnName("FieldID");

                entity.Property(e => e.Font).HasMaxLength(255);

                entity.Property(e => e.FormatId).HasColumnName("FormatID");
            });

            modelBuilder.Entity<CustomBills>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BackgroundImage).HasMaxLength(255);

                entity.Property(e => e.BoolWide).HasColumnName("boolWide");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.FormatName).HasMaxLength(255);
            });

            modelBuilder.Entity<CustomerBills>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount1).HasColumnType("money");

                entity.Property(e => e.Amount2).HasColumnType("money");

                entity.Property(e => e.Amount3).HasColumnType("money");

                entity.Property(e => e.Amount4).HasColumnType("money");

                entity.Property(e => e.Amount5).HasColumnType("money");

                entity.Property(e => e.Amount6).HasColumnType("money");

                entity.Property(e => e.Control1).HasMaxLength(50);

                entity.Property(e => e.Control2).HasMaxLength(50);

                entity.Property(e => e.Control3).HasMaxLength(50);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.Reference).HasMaxLength(50);

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<CustomerMaster>(entity =>
            {
	            entity.ToTable("CustomerMaster");
				entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DateUpdated).HasColumnType("datetime");

                entity.Property(e => e.PartyId).HasColumnName("PartyID");

                entity.Property(e => e.TimeUpdated).HasColumnType("datetime");
            });

            modelBuilder.Entity<Customize>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DefaultBillType).HasMaxLength(50);

                entity.Property(e => e.DefaultCity).HasMaxLength(255);

                entity.Property(e => e.DefaultFlatRate).HasColumnType("money");

                entity.Property(e => e.DefaultInterestMethod).HasMaxLength(50);

                entity.Property(e => e.DefaultPayeeId).HasColumnName("DefaultPayeeID");

                entity.Property(e => e.DefaultState).HasMaxLength(255);

                entity.Property(e => e.DefaultZip).HasMaxLength(255);

                entity.Property(e => e.PrePayReceivableAccount).HasMaxLength(255);

                entity.Property(e => e.RecordSalesTax).HasMaxLength(50);

                entity.Property(e => e.SalesTaxPayableAccount).HasMaxLength(50);

                entity.Property(e => e.SalesTaxReceivableAccount).HasMaxLength(50);

                entity.Property(e => e.ShowLastAraccountInCr).HasColumnName("ShowLastARAccountInCR");

                entity.Property(e => e.Version).HasMaxLength(50);
            });

            modelBuilder.Entity<Cya>(entity =>
            {
                entity.ToTable("CYA");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BoolUseSecurity).HasColumnName("boolUseSecurity");

                entity.Property(e => e.Cyadate)
                    .HasColumnName("CYADate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Cyatime)
                    .HasColumnName("CYATime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description1).HasMaxLength(255);

                entity.Property(e => e.Description2).HasMaxLength(255);

                entity.Property(e => e.Description3).HasMaxLength(255);

                entity.Property(e => e.Description4).HasMaxLength(255);

                entity.Property(e => e.Description5).HasMaxLength(255);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Dbversion>(entity =>
            {
                entity.ToTable("DBVersion");

                entity.Property(e => e.Id).HasColumnName("ID");
            });

            modelBuilder.Entity<DefaultBillType>(entity =>
            {
	            entity.ToTable("DefaultBillTypes");
				entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Account1).HasMaxLength(255);

                entity.Property(e => e.Account2).HasMaxLength(255);

                entity.Property(e => e.Account3).HasMaxLength(255);

                entity.Property(e => e.Account4).HasMaxLength(255);

                entity.Property(e => e.Account5).HasMaxLength(255);

                entity.Property(e => e.Account6).HasMaxLength(255);

                entity.Property(e => e.Control1).HasMaxLength(255);

                entity.Property(e => e.Control2).HasMaxLength(255);

                entity.Property(e => e.Control3).HasMaxLength(255);

                entity.Property(e => e.DefaultAccount).HasMaxLength(50);

                entity.Property(e => e.FlatAmount).HasColumnType("money");

                entity.Property(e => e.FrequencyCode).HasMaxLength(50);

                entity.Property(e => e.InterestMethod).HasMaxLength(50);

                entity.Property(e => e.OverrideIntAccount).HasMaxLength(255);

                entity.Property(e => e.OverrideTaxAccount).HasMaxLength(255);

                entity.Property(e => e.PayeeId).HasColumnName("PayeeID");

                entity.Property(e => e.ReceivableAccount).HasMaxLength(50);

                entity.Property(e => e.Reference).HasMaxLength(255);

                entity.Property(e => e.Title1).HasMaxLength(255);

                entity.Property(e => e.Title1Abbrev).HasMaxLength(255);

                entity.Property(e => e.Title2).HasMaxLength(255);

                entity.Property(e => e.Title2Abbrev).HasMaxLength(255);

                entity.Property(e => e.Title3).HasMaxLength(255);

                entity.Property(e => e.Title3Abbrev).HasMaxLength(255);

                entity.Property(e => e.Title4).HasMaxLength(255);

                entity.Property(e => e.Title4Abbrev).HasMaxLength(255);

                entity.Property(e => e.Title5).HasMaxLength(255);

                entity.Property(e => e.Title5Abbrev).HasMaxLength(255);

                entity.Property(e => e.Title6).HasMaxLength(255);

                entity.Property(e => e.Title6Abbrev).HasMaxLength(255);

                entity.Property(e => e.TypeTitle).HasMaxLength(255);
            });

            modelBuilder.Entity<Payees>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.LogoPath).HasMaxLength(255);

                entity.Property(e => e.PayeeId).HasColumnName("PayeeID");
            });

            modelBuilder.Entity<PaymentRec>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActualSystemDate).HasColumnType("datetime");

                entity.Property(e => e.BudgetaryAccountNumber).HasMaxLength(20);

                entity.Property(e => e.CashDrawer).HasMaxLength(1);

                entity.Property(e => e.Chgintdate)
                    .HasColumnName("CHGINTDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Chgintnumber).HasColumnName("CHGINTNumber");

                entity.Property(e => e.Code).HasMaxLength(1);

                entity.Property(e => e.Comments).HasMaxLength(255);

                entity.Property(e => e.EffectiveInterestDate).HasColumnType("datetime");

                entity.Property(e => e.GeneralLedger).HasMaxLength(1);

                entity.Property(e => e.Interest).HasColumnType("money");

                entity.Property(e => e.InvoiceNumber).HasMaxLength(50);

                entity.Property(e => e.PaidBy).HasMaxLength(255);

                entity.Property(e => e.Principal).HasColumnType("money");

                entity.Property(e => e.RecordedTransactionDate).HasColumnType("datetime");

                entity.Property(e => e.Reference).HasMaxLength(10);

                entity.Property(e => e.Tax).HasColumnType("money");

                entity.Property(e => e.Teller).HasMaxLength(3);

                entity.Property(e => e.TransNumber).HasMaxLength(50);
            });

            modelBuilder.Entity<RateKey>(entity =>
            {
	            entity.ToTable("RateKeys");
				entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BillDate).HasColumnType("datetime");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.End).HasColumnType("datetime");

                entity.Property(e => e.FlatRate).HasColumnType("money");

                entity.Property(e => e.IntStart).HasColumnType("datetime");

                entity.Property(e => e.InterestMethod).HasMaxLength(50);

                entity.Property(e => e.Start).HasColumnType("datetime");
            });

            modelBuilder.Entity<SavedStatusReports>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FieldConstraint0).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint1).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint2).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint3).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint4).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint5).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint6).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint7).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint8).HasMaxLength(255);

                entity.Property(e => e.FieldConstraint9).HasMaxLength(255);

                entity.Property(e => e.LastUpdated).HasColumnType("datetime");

                entity.Property(e => e.ReportName).HasMaxLength(255);

                entity.Property(e => e.SortSelection).HasMaxLength(255);

                entity.Property(e => e.Sql).HasColumnName("SQL");

                entity.Property(e => e.Type).HasMaxLength(255);

                entity.Property(e => e.WhereSelection).HasMaxLength(255);
            });

            modelBuilder.Entity<CustomerParty>()
	            .HasMany(c => c.Bills)
	            .WithOne(e => e.CustomerParty)
	            .HasForeignKey(e => e.AccountKey);

            modelBuilder.Entity<CustomerMaster>()
	            .HasMany(c => c.Bills)
	            .WithOne(e => e.Customer)
	            .HasForeignKey(e => e.AccountKey);

            modelBuilder.Entity<Bill>()
	            .HasMany(c => c.Payments)
	            .WithOne(e => e.Bill)
	            .HasForeignKey(e => e.BillKey);
		}
    }
}
