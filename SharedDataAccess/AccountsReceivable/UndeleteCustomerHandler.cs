﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Messaging;
using SharedDataAccess.CentralData;

namespace SharedDataAccess.AccountsReceivable
{
	public class UndeleteCustomerHandler : CommandHandler<UndeleteCustomer, bool>
	{
		private AccountsReceivableContext accountsReceivableContext;

		public UndeleteCustomerHandler(ITrioContextFactory trioContextFactory)
		{
			accountsReceivableContext = trioContextFactory.GetAccountsReceivableContext();
		}

		protected override bool Handle(UndeleteCustomer command)
		{
			try
			{
				var customer = accountsReceivableContext.CustomerMasters.FirstOrDefault(x =>
					x.CustomerId == command.Account);

				if (customer != null)
				{
					customer.Deleted = false;
				}

				accountsReceivableContext.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
