﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;

namespace SharedDataAccess.AccountsReceivable
{
	public class CustomerQueryHandler : IQueryHandler<CustomerSearchCriteria, IEnumerable<CustomerMaster>>
	{
		private IAccountsReceivableContext accountsReceivableContext;

		public CustomerQueryHandler(ITrioContextFactory contextFactory)
		{
			this.accountsReceivableContext = contextFactory.GetAccountsReceivableContext();
		}

		public IEnumerable<CustomerMaster> ExecuteQuery(CustomerSearchCriteria query)
		{
			IQueryable<CustomerMaster> customerQuery;

			customerQuery = accountsReceivableContext.CustomerMasters;

			if (query.Id > 0)
			{
				customerQuery = customerQuery.Where(x => x.Id == query.Id);
			}

			if (query.AccountNumber > 0)
			{
				customerQuery = customerQuery.Where(x => x.CustomerId == query.AccountNumber);
			}

			if (query.deletedOption == DeletedSelection.NotDeleted)
			{
				customerQuery = customerQuery.Where(x => (x.Deleted ?? false) == false);
			}
			else if (query.deletedOption == DeletedSelection.OnlyDeleted)
			{
				customerQuery = customerQuery.Where(x => (x.Deleted ?? false) == true);
			}

			return customerQuery.ToList();
		}
	}
}
