﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.AccountsReceivable.Models;

namespace SharedDataAccess.AccountsReceivable.Configuration
{
	public class CustomerPartyConfiguration : IEntityTypeConfiguration<CustomerParty>
	{
		public void Configure(EntityTypeBuilder<CustomerParty> builder)
		{
			builder.ToTable("CustomerPartyView");
			builder.Property(e => e.Id).HasColumnName("ID");
			builder.Property(e => e.CustomerId).HasColumnName("CustomerID");
			builder.Property(e => e.DateUpdated).HasColumnType("datetime");
			builder.Property(e => e.PartyId).HasColumnName("PartyID");
			builder.Property(e => e.TimeUpdated).HasColumnType("datetime");
			builder.Property(p => p.FirstName).HasMaxLength(255);
			builder.Property(p => p.MiddleName).HasMaxLength(100);
			builder.Property(p => p.LastName).HasMaxLength(100);
			builder.Property(p => p.Designation).HasMaxLength(50);
			builder.Property(p => p.Email).HasMaxLength(200);
			builder.Property(p => p.WebAddress).HasMaxLength(200);
			builder.Property(p => p.Address1).HasMaxLength(255);
			builder.Property(p => p.Address2).HasMaxLength(255);
			builder.Property(p => p.Address3).HasMaxLength(255);
			builder.Property(p => p.City).HasMaxLength(255);
			builder.Property(p => p.State).HasMaxLength(255);
			builder.Property(p => p.Zip).HasMaxLength(255);
			builder.Property(p => p.Country).HasMaxLength(255);
			builder.Property(p => p.OverrideName).HasMaxLength(255);
			builder.Property(p => p.FullName).HasMaxLength(508);
			builder.Property(p => p.FullNameLF).HasMaxLength(509);
		}
	}
}
