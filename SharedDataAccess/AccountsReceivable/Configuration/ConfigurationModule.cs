﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedDataAccess.CentralData;

namespace SharedDataAccess.AccountsReceivable.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
        {
            RegisterARContext(builder);
			builder.RegisterType<CustomerQueryHandler>()
				.As<IQueryHandler<CustomerSearchCriteria, IEnumerable<CustomerMaster>>>();

			builder.RegisterType<CustomerPartyQueryHandler>()
				.As<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();

			builder.RegisterType<ARBillQueryHandler>()
				.As<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();

			builder.RegisterType<CommentQueryHandler>()
				.As<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();

			builder.RegisterType<DefaultBillTypeQueryHandler>()
				.As<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
		}

        private void RegisterARContext(ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var activeModules = f.Resolve<IGlobalActiveModuleSettings>();
                if (activeModules.AccountsReceivableIsActive)
                {
                    var tcf = f.Resolve<ITrioContextFactory>();
                    return tcf.GetAccountsReceivableContext();
                }
                IAccountsReceivableContext emptyContext = new AccountsReceivableEmptyContext();
                return emptyContext;
            }).As<IAccountsReceivableContext>();
        }
    }
}
