﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;

namespace SharedDataAccess.AccountsReceivable
{
	public class CommentQueryHandler : IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>
	{
		private IAccountsReceivableContext accountsReceivableContext;

		public CommentQueryHandler(ITrioContextFactory contextFactory)
		{
			this.accountsReceivableContext = contextFactory.GetAccountsReceivableContext();
		}

		public IEnumerable<Comment> ExecuteQuery(CommentSearchCriteria query)
		{
			IQueryable<Comment> commentQuery;

			commentQuery = accountsReceivableContext.Comments;

			if (query.CustomerId > 0)
			{
				commentQuery = commentQuery.Where(x => x.Account == query.CustomerId);
			}

			return commentQuery.ToList();
		}
	}

}
