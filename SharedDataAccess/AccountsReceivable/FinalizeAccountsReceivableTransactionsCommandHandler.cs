﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedDataAccess.AccountsReceivable
{
	public class FinalizeAccountsReceivableTransactionsCommandHandler : CommandHandler<FinalizeTransactions<AccountsReceivableTransactionBase>, bool>
	{
		private AccountsReceivableContext accountsReceivableContext;
		private IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>> customerPartyQueryHandler;

		public FinalizeAccountsReceivableTransactionsCommandHandler(ITrioContextFactory trioContextFactory, IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>> customerPartyQueryHandler)
		{
			accountsReceivableContext = trioContextFactory.GetAccountsReceivableContext();
			this.customerPartyQueryHandler = customerPartyQueryHandler;
		}

		protected override bool Handle(FinalizeTransactions<AccountsReceivableTransactionBase> command)
		{
			try
			{
				foreach (var transaction in command.Transactions)
				{
					var customer = customerPartyQueryHandler.ExecuteQuery(new CustomerPartySearchCriteria
					{
						AccountNumber = transaction.AccountNumber
					}).FirstOrDefault();

					foreach (var bill in transaction.TransactionDetails.Bills)
					{
						var savedBill = new Bill();

						if (bill.Pending)
						{
							savedBill = new Bill
							{
								AccountKey = customer.Id,
								ActualAccountNumber = customer.CustomerId,
								CreationDate = DateTime.Today,
								LastUpdatedDate = DateTime.Today,
								BillStatus = "D",
								BillNumber = 0,
								Bname = customer.FullName,
								InvoiceNumber = "0",
								Baddress1 = customer.Address1,
								Baddress2 = customer.Address2,
								Baddress3 = customer.Address3,
								Bcity = customer.City,
								Bstate = customer.State,
								Bzip = customer.Zip == null ? "" : customer.Zip.Length > 5 ? customer.Zip.Left(5) : customer.Zip,
								Bzip4 = customer.Zip == null ? "" : customer.Zip.Contains("-") && customer.Zip.Length > 6 ? customer.Zip.Right(customer.Zip.Length - 6) : ""
							};

							accountsReceivableContext.Bills.Add(savedBill);
						}
						else
						{
							savedBill = accountsReceivableContext.Bills.FirstOrDefault(x => x.Id == bill.Id);
						}

						foreach (var payment in bill.Payments.Where(x => x.Pending))
						{
							PaymentRec newChargedInterestRecord = null;

							if (payment.ChargedInterestRecord != null)
							{
								newChargedInterestRecord = new PaymentRec
								{
									AccountKey = savedBill.AccountKey,
									ActualAccountNumber = savedBill.ActualAccountNumber,
									BillKey = savedBill.Id,
									BillNumber = savedBill.BillNumber,
									InvoiceNumber = savedBill.InvoiceNumber,
									BillType = savedBill.BillType,
									ReceiptNumber = command.ReceiptId,
									Chgintnumber = 0,
									Chgintdate = payment.ChargedInterestRecord.ChargedInterestDate,
									ActualSystemDate = payment.ChargedInterestRecord.ActualSystemDate,
									EffectiveInterestDate = payment.ChargedInterestRecord.EffectiveInterestDate,
									RecordedTransactionDate = payment.ChargedInterestRecord.RecordedTransactionDate,
									Teller = command.TellerId,
									TransNumber = "",
									TransactionIdentifier = transaction.Id,
									CorrelationIdentifier = transaction.CorrelationIdentifier,
									DailyCloseOut = 0,
									Reference = payment.ChargedInterestRecord.Reference,
									Code = payment.ChargedInterestRecord.Code,
									Principal = payment.ChargedInterestRecord.Principal,
									Tax = payment.ChargedInterestRecord.Tax,
									Interest = payment.ChargedInterestRecord.Interest,
									PaidBy = payment.ChargedInterestRecord.PaidBy ?? "",
									Comments = payment.ChargedInterestRecord.Comments ?? "",
									CashDrawer = payment.ChargedInterestRecord.CashDrawer,
									GeneralLedger = payment.ChargedInterestRecord.GeneralLedger,
									BudgetaryAccountNumber = payment.ChargedInterestRecord.BudgetaryAccountNumber
								};

								if (payment.Reference != "REVERS")
								{
									savedBill.IntAdded = savedBill.IntAdded + payment.ChargedInterestRecord.Interest;
								}
								else
								{
									savedBill.IntPaid = savedBill.IntPaid + payment.ChargedInterestRecord.Interest;
								}
							}

							var newPayment = new PaymentRec
							{
								AccountKey = savedBill.AccountKey,
								ActualAccountNumber = savedBill.ActualAccountNumber,
								BillKey = savedBill.Id,
								BillNumber = savedBill.BillNumber,
								InvoiceNumber = savedBill.InvoiceNumber,
								BillType = savedBill.BillType,
								ReceiptNumber = command.ReceiptId,
								Chgintnumber = 0,
								TransactionIdentifier = transaction.Id,
								CorrelationIdentifier = transaction.CorrelationIdentifier,
								ActualSystemDate = payment.ActualSystemDate,
								EffectiveInterestDate = payment.EffectiveInterestDate,
								RecordedTransactionDate = payment.RecordedTransactionDate,
								Teller = command.TellerId,
								Reference = payment.Reference,
								TransNumber = "",
								DailyCloseOut = 0,
								Code = payment.Code,
								Principal = payment.Principal,
								Tax = payment.Tax,
								Interest = payment.Interest,
								PaidBy = payment.PaidBy,
								Comments = payment.Comments ?? "",
								CashDrawer = payment.CashDrawer,
								GeneralLedger = payment.GeneralLedger,
								BudgetaryAccountNumber = payment.BudgetaryAccountNumber
							};


							savedBill.Payments.Add(newPayment);
							if (newChargedInterestRecord != null)
							{
								savedBill.Payments.Add(newChargedInterestRecord);
							}

                            if (payment.Reference != "CHGINT" )
                            {
                                savedBill.IntPaid = savedBill.IntPaid + bill.PendingInterestAmount;
                            }
                            else
                            {
                                savedBill.IntAdded = savedBill.IntAdded + bill.PendingInterestAmount;
							}
                            savedBill.PrinPaid = savedBill.PrinPaid + bill.PendingPrincipalAmount;
							savedBill.TaxPaid = savedBill.TaxPaid + bill.PendingTaxAmount;
							savedBill.IntPaidDate = bill.LastInterestDate;
							savedBill.PreviousInterestPaidDate = bill.PreviousInterestDate;

							if (savedBill.BillNumber == 0)
							{
								savedBill.PrePaymentAmount = savedBill.PrePaymentAmount + bill.PendingPrincipalAmount;
							}

							if (bill.BalanceRemaining == 0  && savedBill.BillStatus != "P" && bill.BillNumber != 0)
							{
								savedBill.BillStatus = "P";
							}
							else if (savedBill.BillStatus != "A")
							{
								savedBill.BillStatus = "A";
							}
						}
					}
				}

				accountsReceivableContext.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
