﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;

namespace SharedDataAccess.AccountsReceivable
{
	public class CustomerPartyQueryHandler : IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>
	{
		private IAccountsReceivableContext accountsReceivableContext;

		public CustomerPartyQueryHandler(ITrioContextFactory contextFactory)
		{
			this.accountsReceivableContext = contextFactory.GetAccountsReceivableContext();
		}

		public IEnumerable<CustomerPaymentStatusResult> ExecuteQuery(CustomerPartySearchCriteria query)
		{

			var result = accountsReceivableContext.CustomerParties.Where(x => (x.Deleted ?? false) == false);
			
			if ((query.Invoice ?? "") != "")
			{
				var invoiceQueryResult = result
					.Join(accountsReceivableContext.Bills,
						cust => cust.CustomerId,
						bill => bill.AccountKey,
						(cust, bill) => new { Customer = cust, Bill = bill })
					.Where(z => z.Bill.InvoiceNumber == query.Invoice);

				return invoiceQueryResult.Select(x => new CustomerPaymentStatusResult
				{
					Id = x.Customer.Id,
					PartyId = x.Customer.PartyId ?? 0,
					Address1 = x.Customer.Address1 ?? "",
					Address2 = x.Customer.Address2 ?? "",
					Address3 = x.Customer.Address3 ?? "",
					FullName = x.Customer.FullName ?? "",
					CustomerId = x.Customer.CustomerId ?? 0,
					State = x.Customer.State ?? "",
					City = x.Customer.City ?? "",
					Zip = x.Customer.Zip ?? "",
					InvoiceNumber = x.Bill.InvoiceNumber ?? "",
					Paid = x.Bill.PrinPaid >= x.Bill.PrinOwed
				}).ToList();
			}
			else
			{
				if ((query.Name ?? "") != "")
				{
					result = result.Where(x => x.FullName.StartsWith(query.Name));
				}

				if ((query.Address ?? "") != "")
				{
					result = result.Where(x => x.Address1.StartsWith(query.Address));
				}

				if (query.AccountNumber != 0)
				{
					result = result.Where(x => x.CustomerId == query.AccountNumber);
				}

				return result.Select(x => new CustomerPaymentStatusResult
				{
					Id = x.Id,
					Address1 = x.Address1 ?? "",
					Address2 = x.Address2 ?? "",
					Address3 = x.Address3 ?? "",
					FullName = x.FullName ?? "",
					CustomerId = x.CustomerId ?? 0,
					PartyId = x.PartyId ?? 0,
					State = x.State ?? "",
					City = x.City ?? "",
					Zip = x.Zip ?? "",
					InvoiceNumber = "",
					Paid = false
				}).ToList();
			}
		}
	}
}
