﻿using System.Collections.Generic;
using System.Linq;
using Remotion.Linq.Parsing.Structure.IntermediateModel;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;

namespace SharedDataAccess.AccountsReceivable
{
    public class AccountsReceivableEmptyContext : IAccountsReceivableContext
    {
        public IQueryable<CustomerParty> CustomerParties { get => new List<CustomerParty>().AsQueryable(); }
        public IQueryable<AuditChanges> AuditChanges { get => new List<AuditChanges>().AsQueryable(); }
        public IQueryable<AuditChangesArchive> AuditChangesArchive { get => new List<AuditChangesArchive>().AsQueryable(); }
        public IQueryable<Bill> Bills { get => new List<Bill>().AsQueryable(); }
        public IQueryable<CloseOut> CloseOut { get => new List<CloseOut>().AsQueryable(); }
        public IQueryable<Comment> Comments { get => new List<Comment>().AsQueryable(); }
        public IQueryable<CustomBillCodes> CustomBillCodes { get => new List<CustomBillCodes>().AsQueryable(); }
        public IQueryable<CustomBillFields> CustomBillFields { get => new List<CustomBillFields>().AsQueryable(); }
        public IQueryable<CustomBills> CustomBills { get => new List<CustomBills>().AsQueryable(); }
        public IQueryable<CustomerBills> CustomerBills { get => new List<CustomerBills>().AsQueryable(); }
        public IQueryable<CustomerMaster> CustomerMasters { get => new List<CustomerMaster>().AsQueryable(); }
        public IQueryable<Customize> Customize { get => new List<Customize>().AsQueryable(); }
        public IQueryable<Cya> Cya { get => new List<Cya>().AsQueryable(); }
        public IQueryable<Dbversion> Dbversion { get => new List<Dbversion>().AsQueryable(); }
        public IQueryable<DefaultBillType> DefaultBillTypes { get => new List<DefaultBillType>().AsQueryable(); }
        public IQueryable<Payees> Payees { get => new List<Payees>().AsQueryable(); }
        public IQueryable<PaymentRec> PaymentRec { get => new List<PaymentRec>().AsQueryable(); }
        public IQueryable<RateKey> RateKeys { get => new List<RateKey>().AsQueryable(); }
        public IQueryable<SavedStatusReports> SavedStatusReports { get => new List<SavedStatusReports>().AsQueryable(); }
    }
}