﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;

namespace SharedDataAccess.AccountsReceivable
{
	public class DefaultBillTypeQueryHandler : IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>
	{
		private IAccountsReceivableContext accountsReceivableContext;

		public DefaultBillTypeQueryHandler(ITrioContextFactory contextFactory)
		{
			this.accountsReceivableContext = contextFactory.GetAccountsReceivableContext();
		}

		public IEnumerable<DefaultBillType> ExecuteQuery(DefaultBillTypeSearchCriteria query)
		{
			IQueryable<DefaultBillType> defaultBillTypeQuery;

			defaultBillTypeQuery = accountsReceivableContext.DefaultBillTypes;

			if (query.TypeCode > 0)
			{
				defaultBillTypeQuery = defaultBillTypeQuery.Where(x => x.TypeCode == query.TypeCode);
			}

			return defaultBillTypeQuery.ToList();
		}
	}
}
