﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Models;

namespace SharedDataAccess.AccountsReceivable
{
	public class ARBillQueryHandler : IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>
	{
		private IAccountsReceivableContext accountsReceivableContext;
        private IPaymentService paymentService;
		public ARBillQueryHandler(ITrioContextFactory contextFactory, IPaymentService paymentService)
		{
			this.accountsReceivableContext = contextFactory.GetAccountsReceivableContext();
            this.paymentService = paymentService;
        }

		public IEnumerable<ARBill> ExecuteQuery(ARBillSearchCriteria query)
		{
			var billQuery = accountsReceivableContext.Bills;

			if (query.CustomerId > 0)
			{
				billQuery = billQuery.Where(x => x.AccountKey == query.CustomerId);
			}

			if (query.VoidedSelection != VoidedStatusSelection.All)
			{
				if (query.VoidedSelection == VoidedStatusSelection.NotVoided)
				{
					billQuery = billQuery.Where(x => x.BillStatus != "V");
				}
				else
				{
					billQuery = billQuery.Where(x => x.BillStatus == "V");
				}
			}

			var rateKeyInfoResult = billQuery.GroupJoin(
					accountsReceivableContext.RateKeys,
					bill => bill.BillNumber,
					rate => rate.Id,
					(bill, rate) => new { Bill = bill, Rates = rate.DefaultIfEmpty()})
					.Select(b => new { b.Bill, Rate = b.Rates.FirstOrDefault()});
            var chgintlist = new List<int>();
			var bills = rateKeyInfoResult.Select(x => new ARBill
			{
				InvoiceNumber = x.Bill.InvoiceNumber,
				Id = x.Bill.Id,
				BillStatus = x.Bill.BillStatus,
				PrinPaid = x.Bill.PrinPaid,
				PrinOwed = x.Bill.PrinOwed,
				BillNumber = x.Bill.BillNumber,
				BillDate = x.Rate == null ? null : x.Rate.BillDate,
				IntPaid = x.Bill.IntPaid,
				TaxOwed = x.Bill.TaxOwed,
				TaxPaid = x.Bill.TaxPaid,
				IntAdded = x.Bill.IntAdded,
				CreatedBy = x.Bill.CreatedBy,
				CurrentInterest = 0,
				Bname = x.Bill.Bname ?? "",
				Bname2 = x.Bill.Bname2 ?? "",
				BillType = x.Bill.BillType ?? 0,
				FlatRate = x.Rate == null ? 0 : x.Rate.FlatRate ?? 0,
				IntPaidDate = x.Bill.IntPaidDate,
				InterestMethod = x.Rate == null ? "" : x.Rate.InterestMethod,
				InterestRate = x.Rate == null ?  0 : x.Rate.IntRate ?? 0,
				InterestStartDate = x.Rate == null ? null : x.Rate.IntStart,
				LastInterestDate = x.Bill.IntPaidDate ?? (x.Rate == null ? null : x.Rate.IntStart),
				PreviousInterestDate = x.Bill.PreviousInterestPaidDate,
				SavedPreviousInterestDate = x.Bill.PreviousInterestPaidDate,
				CurrentInterestDate = null,
				Reference = x.Bill.Reference ?? "",
				Control1 = x.Bill.Control1 ?? "",
				Control2 = x.Bill.Control2 ?? "",
				Control3 = x.Bill.Control3 ?? "",
				Address1 = x.Bill.Baddress1 ?? "",
				Address2 = x.Bill.Baddress2 ?? "",
				Address3 = x.Bill.Baddress3 ?? "",
				City = x.Bill.Bcity ?? "",
				State = x.Bill.Bstate ?? "",
				Zip = x.Bill.Bzip ?? "",
				Zip4 = x.Bill.Bzip4 ?? "",
				Amount1 = x.Bill.Amount1 ?? 0,
				Amount2 = x.Bill.Amount2 ?? 0,
				Amount3 = x.Bill.Amount3 ?? 0,
				Amount4 = x.Bill.Amount4 ?? 0,
				Amount5 = x.Bill.Amount5 ?? 0,
				Amount6 = x.Bill.Amount6 ?? 0,
				BalanceRemaining = (decimal)x.Bill.PrinOwed - (decimal)x.Bill.PrinPaid + (decimal)x.Bill.TaxOwed - (decimal)x.Bill.TaxPaid - (decimal)x.Bill.IntAdded - (decimal)x.Bill.IntPaid,
				PerDiem = 0,
				RateCreatedDate = x.Rate.DateCreated,
				RateStart = x.Rate.Start,
				RateEnd = x.Rate.End,
				Pending = false,
				PendingChargedInterest = 0,
				PendingInterestAmount = 0,
				PendingPrincipalAmount = 0,
				PendingTaxAmount = 0,
				//Payments = x.Bill.Payments.Where(z => !x.Bill.Payments.Any(m => m.TransactionIdentifier == z.TransactionIdentifier && z.Chgintdate != null)).Select(y => new ARPayment
                Payments = x.Bill.Payments.Select(y => new ARPayment
					{
					Id = y.Id,
					ReceiptId = y.ReceiptNumber,
					ActualSystemDate = y.ActualSystemDate,
					Tax = y.Tax,
					Code = y.Code,
					TransactionId = y.TransactionIdentifier,
					Interest = y.Interest,
					Principal = y.Principal,
					Reference = y.Reference,
					Teller = y.Teller,
					PaidBy = y.PaidBy,
					Comments = y.Comments,
					RecordedTransactionDate = y.RecordedTransactionDate,
					GeneralLedger = y.GeneralLedger,
					DailyCloseOut = y.DailyCloseOut,
					BudgetaryAccountNumber = y.BudgetaryAccountNumber,
					CashDrawer = y.CashDrawer,
					BillId = y.BillKey ?? 0,
					EffectiveInterestDate = y.EffectiveInterestDate,
					Pending = false,
					ChargedInterestDate = y.Chgintdate,
					IsReversal = false,
					ReversedPaymentId = 0,
					ChargedInterestRecord = x.Bill.Payments.Where(a => a.TransactionIdentifier == y.TransactionIdentifier && a.TransactionIdentifier != null && a.Chgintdate != null).Select(b => new ARPayment
					{
						Id = b.Id,
						ReceiptId = b.ReceiptNumber,
						ActualSystemDate = b.ActualSystemDate,
						Tax = b.Tax,
						Code = b.Code,
						TransactionId = b.TransactionIdentifier,
						Interest = b.Interest,
						Principal = b.Principal,
						Reference = b.Reference,
						Teller = b.Teller,
						PaidBy = b.PaidBy,
						Comments = b.Comments,
						RecordedTransactionDate = b.RecordedTransactionDate,
						GeneralLedger = b.GeneralLedger,
						DailyCloseOut = b.DailyCloseOut,
						BudgetaryAccountNumber = b.BudgetaryAccountNumber,
						CashDrawer = b.CashDrawer,
						BillId = b.BillKey ?? 0,
						EffectiveInterestDate = b.EffectiveInterestDate,
						Pending = false,
						ChargedInterestDate = b.Chgintdate,
						IsReversal = false,
						ReversedPaymentId = 0,
					}).FirstOrDefault()
				}).ToList()
			}).ToList();

			//remove chgint records that have corresponding payments. leave orphaned chgint records
            foreach (var bill in bills)
            {
                var idList = bill.Payments.Where(p => p.ChargedInterestRecord != null).Select(p => p.ChargedInterestRecord.Id).ToList();
                if (idList.Any())
                {
                    foreach (var id in idList)
                    {
                        bill.Payments.RemoveAll(p => p.Id == id);
                    }
                }
            }

            if (query.CalculateCurrentInterest)
            {
                foreach (var bill in bills)
                {
					var result = paymentService.CalculateBillTotals(bill, query.EffectiveDate);

                    if (result.InterestCalculated)
                    {
                        bill.LastInterestDate = result.LastInterestDate;
                        bill.CurrentInterest = result.CalculatedInterestResult.Interest * -1;
                        bill.CurrentInterestDate = result.CalculatedInterestDate;
                        bill.PerDiem = (decimal)result.CalculatedInterestResult.PerDiem;
						bill.BalanceRemaining = (decimal)bill.PrinOwed - (decimal)bill.PrinPaid - bill.PendingPrincipalAmount + (decimal)bill.TaxOwed -
                                                (decimal)bill.TaxPaid - bill.PendingTaxAmount + BillInterestAmount(bill);
					}
				}
			}

            return bills;
        }

        private decimal BillInterestAmount(ARBill bill)
        {
            return ((bill.IntAdded ?? 0) * -1) - (bill.IntPaid ?? 0) - bill.CurrentInterest - bill.PendingInterestAmount;
        }
	}
}
