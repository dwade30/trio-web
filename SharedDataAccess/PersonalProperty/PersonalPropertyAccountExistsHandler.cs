﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty;

namespace SharedDataAccess.PersonalProperty
{
    public class PersonalPropertyAccountExistsHandler : CommandHandler<PersonalPropertyAccountExists,bool>
    {
        private IPersonalPropertyContext ppContext;

        public PersonalPropertyAccountExistsHandler(IPersonalPropertyContext ppContext)
        {
            this.ppContext = ppContext;
        }
        protected override bool Handle(PersonalPropertyAccountExists command)
        {
            return ppContext.PPMasters.Any(p => p.Account == command.Account);
        }
    }
}