﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.PersonalProperty.Models;

namespace SharedDataAccess.PersonalProperty
{
    public class TranCodeConfiguration : IEntityTypeConfiguration<TranCode>
    {
        public void Configure(EntityTypeBuilder<TranCode> builder)
        {
            builder.ToTable("TranCodes");
            builder.Property(p => p.Code).HasColumnName("TranCode");
            builder.Property(p => p.Description).HasColumnName("TranType").HasMaxLength(255);
        }
    }
}
