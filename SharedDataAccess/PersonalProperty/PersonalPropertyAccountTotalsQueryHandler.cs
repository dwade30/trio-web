﻿using System.Linq;
using SharedApplication;
using SharedApplication.PersonalProperty;

namespace SharedDataAccess.PersonalProperty
{
    public class PersonalPropertyAccountTotalsQueryHandler : IQueryHandler<PersonalPropertyAccountTotalsQuery, PersonalPropertyAccountTotals>
    {
        private IPersonalPropertyContext ppContext;

        public PersonalPropertyAccountTotalsQueryHandler(IPersonalPropertyContext ppContext)
        {
            this.ppContext = ppContext;
        }


        public PersonalPropertyAccountTotals ExecuteQuery(PersonalPropertyAccountTotalsQuery query)
        {
            var valuation = ppContext.PPValuations.FirstOrDefault(v => v.Account == query.Account);            
            if (valuation == null)
            {
                return new PersonalPropertyAccountTotals(){Account = query.Account};
            }

            var categories = ppContext.RatioTrends.OrderBy(r => r.CategoryType).ToList();
            var totals = new PersonalPropertyAccountTotals()
            {
                Account = query.Account,
                TotalItemized = valuation.TotalItemized.GetValueOrDefault(),
                TotalLeased =  valuation.TotalLeased.GetValueOrDefault()
            };
            var ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category1.GetValueOrDefault(), CategoryNumber = 1,
                Exempt = valuation.Cat1Exempt.GetValueOrDefault()
            };
            
            var category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 1);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);
            ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category2.GetValueOrDefault(), CategoryNumber = 2,
                Exempt = valuation.Cat2Exempt.GetValueOrDefault()
            };
            category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 2);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);
            ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category3.GetValueOrDefault(),
                CategoryNumber = 3,
                Exempt = valuation.Cat3Exempt.GetValueOrDefault()
            };
            category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 3);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);
            ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category4.GetValueOrDefault(),
                CategoryNumber = 4,
                Exempt = valuation.Cat4Exempt.GetValueOrDefault()
            };
            category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 4);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);
            ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category5.GetValueOrDefault(),
                CategoryNumber = 5,
                Exempt = valuation.Cat5Exempt.GetValueOrDefault()
            };
            category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 5);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);
            ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category6.GetValueOrDefault(),
                CategoryNumber = 6,
                Exempt = valuation.Cat6Exempt.GetValueOrDefault()
            };
            category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 6);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);
            ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category7.GetValueOrDefault(),
                CategoryNumber = 7,
                Exempt = valuation.Cat7Exempt.GetValueOrDefault()
            };
            category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 7);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);
            ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category8.GetValueOrDefault(),
                CategoryNumber = 8,
                Exempt = valuation.Cat8Exempt.GetValueOrDefault()
            };
            category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 8);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);
            ppValuation = new PersonalPropertyValuation()
            {
                Amount = valuation.Category9.GetValueOrDefault(),
                CategoryNumber = 9,
                Exempt = valuation.Cat9Exempt.GetValueOrDefault()
            };
            category = categories.FirstOrDefault(c => c.Type.GetValueOrDefault() == 9);
            if (category != null)
            {
                ppValuation.CategoryName = category.Description;
            }
            totals.Valuations.AddValuation(ppValuation);

            return totals;
        }
    }
}