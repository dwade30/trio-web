﻿using System.Collections.Generic;
using System.Linq;
using PersonalProperty;
using SharedApplication.PersonalProperty;
using SharedApplication.PersonalProperty.Models;

namespace SharedDataAccess.PersonalProperty
{
    public class PersonalPropertyEmptyContext : IPersonalPropertyContext
    {
        public IQueryable<TranCode> TranCodes { get => new List<TranCode>().AsQueryable(); }
        public IQueryable<ExemptCode> ExemptCodes { get => new List<ExemptCode>().AsQueryable(); }
        public IQueryable<PPMaster> PPMasters { get => new List<PPMaster>().AsQueryable(); }
        public IQueryable<PPValuation> PPValuations { get => new List<PPValuation>().AsQueryable(); }
        public IQueryable<PPRatioOpen> PPRatioOpens { get => new List<PPRatioOpen>().AsQueryable(); }
        public IQueryable<RatioTrend> RatioTrends { get => new List<RatioTrend>().AsQueryable(); }
        public IQueryable<PPAccountPartyView> PPAccountPartyViews => new List<PPAccountPartyView>().AsQueryable();

        public IQueryable<PPAccountPartyAddressView> PPAccountPartyAddressViews => new List<PPAccountPartyAddressView>().AsQueryable();
    }
}