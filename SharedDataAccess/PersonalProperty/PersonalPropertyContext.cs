﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PersonalProperty;
using SharedApplication.PersonalProperty;
using SharedApplication.PersonalProperty.Models;
using SharedDataAccess.PersonalProperty.Configuration;

namespace SharedDataAccess.PersonalProperty
{
    public class PersonalPropertyContext : DataContext<PersonalPropertyContext>, IPersonalPropertyContext
    {
        public DbSet<TranCode> TranCodes { get; set; }
        public DbSet<PPMaster> PPMasters { get; set; }
        public DbSet<PPValuation> PPValuations { get; set; }
        public DbSet<PPRatioOpen> PPRatioOpens { get; set; }
        public DbSet<RatioTrend> RatioTrends { get; set; }
		public DbSet<ExemptCode> ExemptCodes { get; set; }
        public DbQuery<PPAccountPartyView> PPAccountPartyViews { get; set; }
        public DbQuery<PPAccountPartyAddressView> PPAccountPartyAddressViews { get; set; }
        IQueryable<TranCode> IPersonalPropertyContext.TranCodes => TranCodes;
        IQueryable<ExemptCode> IPersonalPropertyContext.ExemptCodes => ExemptCodes;
        IQueryable<PPMaster> IPersonalPropertyContext.PPMasters => PPMasters;
        IQueryable<PPAccountPartyView> IPersonalPropertyContext.PPAccountPartyViews => PPAccountPartyViews;
        IQueryable<PPValuation> IPersonalPropertyContext.PPValuations => PPValuations;
        IQueryable<PPRatioOpen> IPersonalPropertyContext.PPRatioOpens => PPRatioOpens;
        IQueryable<RatioTrend> IPersonalPropertyContext.RatioTrends => RatioTrends;

        IQueryable<PPAccountPartyAddressView> IPersonalPropertyContext.PPAccountPartyAddressViews =>
            PPAccountPartyAddressViews;
        public PersonalPropertyContext(DbContextOptions<PersonalPropertyContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new TranCodeConfiguration());
            modelBuilder.ApplyConfiguration(new PPMasterConfiguration());
            modelBuilder.ApplyConfiguration(new PPAccountPartyViewConfiguration());
            modelBuilder.ApplyConfiguration(new PPValuationConfiguration());
            modelBuilder.ApplyConfiguration(new PPRatioOpenConfiguration());
            modelBuilder.ApplyConfiguration(new RatioTrendConfiguration());
            modelBuilder.ApplyConfiguration(new ExemptCodeConfiguration());
            modelBuilder.ApplyConfiguration(new PPAccountPartyAddressViewConfiguration());
        }
    }
}
