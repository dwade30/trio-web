﻿using System;
using System.Linq;
using SharedApplication;
using SharedApplication.PersonalProperty;

namespace SharedDataAccess.PersonalProperty
{
    public class PersonalPropertyAccountBriefQueryHandler : IQueryHandler<PersonalPropertyAccountBriefQuery,PersonalPropertyAccountBriefInfo>
    {
        private IPersonalPropertyContext ppContext;
        private DataEnvironmentSettings dataEnvSettings;
        public PersonalPropertyAccountBriefQueryHandler(IPersonalPropertyContext ppContext, DataEnvironmentSettings dataEnvSettings)
        {
            this.ppContext = ppContext;
            this.dataEnvSettings = dataEnvSettings;
        }
        public PersonalPropertyAccountBriefInfo ExecuteQuery(PersonalPropertyAccountBriefQuery query)
        {
            var account = ppContext.PPAccountPartyViews.FirstOrDefault(p => p.Account == query.Account);
            if (account == null)
            {
                return new PersonalPropertyAccountBriefInfo(){Account = query.Account};
            }
            return new PersonalPropertyAccountBriefInfo()
            {
                Account = query.Account,
                Deleted = account.Deleted.GetValueOrDefault(),
                PartyId = account.PartyId.GetValueOrDefault(),
                LocationStreet = account.Street,
                LocationNumber = account.StreetNumber.GetValueOrDefault(),
                Name = account.FullNameLastFirst(),
                TownId = dataEnvSettings.IsMultiTown ? Convert.ToInt32(account.TranCode.GetValueOrDefault()) : 0
            };

           
        }
    }
}