﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.PersonalProperty.Models;

namespace SharedDataAccess.PersonalProperty.Configuration
{
    public class PPRatioOpenConfiguration :IEntityTypeConfiguration<PPRatioOpen>
    {
        public void Configure(EntityTypeBuilder<PPRatioOpen> builder)
        {
            builder.ToTable("PPRatioOpens");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.OpenField1).HasMaxLength(255);

            builder.Property(e => e.OpenField2).HasMaxLength(255);

            builder.Property(e => e.Ratio).HasMaxLength(255);
        }
    }
}