﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.PersonalProperty.Models;

namespace SharedDataAccess.PersonalProperty.Configuration
{
    public class PPValuationConfiguration : IEntityTypeConfiguration<PPValuation>
    {
        public void Configure(EntityTypeBuilder<PPValuation> builder)
        {
            builder.ToTable("PPValuations");

            builder.HasIndex(e => e.Account)
                .HasName("ix_ValueKey");

            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.Account).HasColumnName("ValueKey");
        }
    }
}