﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.PersonalProperty.Models;

namespace SharedDataAccess.PersonalProperty.Configuration
{
    public class PPAccountPartyAddressViewConfiguration : IQueryTypeConfiguration<PPAccountPartyAddressView>
    {
        public void Configure(QueryTypeBuilder<PPAccountPartyAddressView> builder)
        {
            builder.ToView("AccountPartyAddressView");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AccountId)
                .HasColumnName("AccountID")
                .HasMaxLength(255);

            builder.Property(e => e.Comment).IsUnicode(false);

            builder.Property(e => e.Da).HasColumnName("DA");

            builder.Property(e => e.Mo).HasColumnName("MO");

            builder.Property(e => e.Open1).HasMaxLength(255);

            builder.Property(e => e.Open2).HasMaxLength(255);

            builder.Property(e => e.Orcode)
                .HasColumnName("ORCode")
                .HasMaxLength(50);

            builder.Property(e => e.PartyId).HasColumnName("PartyID");

            builder.Property(e => e.Rbcode)
                .HasColumnName("RBCode")
                .HasMaxLength(50);

            builder.Property(e => e.Street).HasMaxLength(255);

            builder.Property(e => e.StreetApt).HasMaxLength(255);

            builder.Property(e => e.Updcode)
                .HasColumnName("UPDCode")
                .HasMaxLength(50);

            builder.Property(e => e.Yr).HasColumnName("YR");

            builder.Property(e => e.Designation).HasMaxLength(50);

            builder.Property(e => e.Email).HasMaxLength(200);

            builder.Property(e => e.FirstName).HasMaxLength(255);

            builder.Property(e => e.LastName).HasMaxLength(100);

            builder.Property(e => e.MiddleName).HasMaxLength(100);

            builder.Property(e => e.WebAddress).HasMaxLength(200);
            builder.Property(e => e.Address1).HasMaxLength(255);
            builder.Property(e => e.Address2).HasMaxLength(255);
            builder.Property(e => e.Address3).HasMaxLength(255);
            builder.Property(e => e.City).HasMaxLength(255);
            builder.Property(e => e.State).HasMaxLength(255);
            builder.Property(e => e.State).HasMaxLength(255);
            builder.Property(e => e.Zip).HasMaxLength(255);
            builder.Property(e => e.AddressType).HasMaxLength(255);
            builder.Property(e => e.Country).HasMaxLength(255);
        }
    }
}