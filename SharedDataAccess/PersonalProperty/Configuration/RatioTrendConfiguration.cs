﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.PersonalProperty.Models;

namespace SharedDataAccess.PersonalProperty.Configuration
{
    public class RatioTrendConfiguration : IEntityTypeConfiguration<RatioTrend>
    {
        public void Configure(EntityTypeBuilder<RatioTrend> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.Exponent).HasColumnName("exponent");

            builder.Property(e => e.Sd)
                .HasColumnName("SD")
                .HasMaxLength(255);

            builder.Property(e => e.Trend).HasMaxLength(255);
        }
    }
}