﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.PersonalProperty.Models;

namespace SharedDataAccess.PersonalProperty.Configuration
{
	class ExemptCodeConfiguration : IEntityTypeConfiguration<ExemptCode>
	{
		public void Configure(EntityTypeBuilder<ExemptCode> builder)
		{
			builder.Property(e => e.ID).HasColumnName("ID");

			builder.Property(e => e.Description).HasMaxLength(255);

			builder.Property(e => e.Code).HasColumnName("Code");

			builder.Property(e => e.Amount).HasColumnName("Amount");

			builder.Property(e => e.MVRCategory).HasMaxLength(50);
		}
	}
}
