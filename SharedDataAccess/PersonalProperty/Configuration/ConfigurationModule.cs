﻿using Autofac;
using SharedApplication;
using SharedApplication.PersonalProperty;

namespace SharedDataAccess.PersonalProperty.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PersonalPropertyAccountTotalsQueryHandler>()
                .As<IQueryHandler<PersonalPropertyAccountTotalsQuery, PersonalPropertyAccountTotals>>();
            builder.RegisterType<PersonalPropertyAccountBriefQueryHandler>()
                .As<IQueryHandler<PersonalPropertyAccountBriefQuery, PersonalPropertyAccountBriefInfo>>();
            RegisterPersonalPropertyContext(ref builder);
        }

        private void RegisterPersonalPropertyContext(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var activeModules = f.Resolve<IGlobalActiveModuleSettings>();
                if (activeModules.PersonalPropertyIsActive || activeModules.TaxBillingIsActive || activeModules.TaxCollectionsIsActive)
                {
                    var tcf = f.Resolve<ITrioContextFactory>();
                    return tcf.GetPersonalPropertyContext();
                }
                IPersonalPropertyContext emptyContext = new PersonalPropertyEmptyContext();
                return emptyContext;
            }).As<IPersonalPropertyContext>();
        }
    }
}