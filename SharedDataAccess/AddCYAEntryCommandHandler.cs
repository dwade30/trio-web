﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using SharedApplication;
using SharedApplication.Authorization;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedDataAccess.CashReceipts;
using SharedDataAccess.Clerk;

namespace SharedDataAccess
{
	public class AddCYAEntryCommandHandler : CommandHandler<AddCYAEntry>
	{
		private CashReceiptsContext cashReceiptsContext;
		private ClerkContext clerkContext;
		private UserInformation userInformation;

		public AddCYAEntryCommandHandler(ITrioContextFactory trioContextFactory, UserInformation userInformation)
		{
			cashReceiptsContext = trioContextFactory.GetCashReceiptsContext();
			clerkContext = trioContextFactory.GetClerkContext();
			this.userInformation = userInformation;
		}

		protected override void Handle(AddCYAEntry command)
		{
			try
			{
				switch (command.LoggingProgram)
				{
					case TrioPrograms.CashReceipts:
					
						cashReceiptsContext.CYAs.Add(new Cya
						{
							UserID = userInformation.UserId,
							CYADate = DateTime.Now,
							Description1 = command.Description1,
							Description2 = command.Description2,
							Description3 = command.Description3,
							Description4 = command.Description4,
							Description5 = command.Description5
						});

						cashReceiptsContext.SaveChanges();
						break;
					case TrioPrograms.Clerk:

						clerkContext.Cyas.Add(new SharedApplication.Clerk.Models.Cya
						{
							UserId = userInformation.UserId,
							Cyadate = DateTime.Now,
							Description1 = command.Description1,
							Description2 = command.Description2,
							Description3 = command.Description3,
							Description4 = command.Description4,
							Description5 = command.Description5
						});

						clerkContext.SaveChanges();
						break;
				}
				

			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}
	}
}
