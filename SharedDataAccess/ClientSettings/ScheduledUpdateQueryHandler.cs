﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.ClientSettings;
using SharedApplication.ClientSettings.Enums;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.ClientSettings.Models;

namespace SharedDataAccess.ClientSettings
{
	public class ScheduledUpdateQueryHandler : IQueryHandler<ScheduledUpdateSearchCriteria, IEnumerable<ScheduledUpdate>>
	{
		private IClientSettingsContext clientSettingsContext;

		public ScheduledUpdateQueryHandler(IClientSettingsContext clientSettingsContext)
		{
			this.clientSettingsContext = clientSettingsContext;
		}
		public IEnumerable<ScheduledUpdate> ExecuteQuery(ScheduledUpdateSearchCriteria query)
		{
			IQueryable<ScheduledUpdate> scheduledUpdateQuery;

			scheduledUpdateQuery = clientSettingsContext.ScheduledUpdates;

			if (query.CompletedSelection == ScheduledUpdateCompletedOptions.CompletedOnly)
			{
				scheduledUpdateQuery = scheduledUpdateQuery.Where(x => x.Completed);
			}
			else if (query.CompletedSelection == ScheduledUpdateCompletedOptions.NotCompletedOnly)
			{
				scheduledUpdateQuery = scheduledUpdateQuery.Where(x => !x.Completed);
			}

			return scheduledUpdateQuery.ToList();
		}
	}

}
