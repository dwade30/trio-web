﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.ClientSettings
{
	public class CancelScheduledUpdateHandler : CommandHandler<CancelScheduledUpdate, bool>
	{
		private ClientSettingsContext clientSettingsContext;

		public CancelScheduledUpdateHandler(ITrioContextFactory trioContextFactory)
		{
			clientSettingsContext = trioContextFactory.GetClientSettingsContext();
		}

		protected override bool Handle(CancelScheduledUpdate command)
		{
			try
			{
				var updatesToRemove = clientSettingsContext.ScheduledUpdates.Where(x => !x.Completed).ToList();
				clientSettingsContext.ScheduledUpdates.RemoveRange(updatesToRemove);
				clientSettingsContext.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}

}
