﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.ClientSettings;
using SharedApplication.ClientSettings.Enums;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.ClientSettings.Models;

namespace SharedDataAccess.ClientSettings
{
	public class UserQueryHandler : IQueryHandler<UserSearchCriteria, IEnumerable<User>>
	{
		private IClientSettingsContext clientSettingsContext;

		public UserQueryHandler(IClientSettingsContext clientSettingsContext)
		{
			this.clientSettingsContext = clientSettingsContext;
		}
		public IEnumerable<User> ExecuteQuery(UserSearchCriteria query)
		{
			IQueryable<User> userQuery;

			userQuery = clientSettingsContext.Users;

			if (query.UserId > 0)
			{
				userQuery = userQuery.Where(x => x.Id == query.UserId);
			}
			
			if (query.ClientId != "")
			{
				userQuery = userQuery.Where(x => x.ClientIdentifier == Guid.Parse(query.ClientId));
			}

			return userQuery.ToList();
		}
	}
}
