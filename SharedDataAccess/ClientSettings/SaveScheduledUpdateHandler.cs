﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Messaging;
using SharedDataAccess.CashReceipts;

namespace SharedDataAccess.ClientSettings
{
	public class SaveScheduledUpdateHandler : CommandHandler<SaveScheduledUpdate, bool>
	{
		private ClientSettingsContext clientSettingsContext;

		public SaveScheduledUpdateHandler(ITrioContextFactory trioContextFactory)
		{
			clientSettingsContext = trioContextFactory.GetClientSettingsContext();
		}

		protected override bool Handle(SaveScheduledUpdate command)
		{
			try
			{
				var update = clientSettingsContext.ScheduledUpdates.Where(x => !x.Completed).FirstOrDefault();
				
				if (update != null)
				{
					update.ScheduledDateTime = command.updateToSave.ScheduledDateTime;
					update.ScheduledByUserId = command.updateToSave.ScheduledByUserId;
					update.UpdateResult = command.updateToSave.UpdateResult ?? "";
					update.Completed = command.updateToSave.Completed;
				}
				else
				{
					clientSettingsContext.ScheduledUpdates.Add(command.updateToSave);
				}

				var version = clientSettingsContext.SiteVersions.FirstOrDefault();
				if (version != null)
				{
					version.Version = command.Version;
				}
				else
				{
					clientSettingsContext.SiteVersions.Add(new SiteVersion
					{
						Version = command.Version
					});
				}

				clientSettingsContext.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
