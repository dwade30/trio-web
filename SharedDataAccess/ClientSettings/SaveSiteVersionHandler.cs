﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.ClientSettings
{
	public class SaveSiteVersionHandler : CommandHandler<SaveSiteVersion>
	{
		private ClientSettingsContext clientSettingsContext;

		public SaveSiteVersionHandler(ITrioContextFactory trioContextFactory)
		{
			clientSettingsContext = trioContextFactory.GetClientSettingsContext();
		}

		protected override void Handle(SaveSiteVersion command)
		{
			try
			{
				var version = clientSettingsContext.SiteVersions.FirstOrDefault();

				if (version != null)
				{
					version.Version = command.Version;
				}
				else
				{
					clientSettingsContext.SiteVersions.Add(new SiteVersion
					{
						Version = command.Version
					});
				}

				clientSettingsContext.SaveChanges();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}
	}
}
