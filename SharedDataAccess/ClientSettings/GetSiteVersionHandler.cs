﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.Messaging;

namespace SharedDataAccess.ClientSettings
{
	public class GetSiteVersionHandler : CommandHandler<GetSiteVersion, string>
	{
		private ClientSettingsContext clientSettingsContext;

		public GetSiteVersionHandler(ITrioContextFactory trioContextFactory)
		{
			clientSettingsContext = trioContextFactory.GetClientSettingsContext();
		}

		protected override string Handle(GetSiteVersion command)
		{
			try
			{
				return clientSettingsContext.SiteVersions.FirstOrDefault()?.Version ?? "";
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return "";
			}
		}
	}
}
