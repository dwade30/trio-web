﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.ClientSettings.Models;

namespace SharedDataAccess.ClientSettings.Configuration
{
	public class SiteVersionConfiguration : IEntityTypeConfiguration<SiteVersion>
	{
		public void Configure(EntityTypeBuilder<SiteVersion> builder)
		{
			builder.ToTable("SiteVersion");
		}
	}
}
