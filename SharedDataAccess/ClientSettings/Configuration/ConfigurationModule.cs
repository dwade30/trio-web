﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.ClientSettings;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.ClientSettings.Models;

namespace SharedDataAccess.ClientSettings.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<ScheduledUpdateQueryHandler>().As<IQueryHandler<ScheduledUpdateSearchCriteria, IEnumerable<ScheduledUpdate>>>();
			builder.RegisterType<UserQueryHandler>().As<IQueryHandler<UserSearchCriteria, IEnumerable<User>>>();
			RegisterClientSettingsContext(builder);
		}

		private void RegisterClientSettingsContext(ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var tcf = f.Resolve<ITrioContextFactory>();
				return tcf.GetClientSettingsContext();
			}).As<IClientSettingsContext>();
		}
	}
}
