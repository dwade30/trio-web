﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.ClientSettings.Models;

namespace SharedDataAccess.ClientSettings.Configuration
{

	public class ScheduledUpdateConfiguration : IEntityTypeConfiguration<ScheduledUpdate>
	{
		public void Configure(EntityTypeBuilder<ScheduledUpdate> builder)
		{
			builder.ToTable("ScheduledUpdates");
		}
	}
}
