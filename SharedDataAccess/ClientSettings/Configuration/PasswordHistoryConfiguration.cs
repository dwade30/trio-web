﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.ClientSettings.Models;

namespace SharedDataAccess.ClientSettings.Configuration
{
	public class PasswordHistoryConfiguration : IEntityTypeConfiguration<PasswordHistory>
	{
		public void Configure(EntityTypeBuilder<PasswordHistory> builder)
		{
			builder.ToTable("PasswordHistory");
			builder.Property(p => p.Salt).HasMaxLength(255);
		}
	}
}
