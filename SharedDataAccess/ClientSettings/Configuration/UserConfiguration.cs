﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.ClientSettings.Models;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.ClientSettings.Configuration
{
	public class UserConfiguration : IEntityTypeConfiguration<User>
	{
		public void Configure(EntityTypeBuilder<User> builder)
		{
			builder.ToTable("Users");
			builder.Property(p => p.Salt).HasMaxLength(255);

			builder.HasMany(c => c.PreviousPasswords)
				.WithOne(e => e.User)
				.HasForeignKey(e => e.UserId);
		}
	}
}
