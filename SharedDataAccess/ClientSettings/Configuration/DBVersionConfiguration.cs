﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.ClientSettings.Models;

namespace SharedDataAccess.ClientSettings.Configuration
{

	public class DBVersionConfiguration : IEntityTypeConfiguration<DBVersion>
	{
		public void Configure(EntityTypeBuilder<DBVersion> builder)
		{
			builder.ToTable("DBVersion");
		}
	}
}
