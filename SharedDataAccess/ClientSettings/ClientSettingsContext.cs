﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.ClientSettings.Models;
using SharedDataAccess.ClientSettings.Configuration;

namespace SharedDataAccess.ClientSettings
{
    public class ClientSettingsContext : DataContext<ClientSettingsContext>, IClientSettingsContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<DBVersion> DBVersions { get; set; }
        public DbSet<PasswordHistory> PasswordHistories { get; set; }
        public DbSet<SiteVersion> SiteVersions { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ScheduledUpdate> ScheduledUpdates { get; set; }

        IQueryable<User> IClientSettingsContext.Users => Users;
        IQueryable<DBVersion> IClientSettingsContext.DBVersions => DBVersions;
        IQueryable<SiteVersion> IClientSettingsContext.SiteVersions => SiteVersions;
        IQueryable<PasswordHistory> IClientSettingsContext.PasswordHistories => PasswordHistories;
        IQueryable<Client> IClientSettingsContext.Clients => Clients;
        IQueryable<ScheduledUpdate> IClientSettingsContext.ScheduledUpdates => ScheduledUpdates;


        public ClientSettingsContext(DbContextOptions<ClientSettingsContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new ClientConfiguration());
            modelBuilder.ApplyConfiguration(new SiteVersionConfiguration());
            modelBuilder.ApplyConfiguration(new DBVersionConfiguration());
            modelBuilder.ApplyConfiguration(new PasswordHistoryConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new ScheduledUpdateConfiguration());

        }
    }
}
