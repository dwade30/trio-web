﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.GeneralEntry.Models;
using SharedDataAccess.GeneralEntry.Configuration;

namespace SharedDataAccess.GeneralEntry
{
    public class GeneralEntryContext : DataContext<GeneralEntryContext>
    {
        public  DbSet<tblRegion> tblRegions { get; set; }
        public GeneralEntryContext(DbContextOptions<GeneralEntryContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new tblRegionConfiguration());
        }
    }
}
