﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.SystemSettings.Interfaces;
using SharedApplication.SystemSettings.Models;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;

namespace SharedDataAccess.SystemSettings
{
	public class GetModulesHandler : CommandHandler<GetModules, Module>
	{
		private ISystemSettingsContext systemSettingsContext;
		public GetModulesHandler(ITrioContextFactory contextFactory)
		{
			this.systemSettingsContext = contextFactory.GetSystemSettingsContext();
		}

		protected override Module Handle(GetModules command)
		{
			try
			{
				var utSetting = systemSettingsContext.Modules.FirstOrDefault();
				if (utSetting != null)
				{
					return utSetting;
				}
				return new Module();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}
