﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.SystemSettings.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SharedDataAccess.SystemSettings.Configuration
{
    public class PasswordHistoryConfiguration : IEntityTypeConfiguration<PasswordHistory>
    {
        public void Configure(EntityTypeBuilder<PasswordHistory> builder)
        {
            builder.ToTable("PasswordHistory");
            builder.Property(p => p.Salt).HasMaxLength(255);
        }
    }
}
