﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.SystemSettings.Configuration
{
	public class ModuleConfiguration : IEntityTypeConfiguration<Module>
	{
		public void Configure(EntityTypeBuilder<Module> builder)
		{
			builder.ToTable("Modules");
			builder.Property(p => p.CheckDigits).HasMaxLength(50);
			builder.Property(p => p.TownCheck).HasMaxLength(50);
			builder.Property(p => p.MVRR).HasMaxLength(50);
			builder.Property(p => p.CRMoses).HasMaxLength(50);
			builder.Property(p => p.MV_StateLevel).HasMaxLength(255);
			builder.Property(p => p.MV_RENTALS).HasMaxLength(255);
			builder.Property(p => p.MV_RBLVL_AUTO).HasMaxLength(255);
			builder.Property(p => p.MV_RBLVL_HVTK).HasMaxLength(255);
			builder.Property(p => p.MV_RBLVL_MTCY).HasMaxLength(255);
			builder.Property(p => p.MV_RBLVL_RCVH).HasMaxLength(255);
			builder.Property(p => p.MV_RBLVL_TKBD).HasMaxLength(255);
			builder.Property(p => p.MV_RBLVL_TRAL).HasMaxLength(255);
            builder.Property(p => p.CRMyRec).HasMaxLength(50);
        }
	}
}
