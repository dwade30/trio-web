﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.SystemSettings.Configuration
{
    public class UserPermissionConfiguration : IEntityTypeConfiguration<UserPermission>
    {
        public void Configure(EntityTypeBuilder<UserPermission> builder)
        {
            builder.ToTable("PermissionsTable");
            builder.Property(p => p.ModuleName).HasMaxLength(255);
            builder.Property(p => p.Permission).HasMaxLength(255);
        }
    }
}