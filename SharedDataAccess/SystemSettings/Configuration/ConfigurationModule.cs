﻿using System.Collections.Generic;
using Autofac;
using SharedApplication;
using SharedApplication.Authorization;
using SharedApplication.SystemSettings;
using SharedApplication.SystemSettings.Interfaces;
using SharedApplication.SystemSettings.Models;
using SharedDataAccess.Authorization;
using Module = Autofac.Module;

namespace SharedDataAccess.SystemSettings.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GlobalVariableQueryHandler>().As<IQueryHandler<GlobalVariableQuery,GlobalVariable>>();
            builder.RegisterType<GetUserPermissionsQueryHandler>().As <
                IQueryHandler<GetUserPermissions, IEnumerable<PermissionItem>>>();
            builder.RegisterType<OperatorQueryHandler>().As<
	            IQueryHandler<OperatorQuery, IEnumerable<Operator>>>();
			RegisterSystemSettingsContext(builder);
        }

        private void RegisterSystemSettingsContext(ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return tcf.GetSystemSettingsContext();
            }).As<ISystemSettingsContext>().AsSelf();
        }
    }
}