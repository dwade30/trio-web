﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.SystemSettings.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SharedDataAccess.SystemSettings.Configuration
{
    public class SecurityConfiguration : IEntityTypeConfiguration<Security>
    {
        public void Configure(EntityTypeBuilder<Security> builder)
        {
            builder.ToTable("Security");
            builder.Property(p => p.Salt).HasMaxLength(255);
        }
    }
}
