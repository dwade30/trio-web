﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.SystemSettings.Configuration
{
    public class SettingConfiguration : IEntityTypeConfiguration<Setting>
    {
        public void Configure(EntityTypeBuilder<Setting> builder)
        {
            builder.ToTable("Settings");
            builder.Property(p => p.SettingName).HasMaxLength(255);
            builder.Property(p => p.SettingValue).HasMaxLength(255);
            builder.Property(p => p.SettingType).HasMaxLength(255);
            builder.Property(p => p.Owner).HasMaxLength(255);
            builder.Property(p => p.OwnerType).HasMaxLength(255);
        }
    }
}