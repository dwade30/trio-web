﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.SystemSettings.Configuration
{ 
    public class GlobalVariableConfiguration : IEntityTypeConfiguration<GlobalVariable>
    {
        public void Configure(EntityTypeBuilder<GlobalVariable> builder)
        {
            builder.ToTable("GlobalVariables");
            builder.Property(p => p.MuniName).HasMaxLength(50);
            builder.Property(p => p.CityTown).HasMaxLength(50);
            builder.Property(p => p.TrioSDrive).HasMaxLength(50);
            builder.Property(p => p.UseSecurity).HasMaxLength(50);
            builder.Property(p => p.ReleaseNumber).HasMaxLength(255);
            builder.Property(p => p.ArchiveYear).HasMaxLength(50);
            builder.Property(p => p.FTPSiteAddress).HasMaxLength(255);
            builder.Property(p => p.FTPSitePassword).HasMaxLength(255);
            builder.Property(p => p.FTPSiteUser).HasMaxLength(255);
            builder.Property(p => p.BulkMail1).HasMaxLength(255);
            builder.Property(p => p.BulkMail2).HasMaxLength(255);
            builder.Property(p => p.BulkMail3).HasMaxLength(255);
            builder.Property(p => p.BulkMail4).HasMaxLength(255);
            builder.Property(p => p.BulkMail5).HasMaxLength(255);
            builder.Property(p => p.SMTPServer).HasMaxLength(255);
            builder.Property(p => p.SMTPPassword).HasMaxLength(255);
            builder.Property(p => p.SMTPUser).HasMaxLength(255);
            builder.Property(p => p.UseRegistryOnly).HasMaxLength(255);
            builder.Property(p => p.FromName).HasMaxLength(255);
            builder.Property(p => p.DLLVersion).HasMaxLength(50);
        }
    }
}
