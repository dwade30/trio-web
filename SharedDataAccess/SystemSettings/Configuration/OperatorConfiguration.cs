﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.SystemSettings.Configuration
{
    public class OperatorConfiguration : IEntityTypeConfiguration<Operator>
    {
        public void Configure(EntityTypeBuilder<Operator> builder)
        {
            builder.ToTable("Operators");
            builder.Property(p => p.Code).HasMaxLength(255);
            builder.Property(p => p.Name).HasMaxLength(255);
            builder.Property(p => p.Level).HasMaxLength(255);
            builder.Property(p => p.Password).HasMaxLength(255);
            builder.Property(p => p.DeptDiv).HasMaxLength(50);
        }
    }
}