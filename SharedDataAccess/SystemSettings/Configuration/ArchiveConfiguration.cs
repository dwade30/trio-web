﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.SystemSettings.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SharedDataAccess.SystemSettings.Configuration
{
    public class ArchiveConfiguration : IEntityTypeConfiguration<Archive>
    {
        public void Configure(EntityTypeBuilder<Archive> builder)
        {
            builder.ToTable("Archive");
        }
    }
}
