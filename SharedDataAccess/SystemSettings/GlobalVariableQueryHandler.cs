﻿using System.Linq;
using SharedApplication;
using SharedApplication.SystemSettings;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.SystemSettings
{
    public class GlobalVariableQueryHandler : IQueryHandler<GlobalVariableQuery,GlobalVariable>
    {
        private SystemSettingsContext systemContext;
        public GlobalVariableQueryHandler(ITrioContextFactory contextFactory)
        {
           systemContext = contextFactory.GetSystemSettingsContext();
        }
        public GlobalVariable ExecuteQuery(GlobalVariableQuery query)
        {
            var globalVariable = systemContext.GlobalVariables.FirstOrDefault();
            if (globalVariable == null)
            {
                globalVariable = new GlobalVariable();
            }

            return globalVariable;
        }
    }
}