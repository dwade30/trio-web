﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication.SystemSettings.Interfaces;
using SharedApplication.SystemSettings.Models;
using SharedDataAccess.SystemSettings.Configuration;

namespace SharedDataAccess.SystemSettings
{
    public class SystemSettingsContext : DataContext<SystemSettingsContext>, ISystemSettingsContext
    {
        public DbSet<Archive> Archives { get; set; }
        public DbSet<Security> Securities { get; set; }
        public DbSet<PasswordHistory> PasswordHistories { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<GlobalVariable> GlobalVariables { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<Operator> Operators { get; set; }
        public DbSet<Module> Modules { get; set; }

        IQueryable<UserPermission> ISystemSettingsContext.UserPermissions => UserPermissions;

        IQueryable<Archive> ISystemSettingsContext.Archives => Archives;

        IQueryable<Security> ISystemSettingsContext.Securities => Securities;

        IQueryable<Setting> ISystemSettingsContext.Settings => Settings; 

        IQueryable<PasswordHistory> ISystemSettingsContext.PasswordHistories => PasswordHistories;

        IQueryable<GlobalVariable> ISystemSettingsContext.GlobalVariables => GlobalVariables;

        IQueryable<Operator> ISystemSettingsContext.Operators => Operators;

        IQueryable<Module> ISystemSettingsContext.Modules => Modules;

        public SystemSettingsContext(DbContextOptions<SystemSettingsContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new GlobalVariableConfiguration());
            modelBuilder.ApplyConfiguration(new ArchiveConfiguration());
            modelBuilder.ApplyConfiguration(new SecurityConfiguration());
            modelBuilder.ApplyConfiguration(new SettingConfiguration());
            modelBuilder.ApplyConfiguration(new PasswordHistoryConfiguration());
            modelBuilder.ApplyConfiguration(new UserPermissionConfiguration());
            modelBuilder.ApplyConfiguration(new OperatorConfiguration());
            modelBuilder.ApplyConfiguration(new ModuleConfiguration());
            modelBuilder.ApplyConfiguration(new SettingConfiguration());
            modelBuilder.Entity<Security>()
	            .HasMany(c => c.PreviousPasswords)
	            .WithOne(e => e.User)
	            .HasForeignKey(e => e.SecurityId);


		}
    }
}
