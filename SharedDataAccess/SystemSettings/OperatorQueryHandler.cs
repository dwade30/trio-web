﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.SystemSettings;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.SystemSettings
{
	public class OperatorQueryHandler : IQueryHandler<OperatorQuery, IEnumerable<Operator>>
	{
		private SystemSettingsContext systemContext;
		public OperatorQueryHandler(ITrioContextFactory contextFactory)
		{
			systemContext = contextFactory.GetSystemSettingsContext();
		}
		public IEnumerable<Operator> ExecuteQuery(OperatorQuery query)
		{
			IQueryable<Operator> operatorQuery;

			operatorQuery = systemContext.Operators;

			return operatorQuery.ToList();
		}
	}
}
