﻿using System.Linq;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings;
using SharedApplication.SystemSettings.Interfaces;

namespace SharedDataAccess.SystemSettings
{
    public class GetSystemSettingValueHandler :CommandHandler<GetSystemSettingValue,string>
    {
        public ISystemSettingsContext systemSettingsContext;

        public GetSystemSettingValueHandler(ISystemSettingsContext systemSettingsContext)
        {
            this.systemSettingsContext = systemSettingsContext;
        }
        protected override string Handle(GetSystemSettingValue command)
        {
            var query = systemSettingsContext.Settings.Where(s =>
                s.SettingName == command.SettingName && s.SettingType == command.SettingType);
            switch (command.OwnerType)
            {
                case SettingOwnerType.Machine:
                    query = query.Where(s => s.OwnerType == "Machine" && s.Owner == command.Owner);
                    break;
                case SettingOwnerType.User:
                    query = query.Where(s => s.OwnerType == "User" && s.Owner == command.Owner);
                    break;
                default:
                    query = query.Where(s => s.OwnerType == "");
                    break;
            }

            var result = query.FirstOrDefault();
            if (result == null)
            {
                return "";
            }

            return result.SettingValue;
        }
    }
}