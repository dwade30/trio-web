﻿using System.Linq;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.SystemSettings.Models;

namespace SharedDataAccess.SystemSettings
{
    public class SaveSystemSettingHandler : CommandHandler<SaveSystemSetting>
    {
        private SystemSettingsContext systemSettingsContext;
        public SaveSystemSettingHandler(SystemSettingsContext systemSettingsContext)
        {
            this.systemSettingsContext = systemSettingsContext;
        }
        protected override void Handle(SaveSystemSetting command)
        {
            var query = systemSettingsContext.Settings.Where(s =>
                s.SettingName == command.SettingName && s.SettingType == command.SettingType);
            var owner = "";
            var ownerType = "";
            switch (command.OwnerType)
            {
                case SettingOwnerType.Machine:
                    query = query.Where(s => s.OwnerType == "Machine" && s.Owner == command.Owner);
                    owner = command.Owner;
                    ownerType = "Machine";
                    break;
                case SettingOwnerType.User:
                    query = query.Where(s => s.OwnerType == "User" && s.Owner == command.Owner);
                    owner = command.Owner;
                    ownerType = "User";
                    break;
                default:
                    query = query.Where(s => s.OwnerType == "");
                    owner = "";
                    ownerType = "";
                    break;
            }

            var setting = query.FirstOrDefault();
            if (setting == null)
            {
                setting = new Setting();
                setting.SettingType = command.SettingType;
                setting.SettingName = command.SettingName;
                setting.OwnerType = ownerType;
                setting.Owner = owner;
                systemSettingsContext.Settings.Add(setting);
            }

            setting.SettingValue = command.Value;
            systemSettingsContext.SaveChanges();
        }
    }
}