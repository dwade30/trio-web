﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.SystemSettings.Interfaces;

namespace SharedDataAccess.SystemSettings
{
	public class GetEnvironmentSettingsHandler : CommandHandler<GetEnvironmentSettings, DataEnvironmentSettings>
	{
		private ISystemSettingsContext systemSettingsContext;
		public GetEnvironmentSettingsHandler(ITrioContextFactory contextFactory)
		{
			this.systemSettingsContext = contextFactory.GetSystemSettingsContext();
		}

		protected override DataEnvironmentSettings Handle(GetEnvironmentSettings command)
		{
			try
			{
				var globalVariable = systemSettingsContext.GlobalVariables.FirstOrDefault();
				if (globalVariable != null)
				{
					return new DataEnvironmentSettings
					{
						IsMultiTown = globalVariable.UseMultipleTown.GetValueOrDefault(),
						ClientName = globalVariable.MuniName,
						CityTown = globalVariable.CityTown
					};
				}
				else
				{
					return new DataEnvironmentSettings();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}
