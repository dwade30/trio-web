﻿using Microsoft.EntityFrameworkCore;

namespace SharedDataAccess
{
    public abstract class DataContext<TContext> :DbContext where TContext : DbContext
    {
        public  DataContext(DbContextOptions<TContext> options) : base (options)
        {
        }
       
    }
}
