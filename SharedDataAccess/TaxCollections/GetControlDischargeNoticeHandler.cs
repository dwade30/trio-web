﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class GetControlDischargeNoticeHandler : CommandHandler<GetControlDischargeNotice,ControlDischargeNotice>
    {
        private ITaxCollectionsContext clContext;
        public GetControlDischargeNoticeHandler(ITaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override ControlDischargeNotice Handle(GetControlDischargeNotice command)
        {
            return clContext.ControlDischargeNotices.FirstOrDefault();
        }
    }
}