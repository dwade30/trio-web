﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.AccountPayment;

namespace SharedDataAccess.TaxCollections
{
    public class DeleteCLBatchItemHandler : CommandHandler<DeleteCLBatchItem>
    {
        private TaxCollectionsContext clContext;
        public DeleteCLBatchItemHandler(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override void Handle(DeleteCLBatchItem command)
        {            
            clContext.Database.ExecuteSqlCommand("Delete from BatchRecover where id = " + command.Id);
        }
    }
}