﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class GetControlLienProcessHandler : CommandHandler<GetControlLienProcess,ControlLienProcess>
    {
        private ITaxCollectionsContext clContext;

        public GetControlLienProcessHandler(ITaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override ControlLienProcess Handle(GetControlLienProcess command)
        {
            return clContext.ControlLienProcesses.FirstOrDefault();
        }
    }
}