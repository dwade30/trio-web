﻿using System;
using System.Linq;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.PersonalProperty;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;

namespace SharedDataAccess.TaxCollections
{
    public class PersonalPropertyTaxAccountInfoQueryHandler : IQueryHandler<PersonalPropertyTaxAccountInfoQuery, PersonalPropertyTaxAccountInfo>
    {
        private IPersonalPropertyContext ppContext;
        private ICentralDataContext cdContext;
        private IQueryHandler<TaxAccountCalculationSummaryQuery, TaxBillCalculationSummary> taxCalculationHandler;
        public PersonalPropertyTaxAccountInfoQueryHandler(IPersonalPropertyContext ppContext, ICentralDataContext cdContext, IQueryHandler<TaxAccountCalculationSummaryQuery, TaxBillCalculationSummary> taxCalculationHandler)
        {
            this.ppContext = ppContext;
            this.cdContext = cdContext;
            this.taxCalculationHandler = taxCalculationHandler;
        }
        public PersonalPropertyTaxAccountInfo ExecuteQuery(PersonalPropertyTaxAccountInfoQuery query)
        {
            var accountView = ppContext.PPAccountPartyAddressViews
                .FirstOrDefault(p => p.Account == query.Account);
            if (accountView == null)
            {
                return new PersonalPropertyTaxAccountInfo();
            }

            var reAccount = 0;
            var groupNumber = 0;
            var association = cdContext.ModuleAssociations
                .FirstOrDefault(a => a.Module == "PP" && a.PrimaryAssociate == true && a.Account == query.Account);
            if (association != null)
            {
                var groupMaster = cdContext.GroupMasters.FirstOrDefault(g => g.ID == association.GroupNumber.GetValueOrDefault());
                reAccount = association.REMasterAcct.GetValueOrDefault();
                if (groupMaster != null)
                {
                    groupNumber = groupMaster.GroupNumber.GetValueOrDefault();
                }
            }
            decimal realEstateOwed = 0;
            if (reAccount > 0)
            {
                var calculationSummary = taxCalculationHandler.ExecuteQuery(new TaxAccountCalculationSummaryQuery(reAccount,
                    PropertyTaxBillType.Real, DateTime.Now));
                if (calculationSummary != null)
                {
                    realEstateOwed = calculationSummary.Balance;
                }
            }

            var info = new PersonalPropertyTaxAccountInfo()
            {
                Account = query.Account,
                AssociatedREAccount = reAccount,
                OutstandingREAmount = realEstateOwed,
                LocationStreet = accountView.Street,
                LocationNumber = accountView.StreetNumber.GetValueOrDefault(),
                Name = accountView.FullName(),
                MailingAddress = { Address1 = accountView.Address1, Address2 = accountView.Address2, Address3 = accountView.Address3, City = accountView.City, State = accountView.State, Zip = accountView.Zip },
                Open1 = accountView.Open1,
                Open2 = accountView.Open2,
                AccountGroup = groupNumber
            };
            if (accountView.ExemptCode1.GetValueOrDefault() > 0)
            {
                info.ExemptionCodes.Add(accountView.ExemptCode1.GetValueOrDefault().ToString());
            }

            if (accountView.ExemptCode2.GetValueOrDefault() > 0)
            {
                info.ExemptionCodes.Add(accountView.ExemptCode2.GetValueOrDefault().ToString());
            }

            return info;
        }
    }
}