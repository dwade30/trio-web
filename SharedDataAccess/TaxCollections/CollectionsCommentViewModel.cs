﻿using System;
using System.Linq;
using SharedApplication.TaxCollections.Comment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Comment
{
    public class CollectionsCommentViewModel : ICollectionsCommentViewModel
    {
        public int Account { get; set; }
        public PropertyTaxBillType BillType { get; set; }
        public string Comment { get; set; } = "";

        private TaxCollectionsContext clContext;
        public CollectionsCommentViewModel(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        public void LoadComment()
        {
            var comment = GetComment(BillType == PropertyTaxBillType.Real);
            if (comment != null)
            {
                Comment = comment.Comment;
            }
            else
            {
                Comment = "";
            }
        }

        private CollectionsComment GetComment(bool isRealEstate)
        {
            return clContext.CollectionsComments.FirstOrDefault(c => c.Account == Account && c.IsRealEstate == isRealEstate);
        }
        public void Save()
        {
            var comment = GetComment(BillType == PropertyTaxBillType.Real);
            if (comment == null)
            {
                comment = new CollectionsComment()
                {
                    Account = Account,
                    IsRealEstate = BillType == PropertyTaxBillType.Real,
                    Updated = DateTime.Now
                };
                clContext.CollectionsComments.Add(comment);
            }

            comment.Comment = Comment;
            clContext.SaveChanges();
        }
    }
}