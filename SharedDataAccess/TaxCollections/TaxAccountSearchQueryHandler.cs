﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PersonalProperty;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty.Models;
using SharedApplication.RealEstate.Models;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class TaxAccountSearchQueryHandler : IQueryHandler<TaxAccountSearch,IEnumerable<TaxAccountSearchResult>>
    {
        private ITrioContextFactory trioContextFactory;
        public TaxAccountSearchQueryHandler(ITrioContextFactory trioContextFactory)
        {
            this.trioContextFactory = trioContextFactory;
        }
        public IEnumerable<TaxAccountSearchResult> ExecuteQuery(TaxAccountSearch query)
        {
            switch (query.BillType)
            {
                case PropertyTaxBillType.Personal:
                    return HandlePersonalPropertyQuery(query);
                    break;
                default:
                    return HandleRealEstateQuery(query);
                    break;
            }
        }

        private IEnumerable<TaxAccountSearchResult> HandlePersonalPropertyQuery(TaxAccountSearch query)
        {
            var ppContext = trioContextFactory.GetPersonalPropertyContext();

            var ppAccounts = ppContext.PPAccountPartyViews.AsQueryable();

            string likeString = "";
            switch (query.MatchType)
            {
                case SearchCriteriaMatchType.StartsWith:
                    likeString = query.SearchFor + "%";
                    break;
                case SearchCriteriaMatchType.Contains:
                    likeString = "%" + query.SearchFor + "%";
                    break;
                case SearchCriteriaMatchType.EndsWith:
                    likeString = "%" + query.SearchFor;
                    break;
            }

            if (!query.IncludePreviousOwners)
            {
                if (!query.IncludeDeletedAccounts)
                {
                    ppAccounts = ppAccounts.Where(p => p.Deleted.GetValueOrDefault() != true);
                }
            }

            List<TaxAccountSearchResult> foundAccounts = null;
            switch (query.SearchBy)
            {
                case TaxAccountSearchByOption.Name:
                    ppAccounts = ppAccounts.Where(p => EF.Functions.Like(p.FullNameLastFirst(), likeString)).OrderBy(p => p.FullNameLastFirst());
                    break;
                case TaxAccountSearchByOption.StreetName:
                    ppAccounts = ppAccounts.Where(p => EF.Functions.Like(p.Street, likeString)).OrderBy(p => p.Street).ThenBy(p => p.StreetNumber.GetValueOrDefault());
                    break;
            }
            foundAccounts = ppAccounts.ToTaxAccountSearchResults().ToList();
            if (!query.IncludePreviousOwners)
            {
                return foundAccounts;
            }

            var clContext = trioContextFactory.GetTaxCollectionsContext();
            var bills = clContext.BillingMasters.AsQueryable().Where(b => b.BillingType == "PP");
            var name1List = bills.Where(b => EF.Functions.Like(b.Name1, likeString)).ToTaxAccountSearchResults();
            var name2List = bills.Where(b => EF.Functions.Like(b.Name2, likeString)).ToTaxAccountSearchResultsForSecondName();
            var foundBills = name1List.Union(name2List)
                .GroupBy(b => b.Account, (key, g) => g.OrderByDescending(h => h.Id).FirstOrDefault()).ToList();

            var finalList = foundAccounts.Union(foundBills)
                .GroupBy(a => a.Account, (key, g) => g.OrderBy(h => h.IsPreviousOwner).FirstOrDefault())
                .OrderBy(t => t.Name).ToList();

            if (!query.IncludeDeletedAccounts)
            {
                return finalList.Where(f => !f.IsDeleted);
            }

            return finalList;
        }

        private IEnumerable<TaxAccountSearchResult> HandleRealEstateQuery(TaxAccountSearch query)
        {
            var reContext = trioContextFactory.GetRealEstateContext();
            var reAccounts = reContext.REMasters.AsQueryable().Where(r => r.Rscard == 1);
            
            string likeString = "";
            switch (query.MatchType)
            {
                case SearchCriteriaMatchType.StartsWith:
                    likeString = query.SearchFor + "%";
                    break;
                case SearchCriteriaMatchType.Contains:
                    likeString = "%" + query.SearchFor + "%";
                    break;
                case SearchCriteriaMatchType.EndsWith:
                    likeString = "%" + query.SearchFor;
                    break;
            }

            if (!query.IncludePreviousOwners)
            {
                if (!query.IncludeDeletedAccounts)
                {
                    reAccounts = reAccounts.Where(r => r.Rsdeleted != true);
                }
            }

            List<TaxAccountSearchResult> foundAccounts = null;
            switch (query.SearchBy)
            {
                case TaxAccountSearchByOption.Name:                    
                    var deedName1 = reAccounts.Where(r => EF.Functions.Like(r.DeedName1, likeString)).ToTaxAccountSearchResults();
                    var deedName2 = reAccounts.Where(r => EF.Functions.Like(r.DeedName2, likeString)).ToTaxAccountSearchResultsForSecondName();
                    foundAccounts = deedName1.Union(deedName2).GroupBy(d => d.Account, (key, g) => g.FirstOrDefault()).OrderBy(t => t.Name).ToList();
                    break;
            }
            

            if (!query.IncludePreviousOwners)
            {
                switch (query.SearchBy)
                {
                    case TaxAccountSearchByOption.Name:
                        
                        break;
                    case TaxAccountSearchByOption.MapLot:
                        foundAccounts = reAccounts.Where(r => EF.Functions.Like(r.RsmapLot, likeString)).OrderBy(r => r.RsmapLot).ToTaxAccountSearchResults().ToList();
                        break;
                    case TaxAccountSearchByOption.StreetName:
                        foundAccounts = reAccounts.Where(r => EF.Functions.Like(r.RslocStreet, likeString)).OrderBy(r => r.RslocStreet).ThenBy(r => r.RslocNumAlph).ToTaxAccountSearchResults().ToList();
                        break;
                }
            }

            if (!query.IncludePreviousOwners)
            {
                return foundAccounts;
            }

           var clContext = trioContextFactory.GetTaxCollectionsContext();
           var bills = clContext.BillingMasters.AsQueryable().Where(b => b.BillingType == "RE");
           var name1List = bills.Where(b => EF.Functions.Like(b.Name1, likeString)).ToTaxAccountSearchResults();
           var name2List = bills.Where(b => EF.Functions.Like(b.Name2, likeString)).ToTaxAccountSearchResultsForSecondName();
           var foundBills = name1List.Union(name2List)
               .GroupBy(b => b.Account, (key, g) => g.OrderByDescending(h => h.Id).FirstOrDefault()).ToList();
           var finalList = foundAccounts.Union(foundBills)
               .GroupBy(a => a.Account, (key, g) => g.OrderBy(h => h.IsPreviousOwner).FirstOrDefault())
               .OrderBy(t => t.Name).ToList();
           if (!query.IncludeDeletedAccounts)
           {
               return finalList.Where(f => !f.IsDeleted);
           }

           return finalList;
        }
    }

    public static class PPAccountPartyExtensions
    {
        public static TaxAccountSearchResult ToTaxAccountSearchResult(this PPAccountPartyView ppAccountParty)
        {
            return new TaxAccountSearchResult()
            {
                Id = ppAccountParty.Id,
                Account = ppAccountParty.Account.GetValueOrDefault(),
                AccountType = PropertyTaxBillType.Personal,
                Name = ppAccountParty.FullNameLastFirst(),
                IsDeleted = ppAccountParty.Deleted.GetValueOrDefault(),
                IsPreviousOwner = false,
                MapLot = "",
                StreetName = ppAccountParty.Street,
                StreetNumber = ppAccountParty.StreetNumber.GetValueOrDefault()
            };
        }

        public static IEnumerable<TaxAccountSearchResult> ToTaxAccountSearchResults(
            this IEnumerable<PPAccountPartyView> ppAccountParties)
        {
            return ppAccountParties.Select(p => p.ToTaxAccountSearchResult());
        }
    }
    public static class REMasterExtensions
    {
        public static TaxAccountSearchResult ToTaxAccountSearchResult(this REMaster reMaster)
        {
            return new TaxAccountSearchResult()
            {
                Id =  reMaster.Id,
                Account = reMaster.Rsaccount.GetValueOrDefault(),
                AccountType = PropertyTaxBillType.Real,
                Name = reMaster.DeedName1,
                IsDeleted = reMaster.Rsdeleted.GetValueOrDefault(),
                IsPreviousOwner = false,
                MapLot = reMaster.RsmapLot,
                StreetName = reMaster.RslocStreet,
                StreetNumber = reMaster.RslocNumAlph.ToIntegerValue()
            };
        }

        public static IEnumerable<TaxAccountSearchResult> ToTaxAccountSearchResults(
            this IEnumerable<REMaster> reMasters)
        {
            return reMasters.Select(r => r.ToTaxAccountSearchResult());
        }

        public static TaxAccountSearchResult ToTaxAccountSearchResultForSecondName(this REMaster reMaster)
        {
            return new TaxAccountSearchResult()
            {
                Id = reMaster.Id,
                Account = reMaster.Rsaccount.GetValueOrDefault(),
                AccountType = PropertyTaxBillType.Real,
                Name = reMaster.DeedName2,
                IsDeleted = reMaster.Rsdeleted.GetValueOrDefault(),
                IsPreviousOwner = false,
                MapLot = reMaster.RsmapLot,
                StreetName = reMaster.RslocStreet,
                StreetNumber = reMaster.RslocNumAlph.ToIntegerValue()
            };
        }
        public static IEnumerable<TaxAccountSearchResult> ToTaxAccountSearchResultsForSecondName(
            this IEnumerable<REMaster> reMasters)
        {
            return reMasters.Select(r => r.ToTaxAccountSearchResultForSecondName());
        }
    }

    public static class TaxBillExtensions
    {
        public static TaxAccountSearchResult ToTaxAccountSearchResult(this BillingMaster bill)
        {
            return new TaxAccountSearchResult()
            {
             Id = bill.ID,
             Account  = bill.Account.GetValueOrDefault(),
             AccountType = PropertyTaxBillType.Real,
             Name = bill.Name1,
             StreetName = bill.StreetName,
             StreetNumber = bill.streetnumber.GetValueOrDefault(),
             MapLot = bill.MapLot,
             IsPreviousOwner = true,
             IsDeleted = false
            };
        }

        public static IEnumerable<TaxAccountSearchResult> ToTaxAccountSearchResults(
            this IEnumerable<BillingMaster> bills)
        {
            return bills.Select(b => b.ToTaxAccountSearchResult());
        }

        public static TaxAccountSearchResult ToTaxAccountSearchResultForSecondName(this BillingMaster bill)
        {
            return new TaxAccountSearchResult()
            {
                Id = bill.ID,
                Account = bill.Account.GetValueOrDefault(),
                AccountType = PropertyTaxBillType.Real,
                Name = bill.Name2,
                StreetName = bill.StreetName,
                StreetNumber = bill.streetnumber.GetValueOrDefault(),
                MapLot = bill.MapLot,
                IsPreviousOwner = true,
                IsDeleted = false
            };
        }

        public static IEnumerable<TaxAccountSearchResult> ToTaxAccountSearchResultsForSecondName(
            this IEnumerable<BillingMaster> bills)
        {
            return bills.Select(b => b.ToTaxAccountSearchResultForSecondName());
        }
    }
}