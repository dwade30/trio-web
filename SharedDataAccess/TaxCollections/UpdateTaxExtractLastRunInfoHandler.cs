﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Commands;

namespace SharedDataAccess.TaxCollections
{
    public class UpdateTaxExtractLastRunInfoHandler : CommandHandler<UpdateTaxExtractLastRunInfo>
    {
        private ITrioContextFactory contextFactory;

        public UpdateTaxExtractLastRunInfoHandler(ITrioContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        protected override void Handle(UpdateTaxExtractLastRunInfo command)
        {
            var collectionsContext = contextFactory.GetTaxCollectionsContext();
            var settings = collectionsContext.CollectionSettings.FirstOrDefault();

            if (settings.TSLastRunYear < command.BillYear * 10 + command.Period)
            {
                settings.TSLastRunYear = command.BillYear * 10 + command.Period;
                settings.TSRemindNextDate = null;
                settings.TSRemindLastDate = DateTime.Today;
                collectionsContext.SaveChanges();
            }
        }
    }
}
