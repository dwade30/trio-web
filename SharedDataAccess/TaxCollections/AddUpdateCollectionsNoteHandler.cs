﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Notes;

namespace SharedDataAccess.TaxCollections
{
    public class AddUpdateCollectionsNoteHandler : CommandHandler<AddUpdateCollectionsNote,int>
    {
        private TaxCollectionsContext clContext;
        public AddUpdateCollectionsNoteHandler(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override int Handle(AddUpdateCollectionsNote command)
        {
            Note note = null;
            switch (command.BillType)
            {
                case PropertyTaxBillType.Personal:
                    note = clContext.Notes.FirstOrDefault(n =>
                        n.Account == command.Account && n.AccountTypeAbbreviation == "PP");
                    break;
                case PropertyTaxBillType.Real:
                    note = clContext.Notes.FirstOrDefault(n =>
                        n.Account == command.Account && n.AccountTypeAbbreviation == "RE");
                    break;
            }

            if (note == null)
            {
                note = new Note()
                {
                    Account = command.Account,
                    BillType = command.BillType
                };
                clContext.Notes.Add(note);
            }

            note.Comment = command.Comment;
            note.Priority = command.Priority;
            note.ShowInRealEstate = command.ShowInRealEstate;
            clContext.SaveChanges();
            return note.Id;
        }
    }
}