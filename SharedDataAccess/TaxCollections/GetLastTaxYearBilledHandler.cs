﻿using System.Linq;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;

namespace SharedDataAccess.TaxCollections
{
    public class GetLastTaxYearBilledHandler : CommandHandler<GetLastTaxYearBilled,int>
    {
        private ITaxCollectionsContext clContext;
        public GetLastTaxYearBilledHandler(ITaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override int Handle(GetLastTaxYearBilled command)
        {
            var taxYear = clContext.BillingMasters.Where(b => b.RateRecId > 0).OrderByDescending(b => b.BillingYear).Select(b => b.BillingYear).FirstOrDefault().GetValueOrDefault();
            if (taxYear > 0)
            {
                return taxYear.ToString().Left(4).ToIntegerValue();
            }

            return 0;
        }
    }
}