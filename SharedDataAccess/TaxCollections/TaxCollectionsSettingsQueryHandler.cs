﻿using System.Linq;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Queries;

namespace SharedDataAccess.TaxCollections
{
    public class TaxCollectionsSettingsQueryHandler : IQueryHandler<TaxCollectionsSettingsQuery, GlobalTaxCollectionSettings>
    {
        private ITaxCollectionsContext taxCollectionsContext;

        public TaxCollectionsSettingsQueryHandler(ITaxCollectionsContext taxCollectionsContext)
        {
            this.taxCollectionsContext = taxCollectionsContext;
        }
        public GlobalTaxCollectionSettings ExecuteQuery(TaxCollectionsSettingsQuery query)
        {
            var collectionsSettings = taxCollectionsContext.CollectionSettings.FirstOrDefault();
            var globalTaxCollectionSettings = new GlobalTaxCollectionSettings();
            if (collectionsSettings != null)
            {
                globalTaxCollectionSettings.DaysInAYear = collectionsSettings.DaysInYear.GetValueOrDefault();
                globalTaxCollectionSettings.DiscountRate =
                    collectionsSettings.DiscountPercent.GetValueOrDefault().ToDecimal();
                globalTaxCollectionSettings.OverPaymentInterestRate =
                    collectionsSettings.OverPayInterestRate.GetValueOrDefault().ToDecimal();
                globalTaxCollectionSettings.DisableAutoPaymentFill =
                    collectionsSettings.DisableAutoPmtFill.GetValueOrDefault();
                globalTaxCollectionSettings.ShowLastAccountInCR =
                    collectionsSettings.ShowLastCLAccountInCR.GetValueOrDefault();
                globalTaxCollectionSettings.AccountDetailAssessmentSetting = collectionsSettings.AcctDetailAssesment;
                globalTaxCollectionSettings.ShowReceiptNumberOnDetail =
                    collectionsSettings.ReceiptNumonAcctDetail.GetValueOrDefault();
                globalTaxCollectionSettings.AutoChangeRecordedTransactionDateWithEffectiveDate =
                    collectionsSettings.DefaultRTDToAutoChange.GetValueOrDefault();
            }

            return globalTaxCollectionSettings;
        }
    }
}