﻿using System;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class AddLienDischargeNoticeHandler : CommandHandler<AddLienDischargeNotice>
    {
        private TaxCollectionsContext clContext;
        public AddLienDischargeNoticeHandler(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override void Handle(AddLienDischargeNotice command)
        {
            var discharge = clContext.NeededDischarges.FirstOrDefault(d => d.LienId == command.LienId);
            if (discharge == null)
            {
                discharge = new DischargeNeeded()
                {
                    Batch =  false,
                    BillId = command.BillId,
                    Book = "",
                    Cancelled =  false,
                    DatePaid = command.DatePaid,
                    LienId = command.LienId,
                    Page = "",
                    Printed = false,
                    Teller = command.Teller,
                    UpdatedDate = DateTime.Now
                };
                clContext.Add(discharge);
                clContext.SaveChanges();
            }

        }
    }
}