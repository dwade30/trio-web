﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.CentralParties;
using SharedApplication.CentralParties.Models;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Domain;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.Telemetry;

namespace SharedDataAccess.TaxCollections
{
    public class FinalizePropertyTaxTransactionsHandler : CommandHandler<FinalizeTransactions<PropertyTaxTransaction>,bool>
    {
        private TaxCollectionsContext collectionsContext;
        private IPropertyTaxBillQueryHandler billQueryHandler;
        private IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo> reBriefHandler;
        private IQueryHandler<PersonalPropertyAccountBriefQuery, PersonalPropertyAccountBriefInfo> ppBriefHandler;
        private ICentralPartyContext partyContext;
        private CommandDispatcher commandDispatcher;
        private DataEnvironmentSettings envSettings;
        private ITelemetryService telemetryService;

        public FinalizePropertyTaxTransactionsHandler(TaxCollectionsContext collectionsContext, IPropertyTaxBillQueryHandler billQueryHandler, IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo> reBriefHandler, IQueryHandler<PersonalPropertyAccountBriefQuery, PersonalPropertyAccountBriefInfo> ppBriefHandler, ICentralPartyContext partyContext, CommandDispatcher commandDispatcher,DataEnvironmentSettings envSettings, ITelemetryService telemetryService)
        {
            this.collectionsContext = collectionsContext;
            this.billQueryHandler = billQueryHandler;
            this.reBriefHandler = reBriefHandler;
            this.ppBriefHandler = ppBriefHandler;
            this.partyContext = partyContext;
            this.commandDispatcher = commandDispatcher;
            this.envSettings = envSettings;
            this.telemetryService = telemetryService;
        }
        protected override bool Handle(FinalizeTransactions<PropertyTaxTransaction> command)
        {
            try
            {
                
                var dischargeBills = new List<(RealEstateTaxBill reBill, DateTime recordedDate)>();
                var partialPayments = new List<(RealEstateTaxBill reBill, PropertyTaxLienPartiallyPaid partialEvent)>();
                var savedBills = new Dictionary<int, PropertyTaxBill>();
                foreach (PropertyTaxTransaction transaction in command.Transactions)
                {
                    var payments = transaction.Payments.GetAll();
                    var billId = transaction.BillId;                    
                    if (transaction.BillId == 0)
                    {
                        //var summary = transaction.Payments.GetPaymentsSummary();
                        billId = CreateBill(transaction);                        
                    }

                    PropertyTaxBill taxBill;
                    if (savedBills.ContainsKey(billId))
                    {
                        savedBills.TryGetValue(billId, out taxBill);
                    }
                    else
                    {
                        taxBill = billQueryHandler.Get(billId);
                        savedBills.Add(billId,taxBill);
                    }
                    

                    foreach (var payment in payments)
                    {
                        payment.ReceiptNumber = command.ReceiptId;
                        if (string.IsNullOrWhiteSpace(payment.Teller))
                        {
                            payment.Teller = command.TellerId;
                        }

                        if (payment.BillId == 0)
                        {
                            payment.BillId = billId;
                        }

                        payment.OldInterestDate = payment.OldInterestDate == DateTime.MinValue
                            ? null
                            : payment.OldInterestDate;

                        collectionsContext.PaymentRecords.Add(payment);
                    }

                    if (taxBill.HasLien())
                    {
                        var abatements = taxBill.Lien.Payments.GetAll().Abatements().ToList();
                        decimal abatedPrincipal = 0;
                        decimal abatedInterest = 0;
                        decimal abatedCosts = 0;
                        decimal abatedPreLienInterest = 0;
                        var currentAbatements = transaction.Payments.GetAll().Abatements();
                        if (currentAbatements.Any())
                        {
                            abatements.AddRange(currentAbatements);
                        }

                        if (abatements.Any())
                        {
                            abatedPrincipal = abatements.Sum(a => a.Principal)??0;
                            abatedCosts = abatements.Sum(a => a.LienCost) ?? 0;
                            abatedInterest = abatements.Sum(a => a.CurrentInterest) ?? 0;
                            abatedPreLienInterest = abatements.Sum(a => a.PreLienInterest) ?? 0;
                        }
                        taxBill.Lien.ApplyPayments(transaction.Payments);
                        var summary = taxBill.Lien.LienSummary;
                        var lien = collectionsContext.LienRecords.FirstOrDefault(l => l.Id == taxBill.LienID);
                        lien.PreviousInterestAppliedDate = lien.InterestAppliedThroughDate;
                        lien.InterestAppliedThroughDate = taxBill.Lien.InterestAppliedThroughDate == DateTime.MinValue
                            ? null
                            : taxBill.Lien.InterestAppliedThroughDate;
                        lien.InterestCharged = (summary.OriginalInterestCharged * -1);// - abatedInterest;
                        lien.MaturityFee = summary.MaturityFee * -1;
                        lien.PrincipalPaid = summary.PrincipalPaid + abatedPrincipal;
                        lien.InterestPaid = summary.InterestPaid + abatedInterest;
                        lien.Plipaid = summary.PreLienInterestPaid + abatedPreLienInterest;
                        lien.CostsPaid = summary.CostsPaid + abatedCosts;
                        var transactionEvents = transaction.NewEvents.GetEvents()
                            .Where(ev => ev.EventType == TaxBillEventType.PropertyTaxLienPaid).ToList();
                        var wasPaidOff = false;
                        foreach (var lienEvent in transactionEvents)
                        {
                            wasPaidOff = true;
                            dischargeBills.Add(( reBill: (RealEstateTaxBill)taxBill, recordedDate: transaction.EffectiveDateTime));
                        }

                        if (!wasPaidOff)
                        {
                            var partialEvents = transaction.NewEvents.GetEvents().Where(ev =>
                                ev.EventType == TaxBillEventType.PropertyTaxLienPartiallyPaid).ToList();
                            foreach (var partialEvent in partialEvents)
                            {
                                partialPayments.Add((reBill: (RealEstateTaxBill) taxBill,
                                    partialEvent: (PropertyTaxLienPartiallyPaid) partialEvent));
                            }
                        }
                    }
                    else
                    {
                        var abatements = taxBill.Payments.GetAll().Abatements();
                        decimal abatedPrincipal = 0;
                        decimal abatedInterest = 0;
                        decimal abatedFees = 0;
                        if (abatements.Any())
                        {
                            abatedPrincipal = abatements.Sum(a => a.Principal) ?? 0;
                            abatedFees = abatements.Sum(a => a.LienCost) ?? 0;
                            abatedInterest = abatements.Sum(a => a.CurrentInterest) ?? 0;
                        }
                        taxBill.ApplyPayments(transaction.Payments);                        
                        var bill = collectionsContext.BillingMasters.FirstOrDefault(b => b.ID == taxBill.ID);
                        bill.PreviousInterestAppliedDate = bill.InterestAppliedThroughDate;
                        bill.InterestAppliedThroughDate = taxBill.InterestAppliedThroughDate == DateTime.MinValue
                            ? null
                            : taxBill.InterestAppliedThroughDate;
                        bill.PrincipalPaid = taxBill.BillSummary.PrincipalPaid + abatedPrincipal;

                        bill.InterestPaid = taxBill.BillSummary.InterestPaid + abatedInterest;// * -1;
                        bill.InterestCharged = taxBill.BillSummary.InterestCharged * -1;
                        bill.DemandFees = taxBill.BillSummary.DemandFeesCharged;
                        bill.DemandFeesPaid = taxBill.BillSummary.DemandFeesPaid + abatedFees;
                    }
                    
                }

                collectionsContext.SaveChanges();
                foreach (var billTuple in dischargeBills)
                {
                    var bill = billTuple.reBill;
                    var recordedDate = billTuple.recordedDate;
                    commandDispatcher.Send(new AddLienDischargeNotice(bill.LienID, recordedDate,command.TellerId , bill.ID));
                    if (command.AllowUIInteraction)
                    {
                        if (commandDispatcher
                            .Send(new GetYesOrNoResponse(
                                "Would you like to print the Lien Discharge Notice for " +
                                bill.TaxYear + "-" + bill.BillNumber + "?", "Lien Discharge Notice", false))
                            .Result)
                        {
                            commandDispatcher.Send(new ShowLienDischargeNotice(DateTime.Today, bill));
                        }
                    }
                }

                foreach (var billTuple in partialPayments)
                {
                    var bill = billTuple.reBill;
                    var partialEvent = billTuple.partialEvent;
                    commandDispatcher.Send(new AddPartialPaymentWaiver(bill.LienID, bill.TaxYear, bill.Account.GetValueOrDefault(),
                        partialEvent.PaymentAmount, partialEvent.DatePaid, 0));
                    if (commandDispatcher
                        .Send(new GetYesOrNoResponse("Would you like to print the Lien Partial Payment waiver now?",
                            "Partial Payment Waiver", false)).Result)
                    {
                        commandDispatcher.Send(new ShowPartialPaymentWaiver(envSettings.ClientName,"MAINE", bill.OwnerNames(), partialEvent.DatePaid,
                            partialEvent.PaymentAmount, "", DateTime.Today, bill.MapLot,bill.LienID,false,bill.Account.GetValueOrDefault(),bill.TaxYear));
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                telemetryService.TrackException(ex);
                return false;
            }
            
        }

        private int CreateBill(PropertyTaxTransaction transaction)
        {
            var bill = new BillingMaster()
            {
                Account = transaction.Account,
                AbatementPaymentMade = false,
                BillingType = transaction.BillType == PropertyTaxBillType.Personal ? "PP" : "RE",
                BillingYear = transaction.YearBill,
                CertifiedMailNumber = "",
                Copies = 0,
                DemandFees = 0,
                DemandFeesPaid = 0,
                IMPBTrackingNumber =  "",
                InterestCharged = 0,
                LienProcessExclusion = false,
                InterestPaid = 0,
                LienRecordNumber = 0,
                LienProcessStatus = 0,
                LienStatusEligibility = 0,
                MapLot = "",
                TaxDue1 = 0,
                TaxDue2 = 0,
                TaxDue3 = 0,
                TaxDue4 = 0,
                RateRecId = 0,
                InterestAppliedThroughDate = new DateTime(1899,12,30),
                TransferFromBillingDateFirst = new DateTime(1899,12,30),
                TransferFromBillingDateLast = new DateTime(1899,12,30),
                WhetherBilledBefore = "",
                Category1 = 0,
                Category2 = 0,
                Category3 = 0,
                Category4 = 0,
                Category5 = 0,
                Category6 = 0,
                Category7 = 0,
                Category8 = 0,
                Category9 = 0,
                Acres = 0,
                BuildingValue = 0,
                BuildingCode = 0,
                ExemptValue = 0,
                Exempt3 = 0,
                Exempt1 = 0,
                Exempt2 = 0,
                HomesteadExemption = 0,
                LandCode = 0,
                LandValue = 0,
                OtherExempt1 = 0,
                OtherExempt2 = 0,
                OtherCode1 = 0,
                OtherCode2 = 0,
                PPAssessment = 0,
                PreviousInterestAppliedDate = new DateTime(1899,12,30),
                TGHardAcres = 0,
                TGHardValue = 0,
                TGMixedAcres = 0,
                TGMixedValue = 0,
                TGSoftAcres = 0,
                TGSoftValue = 0,
                PrincipalPaid = 0,
                BookPage = "",
                OwnerGroup = 0,
                Ref1 = "",
                Ref2 = "",
                ShowRef1 = false,
                TranCode = transaction.TownId,
                Address1 =  "",
                Address2 = "",
                Address3 = "",
                Apt = "",
                Zip = "",
                MailingAddress3 = ""
            };
            if (transaction.BillType == PropertyTaxBillType.Personal)
            {
                var ppBrief = GetPPAccountInfo(transaction.Account);
                bill.Name1 = ppBrief.Name;
                bill.Name2 = "";
                bill.streetnumber = ppBrief.LocationNumber;
                bill.StreetName = ppBrief.LocationStreet;
                var party = GetParty(ppBrief.PartyId);
                var address = party?.PrimaryAddress();
                if (address != null)
                {
                    bill.Address1 = address.Address1;
                    bill.Address2 = address.Address2;
                    bill.MailingAddress3 = address.Address3;
                    bill.Address3 = address.City + " " + address.State + " " + address.Zip;
                    bill.Zip = address.Zip;
                }
            }
            else
            {
                var reBrief = GetREAccountInfo(transaction.Account);
                bill.Name1 = reBrief.DeedName1;
                bill.Name2 = reBrief.DeedName2;
                bill.MapLot = reBrief.MapLot;
                bill.streetnumber = reBrief.LocationNumber;
                bill.StreetName = reBrief.LocationStreet;
                var party = GetParty(reBrief.PartyId1);
                var address = party?.PrimaryAddress();
                if (address != null)
                {
                    bill.Address1 = address.Address1;
                    bill.Address2 = address.Address2;
                    bill.MailingAddress3 = address.Address3;
                    bill.Address3 = address.City + " " + address.State + " " + address.Zip;
                    bill.Zip = address.Zip;
                }
            }

            collectionsContext.BillingMasters.Add(bill);
            collectionsContext.SaveChanges();
            return bill.ID;
        }

        private RealEstateAccountBriefInfo GetREAccountInfo(int account)
        {
            return reBriefHandler.ExecuteQuery(new RealEstateAccountBriefQuery() { Account = account});
        }

        private PersonalPropertyAccountBriefInfo GetPPAccountInfo(int account)
        {
            return ppBriefHandler.ExecuteQuery(new PersonalPropertyAccountBriefQuery(account));
        }

        private CentralParty GetParty(int id)
        {
            return partyContext.Parties.Where(p => p.Id == id).Include(p => p.Addresses).FirstOrDefault();
        }
    }
}