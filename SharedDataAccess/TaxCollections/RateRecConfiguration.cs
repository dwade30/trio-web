﻿using SharedApplication.TaxCollections.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SharedDataAccess.TaxCollections
{
    public class RateRecConfiguration : IEntityTypeConfiguration<RateRec>
    {
        public void Configure(EntityTypeBuilder<RateRec> builder)
        {
            builder.ToTable("RateRec");
            builder.Property(p => p.Id).HasColumnName("ID");
            builder.Property(p => p.RateType).HasMaxLength(1);
            builder.Property(p => p.ThirtyDayNoticeDate).HasColumnName("30DNDate");

        }
    }
}
