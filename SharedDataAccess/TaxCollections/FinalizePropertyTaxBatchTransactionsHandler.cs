﻿using System;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.Telemetry;

namespace SharedDataAccess.TaxCollections
{
    public class FinalizePropertyTaxBatchTransactionsHandler : CommandHandler<FinalizeTransactions<PropertyTaxBatchTransaction>,bool>
    {
        private TaxCollectionsContext collectionsContext;
        private IPropertyTaxBillQueryHandler billQueryHandler;
        private ITelemetryService telemetryService;

        public FinalizePropertyTaxBatchTransactionsHandler(TaxCollectionsContext collectionsContext, IPropertyTaxBillQueryHandler billQueryHandler, ITelemetryService telemetryService)
        {
            this.collectionsContext = collectionsContext;
            this.billQueryHandler = billQueryHandler;
            this.telemetryService = telemetryService;
        }
        protected override bool Handle(FinalizeTransactions<PropertyTaxBatchTransaction> command)
        {
            try
            {
                foreach (var transaction in command.Transactions)
                {
                    foreach (var batchItem in transaction.BatchItems)
                    {
                        var payments = batchItem.Payments.GetAll();
                        var billId = batchItem.BillId;
                        var taxBill = billQueryHandler.Get(billId);
                        var batchRecover =
                            collectionsContext.BatchRecovers.FirstOrDefault(b => b.Id == batchItem.BatchItemId);
                        if (batchRecover != null)
                        {
                            batchRecover.Processed = true;
                        }
                        foreach (var payment in payments)
                        {
                            if (payment.BillId == 0)
                            {
                                payment.BillId = billId;
                            }

                            if (payment.IsCreatedInterest() && (payment.OldInterestDate == null || payment.OldInterestDate == DateTime.MinValue))
                            {
                                if (taxBill.HasLien())
                                {
                                    payment.OldInterestDate =
                                        taxBill.Lien.InterestAppliedThroughDate == DateTime.MinValue
                                            ? null
                                            : taxBill.Lien.InterestAppliedThroughDate;
                                }
                                else
                                {
                                    payment.OldInterestDate = taxBill.InterestAppliedThroughDate == DateTime.MinValue
                                        ? null
                                        : taxBill.InterestAppliedThroughDate;
                                }
                            }

                           collectionsContext.PaymentRecords.Add(payment);
                            
                        }
                        if (taxBill.HasLien())
                        {
                            var abatements = taxBill.Lien.Payments.GetAll().Abatements().ToList();
                            decimal abatedPrincipal = 0;
                            decimal abatedInterest = 0;
                            decimal abatedCosts = 0;
                            decimal abatedPreLienInterest = 0;
                            var currentAbatements = batchItem.Payments.GetAll().Abatements();
                            if (currentAbatements.Any())
                            {
                                abatements.AddRange(currentAbatements);
                            }
                            if (abatements.Any())
                            {
                                abatedPrincipal = abatements.Sum(a => a.Principal) ?? 0;
                                abatedCosts = abatements.Sum(a => a.LienCost) ?? 0;
                                abatedInterest = abatements.Sum(a => a.CurrentInterest) ?? 0;
                                abatedPreLienInterest = abatements.Sum(a => a.PreLienInterest) ?? 0;
                            }
                            taxBill.Lien.ApplyPayments(batchItem.Payments);
                            var summary = taxBill.Lien.LienSummary;
                            var lien = collectionsContext.LienRecords.FirstOrDefault(l => l.Id == taxBill.LienID);
                            lien.PreviousInterestAppliedDate = lien.InterestAppliedThroughDate;
                            lien.InterestAppliedThroughDate = taxBill.Lien.InterestAppliedThroughDate;
                            lien.InterestCharged = (summary.OriginalInterestCharged * -1);//- abatedInterest;
                            lien.MaturityFee = summary.MaturityFee * -1;
                            lien.PrincipalPaid = summary.PrincipalPaid + abatedPrincipal;
                            lien.InterestPaid = summary.InterestPaid + abatedInterest;
                            lien.Plipaid = summary.PreLienInterestPaid + abatedPreLienInterest;
                            lien.CostsPaid = summary.CostsPaid + abatedCosts;
                        }
                        else
                        {
                            taxBill.ApplyPayments(batchItem.Payments);
                            var bill = collectionsContext.BillingMasters.FirstOrDefault(b => b.ID == taxBill.ID);
                            bill.PreviousInterestAppliedDate = bill.InterestAppliedThroughDate;
                            bill.InterestAppliedThroughDate = taxBill.InterestAppliedThroughDate;
                            bill.PrincipalPaid = taxBill.BillSummary.PrincipalPaid;
                            bill.InterestPaid = taxBill.BillSummary.InterestPaid * -1;
                            bill.InterestCharged = taxBill.BillSummary.InterestCharged;
                            bill.DemandFees = taxBill.BillSummary.DemandFeesCharged;
                            bill.DemandFeesPaid = taxBill.BillSummary.DemandFeesPaid;
                        }
                    }
                }

                collectionsContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                telemetryService.TrackException(ex);
                return false;
            }
        }
    }
}