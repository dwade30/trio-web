﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class GetTaxCollectionsCommentHandler : CommandHandler<GetTaxCollectionsComment,string>
    {
        private ITaxCollectionsContext clContext;
        public GetTaxCollectionsCommentHandler(ITaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override string Handle(GetTaxCollectionsComment command)
        {
            CollectionsComment comment;
            if (command.BillType == PropertyTaxBillType.Real)
            {
                comment = clContext.CollectionsComments.FirstOrDefault(
                    c => c.Account == command.Account && c.IsRealEstate == true);
            }
            else
            {
                comment = clContext.CollectionsComments.FirstOrDefault(
                    c => c.Account == command.Account && c.IsRealEstate == false);
            }

            if (comment != null)
            {
                return comment.Comment;
            }

            return "";
        }
    }
}