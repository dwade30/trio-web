﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class PropertyTaxBillQuerier
    {
        protected IEnumerable<TaxRateInfo> rateRecords;
        protected ITrioContextInterfaceFactory contextFactory;
        protected IEnumerable<TaxRateInfo> RateRecords
        {
            get
            {
                if (rateRecords == null)
                {
                    var context = contextFactory.GetTaxCollectionsContextInterface();
                    rateRecords = context.RateRecords.AsNoTracking().ToList().MapToTaxRateInfos();
                    //rateRecords = collectionsRepoFactory.CreateRepository<RateRec>().GetAll().MapToTaxRateInfos();
                }

                return rateRecords;
            }
        }

        protected PropertyTaxBill MakePropertyTaxBill(BillingMaster bill)
        {
            PropertyTaxBill propertyTaxBill;
            if (bill.BillingType.ToLower() != "pp")
            {
                propertyTaxBill = MakeRealEstateBill(bill);
            }
            else
            {
                propertyTaxBill = MakePersonalPropertyBill(bill);
            }

            propertyTaxBill.Account = bill.Account;
            propertyTaxBill.BillNumber = bill.BillNumber();
            propertyTaxBill.ID = bill.ID;
            propertyTaxBill.LienID = bill.LienRecordNumber ?? 0;
            propertyTaxBill.InterestAppliedThroughDate = bill.InterestAppliedThroughDate ?? DateTime.MinValue;
            propertyTaxBill.PreviousInterestAppliedDate = bill.PreviousInterestAppliedDate ?? DateTime.MinValue;
            propertyTaxBill.OriginalInterestAppliedThroughDate = propertyTaxBill.InterestAppliedThroughDate;
            propertyTaxBill.OriginalPreviousInterestDate = propertyTaxBill.PreviousInterestAppliedDate;
            propertyTaxBill.RateId = bill.RateRecId ?? 0;
            propertyTaxBill.TaxYear = bill.TaxYear();
            propertyTaxBill.MailingAddress1 = bill.Address1;
            propertyTaxBill.MailingAddress2 = bill.Address2;
            propertyTaxBill.CityStateZip = bill.Address3;
            propertyTaxBill.MailingAddress3 = bill.MailingAddress3;
            propertyTaxBill.Name1 = bill.Name1;
            propertyTaxBill.Name2 = bill.Name2;
            propertyTaxBill.StreetName = bill.StreetName;
            propertyTaxBill.StreetNumber = bill.streetnumber.GetValueOrDefault();
            propertyTaxBill.RateRecord = RateRecords.FirstOrDefault(r => r.id == propertyTaxBill.RateId);
            propertyTaxBill.TranCode = bill.TranCode.GetValueOrDefault();

            if (propertyTaxBill.RateRecord != null)
            {
                if (propertyTaxBill.RateRecord.Installments.Count > 0)
                {
                    var installment = propertyTaxBill.RateRecord.Installments.Where(i => i.InstallmentNumber == 1);
                    propertyTaxBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment() { OriginalAmount = bill.TaxDue1.GetValueOrDefault(), AdjustedAmount = bill.TaxDue1.GetValueOrDefault(), DueDate = propertyTaxBill.RateRecord.Installments[0].DueDate, InstallmentNumber = 1, InterestDate = propertyTaxBill.RateRecord.Installments[0].InterestStartDate });
                    if (propertyTaxBill.RateRecord.Installments.Count > 1)
                    {
                        propertyTaxBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment() { OriginalAmount = bill.TaxDue2.GetValueOrDefault(), AdjustedAmount = bill.TaxDue2.GetValueOrDefault(), DueDate = propertyTaxBill.RateRecord.Installments[1].DueDate, InstallmentNumber = 2, InterestDate = propertyTaxBill.RateRecord.Installments[1].InterestStartDate });
                        if (propertyTaxBill.RateRecord.Installments.Count > 2)
                        {
                            propertyTaxBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment() { OriginalAmount = bill.TaxDue3.GetValueOrDefault(), AdjustedAmount = bill.TaxDue3.GetValueOrDefault(), DueDate = propertyTaxBill.RateRecord.Installments[2].DueDate, InstallmentNumber = 3, InterestDate = propertyTaxBill.RateRecord.Installments[2].InterestStartDate });
                            if (propertyTaxBill.RateRecord.Installments.Count > 3)
                            {
                                propertyTaxBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment() { OriginalAmount = bill.TaxDue4.GetValueOrDefault(), AdjustedAmount = bill.TaxDue4.GetValueOrDefault(), DueDate = propertyTaxBill.RateRecord.Installments[3].DueDate, InstallmentNumber = 4, InterestDate = propertyTaxBill.RateRecord.Installments[3].InterestStartDate });
                            }
                        }
                    }
                }
            }

            if (propertyTaxBill.LienID > 0)
            {
                propertyTaxBill.Lien = GetLien(propertyTaxBill.LienID, propertyTaxBill.Account.GetValueOrDefault(), propertyTaxBill.ID);
            }

            propertyTaxBill.TaxClub = GetTaxClub(bill.ID);
            var payments = GetBillPayments(propertyTaxBill.ID);
            var nonAbatements = payments.GetAll().WithoutAbatements();
            propertyTaxBill.BillSummary.InterestCharged = nonAbatements.InterestChargedPayments().TotalChargedInterest();

            propertyTaxBill.BillSummary.DemandFeesCharged = nonAbatements.TotalCostsCharged();
            propertyTaxBill.BillSummary.DemandFeesPaid = nonAbatements.TotalCostsPaid();

            propertyTaxBill.BillSummary.PrincipalPaid = nonAbatements.TotalPrincipalPaid();
            propertyTaxBill.BillSummary.InterestPaid = nonAbatements.TotalInterestPaid();
            propertyTaxBill.AddPayments(payments);
            return propertyTaxBill;

           

        }

        protected RealEstateTaxBill MakeRealEstateBill(BillingMaster bill)
        {
            var taxBill = new RealEstateTaxBill()
            {
                BillSummary = { PrincipalPaid = bill.PrincipalPaid ?? 0,InterestPaid = bill.InterestPaid ?? 0,
                    PrincipalCharged = (bill.TaxDue1 ?? 0) + (bill.TaxDue2 ?? 0) + (bill.TaxDue3 ?? 0) + (bill.TaxDue4 ?? 0),
                    InterestCharged = ( bill.InterestCharged ?? 0) * -1, DemandFeesCharged = bill.DemandFees ?? 0, DemandFeesPaid = bill.DemandFeesPaid ?? 0}
            }; //bill summary will be replaced with info from payment recs
            taxBill.Acreage = bill.Acres.GetValueOrDefault().ToDecimal();
            taxBill.BookPage = bill.BookPage;
            taxBill.BuildingValue = bill.BuildingValue.GetValueOrDefault();
            taxBill.LandValue = bill.LandValue.GetValueOrDefault();
            taxBill.ExemptValue = bill.ExemptValue.GetValueOrDefault();
            taxBill.ExemptCode1 = bill.Exempt1.GetValueOrDefault();
            taxBill.ExemptCode2 = bill.Exempt2.GetValueOrDefault();
            taxBill.ExemptCode3 = bill.Exempt3.GetValueOrDefault();
            taxBill.HomesteadExemption = bill.HomesteadExemption.GetValueOrDefault().ToInteger();
            taxBill.MapLot = bill.MapLot;
            taxBill.Reference1 = bill.Ref1;
            taxBill.Reference2 = bill.Ref2;
            return taxBill;
        }

        protected PersonalPropertyTaxBill MakePersonalPropertyBill(BillingMaster bill)
        {
            var taxBill = new PersonalPropertyTaxBill()
            {
                BillSummary = { PrincipalPaid = bill.PrincipalPaid ?? 0,InterestPaid = bill.InterestPaid ?? 0,
                    PrincipalCharged = (bill.TaxDue1 ?? 0) + (bill.TaxDue2 ?? 0) + (bill.TaxDue3 ?? 0) + (bill.TaxDue4 ?? 0),
                    InterestCharged =  (bill.InterestCharged ?? 0) * -1, DemandFeesCharged = bill.DemandFees ?? 0, DemandFeesPaid = bill.DemandFeesPaid ?? 0}
            };
            taxBill.OtherCode1 = bill.OtherCode1.GetValueOrDefault();
            taxBill.OtherCode2 = bill.OtherCode2.GetValueOrDefault();
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category1.GetValueOrDefault(), CategoryNumber = 1 });
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category2.GetValueOrDefault(), CategoryNumber = 2 });
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category3.GetValueOrDefault(), CategoryNumber = 3 });
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category4.GetValueOrDefault(), CategoryNumber = 4 });
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category5.GetValueOrDefault(), CategoryNumber = 5 });
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category6.GetValueOrDefault(), CategoryNumber = 6 });
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category7.GetValueOrDefault(), CategoryNumber = 7 });
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category8.GetValueOrDefault(), CategoryNumber = 8 });
            taxBill.PersonalPropertyValuations.AddValuation(new PersonalPropertyValuation() { Amount = bill.Category9.GetValueOrDefault(), CategoryNumber = 9 });
            return taxBill;
        }

        protected PropertyTaxLien GetLien(int lienId, int account, int billId)
        {
            //var lienRepo = collectionsRepoFactory.CreateRepository<LienRec>();
            //var lien = lienRepo.Get(lienId);
            var clContext = contextFactory.GetTaxCollectionsContextInterface();
            var lien = clContext.LienRecords.FirstOrDefault(l => l.Id == lienId);
            if (lien == null)
            {
                return null;
            }
            var propertyTaxLien = new PropertyTaxLien()
            {
                LienSummary =
                {
                   // PrincipalPaid = lien.PrincipalPaid.GetValueOrDefault(),
                    //PrincipalCharged = lien.Principal.GetValueOrDefault(),
                    OriginalPrincipalCharged = lien.Principal.GetValueOrDefault(),
                    AdjustedPrincipalCharged = lien.Principal.GetValueOrDefault(),
                    OriginalCostsCharged = lien.Costs.GetValueOrDefault(),
                    AdjustedCostsCharged = lien.Costs.GetValueOrDefault(),
                    //CostsPaid = lien.CostsPaid.GetValueOrDefault(),
                    //InterestCharged = lien.InterestCharged.GetValueOrDefault() * -1,
                    //InterestPaid = lien.InterestPaid.GetValueOrDefault(),
                   // MaturityFee = lien.MaturityFee.GetValueOrDefault() * -1,
                    //PreLienInterestPaid = lien.Plipaid.GetValueOrDefault(),
                    OriginalPreLienInterestCharged = lien.Interest.GetValueOrDefault(),
                    AdjustedPreLienInterestCharged = lien.Interest.GetValueOrDefault()
                },
                InterestAppliedThroughDate = lien.InterestAppliedThroughDate,
                PreviousInterestAppliedDate = lien.PreviousInterestAppliedDate,
                OriginalInterestAppliedThroughDate = lien.InterestAppliedThroughDate,
                OriginalPreviousInterestDate = lien.PreviousInterestAppliedDate,
                RateId = lien.RateKey,
                Id = lien.Id,
                Status = lien.Status,
                Book = lien.Book,
                DateCreated = lien.DateCreated,
                Page = lien.Page,
                PrintedLdn = lien.PrintedLdn,
                RateRecord = rateRecords.FirstOrDefault(r => r.id == lien.RateKey),
                Account = account,
                BillId = billId
            };
            //propertyTaxLien.AddPayments(GetLienPayments(propertyTaxLien.Id));
            propertyTaxLien.ApplyPayments(GetLienPayments(propertyTaxLien.Id), false);
            return propertyTaxLien;
        }

        protected IEnumerable<TaxRateInfo> TaxRates
        {
            get
            {
                if (rateRecords == null)
                {
                    rateRecords = GetTaxRates();
                }

                return rateRecords;
            }
        }

        
        protected IEnumerable<TaxRateInfo> GetTaxRates()
        {
            var taxRates = new List<TaxRateInfo>();
            var clContext = contextFactory.GetTaxCollectionsContextInterface();
            var rates = clContext.RateRecords.ToList();
            //var rates = ratesRepo.GetAll();
            foreach (var rate in rates)
            {
                taxRates.Add(MakeTaxRateInfo(rate));
            }

            return taxRates;
        }

        protected TaxRateInfo MakeTaxRateInfo(RateRec rateRecord)
        {
            var taxRateInfo = new TaxRateInfo()
            {
                CommitmentDate = rateRecord.CommitmentDate ?? DateTime.MinValue,
                Description = rateRecord.Description,
                InterestRate = Convert.ToDecimal(rateRecord.InterestRate ?? 0),
                TaxRate = Convert.ToDecimal(rateRecord.TaxRate ?? 0),
                id = rateRecord.Id,
                Year = rateRecord.Year ?? 0,
                BillingDate = rateRecord.BillingDate ?? DateTime.MinValue,
                Installments = MakeInstallmentsFromRateRecord(rateRecord),
                LienDate = rateRecord.LienDate ?? DateTime.MinValue,
                MaturityDate = rateRecord.MaturityDate ?? DateTime.MinValue,
                ThirtyDayNoticeDate = rateRecord.ThirtyDayNoticeDate ?? DateTime.MinValue,
                CreationDate = rateRecord.CreationDate?.Date == new DateTime(1899, 12, 30) ? DateTime.MinValue : rateRecord.CreationDate ?? DateTime.MinValue

            };
            switch (rateRecord.RateType.ToLower())
            {
                case "s":
                    taxRateInfo.RateType = PropertyTaxRateType.Supplemental;
                    break;
                case "l":
                    taxRateInfo.RateType = PropertyTaxRateType.Lien;
                    break;
                default:
                    taxRateInfo.RateType = PropertyTaxRateType.Regular;
                    break;
            }

            return taxRateInfo;
        }

        protected List<InstallmentInfo> MakeInstallmentsFromRateRecord(RateRec rateRecord)
        {
            var installments = new List<InstallmentInfo>();
            var installment =
                MakeInstallment(1, rateRecord.DueDate1, rateRecord.InterestStartDate1, rateRecord.Weight1);
            if (installment != null)
            {
                installments.Add(installment);
            }
            installment = MakeInstallment(2, rateRecord.DueDate2, rateRecord.InterestStartDate2, rateRecord.Weight2);
            if (installment != null)
            {
                installments.Add(installment);
            }
            installment = MakeInstallment(3, rateRecord.DueDate3, rateRecord.InterestStartDate3, rateRecord.Weight3);
            if (installment != null)
            {
                installments.Add(installment);
            }
            installment = MakeInstallment(4, rateRecord.DueDate4, rateRecord.InterestStartDate4, rateRecord.Weight4);
            if (installment != null)
            {
                installments.Add(installment);
            }

            return installments;
        }
        protected InstallmentInfo MakeInstallment(int installmentNumber, DateTime? dueDate, DateTime? interestStartDate, double? weight)
        {
            if (installmentNumber < 1 || installmentNumber > 4)
            {
                return null;
            }

            if (!dueDate.HasValue)
            {
                return null;
            }

            if (!interestStartDate.HasValue)
            {
                return null;
            }

            return new InstallmentInfo()
            {
                InterestStartDate = interestStartDate.Value,
                InstallmentNumber = installmentNumber,
                Weight = weight ?? 0,
                DueDate = dueDate.Value
            };
        }
        protected TaxClub GetTaxClub(int billId)
        {
            var clContext = contextFactory.GetTaxCollectionsContextInterface();
            var taxClub = clContext.TaxClubs.FirstOrDefault(t => t.BillId == billId);
            //var taxClub = taxClubRepository.Find(t => t.BillId == billId).FirstOrDefault();
            return taxClub;
        }

        protected Payments GetLienPayments(int lienId)
        {
            var clContext = contextFactory.GetTaxCollectionsContextInterface();
            var lienPayments = clContext.PaymentRecords.Where(p => p.BillCode == "L" && p.BillId == lienId).OrderBy(p => p.EffectiveInterestDate).ToList();
            // var lienPayments = paymentsRepo.Find(p => p.BillCode == "L" && p.BillId == lienId).OrderBy(p => p.EffectiveInterestDate).ToList();
            var payments = new Payments();
            payments.AddPayments(lienPayments);
            return payments;
        }

        protected Payments GetBillPayments(int billId)
        {
            var clContext = contextFactory.GetTaxCollectionsContextInterface();
            var billPayments = clContext.PaymentRecords.Where(p => p.BillCode != "L" && p.BillId == billId)
                .OrderBy(p => p.EffectiveInterestDate).ToList();
            //var billPayments = paymentsRepo.Find(p => p.BillCode != "L" && p.BillId == billId)
            //    .OrderBy(p => p.EffectiveInterestDate).ToList();
            var payments = new Payments();
            payments.AddPayments(billPayments);
            return payments;
        }

        protected Payments GetBillPaymentsAsOf(int billId, DateTime asOfDate)
        {
            var clContext = contextFactory.GetTaxCollectionsContextInterface();
            var effectiveDate = asOfDate.EndOfDay();
            var billPayments = clContext.PaymentRecords.Where(p => p.BillCode != "L" && p.BillId == billId && p.RecordedTransactionDate <= effectiveDate )
                .OrderBy(p => p.EffectiveInterestDate).ToList();
            var payments = new Payments();
            payments.AddPayments(billPayments);
            return payments;
        }
    }
}