﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Queries;

namespace SharedDataAccess.TaxCollections
{
    public class RealEstateTaxAccountSearchQueryHandler : IQueryHandler<RealEstateTaxAccountSearch, IEnumerable<TaxAccountSearchResult>>
    {
        private IRealEstateContext reContext;
        private ITaxCollectionsContext clContext;
        public RealEstateTaxAccountSearchQueryHandler(IRealEstateContext reContext, ITaxCollectionsContext clContext)
        {
            this.reContext = reContext;
            this.clContext = clContext;
        }
        public IEnumerable<TaxAccountSearchResult> ExecuteQuery(RealEstateTaxAccountSearch query)
        {
            var reAccounts = reContext.REMasters.AsQueryable().Where(r => r.Rscard == 1);

            if (!string.IsNullOrWhiteSpace(query.Maplot))
            {
                reAccounts = reAccounts.Where(r => EF.Functions.Like(r.RsmapLot, query.Maplot + "%"));
            }
            List<TaxAccountSearchResult> foundAccounts = null;
            if (!string.IsNullOrWhiteSpace(query.Name))
            {
                    var deedName1 = reAccounts.Where(r => EF.Functions.Like(r.DeedName1, query.Name + "%")).ToTaxAccountSearchResults();
                    var deedName2 = reAccounts.Where(r => EF.Functions.Like(r.DeedName2, query.Name + "%")).ToTaxAccountSearchResultsForSecondName();
                    if (string.IsNullOrWhiteSpace(query.Maplot))
                    {
                        foundAccounts = deedName1.Union(deedName2)
                            .GroupBy(d => d.Account, (key, g) => g.FirstOrDefault()).OrderBy(t => t.Name).ToList();
                    }
                    else
                    {
                        foundAccounts = deedName1.Union(deedName2)
                            .GroupBy(d => d.Account, (key, g) => g.FirstOrDefault()).OrderBy(t => t.Name)
                            .ThenBy(r => r.MapLot).ToList();
                    }
            }
            else
            {
                foundAccounts = reAccounts.OrderBy(r => r.RsmapLot).ToTaxAccountSearchResults().ToList();
            }


            return foundAccounts;           
        }
    }
}