﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class PaymentRecConfiguration : IEntityTypeConfiguration<PaymentRec>
    {
        public void Configure(EntityTypeBuilder<PaymentRec> builder)
        {
            builder.ToTable("PaymentRec");
            builder.HasIndex(e => e.Account);

            builder.HasIndex(e => new { e.Code, e.Principal, e.PreLienInterest, e.CurrentInterest, e.LienCost, e.ReceiptNumber, e.RecordedTransactionDate, e.BillId, e.BillCode })
                .HasName("IX_PaymentRec_BillKey");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ActualSystemDate).HasColumnType("datetime");

            builder.Property(e => e.BillCode).HasMaxLength(1);
            builder.Property(e => e.BillId).HasColumnName("BillKey");

            builder.Property(e => e.BudgetaryAccountNumber).HasMaxLength(50);

            builder.Property(e => e.CashDrawer).HasMaxLength(1);

            builder.Property(e => e.ChargedInterestdate)
                .HasColumnName("CHGINTDate")
                .HasColumnType("datetime");

            builder.Property(e => e.ChargedInterestId).HasColumnName("CHGINTNumber");

            builder.Property(e => e.Code).HasMaxLength(1);

            builder.Property(e => e.Comments).HasMaxLength(255);

            builder.Property(e => e.CurrentInterest).HasColumnType("money");

            builder.Property(e => e.DosreceiptNumber).HasColumnName("DOSReceiptNumber");

            builder.Property(e => e.EffectiveInterestDate).HasColumnType("datetime");

            builder.Property(e => e.GeneralLedger).HasMaxLength(1);

            builder.Property(e => e.LienCost).HasColumnType("money");

            builder.Property(e => e.OldInterestDate).HasColumnType("datetime");

            builder.Property(e => e.PaidBy).HasMaxLength(255);

            builder.Property(e => e.Period).HasMaxLength(1);

            builder.Property(e => e.PreLienInterest).HasColumnType("money");

            builder.Property(e => e.Principal).HasColumnType("money");

            builder.Property(e => e.RecordedTransactionDate).HasColumnType("datetime");

            builder.Property(e => e.Reference).HasMaxLength(10);
            builder.Property(p => p.IsReversal).HasColumnName("Reversal");

            builder.Property(e => e.SetEidate)
                .HasColumnName("SetEIDate")
                .HasColumnType("datetime");

            builder.Property(e => e.Teller).HasMaxLength(3);

            builder.Property(e => e.TransactionNumber).HasMaxLength(50).HasColumnName("TransNumber");

            builder.Property(e => e.ExternalPaymentIdentifier).HasMaxLength(64);
		}
    }
}