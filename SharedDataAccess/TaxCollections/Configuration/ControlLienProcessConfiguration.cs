﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class ControlLienProcessConfiguration : IEntityTypeConfiguration<ControlLienProcess>
    {
        public void Configure(EntityTypeBuilder<ControlLienProcess> builder)
        {
            builder.ToTable("Control_LienProcess");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.CollectorName).HasMaxLength(255);

            builder.Property(e => e.CommissionExpirationDate).HasColumnType("datetime");

            builder.Property(e => e.County).HasMaxLength(255);

            builder.Property(e => e.DateCreated).HasColumnType("datetime");

            builder.Property(e => e.DateUpdated).HasColumnType("datetime");

            builder.Property(e => e.DefaultSort).HasMaxLength(1);

            builder.Property(e => e.Designation).HasMaxLength(255);

            builder.Property(e => e.FilingDate).HasColumnType("datetime");

            builder.Property(e => e.MapPrepareDate).HasMaxLength(50);

            builder.Property(e => e.MapPreparer).HasMaxLength(255);

            builder.Property(e => e.Muni).HasMaxLength(255);

            builder.Property(e => e.OldCollector).HasMaxLength(255);

            builder.Property(e => e.RecommitmentDate).HasColumnType("datetime");

            builder.Property(e => e.SendCopyToNewOwner).HasMaxLength(150);

            builder.Property(e => e.Signer).HasMaxLength(255);

            builder.Property(e => e.State).HasMaxLength(255);

            builder.Property(e => e.User).HasMaxLength(255);
        }
    }
}