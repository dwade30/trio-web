﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class BillingMasterConfiguration : IEntityTypeConfiguration<BillingMaster>
    {
        public void Configure(EntityTypeBuilder<BillingMaster> builder)
        {
            builder.ToTable("BillingMaster");
            builder.Property(p => p.RateRecId).HasColumnName("RateKey");
            builder.Property(p => p.BillingYear).UsePropertyAccessMode(PropertyAccessMode.Property);
            builder.Property(p => p.BillingType).HasMaxLength(2);
            builder.Property(p => p.Name1).HasMaxLength(255);
            builder.Property(p => p.Name2).HasMaxLength(255);
            builder.Property(p => p.Address1).HasMaxLength(255);
            builder.Property(p => p.Address2).HasMaxLength(255);
            builder.Property(p => p.Address3).HasMaxLength(255);
            builder.Property(p => p.MapLot).HasMaxLength(20);
            builder.Property(p => p.Apt).HasMaxLength(1);
            builder.Property(p => p.StreetName).HasMaxLength(255);
            builder.Property(p => p.TaxDue1).HasColumnType("money");
            builder.Property(p => p.TaxDue2).HasColumnType("money");
            builder.Property(p => p.TaxDue3).HasColumnType("money");
            builder.Property(p => p.TaxDue4).HasColumnType("money");
            builder.Property(p => p.PrincipalPaid).HasColumnType("money");
            builder.Property(p => p.InterestPaid).HasColumnType("money");
            builder.Property(p => p.InterestCharged).HasColumnType("money");
            builder.Property(p => p.DemandFees).HasColumnType("money");
            builder.Property(p => p.DemandFeesPaid).HasColumnType("money");
            builder.Property(p => p.WhetherBilledBefore).HasMaxLength(1);
            builder.Property(p => p.BookPage).HasMaxLength(255);
            builder.Property(p => p.CertifiedMailNumber).HasMaxLength(255);
            builder.Property(p => p.Ref2).HasMaxLength(255);
            builder.Property(p => p.Ref1).HasMaxLength(255);
            builder.Property(p => p.Zip).HasMaxLength(50);
            builder.Property(p => p.IMPBTrackingNumber).HasMaxLength(30);
            builder.Property(p => p.MailingAddress3).HasMaxLength(255);
        }
    }
}
