﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Comment;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Queries;
using SharedApplication.TaxCollections.TaxExtract;
using CollectionsCommentViewModel = SharedDataAccess.TaxCollections.Comment.CollectionsCommentViewModel;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {           
            RegisterStatusReportsRepository(ref builder);
            RegisterREBillingMasterRepository(ref builder);
            RegisterPPBillingMasterRepository(ref builder);          
            builder.RegisterType<TaxCollectionsSettingsQueryHandler>().As<IQueryHandler<TaxCollectionsSettingsQuery, GlobalTaxCollectionSettings>>();
            builder.RegisterType<TaxAccountSearchQueryHandler>()
                .As<IQueryHandler<TaxAccountSearch, IEnumerable<TaxAccountSearchResult>>>();
            builder.RegisterType<TaxExtractQueryHandler>()
                .As<IQueryHandler<TaxExtractAccountQuery, IEnumerable<TaxExtractAccountData>>>();
            builder.RegisterType<PropertyTaxBillQueryHandler>().As<IPropertyTaxBillQueryHandler>();
            RegisterTaxCollectionsContext(ref builder);
            builder.RegisterType<CollectionsCommentViewModel>().As<ICollectionsCommentViewModel>();
            builder.RegisterType<CollectionsNoteQueryHandler>().As<IQueryHandler<CollectionsNoteQuery, Note>>();
            builder.RegisterType<RealEstateTaxAccountInfoQueryHandler>()
                .As<IQueryHandler<RealEstateTaxAccountInfoQuery, RealEstateTaxAccountInfo>>();
            builder.RegisterType<PersonalPropertyTaxAccountInfoQueryHandler>()
                .As<IQueryHandler<PersonalPropertyTaxAccountInfoQuery, PersonalPropertyTaxAccountInfo>>();
            builder.RegisterType<RealEstateTaxAccountSearchQueryHandler>()
                .As<IQueryHandler<RealEstateTaxAccountSearch, IEnumerable<TaxAccountSearchResult>>>();
            builder.RegisterType<PropertyTaxStatusQueryHandler>().As<IPropertyTaxStatusQueryHandler>();
        }

        private void RegisterTaxCollectionsContext(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return tcf.GetTaxCollectionsContext();
            }).As<ITaxCollectionsContext>().AsSelf();
        }

        private void RegisterPPBillingMasterRepository(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return new PPBillingMasterRepository(tcf.GetTaxCollectionsContext());
            }).As<IPPBillingMasterRepository>();
        }

        private void RegisterREBillingMasterRepository(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return new REBillingMasterRepository(tcf.GetTaxCollectionsContext());
            }).As<IREBillingMasterRepository>();
        }

        private void RegisterStatusReportsRepository(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return new SavedStatusReportRepository(tcf.GetTaxCollectionsContext());
            }).As<ISavedStatusReportsRepository>();
        }
    }
}
