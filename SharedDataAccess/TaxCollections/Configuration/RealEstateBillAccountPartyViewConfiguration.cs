﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class RealEstateBillAccountPartyViewConfiguration : IQueryTypeConfiguration<RealEstateBillAccountPartyView>
    {
        public void Configure(QueryTypeBuilder<RealEstateBillAccountPartyView> builder)
        {
            builder.ToView("RealEstateBillAccountPartyView");
            builder.Property(e => e.AccountId)
                .HasColumnName("AccountID")
                .HasMaxLength(255);
            builder.Property(e => e.BillingYear).UsePropertyAccessMode(PropertyAccessMode.Property);
            builder.Property(e => e.Address1).HasMaxLength(255);

            builder.Property(e => e.Address2).HasMaxLength(255);

            builder.Property(e => e.Address3).HasMaxLength(255);

            builder.Property(e => e.Apt).HasMaxLength(1);

            builder.Property(e => e.BillingType).HasMaxLength(2);

            builder.Property(e => e.BookPage).HasMaxLength(255);

            builder.Property(e => e.CardId)
                .HasColumnName("CardID")
                .HasMaxLength(255);

            builder.Property(e => e.CertifiedMailNumber).HasMaxLength(255);

            builder.Property(e => e.DeedName1).HasMaxLength(255);

            builder.Property(e => e.DeedName2).HasMaxLength(255);

            builder.Property(e => e.DemandFees).HasColumnType("money");

            builder.Property(e => e.DemandFeesPaid).HasColumnType("money");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ImpbtrackingNumber)
                .HasColumnName("IMPBTrackingNumber")
                .HasMaxLength(30);

            builder.Property(e => e.InterestAppliedThroughDate).HasColumnType("datetime");

            builder.Property(e => e.InterestCharged).HasColumnType("money");

            builder.Property(e => e.InterestPaid).HasColumnType("money");

            builder.Property(e => e.MailingAddress3).HasMaxLength(255);

            builder.Property(e => e.MapLot).HasMaxLength(20);

            builder.Property(e => e.Name1).HasMaxLength(255);

            builder.Property(e => e.Name2).HasMaxLength(255);

            builder.Property(e => e.OwnerAddress1).HasMaxLength(255);

            builder.Property(e => e.OwnerAddress2).HasMaxLength(255);

            builder.Property(e => e.OwnerAddress3).HasMaxLength(255);

            builder.Property(e => e.OwnerCity).HasMaxLength(255);

            builder.Property(e => e.OwnerDesignation).HasMaxLength(50);

            builder.Property(e => e.OwnerEmail).HasMaxLength(200);

            builder.Property(e => e.OwnerFirstName).HasMaxLength(255);

            builder.Property(e => e.OwnerLastName).HasMaxLength(100);

            builder.Property(e => e.OwnerMiddleName).HasMaxLength(100);

            builder.Property(e => e.OwnerPartyId).HasColumnName("OwnerPartyID");

            builder.Property(e => e.OwnerState).HasMaxLength(255);

            builder.Property(e => e.OwnerZip).HasMaxLength(255);

            builder.Property(e => e.Piacres).HasColumnName("PIAcres");

            builder.Property(e => e.Pineighborhood).HasColumnName("PINeighborhood");

            builder.Property(e => e.Piopen1).HasColumnName("PIOpen1");

            builder.Property(e => e.Piopen2).HasColumnName("PIOpen2");

            builder.Property(e => e.PisecZone).HasColumnName("PISecZone");

            builder.Property(e => e.Pistreet).HasColumnName("PIStreet");

            builder.Property(e => e.PistreetCode).HasColumnName("PIStreetCode");

            builder.Property(e => e.Pitopography1).HasColumnName("PITopography1");

            builder.Property(e => e.Pitopography2).HasColumnName("PITopography2");

            builder.Property(e => e.Piutilities1).HasColumnName("PIUtilities1");

            builder.Property(e => e.Piutilities2).HasColumnName("PIUtilities2");

            builder.Property(e => e.Pizone).HasColumnName("PIZone");

            builder.Property(e => e.Ppassessment).HasColumnName("PPAssessment");

            builder.Property(e => e.PreviousInterestAppliedDate).HasColumnType("datetime");

            builder.Property(e => e.PrincipalPaid).HasColumnType("money");

            builder.Property(e => e.Ref1).HasMaxLength(255);

            builder.Property(e => e.Ref2).HasMaxLength(255);

            builder.Property(e => e.RibldgCode).HasColumnName("RIBldgCode");

            builder.Property(e => e.RiexemptCd1).HasColumnName("RIExemptCD1");

            builder.Property(e => e.RiexemptCd2).HasColumnName("RIExemptCD2");

            builder.Property(e => e.RiexemptCd3).HasColumnName("RIExemptCD3");

            builder.Property(e => e.RilandCode).HasColumnName("RILandCode");

            builder.Property(e => e.RitranCode).HasColumnName("RITranCode");

            builder.Property(e => e.RlbldgVal).HasColumnName("RLBldgVal");

            builder.Property(e => e.Rlexemption).HasColumnName("RLExemption");

            builder.Property(e => e.RllandVal).HasColumnName("RLLandVal");

            builder.Property(e => e.Rsaccount).HasColumnName("RSAccount");

            builder.Property(e => e.Rscard).HasColumnName("RSCard");

            builder.Property(e => e.Rsdeleted).HasColumnName("RSDeleted");

            builder.Property(e => e.Rshard).HasColumnName("RSHard");

            builder.Property(e => e.RshardValue).HasColumnName("RSHardValue");

            builder.Property(e => e.RslocApt)
                .HasColumnName("RSLocApt")
                .HasMaxLength(255);

            builder.Property(e => e.RslocNumAlph)
                .HasColumnName("RSLocNumAlph")
                .HasMaxLength(255);

            builder.Property(e => e.RslocStreet)
                .HasColumnName("RSLocStreet")
                .HasMaxLength(255);

            builder.Property(e => e.RsmapLot)
                .HasColumnName("RSMapLot")
                .HasMaxLength(255);

            builder.Property(e => e.Rsmixed).HasColumnName("RSMixed");

            builder.Property(e => e.RsmixedValue).HasColumnName("RSMixedValue");

            builder.Property(e => e.Rsother).HasColumnName("RSOther");

            builder.Property(e => e.RsotherValue).HasColumnName("RSOtherValue");

            builder.Property(e => e.RspreviousMaster)
                .HasColumnName("RSPreviousMaster")
                .HasMaxLength(255);

            builder.Property(e => e.RspropertyCode)
                .HasColumnName("RSPropertyCode")
                .HasMaxLength(255);

            builder.Property(e => e.Rsref1)
                .HasColumnName("RSRef1")
                .HasMaxLength(255);

            builder.Property(e => e.Rsref2)
                .HasColumnName("RSRef2")
                .HasMaxLength(255);

            builder.Property(e => e.Rssoft).HasColumnName("RSSoft");

            builder.Property(e => e.RssoftValue).HasColumnName("RSSoftValue");

            builder.Property(e => e.SaleDate).HasColumnType("datetime");

            builder.Property(e => e.SecOwnerDesignation).HasMaxLength(50);

            builder.Property(e => e.SecOwnerFirstname).HasMaxLength(255);

            builder.Property(e => e.SecOwnerLastName).HasMaxLength(100);

            builder.Property(e => e.SecOwnerMiddleName).HasMaxLength(100);

            builder.Property(e => e.SecOwnerPartyId).HasColumnName("SecOwnerPartyID");

            builder.Property(e => e.StreetName).HasMaxLength(255);

            builder.Property(e => e.Streetnumber).HasColumnName("streetnumber");

            builder.Property(e => e.Tadate)
                .HasColumnName("TADate")
                .HasColumnType("datetime");

            builder.Property(e => e.TaxDue1).HasColumnType("money");

            builder.Property(e => e.TaxDue2).HasColumnType("money");

            builder.Property(e => e.TaxDue3).HasColumnType("money");

            builder.Property(e => e.TaxDue4).HasColumnType("money");

            builder.Property(e => e.TghardAcres).HasColumnName("TGHardAcres");

            builder.Property(e => e.TghardValue).HasColumnName("TGHardValue");

            builder.Property(e => e.TgmixedAcres).HasColumnName("TGMixedAcres");

            builder.Property(e => e.TgmixedValue).HasColumnName("TGMixedValue");

            builder.Property(e => e.TgsoftAcres).HasColumnName("TGSoftAcres");

            builder.Property(e => e.TgsoftValue).HasColumnName("TGSoftValue");

            builder.Property(e => e.TransferFromBillingDateFirst).HasColumnType("datetime");

            builder.Property(e => e.TransferFromBillingDateLast).HasColumnType("datetime");

            builder.Property(e => e.WhetherBilledBefore).HasMaxLength(1);

            builder.Property(e => e.Zip).HasMaxLength(50);

            builder.Property(e => e.LienCosts).HasColumnType("money");

            builder.Property(e => e.LienCostsPaid).HasColumnType("money");

            builder.Property(e => e.LienInterest).HasColumnType("money");

            builder.Property(e => e.LienInterestAppliedThroughDate).HasColumnType("datetime");

            builder.Property(e => e.LienInterestCharged).HasColumnType("money");

            builder.Property(e => e.LienInterestPaid).HasColumnType("money");

            builder.Property(e => e.LienMaturityFee).HasColumnType("money");

            builder.Property(e => e.LienPage)
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.LienPlipaid)
                .HasColumnName("LienPLIPaid")
                .HasColumnType("money");

            builder.Property(e => e.LienPreviousInterestAppliedDate).HasColumnType("datetime");

            builder.Property(e => e.LienPrincipal).HasColumnType("money");

            builder.Property(e => e.LienPrincipalPaid).HasColumnType("money");

            builder.Property(e => e.LienPrintedLdn).HasColumnName("LienPrintedLDN");

            builder.Property(e => e.LienStatus).HasMaxLength(255);
        }
    }
}