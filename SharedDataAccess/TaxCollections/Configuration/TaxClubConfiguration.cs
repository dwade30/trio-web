﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class TaxClubConfiguration : IEntityTypeConfiguration<TaxClub>
    {
        public void Configure(EntityTypeBuilder<TaxClub> builder)
        {
            builder.ToTable("TaxClub");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.BillId).HasColumnName("BillKey");
            builder.Property(e => e.AgreementDate).HasColumnType("datetime");

            builder.Property(e => e.LastPaidDate).HasColumnType("datetime");

            builder.Property(e => e.Location).HasMaxLength(50);

            builder.Property(e => e.MapLot).HasMaxLength(50);

            builder.Property(e => e.Name).HasMaxLength(255);

            builder.Property(e => e.StartDate).HasColumnType("datetime");

            builder.Property(e => e.Type).HasMaxLength(255);
        }
    }
}