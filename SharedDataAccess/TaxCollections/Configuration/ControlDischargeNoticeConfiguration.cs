﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class ControlDischargeNoticeConfiguration : IEntityTypeConfiguration<ControlDischargeNotice>
    {
        public void Configure(EntityTypeBuilder<ControlDischargeNotice> builder)
        {
            builder.ToTable("Control_DischargeNotice");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AltName1).HasMaxLength(255);

            builder.Property(e => e.AltName2).HasMaxLength(255);

            builder.Property(e => e.AltName3).HasMaxLength(255);

            builder.Property(e => e.AltName4).HasMaxLength(255);

            builder.Property(e => e.CommissionExpiration).HasColumnType("datetime");

            builder.Property(e => e.County).HasMaxLength(255);

            builder.Property(e => e.SignerDesignation).HasMaxLength(255);

            builder.Property(e => e.SignerName).HasMaxLength(255);

            builder.Property(e => e.Treasurer).HasMaxLength(255);

            builder.Property(e => e.TreasurerTitle).HasMaxLength(255);
        }
    }
}