﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class CollectionSettingConfiguration : IEntityTypeConfiguration<CollectionSetting>
    {
        public void Configure(EntityTypeBuilder<CollectionSetting> builder)
        {
            builder.ToTable("Collections");            
            builder.Property(p => p.DaysInYear).HasColumnName("Basis");
            builder.Property(p => p.ThirtyDayNotice).HasMaxLength(255).HasColumnName("30DayNotice");
            builder.Property(p => p.TaxBillFormat).HasMaxLength(1);
            builder.Property(p => p.LienNotice).HasMaxLength(255);
            builder.Property(p => p.MaturityNotice).HasMaxLength(255);
            builder.Property(p => p.TreasurerSig).HasMaxLength(255);
            builder.Property(p => p.ThirtyDayNoticeR).HasMaxLength(255).HasColumnName("30DayNoticeR");
            builder.Property(p => p.LienNoticeR).HasMaxLength(255);
            builder.Property(p => p.MaturityNoticeR).HasMaxLength(255);
            builder.Property(p => p.RNForm).HasMaxLength(255);
            builder.Property(p => p.LienAbateText).HasMaxLength(255);
            builder.Property(p => p.ThirtyDNYear).HasColumnName("30DNYear");
            builder.Property(p => p.TownEmail).HasMaxLength(255);
            builder.Property(p => p.AcctDetailAssesment).HasMaxLength(255);
            builder.Property(p => p.TaxServiceEmail).HasMaxLength(255);
            builder.Property(p => p.Version).HasMaxLength(50);
        }
    }
}