﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class LienRecConfiguration : IEntityTypeConfiguration<LienRec>
    {
        public void Configure(EntityTypeBuilder<LienRec> builder)
        {
            builder.ToTable("LienRec");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Book)
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.Costs).HasColumnType("money");

            builder.Property(e => e.CostsPaid).HasColumnType("money");

            builder.Property(e => e.DateCreated).HasColumnType("datetime");

            builder.Property(e => e.Interest).HasColumnType("money");

            builder.Property(e => e.InterestAppliedThroughDate).HasColumnType("datetime");

            builder.Property(e => e.InterestCharged).HasColumnType("money");

            builder.Property(e => e.InterestPaid).HasColumnType("money");

            builder.Property(e => e.MaturityFee).HasColumnType("money");

            builder.Property(e => e.Page)
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.Plipaid)
                .HasColumnName("PLIPaid")
                .HasColumnType("money");

            builder.Property(e => e.Principal).HasColumnType("money");

            builder.Property(e => e.PrincipalPaid).HasColumnType("money");

            builder.Property(e => e.PrintedLdn).HasColumnName("PrintedLDN");

            builder.Property(e => e.Status).HasMaxLength(255);
        }
    }
}