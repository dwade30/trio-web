﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class BatchRecoverConfiguration : IEntityTypeConfiguration<BatchRecover>
    {
        public void Configure(EntityTypeBuilder<BatchRecover> builder)
        {
            builder.ToTable("BatchRecover");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.BatchRecoverDate).HasColumnType("datetime");

            builder.Property(e => e.BillingType).HasMaxLength(10);

            builder.Property(e => e.Etdate)
                .HasColumnName("ETDate")
                .HasColumnType("datetime");

            builder.Property(e => e.ImportId).HasColumnName("ImportID");

            builder.Property(e => e.Name).HasMaxLength(255);

            builder.Property(e => e.PaidBy).HasMaxLength(255);

            builder.Property(e => e.PaymentIdentifier).HasMaxLength(64);

            builder.Property(e => e.Ref).HasMaxLength(255);

            builder.Property(e => e.TellerId)
                .HasColumnName("TellerID")
                .HasMaxLength(255);

            builder.Property(e => e.Type).HasMaxLength(255);
        }
    }
}