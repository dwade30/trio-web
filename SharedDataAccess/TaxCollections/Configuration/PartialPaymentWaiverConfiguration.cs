﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class PartialPaymentWaiverConfiguration : IEntityTypeConfiguration<PartialPaymentWaiver>
    {
        public void Configure(EntityTypeBuilder<PartialPaymentWaiver> builder)
        {
            builder.ToTable("PartialPaymentWaiver");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.EffectiveDate).HasColumnType("datetime");

        }
    }
}