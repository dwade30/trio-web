﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class PersonalPropertyMasterBillingMasterViewConfiguration : IQueryTypeConfiguration<PersonalPropertyMasterBillingMasterView>
    {
        public void Configure(QueryTypeBuilder<PersonalPropertyMasterBillingMasterView> builder)
        {
            builder.ToView("PersonalPropertyMasterBillingMasterView");
            builder.Property(p => p.RateRecId).HasColumnName("RateKey");
            builder.Property(p => p.BillingYear).UsePropertyAccessMode(PropertyAccessMode.Property);
            builder.Property(p => p.BillingType).HasMaxLength(2);
            builder.Property(p => p.Name1).HasMaxLength(255);
            builder.Property(p => p.Name2).HasMaxLength(255);
            builder.Property(p => p.Address1).HasMaxLength(255);
            builder.Property(p => p.Address2).HasMaxLength(255);
            builder.Property(p => p.Address3).HasMaxLength(255);
            builder.Property(p => p.MapLot).HasMaxLength(20);
            builder.Property(p => p.Apt).HasMaxLength(1);
            builder.Property(p => p.StreetName).HasMaxLength(255);
            builder.Property(p => p.TaxDue1).HasColumnType("money");
            builder.Property(p => p.TaxDue2).HasColumnType("money");
            builder.Property(p => p.TaxDue3).HasColumnType("money");
            builder.Property(p => p.TaxDue4).HasColumnType("money");
            builder.Property(p => p.PrincipalPaid).HasColumnType("money");
            builder.Property(p => p.InterestPaid).HasColumnType("money");
            builder.Property(p => p.InterestCharged).HasColumnType("money");
            builder.Property(p => p.DemandFees).HasColumnType("money");
            builder.Property(p => p.DemandFeesPaid).HasColumnType("money");
            builder.Property(p => p.WhetherBilledBefore).HasMaxLength(1);
            builder.Property(p => p.BookPage).HasMaxLength(255);
            builder.Property(p => p.CertifiedMailNumber).HasMaxLength(255);
            builder.Property(p => p.Ref2).HasMaxLength(255);
            builder.Property(p => p.Ref1).HasMaxLength(255);
            builder.Property(p => p.Zip).HasMaxLength(50);
            builder.Property(p => p.IMPBTrackingNumber).HasMaxLength(30);
            builder.Property(p => p.MailingAddress3).HasMaxLength(255);

            builder.Property(e => e.AccountId)
                .HasColumnName("AccountID")
                .HasMaxLength(255);

            builder.Property(e => e.Comment).IsUnicode(false);

            builder.Property(e => e.Da).HasColumnName("DA");

            builder.Property(e => e.Mo).HasColumnName("MO");

            builder.Property(e => e.Open1).HasMaxLength(255);

            builder.Property(e => e.Open2).HasMaxLength(255);

            builder.Property(e => e.Orcode)
                .HasColumnName("ORCode")
                .HasMaxLength(50);

            builder.Property(e => e.PartyId).HasColumnName("PartyID");

            builder.Property(e => e.Rbcode)
                .HasColumnName("RBCode")
                .HasMaxLength(50);

            builder.Property(e => e.Street).HasMaxLength(255);

            builder.Property(e => e.StreetApt).HasMaxLength(255);

            builder.Property(e => e.Updcode)
                .HasColumnName("UPDCode")
                .HasMaxLength(50);

            builder.Property(e => e.Yr).HasColumnName("YR");
        }
    }
}
