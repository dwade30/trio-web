﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class NotesConfiguration : IEntityTypeConfiguration<Note>
    {
        public void Configure(EntityTypeBuilder<Note> builder)
        {
            builder.ToTable("Comments");
            builder.Property("Id").HasColumnName("ID");
            builder.Property("ShowInRealEstate").HasColumnName("ShowInRE");
            builder.Property("AccountTypeAbbreviation").HasColumnName("Type").HasMaxLength(255);
            builder.Property("Comment").HasMaxLength(255);
            builder.Ignore(p => p.BillType);
        }
    }
}