﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class PersonalPropertyBillAccountPartyViewConfiguration : IQueryTypeConfiguration<PersonalPropertyBillAccountPartyView>
    {
        public void Configure(QueryTypeBuilder<PersonalPropertyBillAccountPartyView> entity)
        {
           

            entity.ToView("PersonalPropertyBillAccountPartyView");

            entity.Property(e => e.AccountId)
                .HasColumnName("AccountID")
                .HasMaxLength(255);

            entity.Property(e => e.Address1).HasMaxLength(255);

            entity.Property(e => e.Address2).HasMaxLength(255);

            entity.Property(e => e.Address3).HasMaxLength(255);

            entity.Property(e => e.Apt).HasMaxLength(1);

            entity.Property(e => e.BillingType).HasMaxLength(2);
            entity.Property(e => e.BillingYear).UsePropertyAccessMode(PropertyAccessMode.Property);

            entity.Property(e => e.BookPage).HasMaxLength(255);

            entity.Property(e => e.CertifiedMailNumber).HasMaxLength(255);

            entity.Property(e => e.Comment).IsUnicode(false);

            entity.Property(e => e.DemandFees).HasColumnType("money");

            entity.Property(e => e.DemandFeesPaid).HasColumnType("money");

            entity.Property(e => e.Id).HasColumnName("ID");

            entity.Property(e => e.ImpbtrackingNumber)
                .HasColumnName("IMPBTrackingNumber")
                .HasMaxLength(30);

            entity.Property(e => e.InterestAppliedThroughDate).HasColumnType("datetime");

            entity.Property(e => e.InterestCharged).HasColumnType("money");

            entity.Property(e => e.InterestPaid).HasColumnType("money");

            entity.Property(e => e.MailingAddress3).HasMaxLength(255);

            entity.Property(e => e.MapLot).HasMaxLength(20);

            entity.Property(e => e.Name1).HasMaxLength(255);

            entity.Property(e => e.Name2).HasMaxLength(255);

            entity.Property(e => e.Open1).HasMaxLength(255);

            entity.Property(e => e.Open2).HasMaxLength(255);

            entity.Property(e => e.OwnerAddress1).HasMaxLength(255);

            entity.Property(e => e.OwnerAddress2).HasMaxLength(255);

            entity.Property(e => e.OwnerAddress3).HasMaxLength(255);

            entity.Property(e => e.OwnerCity).HasMaxLength(255);

            entity.Property(e => e.OwnerCountry).HasMaxLength(255);

            entity.Property(e => e.OwnerDesignation).HasMaxLength(50);

            entity.Property(e => e.OwnerEmail).HasMaxLength(200);

            entity.Property(e => e.OwnerFirstName).HasMaxLength(255);

            entity.Property(e => e.OwnerLastName).HasMaxLength(100);

            entity.Property(e => e.OwnerMiddleName).HasMaxLength(100);

            entity.Property(e => e.OwnerState).HasMaxLength(255);

            entity.Property(e => e.OwnerZip).HasMaxLength(255);

            entity.Property(e => e.PartyId).HasColumnName("PartyID");

            entity.Property(e => e.Ppassessment).HasColumnName("PPAssessment");

            entity.Property(e => e.PreviousInterestAppliedDate).HasColumnType("datetime");

            entity.Property(e => e.PrincipalPaid).HasColumnType("money");

            entity.Property(e => e.Rbcode)
                .HasColumnName("RBCode")
                .HasMaxLength(50);

            entity.Property(e => e.Ref1).HasMaxLength(255);

            entity.Property(e => e.Ref2).HasMaxLength(255);

            entity.Property(e => e.Street).HasMaxLength(255);

            entity.Property(e => e.StreetApt).HasMaxLength(255);

            entity.Property(e => e.StreetName).HasMaxLength(255);

            entity.Property(e => e.Streetnumber).HasColumnName("streetnumber");

            entity.Property(e => e.TaxDue1).HasColumnType("money");

            entity.Property(e => e.TaxDue2).HasColumnType("money");

            entity.Property(e => e.TaxDue3).HasColumnType("money");

            entity.Property(e => e.TaxDue4).HasColumnType("money");

            entity.Property(e => e.TghardAcres).HasColumnName("TGHardAcres");

            entity.Property(e => e.TghardValue).HasColumnName("TGHardValue");

            entity.Property(e => e.TgmixedAcres).HasColumnName("TGMixedAcres");

            entity.Property(e => e.TgmixedValue).HasColumnName("TGMixedValue");

            entity.Property(e => e.TgsoftAcres).HasColumnName("TGSoftAcres");

            entity.Property(e => e.TgsoftValue).HasColumnName("TGSoftValue");

            entity.Property(e => e.TransferFromBillingDateFirst).HasColumnType("datetime");

            entity.Property(e => e.TransferFromBillingDateLast).HasColumnType("datetime");

            entity.Property(e => e.WhetherBilledBefore).HasMaxLength(1);

            entity.Property(e => e.Zip).HasMaxLength(50);
        }
    }
}