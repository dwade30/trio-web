﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class CollectionsCommentConfiguration : IEntityTypeConfiguration<CollectionsComment>
    {
        public void Configure(EntityTypeBuilder<CollectionsComment> builder)
        {
            builder.ToTable("CommentRec");
            builder.Property("Id").HasColumnName("ID");
            builder.Property("Account").HasColumnName("AccountKey");
            builder.Property("IsRealEstate").HasColumnName("RE");
            builder.Ignore(p => p.BillType);
        }
    }
}