﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class DischargeNeededConfiguration : IEntityTypeConfiguration<DischargeNeeded>
    {
        public void Configure(EntityTypeBuilder<DischargeNeeded> builder)
        {
            builder.ToTable("DischargeNeeded");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Book)
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.DatePaid).HasColumnType("datetime");

            builder.Property(e => e.Page)
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.Teller).HasMaxLength(255);

            builder.Property(e => e.UpdatedDate).HasColumnType("datetime");

            builder.Property(e => e.BillId).HasColumnName("BillKey");
            builder.Property(e => e.LienId).HasColumnName("LienKey");
            builder.Property(e => e.Cancelled).HasColumnName("Canceled");
        }
    }
}