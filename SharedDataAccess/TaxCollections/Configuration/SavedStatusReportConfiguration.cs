﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections.Configuration
{
    public class SavedStatusReportConfiguration : IEntityTypeConfiguration<SavedStatusReport>
    {
        public void Configure(EntityTypeBuilder<SavedStatusReport> builder)
        {
            builder.Property(e => e.ID).HasColumnName("ID");

            builder.Property(e => e.FieldConstraint0).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint1).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint2).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint3).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint4).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint5).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint6).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint7).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint8).HasMaxLength(255);

            builder.Property(e => e.FieldConstraint9).HasMaxLength(255);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.ReportName).HasMaxLength(255);

            builder.Property(e => e.SortSelection).HasMaxLength(255);

            builder.Property(e => e.SQL).HasColumnName("SQL");

            builder.Property(e => e.Type).HasMaxLength(255);

            builder.Property(e => e.WhereSelection).HasMaxLength(255);
        }
    }
}