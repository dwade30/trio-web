﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;

namespace SharedDataAccess.TaxCollections
{
    public class RealEstateTaxAccountInfoQueryHandler : IQueryHandler<RealEstateTaxAccountInfoQuery,RealEstateTaxAccountInfo>
    {
        private IRealEstateContext reContext;
        private IQueryHandler<RealEstateAccountTotalsQuery, RealEstateAccountTotals> totalHandler;
        private ICentralDataContext cdContext;
        private IQueryHandler<RealEstateBookPageQuery, IEnumerable<BookPage>> bookPageQueryHandler;
        private IQueryHandler<TaxAccountCalculationSummaryQuery, TaxBillCalculationSummary> taxCalculationHandler;
        public RealEstateTaxAccountInfoQueryHandler(IRealEstateContext reContext,  ICentralDataContext cdContext, IQueryHandler<RealEstateAccountTotalsQuery, RealEstateAccountTotals> totalHandler, IQueryHandler<RealEstateBookPageQuery,IEnumerable<BookPage>> bookPageQueryHandler, IQueryHandler<TaxAccountCalculationSummaryQuery, TaxBillCalculationSummary> taxCalculationHandler)
        {
            this.reContext = reContext;
            this.totalHandler = totalHandler;
            this.cdContext = cdContext;
            this.bookPageQueryHandler = bookPageQueryHandler;
            this.taxCalculationHandler = taxCalculationHandler;
        }
        public RealEstateTaxAccountInfo ExecuteQuery(RealEstateTaxAccountInfoQuery query)
        {
            var accountView = reContext.REAccountPartyAddressViews.Where(r => r.Rsaccount == query.Account && r.Rscard == 1).FirstOrDefault();
            if (accountView == null)
            {
                return new RealEstateTaxAccountInfo();
            }

            var totals = totalHandler.ExecuteQuery(new RealEstateAccountTotalsQuery(query.Account));
            decimal personalPropertyOwed = 0;

            var ppAccount = 0;
            var groupNumber = 0;
            var association = cdContext.ModuleAssociations
                .Where(a => a.Module == "PP" && a.PrimaryAssociate == true && a.REMasterAcct == query.Account)
                .FirstOrDefault();
            if (association != null)
            {
                var groupMaster = cdContext.GroupMasters.FirstOrDefault(g => g.ID == association.GroupNumber.GetValueOrDefault());
                ppAccount = association.Account.GetValueOrDefault();
                if (groupMaster != null)
                {
                    groupNumber = groupMaster.GroupNumber.GetValueOrDefault();
                }
            }            

            if (ppAccount > 0)
            {
                var calculationSummary = taxCalculationHandler.ExecuteQuery(new TaxAccountCalculationSummaryQuery(ppAccount,
                    PropertyTaxBillType.Personal, DateTime.Now));
                if (calculationSummary != null)
                {
                    personalPropertyOwed = calculationSummary.Balance;
                }
            }

            var info = new RealEstateTaxAccountInfo()
            {
                Account = query.Account,
                IsTaxAcquired = accountView.TaxAcquired.GetValueOrDefault(),
                LocationStreet = accountView.RslocStreet,
                MailingAddress = {Address1 = accountView.Address1,Address2 = accountView.Address2, Address3 = accountView.Address3, City = accountView.City, State = accountView.State, Zip = accountView.Zip},
                MapLot = accountView.RsmapLot,
                Name1 = accountView.DeedName1,
                Name2 = accountView.DeedName2,
                Acreage = totals.Acreage,
                AssociatedPPAccount = ppAccount,
                OutstandingPPAmount = personalPropertyOwed,                
                AccountGroup = groupNumber
            };
            if (!string.IsNullOrWhiteSpace(accountView.RslocNumAlph))
            {
                info.LocationNumber = accountView.RslocNumAlph.ToIntegerValue();
            }
            if (accountView.RiexemptCd1.GetValueOrDefault() > 0)
            {
                var code = reContext.ExemptCodes.FirstOrDefault(e => e.Code.GetValueOrDefault() == accountView.RiexemptCd1.GetValueOrDefault());
                if (code != null)
                {
                    info.ExemptionCodes.Add(code.Code.GetValueOrDefault().ToString() + " " + code.Description );
                }
            }

            if (accountView.RiexemptCd2.GetValueOrDefault() > 0)
            {
                var code = reContext.ExemptCodes.FirstOrDefault(e => e.Code.GetValueOrDefault() == accountView.RiexemptCd2.GetValueOrDefault());
                if (code != null)
                {
                    info.ExemptionCodes.Add(code.Code.GetValueOrDefault().ToString() + " " + code.Description);
                }
            }

            if (accountView.RiexemptCd3.GetValueOrDefault() > 0)
            {
                var code = reContext.ExemptCodes.FirstOrDefault(e => e.Code.GetValueOrDefault() == accountView.RiexemptCd3.GetValueOrDefault());
                if (code != null)
                {
                    info.ExemptionCodes.Add(code.Code.GetValueOrDefault().ToString() + " " + code.Description);
                }
            }

            var bookPages = bookPageQueryHandler.ExecuteQuery(new RealEstateBookPageQuery(query.Account, true));
            info.BookPage = MakeBookPageString(bookPages);            
            return info;
        }

        private string MakeBookPageString(IEnumerable<BookPage> bookPages)
        {
            if (bookPages == null)
            {
                return "";
            }

            var bookPageString = new StringBuilder();
            var spacer = "";
            foreach (var bookPage in bookPages)
            {
                bookPageString.Append(spacer + "B" + bookPage.Book + "P" + bookPage.Page);
                spacer = " ";
            }

            return bookPageString.ToString();
        }
    }
}