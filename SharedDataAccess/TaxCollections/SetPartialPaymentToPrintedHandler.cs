﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;

namespace SharedDataAccess.TaxCollections
{
    public class SetPartialPaymentToPrintedHandler : CommandHandler<SetPartialPaymentToPrinted>
    {
        private TaxCollectionsContext collectionsContext;

        public SetPartialPaymentToPrintedHandler(TaxCollectionsContext collectionsContext)
        {
            this.collectionsContext = collectionsContext;
        }
        protected override void Handle(SetPartialPaymentToPrinted command)
        {
            var waiver = collectionsContext.PartialPaymentWaivers
                .FirstOrDefault(w => w.LienKey == command.LienId);
            if (waiver != null)
            {
                waiver.Printed = true;
                collectionsContext.SaveChanges();
            }
        }
    }
}