﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using SharedApplication;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class PropertyTaxBillQueryHandler : PropertyTaxBillQuerier, IPropertyTaxBillQueryHandler
    {
        //private IRepositoryFactory collectionsRepoFactory;
       
        //protected ITaxCollectionsContext clContext;
        
        public PropertyTaxBillQueryHandler(ITrioContextInterfaceFactory interfaceFactory)
        {
           // collectionsRepoFactory = taxCollectionsRepoFactory;
           this.contextFactory = interfaceFactory;
        }

        public PropertyTaxBill Get(int id)
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();
            //var billRepo = collectionsRepoFactory.CreateRepository<BillingMaster>();
            var bill = context.BillingMasters.FirstOrDefault(b => b.ID == id);
            //var bill = billRepo.Get(id);
            if (bill == null)
            {
                return null;
            }

            return MakePropertyTaxBill(bill);
        }

        public IEnumerable<PropertyTaxBill> GetAll()
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();
            var taxBills = new List<PropertyTaxBill>();
            //var billRepo = collectionsRepoFactory.CreateRepository<BillingMaster>();
            //var bills = billRepo.GetAll();
            var bills = context.BillingMasters.AsEnumerable();
            foreach (var bill in bills)
            {
                taxBills.Add(MakePropertyTaxBill(bill));
            }

            return taxBills;
        }

        public IEnumerable<PropertyTaxBill> Find(Expression<Func<BillingMaster, bool>> predicate)
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();
            var taxBills = new List<PropertyTaxBill>();
            //var billRepo = collectionsRepoFactory.CreateRepository<BillingMaster>();
            //var bills = billRepo.Find(predicate);
            var bills = context.BillingMasters.Where(predicate);
            foreach (var bill in bills)
            {
                taxBills.Add(MakePropertyTaxBill(bill));
            }

            return taxBills;
        }
    }
}