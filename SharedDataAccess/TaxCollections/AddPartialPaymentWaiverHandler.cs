﻿using System.Linq;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class AddPartialPaymentWaiverHandler : CommandHandler<AddPartialPaymentWaiver>
    {
        private TaxCollectionsContext collectionsContext;
        public AddPartialPaymentWaiverHandler(TaxCollectionsContext collectionsContext)
        {
            this.collectionsContext = collectionsContext;
        }
        protected override void Handle(AddPartialPaymentWaiver command)
        {
            var waiver = collectionsContext.PartialPaymentWaivers.Where(w => w.LienKey == command.LienId)
                .FirstOrDefault();
            if (waiver == null)
            {
                waiver = new PartialPaymentWaiver()
                {
                    Account = command.Account,
                    EffectiveDate = command.EffectiveDate,
                    LienKey = command.LienId,
                    BillYear = command.YearBill,
                    Printed = false,
                    Payment = command.PaymentAmount.ToDouble(),
                    Owed = command.OwedAmount.ToDouble()
                };
                collectionsContext.PartialPaymentWaivers.Add(waiver);
                collectionsContext.SaveChanges();
            }
        }
    }
}