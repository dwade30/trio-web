﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;

namespace SharedDataAccess.TaxCollections
{
    public class GetBasePropertyTaxAccountHandler : CommandHandler<GetBasePropertyTaxAccount,PropertyTaxAccount>
    {
        private ITaxCollectionsContext clContext;
        private IPropertyTaxBillQueryHandler billQueryHandler;
        public GetBasePropertyTaxAccountHandler(ITaxCollectionsContext clContext, IPropertyTaxBillQueryHandler billQueryHandler)
        {
            this.clContext = clContext;
            this.billQueryHandler = billQueryHandler;
        }
        protected override PropertyTaxAccount Handle(GetBasePropertyTaxAccount command)
        {
            if (command.BillType == PropertyTaxBillType.Personal)
            {
                return GetPPAccount(command.Account);
            }
            else
            {
                return GetREAccount(command.Account);
            }
        }

        private RealEstateTaxAccount GetREAccount(int account)
        {
            var bills = billQueryHandler.Find(b => b.Account == account && b.BillingType == "RE").ToList();
            var reAccount = new RealEstateTaxAccount();
            reAccount.Account = account;
            reAccount.AddBills(bills);
            return reAccount;
        }

        private PersonalPropertyTaxAccount GetPPAccount(int account)
        {
            var bills = billQueryHandler.Find(b => b.Account == account && b.BillingType == "PP").ToList();
            var ppAccount = new PersonalPropertyTaxAccount();
            ppAccount.Account = account;
            ppAccount.AddBills(bills);
            return ppAccount;
        }
    }
}