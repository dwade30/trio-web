﻿using System;
using System.Collections.Generic;
using SharedApplication.TaxCollections.Enums;
using SharedDataAccess.TaxCollections;
using  SharedApplication.TaxCollections.Interfaces;
namespace SharedDataAccess.TaxCollections
{
    public class REBillingMasterRepository : BillingMasterRepository, IREBillingMasterRepository
    {       
        public REBillingMasterRepository(TaxCollectionsContext context) : base(context)
        {
            base.BillingType = PropertyTaxBillType.Real;
        }

        #region Implementation of IPurge

        /// <inheritdoc />
        public Dictionary<string, int> Purge(List<string> listYearsToPurge)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
