﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class SetDischargeNeededToPrintedHandler : CommandHandler<SetDischargeNeededToPrinted>
    {
        private TaxCollectionsContext clContext;
        public SetDischargeNeededToPrintedHandler(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override void Handle(SetDischargeNeededToPrinted command)
        {
            var dischargeNeeded = clContext.NeededDischarges.FirstOrDefault(nd => nd.LienId == command.LienId);
            if (dischargeNeeded == null)
            {
                dischargeNeeded = new DischargeNeeded()
                {
                    Teller = "Admin",
                    Batch =  false,
                    BillId =  0,
                    Book = "",
                    Cancelled = false,
                    LienId = command.LienId,
                    Page = "",
                    DatePaid = command.DatePaid
                };
                clContext.NeededDischarges.Add(dischargeNeeded);
            }

            dischargeNeeded.UpdatedDate = command.UpdatedDate;
            dischargeNeeded.Printed = true;
            clContext.SaveChanges();
        }
    }
}