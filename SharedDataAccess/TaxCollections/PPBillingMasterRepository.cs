﻿using SharedDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;
using SharedDataAccess.TaxCollections;

namespace SharedDataAccess.TaxCollections
{
    public class PPBillingMasterRepository :BillingMasterRepository, IPPBillingMasterRepository
    {
            public PPBillingMasterRepository(TaxCollectionsContext context) : base(context)
            {
                base.BillingType = PropertyTaxBillType.Personal;
            }

            #region Implementation of IPPBillingMasterRepository.IPurge

            /// <inheritdoc />
            public Dictionary<string, int> Purge(List<string> listYearsToPurge)
            {
                throw new NotImplementedException();
            }

            #endregion
    }
}
