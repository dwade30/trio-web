﻿using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Notes;
using SharedApplication.TaxCollections.Queries;

namespace SharedDataAccess.TaxCollections
{
    public class GetCollectionsNoteHandler : CommandHandler<GetCollectionsNote,Note>
    {
        private IQueryHandler<CollectionsNoteQuery, Note> queryHandler;

        public GetCollectionsNoteHandler(IQueryHandler<CollectionsNoteQuery, Note> queryHandler)
        {
            this.queryHandler = queryHandler;
        }
        protected override Note Handle(GetCollectionsNote command)
        {
            return queryHandler.ExecuteQuery(new CollectionsNoteQuery(command.Account,command.BillType));
        }
    }
}