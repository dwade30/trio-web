﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
	public class GetDischargeNeededBookPageHandler : CommandHandler<GetDischargeNeededBookPage, string>
	{
		private ITaxCollectionsContext clContext;

		public GetDischargeNeededBookPageHandler(ITaxCollectionsContext clContext)
		{
			this.clContext = clContext;
		}
		protected override string Handle(GetDischargeNeededBookPage command)
		{
			var result = clContext.NeededDischarges.FirstOrDefault(x => x.LienId == command.LienId);
			if (result != null && result?.Book != null && result?.Book != "")
			{
				return "B" + result.Book + "P" + result.Page;
			}
			else
			{
				return "";
			}
		}
	}
}
