﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class SaveControlDischargeNoticeHandler : CommandHandler<SaveControlDischargeNotice>
    {
        private TaxCollectionsContext clContext;

        public SaveControlDischargeNoticeHandler(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override void Handle(SaveControlDischargeNotice command)
        {
            if (command.DischargeNotice != null)
            {
                var dischargeNotice = clContext.ControlDischargeNotices.FirstOrDefault();
                if (dischargeNotice == null)
                {
                    dischargeNotice = new ControlDischargeNotice();
                    clContext.Add(dischargeNotice);
                }

                dischargeNotice.AltBottomText = command.DischargeNotice.AltBottomText;
                dischargeNotice.AltName1 = command.DischargeNotice.AltName1;
                dischargeNotice.AltName2 = command.DischargeNotice.AltName2;
                dischargeNotice.AltName3 = command.DischargeNotice.AltName3;
                dischargeNotice.AltName4 = command.DischargeNotice.AltName4;
                dischargeNotice.AltTopText = command.DischargeNotice.AltTopText;
                dischargeNotice.CommissionExpiration = command.DischargeNotice.CommissionExpiration;
                dischargeNotice.County = command.DischargeNotice.County;
                dischargeNotice.Male = command.DischargeNotice.Male;
                dischargeNotice.SignerDesignation = command.DischargeNotice.SignerDesignation;
                dischargeNotice.SignerName = command.DischargeNotice.SignerName;
                dischargeNotice.Treasurer = command.DischargeNotice.Treasurer;
                dischargeNotice.TreasurerTitle = command.DischargeNotice.TreasurerTitle;
                clContext.SaveChanges();
            }
        }
    }
}