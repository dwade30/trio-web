﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;

namespace SharedDataAccess.TaxCollections
{
    public class SetLienDischargeNoticePrintedHandler : CommandHandler<SetLienDischargeNoticePrinted>
    {
        private TaxCollectionsContext clContext;
        public SetLienDischargeNoticePrintedHandler(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override void Handle(SetLienDischargeNoticePrinted command)
        {
            var lien = clContext.LienRecords.FirstOrDefault(l => l.Id == command.LienId);
            if (lien != null)
            {
                lien.PrintedLdn = true;
            }

            clContext.SaveChanges();
        }
    }
}