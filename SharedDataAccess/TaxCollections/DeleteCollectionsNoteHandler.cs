﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Notes;

namespace SharedDataAccess.TaxCollections
{
    public class DeleteCollectionsNoteHandler : CommandHandler<DeleteCollectionsNote>
    {
        private TaxCollectionsContext clContext;

        public DeleteCollectionsNoteHandler(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override void Handle(DeleteCollectionsNote command)
        {
            var note = clContext.Notes.FirstOrDefault(n => n.Id == command.Id);
            if (note != null)
            {
                clContext.Notes.Remove(note);
                clContext.SaveChanges();
            }
        }
    }
}