﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections.Commands;

namespace SharedDataAccess.TaxCollections
{
    public class GetREPropertyHasNewOwnerHandler : CommandHandler<GetREPropertyHasNewOwner,bool>
    {
        private IRealEstateContext realEstateContext;

        public GetREPropertyHasNewOwnerHandler(IRealEstateContext realEstateContext)
        {
            this.realEstateContext = realEstateContext;
        }
        protected override bool Handle(GetREPropertyHasNewOwner command)
        {
            if (realEstateContext.SRMasters.Any(m =>
                m.Rsaccount == command.Account && m.Rscard == 1 && m.SaleDate > command.EffectiveDate))
            {
                return true;
            }

            if (realEstateContext.PreviousOwners.Any(m =>
                m.Account == command.Account && m.SaleDate > command.EffectiveDate))
            {
                return true;
            }

            return false;
        }
    }
}