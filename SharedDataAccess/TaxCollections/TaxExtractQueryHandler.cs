﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.TaxExtract;

namespace SharedDataAccess.TaxCollections
{
    public class TaxExtractQueryHandler : IQueryHandler<TaxExtractAccountQuery, IEnumerable<TaxExtractAccountData>>
    {
        private ITrioContextFactory trioContextFactory;

        public TaxExtractQueryHandler(ITrioContextFactory trioContextFactory)
        {
            this.trioContextFactory = trioContextFactory;
        }

        public IEnumerable<TaxExtractAccountData> ExecuteQuery(TaxExtractAccountQuery query)
        {
            switch (query.BillType)
            {
                case PropertyTaxBillType.Personal:
                    return HandlePersonalPropertyQuery(query);
                    break;
                default:
                    return HandleRealEstateQuery(query);
                    break;
            }
        }

        private IEnumerable<TaxExtractAccountData> HandleRealEstateQuery(TaxExtractAccountQuery query)
        {
            var collectionsContext = trioContextFactory.GetTaxCollectionsContext();
            var realEstateContext = trioContextFactory.GetRealEstateContext();
            var testYear = (query.BillingYear.ToString() + "9").ToIntegerValue();
            var accounts = collectionsContext.RealEstateMasterBillingMasterViews.Where(x => x.BillingYear <= testYear && x.BillingType == "RE");
            
            if (query.TranCode != 0)
            {
                accounts = accounts.Where(x => x.TranCode == query.TranCode);
            }

            var accountResult = new List<int>();
            accountResult = accounts.Select(x => x.Account ?? 0).Distinct().ToList();

            var resultQuery = realEstateContext.REMasters
                .Where(z => accountResult.Any(a => a == z.Rsaccount) && z.Rscard == 1).GroupJoin(
                    realEstateContext.BookPages.OrderBy(d => d.Card).ThenBy(e => e.Line),
                    a => a.Rsaccount,
                    b => b.Account,
                    (m, bp) => new {Master = m, BookPage = bp.FirstOrDefault()});
                
            return resultQuery.Select(x => new TaxExtractAccountData
            {
                Account = x.Master.Rsaccount ?? 0,
                Acres = (x.Master.Piacres ?? 0).ToDecimal(),
                Book = x.BookPage == null ? "" : x.BookPage.Book ?? "",
                Page = x.BookPage == null ? "" : x.BookPage.Page ?? "",
                LastBuildingValue = x.Master.LastLandVal ?? 0 + x.Master.LastBldgVal ?? 0,
                Ref1 = x.Master.Rsref1,
                MapLot = x.Master.RsmapLot,
                StreetName = x.Master.RslocStreet,
                StreetNumber = x.Master.RslocNumAlph
            }).OrderBy(y => y.Account).ToList();
        }

        protected class AccountCard
        {
            public int Account { get; set; }
            public int Card { get; set; }
        }

        private IEnumerable<TaxExtractAccountData> HandlePersonalPropertyQuery(TaxExtractAccountQuery query)
        {
            var collectionsContext = trioContextFactory.GetTaxCollectionsContext();
            var testYear = (query.BillingYear.ToString() + "9").ToIntegerValue();
            var results = collectionsContext.PersonalPropertyMasterBillingMasterViews.Where(x => x.BillingYear <= testYear);

            if (query.TranCode != 0)
            {
                results = results.Where(x => x.TranCode == query.TranCode);
            }

            return results.Select(x => new TaxExtractAccountData
            {
                Account = x.Account ?? 0,
                Acres = 0,
                Book = "",
                Page = "",
                LastBuildingValue = 0,
                Ref1 = "",
                MapLot = "",
                StreetName = x.StreetName,
                StreetNumber = x.streetnumber == null ? "" : ((int)x.streetnumber).ToString() 

            }).Distinct().OrderBy(y => y.Account).ToList();
        }
    }
}
