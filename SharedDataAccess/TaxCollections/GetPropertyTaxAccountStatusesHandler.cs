﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Commands;

namespace SharedDataAccess.TaxCollections
{
    public class GetPropertyTaxAccountStatusesHandler : CommandHandler<GetPropertyTaxAccountStatuses>
    {
        private TaxCollectionsContext collectionsContext;
        public GetPropertyTaxAccountStatusesHandler(TaxCollectionsContext collectionsContext)
        {
            this.collectionsContext = collectionsContext;
        }

    protected override void Handle(GetPropertyTaxAccountStatuses command)
        {
            
        }
    }
}