﻿using System.Linq;
using SharedApplication;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Queries;

namespace SharedDataAccess.TaxCollections
{
    public class CollectionsNoteQueryHandler : IQueryHandler<CollectionsNoteQuery,Note>
    {
        private ITaxCollectionsContext clContext;
        public CollectionsNoteQueryHandler(ITaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }

        public Note ExecuteQuery(CollectionsNoteQuery query)
        {
            switch (query.BillType)
            {
                case PropertyTaxBillType.Real:
                    return clContext.Notes.FirstOrDefault(n =>
                        n.Account == query.Account && n.AccountTypeAbbreviation == "RE");
                    break;
                case PropertyTaxBillType.Personal:
                    return clContext.Notes.FirstOrDefault(n =>
                        n.Account == query.Account && n.AccountTypeAbbreviation == "PP");
                    break;
            }

            return null;
        }
    }
}