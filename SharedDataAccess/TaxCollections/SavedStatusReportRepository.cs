﻿
using Microsoft.EntityFrameworkCore;
using SharedDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Models;

namespace SharedDataAccess.TaxCollections
{
    public class SavedStatusReportRepository :  Repository<SavedStatusReport>, ISavedStatusReportsRepository
    {
        private TaxCollectionsContext collectionsContext;
        public SavedStatusReportRepository(TaxCollectionsContext context) : base(context)
        {
            
            collectionsContext = context;
        }


        public IEnumerable<DescriptionIDPair> GetDescriptions(string reportType)
        {
            return collectionsContext.SavedStatusReports.Where(o => o.Type == reportType).Select(o => new DescriptionIDPair() { Description = o.ReportName, ID = o.ID }).ToList();
        }

        //public IEnumerable<SavedStatusReport> Find(Expression<Func<SavedStatusReport, bool>> predicate)
        //{
        //    throw new NotImplementedException();
        //}

        //public SavedStatusReport Get(int id)
        //{
        //    clsDRWrapper rs = new clsDRWrapper();
        //    rs.OpenRecordset("select * from savedstatusreports where id = " + id.ToString(), "Collections");
        //    if (!rs.EndOfFile())
        //    {
        //        return new SavedStatusReport()
        //        {
        //             ExcludePaid = rs.Get_Fields_Boolean("ExcludePaid"),
        //             FieldConstraint0 = rs.Get_Fields_String("FieldConstraint0"),
        //             FieldConstraint1 = rs.Get_Fields_String("FieldConstraint1"),
        //             FieldConstraint2 = rs.Get_Fields_String("FieldConstraint2"),
        //             FieldConstraint3 = rs.Get_Fields_String("FieldConstraint3"),
        //             FieldConstraint4 = rs.Get_Fields_String("FieldConstraint4"),
        //             FieldConstraint5 = rs.Get_Fields_String("FieldConstraint5"),
        //             FieldConstraint6 = rs.Get_Fields_String("FieldConstraint6"),
        //             FieldConstraint7 = rs.Get_Fields_String("FieldConstraint7"),
        //             FieldConstraint8 = rs.Get_Fields_String("FieldConstraint8"),
        //             FieldConstraint9 = rs.Get_Fields_String("FieldConstraint9"),
        //             ID = rs.Get_Fields_Int32("ID"),
        //             LastUpdated = rs.Get_Fields_DateTime("LastUpdated"),
        //             ReportName = rs.Get_Fields_String("ReportName"),
        //             ShowCurrentInterest = rs.Get_Fields_Boolean("ShowCurrentInterest"),
        //             ShowPayments = rs.Get_Fields_Boolean("ShowPayments"),
        //             ShowSummaryOnly = rs.Get_Fields_Boolean("ShowSummaryOnly"),
        //             SortSelection = rs.Get_Fields_String("SortSelection"),
        //             SQL = rs.Get_Fields_String("SQL"),
        //             Type = rs.Get_Fields_String("Type"),
        //             WhereSelection = rs.Get_Fields_String("WhereSelection")
        //        };
        //    }
        //    return null;
        //}

        //public IEnumerable<SavedStatusReport> GetAll()
        //{
        //    throw new NotImplementedException();
        //}

        public void Remove(int id)
        {
            this.repoContext.Database.ExecuteSqlCommand("Delete from SavedStatusReports where id = " + id.ToString());
        }

        
    }
}
