﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.AccountPayment;

namespace SharedDataAccess.TaxCollections
{
    public class DeleteCLBatchHandler : CommandHandler<DeleteCLBatch>
    {
        private TaxCollectionsContext clContext;

        public DeleteCLBatchHandler(TaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override void Handle(DeleteCLBatch command)
        {
            clContext.Database.ExecuteSqlCommand("Delete from BatchRecover where BatchIdentifier = '" +
                                                 command.BatchIdentifier + "'");
        }
    }
}