﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Models;
using SharedDataAccess.TaxCollections.Configuration;
using DischargeNeededConfiguration = SharedDataAccess.TaxCollections.Configuration.DischargeNeededConfiguration;
using PaymentRecConfiguration = SharedDataAccess.TaxCollections.Configuration.PaymentRecConfiguration;

namespace SharedDataAccess.TaxCollections
{
    public class TaxCollectionsContext : DataContext<TaxCollectionsContext>, ITaxCollectionsContext
    {
        public DbSet<BillingMaster> BillingMasters { get; set; }
        public DbSet<LienRec> LienRecords { get; set; }
        public DbSet<PaymentRec> PaymentRecords { get; set; }
        public DbSet<RateRec> RateRecords { get; set; }
        public DbSet<SavedStatusReport> SavedStatusReports { get; set; }
        public DbSet<CollectionSetting> CollectionSettings { get; set; }
        public DbSet<TaxClub> TaxClubs { get; set; }
        public DbSet<CollectionsComment> CollectionsComments { get; set; }
        public DbSet<ControlDischargeNotice> ControlDischargeNotices { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<DischargeNeeded> NeededDischarges { get; set; }
        public DbSet<ControlLienProcess> ControlLienProcesses { get; set; }
        public DbSet<BatchRecover> BatchRecovers { get; set; }
        public DbSet<PartialPaymentWaiver> PartialPaymentWaivers { get; set; }
        public DbQuery<RealEstateMasterBillingMasterView> RealEstateMasterBillingMasterViews { get; set; }
        public DbQuery<PersonalPropertyMasterBillingMasterView> PersonalPropertyMasterBillingMasterViews { get; set; }
        public DbQuery< PersonalPropertyBillAccountPartyView> PersonalPropertyBillAccountParties { get; set; }
        public DbQuery<RealEstateBillAccountPartyView> RealEstateBillAccountParties { get; set; }
        IQueryable<BillingMaster> ITaxCollectionsContext.BillingMasters => BillingMasters;
        IQueryable<LienRec> ITaxCollectionsContext.LienRecords => LienRecords;
        IQueryable<PaymentRec> ITaxCollectionsContext.PaymentRecords => PaymentRecords;
        IQueryable<RateRec> ITaxCollectionsContext.RateRecords => RateRecords;
        IQueryable<SavedStatusReport> ITaxCollectionsContext.SavedStatusReports => SavedStatusReports;
        IQueryable<TaxClub> ITaxCollectionsContext.TaxClubs => TaxClubs;
        IQueryable<CollectionSetting> ITaxCollectionsContext.CollectionSettings => CollectionSettings;
        IQueryable<CollectionsComment> ITaxCollectionsContext.CollectionsComments => CollectionsComments;
        IQueryable<Note> ITaxCollectionsContext.Notes => Notes;
        IQueryable<ControlDischargeNotice> ITaxCollectionsContext.ControlDischargeNotices => ControlDischargeNotices;
        IQueryable<DischargeNeeded> ITaxCollectionsContext.NeededDischarges => NeededDischarges;

        IQueryable<ControlLienProcess> ITaxCollectionsContext.ControlLienProcesses => ControlLienProcesses;
        IQueryable<BatchRecover> ITaxCollectionsContext.BatchRecovers => BatchRecovers;
        IQueryable<PartialPaymentWaiver> ITaxCollectionsContext.PartialPaymentWaivers => PartialPaymentWaivers;
        IQueryable<RealEstateMasterBillingMasterView> ITaxCollectionsContext.RealEstateMasterBillingMasterViews => RealEstateMasterBillingMasterViews;
        IQueryable<PersonalPropertyMasterBillingMasterView> ITaxCollectionsContext.PersonalPropertyMasterBillingMasterViews => PersonalPropertyMasterBillingMasterViews;

        IQueryable<PersonalPropertyBillAccountPartyView> ITaxCollectionsContext.PersonalPropertyBillAccountParties =>
            PersonalPropertyBillAccountParties;
        IQueryable<RealEstateBillAccountPartyView> ITaxCollectionsContext.RealEstateBillAccountParties =>
            RealEstateBillAccountParties;
        public TaxCollectionsContext(DbContextOptions<TaxCollectionsContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new BillingMasterConfiguration());
            modelBuilder.ApplyConfiguration(new RateRecConfiguration());
            modelBuilder.ApplyConfiguration(new TaxClubConfiguration());
            modelBuilder.ApplyConfiguration(new LienRecConfiguration());
            modelBuilder.ApplyConfiguration(new PaymentRecConfiguration());
            modelBuilder.ApplyConfiguration(new SavedStatusReportConfiguration());
            modelBuilder.ApplyConfiguration(new CollectionSettingConfiguration());
            modelBuilder.ApplyConfiguration(new CollectionsCommentConfiguration());
            modelBuilder.ApplyConfiguration(new NotesConfiguration());
            modelBuilder.ApplyConfiguration(new DischargeNeededConfiguration());
            modelBuilder.ApplyConfiguration(new ControlDischargeNoticeConfiguration());
            modelBuilder.ApplyConfiguration(new ControlLienProcessConfiguration());
            modelBuilder.ApplyConfiguration(new BatchRecoverConfiguration());
            modelBuilder.ApplyConfiguration(new PartialPaymentWaiverConfiguration());
            modelBuilder.ApplyConfiguration(new RealEstateMasterBillingMasterViewConfiguration());
            modelBuilder.ApplyConfiguration(new PersonalPropertyMasterBillingMasterViewConfiguration());

            modelBuilder.Entity<RateRec>()
	            .HasMany(c => c.Bills)
	            .WithOne(e => e.RateRec)
	            .HasForeignKey(e => e.RateRecId);

            modelBuilder.Entity<RateRec>()
	            .HasMany(c => c.Liens)
	            .WithOne(e => e.RateRec)
	            .HasForeignKey(e => e.RateKey);
            modelBuilder.ApplyConfiguration(new PersonalPropertyBillAccountPartyViewConfiguration());
            modelBuilder.ApplyConfiguration(new RealEstateBillAccountPartyViewConfiguration());
        }

    }
}
