﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.StatusList;

namespace SharedDataAccess.TaxCollections
{
    public class PropertyTaxStatusQueryHandler : PropertyTaxBillQuerier, IPropertyTaxStatusQueryHandler
    {
        private IDateTimeService dateTimeService;
        public PropertyTaxStatusQueryHandler(ITrioContextInterfaceFactory contextFactory,IDateTimeService dateTimeService)
        {
            // collectionsRepoFactory = taxCollectionsRepoFactory;
            this.dateTimeService = dateTimeService;
            this.contextFactory = contextFactory;
        }
        public RealEstateAccountBill GetReal(int id)
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();
            var billAccountParty = context.RealEstateBillAccountParties.FirstOrDefault(b => b.Id == id);
            
            if (billAccountParty == null)
            {
                return null;
            }

            var lienRateKey = billAccountParty.LienRateKey??0;
            List<RateRec> rateRecs;
            if (lienRateKey > 0)
            {
                rateRecs = context.RateRecords
                    .Where(r => r.Id == billAccountParty.RateKey || r.Id == lienRateKey).ToList();
            }
            else
            {
                rateRecs = context.RateRecords.Where(r => r.Id == billAccountParty.RateKey).ToList();
            }

            return MakeRealEstateBill(billAccountParty,rateRecs,dateTimeService.Now(),false);
        }

        public PersonalPropertyAccountBill GetPersonal(int id,bool calculate)
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();
            var billAccountParty = context.PersonalPropertyBillAccountParties.FirstOrDefault(b => b.Id == id);

            if (billAccountParty == null)
            {
                return null;
            }

            var rateRec = context.RateRecords.Where(r => r.Id == billAccountParty.RateKey).ToList();
            return MakePersonalPropertyBill(billAccountParty, rateRec,dateTimeService.Now(),false);
        }

        public IEnumerable<RealEstateAccountBill> GetAllReal()
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();
            var rateRecs = context.RateRecords.ToList();
            return FindReal(null);
        }

        //public IEnumerable<PropertyTaxStatusListDetailItem> GetAll(bool calculate)
        //{
        //    throw new NotImplementedException();
        //}
        public IEnumerable<RealEstateAccountBill> FindReal(Expression<Func<RealEstateBillAccountPartyView, bool>> predicate)
        {
            return FindRealAsOf(predicate, false, dateTimeService.Now());
        }

        public IEnumerable<RealEstateAccountBill> FindRealAsOf(
            Expression<Func<RealEstateBillAccountPartyView, bool>> predicate, bool snapshotAsOfEffectiveDate, DateTime? effectiveDate)
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();

            var rateRecs = context.RateRecords.ToList();
            var accountBills = new List<RealEstateAccountBill>();
            var accountBillParties = context.RealEstateBillAccountParties;
            if (predicate != null)
            {
                accountBillParties = accountBillParties.Where(predicate);
            }

            var accountBillPartiesList = accountBillParties.ToList();
            foreach (var accountBillParty in accountBillPartiesList)
            {
                accountBills.Add(MakeRealEstateBill(accountBillParty, rateRecs,effectiveDate??dateTimeService.Now(),snapshotAsOfEffectiveDate));
            }

            return accountBills;
        }

        public Queue<RealEstateAccountBill> FindQueueOfRealAsOf(
            Expression<Func<RealEstateBillAccountPartyView, bool>> predicate, bool snapshotAsOfEffectiveDate,
            DateTime? effectiveDate)
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();

            var rateRecs = context.RateRecords.ToList();
            var accountBills = new Queue<RealEstateAccountBill>();
            var accountBillParties = context.RealEstateBillAccountParties;
            if (predicate != null)
            {
                accountBillParties = accountBillParties.Where(predicate);
            }

            var accountBillPartiesList = accountBillParties.ToList();
            foreach (var accountBillParty in accountBillPartiesList)
            {
                accountBills.Enqueue(MakeRealEstateBill(accountBillParty, rateRecs, effectiveDate ?? dateTimeService.Now(), snapshotAsOfEffectiveDate));
            }

            return accountBills;
        }
        public IEnumerable<PersonalPropertyAccountBill> FindPersonal(
            Expression<Func<PersonalPropertyBillAccountPartyView, bool>> predicate)
        {
            return FindPersonalAsOf(predicate, false, dateTimeService.Now());
        }
        public IEnumerable<PersonalPropertyAccountBill> FindPersonalAsOf(
            Expression<Func<PersonalPropertyBillAccountPartyView, bool>> predicate, bool snapshotAsOfEffectiveDate, DateTime? effectiveDate)
        {
            var context = contextFactory.GetTaxCollectionsContextInterface();
            var rateRecs = context.RateRecords.ToList();
            var accountBills = new List<PersonalPropertyAccountBill>();
            var accountBillParties = context.PersonalPropertyBillAccountParties;
            if (predicate != null)
            {
                accountBillParties = accountBillParties.Where(predicate);
            }

            var accountBillPartiesList = accountBillParties.ToList();
            foreach (var accountBillParty in accountBillPartiesList)
            {
                accountBills.Add(MakePersonalPropertyBill(accountBillParty,rateRecs,effectiveDate??dateTimeService.Now(),snapshotAsOfEffectiveDate));
            }

            return accountBills;
        }

        //protected PropertyTaxBill MakePropertyTaxBill(BillingMaster bill)
        //{
        //    PropertyTaxBill propertyTaxBill;
        //    if (bill.BillingType.ToLower() != "pp")
        //    {
        //        propertyTaxBill = MakeRealEstateBill(bill);
        //    }
        //    else
        //    {
        //        propertyTaxBill = MakePersonalPropertyBill(bill);
        //    }

        //    propertyTaxBill.Account = bill.Account;
        //    propertyTaxBill.BillNumber = bill.BillNumber();
        //    propertyTaxBill.ID = bill.ID;
        //    propertyTaxBill.LienID = bill.LienRecordNumber ?? 0;
        //    propertyTaxBill.InterestAppliedThroughDate = bill.InterestAppliedThroughDate ?? DateTime.MinValue;
        //    propertyTaxBill.PreviousInterestAppliedDate = bill.PreviousInterestAppliedDate ?? DateTime.MinValue;
        //    propertyTaxBill.OriginalInterestAppliedThroughDate = propertyTaxBill.InterestAppliedThroughDate;
        //    propertyTaxBill.OriginalPreviousInterestDate = propertyTaxBill.PreviousInterestAppliedDate;
        //    propertyTaxBill.RateId = bill.RateRecId ?? 0;
        //    propertyTaxBill.TaxYear = bill.TaxYear();
        //    propertyTaxBill.MailingAddress1 = bill.Address1;
        //    propertyTaxBill.MailingAddress2 = bill.Address2;
        //    propertyTaxBill.CityStateZip = bill.Address3;
        //    propertyTaxBill.MailingAddress3 = bill.MailingAddress3;
        //    propertyTaxBill.Name1 = bill.Name1;
        //    propertyTaxBill.Name2 = bill.Name2;
        //    propertyTaxBill.StreetName = bill.StreetName;
        //    propertyTaxBill.StreetNumber = bill.streetnumber.GetValueOrDefault();
        //    propertyTaxBill.RateRecord = RateRecords.FirstOrDefault(r => r.id == propertyTaxBill.RateId);
        //    propertyTaxBill.TranCode = bill.TranCode.GetValueOrDefault();

        //    if (propertyTaxBill.RateRecord != null)
        //    {
        //        if (propertyTaxBill.RateRecord.Installments.Count > 0)
        //        {
        //            var installment = propertyTaxBill.RateRecord.Installments.Where(i => i.InstallmentNumber == 1);
        //            propertyTaxBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment() { OriginalAmount = bill.TaxDue1.GetValueOrDefault(), AdjustedAmount = bill.TaxDue1.GetValueOrDefault(), DueDate = propertyTaxBill.RateRecord.Installments[0].DueDate, InstallmentNumber = 1, InterestDate = propertyTaxBill.RateRecord.Installments[0].InterestStartDate });
        //            if (propertyTaxBill.RateRecord.Installments.Count > 1)
        //            {
        //                propertyTaxBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment() { OriginalAmount = bill.TaxDue2.GetValueOrDefault(), AdjustedAmount = bill.TaxDue2.GetValueOrDefault(), DueDate = propertyTaxBill.RateRecord.Installments[1].DueDate, InstallmentNumber = 2, InterestDate = propertyTaxBill.RateRecord.Installments[1].InterestStartDate });
        //                if (propertyTaxBill.RateRecord.Installments.Count > 2)
        //                {
        //                    propertyTaxBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment() { OriginalAmount = bill.TaxDue3.GetValueOrDefault(), AdjustedAmount = bill.TaxDue3.GetValueOrDefault(), DueDate = propertyTaxBill.RateRecord.Installments[2].DueDate, InstallmentNumber = 3, InterestDate = propertyTaxBill.RateRecord.Installments[2].InterestStartDate });
        //                    if (propertyTaxBill.RateRecord.Installments.Count > 3)
        //                    {
        //                        propertyTaxBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment() { OriginalAmount = bill.TaxDue4.GetValueOrDefault(), AdjustedAmount = bill.TaxDue4.GetValueOrDefault(), DueDate = propertyTaxBill.RateRecord.Installments[3].DueDate, InstallmentNumber = 4, InterestDate = propertyTaxBill.RateRecord.Installments[3].InterestStartDate });
        //                    }
        //                }
        //            }
        //        }


        //    }
        //    if (propertyTaxBill.LienID > 0)
        //    {
        //        propertyTaxBill.Lien = GetLien(propertyTaxBill.LienID, propertyTaxBill.Account.GetValueOrDefault(), propertyTaxBill.ID);
        //    }

        //    propertyTaxBill.TaxClub = GetTaxClub(bill.ID);
        //    var payments = GetBillPayments(propertyTaxBill.ID);
        //    var nonAbatements = payments.GetAll().WithoutAbatements();
        //    propertyTaxBill.BillSummary.InterestCharged = nonAbatements.InterestChargedPayments().TotalChargedInterest();

        //    propertyTaxBill.BillSummary.DemandFeesCharged = nonAbatements.TotalCostsCharged();
        //    propertyTaxBill.BillSummary.DemandFeesPaid = nonAbatements.TotalCostsPaid();

        //    propertyTaxBill.BillSummary.PrincipalPaid = nonAbatements.TotalPrincipalPaid();
        //    propertyTaxBill.BillSummary.InterestPaid = nonAbatements.TotalInterestPaid();
        //    //var paymentSummary = payments.GetAll().Abatements().SummarizeAsPaymentSummary();
        //    //propertyTaxBill.BillSummary.PrincipalPaid -= paymentSummary.Principal;
        //    //propertyTaxBill.BillSummary.PrincipalCharged -= paymentSummary.Principal;
        //    propertyTaxBill.AddPayments(GetBillPayments(propertyTaxBill.ID));
        //    return propertyTaxBill;
        //}

        protected RealEstateAccountBill MakeRealEstateBill(RealEstateBillAccountPartyView billAccountParty, IEnumerable<RateRec> rateRecs, DateTime effectiveDate, bool snapshotAsOfEffectiveDate)
        {
            if (billAccountParty == null)
            {
                return null;
            }

            var accountBill = billAccountParty.ToAccountBill();
            var reBill = accountBill.Bill;
            var rateRec = (billAccountParty.RateKey??0) > 0 ? rateRecs.FirstOrDefault(r => r.Id == billAccountParty.RateKey.GetValueOrDefault()) : null;
            reBill.RateRecord = rateRec.ToTaxRateInfo();
            if (reBill.RateRecord != null)
            {
                if (reBill.RateRecord.Installments.Count > 0)
                {
                    if (!snapshotAsOfEffectiveDate ||
                        !effectiveDate.IsEarlierThan(billAccountParty.TransferFromBillingDateFirst ?? DateTime.MinValue)
                    )
                    {
                        var installment = reBill.RateRecord.Installments.Where(i => i.InstallmentNumber == 1);
                        reBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment()
                        {
                            OriginalAmount = billAccountParty.TaxDue1.GetValueOrDefault(),
                            AdjustedAmount = billAccountParty.TaxDue1.GetValueOrDefault(),
                            DueDate = reBill.RateRecord.Installments[0].DueDate, InstallmentNumber = 1,
                            InterestDate = reBill.RateRecord.Installments[0].InterestStartDate
                        });
                        if (reBill.RateRecord.Installments.Count > 1)
                        {
                            reBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment()
                            {
                                OriginalAmount = billAccountParty.TaxDue2.GetValueOrDefault(),
                                AdjustedAmount = billAccountParty.TaxDue2.GetValueOrDefault(),
                                DueDate = reBill.RateRecord.Installments[1].DueDate, InstallmentNumber = 2,
                                InterestDate = reBill.RateRecord.Installments[1].InterestStartDate
                            });
                            if (reBill.RateRecord.Installments.Count > 2)
                            {
                                reBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment()
                                {
                                    OriginalAmount = billAccountParty.TaxDue3.GetValueOrDefault(),
                                    AdjustedAmount = billAccountParty.TaxDue3.GetValueOrDefault(),
                                    DueDate = reBill.RateRecord.Installments[2].DueDate, InstallmentNumber = 3,
                                    InterestDate = reBill.RateRecord.Installments[2].InterestStartDate
                                });
                                if (reBill.RateRecord.Installments.Count > 3)
                                {
                                    reBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment()
                                    {
                                        OriginalAmount = billAccountParty.TaxDue4.GetValueOrDefault(),
                                        AdjustedAmount = billAccountParty.TaxDue4.GetValueOrDefault(),
                                        DueDate = reBill.RateRecord.Installments[3].DueDate, InstallmentNumber = 4,
                                        InterestDate = reBill.RateRecord.Installments[3].InterestStartDate
                                    });
                                }
                            }
                        }
                    }
                }


            }

            //if (reBill.Lien != null)
            //{
            //    var lienRate = (reBill.Lien.RateId ?? 0) > 0
            //        ? rateRecs.FirstOrDefault(r => r.Id == reBill.Lien.RateId.GetValueOrDefault())
            //        : null;
            //    reBill.Lien.RateRecord = lienRate.ToTaxRateInfo();

            //}
            Payments payments;
            if (snapshotAsOfEffectiveDate)
            {
                payments = GetBillPaymentsAsOf(reBill.ID,effectiveDate);
            }
            else
            {
                payments = GetBillPayments(reBill.ID);
            }
            
            //var nonAbatements = payments.GetAll().WithoutAbatements();
            //reBill.BillSummary.InterestCharged = nonAbatements.InterestChargedPayments().TotalChargedInterest();
            reBill.BillSummary.PrincipalCharged = reBill.TaxInstallments.GetEffectiveCharged();
            //reBill.BillSummary.DemandFeesCharged = nonAbatements.TotalCostsCharged();
            //reBill.BillSummary.DemandFeesPaid = nonAbatements.TotalCostsPaid();

            //reBill.BillSummary.PrincipalPaid = nonAbatements.TotalPrincipalPaid();
            //reBill.BillSummary.InterestPaid = nonAbatements.TotalInterestPaid();
           // reBill.AddPayments(payments);
           reBill.ApplyPayments(payments);
            if (reBill.HasLien())
            {
                if (!snapshotAsOfEffectiveDate || !reBill.Lien.DateCreated.HasValue || !reBill.Lien.DateCreated.Value.IsLaterThan(effectiveDate))
                {
                    rateRec = rateRecs.FirstOrDefault(r => r.Id == reBill.Lien.RateId);
                    reBill.Lien.RateRecord = rateRec.ToTaxRateInfo();
                    var lienPayments = GetLienPayments(reBill.LienID);
                    reBill.Lien.ApplyPayments(lienPayments);
                }
                else
                {
                    reBill.LienID = 0;
                    reBill.Lien = null;
                }
            }

            return accountBill;
        }

        protected PersonalPropertyAccountBill MakePersonalPropertyBill(
            PersonalPropertyBillAccountPartyView billAccountParty, IEnumerable<RateRec> rateRecs,DateTime effectiveDate, bool snapshotAsOfEffectiveDate)
        {
            if (billAccountParty == null)
            {
                return null;
            }

            var accountBill = billAccountParty.ToAccountBill();
            var ppBill = accountBill.Bill;
            var rateRec = rateRecs.FirstOrDefault(r => r.Id == billAccountParty.RateKey.GetValueOrDefault());
            ppBill.RateRecord = rateRec.ToTaxRateInfo();
            if (ppBill.RateRecord != null)
            {
                if (ppBill.RateRecord.Installments.Count > 0)
                {
                    if (!snapshotAsOfEffectiveDate ||
                        !effectiveDate.IsEarlierThan(billAccountParty.TransferFromBillingDateFirst ?? DateTime.MinValue)
                    )
                    {
                        var installment = ppBill.RateRecord.Installments.Where(i => i.InstallmentNumber == 1);
                        ppBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment()
                        {
                            OriginalAmount = billAccountParty.TaxDue1.GetValueOrDefault(),
                            AdjustedAmount = billAccountParty.TaxDue1.GetValueOrDefault(),
                            DueDate = ppBill.RateRecord.Installments[0].DueDate,
                            InstallmentNumber = 1,
                            InterestDate = ppBill.RateRecord.Installments[0].InterestStartDate
                        });
                        if (ppBill.RateRecord.Installments.Count > 1)
                        {
                            ppBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment()
                            {
                                OriginalAmount = billAccountParty.TaxDue2.GetValueOrDefault(),
                                AdjustedAmount = billAccountParty.TaxDue2.GetValueOrDefault(),
                                DueDate = ppBill.RateRecord.Installments[1].DueDate,
                                InstallmentNumber = 2,
                                InterestDate = ppBill.RateRecord.Installments[1].InterestStartDate
                            });
                            if (ppBill.RateRecord.Installments.Count > 2)
                            {
                                ppBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment()
                                {
                                    OriginalAmount = billAccountParty.TaxDue3.GetValueOrDefault(),
                                    AdjustedAmount = billAccountParty.TaxDue3.GetValueOrDefault(),
                                    DueDate = ppBill.RateRecord.Installments[2].DueDate,
                                    InstallmentNumber = 3,
                                    InterestDate = ppBill.RateRecord.Installments[2].InterestStartDate
                                });
                                if (ppBill.RateRecord.Installments.Count > 3)
                                {
                                    ppBill.TaxInstallments.AddInstallment(new PropertyTaxInstallment()
                                    {
                                        OriginalAmount = billAccountParty.TaxDue4.GetValueOrDefault(),
                                        AdjustedAmount = billAccountParty.TaxDue4.GetValueOrDefault(),
                                        DueDate = ppBill.RateRecord.Installments[3].DueDate,
                                        InstallmentNumber = 4,
                                        InterestDate = ppBill.RateRecord.Installments[3].InterestStartDate
                                    });
                                }
                            }
                        }
                    }
                }
            }

            Payments payments;
            if (snapshotAsOfEffectiveDate)
            {
                payments = GetBillPaymentsAsOf(ppBill.ID, effectiveDate);
            }
            else
            {
                payments = GetBillPayments(ppBill.ID);
            }

            var nonAbatements = payments.GetAll().WithoutAbatements();
           // ppBill.BillSummary.InterestCharged = nonAbatements.InterestChargedPayments().TotalChargedInterest();
            ppBill.BillSummary.PrincipalCharged = ppBill.TaxInstallments.GetEffectiveCharged();
            //ppBill.BillSummary.DemandFeesCharged = nonAbatements.TotalCostsCharged();
           // ppBill.BillSummary.DemandFeesPaid = nonAbatements.TotalCostsPaid();

           // ppBill.BillSummary.PrincipalPaid = nonAbatements.TotalPrincipalPaid();
           // ppBill.BillSummary.InterestPaid = nonAbatements.TotalInterestPaid();
            //ppBill.AddPayments(payments);
            ppBill.ApplyPayments(payments);
            return accountBill;
        }
    }
}