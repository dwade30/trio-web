﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using SharedApplication.CentralData;
using SharedApplication.CentralParties;
using SharedApplication.CentralParties.Models;

namespace SharedDataAccess.CentralParties
{
    public partial class CentralPartyContext : DbContext, ICentralPartyContext
    {
        public CentralPartyContext()
        {
        }

        public CentralPartyContext(DbContextOptions<CentralPartyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<PartyAddress> Addresses { get; set; }
        public virtual DbSet<CentralComment> CentralComments { get; set; }
        public virtual DbSet<CentralPhoneNumber> CentralPhoneNumbers { get; set; }
        public virtual DbSet<ContactPhoneNumber> ContactPhoneNumbers { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Dbversion> Dbversion { get; set; }
        public virtual DbSet<CentralParty> Parties { get; set; }

        IQueryable<PartyAddress> ICentralPartyContext.Addresses
        {
            get => Addresses;
        }
        IQueryable<CentralComment> ICentralPartyContext.CentralComments
        {
            get => CentralComments;
        }
        IQueryable<CentralPhoneNumber> ICentralPartyContext.CentralPhoneNumbers
        {
            get => CentralPhoneNumbers;
        }
        IQueryable<ContactPhoneNumber> ICentralPartyContext.ContactPhoneNumbers
        {
            get => ContactPhoneNumbers;
        }
        IQueryable<Contact> ICentralPartyContext.Contacts
        {
            get => Contacts;
        }
        IQueryable<Dbversion> ICentralPartyContext.Dbversion
        {
            get => Dbversion;
        }
        IQueryable<CentralParty> ICentralPartyContext.Parties
        {
            get => Parties;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {            
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}