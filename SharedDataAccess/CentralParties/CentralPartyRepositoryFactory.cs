﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.CentralParties;

namespace SharedDataAccess.CentralParties
{
    public class CentralPartyRepositoryFactory : RepositoryFactory,ICentralPartyRepositoryFactory
    {
        public CentralPartyRepositoryFactory(DbContext context) : base(context)
        {
        }
    }
}