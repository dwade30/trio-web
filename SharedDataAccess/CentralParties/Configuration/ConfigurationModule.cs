﻿using Autofac;
using SharedApplication.CentralParties;

namespace SharedDataAccess.CentralParties.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
           RegisterCentralPartyRepositoryFactory(ref builder);
           RegisterCentralPartyContext(ref builder);
        }

        private void RegisterCentralPartyContext(ref ContainerBuilder builder)
        {
                builder.Register(f =>
                {
                    var tcf = f.Resolve<ITrioContextFactory>();
                    return tcf.GetCentralPartyContext();
                }).As<ICentralPartyContext>();
        }

        private void RegisterCentralPartyRepositoryFactory(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return new CentralPartyRepositoryFactory(tcf.GetCentralPartyContext());
            }).As<ICentralPartyRepositoryFactory>();
        }
    }
}