﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralParties.Models;

namespace SharedDataAccess.CentralParties.Configuration
{
    public class PartyAddressConfiguration : IEntityTypeConfiguration<PartyAddress>
    {
        public void Configure(EntityTypeBuilder<PartyAddress> builder)
        {
            builder.ToTable("Addresses");
            builder.HasIndex(e => new { e.Id, e.Address1, e.Address2, e.Address3, e.City, e.State, e.Zip, e.Country, e.OverrideName, e.Seasonal, e.ProgModule, e.Comment, e.StartMonth, e.StartDay, e.EndMonth, e.EndDay, e.ModAccountId, e.PartyId, e.AddressType })
                .HasName("_dta_index_Addresses_14_2105058535__K2_K11_1_3_4_5_6_7_8_9_10_12_13_14_15_16_17_18_19");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Address1).HasMaxLength(255);

            builder.Property(e => e.Address2).HasMaxLength(255);

            builder.Property(e => e.Address3).HasMaxLength(255);

            builder.Property(e => e.AddressType).HasMaxLength(255);

            builder.Property(e => e.City).HasMaxLength(255);

            builder.Property(e => e.Comment).HasMaxLength(1024);

            builder.Property(e => e.Country).HasMaxLength(255);

            builder.Property(e => e.ModAccountId).HasColumnName("ModAccountID");

            builder.Property(e => e.OverrideName).HasMaxLength(255);

            builder.Property(e => e.PartyId).HasColumnName("PartyID");

            builder.Property(e => e.ProgModule).HasMaxLength(255);

            builder.Property(e => e.State).HasMaxLength(255);

            builder.Property(e => e.Zip).HasMaxLength(255);

            builder.HasOne(d => d.Party)
                .WithMany(p => p.Addresses)
                .HasForeignKey(d => d.PartyId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("Party_Addresses");
        }
    }
}