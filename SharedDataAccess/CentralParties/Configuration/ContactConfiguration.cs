﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralParties.Models;

namespace SharedDataAccess.CentralParties.Configuration
{
    public class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.ToTable("Contacts");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Comment).HasMaxLength(1024);

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.Email).HasMaxLength(100);

            builder.Property(e => e.Name).HasMaxLength(255);

            builder.Property(e => e.PartyId).HasColumnName("PartyID");

            builder.HasOne(d => d.Party)
                .WithMany(p => p.Contacts)
                .HasForeignKey(d => d.PartyId)
                .HasConstraintName("Contact_Parties");
        }
    }
}