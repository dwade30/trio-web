﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralParties.Models;

namespace SharedDataAccess.CentralParties.Configuration
{
    public class CentralPhoneNumberConfiguration : IEntityTypeConfiguration<CentralPhoneNumber>
    {
        public void Configure(EntityTypeBuilder<CentralPhoneNumber> builder)
        {
            builder.ToTable("CentralPhoneNumbers");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.Extension).HasMaxLength(255);

            builder.Property(e => e.PartyId).HasColumnName("PartyID");

            builder.Property(e => e.PhoneNumber).HasMaxLength(255);

            builder.HasOne(d => d.Party)
                .WithMany(p => p.CentralPhoneNumbers)
                .HasForeignKey(d => d.PartyId)
                .HasConstraintName("CentralPhoneNumber_Parties");
        }
    }
}