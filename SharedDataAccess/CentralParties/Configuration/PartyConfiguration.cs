﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralParties.Models;

namespace SharedDataAccess.CentralParties.Configuration
{
    public class PartyConfiguration : IEntityTypeConfiguration<CentralParty>
    {
        public void Configure(EntityTypeBuilder<CentralParty> builder)
        {
            builder.ToTable("Parties");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.DateCreated).HasColumnType("datetime");

            builder.Property(e => e.Designation).HasMaxLength(50);

            builder.Property(e => e.Email).HasMaxLength(200);

            builder.Property(e => e.FirstName).HasMaxLength(255);

            builder.Property(e => e.LastName).HasMaxLength(100);

            builder.Property(e => e.MiddleName).HasMaxLength(100);

            builder.Property(e => e.WebAddress).HasMaxLength(200);
        }
    }
}