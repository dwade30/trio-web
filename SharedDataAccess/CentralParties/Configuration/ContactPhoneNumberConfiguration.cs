﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralParties.Models;

namespace SharedDataAccess.CentralParties.Configuration
{
    public class ContactPhoneNumberConfiguration : IEntityTypeConfiguration<ContactPhoneNumber>
    {
        public void Configure(EntityTypeBuilder<ContactPhoneNumber> builder)
        {
            builder.ToTable("ContactPhoneNumbers");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ContactId).HasColumnName("ContactID");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.Extension).HasMaxLength(255);

            builder.Property(e => e.PhoneNumber).HasMaxLength(255);

            builder.HasOne(d => d.Contact)
                .WithMany(p => p.ContactPhoneNumbers)
                .HasForeignKey(d => d.ContactId)
                .HasConstraintName("ContactPhoneNumbers_Contacts");
        }
    }
}