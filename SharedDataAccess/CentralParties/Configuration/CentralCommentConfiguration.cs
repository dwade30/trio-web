﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.CentralParties.Models;

namespace SharedDataAccess.CentralParties.Configuration
{
    public class CentralCommentConfiguration : IEntityTypeConfiguration<CentralComment>
    {
        public void Configure(EntityTypeBuilder<CentralComment> builder)
        {
            builder.ToTable("CentralComments");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Comment).HasMaxLength(1024);

            builder.Property(e => e.EnteredBy).HasMaxLength(255);

            builder.Property(e => e.LastModified).HasColumnType("datetime");

            builder.Property(e => e.PartyId).HasColumnName("PartyID");

            builder.Property(e => e.ProgModule).HasMaxLength(255);

            builder.HasOne(d => d.Party)
                .WithMany(p => p.CentralComments)
                .HasForeignKey(d => d.PartyId)
                .HasConstraintName("CentralComments_Parties");
        }
    }
}