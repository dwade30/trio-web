﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication.CentralParties;
using SharedApplication.CentralParties.Commands;
using SharedApplication.CentralParties.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.CentralParties
{
    public class GetCentralPartyHandler : CommandHandler<GetCentralParty,CentralParty>
    {
        private ICentralPartyContext partyContext;
        public GetCentralPartyHandler(ICentralPartyContext partyContext)
        {
            this.partyContext = partyContext;
        }
        protected override CentralParty Handle(GetCentralParty command)
        {
            var party = partyContext.Parties.Where(p => p.Id == command.Id);
            if (command.IncludeChildren)
            {
                party = party.Include(p => p.Addresses).Include(p => p.CentralComments)
                    .Include(p => p.CentralPhoneNumbers).Include(p => p.Contacts);
            }

            return party.FirstOrDefault();
        }
    }
}