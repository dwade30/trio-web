﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.ExpressionVisitors.Internal;
using SharedDataAccess.CentralData;
using SharedDataAccess.PersonalProperty;
using SharedDataAccess.RealEstate;
using SharedDataAccess.TaxCollections;

namespace SharedDataAccess
{
    public class TrioContextFactory : ITrioContextFactory
    {
        private DataContextDetails dataContextDetails;
        private string environmentGroup = "Live";
        public TrioContextFactory(DataContextDetails contextDetails)
        {
            dataContextDetails = contextDetails;
        }

        public RealEstateContext GetRealEstateContext()
        {
            var databaseName = GetFullDatabaseBaseName("RealEstate");
            var optionsBuilder = new DbContextOptionsBuilder<RealEstateContext>();
            optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            return new RealEstateContext(optionsBuilder.Options);
        }

        public TaxCollectionsContext GetTaxCollectionsContext()
        {
            var databaseName = GetFullDatabaseBaseName("Collections");
            var optionsBuilder = new DbContextOptionsBuilder<TaxCollectionsContext>();
            optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            return new TaxCollectionsContext(optionsBuilder.Options);
        }
        private string GetFullDatabaseBaseName(string databaseName)
        {
            return "TRIO_" + dataContextDetails.DataEnvironment + "_" + environmentGroup + "_" + databaseName;
        }

        private string GetConnectionString(string databaseName)
        {
            var stringBuilder = new SqlConnectionStringBuilder();
            stringBuilder.DataSource = dataContextDetails.DataSource;
            stringBuilder.UserID = dataContextDetails.UserID;
            stringBuilder.Password = dataContextDetails.Password;
            stringBuilder.IntegratedSecurity = false;
            stringBuilder.InitialCatalog = databaseName;
            return stringBuilder.ToString();
        }

        public PersonalPropertyContext GetPersonalPropertyContext()
        {
            var databaseName = GetFullDatabaseBaseName("PersonalProperty");
            var optionsBuilder = new DbContextOptionsBuilder<PersonalPropertyContext>();
            optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            return new PersonalPropertyContext(optionsBuilder.Options);
        }

        public CentralDataContext GetCentralDataContext()
        {
            var databaseName = GetFullDatabaseBaseName("CentralData");
            var optionsBuilder = new DbContextOptionsBuilder<CentralDataContext>();
            optionsBuilder.UseSqlServer(GetConnectionString(databaseName));
            return new CentralDataContext(optionsBuilder.Options);
        }
    }
}
