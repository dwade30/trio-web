﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.CodeEnforcement;
using SharedApplication.Messaging;

namespace SharedDataAccess.CodeEnforcement
{
    public class UpdateCodeEnforcementDatabaseHandler : CommandHandler<UpdateCodeEnforcementDatabase,bool>
    {
        protected override bool Handle(UpdateCodeEnforcementDatabase command)
        {
           var updater = new cCodeEnforcementDB(command.SqlConfigInfo,command.DataEnvironment);
           return updater.CheckVersion();
        }
    }
}