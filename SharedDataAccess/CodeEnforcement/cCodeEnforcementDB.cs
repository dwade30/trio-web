﻿using SharedApplication.Models;
using SharedApplication.Versioning;

namespace SharedDataAccess.CodeEnforcement
{
    public class cCodeEnforcementDB : cDatabaseVersionUpdater
    {
        public cCodeEnforcementDB(SQLConfigInfo sqlConfigInfo,string dataEnvironment)
        {
            defaultDatabaseName = "CodeEnforcement";
            dbInfo = sqlConfigInfo;
            DataEnvironment = dataEnvironment;
        }
        protected override bool PerformChecks(cVersionInfo currentVersion)
        {
            //Example:
            //CheckFunction(TestFunction, currentVersion,new cVersionInfo() {Major = 6, Minor = 0, Revision = 1, Build = 0});
            return true;
        }
    }
}