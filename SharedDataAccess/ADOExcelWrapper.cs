﻿using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Text;

namespace SharedDataAccess
{
    public class ADOExcelWrapper
    {        
        private OleDbConnection currentConnection;
        public string CurrentSheetName { get; set; } = "";
        ~ADOExcelWrapper()
        {
            if (currentConnection != null)
            {
                if (currentConnection.State == ConnectionState.Open)
                {
                    currentConnection.Close();
                }
            }
        }
        public void CreateExcelFile(IEnumerable<string> columnCollection, string strFileName, bool useHeader,
            string strSheetName)
        {
            if (columnCollection != null)
            {
                CurrentSheetName = strSheetName;
                if (string.IsNullOrWhiteSpace(CurrentSheetName))
                {
                    CurrentSheetName = "Sheet1";
                }
                var creationCommand = GetCreateCommandFromColumnNames(columnCollection, CurrentSheetName);
                OpenExcelFile(strFileName,useHeader);
                using (var command = new OleDbCommand(creationCommand, currentConnection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void OpenExcelFile(string fileName, bool useHeader)
        {
            var connectionString = GetConnectionString(fileName, useHeader);
            currentConnection = new OleDbConnection(connectionString);
            currentConnection.Open();
        }
        public void AddRow(string sheetName,IEnumerable<string> columnNames, IEnumerable<string> columnValues)
        {
            if (columnNames != null && columnValues != null)
            {
                var columns = "(" + GetCommaDelimitedString(columnNames) + ")";
                var values = "(" + GetCommaDelimitedString(columnValues) + ")";
                var statement = "Insert into [" + sheetName + "] " + columns + " values " + values;
                if (currentConnection.State != ConnectionState.Open)
                {
                    currentConnection.Open();
                }

                using (var command = new OleDbCommand(statement, currentConnection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void CloseFile()
        {
            if (currentConnection != null)
            {
                currentConnection.Close();
            }
        }
        private string GetConnectionString(string fileName, bool useHeader)
        {
            string headerOption = "";
            if (useHeader)
            {
                headerOption = "Yes";
            }
            else
            {
                headerOption = "No";
            }
            return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName +
                                   ";Extended Properties=\"Excel 8.0;HDR=" + headerOption + ";\"";
            
        }

        private string GetCreateCommandFromColumnNames(IEnumerable<string> columnNames, string sheetName)
        {
            var separator = "";
            StringBuilder creationCommand = new StringBuilder("Create Table [" + sheetName + "] (");
            foreach (var columnName in columnNames)
            {
                creationCommand.Append(separator + columnName);
                separator = " ,";
            }

            creationCommand.Append(")");
            return creationCommand.ToString();
        }

        private string GetCommaDelimitedString(IEnumerable<string> stringList)
        {
            var separator = "";
            var listAsString = new StringBuilder();
            foreach (var item in stringList)
            {
                listAsString.Append(separator + item);
                separator = " ,";
            }

            return listAsString.ToString();
        }

    }
}