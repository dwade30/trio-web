﻿using Microsoft.EntityFrameworkCore;
using SharedApplication;

namespace SharedDataAccess
{
    public class RepositoryFactory: IRepositoryFactory
    {
        protected DbContext dataContext;
        public RepositoryFactory( DbContext context)
        {
            dataContext = context;
        }
        public IRepository<TEntity> CreateRepository<TEntity>() where TEntity:class 
        {
            return new Repository<TEntity>(dataContext);
        }
    }

    public class RepositoryFactory<TContextType> : IRepositoryFactory<TContextType> where TContextType : DbContext
    {
        protected TContextType dataContext;
        public RepositoryFactory(TContextType context)
        {
            dataContext = context;
        }

        public IRepository<TEntity> CreateRepository<TEntity>() where TEntity : class
        {
            return new Repository<TEntity>(dataContext);
        }
    }
}
