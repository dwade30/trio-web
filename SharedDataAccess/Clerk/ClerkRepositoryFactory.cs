﻿using Microsoft.EntityFrameworkCore;
using SharedApplication.Clerk;

namespace SharedDataAccess.Clerk
{
    public class ClerkRepositoryFactory : RepositoryFactory, IClerkRepositoryFactory
    {
        public ClerkRepositoryFactory(DbContext context) : base(context)
        {
        }
    }
}