﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedDataAccess.AccountsReceivable;

namespace SharedDataAccess.Clerk.Dogs
{
	public class UndeleteDogOwnerHandler : CommandHandler<UndeleteDogOwner, bool>
	{
		private ClerkContext clerkContext;
		private CommandDispatcher commandDispatcher;

		public UndeleteDogOwnerHandler(ITrioContextFactory trioContextFactory, CommandDispatcher commandDispatcher)
		{
			clerkContext = trioContextFactory.GetClerkContext();
			this.commandDispatcher = commandDispatcher;
		}

		protected override bool Handle(UndeleteDogOwner command)
		{
			try
			{
				var owner = clerkContext.DogOwners.FirstOrDefault(x =>
					x.Id == command.OwnerId);

				if (owner != null)
				{
					owner.Deleted = false;

					clerkContext.SaveChanges();

					commandDispatcher.Send(new AddCYAEntry
					{
						LoggingProgram = TrioPrograms.Clerk,
						Description1 = "Undeleted Dog owner " + command.OwnerId
					});

					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}
	}
}
