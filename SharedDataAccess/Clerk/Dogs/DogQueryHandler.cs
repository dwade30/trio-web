﻿using SharedApplication;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk
{
    public class DogQueryHandler : IQueryHandler<DogQuery,DogInfo>
    {
        private IClerkRepositoryFactory clerkRepositoryFactory;
        public DogQueryHandler(IClerkRepositoryFactory clerkRepositoryFactory)
        {
            this.clerkRepositoryFactory = clerkRepositoryFactory;
        }
        public DogInfo ExecuteQuery(DogQuery query)
        {
            var dogRepo = clerkRepositoryFactory.CreateRepository<DogInfo>();
            return dogRepo.Get(query.Id);
        }
    }
}