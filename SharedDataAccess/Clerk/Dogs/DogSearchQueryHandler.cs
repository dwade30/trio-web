﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.CentralParties.Models;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedDataAccess.CentralParties;

namespace SharedDataAccess.Clerk
{
    public class DogSearchQueryHandler : IQueryHandler<DogSearchCriteria, IEnumerable<DogSearchResult>>
    {       
        private IClerkContext clerkContext;
        private CentralPartyContext partyContext;       
        public DogSearchQueryHandler(ITrioContextFactory contextFactory)
        {
          this.clerkContext = contextFactory.GetClerkContext();
          this.partyContext = contextFactory.GetCentralPartyContext();
        }
        public IEnumerable<DogSearchResult> ExecuteQuery(DogSearchCriteria query)
        { 
            switch (query.TypeToSearchBy)
            {
                case DogSearchBy.City:
                case DogSearchBy.OwnerName:
                case DogSearchBy.Location:
                case DogSearchBy.KennelLicense:
                    return GetResultsPerOwner(query).ToList();
                    break;
                default:
                    return GetResultsPerDog(query).ToList();
                    break;
            }


        }

        private IEnumerable<DogSearchResult> GetResultsPerDog(DogSearchCriteria query)
        {
            IQueryable<DogInfo> dogQuery;
            IEnumerable<DogSearchResult> results;
            if (!query.InlcudeDeletedDogsOrOwners)
            {
                dogQuery = clerkContext.DogInfos.AsNoTracking().Where(d => d.OwnerNum > 0);
            }
            else
            {
                dogQuery = clerkContext.DogInfos.AsNoTracking();
            }

            if (query.TypesOfRecords == DogSearchRecordType.Dog)
            {
                dogQuery = dogQuery.Where(d => d.KennelDog == false);
            }

            if (query.TypesOfRecords == DogSearchRecordType.DogsInKennels)
            {
                dogQuery = dogQuery.Where(d => d.KennelDog == true);
            }
            var likeString = "";
            dogQuery = dogQuery.Include(d => d.Owner);
            switch (query.TypeToSearchBy)
            {
                case DogSearchBy.Breed:
                    switch (query.MatchCriteriaType)
                    {
                        case SearchCriteriaMatchType.StartsWith:
                            likeString = query.Breed + "%";
                            break;
                        case SearchCriteriaMatchType.Contains:
                            likeString = "%" + query.Breed + "%";
                            break;
                        case SearchCriteriaMatchType.EndsWith:
                            likeString = "%" + query.Breed;
                            break;
                    }

                    dogQuery = dogQuery.Where(d => EF.Functions.Like(d.DogBreed, likeString)).OrderBy(d => d.DogBreed).ThenBy(d => d.DogName);
                    break;
                
                case DogSearchBy.Veterinarian:
                    switch (query.MatchCriteriaType)
                    {
                        case SearchCriteriaMatchType.StartsWith:
                            likeString = query.Veterinarian + "%";
                            break;
                        case SearchCriteriaMatchType.Contains:
                            likeString = "%" + query.Veterinarian + "%";
                            break;
                        case SearchCriteriaMatchType.EndsWith:
                            likeString = "%" + query.Veterinarian;
                            break;
                    }

                    dogQuery = dogQuery.Where(d => EF.Functions.Like(d.Dogvet, likeString)).OrderBy(d => d.Dogvet).ThenBy(d => d.DogName);
                    break;
                
                case DogSearchBy.LicenseTagNumber:
                    dogQuery = dogQuery.Where(d => d.TagLicNum == query.LicenseTag);
                    break;
                case DogSearchBy.RabiesTagNumber:
                    dogQuery = dogQuery.Where(d => d.RabiesTagNo == query.RabiesTag);
                    break;
                default:
                    switch (query.MatchCriteriaType)
                    {
                        case SearchCriteriaMatchType.StartsWith:
                            likeString = query.DogName + "%";
                            break;
                        case SearchCriteriaMatchType.Contains:
                            likeString = "%" + query.DogName + "%";
                            break;
                        case SearchCriteriaMatchType.EndsWith:
                            likeString = "%" + query.DogName;
                            break;
                    }
                    dogQuery = dogQuery.Where(d => EF.Functions.Like(d.DogName, likeString)).OrderBy(d => d.DogName);
                    break;
            }

            results = dogQuery.MapToDogSearchResults(partyContext.Parties);
            return results.ToList();
        }

        private IEnumerable<DogSearchResult> GetResultsPerOwner(DogSearchCriteria query)
        {
            IQueryable<DogOwner> ownerQuery;
            IEnumerable<DogSearchResult> results;
            if (!query.InlcudeDeletedDogsOrOwners)
            {
                
                ownerQuery = clerkContext.DogOwners.AsNoTracking().Where(o => o.Deleted == false).Include(o => o.Dogs);
            }
            else
            {
                ownerQuery = clerkContext.DogOwners.AsNoTracking().Include(o => o.Dogs);
            }

            if (query.TypesOfRecords == DogSearchRecordType.Dog)
            {
                ownerQuery = ownerQuery.Where(o => o.Dogs.Where(d => d.KennelDog != true).Any());
            }
            else if (query.TypesOfRecords == DogSearchRecordType.DogsInKennels)
            {
                ownerQuery = ownerQuery.Where(o => o.KennelLicenses.Any());
            }
            switch (query.TypeToSearchBy)
            {
                case DogSearchBy.City:
                    switch (query.MatchCriteriaType)
                    {
                        case SearchCriteriaMatchType.Contains:
                            results = ownerQuery.MapToDogSearchResultsWithCities(partyContext.Parties)
                                .Where(sr => sr.City.ToLower().Contains(query.City.ToLower()));
                            break;
                        case SearchCriteriaMatchType.EndsWith:
                            results = ownerQuery.MapToDogSearchResultsWithCities(partyContext.Parties)
                                .Where(sr => sr.City.ToLower().EndsWith(query.City.ToLower()));
                            break;
                        default:
                            results = ownerQuery.MapToDogSearchResultsWithCities(partyContext.Parties)
                                .Where(sr => sr.City.ToLower().StartsWith(query.City.ToLower()));
                            break;
                    }

                    results = results.OrderBy(o => o.City).ThenBy(o => o.OwnerLast).ThenBy(o => o.OwnerFirst);
                    break;
                case DogSearchBy.Location:
                    if (query.LocationNumber > 0)
                    {
                        ownerQuery = ownerQuery.Where(o => o.LocationNum == query.LocationNumber.ToString());
                    }

                    if (!String.IsNullOrWhiteSpace(query.LocationStreet))
                    {
                        var likeString = "";
                        switch (query.MatchCriteriaType)
                        {
                            case SearchCriteriaMatchType.Contains:
                                likeString = "%" + query.LocationStreet + "%";
                                break;
                            case SearchCriteriaMatchType.EndsWith:
                                likeString = "%" + query.LocationStreet;
                                break;
                            case SearchCriteriaMatchType.StartsWith:
                                likeString =query.LocationStreet + "%";
                                break;
                        }

                        ownerQuery = ownerQuery.Where(o => EF.Functions.Like(o.LocationStr,likeString));
                    }

                    results = ownerQuery.MapToDogSearchResults(partyContext.Parties).OrderBy(o => o.StreetName).ThenBy(o => o.StreetNumber).ThenBy(o => o.OwnerLast).ThenBy(o => o.OwnerFirst);
                    break;
                case DogSearchBy.OwnerName:
                    results = ownerQuery.MapToDogSearchResults(partyContext.Parties);
                    if (!String.IsNullOrWhiteSpace(query.OwnerLastName))
                    {                       
                        switch (query.MatchCriteriaType)
                        {
                            case SearchCriteriaMatchType.Contains:
                                results = results.Where(sr => sr.OwnerLast.ToLower().Contains(query.OwnerLastName.ToLower()));
                                break;
                            case SearchCriteriaMatchType.EndsWith:
                                results = results.Where(sr => sr.OwnerLast.ToLower().EndsWith(query.OwnerLastName.ToLower()));
                                break;
                            case SearchCriteriaMatchType.StartsWith:
                                results = results.Where(sr => sr.OwnerLast.ToLower().StartsWith(query.OwnerLastName.ToLower()));
                                break;
                        }
                    }

                    if (!String.IsNullOrWhiteSpace(query.OwnerFirstName))
                    {
                        switch (query.MatchCriteriaType)
                        {
                            case SearchCriteriaMatchType.Contains:
                                results = results.Where(sr => sr.OwnerFirst.ToLower().Contains(query.OwnerFirstName.ToLower()));
                                break;
                            case SearchCriteriaMatchType.EndsWith:
                                results = results.Where(sr => sr.OwnerFirst.ToLower().EndsWith(query.OwnerFirstName.ToLower()));
                                break;
                            case SearchCriteriaMatchType.StartsWith:
                                results = results.Where(sr => sr.OwnerFirst.ToLower().StartsWith(query.OwnerFirstName.ToLower()));
                                break;
                        }
                    }

                    results = results.OrderBy(o => o.OwnerLast).ThenBy(o => o.OwnerFirst);
                    break;
                case DogSearchBy.KennelLicense:
                    IQueryable<DogSearchResult> kennelQuery;
                    if (!query.InlcudeDeletedDogsOrOwners)
                    {
                        kennelQuery = clerkContext.KennelLicenses.AsNoTracking().Include(k => k.Owner).Where(k => k.Owner.Deleted == false).Join(clerkContext.DogInventories.AsNoTracking().Where(di => di.StickerNumber == query.KennelLicense.ToString() && di.Type == "K"), kl => kl.LicenseId, inv => inv.Id, (kl, inv) => new DogSearchResult() { OwnerId = kl.OwnerNum.GetValueOrDefault(), KennelLicense = inv.StickerNumber, PartyId = kl.Owner.PartyId, StreetName = kl.Owner.LocationStr, StreetNumber = kl.Owner.LocationNum.ToIntegerValue() }).Where(sr => sr.KennelLicense == query.KennelLicense.ToString());
                    }
                    else
                    {
                        kennelQuery = clerkContext.KennelLicenses.AsNoTracking().Include(k => k.Owner).Join(clerkContext.DogInventories.AsNoTracking().Where(di => di.StickerNumber == query.KennelLicense.ToString() && di.Type == "K"), kl => kl.LicenseId, inv => inv.Id, (kl, inv) => new DogSearchResult() { OwnerId = kl.OwnerNum.GetValueOrDefault(), KennelLicense = inv.StickerNumber, PartyId = kl.Owner.PartyId, StreetName = kl.Owner.LocationStr, StreetNumber = kl.Owner.LocationNum.ToIntegerValue() }).Where(sr => sr.KennelLicense == query.KennelLicense.ToString());

                    }
                    //kennelQuery = clerkContext.KennelLicenses.Include(k => k.Owner).Join(clerkContext.DogInventories.Where(di => di.StickerNumber == query.KennelLicense.ToString() && di.Type == "K"),kl => kl.LicenseId, inv => inv.Id, (kl,inv) => new DogSearchResult(){OwnerId = kl.OwnerNum.GetValueOrDefault(), KennelLicense = inv.StickerNumber, PartyId = kl.Owner.PartyId, StreetName = kl.Owner.LocationStr, StreetNumber = kl.Owner.LocationNum.ToIntegerValue()}).Where(sr => sr.KennelLicense == query.KennelLicense.ToString());
                    if (!query.InlcudeDeletedDogsOrOwners)
                    {
                        
                    }

                    results = kennelQuery;
                    
                    break;
                default:
                    results = ownerQuery.MapToDogSearchResults(partyContext.Parties).OrderBy(o => o.OwnerLast).ThenBy(o => o.OwnerFirst);
                    break;
            }           
            return results.ToList();
        }
    }

    public static class DogSearchExtensions
    {
        public static DogSearchResult ToDogSearchResult(this DogOwner dogOwner, IQueryable<CentralParty> parties)
        {
            var searchResult = new DogSearchResult();
            searchResult.OwnerId = dogOwner.Id;
            searchResult.PartyId = dogOwner.PartyId;
            searchResult.StreetName = dogOwner.LocationStr;
            searchResult.StreetNumber = dogOwner.LocationNum.ToIntegerValue();
            searchResult.OwnerDeleted = dogOwner.Deleted.GetValueOrDefault();
            searchResult.DogNames = dogOwner.Dogs.Select(d => d.DogName);
            if (dogOwner.PartyId > 0)
            {
                var party = parties.Where(p => p.Id == dogOwner.PartyId).FirstOrDefault();
                if (party != null)
                {
                    searchResult.OwnerFirst = party.FirstName;
                    searchResult.OwnerLast = party.LastName;                    
                }
            }

            return searchResult;
        }

        public static IEnumerable<DogSearchResult> MapToDogSearchResults(this IEnumerable<DogOwner> dogOwners,IQueryable<CentralParty> parties)
        {
            return dogOwners.Select(o => o.ToDogSearchResult(parties));
        }

        public static DogSearchResult ToDogSearchResultWithCity(this DogOwner dogOwner,
            IQueryable<CentralParty> parties)
        {
            var searchResult = new DogSearchResult();
            searchResult.OwnerId = dogOwner.Id;
            searchResult.PartyId = dogOwner.PartyId;
            searchResult.StreetName = dogOwner.LocationStr;
            searchResult.StreetNumber = dogOwner.LocationNum.ToIntegerValue();
            searchResult.OwnerDeleted = dogOwner.Deleted.GetValueOrDefault();
            searchResult.DogNames = dogOwner.Dogs.Select(d => d.DogName);
            if (dogOwner.PartyId > 0)
            {
                //var party = parties.Include(p => p.Addresses.Where(a => a.AddressType == "Primary" ).FirstOrDefault()).Where(p => p.Id == dogOwner.PartyId).FirstOrDefault();
                var party = parties.Include(p => p.Addresses).Where(p => p.Id == dogOwner.PartyId && p.Addresses.Where(a => a.AddressType == "Primary").Any()).FirstOrDefault();
                if (party != null)
                {
                    searchResult.OwnerFirst = party.FirstName;
                    searchResult.OwnerLast = party.LastName;
                    if (party.Addresses.Any())
                    {
                        searchResult.City = party.Addresses.FirstOrDefault().City;
                    }
                }
            }

            return searchResult;
        }

        public static IEnumerable<DogSearchResult> MapToDogSearchResultsWithCities(this IEnumerable<DogOwner> dogOwners,
            IQueryable<CentralParty> parties)
        {
            return dogOwners.Select(o => o.ToDogSearchResultWithCity(parties));
        }

        public static IEnumerable<DogSearchResult> MapToDogSearchResults(this IEnumerable<DogInfo> dogs, IQueryable<CentralParty> parties)
        {
            return dogs.Select(d => d.ToDogSearchResult(parties));
        }

        public static DogSearchResult ToDogSearchResult(this DogInfo dog, IQueryable<CentralParty> parties)
        {
            var searchResult = new DogSearchResult() { DogName = dog.DogName, Breed = dog.DogBreed, Color = dog.DogColor, Veterinarian = dog.Dogvet, DogId = dog.Id, OwnerDeleted = dog.OwnerNum <= 0,OwnerId = dog.OwnerNum.GetValueOrDefault()};
            if (dog.Owner != null)
            {
                if (dog.Owner.PartyId > 0)
                {
                    var party = parties.Where(p => p.Id == dog.Owner.PartyId).FirstOrDefault();
                    if (party != null)
                    {
                        searchResult.OwnerFirst = party.FirstName;
                        searchResult.OwnerLast = party.LastName;
                    }
                }
            }

            return searchResult;
        }
    }
}