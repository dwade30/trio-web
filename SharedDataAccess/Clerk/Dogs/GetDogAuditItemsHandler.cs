﻿using System;
using System.Collections.Generic;
using System.Linq;
using Remotion.Linq.Clauses;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace SharedDataAccess.Clerk.Dogs
{
    public class GetDogAuditItemsHandler: CommandHandler<GetDogAuditItems,IEnumerable<DogAuditItem>>
    {
        private ClerkContext ckContext;
        public GetDogAuditItemsHandler(ClerkContext ckContext)
        {
            this.ckContext = ckContext;
        }
        protected override IEnumerable<DogAuditItem> Handle(GetDogAuditItems command)
        {
            IQueryable<DogTransaction> dogQuery = ckContext.DogTransactions;
            if (!command.IncludeOnlineTransactions)
            {
                dogQuery = dogQuery.Where(d => !d.Online.GetValueOrDefault());
            }

            if (command.StartDate != DateTime.MinValue)
            {
                dogQuery = dogQuery.Where(d => d.TransactionDate >= command.StartDate);
            }

            if (command.EndDate != DateTime.MinValue && command.EndDate != DateTime.MaxValue)
            {
                dogQuery = dogQuery.Where(d => d.TransactionDate <= command.EndDate);
            }

            try
            {
                var blankView = new DogOwnerPartyView();
                var query = from dogTrans in dogQuery
                    join trans in ckContext.TransactionTables on dogTrans.TransactionNumber equals trans.Id into
                        grouping
                    from trans in grouping.DefaultIfEmpty()
                    join dogOwner in ckContext.DogOwnerParties
                        on dogTrans.OwnerNum equals dogOwner.Id into secondGrouping
                    from dogOwner in secondGrouping.DefaultIfEmpty(blankView)
                    join doggy in ckContext.DogInfos
                        on dogTrans.DogNumber equals doggy.Id into thirdGrouping
                        from doggy in thirdGrouping.DefaultIfEmpty()
                    orderby dogTrans.TransactionDate descending 
                    
                    select new DogAuditItem()
                    {
                        DogTransactionID = dogTrans.Id,
                        Description = dogTrans.Description,
                        Amount = dogTrans.Amount.GetValueOrDefault().ToDecimal(),
                        UserID = trans.UserId.GetValueOrDefault(),
                        AnimalControl = dogTrans.AnimalControl.GetValueOrDefault().ToDecimal(),
                        IsAdjustmentVoid = dogTrans.BoolAdjustmentVoid.GetValueOrDefault(),
                        IsReplacementSticker = dogTrans.BoolReplacementSticker.GetValueOrDefault(),
                        ClerkFee = dogTrans.ClerkFee.GetValueOrDefault().ToDecimal(),
                        DangerousDog = dogTrans.DangerousDog.GetValueOrDefault(),
                        DogNumber = dogTrans.DogNumber.GetValueOrDefault(),
                        DogYear = dogTrans.DogYear.GetValueOrDefault(),
                        IsHearingGuid = dogTrans.BoolHearingGuid.GetValueOrDefault(),
                        IsKennel = dogTrans.BoolKennel.GetValueOrDefault(),
                        IsLicense = dogTrans.BoolLicense.GetValueOrDefault(),
                        IsReplacementLicense = dogTrans.BoolReplacementLicense.GetValueOrDefault(),
                        IsReplacementTag = dogTrans.BoolReplacementTag.GetValueOrDefault(),
                        IsSearchRescue = dogTrans.BoolSearchRescue.GetValueOrDefault(),
                        IsSpayNeuter = dogTrans.BoolSpayNeuter.GetValueOrDefault(),
                        IsTempLicense = dogTrans.BoolTempLicense.GetValueOrDefault(),
                        IsTransfer = dogTrans.BoolTransfer.GetValueOrDefault(),
                        KennelDogs = dogTrans.KennelDogs,
                        KennelLateFee = dogTrans.KennelLateFee.GetValueOrDefault().ToDecimal(),
                        KennelLic1Fee = dogTrans.KennelLic1Fee.GetValueOrDefault().ToDecimal(),
                        KennelLic1Num = dogTrans.KennelLic1Num.GetValueOrDefault(),
                        KennelLic2Fee = dogTrans.KennelLic2Fee.GetValueOrDefault().ToDecimal(),
                        KennelLic2Num = dogTrans.KennelLic2Num.GetValueOrDefault(),
                        KennelLicenseId = dogTrans.KennelLicenseId.GetValueOrDefault(),
                        KennelWarrantFee = dogTrans.KennelWarrantFee.GetValueOrDefault().ToDecimal(),
                        LateFee = dogTrans.LateFee.GetValueOrDefault().ToDecimal(),
                        NuisanceDog = dogTrans.NuisanceDog.GetValueOrDefault(),
                        OldIssueDate = dogTrans.OldIssueDate,
                        OldSticker = dogTrans.OldSticker.GetValueOrDefault(),
                        OldTagNumber = dogTrans.OldTagNumber,
                        OldYear = dogTrans.OldYear.GetValueOrDefault(),
                        Online = dogTrans.Online.GetValueOrDefault(),
                        OwnerNum = dogTrans.OwnerNum.GetValueOrDefault(),
                        RabiesTagNumber = dogTrans.RabiesTagNumber,
                        ReplacementLicenseFee = dogTrans.ReplacementLicenseFee.GetValueOrDefault().ToDecimal(),
                        ReplacementStickerFee = dogTrans.ReplacementStickerFee.GetValueOrDefault().ToDecimal(),
                        ReplacementTagFee = dogTrans.ReplacementTagFee.GetValueOrDefault().ToDecimal(),
                        SpayNonSpayAmount = dogTrans.SpayNonSpayAmount.GetValueOrDefault().ToDecimal(),
                        StateFee = dogTrans.StateFee.GetValueOrDefault().ToDecimal(),
                        StickerNumber = dogTrans.StickerNumber.GetValueOrDefault(),
                        TagNumber = dogTrans.TagNumber,
                        TempLicenseFee = dogTrans.TempLicenseFee.GetValueOrDefault().ToDecimal(),
                        TimeStamp = dogTrans.TimeStamp,
                        TownCode = trans.TownCode.GetValueOrDefault(),
                        TownFee = dogTrans.TownFee.GetValueOrDefault().ToDecimal(),
                        TransactionDate = trans.TransactionDate.GetValueOrDefault(),
                        TransactionDescription = trans.Description,
                        TransactionNumber = dogTrans.TransactionNumber.GetValueOrDefault(),
                        TransactionPerson = trans.TransactionPerson,
                        TransferLicenseFee = dogTrans.TransferLicenseFee.GetValueOrDefault().ToDecimal(),
                        UserName = trans.UserName,
                        WarrantFee = dogTrans.WarrantFee.GetValueOrDefault().ToDecimal() ,
                         OwnerName = dogOwner.FullName() ,
                         DogName = doggy.DogName
                    };
                return query.ToList().OrderByDescending(d => d.TransactionDate);
            }
            catch (Exception ex)
            {
                return new List<DogAuditItem>();
            }

        }
    }
}