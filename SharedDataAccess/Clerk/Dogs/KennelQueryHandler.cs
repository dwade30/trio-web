﻿using SharedApplication;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk
{
    public class KennelQueryHandler : IQueryHandler<KennelQuery,KennelLicense>
    {
        private IClerkRepositoryFactory clerkRepositoryFactory;

        public KennelQueryHandler(IClerkRepositoryFactory clerkRepositoryFactory)
        {
            this.clerkRepositoryFactory = clerkRepositoryFactory;
        }
        public KennelLicense ExecuteQuery(KennelQuery query)
        {
            var kennelRepo = clerkRepositoryFactory.CreateRepository<KennelLicense>();
            return kennelRepo.Get(query.Id);
        }
    }
}