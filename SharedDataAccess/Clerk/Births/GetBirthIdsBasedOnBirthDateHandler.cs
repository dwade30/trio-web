﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Clerk.Births;
using SharedApplication.Clerk.Commands;
using SharedApplication.Clerk.Enums;
using SharedApplication.Messaging;

namespace SharedDataAccess.Clerk.Births
{
	public class GetBirthIdsBasedOnBirthDateHandler : CommandHandler<GetBirthIdsBasedOnBirthDate, IEnumerable<int>>
	{
		private ClerkContext clerkContext;

		public GetBirthIdsBasedOnBirthDateHandler(ITrioContextFactory contextFactory)
		{
			this.clerkContext = contextFactory.GetClerkContext();
		}

		protected override IEnumerable<int> Handle(GetBirthIdsBasedOnBirthDate command)
		{
			var result = new List<int>();
			var dateInfo = new List<BirthDateCheckInfo>();

			if (command.Type == BirthDateType.ForeignBirth)
			{
				dateInfo = clerkContext.BirthsForeigns.Select(x => new BirthDateCheckInfo
				{
					Id = x.Id,
					ActualBirthDate = x.ActualDate,
					RecordedBirthDate = x.ChildDob
				}).ToList();
			}
			else if (command.Type == BirthDateType.DelayedBirth)
			{
				dateInfo = clerkContext.BirthsDelayeds.Select(x => new BirthDateCheckInfo
				{
					Id = x.Id,
					ActualBirthDate = x.ActualDate,
					RecordedBirthDate = x.ChildDob
				}).ToList();
			}
			else if (command.Type == BirthDateType.LiveBirth)
			{
				dateInfo = clerkContext.Births.Select(x => new BirthDateCheckInfo
				{
					Id = x.Id,
					ActualBirthDate = x.ActualDate,
					RecordedBirthDate = x.Dateofbirth
				}).ToList();
			}
			else if (command.Type == BirthDateType.MarraigeBride)
			{
				dateInfo = clerkContext.Marriages.Select(x => new BirthDateCheckInfo
				{
					Id = x.Id,
					ActualBirthDate = null,
					RecordedBirthDate = x.BridesDob
				}).ToList();
			}
			else if (command.Type == BirthDateType.MarraigeGroom)
			{
				dateInfo = clerkContext.Marriages.Select(x => new BirthDateCheckInfo
				{
					Id = x.Id,
					ActualBirthDate = null,
					RecordedBirthDate = x.GroomsDob
				}).ToList();
			}
			else if (command.Type == BirthDateType.Death)
			{
				dateInfo = clerkContext.Deaths.Select(x => new BirthDateCheckInfo
				{
					Id = x.Id,
					ActualBirthDate = null,
					RecordedBirthDate = x.DateOfBirth
				}).ToList();
			}

			foreach (var record in dateInfo)
			{
				DateTime checkDate;
				bool match = false;

				if (DateTime.TryParse(record.RecordedBirthDate, out checkDate))
				{
					if (command.EndDate != null)
					{
						if (checkDate >= command.StartDate && checkDate <= command.EndDate)
						{
							match = true;
						}
						else if (record.ActualBirthDate != null && (record.ActualBirthDate >= command.StartDate && record.ActualBirthDate <= command.EndDate))
						{
							match = true;
						}
					}
					else
					{
						if (checkDate == command.StartDate)
						{
							match = true;
						}
						else if (record.ActualBirthDate != null && record.ActualBirthDate == command.StartDate)
						{
							match = true;
						}
					}
				}

				if (match)
				{
					result.Add(record.Id);
				}
			}

			return result;
		}

		private class BirthDateCheckInfo
		{
			public int Id { get; set; }
			public string RecordedBirthDate { get; set; }
			public DateTime? ActualBirthDate { get; set; }
		}
	}

	
}
