﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Clerk.Commands;
using SharedApplication.Clerk.Enums;
using SharedApplication.Clerk.Models;
using SharedApplication.Messaging;

namespace SharedDataAccess.Clerk.Births
{
	public class GetBirthReportInfoHandler : CommandHandler<GetBirthReportInfo, IEnumerable<BirthReportInfo>>
	{
		private ClerkContext clerkContext;

		public GetBirthReportInfoHandler(ITrioContextFactory contextFactory)
		{
			this.clerkContext = contextFactory.GetClerkContext();
		}

		protected override IEnumerable<BirthReportInfo> Handle(GetBirthReportInfo command)
		{


			IQueryable<Birth> birthQuery;

			birthQuery = clerkContext.Births;

			if (command.StartLastName != "")
			{
				birthQuery = birthQuery.Where(x => x.LastName.CompareTo(command.StartLastName) >= 0);
			}

			if (command.EndLastName != "")
			{
				birthQuery = birthQuery.Where(x => x.LastName.CompareTo(command.EndLastName + "zzz") <= 0);
			}

			if (command.StartMothersLastName != "")
			{
				birthQuery = birthQuery.Where(x => x.MothersLastName.CompareTo(command.StartMothersLastName) >= 0);
			}

			if (command.EndMothersLastName != "")
			{
				birthQuery =
					birthQuery.Where(x => x.MothersLastName.CompareTo(command.EndMothersLastName + "zzz") <= 0);
			}

			if (command.StartBirthPlace != "")
			{
				birthQuery = birthQuery.Where(x => x.BirthPlace.CompareTo(command.StartBirthPlace) >= 0);
			}

			if (command.EndBirthPlace != "")
			{
				birthQuery = birthQuery.Where(x => x.BirthPlace.CompareTo(command.EndBirthPlace + "zzz") <= 0);
			}

			var results = birthQuery.Select(x => new BirthReportInfo
			{
				BirthPlace = x.BirthPlace,
				MothersLastName = x.MothersLastName,
				LastName = x.LastName,
				ActualDateOfBirth = x.ActualDate,
				RecordedDateOfBirth = x.Dateofbirth,
				FirstName = x.FirstName,
				MiddleName = x.MiddleName,
				MothersFirstName = x.MothersFirstName,
				MothersMiddleName = x.MothersMiddleName,
				Sex = x.Sex
			}).ToList();



			for (int i = results.Count - 1; i >= 0; i--)
			{
				if (command.SortBy == BirthReportSortOptions.BirthDate)
				{
					DateTime testDate;

					if (DateTime.TryParse(results[i].RecordedDateOfBirth, out testDate))
					{
						results[i].ConvertedRecordedDateOfBirth = testDate;
					}
				}

				if (command.StartBirthDate.Trim() != "")
				{
					if (!IsMatchingBirthDate(command, results[i]))
					{
						results.RemoveAt(i);
					}
				}
			}

			if (command.SortBy == BirthReportSortOptions.BirthDate)
			{
				return results.OrderBy(x => x.ConvertedRecordedDateOfBirth);
			}
			else if (command.SortBy == BirthReportSortOptions.BirthPlace)
			{
				return results.OrderBy(x => x.BirthPlace);
			}
			else if (command.SortBy == BirthReportSortOptions.MothersName)
			{
				return results.OrderBy(x => x.MothersLastName).ThenBy(y => y.MothersFirstName);
			}
			else
			{
				return results.OrderBy(x => x.LastName).ThenBy(y => y.FirstName);
			}
		}

		private bool IsMatchingBirthDate(GetBirthReportInfo command, BirthReportInfo recordToCheck)
		{
			DateTime testDate;
			DateTime? startDate = null;
			DateTime? endDate = null;

			if (command.SortBy == BirthReportSortOptions.BirthDate)
			{
				if (!DateTime.TryParse(recordToCheck.RecordedDateOfBirth, out testDate))
				{
					return false;
				}
			}

			if (command.StartBirthDate.Trim() != "")
			{
				if (DateTime.TryParse(command.StartBirthDate, out testDate))
				{
					startDate = testDate;
				}
			}

			if (command.EndBirthDate.Trim() != "")
			{
				if (DateTime.TryParse(command.EndBirthDate, out testDate))
				{
					endDate = testDate;
				}
			}

			if (command.EndBirthDate.Trim() != "")
			{
				if (startDate != null && endDate != null)
				{
					if (!DateTime.TryParse(recordToCheck.RecordedDateOfBirth, out testDate))
					{
						return false;
					}
					else
					{
						return (testDate >= startDate && testDate <= endDate) ||
						       (recordToCheck.ActualDateOfBirth >= startDate &&
						        recordToCheck.ActualDateOfBirth <= endDate);
					}
				}
				else
				{
					return recordToCheck.RecordedDateOfBirth.CompareTo(command.StartBirthDate) >= 0 &&
					       recordToCheck.RecordedDateOfBirth.CompareTo(command.EndBirthDate) <= 0;
				}
			}
			else
			{
				if (startDate != null)
				{
					if (!DateTime.TryParse(recordToCheck.RecordedDateOfBirth, out testDate))
					{
						return false;
					}
					else
					{
						return testDate == startDate || recordToCheck.ActualDateOfBirth == startDate;
					}
				}
				else
				{
					return recordToCheck.RecordedDateOfBirth == command.StartBirthDate;
				}
			}
		}
	}
}
