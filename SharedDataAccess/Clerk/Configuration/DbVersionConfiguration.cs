﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DbVersionConfiguration : IEntityTypeConfiguration<Dbversion>
    {
        public void Configure(EntityTypeBuilder<Dbversion> builder)
        {
            builder.ToTable("DBVersion");

            builder.Property(e => e.Id).HasColumnName("ID");
        }
    }
}