﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class CyaConfiguration : IEntityTypeConfiguration<Cya>
    {
        public void Configure(EntityTypeBuilder<Cya> builder)
        {
            builder.ToTable("CYA");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.BoolUseSecurity).HasColumnName("boolUseSecurity");

            builder.Property(e => e.Cyadate)
                .HasColumnName("CYADate")
                .HasColumnType("datetime");

            builder.Property(e => e.Cyatime)
                .HasColumnName("CYATime")
                .HasColumnType("datetime");

            builder.Property(e => e.Description1).HasMaxLength(255);

            builder.Property(e => e.Description2).HasMaxLength(255);

            builder.Property(e => e.Description3).HasMaxLength(255);

            builder.Property(e => e.Description4).HasMaxLength(255);

            builder.Property(e => e.Description5).HasMaxLength(255);

            builder.Property(e => e.UserId)
                .HasColumnName("UserID")
                .HasMaxLength(255);
        }
    }
}