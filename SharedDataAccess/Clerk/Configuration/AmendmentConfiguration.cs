﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class AmendmentConfiguration : IEntityTypeConfiguration<Amendment>
    {
        public void Configure(EntityTypeBuilder<Amendment> builder)
        {
            builder.ToTable("Amendments");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.Amendment1)
                    .HasColumnName("Amendment")
                    .HasMaxLength(255);
            builder.Property(e => e.AmendmentId)
                    .HasColumnName("AmendmentID")
                    .HasMaxLength(255);
            builder.Property(e => e.LastUpdated).HasColumnType("datetime");
            builder.Property(e => e.Parameter1).HasMaxLength(255);
            builder.Property(e => e.Parameter2).HasMaxLength(255);
            builder.Property(e => e.Parameter3).HasMaxLength(255);
            builder.Property(e => e.Statement).HasMaxLength(255);
            builder.Property(e => e.Type).HasMaxLength(255);
          
        }
    }
}