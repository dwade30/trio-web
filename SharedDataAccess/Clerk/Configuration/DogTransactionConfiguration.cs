﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DogTransactionConfiguration : IEntityTypeConfiguration<DogTransaction>
    {
        public void Configure(EntityTypeBuilder<DogTransaction> builder)
        {
            builder.ToTable("DogTransactions");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.BoolAdjustmentVoid).HasColumnName("boolAdjustmentVoid");

            builder.Property(e => e.BoolHearingGuid).HasColumnName("boolHearingGuid");

            builder.Property(e => e.BoolKennel).HasColumnName("boolKennel");

            builder.Property(e => e.BoolLicense).HasColumnName("boolLicense");

            builder.Property(e => e.BoolReplacementLicense).HasColumnName("boolReplacementLicense");

            builder.Property(e => e.BoolReplacementSticker).HasColumnName("boolReplacementSticker");

            builder.Property(e => e.BoolReplacementTag).HasColumnName("boolReplacementTag");

            builder.Property(e => e.BoolSearchRescue).HasColumnName("boolSearchRescue");

            builder.Property(e => e.BoolSpayNeuter).HasColumnName("boolSpayNeuter");

            builder.Property(e => e.BoolTempLicense).HasColumnName("boolTempLicense");

            builder.Property(e => e.BoolTransfer).HasColumnName("boolTransfer");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.KennelDogs).HasMaxLength(255);

            builder.Property(e => e.KennelLicenseId).HasColumnName("KennelLicenseID");

            builder.Property(e => e.OldIssueDate).HasColumnType("datetime");

            builder.Property(e => e.OldTagNumber)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.RabiesTagNumber).HasMaxLength(50);

            builder.Property(e => e.TagNumber)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.TimeStamp).HasColumnType("datetime");

            builder.Property(e => e.TransactionDate).HasColumnType("datetime");
            builder.HasOne(t => t.TransactionTable);

        }
    }
}