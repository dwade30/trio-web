﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class BirthConfiguration : IEntityTypeConfiguration<Birth>
    {
        public void Configure(EntityTypeBuilder<Birth> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ActualDate).HasColumnType("datetime");

            builder.Property(e => e.Amended).HasMaxLength(255);

            builder.Property(e => e.AmendedInfo).HasMaxLength(255);

            builder.Property(e => e.Aop).HasColumnName("AOP");

            builder.Property(e => e.AttendantTitle).HasMaxLength(50);

            builder.Property(e => e.AttendantsFirstName).HasMaxLength(50);

            builder.Property(e => e.AttendantsLastName).HasMaxLength(50);

            builder.Property(e => e.AttendantsMiddleInitial).HasMaxLength(50);

            builder.Property(e => e.AttestDate).HasColumnType("datetime");

            builder.Property(e => e.BirthPlace).HasMaxLength(255);

            builder.Property(e => e.ChildCounty).HasMaxLength(255);

            builder.Property(e => e.ChildTime).HasMaxLength(255);

            builder.Property(e => e.ChildTown).HasMaxLength(255);

            builder.Property(e => e.CityOrTown).HasMaxLength(50);

            builder.Property(e => e.Court).HasMaxLength(255);

            builder.Property(e => e.DateAmended).HasColumnType("datetime");

            builder.Property(e => e.DateOfFiling).HasColumnType("datetime");

            builder.Property(e => e.Dateofbirth)
                .HasColumnName("dateofbirth")
                .HasMaxLength(255);

            builder.Property(e => e.DeceasedDate).HasColumnType("datetime");

            builder.Property(e => e.Designation).HasMaxLength(50);

            builder.Property(e => e.FacilityName).HasMaxLength(255);

            builder.Property(e => e.FathersBirthPlace).HasMaxLength(255);

            builder.Property(e => e.FathersDesignation).HasMaxLength(50);

            builder.Property(e => e.FathersDob)
                .HasColumnName("FathersDOB")
                .HasMaxLength(255);

            builder.Property(e => e.FathersFirstName).HasMaxLength(50);

            builder.Property(e => e.FathersLastName).HasMaxLength(50);

            builder.Property(e => e.FathersMiddleInitial).HasMaxLength(50);

            builder.Property(e => e.FileNumber).HasMaxLength(50);

            builder.Property(e => e.FirstName).HasMaxLength(50);

            builder.Property(e => e.LastName).HasMaxLength(50);

            builder.Property(e => e.LastUpdate).HasColumnType("datetime");

            builder.Property(e => e.MiddleName).HasMaxLength(50);

            builder.Property(e => e.MotherCounty).HasMaxLength(255);

            builder.Property(e => e.MotherState).HasMaxLength(255);

            builder.Property(e => e.MotherTown).HasMaxLength(255);

            builder.Property(e => e.MothersAddress).HasMaxLength(255);

            builder.Property(e => e.MothersBirthPlace).HasMaxLength(255);

            builder.Property(e => e.MothersDob)
                .HasColumnName("MothersDOB")
                .HasMaxLength(255);

            builder.Property(e => e.MothersFirstName).HasMaxLength(50);

            builder.Property(e => e.MothersLastName).HasMaxLength(50);

            builder.Property(e => e.MothersMaidenName).HasMaxLength(50);

            builder.Property(e => e.MothersMiddleName).HasMaxLength(50);

            builder.Property(e => e.MothersYears).HasMaxLength(255);

            builder.Property(e => e.MothersZipCode).HasMaxLength(255);

            builder.Property(e => e.RecordingClerksFirstName).HasMaxLength(50);

            builder.Property(e => e.RecordingClerksLastName).HasMaxLength(50);

            builder.Property(e => e.RecordingClerksMiddleInitial).HasMaxLength(50);

            builder.Property(e => e.RegTown).HasMaxLength(255);

            builder.Property(e => e.RegistrarDateFiled).HasColumnType("datetime");

            builder.Property(e => e.RegistrarDateFiledDescription).HasMaxLength(255);

            builder.Property(e => e.RegistrarFirstName).HasMaxLength(255);

            builder.Property(e => e.RegistrarLastName).HasMaxLength(255);

            builder.Property(e => e.RegistrarMiddleName).HasMaxLength(255);

            builder.Property(e => e.RegistrarName).HasMaxLength(255);

            builder.Property(e => e.ResidenceOfMother).HasMaxLength(50);

            builder.Property(e => e.Sex).HasMaxLength(50);

            builder.Property(e => e.StateRegistrarMunicipalClerk).HasMaxLength(50);

            builder.Property(e => e.TownOf).HasMaxLength(50);
        }
    }
}