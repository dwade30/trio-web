﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DefaultTownConfiguration : IEntityTypeConfiguration<DefaultTown>
    {
        public void Configure(EntityTypeBuilder<DefaultTown> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.Name).HasMaxLength(200);
        }
    }
}