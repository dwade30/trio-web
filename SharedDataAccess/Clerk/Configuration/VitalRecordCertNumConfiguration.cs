﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class VitalRecordCertNumConfiguration : IEntityTypeConfiguration<VitalRecordCertNum>
    {
        public void Configure(EntityTypeBuilder<VitalRecordCertNum> builder)
        {
            builder.ToTable("VitalRecordCertNum");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AttestedBy).HasMaxLength(50);

            builder.Property(e => e.CertNumber).HasMaxLength(50);

            builder.Property(e => e.PrintDate).HasColumnType("datetime");

            builder.Property(e => e.TblName)
                .HasColumnName("tblName")
                .HasMaxLength(50);
        }
    }
}