﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class BirthsDelayedConfiguration : IEntityTypeConfiguration<BirthsDelayed>
    {
        public void Configure(EntityTypeBuilder<BirthsDelayed> builder)
        {
            builder.ToTable("BirthsDelayed");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ActualDate).HasColumnType("datetime");

            builder.Property(e => e.AmendedInfo).HasMaxLength(255);

            builder.Property(e => e.BirthPlace1).HasMaxLength(255);

            builder.Property(e => e.BirthPlace2).HasMaxLength(255);

            builder.Property(e => e.BirthPlace3).HasMaxLength(255);

            builder.Property(e => e.ChildCounty).HasMaxLength(255);

            builder.Property(e => e.ChildCurrentAddress).HasMaxLength(255);

            builder.Property(e => e.ChildDob)
                .HasColumnName("ChildDOB")
                .HasMaxLength(255);

            builder.Property(e => e.ChildDobdescription)
                .HasColumnName("ChildDOBDescription")
                .HasMaxLength(255);

            builder.Property(e => e.ChildFirstName).HasMaxLength(255);

            builder.Property(e => e.ChildLastName).HasMaxLength(255);

            builder.Property(e => e.ChildMiddleName).HasMaxLength(255);

            builder.Property(e => e.ChildRace).HasMaxLength(255);

            builder.Property(e => e.ChildSex).HasMaxLength(255);

            builder.Property(e => e.ChildTown).HasMaxLength(255);

            builder.Property(e => e.ClerkFirstName).HasMaxLength(255);

            builder.Property(e => e.ClerkLastName).HasMaxLength(255);

            builder.Property(e => e.ClerkMiddleName).HasMaxLength(255);

            builder.Property(e => e.ClerkSwornDate).HasColumnType("datetime");

            builder.Property(e => e.ClerkTown).HasMaxLength(255);

            builder.Property(e => e.DateIssued1).HasMaxLength(255);

            builder.Property(e => e.DateIssued2).HasMaxLength(255);

            builder.Property(e => e.DateIssued3).HasMaxLength(255);

            builder.Property(e => e.DateOfFiling).HasMaxLength(255);

            builder.Property(e => e.DateOrigEntry1).HasMaxLength(255);

            builder.Property(e => e.DateOrigEntry2).HasMaxLength(255);

            builder.Property(e => e.DateOrigEntry3).HasMaxLength(255);

            builder.Property(e => e.DeceasedDate).HasColumnType("datetime");

            builder.Property(e => e.Dob1)
                .HasColumnName("DOB1")
                .HasColumnType("datetime");

            builder.Property(e => e.Dob2)
                .HasColumnName("DOB2")
                .HasColumnType("datetime");

            builder.Property(e => e.Dob3)
                .HasColumnName("DOB3")
                .HasColumnType("datetime");

            builder.Property(e => e.EvidenceReviewedBy).HasMaxLength(255);

            builder.Property(e => e.FathersBirthPlace).HasMaxLength(255);

            builder.Property(e => e.FathersDesignation).HasMaxLength(255);

            builder.Property(e => e.FathersFirstName).HasMaxLength(255);

            builder.Property(e => e.FathersLastName).HasMaxLength(255);

            builder.Property(e => e.FathersMiddleName).HasMaxLength(255);

            builder.Property(e => e.FileNumber).HasMaxLength(255);

            builder.Property(e => e.FullNameFather1).HasMaxLength(255);

            builder.Property(e => e.FullNameFather2).HasMaxLength(255);

            builder.Property(e => e.FullNameFather3).HasMaxLength(255);

            builder.Property(e => e.FullNameMother1).HasMaxLength(255);

            builder.Property(e => e.FullNameMother2).HasMaxLength(255);

            builder.Property(e => e.FullNameMother3).HasMaxLength(255);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.MothersBirthPlace).HasMaxLength(255);

            builder.Property(e => e.MothersFirstName).HasMaxLength(255);

            builder.Property(e => e.MothersLastName).HasMaxLength(255);

            builder.Property(e => e.MothersMaidenName).HasMaxLength(255);

            builder.Property(e => e.MothersMiddleName).HasMaxLength(255);

            builder.Property(e => e.NotaryCommissionExpires).HasMaxLength(255);

            builder.Property(e => e.NotaryFirstName).HasMaxLength(255);

            builder.Property(e => e.NotaryLastName).HasMaxLength(255);

            builder.Property(e => e.NotaryMiddleName).HasMaxLength(255);

            builder.Property(e => e.NotarySwornDate).HasColumnType("datetime");

            builder.Property(e => e.StateRegistrar).HasMaxLength(255);

            builder.Property(e => e.TypeDocument1).HasMaxLength(255);

            builder.Property(e => e.TypeDocument2).HasMaxLength(255);

            builder.Property(e => e.TypeDocument3).HasMaxLength(255);

            builder.Property(e => e.WhomSigned1).HasMaxLength(255);

            builder.Property(e => e.WhomSigned2).HasMaxLength(255);

            builder.Property(e => e.WhomSigned3).HasMaxLength(255);
        }
    }
}