﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class GroupListConfiguration : IEntityTypeConfiguration<GroupList>
    {
        public void Configure(EntityTypeBuilder<GroupList> builder)
        {
            builder.ToTable("GroupList");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.GroupDescription).HasMaxLength(255);

            builder.Property(e => e.GroupName).HasMaxLength(255);

            builder.Property(e => e.UserDefinedDescription).HasMaxLength(255);
        }
    }
}