﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DogArchiveConfiguration : IEntityTypeConfiguration<DogArchive>
    {
        public void Configure(EntityTypeBuilder<DogArchive> builder)
        {
            builder.ToTable("DogArchive");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.CurrentOwnerId).HasColumnName("CurrentONum");

            builder.Property(e => e.OldOwnerId).HasColumnName("OldONum");

            builder.Property(e => e.ThisRecDate).HasColumnType("datetime");

            builder.Property(e => e.Transaction).HasMaxLength(50);
        }
    }
}