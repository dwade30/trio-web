﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DefaultRegistrarNameConfiguration : IEntityTypeConfiguration<DefaultRegistrarName>
    {
        public void Configure(EntityTypeBuilder<DefaultRegistrarName> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ClerkTown).HasMaxLength(255);

            builder.Property(e => e.ClerkTownId).HasColumnName("ClerkTownID");

            builder.Property(e => e.FirstName).HasMaxLength(255);

            builder.Property(e => e.LastName).HasMaxLength(255);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.Mi)
                .HasColumnName("MI")
                .HasMaxLength(255);

            builder.Property(e => e.Name).HasMaxLength(200);
        }
    }
}