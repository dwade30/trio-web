﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class StateConfiguration : IEntityTypeConfiguration<State>
    {
        public void Configure(EntityTypeBuilder<State> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ConcurrencyId).HasColumnName("ConcurrencyID");

            builder.Property(e => e.Description).HasMaxLength(40);

            builder.Property(e => e.LastUpdate).HasColumnType("datetime");

            builder.Property(e => e.LastUserId).HasMaxLength(10);

            builder.Property(e => e.StateAbbreviation)
                .HasColumnName("State")
                .HasMaxLength(2);
        }
    }
}