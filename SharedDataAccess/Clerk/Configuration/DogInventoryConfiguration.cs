﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DogInventoryConfiguration : IEntityTypeConfiguration<DogInventory>
    {
        public void Configure(EntityTypeBuilder<DogInventory> builder)
        {

                builder.ToTable("DogInventory");

                builder.Property(e => e.Id).HasColumnName("ID");

                builder.Property(e => e.DateReceived).HasColumnType("datetime");

                builder.Property(e => e.LastUpdated).HasColumnType("datetime");

                builder.Property(e => e.PermTemp).HasMaxLength(50);

                builder.Property(e => e.ReceivedOpid)
                    .HasColumnName("ReceivedOPID")
                    .HasMaxLength(50);

                builder.Property(e => e.Status).HasMaxLength(50);

                builder.Property(e => e.StickerNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                builder.Property(e => e.Type).HasMaxLength(50);
            
        }
    }
}