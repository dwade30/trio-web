﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class BirthsForeignConfiguration : IEntityTypeConfiguration<BirthsForeign>
    {
        public void Configure(EntityTypeBuilder<BirthsForeign> builder)
        {
            builder.ToTable("BirthsForeign");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ActualDate).HasColumnType("datetime");

            builder.Property(e => e.AmendedInfo).HasMaxLength(255);

            builder.Property(e => e.ChildCountry).HasMaxLength(255);

            builder.Property(e => e.ChildDob)
                .HasColumnName("ChildDOB")
                .HasMaxLength(255);

            builder.Property(e => e.ChildDobdescription)
                .HasColumnName("ChildDOBDescription")
                .HasMaxLength(255);

            builder.Property(e => e.ChildFirstName).HasMaxLength(255);

            builder.Property(e => e.ChildLastName).HasMaxLength(255);

            builder.Property(e => e.ChildMiddleName).HasMaxLength(255);

            builder.Property(e => e.ChildSex).HasMaxLength(255);

            builder.Property(e => e.ChildState).HasMaxLength(255);

            builder.Property(e => e.ChildTown).HasMaxLength(255);

            builder.Property(e => e.ClerkAttestDate).HasMaxLength(255);

            builder.Property(e => e.ClerkFirstName).HasMaxLength(255);

            builder.Property(e => e.ClerkLastName).HasMaxLength(255);

            builder.Property(e => e.ClerkMiddleName).HasMaxLength(255);

            builder.Property(e => e.ClerkName).HasMaxLength(255);

            builder.Property(e => e.ClerkTown).HasMaxLength(255);

            builder.Property(e => e.DeceasedDate).HasColumnType("datetime");

            builder.Property(e => e.FatherDob)
                .HasColumnName("FatherDOB")
                .HasMaxLength(50);

            builder.Property(e => e.FathersAge).HasMaxLength(255);

            builder.Property(e => e.FathersDesignation).HasMaxLength(255);

            builder.Property(e => e.FathersFirstName).HasMaxLength(255);

            builder.Property(e => e.FathersLastName).HasMaxLength(255);

            builder.Property(e => e.FathersMiddleName).HasMaxLength(255);

            builder.Property(e => e.FathersStateOfBirth).HasMaxLength(255);

            builder.Property(e => e.FileNumber).HasMaxLength(255);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.MotherDob)
                .HasColumnName("MotherDOB")
                .HasMaxLength(50);

            builder.Property(e => e.MothersAge).HasMaxLength(255);

            builder.Property(e => e.MothersFirstName).HasMaxLength(255);

            builder.Property(e => e.MothersLastName).HasMaxLength(255);

            builder.Property(e => e.MothersMaidenName).HasMaxLength(255);

            builder.Property(e => e.MothersMiddleName).HasMaxLength(255);

            builder.Property(e => e.MothersState).HasMaxLength(255);

            builder.Property(e => e.MothersStateOfBirth).HasMaxLength(255);

            builder.Property(e => e.MothersStreet).HasMaxLength(255);

            builder.Property(e => e.MothersTown).HasMaxLength(255);
        }
    }
}