﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DogLicenseReportConfiguration : IEntityTypeConfiguration<DogLicenseReport>
    {
        public void Configure(EntityTypeBuilder<DogLicenseReport> builder)
        {
            builder.ToTable("DogLicenseReport");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Address1).HasMaxLength(50);

            builder.Property(e => e.Address2).HasMaxLength(50);

            builder.Property(e => e.ClerkName).HasMaxLength(50);

            builder.Property(e => e.Municipality).HasMaxLength(50);

            builder.Property(e => e.Phone).HasMaxLength(50);
        }
    }
}