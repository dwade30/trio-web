﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DatabaseVersionConfiguration : IEntityTypeConfiguration<DatabaseVersion>
    {
        public void Configure(EntityTypeBuilder<DatabaseVersion> builder)
        {
            builder.ToTable("DatabaseVersion");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");
        }
    }
}