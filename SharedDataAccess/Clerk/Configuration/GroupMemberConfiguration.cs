﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class GroupMemberConfiguration : IEntityTypeConfiguration<GroupMember>
    {
        public void Configure(EntityTypeBuilder<GroupMember> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Address1).HasMaxLength(255);

            builder.Property(e => e.Address2).HasMaxLength(255);

            builder.Property(e => e.CellNumber).HasMaxLength(255);

            builder.Property(e => e.City).HasMaxLength(255);

            builder.Property(e => e.Email).HasMaxLength(255);

            builder.Property(e => e.GroupId).HasColumnName("GroupID");

            builder.Property(e => e.HomeNumber).HasMaxLength(255);

            builder.Property(e => e.Name).HasMaxLength(255);

            builder.Property(e => e.OtherNumber).HasMaxLength(255);

            builder.Property(e => e.State).HasMaxLength(255);

            builder.Property(e => e.Telephone).HasMaxLength(255);

            builder.Property(e => e.TermFrom).HasColumnType("datetime");

            builder.Property(e => e.TermTo).HasColumnType("datetime");

            builder.Property(e => e.Title).HasMaxLength(255);

            builder.Property(e => e.UserDefined).HasMaxLength(255);

            builder.Property(e => e.Zip).HasMaxLength(25);

            builder.Property(e => e.Zip4).HasMaxLength(25);
        }
    }
}