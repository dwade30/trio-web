﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class LinkedDocumentConfiguration : IEntityTypeConfiguration<LinkedDocument>
    {
        public void Configure(EntityTypeBuilder<LinkedDocument> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.DocReferenceId).HasColumnName("DocReferenceID");

            builder.Property(e => e.FileName).HasMaxLength(255);
        }
    }
}