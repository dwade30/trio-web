﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class PrinterSettingConfiguration : IEntityTypeConfiguration<PrinterSetting>
    {
        public void Configure(EntityTypeBuilder<PrinterSetting> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.BirthAdjustment).HasMaxLength(255);

            builder.Property(e => e.BurialAdjustment).HasMaxLength(255);

            builder.Property(e => e.DateVaccinated).HasColumnType("datetime");

            builder.Property(e => e.DeathAdjustment).HasMaxLength(255);

            builder.Property(e => e.DogAdjustment).HasMaxLength(255);

            builder.Property(e => e.KennelAdjustment).HasMaxLength(255);

            builder.Property(e => e.LaserLineAdjustment).HasMaxLength(255);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.LicenseYear).HasMaxLength(255);

            builder.Property(e => e.MarriageAdjustment).HasMaxLength(255);

            builder.Property(e => e.MarriageLicenseAdjustment).HasMaxLength(255);
        }
    }
}