﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class FacilityNameConfiguration: IEntityTypeConfiguration<FacilityName>
    {
        public void Configure(EntityTypeBuilder<FacilityName> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Address1).HasMaxLength(255);

            builder.Property(e => e.Address2).HasMaxLength(255);

            builder.Property(e => e.City).HasMaxLength(255);

            builder.Property(e => e.Facility).HasMaxLength(255);

            builder.Property(e => e.FacilityNumber).HasMaxLength(255);

            builder.Property(e => e.FirstName).HasMaxLength(255);

            builder.Property(e => e.LastName).HasMaxLength(255);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.State).HasMaxLength(255);

            builder.Property(e => e.Zip).HasMaxLength(255);
        }
    }
}