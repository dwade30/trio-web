﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DefaultColorConfiguration : IEntityTypeConfiguration<DefaultColor>
    {
        public void Configure(EntityTypeBuilder<DefaultColor> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.Name).HasMaxLength(255);
        }
    }
}