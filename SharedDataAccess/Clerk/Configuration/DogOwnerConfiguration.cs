﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DogOwnerConfiguration : IEntityTypeConfiguration<DogOwner>
    {
        public void Configure(EntityTypeBuilder<DogOwner> builder)
        {
            builder.ToTable("DogOwner");
            builder.HasKey(d => d.Id);
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.DogNumbers).HasColumnName("dogNUMBERs");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.LocationStr).HasColumnName("LocationSTR");

            builder.Property(e => e.PartyId).HasColumnName("PartyID");
        
        }
    }
}