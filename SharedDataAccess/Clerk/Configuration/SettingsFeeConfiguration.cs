﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class SettingsFeeConfiguration : IEntityTypeConfiguration<SettingsFee>
    {
        public void Configure(EntityTypeBuilder<SettingsFee> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AgentFeeAtv)
                .HasColumnName("AgentFeeATV")
                .HasColumnType("money");

            builder.Property(e => e.AgentFeeBoat).HasColumnType("money");

            builder.Property(e => e.AgentFeeSnow).HasColumnType("money");

            builder.Property(e => e.AgentRegFeeAtv)
                .HasColumnName("AgentRegFeeATV")
                .HasColumnType("money");

            builder.Property(e => e.AgentRegFeeBoat).HasColumnType("money");

            builder.Property(e => e.AgentRegFeeSnow).HasColumnType("money");

            builder.Property(e => e.DangerousClerkFee).HasColumnType("decimal(5, 2)");

            builder.Property(e => e.DangerousLateFee).HasColumnType("decimal(5, 2)");

            builder.Property(e => e.DangerousStateFee).HasColumnType("decimal(5, 2)");

            builder.Property(e => e.DangerousTownFee).HasColumnType("decimal(5, 2)");

            builder.Property(e => e.DogTagFee).HasColumnType("money");

            builder.Property(e => e.FixedDog)
                .HasColumnName("Fixed_Dog")
                .HasColumnType("money");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.LateDog)
                .HasColumnName("Late_Dog")
                .HasColumnType("money");

            builder.Property(e => e.NuisanceClerkFee).HasColumnType("decimal(5, 2)");

            builder.Property(e => e.NuisanceLateFee).HasColumnType("decimal(5, 2)");

            builder.Property(e => e.NuisanceStateFee).HasColumnType("decimal(5, 2)");

            builder.Property(e => e.NuisanceTownFee).HasColumnType("decimal(5, 2)");

            builder.Property(e => e.ReplacementDog)
                .HasColumnName("Replacement_Dog")
                .HasColumnType("money");

            builder.Property(e => e.TransferDog)
                .HasColumnName("Transfer_Dog")
                .HasColumnType("money");

            builder.Property(e => e.UnFixedDog)
                .HasColumnName("UnFixed_Dog")
                .HasColumnType("money");

            builder.Property(e => e.UserDefinedDescription1).HasMaxLength(50);

            builder.Property(e => e.WarrantDog)
                .HasColumnName("Warrant_Dog")
                .HasColumnType("money");
        }
    }
}