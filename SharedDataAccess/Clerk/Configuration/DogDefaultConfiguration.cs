﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DogDefaultConfiguration : IEntityTypeConfiguration<DogDefault>
    {
        public void Configure(EntityTypeBuilder<DogDefault> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.City).HasMaxLength(50);

            builder.Property(e => e.State).HasMaxLength(50);

            builder.Property(e => e.Zip)
                .HasColumnName("zip")
                .HasMaxLength(50);
        }
    }
}