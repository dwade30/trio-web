﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DefaultNameConfiguration : IEntityTypeConfiguration<DefaultName>
    {
        public void Configure(EntityTypeBuilder<DefaultName> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.Name).HasMaxLength(200);

            builder.Property(e => e.Other).HasMaxLength(50);

            builder.Property(e => e.Title).HasMaxLength(50);
        }
    }
}