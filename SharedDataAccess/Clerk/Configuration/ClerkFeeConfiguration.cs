﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class ClerkFeeConfiguration: IEntityTypeConfiguration<ClerkFee>
    {
        public void Configure(EntityTypeBuilder<ClerkFee> builder)
        {
            builder.ToTable("ClerkFees");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.AdditionalFeeCaption1).HasMaxLength(255);

            builder.Property(e => e.AdditionalFeeCaption2).HasMaxLength(255);

            builder.Property(e => e.AdditionalFeeCaption3).HasMaxLength(255);

            builder.Property(e => e.AdditionalFeeCaption4).HasMaxLength(255);

            builder.Property(e => e.AdditionalFeeCaption5).HasMaxLength(255);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");
        }
    }
}