﻿using System.Collections.Generic;
using Autofac;
using SharedApplication;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterClerkRepositoryFactory(ref builder);
            RegisterDogSearchQueryHandler(ref builder);
            RegisterDogQueryHandler(ref builder);
            RegisterKennelQueryHandler(ref builder);
            RegisterClerkContext(ref builder);
        }


        private void RegisterClerkContext(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return tcf.GetClerkContext();
            }).As<IClerkContext>().AsSelf();
        }

        private void RegisterDogSearchQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<DogSearchQueryHandler>()
                .As<IQueryHandler<DogSearchCriteria, IEnumerable<DogSearchResult>>>();
        }

        private void RegisterClerkRepositoryFactory(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<ITrioContextFactory>();
                return new ClerkRepositoryFactory(tcf.GetClerkContext());
            }).As<IClerkRepositoryFactory>();

            
        }

        private void RegisterDogQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<DogQueryHandler>().As<IQueryHandler<DogQuery, DogInfo>>();
        }

        private void RegisterKennelQueryHandler(ref ContainerBuilder builder)
        {
            builder.RegisterType<KennelQueryHandler>().As<IQueryHandler<KennelQuery, KennelLicense>>();
        }

    }
}