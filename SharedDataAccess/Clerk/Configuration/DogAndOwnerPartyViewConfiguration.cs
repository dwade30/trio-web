﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DogAndOwnerPartyViewConfiguration : IQueryTypeConfiguration<DogAndOwnerPartyView>
    {
        public void Configure(QueryTypeBuilder<DogAndOwnerPartyView> builder)
        {
            //builder.HasNoKey();

            builder.ToView("DogAndOwnerPartyView");

            builder.Property(e => e.AmountC).HasColumnType("money");

            builder.Property(e => e.DateVaccinated).HasColumnType("datetime");

            builder.Property(e => e.Designation).HasMaxLength(50);

            builder.Property(e => e.DogBreed).HasMaxLength(50);

            builder.Property(e => e.DogColor).HasMaxLength(50);

            builder.Property(e => e.DogDob)
                .HasColumnName("DogDOB")
                .HasColumnType("datetime");

            builder.Property(e => e.DogName).HasMaxLength(50);

            builder.Property(e => e.DogNumbers).HasColumnName("dogNUMBERs");

            builder.Property(e => e.DogSex).HasMaxLength(50);

            builder.Property(e => e.DogToKennelDate).HasColumnType("datetime");

            builder.Property(e => e.Dogmarkings)
                .HasColumnName("dogmarkings")
                .HasMaxLength(255);

            builder.Property(e => e.Dogvet)
                .HasColumnName("dogvet")
                .HasMaxLength(255);

            builder.Property(e => e.Email).HasMaxLength(200);

            builder.Property(e => e.FirstName).HasMaxLength(255);

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.LastName).HasMaxLength(100);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.MiddleName).HasMaxLength(100);

            builder.Property(e => e.NewTagAdded).HasMaxLength(255);

            builder.Property(e => e.NewTagAddedDate).HasColumnType("datetime");

            builder.Property(e => e.PreviousTag).HasMaxLength(255);

            builder.Property(e => e.RabStickerIssue).HasColumnType("datetime");

            builder.Property(e => e.RabStickerNum).HasMaxLength(50);

            builder.Property(e => e.RabiesCertNum).HasMaxLength(50);

            builder.Property(e => e.RabiesExpDate).HasColumnType("datetime");

            builder.Property(e => e.RabiesShotDate).HasColumnType("datetime");

            builder.Property(e => e.RabiesTagNo).HasMaxLength(50);

            builder.Property(e => e.ReplacementLicenseDate).HasColumnType("datetime");

            builder.Property(e => e.ReplacementStickerDate).HasColumnType("datetime");

            builder.Property(e => e.ReplacementTagDate).HasColumnType("datetime");

            builder.Property(e => e.SncertNum)
                .HasColumnName("SNCertNum")
                .HasMaxLength(50);

            builder.Property(e => e.StickerLicNum).HasMaxLength(50);

            builder.Property(e => e.TagLicNum).HasMaxLength(50);

            builder.Property(e => e.TransferLicenseDate).HasColumnType("datetime");

            builder.Property(e => e.WebAddress).HasMaxLength(200);
        }
    }
}