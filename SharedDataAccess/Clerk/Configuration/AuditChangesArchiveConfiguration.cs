﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class AuditChangesArchiveConfiguration : IEntityTypeConfiguration<AuditChangesArchive>
    {
        public void Configure(EntityTypeBuilder<AuditChangesArchive> builder)
        {
            builder.ToTable("AuditChangesArchive");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ChangeDescription).HasMaxLength(255);

            builder.Property(e => e.DateUpdated).HasColumnType("datetime");

            builder.Property(e => e.Location).HasMaxLength(255);

            builder.Property(e => e.TimeUpdated).HasColumnType("datetime");

            builder.Property(e => e.UserField1).HasMaxLength(255);

            builder.Property(e => e.UserField2).HasMaxLength(255);

            builder.Property(e => e.UserField3).HasMaxLength(255);

            builder.Property(e => e.UserField4).HasMaxLength(255);

            builder.Property(e => e.UserId)
                .HasColumnName("UserID")
                .HasMaxLength(255);
        }
    }
}