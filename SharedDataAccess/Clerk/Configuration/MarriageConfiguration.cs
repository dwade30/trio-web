﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class MarriageConfiguration : IEntityTypeConfiguration<Marriage>
    {
        public void Configure(EntityTypeBuilder<Marriage> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ActualDate).HasColumnType("datetime");

            builder.Property(e => e.BrideSsn)
                .HasColumnName("BrideSSN")
                .HasMaxLength(50);

            builder.Property(e => e.BrideZip).HasMaxLength(255);

            builder.Property(e => e.BridesBirthplace).HasMaxLength(50);

            builder.Property(e => e.BridesCity).HasMaxLength(50);

            builder.Property(e => e.BridesCounty).HasMaxLength(50);

            builder.Property(e => e.BridesCourtName).HasMaxLength(255);

            builder.Property(e => e.BridesCurrentLastName).HasMaxLength(50);

            builder.Property(e => e.BridesDob)
                .HasColumnName("BridesDOB")
                .HasMaxLength(255);

            builder.Property(e => e.BridesFathersBirthplace).HasMaxLength(50);

            builder.Property(e => e.BridesFathersName).HasMaxLength(100);

            builder.Property(e => e.BridesFirstName).HasMaxLength(50);

            builder.Property(e => e.BridesFormerSpouseName).HasMaxLength(255);

            builder.Property(e => e.BridesMaidenSurname).HasMaxLength(50);

            builder.Property(e => e.BridesMarriageNumber).HasMaxLength(50);

            builder.Property(e => e.BridesMiddleName).HasMaxLength(50);

            builder.Property(e => e.BridesMothersBirthplace).HasMaxLength(50);

            builder.Property(e => e.BridesMothersName).HasMaxLength(100);

            builder.Property(e => e.BridesPrevMarriageEndDate).HasMaxLength(255);

            builder.Property(e => e.BridesPreviousMarriageEnded).HasMaxLength(50);

            builder.Property(e => e.BridesState).HasMaxLength(50);

            builder.Property(e => e.BridesStreetName).HasMaxLength(50);

            builder.Property(e => e.BridesStreetNumber).HasMaxLength(50);

            builder.Property(e => e.CeremonyDate).HasMaxLength(255);

            builder.Property(e => e.CityMarried).HasMaxLength(50);

            builder.Property(e => e.CityOfIssue).HasMaxLength(50);

            builder.Property(e => e.CountyMarried).HasMaxLength(50);

            builder.Property(e => e.DateClerkFiled).HasMaxLength(255);

            builder.Property(e => e.DateIntentionsFiled).HasMaxLength(255);

            builder.Property(e => e.DateLicenseIssued).HasMaxLength(255);

            builder.Property(e => e.DateOfLicenseForPerson).HasMaxLength(255);

            builder.Property(e => e.FileNumber).HasMaxLength(50);

            builder.Property(e => e.GroomSsn)
                .HasColumnName("GroomSSN")
                .HasMaxLength(50);

            builder.Property(e => e.GroomZip).HasMaxLength(255);

            builder.Property(e => e.GroomsBirthplace).HasMaxLength(50);

            builder.Property(e => e.GroomsCity).HasMaxLength(50);

            builder.Property(e => e.GroomsCounty).HasMaxLength(50);

            builder.Property(e => e.GroomsCourtName).HasMaxLength(255);

            builder.Property(e => e.GroomsDesignation).HasMaxLength(50);

            builder.Property(e => e.GroomsDob)
                .HasColumnName("GroomsDOB")
                .HasMaxLength(255);

            builder.Property(e => e.GroomsFathersBirthplace).HasMaxLength(50);

            builder.Property(e => e.GroomsFathersName).HasMaxLength(100);

            builder.Property(e => e.GroomsFirstName).HasMaxLength(50);

            builder.Property(e => e.GroomsFormerSpouseName).HasMaxLength(255);

            builder.Property(e => e.GroomsLastName).HasMaxLength(50);

            builder.Property(e => e.GroomsMarriageNumber).HasMaxLength(50);

            builder.Property(e => e.GroomsMiddleName).HasMaxLength(50);

            builder.Property(e => e.GroomsMothersBirthplace).HasMaxLength(100);

            builder.Property(e => e.GroomsMothersName).HasMaxLength(100);

            builder.Property(e => e.GroomsPrevMarriageEndDate).HasMaxLength(255);

            builder.Property(e => e.GroomsPreviousMarriageEnded).HasMaxLength(50);

            builder.Property(e => e.GroomsState).HasMaxLength(50);

            builder.Property(e => e.GroomsStreetAddress).HasMaxLength(50);

            builder.Property(e => e.GroomsStreetName).HasMaxLength(50);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.LicenseValidUntil).HasMaxLength(50);

            builder.Property(e => e.MailingAddressOfPersonPerformingCeremony).HasMaxLength(255);

            builder.Property(e => e.NameOfClerk).HasMaxLength(75);

            builder.Property(e => e.NameOfWitnessOne).HasMaxLength(100);

            builder.Property(e => e.NameOfWitnessTwo).HasMaxLength(100);

            builder.Property(e => e.PartyAbirthName)
                .HasColumnName("PartyABirthName")
                .HasMaxLength(255);

            builder.Property(e => e.PartyAgender)
                .HasColumnName("PartyAGender")
                .HasMaxLength(10);

            builder.Property(e => e.PartyAtype).HasColumnName("PartyAType");

            builder.Property(e => e.PartyBdesignation)
                .HasColumnName("PartyBDesignation")
                .HasMaxLength(255);

            builder.Property(e => e.PartyBgender)
                .HasColumnName("PartyBGender")
                .HasMaxLength(10);

            builder.Property(e => e.PartyBtype).HasColumnName("PartyBType");

            builder.Property(e => e.PersonPerformingCeremony).HasMaxLength(100);

            builder.Property(e => e.ResidenceOfPersonPerformingCeremony).HasMaxLength(50);

            builder.Property(e => e.TitleofPersonPerforming).HasMaxLength(50);

            builder.Property(e => e.VoidedDate).HasColumnType("datetime");
        }
    }
}