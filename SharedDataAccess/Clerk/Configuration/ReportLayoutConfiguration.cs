﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class ReportLayoutConfiguration : IEntityTypeConfiguration<ReportLayout>
    {
        public void Configure(EntityTypeBuilder<ReportLayout> builder)
        {
            builder.ToTable("tblReportLayout");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ColumnId).HasColumnName("ColumnID");

            builder.Property(e => e.DisplayText).HasMaxLength(255);

            builder.Property(e => e.FieldId).HasColumnName("FieldID");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.ReportId).HasColumnName("ReportID");

            builder.Property(e => e.RowId).HasColumnName("RowID");
        }
    }
}