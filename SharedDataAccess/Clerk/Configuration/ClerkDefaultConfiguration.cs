﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class ClerkDefaultConfiguration : IEntityTypeConfiguration<ClerkDefault>
    {
        public void Configure(EntityTypeBuilder<ClerkDefault> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Address1).HasMaxLength(250);

            builder.Property(e => e.Address2).HasMaxLength(250);

            builder.Property(e => e.AgentNumber).HasMaxLength(50);

            builder.Property(e => e.BirthDefaultCounty).HasMaxLength(255);

            builder.Property(e => e.BirthDefaultTown).HasMaxLength(255);

            builder.Property(e => e.BoolFileDuplicateWarning).HasColumnName("boolFileDuplicateWarning");

            builder.Property(e => e.City).HasMaxLength(50);

            builder.Property(e => e.County).HasMaxLength(255);

            builder.Property(e => e.LastBirthFileNumber).HasMaxLength(255);

            builder.Property(e => e.LastDeathFileNumber).HasMaxLength(255);

            builder.Property(e => e.LastMarriageFileNumber).HasMaxLength(255);

            builder.Property(e => e.LegalResidence).HasMaxLength(50);

            builder.Property(e => e.MotherDefaultCity).HasMaxLength(255);

            builder.Property(e => e.MotherDefaultCounty).HasMaxLength(255);

            builder.Property(e => e.MotherDefaultState).HasMaxLength(255);

            builder.Property(e => e.Phone).HasMaxLength(50);

            builder.Property(e => e.StateName).HasMaxLength(255);

            builder.Property(e => e.Zip).HasMaxLength(50);
        }
    }
}