﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class CustomReportConfiguration : IEntityTypeConfiguration<CustomReport>
    {
        public void Configure(EntityTypeBuilder<CustomReport> builder)
        {
            builder.ToTable("tblCustomReports");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.ReportName).HasMaxLength(255);

            builder.Property(e => e.Sql).HasColumnName("SQL");

            builder.Property(e => e.Type).HasMaxLength(255);
        }
    }
}