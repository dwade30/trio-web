﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Budgetary.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class AuditChangeConfiguration : IEntityTypeConfiguration<AuditChange>
    {
        public void Configure(EntityTypeBuilder<AuditChange> builder)
        {
            builder.ToTable("AuditChanges");
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ChangeDescription).HasMaxLength(255);

            builder.Property(e => e.DateUpdated).HasColumnType("datetime");

            builder.Property(e => e.Location).HasMaxLength(255);

            builder.Property(e => e.TimeUpdated).HasColumnType("datetime");

            builder.Property(e => e.UserField1).HasMaxLength(255);

            builder.Property(e => e.UserField2).HasMaxLength(255);

            builder.Property(e => e.UserField3).HasMaxLength(255);

            builder.Property(e => e.UserField4).HasMaxLength(255);

            builder.Property(e => e.UserId)
                .HasColumnName("UserID")
                .HasMaxLength(255);
        }
    }
}