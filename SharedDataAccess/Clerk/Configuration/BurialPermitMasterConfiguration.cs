﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class BurialPermitMasterConfiguration : IEntityTypeConfiguration<BurialPermitMaster>
    {
        public void Configure(EntityTypeBuilder<BurialPermitMaster> builder)
        {
            builder.ToTable("BurialPermitMaster");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Age).HasMaxLength(50);

            builder.Property(e => e.Authorizationforpermit)
                .HasColumnName("authorizationforpermit")
                .HasMaxLength(255);

            builder.Property(e => e.BusinessAddress).HasMaxLength(50);

            builder.Property(e => e.BusinessAddress2).HasMaxLength(255);

            builder.Property(e => e.BusinessCity).HasMaxLength(255);

            builder.Property(e => e.BusinessState).HasMaxLength(255);

            builder.Property(e => e.BusinessZip).HasMaxLength(255);

            builder.Property(e => e.CityOrTown).HasMaxLength(50);

            builder.Property(e => e.ClerkTown).HasMaxLength(255);

            builder.Property(e => e.CremDate).HasMaxLength(50);

            builder.Property(e => e.CremLoc).HasMaxLength(255);

            builder.Property(e => e.CremName).HasMaxLength(255);

            builder.Property(e => e.DateOfDeath).HasMaxLength(50);

            builder.Property(e => e.DateOfDisposition).HasMaxLength(50);

            builder.Property(e => e.DateSigned).HasMaxLength(50);

            builder.Property(e => e.Designation).HasMaxLength(50);

            builder.Property(e => e.Disposition).HasMaxLength(50);

            builder.Property(e => e.DispositionDate).HasMaxLength(50);

            builder.Property(e => e.Field5).HasMaxLength(50);

            builder.Property(e => e.FirstName).HasMaxLength(50);

            builder.Property(e => e.LastName).HasMaxLength(50);

            builder.Property(e => e.LicenseNumber).HasMaxLength(50);

            builder.Property(e => e.Location).HasMaxLength(50);

            builder.Property(e => e.LocationState).HasMaxLength(255);

            builder.Property(e => e.MiddleName).HasMaxLength(50);

            builder.Property(e => e.NameOfCemetery).HasMaxLength(50);

            builder.Property(e => e.NameOfClerkOrSubregistrar).HasMaxLength(50);

            builder.Property(e => e.Nameoffuneralestablishment)
                .HasColumnName("nameoffuneralestablishment")
                .HasMaxLength(255);

            builder.Property(e => e.PlaceOfDeathCity).HasMaxLength(50);

            builder.Property(e => e.PlaceOfDeathState).HasMaxLength(50);

            builder.Property(e => e.PlaceOfDisposition).HasMaxLength(50);

            builder.Property(e => e.Race).HasMaxLength(50);

            builder.Property(e => e.Sex).HasMaxLength(50);

            builder.Property(e => e.Signature).HasMaxLength(255);

            builder.Property(e => e.TempCemetery).HasMaxLength(255);

            builder.Property(e => e.TempDate).HasMaxLength(255);

            builder.Property(e => e.TempLocation).HasMaxLength(255);

            builder.Property(e => e.TempSignature).HasMaxLength(255);

            builder.Property(e => e.Typeofpermit)
                .HasColumnName("typeofpermit")
                .HasMaxLength(255);
        }
    }
}