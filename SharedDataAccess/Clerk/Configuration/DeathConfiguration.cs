﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DeathConfiguration : IEntityTypeConfiguration<Death>
    {
        public void Configure(EntityTypeBuilder<Death> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ActualDate).HasColumnType("datetime");

            builder.Property(e => e.Ancestry).HasMaxLength(50);

            builder.Property(e => e.Birthplace).HasMaxLength(50);

            builder.Property(e => e.CauseA).HasMaxLength(255);

            builder.Property(e => e.CauseB).HasMaxLength(255);

            builder.Property(e => e.CauseC).HasMaxLength(255);

            builder.Property(e => e.CauseD).HasMaxLength(255);

            builder.Property(e => e.CityorTownofDeath).HasMaxLength(50);

            builder.Property(e => e.ClerkofRecordCity).HasMaxLength(50);

            builder.Property(e => e.ClerkofRecordName).HasMaxLength(50);

            builder.Property(e => e.CountyofDeath).HasMaxLength(50);

            builder.Property(e => e.DateOfBirth).HasMaxLength(255);

            builder.Property(e => e.DateOfDeathDescription).HasMaxLength(255);

            builder.Property(e => e.DateOriginalFiling).HasMaxLength(50);

            builder.Property(e => e.DateSigned).HasMaxLength(255);

            builder.Property(e => e.DateofDeath).HasColumnType("datetime");

            builder.Property(e => e.DateofDisposition).HasMaxLength(50);

            builder.Property(e => e.Designation).HasMaxLength(50);

            builder.Property(e => e.Disposition).HasMaxLength(50);

            builder.Property(e => e.DispositionLocation).HasMaxLength(50);

            builder.Property(e => e.DispositionOther).HasMaxLength(50);

            builder.Property(e => e.EducationCollege).HasMaxLength(50);

            builder.Property(e => e.EducationElementary).HasMaxLength(50);

            builder.Property(e => e.FacilityName).HasMaxLength(255);

            builder.Property(e => e.FatherDesignation).HasMaxLength(50);

            builder.Property(e => e.FatherFirstName).HasMaxLength(50);

            builder.Property(e => e.FatherLastName).HasMaxLength(50);

            builder.Property(e => e.FatherMiddleName).HasMaxLength(50);

            builder.Property(e => e.FileNumber).HasMaxLength(50);

            builder.Property(e => e.FirstName).HasMaxLength(50);

            builder.Property(e => e.FuneralEstablishmentLicenseNumber).HasMaxLength(50);

            builder.Property(e => e.InformantMailingAddress).HasMaxLength(250);

            builder.Property(e => e.InformantName).HasMaxLength(50);

            builder.Property(e => e.KindofBusiness).HasMaxLength(50);

            builder.Property(e => e.LastName).HasMaxLength(50);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.LicenseeNumber).HasMaxLength(50);

            builder.Property(e => e.MaritalStatus).HasMaxLength(50);

            builder.Property(e => e.MiddleName).HasMaxLength(50);

            builder.Property(e => e.MostRecentSpouse).HasMaxLength(50);

            builder.Property(e => e.MostRecentSpouseName).HasMaxLength(50);

            builder.Property(e => e.MotherFirstName).HasMaxLength(50);

            builder.Property(e => e.MotherLastName).HasMaxLength(50);

            builder.Property(e => e.MotherMiddleName).HasMaxLength(50);

            builder.Property(e => e.NameandAddressofCertifier).HasMaxLength(250);

            builder.Property(e => e.NameandAddressofFacility).HasMaxLength(250);

            builder.Property(e => e.NameofAttendingPhysician).HasMaxLength(50);

            builder.Property(e => e.NameofCertifier).HasMaxLength(50);

            builder.Property(e => e.NameofPhysician).HasMaxLength(50);

            builder.Property(e => e.Occupation).HasMaxLength(50);

            builder.Property(e => e.OnsetA).HasMaxLength(255);

            builder.Property(e => e.OnsetB).HasMaxLength(255);

            builder.Property(e => e.OnsetC).HasMaxLength(255);

            builder.Property(e => e.OnsetD).HasMaxLength(255);

            builder.Property(e => e.PlaceOfDisposition).HasMaxLength(255);

            builder.Property(e => e.PlaceofDeath).HasMaxLength(50);

            builder.Property(e => e.PlaceofDeathOther).HasMaxLength(50);

            builder.Property(e => e.PlaceofDispositionCity).HasMaxLength(50);

            builder.Property(e => e.PlaceofDispositionState).HasMaxLength(50);

            builder.Property(e => e.Race).HasMaxLength(50);

            builder.Property(e => e.ResidenceCityorTown).HasMaxLength(50);

            builder.Property(e => e.ResidenceCounty).HasMaxLength(50);

            builder.Property(e => e.ResidenceState).HasMaxLength(50);

            builder.Property(e => e.ResidenceStreet).HasMaxLength(50);

            builder.Property(e => e.Sex).HasMaxLength(50);

            builder.Property(e => e.Signature).HasMaxLength(50);

            builder.Property(e => e.Socialsecuritynumber)
                .HasColumnName("socialsecuritynumber")
                .HasMaxLength(255);

            builder.Property(e => e.TimeOfDeath).HasMaxLength(255);

            builder.Property(e => e.UsarmedServices).HasColumnName("USArmedServices");
        }
    }
}