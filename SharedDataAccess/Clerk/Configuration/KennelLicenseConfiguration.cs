﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class KennelLicenseConfiguration : IEntityTypeConfiguration<KennelLicense>
    {
        public void Configure(EntityTypeBuilder<KennelLicense> builder)
        {
            builder.ToTable("KennelLicense");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.DogNumbers).HasMaxLength(255);

            builder.Property(e => e.InspectionDate).HasColumnType("datetime");

            builder.Property(e => e.LicenseId).HasColumnName("LicenseID");

            builder.Property(e => e.OriginalIssueDate).HasColumnType("datetime");

            builder.Property(e => e.ReIssueDate).HasColumnType("datetime");
            builder.HasOne(o => o.Owner)
                .WithMany(d => d.KennelLicenses)
                .HasForeignKey(o => o.OwnerNum)
                .IsRequired(false);
        }
    }
}