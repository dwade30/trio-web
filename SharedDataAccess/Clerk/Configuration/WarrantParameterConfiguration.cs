﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class WarrantParameterConfiguration : IEntityTypeConfiguration<WarrantParameter>
    {
        public void Configure(EntityTypeBuilder<WarrantParameter> builder)
        {
            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.ControlOfficer).HasMaxLength(200);

            builder.Property(e => e.CourtCounty).HasMaxLength(200);

            builder.Property(e => e.CourtTown).HasMaxLength(200);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.TownCounty).HasMaxLength(200);

            builder.Property(e => e.TownName).HasMaxLength(200);
        }
    }
}