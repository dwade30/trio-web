﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DispositionLocationConfiguration : IEntityTypeConfiguration<DispositionLocation>
    {
        public void Configure(EntityTypeBuilder<DispositionLocation> builder)
        {
            builder.ToTable("DispositionLocation");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.Name).HasMaxLength(200);
        }
    }
}