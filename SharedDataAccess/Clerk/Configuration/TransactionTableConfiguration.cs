﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class TransactionTableConfiguration : IEntityTypeConfiguration<TransactionTable>
    {
        public void Configure(EntityTypeBuilder<TransactionTable> builder)
        {
            builder.ToTable("TransactionTable");

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.Description).HasMaxLength(255);

            builder.Property(e => e.TransactionDate).HasColumnType("datetime");

            builder.Property(e => e.TransactionPerson).HasMaxLength(255);

            builder.Property(e => e.UserId).HasColumnName("UserID");

            builder.Property(e => e.UserName).HasMaxLength(255);
        }
    }
}