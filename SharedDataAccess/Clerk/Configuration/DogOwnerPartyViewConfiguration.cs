﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SharedApplication.Clerk.Models;

namespace SharedDataAccess.Clerk.Configuration
{
    public class DogOwnerPartyViewConfiguration : IQueryTypeConfiguration<DogOwnerPartyView>
    {
        public void Configure(QueryTypeBuilder<DogOwnerPartyView> builder)
        {
            //builder.HasNoKey();

            builder.ToView("DogOwnerPartyView");

            builder.Property(e => e.Designation).HasMaxLength(50);

            builder.Property(e => e.DogNumbers).HasColumnName("dogNUMBERs");

            builder.Property(e => e.Email).HasMaxLength(200);

            builder.Property(e => e.FirstName).HasMaxLength(255);

            builder.Property(e => e.Id).HasColumnName("ID");

            builder.Property(e => e.LastName).HasMaxLength(100);

            builder.Property(e => e.LastUpdated).HasColumnType("datetime");

            builder.Property(e => e.LocationStr).HasColumnName("LocationSTR");

            builder.Property(e => e.MiddleName).HasMaxLength(100);

            builder.Property(e => e.PartyId).HasColumnName("PartyID");

            builder.Property(e => e.WebAddress).HasMaxLength(200);
        }
    }
}