﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using SharedApplication;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Models;
using SharedDataAccess.Clerk.Configuration;

namespace SharedDataAccess.Clerk
{
    public class WritableDBSet<TEntity> : DbSet<TEntity>, IWritableSet<TEntity> where TEntity : class
    {
        public void AddEntity(TEntity entity)
        {
            this.Add(entity);
        }

    }
    public class ClerkContext : DbContext, IClerkContext
    {
        public ClerkContext()
        {
        }

        public ClerkContext(DbContextOptions<ClerkContext> options)
            : base(options)
        {

        }

        //public virtual DbSet<Amendment> Amendments { get; set; }
        //public virtual DbSet<AuditChange> AuditChanges { get; set; }
        //public virtual DbSet<AuditChangesArchive> AuditChangesArchives { get; set; }
        public DbSet<Birth> Births { get; set; }
        public DbSet<BirthsDelayed> BirthsDelayeds { get; set; }
        public DbSet<BirthsForeign> BirthsForeigns { get; set; }
        public DbSet<Marriage> Marriages { get; set; }
        //public virtual DbSet<BurialPermitMaster> BurialPermitMasters { get; set; }
        //public virtual DbSet<ClerkDefault> ClerkDefaults { get; set; }
        public virtual WritableDBSet<ClerkFee> ClerkFees { get; set; }
        public DbSet<Cya> Cyas { get; set; }
        //public virtual DbSet<DatabaseVersion> DatabaseVersions { get; set; }
        //public virtual DbSet<Dbversion> Dbversions { get; set; }
        public DbSet<Death> Deaths { get; set; }
        //public virtual DbSet<DefaultBreed> DefaultBreeds { get; set; }
        //public virtual DbSet<DefaultColor> DefaultColors { get; set; }
        //public virtual DbSet<DefaultCounty> DefaultCounties { get; set; }
        //public virtual DbSet<DefaultName> DefaultNames { get; set; }
        //public virtual DbSet<DefaultRegistrarName> DefaultRegistrarNames { get; set; }
        //public virtual DbSet<DefaultTown> DefaultTowns { get; set; }
        //public virtual DbSet<DispositionLocation> DispositionLocations { get; set; }
        //public virtual DbSet<DogArchive> DogArchives { get; set; }
        //public virtual DbSet<DogDefault> DogDefaults { get; set; }

        public virtual DbQuery<DogAndOwnerPartyView> DogAndOwnerParties { get; set; }
        public virtual DbSet<DogInfo> DogInfos { get; set; }
        public virtual DbSet<DogInventory> DogInventories { get; set; }
        //public virtual DbSet<DogLicenseReport> DogLicenseReports { get; set; }
        
        public virtual DbSet<DogOwner> DogOwners { get; set; }
        public virtual DbQuery<DogOwnerPartyView> DogOwnerParties { get; set; }
        IQueryable<DogAndOwnerPartyView> IClerkContext.DogAndOwnerParties => DogAndOwnerParties;
        IQueryable<DogInfo> IClerkContext.DogInfos { get => DogInfos; }
        IQueryable<DogOwner> IClerkContext.DogOwners { get => DogOwners; }
        IQueryable<Cya> IClerkContext.Cyas { get => Cyas; }
        IQueryable<Birth> IClerkContext.Births { get => Births; }
        IQueryable<BirthsDelayed> IClerkContext.BirthsDelayeds { get => BirthsDelayeds; }
        IQueryable<BirthsForeign> IClerkContext.BirthsForeigns { get => BirthsForeigns; }
        IQueryable<Marriage> IClerkContext.Marriages { get => Marriages; }
        IQueryable<Death> IClerkContext.Deaths { get => Deaths; }
        IQueryable<DogOwnerPartyView> IClerkContext.DogOwnerParties => DogOwnerParties;
        IQueryable<KennelLicense> IClerkContext.KennelLicenses
        {
            get => KennelLicenses;
        }
        IQueryable<DogInventory> IClerkContext.DogInventories
        {
            get => DogInventories;
        }

        IQueryable<DogTransaction> IClerkContext.DogTransactions => DogTransactions;
        IQueryable<TransactionTable> IClerkContext.TransactionTables => TransactionTables;

        public virtual DbSet<DogTransaction> DogTransactions { get; set; }
        //public virtual DbSet<FacilityName> FacilityNames { get; set; }
        //public virtual DbSet<GroupList> GroupLists { get; set; }
        //public virtual DbSet<GroupMember> GroupMembers { get; set; }
        public virtual DbSet<KennelLicense> KennelLicenses { get; set; }
        //public virtual DbSet<LinkedDocument> LinkedDocuments { get; set; }
        
        //public virtual DbSet<PrinterSetting> PrinterSettings { get; set; }
        //public virtual DbSet<SettingsFee> SettingsFees { get; set; }
        //public virtual DbSet<State> States { get; set; }
        //public virtual DbSet<CustomReport> CustomReports { get; set; }
        //public virtual DbSet<ReportLayout> ReportLayouts { get; set; }
        //public virtual DbSet<Title> Titles { get; set; }
        public virtual DbSet<TransactionTable> TransactionTables { get; set; }
        //public virtual DbSet<Veterinarian> Veterinarians { get; set; }
        //public virtual DbSet<VitalRecordCertNum> VitalRecordCertNums { get; set; }
        //public virtual DbSet<WarrantParameter> WarrantParameters { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //    }
        //}

       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            // modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new DogOwnerConfiguration());
            modelBuilder.ApplyConfiguration(new DogInfoConfiguration());
            modelBuilder.ApplyConfiguration(new KennelLicenseConfiguration());
            modelBuilder.ApplyConfiguration(new DogInventoryConfiguration());
            modelBuilder.ApplyConfiguration(new ClerkFeeConfiguration());
            modelBuilder.ApplyConfiguration(new CyaConfiguration());
            modelBuilder.ApplyConfiguration(new BirthConfiguration());
            modelBuilder.ApplyConfiguration(new BirthsDelayedConfiguration());
            modelBuilder.ApplyConfiguration(new BirthsForeignConfiguration());
            modelBuilder.ApplyConfiguration(new MarriageConfiguration());
            modelBuilder.ApplyConfiguration(new DeathConfiguration());
            modelBuilder.ApplyConfiguration(new DogAndOwnerPartyViewConfiguration());
            modelBuilder.ApplyConfiguration(new DogOwnerPartyViewConfiguration());
            modelBuilder.ApplyConfiguration(new DogTransactionConfiguration());
            modelBuilder.ApplyConfiguration(new TransactionTableConfiguration());
            //modelBuilder.ApplyConfigurationsFromAssembly(typeof(ClerkContext).Assembly);
            //  OnModelCreatingPartial(modelBuilder);

        }

       // partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}