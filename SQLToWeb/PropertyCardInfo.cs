﻿using System;

namespace SQLToWeb
{
    public class PropertyCardInfo
    {
        public int Account { get; set; } = 0;
        public int Card { get; set; } = 0;
        public string CardId { get; set; } = "";
        public int Id { get; set; } = 0;
        public Guid CardIdentifier
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(CardId))
                {
                   return Guid.Parse(CardId);
                }
                return Guid.Empty;
            }
        }
    }

    public class SketchCardInfo
    {
        public int Account { get; set; } = 0;
        public int Card { get; set; } = 0;
        public int Id { get; set; } = 0;
        public Guid CardIdentifier { get; set; } = Guid.Empty;
        public int SequenceNumber { get; set; } = 0;

    }
}