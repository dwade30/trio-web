﻿using System.Collections.Generic;

namespace SQLToWeb
{
    public class Client
    {
        public string Name { get; set; } = "";
        public string DataSource { get; set; } = "";
        public string UserName { get; set; } = "";
        public string Password { get; set; } = "";
        public List<WebDataEnvironment> DataEnvironments { get; set; } = new List<WebDataEnvironment>();
    }
}