using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Identity.Client;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace SQLToWeb
{
    public partial class frmConversion : Form
    {
        private bool loggedIn = false;
        private static string strToUse = "B4A45E16-2828-4157-908E-F2266FA1810A";
        private static string sToUse = "O72SajdzgdE=";
        private static string ClientId = "648198d1-cf6a-41f5-8c3c-53b05f3d7b57";

        // Note: Tenant is important for the quickstart. We'd need to check with Andre/Portal if we
        // want to change to the AadAuthorityAudience.
        private static string Tenant = "d0748657-f5a8-4d8b-8a16-b848c23ea8cb";
        private static string Instance = "https://login.microsoftonline.com/";
        private static IPublicClientApplication _clientApp;
        string graphAPIEndpoint = "https://graph.microsoft.com/v1.0/me";

        //Set the scope for API call to user.read
        private string[] scopes = new string[] { };
        //public static IPublicClientApplication PublicClientApp { get { return _clientApp; } }

        public frmConversion()
        {
            InitializeComponent();
            InitializeComponentEx();
            _clientApp = PublicClientApplicationBuilder.Create(ClientId)
                .WithAuthority($"{Instance}{Tenant}")
                .WithDefaultRedirectUri()
                .Build();
            TokenCacheHelper.EnableSerialization(_clientApp.UserTokenCache);
        }

        private void InitializeComponentEx()
        {
            this.txtSQLConfigPath.Validated += TxtSQLConfigPath_Validated;
            this.txtWebConfigPath.Validated += TxtWebConfigPath_Validated;
            this.btnSQLConfiguration.Click += btnSQLConfiguration_Click;
            this.btnWebConfiguration.Click += btnWebConfiguration_Click;
            this.cmbClient.SelectedIndexChanged += CmbClient_SelectedIndexChanged;
            this.btnLogout.Click += BtnLogout_Click;
            this.FormClosing += FrmConversion_FormClosing;
            this.Load += FrmConversion_Load;
            this.btnUpdateUsers.Click += btnUpdateUsers_Click;
            this.btnDecrypt.Click += btnDecrypt_Click;
            this.btnSqlRefresh.Click += btnSqlRefresh_Click;
            this.btnWebRefresh.Click += btnWebRefresh_Click;
            this.btnConvert.Click += btnConvert_Click;
        }


        private void FrmConversion_Load(object sender, EventArgs e)
        {
            //if (LoginUser().Result)
            //{                
            //    panelLoggedIn.Enabled = true;
            //    this.btnLogout.Enabled = true;
            //    btnLogin.Enabled = false;
            //}
            //else
            //{
            //    //this.Close();
            //    Environment.Exit(0);
            //}
            LoginUser();
            //if (loggedIn)
            //{
            //    panelLoggedIn.Enabled = true;
            //    this.btnLogout.Enabled = true;
            //    btnLogin.Enabled = false;
            //}
            //else
            //{
            //    Environment.Exit(0);
            //}
        }

        private void FrmConversion_FormClosing(object sender, FormClosingEventArgs e)
        {
            LogoutUser();
        }

        private async void BtnLogout_Click(object sender, EventArgs e)
        {
            LogoutUser();
        }

        private async void LogoutUser()
        {
            if (!loggedIn)
            {
                return;
            }
            //var accounts = await PublicClientApp.GetAccountsAsync();
            //if (accounts.Any())
            //{
            //    try
            //    {
            //        await PublicClientApp.RemoveAsync(accounts.FirstOrDefault());                   
            //    }
            //    catch (MsalException ex)
            //    {                    
            //    }
                panelLoggedIn.Enabled = false;
                this.btnLogin.Enabled = true;
                this.btnLogout.Enabled = false;
            //}
        }

      private void CmbClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbClient.SelectedItem != null)
            {
                var client = (Client)cmbClient.SelectedItem;
                ShowWebEnvironments(client);
            }            
        }

        private void TxtWebConfigPath_Validated(object sender, EventArgs e)
        {
            WebConfigLocation = txtWebConfigPath.Text;
            LoadWebEnvironments();
        }

        private void TxtSQLConfigPath_Validated(object sender, EventArgs e)
        {
            SQLConfigLocation = txtSQLConfigPath.Text;
            if (!string.IsNullOrWhiteSpace(SQLConfigLocation))
            {
                LoadSQLEnvironments();
            }
        }

        private String ConfigFileName { get; set; } = "TrioClientConfig.xml";
        private String SQLConfigLocation { get; set; } = "";
        private String WebConfigLocation { get; set; } = "";
        private SQLConfigInfo SQLServerInfo { get; set; } = new SQLConfigInfo();
        private SQLConfigInfo WebServerInfo { get; set; } = new SQLConfigInfo();
        private List<SQLDataEnvironment> SQLDataEnvironments { get; set; } = new List<SQLDataEnvironment>();
        private List<SQLDataEnvironment> WebDataEnvironments { get; set; } = new List<SQLDataEnvironment>();
        private List<Client> WebClients { get; set; } = new List<Client>();
        private cGlobalSettings GlobalConfigSettings { get; set; } = new cGlobalSettings();
        public void LoadSQLConfigFile()
        {
            if (String.IsNullOrEmpty(SQLConfigLocation))
            {
                throw new Exception("No path specified");
            }
            if (!System.IO.Directory.Exists(SQLConfigLocation))
            {
                throw new Exception("Invalid path");
            }
            if (!System.IO.File.Exists(SQLConfigLocation + "\\" + ConfigFileName))
            {
                throw new Exception("File doesn't exist");
            }
            //if (String.IsNullOrWhiteSpace(SQLRootName))
            //{
            var dirs = SQLConfigLocation.Split(System.IO.Path.DirectorySeparatorChar);
            var last = dirs.LastOrDefault();


            var temp = "";
            var doc = XDocument.Load(SQLConfigLocation + "\\" + ConfigFileName);
            var root = doc.Root;
            var serv = root.Element("Server");
            SQLServerInfo.ServerInstance = serv.Element("DataSource").Value;
            temp = serv.Element("UserName").Value;
            SQLServerInfo.UserName = SQLToWeb.SQL.CryptoUtility.DecryptByPassword(temp, strToUse, sToUse);
            temp = serv.Element("Password").Value;
            SQLServerInfo.Password = SQLToWeb.SQL.CryptoUtility.DecryptByPassword(temp, strToUse, sToUse);
            var globl = root.Element("Global");
            //SQLBackupLocation = globl.Element("BackupDirectory").Value;
            //SQLMasterLocation = globl.Element("MasterDirectory").Value;
            if (root.Elements("Envs").Any())
            {
                SQLDataEnvironments.Clear();
                var envs = root.Element("Envs");
                foreach (var env in envs.Elements())
                {
                    SQLDataEnvironments.Add(new SQLDataEnvironment()
                    {
                        Name = env.Element("Name").Value,
                        DataDirectory = env.Element("DataDirectory").Value
                    });
                }
            }

            ShowSQLEnvironments();
        }

        private void ShowSQLEnvironments()
        {
            cmbSQLEnvironment.Items.Clear();
            cmbSQLEnvironment.ValueMember = "Name";
            cmbSQLEnvironment.DisplayMember = "Name";
            foreach (var dataEnvironment in SQLDataEnvironments)
            {
                cmbSQLEnvironment.Items.Add(dataEnvironment);
            }

            if (SQLDataEnvironments.Any())
            {
                cmbSQLEnvironment.SelectedIndex = 0;
            }
        }
        public List<Client> LoadWebConfigFile()
        {
            if (String.IsNullOrEmpty(WebConfigLocation))
            {
                throw new Exception("No path specified");
            }
            if (!System.IO.Directory.Exists(WebConfigLocation))
            {
                throw new Exception("Invalid path");
            }
            if (!System.IO.File.Exists(WebConfigLocation + "\\" + ConfigFileName))
            {
                throw new Exception("File doesn't exist");
            }
            var clientList = new List<Client>();
            try
            {
                var dirs = WebConfigLocation.Split(System.IO.Path.DirectorySeparatorChar);
                var last = dirs.LastOrDefault();
                

                var temp = "";
                var doc = XDocument.Load(WebConfigLocation + "\\" + ConfigFileName);
                var root = doc.Element("Setup");

                var clients = root.Element("Clients");

                if (clients != null)
                {
                    foreach (var client in clients.Elements())
                    {
                        var webClient = new Client();
                        webClient.Name = client.Attribute("Name").Value;
                        if (string.IsNullOrWhiteSpace(webClient.Name))
                        {
                            webClient.Name = "Default";
                        }

                        var serv = client.Element("Server");
                        webClient.DataSource = serv.Element("DataSource").Value;
                        temp = serv.Element("UserName").Value;
                        webClient.UserName = SQLToWeb.SQL.CryptoUtility.DecryptByPassword(temp, strToUse, sToUse);
                        temp = serv.Element("Password").Value;
                        webClient.Password = SQLToWeb.SQL.CryptoUtility.DecryptByPassword(temp, strToUse, sToUse);
                        if (client.Elements("Envs").Any())
                        {
                            var envs = client.Element("Envs");
                            foreach (var env in envs.Elements())
                            {
                                var webEnv = new WebDataEnvironment()
                                {
                                    Name = env.Element("Name").Value,
                                    DataDirectory = env.Element("DataDirectory").Value
                                };
                                webClient.DataEnvironments.Add(webEnv);
                            }
                        }

                        clientList.Add(webClient);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return clientList;
        }

        private void ShowWebEnvironments(Client client)
        {
            cmbWebEnvironment.Items.Clear();
            cmbWebEnvironment.ValueMember = "Name";
            cmbWebEnvironment.DisplayMember = "Name";
            foreach (var dataEnvironment in client.DataEnvironments)
            {
                cmbWebEnvironment.Items.Add(dataEnvironment);
            }

            if (client.DataEnvironments.Any())
            {
                cmbWebEnvironment.SelectedIndex = 0;
            }
        }

        private string BrowsePath(string initialPath)
        {
            var fbd = new FolderBrowserDialog();
            fbd.Description = "Choose configuration location";
            fbd.ShowNewFolderButton = false;
            var thePath = "";
            if (initialPath.Trim() != string.Empty)
            {
                if (System.IO.Directory.Exists(initialPath))
                {
                    thePath = initialPath;
                    fbd.SelectedPath = thePath;
                }
            }
            if (string.IsNullOrWhiteSpace(thePath))
            {
                fbd.RootFolder = Environment.SpecialFolder.MyComputer;
            }
            var result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
               return fbd.SelectedPath;
            }

            return "";
        }

        private void btnSQLConfiguration_Click(object sender, EventArgs e)
        {
            BrowseSQLConfig();
        }

        private void btnWebConfiguration_Click(object sender, EventArgs e)
        {
            BrowseWebConfig();
        }

        private void BrowseSQLConfig()
        {
            var path = BrowsePath(txtSQLConfigPath.Text);
            if (!string.IsNullOrWhiteSpace(path))
            {
                txtSQLConfigPath.Text = path;
                SQLConfigLocation = path;
                LoadSQLEnvironments();
            }
        }

        private void BrowseWebConfig()
        {
            try
            {
                var path = BrowsePath(txtWebConfigPath.Text);
                if (!string.IsNullOrWhiteSpace(path))
                {
                    txtWebConfigPath.Text = path;
                    WebConfigLocation = path;
                    LoadWebEnvironments();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSqlRefresh_Click(object sender, EventArgs e)
        {
            LoadSQLEnvironments();
        }

        private void LoadSQLEnvironments()
        {
            SQLConfigLocation = txtSQLConfigPath.Text;
            LoadSQLConfigFile();
        }

        private void btnWebRefresh_Click(object sender, EventArgs e)
        {
            LoadWebEnvironments();
        }

        private void LoadWebEnvironments()
        {
            WebConfigLocation = txtWebConfigPath.Text;
            WebClients = LoadWebConfigFile();
            ShowClients();
        }

        private void ShowClients()
        {
            cmbClient.Items.Clear();
            foreach (var client in WebClients)
            {
                cmbClient.Items.Add(client);
            }

            if (WebClients.Any())
            {
                cmbClient.SelectedIndex = 0;
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            Convert();
        }

        private void Convert()
        {
            rtbOutput.Clear();
            
            if (cmbWebEnvironment.SelectedItem == null)
            {
                MessageBox.Show("You must choose a web environment first", "No Web Environment", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            var sqlEnvironment = (SQLDataEnvironment) cmbSQLEnvironment.SelectedItem;
            var webEnvironment = (WebDataEnvironment) cmbWebEnvironment.SelectedItem;
            var client = (Client)cmbClient.SelectedItem;
            var webConfig = new SQLConfigInfo()
            {
                Password = client.Password,
                ServerInstance = client.DataSource,
                UserName = client.UserName
            };
            GlobalConfigSettings = new cGlobalSettings()
            {
                DataSource = client.DataSource,
                DataEnvironment = webEnvironment.Name,
                Password = client.Password,
                UserName = client.UserName,
                GlobalDataDirectory = webEnvironment.DataDirectory
            };
            var convertor = new SQLToWebConvertor(sqlEnvironment, webEnvironment, SQLServerInfo, webConfig, GlobalConfigSettings, chkUpgradeSketches.Checked, client);
            convertor.MessageNotification += Convertor_MessageNotification;
            convertor.ConvertData();
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            SetupAlwaysEncrypted();
        }

        private void SetupAlwaysEncrypted()
        {
            var powershellScriptPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "setupencryption.ps1");
            if (!File.Exists(powershellScriptPath)) throw new FileNotFoundException($"The powershell script not found at '{powershellScriptPath}'");
            
            var powershellExePath = @"C:\windows\system32\windowspowershell\v1.0\powershell.exe";
            if (!File.Exists(powershellExePath)) throw new FileNotFoundException($"Powershell was not found at '{powershellExePath}'");

            // powershell -windowstyle hidden -command <PowerShell Command String>

            // first set the execution policy
            var process = new Process
            {
                StartInfo =
                {
                    UseShellExecute = true, 
                    FileName = powershellExePath, 
                    Arguments = "-windowstyle hidden -ExecutionPolicy Unrestricted"
                }
            };
            process.Start();
            
            // now start up the encryption script
            var client = (Client)cmbClient.SelectedItem;
            var webEnvironment = (WebDataEnvironment) cmbWebEnvironment.SelectedItem;
            process = new Process
            {
                StartInfo =
                {
                    UseShellExecute = true, 
                    FileName = powershellExePath, 
                    Arguments = $"\"&'{powershellScriptPath}' '{webEnvironment.Name}' '{client.DataSource}' '{client.UserName.Replace("\'","\'\'")}' '{client.Password.Replace("\'","\'\'")}'\" -NoExit"
                }
            };
            process.Start();
        }

        private void Convertor_MessageNotification(object sender, string e)
        {
            if (!String.IsNullOrWhiteSpace(e))
            {
                SetRTBText(e + "\n");
            }
        }

        delegate void SetRTBTextCallback(string text);
        private void SetRTBText(String text)
        {
            if (this.rtbOutput.InvokeRequired)
            {
                SetRTBTextCallback d = new SetRTBTextCallback(SetRTBText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.rtbOutput.AppendText(text);
                this.rtbOutput.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private void btnUpdateUsers_Click(object sender, EventArgs e)
        {
	        UpdateUsers();
        }

        private void UpdateUsers()
        {
	        rtbOutput.Clear();

	        if (cmbWebEnvironment.SelectedItem == null)
	        {
		        MessageBox.Show("You must choose a web environment first", "No Web Environment", MessageBoxButtons.OK,
			        MessageBoxIcon.Warning);
		        return;
	        }

	        var sqlEnvironment = (SQLDataEnvironment)cmbSQLEnvironment.SelectedItem;
	        var webEnvironment = (WebDataEnvironment)cmbWebEnvironment.SelectedItem;
	        var client = (Client)cmbClient.SelectedItem;
	        var webConfig = new SQLConfigInfo()
	        {
		        Password = client.Password,
		        ServerInstance = client.DataSource,
		        UserName = client.UserName
	        };
	        GlobalConfigSettings = new cGlobalSettings()
	        {
		        DataSource = client.DataSource,
		        DataEnvironment = webEnvironment.Name,
		        Password = client.Password,
		        UserName = client.UserName,
		        GlobalDataDirectory = webEnvironment.DataDirectory
	        };
	        var convertor = new SQLToWebConvertor(sqlEnvironment, webEnvironment, SQLServerInfo, webConfig, GlobalConfigSettings, chkUpgradeSketches.Checked, client);
	        convertor.MessageNotification += Convertor_MessageNotification;
	        convertor.UpdateUsers();
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            DecryptData();
        }

        private void DecryptData()
        {
            rtbOutput.Clear();
            if (cmbWebEnvironment.SelectedItem == null)
            {
                MessageBox.Show("You must choose a web environment first", "No Web Environment", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            var sqlEnvironment = (SQLDataEnvironment)cmbSQLEnvironment.SelectedItem;
            var webEnvironment = (WebDataEnvironment)cmbWebEnvironment.SelectedItem;
            var client = (Client)cmbClient.SelectedItem;
            var webConfig = new SQLConfigInfo()
            {
                Password = client.Password,
                ServerInstance = client.DataSource,
                UserName = client.UserName
            };
            GlobalConfigSettings = new cGlobalSettings()
            {
                DataSource = client.DataSource,
                DataEnvironment = webEnvironment.Name,
                Password = client.Password,
                UserName = client.UserName,
                GlobalDataDirectory = webEnvironment.DataDirectory
            };
            var convertor = new SQLToWebConvertor(sqlEnvironment, webEnvironment, SQLServerInfo, webConfig, GlobalConfigSettings, chkUpgradeSketches.Checked, client);
            convertor.MessageNotification += Convertor_MessageNotification;
            convertor.DecryptDatabases();
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            LoginUser();
            //if (loggedIn)
            //{
            //    panelLoggedIn.Enabled = true;
            //    this.btnLogout.Enabled = true;
            //    btnLogin.Enabled = false;
            //}
        }

        private async void LoginUser()
        {
            loggedIn = false;
           

            try
            {
                var password = "";
                var result = ShowInputDialog(ref password);
                if (result == DialogResult.OK)
                {
                    if (password == "ViciousCuttlefishB@by")
                    {
                        LoginSuccessful();
                        loggedIn = true;
                        return;
                    }
                }               
            }
            catch (Exception ex)
            {
                LoginFailed();
                return;// false;
            }

            LoginFailed();
            loggedIn = false;
            return;
        }
        //private async void LoginUser()
        //{
        //    loggedIn = false;
        //    AuthenticationResult authResult = null;
        //    var app = PublicClientApp;

        //    var accounts = await app.GetAccountsAsync();
        //    var firstAccount = accounts.FirstOrDefault();

        //    try
        //    {
        //        authResult = await app.AcquireTokenSilent(scopes, firstAccount)
        //            .ExecuteAsync();
        //    }
        //    catch (MsalUiRequiredException ex)
        //    {
        //        try
        //        {
                    
        //            System.Diagnostics.Debug.WriteLine($"MsalUiRequiredException: {ex.Message}");
        //            authResult = await app.AcquireTokenInteractive(scopes)
        //                .WithAccount(accounts.FirstOrDefault())
        //                .WithParentActivityOrWindow(this.Handle)//new WindowInteropHelper(this).Handle) // optional, used to center the browser on the window
        //                .WithPrompt(Prompt.SelectAccount)
        //                .ExecuteAsync();
        //            if (authResult != null)
        //            {
        //                var resultText = await GetHttpContentWithToken(graphAPIEndpoint, authResult.AccessToken);
        //                //loggedIn = true;
        //                //return true;
        //            }
        //        }
        //        catch (MsalException msalex)
        //        {
        //            LoginFailed();
        //            return;//false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoginFailed();
        //        return;// false;
        //    }

        //    if (authResult != null)
        //    {
        //        LoginSuccessful();
        //        loggedIn = true;
        //        return;// true;
        //    }
        //    else
        //    {
        //        LoginFailed();
        //        loggedIn = false;
        //        return;// false;
        //    }
        //}

        private void LoginFailed()
        {
            loggedIn = false;
            panelLoggedIn.Enabled = false;
            this.btnLogout.Enabled = false;
            btnLogin.Enabled = true;
        }

        private void LoginSuccessful()
        {
            loggedIn = true;
            panelLoggedIn.Enabled = true;
            this.btnLogout.Enabled = true;
            btnLogin.Enabled = false;
        }
        //private async void TestLogin()
        //{
        //    AuthenticationResult authResult = null;
        //    var app = PublicClientApp;

        //    var accounts = await app.GetAccountsAsync();
        //    var firstAccount = accounts.FirstOrDefault();

        //    try
        //    {
        //        authResult = await app.AcquireTokenSilent(scopes, firstAccount)
        //            .ExecuteAsync();
        //    }
        //    catch (MsalUiRequiredException ex)
        //    {
        //        // A MsalUiRequiredException happened on AcquireTokenSilent. 
        //        // This indicates you need to call AcquireTokenInteractive to acquire a token
        //        System.Diagnostics.Debug.WriteLine($"MsalUiRequiredException: {ex.Message}");

        //        try
        //        {
        //            authResult = await app.AcquireTokenInteractive(scopes)
        //                .WithAccount(accounts.FirstOrDefault())
        //                .WithParentActivityOrWindow(this.Handle)//new WindowInteropHelper(this).Handle) // optional, used to center the browser on the window
        //                .WithPrompt(Prompt.SelectAccount)
        //                .ExecuteAsync();
        //        }
        //        catch (MsalException msalex)
        //        {
        //            //ResultText.Text = $"Error Acquiring Token:{System.Environment.NewLine}{msalex}";
        //            MessageBox.Show($"Error Acquiring Token:{System.Environment.NewLine}{msalex}", "Error");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //ResultText.Text = $"Error Acquiring Token Silently:{System.Environment.NewLine}{ex}";
        //        MessageBox.Show($"Error Acquiring Token Silently:{System.Environment.NewLine}{ex}", "Error");
        //        return;
        //    }

        //    if (authResult != null)
        //    {
        //        var resultText = await GetHttpContentWithToken(graphAPIEndpoint, authResult.AccessToken);
        //        MessageBox.Show(resultText, "result text");
        //        DisplayBasicTokenInfo(authResult);
        //        panelLoggedIn.Enabled = true;
        //        this.btnLogout.Visible = true;
        //        btnLogin.Visible = false;
        //    }

        //}
        //private void DisplayBasicTokenInfo(AuthenticationResult authResult)
        //{
        //    var tokenText = "";
        //    if (authResult != null)
        //    {
        //        tokenText += $"Username: {authResult.Account.Username}" + Environment.NewLine;
        //        tokenText += $"Token Expires: {authResult.ExpiresOn.ToLocalTime()}" + Environment.NewLine;
        //    }

        //    MessageBox.Show(tokenText, "token text");
        //}

        //public async Task<string> GetHttpContentWithToken(string url, string token)
        //{
        //    var httpClient = new System.Net.Http.HttpClient();
        //    System.Net.Http.HttpResponseMessage response;
        //    try
        //    {
        //        var request = new System.Net.Http.HttpRequestMessage(System.Net.Http.HttpMethod.Get, url);
        //        //Add the token in Authorization header
        //        request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
        //        response = await httpClient.SendAsync(request);
        //        var content = await response.Content.ReadAsStringAsync();
        //        return content;
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.ToString();
        //    }
        //}

        private static DialogResult ShowInputDialog(ref string input)
        {
            System.Drawing.Size size = new System.Drawing.Size(300, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = "Password";

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            textBox.PasswordChar = '*';
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        private void btnUpgradeReceipts_Click(object sender, EventArgs e)
        {
            rtbOutput.Clear();

            if (cmbWebEnvironment.SelectedItem == null)
            {
                MessageBox.Show("You must choose a web environment first", "No Web Environment", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            var sqlEnvironment = (SQLDataEnvironment)cmbSQLEnvironment.SelectedItem;
            var webEnvironment = (WebDataEnvironment)cmbWebEnvironment.SelectedItem;
            var client = (Client)cmbClient.SelectedItem;
            var webConfig = new SQLConfigInfo()
            {
                Password = client.Password,
                ServerInstance = client.DataSource,
                UserName = client.UserName
            };
            GlobalConfigSettings = new cGlobalSettings()
            {
                DataSource = client.DataSource,
                DataEnvironment = webEnvironment.Name,
                Password = client.Password,
                UserName = client.UserName,
                GlobalDataDirectory = webEnvironment.DataDirectory
            };
            var convertor = new SQLToWebConvertor(sqlEnvironment, webEnvironment, SQLServerInfo, webConfig, GlobalConfigSettings, chkUpgradeSketches.Checked, client);
            convertor.MessageNotification += Convertor_MessageNotification;
            convertor.UpgradeReceipts();
        }

        private void btnDecrypt_Click_1(object sender, EventArgs e)
        {

        }
    }

    public class DirectoryItem
    {
        public String Name { get; set; } = "";
        public String Path { get; set; } = "";
    }

    
}
