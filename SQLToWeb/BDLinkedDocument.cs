﻿namespace SQLToWeb
{
    public class BDLinkedDocument
    {
        public int Id { get; set; } = 0;
        public string FileName { get; set; } = "";
        public string Description { get; set; } = "";
        public int DocReferenceId { get; set; } = 0;
        public string DocReferenceType { get; set; } = "";
    }
}