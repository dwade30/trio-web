﻿using System;

namespace SQLToWeb
{
    public class PictureRecord
    {
        public int Id { get; set; } = 0;
        public int Account { get; set; } = 0;
        public int Card { get; set; } = 0;
        public int PicNum { get; set; } = 0;
        public string FileName { get; set; } = "";
        public string Description { get; set; } = "";
        public Guid DocumentIdentifier { get; set; } = Guid.NewGuid();
        public string CardId { get; set; } = Guid.Empty.ToString();
    }
}