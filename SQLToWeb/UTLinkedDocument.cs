﻿namespace SQLToWeb
{
    public class UTLinkedDocument
    {
        public int Id { get; set; } = 0;
        public string FileName { get; set; } = "";
        public string Description { get; set; } = "";
        public int AccountNumber { get; set; } = 0;
    }
}