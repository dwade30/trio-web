﻿namespace SQLToWeb
{
    public class LinkedDocument
    {
        public int Id { get; set; } = 0;
        public string FileName { get; set; } = "";
        public string Description { get; set; } = "";
        public int DocReferenceId { get; set; } = 0;
        public int DocReferenceType { get; set; } = 0;
    }
}