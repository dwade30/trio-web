﻿using System;

namespace SQLToWeb
{
    public class CentralDocumentDBUtil
    {
        private SQLConfigInfo configurationInfo;
        private string dataEnvironment = "";
        private DBHelper dbHelper = new DBHelper();
        public CentralDocumentDBUtil(string dataEnvironment, SQLConfigInfo config)
        {
            this.dataEnvironment = dataEnvironment;
            configurationInfo = config;
        }
        public void CheckSchema()
        {
            CheckDocuments();
            CheckSketches();
        }

        private void CheckDocuments()
        {
            
            if (!dbHelper.TableExists("Documents","TRIO_" + dataEnvironment + "_Live_CentralDocuments", configurationInfo))
            {
                AddDocumentsTable("TRIO_" + dataEnvironment + "_Live_CentralDocuments");
            }

            CheckDocumentFields("TRIO_" + dataEnvironment + "_Live_CentralDocuments");
            AddDocumentsStoredProcedures("TRIO_" + dataEnvironment + "_Live_CentralDocuments");
        }

        private void AddDocumentsTable(string databaseName)
        {
            string strSQL = "";
           
            if (!dbHelper.TableExists("Documents", databaseName,configurationInfo))
            {
                strSQL = dbHelper.GetTableCreationHeaderSQL("Documents");
                strSQL += "[MediaType] [nvarchar] (50) NULL,";
                strSQL += "[ItemData] [varbinary] (Max) NULL,";
                strSQL += "[ItemName] [nvarchar] (255) NULL,";
                strSQL += "[ItemDescription] [nvarchar] (255) NULL,";
                strSQL += "[DataGroup] [nvarchar] (255) NULL, ";
                strSQL += "[ReferenceType] [nvarchar] (255) NULL,";
                strSQL += "[ReferenceId] [int] NULL,";
                strSQL += "[AltReference] [nvarchar] (50) NULL,";
                strSQL += "[DocumentIdentifier] [UniqueIdentifier] Not Null ";
                strSQL += dbHelper.GetTablePKSql("Documents");
                dbHelper.Execute(strSQL,databaseName,configurationInfo);
            }
        }

        private void CheckDocumentFields(string databaseName)
        {
            if (!dbHelper.FieldExists("DocumentIdentifier", "Documents", databaseName,configurationInfo))
            {
                AddDocumentIdentifierToDocuments(databaseName);
            }

            if (!dbHelper.FieldExists("DateCreated", "Documents", databaseName, configurationInfo))
            {
                AddDateCreatedToDocuments(databaseName);
            }
        }

        private void AddDocumentIdentifierToDocuments(string databaseName)
        {
            dbHelper.Execute("Alter Table Documents Add DocumentIdentifier UniqueIdentifier Not Null Default newid()", databaseName,configurationInfo);
            dbHelper.Execute("Update Documents set DocumentIdentifier = NEWID() where DocumentIdentifier is null",
                databaseName, configurationInfo);                
        }

        private void AddDateCreatedToDocuments(string databaseName)
        {
            dbHelper.Execute("Alter Table Documents Add DateCreated DateTime not null Default GetDate()", databaseName,
                configurationInfo);
        }

        private bool AddDocumentsStoredProcedures(string databaseName)
        {
            
            if (AddInsertDocumentsProcedure(databaseName))
            {
                if (AddUpdateDocumentsProcedure(databaseName))
                {
                    if (AddUpdateDocumentInfoProcedure(databaseName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool AddInsertDocumentsProcedure(string databaseName)
        {           
                string strSQL = "";
                if (dbHelper.StoredProcedureExists("Insert_Document", databaseName, configurationInfo))
                {
                    dbHelper.Execute("Drop procedure [dbo].[Insert_Document]",databaseName,configurationInfo);
                }

                strSQL = "Create Procedure [dbo].[Insert_Document] ";
                strSQL += " @MediaType nvarchar(50) = '', ";
                strSQL += "@ItemData varbinary(Max) , ";
                strSQL += "@ItemName nvarchar(50) = '', ";
                strSQL += "@ItemDescription nvarchar(255) = '', ";
                strSQL += "@ReferenceType nvarchar(50) = '', ";
                strSQL += "@ReferenceId int = 0, ";
                strSQL += "@AltReference nvarchar(50) = '', ";
                strSQL += "@DataGroup nvarchar(50) = '', ";
                strSQL += "@DocumentIdentifier UniqueIdentifier  ,";
                strSQL += "@DateCreated DateTime ,";
                strSQL += "@ID int OUTPUT ";
                strSQL += "AS BEGIN SET NOCOUNT ON; ";
                strSQL += "Insert into [Documents] ([MediaType],[ItemData],[ItemName],[ItemDescription],[ReferenceType],[ReferenceId],[AltReference],[ReferenceGroup],[DocumentIdentifier],[DateCreated]) ";
                strSQL += "Values ";
                strSQL += "(@MediaType,@ItemData,@ItemName,@ItemDescription,@ReferenceType,@ReferenceId,@AltReference,@DataGroup,@DocumentIdentifier,@DateCreated) ";
                strSQL += "set @ID = SCOPE_IDENTITY() ";
                strSQL += "END";
                dbHelper.Execute(strSQL,databaseName,configurationInfo);
                return true;



        }

        private bool AddUpdateDocumentsProcedure(string databaseName)
        {
            string strSQL = "";
            if (dbHelper.StoredProcedureExists("Update_Document", databaseName, configurationInfo))
            {
                dbHelper.Execute("Drop procedure [dbo].[Update_Document]",databaseName,configurationInfo);
            }                

            strSQL = "Create Procedure [dbo].[Update_Document] ";
            strSQL += "@ID int ,";
            strSQL += "@MediaType nvarchar(50) = '', ";
            strSQL += "@ItemData varbinary(Max) , ";
            strSQL += "@ItemName nvarchar(50) = '', ";
            strSQL += "@ItemDescription nvarchar(255) = '', ";
            strSQL += "@ReferenceType nvarchar(50) = '', ";
            strSQL += "@ReferenceId int = 0, ";
            strSQL += "@AltReference nvarchar(50) = '', ";
            strSQL += "@DataGroup nvarchar(50) = '' ";
            strSQL += "as BEGIN SET NOCOUNT ON; ";
            strSQL += "update [Documents] set ";
            strSQL += "AltReference = @AltReference, ";
            strSQL += "MediaType = @MediaType, ";
            strSQL += "ItemData = @ItemData, ";
            strSQL += "ItemName = @ItemName, ";
            strSQL += "ItemDescription = @ItemDescription, ";
            strSQL += "ReferenceType = @ReferenceType, ";
            strSQL += "ReferenceId = @ReferenceId, ";
            strSQL += "ReferenceGroup = @DataGroup ";
            strSQL += "where ";
            strSQL += "ID = @ID ";
            strSQL += "END";
            dbHelper.Execute(strSQL,databaseName,configurationInfo);                
            return true;
        }

        private bool AddUpdateDocumentInfoProcedure(string databaseName)
        {
            // On Error GoTo ErrorHandler
            string strSQL = "";
            if (dbHelper.StoredProcedureExists("Update_DocumentInfo", databaseName, configurationInfo))
            {
                dbHelper.Execute("Drop procedure [dbo].[Update_DocumentInfo]",databaseName,configurationInfo);
            }
                            

            strSQL = "Create Procedure [dbo].[Update_DocumentInfo] ";
            strSQL += "@ID int ,";
            strSQL += "@MediaType nvarchar(50) = '', ";
            strSQL += "@ItemName nvarchar(50) = '', ";
            strSQL += "@ItemDescription nvarchar(255) = '', ";
            strSQL += "@ReferenceType nvarchar(50) = '', ";
            strSQL += "@ReferenceId int = 0, ";
            strSQL += "@AltReference nvarchar(50) = '', ";
            strSQL += "@DataGroup nvarchar(50) = '' ";
            strSQL += "as BEGIN SET NOCOUNT ON; ";
            strSQL += "update [Documents] set ";
            strSQL += "AltReference = @AltReference, ";
            strSQL += "MediaType = @MediaType, ";
            strSQL += "ItemName = @ItemName, ";
            strSQL += "ItemDescription = @ItemDescription, ";
            strSQL += "ReferenceType = @ReferenceType, ";
            strSQL += "ReferenceId = @ReferenceId, ";
            strSQL += "ReferenceGroup = @DataGroup ";
            strSQL += "where ";
            strSQL += "ID = @ID ";
            strSQL += "END";
            dbHelper.Execute(strSQL,databaseName,configurationInfo);                
            return true;            
        }

        private void CheckSketches()
        {
            if (!dbHelper.TableExists("Sketches", "TRIO_" + dataEnvironment + "_Live_CentralDocuments", configurationInfo))
            {
                AddSketchesTable("TRIO_" + dataEnvironment + "_Live_CentralDocuments");
            }
        }

        private void AddSketchesTable(string databaseName)
        {
            if (!dbHelper.TableExists("Sketches", databaseName, configurationInfo))
            {

                var strSQL = dbHelper.GetTableCreationHeaderSQL("Sketches");
                strSQL += "[MediaType] [nvarchar] (50) NULL,";
                strSQL += "[SketchData] [varbinary] (Max) NULL,";
                strSQL += "[CardIdentifier] [UniqueIdentifier] Not Null,";
                strSQL += "[SketchIdentifier] [UniqueIdentifier] Not Null,";
                strSQL += "[SketchImage] [varbinary] (Max) NULL,";
                strSQL += "[SequenceNumber] [int] NULL,";
                strSQL += "[Calculations] [nvarchar] (1024) NULL,";
                strSQL += "[DateUpdated] [DateTime] Not Null Default GetDate()";
                strSQL += dbHelper.GetTablePKSql("Sketches");
                dbHelper.Execute(strSQL,databaseName,configurationInfo);
            }
        }
    }
}