﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace SQLToWeb
{
    public static class FileUtil
    {
        public static byte[] ResizeImageAsJpeg(byte[] imageData, int maxWidthInPixels, int maxHeightInPixels)
        {
            using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
            {
                using (Image img = Image.FromStream(ms))
                {
                    int newWidth = img.Width;
                    int newHeight = img.Height;
                    if (img.Width > maxWidthInPixels || img.Height > maxHeightInPixels)
                    {
                        var ratio = Convert.ToDouble(img.Width) / Convert.ToDouble(img.Height);
                        newWidth = maxWidthInPixels;
                        newHeight = maxHeightInPixels;

                        if (maxWidthInPixels / ratio > maxHeightInPixels)
                        {
                            newWidth = (int)(maxHeightInPixels * ratio);
                        }
                        else
                        {
                            newHeight = (int)(maxWidthInPixels / ratio);
                        }
                    }

                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    ImageCodecInfo imageCodecInfo = ImageCodecInfo.GetImageEncoders().FirstOrDefault(e => e.MimeType == "image/jpeg");
                    Encoder imageEncoder = Encoder.Quality;
                    EncoderParameter encoderParameter = new EncoderParameter(imageEncoder, 90L);
                    encoderParameters.Param[0] = encoderParameter;
                    using (Bitmap b = new Bitmap(img, new Size(newWidth, newHeight)))
                    {
                        using (MemoryStream destinationStream = new MemoryStream())
                        {
                            b.Save(destinationStream, imageCodecInfo, encoderParameters);
                            return destinationStream.ToArray();
                        }
                    }

                }
            }
        }

    }
}