﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using Autofac;
using MediatR.Extensions.Autofac.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using SharedApplication;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Telemetry;
using SharedDataAccess;

namespace SQLToWeb
{
    public class SQLToWebConvertor
    {
        //private winskt.WinSkt appWinSketch;
        private SQLDataEnvironment sqlEnvironment;
        private WebDataEnvironment webEnvironment;
        private SQLConfigInfo sqlConfigInfo;
        private SQLConfigInfo webConfigInfo;
        public event EventHandler<string> MessageNotification;
        private DBHelper dbHelper = new DBHelper();
        private Autofac.IContainer diContainer;
        private CommandDispatcher commandDispatcher;
        private cGlobalSettings globalSettings;
        private Client selectedClient;

        private bool upgradeSketchImages = false;
        public SQLToWebConvertor(SQLDataEnvironment sqlEnvironment, WebDataEnvironment webEnvironment, SQLConfigInfo sqlConfiguration, SQLConfigInfo webConfiguration,cGlobalSettings globalSettings,bool upgradeSketchImages, Client selectedClient)
        {
            this.globalSettings = globalSettings;
            this.sqlEnvironment = sqlEnvironment;
            this.webEnvironment = webEnvironment;
            this.sqlConfigInfo = sqlConfiguration;
            this.webConfigInfo = webConfiguration;
            this.upgradeSketchImages = upgradeSketchImages;
            this.selectedClient = selectedClient;
        }

        public void UpgradeReceipts()
        {
            try
            {
                if (HasDatabase(webEnvironment.Name, "CashReceipts", webConfigInfo))
                {
                    var crUtil = new CashReceiptsDBUtil(webEnvironment.Name, webConfigInfo);
                    SendMessage("Checking cash receipts schema");
                    crUtil.CheckSchema();
                }
                else
                {
                    SendMessage("No Cash Receipts Database Found");
                    return;
                }

                WireUpDependencies(webConfigInfo, webEnvironment.Name);
                using (var scope = diContainer.BeginLifetimeScope())
                {
                    commandDispatcher = scope.Resolve<CommandDispatcher>();
                    var factory = scope.Resolve<ITrioContextFactory>();

                    FillCashReceiptsIdentifiers(webEnvironment.Name, factory, webConfigInfo);

                    SendMessage("Receipt Upgrade finished");
                }
            }
            catch (Exception e)
            {
                SendMessage(e.Message);
            }
        }

        public void UpdateUsers()
        {
            try
            {
                var cSetUtil = new ClientSettingsDBUtil(webConfigInfo);
                SendMessage("Checking client settings schema");
                cSetUtil.CheckSchema();

                WireUpDependencies(webConfigInfo, webEnvironment.Name);
                using (var scope = diContainer.BeginLifetimeScope())
                {
                    commandDispatcher = scope.Resolve<CommandDispatcher>();
                    var factory = scope.Resolve<ITrioContextFactory>();

                    ConvertInfoToClientSettings(selectedClient, factory, webConfigInfo);
                    
                    SendMessage("User Update finished");
                }
            }
            catch (Exception e)
            {
                SendMessage(e.Message);
            }
        }

        public void DecryptDatabases()
        {
            SendMessage("Decrypting databases");
            WireUpDependencies(webConfigInfo, webEnvironment.Name);
            using (var scope = diContainer.BeginLifetimeScope())
            {
                commandDispatcher = scope.Resolve<CommandDispatcher>();
                var muniName = "";
                using (var connection = dbHelper.GetConnectionFromConfig(webConfigInfo, "TRIO_" + webEnvironment.Name + "_Live_SystemSettings"))
                {
                    connection.Open();
                    
                    using (var adapter = new SqlDataAdapter("select * from GlobalVariables", connection))
                    {
                        DataTable dataTable = new DataTable("ConvertedTables");
                        adapter.Fill(dataTable);
                        DataRow row = dataTable.Rows[0];
                        muniName = row["MuniName"].ToString();                            
                    }
                    
                }
                DecryptPayrollDatabase(webEnvironment.Name, webConfigInfo,muniName);
                DecryptBudgetaryDatabases(webEnvironment.Name,webConfigInfo,muniName);
                DecryptClerkDatabase(webEnvironment.Name, webConfigInfo, muniName);
                DecryptUtilityDatabase(webEnvironment.Name, webConfigInfo, muniName);
                DecryptMotorVehicleDatabase(webEnvironment.Name, webConfigInfo, muniName);
            }
            SendMessage("Finished decrypting databases");
        }

        private (string InitializationVector, string EncryptionKey) GetEncryptionVectors(string muniName)
        {
            var iv = "010003020504070609080A0B0C0D0E0F";
            var keyHex = "0123456789" + muniName + "101112131415161718191A1B1C1D1E1F";
            keyHex = keyHex.Left(32);
            return (InitializationVector: iv, EncryptionKey: keyHex);
        }

        private void DecryptBudgetaryDatabase(string dataEnvironment, SQLConfigInfo config, string muniName,string archiveType)
        {
            if (HasDatabase(dataEnvironment, "Budgetary",archiveType, config))
            {
                SendMessage("Decrypting budgetary " + archiveType + " database");

                var encryptionVectors = GetEncryptionVectors(muniName);
                var decryptor = new ChilkatCryptoUtility(encryptionVectors.InitializationVector,
                    encryptionVectors.EncryptionKey);
                var convertedTables = new List<string>();
                using (var connection = dbHelper.GetConnectionFromConfig(config, "TRIO_" + dataEnvironment + "_" +  archiveType + "_Budgetary"))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from ConvertedTables", connection))
                    {
                        DataTable dataTable = new DataTable("ConvertedTables");
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            convertedTables.Add(row["TableName"].ToString());
                        }
                    }
                }
                foreach (var tableName in convertedTables)
                {
                    switch (tableName.ToLower())
                    {
                        case "banks":
                            SendMessage("Decrypting budgetary banks");
                            DecryptBudgetaryBanks(dataEnvironment, archiveType, config, decryptor);
                            break;
                        case "vendormaster":
                            DecryptBudgetaryVendorMaster(dataEnvironment, archiveType, config, decryptor);
                            SendMessage("Decrypting budgetary vendor master");
                            break;
                    }
                }

                SendMessage("Finished decrypting budgetary " + archiveType + " database");
            }
        }


        private void DecryptBudgetaryBanks(string dataEnvironment,string archiveGroup, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var tableName = "Banks";
            var budgetaryDB = MakeDBName(dataEnvironment, "Budgetary",archiveGroup);
            using (var connection = dbHelper.GetConnectionFromConfig(config, budgetaryDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["BankAccountNumber"].ToString());
                        var saveCommand = new SqlCommand("Update "+ tableName + " set BankAccountNumber = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", budgetaryDB, config);
        }

        private void DecryptBudgetaryVendorMaster(string dataEnvironment, string archiveGroup, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var tableName = "VendorMaster";
            var budgetaryDB = MakeDBName(dataEnvironment, "Budgetary", archiveGroup);
            using (var connection = dbHelper.GetConnectionFromConfig(config, budgetaryDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var taxNumber = decryptor.Decrypt(row["TaxNumber"].ToString());
                        var account = decryptor.Decrypt(row["BankAccountNumber"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set TaxNumber = '" + taxNumber + "',BankAccountNumber = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", budgetaryDB, config);
        }
        private void DecryptBudgetaryDatabases(string dataEnvironment, SQLConfigInfo config,string muniName)
        {
            var budgetaryDatabases = new List<string>();
            using (var connection = dbHelper.GetConnectionFromConfig(config, "TRIO_" + dataEnvironment + "_Live_SystemSettings"))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select name,database_id from sys.databases where name like 'TRIO_" + dataEnvironment + "_%_Budgetary'", connection))
                {
                    DataTable dataTable = new DataTable("Archives");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        budgetaryDatabases.Add(row["name"].ToString());
                    }
                }
            }

            if (budgetaryDatabases.Any())
            {
                SendMessage("Decrypting budgetary databases");
                foreach (var database in budgetaryDatabases)
                {
                    var archiveType = database.Replace("TRIO_" + dataEnvironment + "_", "").Replace("_Budgetary", "");
                    DecryptBudgetaryDatabase(dataEnvironment, config, muniName, archiveType);
                }
                SendMessage("Finished decrypting budgetary databases");
            }
            
        }
        private void DecryptClerkDatabase(string dataEnvironment, SQLConfigInfo config, string muniName)
        {
            if (HasDatabase(dataEnvironment,"Clerk", config))
            {
                SendMessage("Decrypting clerk database");
                var encryptionVectors = GetEncryptionVectors(muniName);
                var decryptor = new ChilkatCryptoUtility(encryptionVectors.InitializationVector,
                    encryptionVectors.EncryptionKey);
                var convertedTables = new List<string>();
                var clerkDB = MakeLiveDBName(dataEnvironment, "Clerk");
                using (var connection = dbHelper.GetConnectionFromConfig(config, clerkDB))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from ConvertedTables", connection))
                    {
                        DataTable dataTable = new DataTable("ConvertedTables");
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            convertedTables.Add(row["TableName"].ToString());
                        }
                    }
                }

                
                foreach (var tableName in convertedTables)
                {
                    switch (tableName.ToLower())
                    {
                        case "births":
                            SendMessage("Decrypting births");
                            DecryptBirths(dataEnvironment, config, decryptor);
                            break;
                        case "birthsdelayed":
                            SendMessage("Decrypting delayed births");
                            DecryptDelayedBirths(dataEnvironment, config, decryptor);
                            break;
                        case "birthsforeign":
                            SendMessage("Decrypting foreign births");
                            DecryptForeignBirths(dataEnvironment, config, decryptor);
                            break;
                        case "deaths":
                            SendMessage("Decrypting deaths");
                            DecryptDeaths(dataEnvironment, config, decryptor);
                            break;
                        case "marriages":
                            SendMessage("Decrypting marriages");
                            DecryptMarriages(dataEnvironment, config, decryptor);
                            break;
                    }
                }

                SendMessage("Finished decrypting clerk database");
            }
        }

        private void DecryptBirths(string dataEnvironment,  SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var tableName = "Births";
            var clerkDB = MakeLiveDBName(dataEnvironment, "Clerk");
            if (dbHelper.GetDataType("Births", "ActualDate", clerkDB, config).ToLower() != "datetime")
            {
                using (var connection = dbHelper.GetConnectionFromConfig(config, clerkDB))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                    {
                        DataTable dataTable = new DataTable(tableName);
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var actualDate = decryptor.Decrypt(row["ActualDate"].ToString());
                            var mothersDOB = decryptor.Decrypt(row["Mothersdob"].ToString());
                            var fathersDOB = decryptor.Decrypt(row["FathersDOB"].ToString());
                            var dateOfBirth = decryptor.Decrypt(row["dateofbirth"].ToString());
                            var saveCommand =
                                new SqlCommand(
                                    "Update " + tableName + " set dateofbirth = '" + dateOfBirth + "', ActualDate = '" + actualDate + "', Mothersdob = '" + mothersDOB + "', Fathersdob = '" + fathersDOB +"' where id = " +
                                    Convert.ToInt32(row["Id"]), connection);
                            saveCommand.ExecuteNonQuery();
                        }
                    }
                }
                dbHelper.Execute("Alter Table Births Alter Column ActualDate datetime null", clerkDB, config);
                dbHelper.Execute("update " + tableName + " set ActualDate = '12/30/1899' where ActualDate = '01/01/1900'",clerkDB,config);
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", clerkDB, config);
        }

        private void DecryptDelayedBirths(string dataEnvironment,  SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var tableName = "BirthsDelayed";
            var clerkDB = MakeLiveDBName(dataEnvironment, "Clerk");
            if (dbHelper.GetDataType("BirthsDelayed", "ActualDate", clerkDB, config).ToLower() != "datetime")
            {
                using (var connection = dbHelper.GetConnectionFromConfig(config, clerkDB))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                    {
                        DataTable dataTable = new DataTable(tableName);
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var actualDate = decryptor.Decrypt(row["ActualDate"].ToString());
                            var dob1 = decryptor.Decrypt(row["Dob1"].ToString());
                            var dob2 = decryptor.Decrypt(row["Dob2"].ToString());
                            var dob3 = decryptor.Decrypt(row["Dob3"].ToString());
                            var desc = decryptor.Decrypt(row["ChildDOBDescription"].ToString());
                            var saveCommand =
                                new SqlCommand(
                                    "Update " + tableName + " set ActualDate = '" + actualDate + "', Dob1 = '" +
                                    dob1 + "', Dob2 = '" + dob2 + "', Dob3 = '" + dob3 +
                                    "', ChildDobDescription = '" + desc + "' where id = " +
                                    Convert.ToInt32(row["Id"]), connection);
                            saveCommand.ExecuteNonQuery();
                        }
                    }
                }
                dbHelper.Execute("Alter Table " + tableName + " Alter Column ActualDate datetime null", clerkDB, config);
                dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob1 datetime null", clerkDB, config);
                dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob2 datetime null", clerkDB, config);
                dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob3 datetime null", clerkDB, config);
                dbHelper.Execute("update " + tableName + " set Dob1 = '12/30/1899' where Dob1 = '01/01/1900'",clerkDB,config);
                dbHelper.Execute("update " + tableName + " set Dob2 = '12/30/1899' where Dob2 = '01/01/1900'",clerkDB,config);
                dbHelper.Execute("update " + tableName + " set Dob3 = '12/30/1899' where Dob3 = '01/01/1900'",clerkDB,config);
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", clerkDB, config);
        }

        private void DecryptForeignBirths(string dataEnvironment,  SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var tableName = "BirthsForeign";
            var clerkDB = MakeLiveDBName(dataEnvironment, "Clerk");
            if (dbHelper.GetDataType("BirthsForeign", "ActualDate", clerkDB, config).ToLower() != "datetime")
            {
                using (var connection = dbHelper.GetConnectionFromConfig(config, clerkDB))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                    {
                        DataTable dataTable = new DataTable(tableName);
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var actualDate = decryptor.Decrypt(row["ActualDate"].ToString());
                            var childDOB = decryptor.Decrypt(row["Childdob"].ToString());
                            var childDOBDescription = decryptor.Decrypt(row["childDOBdescription"].ToString());
                            var mothersDOB = decryptor.Decrypt(row["Motherdob"].ToString());
                            var fathersDOB = decryptor.Decrypt(row["FatherDOB"].ToString());

                            var saveCommand =
                                new SqlCommand(
                                    "Update " + tableName + " set ActualDate = '" + actualDate + "',Motherdob = '" + mothersDOB + "',FatherDOB = '" + fathersDOB +  "', ChildDob = '" + childDOB + "', ChildDobDescription = '" + childDOBDescription + "' where id = " +
                                    Convert.ToInt32(row["Id"]), connection);
                            saveCommand.ExecuteNonQuery();
                        }
                    }
                }
                dbHelper.Execute("Alter Table BirthsForeign Alter Column ActualDate datetime null", clerkDB, config);  
                dbHelper.Execute("update BirthsForeign set ActualDate = '12/30/1899' where ActualDate = '01/01/1900'",clerkDB,config);
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", clerkDB, config);
        }
        private void DecryptDeaths(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var tableName = "Deaths";
            var clerkDB = MakeLiveDBName(dataEnvironment, "Clerk");
            using (var connection = dbHelper.GetConnectionFromConfig(config, clerkDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var actualDate = decryptor.Decrypt(row["DateOfBirth"].ToString());
                        var ssn = decryptor.Decrypt(row["SocialSecurityNumber"].ToString());                       
                        var saveCommand =
                            new SqlCommand(
                                "Update " + tableName + " set DateOfBirth = '" + actualDate + "', SocialSecurityNumber = '" + ssn + "' where id = " +
                                Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", clerkDB, config);
        }

        private void DecryptMarriages(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var tableName = "Marriages";
            var clerkDB = MakeLiveDBName(dataEnvironment, "Clerk");
            using (var connection = dbHelper.GetConnectionFromConfig(config, clerkDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var bridessn = decryptor.Decrypt(row["bridessn"].ToString());
                        var groomssn = decryptor.Decrypt(row["groomSSN"].ToString());
                        var bridesDob = decryptor.Decrypt(row["Bridesdob"].ToString());
                        var groomsDob = decryptor.Decrypt(row["Groomsdob"].ToString());
                        var saveCommand =
                            new SqlCommand(
                                "Update " + tableName + " set bridessn = '" + bridessn + "', groomssn = '" + groomssn + "', Bridesdob = '" + bridesDob + "', groomsdob = '" + groomsDob + "' where id = " +
                                Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", clerkDB, config);
        }
        private void DecryptPayrollDatabase(string dataEnvironment, SQLConfigInfo config,string muniName)
        {
            if (HasPayrollDatabase(dataEnvironment, config))
            {
                SendMessage("Decrypting payroll database");
                var encryptionVectors = GetEncryptionVectors(muniName);
                var decryptor = new ChilkatCryptoUtility(encryptionVectors.InitializationVector,
                    encryptionVectors.EncryptionKey);
                var convertedTables = new List<string>();
                using (var connection = dbHelper.GetConnectionFromConfig(config, "TRIO_" + dataEnvironment + "_Live_Payroll"))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from ConvertedTables", connection))
                    {
                        DataTable dataTable = new DataTable("ConvertedTables");
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            convertedTables.Add(row["TableName"].ToString());                             
                        }
                    }
                }

                var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
                dbHelper.Execute("delete from tblMSRSTempDetailReport", payrollDB,config);
                if (dbHelper.GetDataType("tblMSRSTempDetailReport", "DateOfBirth",
                        payrollDB, config).ToLower() != "datetime")
                {
                    dbHelper.Execute("Alter table tblMSRSTempDetailReport alter column DateOfBirth DateTime null",payrollDB,config);
                }
                foreach (var tableName in convertedTables)
                {
                    switch (tableName.ToLower())
                    {
                        case "tblemployeemaster":
                            SendMessage("Decrypting employee master");
                            DecryptEmployeeMaster(dataEnvironment,config,decryptor);
                            break;
                        case "acaemployeedependents":
                            SendMessage("Decrypting aca employee dependents");
                            DecryptPayrollACAEmployeeDependents(dataEnvironment, config, decryptor);
                            break;
                        case "tblachinformation":
                            SendMessage("Decrypting payroll ach information");
                            DecryptPayrollACHInformation(dataEnvironment, config, decryptor);
                            break;
                        case "tblbanks":
                            SendMessage("Decrypting payroll banks");
                            DecryptPayrollBanks(dataEnvironment, config, decryptor);
                            break;
                        case "tblcheckdetail":
                            SendMessage("Decrypting payroll check detail");
                            DecryptPayrollCheckDetail(dataEnvironment, config, decryptor);
                            break;
                        case "tblcheckreturn":
                            SendMessage("Decrypting payroll check return");
                            DecryptPayrollCheckReturn(dataEnvironment, config, decryptor);
                            break;
                        case "tbldirectdeposit":
                            SendMessage("Decrypting payroll direct deposit");
                            DecryptPayrollDirectDeposit(dataEnvironment, config, decryptor);
                            break;
                        case "tblmsrsnonpaid":
                            SendMessage("Decrypting payroll MSRS non-paid");
                            DecryptPayrollMSRSNonPaid(dataEnvironment,config,decryptor);
                            break;
                        case "tblrecipients":
                            SendMessage("Decrypting payroll check recipient");
                            DecryptPayrollCheckRecipient(dataEnvironment, config, decryptor);
                            break;
                        case "tbltemppayprocess":
                            SendMessage("Decrypting payroll temp check detail");
                            DecryptPayrollTempCheckDetail(dataEnvironment, config, decryptor);
                            break;
                        case "tblw2archive":
                            SendMessage("Decrypting payroll w2 archive");
                            DecryptPayrollW2Archive(dataEnvironment, config, decryptor);
                            break;
                        case "tblw2edittable":
                            SendMessage("Decrypting payroll w2 edit table");
                            DecryptPayrollW2EditTable(dataEnvironment, config, decryptor);
                            break;
                        case "tblw2original":
                            SendMessage("Decrypting payroll w2 original");
                            DecryptPayrollW2Original(dataEnvironment, config, decryptor);
                            break;
                        case "w2c":
                            SendMessage("Decrypting payroll w2-c");
                            DecryptPayrollW2C(dataEnvironment, config, decryptor);
                            break;
                    }
                }

                SendMessage("Finished decrypting payroll database");
            }
        }

        private void DecryptEmployeeMaster(string dataEnvironment, SQLConfigInfo config, ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            if (dbHelper.GetDataType("tblEmployeeMaster", "DateBirth", MakeLiveDBName(dataEnvironment,"Payroll"), config).ToLower() != "datetime")
            {                
                using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from tblEmployeeMaster", connection))
                    {
                        DataTable dataTable = new DataTable("tblEmployeeMaster");
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var ssn = decryptor.Decrypt(row["SSN"].ToString());
                            var dateBirth = decryptor.Decrypt(row["DateBirth"].ToString());
                            //dbHelper.Execute("Update tblEmployeeMaster set SSN = '" + ssn + "', DateBirth = '" + dateBirth + "' where id = " + Convert.ToInt32(row["Id"]),payrollDB,config );
                            var saveCommand = new SqlCommand("Update tblEmployeeMaster set SSN = '" + ssn + "', DateBirth = '" + dateBirth + "' where id = " + Convert.ToInt32(row["Id"]),connection);
                            saveCommand.ExecuteNonQuery();
                        }
                    }                    
                }
                dbHelper.Execute("Alter Table tblEmployeeMaster Alter Column DateBirth datetime null",payrollDB,config);
                dbHelper.Execute("update tblEmployeeMaster set DateBirth = '12/30/1899' where DateBirth = '01/01/1900'",payrollDB,config);
            }
            dbHelper.Execute("delete from convertedtables where TableName = 'tblEmployeeMaster'",payrollDB,config);

        }

        private void DecryptPayrollACAEmployeeDependents(string dataEnvironment, SQLConfigInfo config, ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from ACAEmployeeDependents", connection))
                {
                    DataTable dataTable = new DataTable("ACAEmployeeDependents");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var ssn = decryptor.Decrypt(row["SSN"].ToString());
                        var dateBirth = decryptor.Decrypt(row["DateOfBirth"].ToString());
                        //dbHelper.Execute("Update tblEmployeeMaster set SSN = '" + ssn + "', DateBirth = '" + dateBirth + "' where id = " + Convert.ToInt32(row["Id"]),payrollDB,config );
                        var saveCommand = new SqlCommand("Update ACAEmployeeDependents set SSN = '" + ssn + "', DateOfBirth = '" + dateBirth + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("delete from convertedtables where TableName = 'ACAEmployeeDependents'", payrollDB, config);
        }

        private void DecryptPayrollACHInformation(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblACHInformation", connection))
                {
                    DataTable dataTable = new DataTable("tblACHInformation");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["EmployerAccount"].ToString());
                        var saveCommand = new SqlCommand("Update tblACHInformation set EmployerAccount = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = 'tblACHInformation'", payrollDB, config);
        }

        private void DecryptPayrollCheckRecipient(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblRecipients", connection))
                {
                    DataTable dataTable = new DataTable("tblRecipients");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["EFTAccount"].ToString());                        
                        var saveCommand = new SqlCommand("Update tblRecipients set EFTAccount = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = 'tblRecipients'", payrollDB, config);
        }

        private void DecryptPayrollBanks(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblBanks", connection))
                {
                    DataTable dataTable = new DataTable("tblBanks");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["CheckingAccount"].ToString());
                        var saveCommand = new SqlCommand("Update tblBanks set CheckingAccount = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = 'tblBanks'", payrollDB, config);
        }

        private void DecryptPayrollCheckDetail(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblCheckDetail", connection))
                {
                    DataTable dataTable = new DataTable("tblCheckDetail");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(row["DDAccountNumber"].ToString()))
                        {
                            var account = decryptor.Decrypt(row["DDAccountNumber"].ToString());
                            var saveCommand =
                                new SqlCommand(
                                    "Update tblCheckDetail set DDAccountNumber = '" + account + "' where id = " +
                                    Convert.ToInt32(row["Id"]), connection);
                            saveCommand.ExecuteNonQuery();
                        }
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = 'tblCheckDetail'", payrollDB, config);
        }

        private void DecryptPayrollCheckReturn(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblCheckReturn", connection))
                {
                    DataTable dataTable = new DataTable("tblCheckReturn");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["DDAccountNumber"].ToString());
                        var saveCommand = new SqlCommand("Update tblCheckReturn set DDAccountNumber = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = 'tblCheckReturn'", payrollDB, config);
        }

        private void DecryptPayrollDirectDeposit(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblDirectDeposit", connection))
                {
                    DataTable dataTable = new DataTable("tblDirectDeposit");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["Account"].ToString());
                        var saveCommand = new SqlCommand("Update tblDirectDeposit set Account = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = 'tblDirectDeposit'", payrollDB, config);
        }

        private void DecryptPayrollMSRSNonPaid(string dataEnvironment, SQLConfigInfo config, ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            if (dbHelper.GetDataType("tblMSRSNonPaid", "DateOfBirth", MakeLiveDBName(dataEnvironment, "Payroll"), config).ToLower() != "datetime")
            {
                using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from tblMSRSNonPaid", connection))
                    {
                        DataTable dataTable = new DataTable("tblMSRSNonPaid");
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            var ssn = decryptor.Decrypt(row["SocialSecurityNumber"].ToString());
                            var dateBirth = decryptor.Decrypt(row["DateOfBirth"].ToString());                            
                            var saveCommand = new SqlCommand("Update tblMSRSNonPaid set SocialSecurityNumber = '" + ssn + "', DateOfBirth = '" + dateBirth + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                            saveCommand.ExecuteNonQuery();
                        }
                    }
                }
                dbHelper.Execute("Alter Table tblMSRSNonPaid Alter Column DateOfBirth datetime null", payrollDB, config);
                dbHelper.Execute("update tblMSRSNonPaid set DateOfBirth = '12/30/1899' where DateOfBirth = '01/01/1900'",payrollDB,config);
            }
            dbHelper.Execute("delete from convertedtables where TableName = 'tblMSRSNonPaid'", payrollDB, config);

        }

        private void DecryptPayrollTempCheckDetail(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblTempPayProcess", connection))
                {
                    DataTable dataTable = new DataTable("tblTempPayProcess");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["DDAccountNumber"].ToString());
                        var saveCommand = new SqlCommand("Update tblTempPayProcess set DDAccountNumber = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = 'tblTempPayProcess'", payrollDB, config);
        }

        private void DecryptPayrollW2Archive(string dataEnvironment, SQLConfigInfo config, ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblW2Archive", connection))
                {
                    DataTable dataTable = new DataTable("tblW2Archive");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var ssn = decryptor.Decrypt(row["SSN"].ToString());                        
                        var saveCommand = new SqlCommand("Update tblW2Archive set SSN = '" + ssn + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }            
            dbHelper.Execute("delete from convertedtables where TableName = 'tblW2Archive'", payrollDB, config);

        }

        private void DecryptPayrollW2EditTable(string dataEnvironment, SQLConfigInfo config, ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblW2EditTable", connection))
                {
                    DataTable dataTable = new DataTable("tblW2EditTable");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var ssn = decryptor.Decrypt(row["SSN"].ToString());
                        var saveCommand = new SqlCommand("Update tblW2EditTable set SSN = '" + ssn + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("delete from convertedtables where TableName = 'tblW2EditTable'", payrollDB, config);

        }

        private void DecryptPayrollW2Original(string dataEnvironment, SQLConfigInfo config, ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from tblW2Original", connection))
                {
                    DataTable dataTable = new DataTable("tblW2Original");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var ssn = decryptor.Decrypt(row["SSN"].ToString());
                        var saveCommand = new SqlCommand("Update tblW2Original set SSN = '" + ssn + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("delete from convertedtables where TableName = 'tblW2Original'", payrollDB, config);

        }

        private void DecryptPayrollW2C(string dataEnvironment, SQLConfigInfo config, ICryptoUtility decryptor)
        {
            var payrollDB = MakeLiveDBName(dataEnvironment, "Payroll");
            using (var connection = dbHelper.GetConnectionFromConfig(config, payrollDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from W2C", connection))
                {
                    DataTable dataTable = new DataTable("W2C");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var ssn = decryptor.Decrypt(row["SSN"].ToString());
                        var prevssn = decryptor.Decrypt(row["prevssn"].ToString());
                        var saveCommand = new SqlCommand("Update W2C set SSN = '" + ssn + "', prevssn = '" + prevssn + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("delete from convertedtables where TableName = 'W2C'", payrollDB, config);

        }

        private void DecryptMotorVehicleDatabase(string dataEnvironment, SQLConfigInfo config, string muniName)
        {
            if (HasDatabase(dataEnvironment, "MotorVehicle", config))
            {
                SendMessage("Decrypting motor vehicle database");
                var encryptionVectors = GetEncryptionVectors(muniName);
                var decryptor = new ChilkatCryptoUtility(encryptionVectors.InitializationVector,
                    encryptionVectors.EncryptionKey);
                SendMessage("Decrypting master");
                DecryptMotorVehicleMaster(dataEnvironment, config, decryptor);
                SendMessage("Decrypting activity master");
                DecryptMotorVehicleActivityMaster(dataEnvironment, config, decryptor);
                SendMessage("Decrypting archive master");
                DecryptMotorVehicleArchiveMaster(dataEnvironment, config, decryptor);
                SendMessage("Decrypting held registration master");
                DecryptMotorVehicleHeldRegistrations(dataEnvironment, config, decryptor);
                SendMessage("Decrypting pending activity master");
                DecryptMotorVehiclePendingActivity(dataEnvironment, config, decryptor);
                SendMessage("Decrypting master temp");
                DecryptMotorVehicleMasterTemp(dataEnvironment, config, decryptor);
                SendMessage("Decrypting fleet master");
                DecryptMotorVehicleFleetMaster(dataEnvironment, config, decryptor);
                SendMessage("Decrypting title table");
                DecryptMotorVehicleTitle(dataEnvironment, config, decryptor);
                SendMessage("Decrypting double cta");
                DecryptMotorVehicleDoubleCTA(dataEnvironment, config, decryptor);
                SendMessage("Decrypting transit plates");
                DecryptMotorVehicleTransitPlates(dataEnvironment, config, decryptor);
                SendMessage("Decrypting use tax");
                DecryptMotorVehicleUseTax(dataEnvironment, config, decryptor);
                SendMessage("Finished decrypting motor vehicle database");
            }
        }

        private void DecryptUtilityDatabase(string dataEnvironment, SQLConfigInfo config, string muniName)
        {
            if (HasDatabase(dataEnvironment, "UtilityBilling", config))
            {
                SendMessage("Decrypting utility billing database");
                var encryptionVectors = GetEncryptionVectors(muniName);
                var decryptor = new ChilkatCryptoUtility(encryptionVectors.InitializationVector,
                    encryptionVectors.EncryptionKey);
                var convertedTables = new List<string>();
                var utDB = MakeLiveDBName(dataEnvironment, "UtilityBilling");
                using (var connection = dbHelper.GetConnectionFromConfig(config, utDB))
                {
                    connection.Open();
                    using (var adapter = new SqlDataAdapter("select * from ConvertedTables", connection))
                    {
                        DataTable dataTable = new DataTable("ConvertedTables");
                        adapter.Fill(dataTable);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            convertedTables.Add(row["TableName"].ToString());
                        }
                    }
                }


                foreach (var tableName in convertedTables)
                {
                    switch (tableName.ToLower())
                    {
                        case "tblacctach":
                            SendMessage("Decrypting tblacctach");
                            DecryptUtilityAcctACH(dataEnvironment, config, decryptor);
                            break;
                        case "tblachinformation":
                            SendMessage("Decrypting ACH information");
                            DecryptUtilityACHInformation(dataEnvironment, config, decryptor);
                            break;
                    }
                }

                SendMessage("Finished decrypting utility billing database");
            }
        }

        private void DecryptMotorVehicleMaster(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "Master";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var taxIDNumber = decryptor.Decrypt(row["TaxIDNumber"].ToString());
                        var dob1 = decryptor.Decrypt(row["Dob1"].ToString());
                        var dob2 = decryptor.Decrypt(row["Dob2"].ToString());
                        var dob3 = decryptor.Decrypt(row["Dob3"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set TaxIDNumber = '" + taxIDNumber + "', Dob1 = '" + dob1 + "',Dob2 = '" + dob2 + "',Dob3 = '" + dob3 + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob1 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob2 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob3 datetime null", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB1 = '12/30/1899' where dob1 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB2 = '12/30/1899' where dob2 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB3 = '12/30/1899' where dob3 = '01/01/1900'", mvDB, config);
            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }

        private void DecryptMotorVehicleActivityMaster(string dataEnvironment, SQLConfigInfo config, ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "ActivityMaster";

            if (dbHelper.GetDataType(tableName, "Dob1", MakeLiveDBName(dataEnvironment, "MotorVehicle"), config).ToLower() == "datetime") return;

            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                using (var selectCommand = new SqlCommand("select id,TaxIDNumber, dob1,dob2,dob3 from ActivityMaster", connection))
                {
                    using (var saveConnection = dbHelper.GetConnectionFromConfig(config, mvDB))
                    {
                        using (var saveCommand =
                            new SqlCommand(
                                "Update ActivityMaster set TaxIDNumber = @taxIDNumber, Dob1 = @dob1, Dob2 = @dob2, Dob3 = @dob3 where id = @id",
                                saveConnection))
                        {
                            saveCommand.Parameters.Add("@id", SqlDbType.Int);
                            saveCommand.Parameters.Add("@TaxIDNumber", SqlDbType.VarChar);
                            saveCommand.Parameters.Add("@dob1", SqlDbType.VarChar);
                            saveCommand.Parameters.Add("@dob2", SqlDbType.VarChar);
                            saveCommand.Parameters.Add("@dob3", SqlDbType.VarChar);

                            connection.Open();
                            saveConnection.Open();
                            using (var rdr = selectCommand.ExecuteReader(CommandBehavior.SequentialAccess))
                            {
                                while (rdr.Read())
                                {
                                    saveCommand.Parameters["@id"].Value = Convert.ToInt32(rdr["Id"]);
                                    saveCommand.Parameters["@taxIDNumber"].Value =
                                        decryptor.Decrypt(rdr["TaxIDNumber"].ToString());
                                    saveCommand.Parameters["@dob1"].Value = decryptor.Decrypt(rdr["Dob1"].ToString());
                                    saveCommand.Parameters["@dob2"].Value = decryptor.Decrypt(rdr["Dob2"].ToString());
                                    saveCommand.Parameters["@dob3"].Value = decryptor.Decrypt(rdr["Dob3"].ToString());
                                    saveCommand.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
            }
            dbHelper.Execute("Alter Table ActivityMaster Alter Column Dob1 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table ActivityMaster Alter Column Dob2 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table ActivityMaster Alter Column Dob3 datetime null", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB1 = '12/30/1899' where dob1 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB2 = '12/30/1899' where dob2 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB3 = '12/30/1899' where dob3 = '01/01/1900'", mvDB, config);
            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }

        private void DecryptMotorVehicleArchiveMaster(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "ArchiveMaster";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var taxIDNumber = decryptor.Decrypt(row["TaxIDNumber"].ToString());
                        var dob1 = decryptor.Decrypt(row["Dob1"].ToString());
                        var dob2 = decryptor.Decrypt(row["Dob2"].ToString());
                        var dob3 = decryptor.Decrypt(row["Dob3"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set TaxIDNumber = '" + taxIDNumber + "', Dob1 = '" + dob1 + "',Dob2 = '" + dob2 + "',Dob3 = '" + dob3 + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
                dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob1 datetime null", mvDB, config);
                dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob2 datetime null", mvDB, config);
                dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob3 datetime null", mvDB, config);
                dbHelper.Execute("update " + tableName + " set DOB1 = '12/30/1899' where dob1 = '01/01/1900'", mvDB, config);
                dbHelper.Execute("update " + tableName + " set DOB2 = '12/30/1899' where dob2 = '01/01/1900'", mvDB, config);
                dbHelper.Execute("update " + tableName + " set DOB3 = '12/30/1899' where dob3 = '01/01/1900'", mvDB, config);
            }

            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }
        private void DecryptMotorVehicleHeldRegistrations(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "HeldRegistrationMaster";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var taxIDNumber = decryptor.Decrypt(row["TaxIDNumber"].ToString());
                        var dob1 = decryptor.Decrypt(row["Dob1"].ToString());
                        var dob2 = decryptor.Decrypt(row["Dob2"].ToString());
                        var dob3 = decryptor.Decrypt(row["Dob3"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set TaxIDNumber = '" + taxIDNumber + "', Dob1 = '" + dob1 + "',Dob2 = '" + dob2 + "',Dob3 = '" + dob3 + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob1 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob2 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob3 datetime null", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB1 = '12/30/1899' where dob1 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB2 = '12/30/1899' where dob2 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB3 = '12/30/1899' where dob3 = '01/01/1900'", mvDB, config);
            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }
        private void DecryptMotorVehiclePendingActivity(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "PendingActivityMaster";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var taxIDNumber = decryptor.Decrypt(row["TaxIDNumber"].ToString());
                        var dob1 = decryptor.Decrypt(row["Dob1"].ToString());
                        var dob2 = decryptor.Decrypt(row["Dob2"].ToString());
                        var dob3 = decryptor.Decrypt(row["Dob3"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set TaxIDNumber = '" + taxIDNumber + "', Dob1 = '" + dob1 + "',Dob2 = '" + dob2 + "',Dob3 = '" + dob3 + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob1 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob2 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob3 datetime null", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB1 = '12/30/1899' where dob1 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB2 = '12/30/1899' where dob2 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB3 = '12/30/1899' where dob3 = '01/01/1900'", mvDB, config);
            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }
        private void DecryptMotorVehicleMasterTemp(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "tblMasterTemp";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var taxIDNumber = decryptor.Decrypt(row["TaxIDNumber"].ToString());
                        var dob1 = decryptor.Decrypt(row["Dob1"].ToString());
                        var dob2 = decryptor.Decrypt(row["Dob2"].ToString());
                        var dob3 = decryptor.Decrypt(row["Dob3"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set TaxIDNumber = '" + taxIDNumber + "', Dob1 = '" + dob1 + "',Dob2 = '" + dob2 + "',Dob3 = '" + dob3 + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob1 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob2 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob3 datetime null", mvDB, config);
            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB1 = '12/30/1899' where dob1 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB2 = '12/30/1899' where dob2 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB3 = '12/30/1899' where dob3 = '01/01/1900'", mvDB, config);
        }
        private void DecryptMotorVehicleFleetMaster(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "FleetMaster";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var taxIDNumber = decryptor.Decrypt(row["TaxID"].ToString());
                        var dob1 = decryptor.Decrypt(row["Dob1"].ToString());
                        var dob2 = decryptor.Decrypt(row["Dob2"].ToString());
                        var dob3 = decryptor.Decrypt(row["Dob3"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set TaxID = '" + taxIDNumber + "', Dob1 = '" + dob1 + "',Dob2 = '" + dob2 + "',Dob3 = '" + dob3 + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob1 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob2 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob3 datetime null", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB1 = '12/30/1899' where dob1 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB2 = '12/30/1899' where dob2 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB3 = '12/30/1899' where dob3 = '01/01/1900'", mvDB, config);
            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }
        private void DecryptMotorVehicleTitle(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "Title";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {                       
                        var dob1 = decryptor.Decrypt(row["Dob1"].ToString());
                        var dob2 = decryptor.Decrypt(row["Dob2"].ToString());                       
                        var saveCommand = new SqlCommand("Update " + tableName + " set  Dob1 = '" + dob1 + "',Dob2 = '" + dob2 + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob1 datetime null", mvDB, config);
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob2 datetime null", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB1 = '12/30/1899' where dob1 = '01/01/1900'", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB2 = '12/30/1899' where dob2 = '01/01/1900'", mvDB, config);
            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }
        private void DecryptMotorVehicleDoubleCTA(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "DoubleCTA";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var dob1 = decryptor.Decrypt(row["Dob"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set  Dob = '" + dob1 + "' where id = " + Convert.ToInt32(row["Id"]), connection);

                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob datetime null", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB = '12/30/1899' where dob = '01/01/1900'", mvDB, config);
            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }
        private void DecryptMotorVehicleTransitPlates(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "TransitPlates";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var dob1 = decryptor.Decrypt(row["Dob"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set  Dob = '" + dob1 + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }
            dbHelper.Execute("Alter Table " + tableName + " Alter Column Dob datetime null", mvDB, config);
            dbHelper.Execute("update " + tableName + " set DOB = '12/30/1899' where dob = '01/01/1900'", mvDB, config);
        }
        private void DecryptMotorVehicleUseTax(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var mvDB = MakeLiveDBName(dataEnvironment, "MotorVehicle");
            var tableName = "UseTax";
            using (var connection = dbHelper.GetConnectionFromConfig(config, mvDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var ssn = decryptor.Decrypt(row["PurchaserSSN"].ToString()).Replace("'","");
                        var saveCommand = new SqlCommand("Update " + tableName + " set  PurchaserSSN = '" + ssn + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            //dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", mvDB, config);
        }
        private void DecryptUtilityACHInformation(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var utilityDB = MakeLiveDBName(dataEnvironment, "UtilityBilling");
            var tableName = "tblACHInformation";
            using (var connection = dbHelper.GetConnectionFromConfig(config, utilityDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["CompanyAccount"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set CompanyAccount = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", utilityDB, config);
        }

        private void DecryptUtilityAcctACH(string dataEnvironment, SQLConfigInfo config,
            ICryptoUtility decryptor)
        {
            var utilityDB = MakeLiveDBName(dataEnvironment, "UtilityBilling");
            var tableName = "tblAcctACH";
            using (var connection = dbHelper.GetConnectionFromConfig(config, utilityDB))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from " + tableName, connection))
                {
                    DataTable dataTable = new DataTable(tableName);
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var account = decryptor.Decrypt(row["ACHAcctNumber"].ToString());
                        var saveCommand = new SqlCommand("Update " + tableName + " set ACHAcctNumber = '" + account + "' where id = " + Convert.ToInt32(row["Id"]), connection);
                        saveCommand.ExecuteNonQuery();
                    }
                }
            }

            dbHelper.Execute("delete from convertedtables where TableName = '" + tableName + "'", utilityDB, config);
        }
        private string MakeLiveDBName(string environment,string database)
        {
            return MakeDBName(environment, database, "Live");
        }

        private string MakeDBName(string environment, string database, string archiveType)
        {
            return "TRIO_" + environment + "_" + archiveType + "_" + database;
        }
        public void ConvertData()
        {
            try
            {
                var cdocUtil = new CentralDocumentDBUtil(webEnvironment.Name,webConfigInfo);
                SendMessage("Checking central documents schema");
                cdocUtil.CheckSchema();

                if (HasRealEstateDatabase(webEnvironment.Name, webConfigInfo))
                {
	                var reUtil = new RealEstateDBUtil(webEnvironment.Name, webConfigInfo);
	                SendMessage("Checking Real Estate schema");
	                reUtil.CheckSchema();
                }

                WireUpDependencies(webConfigInfo,webEnvironment.Name);
                using (var scope = diContainer.BeginLifetimeScope())
                {                    
                    commandDispatcher = scope.Resolve<CommandDispatcher>();
                    

                    ConvertAttachedDocuments(webEnvironment.Name, webConfigInfo);
                    if (HasRealEstateDatabase(webEnvironment.Name, webConfigInfo))
                    {
                        ConvertPictures();
                        var sketchPath = Path.Combine(sqlEnvironment.DataDirectory, "sketches");
                        
                        if (Directory.Exists(sketchPath))
                        {
                            if (upgradeSketchImages)
                            {
                                var tempSketchPath = Path.Combine(sqlEnvironment.DataDirectory, "tempsketches");
                                if (!Directory.Exists(tempSketchPath))
                                {
                                    Directory.CreateDirectory(tempSketchPath);
                                }
                                if (UpdateSketches(sketchPath,tempSketchPath))
                                {
                                    ConvertSketches(sqlEnvironment, webEnvironment.Name, webConfigInfo,
                                        commandDispatcher,false);
                                }
                            }
                            else
                            {
                                ConvertSketches(sqlEnvironment, webEnvironment.Name, webConfigInfo, commandDispatcher,true);
                            }
                        }
                        else
                        {
                            SendMessage("Sketch path not found");
                        }
                    }

                   
                    SendMessage("Conversion finished");
                }
            }
            catch (Exception e)
            {
                SendMessage(e.Message);                
            }
        }

        private bool IsSameData()
        {
            if (sqlEnvironment.Name.ToLower() == webEnvironment.Name.ToLower())
            {
                if (sqlConfigInfo.ServerInstance.ToLower() == webConfigInfo.ServerInstance.ToLower())
                {
                    return true;
                }
            }

            return false;
        }

        private void ConvertInfoToClientSettings(Client selectedClient, ITrioContextFactory factory,
	        SQLConfigInfo webConfigInfo)
        {
	        var clientSettingsContext = factory.GetClientSettingsContext();

            var clientName = selectedClient.Name.Trim() == "" ? "Default" : selectedClient.Name.Trim();

	        var existingClient = clientSettingsContext.Clients.FirstOrDefault(x => x.Name.ToLower() == clientName.ToLower());

	        if (existingClient == null)
	        {
                clientSettingsContext.Clients.Add(new SharedApplication.ClientSettings.Models.Client
		        {
			        Name = clientName,
			        ClientIdentifier = Guid.NewGuid()
		        });
                clientSettingsContext.SaveChanges();
                existingClient = clientSettingsContext.Clients.FirstOrDefault(x => x.Name.ToLower() == clientName.ToLower());
            }

	        var dataDetails = new DataContextDetails();
	        dataDetails.DataSource = webConfigInfo.ServerInstance;
	        dataDetails.Password = webConfigInfo.Password;
	        dataDetails.UserID = webConfigInfo.UserName;
	        
            foreach (var environment in selectedClient.DataEnvironments)
	        {
		        var ssUtil = new SystemSettingsDBUtil(environment.Name, webConfigInfo);
                if (HasDatabase(environment.Name, "SystemSettings", webConfigInfo))
                {
                    SendMessage("Checking " + environment.Name + " System Settings schema");
                    ssUtil.CheckSchema();
                    var hasPayrollDatabase = HasPayrollDatabase(environment.Name, webConfigInfo);
                    if (hasPayrollDatabase)
                    {
                        var pyUtil = new PayrollDBUtil(environment.Name, webConfigInfo);
                        SendMessage("Checking " + environment.Name + " Payroll schema");
                        pyUtil.CheckSchema();
                    }

                    var hasRealEstateDatabase = HasRealEstateDatabase(environment.Name, webConfigInfo);

                    if (hasRealEstateDatabase)
                    {
                        var reUtil = new RealEstateDBUtil(environment.Name, webConfigInfo);
                        SendMessage("Checking " + environment.Name + " Real Estate schema");
                        reUtil.CheckSchema();
                    }

                    dataDetails.DataEnvironment = environment.Name;
                    var contextFactory = new TrioContextFactory(dataDetails, globalSettings);
                    var systemSettingsContext = contextFactory.GetSystemSettingsContext();

                    CryptoUtility cryptoUtility = new CryptoUtility();

                    var salt = cryptoUtility.GetRandomSalt();
                    var trioPasswordHash = cryptoUtility.ComputeSHA512Hash("trio", salt);

                    foreach (var systemSettingsUser in systemSettingsContext.Securities.Include(
                        x => x.PreviousPasswords))
                    {
                        var existingUser = clientSettingsContext.Users.FirstOrDefault(x =>
                            x.UserID.ToLower() == systemSettingsUser.UserID.ToLower() &&
                            x.ClientIdentifier == existingClient.ClientIdentifier);

                        if (existingUser == null)
                        {
                            clientSettingsContext.Users.Add(new User
                            {
                                ClientIdentifier = existingClient.ClientIdentifier,
                                CanUpdateSite = false,
                                DateChanged = systemSettingsUser.DateChanged,
                                DefaultAdvancedSearch = systemSettingsUser.DefaultAdvancedSearch,
                                FailedAttempts = systemSettingsUser.FailedAttempts,
                                Frequency = systemSettingsUser.Frequency,
                                Inactive = systemSettingsUser.Inactive,
                                LockedOut = systemSettingsUser.LockedOut,
                                LockoutDateTime = systemSettingsUser.LockoutDateTime,
                                OPID = systemSettingsUser.OPID,
                                Password = trioPasswordHash,
                                Salt = salt,
                                UpdateDate = systemSettingsUser.UpdateDate,
                                UserID = systemSettingsUser.UserID,
                                UserIdentifier = Guid.NewGuid(),
                                UserName = systemSettingsUser.UserName,
                                PreviousPasswords = systemSettingsUser.PreviousPasswords.Select(x => new PasswordHistory
                                {
                                    DateAdded = x.DateAdded,
                                    Password = x.Password,
                                    Salt = x.Salt,
                                }).ToList()
                            });

                            clientSettingsContext.SaveChanges();

                            existingUser = clientSettingsContext.Users.FirstOrDefault(x =>
                                x.UserID.ToLower() == systemSettingsUser.UserID.ToLower() &&
                                x.ClientIdentifier == existingClient.ClientIdentifier);
                        }

                        foreach (var permission in systemSettingsContext.UserPermissions.Where(x =>
                            (!x.MovedToClientSettings ?? false) && x.UserID == systemSettingsUser.ID))
                        {
                            permission.UserID = existingUser.Id;
                            permission.MovedToClientSettings = true;
                        }

                        foreach (var permission in systemSettingsContext.Operators.Where(x =>
                            (!x.MovedToClientSettings ?? false) && x.SecurityID == systemSettingsUser.ID))
                        {
                            permission.SecurityID = existingUser.Id;
                            permission.MovedToClientSettings = true;
                        }

                        systemSettingsContext.SaveChanges();

                        if (hasPayrollDatabase)
                        {
                            var payrollContext = contextFactory.GetPayrollContext();

                            foreach (var permission in payrollContext.PayrollPermissions.Where(x =>
                                (!x.MovedToClientSettings ?? false) && x.UserId == systemSettingsUser.ID))
                            {
                                permission.UserId = existingUser.Id;
                                permission.MovedToClientSettings = true;
                            }

                            payrollContext.SaveChanges();
                        }

                        if (hasRealEstateDatabase)
                        {
                            var realEstateContext = contextFactory.GetRealEstateContext();

                            foreach (var report in realEstateContext.ReportTitles.Where(x =>
                                (!x.MovedToClientSettings ?? false) && x.UserId == systemSettingsUser.ID))
                            {
                                report.UserId = existingUser.Id;
                                report.MovedToClientSettings = true;
                            }

                            realEstateContext.SaveChanges();
                        }


                    }
                }
                else
                {
                    SendMessage("Skipping " + environment.Name + " because system settings not found");
                }
            }
        }
			

        private void ConvertAttachedDocuments(string environmentName,SQLConfigInfo config)
        {
            if (HasRealEstateDatabase(environmentName, config))
            {
                ConvertRELinkedDocuments(environmentName, config);
            }

            if (HasDatabase(environmentName, "Budgetary",config))
            {
                ConvertBDLinkedDocuments(environmentName, config);
            }

            if (HasDatabase(environmentName, "UtilityBilling", config))
            {
                ConvertUTLinkedDocuments(environmentName, config);
            }

            if (HasDatabase(environmentName, "Clerk", config))
            {
                ConvertCKLinkedDocuments(environmentName,config);
            }

            if (HasDatabase(environmentName, "CodeEnforcement", config))
            {
                ConvertCELinkedDocuments(environmentName, config);
            }
        }

        private void ConvertPictures()
        {
            //if (HasRealEstateDatabase(sqlEnvironment.Name,sqlConfigInfo))
            //{
                if (HasRealEstateDatabase(webEnvironment.Name, webConfigInfo))
                {
                    SendMessage("Importing Real Estate Pictures");
                    var pictures = GetPictureRecords(webEnvironment.Name, webConfigInfo);
                    var locations = GetDefaultPictureLocations(webEnvironment.Name, webConfigInfo);
                    ImportPictures(pictures, locations, webEnvironment.Name, webConfigInfo);
                }
                else
                {
                    SendMessage("Skipped converting pictures. Real Estate for " + webEnvironment.Name + " doesn't exist");
                }
           // }

        }

        private void FillCashReceiptsIdentifiers(string environmentName, ITrioContextFactory factory,
            SQLConfigInfo webConfigInfo)
        {
            SendMessage("Filling Receipt Identifiers in Archive table");
            var cashReceiptsContext = factory.GetCashReceiptsContext();

            var receipts = cashReceiptsContext.Receipts.Include(x => x.Archives);
            
            foreach (var receipt in receipts)
            {
                var receiptId = Guid.NewGuid();

                receipt.ReceiptIdentifier = receiptId;
                foreach (var archive in receipt.Archives)
                {
                    archive.ReceiptIdentifier = receiptId;
                    archive.TransactionIdentifier = Guid.NewGuid();
                }
            }

            cashReceiptsContext.SaveChanges();
            SendMessage("Archive update complete");

            SendMessage("Filling Receipt Identifiers in LasYearArchive table");
            var lastYearReceipts = cashReceiptsContext.LastYearReceipts.Include(x => x.Archives);

            foreach (var receipt in lastYearReceipts)
            {
                var receiptId = Guid.NewGuid();

                receipt.ReceiptIdentifier = receiptId;
                foreach (var archive in receipt.Archives)
                {
                    archive.ReceiptIdentifier = receiptId;
                    archive.TransactionIdentifier = Guid.NewGuid();
                }
            }

            cashReceiptsContext.SaveChanges();
            SendMessage("LastYearArchive update complete");
        }

        private void ConvertRELinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            SendMessage("Importing Real Estate documents");
            var documents = GetRELinkedDocuments(dataEnvironment, config);
            ImportRELinkedDocuments(documents, dataEnvironment, config);
        }

        private List<LinkedDocument> GetRELinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            var documents = new List<LinkedDocument>();
            using (var connection = dbHelper.GetConnectionFromConfig(config,"TRIO_" + dataEnvironment + "_Live_RealEstate"))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from LinkedDocuments", connection))
                {
                    DataTable dataTable = new DataTable("LinkedDocuments");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        documents.Add(new LinkedDocument()
                        {
                            Id = Convert.ToInt32(row["ID"]),
                            Description = row["Description"].ToString(),
                            DocReferenceId = Convert.ToInt32(row["DocReferenceID"]),
                            DocReferenceType = Convert.ToInt32(row["DocReferenceType"]),
                            FileName = row["FileName"].ToString()
                        });
                    }
                }
            }

            return documents;
        }

        private List<BDLinkedDocument> GetBDLinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            var documents = new List<BDLinkedDocument>();
            using (var connection =
                dbHelper.GetConnectionFromConfig(config, "TRIO_" + dataEnvironment + "_Live_Budgetary"))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from LinkedDocuments", connection))
                {
                    DataTable dataTable = new DataTable("LinkedDocuments");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        documents.Add(new BDLinkedDocument()
                        {
                            Id = Convert.ToInt32(row["ID"]),
                            Description = row["Description"].ToString(),
                            DocReferenceId = Convert.ToInt32(row["DocReferenceID"]),
                            DocReferenceType = row["DocReferenceType"].ToString(),
                            FileName = row["FileName"].ToString()
                        });
                    }
                }
            }
                return documents;
        }
        private void ConvertBDLinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            SendMessage("Importing Budgetary documents");
            var documents = GetBDLinkedDocuments(dataEnvironment, config);
            ImportBDLinkedDocuments(documents, dataEnvironment, config);
        }

        private void ConvertUTLinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            SendMessage("Importing Utility Billing documents");
            var documents = GetUTLinkedDocuments(dataEnvironment, config);
            ImportUTLinkedDocuments(documents, dataEnvironment, config);
        }

        private void ConvertCKLinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            SendMessage("Importing Clerk documents");
            var documents = GetCKLinkedDocuments(dataEnvironment, config);
            ImportCKLinkedDocuments(documents, dataEnvironment, config);
        }

        private void ConvertCELinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            SendMessage("Importing Code Enforcement documents");
            var documents = GetCELinkedDocuments(dataEnvironment, config);
            ImportCELinkedDocuments(documents, dataEnvironment, config);
        }
        private List<UTLinkedDocument> GetUTLinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            var documents = new List<UTLinkedDocument>();
            using (var connection =
                dbHelper.GetConnectionFromConfig(config, "TRIO_" + dataEnvironment + "_Live_UtilityBilling"))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from LinkedDocuments", connection))
                {
                    DataTable dataTable = new DataTable("LinkedDocuments");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        documents.Add(new UTLinkedDocument()
                        {
                            Id = Convert.ToInt32(row["ID"]),
                            Description = row["Description"].ToString(),
                            AccountNumber = Convert.ToInt32(row["AccountNumber"]),                            
                            FileName = row["FileName"].ToString()
                        });
                    }
                }
            }
            return documents;
        }

        private List<LinkedDocument> GetCKLinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            var documents = new List<LinkedDocument>();
            using (var connection = dbHelper.GetConnectionFromConfig(config, "TRIO_" + dataEnvironment + "_Live_Clerk"))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from LinkedDocuments", connection))
                {
                    DataTable dataTable = new DataTable("LinkedDocuments");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        documents.Add(new LinkedDocument()
                        {
                            Id = Convert.ToInt32(row["ID"]),
                            Description = row["Description"].ToString(),
                            DocReferenceId = Convert.ToInt32(row["DocReferenceID"]),
                            DocReferenceType = Convert.ToInt32(row["DocReferenceType"]),
                            FileName = row["FileName"].ToString()
                        });
                    }
                }
            }

            return documents;
        }

        private List<LinkedDocument> GetCELinkedDocuments(string dataEnvironment, SQLConfigInfo config)
        {
            var documents = new List<LinkedDocument>();
            using (var connection = dbHelper.GetConnectionFromConfig(config, "TRIO_" + dataEnvironment + "_Live_CodeEnforcement"))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from LinkedDocuments", connection))
                {
                    DataTable dataTable = new DataTable("LinkedDocuments");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        documents.Add(new LinkedDocument()
                        {
                            Id = Convert.ToInt32(row["ID"]),
                            Description = row["Description"].ToString(),
                            DocReferenceId = Convert.ToInt32(row["DocReferenceID"]),
                            DocReferenceType = Convert.ToInt32(row["DocReferenceType"]),
                            FileName = row["FileName"].ToString()
                        });
                    }
                }
            }

            return documents;
        }
        private bool HasRealEstateDatabase(string environmentName,SQLConfigInfo config)
        {
            using (var connection = dbHelper.GetConnectionFromConfig(config))
            {
                connection.Open();
                var comm = new SqlCommand("select Cast( case when db_id('TRIO_" + environmentName + "_Live_RealEstate') is not null then 1 else 0 end as bit) as databaseexists",connection);
                var reExists = (bool)comm.ExecuteScalar();
                comm.Dispose();
                return reExists;
            }
        }

        private bool HasPayrollDatabase(string environmentName, SQLConfigInfo config)
        {
	        using (var connection = dbHelper.GetConnectionFromConfig(config))
	        {
		        connection.Open();
		        var comm = new SqlCommand("select Cast( case when db_id('TRIO_" + environmentName + "_Live_Payroll') is not null then 1 else 0 end as bit) as databaseexists", connection);
		        var pyExists = (bool)comm.ExecuteScalar();
		        comm.Dispose();
		        return pyExists;
	        }
        }

        private bool HasDatabase(string environmentName, string databaseType, SQLConfigInfo config)
        {
            return HasDatabase(environmentName, databaseType, "Live", config);
        }

        private bool HasDatabase(string environmentName, string databaseType, string groupType, SQLConfigInfo config)
        {
            using (var connection = dbHelper.GetConnectionFromConfig(config))
            {
                connection.Open();
                var comm = new SqlCommand("select Cast( case when db_id('TRIO_" + environmentName + "_" + groupType + "_" + databaseType + "') is not null then 1 else 0 end as bit) as databaseexists", connection);
                var reExists = (bool)comm.ExecuteScalar();
                comm.Dispose();
                return reExists;
            }
        }

        private List<PictureRecord> GetPictureRecords(string dataEnvironment, SQLConfigInfo config)
        {
            var pictures = new List<PictureRecord>();
            var cards = new List<PropertyCardInfo>();
            using (var connection = dbHelper.GetConnectionFromConfig(config))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select RSAccount,RSCard,CardId,Id from [" + GetDBName(dataEnvironment,"RealEstate") + "].[dbo].Master order by RSaccount,RSCard",connection))
                {
                    DataTable dataTable = new DataTable("Master");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        cards.Add(new PropertyCardInfo()
                        {                            
                            Account = Convert.ToInt32(row["RsAccount"]),
                            Card = Convert.ToInt32(row["RSCard"]),
                            CardId = row["CardId"].ToString()
                        });
                    }
                }
                using (var adapter =
                    new SqlDataAdapter(
                        "select * from [" + GetDBName(dataEnvironment, "RealEstate") +
                        "].[dbo].pictureRecord order by MRAccountNumber,RSCard,PicNum", connection))
                {
                    DataTable dataTable = new DataTable("PictureRecord");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var cardId = "";
                        var card = cards.FirstOrDefault(c => c.Account == Convert.ToInt32(row["MRAccountNumber"]) &&
                                                             c.Card == Convert.ToInt32(row["RSCard"]));
                        if (card != null)
                        {
                            cardId = card.CardId.Replace("{","").Replace("}","");
                        }
                        pictures.Add(new PictureRecord()
                        {
                            Id = Convert.ToInt32(row["Id"]),
                            Account = Convert.ToInt32(row["MRAccountNumber"]),
                            Card = Convert.ToInt32(row["RSCard"]),
                            PicNum = Convert.ToInt32(row["PicNum"]),
                            FileName = row["PictureLocation"].ToString(),
                            DocumentIdentifier = (Guid)(row["DocumentIdentifier"]),
                            Description = row["Description"].ToString(),
                            CardId = cardId
                        });
                    }
                    dataTable.Dispose();
                }
                    
            }

            return pictures;
        }

        private List<string> GetDefaultPictureLocations(string dataEnvironment, SQLConfigInfo config)
        {
            var locations = new List<string>();
            using (var connection = dbHelper.GetConnectionFromConfig(config))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter("select * from [" + GetDBName(dataEnvironment, "RealEstate") +
                                                        "].[dbo].DefaultPictureLocations", connection))
                {
                    DataTable dataTable = new DataTable("PictureRecord");
                    adapter.Fill(dataTable);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        locations.Add(row["location"].ToString());
                    }
                    dataTable.Dispose();
                }
            }
                return locations;
        }

        private void ImportBDLinkedDocuments(List<BDLinkedDocument> documents, string dataEnvironment,
            SQLConfigInfo config)
        {
            foreach (var document in documents)
            {
                if (!string.IsNullOrWhiteSpace(document.FileName))
                {
                    ImportBDLinkedDocument(document, dataEnvironment, config);
                }
            }
        }

        private void ImportUTLinkedDocuments(List<UTLinkedDocument> documents, string dataEnvironment,
            SQLConfigInfo config)
        {
            foreach (var document in documents)
            {
                if (!string.IsNullOrWhiteSpace(document.FileName))
                {
                    ImportUTLinkedDocument(document,dataEnvironment,config);
                }
            }
        }
        private void ImportRELinkedDocuments(List<LinkedDocument> documents, string dataEnvironment,
            SQLConfigInfo config)
        {
            foreach (var document in documents)
            {
                if (!string.IsNullOrWhiteSpace(document.FileName))
                {
                    ImportRELinkedDocument(document,dataEnvironment,config);
                }
            }
        }

        private void ImportCKLinkedDocuments(List<LinkedDocument> documents, string dataEnvironment,
            SQLConfigInfo config)
        {
            foreach (var document in documents)
            {
                if (!string.IsNullOrWhiteSpace(document.FileName))
                {
                    ImportCKLinkedDocument(document,dataEnvironment,config);
                }
            }
        }

        private void ImportCELinkedDocuments(List<LinkedDocument> documents, string dataEnvironment,
            SQLConfigInfo config)
        {
            foreach (var document in documents)
            {
                if (!string.IsNullOrWhiteSpace(document.FileName))
                {
                    ImportCELinkedDocument(document, dataEnvironment, config);
                }
            }
        }

        private void ImportRELinkedDocument(LinkedDocument document, string dataEnvironment, SQLConfigInfo config)
        {
            if (File.Exists(document.FileName))
            {
                var mediaType = GetMediaTypeFromFilename(document.FileName);
                if (!string.IsNullOrWhiteSpace(mediaType))
                {
                    var fileBytes = File.ReadAllBytes(document.FileName);                    
                    var createCommand = new CreateCentralDocument(mediaType, document.Description,"" , document.DocReferenceId, "", "RealEstate", Guid.NewGuid(), fileBytes, Path.GetFileName(document.FileName), DateTime.Now);
                    AddDocument(config, dataEnvironment, createCommand);
                    dbHelper.Execute("delete from LinkedDocuments where id = " + document.Id, "TRIO_" + dataEnvironment + "_Live_RealEstate", config);
                }                
            }            
        }

        private void ImportCKLinkedDocument(LinkedDocument document, string dataEnvironment, SQLConfigInfo config)
        {
            if (File.Exists(document.FileName))
            {
                var mediaType = GetMediaTypeFromFilename(document.FileName);
                if (!string.IsNullOrWhiteSpace(mediaType))
                {
                    var fileBytes = File.ReadAllBytes(document.FileName);
                    var refType = "";
                    switch (document.DocReferenceType)
                    {
                        case 1:
                            refType = "Dog";
                            break;
                        case 2:
                            refType = "Kennel";
                            break;
                        case 3:
                            refType = "Birth";
                            break;
                        case 4:
                            refType = "Death";
                            break;
                        case 5:
                            refType = "Burial";
                            break;
                        case 6:
                            refType = "Marriage";
                            break;

                    }
                    var createCommand = new CreateCentralDocument(mediaType, document.Description, refType, document.DocReferenceId, "", "Clerk", Guid.NewGuid(), fileBytes, Path.GetFileName(document.FileName), DateTime.Now);
                    AddDocument(config, dataEnvironment, createCommand);
                    dbHelper.Execute("delete from LinkedDocuments where id = " + document.Id, "TRIO_" + dataEnvironment + "_Live_Clerk", config);

                }
            }
        }

        private void ImportCELinkedDocument(LinkedDocument document, string dataEnvironment, SQLConfigInfo config)
        {
            if (File.Exists(document.FileName))
            {
                var mediaType = GetMediaTypeFromFilename(document.FileName);
                if (!string.IsNullOrWhiteSpace(mediaType))
                {
                    var fileBytes = File.ReadAllBytes(document.FileName);
                    var refType = "";
                    switch (document.DocReferenceType)
                    {
                        case 0:
                            refType = "Property";
                            break;
                        case 16:
                            refType = "Violation";
                            break;
                        case 19:
                            refType = "Activity";
                            break;                       
                    }
                    var createCommand = new CreateCentralDocument(mediaType, document.Description, refType, document.DocReferenceId, "", "CodeEnforcement", Guid.NewGuid(), fileBytes, Path.GetFileName(document.FileName), DateTime.Now);
                    AddDocument(config, dataEnvironment, createCommand);
                    dbHelper.Execute("delete from LinkedDocuments where id = " + document.Id, "TRIO_" + dataEnvironment + "_Live_CodeEnforcement", config);

                }
            }
        }
        private void ImportBDLinkedDocument(BDLinkedDocument document, string dataEnvironment, SQLConfigInfo config)
        {
            if (File.Exists(document.FileName))
            {
                var mediaType = GetMediaTypeFromFilename(document.FileName);
                if (!string.IsNullOrWhiteSpace(mediaType))
                {
                    var fileBytes = File.ReadAllBytes(document.FileName);
                    document.DocReferenceType = document.DocReferenceType.Replace("Budgetary_", "");
                    var createCommand = new CreateCentralDocument(mediaType, document.Description, document.DocReferenceType, document.DocReferenceId, "", "Budgetary", Guid.NewGuid(), fileBytes, Path.GetFileName(document.FileName), DateTime.Now);
                    AddDocument(config, dataEnvironment, createCommand);
                    dbHelper.Execute("delete from LinkedDocuments where id = " + document.Id, "TRIO_" + dataEnvironment + "_Live_Budgetary", config);
                }
            }
        }

        private void ImportUTLinkedDocument(UTLinkedDocument document, string dataEnvironment, SQLConfigInfo config)
        {
            if (File.Exists(document.FileName))
            {
                var mediaType = GetMediaTypeFromFilename(document.FileName);
                if (!string.IsNullOrWhiteSpace(mediaType))
                {
                    var fileBytes = File.ReadAllBytes(document.FileName);                    
                    var createCommand = new CreateCentralDocument(mediaType, document.Description, "Account", document.AccountNumber, "", "UtilityBilling", Guid.NewGuid(), fileBytes, Path.GetFileName(document.FileName), DateTime.Now);
                    AddDocument(config, dataEnvironment, createCommand);
                    dbHelper.Execute("delete from LinkedDocuments where id = " + document.Id, "TRIO_" + dataEnvironment + "_Live_UtilityBilling", config);
                }
            }
        }
        private string GetMediaTypeFromFilename(string fileName)
        {
            var extension = Path.GetExtension(fileName).Replace(".", "").ToLower();
            switch (extension)
            {
                case "pdf":
                    return "application/pdf";
                    break;
                case "jpg":
                case "jpeg":
                    return "image/jpeg";
                    break;
                case "bmp":
                    return "image/bmp";
                    break;
                case "png":
                    return "image/png";
                    break;
                case "gif":
                    return "image/gif";
                    break;
                case "tif":
                case "tiff":
                    return "image/tif";
                    break;
            }

            return "";
        }
        private void ImportPictures(List<PictureRecord> pictures, List<string> locations, string dataEnvironment,
            SQLConfigInfo config)
        {
            foreach (var pictureRecord in pictures)
            {
                if (!string.IsNullOrWhiteSpace(pictureRecord.FileName))
                {
                    var fileName = FindFile(pictureRecord.FileName, locations);
                    if (!string.IsNullOrWhiteSpace(fileName))
                    {
                        ImportPicture(fileName,pictureRecord, dataEnvironment,config);
                    }
                }
            }
        }

        private string FindFile(string fileName,List<string> locations)
        {
            if (File.Exists(fileName))
            {
                return fileName;
            }

            var file = Path.GetFileName(fileName);
            foreach (var location in locations)
            {
                var newFile = Path.Combine(location, file);
                if (File.Exists(newFile))
                {
                    return newFile;
                }
            }

            return "";
        }

       private string  GetDBName(string dataEnvironment, string db)
       {
           return "TRIO_" + dataEnvironment + "_Live_" + db;
       }
        
        private void ConvertSketches(SQLDataEnvironment sqlEnvironment,string dataEnvironment, SQLConfigInfo config,CommandDispatcher dispatcher,bool copyFiles)
        {
            SendMessage("Importing Sketches");
            var sketchesPath = Path.Combine(sqlEnvironment.DataDirectory, "Sketches");
            var tempSketchesPath = Path.Combine(sqlEnvironment.DataDirectory, "tempsketches");
            if (!Directory.Exists(tempSketchesPath))
            {
                Directory.CreateDirectory(tempSketchesPath);
            }
            var files = Directory.GetFiles(sketchesPath).ToList();

            if (copyFiles)
            {
                foreach (var fileName in files)
                {
                    File.Copy(fileName, Path.Combine(tempSketchesPath, Path.GetFileName(fileName)));
                }
            }
            var sketchInfos = new List<SketchFileInfo>();
            files = Directory.GetFiles(tempSketchesPath).ToList();
            foreach (var fileName in files)
            {
                var extension = Path.GetExtension(fileName).ToLower();
                var justFile = Path.GetFileNameWithoutExtension(fileName);
                if (extension == ".skt" || extension == ".bmp")
                {
                    var sketchInfo = sketchInfos.FirstOrDefault(s => s.FileName == justFile);
                    if (sketchInfo == null)
                    {
                        sketchInfo = new SketchFileInfo(){FileName =  justFile};
                        sketchInfos.Add(sketchInfo);
                    }

                    if (!(extension == ".bmp"))
                    {
                        sketchInfo.HasSketch = true;
                    }
                    else
                    {
                        sketchInfo.HasPicture = true;
                    }
                }
            }

            var sketchFiles = sketchInfos.Where(s => s.HasSketch == true).ToList();
            var cards = new List<PropertyCardInfo>();
            using (var connection = dbHelper.GetConnectionFromConfig(config))
            {
                connection.Open();
                using (var adapter =
                    new SqlDataAdapter(
                        "select RSAccount,RSCard,CardId,Id from [" + GetDBName(dataEnvironment, "RealEstate") +
                        "].[dbo].Master order by RSaccount,RSCard", connection))
                {
                    DataTable dataTable = new DataTable("Master");
                    adapter.Fill(dataTable);                    
                    foreach (DataRow row in dataTable.Rows)
                    {
                        cards.Add(new PropertyCardInfo()
                        {
                            Account = Convert.ToInt32(row["RSAccount"]),
                            Card = Convert.ToInt32(row["RSCard"]),
                            CardId = row["CardId"].ToString(),
                            Id = Convert.ToInt32(row["ID"])
                        });
                    }                   
                }
                    
                foreach (var sketchFile in sketchFiles)
                {
                    var card = GetPropertyCardInfoFromSketch(sketchFile.FileName,cards);
                    if (card != null)
                    {
                        var sketchName = Path.Combine(sketchesPath, sketchFile.FileName + ".skt");
                        var picName = Path.Combine(sketchesPath, sketchFile.FileName + ".bmp");
                        var sketchBytes = File.ReadAllBytes(sketchName);
                        byte[] picBytes = null;
                        if (File.Exists(picName))
                        {
                            picBytes = File.ReadAllBytes(picName);
                            SendMessage("Saving Sketch " + sketchName + " for account " + card.Account);
                            dispatcher.Send(new CreateSketchDocument(card.CardIdentifier, Guid.NewGuid(),
                                card.SequenceNumber, DateTime.Now, "", @"image/bmp", picBytes, sketchBytes));
                        }
                        else
                        {
                            SendMessage("Sketch " + sketchName + " had no picture");
                        }
                        
                        
                    }
                    else
                    {
                        SendMessage("No card found for sketch " + sketchFile.FileName);
                    }
                }
            }
            Directory.Delete(tempSketchesPath,true);
        }

        private SketchCardInfo GetPropertyCardInfoFromSketch(string filename, List<PropertyCardInfo> cards)
        {
            var cardInfo = new SketchCardInfo();
            var hexedSequence = Right(filename, 1);
            var hexedId = filename.Substring(0, filename.Length - 1);
            var sequenceNumber = Int32.Parse(hexedSequence,System.Globalization.NumberStyles.HexNumber);
            var recordId = Int32.Parse(hexedId, System.Globalization.NumberStyles.HexNumber);
            var card = cards.FirstOrDefault(c => c.Id == recordId);
            if (card != null)
            {
                cardInfo.Id = card.Id;
                cardInfo.Account = card.Account;
                cardInfo.Card = card.Card;
                cardInfo.CardIdentifier = card.CardIdentifier;
                cardInfo.SequenceNumber = sequenceNumber;
            }

            return cardInfo;
        }

        private void SendMessage(string text)
        {
            if (MessageNotification != null)
            {
                MessageNotification(this, text);
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void ImportPicture(string fileName,PictureRecord pictureRecord, string dataEnvironment, SQLConfigInfo config)
        {
            var fileBytes = File.ReadAllBytes(fileName);
            var resizedBytes = FileUtil.ResizeImageAsJpeg(fileBytes, 1920, 1080);
            var createCommand = new CreateCentralDocument(@"image/jpeg", pictureRecord.Description,"PropertyPicture",pictureRecord.Account,pictureRecord.CardId,"RealEstate",pictureRecord.DocumentIdentifier,resizedBytes,Path.GetFileName(fileName),DateTime.Now);
            AddDocument(config,dataEnvironment,createCommand);
            pictureRecord.FileName = Path.GetFileName(fileName);
            UpdatePictureRecord(config, dataEnvironment, pictureRecord);
        }

        private void AddDocument(SQLConfigInfo config,string dataEnvironment,CreateCentralDocument createCommand)
        {
            using (var connection = dbHelper.GetConnectionFromConfig(config, "TRIO_" + dataEnvironment + "_Live_CentralDocuments"))
            {
                using (var command = new SqlCommand("Insert_Document",connection)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    command.Parameters.AddWithValue("@ID", 0);
                    command.Parameters.AddWithValue("@AltReference", createCommand.AltReference);
                    command.Parameters.AddWithValue("@ItemDescription", createCommand.Description);
                    command.Parameters.AddWithValue("@ItemName", createCommand.ItemName);
                    command.Parameters.AddWithValue("@MediaType", createCommand.MediaType);
                    command.Parameters.AddWithValue("@ReferenceId", createCommand.ReferenceId);
                    command.Parameters.AddWithValue("@ReferenceType", createCommand.ReferenceType);
                    command.Parameters.AddWithValue("@DataGroup", createCommand.DataGroup);
                    command.Parameters.AddWithValue("@ItemData", createCommand.ItemData);
                    command.Parameters.AddWithValue("@DocumentIdentifier", createCommand.DocumentIdentifier);
                    command.Parameters.AddWithValue("@DateCreated", createCommand.DateCreated);
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        private void UpdatePictureRecord(SQLConfigInfo config, string dataEnvironment, PictureRecord pictureRecord)
        {
            dbHelper.Execute("Update PictureRecord set PictureLocation = '" + pictureRecord.FileName + "' where id = " + pictureRecord.Id,"TRIO_" + dataEnvironment + "_Live_RealEstate",config);
        }

        private void WireUpDependencies(SQLConfigInfo config, string dataEnvironment)
        {
            var dataDetails = new DataContextDetails();
            dataDetails.DataSource = config.ServerInstance;
            dataDetails.DataEnvironment = dataEnvironment;
            dataDetails.Password = config.Password;
            dataDetails.UserID = config.UserName;
            var contextFactory = new TrioContextFactory(dataDetails, globalSettings);

            var builder = new ContainerBuilder();
            builder.RegisterInstance(dataDetails).As<DataContextDetails>();
            builder.RegisterType<TelemetryDummy>().As<ITelemetryService>();
            builder.RegisterInstance(globalSettings).As<cGlobalSettings>();
            builder.RegisterInstance(contextFactory).As<ITrioContextFactory>();
            builder.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            builder.RegisterAssemblyModules(AppDomain.CurrentDomain.GetAssemblies());
            diContainer = builder.Build();
        }

        private bool UpdateSketches(string sketchPath,string tempSketchPath)
        {
            SendMessage("Updating sketches.  This may take a while");
            if (!Directory.Exists(sketchPath))
            {
                SendMessage("Directory not found");                
                return false;
            }

            var sketchForm = new frmSketchUpdate(sketchPath,tempSketchPath);
            sketchForm.ShowDialog();
            
            SendMessage("Sketch images updated");
            return true;
        }

       
        private class SketchFileInfo
        {
            public string FileName { get; set; }
            public bool HasSketch { get; set; } = false;
            public bool HasPicture { get; set; } = false;
            public int Sequencenumber { get; set; } = 0;
            public int RecordId { get; set; } = 0;
            public Guid CardIdentifier { get; set; } = Guid.Empty;
        }

        public  string Right( string sourceString, int numberOfCharacters)
        {
            if (numberOfCharacters > sourceString.Length)
            {
                return sourceString;
            }

            if (numberOfCharacters < 1)
            {
                return "";
            }
            return sourceString.Substring(sourceString.Length - numberOfCharacters, numberOfCharacters);
        }
    }
}