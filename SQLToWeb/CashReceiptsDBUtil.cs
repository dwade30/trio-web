﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLToWeb
{
    public class CashReceiptsDBUtil
    {
        private SQLConfigInfo configurationInfo;
        private DBHelper dbHelper = new DBHelper();
        private string dataEnvironment = "";

        public CashReceiptsDBUtil(string dataEnvironment, SQLConfigInfo config)
        {
            configurationInfo = config;
            this.dataEnvironment = dataEnvironment;
        }

        public void CheckSchema()
        {
            AddIdentifiers();
        }

        private string BuildDatabaseName()
        {
            return "TRIO_" + dataEnvironment + "_Live_CashReceipts";
        }

        private void AddIdentifiers()
        {
            string strSQL = "";

            if (!dbHelper.FieldExists("ReceiptIdentifier", "Archive", BuildDatabaseName(), configurationInfo))
            {
                strSQL = "Alter Table.[dbo].[Archive] Add [ReceiptIdentifier] uniqueidentifier NULL";
                dbHelper.Execute(strSQL, BuildDatabaseName(), configurationInfo);
            }

            if (!dbHelper.FieldExists("TransactionIdentifier", "Archive", BuildDatabaseName(), configurationInfo))
            {
                strSQL = "Alter Table.[dbo].[Archive] Add [TransactionIdentifier] uniqueidentifier NULL";
                dbHelper.Execute(strSQL, BuildDatabaseName(), configurationInfo);
            }

            if (!dbHelper.FieldExists("ReceiptIdentifier", "LastYearArchive", BuildDatabaseName(), configurationInfo))
            {
                strSQL = "Alter Table.[dbo].[LastYearArchive] Add [ReceiptIdentifier] uniqueidentifier NULL";
                dbHelper.Execute(strSQL, BuildDatabaseName(), configurationInfo);
            }

            if (!dbHelper.FieldExists("TransactionIdentifier", "LastYearArchive", BuildDatabaseName(), configurationInfo))
            {
                strSQL = "Alter Table.[dbo].[LastYearArchive] Add [TransactionIdentifier] uniqueidentifier NULL";
                dbHelper.Execute(strSQL, BuildDatabaseName(), configurationInfo);
            }

            if (!dbHelper.FieldExists("ReceiptIdentifier", "Receipt", BuildDatabaseName(), configurationInfo))
            {
                strSQL = "Alter Table.[dbo].[Receipt] Add [ReceiptIdentifier] uniqueidentifier NULL";
                dbHelper.Execute(strSQL, BuildDatabaseName(), configurationInfo);
            }

            if (!dbHelper.FieldExists("ReceiptIdentifier", "LastYearReceipt", BuildDatabaseName(), configurationInfo))
            {
                strSQL = "Alter Table.[dbo].[LastYearReceipt] Add [ReceiptIdentifier] uniqueidentifier NULL";
                dbHelper.Execute(strSQL, BuildDatabaseName(), configurationInfo);
            }
        }
    }
}
