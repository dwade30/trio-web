﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLToWeb
{
    public partial class frmSketchUpdate : Form
    {
        private winskt.WinSkt appWinSketch;
        private string pathToSketches = "";
        private string tempPathToSketches = "";
        public frmSketchUpdate()
        {
            InitializeComponent();
           
        }

        public frmSketchUpdate(string sketchPath,string tempSketchPath) : this()
        {
            pathToSketches = sketchPath;
            tempPathToSketches = tempSketchPath;
            this.Shown += FrmSketchUpdate_Shown;            
        }

        private void FrmSketchUpdate_Shown(object sender, EventArgs e)
        {
            UpdateSketches(pathToSketches,tempPathToSketches);
        }

        private void UpdateSketches(string sketchPath,string tempSketchPath)
        {
            if (!Directory.Exists(sketchPath))
            {
                MessageBox.Show("Directory not found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinSkt"))
            {
                process.Kill();
            }
            appWinSketch = null;
            var sketchFiles = Directory.GetFiles(sketchPath, "*.skt").ToList();
            foreach (var filename in sketchFiles)
            {
                if (Path.GetExtension(filename).ToLower() == ".skt")
                {
                    rtbOutput.AppendText("\nUpdating " + filename);
                    File.Copy(filename, Path.Combine(tempSketchPath, Path.GetFileName(filename)));
                    UpdateSketch(tempSketchPath, Path.GetFileName(filename));
                }
            }
            foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinSkt"))
            {
                process.Kill();
            }

            appWinSketch = null;
            MessageBox.Show("Sketch images updated", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void UpdateSketch(string sketchFolder,string filename)
        {

            if (appWinSketch == null)
            {
                appWinSketch = new winskt.WinSkt();
            }

            appWinSketch.OpenSketch(Path.Combine(sketchFolder,filename));
            if (appWinSketch.GetImage(0))
            {
                try
                {
                    Bitmap newImage = Utils.GetImageFromClipboard();
                    if (newImage != null )
                    {
                        //save to file
                        string bitmapFileName = filename.Replace(".skt", ".bmp");
                        string bitmapFilePath = Path.Combine(sketchFolder, bitmapFileName);
                        if (File.Exists(bitmapFilePath))
                        {
                            File.Delete(bitmapFilePath);
                        }
                        using (var croppedBitmap = Utils.CropWhiteSpace(newImage))
                        {
                            croppedBitmap.Save(bitmapFilePath,System.Drawing.Imaging.ImageFormat.Bmp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    rtbOutput.AppendText("\n" + ex.Message);
                }
            }
          
        }

        //private string BrowsePath(string initialPath)
        //{
        //    var fbd = new FolderBrowserDialog();
        //    fbd.Description = "Choose sketch location";
        //    fbd.ShowNewFolderButton = false;
        //    var thePath = "";
        //    if (initialPath.Trim() != string.Empty)
        //    {
        //        if (System.IO.Directory.Exists(initialPath))
        //        {
        //            thePath = initialPath;
        //            fbd.SelectedPath = thePath;
        //        }
        //    }
        //    if (string.IsNullOrWhiteSpace(thePath))
        //    {
        //        fbd.RootFolder = Environment.SpecialFolder.MyComputer;
        //    }
        //    var result = fbd.ShowDialog();
        //    if (result == DialogResult.OK)
        //    {
        //        return fbd.SelectedPath;
        //    }

        //    return "";
        //}
    }
}
