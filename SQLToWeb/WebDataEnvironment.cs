﻿using System;

namespace SQLToWeb
{
    public class WebDataEnvironment
    {
        public String Name { get; set; } = "";
        public String DataDirectory { get; set; } = "";
    }
}