﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLToWeb
{
	public class ClientSettingsDBUtil
	{
        private SQLConfigInfo configurationInfo;
        private DBHelper dbHelper = new DBHelper();
        private string databaseName = "TRIO_All_Live_ClientSettings";

        public ClientSettingsDBUtil(SQLConfigInfo config)
        {
            configurationInfo = config;
        }
        public void CheckSchema()
        {
            CheckClientSettings();
        }

        private void CheckClientSettings()
        {
            if (!dbHelper.DatabaseExists(databaseName, configurationInfo))
            {
                CreateClientSettingsDatabase();
            }
            AddDBVersion();
            AddClientTable();
            AddUserTable();
            AddSiteVersionTable();
            AddPasswordHistoryTable();
        }

        private void CreateClientSettingsDatabase()
        {
	        string strSQL;
	        
	        strSQL = "Create Database [" + databaseName + "]";
	        dbHelper.Execute(strSQL, "Master", configurationInfo);
        }

        private void AddDBVersion()
        {

            string strSQL = "";
            if (!dbHelper.TableExists("DBVersion", databaseName, configurationInfo))
            {
                strSQL = dbHelper.GetTableCreationHeaderSQL("DBVersion");
                strSQL += "[Build] [int]  NULL,";
                strSQL += "[Major] [int] NULL,";
                strSQL += "[Minor] [int] NULL,";
                strSQL += "[Revision] [int] NULL ";
                strSQL += dbHelper.GetTablePKSql("DBVersion");
                dbHelper.Execute(strSQL, databaseName, configurationInfo);
            }
        }

        private void AddSiteVersionTable()
        {
	        string strSQL = "";
	        if (!dbHelper.TableExists("SiteVersion", databaseName, configurationInfo))
	        {
		        strSQL = dbHelper.GetTableCreationHeaderSQL("SiteVersion");
		        strSQL += "[Version] [nvarchar] (50) NULL";
                strSQL += dbHelper.GetTablePKSql("SiteVersion");
		        dbHelper.Execute(strSQL, databaseName, configurationInfo);
	        }
        }

        private void AddUserTable()
        {
	        string strSQL = "";
	        if (!dbHelper.TableExists("Users", databaseName, configurationInfo))
	        {
		        strSQL = dbHelper.GetTableCreationHeaderSQL("Users");
		        strSQL += "[UserID] [nvarchar] (255) NULL,";
		        strSQL += "[OPID] [nvarchar] (255) NULL,";
		        strSQL += "[UserName] [nvarchar] (255) NULL,";
		        strSQL += "[Frequency] [int] NULL,";
		        strSQL += "[DateChanged] [datetime] NULL, ";
		        strSQL += "[UpdateDate] [datetime] NULL,";
		        strSQL += "[Password] [nvarchar] (Max) NULL,";
		        strSQL += "[DefaultAdvancedSearch] [bit] NULL,";
		        strSQL += "[LockedOut] [bit] Null, ";
		        strSQL += "[Inactive] [bit] NULL,";
		        strSQL += "[FailedAttempts] [int] NULL,";
		        strSQL += "[LockoutDateTime] [datetime] NULL,";
		        strSQL += "[Salt] [nvarchar] (255) Null, ";
		        strSQL += "[CanUpdateSite] [bit] NULL,";
		        strSQL += "[ClientIdentifier] [UniqueIdentifier] Not Null, ";
		        strSQL += "[UserIdentifier] [UniqueIdentifier] Not Null ";
                strSQL += dbHelper.GetTablePKSql("Users");
		        dbHelper.Execute(strSQL, databaseName, configurationInfo);
	        }
        }

        private void AddClientTable()
        {

	        string strSQL = "";
	        if (!dbHelper.TableExists("Clients", databaseName, configurationInfo))
	        {
		        strSQL = dbHelper.GetTableCreationHeaderSQL("Clients");
		        strSQL += "[Name] [nvarchar] (255) NULL,";
		        strSQL += "[ClientIdentifier] [UniqueIdentifier] Not Null ";
                strSQL += dbHelper.GetTablePKSql("Clients");
		        dbHelper.Execute(strSQL, databaseName, configurationInfo);
	        }
        }

        private void AddPasswordHistoryTable()
        {

	        string strSQL = "";
	        if (!dbHelper.TableExists("PasswordHistory", databaseName, configurationInfo))
	        {
		        strSQL = dbHelper.GetTableCreationHeaderSQL("PasswordHistory");
		        strSQL += "[UserId] [int] NOT NULL,";
		        strSQL += "[Password] [nvarchar] (Max) NULL,";
		        strSQL += "[Salt] [nvarchar] (255) NULL,";
		        strSQL += "[DateAdded] [datetime] NULL";
                strSQL += dbHelper.GetTablePKSql("PasswordHistory");
		        dbHelper.Execute(strSQL, databaseName, configurationInfo);
	        }
        }
	}
}
