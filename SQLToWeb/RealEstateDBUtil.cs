﻿namespace SQLToWeb
{
    public class RealEstateDBUtil
    {
        private SQLConfigInfo configurationInfo;
        private string dataEnvironment = "";
        private DBHelper dbHelper = new DBHelper();

        public RealEstateDBUtil(string dataEnvironment, SQLConfigInfo config)
        {
            this.dataEnvironment = dataEnvironment;
            configurationInfo = config;
        }
        public void CheckSchema()
        {
            CheckPictureRecords("TRIO_" + dataEnvironment + "_Live_RealEstate");
            CheckReportTitles("TRIO_" + dataEnvironment + "_Live_RealEstate");
        }

        private void CheckReportTitles(string databaseName)
        {
	        if (!dbHelper.FieldExists("MovedToClientSettings", "ReportTitles", databaseName, configurationInfo))
	        {
		        dbHelper.Execute("Alter Table ReportTitles Add MovedToClientSettings bit Null", databaseName, configurationInfo);
		        dbHelper.Execute("Update ReportTitles set MovedToClientSettings = 0 where MovedToClientSettings is null",
			        databaseName, configurationInfo);
	        }
        }

        private void CheckPictureRecords(string databaseName)
        {
            if (!dbHelper.FieldExists("DocumentIdentifier", "PictureRecord", databaseName, configurationInfo))
            {
                AddDocumentIdentifierToPictures(databaseName);
            }
        }

        private void AddDocumentIdentifierToPictures(string databaseName)
        {
            dbHelper.Execute("Alter Table PictureRecord Add DocumentIdentifier UniqueIdentifier Not Null Default newid()", databaseName, configurationInfo);
            dbHelper.Execute("Update PictureRecord set DocumentIdentifier = NEWID() where DocumentIdentifier is null",
                databaseName, configurationInfo);
        }
    }
}