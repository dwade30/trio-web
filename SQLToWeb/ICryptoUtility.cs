﻿namespace SQLToWeb
{
    public interface ICryptoUtility
    {
        string Decrypt(string encryptedValue);
        string Encrypt(string value);
    }
}