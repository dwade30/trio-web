﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLToWeb
{
	public class PayrollDBUtil
	{
		private SQLConfigInfo configurationInfo;
		private DBHelper dbHelper = new DBHelper();
		private string dataEnvironment = "";

		public PayrollDBUtil(string dataEnvironment, SQLConfigInfo config)
		{
			configurationInfo = config;
			this.dataEnvironment = dataEnvironment;
		}

		public void CheckSchema()
		{
			CheckClientSettings();
		}

		private void CheckClientSettings()
		{
			UpdatePermissionsTable("TRIO_" + dataEnvironment + "_Live_Payroll");
		}

		private void UpdatePermissionsTable(string databaseName)
		{
			if (!dbHelper.FieldExists("MovedToClientSettings", "PayrollPermissions", databaseName, configurationInfo))
			{
				dbHelper.Execute("Alter Table PayrollPermissions Add MovedToClientSettings bit Null", databaseName, configurationInfo);
				dbHelper.Execute("Update PayrollPermissions set MovedToClientSettings = 0 where MovedToClientSettings is null",
					databaseName, configurationInfo);
			}
		}
	}
}
