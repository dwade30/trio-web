﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Remotion.Linq.Clauses.Expressions;

namespace SQLToWeb
{
	public class SystemSettingsDBUtil
	{
		private SQLConfigInfo configurationInfo;
		private DBHelper dbHelper = new DBHelper();
		private string dataEnvironment = "";

		public SystemSettingsDBUtil(string dataEnvironment, SQLConfigInfo config)
		{
			configurationInfo = config;
			this.dataEnvironment = dataEnvironment;
		}

		public void CheckSchema()
		{
			CheckClientSettings();
		}

		private void CheckClientSettings()
		{
			UpdateSecurityTable("TRIO_" + dataEnvironment + "_Live_SystemSettings");
			UpdatePermissionsTable("TRIO_" + dataEnvironment + "_Live_SystemSettings");
			UpdateOperatorsTable("TRIO_" + dataEnvironment + "_Live_SystemSettings");
		}

		private void UpdateSecurityTable(string databaseName)
		{
			if (!dbHelper.FieldExists("LockedOut","Security", databaseName, configurationInfo))
			{
				dbHelper.Execute("Alter Table Security Add [LockedOut] bit null", databaseName, configurationInfo);
				dbHelper.Execute("Update Security set LockedOut = 0", databaseName, configurationInfo);
			}

			if (!dbHelper.FieldExists("Inactive", "Security", databaseName, configurationInfo))
			{
				dbHelper.Execute("Alter Table Security Add [Inactive] bit null", databaseName, configurationInfo);
				dbHelper.Execute("Update Security set Inactive = 0", databaseName, configurationInfo);
			}

			if (!dbHelper.FieldExists("FailedAttempts", "Security", databaseName, configurationInfo))
			{
				dbHelper.Execute("Alter Table Security Add [FailedAttempts] int null", databaseName, configurationInfo);
				dbHelper.Execute("Update Security set FailedAttempts = 0", databaseName, configurationInfo);
			}

			if (!dbHelper.FieldExists("LockoutDateTime", "Security", databaseName, configurationInfo))
			{
				dbHelper.Execute("Alter Table Security Add [LockoutDateTime] datetime null", databaseName, configurationInfo);
			}

			if (!dbHelper.FieldExists("Salt", "Security", databaseName, configurationInfo))
			{
				dbHelper.Execute("Alter Table Security Add [Salt] nvarchar(255) null", databaseName, configurationInfo);
			}


			if (!dbHelper.TableExists("PasswordHistory", databaseName, configurationInfo))
			{
				var sb = new StringBuilder();
				sb.Append(dbHelper.GetTableCreationHeaderSQL("PasswordHistory"));
				sb.Append("[SecurityId] [int] Not NULL,");
				sb.Append("[Password] [nvarchar] (max) NULL,");
				sb.Append("[Salt] [nvarchar] (255) NULL,");
				sb.Append("[DateAdded] [datetime] NULL,");
				sb.Append(dbHelper.GetTablePKSql("PasswordHistory"));
				dbHelper.Execute(sb.ToString(), databaseName, configurationInfo);
			}
		}

		private void UpdatePermissionsTable(string databaseName)
		{
			if (!dbHelper.FieldExists("MovedToClientSettings", "PermissionsTable", databaseName, configurationInfo))
			{
				dbHelper.Execute("Alter Table PermissionsTable Add MovedToClientSettings bit Null", databaseName, configurationInfo);
				dbHelper.Execute("Update PermissionsTable set MovedToClientSettings = 0 where MovedToClientSettings is null",
					databaseName, configurationInfo);
			}
		}

		private void UpdateOperatorsTable(string databaseName)
		{
			if (!dbHelper.FieldExists("MovedToClientSettings", "Operators", databaseName, configurationInfo))
			{
				dbHelper.Execute("Alter Table Operators Add MovedToClientSettings bit Null", databaseName, configurationInfo);
				dbHelper.Execute("Update Operators set MovedToClientSettings = 0 where MovedToClientSettings is null",
					databaseName, configurationInfo);
			}
		}
	}
}
