﻿using System;

namespace SQLToWeb
{
    public class SQLDataEnvironment
    {
        public String Name { get; set; } = "";
        public String DataDirectory { get; set; } = "";
        //public String AccessDirectory { get; set; } = "";
    }
}