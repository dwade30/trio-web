﻿function Build-ConnectionString{
    Write-Host ""
    Write-Host "*** $($global:moduleName)" -ForegroundColor Green
    Write-Host ""

    $global:databaseName = "TRIO_$($global:customer)_$($global:env)_$($global:moduleName)"
    $global:myConnStr = "Server=$($global:serverName);Database=$($global:databaseName);uid=$($global:uid);pwd=$($global:pw);" 
    
}

function GetArchiveDatabases{
    Write-Host "Getting Archive Database List"
    return Invoke-Sqlcmd -ServerInstance "$($global:serverName)" -Database "$($global:databaseName)" -Username "$($global:uid)" -Password "$($global:pw)" -Query "SELECT DISTINCT [ArchiveType], [ArchiveID] FROM [Archives]"
}

function DoesDatabaseExist{
    return Invoke-Sqlcmd -ServerInstance "$($global:serverName)" -Username $global:uid -Password $global:pw -Query "IF EXISTS (SELECT  [name] FROM sys.databases WHERE [name] = '$($global:databaseName)') select 'true' else select 'false'"
}

function DoesTableExist{
    return Invoke-Sqlcmd -ServerInstance "$($global:serverName)" -Database "$($global:databaseName)" -Username $global:uid -Password $global:pw -Query "IF EXISTS (SELECT [name] FROM sysobjects Where [name] = '$($global:tableName)' AND xType= 'U') select 'true' else select 'false'"
}

function ErrorVerifyingDatabase{
    Write-Host "** Error verifying if the database exists" -ForegroundColor Red -BackgroundColor Yellow
    Write-Host $_ -ForegroundColor Red
    Write-Host ""
    Read-Host -Prompt "Stopping the program. Press any key"
    exit
}

function Remove-Existing-Keys{
    try
    {
       # only uncomment and use ONLY if necessary. 
       # Usually, just restoring a backup and redoing the encryption is more than enough
       # $global:moduleName | Remove-SqlColumnEncryptionKey -Name "CEK1"
       # $global:moduleName | Remove-SqlColumnMasterKey -Name "CMK1"
    }
    catch
    {
        
    }
}
    
function Setup{


    Write-Host "*** STARTING ***" -ForegroundColor Green
    Write-Host ""

    # set the TLS protocol or it will bomb on you saying it cannot connect to the web
    Write-Host "Setting TLS..."
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]'Tls11,Tls12'

    # be sure you have the right version of powershell
    Write-Host "Verifying Powershell version..."
    if (((Get-Host).Version).Major -lt 5) {
        Write-Host "Updating Powershell..."
        Update-Module -Name PowerShellGet
    }

    # NuGet
    Write-Host "Verifying NuGet version...this can take a minute..."
    Install-PackageProvider -Name NuGet -Scope CurrentUser

    # SQL Powershell Module
    Write-Host "Installing SQL Powershell module..."
    Install-Module -Name SqlServer -Force -AllowClobber
    Write-Host ""
    
    # **** the following are set as arguments sent into the script so no longer need to be requested from the user
    # **  Then get the sql server instance name
    # $global:serverName = Read-Host –Prompt "Enter the database instance name (ex. Machine123\SQLExpress)"
    # **  get the creds too
    #$global:uid = Read-Host –Prompt "Username" 
    #$securepw = Read-Host -AsSecureString –Prompt "Password" 
    #$bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securepw)
    #$value = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)
    #$global:pw = $value

    # Create cert on the local machine
    $CertName = $global:customer + ".trio-web.com"
    Write-Host ""
    Write-Host "Creating Self Signed Certificate $($CertName)..." -ForegroundColor Green
    $Cert = New-SelfSignedCertificate -DnsName $CertName -CertStoreLocation Cert:\LocalMachine\My -KeyExportPolicy Exportable -Type DocumentEncryptionCert -KeyUsage DataEncipherment -KeySpec KeyExchange
    $thumbprint = $Cert.Thumbprint
    $global:cmkPath = "Cert:\LocalMachine\My\$($thumbprint)"
    Write-Host "Certificate Path: $($global:cmkPath)"

    # Save pfx of cert to desktop
    $global:Pfx = "c:\inetpub\trioweb\$($CertName).pfx"
    $Password = "Hands0ffMyKey!" | ConvertTo-SecureString -AsPlainText -Force
    Export-PfxCertificate -cert $global:cmkPath -FilePath $global:Pfx -Password $Password

    # Using the same cert for all the databases so grab it here and prepare for making keys with it
    $global:cmkSettings = New-SqlCertificateStoreColumnMasterKeySettings -CertificateStoreLocation "LocalMachine" -Thumbprint $Cert.Thumbprint
}

function CanProcess{
    Build-ConnectionString
    # Write-Host "Connecting to $($global:myConnStr)"

    try
    {
        $databaseExists = DoesDatabaseExist
        $canProcess = $databaseExists[0]
        # Write-Host "Can Process: $($canProcess)"
        return $canProcess
    }
    catch
    {
       ErrorVerifyingDatabase
       return false
    }
}

function PrepEncryption{
    Write-Host "Processing $($global:moduleName)"  -ForegroundColor Green
    Write-Host ""
    $global:database = Get-SqlDatabase -ConnectionString $global:myConnStr
        
    # Create column master key metadata in the database, after removing any that may already exist with that name
    Write-Host "Creating encryption keys…"
    Remove-Existing-Keys
    $global:cmk = New-SqlColumnMasterKey -Name $global:cmkName -InputObject $global:database -ColumnMasterKeySettings $global:cmkSettings
    #$global:cmk | Select-Object -Property *
    #Read-Host
        
    $global:cek = New-SqlColumnEncryptionKey -ColumnMasterKey $global:cmkName -InputObject $global:database -Name $global:cekName
}

function EncryptPayroll{
    # Connect to your Payroll database 
    $global:moduleName = "Payroll"
    
    $can = CanProcess
    if ($can -eq "true")
    {
        PrepEncryption
        $ces = @()
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblEmployeeMaster.DateBirth -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblEmployeeMaster.SSN -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.ACAEmployeeDependents.DateOfBirth -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.ACAEmployeeDependents.SSN -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblACHInformation.EmployerAccount -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblBanks.CheckingAccount -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblCheckDetail.DDAccountNumber -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblCheckReturn.DDAccountNumber -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblDirectDeposit.Account -EncryptionType Deterministic -EncryptionKey CEK1      
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblMSRSNonPaid.SocialSecurityNumber -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblMSRSNonPaid.DateOfBirth -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblMSRSTempDetailReport.SocialSecurityNumber -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblMSRSTempDetailReport.DateOfBirth -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblRecipients.EFTAccount -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblTempPayProcess.DDAccountNumber -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblW2Archive.SSN -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblW2EditTable.SSN -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.tblW2Original.SSN -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.W2C.SSN -EncryptionType Deterministic -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName dbo.W2C.PrevSSN -EncryptionType Deterministic -EncryptionKey CEK1
        
        Write-Host "Doing encryption..."
        Set-SqlColumnEncryption -InputObject $global:database -ColumnEncryptionSettings $ces
        Write-Host "Done with Payroll."
    }
    else
    {
        Write-Host "Module not used" -ForegroundColor Yellow -BackgroundColor DarkGreen
    }

    Write-Host ""
}

function EncryptBudgetary{
    # Connect to your Budgetary database 
    $global:moduleName = "Budgetary"

    $can = CanProcess
    if ($can -eq "true")
    {
        PrepEncryption
        $ces = @()
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Banks.BankAccountNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.VendorMaster.BankAccountNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.VendorMaster.TaxNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        Set-SqlColumnEncryption -InputObject $global:database -ColumnEncryptionSettings $ces
        Write-Host "Done with Budgetary."
    }
    else
    {
        Write-Host "Module not used" -ForegroundColor Yellow -BackgroundColor DarkGreen
    }  
    Write-Host ""
}

function EncryptBudgetaryArchives{
    
    $global:moduleName = "SystemSettings"
    Build-ConnectionString
    
    $archives = GetArchiveDatabases
    $global:moduleName = "Budgetary"
    foreach ($db in $archives){
        $global:env = "$($db.ArchiveType)_$($db.ArchiveID)"
        if ($db.ArchiveType -eq "Archive"){
            EncryptBudgetary
        }
    }
   
    Write-Host ""
}

function EncryptUtilityBilling{
    $global:moduleName = "UtilityBilling"
    
    $can = CanProcess
    if ($can -eq "true")
    {
        PrepEncryption
        $ces = @()
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.tblAcctACH.ACHAcctNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.tblACHInformation.CompanyAccount" -EncryptionType "Deterministic" -EncryptionKey CEK1
        Set-SqlColumnEncryption -InputObject $global:database -ColumnEncryptionSettings $ces
        Write-Host "Done with Utility Billing."
    }
    else
    {
        Write-Host "Module not used" -ForegroundColor Yellow -BackgroundColor DarkGreen
    }
    Write-Host ""
}

function EncryptClerk{
    $global:moduleName = "Clerk"
    
    $can = CanProcess
    if ($can -eq "true")
    {
        PrepEncryption

        $ces = @()

        # Some Clerk tables won't exist due to being converted to SQL early on in the years, so just ignore those
        
        $global:tableName = "dbo.Births"
        $tableExists = DoesTableExist
        $can = $tableExists[0]
        if ($can -eq "true")
        {
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Births.dateofbirth" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Births.ActualDate" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Births.MothersDOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Births.FathersDOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
        }

        $global:tableName = "dbo.BirthsDelayed"
        $tableExists = DoesTableExist
        $can = $tableExists[0]
        if ($can -eq "true")
        {
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsDelayed.ChildDOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsDelayed.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsDelayed.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsDelayed.DOB3" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsDelayed.ActualDate" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsDelayed.ChildDOBDescription" -EncryptionType "Deterministic" -EncryptionKey CEK1
        }

        $global:tableName = "dbo.BirthsForeign"
        $tableExists = DoesTableExist
        $can = $tableExists[0]
        if ($can -eq "true")
        {

            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsForeign.MotherDOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsForeign.FatherDOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsForeign.ActualDate" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsForeign.ChildDOBDescription" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.BirthsForeign.ChildDOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
        }

        $global:tableName = "dbo.Deaths"
        $tableExists = DoesTableExist
        $can = $tableExists[0]
        if ($can -eq "true")
        {
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Deaths.socialsecuritynumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Deaths.DateOfBirth" -EncryptionType "Deterministic" -EncryptionKey CEK1
        }
        
        $global:tableName = "dbo.Marriages"
        $tableExists = DoesTableExist
        $can = $tableExists[0]
        if ($can -eq "true")
        {
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Marriages.GroomsDOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Marriages.BridesDOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Marriages.GroomSSN" -EncryptionType "Deterministic" -EncryptionKey CEK1
            $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Marriages.BrideSSN" -EncryptionType "Deterministic" -EncryptionKey CEK1
        }

        if ($ces -ne "") {
            Set-SqlColumnEncryption -InputObject $global:database -ColumnEncryptionSettings $ces
        }

        Write-Host "Done with Clerk."
    }
    else
    {
        Write-Host "Module not used" -ForegroundColor Yellow -BackgroundColor DarkGreen
    }
    Write-Host ""
}

function EncryptMotorVehicle{
    $global:moduleName = "MotorVehicle"
    
    $can = CanProcess
    if ($can -eq "true")
    {
        PrepEncryption
        $ces = @()
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.ActivityMaster.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.ActivityMaster.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.ActivityMaster.DOB3" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.ActivityMaster.TaxIDNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.ArchiveMaster.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.ArchiveMaster.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.ArchiveMaster.DOB3" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.ArchiveMaster.TaxIDNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.FleetMaster.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.FleetMaster.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.FleetMaster.DOB3" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.FleetMaster.TaxID" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.HeldRegistrationMaster.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.HeldRegistrationMaster.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.HeldRegistrationMaster.DOB3" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.HeldRegistrationMaster.TaxIDNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Master.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Master.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Master.DOB3" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Master.TaxIDNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.PendingActivityMaster.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.PendingActivityMaster.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.PendingActivityMaster.DOB3" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.PendingActivityMaster.TaxIDNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.tblMasterTemp.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.tblMasterTemp.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.tblMasterTemp.DOB3" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.tblMasterTemp.TaxIDNumber" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.DoubleCTA.DOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Title.DOB1" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.Title.DOB2" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.TransitPlates.DOB" -EncryptionType "Deterministic" -EncryptionKey CEK1
        $ces += New-SqlColumnEncryptionSettings -ColumnName "dbo.UseTax.PurchaserSSN" -EncryptionType "Deterministic" -EncryptionKey CEK1 
        Set-SqlColumnEncryption -InputObject $global:database -ColumnEncryptionSettings $ces
        Write-Host "Done with Motor Vehicle."
    }
    else
    {
        Write-Host "Module not used" -ForegroundColor Yellow -BackgroundColor DarkGreen
    }
    Write-Host ""
}




# **** BEGIN ****

# set it to catch errors in a try-catch
$ErrorActionPreference = 'Stop'

# setup and initialization
try
{
    # Self-elevate the script if required
    if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
     if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
      $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
      Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
      Exit
     }
    }

    # process arguments

    #   customer name
    $global:customer = $args[0]
    if ($global:customer.Length -eq 0){
        Write-Host "The customer name was not passed in as an argument. Please revise your command calling this script." -ForegroundColor Red -BackgroundColor Yellow
        Write-Host $_ -ForegroundColor Red
        Write-Host ""
        Read-Host -Prompt "Stopping the program. Press any key"
        exit
    }    
    #   data source
    $global:serverName = $args[1]
    if ($global:serverName.Length -eq 0){
        Write-Host "The datasource was not passed in as an argument. Please revise your command calling this script." -ForegroundColor Red -BackgroundColor Yellow
        Write-Host $_ -ForegroundColor Red
        Write-Host ""
        Read-Host -Prompt "Stopping the program. Press any key"
        exit
    }    
    #   username
    $global:uid = $args[2]
    if ($global:uid.Length -eq 0){
        Write-Host "The username was not passed in as an argument. Please revise your command calling this script." -ForegroundColor Red -BackgroundColor Yellow
        Write-Host $_ -ForegroundColor Red
        Write-Host ""
        Read-Host -Prompt "Stopping the program. Press any key"
        exit
    }    
    #   pw
    $global:pw = $args[3]
    if ($global:pw.Length -eq 0){
        Write-Host "The password was not passed in as an argument. Please revise your command calling this script." -ForegroundColor Red -BackgroundColor Yellow
        Write-Host $_ -ForegroundColor Red
        Write-Host ""
        Read-Host -Prompt "Stopping the program. Press any key"
        exit
    }
    

    # Do all the setup and getting info from user
    Setup
}
catch
{
    Write-Host ""
    Write-Host "** Error in setup" -ForegroundColor Red -BackgroundColor Yellow
    Write-Host $_ -ForegroundColor Red
    Write-Host ""
    Write-Host "Exiting script."
    Read-Host -Prompt "Press any key"
    exit
}

$global:cmkName = "CMK1"
$global:cekName = "CEK1"
$global:moduleName = ""
$databaseExists = "false"
$canProcess = $false
    
try
{
    # ****** PAYROLL ******
    $global:env = "Live"
    EncryptPayroll

    # ****** BUDGETARY ******
    $global:env = "Live"
    EncryptBudgetary

    # ****** BUDGETARY ARCHIVES ******
    Write-Host " *** BUDGETARY ARCHIVES *** "
    EncryptBudgetaryArchives

    # ****** UTILITY BILLING ******
    $global:env = "Live" 
    EncryptUtilityBilling

    # ****** CLERK ******
    $global:env = "Live" 
    EncryptClerk

    # ****** Motor Vehicle ******
    $global:env = "Live" 
    EncryptMotorVehicle

    # open the certificate manager
    #Certmgr

    # have them export the cert
    Write-Host "Ok, done encrypting the databases." -ForegroundColor Green
    Write-Host "Customer master key exported for you: $($global:Pfx)"
    Write-Host "Go get that file now and copy it somewhere safe like a flash drive"
    Write-Host ""
    Read-Host -Prompt "Press any key when done ..."

    # done
    # Remove-Item $global:Pfx
    Write-Host "That is it. All done. Nice work!" -ForegroundColor Green
    exit
}
catch
{
    Write-Host ""
    Write-Host "An error occurred:" -ForegroundColor Red -BackgroundColor Yellow
    Write-Host $_ -ForegroundColor Red
    Write-Host ""
    Read-Host -Prompt "Press any key to exit program"
}

