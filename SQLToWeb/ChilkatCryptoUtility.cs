﻿using Chilkat;

namespace SQLToWeb
{
    public class ChilkatCryptoUtility : ICryptoUtility
    {
        private Chilkat.Crypt2 crypt = new Crypt2();
        public ChilkatCryptoUtility(string initializationVector, string encryptionKey)
        {
            var test = crypt.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
            crypt.CryptAlgorithm = "aes";
            crypt.CipherMode = "cbc";
            crypt.KeyLength = 256;
            crypt.PaddingScheme = 0;
            crypt.EncodingMode = "base64";
            crypt.SetEncodedIV(initializationVector, "hex");
            crypt.SetEncodedKey(encryptionKey, "us-ascii");
        }
        public string Decrypt(string encryptedValue)
        {
            if (string.IsNullOrWhiteSpace(encryptedValue))
            {
                return "";
            }
            return crypt.DecryptStringENC(encryptedValue);
        }

        public string Encrypt(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "";
            }
            return crypt.EncryptStringENC(value);
        }
    }
}