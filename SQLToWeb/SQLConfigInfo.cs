﻿using System;

namespace SQLToWeb
{
    public class SQLConfigInfo
    {
        public String ServerInstance { get; set; }
        public String UserName { get; set; }
        public String Password { get; set; }
    }
}