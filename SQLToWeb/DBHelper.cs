﻿using System;
using System.Data.SqlClient;

namespace SQLToWeb
{
    public class DBHelper
    {
        public string GetTableCreationHeaderSQL(string strTableName)
        {
            string GetTableCreationHeaderSQL = "";
            string strSQL;
            strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') ";
            strSQL += "Create Table [dbo].[" + strTableName + "] (";
            strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
            GetTableCreationHeaderSQL = strSQL;
            return GetTableCreationHeaderSQL;
        }

        public string GetTablePKSql(string strTableName)
        {
            string GetTablePKSql = "";
            string strSQL;
            strSQL = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
            GetTablePKSql = strSQL;
            return GetTablePKSql;
        }

        public SqlConnection GetConnectionFromConfig(SQLConfigInfo config)
        {
            var connString = new SqlConnectionStringBuilder();
            connString.DataSource = config.ServerInstance;
            connString.Password = config.Password;
            connString.UserID = config.UserName;            
            return new SqlConnection(connString.ConnectionString);
        }
        public SqlConnection GetConnectionFromConfig(SQLConfigInfo config,string defaultDatabase)
        {
            var connString = new SqlConnectionStringBuilder();
            connString.DataSource = config.ServerInstance;
            connString.Password = config.Password;
            connString.UserID = config.UserName;
            connString.InitialCatalog = defaultDatabase;
            return new SqlConnection(connString.ConnectionString);
        }

        public bool TableExists(string tableName,string databaseName,SQLConfigInfo config)
        {
            using (var connection = GetConnectionFromConfig(config,databaseName))
            {
                connection.Open();
                var comm = new SqlCommand("if exists (select * from sys.tables where name = '" + tableName + "' and type = 'U') select cast(1 as bit) as foundit else select cast(0 as bit)  as foundit", connection);
                var tableExists = (bool)comm.ExecuteScalar();
                comm.Dispose();
                return tableExists;
            }
        }

        public bool DatabaseExists(string databaseName, SQLConfigInfo config)
        {
	        using (var connection = GetConnectionFromConfig(config, "Master"))
	        {
		        connection.Open();
		        var comm = new SqlCommand("if exists (select * from sys.sysdatabases where name = '" + databaseName + "') select cast(1 as bit) as foundit else select cast(0 as bit)  as foundit", connection);
		        var tableExists = (bool)comm.ExecuteScalar();
		        comm.Dispose();
		        return tableExists;
	        }
        }

        public bool FieldExists(string fieldName, string tableName, string databaseName, SQLConfigInfo config)
        {
            using (var connection = GetConnectionFromConfig(config, databaseName))
            {
                connection.Open();
                var comm = new SqlCommand("if exists (Select column_name from information_schema.columns where column_name = '" + fieldName + "' and table_name ='" + tableName + "') select cast (1 as bit) as foundit else select cast(0 as bit) as foundit", connection);
                var fieldExists = (bool)comm.ExecuteScalar();
                comm.Dispose();
                return fieldExists;
            }
        }

        public bool StoredProcedureExists(string procedureName, string databaseName, SQLConfigInfo config)
        {
            using (var connection = GetConnectionFromConfig(config, databaseName))
            {
                connection.Open();
                var comm = new SqlCommand("if exists (Select * from sys.objects where object_id = OBJECT_ID(N'" + procedureName + "') and type in (N'P', N'PC')) select cast (1 as bit) as foundit else select cast (0 as bit) as foundit", connection);
                var procedureExists = (bool)comm.ExecuteScalar();
                comm.Dispose();
                return procedureExists;
            }
        }

        public void Execute(string sql, string databaseName, SQLConfigInfo config)
        {
            using (var connection = GetConnectionFromConfig(config, databaseName))
            {
                connection.Open();
                var comm = new SqlCommand(sql, connection);
                comm.ExecuteNonQuery();
                comm.Dispose();               
            }
        }

        public string GetDataType(string tableName,string fieldName, string databaseName, SQLConfigInfo config)
        {
            using (var connection = GetConnectionFromConfig(config, databaseName))
            {
                connection.Open();
                var comm = new SqlCommand("select data_type from information_schema.columns where table_name = '" + tableName + "' and column_name = '" + fieldName + "'", connection);
                var dataReader = comm.ExecuteReader();
                var dataType = "";
                if (dataReader.Read())
                {
                    dataType =  dataReader["data_type"].ToString();
                }
                dataReader.Dispose();
                comm.Dispose();
                return dataType;
            }
        }
    }
}