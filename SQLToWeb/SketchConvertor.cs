﻿using System;
using System.IO;
namespace SQLToWeb
{
    public class SketchConvertor
    {
        private string sourcePath;
        private SQLConfigInfo destinationConfiguration;
        private string environmentName;
        public event EventHandler<string> MessageNotification;
        public SketchConvertor(string sourcePath, SQLConfigInfo destinationConfiguration,
            string destinationEnvironmentName)
        {
            this.sourcePath = sourcePath;
            this.destinationConfiguration = destinationConfiguration;
            environmentName = destinationEnvironmentName;
        }

        public void Import()
        {
            try
            {
                SendMessage("Importing sketches from " + sourcePath);
                Directory.GetFiles(sourcePath, "*.skt");
            }
            catch (Exception e)
            {
                SendMessage(e.Message);
                throw;
            }
        }

         private void SendMessage(string text)
        {
            if (MessageNotification != null)
            {
                MessageNotification(this, text);
            }
            System.Windows.Forms.Application.DoEvents();
        }
    }
}