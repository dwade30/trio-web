﻿namespace SQLToWeb
{
    partial class frmConversion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.panelLoggedIn = new System.Windows.Forms.Panel();
            this.btnUpgradeReceipts = new System.Windows.Forms.Button();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.btnUpdateUsers = new System.Windows.Forms.Button();
            this.chkUpgradeSketches = new System.Windows.Forms.CheckBox();
            this.cmbClient = new System.Windows.Forms.ComboBox();
            this.btnWebRefresh = new System.Windows.Forms.Button();
            this.btnSqlRefresh = new System.Windows.Forms.Button();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.btnConvert = new System.Windows.Forms.Button();
            this.cmbWebEnvironment = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbSQLEnvironment = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnWebConfiguration = new System.Windows.Forms.Button();
            this.btnSQLConfiguration = new System.Windows.Forms.Button();
            this.txtWebConfigPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSQLConfigPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelLoggedIn.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Enabled = false;
            this.btnLogin.Location = new System.Drawing.Point(20, 12);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(95, 30);
            this.btnLogin.TabIndex = 1;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Enabled = false;
            this.btnLogout.Location = new System.Drawing.Point(134, 12);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(95, 30);
            this.btnLogout.TabIndex = 2;
            this.btnLogout.Text = "Log Out";
            this.btnLogout.UseVisualStyleBackColor = true;
            // 
            // panelLoggedIn
            // 
            this.panelLoggedIn.Controls.Add(this.btnUpgradeReceipts);
            this.panelLoggedIn.Controls.Add(this.btnEncrypt);
            this.panelLoggedIn.Controls.Add(this.btnDecrypt);
            this.panelLoggedIn.Controls.Add(this.btnUpdateUsers);
            this.panelLoggedIn.Controls.Add(this.chkUpgradeSketches);
            this.panelLoggedIn.Controls.Add(this.cmbClient);
            this.panelLoggedIn.Controls.Add(this.btnWebRefresh);
            this.panelLoggedIn.Controls.Add(this.btnSqlRefresh);
            this.panelLoggedIn.Controls.Add(this.rtbOutput);
            this.panelLoggedIn.Controls.Add(this.btnConvert);
            this.panelLoggedIn.Controls.Add(this.cmbWebEnvironment);
            this.panelLoggedIn.Controls.Add(this.label4);
            this.panelLoggedIn.Controls.Add(this.cmbSQLEnvironment);
            this.panelLoggedIn.Controls.Add(this.label3);
            this.panelLoggedIn.Controls.Add(this.btnWebConfiguration);
            this.panelLoggedIn.Controls.Add(this.btnSQLConfiguration);
            this.panelLoggedIn.Controls.Add(this.txtWebConfigPath);
            this.panelLoggedIn.Controls.Add(this.label2);
            this.panelLoggedIn.Controls.Add(this.txtSQLConfigPath);
            this.panelLoggedIn.Controls.Add(this.label1);
            this.panelLoggedIn.Enabled = false;
            this.panelLoggedIn.Location = new System.Drawing.Point(3, 58);
            this.panelLoggedIn.Name = "panelLoggedIn";
            this.panelLoggedIn.Size = new System.Drawing.Size(837, 530);
            this.panelLoggedIn.TabIndex = 3;
            // 
            // btnUpgradeReceipts
            // 
            this.btnUpgradeReceipts.Location = new System.Drawing.Point(366, 203);
            this.btnUpgradeReceipts.Name = "btnUpgradeReceipts";
            this.btnUpgradeReceipts.Size = new System.Drawing.Size(128, 42);
            this.btnUpgradeReceipts.TabIndex = 31;
            this.btnUpgradeReceipts.Text = "Upgrade Receipts";
            this.btnUpgradeReceipts.UseVisualStyleBackColor = true;
            this.btnUpgradeReceipts.Click += new System.EventHandler(this.btnUpgradeReceipts_Click);
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(130, 203);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(107, 42);
            this.btnEncrypt.TabIndex = 30;
            this.btnEncrypt.Text = "Encrypt";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(15, 203);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(107, 42);
            this.btnDecrypt.TabIndex = 16;
            this.btnDecrypt.Text = "Decrypt";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click_1);
            // 
            // btnUpdateUsers
            // 
            this.btnUpdateUsers.Location = new System.Drawing.Point(245, 203);
            this.btnUpdateUsers.Name = "btnUpdateUsers";
            this.btnUpdateUsers.Size = new System.Drawing.Size(113, 42);
            this.btnUpdateUsers.TabIndex = 15;
            this.btnUpdateUsers.Text = "Update Users";
            this.btnUpdateUsers.UseVisualStyleBackColor = true;
            this.btnUpdateUsers.Click += new System.EventHandler(this.btnUpdateUsers_Click);
            // 
            // chkUpgradeSketches
            // 
            this.chkUpgradeSketches.AutoSize = true;
            this.chkUpgradeSketches.Location = new System.Drawing.Point(653, 213);
            this.chkUpgradeSketches.Name = "chkUpgradeSketches";
            this.chkUpgradeSketches.Size = new System.Drawing.Size(171, 24);
            this.chkUpgradeSketches.TabIndex = 13;
            this.chkUpgradeSketches.Text = "Upgrade sketch images";
            this.chkUpgradeSketches.UseVisualStyleBackColor = true;
            // 
            // cmbClient
            // 
            this.cmbClient.DisplayMember = "Name";
            this.cmbClient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClient.FormattingEnabled = true;
            this.cmbClient.Location = new System.Drawing.Point(189, 149);
            this.cmbClient.Name = "cmbClient";
            this.cmbClient.Size = new System.Drawing.Size(218, 28);
            this.cmbClient.TabIndex = 10;
            // 
            // btnWebRefresh
            // 
            this.btnWebRefresh.Location = new System.Drawing.Point(695, 146);
            this.btnWebRefresh.Name = "btnWebRefresh";
            this.btnWebRefresh.Size = new System.Drawing.Size(105, 32);
            this.btnWebRefresh.TabIndex = 12;
            this.btnWebRefresh.Text = "Refresh";
            this.btnWebRefresh.UseVisualStyleBackColor = true;
            // 
            // btnSqlRefresh
            // 
            this.btnSqlRefresh.Location = new System.Drawing.Point(471, 99);
            this.btnSqlRefresh.Name = "btnSqlRefresh";
            this.btnSqlRefresh.Size = new System.Drawing.Size(105, 32);
            this.btnSqlRefresh.TabIndex = 9;
            this.btnSqlRefresh.Text = "Refresh";
            this.btnSqlRefresh.UseVisualStyleBackColor = true;
            // 
            // rtbOutput
            // 
            this.rtbOutput.Location = new System.Drawing.Point(9, 255);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.Size = new System.Drawing.Size(815, 263);
            this.rtbOutput.TabIndex = 29;
            this.rtbOutput.Text = "";
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(502, 203);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(142, 42);
            this.btnConvert.TabIndex = 14;
            this.btnConvert.Text = "Import Documents";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // cmbWebEnvironment
            // 
            this.cmbWebEnvironment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWebEnvironment.FormattingEnabled = true;
            this.cmbWebEnvironment.Location = new System.Drawing.Point(413, 149);
            this.cmbWebEnvironment.Name = "cmbWebEnvironment";
            this.cmbWebEnvironment.Size = new System.Drawing.Size(254, 28);
            this.cmbWebEnvironment.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "Web Environment";
            // 
            // cmbSQLEnvironment
            // 
            this.cmbSQLEnvironment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSQLEnvironment.FormattingEnabled = true;
            this.cmbSQLEnvironment.Location = new System.Drawing.Point(189, 102);
            this.cmbSQLEnvironment.Name = "cmbSQLEnvironment";
            this.cmbSQLEnvironment.Size = new System.Drawing.Size(254, 28);
            this.cmbSQLEnvironment.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 20);
            this.label3.TabIndex = 24;
            this.label3.Text = "SQL Environment";
            // 
            // btnWebConfiguration
            // 
            this.btnWebConfiguration.Location = new System.Drawing.Point(694, 53);
            this.btnWebConfiguration.Name = "btnWebConfiguration";
            this.btnWebConfiguration.Size = new System.Drawing.Size(105, 32);
            this.btnWebConfiguration.TabIndex = 7;
            this.btnWebConfiguration.Text = "Browse";
            // 
            // btnSQLConfiguration
            // 
            this.btnSQLConfiguration.Location = new System.Drawing.Point(694, 6);
            this.btnSQLConfiguration.Name = "btnSQLConfiguration";
            this.btnSQLConfiguration.Size = new System.Drawing.Size(106, 32);
            this.btnSQLConfiguration.TabIndex = 5;
            this.btnSQLConfiguration.Text = "Browse";
            this.btnSQLConfiguration.UseVisualStyleBackColor = true;
            // 
            // txtWebConfigPath
            // 
            this.txtWebConfigPath.Location = new System.Drawing.Point(189, 56);
            this.txtWebConfigPath.Name = "txtWebConfigPath";
            this.txtWebConfigPath.Size = new System.Drawing.Size(492, 26);
            this.txtWebConfigPath.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 20);
            this.label2.TabIndex = 20;
            this.label2.Text = "Web Configuration Location";
            // 
            // txtSQLConfigPath
            // 
            this.txtSQLConfigPath.Location = new System.Drawing.Point(189, 9);
            this.txtSQLConfigPath.Name = "txtSQLConfigPath";
            this.txtSQLConfigPath.Size = new System.Drawing.Size(492, 26);
            this.txtSQLConfigPath.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "SQL Configuration Location";
            // 
            // frmConversion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 588);
            this.Controls.Add(this.panelLoggedIn);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.btnLogin);
            this.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmConversion";
            this.Text = "Convert SQL to Web";
            this.panelLoggedIn.ResumeLayout(false);
            this.panelLoggedIn.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Panel panelLoggedIn;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.Button btnUpdateUsers;
        private System.Windows.Forms.CheckBox chkUpgradeSketches;
        private System.Windows.Forms.ComboBox cmbClient;
        private System.Windows.Forms.Button btnWebRefresh;
        private System.Windows.Forms.Button btnSqlRefresh;
        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.ComboBox cmbWebEnvironment;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbSQLEnvironment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnWebConfiguration;
        private System.Windows.Forms.Button btnSQLConfiguration;
        private System.Windows.Forms.TextBox txtWebConfigPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSQLConfigPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpgradeReceipts;
        private System.Windows.Forms.Button btnEncrypt;
    }
}

