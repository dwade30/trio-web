﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.Drawing;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillLabels.
	/// </summary>
	public partial class rptBillLabels : BaseSectionReport
	{
		public rptBillLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bill Labels";
		}

		public static rptBillLabels InstancePtr
		{
			get
			{
				return (rptBillLabels)Sys.GetInstance(typeof(rptBillLabels));
			}
		}

		protected rptBillLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsBills.Dispose();
				rsMultiRecipients.Dispose();
				clsMortgageAssociations.Dispose();
				clsMortgageHolders.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		
		//clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		/// <summary>
		/// Dim rsData               As New clsDRWrapper
		/// </summary>
		clsDRWrapper clsBills = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(double, short)
		int intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		int intBlankLabelsLeftToPrint;
		bool boolReprintNewOwnerCopy;
		/// <summary>
		/// this will be set to true if there is a new owner and the recordset will not be advanced to create a copy for him
		/// </summary>
		bool boolPrintRecipientCopy;
		clsDRWrapper rsMultiRecipients = new clsDRWrapper();
		bool boolFirstBill;
		bool boolREBill;
		clsBillFormat clsTaxBillFormat = new clsBillFormat();
		clsDRWrapper clsMortgageHolders = new clsDRWrapper();
		clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
		bool boolPPBill;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = clsBills.EndOfFile();
		}

		private void GetBills(ref string strSQL)
		{
			clsBills.OpenRecordset(strSQL, "twcl0000.vb1");
		}
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intLabelStartPosition As short	OnWriteFCConvert.ToInt32(
		public void Init(clsBillFormat clsTaxFormat, int intLabelType, string strPrinterName, string strFonttoUse, bool modalDialog, int intLabelStartPosition = 1)
		{
			string strDBName = "";
			int intReturn;
			string strSQL;
			int x;
			bool boolUseFont;
			bool boolIsLaser = false;
			intBlankLabelsLeftToPrint = intLabelStartPosition - 1;
			strFont = strFonttoUse;
			this.Document.Printer.PrinterName = strPrinterName;
			clsTaxBillFormat.CopyFormat(ref clsTaxFormat);
			boolDifferentPageSize = false;
			switch (intLabelType)
			{
				case 0:
					{
						// 4013
						boolDifferentPageSize = true;
						PageSettings.Margins.Top = 0;
						PageSettings.Margins.Left = 0;
						PageSettings.Margins.Right = 0;
						//PageSettings.PaperSize = 255;
						PageSettings.PaperHeight = 1;
						PageSettings.PaperWidth = FCConvert.ToSingle(3.5);
						PageSettings.Margins.Bottom = 0;
						PrintWidth = 3.5f - 1 / 1440f;
						this.Detail.Height = 1439 / 1440f;
						boolIsLaser = false;
						break;
					}
				case 1:
					{
						// 4014
						boolDifferentPageSize = true;
						PageSettings.Margins.Top = 0;
						PageSettings.Margins.Left = 0;
						PageSettings.Margins.Right = 0;
						PageSettings.Margins.Bottom = 0;
						//PageSettings.PaperSize = 255;
						PageSettings.PaperHeight = FCConvert.ToSingle(1.5);
						PageSettings.PaperWidth = 4;
                        //FC:FINAL:MSH - replace wrong size calculations
                        //PrintWidth = 4f - 1;
                        PrintWidth = 4f - (1 / 1440f);
                        //FC:FINAL:MSH - replace wrong size calculations
                        //this.Detail.Height = 1.5f - 1;
                        this.Detail.Height = 1.5f - (1 / 1440f);
						boolIsLaser = false;
						break;
					}
				case 2:
					{
						// Avery 5260 (3 X 10)  Height = 1" Width = 2.63"
						PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
						PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
						PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
						PrintWidth = 8.5f - PageSettings.Margins.Left - PageSettings.Margins.Right;
						intLabelWidth = FCConvert.ToInt32((2.625));
						Detail.Height = 1;
						Detail.ColumnCount = 3;
						Detail.ColumnSpacing = 0.125f;
						boolIsLaser = true;
						break;
					}
				case 3:
					{
						// Avery 5261 (2 X 10)  Height = 1" Width = 4"
						PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
						PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
						PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
						PrintWidth = 8.5f - PageSettings.Margins.Left - PageSettings.Margins.Right;
						intLabelWidth = 4;
						Detail.Height = 1;
						Detail.ColumnCount = 2;
						Detail.ColumnSpacing = 0.1875f;
						boolIsLaser = true;
						break;
					}
				case 4:
					{
						// Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
						PageSettings.Margins.Top = FCConvert.ToSingle(0.8125);
						PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
						PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
						PrintWidth = 8.5f - PageSettings.Margins.Left - PageSettings.Margins.Right;
						intLabelWidth = 4;
						Detail.Height = 1.33f;
						Detail.ColumnCount = 2;
						Detail.ColumnSpacing = 0.1875f;
						boolIsLaser = true;
						break;
					}
				case 5:
					{
						// Avery 5163 (2 X 5)  Height = 2" Width = 4"
						PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
						PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
						PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
						PrintWidth = 8.5f - PageSettings.Margins.Left - PageSettings.Margins.Right;
						intLabelWidth = 4;
						Detail.Height = 2f;
						Detail.ColumnCount = 2;
						Detail.ColumnSpacing = 0.15625f;
						boolIsLaser = true;
						break;
					}
			}
			//end switch
			if (boolDifferentPageSize)
			{
				Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			if (boolIsLaser)
			{
				double dblAdjust = 0;
				// vbPorter upgrade warning: dblMargin As double	OnWrite(float, short)
				double dblMargin = 0;
				dblAdjust = Conversion.Val(modRegistry.GetRegistryKey("BLLabelLaserAdjustment"));
				if (dblAdjust != 0)
				{
					dblMargin = PageSettings.Margins.Top + dblAdjust * (240 / 1440f);
					if (dblMargin < 0)
						dblMargin = 0;
					PageSettings.Margins.Top = FCConvert.ToSingle(dblMargin);
				}
			}
			// End If
			CreateDataFields();
			if (boolREBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
			{
				boolFirstBill = true;
				clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
				clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT", "CentralData");
				if (!clsMortgageAssociations.EndOfFile())
				{
					clsMortgageAssociations.MoveFirst();
					clsMortgageAssociations.MovePrevious();
				}
			}
			else
			{
				boolFirstBill = true;
			}
			strSQL = clsTaxBillFormat.SQLStatement;
			GetBills(ref strSQL);
			if (clsBills.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			if (boolREBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
			{
				boolFirstBill = true;
				clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
				clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT", "CentralData");
				if (!clsMortgageAssociations.EndOfFile())
				{
					clsMortgageAssociations.MoveFirst();
					clsMortgageAssociations.MovePrevious();
				}
			}
			else
			{
				boolFirstBill = true;
			}
            // Me.Show , MDIParent
            frmReportViewer.InstancePtr.Init(rptObj:this, strPrinter: FCGlobal.Printer.DeviceName, intModal:1, boolAllowEmail:true, strAttachmentName: "Labels", showModal: modalDialog);
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomLabel", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
        }

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intCol;
			int intNumber;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 5;
			if (boolMort)
				intNumber = 5;
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
				NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				NewField.CanGrow = false;
				NewField.Name = "txtData" + FCConvert.ToString(intRow);
				NewField.Top = (intRow - 1) * 225 / 1440f;
				NewField.Left = 144 / 1440f;
				// one space
				NewField.Width = PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing) - 145 / 1440f;
				NewField.Height = 225 / 1440f;
				NewField.Text = string.Empty;
				NewField.MultiLine = false;
				NewField.WordWrap = true;
				if (Strings.Trim(strFont) != string.Empty)
				{
					NewField.Font = new Font(strFont, NewField.Font.Size);
				}
				Detail.Controls.Add(NewField);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			int intControl;
			int intRow;
			string[] straddr = new string[6 + 1];
			int x;
			string strOldName = "";
			string strSecOwner = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			string strCountry;
			strCountry = "";
			if (!clsBills.EndOfFile())
			{
				if (intBlankLabelsLeftToPrint <= 0)
				{
					if (FCConvert.ToString(clsBills.Get_Fields_String("Billingtype")) == "RE")
					{
						modGlobalVariables.Statics.boolRE = true;
						boolREBill = true;
						boolPP = false;
						boolPPBill = false;
					}
					else if (clsBills.Get_Fields_String("Billingtype") == "PP")
					{
						modGlobalVariables.Statics.boolRE = false;
						boolREBill = false;
						boolPP = true;
						boolPPBill = true;
					}
					else
					{
						// combined
						modGlobalVariables.Statics.boolRE = true;
						boolREBill = true;
						boolPP = true;
						boolPPBill = true;
					}
					straddr[1] = "";
					straddr[2] = "";
					straddr[3] = "";
					straddr[4] = "";
					straddr[5] = "";
					straddr[6] = "";
					string temp = "";
					if (boolPrintRecipientCopy)
					{
						straddr[1] = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("Name"));
						straddr[2] = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("address1"));
						straddr[3] = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("address2"));
						straddr[4] = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
						straddr[5] = "";
					}
					else if (!boolFirstBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
					{
						clsMortgageHolders.FindFirstRecord("ID", clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"));
						if (!clsMortgageHolders.NoMatch)
						{
							straddr[1] = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							straddr[2] = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
							straddr[3] = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
							straddr[5] = Strings.Trim(clsMortgageHolders.Get_Fields_String("city") + " " + clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
							straddr[4] = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
						}
						else
						{
							straddr[1] = "Mortgage Holder Not Found";
							straddr[2] = "";
							straddr[3] = "";
							straddr[4] = "";
							straddr[5] = "";
						}
					}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (boolReprintNewOwnerCopy && modMain.HasNewOwner_28439(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3, ref strCountry, ref temp))
					{
						straddr[3] = strAddr1;
						straddr[4] = strAddr2;
						straddr[5] = strAddr3;
						straddr[6] = strCountry;
						straddr[1] = strOldName;
						straddr[2] = strSecOwner;
					}
					else
					{
						straddr[3] = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
						straddr[4] = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
						straddr[5] = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
						straddr[1] = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						straddr[2] = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name2")));
					}
					// condense
					if (Strings.Trim(straddr[4]) == string.Empty)
					{
						straddr[4] = straddr[5];
						straddr[5] = straddr[6];
						straddr[6] = "";
					}
					if (Strings.Trim(straddr[3]) == string.Empty)
					{
						straddr[3] = straddr[4];
						if (boolMort)
						{
							straddr[4] = straddr[5];
							straddr[5] = straddr[6];
							straddr[6] = "";
						}
						else
						{
							straddr[4] = "";
						}
					}
					if (Strings.Trim(straddr[2]) == string.Empty)
					{
						straddr[2] = straddr[3];
						straddr[3] = straddr[4];
						if (boolMort)
						{
							straddr[4] = straddr[5];
							straddr[5] = straddr[6];
							straddr[6] = "";
						}
						else
						{
							straddr[4] = "";
						}
					}
					if (Strings.Trim(straddr[1]) == string.Empty)
					{
						straddr[1] = straddr[2];
						straddr[2] = straddr[3];
						straddr[3] = straddr[4];
						if (boolMort)
						{
							straddr[4] = straddr[5];
							straddr[5] = straddr[6];
							straddr[6] = "";
						}
						else
						{
							straddr[4] = "";
						}
					}
					for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
					{
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
						{
							intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
							switch (intRow)
							{
								case 1:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[1];
										break;
									}
								case 2:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[2];
										break;
									}
								case 3:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[3];
										break;
									}
								case 4:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[4];
										break;
									}
								case 5:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[5];
										break;
									}
							}
							//end switch
						}
					}
					// intControl
					// 
					// rsData.MoveNext
					if (modGlobalVariables.Statics.boolRE)
					{
						if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
						{
							boolReprintNewOwnerCopy = false;
							// if the copy for a new owner needs to be sent, then check it here
							if (clsTaxBillFormat.PrintCopyForNewOwner)
							{
								// if this is the main bill, only check for the new owner on the mail bill
								if (boolFirstBill)
								{
									// If Month(Date) < 4 Then                     'if the month is before april then use last years commitment date
									// dtLastDate = CDate("04/01/" & Year(Date) - 1)
									// Else
									// dtLastDate = CDate("04/01/" & Year(Date))
									// End If
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									if (modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1")))
									{
										// if there has been a new owner
										// If NewOwner(clsBills.Fields("Account"), dtLastDate) Then     'if there has been a new owner
										boolReprintNewOwnerCopy = true;
										// End If
									}
								}
							}
						}
						else if (!boolPrintRecipientCopy && boolFirstBill)
						{
							// turn off the copy so that the record can advance
							boolReprintNewOwnerCopy = false;
						}
						if (!boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
						{
							if (clsTaxBillFormat.PrintCopyForMultiOwners)
							{
								if (boolFirstBill)
								{
									rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
									if (!rsMultiRecipients.EndOfFile())
									{
										boolPrintRecipientCopy = true;
									}
								}
							}
						}
						else if (boolPrintRecipientCopy && boolFirstBill)
						{
							rsMultiRecipients.MoveNext();
							if (rsMultiRecipients.EndOfFile())
							{
								boolPrintRecipientCopy = false;
							}
						}
						if (clsTaxBillFormat.PrintCopyForMortgageHolders && !boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
						{
							if (!clsMortgageAssociations.EndOfFile())
							{
								if (boolFirstBill)
								{
									// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
									clsMortgageAssociations.FindFirstRecord("account", clsBills.Get_Fields("account"));
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
									clsMortgageAssociations.FindNextRecord("account", clsBills.Get_Fields("account"));
								}
								// Call clsMortgageAssociations.FindNextRecord("account", clsBills.Fields("ac"))
								if (clsMortgageAssociations.NoMatch)
								{
									boolFirstBill = true;
									if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
										clsBills.MoveNext();
								}
								else
								{
									boolFirstBill = false;
								}
								// End If
							}
							else
							{
								boolFirstBill = true;
								if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
									clsBills.MoveNext();
							}
						}
						else
						{
							if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
								clsBills.MoveNext();
						}
					}
					else
					{
						if (!boolPrintRecipientCopy)
						{
							if (clsTaxBillFormat.PrintCopyForMultiOwners)
							{
								rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
								if (!rsMultiRecipients.EndOfFile())
								{
									boolPrintRecipientCopy = true;
								}
							}
						}
						else
						{
							rsMultiRecipients.MoveNext();
							if (rsMultiRecipients.EndOfFile())
							{
								boolPrintRecipientCopy = false;
							}
						}
						if (!boolPrintRecipientCopy)
						{
							clsBills.MoveNext();
						}
					}
				}
				else
				{
					intBlankLabelsLeftToPrint -= 1;
				}
			}
		}

		
	}
}
