//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREPPAssociation.
	/// </summary>
	public class rptREPPAssociation : fecherFoundation.FCForm
	{
		// nObj = 1
		//   0	rptREPPAssociation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsre = new clsDRWrapper();
		clsDRWrapper clsPP = new clsDRWrapper();

		private void ActiveReport_FetchData(ref bool EOF)
		{
			EOF = clsre.EndOfFile();
		}

		public void Init(ref bool boolNameOrder)
		{
			string strSQL = "";
			string strOrder = "";
			if (boolNameOrder)
			{
				strOrder = " order by FullNameLF ";
			}
			else
			{
				strOrder = " order by master.rsaccount ";
			}
			clsre.OpenRecordset("select * from master inner join " + clsre.CurrentPrefix + "CentralData.dbo.moduleassociation on (moduleassociation.remasteracct = master.rsaccount) CROSS APPLY " + clsre.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(master.OwnerPartyID) as p where moduleassociation.module = 'PP' and master.rsdeleted <> 1 and master.rscard = 1" + strOrder, "twre0000.vb1");
			clsPP.OpenRecordset("select * from ppmaster order by account", "twpp0000.vb1");
			// Me.Show , MDIParent
			;
		}

		private void ActiveReport_ReportStart()
		{
			modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			txtDate.Text = DateTime.Today;
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm AMPM");
		}

		private void Detail_Format()
		{
			if (!clsre.EndOfFile())
			{
				txtAccount.Text = clsre.Get_Fields_Int32("rsaccount");
				txtName.Text = clsre.Get_Fields_String("FullNameLF");
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtPPAccount.Text = clsre.Get_Fields("account");
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				clsPP.FindFirstRecord("account", clsre.Get_Fields("account"));
				if (clsPP.NoMatch)
				{
					txtPPName.Text = "Error PP record not found.";
				}
				else
				{
					txtPPName.Text = clsPP.Get_Fields_String("name");
				}
				clsre.MoveNext();
			}
		}

		private void rptREPPAssociation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptREPPAssociation properties;
			//rptREPPAssociation.Caption	= "Mortgage Holder List";
			//rptREPPAssociation.Icon	= "rptREPP.dsx":0000";
			//rptREPPAssociation.Left	= 0;
			//rptREPPAssociation.Top	= 0;
			//rptREPPAssociation.Width	= 12960;
			//rptREPPAssociation.Height	= 7980;
			//rptREPPAssociation.StartUpPosition	= 3;
			//rptREPPAssociation.SectionData	= "rptREPP.dsx":058A;
			//End Unmaped Properties
		}
	}
}
