﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmChooseImage.
	/// </summary>
	public partial class frmChooseImage : BaseForm
	{
		public frmChooseImage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseImage InstancePtr
		{
			get
			{
				return (frmChooseImage)Sys.GetInstance(typeof(frmChooseImage));
			}
		}

		protected frmChooseImage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strPath = "";
		string strFileName = "";
		//FileSystemObject fso = new FileSystemObject();
		DriveInfo drv;

		private void chkUseSeal_CheckedChanged(object sender, System.EventArgs e)
		{
			string strTempPath = "";
			if (chkUseSeal.CheckState == Wisej.Web.CheckState.Checked)
			{
				//framOptions.Visible = false;
				upload1.Visible = false;
				strTempPath = FCFileSystem.CurDir();
				if (Strings.Right(strTempPath, 1) != "\\")
				{
					strTempPath += "\\";
				}
				if (File.Exists(strTempPath + "TownSeal.Pic"))
				{
					Image1.Image = FCUtils.LoadPicture(strTempPath + "TownSeal.Pic");
				}
				else
				{
					Image1.Image = null;
					MessageBox.Show("Could not find the town seal file", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				//framOptions.Visible = true;
				upload1.Visible = true;
				//FC:FINAL:MSH - restore image after changing state of checkbox(as in original)
				if (!string.IsNullOrEmpty(strFileName) && !string.IsNullOrEmpty(strPath) && File.Exists(strPath + strFileName))
				{
					Image1.Image = FCUtils.LoadPicture(Path.Combine(strPath, strFileName));
				}
				//File1_Click();
			}
		}
		//private void Dir1_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//    File1.Path = Dir1.Path;
		//}
		//private void Drive1_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//    drv = new DriveInfo((Directory.GetDirectoryRoot(Drive1.Drive)));
		//    if (drv.IsReady)
		//    {
		//        Dir1.Path = Drive1.Drive;
		//    }
		//}
		//private void File1_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//    if (File1.FileName != string.Empty)
		//    {
		//        strFileName = File1.FileName;
		//        strPath = File1.Path;
		//        if (strPath.Length > 0)
		//        {
		//            if (Strings.Right(strPath, 1) != "\\" && Strings.Right(strPath, 1) != "/")
		//                strPath += "\\";
		//        }
		//        Image1.Image = FCUtils.LoadPicture(strPath + strFileName);
		//    }
		//}
		//public void File1_Click()
		//{
		//    File1_SelectedIndexChanged(File1, new System.EventArgs());
		//}
		private void frmChooseImage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmChooseImage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseImage properties;
			//frmChooseImage.ScaleWidth	= 9045;
			//frmChooseImage.ScaleHeight	= 7350;
			//frmChooseImage.LinkTopic	= "Form1";
			//End Unmaped Properties
			clsDRWrapper clsTemp = new clsDRWrapper();
			int x;
			string strTemp = "";
			clsTemp.OpenRecordset("select * from billsetup", "twbl0000.vb1");
			if (!clsTemp.EndOfFile())
			{
				if (!FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("showpicture")))
				{
					cmbShow.SelectedIndex = 1;
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicture"))) != string.Empty)
					{
						strPath = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicturepath")));
						strFileName = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicture")));
						if (File.Exists(strPath + strFileName))
						{
							Image1.Image = FCUtils.LoadPicture(strPath + strFileName);
							//Drive1.Drive = Directory.GetDirectoryRoot(strPath);
							//Dir1.Path = Directory.GetParent(strPath + strFileName).FullName;
							// File1.FileName = strFileName
							//if (File1.Items.Count > 0)
							//{
							//    for (x = 0; x <= File1.Items.Count - 1; x++)
							//    {
							//        if (Strings.UCase(File1.Items[x].ToString()) == Strings.UCase(strFileName))
							//        {
							//            File1.SelectedIndex = x;
							//            break;
							//        }
							//    }
							//    // x
							//}
							if (Strings.UCase(strFileName) == "TOWNSEAL.PIC")
							{
								strTemp = FCFileSystem.CurDir();
								if (Strings.Right(strTemp, 1) != "\\")
									strTemp += "\\";
								if (Strings.UCase(strPath) == Strings.UCase(strTemp))
								{
									chkUseSeal.CheckState = Wisej.Web.CheckState.Checked;
								}
								else
								{
									chkUseSeal.CheckState = Wisej.Web.CheckState.Unchecked;
								}
							}
						}
						else
						{
							strFileName = "";
						}
					}
					else
					{
						strPath = "";
						strFileName = "";
					}
				}
			}
			else
			{
				strPath = "";
				strFileName = "";
			}
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			Label1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//Label2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//Label3.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//Label4.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "In Exit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				clsTemp.OpenRecordset("select * from billsetup", "twbl0000.vb1");
				if (clsTemp.EndOfFile())
				{
					clsTemp.AddNew();
				}
				else
				{
					clsTemp.Edit();
				}
				if (cmbShow.SelectedIndex == 0)
				{
					clsTemp.Set_Fields("showpicture", true);
					if (!(chkUseSeal.CheckState == Wisej.Web.CheckState.Checked))
					{
						if (strFileName != string.Empty)
						{
							clsTemp.Set_Fields("billpicture", strFileName);
							clsTemp.Set_Fields("billpicturepath", strPath);
						}
					}
					else
					{
						if (File.Exists("TownSeal.pic"))
						{
							clsTemp.Set_Fields("billpicture", "TownSeal.pic");
							if (Strings.Right(FCFileSystem.CurDir(), 1) != "\\")
							{
								clsTemp.Set_Fields("billpicturepath", FCFileSystem.CurDir() + "\\");
							}
							else
							{
								clsTemp.Set_Fields("billpicturepath", FCFileSystem.CurDir());
							}
						}
					}
				}
				else
				{
					clsTemp.Set_Fields("showpicture", false);
				}
				clsTemp.Update();
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "In Exit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdSaveAndExit_Click(object sender, EventArgs e)
		{
			mnuSaveExit_Click(mnuSaveExit, EventArgs.Empty);
		}

		private void upload1_Uploaded(object sender, UploadedEventArgs e)
		{
			if (e.Files != null && e.Files.Count > 0)
			{
				System.Web.HttpPostedFile file = e.Files[0];
				strPath = FCFileSystem.Statics.UserDataFolder + "\\";
				if (!Directory.Exists(strPath))
				{
					Directory.CreateDirectory(strPath);
				}
				strFileName = file.FileName;
				using (FileStream fileStream = new FileStream(Path.Combine(strPath, strFileName), FileMode.Create, FileAccess.Write))
				{
					file.InputStream.CopyTo(fileStream);
				}
				Image1.Image = FCUtils.LoadPicture(Path.Combine(strPath, strFileName));
			}
		}
	}
}
