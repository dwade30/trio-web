﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPrintorPreview.
	/// </summary>
	partial class frmPrintorPreview : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCFrame framFile;
		public fecherFoundation.FCTextBox txtFileName;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtLimit;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintorPreview));
			this.cmbPrint = new fecherFoundation.FCComboBox();
			this.lblPrint = new fecherFoundation.FCLabel();
			this.framFile = new fecherFoundation.FCFrame();
			this.txtFileName = new fecherFoundation.FCTextBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtLimit = new fecherFoundation.FCTextBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framFile)).BeginInit();
			this.framFile.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 263);
			this.BottomPanel.Size = new System.Drawing.Size(346, 108);
			this.BottomPanel.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbPrint);
			this.ClientArea.Controls.Add(this.lblPrint);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.framFile);
			this.ClientArea.Size = new System.Drawing.Size(346, 203);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(346, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(62, 30);
			this.HeaderText.Text = "Print";
			// 
			// cmbPrint
			// 
			this.cmbPrint.AutoSize = false;
			this.cmbPrint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPrint.FormattingEnabled = true;
			this.cmbPrint.Items.AddRange(new object[] {
				"Print All",
				"Print Preview",
				"Create as PDF"
			});
			this.cmbPrint.Location = new System.Drawing.Point(130, 30);
			this.cmbPrint.Name = "cmbPrint";
			this.cmbPrint.Size = new System.Drawing.Size(185, 40);
			this.cmbPrint.TabIndex = 1;
			this.cmbPrint.Text = "Print All";
			this.cmbPrint.SelectedIndexChanged += new System.EventHandler(this.cmbPrint_SelectedIndexChanged);
			// 
			// lblPrint
			// 
			this.lblPrint.AutoSize = true;
			this.lblPrint.Location = new System.Drawing.Point(30, 44);
			this.lblPrint.Name = "lblPrint";
			this.lblPrint.Size = new System.Drawing.Size(45, 15);
			this.lblPrint.TabIndex = 0;
			this.lblPrint.Text = "PRINT";
			// 
			// framFile
			// 
			this.framFile.Controls.Add(this.txtFileName);
			this.framFile.Location = new System.Drawing.Point(30, 90);
			this.framFile.Name = "framFile";
			this.framFile.Size = new System.Drawing.Size(285, 90);
			this.framFile.TabIndex = 2;
			this.framFile.Text = "File Name";
			this.framFile.Visible = false;
			// 
			// txtFileName
			// 
			this.txtFileName.AutoSize = false;
			this.txtFileName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFileName.LinkItem = null;
			this.txtFileName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFileName.LinkTopic = null;
			this.txtFileName.Location = new System.Drawing.Point(20, 30);
			this.txtFileName.Name = "txtFileName";
			this.txtFileName.Size = new System.Drawing.Size(245, 40);
			this.txtFileName.TabIndex = 0;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtLimit);
			this.Frame1.Location = new System.Drawing.Point(30, 90);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(140, 90);
			this.Frame1.TabIndex = 2;
			this.Frame1.Text = "Preview Limit";
			this.Frame1.Visible = false;
			// 
			// txtLimit
			// 
			this.txtLimit.AutoSize = false;
			this.txtLimit.BackColor = System.Drawing.SystemColors.Window;
			this.txtLimit.LinkItem = null;
			this.txtLimit.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLimit.LinkTopic = null;
			this.txtLimit.Location = new System.Drawing.Point(20, 30);
			this.txtLimit.MaxLength = 4;
			this.txtLimit.Name = "txtLimit";
			this.txtLimit.Size = new System.Drawing.Size(100, 40);
			this.txtLimit.TabIndex = 3;
			this.txtLimit.Text = "10";
			this.txtLimit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Save & Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(83, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(180, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Save & Continue";
			this.cmdContinue.Click += new System.EventHandler(this.cmdContinue_Click);
			// 
			// frmPrintorPreview
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(346, 371);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPrintorPreview";
			this.Text = "Print";
			this.Load += new System.EventHandler(this.frmPrintorPreview_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintorPreview_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framFile)).EndInit();
			this.framFile.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
