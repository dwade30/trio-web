﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptOutprintSummary.
	/// </summary>
	public partial class rptOutprintSummary : BaseSectionReport
	{
		public rptOutprintSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outprint Summary";
		}

		public static rptOutprintSummary InstancePtr
		{
			get
			{
				return (rptOutprintSummary)Sys.GetInstance(typeof(rptOutprintSummary));
			}
		}

		protected rptOutprintSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOutprintSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public void Init(ref double dblOrRE, ref double dblOrPP, ref double dblREPrePay, ref double dblPPPrePay, ref double dblRENet, ref double dblPPNet, ref double dblREAssess, ref double dblPPAssess, ref string strMessage, ref int lngNumNews, ref int lngNumMHs, ref double dblTotalCopiesTax, ref int lngNumIPs)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtNetRE.Text = Strings.Format(dblREAssess, "#,###,###,##0");
			txtNetPP.Text = Strings.Format(dblPPAssess, "#,###,###,##0");
			txtNet.Text = Strings.Format(dblREAssess + dblPPAssess, "#,###,###,##0");
			txtOriginalPPTax.Text = Strings.Format(dblOrPP, "#,###,###,##0.00");
			txtOriginalRETax.Text = Strings.Format(dblOrRE, "#,###,###,##0.00");
			txtOriginalTax.Text = Strings.Format(dblOrPP + dblOrRE, "#,###,###,##0.00");
			txtREPrePay.Text = Strings.Format(dblREPrePay, "#,###,###,##0.00");
			txtPPPrePay.Text = Strings.Format(dblPPPrePay, "#,###,###,##0.00");
			txtPrePay.Text = Strings.Format(dblPPPrePay + dblREPrePay, "#,###,###,##0.00");
			txtTotalREDue.Text = Strings.Format(dblRENet, "#,###,###,##0.00");
			txtTotalPPDue.Text = Strings.Format(dblPPNet, "#,###,###,##0.00");
			txtTotalDue.Text = Strings.Format(dblRENet + dblPPNet, "#,###,###,##0.00");
			txtInfo.Text = strMessage;
			if (lngNumMHs == 0 && lngNumNews == 0 && lngNumIPs == 0)
			{
				txtMortgageHolders.Visible = false;
				txtNewOwners.Visible = false;
				txtTotalCopiesDue.Visible = false;
				txtTotalCopiesOrigDue.Visible = false;
				Label8.Visible = false;
				Label9.Visible = false;
				Label10.Visible = false;
				Label11.Visible = false;
				Label12.Visible = false;
				txtInterestedParties.Visible = false;
			}
			else
			{
				txtMortgageHolders.Text = Strings.Format(lngNumMHs, "#,###,###,##0");
				txtNewOwners.Text = Strings.Format(lngNumNews, "#,###,###,##0");
				txtTotalCopiesDue.Text = Strings.Format(dblTotalCopiesTax, "#,###,###,##0.00");
				txtTotalCopiesOrigDue.Text = Strings.Format(dblTotalCopiesTax + dblRENet + dblPPNet, "#,###,###,##0.00");
				txtInterestedParties.Text = Strings.Format(lngNumIPs, "#,###,###,##0");
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "OutprintSummary");
		}

		private void rptOutprintSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptOutprintSummary properties;
			//rptOutprintSummary.Caption	= "Outprint Summary";
			//rptOutprintSummary.Icon	= "rptOutprintSummary.dsx":0000";
			//rptOutprintSummary.Left	= 0;
			//rptOutprintSummary.Top	= 0;
			//rptOutprintSummary.Width	= 19080;
			//rptOutprintSummary.Height	= 14850;
			//rptOutprintSummary.StartUpPosition	= 3;
			//rptOutprintSummary.SectionData	= "rptOutprintSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
