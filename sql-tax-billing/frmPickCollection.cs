﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPickCollection.
	/// </summary>
	public partial class frmPickCollection : BaseForm
	{
		public frmPickCollection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmPickCollection_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuContinue_Click();
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmPickCollection_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPickCollection properties;
			//frmPickCollection.ScaleWidth	= 7425;
			//frmPickCollection.ScaleHeight	= 4290;
			//frmPickCollection.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			Label1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			Label2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			int x;
			// loop counter
			int NumFonts;
			// number of fonts this printer has
			bool boolUseFont;
			// did we find a printer font?
			string strFont = "";
			// font name
			string strPrinterName;
			// devicename
			int intWhichReport = 0;
			// reg,landscape, or wide
			int intCPI = 0;
			// how many cpi we are looking for
			double dblDiscount = 0;
			int intOrder = 0;
			// make sure we have valid data before continuing
			if (Strings.Trim(txtYear.Text).Length != 4)
			{
				MessageBox.Show("You must enter a 4 digit year to continue", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				if (Conversion.Val(Strings.Trim(txtYear.Text)) < 1980 || (Conversion.Val(Strings.Trim(txtYear.Text)) > (DateTime.Now.Year + 1)))
				{
					MessageBox.Show("You must enter a year that is in a valid range.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbOptions.SelectedIndex == 2)
			{
				dblDiscount = Conversion.Val(txtDiscount.Text);
			}
			else
			{
				dblDiscount = 0;
			}
			if (cmbOptions.SelectedIndex == 0)
			{
				intOrder = 1;
			}
			else if (cmbOrder.SelectedIndex == 1)
			{
				intOrder = 2;
			}
			else
			{
				intOrder = 3;
			}
			// make them choose the printer so we can set the correct printer font
			//FC:FINAL:DDU:#i1563 - initialize CommonDialog1
			//MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
			//MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
			strPrinterName = FCGlobal.Printer.DeviceName;
			NumFonts = FCGlobal.Printer.Fonts.Count;
			boolUseFont = false;
			if (cmbFormat.SelectedIndex == 0)
			{
				// regular
				intWhichReport = 1;
			}
			else if (cmbFormat.SelectedIndex == 1)
			{
				// landscape
				intWhichReport = 2;
			}
			else
			{
				// wide printer
				intWhichReport = 3;
			}
			if (intWhichReport == 1)
			{
				if (cmbOptions.SelectedIndex == 0)
				{
					// get a 10 cpi printer font if possible
					for (x = 0; x <= NumFonts - 1; x++)
					{
						strFont = FCGlobal.Printer.Fonts[x];
						if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
						{
							strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
							if (Strings.Right(strFont, 2) == "10" || Strings.Right(strFont, 3) == "10 ")
							{
								boolUseFont = true;
								strFont = FCGlobal.Printer.Fonts[x];
								break;
							}
						}
					}
					// x
					// which commitment book are we printing?
					if (cmbCollection.SelectedIndex == 0)
					{
						rptRECollectionSimple.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, dblDiscount, intOrder);
					}
					else
					{
						rptPPCollectionSimple.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, dblDiscount, intOrder);
					}
				}
				else
				{
					// get a 17 cpi printer font if possible
					for (x = 0; x <= NumFonts - 1; x++)
					{
						strFont = FCGlobal.Printer.Fonts[x];
						if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
						{
							strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
							if (Strings.Right(strFont, 2) == "17" || Strings.Right(strFont, 3) == "17 ")
							{
								boolUseFont = true;
								strFont = FCGlobal.Printer.Fonts[x];
								break;
							}
						}
					}
					// x
					// which commitment book are we printing?
					if (cmbCollection.SelectedIndex == 0)
					{
						rptRECollectionList.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, dblDiscount, intOrder);
					}
					else
					{
						rptPPCollectionList.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, dblDiscount, intOrder);
					}
				}
			}
			else
			{
				// look for a 12 cpi font
				if (intWhichReport == 2)
				{
					intCPI = 12;
				}
				else
				{
					intCPI = 12;
				}
				for (x = 0; x <= NumFonts - 1; x++)
				{
					strFont = FCGlobal.Printer.Fonts[x];
					if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
					{
						strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
						if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
						{
							boolUseFont = true;
							strFont = FCGlobal.Printer.Fonts[x];
							break;
						}
					}
				}
				// x
				if (cmbCollection.SelectedIndex == 0)
				{
					rptRECollectionWide.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, intWhichReport, dblDiscount, intOrder);
				}
				else
				{
					rptPPCollectionWide.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, intWhichReport, dblDiscount, intOrder);
				}
			}
			Close();
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void optCollection_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 1)
			{
				if (cmbOrder.SelectedIndex == 2)
				{
					cmbOrder.SelectedIndex = 0;
				}
				if (cmbOrder.Items.Contains("Map / Lot"))
				{
					cmbOrder.Items.Remove("Map / Lot");
				}
			}
			else
			{
				if (!cmbOrder.Items.Contains("Map / Lot"))
				{
					cmbOrder.Items.Insert(2, "Map / Lot");
				}
			}
		}

		private void optCollection_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbCollection.SelectedIndex);
			optCollection_CheckedChanged(index, sender, e);
		}

		private void optOptions_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				cmbFormat.SelectedIndex = 0;
				if (cmbFormat.Items.Contains("8.5 X 11 Landscape"))
				{
					cmbFormat.Items.Remove("8.5 X 11 Landscape");
				}
				if (cmbFormat.Items.Contains("Wide Printer"))
				{
					cmbFormat.Items.Remove("Wide Printer");
				}
			}
			else
			{
				if (!cmbFormat.Items.Contains("8.5 X 11 Landscape"))
				{
					cmbFormat.Items.Insert(1, "8.5 X 11 Landscape");
				}
				if (!cmbFormat.Items.Contains("Wide Printer"))
				{
					cmbFormat.Items.Insert(2, "Wide Printer");
				}
			}
		}

		private void optOptions_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbOptions.SelectedIndex);
			optOptions_CheckedChanged(index, sender, e);
		}

		private void cmbCollection_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbCollection.SelectedIndex == 0)
			{
				optCollection_CheckedChanged(sender, e);
			}
			else if (cmbCollection.SelectedIndex == 1)
			{
				optCollection_CheckedChanged(sender, e);
			}
		}

		private void cmbOptions_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbOptions.SelectedIndex == 0)
			{
				optOptions_CheckedChanged(sender, e);
			}
			else if (cmbOptions.SelectedIndex == 1)
			{
				optOptions_CheckedChanged(sender, e);
			}
			else if (cmbOptions.SelectedIndex == 2)
			{
				optOptions_CheckedChanged(sender, e);
			}
		}
	}
}
