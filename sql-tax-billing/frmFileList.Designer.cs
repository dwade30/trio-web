//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmFileList.
	/// </summary>
	partial class frmFileList : BaseForm
	{
		public fecherFoundation.FCFileListBox File1;
		public fecherFoundation.FCDirListBox Dir1;
		public fecherFoundation.FCDriveListBox Drive1;
		public fecherFoundation.FCLabel lblFile;
		public fecherFoundation.FCLabel lblDirectory;
		public fecherFoundation.FCLabel lblDrive;
		public fecherFoundation.FCLabel lblMessage;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFileList));
			this.File1 = new fecherFoundation.FCFileListBox();
			this.Dir1 = new fecherFoundation.FCDirListBox();
			this.Drive1 = new fecherFoundation.FCDriveListBox();
			this.lblFile = new fecherFoundation.FCLabel();
			this.lblDirectory = new fecherFoundation.FCLabel();
			this.lblDrive = new fecherFoundation.FCLabel();
			this.lblMessage = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 573);
			this.BottomPanel.Size = new System.Drawing.Size(645, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.File1);
			this.ClientArea.Controls.Add(this.Dir1);
			this.ClientArea.Controls.Add(this.Drive1);
			this.ClientArea.Controls.Add(this.lblFile);
			this.ClientArea.Controls.Add(this.lblDirectory);
			this.ClientArea.Controls.Add(this.lblDrive);
			this.ClientArea.Controls.Add(this.lblMessage);
			this.ClientArea.Size = new System.Drawing.Size(645, 513);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(645, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(52, 30);
			this.HeaderText.Text = "File";
			// 
			// File1
			// 
			this.File1.Location = new System.Drawing.Point(337, 286);
			this.File1.Name = "File1";
			this.File1.Size = new System.Drawing.Size(267, 256);
			this.File1.TabIndex = 6;
			// 
			// Dir1
			// 
			this.Dir1.Location = new System.Drawing.Point(30, 286);
			this.Dir1.Name = "Dir1";
			this.Dir1.Size = new System.Drawing.Size(267, 256);
			this.Dir1.TabIndex = 4;
			this.Dir1.SelectedIndexChanged += new System.EventHandler(this.Dir1_SelectedIndexChanged);
			// 
			// Drive1
			// 
			this.Drive1.Location = new System.Drawing.Point(30, 154);
			this.Drive1.Name = "Drive1";
			this.Drive1.Size = new System.Drawing.Size(267, 40);
			this.Drive1.TabIndex = 2;
			this.Drive1.SelectedIndexChanged += new System.EventHandler(this.Drive1_SelectedIndexChanged);
			// 
			// lblFile
			// 
			this.lblFile.Location = new System.Drawing.Point(337, 216);
			this.lblFile.Name = "lblFile";
			this.lblFile.Size = new System.Drawing.Size(271, 48);
			this.lblFile.TabIndex = 5;
			this.lblFile.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblDirectory
			// 
			this.lblDirectory.Location = new System.Drawing.Point(30, 216);
			this.lblDirectory.Name = "lblDirectory";
			this.lblDirectory.Size = new System.Drawing.Size(271, 48);
			this.lblDirectory.TabIndex = 3;
			this.lblDirectory.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblDrive
			// 
			this.lblDrive.Location = new System.Drawing.Point(30, 100);
			this.lblDrive.Name = "lblDrive";
			this.lblDrive.Size = new System.Drawing.Size(586, 32);
			this.lblDrive.TabIndex = 1;
			this.lblDrive.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblMessage
			// 
			this.lblMessage.Location = new System.Drawing.Point(30, 30);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(584, 48);
			this.lblMessage.TabIndex = 0;
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(205, 44);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(125, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.cmdContinue_Click);
			// 
			// frmFileList
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(645, 681);
			this.ControlBox = false;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmFileList";
			this.Text = " ";
			this.Load += new System.EventHandler(this.frmFileList_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmFileList_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}