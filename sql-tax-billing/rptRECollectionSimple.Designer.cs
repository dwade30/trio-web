﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptRECollectionSimple.
	/// </summary>
	partial class rptRECollectionSimple
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRECollectionSimple));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtTotal,
				this.txtAddress1,
				this.txtAddress2,
				this.txtMailingAddress3,
				this.txtAddress3,
				this.Field10,
				this.txtLocation,
				this.txtMapLot,
				this.txtTax,
				this.txtDiscount,
				this.lblDiscount,
				this.txtBookPage
			});
			this.Detail.Height = 1.520833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.Visible = false;
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.txtTotalSub,
				this.txtTaxSub,
				this.Label6,
				this.Label7
			});
			this.ReportFooter.Height = 0.46875F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.txtDate,
				this.lblMuniname,
				this.lblTitle,
				this.Label9,
				this.Field11,
				this.Label13,
				this.Label14,
				this.txtTime
			});
			this.PageHeader.Height = 0.53125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.CanGrow = false;
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtTotTot,
				this.txtTaxTot
			});
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			this.PageFooter.Visible = false;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.15625F;
			this.txtPage.Left = 5.875F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-size: 10pt; text-align: right";
			this.txtPage.Tag = "textbox";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 1.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.15625F;
			this.txtDate.Left = 6.625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-size: 10pt; text-align: right";
			this.txtDate.Tag = "textbox";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 0.8125F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.15625F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "font-size: 10pt; text-align: left";
			this.lblMuniname.Tag = "textbox";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.4375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
            this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Tag = "bold";
			this.lblTitle.Text = "Real Estate Collection List - ";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Account";
			this.Label9.Top = 0.34375F;
			this.Label9.Width = 0.6875F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 0.75F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-weight: bold";
			this.Field11.Tag = "bold";
			this.Field11.Text = "Name & Address";
			this.Field11.Top = 0.34375F;
			this.Field11.Width = 1.6875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.6875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Tag = "bold";
			this.Label13.Text = "Total";
			this.Label13.Top = 0.375F;
			this.Label13.Width = 1.0625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.9375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Tag = "bold";
			this.Label14.Text = "Tax";
			this.Label14.Top = 0.34375F;
			this.Label14.Width = 1.5F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.15625F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-size: 10pt; text-align: left";
			this.txtTime.Tag = "textbox";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.15625F;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 8.5pt; text-align: right";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.15625F;
			this.txtName.Left = 0.75F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 8.5pt";
			this.txtName.Tag = "textbox";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 3.1875F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.15625F;
			this.txtTotal.Left = 4F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat");
			this.txtTotal.Style = "font-size: 8.5pt; text-align: right";
			this.txtTotal.Tag = "textbox";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 1.75F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.15625F;
			this.txtAddress1.Left = 0.75F;
			this.txtAddress1.MultiLine = false;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-size: 8.5pt";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 3.1875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.15625F;
			this.txtAddress2.Left = 0.75F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-size: 8.5pt";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 3.1875F;
			// 
			// txtMailingAddress3
			// 
			this.txtMailingAddress3.Height = 0.15625F;
			this.txtMailingAddress3.Left = 0.75F;
			this.txtMailingAddress3.MultiLine = false;
			this.txtMailingAddress3.Name = "txtMailingAddress3";
			this.txtMailingAddress3.Style = "font-size: 8.5pt";
			this.txtMailingAddress3.Tag = "textbox";
			this.txtMailingAddress3.Text = null;
			this.txtMailingAddress3.Top = 0.46875F;
			this.txtMailingAddress3.Width = 3.1875F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.Height = 0.15625F;
			this.txtAddress3.Left = 0.75F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Style = "font-size: 8.5pt";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.625F;
			this.txtAddress3.Width = 3.1875F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.15625F;
			this.Field10.Left = 0.75F;
			this.Field10.MultiLine = false;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-size: 8.5pt";
			this.Field10.Tag = "textbox";
			this.Field10.Text = null;
			this.Field10.Top = 0.78125F;
			this.Field10.Width = 3.1875F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.15625F;
			this.txtLocation.Left = 0.75F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-size: 8.5pt";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.9375F;
			this.txtLocation.Width = 3.1875F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.15625F;
			this.txtMapLot.Left = 0.75F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-size: 8.5pt";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.09375F;
			this.txtMapLot.Width = 3.1875F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.15625F;
			this.txtTax.Left = 5.9375F;
			this.txtTax.MultiLine = false;
			this.txtTax.Name = "txtTax";
			this.txtTax.OutputFormat = resources.GetString("txtTax.OutputFormat");
			this.txtTax.Style = "font-size: 8.5pt; text-align: right";
			this.txtTax.Tag = "textbox";
			this.txtTax.Text = null;
			this.txtTax.Top = 0F;
			this.txtTax.Width = 1.5F;
			// 
			// txtDiscount
			// 
			this.txtDiscount.DataField = "taxval";
			this.txtDiscount.Height = 0.15625F;
			this.txtDiscount.Left = 6.3125F;
			this.txtDiscount.MultiLine = false;
			this.txtDiscount.Name = "txtDiscount";
			this.txtDiscount.OutputFormat = resources.GetString("txtDiscount.OutputFormat");
			this.txtDiscount.Style = "font-size: 8.5pt; text-align: right";
			this.txtDiscount.Tag = "textbox";
			this.txtDiscount.Text = null;
			this.txtDiscount.Top = 0.3125F;
			this.txtDiscount.Width = 1.125F;
			// 
			// lblDiscount
			// 
			this.lblDiscount.Height = 0.15625F;
			this.lblDiscount.Left = 6.5F;
			this.lblDiscount.MultiLine = false;
			this.lblDiscount.Name = "lblDiscount";
			this.lblDiscount.OutputFormat = resources.GetString("lblDiscount.OutputFormat");
			this.lblDiscount.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.lblDiscount.Tag = "bold";
			this.lblDiscount.Text = "Discount";
			this.lblDiscount.Top = 0.15625F;
			this.lblDiscount.Width = 0.9375F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.Height = 0.1875F;
			this.txtBookPage.Left = 0.75F;
			this.txtBookPage.MultiLine = false;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Style = "font-size: 8.5pt";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.25F;
			this.txtBookPage.Width = 3.1875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.15625F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 10pt; font-weight: bold";
			this.Label1.Tag = "bold";
			this.Label1.Text = "Page Totals:";
			this.Label1.Top = 0.15625F;
			this.Label1.Width = 1.25F;
			// 
			// txtTotTot
			// 
			this.txtTotTot.DataField = "totalval";
			this.txtTotTot.Height = 0.15625F;
			this.txtTotTot.Left = 4F;
			this.txtTotTot.Name = "txtTotTot";
			this.txtTotTot.OutputFormat = resources.GetString("txtTotTot.OutputFormat");
			this.txtTotTot.Style = "font-size: 10pt; text-align: right";
			this.txtTotTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTotTot.Tag = "textbox";
			this.txtTotTot.Text = null;
			this.txtTotTot.Top = 0.15625F;
			this.txtTotTot.Width = 1.75F;
			// 
			// txtTaxTot
			// 
			this.txtTaxTot.DataField = "taxval";
			this.txtTaxTot.Height = 0.15625F;
			this.txtTaxTot.Left = 5.875F;
			this.txtTaxTot.Name = "txtTaxTot";
			this.txtTaxTot.OutputFormat = resources.GetString("txtTaxTot.OutputFormat");
			this.txtTaxTot.Style = "font-size: 10pt; text-align: right";
			this.txtTaxTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTaxTot.Tag = "textbox";
			this.txtTaxTot.Text = null;
			this.txtTaxTot.Top = 0.15625F;
			this.txtTaxTot.Width = 1.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.15625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.5625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 10pt; font-weight: bold";
			this.Label2.Tag = "bold";
			this.Label2.Text = "Totals:";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 1.25F;
			// 
			// txtTotalSub
			// 
			this.txtTotalSub.Height = 0.15625F;
			this.txtTotalSub.Left = 4F;
			this.txtTotalSub.MultiLine = false;
			this.txtTotalSub.Name = "txtTotalSub";
			this.txtTotalSub.OutputFormat = resources.GetString("txtTotalSub.OutputFormat");
			this.txtTotalSub.Style = "font-size: 10pt; text-align: right";
			this.txtTotalSub.Tag = "textbox";
			this.txtTotalSub.Text = null;
			this.txtTotalSub.Top = 0.3125F;
			this.txtTotalSub.Width = 1.75F;
			// 
			// txtTaxSub
			// 
			this.txtTaxSub.Height = 0.15625F;
			this.txtTaxSub.Left = 5.875F;
			this.txtTaxSub.MultiLine = false;
			this.txtTaxSub.Name = "txtTaxSub";
			this.txtTaxSub.OutputFormat = resources.GetString("txtTaxSub.OutputFormat");
			this.txtTaxSub.Style = "font-size: 10pt; text-align: right";
			this.txtTaxSub.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtTaxSub.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtTaxSub.Tag = "textbox";
			this.txtTaxSub.Text = null;
			this.txtTaxSub.Top = 0.3125F;
			this.txtTaxSub.Width = 1.5625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.15625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label6.Tag = "bold";
			this.Label6.Text = "Total";
			this.Label6.Top = 0.15625F;
			this.Label6.Width = 1.75F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.15625F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.9375F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label7.Tag = "bold";
			this.Label7.Text = "Tax";
			this.Label7.Top = 0.15625F;
			this.Label7.Width = 1.5F;
			// 
			// rptRECollectionSimple
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailingAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxSub;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxTot;
	}
}
