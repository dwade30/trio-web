﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmTaxMessage.
	/// </summary>
	partial class frmTaxMessage : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDistributionPercent;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDistribution;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDistAmount;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMessage;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtReturnAddress;
		public fecherFoundation.FCPanel framMailingAddress;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCPanel framDistribution;
		public fecherFoundation.FCLabel lblDistributionPercent_4;
		public fecherFoundation.FCLabel lblDistributionPercent_3;
		public fecherFoundation.FCLabel lblDistributionPercent_2;
		public fecherFoundation.FCLabel lblDistributionPercent_1;
		public fecherFoundation.FCLabel lblDistributionPercent_0;
		public fecherFoundation.FCLabel lblDistribution_4;
		public fecherFoundation.FCLabel lblDistribution_3;
		public fecherFoundation.FCLabel lblDistribution_2;
		public fecherFoundation.FCLabel lblDistribution_1;
		public fecherFoundation.FCLabel lblDistribution_0;
		public fecherFoundation.FCPanel framDistTotals;
		public fecherFoundation.FCLabel lblDistAmount_4;
		public fecherFoundation.FCLabel lblDistAmount_3;
		public fecherFoundation.FCLabel lblDistAmount_2;
		public fecherFoundation.FCLabel lblDistAmount_1;
		public fecherFoundation.FCLabel lblDistAmount_0;
		public fecherFoundation.FCPanel framMessage;
		public fecherFoundation.FCTextBox txtMessage_12;
		public fecherFoundation.FCTextBox txtMessage_11;
		public fecherFoundation.FCTextBox txtMessage_10;
		public fecherFoundation.FCTextBox txtMessage_9;
		public fecherFoundation.FCTextBox txtMessage_8;
		public fecherFoundation.FCTextBox txtMessage_7;
		public fecherFoundation.FCTextBox txtMessage_6;
		public fecherFoundation.FCTextBox txtMessage_5;
		public fecherFoundation.FCTextBox txtMessage_4;
		public fecherFoundation.FCTextBox txtMessage_3;
		public fecherFoundation.FCTextBox txtMessage_2;
		public fecherFoundation.FCTextBox txtMessage_1;
		public fecherFoundation.FCTextBox txtMessage_0;
		public fecherFoundation.FCLabel lblTest;
		public fecherFoundation.FCPanel framReturnAddress;
		public fecherFoundation.FCTextBox txtReturnAddress_3;
		public fecherFoundation.FCTextBox txtReturnAddress_2;
		public fecherFoundation.FCTextBox txtReturnAddress_1;
		public fecherFoundation.FCTextBox txtReturnAddress_0;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxMessage));
			this.framMailingAddress = new fecherFoundation.FCPanel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.framDistribution = new fecherFoundation.FCPanel();
			this.lblDistributionPercent_4 = new fecherFoundation.FCLabel();
			this.lblDistributionPercent_3 = new fecherFoundation.FCLabel();
			this.lblDistributionPercent_2 = new fecherFoundation.FCLabel();
			this.lblDistributionPercent_1 = new fecherFoundation.FCLabel();
			this.lblDistributionPercent_0 = new fecherFoundation.FCLabel();
			this.lblDistribution_4 = new fecherFoundation.FCLabel();
			this.lblDistribution_3 = new fecherFoundation.FCLabel();
			this.lblDistribution_2 = new fecherFoundation.FCLabel();
			this.lblDistribution_1 = new fecherFoundation.FCLabel();
			this.lblDistribution_0 = new fecherFoundation.FCLabel();
			this.framDistTotals = new fecherFoundation.FCPanel();
			this.lblDistAmount_4 = new fecherFoundation.FCLabel();
			this.lblDistAmount_3 = new fecherFoundation.FCLabel();
			this.lblDistAmount_2 = new fecherFoundation.FCLabel();
			this.lblDistAmount_1 = new fecherFoundation.FCLabel();
			this.lblDistAmount_0 = new fecherFoundation.FCLabel();
			this.framMessage = new fecherFoundation.FCPanel();
			this.txtMessage_12 = new fecherFoundation.FCTextBox();
			this.txtMessage_11 = new fecherFoundation.FCTextBox();
			this.txtMessage_10 = new fecherFoundation.FCTextBox();
			this.txtMessage_9 = new fecherFoundation.FCTextBox();
			this.txtMessage_8 = new fecherFoundation.FCTextBox();
			this.txtMessage_7 = new fecherFoundation.FCTextBox();
			this.txtMessage_6 = new fecherFoundation.FCTextBox();
			this.txtMessage_5 = new fecherFoundation.FCTextBox();
			this.txtMessage_4 = new fecherFoundation.FCTextBox();
			this.txtMessage_3 = new fecherFoundation.FCTextBox();
			this.txtMessage_2 = new fecherFoundation.FCTextBox();
			this.txtMessage_1 = new fecherFoundation.FCTextBox();
			this.txtMessage_0 = new fecherFoundation.FCTextBox();
			this.lblTest = new fecherFoundation.FCLabel();
			this.framReturnAddress = new fecherFoundation.FCPanel();
			this.txtReturnAddress_3 = new fecherFoundation.FCTextBox();
			this.txtReturnAddress_2 = new fecherFoundation.FCTextBox();
			this.txtReturnAddress_1 = new fecherFoundation.FCTextBox();
			this.txtReturnAddress_0 = new fecherFoundation.FCTextBox();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framMailingAddress)).BeginInit();
			this.framMailingAddress.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framDistribution)).BeginInit();
			this.framDistribution.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framDistTotals)).BeginInit();
			this.framDistTotals.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framMessage)).BeginInit();
			this.framMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framReturnAddress)).BeginInit();
			this.framReturnAddress.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 639);
			this.BottomPanel.Size = new System.Drawing.Size(974, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framMailingAddress);
			this.ClientArea.Controls.Add(this.framDistribution);
			this.ClientArea.Controls.Add(this.framDistTotals);
			this.ClientArea.Controls.Add(this.framMessage);
			this.ClientArea.Controls.Add(this.framReturnAddress);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Size = new System.Drawing.Size(974, 579);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(974, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(213, 30);
			this.HeaderText.Text = "Input Bill Message";
			// 
			// framMailingAddress
			// 
			this.framMailingAddress.AppearanceKey = "groupBoxNoBorders";
			this.framMailingAddress.Controls.Add(this.Label4);
			this.framMailingAddress.Controls.Add(this.Label3);
			this.framMailingAddress.Controls.Add(this.Label2);
			this.framMailingAddress.Controls.Add(this.Label1);
			this.framMailingAddress.Location = new System.Drawing.Point(437, 445);
			this.framMailingAddress.Name = "framMailingAddress";
			this.framMailingAddress.Size = new System.Drawing.Size(233, 113);
			this.framMailingAddress.TabIndex = 4;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(0, 90);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(206, 18);
			this.Label4.TabIndex = 3;
			this.Label4.Text = "TRIOVILLE, ME  04419-1234";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(0, 60);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(226, 18);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "TAXPAYER ADDRESS LINE 2";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(0, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(165, 20);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "TAXPAYER ADDRESS LINE 1";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(0, 0);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(161, 18);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "TAXPAYER, JOHN J.";
			// 
			// framDistribution
			// 
			this.framDistribution.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right | Wisej.Web.AnchorStyles.Bottom));
			this.framDistribution.AppearanceKey = "groupBoxNoBorders";
			this.framDistribution.Controls.Add(this.lblDistributionPercent_4);
			this.framDistribution.Controls.Add(this.lblDistributionPercent_3);
			this.framDistribution.Controls.Add(this.lblDistributionPercent_2);
			this.framDistribution.Controls.Add(this.lblDistributionPercent_1);
			this.framDistribution.Controls.Add(this.lblDistributionPercent_0);
			this.framDistribution.Controls.Add(this.lblDistribution_4);
			this.framDistribution.Controls.Add(this.lblDistribution_3);
			this.framDistribution.Controls.Add(this.lblDistribution_2);
			this.framDistribution.Controls.Add(this.lblDistribution_1);
			this.framDistribution.Controls.Add(this.lblDistribution_0);
			this.framDistribution.Location = new System.Drawing.Point(30, 154);
			this.framDistribution.Name = "framDistribution";
			this.framDistribution.Size = new System.Drawing.Size(226, 151);
			this.framDistribution.TabIndex = 1;
			// 
			// lblDistributionPercent_4
			// 
			this.lblDistributionPercent_4.Location = new System.Drawing.Point(152, 120);
			this.lblDistributionPercent_4.Name = "lblDistributionPercent_4";
			this.lblDistributionPercent_4.Size = new System.Drawing.Size(56, 14);
			this.lblDistributionPercent_4.TabIndex = 9;
			this.lblDistributionPercent_4.Text = "XX.XX%";
			// 
			// lblDistributionPercent_3
			// 
			this.lblDistributionPercent_3.Location = new System.Drawing.Point(152, 90);
			this.lblDistributionPercent_3.Name = "lblDistributionPercent_3";
			this.lblDistributionPercent_3.Size = new System.Drawing.Size(56, 14);
			this.lblDistributionPercent_3.TabIndex = 7;
			this.lblDistributionPercent_3.Text = "XX.XX%";
			// 
			// lblDistributionPercent_2
			// 
			this.lblDistributionPercent_2.Location = new System.Drawing.Point(152, 60);
			this.lblDistributionPercent_2.Name = "lblDistributionPercent_2";
			this.lblDistributionPercent_2.Size = new System.Drawing.Size(56, 14);
			this.lblDistributionPercent_2.TabIndex = 5;
			this.lblDistributionPercent_2.Text = "XX.XX%";
			// 
			// lblDistributionPercent_1
			// 
			this.lblDistributionPercent_1.Location = new System.Drawing.Point(152, 30);
			this.lblDistributionPercent_1.Name = "lblDistributionPercent_1";
			this.lblDistributionPercent_1.Size = new System.Drawing.Size(56, 14);
			this.lblDistributionPercent_1.TabIndex = 3;
			this.lblDistributionPercent_1.Text = "XX.XX%";
			// 
			// lblDistributionPercent_0
			// 
			this.lblDistributionPercent_0.Location = new System.Drawing.Point(152, 0);
			this.lblDistributionPercent_0.Name = "lblDistributionPercent_0";
			this.lblDistributionPercent_0.Size = new System.Drawing.Size(56, 14);
			this.lblDistributionPercent_0.TabIndex = 1;
			this.lblDistributionPercent_0.Text = "XX.XX%";
			// 
			// lblDistribution_4
			// 
			this.lblDistribution_4.Location = new System.Drawing.Point(0, 120);
			this.lblDistribution_4.Name = "lblDistribution_4";
			this.lblDistribution_4.Size = new System.Drawing.Size(130, 20);
			this.lblDistribution_4.TabIndex = 8;
			this.lblDistribution_4.Text = "LABEL14";
			// 
			// lblDistribution_3
			// 
			this.lblDistribution_3.Location = new System.Drawing.Point(0, 90);
			this.lblDistribution_3.Name = "lblDistribution_3";
			this.lblDistribution_3.Size = new System.Drawing.Size(130, 20);
			this.lblDistribution_3.TabIndex = 6;
			this.lblDistribution_3.Text = "LABEL13";
			// 
			// lblDistribution_2
			// 
			this.lblDistribution_2.Location = new System.Drawing.Point(0, 60);
			this.lblDistribution_2.Name = "lblDistribution_2";
			this.lblDistribution_2.Size = new System.Drawing.Size(130, 20);
			this.lblDistribution_2.TabIndex = 4;
			this.lblDistribution_2.Text = "LABEL12";
			// 
			// lblDistribution_1
			// 
			this.lblDistribution_1.Location = new System.Drawing.Point(0, 30);
			this.lblDistribution_1.Name = "lblDistribution_1";
			this.lblDistribution_1.Size = new System.Drawing.Size(130, 20);
			this.lblDistribution_1.TabIndex = 2;
			this.lblDistribution_1.Text = "LABEL11";
			// 
			// lblDistribution_0
			// 
			this.lblDistribution_0.Location = new System.Drawing.Point(0, 0);
			this.lblDistribution_0.Name = "lblDistribution_0";
			this.lblDistribution_0.Size = new System.Drawing.Size(130, 20);
			this.lblDistribution_0.TabIndex = 0;
			this.lblDistribution_0.Text = "DISTRIBUTION 1";
			// 
			// framDistTotals
			// 
			this.framDistTotals.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right | Wisej.Web.AnchorStyles.Bottom));
			this.framDistTotals.AppearanceKey = "groupBoxNoBorders";
			this.framDistTotals.Controls.Add(this.lblDistAmount_4);
			this.framDistTotals.Controls.Add(this.lblDistAmount_3);
			this.framDistTotals.Controls.Add(this.lblDistAmount_2);
			this.framDistTotals.Controls.Add(this.lblDistAmount_1);
			this.framDistTotals.Controls.Add(this.lblDistAmount_0);
			this.framDistTotals.Location = new System.Drawing.Point(293, 154);
			this.framDistTotals.Name = "framDistTotals";
			this.framDistTotals.Size = new System.Drawing.Size(116, 151);
			this.framDistTotals.TabIndex = 2;
			// 
			// lblDistAmount_4
			// 
			this.lblDistAmount_4.Location = new System.Drawing.Point(0, 120);
			this.lblDistAmount_4.Name = "lblDistAmount_4";
			this.lblDistAmount_4.Size = new System.Drawing.Size(89, 16);
			this.lblDistAmount_4.TabIndex = 4;
			this.lblDistAmount_4.Text = "11,111,111.11";
			// 
			// lblDistAmount_3
			// 
			this.lblDistAmount_3.Location = new System.Drawing.Point(0, 90);
			this.lblDistAmount_3.Name = "lblDistAmount_3";
			this.lblDistAmount_3.Size = new System.Drawing.Size(89, 16);
			this.lblDistAmount_3.TabIndex = 3;
			this.lblDistAmount_3.Text = "11,111,111.11";
			// 
			// lblDistAmount_2
			// 
			this.lblDistAmount_2.Location = new System.Drawing.Point(0, 60);
			this.lblDistAmount_2.Name = "lblDistAmount_2";
			this.lblDistAmount_2.Size = new System.Drawing.Size(89, 16);
			this.lblDistAmount_2.TabIndex = 2;
			this.lblDistAmount_2.Text = "11,111,111.11";
			// 
			// lblDistAmount_1
			// 
			this.lblDistAmount_1.Location = new System.Drawing.Point(0, 30);
			this.lblDistAmount_1.Name = "lblDistAmount_1";
			this.lblDistAmount_1.Size = new System.Drawing.Size(89, 16);
			this.lblDistAmount_1.TabIndex = 1;
			this.lblDistAmount_1.Text = "11,111,111.11";
			// 
			// lblDistAmount_0
			// 
			this.lblDistAmount_0.Location = new System.Drawing.Point(0, 0);
			this.lblDistAmount_0.Name = "lblDistAmount_0";
			this.lblDistAmount_0.Size = new System.Drawing.Size(89, 18);
			this.lblDistAmount_0.TabIndex = 0;
			this.lblDistAmount_0.Text = "11,111,111.11";
			// 
			// framMessage
			// 
			this.framMessage.AppearanceKey = "groupBoxNoBorders";
			this.framMessage.Controls.Add(this.txtMessage_12);
			this.framMessage.Controls.Add(this.txtMessage_11);
			this.framMessage.Controls.Add(this.txtMessage_10);
			this.framMessage.Controls.Add(this.txtMessage_9);
			this.framMessage.Controls.Add(this.txtMessage_8);
			this.framMessage.Controls.Add(this.txtMessage_7);
			this.framMessage.Controls.Add(this.txtMessage_6);
			this.framMessage.Controls.Add(this.txtMessage_5);
			this.framMessage.Controls.Add(this.txtMessage_4);
			this.framMessage.Controls.Add(this.txtMessage_3);
			this.framMessage.Controls.Add(this.txtMessage_2);
			this.framMessage.Controls.Add(this.txtMessage_1);
			this.framMessage.Controls.Add(this.txtMessage_0);
			this.framMessage.Controls.Add(this.lblTest);
			this.framMessage.Location = new System.Drawing.Point(600, 200);
			this.framMessage.Name = "framMessage";
			this.framMessage.Size = new System.Drawing.Size(700, 401);
			this.framMessage.TabIndex = 3;
			// 
			// txtMessage_12
			// 
			this.txtMessage_12.AutoSize = false;
			this.txtMessage_12.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_12.LinkItem = null;
			this.txtMessage_12.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_12.LinkTopic = null;
			this.txtMessage_12.Location = new System.Drawing.Point(0, 360);
			this.txtMessage_12.Name = "txtMessage_12";
			this.txtMessage_12.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_12.TabIndex = 12;
			this.txtMessage_12.Text = "Text6";
			this.txtMessage_12.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_12.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_11
			// 
			this.txtMessage_11.AutoSize = false;
			this.txtMessage_11.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_11.LinkItem = null;
			this.txtMessage_11.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_11.LinkTopic = null;
			this.txtMessage_11.Location = new System.Drawing.Point(0, 330);
			this.txtMessage_11.Name = "txtMessage_11";
			this.txtMessage_11.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_11.TabIndex = 11;
			this.txtMessage_11.Text = "Text5";
			this.txtMessage_11.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_11.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_10
			// 
			this.txtMessage_10.AutoSize = false;
			this.txtMessage_10.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_10.LinkItem = null;
			this.txtMessage_10.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_10.LinkTopic = null;
			this.txtMessage_10.Location = new System.Drawing.Point(0, 300);
			this.txtMessage_10.Name = "txtMessage_10";
			this.txtMessage_10.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_10.TabIndex = 10;
			this.txtMessage_10.Text = "Text4";
			this.txtMessage_10.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_10.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_9
			// 
			this.txtMessage_9.AutoSize = false;
			this.txtMessage_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_9.LinkItem = null;
			this.txtMessage_9.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_9.LinkTopic = null;
			this.txtMessage_9.Location = new System.Drawing.Point(0, 270);
			this.txtMessage_9.Name = "txtMessage_9";
			this.txtMessage_9.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_9.TabIndex = 9;
			this.txtMessage_9.Text = "Text3";
			this.txtMessage_9.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_8
			// 
			this.txtMessage_8.AutoSize = false;
			this.txtMessage_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_8.LinkItem = null;
			this.txtMessage_8.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_8.LinkTopic = null;
			this.txtMessage_8.Location = new System.Drawing.Point(0, 240);
			this.txtMessage_8.Name = "txtMessage_8";
			this.txtMessage_8.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_8.TabIndex = 8;
			this.txtMessage_8.Text = "Text2";
			this.txtMessage_8.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_7
			// 
			this.txtMessage_7.AutoSize = false;
			this.txtMessage_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_7.LinkItem = null;
			this.txtMessage_7.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_7.LinkTopic = null;
			this.txtMessage_7.Location = new System.Drawing.Point(0, 210);
			this.txtMessage_7.Name = "txtMessage_7";
			this.txtMessage_7.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_7.TabIndex = 7;
			this.txtMessage_7.Text = "Text1";
			this.txtMessage_7.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_6
			// 
			this.txtMessage_6.AutoSize = false;
			this.txtMessage_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_6.LinkItem = null;
			this.txtMessage_6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_6.LinkTopic = null;
			this.txtMessage_6.Location = new System.Drawing.Point(0, 180);
			this.txtMessage_6.Name = "txtMessage_6";
			this.txtMessage_6.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_6.TabIndex = 6;
			this.txtMessage_6.Text = "Text7";
			this.txtMessage_6.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_5
			// 
			this.txtMessage_5.AutoSize = false;
			this.txtMessage_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_5.LinkItem = null;
			this.txtMessage_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_5.LinkTopic = null;
			this.txtMessage_5.Location = new System.Drawing.Point(0, 150);
			this.txtMessage_5.Name = "txtMessage_5";
			this.txtMessage_5.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_5.TabIndex = 5;
			this.txtMessage_5.Text = "Text6";
			this.txtMessage_5.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_4
			// 
			this.txtMessage_4.AutoSize = false;
			this.txtMessage_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_4.LinkItem = null;
			this.txtMessage_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_4.LinkTopic = null;
			this.txtMessage_4.Location = new System.Drawing.Point(0, 120);
			this.txtMessage_4.Name = "txtMessage_4";
			this.txtMessage_4.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_4.TabIndex = 4;
			this.txtMessage_4.Text = "Text5";
			this.txtMessage_4.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_3
			// 
			this.txtMessage_3.AutoSize = false;
			this.txtMessage_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_3.LinkItem = null;
			this.txtMessage_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_3.LinkTopic = null;
			this.txtMessage_3.Location = new System.Drawing.Point(0, 90);
			this.txtMessage_3.Name = "txtMessage_3";
			this.txtMessage_3.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_3.TabIndex = 3;
			this.txtMessage_3.Text = "Text4";
			this.txtMessage_3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_2
			// 
			this.txtMessage_2.AutoSize = false;
			this.txtMessage_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_2.LinkItem = null;
			this.txtMessage_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_2.LinkTopic = null;
			this.txtMessage_2.Location = new System.Drawing.Point(0, 60);
			this.txtMessage_2.Name = "txtMessage_2";
			this.txtMessage_2.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_2.TabIndex = 2;
			this.txtMessage_2.Text = "Text3";
			this.txtMessage_2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_1
			// 
			this.txtMessage_1.AutoSize = false;
			this.txtMessage_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_1.LinkItem = null;
			this.txtMessage_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_1.LinkTopic = null;
			this.txtMessage_1.Location = new System.Drawing.Point(0, 30);
			this.txtMessage_1.Name = "txtMessage_1";
			this.txtMessage_1.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_1.TabIndex = 1;
			this.txtMessage_1.Text = "Text2";
			this.txtMessage_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// txtMessage_0
			// 
			this.txtMessage_0.AutoSize = false;
			this.txtMessage_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_0.LinkItem = null;
			this.txtMessage_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_0.LinkTopic = null;
			this.txtMessage_0.Location = new System.Drawing.Point(0, 0);
			this.txtMessage_0.Name = "txtMessage_0";
			this.txtMessage_0.Size = new System.Drawing.Size(459, 40);
			this.txtMessage_0.TabIndex = 0;
			this.txtMessage_0.Text = "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
			this.txtMessage_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			this.txtMessage_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMessage_KeyPress);
			// 
			// lblTest
			// 
			this.lblTest.AutoSize = true;
			this.lblTest.Location = new System.Drawing.Point(464, 10);
			this.lblTest.Name = "lblTest";
			this.lblTest.Size = new System.Drawing.Size(53, 15);
			this.lblTest.TabIndex = 41;
			this.lblTest.Text = "LABEL5";
			this.lblTest.Visible = false;
			// 
			// framReturnAddress
			// 
			this.framReturnAddress.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right | Wisej.Web.AnchorStyles.Bottom));
			this.framReturnAddress.AppearanceKey = "groupBoxNoBorders";
			this.framReturnAddress.Controls.Add(this.txtReturnAddress_3);
			this.framReturnAddress.Controls.Add(this.txtReturnAddress_2);
			this.framReturnAddress.Controls.Add(this.txtReturnAddress_1);
			this.framReturnAddress.Controls.Add(this.txtReturnAddress_0);
			this.framReturnAddress.Location = new System.Drawing.Point(30, 30);
			this.framReturnAddress.Name = "framReturnAddress";
			this.framReturnAddress.Size = new System.Drawing.Size(258, 115);
			this.framReturnAddress.TabIndex = 0;
			// 
			// txtReturnAddress_3
			// 
			this.txtReturnAddress_3.MaxLength = 30;
			this.txtReturnAddress_3.Appearance = 0;
			this.txtReturnAddress_3.AutoSize = false;
			this.txtReturnAddress_3.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.txtReturnAddress_3.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtReturnAddress_3.LinkItem = null;
			this.txtReturnAddress_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReturnAddress_3.LinkTopic = null;
			this.txtReturnAddress_3.Location = new System.Drawing.Point(0, 75);
			this.txtReturnAddress_3.LockedOriginal = true;
			this.txtReturnAddress_3.Name = "txtReturnAddress_3";
			this.txtReturnAddress_3.ReadOnly = true;
			this.txtReturnAddress_3.Size = new System.Drawing.Size(226, 20);
			this.txtReturnAddress_3.TabIndex = 0;
			this.txtReturnAddress_3.Text = "before printing bills.";
			// 
			// txtReturnAddress_2
			// 
			this.txtReturnAddress_2.MaxLength = 31;
			this.txtReturnAddress_2.Appearance = 0;
			this.txtReturnAddress_2.AutoSize = false;
			this.txtReturnAddress_2.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.txtReturnAddress_2.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtReturnAddress_2.LinkItem = null;
			this.txtReturnAddress_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReturnAddress_2.LinkTopic = null;
			this.txtReturnAddress_2.Location = new System.Drawing.Point(0, 50);
			this.txtReturnAddress_2.LockedOriginal = true;
			this.txtReturnAddress_2.Name = "txtReturnAddress_2";
			this.txtReturnAddress_2.ReadOnly = true;
			this.txtReturnAddress_2.Size = new System.Drawing.Size(226, 20);
			this.txtReturnAddress_2.TabIndex = 3;
			this.txtReturnAddress_2.Text = "and fill in the return address";
			// 
			// txtReturnAddress_1
			// 
			this.txtReturnAddress_1.MaxLength = 30;
			this.txtReturnAddress_1.Appearance = 0;
			this.txtReturnAddress_1.AutoSize = false;
			this.txtReturnAddress_1.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.txtReturnAddress_1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtReturnAddress_1.LinkItem = null;
			this.txtReturnAddress_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReturnAddress_1.LinkTopic = null;
			this.txtReturnAddress_1.Location = new System.Drawing.Point(0, 25);
			this.txtReturnAddress_1.LockedOriginal = true;
			this.txtReturnAddress_1.Name = "txtReturnAddress_1";
			this.txtReturnAddress_1.ReadOnly = true;
			this.txtReturnAddress_1.Size = new System.Drawing.Size(242, 20);
			this.txtReturnAddress_1.TabIndex = 2;
			this.txtReturnAddress_1.Text = "that you go to file maintenance";
			// 
			// txtReturnAddress_0
			// 
			this.txtReturnAddress_0.MaxLength = 30;
			this.txtReturnAddress_0.Appearance = 0;
			this.txtReturnAddress_0.AutoSize = false;
			this.txtReturnAddress_0.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.txtReturnAddress_0.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtReturnAddress_0.LinkItem = null;
			this.txtReturnAddress_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReturnAddress_0.LinkTopic = null;
			this.txtReturnAddress_0.Location = new System.Drawing.Point(0, 0);
			this.txtReturnAddress_0.LockedOriginal = true;
			this.txtReturnAddress_0.Name = "txtReturnAddress_0";
			this.txtReturnAddress_0.ReadOnly = true;
			this.txtReturnAddress_0.Size = new System.Drawing.Size(226, 20);
			this.txtReturnAddress_0.TabIndex = 1;
			this.txtReturnAddress_0.Text = "Please make sure";
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(463, 13);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(458, 14);
			this.lblDescription.TabIndex = 3;
			this.lblDescription.Text = "LABEL5";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblDescription.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Save && Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(415, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(149, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmTaxMessage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(974, 747);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmTaxMessage";
			this.Text = "Input Bill Messages";
			this.Load += new System.EventHandler(this.frmTaxMessage_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTaxMessage_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framMailingAddress)).EndInit();
			this.framMailingAddress.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.framDistribution)).EndInit();
			this.framDistribution.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.framDistTotals)).EndInit();
			this.framDistTotals.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.framMessage)).EndInit();
			this.framMessage.ResumeLayout(false);
			this.framMessage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framReturnAddress)).EndInit();
			this.framReturnAddress.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveContinue;
	}
}
