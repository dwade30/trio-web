﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmExport.
	/// </summary>
	partial class frmExport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbBills;
		public fecherFoundation.FCLabel lblBills;
		public fecherFoundation.FCComboBox cmbBillType;
		public fecherFoundation.FCLabel lblBillType;
		public fecherFoundation.FCCheckBox chkHeaders;
		public fecherFoundation.FCFrame framFile;
		public fecherFoundation.FCTextBox txtFilename;
		//FC:FINAL:MSH - i.issue #1456: will not be needed on web
		//public fecherFoundation.FCButton cmdBrowse;
		//public fecherFoundation.FCTextBox txtPath;
		public fecherFoundation.FCLabel Label2;
		//public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame framOrderBy;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCFrame framGroup;
		public FCGrid GridGroup;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuViewFormat;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExport));
			this.cmbOrder = new fecherFoundation.FCComboBox();
			this.lblOrder = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbBills = new fecherFoundation.FCComboBox();
			this.lblBills = new fecherFoundation.FCLabel();
			this.cmbBillType = new fecherFoundation.FCComboBox();
			this.lblBillType = new fecherFoundation.FCLabel();
			this.chkHeaders = new fecherFoundation.FCCheckBox();
			this.framFile = new fecherFoundation.FCFrame();
			this.txtFilename = new fecherFoundation.FCTextBox();
			//this.cmdBrowse = new fecherFoundation.FCButton();
			//this.txtPath = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			//this.Label1 = new fecherFoundation.FCLabel();
			this.framOrderBy = new fecherFoundation.FCFrame();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.framGroup = new fecherFoundation.FCFrame();
			this.GridGroup = new fecherFoundation.FCGrid();
			this.mnuViewFormat = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveAndContinue = new fecherFoundation.FCButton();
			this.cmdViewFormat = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkHeaders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framFile)).BeginInit();
			this.framFile.SuspendLayout();
			//((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framOrderBy)).BeginInit();
			this.framOrderBy.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framGroup)).BeginInit();
			this.framGroup.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdViewFormat)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAndContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 537);
			this.BottomPanel.Size = new System.Drawing.Size(820, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkHeaders);
			this.ClientArea.Controls.Add(this.framFile);
			this.ClientArea.Controls.Add(this.framOrderBy);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.framGroup);
			this.ClientArea.Controls.Add(this.cmbBillType);
			this.ClientArea.Controls.Add(this.lblBillType);
			this.ClientArea.Size = new System.Drawing.Size(820, 477);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdViewFormat);
			this.TopPanel.Size = new System.Drawing.Size(820, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdViewFormat, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(201, 30);
			this.HeaderText.Text = "Database Extract";
			// 
			// cmbOrder
			// 
			this.cmbOrder.AutoSize = false;
			this.cmbOrder.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOrder.FormattingEnabled = true;
			this.cmbOrder.Items.AddRange(new object[] {
				"Name",
				"Account",
				"Zip Code"
			});
			this.cmbOrder.Location = new System.Drawing.Point(125, 30);
			this.cmbOrder.Name = "cmbOrder";
			this.cmbOrder.Size = new System.Drawing.Size(173, 40);
			this.cmbOrder.TabIndex = 1;
			this.cmbOrder.Text = "Name";
			this.cmbOrder.SelectedIndexChanged += new System.EventHandler(this.cmbOrder_SelectedIndexChanged);
			// 
			// lblOrder
			// 
			this.lblOrder.AutoSize = true;
			this.lblOrder.Location = new System.Drawing.Point(20, 44);
			this.lblOrder.Name = "lblOrder";
			this.lblOrder.Size = new System.Drawing.Size(52, 15);
			this.lblOrder.TabIndex = 0;
			this.lblOrder.Text = "ORDER";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Range"
			});
			this.cmbRange.Location = new System.Drawing.Point(613, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(180, 40);
			this.cmbRange.TabIndex = 3;
			this.cmbRange.Text = "All";
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.cmbRange_SelectedIndexChanged);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(505, 44);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 2;
			this.lblRange.Text = "RANGE";
			// 
			// cmbBills
			// 
			this.cmbBills.AutoSize = false;
			this.cmbBills.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBills.FormattingEnabled = true;
			this.cmbBills.Items.AddRange(new object[] {
				"Year",
				"Rate Record"
			});
			this.cmbBills.Location = new System.Drawing.Point(121, 30);
			this.cmbBills.Name = "cmbBills";
			this.cmbBills.Size = new System.Drawing.Size(177, 40);
			this.cmbBills.TabIndex = 1;
			this.cmbBills.Text = "Year";
			this.cmbBills.SelectedIndexChanged += new System.EventHandler(this.cmbBills_SelectedIndexChanged);
			// 
			// lblBills
			// 
			this.lblBills.AutoSize = true;
			this.lblBills.Location = new System.Drawing.Point(20, 44);
			this.lblBills.Name = "lblBills";
			this.lblBills.Size = new System.Drawing.Size(41, 15);
			this.lblBills.TabIndex = 0;
			this.lblBills.Text = "BILLS";
			// 
			// cmbBillType
			// 
			this.cmbBillType.AutoSize = false;
			this.cmbBillType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBillType.FormattingEnabled = true;
			this.cmbBillType.Items.AddRange(new object[] {
				"Real Estate",
				"Personal Property"
			});
			this.cmbBillType.Location = new System.Drawing.Point(156, 30);
			this.cmbBillType.Name = "cmbBillType";
			this.cmbBillType.Size = new System.Drawing.Size(172, 40);
			this.cmbBillType.TabIndex = 1;
			this.cmbBillType.Text = "Real Estate";
			// 
			// lblBillType
			// 
			this.lblBillType.AutoSize = true;
			this.lblBillType.Location = new System.Drawing.Point(30, 44);
			this.lblBillType.Name = "lblBillType";
			this.lblBillType.Size = new System.Drawing.Size(68, 15);
			this.lblBillType.TabIndex = 0;
			this.lblBillType.Text = "BILL TYPE";
			// 
			// chkHeaders
			// 
			this.chkHeaders.Location = new System.Drawing.Point(30, 369);
			this.chkHeaders.Name = "chkHeaders";
			this.chkHeaders.Size = new System.Drawing.Size(271, 27);
			this.chkHeaders.TabIndex = 7;
			this.chkHeaders.Text = "Make first record column headers";
			// 
			// framFile
			// 
			this.framFile.AppearanceKey = "groupBoxNoBorders";
			this.framFile.Controls.Add(this.txtFilename);
			//this.framFile.Controls.Add(this.cmdBrowse);
			//this.framFile.Controls.Add(this.txtPath);
			this.framFile.Controls.Add(this.Label2);
			//this.framFile.Controls.Add(this.Label1);
			this.framFile.Location = new System.Drawing.Point(30, 276);
			this.framFile.Name = "framFile";
			this.framFile.Size = new System.Drawing.Size(762, 145);
			this.framFile.TabIndex = 6;
			// 
			// txtFilename
			// 
			this.txtFilename.AutoSize = false;
			this.txtFilename.BackColor = System.Drawing.SystemColors.Window;
			this.txtFilename.LinkItem = null;
			this.txtFilename.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFilename.LinkTopic = null;
			this.txtFilename.Location = new System.Drawing.Point(156, 30);
			this.txtFilename.Name = "txtFilename";
			this.txtFilename.Size = new System.Drawing.Size(284, 40);
			this.txtFilename.TabIndex = 4;
			this.txtFilename.Text = "Billing.csv";
			// 
			// cmdBrowse
			// 
			//this.cmdBrowse.AppearanceKey = "actionButton";
			//this.cmdBrowse.ForeColor = System.Drawing.Color.White;
			//this.cmdBrowse.Location = new System.Drawing.Point(616, 30);
			//this.cmdBrowse.Name = "cmdBrowse";
			//this.cmdBrowse.Size = new System.Drawing.Size(84, 40);
			//this.cmdBrowse.TabIndex = 2;
			//this.cmdBrowse.Text = "Browse";
			//this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// txtPath
			// 
			//this.txtPath.AutoSize = false;
			//this.txtPath.BackColor = System.Drawing.SystemColors.Window;
			//this.txtPath.Enabled = false;
			//this.txtPath.LinkItem = null;
			//this.txtPath.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			//this.txtPath.LinkTopic = null;
			//this.txtPath.Location = new System.Drawing.Point(156, 30);
			//this.txtPath.Name = "txtPath";
			//this.txtPath.Size = new System.Drawing.Size(401, 40);
			//this.txtPath.TabIndex = 1;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(111, 20);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "FILENAME";
			// 
			// Label1
			// 
			//this.Label1.Location = new System.Drawing.Point(20, 44);
			//this.Label1.Name = "Label1";
			//this.Label1.Size = new System.Drawing.Size(67, 20);
			//this.Label1.TabIndex = 0;
			//this.Label1.Text = "DIRECTORY";
			// 
			// framOrderBy
			// 
			this.framOrderBy.Controls.Add(this.txtEnd);
			this.framOrderBy.Controls.Add(this.cmbOrder);
			this.framOrderBy.Controls.Add(this.lblOrder);
			this.framOrderBy.Controls.Add(this.txtStart);
			this.framOrderBy.Controls.Add(this.lblTo);
			this.framOrderBy.Location = new System.Drawing.Point(30, 181);
			this.framOrderBy.Name = "framOrderBy";
			this.framOrderBy.Size = new System.Drawing.Size(787, 82);
			this.framOrderBy.TabIndex = 5;
			this.framOrderBy.Text = "Order By";
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(640, 30);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(127, 40);
			this.txtEnd.TabIndex = 4;
			this.txtEnd.Visible = false;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(364, 30);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(127, 40);
			this.txtStart.TabIndex = 2;
			this.txtStart.Visible = false;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(527, 44);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(72, 15);
			this.lblTo.TabIndex = 3;
			this.lblTo.Text = "TO";
			this.lblTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTo.Visible = false;
			// 
			// framGroup
			// 
			this.framGroup.Controls.Add(this.GridGroup);
			this.framGroup.Controls.Add(this.cmbBills);
			this.framGroup.Controls.Add(this.lblBills);
			this.framGroup.Location = new System.Drawing.Point(30, 80);
			this.framGroup.Name = "framGroup";
			this.framGroup.Size = new System.Drawing.Size(763, 85);
			this.framGroup.TabIndex = 4;
			this.framGroup.Text = "Group By";
			// 
			// GridGroup
			// 
			this.GridGroup.AllowSelection = false;
			this.GridGroup.AllowUserToResizeColumns = false;
			this.GridGroup.AllowUserToResizeRows = false;
			this.GridGroup.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridGroup.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridGroup.BackColorBkg = System.Drawing.Color.Empty;
			this.GridGroup.BackColorFixed = System.Drawing.Color.Empty;
			this.GridGroup.BackColorSel = System.Drawing.Color.Empty;
			this.GridGroup.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridGroup.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridGroup.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridGroup.ColumnHeadersHeight = 30;
			this.GridGroup.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridGroup.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridGroup.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridGroup.DragIcon = null;
			this.GridGroup.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridGroup.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridGroup.ExtendLastCol = true;
			this.GridGroup.FixedCols = 0;
			this.GridGroup.FixedRows = 0;
			this.GridGroup.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridGroup.FrozenCols = 0;
			this.GridGroup.GridColor = System.Drawing.Color.Empty;
			this.GridGroup.GridColorFixed = System.Drawing.Color.Empty;
			this.GridGroup.Location = new System.Drawing.Point(368, 30);
			this.GridGroup.Name = "GridGroup";
			this.GridGroup.RowHeadersVisible = false;
			this.GridGroup.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridGroup.RowHeightMin = 0;
			this.GridGroup.Rows = 1;
			this.GridGroup.ScrollTipText = null;
			this.GridGroup.ShowColumnVisibilityMenu = false;
			this.GridGroup.Size = new System.Drawing.Size(371, 43);
			this.GridGroup.StandardTab = true;
			this.GridGroup.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridGroup.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridGroup.TabIndex = 2;
			// 
			// mnuViewFormat
			// 
			this.mnuViewFormat.Index = 0;
			this.mnuViewFormat.Name = "mnuViewFormat";
			this.mnuViewFormat.Text = "View Format";
			this.mnuViewFormat.Click += new System.EventHandler(this.mnuViewFormat_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuViewFormat,
				this.mnuSepar2,
				this.mnuSaveExit,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 2;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Continue";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 3;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveAndContinue
			// 
			this.cmdSaveAndContinue.AppearanceKey = "acceptButton";
			this.cmdSaveAndContinue.Location = new System.Drawing.Point(322, 30);
			this.cmdSaveAndContinue.Name = "cmdSaveAndContinue";
			this.cmdSaveAndContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAndContinue.Size = new System.Drawing.Size(180, 48);
			this.cmdSaveAndContinue.TabIndex = 0;
			this.cmdSaveAndContinue.Text = "Save & Continue";
			this.cmdSaveAndContinue.Click += new System.EventHandler(this.cmdSaveAndContinue_Click);
			// 
			// cmdViewFormat
			// 
			this.cmdViewFormat.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdViewFormat.AppearanceKey = "toolbarButton";
			this.cmdViewFormat.ImageKey = "(none)";
			this.cmdViewFormat.Location = new System.Drawing.Point(683, 29);
			this.cmdViewFormat.Name = "cmdViewFormat";
			this.cmdViewFormat.Size = new System.Drawing.Size(97, 24);
			this.cmdViewFormat.TabIndex = 1;
			this.cmdViewFormat.Text = "View Format";
			this.cmdViewFormat.Click += new System.EventHandler(this.mnuViewFormat_Click);
			// 
			// frmExport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(820, 645);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmExport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Database Extract";
			this.Load += new System.EventHandler(this.frmExport_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkHeaders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framFile)).EndInit();
			this.framFile.ResumeLayout(false);
			//((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framOrderBy)).EndInit();
			this.framOrderBy.ResumeLayout(false);
			this.framOrderBy.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framGroup)).EndInit();
			this.framGroup.ResumeLayout(false);
			this.framGroup.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdViewFormat)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndContinue;
		private FCButton cmdViewFormat;
	}
}
