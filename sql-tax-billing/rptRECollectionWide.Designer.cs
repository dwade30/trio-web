﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptRECollectionWide.
	/// </summary>
	partial class rptRECollectionWide
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRECollectionWide));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDiscount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLand,
				this.txtAccount,
				this.txtName,
				this.txtBldg,
				this.txtExempt,
				this.txtTotal,
				this.txtAddress1,
				this.txtAddress2,
				this.txtMailingAddress3,
				this.txtAddress3,
				this.Field10,
				this.txtLocation,
				this.txtMapLot,
				this.txtTax,
				this.txtAcres,
				this.txtExempt1,
				this.txtExempt2,
				this.txtExempt3,
				this.lblDiscount,
				this.txtDiscount,
				this.txtBookPage
			});
			this.Detail.Height = 1.489583F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label3,
				this.txtLandSub,
				this.txtBldgSub,
				this.txtExemptSub,
				this.txtTotalSub,
				this.txtTaxSub,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8
			});
			this.ReportFooter.Height = 0.46875F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblMuniname,
				this.lblTitle,
				this.txtPage,
				this.lblPage,
				this.txtDate,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.txtTime
			});
			this.PageHeader.Height = 0.46875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.CanGrow = false;
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLandTot,
				this.txtBldgTot,
				this.txtExemptTot,
				this.txtTotTot,
				this.txtTaxTot
			});
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.19F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "text-align: left";
			this.lblMuniname.Tag = "textbox";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
            this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 3F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Tag = "bold";
			this.lblTitle.Text = "Real Estate Collection List - ";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 4F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 9F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Tag = "textbox";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 0.9375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.19F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 8.0625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "text-align: right";
			this.lblPage.Tag = "textbox";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.15625F;
			this.lblPage.Width = 0.875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 8.875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Tag = "textbox";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Account";
			this.Label9.Top = 0.3125F;
			this.Label9.Width = 0.625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.6875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Tag = "bold";
			this.Label10.Text = "Name";
			this.Label10.Top = 0.3125F;
			this.Label10.Width = 0.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.0625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Tag = "bold";
			this.Label11.Text = "Land";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 1.25F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.375F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: right";
			this.Label12.Tag = "bold";
			this.Label12.Text = "Building";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 1.375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Tag = "bold";
			this.Label13.Text = "Exempt";
			this.Label13.Top = 0.3125F;
			this.Label13.Width = 1.25F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 7.1875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Tag = "bold";
			this.Label14.Text = "Total";
			this.Label14.Top = 0.3125F;
			this.Label14.Width = 1.375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 8.625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Tag = "bold";
			this.Label15.Text = "Tax";
			this.Label15.Top = 0.3125F;
			this.Label15.Width = 1.3125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "text-align: left";
			this.txtTime.Tag = "textbox";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.0625F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 3.0625F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.OutputFormat = resources.GetString("txtLand.OutputFormat");
			this.txtLand.Style = "font-size: 10pt; text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 0F;
			this.txtLand.Width = 1.25F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 10pt; text-align: right";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.625F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.6875F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 10pt";
			this.txtName.Tag = "textbox";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.3125F;
			// 
			// txtBldg
			// 
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 4.375F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.OutputFormat = resources.GetString("txtBldg.OutputFormat");
			this.txtBldg.Style = "font-size: 10pt; text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0F;
			this.txtBldg.Width = 1.4375F;
			// 
			// txtExempt
			// 
			this.txtExempt.DataField = "exemptval";
			this.txtExempt.Height = 0.19F;
			this.txtExempt.Left = 5.875F;
			this.txtExempt.MultiLine = false;
			this.txtExempt.Name = "txtExempt";
			this.txtExempt.OutputFormat = resources.GetString("txtExempt.OutputFormat");
			this.txtExempt.Style = "font-size: 10pt; text-align: right";
			this.txtExempt.Tag = "textbox";
			this.txtExempt.Text = null;
			this.txtExempt.Top = 0F;
			this.txtExempt.Width = 1.25F;
			// 
			// txtTotal
			// 
			this.txtTotal.DataField = "totalval";
			this.txtTotal.Height = 0.19F;
			this.txtTotal.Left = 7.1875F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat");
			this.txtTotal.Style = "font-size: 10pt; text-align: right";
			this.txtTotal.Tag = "textbox";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 1.375F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 0.6875F;
			this.txtAddress1.MultiLine = false;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-size: 10pt";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 2.3125F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 0.6875F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-size: 10pt";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 2.3125F;
			// 
			// txtMailingAddress3
			// 
			this.txtMailingAddress3.Height = 0.19F;
			this.txtMailingAddress3.Left = 0.6875F;
			this.txtMailingAddress3.MultiLine = false;
			this.txtMailingAddress3.Name = "txtMailingAddress3";
			this.txtMailingAddress3.Style = "font-size: 10pt";
			this.txtMailingAddress3.Tag = "textbox";
			this.txtMailingAddress3.Text = null;
			this.txtMailingAddress3.Top = 0.46875F;
			this.txtMailingAddress3.Width = 2.3125F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.Left = 0.6875F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Style = "font-size: 10pt";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.625F;
			this.txtAddress3.Width = 2.3125F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.19F;
			this.Field10.Left = 0.6875F;
			this.Field10.MultiLine = false;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-size: 10pt";
			this.Field10.Tag = "textbox";
			this.Field10.Text = null;
			this.Field10.Top = 0.78125F;
			this.Field10.Width = 2.3125F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 0.6875F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-size: 10pt";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.9375F;
			this.txtLocation.Width = 2.3125F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 0.6875F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-size: 10pt";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.09375F;
			this.txtMapLot.Width = 2.3125F;
			// 
			// txtTax
			// 
			this.txtTax.DataField = "taxval";
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 8.625F;
			this.txtTax.Name = "txtTax";
			this.txtTax.OutputFormat = resources.GetString("txtTax.OutputFormat");
			this.txtTax.Style = "font-size: 10pt; text-align: right";
			this.txtTax.Tag = "textbox";
			this.txtTax.Text = null;
			this.txtTax.Top = 0F;
			this.txtTax.Width = 1.3125F;
			// 
			// txtAcres
			// 
			this.txtAcres.Height = 0.19F;
			this.txtAcres.Left = 3.5625F;
			this.txtAcres.MultiLine = false;
			this.txtAcres.Name = "txtAcres";
			this.txtAcres.Style = "font-size: 10pt";
			this.txtAcres.Tag = "textbox";
			this.txtAcres.Text = null;
			this.txtAcres.Top = 0.15625F;
			this.txtAcres.Width = 1.4375F;
			// 
			// txtExempt1
			// 
			this.txtExempt1.Height = 0.19F;
			this.txtExempt1.Left = 5.875F;
			this.txtExempt1.MultiLine = false;
			this.txtExempt1.Name = "txtExempt1";
			this.txtExempt1.Style = "font-size: 10pt";
			this.txtExempt1.Tag = "textbox";
			this.txtExempt1.Text = null;
			this.txtExempt1.Top = 0.15625F;
			this.txtExempt1.Width = 2.75F;
			// 
			// txtExempt2
			// 
			this.txtExempt2.Height = 0.19F;
			this.txtExempt2.Left = 5.875F;
			this.txtExempt2.Name = "txtExempt2";
			this.txtExempt2.Style = "font-size: 10pt";
			this.txtExempt2.Tag = "textbox";
			this.txtExempt2.Text = null;
			this.txtExempt2.Top = 0.3125F;
			this.txtExempt2.Width = 2.75F;
			// 
			// txtExempt3
			// 
			this.txtExempt3.Height = 0.19F;
			this.txtExempt3.Left = 5.875F;
			this.txtExempt3.Name = "txtExempt3";
			this.txtExempt3.Style = "font-size: 10pt";
			this.txtExempt3.Tag = "textbox";
			this.txtExempt3.Text = null;
			this.txtExempt3.Top = 0.46875F;
			this.txtExempt3.Width = 2.75F;
			// 
			// lblDiscount
			// 
			this.lblDiscount.Height = 0.19F;
			this.lblDiscount.HyperLink = null;
			this.lblDiscount.Left = 9.0625F;
			this.lblDiscount.Name = "lblDiscount";
			this.lblDiscount.Style = "font-weight: bold; text-align: right";
			this.lblDiscount.Tag = "bold";
			this.lblDiscount.Text = "Discount";
			this.lblDiscount.Top = 0.15625F;
			this.lblDiscount.Width = 0.875F;
			// 
			// txtDiscount
			// 
			this.txtDiscount.Height = 0.19F;
			this.txtDiscount.Left = 9.0625F;
			this.txtDiscount.Name = "txtDiscount";
			this.txtDiscount.Style = "text-align: right";
			this.txtDiscount.Tag = "textbox";
			this.txtDiscount.Text = null;
			this.txtDiscount.Top = 0.3125F;
			this.txtDiscount.Width = 0.875F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.Height = 0.1875F;
			this.txtBookPage.Left = 0.6875F;
			this.txtBookPage.MultiLine = false;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Style = "font-size: 10pt";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.25F;
			this.txtBookPage.Width = 2.3125F;
			// 
			// txtLandTot
			// 
			this.txtLandTot.DataField = "landval";
			this.txtLandTot.Height = 0.19F;
			this.txtLandTot.Left = 1.3125F;
			this.txtLandTot.Name = "txtLandTot";
			this.txtLandTot.OutputFormat = resources.GetString("txtLandTot.OutputFormat");
			this.txtLandTot.Style = "font-size: 10pt; text-align: right";
			this.txtLandTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtLandTot.Tag = "textbox";
			this.txtLandTot.Text = null;
			this.txtLandTot.Top = 0F;
			this.txtLandTot.Visible = false;
			this.txtLandTot.Width = 1.5625F;
			// 
			// txtBldgTot
			// 
			this.txtBldgTot.DataField = "bldgval";
			this.txtBldgTot.Height = 0.19F;
			this.txtBldgTot.Left = 2.9375F;
			this.txtBldgTot.Name = "txtBldgTot";
			this.txtBldgTot.OutputFormat = resources.GetString("txtBldgTot.OutputFormat");
			this.txtBldgTot.Style = "font-size: 10pt; text-align: right";
			this.txtBldgTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtBldgTot.Tag = "textbox";
			this.txtBldgTot.Text = null;
			this.txtBldgTot.Top = 0F;
			this.txtBldgTot.Visible = false;
			this.txtBldgTot.Width = 1.8125F;
			// 
			// txtExemptTot
			// 
			this.txtExemptTot.DataField = "exemptval";
			this.txtExemptTot.Height = 0.19F;
			this.txtExemptTot.Left = 4.8125F;
			this.txtExemptTot.Name = "txtExemptTot";
			this.txtExemptTot.OutputFormat = resources.GetString("txtExemptTot.OutputFormat");
			this.txtExemptTot.Style = "font-size: 10pt; text-align: right";
			this.txtExemptTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtExemptTot.Tag = "textbox";
			this.txtExemptTot.Text = null;
			this.txtExemptTot.Top = 0F;
			this.txtExemptTot.Visible = false;
			this.txtExemptTot.Width = 1.6875F;
			// 
			// txtTotTot
			// 
			this.txtTotTot.DataField = "totalval";
			this.txtTotTot.Height = 0.19F;
			this.txtTotTot.Left = 6.5625F;
			this.txtTotTot.Name = "txtTotTot";
			this.txtTotTot.OutputFormat = resources.GetString("txtTotTot.OutputFormat");
			this.txtTotTot.Style = "font-size: 10pt; text-align: right";
			this.txtTotTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTotTot.Tag = "textbox";
			this.txtTotTot.Text = null;
			this.txtTotTot.Top = 0F;
			this.txtTotTot.Visible = false;
			this.txtTotTot.Width = 1.75F;
			// 
			// txtTaxTot
			// 
			this.txtTaxTot.DataField = "taxval";
			this.txtTaxTot.Height = 0.19F;
			this.txtTaxTot.Left = 8.375F;
			this.txtTaxTot.Name = "txtTaxTot";
			this.txtTaxTot.OutputFormat = resources.GetString("txtTaxTot.OutputFormat");
			this.txtTaxTot.Style = "font-size: 10pt; text-align: right";
			this.txtTaxTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTaxTot.Tag = "textbox";
			this.txtTaxTot.Text = null;
			this.txtTaxTot.Top = 0F;
			this.txtTaxTot.Visible = false;
			this.txtTaxTot.Width = 1.625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 10pt; font-weight: bold";
			this.Label3.Tag = "bold";
			this.Label3.Text = "Totals:";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 1.25F;
			// 
			// txtLandSub
			// 
			this.txtLandSub.Height = 0.19F;
			this.txtLandSub.Left = 1.3125F;
			this.txtLandSub.MultiLine = false;
			this.txtLandSub.Name = "txtLandSub";
			this.txtLandSub.OutputFormat = resources.GetString("txtLandSub.OutputFormat");
			this.txtLandSub.Style = "font-size: 10pt; text-align: right";
			this.txtLandSub.Tag = "textbox";
			this.txtLandSub.Text = null;
			this.txtLandSub.Top = 0.3125F;
			this.txtLandSub.Width = 1.5625F;
			// 
			// txtBldgSub
			// 
			this.txtBldgSub.Height = 0.19F;
			this.txtBldgSub.Left = 2.9375F;
			this.txtBldgSub.MultiLine = false;
			this.txtBldgSub.Name = "txtBldgSub";
			this.txtBldgSub.OutputFormat = resources.GetString("txtBldgSub.OutputFormat");
			this.txtBldgSub.Style = "font-size: 10pt; text-align: right";
			this.txtBldgSub.Tag = "textbox";
			this.txtBldgSub.Text = null;
			this.txtBldgSub.Top = 0.3125F;
			this.txtBldgSub.Width = 1.8125F;
			// 
			// txtExemptSub
			// 
			this.txtExemptSub.Height = 0.19F;
			this.txtExemptSub.Left = 4.8125F;
			this.txtExemptSub.MultiLine = false;
			this.txtExemptSub.Name = "txtExemptSub";
			this.txtExemptSub.Style = "font-size: 10pt; text-align: right";
			this.txtExemptSub.Tag = "textbox";
			this.txtExemptSub.Text = null;
			this.txtExemptSub.Top = 0.3125F;
			this.txtExemptSub.Width = 1.6875F;
			// 
			// txtTotalSub
			// 
			this.txtTotalSub.Height = 0.19F;
			this.txtTotalSub.Left = 6.5625F;
			this.txtTotalSub.MultiLine = false;
			this.txtTotalSub.Name = "txtTotalSub";
			this.txtTotalSub.Style = "font-size: 10pt; text-align: right";
			this.txtTotalSub.Tag = "textbox";
			this.txtTotalSub.Text = null;
			this.txtTotalSub.Top = 0.3125F;
			this.txtTotalSub.Width = 1.75F;
			// 
			// txtTaxSub
			// 
			this.txtTaxSub.Height = 0.19F;
			this.txtTaxSub.Left = 8.375F;
			this.txtTaxSub.MultiLine = false;
			this.txtTaxSub.Name = "txtTaxSub";
			this.txtTaxSub.Style = "font-size: 10pt; text-align: right";
			this.txtTaxSub.Tag = "textbox";
			this.txtTaxSub.Text = null;
			this.txtTaxSub.Top = 0.3125F;
			this.txtTaxSub.Width = 1.625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.5625F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label4.Tag = "bold";
			this.Label4.Text = "Land";
			this.Label4.Top = 0.15625F;
			this.Label4.Width = 1.3125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.9375F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label5.Tag = "bold";
			this.Label5.Text = "Building";
			this.Label5.Top = 0.15625F;
			this.Label5.Width = 1.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.8125F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label6.Tag = "bold";
			this.Label6.Text = "Exempt";
			this.Label6.Top = 0.15625F;
			this.Label6.Width = 1.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.5625F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label7.Tag = "bold";
			this.Label7.Text = "Total";
			this.Label7.Top = 0.15625F;
			this.Label7.Width = 1.75F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 8.8125F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label8.Tag = "bold";
			this.Label8.Text = "Tax";
			this.Label8.Top = 0.15625F;
			this.Label8.Width = 1.1875F;
			// 
			// rptRECollectionWide
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailingAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxSub;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxTot;
	}
}
