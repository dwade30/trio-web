﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormat5_6.
	/// </summary>
	partial class rptBillFormat5_6
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillFormat5_6));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCatOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessmentLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmessage7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInterest1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInterest2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPageLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaidLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLotLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIRate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIRate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessmentLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmessage7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterest1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterest2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLotLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIRate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIRate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtLand,
				this.txtBldg,
				this.lblLand,
				this.txtBldgLabel,
				this.txtCat1,
				this.txtCat2,
				this.txtCat3,
				this.txtCatOther,
				this.txtCat1label,
				this.txtCat2Label,
				this.txtCat3Label,
				this.txtOtherLabel,
				this.txtExemption,
				this.txtAssessment,
				this.txtMailing1,
				this.txtMailing2,
				this.txtMailing3,
				this.txtMailing4,
				this.txtAddress1,
				this.txtAddress2,
				this.txtAddress3,
				this.txtAddress4,
				this.txtExemptLabel,
				this.txtAssessmentLabel,
				this.txtMessage1,
				this.txtMessage4,
				this.txtMessage5,
				this.txtMessage6,
				this.txtmessage7,
				this.txtMessage2,
				this.txtMessage3,
				this.txtMessage8,
				this.txtMessage9,
				this.txtMessage10,
				this.txtMessage11,
				this.txtMessage12,
				this.txtMessage13,
				this.txtMessage14,
				this.txtMessage15,
				this.txtDueDate1,
				this.txtAmount1,
				this.txtAcct1,
				this.txtInterest1,
				this.txtDueDate2,
				this.txtAmount2,
				this.txtAcct2,
				this.txtInterest2,
				this.txtTaxDue,
				this.txtRate,
				this.txtMapLot,
				this.txtLocation,
				this.txtBookPageLabel,
				this.txtBookPage,
				this.txtDist1,
				this.txtDistPerc1,
				this.txtDist2,
				this.txtDist3,
				this.txtDist4,
				this.txtDist5,
				this.txtDistPerc2,
				this.txtDistPerc3,
				this.txtDistPerc4,
				this.txtDistPerc5,
				this.txtDistAmount1,
				this.txtDistAmount2,
				this.txtDistAmount3,
				this.txtDistAmount4,
				this.txtDistAmount5,
				this.txtPrePaidLabel,
				this.txtPrePaid,
				this.txtMapLotLabel,
				this.Field1,
				this.txtIRate1,
				this.Field2,
				this.txtIRate2,
				this.txtMailing5
			});
			this.Detail.Height = 4.916667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0.5F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 1.15625F;
			this.txtAccount.Width = 1.125F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 5.1875F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 0.28125F;
			this.txtLand.Width = 1.3125F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 5.1875F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0.4375F;
			this.txtBldg.Width = 1.3125F;
			// 
			// lblLand
			// 
			this.lblLand.CanGrow = false;
			this.lblLand.Height = 0.19F;
			this.lblLand.Left = 3.125F;
			this.lblLand.MultiLine = false;
			this.lblLand.Name = "lblLand";
			this.lblLand.Tag = "textbox";
			this.lblLand.Text = "Land";
			this.lblLand.Top = 0.28125F;
			this.lblLand.Width = 0.9375F;
			// 
			// txtBldgLabel
			// 
			this.txtBldgLabel.CanGrow = false;
			this.txtBldgLabel.Height = 0.19F;
			this.txtBldgLabel.Left = 3.125F;
			this.txtBldgLabel.MultiLine = false;
			this.txtBldgLabel.Name = "txtBldgLabel";
			this.txtBldgLabel.Tag = "textbox";
			this.txtBldgLabel.Text = "Buildings";
			this.txtBldgLabel.Top = 0.4375F;
			this.txtBldgLabel.Width = 1.0625F;
			// 
			// txtCat1
			// 
			this.txtCat1.CanGrow = false;
			this.txtCat1.Height = 0.19F;
			this.txtCat1.Left = 5.1875F;
			this.txtCat1.MultiLine = false;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.Style = "text-align: right";
			this.txtCat1.Tag = "textbox";
			this.txtCat1.Text = null;
			this.txtCat1.Top = 0.59375F;
			this.txtCat1.Width = 1.3125F;
			// 
			// txtCat2
			// 
			this.txtCat2.CanGrow = false;
			this.txtCat2.Height = 0.19F;
			this.txtCat2.Left = 5.1875F;
			this.txtCat2.MultiLine = false;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.Style = "text-align: right";
			this.txtCat2.Tag = "textbox";
			this.txtCat2.Text = null;
			this.txtCat2.Top = 0.75F;
			this.txtCat2.Width = 1.3125F;
			// 
			// txtCat3
			// 
			this.txtCat3.CanGrow = false;
			this.txtCat3.Height = 0.19F;
			this.txtCat3.Left = 5.1875F;
			this.txtCat3.MultiLine = false;
			this.txtCat3.Name = "txtCat3";
			this.txtCat3.Style = "text-align: right";
			this.txtCat3.Tag = "textbox";
			this.txtCat3.Text = null;
			this.txtCat3.Top = 0.90625F;
			this.txtCat3.Width = 1.3125F;
			// 
			// txtCatOther
			// 
			this.txtCatOther.CanGrow = false;
			this.txtCatOther.Height = 0.19F;
			this.txtCatOther.Left = 5.1875F;
			this.txtCatOther.MultiLine = false;
			this.txtCatOther.Name = "txtCatOther";
			this.txtCatOther.Style = "text-align: right";
			this.txtCatOther.Tag = "textbox";
			this.txtCatOther.Text = null;
			this.txtCatOther.Top = 1.0625F;
			this.txtCatOther.Width = 1.3125F;
			// 
			// txtCat1label
			// 
			this.txtCat1label.CanGrow = false;
			this.txtCat1label.Height = 0.19F;
			this.txtCat1label.Left = 3.125F;
			this.txtCat1label.MultiLine = false;
			this.txtCat1label.Name = "txtCat1label";
			this.txtCat1label.Tag = "textbox";
			this.txtCat1label.Text = "Field26";
			this.txtCat1label.Top = 0.59375F;
			this.txtCat1label.Width = 1.6875F;
			// 
			// txtCat2Label
			// 
			this.txtCat2Label.CanGrow = false;
			this.txtCat2Label.Height = 0.19F;
			this.txtCat2Label.Left = 3.125F;
			this.txtCat2Label.MultiLine = false;
			this.txtCat2Label.Name = "txtCat2Label";
			this.txtCat2Label.Tag = "textbox";
			this.txtCat2Label.Text = "Field27";
			this.txtCat2Label.Top = 0.75F;
			this.txtCat2Label.Width = 1.6875F;
			// 
			// txtCat3Label
			// 
			this.txtCat3Label.CanGrow = false;
			this.txtCat3Label.Height = 0.19F;
			this.txtCat3Label.Left = 3.125F;
			this.txtCat3Label.MultiLine = false;
			this.txtCat3Label.Name = "txtCat3Label";
			this.txtCat3Label.Tag = "textbox";
			this.txtCat3Label.Text = "Field28";
			this.txtCat3Label.Top = 0.90625F;
			this.txtCat3Label.Width = 1.6875F;
			// 
			// txtOtherLabel
			// 
			this.txtOtherLabel.CanGrow = false;
			this.txtOtherLabel.Height = 0.19F;
			this.txtOtherLabel.Left = 3.125F;
			this.txtOtherLabel.MultiLine = false;
			this.txtOtherLabel.Name = "txtOtherLabel";
			this.txtOtherLabel.Tag = "textbox";
			this.txtOtherLabel.Text = "Other";
			this.txtOtherLabel.Top = 1.0625F;
			this.txtOtherLabel.Width = 1.6875F;
			// 
			// txtExemption
			// 
			this.txtExemption.CanGrow = false;
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 5.1875F;
			this.txtExemption.MultiLine = false;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Tag = "textbox";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 1.21875F;
			this.txtExemption.Width = 1.3125F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.CanGrow = false;
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 5.1875F;
			this.txtAssessment.MultiLine = false;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Tag = "textbox";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 1.375F;
			this.txtAssessment.Width = 1.3125F;
			// 
			// txtMailing1
			// 
			this.txtMailing1.CanGrow = false;
			this.txtMailing1.Height = 0.19F;
			this.txtMailing1.Left = 0.25F;
			this.txtMailing1.MultiLine = false;
			this.txtMailing1.Name = "txtMailing1";
			this.txtMailing1.Tag = "textbox";
			this.txtMailing1.Text = null;
			this.txtMailing1.Top = 2.90625F;
			this.txtMailing1.Width = 3F;
			// 
			// txtMailing2
			// 
			this.txtMailing2.CanGrow = false;
			this.txtMailing2.Height = 0.19F;
			this.txtMailing2.Left = 0.25F;
			this.txtMailing2.MultiLine = false;
			this.txtMailing2.Name = "txtMailing2";
			this.txtMailing2.Tag = "textbox";
			this.txtMailing2.Text = null;
			this.txtMailing2.Top = 3.0625F;
			this.txtMailing2.Width = 3F;
			// 
			// txtMailing3
			// 
			this.txtMailing3.CanGrow = false;
			this.txtMailing3.Height = 0.19F;
			this.txtMailing3.Left = 0.25F;
			this.txtMailing3.MultiLine = false;
			this.txtMailing3.Name = "txtMailing3";
			this.txtMailing3.Tag = "textbox";
			this.txtMailing3.Text = null;
			this.txtMailing3.Top = 3.21875F;
			this.txtMailing3.Width = 3F;
			// 
			// txtMailing4
			// 
			this.txtMailing4.CanGrow = false;
			this.txtMailing4.Height = 0.19F;
			this.txtMailing4.Left = 0.25F;
			this.txtMailing4.MultiLine = false;
			this.txtMailing4.Name = "txtMailing4";
			this.txtMailing4.Tag = "textbox";
			this.txtMailing4.Text = null;
			this.txtMailing4.Top = 3.375F;
			this.txtMailing4.Width = 3F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.CanGrow = false;
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 0.25F;
			this.txtAddress1.MultiLine = false;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.125F;
			this.txtAddress1.Width = 2.3125F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.CanGrow = false;
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 0.25F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.28125F;
			this.txtAddress2.Width = 2.3125F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.CanGrow = false;
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.Left = 0.25F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.4375F;
			this.txtAddress3.Width = 2.3125F;
			// 
			// txtAddress4
			// 
			this.txtAddress4.CanGrow = false;
			this.txtAddress4.Height = 0.19F;
			this.txtAddress4.Left = 0.25F;
			this.txtAddress4.MultiLine = false;
			this.txtAddress4.Name = "txtAddress4";
			this.txtAddress4.Tag = "textbox";
			this.txtAddress4.Text = null;
			this.txtAddress4.Top = 0.59375F;
			this.txtAddress4.Width = 2.3125F;
			// 
			// txtExemptLabel
			// 
			this.txtExemptLabel.CanGrow = false;
			this.txtExemptLabel.Height = 0.19F;
			this.txtExemptLabel.Left = 3.125F;
			this.txtExemptLabel.MultiLine = false;
			this.txtExemptLabel.Name = "txtExemptLabel";
			this.txtExemptLabel.Tag = "textbox";
			this.txtExemptLabel.Text = "Exemption";
			this.txtExemptLabel.Top = 1.21875F;
			this.txtExemptLabel.Width = 1.625F;
			// 
			// txtAssessmentLabel
			// 
			this.txtAssessmentLabel.CanGrow = false;
			this.txtAssessmentLabel.Height = 0.19F;
			this.txtAssessmentLabel.Left = 3.125F;
			this.txtAssessmentLabel.MultiLine = false;
			this.txtAssessmentLabel.Name = "txtAssessmentLabel";
			this.txtAssessmentLabel.Tag = "textbox";
			this.txtAssessmentLabel.Text = "Assessment";
			this.txtAssessmentLabel.Top = 1.375F;
			this.txtAssessmentLabel.Width = 1.625F;
			// 
			// txtMessage1
			// 
			this.txtMessage1.CanGrow = false;
			this.txtMessage1.Height = 0.19F;
			this.txtMessage1.Left = 3.625F;
			this.txtMessage1.MultiLine = false;
			this.txtMessage1.Name = "txtMessage1";
			this.txtMessage1.Tag = "textbox";
			this.txtMessage1.Text = null;
			this.txtMessage1.Top = 2.09375F;
			this.txtMessage1.Width = 3.125F;
			// 
			// txtMessage4
			// 
			this.txtMessage4.CanGrow = false;
			this.txtMessage4.Height = 0.19F;
			this.txtMessage4.Left = 3.625F;
			this.txtMessage4.MultiLine = false;
			this.txtMessage4.Name = "txtMessage4";
			this.txtMessage4.Tag = "textbox";
			this.txtMessage4.Text = null;
			this.txtMessage4.Top = 2.59375F;
			this.txtMessage4.Width = 3.125F;
			// 
			// txtMessage5
			// 
			this.txtMessage5.CanGrow = false;
			this.txtMessage5.Height = 0.19F;
			this.txtMessage5.Left = 3.625F;
			this.txtMessage5.MultiLine = false;
			this.txtMessage5.Name = "txtMessage5";
			this.txtMessage5.Tag = "textbox";
			this.txtMessage5.Text = null;
			this.txtMessage5.Top = 2.75F;
			this.txtMessage5.Width = 3.125F;
			// 
			// txtMessage6
			// 
			this.txtMessage6.CanGrow = false;
			this.txtMessage6.Height = 0.19F;
			this.txtMessage6.Left = 3.625F;
			this.txtMessage6.MultiLine = false;
			this.txtMessage6.Name = "txtMessage6";
			this.txtMessage6.Tag = "textbox";
			this.txtMessage6.Text = null;
			this.txtMessage6.Top = 2.90625F;
			this.txtMessage6.Width = 3.125F;
			// 
			// txtmessage7
			// 
			this.txtmessage7.CanGrow = false;
			this.txtmessage7.Height = 0.19F;
			this.txtmessage7.Left = 3.625F;
			this.txtmessage7.MultiLine = false;
			this.txtmessage7.Name = "txtmessage7";
			this.txtmessage7.Tag = "textbox";
			this.txtmessage7.Text = null;
			this.txtmessage7.Top = 3.0625F;
			this.txtmessage7.Width = 3.125F;
			// 
			// txtMessage2
			// 
			this.txtMessage2.CanGrow = false;
			this.txtMessage2.Height = 0.1875F;
			this.txtMessage2.Left = 3.625F;
			this.txtMessage2.MultiLine = false;
			this.txtMessage2.Name = "txtMessage2";
			this.txtMessage2.Tag = "textbox";
			this.txtMessage2.Text = null;
			this.txtMessage2.Top = 2.25F;
			this.txtMessage2.Width = 3.125F;
			// 
			// txtMessage3
			// 
			this.txtMessage3.CanGrow = false;
			this.txtMessage3.Height = 0.19F;
			this.txtMessage3.Left = 3.625F;
			this.txtMessage3.MultiLine = false;
			this.txtMessage3.Name = "txtMessage3";
			this.txtMessage3.Tag = "textbox";
			this.txtMessage3.Text = null;
			this.txtMessage3.Top = 2.4375F;
			this.txtMessage3.Width = 3.125F;
			// 
			// txtMessage8
			// 
			this.txtMessage8.CanGrow = false;
			this.txtMessage8.Height = 0.19F;
			this.txtMessage8.Left = 3.625F;
			this.txtMessage8.MultiLine = false;
			this.txtMessage8.Name = "txtMessage8";
			this.txtMessage8.Tag = "textbox";
			this.txtMessage8.Text = " ";
			this.txtMessage8.Top = 3.21875F;
			this.txtMessage8.Width = 3.125F;
			// 
			// txtMessage9
			// 
			this.txtMessage9.CanGrow = false;
			this.txtMessage9.Height = 0.19F;
			this.txtMessage9.Left = 3.625F;
			this.txtMessage9.MultiLine = false;
			this.txtMessage9.Name = "txtMessage9";
			this.txtMessage9.Tag = "textbox";
			this.txtMessage9.Text = " ";
			this.txtMessage9.Top = 3.375F;
			this.txtMessage9.Width = 3.125F;
			// 
			// txtMessage10
			// 
			this.txtMessage10.CanGrow = false;
			this.txtMessage10.Height = 0.19F;
			this.txtMessage10.Left = 3.625F;
			this.txtMessage10.MultiLine = false;
			this.txtMessage10.Name = "txtMessage10";
			this.txtMessage10.Tag = "textbox";
			this.txtMessage10.Text = null;
			this.txtMessage10.Top = 3.53125F;
			this.txtMessage10.Width = 3.125F;
			// 
			// txtMessage11
			// 
			this.txtMessage11.CanGrow = false;
			this.txtMessage11.Height = 0.19F;
			this.txtMessage11.Left = 3.625F;
			this.txtMessage11.MultiLine = false;
			this.txtMessage11.Name = "txtMessage11";
			this.txtMessage11.Tag = "textbox";
			this.txtMessage11.Text = null;
			this.txtMessage11.Top = 3.6875F;
			this.txtMessage11.Width = 3.125F;
			// 
			// txtMessage12
			// 
			this.txtMessage12.CanGrow = false;
			this.txtMessage12.Height = 0.19F;
			this.txtMessage12.Left = 3.625F;
			this.txtMessage12.MultiLine = false;
			this.txtMessage12.Name = "txtMessage12";
			this.txtMessage12.Tag = "textbox";
			this.txtMessage12.Text = null;
			this.txtMessage12.Top = 3.84375F;
			this.txtMessage12.Width = 3.125F;
			// 
			// txtMessage13
			// 
			this.txtMessage13.CanGrow = false;
			this.txtMessage13.Height = 0.19F;
			this.txtMessage13.Left = 3.625F;
			this.txtMessage13.MultiLine = false;
			this.txtMessage13.Name = "txtMessage13";
			this.txtMessage13.Tag = "textbox";
			this.txtMessage13.Text = null;
			this.txtMessage13.Top = 4F;
			this.txtMessage13.Width = 3.125F;
			// 
			// txtMessage14
			// 
			this.txtMessage14.CanGrow = false;
			this.txtMessage14.Height = 0.19F;
			this.txtMessage14.Left = 3.625F;
			this.txtMessage14.MultiLine = false;
			this.txtMessage14.Name = "txtMessage14";
			this.txtMessage14.Tag = "textbox";
			this.txtMessage14.Text = null;
			this.txtMessage14.Top = 4.15625F;
			this.txtMessage14.Visible = false;
			this.txtMessage14.Width = 3.125F;
			// 
			// txtMessage15
			// 
			this.txtMessage15.CanGrow = false;
			this.txtMessage15.Height = 0.19F;
			this.txtMessage15.Left = 3.625F;
			this.txtMessage15.MultiLine = false;
			this.txtMessage15.Name = "txtMessage15";
			this.txtMessage15.Tag = "textbox";
			this.txtMessage15.Text = null;
			this.txtMessage15.Top = 4.3125F;
			this.txtMessage15.Visible = false;
			this.txtMessage15.Width = 3.125F;
			// 
			// txtDueDate1
			// 
			this.txtDueDate1.CanGrow = false;
			this.txtDueDate1.Height = 0.19F;
			this.txtDueDate1.Left = 8F;
			this.txtDueDate1.MultiLine = false;
			this.txtDueDate1.Name = "txtDueDate1";
			this.txtDueDate1.Tag = "textbox";
			this.txtDueDate1.Text = null;
			this.txtDueDate1.Top = 3.53125F;
			this.txtDueDate1.Width = 1.125F;
			// 
			// txtAmount1
			// 
			this.txtAmount1.CanGrow = false;
			this.txtAmount1.Height = 0.19F;
			this.txtAmount1.Left = 8F;
			this.txtAmount1.MultiLine = false;
			this.txtAmount1.Name = "txtAmount1";
			this.txtAmount1.Style = "text-align: left";
			this.txtAmount1.Tag = "textbox";
			this.txtAmount1.Text = null;
			this.txtAmount1.Top = 4.65625F;
			this.txtAmount1.Width = 1.125F;
			// 
			// txtAcct1
			// 
			this.txtAcct1.CanGrow = false;
			this.txtAcct1.Height = 0.19F;
			this.txtAcct1.Left = 8F;
			this.txtAcct1.MultiLine = false;
			this.txtAcct1.Name = "txtAcct1";
			this.txtAcct1.Tag = "textbox";
			this.txtAcct1.Text = null;
			this.txtAcct1.Top = 2.75F;
			this.txtAcct1.Width = 1.125F;
			// 
			// txtInterest1
			// 
			this.txtInterest1.CanGrow = false;
			this.txtInterest1.Height = 0.19F;
			this.txtInterest1.Left = 8F;
			this.txtInterest1.MultiLine = false;
			this.txtInterest1.Name = "txtInterest1";
			this.txtInterest1.Style = "text-align: left";
			this.txtInterest1.Tag = "textbox";
			this.txtInterest1.Text = null;
			this.txtInterest1.Top = 4.15625F;
			this.txtInterest1.Width = 1.125F;
			// 
			// txtDueDate2
			// 
			this.txtDueDate2.CanGrow = false;
			this.txtDueDate2.Height = 0.19F;
			this.txtDueDate2.Left = 6.8125F;
			this.txtDueDate2.MultiLine = false;
			this.txtDueDate2.Name = "txtDueDate2";
			this.txtDueDate2.Tag = "textbox";
			this.txtDueDate2.Text = null;
			this.txtDueDate2.Top = 3.53125F;
			this.txtDueDate2.Width = 1.125F;
			// 
			// txtAmount2
			// 
			this.txtAmount2.CanGrow = false;
			this.txtAmount2.Height = 0.19F;
			this.txtAmount2.Left = 6.8125F;
			this.txtAmount2.MultiLine = false;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Style = "text-align: left";
			this.txtAmount2.Tag = "textbox";
			this.txtAmount2.Text = null;
			this.txtAmount2.Top = 4.65625F;
			this.txtAmount2.Width = 1.125F;
			// 
			// txtAcct2
			// 
			this.txtAcct2.CanGrow = false;
			this.txtAcct2.Height = 0.19F;
			this.txtAcct2.Left = 6.8125F;
			this.txtAcct2.MultiLine = false;
			this.txtAcct2.Name = "txtAcct2";
			this.txtAcct2.Tag = "textbox";
			this.txtAcct2.Text = null;
			this.txtAcct2.Top = 2.75F;
			this.txtAcct2.Width = 1.125F;
			// 
			// txtInterest2
			// 
			this.txtInterest2.CanGrow = false;
			this.txtInterest2.Height = 0.19F;
			this.txtInterest2.Left = 6.8125F;
			this.txtInterest2.MultiLine = false;
			this.txtInterest2.Name = "txtInterest2";
			this.txtInterest2.Style = "text-align: left";
			this.txtInterest2.Tag = "textbox";
			this.txtInterest2.Text = null;
			this.txtInterest2.Top = 4.15625F;
			this.txtInterest2.Width = 1.125F;
			// 
			// txtTaxDue
			// 
			this.txtTaxDue.CanGrow = false;
			this.txtTaxDue.Height = 0.19F;
			this.txtTaxDue.Left = 7.6875F;
			this.txtTaxDue.MultiLine = false;
			this.txtTaxDue.Name = "txtTaxDue";
			this.txtTaxDue.Style = "text-align: right";
			this.txtTaxDue.Tag = "textbox";
			this.txtTaxDue.Text = null;
			this.txtTaxDue.Top = 1.6875F;
			this.txtTaxDue.Width = 1.3125F;
			// 
			// txtRate
			// 
			this.txtRate.CanGrow = false;
			this.txtRate.Height = 0.19F;
			this.txtRate.Left = 6.75F;
			this.txtRate.MultiLine = false;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "text-align: right";
			this.txtRate.Tag = "textbox";
			this.txtRate.Text = null;
			this.txtRate.Top = 1.375F;
			this.txtRate.Width = 0.9375F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 1.0625F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.53125F;
			this.txtMapLot.Width = 1.9375F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 1.0625F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 2.28125F;
			this.txtLocation.Width = 2.3125F;
			// 
			// txtBookPageLabel
			// 
			this.txtBookPageLabel.CanGrow = false;
			this.txtBookPageLabel.Height = 0.19F;
			this.txtBookPageLabel.Left = 0F;
			this.txtBookPageLabel.MultiLine = false;
			this.txtBookPageLabel.Name = "txtBookPageLabel";
			this.txtBookPageLabel.Tag = "textbox";
			this.txtBookPageLabel.Text = "Book & Page";
			this.txtBookPageLabel.Top = 1.6875F;
			this.txtBookPageLabel.Width = 1.125F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.CanGrow = false;
			this.txtBookPage.Height = 0.19F;
			this.txtBookPage.Left = 1.1875F;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.6875F;
			this.txtBookPage.Width = 2F;
			// 
			// txtDist1
			// 
			this.txtDist1.CanGrow = false;
			this.txtDist1.Height = 0.19F;
			this.txtDist1.Left = 0.25F;
			this.txtDist1.MultiLine = false;
			this.txtDist1.Name = "txtDist1";
			this.txtDist1.Tag = "textbox";
			this.txtDist1.Text = null;
			this.txtDist1.Top = 4.03125F;
			this.txtDist1.Width = 1F;
			// 
			// txtDistPerc1
			// 
			this.txtDistPerc1.CanGrow = false;
			this.txtDistPerc1.Height = 0.19F;
			this.txtDistPerc1.Left = 1.25F;
			this.txtDistPerc1.MultiLine = false;
			this.txtDistPerc1.Name = "txtDistPerc1";
			this.txtDistPerc1.Style = "text-align: right";
			this.txtDistPerc1.Tag = "textbox";
			this.txtDistPerc1.Text = null;
			this.txtDistPerc1.Top = 4.03125F;
			this.txtDistPerc1.Width = 0.6875F;
			// 
			// txtDist2
			// 
			this.txtDist2.CanGrow = false;
			this.txtDist2.Height = 0.19F;
			this.txtDist2.Left = 0.25F;
			this.txtDist2.MultiLine = false;
			this.txtDist2.Name = "txtDist2";
			this.txtDist2.Tag = "textbox";
			this.txtDist2.Text = " ";
			this.txtDist2.Top = 4.1875F;
			this.txtDist2.Visible = false;
			this.txtDist2.Width = 1F;
			// 
			// txtDist3
			// 
			this.txtDist3.CanGrow = false;
			this.txtDist3.Height = 0.19F;
			this.txtDist3.Left = 0.25F;
			this.txtDist3.MultiLine = false;
			this.txtDist3.Name = "txtDist3";
			this.txtDist3.Tag = "textbox";
			this.txtDist3.Text = " ";
			this.txtDist3.Top = 4.34375F;
			this.txtDist3.Visible = false;
			this.txtDist3.Width = 1F;
			// 
			// txtDist4
			// 
			this.txtDist4.CanGrow = false;
			this.txtDist4.Height = 0.19F;
			this.txtDist4.Left = 0.25F;
			this.txtDist4.MultiLine = false;
			this.txtDist4.Name = "txtDist4";
			this.txtDist4.Tag = "textbox";
			this.txtDist4.Text = " ";
			this.txtDist4.Top = 4.5F;
			this.txtDist4.Visible = false;
			this.txtDist4.Width = 1F;
			// 
			// txtDist5
			// 
			this.txtDist5.CanGrow = false;
			this.txtDist5.Height = 0.19F;
			this.txtDist5.Left = 0.25F;
			this.txtDist5.MultiLine = false;
			this.txtDist5.Name = "txtDist5";
			this.txtDist5.Tag = "textbox";
			this.txtDist5.Text = " ";
			this.txtDist5.Top = 4.65625F;
			this.txtDist5.Visible = false;
			this.txtDist5.Width = 1F;
			// 
			// txtDistPerc2
			// 
			this.txtDistPerc2.CanGrow = false;
			this.txtDistPerc2.Height = 0.19F;
			this.txtDistPerc2.Left = 1.25F;
			this.txtDistPerc2.MultiLine = false;
			this.txtDistPerc2.Name = "txtDistPerc2";
			this.txtDistPerc2.Style = "text-align: right";
			this.txtDistPerc2.Tag = "textbox";
			this.txtDistPerc2.Text = " ";
			this.txtDistPerc2.Top = 4.1875F;
			this.txtDistPerc2.Visible = false;
			this.txtDistPerc2.Width = 0.6875F;
			// 
			// txtDistPerc3
			// 
			this.txtDistPerc3.CanGrow = false;
			this.txtDistPerc3.Height = 0.19F;
			this.txtDistPerc3.Left = 1.25F;
			this.txtDistPerc3.MultiLine = false;
			this.txtDistPerc3.Name = "txtDistPerc3";
			this.txtDistPerc3.Style = "text-align: right";
			this.txtDistPerc3.Tag = "textbox";
			this.txtDistPerc3.Text = " ";
			this.txtDistPerc3.Top = 4.34375F;
			this.txtDistPerc3.Visible = false;
			this.txtDistPerc3.Width = 0.6875F;
			// 
			// txtDistPerc4
			// 
			this.txtDistPerc4.CanGrow = false;
			this.txtDistPerc4.Height = 0.19F;
			this.txtDistPerc4.Left = 1.25F;
			this.txtDistPerc4.MultiLine = false;
			this.txtDistPerc4.Name = "txtDistPerc4";
			this.txtDistPerc4.Style = "text-align: right";
			this.txtDistPerc4.Tag = "textbox";
			this.txtDistPerc4.Text = " ";
			this.txtDistPerc4.Top = 4.5F;
			this.txtDistPerc4.Visible = false;
			this.txtDistPerc4.Width = 0.6875F;
			// 
			// txtDistPerc5
			// 
			this.txtDistPerc5.CanGrow = false;
			this.txtDistPerc5.Height = 0.19F;
			this.txtDistPerc5.Left = 1.25F;
			this.txtDistPerc5.MultiLine = false;
			this.txtDistPerc5.Name = "txtDistPerc5";
			this.txtDistPerc5.Style = "text-align: right";
			this.txtDistPerc5.Tag = "textbox";
			this.txtDistPerc5.Text = " ";
			this.txtDistPerc5.Top = 4.65625F;
			this.txtDistPerc5.Visible = false;
			this.txtDistPerc5.Width = 0.6875F;
			// 
			// txtDistAmount1
			// 
			this.txtDistAmount1.CanGrow = false;
			this.txtDistAmount1.Height = 0.19F;
			this.txtDistAmount1.Left = 1.9375F;
			this.txtDistAmount1.MultiLine = false;
			this.txtDistAmount1.Name = "txtDistAmount1";
			this.txtDistAmount1.Style = "text-align: right";
			this.txtDistAmount1.Tag = "textbox";
			this.txtDistAmount1.Text = null;
			this.txtDistAmount1.Top = 4.03125F;
			this.txtDistAmount1.Visible = false;
			this.txtDistAmount1.Width = 0.875F;
			// 
			// txtDistAmount2
			// 
			this.txtDistAmount2.CanGrow = false;
			this.txtDistAmount2.Height = 0.19F;
			this.txtDistAmount2.Left = 1.9375F;
			this.txtDistAmount2.MultiLine = false;
			this.txtDistAmount2.Name = "txtDistAmount2";
			this.txtDistAmount2.Style = "text-align: right";
			this.txtDistAmount2.Tag = "textbox";
			this.txtDistAmount2.Text = " ";
			this.txtDistAmount2.Top = 4.1875F;
			this.txtDistAmount2.Visible = false;
			this.txtDistAmount2.Width = 0.875F;
			// 
			// txtDistAmount3
			// 
			this.txtDistAmount3.CanGrow = false;
			this.txtDistAmount3.Height = 0.19F;
			this.txtDistAmount3.Left = 1.9375F;
			this.txtDistAmount3.MultiLine = false;
			this.txtDistAmount3.Name = "txtDistAmount3";
			this.txtDistAmount3.Style = "text-align: right";
			this.txtDistAmount3.Tag = "textbox";
			this.txtDistAmount3.Text = " ";
			this.txtDistAmount3.Top = 4.34375F;
			this.txtDistAmount3.Visible = false;
			this.txtDistAmount3.Width = 0.875F;
			// 
			// txtDistAmount4
			// 
			this.txtDistAmount4.CanGrow = false;
			this.txtDistAmount4.Height = 0.19F;
			this.txtDistAmount4.Left = 1.9375F;
			this.txtDistAmount4.MultiLine = false;
			this.txtDistAmount4.Name = "txtDistAmount4";
			this.txtDistAmount4.Style = "text-align: right";
			this.txtDistAmount4.Tag = "textbox";
			this.txtDistAmount4.Text = " ";
			this.txtDistAmount4.Top = 4.5F;
			this.txtDistAmount4.Visible = false;
			this.txtDistAmount4.Width = 0.875F;
			// 
			// txtDistAmount5
			// 
			this.txtDistAmount5.CanGrow = false;
			this.txtDistAmount5.Height = 0.19F;
			this.txtDistAmount5.Left = 1.9375F;
			this.txtDistAmount5.MultiLine = false;
			this.txtDistAmount5.Name = "txtDistAmount5";
			this.txtDistAmount5.Style = "text-align: right";
			this.txtDistAmount5.Tag = "textbox";
			this.txtDistAmount5.Text = " ";
			this.txtDistAmount5.Top = 4.65625F;
			this.txtDistAmount5.Visible = false;
			this.txtDistAmount5.Width = 0.875F;
			// 
			// txtPrePaidLabel
			// 
			this.txtPrePaidLabel.CanGrow = false;
			this.txtPrePaidLabel.Height = 0.19F;
			this.txtPrePaidLabel.Left = 3.5F;
			this.txtPrePaidLabel.MultiLine = false;
			this.txtPrePaidLabel.Name = "txtPrePaidLabel";
			this.txtPrePaidLabel.Tag = "textbox";
			this.txtPrePaidLabel.Text = "Paid to Date";
			this.txtPrePaidLabel.Top = 1.6875F;
			this.txtPrePaidLabel.Width = 1.25F;
			// 
			// txtPrePaid
			// 
			this.txtPrePaid.CanGrow = false;
			this.txtPrePaid.Height = 0.19F;
			this.txtPrePaid.Left = 4.9375F;
			this.txtPrePaid.MultiLine = false;
			this.txtPrePaid.Name = "txtPrePaid";
			this.txtPrePaid.Tag = "textbox";
			this.txtPrePaid.Text = null;
			this.txtPrePaid.Top = 1.6875F;
			this.txtPrePaid.Width = 1.5625F;
			// 
			// txtMapLotLabel
			// 
			this.txtMapLotLabel.CanGrow = false;
			this.txtMapLotLabel.Height = 0.19F;
			this.txtMapLotLabel.Left = 0F;
			this.txtMapLotLabel.MultiLine = false;
			this.txtMapLotLabel.Name = "txtMapLotLabel";
			this.txtMapLotLabel.Tag = "textbox";
			this.txtMapLotLabel.Text = "Map/Lot";
			this.txtMapLotLabel.Top = 1.53125F;
			this.txtMapLotLabel.Width = 0.9375F;
			// 
			// Field1
			// 
			this.Field1.CanGrow = false;
			this.Field1.Height = 0.19F;
			this.Field1.Left = 8F;
			this.Field1.MultiLine = false;
			this.Field1.Name = "Field1";
			this.Field1.Tag = "textbox";
			this.Field1.Text = "Int. Rate";
			this.Field1.Top = 2.90625F;
			this.Field1.Width = 1.125F;
			// 
			// txtIRate1
			// 
			this.txtIRate1.CanGrow = false;
			this.txtIRate1.Height = 0.19F;
			this.txtIRate1.Left = 8F;
			this.txtIRate1.MultiLine = false;
			this.txtIRate1.Name = "txtIRate1";
			this.txtIRate1.Tag = "textbox";
			this.txtIRate1.Text = null;
			this.txtIRate1.Top = 3.0625F;
			this.txtIRate1.Width = 1.125F;
			// 
			// Field2
			// 
			this.Field2.CanGrow = false;
			this.Field2.Height = 0.19F;
			this.Field2.Left = 6.8125F;
			this.Field2.MultiLine = false;
			this.Field2.Name = "Field2";
			this.Field2.Tag = "textbox";
			this.Field2.Text = "Int. Rate";
			this.Field2.Top = 2.90625F;
			this.Field2.Width = 1.125F;
			// 
			// txtIRate2
			// 
			this.txtIRate2.CanGrow = false;
			this.txtIRate2.Height = 0.19F;
			this.txtIRate2.Left = 6.8125F;
			this.txtIRate2.MultiLine = false;
			this.txtIRate2.Name = "txtIRate2";
			this.txtIRate2.Tag = "textbox";
			this.txtIRate2.Text = null;
			this.txtIRate2.Top = 3.0625F;
			this.txtIRate2.Width = 1.125F;
			// 
			// txtMailing5
			// 
			this.txtMailing5.CanGrow = false;
			this.txtMailing5.Height = 0.19F;
			this.txtMailing5.Left = 0.25F;
			this.txtMailing5.MultiLine = false;
			this.txtMailing5.Name = "txtMailing5";
			this.txtMailing5.Tag = "textbox";
			this.txtMailing5.Text = null;
			this.txtMailing5.Top = 3.53125F;
			this.txtMailing5.Width = 3F;
			// 
			// rptBillFormat5_6
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.197917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessmentLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmessage7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterest1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterest2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLotLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIRate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIRate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessmentLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmessage7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInterest1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInterest2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPageLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaidLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLotLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIRate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIRate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing5;
	}
}
