﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPNotBilled.
	/// </summary>
	public partial class rptPPNotBilled : BaseSectionReport
	{
		public rptPPNotBilled()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Personal Property Accounts Not Billed";
		}

		public static rptPPNotBilled InstancePtr
		{
			get
			{
				return (rptPPNotBilled)Sys.GetInstance(typeof(rptPPNotBilled));
			}
		}

		protected rptPPNotBilled _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsBill.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPNotBilled	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsBill = new clsDRWrapper();
		int lngYear;
		cPartyController tPCont = new cPartyController();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsBill.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strResponse = "";
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "Report Start";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = DateTime.Today.ToString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblTitle.Text = lblTitle.Text + FCConvert.ToString(lngYear);
			// must build a querydef to handle this sql statement
			clsBill.OpenRecordset("select * from ppmaster left join (SELECT * From " + clsBill.CurrentPrefix + "Collections.dbo.billingmaster WHERE billingtype = 'PP' and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ") as ppbillingquery on (ppbillingquery.account = ppmaster.account) where isnull(ppmaster.deleted, 0) = 0 and isnull(ppbillingquery.account, -1) = -1 AND PPMASTER.ACCOUNT > 0 order by ppmaster.account", "twpp0000.vb1");
			if (clsBill.EndOfFile())
			{
				MessageBox.Show("No accounts found.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsBill.EndOfFile())
			{
				cParty tParty;
				tParty = tPCont.GetParty(FCConvert.ToInt32(Conversion.Val(clsBill.Get_Fields_Int32("partyid"))));
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = FCConvert.ToString(clsBill.Get_Fields("account"));
				txtName.Text = "";
				if (!(tParty == null))
				{
					txtName.Text = tParty.FullNameLastFirst;
				}
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				txtAssessment.Text = Strings.Format(Conversion.Val(clsBill.Get_Fields("value")) - Conversion.Val(clsBill.Get_Fields_Int32("exemption")), "###,###,###,##0");
				clsBill.MoveNext();
			}
		}
		// vbPorter upgrade warning: intYear As short	OnWriteFCConvert.ToDouble(
		public void Init(int intYear)
		{
			lngYear = intYear;
			// Me.Show , MDIParent
			;
			//FC:FINAL:CHN - i.issue #1544: error when printing errors.
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "PPNotBilled");
		}

		
	}
}
