﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmAuditInfo.
	/// </summary>
	public partial class frmAuditInfo : BaseForm
	{
		public frmAuditInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.frPeriod = new System.Collections.Generic.List<fecherFoundation.FCFrame>();
			this.txtWeight = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.t2kInterestDate = new System.Collections.Generic.List<T2KDateBox>();
			this.t2kDueDate = new System.Collections.Generic.List<T2KDateBox>();
			this.Label5 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblWeight = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblDDate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblIDate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.frPeriod.AddControlArrayElement(frPeriod_3, 3);
			this.frPeriod.AddControlArrayElement(frPeriod_2, 2);
			this.frPeriod.AddControlArrayElement(frPeriod_1, 1);
			this.frPeriod.AddControlArrayElement(frPeriod_0, 0);
			this.txtWeight.AddControlArrayElement(txtWeight_3, 3);
			this.txtWeight.AddControlArrayElement(txtWeight_2, 2);
			this.txtWeight.AddControlArrayElement(txtWeight_1, 1);
			this.txtWeight.AddControlArrayElement(txtWeight_0, 0);
			this.t2kInterestDate.AddControlArrayElement(t2kInterestDate_3, 3);
			this.t2kInterestDate.AddControlArrayElement(t2kInterestDate_2, 2);
			this.t2kInterestDate.AddControlArrayElement(t2kInterestDate_1, 1);
			this.t2kInterestDate.AddControlArrayElement(t2kInterestDate_0, 0);
			this.t2kDueDate.AddControlArrayElement(t2kDueDate_3, 3);
			this.t2kDueDate.AddControlArrayElement(t2kDueDate_2, 2);
			this.t2kDueDate.AddControlArrayElement(t2kDueDate_1, 1);
			this.t2kDueDate.AddControlArrayElement(t2kDueDate_0, 0);
			this.Label5.AddControlArrayElement(Label5_3, 3);
			this.Label5.AddControlArrayElement(Label5_2, 2);
			this.Label5.AddControlArrayElement(Label5_1, 1);
			this.Label5.AddControlArrayElement(Label5_0, 0);
			this.lblWeight.AddControlArrayElement(lblWeight_3, 3);
			this.lblWeight.AddControlArrayElement(lblWeight_2, 2);
			this.lblWeight.AddControlArrayElement(lblWeight_1, 1);
			this.lblWeight.AddControlArrayElement(lblWeight_0, 0);
			this.lblDDate.AddControlArrayElement(lblDDate_3, 3);
			this.lblDDate.AddControlArrayElement(lblDDate_2, 2);
			this.lblDDate.AddControlArrayElement(lblDDate_1, 1);
			this.lblDDate.AddControlArrayElement(lblDDate_0, 0);
			this.lblIDate.AddControlArrayElement(lblIDate_3, 3);
			this.lblIDate.AddControlArrayElement(lblIDate_2, 2);
			this.lblIDate.AddControlArrayElement(lblIDate_1, 1);
			this.lblIDate.AddControlArrayElement(lblIDate_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAuditInfo InstancePtr
		{
			get
			{
				return (frmAuditInfo)Sys.GetInstance(typeof(frmAuditInfo));
			}
		}

		protected frmAuditInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolUseChange;
		int intCalledFrom;
		public int REorPP;
		clsRateRecord clsrate = new clsRateRecord();
		public int lngRKey;
		bool boolChanged;
		bool boolNotAllowedToChange;
		// vbPorter upgrade warning: intREorPP As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		public void Init(int intWhereFrom, int intREorPP = 0)
		{
			intCalledFrom = intWhereFrom;
			if (!Information.IsNothing(intREorPP))
			{
				REorPP = intREorPP;
			}
			else
			{
				REorPP = 0;
			}
			modGlobalVariables.Statics.CancelledIt = false;
			frmRateforAuditXfer.InstancePtr.Init(intCalledFrom);
			if (modGlobalVariables.Statics.CancelledIt)
			{
				modGlobalVariables.Statics.CancelledIt = false;
				this.Close();
				return;
			}
			//FC:FINAL:MSH - i.issue #1443: init combobox for correct call LoadTaxInfo method 
			InitForm();
			//FC:FINAL:DDU:#1331 - show meaasgebox correctly
			this.Show(App.MainForm);
			boolUseChange = false;
			LoadTaxInfo(lngRKey);
			boolUseChange = true;
		}

		private bool CheckIfOkay()
		{
			bool CheckIfOkay = false;
			int lngRet;
			CheckIfOkay = false;
			if (boolNotAllowedToChange)
			{
				if (MessageBox.Show("This rate record is already used by at least one bill." + "\r\n" + "Are you sure you want to edit it?", "Rate record in use", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					if (MessageBox.Show("Tax amount will not change automatically. The transfer will have to be redone to recalculate tax.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
					{
						modGlobalFunctions.AddCYAEntry_26("CL", "Billing", "Changed RateRec" + FCConvert.ToString(lngRKey));
						boolNotAllowedToChange = false;
						CheckIfOkay = true;
						// lngRet = Val(InputBox("In order to edit this rate record call TRIO to obtain today's Billing override code.", "Enter Override Code"))
						// If CheckWinModCode(lngRet, "BL") Then
						// Call clsTemp.Execute("insert into CYATable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'OV Bills','Tax Billing overwrote RE bills for tax year " & Left(clsRateInfo.TaxYear, 4) & "')", "twbl0000.vb1")
						// boolNotAllowedToChange = False
						// CheckIfOkay = True
						// Else
						// MsgBox "That code is invalid", vbExclamation, "Invalid Code"
						// Exit Function
						// End If
					}
				}
				else
				{
					return CheckIfOkay;
				}
			}
			else
			{
				CheckIfOkay = true;
			}
			return CheckIfOkay;
		}

		private void cmbPeriods_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int x;
			int intP;
			intP = cmbPeriods.SelectedIndex + 1;
			if (intP == 1)
			{
				txtWeight[0].Text = FCConvert.ToString(100);
				txtWeight[1].Text = "";
				txtWeight[2].Text = "";
				txtWeight[3].Text = "";
			}
			else if (intP == 2)
			{
				txtWeight[0].Text = FCConvert.ToString(50);
				txtWeight[1].Text = FCConvert.ToString(50);
				txtWeight[2].Text = "";
				txtWeight[3].Text = "";
			}
			else if (intP == 3)
			{
				txtWeight[0].Text = FCConvert.ToString(34);
				txtWeight[1].Text = FCConvert.ToString(33);
				txtWeight[2].Text = FCConvert.ToString(33);
			}
			else if (intP == 4)
			{
				txtWeight[0].Text = FCConvert.ToString(25);
				txtWeight[1].Text = FCConvert.ToString(25);
				txtWeight[2].Text = FCConvert.ToString(25);
				txtWeight[3].Text = FCConvert.ToString(25);
			}
			for (x = 1; x <= cmbPeriods.Items.Count - 1; x++)
			{
				if (x <= cmbPeriods.SelectedIndex)
				{
					frPeriod[FCConvert.ToInt16(x)].Enabled = true;
					// txtIDay(X).Enabled = True
					// txtIMonth(X).Enabled = True
					// txtIYear(X).Enabled = True
					t2kInterestDate[x].Enabled = true;
					// txtDDay(X).Enabled = True
					// txtDMonth(X).Enabled = True
					// txtDYear(X).Enabled = True
					t2kDueDate[x].Enabled = true;
					lblIDate[FCConvert.ToInt16(x)].Enabled = true;
					lblDDate[FCConvert.ToInt16(x)].Enabled = true;
				}
				else
				{
					frPeriod[FCConvert.ToInt16(x)].Enabled = false;
					// txtIDay(X).Enabled = False
					// txtIMonth(X).Enabled = False
					// txtIYear(X).Enabled = False
					t2kInterestDate[x].Enabled = false;
					// txtDDay(X).Enabled = False
					// txtDMonth(X).Enabled = False
					// txtDYear(X).Enabled = False
					t2kDueDate[x].Enabled = false;
					lblIDate[FCConvert.ToInt16(x)].Enabled = false;
					lblDDate[FCConvert.ToInt16(x)].Enabled = false;
				}
			}
			// x
		}

		private void frmAuditInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuCancel_Click();
			}
		}

		private void frmAuditInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl is FCTextBox || this.ActiveControl is FCComboBox || this.ActiveControl is Global.T2KDateBox)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			else if (KeyAscii == Keys.F16 || KeyAscii == Keys.Back)
			{
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmAuditInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAuditInfo properties;
			//frmAuditInfo.ScaleWidth	= 9045;
			//frmAuditInfo.ScaleHeight	= 7320;
			//frmAuditInfo.LinkTopic	= "Form1";
			//frmAuditInfo.LockControls	= true;
			//End Unmaped Properties
			t2kBilldate.Appearance(1);
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			Label24.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			boolUseChange = false;
			if (Strings.LCase(modGlobalConstants.Statics.MuniName) != "saint george")
			{
				int x;
				for (x = 0; x <= 3; x++)
				{
					Label5[FCConvert.ToInt16(x)].Visible = false;
					txtWeight[FCConvert.ToInt16(x)].Visible = false;
					lblWeight[FCConvert.ToInt16(x)].Visible = false;
				}
				// x
			}
			// boolUseChange = True
			if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTEDITTAXRATES, false))
			{
				// must not allow user to edit anything
				txtTaxYear.ReadOnly = true;
				txtDescription.ReadOnly = true;
				txtTaxRate.ReadOnly = true;
				txtInterestRate.ReadOnly = true;
				t2kCommitment.Locked = true;
				t2kBilldate.Locked = true;
				cmbPeriods.Enabled /*Locked*/ = true;
				t2kInterestDate[0].Locked = true;
				t2kInterestDate[1].Locked = true;
				t2kInterestDate[2].Locked = true;
				t2kInterestDate[3].Locked = true;
				t2kDueDate[0].Locked = true;
				t2kDueDate[1].Locked = true;
				t2kDueDate[2].Locked = true;
				t2kDueDate[3].Locked = true;
				txtWeight[0].ReadOnly = true;
				txtWeight[1].ReadOnly = true;
				txtWeight[2].ReadOnly = true;
				txtWeight[3].ReadOnly = true;
			}
		}

		private void mnuExit_Click()
		{
			Close();
		}

		private void InitForm()
		{
			//FC:FINAL:MSH - i.issue #1443: clear combobox to prevent multi adding elements
			cmbPeriods.Clear();
			cmbPeriods.AddItem("1");
			cmbPeriods.AddItem("2");
			cmbPeriods.AddItem("3");
			cmbPeriods.AddItem("4");
			cmbPeriods.SelectedIndex = 0;
		}

		private void mnuBack_Click(object sender, System.EventArgs e)
		{
			frmRateforAuditXfer.InstancePtr.Init(intCalledFrom);
			boolUseChange = false;
			LoadTaxInfo(lngRKey);
			boolUseChange = true;
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuCancel_Click()
		{
			mnuCancel_Click(mnuCancel, new System.EventArgs());
		}
		// vbPorter upgrade warning: lngRateKey As Variant --> As int
		private void LoadTaxInfo(int lngRateKey = 0)
		{
			clsDRWrapper clsRateRec = new clsDRWrapper();
			clsDRWrapper clsYear = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngKey = 0;
			// vbPorter upgrade warning: x As short --> As int	OnWrite(short, double)
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				boolNotAllowedToChange = false;
				boolUseChange = false;
				if (Information.IsNothing(lngRateKey))
				{
					lngKey = 0;
				}
				else
				{
					lngKey = lngRateKey;
				}
				clsYear.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
				if (clsYear.EndOfFile())
				{
					clsYear.AddNew();
				}
				else
				{
					clsYear.Edit();
				}
				if (lngKey == 0)
				{
					txtTaxRate.Text = "1.00";
					txtTaxYear.Text = FCConvert.ToString(clsYear.Get_Fields_Int16("defaultyear"));
					txtDescription.Text = "";
					clsrate.RateKey = 0;
				}
				else
				{
					clsRateRec.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngKey), "twcl0000.vb1");
					if (clsRateRec.EndOfFile())
					{
						txtTaxRate.Text = "1.00";
						txtTaxYear.Text = FCConvert.ToString(clsYear.Get_Fields_Int16("defaultyear"));
						txtDescription.Text = "";
						clsrate.RateKey = 0;
					}
					else
					{
						clsrate.LoadRate(clsRateRec.Get_Fields_Int32("ID"));
						txtDescription.Text = Strings.Trim(clsRateRec.Get_Fields_String("description"));
						if (txtDescription.Text == string.Empty)
							txtDescription.Text = " ";
						// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
						if ((clsRateRec.Get_Fields_String("year")).Length > 4)
						{
							// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
							txtTaxYear.Text = FCConvert.ToString(clsRateRec.Get_Fields("year") / 10);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
							txtTaxYear.Text = FCConvert.ToString(clsRateRec.Get_Fields("year"));
						}
						clsYear.Set_Fields("defaultyear", FCConvert.ToString(Conversion.Val(txtTaxYear.Text)));
						txtTaxRate.Text = FCConvert.ToString(clsRateRec.Get_Fields_Double("taxrate") * 1000);
						cmbPeriods.SelectedIndex = FCConvert.ToInt32(Conversion.Val(clsRateRec.Get_Fields_Int16("numberofperiods")) - 1);
						if (FCConvert.ToDateTime(clsRateRec.Get_Fields_DateTime("billingdate") as object).ToOADate() != 0)
						{
							// txtBillDay.Text = Day(clsRateRec.Fields("billingdate"))
							// txtBillMonth.Text = Month(clsRateRec.Fields("billingdate"))
							// txtBillYear.Text = Year(clsRateRec.Fields("billingdate"))
							t2kBilldate.Text = Strings.Format(clsRateRec.Get_Fields_DateTime("billingdate"), "MM/dd/yyyy");
						}
						for (x = 0; x <= FCConvert.ToInt32(Conversion.Val(clsRateRec.Get_Fields_Int16("numberofperiods")) - 1); x++)
						{
							if (FCConvert.ToDateTime(clsRateRec.Get_Fields("intereststartdate" + FCConvert.ToString(x + 1)) as object).ToOADate() != 0)
							{
								// txtIMonth(X).Text = Month(clsRateRec.Fields("intereststartdate" & X + 1))
								// txtIDay(X).Text = Day(clsRateRec.Fields("intereststartdate" & X + 1))
								// txtIYear(X).Text = Year(clsRateRec.Fields("intereststartdate" & X + 1))
								t2kInterestDate[x].Text = Strings.Format(clsRateRec.Get_Fields_DateTime("intereststartdate" + FCConvert.ToString(x + 1)), "MM/dd/yyyy");
							}
							// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
							if (FCConvert.ToDateTime(clsRateRec.Get_Fields("duedate" + FCConvert.ToString(x + 1)) as object).ToOADate() != 0)
							{
								// txtDMonth(X).Text = Month(clsRateRec.Fields("duedate" & X + 1))
								// txtDDay(X).Text = Day(clsRateRec.Fields("duedate" & X + 1))
								// txtDYear(X).Text = Year(clsRateRec.Fields("duedate" & X + 1))
								// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
								t2kDueDate[x].Text = Strings.Format(clsRateRec.Get_Fields_DateTime("duedate" + FCConvert.ToString(x + 1)), "MM/dd/yyyy");
							}
							// TODO Get_Fields: Field [Weight] not found!! (maybe it is an alias?)
							txtWeight[FCConvert.ToInt16(x)].Text = FCConvert.ToString(100 * Conversion.Val(clsRateRec.Get_Fields("Weight" + FCConvert.ToString(x + 1))));
						}
						// x
						if (FCConvert.ToDateTime(clsRateRec.Get_Fields_DateTime("commitmentdate") as object).ToOADate() != 0)
						{
							if (FCConvert.ToString(clsRateRec.Get_Fields_DateTime("commitmentdate")) == string.Empty)
							{
								t2kCommitment.Text = t2kBilldate.Text;
							}
							else
							{
								t2kCommitment.Text = Strings.Format(clsRateRec.Get_Fields_DateTime("commitmentdate"), "MM/dd/yyyy");
							}
						}
						txtInterestRate.Text = FCConvert.ToString(clsRateRec.Get_Fields_Double("interestrate") * 100);
						mnuContinue.Enabled = true;
						clsTemp.OpenRecordset("select top 1 ACCOUNT from billingmaster where ratekey = " + clsRateRec.Get_Fields_Int32("ID"), "twcl0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							// ratekey has been used so we can't just let them change it
							boolNotAllowedToChange = true;
							txtTaxYear.Enabled = false;
						}
					}
				}
				clsYear.Update();
				boolUseChange = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadTaxInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			// Dim clsRate As New clsRateRecord
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper clsYear = new clsDRWrapper();
			int x;
			if (!CheckEntries())
			{
				// mnuContinue.Enabled = False
				return;
			}
			if (!SaveRateRec())
			{
				return;
			}
			if (clsrate.RateKey == 0)
			{
				// Was loaded, not just entered so this is a regular ratetype
				clsTemp.OpenRecordset("select * from raterec where year = " + txtTaxYear.Text + " and ratetype = 'R'", "twcl0000.vb1");
			}
			else
			{
				clsTemp.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(clsrate.RateKey), "twcl0000.vb1");
				// Call clsTemp.OpenRecordset("select * from raterec where year = " & txtTaxYear.Text & " and ratetype = '" & clsRate.RateType & "'", "twcl0000.vb1")
			}
			clsrate.RateKey = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("ID"));
			clsrate.TaxYear = FCConvert.ToInt16(txtTaxYear.Text);
			clsYear.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
			if (clsYear.EndOfFile())
			{
				clsYear.AddNew();
			}
			else
			{
				clsYear.Edit();
			}
			clsYear.Set_Fields("defaultyear", clsrate.TaxYear);
			clsYear.Update();
			clsrate.TaxRate = FCConvert.ToDouble(txtTaxRate.Text) / 1000;
			clsrate.NumberOfPeriods = FCConvert.ToInt16(cmbPeriods.SelectedIndex + 1);
			// clsRate.BillingDate = txtBillMonth.Text & "/" & txtBillDay.Text & "/" & txtBillYear.Text
			clsrate.BillingDate = FCConvert.ToDateTime(t2kBilldate.Text);
			clsrate.InterestRate = FCConvert.ToSingle(FCConvert.ToDouble(txtInterestRate.Text)) / 100;
			clsrate.CommitmentDate = FCConvert.ToDateTime(t2kCommitment.Text);
			if (clsrate.RateType == string.Empty)
			{
				if (intCalledFrom != 5)
				{
					clsrate.RateType = "R";
				}
				else
				{
					clsrate.RateType = "S";
				}
			}
			for (x = 0; x <= cmbPeriods.SelectedIndex; x++)
			{
				// clsRate.Get_InterestStartDate(X + 1) = txtIMonth(X).Text & "/" & txtIDay(X).Text & "/" & txtIYear(X).Text
				clsrate.Set_InterestStartDate(x + 1, DateAndTime.DateValue(t2kInterestDate[x].Text));
				// clsRate.Get_DueDate(X + 1) = txtDMonth(X).Text & "/" & txtDDay(X).Text & "/" & txtDYear(X).Text
				clsrate.Set_DueDate(x + 1, DateAndTime.DateValue(t2kDueDate[x].Text));
				clsrate.Set_BillWeight(x + 1, FCConvert.ToDouble(Conversion.Val(txtWeight[FCConvert.ToInt16(x)].Text)) / 100);
			}
			// x
			// zero out any date you won't be using
			for (x = cmbPeriods.SelectedIndex + 1; x <= 3; x++)
			{
				clsrate.Set_InterestStartDate(x + 1, FCConvert.ToDateTime(0));
				clsrate.Set_DueDate(x + 1, FCConvert.ToDateTime(0));
				clsrate.Set_BillWeight(x + 1, 0);
			}
			// x
			Close();
			switch (intCalledFrom)
			{
				case 1:
				case 2:
				case 3:
				case 4:
					{
						frmPickREorPP.InstancePtr.Init(clsrate.RateKey, intCalledFrom);
						// Case 1, 3
						// Real Estate Audit or transfer
						// Load frmREAudit
						// Call frmREAudit.Init(clsRate, intCalledFrom)
						// Case 2, 4
						// Personal Property Audit or transfer
						// Load frmPPAudit
						// Call frmPPAudit.Init(clsRate, intCalledFrom)
						break;
					}
				case 5:
					{
						// supplemental billing
						if (REorPP == 1)
						{
							// Real Estate
							frmSupplementalBill.InstancePtr.FillRERate(clsrate);
							modREPPBudStuff.LoadREPPAccountInfo_2(clsrate.TaxYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
							frmSupplementalBill.InstancePtr.MakeRESupplemental();
						}
						else if (REorPP == 2)
						{
							// Personal Property
							frmSupplementalBill.InstancePtr.FillPPRate(clsrate);
							modREPPBudStuff.LoadREPPAccountInfo_2(clsrate.TaxYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
							frmSupplementalBill.InstancePtr.MakePPSupplemental();
						}
						else if (REorPP == 3)
						{
							// real estate but not making a bill, just changing rate
							frmSupplementalBill.InstancePtr.FillRERate(clsrate);
							modREPPBudStuff.LoadREPPAccountInfo_2(clsrate.TaxYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
						}
						else if (REorPP == 4)
						{
							frmSupplementalBill.InstancePtr.FillPPRate(clsrate);
							modREPPBudStuff.LoadREPPAccountInfo_2(clsrate.TaxYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
						}
						else if (REorPP == 5)
						{
							// real estate but doing a transfer of values
							frmSupplementalBill.InstancePtr.FillRERate(clsrate);
							frmSupplementalBill.InstancePtr.mnuTranRE_Click();
						}
						else if (REorPP == 6)
						{
							// pp doing a transfer of values
							frmSupplementalBill.InstancePtr.FillPPRate(clsrate);
							frmSupplementalBill.InstancePtr.mnuTranPP_Click();
						}
						break;
					}
			}
			//end switch
		}

		private bool SaveRateRec()
		{
			bool SaveRateRec = false;
			clsDRWrapper clsRateRec = new clsDRWrapper();
			int x;
			bool boolAddNew = false;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp;
			string strTemp2;
			string strTemp3;
			string strTemp4;
			string strTemp5;
			try
			{
				// On Error GoTo ErrorHandler
				SaveRateRec = false;
				if (!CheckEntries())
				{
					// mnuContinue.Enabled = False
					return SaveRateRec;
				}
				strTemp = "Edited Rate,Key " + FCConvert.ToString(lngRKey) + ",Tax " + FCConvert.ToString(clsrate.TaxRate) + ",IntRate " + FCConvert.ToString(clsrate.InterestRate) + ",Periods " + FCConvert.ToString(clsrate.NumberOfPeriods);
				strTemp2 = "Commitment date " + Strings.Format(clsrate.CommitmentDate, "MM/dd/yyyy") + ",Bill date " + Strings.Format(clsrate.BillingDate, "MM/dd/yyyy");
				strTemp3 = "Due date 1 " + Strings.Format(clsrate.Get_DueDate(1), "MM/dd/yyyy") + ",Due 2 " + Strings.Format(clsrate.Get_DueDate(2), "MM/dd/yyyy");
				strTemp4 = "Due 3 " + Strings.Format(clsrate.Get_DueDate(3), "MM/dd/yyyy") + ",Due 4 " + Strings.Format(clsrate.Get_DueDate(4), "MM/dd/yyyy");
				strTemp5 = "IntDate 1 " + Strings.Format(clsrate.Get_InterestStartDate(1), "MM/dd/yyyy") + ",IntDate 2 " + Strings.Format(clsrate.Get_InterestStartDate(2), "MM/dd/yyyy");
				// strTemp4 = strTemp4 & ",Interest Date 3 " & clsrate.Get_InterestStartDate(3) & ",Interest Date 4 " & clsrate.Get_InterestStartDate(4)
				clsrate = new clsRateRecord();
				// check if this is a supplemental rate or a regular
				if (intCalledFrom != 5)
				{
					if (lngRKey > 0)
					{
						// Call clsRateRec.OpenRecordset("select * from raterec where [year] = " & Val(txtTaxYear.Text) & " and ratetype = 'R'", "twcl0000.vb1")
						clsRateRec.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngRKey), "twcl0000.vb1");
					}
					else
					{
						// new but can only be one Regular raterec per year
						// Call clsRateRec.OpenRecordset("select * from raterec where [year] = " & Val(txtTaxYear.Text) & " and ratetype = 'R'", "twcl0000.vb1")
						// don't limit to one regular raterec per year
						clsRateRec.OpenRecordset("select * from raterec where [year] = 1600", "twcl0000.vb1");
					}
				}
				else
				{
					// We don't want a match so use a year they can't have a record for
					if (lngRKey > 0)
					{
						clsRateRec.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngRKey), "twcl0000.vb1");
					}
					else
					{
						clsRateRec.OpenRecordset("select * from raterec where ID = -1", "twcl0000.vb1");
					}
				}
				if (clsRateRec.EndOfFile())
				{
					clsRateRec.AddNew();
					boolAddNew = true;
				}
				else
				{
					boolAddNew = false;
					if (boolChanged && boolNotAllowedToChange)
					{
						// x = MsgBox("This " & txtTaxYear.Text & " rate record already exists and you have made changes." & vbNewLine & "Do you want to overwrite it?", vbYesNo + vbQuestion, "Overwrite Record?")
						if (!CheckIfOkay())
						{
							if (lngRKey > 0)
							{
								clsrate.LoadRate(lngRKey);
								LoadTaxInfo(lngRKey);
								boolChanged = false;
							}
							return SaveRateRec;
						}
						else
						{
							// strTemp = "Ratekey " & lngRKey & ",Taxrate " & clsrate.TaxRate & ",Interest Rate " & clsrate.InterestRate & ","
							// strTemp = strTemp & "Periods " & clsrate.NumberOfPeriods & ",Commitment date " & clsrate.CommitmentDate
							// strTemp2 = "Billing date " & clsrate.BillingDate & ",Due date 1 " & clsrate.Get_DueDate(1)
							// strTemp3 = "Due date 2 " & clsrate.Get_DueDate(2) & ",Due date 3 " & clsrate.Get_DueDate(3) & ",Due date 4" & clsrate.Get_DueDate(4)
							// strTemp4 = "Interest Date 1 " & clsrate.Get_InterestStartDate(1) & ",Interest Date 2 " & clsrate.Get_InterestStartDate(2)
							// strTemp4 = strTemp4 & ",Interest Date 3 " & clsrate.Get_InterestStartDate(3) & ",Interest Date 4 " & clsrate.Get_InterestStartDate(4)
							modGlobalFunctions.AddCYAEntry("CL", strTemp, strTemp2, strTemp3, strTemp4, strTemp5);
							// strTemp = "('" & clsSecurityClass.Get_UsersUserID & "',#" & Now & "#,#" & Time & "#,'Overwrote Rate Record','Rate Key " & Val(clsRateRec.Fields("ratekey")) & ". Previous Value " & Val(clsRateRec.Fields("taxrate")) & "')"
							// Call clsTemp.Execute("INsert into cya (userid,[date],[time],description1,description2) values " & strTemp, "twcl0000.vb1")
						}
					}
					clsRateRec.Edit();
				}
				clsRateRec.Set_Fields("year", txtTaxYear.Text);
				// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
				clsrate.TaxYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsRateRec.Get_Fields("year"))));
				clsRateRec.Set_Fields("commitmentdate", t2kCommitment.Text);
				if (Strings.Trim(txtDescription.Text) == string.Empty)
				{
					clsRateRec.Set_Fields("description", " ");
				}
				else
				{
					clsRateRec.Set_Fields("description", Strings.Trim(txtDescription.Text));
				}
				clsrate.Description = Strings.Trim(txtDescription.Text);
				clsRateRec.Set_Fields("taxrate", FCConvert.ToDouble(txtTaxRate.Text) / 1000);
				clsrate.TaxRate = clsRateRec.Get_Fields_Double("taxrate");
				clsRateRec.Set_Fields("numberofperiods", cmbPeriods.SelectedIndex + 1);
				clsrate.NumberOfPeriods = FCConvert.ToInt16(clsRateRec.Get_Fields_Int16("numberofperiods"));
				clsRateRec.Set_Fields("creationdate", DateTime.Today);
				// clsRateRec.Fields("billingdate") = txtBillMonth.Text & "/" & txtBillDay.Text & "/" & txtBillYear.Text
				clsRateRec.Set_Fields("billingdate", t2kBilldate.Text);
				clsrate.BillingDate = (DateTime)clsRateRec.Get_Fields_DateTime("billingdate");
				clsRateRec.Set_Fields("interestrate", FCConvert.ToSingle(FCConvert.ToDouble(txtInterestRate.Text)) / 100);
				clsrate.InterestRate = FCConvert.ToSingle(clsRateRec.Get_Fields_Double("interestrate"));
				if (Strings.Trim(Strings.UCase(clsrate.RateType)) != "R" && Strings.Trim(Strings.UCase(clsrate.RateType)) != "S")
				{
					if (intCalledFrom != 5)
					{
						clsRateRec.Set_Fields("ratetype", "R");
						// Regular bills, not supplemental
						clsrate.RateType = "R";
					}
					else
					{
						// supplemental
						clsRateRec.Set_Fields("ratetype", "S");
						clsrate.RateType = "S";
					}
				}
				else
				{
					// this is an overwrite.  Don't want to change the type
					clsRateRec.Set_Fields("ratetype", clsrate.RateType);
				}
				for (x = 0; x <= cmbPeriods.SelectedIndex; x++)
				{
					// clsRateRec.Fields("intereststartdate" & X + 1) = txtIMonth(X).Text & "/" & txtIDay(X).Text & "/" & txtIYear(X).Text
					clsRateRec.Set_Fields("intereststartdate" + (x + 1), t2kInterestDate[x].Text);
					// clsRate.Get_InterestStartDate(X + 1) = txtIMonth(X).Text & "/" & txtIDay(X).Text & "/" & txtIYear(X).Text
					clsrate.Set_InterestStartDate(x + 1, DateAndTime.DateValue(t2kInterestDate[x].Text));
					// clsRateRec.Fields("duedate" & X + 1) = txtDMonth(X).Text & "/" & txtDDay(X).Text & "/" & txtDYear(X).Text
					clsRateRec.Set_Fields("duedate" + (x + 1), t2kDueDate[x].Text);
					// clsRate.Get_DueDate(X + 1) = txtDMonth(X).Text & "/" & txtDDay(X).Text & "/" & txtDYear(X).Text
					clsrate.Set_DueDate(x + 1, DateAndTime.DateValue(t2kDueDate[x].Text));
					clsrate.Set_BillWeight(x + 1, FCConvert.ToDouble(txtWeight[FCConvert.ToInt16(x)].Text));					
                    clsRateRec.Set_Fields("Weight" + (x + 1),txtWeight[x].Text.ToDecimalValue() / 100);
                }
				// x
				// zero out any date you won't be using
				for (x = cmbPeriods.SelectedIndex + 1; x <= 3; x++)
				{
					clsRateRec.Set_Fields("intereststartdate" + (x + 1), 0);
					clsrate.Set_InterestStartDate(x + 1, FCConvert.ToDateTime(0));
					clsRateRec.Set_Fields("duedate" + (x + 1), 0);
					clsrate.Set_DueDate(x + 1, FCConvert.ToDateTime(0));
					clsrate.Set_BillWeight(x + 1, 0);
					clsRateRec.Set_Fields("Weight" + (x + 1), 0);
				}
				// x
				if (boolAddNew)
				{
					clsTemp.OpenRecordset("select max(ID) as maxkey from raterec", "twcl0000.vb1");
					if (clsTemp.EndOfFile())
					{
						// clsRateRec.Fields("ID") = 1
					}
					else
					{
						// addnew already sets this field
						// clsRateRec.Fields("ratekey") = clsTemp.Fields("maxkey") + 1
					}
				}
				clsrate.RateKey = FCConvert.ToInt32(clsRateRec.Get_Fields_Int32("ID"));
				clsRateRec.Update();
				clsrate.RateKey = FCConvert.ToInt32(clsRateRec.Get_Fields_Int32("ID"));
				mnuContinue.Enabled = true;
				boolChanged = false;
				SaveRateRec = true;
				return SaveRateRec;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Save", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveRateRec;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveRateRec();
		}

		private void t2kBilldate_Change(object sender, System.EventArgs e)
		{
			if (boolUseChange)
			{
				boolChanged = true;
			}
		}

		private void t2kCommitment_Change(object sender, System.EventArgs e)
		{
			if (boolUseChange)
			{
				boolChanged = true;
			}
		}

		private void t2kDueDate_Change(short Index, object sender, System.EventArgs e)
		{
			if (boolUseChange)
			{
				boolChanged = true;
			}
		}

		private void t2kDueDate_Change(object sender, System.EventArgs e)
		{
			short index = t2kDueDate.GetIndex((Global.T2KDateBox)sender);
			t2kDueDate_Change(index, sender, e);
		}

		private void t2kDueDate_Validate(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string, short)
			DateTime dtTemp;
			if (!Information.IsDate(t2kInterestDate[Index].Text))
			{
				if (Information.IsDate(t2kDueDate[Index].Text))
				{
					dtTemp = FCConvert.ToDateTime(t2kDueDate[Index].Text);
					dtTemp = dtTemp.AddDays(1);
					t2kInterestDate[Index].Text = Strings.Format(dtTemp, "MM/dd/yyyy");
				}
			}
		}

		private void t2kDueDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = t2kDueDate.GetIndex((Global.T2KDateBox)sender);
			t2kDueDate_Validate(index, sender, e);
		}

		private void t2kInterestDate_Change(short Index, object sender, System.EventArgs e)
		{
			if (boolUseChange)
			{
				boolChanged = true;
			}
		}

		private void t2kInterestDate_Change(object sender, System.EventArgs e)
		{
			short index = t2kInterestDate.GetIndex((Global.T2KDateBox)sender);
			t2kInterestDate_Change(index, sender, e);
		}

		private void t2kInterestDate_Validate(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: dtTemp As DateTime	OnWrite(string, short)
			DateTime dtTemp;
			if (!Information.IsDate(t2kDueDate[Index].Text))
			{
				if (Information.IsDate(t2kInterestDate[Index].Text))
				{
					dtTemp = FCConvert.ToDateTime(t2kInterestDate[Index].Text);
					dtTemp = dtTemp.AddDays(-1);
					t2kDueDate[Index].Text = Strings.Format(dtTemp, "MM/dd/yyyy");
				}
			}
		}

		private void t2kInterestDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = t2kInterestDate.GetIndex((Global.T2KDateBox)sender);
			t2kInterestDate_Validate(index, sender, e);
		}
		/// <summary>
		/// Private Sub txtBillDay_Change()
		/// </summary>
		/// <summary>
		/// If boolUseChange Then
		/// </summary>
		/// <summary>
		/// mnuContinue.Enabled = False
		/// </summary>
		/// <summary>
		/// If Len(txtBillDay.Text) = txtBillDay.MaxLength Then
		/// </summary>
		/// <summary>
		/// SendKeys "{TAB}"
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		/// Private Sub txtBillDay_GotFocus()
		/// </summary>
		/// <summary>
		/// txtBillDay.SelStart = 0
		/// </summary>
		/// <summary>
		/// txtBillDay.SelLength = Len(txtBillDay.Text)
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		/// Private Sub txtBillMonth_Change()
		/// </summary>
		/// <summary>
		/// If Not boolUseChange Then Exit Sub
		/// </summary>
		/// <summary>
		/// mnuContinue.Enabled = False
		/// </summary>
		/// <summary>
		/// If Len(txtBillMonth.Text) = txtBillMonth.MaxLength Then
		/// </summary>
		/// <summary>
		/// SendKeys "{TAB}"
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Private Sub txtBillMonth_GotFocus()
		/// </summary>
		/// <summary>
		/// txtBillMonth.SelStart = 0
		/// </summary>
		/// <summary>
		/// txtBillMonth.SelLength = Len(txtBillMonth.Text)
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Private Sub txtBillYear_Change()
		/// </summary>
		/// <summary>
		/// If Not boolUseChange Then Exit Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// mnuContinue.Enabled = False
		/// </summary>
		/// <summary>
		/// If Len(txtBillYear.Text) = txtBillYear.MaxLength Then
		/// </summary>
		/// <summary>
		/// SendKeys "{TAB}"
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Private Sub txtBillYear_GotFocus()
		/// </summary>
		/// <summary>
		/// txtBillYear.SelStart = 0
		/// </summary>
		/// <summary>
		/// txtBillYear.SelLength = Len(txtBillYear.Text)
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void txtDescription_TextChanged(object sender, System.EventArgs e)
		{
			// If Not boolUseChange Then Exit Sub
			// mnuContinue.Enabled = False
			if (boolUseChange)
			{
				boolChanged = true;
			}
		}

		private void txtInterestRate_TextChanged(object sender, System.EventArgs e)
		{
			if (boolUseChange)
			{
				boolChanged = true;
			}
		}

		private void txtTaxRate_TextChanged(object sender, System.EventArgs e)
		{
			if (boolUseChange)
			{
				boolChanged = true;
			}
		}

		private void txtTaxYear_TextChanged(object sender, System.EventArgs e)
		{
			// If boolUseChange Then
			// mnuContinue.Enabled = False
			// If Len(txtTaxYear.Text) = 4 Then
			// Call LoadTaxInfo(txtTaxYear.Text)
			// SendKeys "{TAB}"
			// End If
			// End If
			if (boolUseChange)
			{
				boolChanged = true;
			}
		}

		private void txtTaxYear_Enter(object sender, System.EventArgs e)
		{
			txtTaxYear.SelectionStart = 0;
			txtTaxYear.SelectionLength = txtTaxYear.Text.Length;
		}

		private bool CheckEntries()
		{
			bool CheckEntries = false;
			string strTemp;
			int x;
			// vbPorter upgrade warning: strIDate As string	OnRead(DateTime)
			string[] strIDate = new string[4 + 1];
			// vbPorter upgrade warning: strDDate As string	OnRead(DateTime)
			string[] strDDate = new string[4 + 1];
			// vbPorter upgrade warning: dtTemp1 As DateTime	OnWrite(string)
			DateTime dtTemp1;
			// vbPorter upgrade warning: dtTemp2 As DateTime	OnWrite(string)
			DateTime dtTemp2;
			double[] dblWeight = new double[4 + 1];
			CheckEntries = false;
			if ((Conversion.Val(txtTaxYear.Text) < 1990) || (Conversion.Val(txtTaxYear.Text) > 2030))
			{
				MessageBox.Show("The tax year is not valid, cannot save.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			// strTemp = Val(txtBillMonth.Text) & "/" & Val(txtBillDay.Text) & "/" & Val(txtBillYear.Text)
			strTemp = t2kBilldate.Text;
			if (!Information.IsDate(strTemp))
			{
				MessageBox.Show("The Billing Date is not a valid date. Cannot save.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			strTemp = t2kCommitment.Text;
			if (!Information.IsDate(strTemp))
			{
				MessageBox.Show("The Commitment Date is not a valid date. Cannot save.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			if (Conversion.Val(txtTaxRate.Text) <= 0)
			{
				MessageBox.Show("The Tax Rate is invalid. Cannot save.", "Invalid Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			if (!Information.IsNumeric(txtInterestRate.Text))
			{
				MessageBox.Show("You must enter an interest rate.  Cannot save.", "Invalid Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			double dblTemp;
			dblTemp = 0;
			for (x = 0; x <= cmbPeriods.SelectedIndex; x++)
			{
				// strIDate(X) = Val(txtIMonth(X).Text) & "/" & Val(txtIDay(X).Text) & "/" & Val(txtIYear(X).Text)
				strIDate[x] = t2kInterestDate[x].Text;
				// strDDate(X) = Val(txtDMonth(X).Text) & "/" & Val(txtDDay(X).Text) & "/" & Val(txtDYear(X).Text)
				strDDate[x] = t2kDueDate[x].Text;
				dblWeight[x] = Conversion.Val(txtWeight[FCConvert.ToInt16(x)].Text);
				if (dblWeight[x] <= 0)
				{
					MessageBox.Show("Invalid weight for Period " + FCConvert.ToString(x + 1), "Invalid Weight", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckEntries;
				}
				dblTemp += dblWeight[x];
			}
			// x
			if (dblTemp != 100)
			{
				MessageBox.Show("The sum of weights must equal 100.", "Invalid Weights", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			if (!Information.IsDate(strIDate[0]))
			{
				MessageBox.Show("Invalid interest date for Period 1", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			if (!Information.IsDate(strDDate[0]))
			{
				MessageBox.Show("Invalid Due Date for Period 1", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			dtTemp1 = FCConvert.ToDateTime(strIDate[0]);
			dtTemp2 = FCConvert.ToDateTime(strDDate[0]);
			if (!(dtTemp1.ToOADate() > dtTemp2.ToOADate()))
			{
				MessageBox.Show("The interest date for Period 1 must be greater than the due date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckEntries;
			}
			for (x = 1; x <= cmbPeriods.SelectedIndex; x++)
			{
				if (!Information.IsDate(strIDate[x]))
				{
					MessageBox.Show("Invalid interest date for Period " + FCConvert.ToString(x + 1), "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckEntries;
				}
				if (!Information.IsDate(strDDate[x]))
				{
					MessageBox.Show("Invalid due date for Period " + FCConvert.ToString(x + 1), "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckEntries;
				}
				dtTemp1 = FCConvert.ToDateTime(strIDate[x]);
				dtTemp2 = FCConvert.ToDateTime(strDDate[x]);
				if (!(dtTemp1.ToOADate() > dtTemp2.ToOADate()))
				{
					MessageBox.Show("The interest date for Period " + FCConvert.ToString(x + 1) + " must be greater than the due date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckEntries;
				}
				dtTemp1 = FCConvert.ToDateTime(strDDate[x - 1]);
				if (!(dtTemp2.ToOADate() > dtTemp1.ToOADate()))
				{
					MessageBox.Show("The due date for Period " + FCConvert.ToString(x + 1) + " must be greater than the due date for Period " + FCConvert.ToString(x), "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckEntries;
				}
			}
			// x
			CheckEntries = true;
			return CheckEntries;
			ErrorHandler:
			;
			MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\r\n" + "Occured in Check Entries", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			return CheckEntries;
		}

		private void cmdSaveAndContinue_Click(object sender, EventArgs e)
		{
			mnuContinue_Click(mnuContinue, EventArgs.Empty);
		}
	}
}
