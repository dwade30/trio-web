﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmBillViewer.
	/// </summary>
	partial class frmBillViewer : FCForm
	{
		public ARViewer ARViewer21;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEmail;
		public fecherFoundation.FCToolStripMenuItem mnuEmailRTF;
		public fecherFoundation.FCToolStripMenuItem mnuEmailPDF;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuExport;
		public fecherFoundation.FCToolStripMenuItem mnuRTF;
		public fecherFoundation.FCToolStripMenuItem mnuExportPDF;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAll;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreviewed;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillViewer));
			this.ARViewer21 = new Global.ARViewer();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEmail = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEmailRTF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEmailPDF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRTF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExportPDF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreviewed = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.toolBar1 = new Wisej.Web.ToolBar();
			this.toolBarButtonEmailPDF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonEmailRTF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportPDF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportRTF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonPrintAll = new Wisej.Web.ToolBarButton();
			this.toolBarButtonPrintPreview = new Wisej.Web.ToolBarButton();
			this.SuspendLayout();
			// 
			// ARViewer21
			// 
			this.ARViewer21.Dock = Wisej.Web.DockStyle.Fill;
			this.ARViewer21.Location = new System.Drawing.Point(0, 40);
			this.ARViewer21.Name = "ARViewer21";
			this.ARViewer21.ReportName = null;
			this.ARViewer21.ReportSource = null;
			this.ARViewer21.ScrollBars = false;
			this.ARViewer21.Size = new System.Drawing.Size(969, 498);
			this.ARViewer21.TabIndex = 1;
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuFile});
			//this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuEmail,
				this.mnuSepar3,
				this.mnuExport,
				this.mnuSepar2,
				this.mnuPrintAll,
				this.mnuPrintPreviewed,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuEmail
			// 
			this.mnuEmail.Index = 0;
			this.mnuEmail.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuEmailRTF,
				this.mnuEmailPDF
			});
			this.mnuEmail.Name = "mnuEmail";
			this.mnuEmail.Text = "E-mail";
			// 
			// mnuEmailRTF
			// 
			this.mnuEmailRTF.Index = 0;
			this.mnuEmailRTF.Name = "mnuEmailRTF";
			this.mnuEmailRTF.Text = "E-mail as Rich Text";
			this.mnuEmailRTF.Click += new System.EventHandler(this.mnuEmailRTF_Click);
			// 
			// mnuEmailPDF
			// 
			this.mnuEmailPDF.Index = 1;
			this.mnuEmailPDF.Name = "mnuEmailPDF";
			this.mnuEmailPDF.Text = "E-mail as PDF";
			this.mnuEmailPDF.Click += new System.EventHandler(this.mnuEmailPDF_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 1;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuExport
			// 
			this.mnuExport.Index = 2;
			this.mnuExport.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRTF,
				this.mnuExportPDF
			});
			this.mnuExport.Name = "mnuExport";
			this.mnuExport.Text = "Export";
			// 
			// mnuRTF
			// 
			this.mnuRTF.Index = 0;
			this.mnuRTF.Name = "mnuRTF";
			this.mnuRTF.Text = "Export as Rich Text Format";
			this.mnuRTF.Click += new System.EventHandler(this.mnuRTF_Click);
			// 
			// mnuExportPDF
			// 
			this.mnuExportPDF.Index = 1;
			this.mnuExportPDF.Name = "mnuExportPDF";
			this.mnuExportPDF.Text = "Export as PDF";
			this.mnuExportPDF.Click += new System.EventHandler(this.mnuExportPDF_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 3;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuPrintAll
			// 
			this.mnuPrintAll.Index = 4;
			this.mnuPrintAll.Name = "mnuPrintAll";
			this.mnuPrintAll.Text = "Print All Bills";
			this.mnuPrintAll.Click += new System.EventHandler(this.mnuPrintAll_Click);
			// 
			// mnuPrintPreviewed
			// 
			this.mnuPrintPreviewed.Index = 5;
			this.mnuPrintPreviewed.Name = "mnuPrintPreviewed";
			this.mnuPrintPreviewed.Text = "Print Previewed Bills";
			this.mnuPrintPreviewed.Click += new System.EventHandler(this.mnuPrintPreviewed_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 6;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 7;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// toolBar1
			// 
			this.toolBar1.AutoSize = false;
			this.toolBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
			this.toolBar1.Buttons.AddRange(new Wisej.Web.ToolBarButton[] {
				this.toolBarButtonEmailPDF,
				this.toolBarButtonEmailRTF,
				this.toolBarButtonExportPDF,
				this.toolBarButtonExportRTF,
				this.toolBarButtonPrintAll,
				this.toolBarButtonPrintPreview
			});
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.ShowToolTips = true;
			this.toolBar1.Size = new System.Drawing.Size(969, 40);
			this.toolBar1.TabIndex = 1;
			this.toolBar1.TabStop = false;
			this.toolBar1.ButtonClick += new Wisej.Web.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
			// 
			// toolBarButtonEmailPDF
			// 
			this.toolBarButtonEmailPDF.ImageSource = "icon-report-email-pdf";
			this.toolBarButtonEmailPDF.Name = "toolBarButtonEmailPDF";
			this.toolBarButtonEmailPDF.ToolTipText = "Email as PDF";
			// 
			// toolBarButtonEmailRTF
			// 
			this.toolBarButtonEmailRTF.ImageSource = "icon-report-email-rtf";
			this.toolBarButtonEmailRTF.Name = "toolBarButtonEmailRTF";
			this.toolBarButtonEmailRTF.ToolTipText = "Email as RTF";
			// 
			// toolBarButtonExportPDF
			// 
			this.toolBarButtonExportPDF.ImageSource = "icon-report-export-pdf";
			this.toolBarButtonExportPDF.Name = "toolBarButtonExportPDF";
			this.toolBarButtonExportPDF.ToolTipText = "Export as PDF";
			// 
			// toolBarButtonExportRTF
			// 
			this.toolBarButtonExportRTF.ImageSource = "icon-report-export-rtf";
			this.toolBarButtonExportRTF.Name = "toolBarButtonExportRTF";
			this.toolBarButtonExportRTF.ToolTipText = "Export as RTF";
			// 
			// toolBarButtonPrintAll
			// 
			this.toolBarButtonPrintAll.ImageSource = "icon-report-print";
			this.toolBarButtonPrintAll.Name = "toolBarButtonPrintAll";
			this.toolBarButtonPrintAll.ToolTipText = "Print All Bills";
			// 
			// toolBarButtonPrintPreview
			// 
			this.toolBarButtonPrintPreview.ImageSource = "icon-report-print";
			this.toolBarButtonPrintPreview.Name = "toolBarButtonPrintPreview";
			this.toolBarButtonPrintPreview.ToolTipText = "Print Previewed Bills";
			// 
			// frmBillViewer
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(969, 538);
			this.Controls.Add(this.ARViewer21);
			this.Controls.Add(this.toolBar1);
			this.FillColor = 16777215;
			this.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.Name = "frmBillViewer";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Bills";
			this.Load += new System.EventHandler(this.frmBillViewer_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBillViewer_KeyDown);
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private ToolBarButton toolBarButtonPrintAll;
		private ToolBarButton toolBarButtonExportRTF;
		private ToolBarButton toolBarButtonExportPDF;
		private ToolBarButton toolBarButtonEmailRTF;
		private ToolBarButton toolBarButtonEmailPDF;
		private ToolBar toolBar1;
		private ToolBarButton toolBarButtonPrintPreview;
	}
}
