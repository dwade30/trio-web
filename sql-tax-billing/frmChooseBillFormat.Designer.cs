﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmChooseBillFormat.
	/// </summary>
	partial class frmChooseBillFormat : BaseForm
	{
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCPictureBox imgBreakdown;
		public fecherFoundation.FCPictureBox imgCombination;
		public fecherFoundation.FCPictureBox imgDiscounts;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblCombination;
		public fecherFoundation.FCLabel lblDiscounts;
		public fecherFoundation.FCComboBox cmbBillFormat;
		public fecherFoundation.FCLabel lblFormatDescription;
		public Wisej.Web.ImageList BillImages;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCPictureBox BillPic;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChooseBillFormat));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images"))));
			Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images1"))));
			Wisej.Web.ImageListEntry imageListEntry3 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images2"))));
			Wisej.Web.ImageListEntry imageListEntry4 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images3"))));
			Wisej.Web.ImageListEntry imageListEntry5 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images4"))));
			Wisej.Web.ImageListEntry imageListEntry6 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images5"))));
			Wisej.Web.ImageListEntry imageListEntry7 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images6"))));
			Wisej.Web.ImageListEntry imageListEntry8 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images7"))));
			Wisej.Web.ImageListEntry imageListEntry9 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images8"))));
			Wisej.Web.ImageListEntry imageListEntry10 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images9"))));
			Wisej.Web.ImageListEntry imageListEntry11 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images10"))));
			Wisej.Web.ImageListEntry imageListEntry12 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images11"))));
			Wisej.Web.ImageListEntry imageListEntry13 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images12"))));
			Wisej.Web.ImageListEntry imageListEntry14 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images13"))));
			Wisej.Web.ImageListEntry imageListEntry15 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images14"))));
			Wisej.Web.ImageListEntry imageListEntry16 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images15"))));
			this.Frame3 = new fecherFoundation.FCFrame();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.imgBreakdown = new fecherFoundation.FCPictureBox();
			this.imgCombination = new fecherFoundation.FCPictureBox();
			this.imgDiscounts = new fecherFoundation.FCPictureBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblCombination = new fecherFoundation.FCLabel();
			this.lblDiscounts = new fecherFoundation.FCLabel();
			this.cmbBillFormat = new fecherFoundation.FCComboBox();
			this.lblFormatDescription = new fecherFoundation.FCLabel();
			this.BillImages = new Wisej.Web.ImageList(this.components);
			this.Frame1 = new fecherFoundation.FCFrame();
			this.BillPic = new fecherFoundation.FCPictureBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgBreakdown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCombination)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDiscounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BillPic)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(222, 30);
			this.HeaderText.Text = "Choose Bill Format";
			// 
			// Frame3
			// 
			this.Frame3.AppearanceKey = "groupBoxLeftBorder";
			this.Frame3.Controls.Add(this.Frame2);
			this.Frame3.Controls.Add(this.cmbBillFormat);
			this.Frame3.Controls.Add(this.lblFormatDescription);
			this.Frame3.Location = new System.Drawing.Point(30, 30);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(736, 170);
			this.Frame3.TabIndex = 1;
			this.Frame3.Text = "Format";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.imgBreakdown);
			this.Frame2.Controls.Add(this.imgCombination);
			this.Frame2.Controls.Add(this.imgDiscounts);
			this.Frame2.Controls.Add(this.Label1);
			this.Frame2.Controls.Add(this.lblCombination);
			this.Frame2.Controls.Add(this.lblDiscounts);
			this.Frame2.Location = new System.Drawing.Point(441, 30);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(276, 123);
			this.Frame2.TabIndex = 2;
			this.Frame2.Text = "Options Allowed";
			// 
			// imgBreakdown
			// 
			this.imgBreakdown.AllowDrop = true;
			this.imgBreakdown.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgBreakdown.DrawStyle = ((short)(0));
			this.imgBreakdown.DrawWidth = ((short)(1));
			this.imgBreakdown.FillStyle = ((short)(1));
			this.imgBreakdown.FontTransparent = true;
			this.imgBreakdown.Image = ((System.Drawing.Image)(resources.GetObject("imgBreakdown.Image")));
			this.imgBreakdown.Location = new System.Drawing.Point(30, 90);
			this.imgBreakdown.Name = "imgBreakdown";
			this.imgBreakdown.Picture = ((System.Drawing.Image)(resources.GetObject("imgBreakdown.Picture")));
			this.imgBreakdown.Size = new System.Drawing.Size(17, 17);
			this.imgBreakdown.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgBreakdown.TabIndex = 0;
			// 
			// imgCombination
			// 
			this.imgCombination.AllowDrop = true;
			this.imgCombination.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgCombination.DrawStyle = ((short)(0));
			this.imgCombination.DrawWidth = ((short)(1));
			this.imgCombination.FillStyle = ((short)(1));
			this.imgCombination.FontTransparent = true;
			this.imgCombination.Image = ((System.Drawing.Image)(resources.GetObject("imgCombination.Image")));
			this.imgCombination.Location = new System.Drawing.Point(30, 60);
			this.imgCombination.Name = "imgCombination";
			this.imgCombination.Picture = ((System.Drawing.Image)(resources.GetObject("imgCombination.Picture")));
			this.imgCombination.Size = new System.Drawing.Size(17, 17);
			this.imgCombination.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgCombination.TabIndex = 1;
			// 
			// imgDiscounts
			// 
			this.imgDiscounts.AllowDrop = true;
			this.imgDiscounts.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgDiscounts.DrawStyle = ((short)(0));
			this.imgDiscounts.DrawWidth = ((short)(1));
			this.imgDiscounts.FillStyle = ((short)(1));
			this.imgDiscounts.FontTransparent = true;
			this.imgDiscounts.Image = ((System.Drawing.Image)(resources.GetObject("imgDiscounts.Image")));
			this.imgDiscounts.Location = new System.Drawing.Point(30, 30);
			this.imgDiscounts.Name = "imgDiscounts";
			this.imgDiscounts.Picture = ((System.Drawing.Image)(resources.GetObject("imgDiscounts.Picture")));
			this.imgDiscounts.Size = new System.Drawing.Size(17, 17);
			this.imgDiscounts.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgDiscounts.TabIndex = 2;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(79, 90);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(180, 18);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "BREAKDOWN OF TAXES";
			// 
			// lblCombination
			// 
			this.lblCombination.Location = new System.Drawing.Point(79, 60);
			this.lblCombination.Name = "lblCombination";
			this.lblCombination.Size = new System.Drawing.Size(180, 18);
			this.lblCombination.TabIndex = 1;
			this.lblCombination.Text = "COMBINATION OF RE & PP";
			// 
			// lblDiscounts
			// 
			this.lblDiscounts.Location = new System.Drawing.Point(79, 30);
			this.lblDiscounts.Name = "lblDiscounts";
			this.lblDiscounts.Size = new System.Drawing.Size(180, 18);
			this.lblDiscounts.TabIndex = 0;
			this.lblDiscounts.Text = "DISCOUNTS";
			// 
			// cmbBillFormat
			// 
			this.cmbBillFormat.AutoSize = false;
			this.cmbBillFormat.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBillFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBillFormat.FormattingEnabled = true;
			this.cmbBillFormat.Location = new System.Drawing.Point(30, 30);
			this.cmbBillFormat.Name = "cmbBillFormat";
			this.cmbBillFormat.Size = new System.Drawing.Size(327, 40);
			this.cmbBillFormat.TabIndex = 0;
			this.cmbBillFormat.SelectedIndexChanged += new System.EventHandler(this.cmbBillFormat_SelectedIndexChanged);
			// 
			// lblFormatDescription
			// 
			this.lblFormatDescription.Location = new System.Drawing.Point(30, 90);
			this.lblFormatDescription.Name = "lblFormatDescription";
			this.lblFormatDescription.Size = new System.Drawing.Size(363, 80);
			this.lblFormatDescription.TabIndex = 1;
			this.lblFormatDescription.Text = "LBLFORMATDESCRIPTION";
			// 
			// BillImages
			// 
			this.BillImages.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1,
				imageListEntry2,
				imageListEntry3,
				imageListEntry4,
				imageListEntry5,
				imageListEntry6,
				imageListEntry7,
				imageListEntry8,
				imageListEntry9,
				imageListEntry10,
				imageListEntry11,
				imageListEntry12,
				imageListEntry13,
				imageListEntry14,
				imageListEntry15,
				imageListEntry16
			});
			this.BillImages.ImageSize = new System.Drawing.Size(256, 256);
			this.BillImages.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = " groupBoxNoBorder";
			this.Frame1.Controls.Add(this.BillPic);
			this.Frame1.Location = new System.Drawing.Point(30, 230);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(1019, 282);
			this.Frame1.TabIndex = 0;
			// 
			// BillPic
			// 
			this.BillPic.AllowDrop = true;
			this.BillPic.BorderStyle = Wisej.Web.BorderStyle.None;
			this.BillPic.DrawStyle = ((short)(0));
			this.BillPic.DrawWidth = ((short)(1));
			this.BillPic.FillStyle = ((short)(1));
			this.BillPic.FontTransparent = true;
			this.BillPic.Image = ((System.Drawing.Image)(resources.GetObject("BillPic.Image")));
			this.BillPic.Location = new System.Drawing.Point(0, 30);
			this.BillPic.Name = "BillPic";
			this.BillPic.Picture = ((System.Drawing.Image)(resources.GetObject("BillPic.Picture")));
			this.BillPic.Size = new System.Drawing.Size(1019, 246);
			this.BillPic.TabIndex = 0;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(431, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(117, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.cmdContinue_Click);
			// 
			// frmChooseBillFormat
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmChooseBillFormat";
			this.Text = "Choose Bill Format";
			this.Load += new System.EventHandler(this.frmChooseBillFormat_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChooseBillFormat_KeyDown);
			this.Resize += new System.EventHandler(this.frmChooseBillFormat_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgBreakdown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCombination)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgDiscounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.BillPic)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
