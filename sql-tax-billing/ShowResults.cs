﻿//Fecher vbPorter - Version 1.0.0.35
using Wisej.Web;
using Global;
using fecherFoundation;
using System;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmShowResults.
	/// </summary>
	public partial class frmShowResults : BaseForm
	{
		public frmShowResults()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmShowResults InstancePtr
		{
			get
			{
				return (frmShowResults)Sys.GetInstance(typeof(frmShowResults));
			}
		}

		protected frmShowResults _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private bool boolNoParent;

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			frmShowResults.InstancePtr.Close();
		}

		public void cmdOK_Click()
		{
			// cmdOK_Click(cmdOK, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				// CommonDialog1_Save.Flags = (vbPorterConverter.cdlPDReturnDC+vbPorterConverter.cdlPDNoPageNums)	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- CommonDialog1.CancelError = true;
				//if (rtbText.SelLength == 0)
				//{
				//	// CommonDialog1_Save.Flags = 0 /*? CommonDialog1_Save.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDAllPages	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//	// CommonDialog1_Save.Flags = 0 /*? CommonDialog1_Save.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDSelection	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//	Information.Err().Clear();
				//	CommonDialog1.ShowPrinter();
				//	if (Information.Err().Number == 0)
				//	{
				rtbText.SelPrint(0);
				//		FCGlobal.Printer.EndDoc();
				//	}
				//}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void frmShowResults_Activated(object sender, System.EventArgs e)
		{
			this.BringToFront();
		}

		private void frmShowResults_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmShowResults_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				Support.SendKeys("{TAB}", false);
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmShowResults_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmShowResults properties;
			//frmShowResults.ScaleMode	= 4;
			//frmShowResults.ScaleWidth	= 75.375;
			//frmShowResults.ScaleHeight	= 31.3129997253418;
			//frmShowResults.LinkTopic	= "Form1";
			//rtbText properties;
			//rtbText.ScrollBars	= 2;
			//rtbText.ReadOnly	= true;
			//rtbText.TextRTF	= $"ShowResults.frx":058A;
			//End Unmaped Properties
			this.Text = "File Format";
            this.HeaderText.Text = "File Format";
			// lblWMUNINAME.Caption = "- " + Trim$(MuniName) + " -"
			// lblScreenTitle.Caption = "File Format"
			// lblDate.Caption = Date
			// lbltime.Caption = Time
			if (!boolNoParent)
			{
				modGlobalFunctions.SetFixedSize(this);
				modGlobalFunctions.SetTRIOColors(this);
			}
		}

		public void Init(string txtData, bool boolNoMdiParent = false)
		{
			boolNoParent = boolNoMdiParent;
			rtbText.SelTabCount = 4;
			rtbText.SelTabs(0, 35);
			rtbText.SelTabs(1, 50);
			rtbText.SelTabs(2, 65);
			rtbText.Text = txtData;
			//FC:FINAL:MSH - move caret to start of the text and scroll textbox to top for displaying data from the start(as in original)
			// In WiseJ moving caret and scrolling will work only if we send SelectionLength to 1 and then to 0
			rtbText.SelectionStart = 0;
			rtbText.SelectionLength = 1;
			rtbText.SelectionLength = 0;
			this.Show(FormShowEnum.Modal);
		}

		private void frmShowResults_Resize(object sender, System.EventArgs e)
		{
			rtbText.SelTabCount = 4;
			rtbText.SelTabs(0, 35);
			rtbText.SelTabs(1, 50);
			rtbText.SelTabs(2, 65);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdOK_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}
	}
}
