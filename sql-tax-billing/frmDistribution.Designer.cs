﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmDistribution.
	/// </summary>
	partial class frmDistribution : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtName;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtPercent;
		public fecherFoundation.FCTextBox txtName_5;
		public fecherFoundation.FCTextBox txtName_6;
		public fecherFoundation.FCTextBox txtPercent_5;
		public fecherFoundation.FCTextBox txtPercent_6;
		public fecherFoundation.FCTextBox txtTotal;
		public fecherFoundation.FCTextBox txtPercent_4;
		public fecherFoundation.FCTextBox txtPercent_3;
		public fecherFoundation.FCTextBox txtPercent_2;
		public fecherFoundation.FCTextBox txtPercent_1;
		public fecherFoundation.FCTextBox txtName_4;
		public fecherFoundation.FCTextBox txtName_3;
		public fecherFoundation.FCTextBox txtName_2;
		public fecherFoundation.FCTextBox txtName_1;
		public fecherFoundation.FCTextBox txtPercent_0;
		public fecherFoundation.FCTextBox txtName_0;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDistribution));
			this.txtName_5 = new fecherFoundation.FCTextBox();
			this.txtName_6 = new fecherFoundation.FCTextBox();
			this.txtPercent_5 = new fecherFoundation.FCTextBox();
			this.txtPercent_6 = new fecherFoundation.FCTextBox();
			this.txtTotal = new fecherFoundation.FCTextBox();
			this.txtPercent_4 = new fecherFoundation.FCTextBox();
			this.txtPercent_3 = new fecherFoundation.FCTextBox();
			this.txtPercent_2 = new fecherFoundation.FCTextBox();
			this.txtPercent_1 = new fecherFoundation.FCTextBox();
			this.txtName_4 = new fecherFoundation.FCTextBox();
			this.txtName_3 = new fecherFoundation.FCTextBox();
			this.txtName_2 = new fecherFoundation.FCTextBox();
			this.txtName_1 = new fecherFoundation.FCTextBox();
			this.txtPercent_0 = new fecherFoundation.FCTextBox();
			this.txtName_0 = new fecherFoundation.FCTextBox();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveAndExit = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAndExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 548);
			this.BottomPanel.Size = new System.Drawing.Size(512, 79);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtName_5);
			this.ClientArea.Controls.Add(this.txtName_6);
			this.ClientArea.Controls.Add(this.txtPercent_5);
			this.ClientArea.Controls.Add(this.txtPercent_6);
			this.ClientArea.Controls.Add(this.txtTotal);
			this.ClientArea.Controls.Add(this.txtPercent_4);
			this.ClientArea.Controls.Add(this.txtPercent_3);
			this.ClientArea.Controls.Add(this.txtPercent_2);
			this.ClientArea.Controls.Add(this.txtPercent_1);
			this.ClientArea.Controls.Add(this.txtName_4);
			this.ClientArea.Controls.Add(this.txtName_3);
			this.ClientArea.Controls.Add(this.txtName_2);
			this.ClientArea.Controls.Add(this.txtName_1);
			this.ClientArea.Controls.Add(this.txtPercent_0);
			this.ClientArea.Controls.Add(this.txtName_0);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(512, 488);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(512, 60);
			this.TopPanel.TabIndex = 0;
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// txtName_5
			// 
			this.txtName_5.AutoSize = false;
			this.txtName_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtName_5.LinkItem = null;
			this.txtName_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName_5.LinkTopic = null;
			this.txtName_5.Location = new System.Drawing.Point(105, 280);
			this.txtName_5.Name = "txtName_5";
			this.txtName_5.Size = new System.Drawing.Size(227, 40);
			this.txtName_5.TabIndex = 16;
			// 
			// txtName_6
			// 
			this.txtName_6.AutoSize = false;
			this.txtName_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtName_6.LinkItem = null;
			this.txtName_6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName_6.LinkTopic = null;
			this.txtName_6.Location = new System.Drawing.Point(105, 330);
			this.txtName_6.Name = "txtName_6";
			this.txtName_6.Size = new System.Drawing.Size(227, 40);
			this.txtName_6.TabIndex = 19;
			// 
			// txtPercent_5
			// 
			this.txtPercent_5.AutoSize = false;
			this.txtPercent_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent_5.LinkItem = null;
			this.txtPercent_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent_5.LinkTopic = null;
			this.txtPercent_5.Location = new System.Drawing.Point(402, 280);
			this.txtPercent_5.Name = "txtPercent_5";
			this.txtPercent_5.Size = new System.Drawing.Size(82, 40);
			this.txtPercent_5.TabIndex = 17;
			this.txtPercent_5.Leave += new System.EventHandler(this.txtPercent_Validating);
			// 
			// txtPercent_6
			// 
			this.txtPercent_6.AutoSize = false;
			this.txtPercent_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent_6.LinkItem = null;
			this.txtPercent_6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent_6.LinkTopic = null;
			this.txtPercent_6.Location = new System.Drawing.Point(402, 330);
			this.txtPercent_6.Name = "txtPercent_6";
			this.txtPercent_6.Size = new System.Drawing.Size(82, 40);
			this.txtPercent_6.TabIndex = 20;
			this.txtPercent_6.Leave += new System.EventHandler(this.txtPercent_Validating);
			// 
			// txtTotal
			// 
			this.txtTotal.AutoSize = false;
			this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotal.Enabled = false;
			this.txtTotal.LinkItem = null;
			this.txtTotal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTotal.LinkTopic = null;
			this.txtTotal.Location = new System.Drawing.Point(402, 380);
			this.txtTotal.LockedOriginal = true;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.ReadOnly = true;
			this.txtTotal.Size = new System.Drawing.Size(82, 40);
			this.txtTotal.TabIndex = 22;
			// 
			// txtPercent_4
			// 
			this.txtPercent_4.AutoSize = false;
			this.txtPercent_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent_4.LinkItem = null;
			this.txtPercent_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent_4.LinkTopic = null;
			this.txtPercent_4.Location = new System.Drawing.Point(402, 230);
			this.txtPercent_4.Name = "txtPercent_4";
			this.txtPercent_4.Size = new System.Drawing.Size(82, 40);
			this.txtPercent_4.TabIndex = 14;
			this.txtPercent_4.Leave += new System.EventHandler(this.txtPercent_Validating);
			// 
			// txtPercent_3
			// 
			this.txtPercent_3.AutoSize = false;
			this.txtPercent_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent_3.LinkItem = null;
			this.txtPercent_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent_3.LinkTopic = null;
			this.txtPercent_3.Location = new System.Drawing.Point(402, 180);
			this.txtPercent_3.Name = "txtPercent_3";
			this.txtPercent_3.Size = new System.Drawing.Size(82, 40);
			this.txtPercent_3.TabIndex = 11;
			this.txtPercent_3.Leave += new System.EventHandler(this.txtPercent_Validating);
			// 
			// txtPercent_2
			// 
			this.txtPercent_2.AutoSize = false;
			this.txtPercent_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent_2.LinkItem = null;
			this.txtPercent_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent_2.LinkTopic = null;
			this.txtPercent_2.Location = new System.Drawing.Point(402, 130);
			this.txtPercent_2.Name = "txtPercent_2";
			this.txtPercent_2.Size = new System.Drawing.Size(82, 40);
			this.txtPercent_2.TabIndex = 8;
			this.txtPercent_2.Leave += new System.EventHandler(this.txtPercent_Validating);
			// 
			// txtPercent_1
			// 
			this.txtPercent_1.AutoSize = false;
			this.txtPercent_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent_1.LinkItem = null;
			this.txtPercent_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent_1.LinkTopic = null;
			this.txtPercent_1.Location = new System.Drawing.Point(402, 80);
			this.txtPercent_1.Name = "txtPercent_1";
			this.txtPercent_1.Size = new System.Drawing.Size(82, 40);
			this.txtPercent_1.TabIndex = 5;
			this.txtPercent_1.Leave += new System.EventHandler(this.txtPercent_Validating);
			// 
			// txtName_4
			// 
			this.txtName_4.AutoSize = false;
			this.txtName_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtName_4.LinkItem = null;
			this.txtName_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName_4.LinkTopic = null;
			this.txtName_4.Location = new System.Drawing.Point(105, 230);
			this.txtName_4.Name = "txtName_4";
			this.txtName_4.Size = new System.Drawing.Size(227, 40);
			this.txtName_4.TabIndex = 13;
			// 
			// txtName_3
			// 
			this.txtName_3.AutoSize = false;
			this.txtName_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtName_3.LinkItem = null;
			this.txtName_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName_3.LinkTopic = null;
			this.txtName_3.Location = new System.Drawing.Point(105, 180);
			this.txtName_3.Name = "txtName_3";
			this.txtName_3.Size = new System.Drawing.Size(227, 40);
			this.txtName_3.TabIndex = 10;
			// 
			// txtName_2
			// 
			this.txtName_2.AutoSize = false;
			this.txtName_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtName_2.LinkItem = null;
			this.txtName_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName_2.LinkTopic = null;
			this.txtName_2.Location = new System.Drawing.Point(105, 130);
			this.txtName_2.Name = "txtName_2";
			this.txtName_2.Size = new System.Drawing.Size(227, 40);
			this.txtName_2.TabIndex = 7;
			// 
			// txtName_1
			// 
			this.txtName_1.AutoSize = false;
			this.txtName_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtName_1.LinkItem = null;
			this.txtName_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName_1.LinkTopic = null;
			this.txtName_1.Location = new System.Drawing.Point(105, 80);
			this.txtName_1.Name = "txtName_1";
			this.txtName_1.Size = new System.Drawing.Size(227, 40);
			this.txtName_1.TabIndex = 4;
			// 
			// txtPercent_0
			// 
			this.txtPercent_0.AutoSize = false;
			this.txtPercent_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent_0.LinkItem = null;
			this.txtPercent_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent_0.LinkTopic = null;
			this.txtPercent_0.Location = new System.Drawing.Point(402, 30);
			this.txtPercent_0.Name = "txtPercent_0";
			this.txtPercent_0.Size = new System.Drawing.Size(82, 40);
			this.txtPercent_0.TabIndex = 2;
			this.txtPercent_0.Leave += new System.EventHandler(this.txtPercent_Validating);
			// 
			// txtName_0
			// 
			this.txtName_0.AutoSize = false;
			this.txtName_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtName_0.LinkItem = null;
			this.txtName_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName_0.LinkTopic = null;
			this.txtName_0.Location = new System.Drawing.Point(105, 30);
			this.txtName_0.Name = "txtName_0";
			this.txtName_0.Size = new System.Drawing.Size(227, 40);
			this.txtName_0.TabIndex = 1;
			this.txtName_0.Text = "hghdfghdfghdfghfg";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(30, 292);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(19, 24);
			this.Label9.TabIndex = 15;
			this.Label9.Text = "6";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(30, 344);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(19, 24);
			this.Label8.TabIndex = 18;
			this.Label8.Text = "7";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(282, 394);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(78, 24);
			this.Label7.TabIndex = 21;
			this.Label7.Text = "TOTAL";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(30, 244);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(19, 24);
			this.Label5.TabIndex = 12;
			this.Label5.Text = "5";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 194);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(19, 24);
			this.Label4.TabIndex = 9;
			this.Label4.Text = "4";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 144);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(19, 24);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "3";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 91);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(19, 24);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "2";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(19, 24);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "1";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSave,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 0;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSave.Text = "Save & Exit";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveAndExit
			// 
			this.cmdSaveAndExit.AppearanceKey = "acceptButton";
			this.cmdSaveAndExit.Location = new System.Drawing.Point(211, 17);
			this.cmdSaveAndExit.Name = "cmdSaveAndExit";
			this.cmdSaveAndExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAndExit.Size = new System.Drawing.Size(91, 48);
			this.cmdSaveAndExit.TabIndex = 23;
			this.cmdSaveAndExit.Text = "Save";
			this.cmdSaveAndExit.Click += new System.EventHandler(this.cmdSaveAndExit_Click);
			// 
			// frmDistribution
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(512, 627);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmDistribution";
			this.Text = "Distribution Percentages";
			this.Load += new System.EventHandler(this.frmDistribution_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDistribution_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDistribution_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndExit)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndExit;
	}
}
