﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPrintorPreview.
	/// </summary>
	public partial class frmPrintorPreview : BaseForm
	{
		public frmPrintorPreview()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintorPreview InstancePtr
		{
			get
			{
				return (frmPrintorPreview)Sys.GetInstance(typeof(frmPrintorPreview));
			}
		}

		protected frmPrintorPreview _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intNumPreview;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBillPrintReturn tReturn = new cBillPrintReturn();
		private cBillPrintReturn tReturn_AutoInitialized;

		private cBillPrintReturn tReturn
		{
			get
			{
				if (tReturn_AutoInitialized == null)
				{
					tReturn_AutoInitialized = new cBillPrintReturn();
				}
				return tReturn_AutoInitialized;
			}
			set
			{
				tReturn_AutoInitialized = value;
			}
		}

		private void frmPrintorPreview_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPrintorPreview_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintorPreview properties;
			//frmPrintorPreview.ScaleWidth	= 3885;
			//frmPrintorPreview.ScaleHeight	= 2385;
			//frmPrintorPreview.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
        {
            switch (cmbPrint.SelectedIndex)
            {
                case 1 when Conversion.Val(txtLimit.Text) <= 0:
                    MessageBox.Show("You must enter a valid preview limit", "Invalid Limit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                case 1:
                    intNumPreview = FCConvert.ToInt32(Math.Round(Conversion.Val(txtLimit.Text)));
                    tReturn.PreviewLimit = FCConvert.ToInt16(intNumPreview);
                    tReturn.OutputType = 1;
                    Close();

                    break;
                case 2 when Strings.Trim(txtFileName.Text) == "":
                    MessageBox.Show("You must enter a filename", "Invalid Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                case 2:
                    tReturn.OutputType = 2;
                    tReturn.Filename = txtFileName.Text;
                    Close();

                    break;
                default:
                    tReturn.OutputType = 0;
                    tReturn.PreviewLimit = 0;
                    Close();

                    break;
            }
        }

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public cBillPrintReturn Init(bool boolShowExport = true)
		{
			cBillPrintReturn Init = null;
			intNumPreview = -1;
			tReturn.OutputType = -1;
			tReturn.Filename = "";
			tReturn.PreviewLimit = -1;
			if (boolShowExport)
			{
				if (!cmbPrint.Items.Contains("Create as PDF"))
				{
					cmbPrint.Items.Insert(2, "Create as PDF");
				}
			}
			else
			{
				if (cmbPrint.Items.Contains("Create as PDF"))
				{
					cmbPrint.Items.Remove("Create as PDF");
				}
			}
			Init = tReturn;
			this.Show(FormShowEnum.Modal, App.MainForm);
			Init = tReturn;
			return Init;
		}

		private void optPrint_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				Frame1.Visible = false;
				framFile.Visible = false;
			}
			else if (Index == 1)
			{
				Frame1.Visible = true;
				framFile.Visible = false;
			}
			else
			{
				framFile.Visible = true;
				Frame1.Visible = false;
			}
		}

		private void optPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbPrint.SelectedIndex);
			optPrint_CheckedChanged(index, sender, e);
		}

		private void cmbPrint_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbPrint.SelectedIndex == 0)
			{
				optPrint_CheckedChanged(sender, e);
			}
			else if (cmbPrint.SelectedIndex == 1)
			{
				optPrint_CheckedChanged(sender, e);
			}
			else if (cmbPrint.SelectedIndex == 2)
			{
				optPrint_CheckedChanged(sender, e);
			}
		}

		private void cmdContinue_Click(object sender, EventArgs e)
		{
			mnuContinue_Click(mnuContinue, EventArgs.Empty);
		}
	}
}
