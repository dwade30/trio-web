﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormatA.
	/// </summary>
	partial class rptBillFormatA
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillFormatA));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCatOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPageLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLateDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPercent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaidLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSubType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDueDate1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTaxDueCopy = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDist2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDist3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDist4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDistPerc2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDistPerc3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDistPerc4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDistPerc1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTaxDue2Copy = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMailing5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLateDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDueCopy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue2Copy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.txtCatOther,
				this.txtLocation,
				this.txtLand,
				this.txtBldg,
				this.txtValue,
				this.txtExemption,
				this.txtAssessment,
				this.txtTaxDue,
				this.txtBookPageLabel,
				this.txtBookPage,
				this.txtMessage1,
				this.txtLateDate,
				this.txtPercent,
				this.txtMessage4,
				this.txtMessage2,
				this.txtMessage3,
				this.txtDueDate2,
				this.txtAmount2,
				this.txtPrePaidLabel,
				this.txtPrePaid,
				this.txtAcct1,
				this.txtMailing1,
				this.txtMailing2,
				this.txtMailing3,
				this.txtMailing4,
				this.txtMapLot,
				this.lblTitle1,
				this.txtSubType,
				this.txtAccount,
				this.txtRate,
				this.txtAmount1,
				this.txtDueDate1,
				this.txtTaxDueCopy,
				this.txtMessage5,
				this.txtMessage6,
				this.txtDist1,
				this.txtDist2,
				this.txtDist3,
				this.txtDist4,
				this.txtDistPerc2,
				this.txtDistPerc3,
				this.txtDistPerc4,
				this.txtDistPerc1,
				this.txtTaxDue2Copy,
				this.txtMailing5
			});
			this.Detail.Height = 5.5F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.19F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 4.076389F;
			this.lblTitle.MultiLine = false;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "text-align: center";
			this.lblTitle.Tag = "textbox";
			this.lblTitle.Text = null;
			this.lblTitle.Top = 0.46875F;
			this.lblTitle.Width = 0.6875F;
			// 
			// txtCatOther
			// 
			this.txtCatOther.CanGrow = false;
			this.txtCatOther.Height = 0.19F;
			this.txtCatOther.Left = 3.625F;
			this.txtCatOther.MultiLine = false;
			this.txtCatOther.Name = "txtCatOther";
			this.txtCatOther.Style = "text-align: right";
			this.txtCatOther.Tag = "textbox";
			this.txtCatOther.Text = null;
			this.txtCatOther.Top = 1.46875F;
			this.txtCatOther.Width = 1F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 0F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1.625F;
			this.txtLocation.Width = 2.375F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 0F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 2.09375F;
			this.txtLand.Width = 1F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 1.0625F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 2.09375F;
			this.txtBldg.Width = 1.0625F;
			// 
			// txtValue
			// 
			this.txtValue.CanGrow = false;
			this.txtValue.Height = 0.19F;
			this.txtValue.Left = 2.25F;
			this.txtValue.MultiLine = false;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "text-align: right";
			this.txtValue.Tag = "textbox";
			this.txtValue.Text = null;
			this.txtValue.Top = 2.09375F;
			this.txtValue.Width = 1.25F;
			// 
			// txtExemption
			// 
			this.txtExemption.CanGrow = false;
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 2.375F;
			this.txtExemption.MultiLine = false;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Tag = "textbox";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 2.25F;
			this.txtExemption.Width = 1.1875F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.CanGrow = false;
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 2.5625F;
			this.txtAssessment.MultiLine = false;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Tag = "textbox";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 2.40625F;
			this.txtAssessment.Width = 1F;
			// 
			// txtTaxDue
			// 
			this.txtTaxDue.Height = 0.19F;
			this.txtTaxDue.Left = 3.625F;
			this.txtTaxDue.MultiLine = false;
			this.txtTaxDue.Name = "txtTaxDue";
			this.txtTaxDue.Style = "text-align: left; white-space: nowrap";
			this.txtTaxDue.Tag = "textbox";
			this.txtTaxDue.Text = null;
			this.txtTaxDue.Top = 2.40625F;
			this.txtTaxDue.Width = 1.1875F;
			// 
			// txtBookPageLabel
			// 
			this.txtBookPageLabel.CanGrow = false;
			this.txtBookPageLabel.Height = 0.19F;
			this.txtBookPageLabel.Left = 0.0625F;
			this.txtBookPageLabel.MultiLine = false;
			this.txtBookPageLabel.Name = "txtBookPageLabel";
			this.txtBookPageLabel.Tag = "textbox";
			this.txtBookPageLabel.Text = "Book & Page";
			this.txtBookPageLabel.Top = 2.71875F;
			this.txtBookPageLabel.Width = 1.125F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.CanGrow = false;
			this.txtBookPage.Height = 0.19F;
			this.txtBookPage.Left = 1.1875F;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 2.71875F;
			this.txtBookPage.Width = 2.1875F;
			// 
			// txtMessage1
			// 
			this.txtMessage1.CanGrow = false;
			this.txtMessage1.Height = 0.19F;
			this.txtMessage1.Left = 0.0625F;
			this.txtMessage1.MultiLine = false;
			this.txtMessage1.Name = "txtMessage1";
			this.txtMessage1.Tag = "textbox";
			this.txtMessage1.Text = " WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
			this.txtMessage1.Top = 2.875F;
			this.txtMessage1.Width = 4.5625F;
			// 
			// txtLateDate
			// 
			this.txtLateDate.CanGrow = false;
			this.txtLateDate.Height = 0.19F;
			this.txtLateDate.Left = 0.75F;
			this.txtLateDate.MultiLine = false;
			this.txtLateDate.Name = "txtLateDate";
			this.txtLateDate.Tag = "textbox";
			this.txtLateDate.Text = null;
			this.txtLateDate.Top = 3.65625F;
			this.txtLateDate.Width = 1F;
			// 
			// txtPercent
			// 
			this.txtPercent.CanGrow = false;
			this.txtPercent.Height = 0.19F;
			this.txtPercent.Left = 0.75F;
			this.txtPercent.MultiLine = false;
			this.txtPercent.Name = "txtPercent";
			this.txtPercent.Tag = "textbox";
			this.txtPercent.Text = null;
			this.txtPercent.Top = 3.34375F;
			this.txtPercent.Width = 0.5625F;
			// 
			// txtMessage4
			// 
			this.txtMessage4.CanGrow = false;
			this.txtMessage4.Height = 0.19F;
			this.txtMessage4.Left = 0.0625F;
			this.txtMessage4.MultiLine = false;
			this.txtMessage4.Name = "txtMessage4";
			this.txtMessage4.Tag = "textbox";
			this.txtMessage4.Text = " ";
			this.txtMessage4.Top = 4.90625F;
			this.txtMessage4.Width = 4.75F;
			// 
			// txtMessage2
			// 
			this.txtMessage2.CanGrow = false;
			this.txtMessage2.Height = 0.19F;
			this.txtMessage2.Left = 0.0625F;
			this.txtMessage2.MultiLine = false;
			this.txtMessage2.Name = "txtMessage2";
			this.txtMessage2.Tag = "textbox";
			this.txtMessage2.Text = " ";
			this.txtMessage2.Top = 4.59375F;
			this.txtMessage2.Width = 4.75F;
			// 
			// txtMessage3
			// 
			this.txtMessage3.CanGrow = false;
			this.txtMessage3.Height = 0.19F;
			this.txtMessage3.Left = 0.0625F;
			this.txtMessage3.MultiLine = false;
			this.txtMessage3.Name = "txtMessage3";
			this.txtMessage3.Tag = "textbox";
			this.txtMessage3.Text = " ";
			this.txtMessage3.Top = 4.75F;
			this.txtMessage3.Width = 4.75F;
			// 
			// txtDueDate2
			// 
			this.txtDueDate2.CanGrow = false;
			this.txtDueDate2.Height = 0.19F;
			this.txtDueDate2.Left = 2.4375F;
			this.txtDueDate2.MultiLine = false;
			this.txtDueDate2.Name = "txtDueDate2";
			this.txtDueDate2.Style = "text-align: left";
			this.txtDueDate2.Tag = "textbox";
			this.txtDueDate2.Text = null;
			this.txtDueDate2.Top = 4.25F;
			this.txtDueDate2.Visible = false;
			this.txtDueDate2.Width = 1F;
			// 
			// txtAmount2
			// 
			this.txtAmount2.Height = 0.19F;
			this.txtAmount2.Left = 3.5625F;
			this.txtAmount2.MultiLine = false;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Style = "text-align: left; white-space: nowrap";
			this.txtAmount2.Tag = "textbox";
			this.txtAmount2.Text = null;
			this.txtAmount2.Top = 4.25F;
			this.txtAmount2.Visible = false;
			this.txtAmount2.Width = 1.0625F;
			// 
			// txtPrePaidLabel
			// 
			this.txtPrePaidLabel.CanGrow = false;
			this.txtPrePaidLabel.Height = 0.19F;
			this.txtPrePaidLabel.Left = 2F;
			this.txtPrePaidLabel.MultiLine = false;
			this.txtPrePaidLabel.Name = "txtPrePaidLabel";
			this.txtPrePaidLabel.Tag = "textbox";
			this.txtPrePaidLabel.Text = "Paid to Date";
			this.txtPrePaidLabel.Top = 3.03125F;
			this.txtPrePaidLabel.Width = 1.25F;
			// 
			// txtPrePaid
			// 
			this.txtPrePaid.CanGrow = false;
			this.txtPrePaid.Height = 0.19F;
			this.txtPrePaid.Left = 3.25F;
			this.txtPrePaid.MultiLine = false;
			this.txtPrePaid.Name = "txtPrePaid";
			this.txtPrePaid.Tag = "textbox";
			this.txtPrePaid.Text = null;
			this.txtPrePaid.Top = 3.03125F;
			this.txtPrePaid.Width = 1.375F;
			// 
			// txtAcct1
			// 
			this.txtAcct1.CanGrow = false;
			this.txtAcct1.Height = 0.19F;
			this.txtAcct1.Left = 5.125F;
			this.txtAcct1.MultiLine = false;
			this.txtAcct1.Name = "txtAcct1";
			this.txtAcct1.Tag = "textbox";
			this.txtAcct1.Text = null;
			this.txtAcct1.Top = 2.09375F;
			this.txtAcct1.Width = 1.125F;
			// 
			// txtMailing1
			// 
			this.txtMailing1.CanGrow = false;
			this.txtMailing1.Height = 0.19F;
			this.txtMailing1.Left = 5.125F;
			this.txtMailing1.MultiLine = false;
			this.txtMailing1.Name = "txtMailing1";
			this.txtMailing1.Tag = "textbox";
			this.txtMailing1.Text = null;
			this.txtMailing1.Top = 2.40625F;
			this.txtMailing1.Width = 2.8125F;
			// 
			// txtMailing2
			// 
			this.txtMailing2.CanGrow = false;
			this.txtMailing2.Height = 0.19F;
			this.txtMailing2.Left = 5.125F;
			this.txtMailing2.MultiLine = false;
			this.txtMailing2.Name = "txtMailing2";
			this.txtMailing2.Tag = "textbox";
			this.txtMailing2.Text = null;
			this.txtMailing2.Top = 2.5625F;
			this.txtMailing2.Width = 2.8125F;
			// 
			// txtMailing3
			// 
			this.txtMailing3.CanGrow = false;
			this.txtMailing3.Height = 0.19F;
			this.txtMailing3.Left = 5.125F;
			this.txtMailing3.MultiLine = false;
			this.txtMailing3.Name = "txtMailing3";
			this.txtMailing3.Tag = "textbox";
			this.txtMailing3.Text = null;
			this.txtMailing3.Top = 2.71875F;
			this.txtMailing3.Width = 2.8125F;
			// 
			// txtMailing4
			// 
			this.txtMailing4.CanGrow = false;
			this.txtMailing4.Height = 0.19F;
			this.txtMailing4.Left = 5.125F;
			this.txtMailing4.MultiLine = false;
			this.txtMailing4.Name = "txtMailing4";
			this.txtMailing4.Tag = "textbox";
			this.txtMailing4.Text = null;
			this.txtMailing4.Top = 2.875F;
			this.txtMailing4.Width = 2.8125F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 2.5F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.46875F;
			this.txtMapLot.Width = 1.333333F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.19F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 5.125F;
			this.lblTitle1.MultiLine = false;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "";
			this.lblTitle1.Tag = "textbox";
			this.lblTitle1.Text = null;
			this.lblTitle1.Top = 4.59375F;
			this.lblTitle1.Width = 0.5F;
			// 
			// txtSubType
			// 
			this.txtSubType.Height = 0.19F;
			this.txtSubType.HyperLink = null;
			this.txtSubType.Left = 2.5F;
			this.txtSubType.MultiLine = false;
			this.txtSubType.Name = "txtSubType";
			this.txtSubType.Style = "";
			this.txtSubType.Tag = "textbox";
			this.txtSubType.Text = null;
			this.txtSubType.Top = 1.78125F;
			this.txtSubType.Width = 1F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.HyperLink = null;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.96875F;
			this.txtAccount.Width = 1F;
			// 
			// txtRate
			// 
			this.txtRate.Height = 0.19F;
			this.txtRate.HyperLink = null;
			this.txtRate.Left = 3.9375F;
			this.txtRate.MultiLine = false;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "";
			this.txtRate.Tag = "textbox";
			this.txtRate.Text = null;
			this.txtRate.Top = 0.96875F;
			this.txtRate.Width = 0.875F;
			// 
			// txtAmount1
			// 
			this.txtAmount1.Height = 0.19F;
			this.txtAmount1.HyperLink = null;
			this.txtAmount1.Left = 3.5625F;
			this.txtAmount1.MultiLine = false;
			this.txtAmount1.Name = "txtAmount1";
			this.txtAmount1.Style = "";
			this.txtAmount1.Tag = "textbox";
			this.txtAmount1.Text = null;
			this.txtAmount1.Top = 3.8125F;
			this.txtAmount1.Width = 1.25F;
			// 
			// txtDueDate1
			// 
			this.txtDueDate1.Height = 0.19F;
			this.txtDueDate1.HyperLink = null;
			this.txtDueDate1.Left = 2.4375F;
			this.txtDueDate1.MultiLine = false;
			this.txtDueDate1.Name = "txtDueDate1";
			this.txtDueDate1.Style = "text-align: left";
			this.txtDueDate1.Tag = "textbox";
			this.txtDueDate1.Text = null;
			this.txtDueDate1.Top = 3.8125F;
			this.txtDueDate1.Width = 1F;
			// 
			// txtTaxDueCopy
			// 
			this.txtTaxDueCopy.Height = 0.19F;
			this.txtTaxDueCopy.HyperLink = null;
			this.txtTaxDueCopy.Left = 5.125F;
			this.txtTaxDueCopy.MultiLine = false;
			this.txtTaxDueCopy.Name = "txtTaxDueCopy";
			this.txtTaxDueCopy.Style = "";
			this.txtTaxDueCopy.Tag = "textbox";
			this.txtTaxDueCopy.Text = null;
			this.txtTaxDueCopy.Top = 3.8125F;
			this.txtTaxDueCopy.Width = 1.3125F;
			// 
			// txtMessage5
			// 
			this.txtMessage5.CanGrow = false;
			this.txtMessage5.Height = 0.19F;
			this.txtMessage5.Left = 0.0625F;
			this.txtMessage5.MultiLine = false;
			this.txtMessage5.Name = "txtMessage5";
			this.txtMessage5.Tag = "textbox";
			this.txtMessage5.Text = " ";
			this.txtMessage5.Top = 5.0625F;
			this.txtMessage5.Width = 4.75F;
			// 
			// txtMessage6
			// 
			this.txtMessage6.CanGrow = false;
			this.txtMessage6.Height = 0.19F;
			this.txtMessage6.Left = 0.0625F;
			this.txtMessage6.MultiLine = false;
			this.txtMessage6.Name = "txtMessage6";
			this.txtMessage6.Tag = "textbox";
			this.txtMessage6.Text = " ";
			this.txtMessage6.Top = 5.21875F;
			this.txtMessage6.Visible = false;
			this.txtMessage6.Width = 4.75F;
			// 
			// txtDist1
			// 
			this.txtDist1.Height = 0.19F;
			this.txtDist1.HyperLink = null;
			this.txtDist1.Left = 0F;
			this.txtDist1.MultiLine = false;
			this.txtDist1.Name = "txtDist1";
			this.txtDist1.Style = "font-size: 10pt";
			this.txtDist1.Tag = "textbox";
			this.txtDist1.Text = null;
			this.txtDist1.Top = 0.625F;
			this.txtDist1.Visible = false;
			this.txtDist1.Width = 0.9375F;
			// 
			// txtDist2
			// 
			this.txtDist2.Height = 0.19F;
			this.txtDist2.HyperLink = null;
			this.txtDist2.Left = 1F;
			this.txtDist2.MultiLine = false;
			this.txtDist2.Name = "txtDist2";
			this.txtDist2.Style = "font-size: 10pt";
			this.txtDist2.Tag = "textbox";
			this.txtDist2.Text = null;
			this.txtDist2.Top = 0.625F;
			this.txtDist2.Visible = false;
			this.txtDist2.Width = 0.9375F;
			// 
			// txtDist3
			// 
			this.txtDist3.Height = 0.19F;
			this.txtDist3.HyperLink = null;
			this.txtDist3.Left = 2F;
			this.txtDist3.MultiLine = false;
			this.txtDist3.Name = "txtDist3";
			this.txtDist3.Style = "font-size: 10pt";
			this.txtDist3.Tag = "textbox";
			this.txtDist3.Text = null;
			this.txtDist3.Top = 0.625F;
			this.txtDist3.Visible = false;
			this.txtDist3.Width = 0.9375F;
			// 
			// txtDist4
			// 
			this.txtDist4.Height = 0.19F;
			this.txtDist4.HyperLink = null;
			this.txtDist4.Left = 3F;
			this.txtDist4.MultiLine = false;
			this.txtDist4.Name = "txtDist4";
			this.txtDist4.Style = "font-size: 10pt";
			this.txtDist4.Tag = "textbox";
			this.txtDist4.Text = null;
			this.txtDist4.Top = 0.625F;
			this.txtDist4.Visible = false;
			this.txtDist4.Width = 0.9375F;
			// 
			// txtDistPerc2
			// 
			this.txtDistPerc2.Height = 0.19F;
			this.txtDistPerc2.HyperLink = null;
			this.txtDistPerc2.Left = 1.125F;
			this.txtDistPerc2.MultiLine = false;
			this.txtDistPerc2.Name = "txtDistPerc2";
			this.txtDistPerc2.Style = "font-size: 10pt; text-align: center";
			this.txtDistPerc2.Tag = "textbox";
			this.txtDistPerc2.Text = null;
			this.txtDistPerc2.Top = 0.96875F;
			this.txtDistPerc2.Visible = false;
			this.txtDistPerc2.Width = 0.6875F;
			// 
			// txtDistPerc3
			// 
			this.txtDistPerc3.Height = 0.19F;
			this.txtDistPerc3.HyperLink = null;
			this.txtDistPerc3.Left = 2.125F;
			this.txtDistPerc3.MultiLine = false;
			this.txtDistPerc3.Name = "txtDistPerc3";
			this.txtDistPerc3.Style = "font-size: 10pt; text-align: center";
			this.txtDistPerc3.Tag = "textbox";
			this.txtDistPerc3.Text = null;
			this.txtDistPerc3.Top = 0.96875F;
			this.txtDistPerc3.Visible = false;
			this.txtDistPerc3.Width = 0.6875F;
			// 
			// txtDistPerc4
			// 
			this.txtDistPerc4.Height = 0.19F;
			this.txtDistPerc4.HyperLink = null;
			this.txtDistPerc4.Left = 3.125F;
			this.txtDistPerc4.MultiLine = false;
			this.txtDistPerc4.Name = "txtDistPerc4";
			this.txtDistPerc4.Style = "font-size: 10pt; text-align: center";
			this.txtDistPerc4.Tag = "textbox";
			this.txtDistPerc4.Text = null;
			this.txtDistPerc4.Top = 0.96875F;
			this.txtDistPerc4.Visible = false;
			this.txtDistPerc4.Width = 0.6875F;
			// 
			// txtDistPerc1
			// 
			this.txtDistPerc1.Height = 0.19F;
			this.txtDistPerc1.HyperLink = null;
			this.txtDistPerc1.Left = 0.1388889F;
			this.txtDistPerc1.MultiLine = false;
			this.txtDistPerc1.Name = "txtDistPerc1";
			this.txtDistPerc1.Style = "font-size: 10pt; text-align: center";
			this.txtDistPerc1.Tag = "textbox";
			this.txtDistPerc1.Text = null;
			this.txtDistPerc1.Top = 0.96875F;
			this.txtDistPerc1.Visible = false;
			this.txtDistPerc1.Width = 0.6875F;
			// 
			// txtTaxDue2Copy
			// 
			this.txtTaxDue2Copy.Height = 0.19F;
			this.txtTaxDue2Copy.HyperLink = null;
			this.txtTaxDue2Copy.Left = 5.125F;
			this.txtTaxDue2Copy.MultiLine = false;
			this.txtTaxDue2Copy.Name = "txtTaxDue2Copy";
			this.txtTaxDue2Copy.Style = "";
			this.txtTaxDue2Copy.Tag = "textbox";
			this.txtTaxDue2Copy.Text = null;
			this.txtTaxDue2Copy.Top = 4.25F;
			this.txtTaxDue2Copy.Visible = false;
			this.txtTaxDue2Copy.Width = 1.3125F;
			// 
			// txtMailing5
			// 
			this.txtMailing5.CanGrow = false;
			this.txtMailing5.Height = 0.19F;
			this.txtMailing5.Left = 5.125F;
			this.txtMailing5.MultiLine = false;
			this.txtMailing5.Name = "txtMailing5";
			this.txtMailing5.Tag = "textbox";
			this.txtMailing5.Text = null;
			this.txtMailing5.Top = 3.03125F;
			this.txtMailing5.Width = 2.8125F;
			// 
			// rptBillFormatA
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.999306F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLateDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDueCopy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue2Copy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPageLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLateDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPercent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaidLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtSubType;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDueDate1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTaxDueCopy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDist1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDist2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDist3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDist4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDistPerc2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDistPerc3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDistPerc4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDistPerc1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTaxDue2Copy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing5;
	}
}
