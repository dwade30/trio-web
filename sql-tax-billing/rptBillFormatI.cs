﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using System.IO;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormatI.
	/// </summary>
	public partial class rptBillFormatI : BaseSectionReport
	{
		public rptBillFormatI()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Laser Format";
		}

		public static rptBillFormatI InstancePtr
		{
			get
			{
				return (rptBillFormatI)Sys.GetInstance(typeof(rptBillFormatI));
			}
		}

		protected rptBillFormatI _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsMortgageAssociations.Dispose();
				clsMortgageHolders.Dispose();
				clsBills.Dispose();
				rsMultiRecipients.Dispose();
            }
			base.Dispose(disposing);
		}
		
		bool boolNotJustUnloading;
		bool boolShownModally;
		//clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		bool boolFirstBill;
		clsDRWrapper clsMortgageHolders = new clsDRWrapper();
		clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
		clsDRWrapper clsRateRecs = new clsDRWrapper();
		bool boolUsePreviousOwner;
		clsBillFormat clsTaxBillFormat = new clsBillFormat();
		clsRateRecord clsTaxRate = new clsRateRecord();
		double TaxRate;
		int intStartPage;
		clsDRWrapper clsBills = new clsDRWrapper();
		bool boolRE;
		bool boolPP;
		double Tax1;
		double Tax2;
		double Tax3;
		double Tax4;
		// vbPorter upgrade warning: Prepaid As double	OnWrite(double, string)
		double Prepaid;
		double DistPerc1;
		double DistPerc2;
		double DistPerc3;
		double DistPerc4;
		double DistPerc5;
		double DistPerc6;
		double DistPerc7;
		int intDistCats;
		double dblDiscountPercent;
		string strCat1 = "";
		string strCat2 = "";
		string strCat3 = "";
		int lngExempt;
		// vbPorter upgrade warning: lngTotAssess As int	OnWrite(short, double)
		int lngTotAssess;
		bool boolBlank;
		/// <summary>
		/// is this a blank paper or pre-printed paper
		/// </summary>
		clsDRWrapper rsMultiRecipients = new clsDRWrapper();
		const int ROWTOTALDUE = 17;
		const int ROWSECONDDUE = 16;
		const int ROWFIRSTDUE = 15;
		const int ROWPAIDTODATE = 14;
		const int ROWORIGBILL = 13;
		const int ROWRATE = 12;
		const int ROWTAXABLE = 10;
		const int ROWOTHER = 9;
		const int ROWHOMESTEAD = 8;
		const int ROWASSESSMENT = 7;
		bool boolReprintNewOwnerCopy;
		/// <summary>
		/// this will be set to true if there is a new owner and the recordset will not be advanced to create a copy for him
		/// </summary>
		bool boolPrintRecipientCopy;
		bool boolIsRegional;

		private struct AddrType
		{
			public string[] strAddress/*?  = new string[4 + 1] */;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public AddrType(int unusedParam)
			{
				this.strAddress = new string[5] {
					string.Empty,
					string.Empty,
					string.Empty,
					string.Empty,
					string.Empty
				};
			}
		};

		AddrType[] aryReturnAddress = null;

		public void LoadReturnAddresses()
		{
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                if (!modRegionalTown.IsRegionalTown())
                {
                    aryReturnAddress = new AddrType[1 + 1];
                    //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                    aryReturnAddress[0] = new AddrType(0);
                    boolIsRegional = false;
                    rsLoad.OpenRecordset("select * from returnaddress", modGlobalVariables.strBLDatabase);
                    aryReturnAddress[0].strAddress[0] = FCConvert.ToString(rsLoad.Get_Fields_String("address1"));
                    aryReturnAddress[0].strAddress[1] = FCConvert.ToString(rsLoad.Get_Fields_String("address2"));
                    aryReturnAddress[0].strAddress[2] = FCConvert.ToString(rsLoad.Get_Fields_String("address3"));
                    aryReturnAddress[0].strAddress[3] = FCConvert.ToString(rsLoad.Get_Fields_String("address4"));
                    if (Strings.Trim(aryReturnAddress[0].strAddress[2]) == "")
                    {
                        aryReturnAddress[0].strAddress[2] = Strings.Trim(aryReturnAddress[0].strAddress[3]);
                        aryReturnAddress[0].strAddress[3] = "";
                    }

                    if (Strings.Trim(aryReturnAddress[0].strAddress[1]) == "")
                    {
                        aryReturnAddress[0].strAddress[1] = Strings.Trim(aryReturnAddress[0].strAddress[2]);
                        aryReturnAddress[0].strAddress[2] = Strings.Trim(aryReturnAddress[0].strAddress[3]);
                        aryReturnAddress[0].strAddress[3] = "";
                    }

                    if (Strings.Trim(aryReturnAddress[0].strAddress[0]) == "")
                    {
                        aryReturnAddress[0].strAddress[0] = Strings.Trim(aryReturnAddress[0].strAddress[1]);
                        aryReturnAddress[0].strAddress[1] = Strings.Trim(aryReturnAddress[0].strAddress[2]);
                        aryReturnAddress[0].strAddress[2] = Strings.Trim(aryReturnAddress[0].strAddress[3]);
                        aryReturnAddress[0].strAddress[3] = "";
                    }
                }
                else
                {
                    int x = 0;
                    x = 0;
                    boolIsRegional = true;
                    rsLoad.OpenRecordset("select * from TBLREgions where townnumber > 0 order by townname",
                        "CentralData");
                    while (!rsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        if (Conversion.Val(rsLoad.Get_Fields("townnumber")) > x)
                        {
                            int oldX = x;
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            x = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("townnumber"))));
                            Array.Resize(ref aryReturnAddress, x + 1);
                            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                            for (int i = oldX; i < x; i++)
                            {
                                aryReturnAddress[i] = new AddrType(0);
                            }
                        }

                        rsLoad.MoveNext();
                    }

                    rsLoad.OpenRecordset("select * from returnaddress order by townnumber",
                        modGlobalVariables.strBLDatabase);
                    while (!rsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[0] =
                            FCConvert.ToString(rsLoad.Get_Fields_String("address1"));
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[1] =
                            FCConvert.ToString(rsLoad.Get_Fields_String("address2"));
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[2] =
                            FCConvert.ToString(rsLoad.Get_Fields_String("address3"));
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[3] =
                            FCConvert.ToString(rsLoad.Get_Fields_String("address4"));
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        if (Strings.Trim(aryReturnAddress[FCConvert.ToString(rsLoad.Get_Fields("townnumber"))]
                            .strAddress[2]) == "")
                        {
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[2] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("townnumber")))]
                                    .strAddress[3]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[3] = "";
                        }

                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        if (Strings.Trim(aryReturnAddress[FCConvert.ToString(rsLoad.Get_Fields("townnumber"))]
                            .strAddress[1]) == "")
                        {
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[1] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("townnumber")))]
                                    .strAddress[2]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[2] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("townnumber")))]
                                    .strAddress[3]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[3] = "";
                        }

                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        if (Strings.Trim(aryReturnAddress[FCConvert.ToString(rsLoad.Get_Fields("townnumber"))]
                            .strAddress[0]) == "")
                        {
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[0] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("townnumber")))]
                                    .strAddress[1]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[1] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("townnumber")))]
                                    .strAddress[2]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[2] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("townnumber")))]
                                    .strAddress[3]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[rsLoad.Get_Fields("townnumber")].strAddress[3] = "";
                        }

                        rsLoad.MoveNext();
                    }
                }
            }
        }
		// vbPorter upgrade warning: intYear As short	OnWrite(double, int)
		public void Init(clsBillFormat clsTaxFormat, int intYear, string strFontName, bool boolShow = true, string strPrinterN = "", bool boolModal = false, bool boolExportOnly = false, string strExportFile = "")
		{
			int x;
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                string strTemp = "";
                string strSql;
                //FileSystemObject fso = new FileSystemObject();
                boolShownModally = boolModal;
                boolNotJustUnloading = true;
                if (strPrinterN != string.Empty)
                {
                    this.Document.Printer.PrinterName = strPrinterN;
                }

                boolBlank = true;
                if (clsTaxFormat.BillsFrom == 0)
                {
                    boolRE = true;
                    if (clsTaxFormat.Combined)
                    {
                        boolPP = true;
                    }
                    else
                    {
                        boolPP = false;
                    }
                }
                else
                {
                    boolRE = false;
                    boolPP = true;
                }

                if (boolBlank)
                {
                    txtFirstPayment.Visible = true;
                    txtRemit1.Visible = true;
                    // txtFirstHalfDue.Visible = True
                    txtLocLabel.Visible = true;
                    txtMapLotLabel.Visible = true;
                    shapeInformation.Visible = true;
                    Line1.Visible = true;
                    Line2.Visible = true;
                    txtTaxBillYear.Visible = true;
                    txtTaxBillYear.Text = FCConvert.ToString(intYear) + " Tax Bill";
                    if (clsTaxFormat.UseClearLaserBackground)
                    {
                        //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                        //GridStub1.ForeColor = Color.FromArgb(1, 1, 1);
                        //GridStub1.BackColor = Color.FromArgb(255, 255, 255);
                        //GridStub2.ForeColor = GridStub1.ForeColor;
                        //GridStub2.BackColor = GridStub1.BackColor;
                        //FC:FINAL:MSH - restore missing settings
                        txtStub11.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub11.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub12.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub12.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub13.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub13.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub21.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub21.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub22.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub22.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub23.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub23.BackColor = Color.FromArgb(255, 255, 255);
                    }
                    else
                    {
                        //FC:FINAL:MSH - set default color values for textboxes
                        txtStub11.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub11.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub12.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub12.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub13.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub13.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub21.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub21.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub22.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub22.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub23.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub23.BackColor = Color.FromArgb(1, 1, 1);
                    }
                }

                clsTemp.OpenRecordset("select * from billsetup", "twbl0000.vb1");
                if (!clsTemp.EndOfFile())
                {
                    if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("showpicture")))
                    {
                        if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicture"))) != string.Empty)
                        {
                            if (File.Exists(
                                Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicturepath"))) +
                                Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicture")))))
                            {
                                Image1.Image = FCUtils.LoadPicture(
                                    Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicturepath"))) +
                                    Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicture"))));
                                Image1.Visible = true;
                            }
                        }
                        else
                        {
                            Image1.Image = null;
                        }
                    }
                    else
                    {
                        Image1.Image = null;
                    }
                }
                else
                {
                    Image1.Image = null;
                }

                clsTaxBillFormat.CopyFormat(ref clsTaxFormat);
                SetupGrids();
                if (!boolRE)
                {
                    txtBookPage.Visible = false;
                }

                if (boolRE && clsTaxBillFormat.PrintCopyForMortgageHolders)
                {
                    boolFirstBill = true;
                    clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
                    clsMortgageAssociations.OpenRecordset(
                        "select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT",
                        "CentralData");
                    if (!clsMortgageAssociations.EndOfFile())
                    {
                        clsMortgageAssociations.MoveFirst();
                        clsMortgageAssociations.MovePrevious();
                    }
                }
                else
                {
                    boolFirstBill = true;
                }

                strSql = clsTaxBillFormat.SQLStatement;
                clsTaxRate.TaxYear = intYear;
                clsRateRecs.OpenRecordset("select * from raterec where year = " + FCConvert.ToString(intYear),
                    "twcl0000.vb1");
                if (!clsRateRecs.EndOfFile())
                {
                    clsTaxRate.LoadRate(clsRateRecs.Get_Fields_Int32("ID"));
                }

                clsTemp.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
                    switch (FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("line"))))
                    {
                        case 0:
                        {
                            txtInfoLbl1.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 1:
                        {
                            txtInfoLbl2.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 2:
                        {
                            txtInfoLbl3.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 3:
                        {
                            txtInfoLbl4.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 4:
                        {
                            txtInfoLbl5.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 5:
                        {
                            txtInfoLbl6.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 6:
                        {
                            txtInfoLbl7.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 7:
                        {
                            txtInfoLbl8.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 8:
                        {
                            txtInfoLbl9.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 9:
                        {
                            txtInfoLbl10.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 10:
                        {
                            txtInfoLbl11.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 11:
                        {
                            txtInfoLbl12.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                    }

                    //end switch
                    clsTemp.MoveNext();
                }

                clsTemp.OpenRecordset("select * from remittancemessage order by line", "twbl0000.vb1");
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
                    switch (FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("line"))))
                    {
                        case 0:
                        {
                            txtRemitLbl1.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 1:
                        {
                            txtRemitLbl2.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 2:
                        {
                            txtRemitLbl3.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 3:
                        {
                            txtRemitLbl4.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 4:
                        {
                            txtRemitLbl5.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 5:
                        {
                            txtRemitLbl6.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                        case 6:
                        {
                            txtRemitLbl7.Text = clsTemp.Get_Fields_String("message");
                            break;
                        }
                    }

                    //end switch
                    clsTemp.MoveNext();
                }

                if (clsTaxBillFormat.Discount)
                {
                    clsTemp.OpenRecordset("select * from discount", "twbl0000.vb1");
                    if (!clsTemp.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
                        dblDiscountPercent = Conversion.Val(clsTemp.Get_Fields("discount")) / 100;
                        // TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
                        txtDiscount.Text = Strings.Format(clsTemp.Get_Fields("discount"), "#0.00") + "%";
                        // TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
                        txtDiscountDate.Text = clsTemp.Get_Fields("duedate");
                        txtDiscountMsg1.Visible = true;
                        txtDiscountMsg2.Visible = true;
                        txtDiscount.Visible = true;
                        txtDiscountDate.Visible = true;
                        txtDiscountAmount.Visible = true;
                    }
                }

                if (clsTaxRate.NumberOfPeriods < 2)
                {
                    txtAmount2.Visible = false;
                    txtDueDate2.Visible = false;
                    if (boolBlank)
                    {
                        txtNA.Visible = true;
                    }
                }
                else
                {
                    txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
                    txtBillYearStub2.Visible = true;
                    //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                    //CustomGridStubInfo2.Visible = true;
                    txtStubInfo211.Visible = true;
                    txtStubInfo212.Visible = true;
                    txtStubInfo221.Visible = true;
                    txtStubInfo222.Visible = true;
                    txtStubInfo231.Visible = true;
                    txtStubInfo232.Visible = true;
                    txtStubInfo241.Visible = true;
                    txtStubInfo242.Visible = true;
                    txtAmount2.Visible = true;
                    txtDueDate2.Visible = true;
                    if (boolBlank)
                    {
                        txtSecondPayment.Visible = true;
                        txtRemit2.Visible = true;
                    }
                }

                txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
                if (clsTaxBillFormat.UseLaserAlignment)
                {
                    if (Conversion.Val(clsTaxBillFormat.LaserAlignment) != 0)
                    {
                        for (x = 0; x <= Detail.Controls.Count - 1; x++)
                        {
                            if (FCConvert.ToString(Detail.Controls[x].Tag) == "Line")
                            {
                                (Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 +=
                                    FCConvert.ToSingle(240 * clsTaxBillFormat.LaserAlignment) / 1440F;
                                (Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Line).Y2 +=
                                    FCConvert.ToSingle(240 * clsTaxBillFormat.LaserAlignment) / 1440F;
                            }
                            else
                            {
                                Detail.Controls[x].Top +=
                                    FCConvert.ToSingle(240 * clsTaxBillFormat.LaserAlignment) / 1440F;
                            }
                        }

                        // x
                    }
                }

                GetBills(ref strSql);
                if (boolShow)
                {
                    // Me.Show , MDIParent
                }
                else
                {
                    this.PrintReport(false);
                }
            }
        }

		private void GetBills(ref string strSql)
		{
			clsBills.OpenRecordset(strSql, "twcl0000.vb1");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsBills.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
			
			if (clsTaxBillFormat.ReturnAddress)
			{
				LoadReturnAddresses();
				if (!boolIsRegional)
				{
					txtAddress1.Text = aryReturnAddress[0].strAddress[0];
					txtAddress2.Text = aryReturnAddress[0].strAddress[1];
					txtAddress3.Text = aryReturnAddress[0].strAddress[2];
					txtAddress4.Text = aryReturnAddress[0].strAddress[3];
				}
				if (txtAddress3.Text == "")
				{
					txtAddress3.Text = txtAddress4.Text;
					txtAddress4.Text = "";
				}
				if (txtAddress2.Text == "")
				{
					txtAddress2.Text = txtAddress3.Text;
					txtAddress3.Text = txtAddress4.Text;
					txtAddress4.Text = "";
				}
			}
			else
			{
				txtAddress1.Visible = false;
				txtAddress2.Visible = false;
				txtAddress3.Visible = false;
				txtAddress4.Visible = false;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double Dist1 = 0;
			double Dist2 = 0;
			double Dist3 = 0;
			double Dist4 = 0;
			double Dist5 = 0;
			double Dist6 = 0;
			double Dist7 = 0;
			// vbPorter upgrade warning: TotTax As double	OnWrite(string)
			double TotTax = 0;
			string strTemp = "";
			string strAcct = "";
			string strMapLot = "";
			string strName = "";
			string strLocation = "";
			int lngHomestead = 0;
			int lngOther = 0;
			DateTime dtLastDate;
			string strOldName = "";
			string strSecOwner = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string, double, short)
			double dblTemp = 0;
			string strCountry = "";
			string strMailingAddress3 = "";
			txtMailing6.Text = "";
			if (!clsBills.EndOfFile())
			{
				txtMailing5.Text = "";
				if (FCConvert.ToString(clsBills.Get_Fields_String("Billingtype")) == "RE")
				{
					boolRE = true;
					boolPP = false;
				}
				else if (clsBills.Get_Fields_String("Billingtype") == "PP")
				{
					boolRE = false;
					boolPP = true;
				}
				else
				{
					// combined
					boolRE = true;
					boolPP = true;
				}
				if (!boolRE)
				{
					txtBookPage.Visible = false;
				}
				else
				{
					txtBookPage.Visible = true;
				}
				if (clsRateRecs.FindFirstRecord("ID", clsBills.Get_Fields_Int32("ratekey")))
				{
					clsTaxRate.LoadRate(clsBills.Get_Fields_Int32("ratekey"), clsRateRecs);
					if (clsTaxRate.NumberOfPeriods < 2)
					{
						// GridValuation.TextMatrix(ROWSECONDDUE, 0) = ""
						// GridValuation.TextMatrix(ROWSECONDDUE, 1) = ""
						// GridValuation.TextMatrix(ROWFIRSTDUE, 0) = ""
						// GridValuation.TextMatrix(ROWFIRSTDUE, 1) = ""
						(Detail.Controls["txtValLbl" + ROWSECONDDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtValData" + ROWSECONDDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtValLbl" + ROWFIRSTDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtValData" + ROWFIRSTDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						// framStub2.Visible = False
						// txtPayment2Label.Visible = False
						txtAmount2.Visible = false;
						txtDueDate2.Visible = false;
						if (boolBlank)
						{
							txtNA.Visible = true;
						}
						//FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
						//CustomGridStubInfo2.Visible = false;
						txtStubInfo211.Visible = false;
						txtStubInfo212.Visible = false;
						txtStubInfo221.Visible = false;
						txtStubInfo222.Visible = false;
						txtStubInfo231.Visible = false;
						txtStubInfo232.Visible = false;
						txtStubInfo241.Visible = false;
						txtStubInfo242.Visible = false;
						txtSecondPayment.Visible = false;
						txtRemit2.Visible = false;
					}
					else
					{
						// framStub2.Visible = True
						txtNA.Visible = false;
						// GridValuation.TextMatrix(ROWSECONDDUE, 0) = "Second Due " & Format(clsTaxRate.Get_DueDate(2), "M/dd/yy")
						(Detail.Controls["txtValLbl" + ROWSECONDDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Second Due " + Strings.Format(clsTaxRate.Get_DueDate(2), "M/dd/yy");
						// GridValuation.TextMatrix(ROWFIRSTDUE, 0) = "First Due " & Format(clsTaxRate.Get_DueDate(1), "M/dd/yy")
						(Detail.Controls["txtValLbl" + ROWFIRSTDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "First Due " + Strings.Format(clsTaxRate.Get_DueDate(1), "M/dd/yy");
						txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
						txtBillYearStub2.Visible = true;
						//FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
						//CustomGridStubInfo2.Visible = true;
						txtStubInfo211.Visible = true;
						txtStubInfo212.Visible = true;
						txtStubInfo221.Visible = true;
						txtStubInfo222.Visible = true;
						txtStubInfo231.Visible = true;
						txtStubInfo232.Visible = true;
						txtStubInfo241.Visible = true;
						txtStubInfo242.Visible = true;
						txtAmount2.Visible = true;
						txtDueDate2.Visible = true;
						// txtPayment2Label.Visible = True
						// txtSecondDue.Visible = True
						// txtSecondDue.Text = clsTaxRate.Get_DueDate(2)
						if (boolBlank)
						{
							txtSecondPayment.Visible = true;
							txtRemit2.Visible = true;
							// txtSecondHalfDue.Visible = True
							// txtFirstHalfDue.Text = "First Half Due"
							// txtSecondAmountDue.Visible = True
						}
					}
					// End If
					txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
					// txtFirstDue.Text = clsTaxRate.Get_DueDate(1)
				}
				lngTotAssess = 0;
				lngExempt = 0;
				Tax1 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue1"));
				Tax2 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue2"));
				Tax3 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue3"));
				Tax4 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue4"));
				TotTax = FCConvert.ToDouble(Strings.Format(Tax1 + Tax2 + Tax3 + Tax4, "0.00"));
				// GridValuation.TextMatrix(ROWTOTALDUE, 1) = Format(TotTax, "#,###,###,##0.00")
				(Detail.Controls["txtValData" + ROWTOTALDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(TotTax, "#,###,###,##0.00");
				// GridValuation.TextMatrix(ROWORIGBILL, 0) = "Original Bill"
				(Detail.Controls["txtValLbl" + ROWORIGBILL] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Original Bill";
				// GridValuation.TextMatrix(ROWORIGBILL, 1) = Format(TotTax, "#,###,###,##0.00")
				(Detail.Controls["txtValData" + ROWORIGBILL] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(TotTax, "#,###,###,##0.00");
				if (clsTaxBillFormat.BreakdownDollars)
				{
					Dist1 = TotTax * DistPerc1;
					Dist2 = TotTax * DistPerc2;
					Dist3 = TotTax * DistPerc3;
					Dist4 = TotTax * DistPerc4;
					Dist5 = TotTax * DistPerc5;
					Dist6 = TotTax * DistPerc6;
					Dist7 = TotTax * DistPerc7;
					if (Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7 != TotTax)
					{
						// correct it so the sum of the dists is equal to the total tax
						if (Dist7 > 0)
						{
							Dist7 += (TotTax - (Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7));
						}
						else if (Dist6 > 0)
						{
							Dist6 += (TotTax - (Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7));
						}
						else if (Dist5 > 0)
						{
							Dist5 += (TotTax - (Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7));
						}
						else if (Dist4 > 0)
						{
							Dist4 += (TotTax - (Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7));
						}
						else if (Dist3 > 0)
						{
							Dist3 += (TotTax - (Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7));
						}
						else if (Dist2 > 0)
						{
							Dist2 += (TotTax - (Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7));
						}
						else if (Dist1 > 0)
						{
							Dist1 += (TotTax - (Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7));
						}
					}
					// GridDistribution.TextMatrix(1, 2) = Format(Dist1, "##,###,##0.00")
					txtDistAm1.Text = Strings.Format(Dist1, "##,###,##0.00");
					if (intDistCats > 1)
					{
						// GridDistribution.TextMatrix(2, 2) = Format(Dist2, "##,###,##0.00")
						txtDistAm2.Text = Strings.Format(Dist2, "#,###,##0.00");
					}
					if (intDistCats > 2)
					{
						// GridDistribution.TextMatrix(3, 2) = Format(Dist3, "##,###,##0.00")
						txtDistAm3.Text = Strings.Format(Dist3, "#,###,##0.00");
					}
					if (intDistCats > 3)
					{
						// GridDistribution.TextMatrix(4, 2) = Format(Dist4, "##,###,##0.00")
						txtDistAm4.Text = Strings.Format(Dist4, "##,###,##0.00");
					}
					if (intDistCats > 4)
					{
						// GridDistribution.TextMatrix(5, 2) = Format(Dist5, "##,###,##0.00")
						txtDistAm5.Text = Strings.Format(Dist5, "#,###,##0.00");
					}
					if (intDistCats > 5)
					{
						// GridDistribution.TextMatrix(6, 2) = Format(Dist6, "##,###,##0.00")
						txtDistAm6.Text = Strings.Format(Dist6, "#,###,##0.00");
					}
					if (intDistCats > 6)
					{
						// GridDistribution.TextMatrix(7, 2) = Format(Dist7, "##,###,##0.00")
						txtDistAm7.Text = Strings.Format(Dist7, "#,###,##0.00");
					}
				}
				Prepaid = Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid"));
				double dblAbated = 0;
				double dblAbate1 = 0;
				double dblAbate2 = 0;
				double dblAbate3 = 0;
				double dblAbate4 = 0;
				int intPer;
				double[] dblTAbate = new double[4 + 1];
				dblAbated = 0;
				if ((Tax2 > 0 || Tax3 > 0 || Tax4 > 0) && Prepaid > 0)
				{
					dblAbated = modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields_Int32("ID"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
					dblAbate1 = dblTAbate[1];
					dblAbate2 = dblTAbate[2];
					dblAbate3 = dblTAbate[3];
					dblAbate4 = dblTAbate[4];
					if (boolRE && boolPP)
					{
						// TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
						dblAbated += modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields("ppbillkey"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
						dblAbate1 += dblTAbate[1];
						dblAbate2 += dblTAbate[2];
						dblAbate3 += dblTAbate[3];
						dblAbate4 += dblTAbate[4];
					}
					Prepaid -= dblAbated;
				}
				if (dblAbated > 0)
				{
					Tax1 -= dblAbate1;
					Tax2 -= dblAbate2;
					Tax3 -= dblAbate3;
					Tax4 -= dblAbate4;
				}
				if (Prepaid > 0)
				{
					if (Prepaid > Tax1)
					{
						Prepaid -= Tax1;
						Tax1 = 0;
						if (Prepaid > Tax2)
						{
							Prepaid -= Tax2;
							Tax2 = 0;
							if (Prepaid > Tax3)
							{
								Prepaid -= Tax3;
								Tax3 = 0;
								if (Prepaid > Tax4)
								{
									Prepaid -= Tax4;
									Tax4 = 0;
								}
								else
								{
									Tax4 -= Prepaid;
								}
							}
							else
							{
								Tax3 -= Prepaid;
							}
						}
						else
						{
							Tax2 -= Prepaid;
						}
					}
					else
					{
						Tax1 -= Prepaid;
					}
				}
				Prepaid = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid")), "0.00"));
				if (Prepaid == 0)
				{
					// GridValuation.TextMatrix(ROWPAIDTODATE, 0) = ""
					(Detail.Controls["txtValLbl" + ROWPAIDTODATE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					// GridValuation.TextMatrix(ROWPAIDTODATE, 1) = ""
					(Detail.Controls["txtValData" + ROWPAIDTODATE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
				else
				{
					// GridValuation.TextMatrix(ROWPAIDTODATE, 0) = "Paid To Date"
					(Detail.Controls["txtValLbl" + ROWPAIDTODATE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Paid To Date";
					// GridValuation.TextMatrix(ROWPAIDTODATE, 1) = Format(Prepaid, "#,###,##0.00")
					(Detail.Controls["txtValData" + ROWPAIDTODATE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(Prepaid, "#,###,##0.00");
					if (Prepaid > TotTax)
					{
						// GridValuation.TextMatrix(ROWTOTALDUE, 1) = "Overpaid"
						(Detail.Controls["txtValData" + ROWTOTALDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Overpaid";
					}
					else
					{
						// GridValuation.TextMatrix(ROWTOTALDUE, 1) = Format(TotTax - Prepaid, "#,###,##0.00")
						(Detail.Controls["txtValData" + ROWTOTALDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(TotTax - Prepaid, "#,###,##0.00");
					}
				}
				// GridValuation.TextMatrix(ROWRATE, 1) = Format(clsTaxRate.TaxRate * 1000, "#0.000")
				(Detail.Controls["txtValData" + ROWRATE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTaxRate.TaxRate * 1000, "#0.000");
				txtAmount1.Text = Strings.Format(Tax1, "#,###,##0.00");
				txtAmount2.Text = Strings.Format(Tax2, "#,###,##0.00");
				// txtFirstAmountDue.Text = txtAmount1.Text
				if (clsTaxRate.NumberOfPeriods > 1)
				{
					// GridValuation.TextMatrix(ROWFIRSTDUE, 1) = txtAmount1.Text
					(Detail.Controls["txtValData" + ROWFIRSTDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = txtAmount1.Text;
					// GridValuation.TextMatrix(ROWSECONDDUE, 1) = txtAmount2.Text
					(Detail.Controls["txtValData" + ROWSECONDDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = txtAmount2.Text;
				}
				// txtSecondAmountDue.Text = txtAmount2.Text
				// txtAmount3.Text = Format(Tax3, "#,###,##0.00")
				// txtAmount4.Text = Format(Tax4, "#,###,##0.00")
				// If clsTaxBillFormat.Discount Then
				// txtDiscountAmount.Text = Format(TotTax - (TotTax * dblDiscountPercent), "#,###,##0.00")
				// End If
				if (clsTaxBillFormat.Discount)
				{
					double dblDisc = 0;
					dblDisc = modMain.Round(TotTax * dblDiscountPercent, 2);
					txtDiscountAmount.Text = Strings.Format(TotTax - FCConvert.ToDouble(Strings.Format(dblDisc, "0.00")), "#,###,##0.00");
					dblTemp = FCConvert.ToDouble(Strings.Format(TotTax - FCConvert.ToDouble(Strings.Format(dblDisc, "0.00")), "0.00"));
					if (Prepaid > TotTax)
					{
						txtDiscountAmount.Text = "0.00";
					}
					else
					{
						if (Prepaid > 0)
						{
							dblTemp -= Prepaid;
							if (dblTemp < 0)
								dblTemp = 0;
							txtDiscountAmount.Text = Strings.Format(dblTemp, "#,###,##0.00");
						}
						else
						{
							txtDiscountAmount.Text = Strings.Format(dblTemp, "#,###,##0.00");
						}
					}
				}
				// 
				strAcct = "";
				txtAcres.Visible = false;
				lblAcres.Visible = false;
				txtValData3.Text = "";
				txtValData4.Text = "";
				txtValData5.Text = "";
				txtValData6.Text = "";
				txtValLbl3.Text = "";
				txtValLbl4.Text = "";
				txtValLbl5.Text = "";
				txtValLbl6.Text = "";
				if (clsTaxBillFormat.ReturnAddress)
				{
					if (boolIsRegional)
					{
						int intTemp = 0;
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields("trancode"))));
						if (intTemp > 0)
						{
							txtAddress1.Text = aryReturnAddress[intTemp].strAddress[0];
							txtAddress2.Text = aryReturnAddress[intTemp].strAddress[1];
							txtAddress3.Text = aryReturnAddress[intTemp].strAddress[2];
							txtAddress4.Text = aryReturnAddress[intTemp].strAddress[3];
						}
					}
				}
				txtMailing6.Text = "";
				if (boolRE)
				{
					txtValLbl1.Visible = true;
					txtValLbl2.Visible = true;
					txtTaxBillYear.Text = FCConvert.ToString(clsTaxRate.TaxYear) + " Real Estate Tax Bill";
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					strAcct = "R" + clsBills.Get_Fields("account");
					if (boolBlank)
					{
						txtAcres.Visible = true;
						lblAcres.Visible = true;
						txtAcres.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Double("acres")), "#,##0.00");
					}
					if (Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage"))) != string.Empty)
					{
						txtBookPage.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage")));
						txtBookPage.Visible = true;
						txtBookPageLabel.Visible = true;
					}
					else
					{
						txtBookPage.Visible = false;
						txtBookPageLabel.Visible = false;
					}
					txtValData1.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")), "#,###,###,##0");
					txtValData2.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")), "#,###,###,##0");
					lngTotAssess = FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")) + Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")));
					if (boolPrintRecipientCopy)
					{
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						txtMailing2.Text = "C/O " + rsMultiRecipients.Get_Fields_String("Name");
						txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address1");
						txtMailing4.Text = rsMultiRecipients.Get_Fields_String("address2");
						txtMailing5.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
						// ElseIf clsTaxBillFormat.UsePreviousOwnerInfo And NewOwner(clsBills.Fields("Account"), clsTaxBillFormat.BillAsOfDate, strOldName, strAddr1, strAddr2, strAddr3) Then
					}
					else if (!boolFirstBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
					{
						clsMortgageHolders.FindFirstRecord("ID", clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"));
						if (!clsMortgageHolders.NoMatch)
						{
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing2.Text = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
							txtMailing5.Text = Strings.Trim(clsMortgageHolders.Get_Fields_String("city") + " " + clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
						}
						else
						{
							txtMailing1.Text = "Mortgage Holder Not Found";
							txtMailing2.Text = "";
							txtMailing3.Text = "";
							txtMailing4.Text = "";
							txtMailing5.Text = "";
						}
					}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (boolReprintNewOwnerCopy && modMain.HasNewOwner_28439(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3, ref strCountry, ref strMailingAddress3))
					{
						// If boolReprintNewOwnerCopy Then
						// txtMailing2.Text = Trim(clsBills.Fields("rsaddr1"))
						// txtMailing3.Text = Trim(clsBills.Fields("rsaddr2"))
						// txtMailing4.Text = Trim(clsBills.Fields("rsaddr3")) & "  " & clsBills.Fields("rsstate") & "  " & clsBills.Fields("rszip")
						// If Trim(clsBills.Fields("rszip4")) <> vbNullString Then
						// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("rszip4"))
						// End If
						// txtMailing1.Text = Trim(clsBills.Fields("rsname"))
						// Else
						txtMailing1.Text = strOldName;
						txtMailing2.Text = strSecOwner;
						txtMailing3.Text = strAddr1;
						txtMailing4.Text = strAddr2;
						if (strMailingAddress3 != "")
						{
							txtMailing5.Text = strMailingAddress3;
							txtMailing6.Text = strAddr3;
						}
						else
						{
							txtMailing5.Text = strAddr3;
							txtMailing6.Text = strCountry;
						}
						// End If
					}
					else
					{
						txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
						txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
						txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("MailingAddress3")));
						txtMailing6.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
						// If Trim(clsBills.Fields("rssecowner")) <> vbNullString Then
						// If Trim(txtMailing2.Text) = vbNullString Or Trim(txtMailing3.Text) = vbNullString Then
						// If Trim(txtMailing2.Text) <> vbNullString Then
						// txtMailing3.Text = txtMailing2.Text
						// End If
						// txtMailing2.Text = Trim(clsBills.Fields("rssecowner"))
						// 
						// End If
						// End If
						// txtMailing4.Text = Trim(clsBills.Fields("rsaddr3")) & "  " & clsBills.Fields("rsstate") & "  " & clsBills.Fields("rszip")
						// If Trim(clsBills.Fields("rszip4")) <> vbNullString Then
						// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("rszip4"))
						// End If
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						strName = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name2")));
					}
					if (txtMailing5.Text == "")
					{
						txtMailing5.Text = txtMailing6.Text;
						txtMailing6.Text = "";
					}
					if (txtMailing4.Text == "")
					{
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = txtMailing6.Text;
						txtMailing6.Text = "";
					}
					if (txtMailing3.Text == "")
					{
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = txtMailing6.Text;
						txtMailing6.Text = "";
					}
					if (txtMailing2.Text == "")
					{
						txtMailing2.Text = txtMailing3.Text;
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = txtMailing6.Text;
						txtMailing6.Text = "";
					}
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						strLocation = FCConvert.ToString(clsBills.Get_Fields("streetnumber"));
					}
					else
					{
						strLocation = "";
					}
					strLocation = Strings.Trim(Strings.Trim(strLocation + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
					txtLocation.Text = strLocation;
					txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("maplot")));
					strMapLot = txtMapLot.Text;
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				}
				if (boolPP)
				{
					// pp
					if (boolRE)
					{
						// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBills.Get_Fields("ppac")) > 0)
						{
							strAcct += " P";
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							strAcct += FCConvert.ToString(Conversion.Val(clsBills.Get_Fields("ppac")));
							txtTaxBillYear.Text = FCConvert.ToString(clsTaxRate.TaxYear) + " Combined Tax Bill";
						}
						txtValLbl1.Visible = true;
						txtValLbl2.Visible = true;
					}
					else
					{
						strAcct = "P";
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						strAcct += clsBills.Get_Fields("account");
						txtTaxBillYear.Text = FCConvert.ToString(clsTaxRate.TaxYear) + " Personal Property Tax Bill";
						txtValLbl1.Visible = false;
						txtValLbl2.Visible = false;
						txtValData1.Text = "";
						txtValData2.Text = "";
					}
					txtValLbl3.Text = strCat1;
					txtValLbl4.Text = strCat2;
					txtValLbl5.Text = strCat3;
					txtValLbl6.Text = "Other P/P";
					// GridValuation.TextMatrix(3, 1) = Format(Val(clsBills.Fields("cat1")), "#,###,###,##0")
					txtValData3.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category1")), "#,###,###,##0");
					// GridValuation.TextMatrix(4, 1) = Format(Val(clsBills.Fields("cat2")), "#,###,###,##0")
					txtValData4.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category2")), "#,###,###,##0");
					// GridValuation.TextMatrix(5, 1) = Format(Val(clsBills.Fields("cat3")), "#,###,###,##0")
					txtValData5.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category3")), "#,###,###,##0");
					// GridValuation.TextMatrix(6, 1) = Format(Val(clsBills.Fields("cat4")) + Val(clsBills.Fields("cat5")) + Val(clsBills.Fields("cat6")) + Val(clsBills.Fields("cat7")) + Val(clsBills.Fields("cat8")) + Val(clsBills.Fields("cat9")), "#,###,###,##0")
					txtValData6.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category4")) + Conversion.Val(clsBills.Get_Fields_Int32("category5")) + Conversion.Val(clsBills.Get_Fields_Int32("category6")) + Conversion.Val(clsBills.Get_Fields_Int32("category7")) + Conversion.Val(clsBills.Get_Fields_Int32("category8")) + Conversion.Val(clsBills.Get_Fields_Int32("category9")), "#,###,###,##0");
					lngTotAssess += FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("ppassessment")));
					if (!boolRE)
					{
						txtBookPage.Visible = false;
						txtBookPageLabel.Visible = false;
						txtMapLot.Text = "";
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						if (Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0)
						{
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							strLocation = FCConvert.ToString(clsBills.Get_Fields("streetnumber"));
						}
						else
						{
							strLocation = "";
						}
						strLocation = Strings.Trim(Strings.Trim(strLocation + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
						txtLocation.Text = strLocation;
						strName = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						strMapLot = "";
						if (boolPrintRecipientCopy)
						{
							txtMailing1.Text = rsMultiRecipients.Get_Fields_String("Name");
							txtMailing2.Text = rsMultiRecipients.Get_Fields_String("address1");
							txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address2");
							txtMailing4.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
							txtMailing5.Text = "";
						}
						else
						{
							txtMailing5.Text = "";
							txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("MailingAddress3")));
							txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
							// strTemp = Trim(clsBills.Fields("zip"))
							// txtMailing4.Text = Trim(clsBills.Fields("city")) & "  " & clsBills.Fields("state") & "  " & strTemp
							// If Trim(clsBills.Fields("zip4")) <> vbNullString Then
							// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("zip4"))
							// End If
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						}
						if (txtMailing4.Text == "")
						{
							txtMailing4.Text = txtMailing5.Text;
							txtMailing5.Text = txtMailing6.Text;
							txtMailing6.Text = "";
						}
						if (txtMailing3.Text == "")
						{
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = txtMailing5.Text;
							txtMailing5.Text = txtMailing6.Text;
							txtMailing6.Text = "";
						}
						if (txtMailing2.Text == "")
						{
							txtMailing2.Text = txtMailing3.Text;
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = txtMailing5.Text;
							txtMailing5.Text = txtMailing6.Text;
							txtMailing6.Text = "";
						}
					}
					else
					{
					}
				}
				txtBillYearStub1.Text = txtTaxBillYear.Text;
				txtBillYearStub2.Text = txtTaxBillYear.Text;
				txtAccountAddress.Text = strAcct;
				//FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
				//GridStubInfo1.TextMatrix(0, 1, strAcct);
				//GridStubInfo1.TextMatrix(1, 1, strName);
				//GridStubInfo1.TextMatrix(2, 1, strMapLot);
				//GridStubInfo1.TextMatrix(3, 1, strLocation);
				txtStubInfo112.Text = strAcct;
				txtStubInfo122.Text = strName;
				txtStubInfo132.Text = strMapLot;
				txtStubInfo142.Text = strLocation;
				//FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
				//GridStubInfo2.TextMatrix(0, 1, strAcct);
				//GridStubInfo2.TextMatrix(1, 1, strName);
				//GridStubInfo2.TextMatrix(2, 1, strMapLot);
				//GridStubInfo2.TextMatrix(3, 1, strLocation);
				txtStubInfo212.Text = strAcct;
				txtStubInfo222.Text = strAcct;
				txtStubInfo232.Text = strAcct;
				txtStubInfo242.Text = strAcct;
				lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				// GridValuation.TextMatrix(7, 1) = Format(lngTotAssess, "#,###,###,##0")
				txtValData7.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
				// GridValuation.TextMatrix(8, 1) = Format(lngExempt, "#,###,###,##0")
				lngHomestead = 0;
				lngOther = 0;
				if (boolRE)
				{
					lngHomestead = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Double("homesteadexemption"))));
					lngOther = lngExempt - lngHomestead;
					// GridValuation.TextMatrix(ROWHOMESTEAD, 1) = Format(lngHomestead, "#,###,###,##0")
					(Detail.Controls["txtValData" + ROWHOMESTEAD] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(lngHomestead, "#,###,###,##0");
					// GridValuation.TextMatrix(ROWOTHER, 1) = Format(lngOther, "#,###,###,##0")
					(Detail.Controls["txtValData" + ROWOTHER] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(lngOther, "#,###,###,##0");
				}
				else
				{
					// GridValuation.TextMatrix(ROWHOMESTEAD, 1) = Format(lngExempt, "#,###,###,##0")
					(Detail.Controls["txtValData" + ROWHOMESTEAD] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(lngExempt, "#,###,###,##0");
				}
				// GridValuation.TextMatrix(ROWTAXABLE, 1) = Format(lngTotAssess - lngExempt, "#,###,###,##0")
				(Detail.Controls["txtValData" + ROWTAXABLE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(lngTotAssess - lngExempt, "#,###,###,##0");
				// txtAccount.Text = strAcct
				if (boolRE)
				{
					if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
					{
						boolReprintNewOwnerCopy = false;
						// if the copy for a new owner needs to be sent, then check it here
						if (clsTaxBillFormat.PrintCopyForNewOwner)
						{
							// if this is the main bill, only check for the new owner on the mail bill
							if (boolFirstBill)
							{
								if (DateTime.Today.Month < 4)
								{
									// if the month is before april then use last years commitment date
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1));
								}
								else
								{
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));
								}
								// If NewOwner(clsBills.Fields("Account"), dtLastDate) Then     'if there has been a new owner
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1")))
								{
									// if there has been a new owner
									boolReprintNewOwnerCopy = true;
								}
							}
						}
					}
					else if (!boolPrintRecipientCopy && boolFirstBill)
					{
						// turn off the copy so that the record can advance
						boolReprintNewOwnerCopy = false;
					}
					if (!boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							if (boolFirstBill)
							{
								rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
								if (!rsMultiRecipients.EndOfFile())
								{
									boolPrintRecipientCopy = true;
								}
							}
						}
					}
					else if (boolPrintRecipientCopy && boolFirstBill)
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (clsTaxBillFormat.PrintCopyForMortgageHolders && !boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (!clsMortgageAssociations.EndOfFile())
						{
							if (boolFirstBill)
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindFirstRecord("account", clsBills.Get_Fields("account"));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindNextRecord("account", clsBills.Get_Fields("account"));
							}
							// Call clsMortgageAssociations.FindNextRecord("account", clsBills.Fields("ac"))
							if (clsMortgageAssociations.NoMatch)
							{
								boolFirstBill = true;
								if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
									clsBills.MoveNext();
							}
							else
							{
								boolFirstBill = false;
							}
							// End If
						}
						else
						{
							boolFirstBill = true;
							if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
								clsBills.MoveNext();
						}
					}
					else
					{
						if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
							clsBills.MoveNext();
					}
				}
				else
				{
					if (!boolPrintRecipientCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
							if (!rsMultiRecipients.EndOfFile())
							{
								boolPrintRecipientCopy = true;
							}
						}
					}
					else
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (!boolPrintRecipientCopy)
					{
						clsBills.MoveNext();
					}
				}
			}
		}

		private void SetupGrids()
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                string strTemp = "";
                if (boolBlank)
                {
                    txtDistLbl0.Font = new Font(txtDistLbl0.Font, FontStyle.Bold);
                    txtVallbl0.Text = "Current Billing Information";
                    // show boxes too
                    shapeValuation.Visible = true;
                    ValuationLine1.Visible = true;
                    valuationLine2.Visible = true;
                    (Detail.Controls["txtValLbl" + ROWTOTALDUE] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                        .Text = "Total Due";
                    if (!clsTaxBillFormat.UseClearLaserBackground)
                    {
                        txtVallbl0.ForeColor = Color.FromArgb(255, 255, 255);
                        txtVallbl0.BackColor = Color.FromArgb(1, 1, 1);
                        //txtVallbl0.BackStyle = ddBKNormal;
                        txtDistLbl0.ForeColor = txtVallbl0.ForeColor;
                        txtDistLbl0.BackColor = txtVallbl0.BackColor;
                        //txtDistLbl0.BackStyle = ddBKNormal;
                        txtRemitLbl0.ForeColor = txtDistLbl0.ForeColor;
                        txtRemitLbl0.BackColor = txtDistLbl0.BackColor;
                        //txtRemitLbl0.BackStyle = ddBKNormal;
                        txtInfoLbl0.ForeColor = txtVallbl0.ForeColor;
                        txtInfoLbl0.BackColor = txtVallbl0.BackColor;
                        //txtInfoLbl0.BackStyle = ddBKNormal;
                        txtInfoLbl0.Font = new Font(txtInfoLbl0.Font, FontStyle.Bold);
                        (Detail.Controls["txtValLbl" + ROWTOTALDUE] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font(
                            (Detail.Controls["txtValLbl" + ROWTOTALDUE] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Font, FontStyle.Bold);
                        (Detail.Controls["txtValLbl" + ROWTOTALDUE] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).BackColor = Color.FromArgb(1, 1, 1);
                        //Detail.Controls("txtValLbl" + ROWTOTALDUE).BackStyle = ddBKNormal;
                        (Detail.Controls["txtValLbl" + ROWTOTALDUE] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).ForeColor =
                            Color.FromArgb(255, 255, 255);
                    }
                }
                else
                {
                }

                // now fill in categories
                txtValLbl7.Text = "Assessment";
                if (!boolRE)
                {
                    (Detail.Controls["txtValLbl" + ROWHOMESTEAD] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                        .Text = "Exemption";
                    (Detail.Controls["txtValLbl" + ROWOTHER] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                        .Text = "";
                }
                else
                {
                    (Detail.Controls["txtValLbl" + ROWHOMESTEAD] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                        .Text = "Homestead Exempt";
                    (Detail.Controls["txtValLbl" + ROWOTHER] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text
                        = "Other Exemption";
                }

                (Detail.Controls["txtValLbl" + ROWTAXABLE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                    "Taxable";
                (Detail.Controls["txtValLbl" + ROWRATE] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                    "Rate Per $1000";
                (Detail.Controls["txtValLbl" + ROWPAIDTODATE] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                    .Text = "Paid To Date";
                (Detail.Controls["txtValLbl" + ROWORIGBILL] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                    .Text = "Original Bill";
                if (boolRE)
                {
                    txtValLbl1.Text = "Land";
                    txtValLbl2.Text = "Building";
                }
                else
                {
                    txtValLbl1.Text = "";
                    txtValLbl2.Text = "";
                }

                if (boolPP)
                {
                    clsTemp.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
                    strCat1 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    txtValLbl3.Text = strCat1;
                    clsTemp.MoveNext();
                    strCat2 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    txtValLbl4.Text = strCat2;
                    clsTemp.MoveNext();
                    strCat3 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    txtValLbl5.Text = strCat3;
                    txtValLbl6.Text = "Other P/P";
                }
                else
                {
                }

                if (boolBlank)
                {
                    shapeDistribution.Visible = true;
                }

                DistPerc1 = 0;
                DistPerc2 = 0;
                DistPerc3 = 0;
                DistPerc4 = 0;
                DistPerc5 = 0;
                if (clsTaxBillFormat.Breakdown)
                {
                    if (boolBlank)
                    {
                        txtDistLbl0.Text = "Current Billing Distribution";
                    }

                    clsTemp.OpenRecordset("select * from distributiontable order by distributionnumber",
                        "twbl0000.vb1");
                    if (!clsTemp.EndOfFile())
                    {
                        clsTemp.MoveLast();
                        clsTemp.MoveFirst();
                        intDistCats = clsTemp.RecordCount();
                        DistPerc1 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
                        txtDistPerc1.Text = Strings.Format(DistPerc1, "#0.00") + "%";
                        DistPerc1 /= 100;
                        strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
                        txtDistLbl1.Text = strTemp;
                        if (intDistCats > 1)
                        {
                            clsTemp.MoveNext();
                            DistPerc2 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
                            txtDistPer2.Text = Strings.Format(DistPerc2, "#0.00") + "%";
                            DistPerc2 /= 100;
                            strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
                            txtDistLbl2.Text = strTemp;
                            if (intDistCats > 2)
                            {
                                clsTemp.MoveNext();
                                DistPerc3 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
                                txtDistPerc3.Text = Strings.Format(DistPerc3, "#0.00") + "%";
                                DistPerc3 /= 100;
                                strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
                                txtDistLBL3.Text = strTemp;
                                if (intDistCats > 3)
                                {
                                    clsTemp.MoveNext();
                                    DistPerc4 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
                                    txtDistPerc4.Text = Strings.Format(DistPerc4, "#0.00") + "%";
                                    DistPerc4 /= 100;
                                    strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
                                    txtDistLbl4.Text = strTemp;
                                    if (intDistCats > 4)
                                    {
                                        clsTemp.MoveNext();
                                        DistPerc5 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
                                        txtDistPerc5.Text = Strings.Format(DistPerc5, "#0.00") + "%";
                                        DistPerc5 /= 100;
                                        strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
                                        txtDistLbl5.Text = strTemp;
                                        if (intDistCats > 5)
                                        {
                                            clsTemp.MoveNext();
                                            DistPerc6 = Conversion.Val(
                                                clsTemp.Get_Fields_Double("distributionpercent"));
                                            txtDistPerc6.Text = Strings.Format(DistPerc6, "#0.00") + "%";
                                            DistPerc6 /= 100;
                                            strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
                                            txtDistLbl6.Text = strTemp;
                                            if (intDistCats > 6)
                                            {
                                                clsTemp.MoveNext();
                                                DistPerc7 = Conversion.Val(
                                                    clsTemp.Get_Fields_Double("distributionpercent"));
                                                txtDistPerc7.Text = Strings.Format(DistPerc7, "#0.00") + "%";
                                                DistPerc7 /= 100;
                                                strTemp = FCConvert.ToString(
                                                    clsTemp.Get_Fields_String("distributionname"));
                                                txtDistLbl7.Text = strTemp;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        intDistCats = 0;
                    }
                }

                if (boolBlank)
                {
                    //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                    //GridStub1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                    //GridStub1.TextMatrix(0, 0, "Due Date");
                    //GridStub1.TextMatrix(0, 1, "Amount Due");
                    //GridStub1.TextMatrix(0, 2, "Amount Paid");
                    //GridStub1.ColWidth(0, FCConvert.ToInt32(0.33 * GridStub1.Width));
                    //GridStub1.ColWidth(1, FCConvert.ToInt32(0.33 * GridStub1.Width));
                    txtStub11.Text = "Due Date";
                    txtStub12.Text = "Amount Due";
                    txtStub13.Text = "Amount Paid";
                }
                else
                {
                    //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                    //GridStub1.Visible = false;
                    txtStub11.Visible = false;
                    txtStub12.Visible = false;
                    txtStub13.Visible = false;
                }

                if (boolBlank)
                {
                    //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                    //GridStub2.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                    //GridStub2.TextMatrix(0, 0, "Due Date");
                    //GridStub2.TextMatrix(0, 1, "Amount Due");
                    //GridStub2.TextMatrix(0, 2, "Amount Paid");
                    //GridStub2.ColWidth(0, FCConvert.ToInt32(0.33 * GridStub2.Width));
                    //GridStub2.ColWidth(1, FCConvert.ToInt32(0.33 * GridStub2.Width));
                    txtStub21.Text = "Due Date";
                    txtStub22.Text = "Amount Due";
                    txtStub23.Text = "Amount Paid";
                }
                else
                {
                    //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                    //GridStub2.Visible = false;
                    txtStub21.Visible = false;
                    txtStub22.Visible = false;
                    txtStub23.Visible = false;
                }

                //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                //GridStubInfo1.ColWidth(0, FCConvert.ToInt32(0.25 * GridStubInfo1.Width));
                if (boolBlank)
                {
                    //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                    //GridStubInfo1.TextMatrix(0, 0, "Account:");
                    //GridStubInfo1.TextMatrix(1, 0, "Name:");
                    //GridStubInfo1.TextMatrix(2, 0, "Map/Lot:");
                    //GridStubInfo1.TextMatrix(3, 0, "Location:");
                    txtStubInfo111.Text = "Account:";
                    txtStubInfo121.Text = "Name:";
                    txtStubInfo131.Text = "Map/Lot:";
                    txtStubInfo141.Text = "Location:";
                }

                //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                //GridStubInfo2.ColWidth(0, FCConvert.ToInt32(0.25 * GridStubInfo2.Width));
                if (boolBlank)
                {
                    //FC:FINAL:MSH - use textboxes instead of grid to avoid missing data
                    //GridStubInfo2.TextMatrix(0, 0, "Account:");
                    //GridStubInfo2.TextMatrix(1, 0, "Name:");
                    //GridStubInfo2.TextMatrix(2, 0, "Map/Lot:");
                    //GridStubInfo2.TextMatrix(3, 0, "Location:");
                    txtStubInfo211.Text = "Account:";
                    txtStubInfo221.Text = "Name:";
                    txtStubInfo231.Text = "Map/Lot:";
                    txtStubInfo241.Text = "Location:";
                }

                // With GridRemittance
                if (boolBlank)
                {
                    // .TextMatrix(0, 0) = "Remittance Instructions"
                    // .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0) = flexAlignCenterCenter
                    // .Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0) = True
                    txtRemitLbl0.Text = "Remittance Instructions";
                    txtRemitLbl0.Font = new Font(txtRemitLbl0.Font, FontStyle.Bold);
                    shapeRemittance.Visible = true;
                }
                else
                {
                    // .BackColorFixed = RGB(255, 255, 255)
                }

                // End With
                // With GridInformation
                if (boolBlank)
                {
                    txtInfoLbl0.Text = "Information";
                    txtInfoLbl0.Font = new Font(txtInfoLbl0.Font, FontStyle.Bold);
                    // .TextMatrix(0, 0) = "Information"
                    // .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0) = flexAlignCenterCenter
                    // .Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0) = True
                    shapeInformation.Visible = true;
                }
                else
                {
                    // .BackColorFixed = RGB(255, 255, 255)
                }

                // End With
            }
        }

		
	}
}
