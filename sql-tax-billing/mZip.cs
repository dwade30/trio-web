//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Text;
using System.Runtime.InteropServices;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	public class mZip
	{
		//=========================================================
		/// <summary>
		/// ======================================================================================
		/// </summary>
		/// <summary>
		/// Name:     mzip
		/// </summary>
		/// <summary>
		/// Author:   Steve McMahon (steve@vbaccelerator.com)
		/// </summary>
		/// <summary>
		/// Date:     1 January 2000
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Requires: Info-ZIP's Zip32.DLL v2.32, renamed to vbzip10.dll
		/// </summary>
		/// <summary>
		/// cUnzip.cls
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Copyright ? 2000 Steve McMahon for vbAccelerator
		/// </summary>
		/// <summary>
		/// --------------------------------------------------------------------------------------
		/// </summary>
		/// <summary>
		/// Visit vbAccelerator - advanced free source code for VB programmers
		/// </summary>
		/// <summary>
		/// http://vbaccelerator.com
		/// </summary>
		/// <summary>
		/// --------------------------------------------------------------------------------------
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Part of the implementation of cUnzip.cls, a class which gives a
		/// </summary>
		/// <summary>
		/// simple interface to Info-ZIP's excellent, free zipping library
		/// </summary>
		/// <summary>
		/// (Zip32.DLL).
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// This sample uses decompression code by the Info-ZIP group.  The
		/// </summary>
		/// <summary>
		/// original Info-Zip sources are freely available from their website
		/// </summary>
		/// <summary>
		/// at
		/// </summary>
		/// <summary>
		/// http://www.cdrcom.com/pubs/infozip/
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Please ensure you visit the site and read their free source licensing
		/// </summary>
		/// <summary>
		/// information and requirements before using their code in your own
		/// </summary>
		/// <summary>
		/// application.
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// ======================================================================================
		/// </summary>
		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(ref byte lpvDest, ref byte lpvSource, int cbCopy);

		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(ref byte lpvDest, ref Any lpvSource, int cbCopy);

		private static void CopyMemoryWrp(ref byte lpvDest, ref Any lpvSource, int cbCopy)
		{
			CopyMemory(ref lpvDest, ref lpvSource, cbCopy);
		}

		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(ref byte lpvDest, ref Any lpvSource, int cbCopy);

		private static void CopyMemoryWrp(ref byte lpvDest, ref Any lpvSource, int cbCopy)
		{
			CopyMemory(ref lpvDest, ref lpvSource, cbCopy);
		}
		/// <summary>
		/// argv
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		private struct ZIPnames
		{
			public string[] s/*?  = new string[1023 + 1] */;
		};
		/// <summary>
		/// Callback large "string" (sic)
		/// </summary>
		private struct CBChar
		{
			// vbPorter upgrade warning: ch As byte	OnWrite(string)
			public byte[] ch;
		};
		/// <summary>
		/// Callback small "string" (sic)
		/// </summary>
		private struct CBCh
		{
			public byte[] ch;
		};
		/// <summary>
		/// Store the callback functions
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		private struct ZIPUSERFUNCTIONS
		{
			public int lPtrPrint;
			// Pointer to application's print routine
			public int lptrPassword;
			// Pointer to application's password routine.
			public int lptrComment;
			public int lptrService;
			// callback function designed to be used for allowing the
			// app to process Windows messages, or cancelling the operation
			// as well as giving option of progress.  If this function returns
			// non-zero, it will terminate what it is doing.  It provides the app
			// with the name of the archive member it has just processed, as well
			// as the original size.
		};

		public struct ZPOPT
		{
			public string Date;
			// US Date (8 Bytes Long) "12/31/98"?
			public string szRootDir;
			// Root Directory Pathname (Up To 256 Bytes Long)
			public string szTempDir;
			// Temp Directory Pathname (Up To 256 Bytes Long)
			public int fTemp;
			// 1 If Temp dir Wanted, Else 0
			public int fSuffix;
			// Include Suffixes (Not Yet Implemented!)
			public int fEncrypt;
			// 1 If Encryption Wanted, Else 0
			public int fSystem;
			// 1 To Include System/Hidden Files, Else 0
			public int fVolume;
			// 1 If Storing Volume Label, Else 0
			public int fExtra;
			// 1 If Excluding Extra Attributes, Else 0
			public int fNoDirEntries;
			// 1 If Ignoring Directory Entries, Else 0
			public int fExcludeDate;
			// 1 If Excluding Files Earlier Than Specified Date, Else 0
			public int fIncludeDate;
			// 1 If Including Files Earlier Than Specified Date, Else 0
			public int fVerbose;
			// 1 If Full Messages Wanted, Else 0
			public int fQuiet;
			// 1 If Minimum Messages Wanted, Else 0
			public int fCRLF_LF;
			// 1 If Translate CR/LF To LF, Else 0
			public int fLF_CRLF;
			// 1 If Translate LF To CR/LF, Else 0
			public int fJunkDir;
			// 1 If Junking Directory Names, Else 0
			public int fGrow;
			// 1 If Allow Appending To Zip File, Else 0
			public int fForce;
			// 1 If Making Entries Using DOS File Names, Else 0
			public int fMove;
			// 1 If Deleting Files Added Or Updated, Else 0
			public int fDeleteEntries;
			// 1 If Files Passed Have To Be Deleted, Else 0
			public int fUpdate;
			// 1 If Updating Zip File-Overwrite Only If Newer, Else 0
			public int fFreshen;
			// 1 If Freshing Zip File-Overwrite Only, Else 0
			public int fJunkSFX;
			// 1 If Junking SFX Prefix, Else 0
			public int fLatestTime;
			// 1 If Setting Zip File Time To Time Of Latest File In Archive, Else 0
			public int fComment;
			// 1 If Putting Comment In Zip File, Else 0
			public int fOffsets;
			// 1 If Updating Archive Offsets For SFX Files, Else 0
			public int fPrivilege;
			// 1 If Not Saving Privileges, Else 0
			public int fEncryption;
			// Read Only Property!!!
			public int fRecurse;
			// 1 (-r), 2 (-R) If Recursing Into Sub-Directories, Else 0
			public int fRepair;
			// 1 = Fix Archive, 2 = Try Harder To Fix, Else 0
			public byte flevel;
			// Compression Level - 0 = Stored 6 = Default 9 = Max
		};
		/// <summary>
		/// This assumes zip32.dll is in your \windows\system directory!
		/// </summary>
		[DllImport("vbzip10.dll")]
		private static extern int ZpInit(ref ZIPUSERFUNCTIONS tUserFn);
		/// <summary>
		/// Set Zip Callbacks
		/// </summary>
		[DllImport("vbzip10.dll")]
		private static extern int ZpSetOptions(ref ZPOPT tOpts);
		/// <summary>
		/// Set Zip options
		/// </summary>
		[DllImport("vbzip10.dll")]
		private static extern ZPOPT ZpGetOptions();
		/// <summary>
		/// used to check encryption flag only
		/// </summary>
		[DllImport("vbzip10.dll")]
		private static extern int ZpArchive(int argc, IntPtr funame, ref ZIPnames argv);

		private static int ZpArchiveWrp(int argc, ref string funame, ref ZIPnames argv)
		{
			int ret;
			IntPtr pfuname = FCUtils.GetByteFromString(funame);
			ret = ZpArchive(argc, pfuname, ref argv);
			FCUtils.GetStringFromByte(ref funame, pfuname);
			return ret;
		}
		// vbPorter upgrade warning: lPtr As int	OnWrite(void*)
		private static int plAddressOf(int lPtr)
		{
			int plAddressOf = 0;
			// VB Bug workaround fn
			plAddressOf = lPtr;
			return plAddressOf;
		}

		public static int VBZip(ref cZip cZipObject, ref ZPOPT tZPOPT, ref string[] sFileSpecs, ref int iFileCount)
		{
			int VBZip = 0;
			ZIPUSERFUNCTIONS tUser = new ZIPUSERFUNCTIONS();
			int lR;
			int i;
			string sZipFile;
			ZIPnames tZipName = new ZIPnames();
			Statics.m_bCancel = false;
			Statics.m_cZip = cZipObject;
			if (!(Strings.Trim(Statics.m_cZip.BasePath).Length == 0))
			{
				Environment.CurrentDirectory = Statics.m_cZip.BasePath;
			}
			// Set address of callback functions
			tUser.lPtrPrint = plAddressOf(ZipPrintCallback);
			tUser.lptrPassword = plAddressOf(ZipPasswordCallback);
			tUser.lptrComment = plAddressOf(ZipCommentCallback);
			tUser.lptrService = plAddressOf(ZipServiceCallback);
			// not coded yet :-)
			lR = ZpInit(ref tUser);
			// Set options
			lR = ZpSetOptions(ref tZPOPT);
			// Go for it!
			for (i = 1; i <= iFileCount; i++)
			{
				tZipName.s[i - 1] = sFileSpecs[i];
			}
			// i
			tZipName.s[i] = "\0";
			sZipFile = cZipObject.ZipFile;
			lR = ZpArchiveWrp(iFileCount, ref sZipFile, ref tZipName);
			VBZip = lR;
			return VBZip;
		}

		private delegate int Address_ZipServiceCallback(ref CBChar mname, int X);

		private static int ZipServiceCallback(ref CBChar mname, int X)
		{
			int ZipServiceCallback = 0;
			byte[] b = null;
			// - "AutoDim"
			int iPos = 0;
			string sInfo = "";
			bool bCancel = false;
			// -- Always Put This In Callback Routines!
			/*? On Error Resume Next  */
			try
			{
				// Check we've got a message:
				if (X > 1 && X < 32000)
				{
					// If so, then get the readable portion of it:
					b = new byte[X + 1];
					CopyMemoryWrp(ref b[0], ref mname, X);
					// Convert to VB string:
					sInfo = Encoding.Default.GetString(b);
					iPos = Strings.InStr(sInfo, "\0", CompareConstants.vbBinaryCompare);
					if (iPos > 0)
					{
						sInfo = Strings.Left(sInfo, iPos - 1);
					}
					Statics.m_cZip.Service(sInfo, ref bCancel);
					if (bCancel)
					{
						ZipServiceCallback = 1;
					}
					else
					{
						ZipServiceCallback = 0;
					}
				}
			}
			catch (Exception ex)
			{
			}
			return ZipServiceCallback;
		}

		private delegate int Address_ZipPrintCallback(ref CBChar fname, int X);

		private static int ZipPrintCallback(ref CBChar fname, int X)
		{
			int ZipPrintCallback = 0;
			byte[] b = null;
			// - "AutoDim"
			int iPos = 0;
			string sFIle = "";
			/*? On Error Resume Next  */
			try
			{
				// Check we've got a message:
				if (X > 1 && X < 32000)
				{
					// If so, then get the readable portion of it:
					b = new byte[X + 1];
					CopyMemoryWrp(ref b[0], ref fname, X);
					// Convert to VB string:
					sFIle = Encoding.Default.GetString(b);
					if (iPos > 0)
					{
						sFIle = Strings.Left(sFIle, iPos - 1);
					}
					// Fix up backslashes:
					ReplaceSection(ref sFIle, "/", "\\");
					// Tell the caller about it
					Statics.m_cZip.ProgressReport(sFIle);
				}
				ZipPrintCallback = 0;
			}
			catch (Exception ex)
			{
			}
			return ZipPrintCallback;
		}

		private delegate CBChar Address_ZipCommentCallback(ref CBChar s1);

		private static CBChar ZipCommentCallback(ref CBChar s1)
		{
			CBChar ZipCommentCallback = new CBChar();
			// always put this in callback routines!
			/*? On Error Resume Next  */
			try
			{
				// not supported always return \0
				s1.ch[0] = Convert.ToByte(string.Empty);
				ZipCommentCallback = s1;
			}
			catch (Exception ex)
			{
			}
			return ZipCommentCallback;
		}

		private delegate int Address_ZipPasswordCallback(ref CBCh pwd, int X, ref CBCh s2, ref CBCh Name);

		private static int ZipPasswordCallback(ref CBCh pwd, int X, ref CBCh s2, ref CBCh Name)
		{
			int ZipPasswordCallback = 0;
			bool bCancel = false;
			string sPassword = "";
			byte[] b = null;
			int lSize;
			/*? On Error Resume Next  */
			try
			{
				// The default:
				ZipPasswordCallback = 1;
				if (Statics.m_bCancel)
				{
					return ZipPasswordCallback;
				}
				// Ask for password:
				Statics.m_cZip.PasswordRequest(ref sPassword, ref bCancel);
				sPassword = Strings.Trim(sPassword);
				// Cancel out if no useful password:
				if (bCancel || sPassword.Length == 0)
				{
					Statics.m_bCancel = true;
					return ZipPasswordCallback;
				}
				// Put password into return parameter:
				lSize = sPassword.Length;
				if (lSize > 254)
				{
					lSize = 254;
				}
				b = Encoding.Default.GetBytes(sPassword);
				CopyMemory(ref pwd.ch[0], ref b[0], lSize);
				// Ask UnZip to process it:
				ZipPasswordCallback = 0;
			}
			catch (Exception ex)
			{
			}
			return ZipPasswordCallback;
		}

		private static int ReplaceSection(ref string sString, string sToReplace, string sReplaceWith)
		{
			int ReplaceSection = 0;
			int iPos = 0;
			int iLastPos;
			iLastPos = 1;
			do
			{
				iPos = Strings.InStr(iLastPos, sString, "/", CompareConstants.vbBinaryCompare);
				if (iPos > 1)
				{
					fecherFoundation.Strings.MidSet(ref sString, iPos, "\\", 1);
					iLastPos = iPos + 1;
				}
			}
			while (!(iPos == 0));
			ReplaceSection = iLastPos;
			return ReplaceSection;
		}

		public class StaticVariables
		{
			/// </summary>
			/// Object for callbacks:
			/// <summary>
			/// </summary>
			/// Real zipping action
			/// <summary>
			public cZip m_cZip;
			public bool m_bCancel;
			public Address_ZipServiceCallback Ptr_ZipServiceCallback = new Address_ZipServiceCallback(ZipServiceCallback);
			public Address_ZipPrintCallback Ptr_ZipPrintCallback = new Address_ZipPrintCallback(ZipPrintCallback);
			public Address_ZipCommentCallback Ptr_ZipCommentCallback = new Address_ZipCommentCallback(ZipCommentCallback);
			public Address_ZipPasswordCallback Ptr_ZipPasswordCallback = new Address_ZipPasswordCallback(ZipPasswordCallback);
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
