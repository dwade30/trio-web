﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	public partial class frmCustomize : BaseForm
	{
		public frmCustomize()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomize InstancePtr
		{
			get
			{
				return (frmCustomize)Sys.GetInstance(typeof(frmCustomize));
			}
		}

		protected frmCustomize _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsGridAccount clsAltAcct = new clsGridAccount();

		private void chkPayInterestOnPrepayments_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPayInterestOnPrepayments.CheckState == Wisej.Web.CheckState.Checked)
			{
				clsAltAcct.GRID7Light = GridAlternate;
			}
			else
			{
				clsAltAcct.GRID7Light = null;
			}
		}

		private void frmCustomize_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						return;
					}
			}
			//end switch
			if (Strings.UCase(this.ActiveControl.GetName()) == "GRIDALTERNATE")
			{
				if (GridAlternate.Row > 0)
				{
					modNewAccountBox.CheckFormKeyDown(GridAlternate, GridAlternate.Row, GridAlternate.Col, KeyCode, Shift, GridAlternate.EditSelStart, GridAlternate.EditText, GridAlternate.EditSelLength);
				}
			}
		}

		private void frmCustomize_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomize properties;
			//frmCustomize.ScaleWidth	= 9225;
			//frmCustomize.ScaleHeight	= 7665;
			//frmCustomize.LinkTopic	= "Form1";
			//frmCustomize.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			clsAltAcct.GRID7Light = GridAlternate;
			clsAltAcct.AccountCol = 0;
			clsAltAcct.DefaultAccountType = "E";
			clsAltAcct.AllowSplits = false;
			clsAltAcct.Validation = true;
			GridAlternate.TextMatrix(0, 0, "Pre-Payment Interest Account");
			Loadinfo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void Loadinfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("Select * from defaultstable", "twbl0000.vb1");
			GridAlternate.TextMatrix(1, 0, FCConvert.ToString(clsLoad.Get_Fields_String("alternateAccount")));
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ignoreprinterfonts")))
			{
				chkPrinterFonts.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkPrinterFonts.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ApplyDiscounts")))
			{
				chkApplyDiscounts.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkApplyDiscounts.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("DuplexCommitment")))
			{
				chkDuplexCommitment.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkDuplexCommitment.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("Showref1")))
			{
				chkShowRef1.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkShowRef1.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ShowRef2")))
			{
				chkShowRef2.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkShowRef2.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ShowTreeGrowth")))
			{
				chkShowTreeGrowth.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkShowTreeGrowth.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ShowDollarSigns")))
			{
				chkShowDollarSigns.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkShowDollarSigns.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ZeroLiabilityOption")))
			{
				chkCreateZeroBills.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkCreateZeroBills.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("SplitSmallAmounts")))
			{
				cmbBillSplit.SelectedIndex = 1;
			}
			else
			{
				cmbBillSplit.SelectedIndex = 0;
			}
			clsLoad.OpenRecordset("select * from collections", modGlobalVariables.strCLDatabase);
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("payintonprepayment")))
				{
					chkPayInterestOnPrepayments.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkPayInterestOnPrepayments.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkPayInterestOnPrepayments.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (chkPayInterestOnPrepayments.CheckState == Wisej.Web.CheckState.Checked)
			{
				clsAltAcct.GRID7Light = GridAlternate;
			}
			else
			{
				clsAltAcct.GRID7Light = null;
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				// GridAlternate.Row = 0
				// DoEvents
				clsSave.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
				clsSave.Edit();
				if (chkPrinterFonts.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("IgnorePrinterFonts", true);
				}
				else
				{
					clsSave.Set_Fields("IgnorePrinterFonts", false);
				}
				if (chkApplyDiscounts.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("ApplyDiscounts", true);
				}
				else
				{
					clsSave.Set_Fields("ApplyDiscounts", false);
				}
				if (chkDuplexCommitment.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("DuplexCommitment", true);
				}
				else
				{
					clsSave.Set_Fields("DuplexCommitment", false);
				}
				if (chkShowRef1.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("ShowRef1", true);
				}
				else
				{
					clsSave.Set_Fields("ShowRef1", false);
				}
				if (chkShowRef2.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("ShowRef2", true);
				}
				else
				{
					clsSave.Set_Fields("ShowRef2", false);
				}
				if (chkShowTreeGrowth.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("ShowTreeGrowth", true);
				}
				else
				{
					clsSave.Set_Fields("ShowTreeGrowth", false);
				}
				if (chkShowDollarSigns.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("ShowDollarSigns", true);
				}
				else
				{
					clsSave.Set_Fields("ShowDollarSigns", false);
				}
				if (chkCreateZeroBills.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("zeroliabilityoption", true);
				}
				else
				{
					clsSave.Set_Fields("zeroliabilityoption", false);
				}
				if (chkPayInterestOnPrepayments.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (!modValidateAccount.AccountValidate(GridAlternate.TextMatrix(1, 0)))
					{
						MessageBox.Show("Invalid Pre-Payment Interest Account", "Bad Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
				}
				if (cmbBillSplit.SelectedIndex == 1)
				{
					clsSave.Set_Fields("splitsmallamounts", true);
				}
				else
				{
					clsSave.Set_Fields("SplitSmallAmounts", false);
				}
				clsSave.Set_Fields("AlternateAccount", GridAlternate.TextMatrix(1, 0));
				clsSave.Update();
				clsSave.OpenRecordset("select * from collections", modGlobalVariables.strCLDatabase);
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				if (chkPayInterestOnPrepayments.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("payintonprepayment", true);
				}
				else
				{
					clsSave.Set_Fields("payintonprepayment", false);
				}
				clsSave.Update();
				modMain.LoadCustomizedInfo();
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveInfo = true;
                //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                App.MainForm.ReloadNavigationMenu();
                return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			SaveInfo();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}
	}
}
