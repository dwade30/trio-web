﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	public class clsBillFormat
	{
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// WRITTEN BY     :               Corey Gray              *
		/// </summary>
		/// <summary>
		/// DATE           :                                       *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// MODIFIED BY    :               Jim Bertolino           *
		/// </summary>
		/// <summary>
		/// LAST UPDATED   :               07/29/2004              *
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		int BillType;
		int intBillAccountsFrom;
		bool boolBreakdown;
		bool boolUseLaserAdjustment;
		double dblLaserAdjustment;
		bool boolCombined;
		bool boolDiscount;
		bool boolBreakdownDollars;
		int intMessageLines;
		bool boolReturnAddress;
		bool boolCopyForMortgageHolders;
		bool boolCopyForMultiOwners;
		int lngMessageLeft;
		int intMessageChars;
		int lngMessageTop;
		int lngDistributionLeft;
		int lngDistributionTop;
		bool boolUsePreviousOwner;
		DateTime dtPreviousOwnerDate;
		string strSQL;
		bool boolClearLaserBoxes;
		bool boolPrintCopyForNewOwner;
		DateTime dtBillAsOf;
		int lngCustomBillFormat;
		bool boolDefaultMargin;

		public bool HasDefaultMargin
		{
			set
			{
				boolDefaultMargin = value;
			}
			get
			{
				bool HasDefaultMargin = false;
				HasDefaultMargin = boolDefaultMargin;
				return HasDefaultMargin;
			}
		}

		public int CustomBillID
		{
			set
			{
				lngCustomBillFormat = value;
			}
			get
			{
				int CustomBillID = 0;
				CustomBillID = lngCustomBillFormat;
				return CustomBillID;
			}
		}

		public bool UseClearLaserBackground
		{
			set
			{
				boolClearLaserBoxes = value;
			}
			get
			{
				bool UseClearLaserBackground = false;
				UseClearLaserBackground = boolClearLaserBoxes;
				return UseClearLaserBackground;
			}
		}

		public bool PrintCopyForMortgageHolders
		{
			set
			{
				boolCopyForMortgageHolders = value;
			}
			get
			{
				bool PrintCopyForMortgageHolders = false;
				PrintCopyForMortgageHolders = boolCopyForMortgageHolders;
				return PrintCopyForMortgageHolders;
			}
		}

		public string SQLStatement
		{
			set
			{
				strSQL = value;
			}
			get
			{
				string SQLStatement = "";
				SQLStatement = strSQL;
				return SQLStatement;
			}
		}

		public short BillsFrom
		{
			set
			{
				// 0 for real estate
				// 1 for personal property
				intBillAccountsFrom = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short BillsFrom = 0;
				BillsFrom = FCConvert.ToInt16(intBillAccountsFrom);
				return BillsFrom;
			}
		}

		public short MessageCharacters
		{
			set
			{
				intMessageChars = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short MessageCharacters = 0;
				MessageCharacters = FCConvert.ToInt16(intMessageChars);
				return MessageCharacters;
			}
		}

		public short Billformat
		{
			set
			{
				BillType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Billformat = 0;
				Billformat = FCConvert.ToInt16(BillType);
				return Billformat;
			}
		}

		public bool Breakdown
		{
			set
			{
				boolBreakdown = value;
				if (!boolBreakdown)
					boolBreakdownDollars = false;
			}
			get
			{
				bool Breakdown = false;
				Breakdown = boolBreakdown;
				return Breakdown;
			}
		}

		public bool Combined
		{
			set
			{
				boolCombined = value;
			}
			get
			{
				bool Combined = false;
				Combined = boolCombined;
				return Combined;
			}
		}

		public bool Discount
		{
			set
			{
				boolDiscount = value;
			}
			get
			{
				bool Discount = false;
				Discount = boolDiscount;
				return Discount;
			}
		}

		public bool BreakdownDollars
		{
			set
			{
				boolBreakdownDollars = value;
				if (boolBreakdownDollars)
					boolBreakdown = true;
			}
			get
			{
				bool BreakdownDollars = false;
				BreakdownDollars = boolBreakdownDollars;
				return BreakdownDollars;
			}
		}

		public short NumMessageLines
		{
			set
			{
				intMessageLines = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short NumMessageLines = 0;
				NumMessageLines = FCConvert.ToInt16(intMessageLines);
				return NumMessageLines;
			}
		}

		public bool ReturnAddress
		{
			set
			{
				boolReturnAddress = value;
			}
			get
			{
				bool ReturnAddress = false;
				ReturnAddress = boolReturnAddress;
				return ReturnAddress;
			}
		}

		public int MessageLeft
		{
			set
			{
				lngMessageLeft = value;
			}
			get
			{
				int MessageLeft = 0;
				MessageLeft = lngMessageLeft;
				return MessageLeft;
			}
		}

		public int MessageTop
		{
			set
			{
				lngMessageTop = value;
			}
			get
			{
				int MessageTop = 0;
				MessageTop = lngMessageTop;
				return MessageTop;
			}
		}

		public int DistributionLeft
		{
			set
			{
				lngDistributionLeft = value;
			}
			get
			{
				int DistributionLeft = 0;
				DistributionLeft = lngDistributionLeft;
				return DistributionLeft;
			}
		}

		public int DistributionTop
		{
			set
			{
				lngDistributionTop = value;
			}
			get
			{
				int DistributionTop = 0;
				DistributionTop = lngDistributionTop;
				return DistributionTop;
			}
		}

		public bool UsePreviousOwnerInfo
		{
			set
			{
				boolUsePreviousOwner = value;
			}
			get
			{
				bool UsePreviousOwnerInfo = false;
				UsePreviousOwnerInfo = boolUsePreviousOwner;
				return UsePreviousOwnerInfo;
			}
		}

		public bool PrintCopyForNewOwner
		{
			set
			{
				boolPrintCopyForNewOwner = value;
			}
			get
			{
				bool PrintCopyForNewOwner = false;
				PrintCopyForNewOwner = boolPrintCopyForNewOwner;
				return PrintCopyForNewOwner;
			}
		}

		public bool PrintCopyForMultiOwners
		{
			set
			{
				boolCopyForMultiOwners = value;
			}
			get
			{
				bool PrintCopyForMultiOwners = false;
				PrintCopyForMultiOwners = boolCopyForMultiOwners;
				return PrintCopyForMultiOwners;
			}
		}

		public DateTime PreviousOwnerDate
		{
			set
			{
				dtPreviousOwnerDate = value;
			}
			get
			{
				DateTime PreviousOwnerDate = System.DateTime.Now;
				PreviousOwnerDate = dtPreviousOwnerDate;
				return PreviousOwnerDate;
			}
		}

		public DateTime BillAsOfDate
		{
			set
			{
				dtBillAsOf = value;
			}
			get
			{
				DateTime BillAsOfDate = System.DateTime.Now;
				BillAsOfDate = dtBillAsOf;
				return BillAsOfDate;
			}
		}

		public bool UseLaserAlignment
		{
			set
			{
				boolUseLaserAdjustment = value;
			}
			get
			{
				bool UseLaserAlignment = false;
				UseLaserAlignment = boolUseLaserAdjustment;
				return UseLaserAlignment;
			}
		}

		public double LaserAlignment
		{
			set
			{
				dblLaserAdjustment = value;
			}
			get
			{
				double LaserAlignment = 0;
				LaserAlignment = dblLaserAdjustment;
				return LaserAlignment;
			}
		}

		public void CopyFormat(ref clsBillFormat clsFrmt)
		{
			this.Billformat = clsFrmt.Billformat;
			this.BillsFrom = clsFrmt.BillsFrom;
			this.Breakdown = clsFrmt.Breakdown;
			this.BreakdownDollars = clsFrmt.BreakdownDollars;
			this.Combined = clsFrmt.Combined;
			this.CustomBillID = clsFrmt.CustomBillID;
			this.Discount = clsFrmt.Discount;
			this.DistributionLeft = clsFrmt.DistributionLeft;
			this.DistributionTop = clsFrmt.DistributionTop;
			this.MessageCharacters = clsFrmt.MessageCharacters;
			this.MessageLeft = clsFrmt.MessageLeft;
			this.MessageTop = clsFrmt.MessageTop;
			this.NumMessageLines = clsFrmt.NumMessageLines;
			this.ReturnAddress = clsFrmt.ReturnAddress;
			this.SQLStatement = clsFrmt.SQLStatement;
			this.UsePreviousOwnerInfo = clsFrmt.UsePreviousOwnerInfo;
			this.PreviousOwnerDate = clsFrmt.PreviousOwnerDate;
			this.PrintCopyForMortgageHolders = clsFrmt.PrintCopyForMortgageHolders;
			this.UseLaserAlignment = clsFrmt.UseLaserAlignment;
			this.LaserAlignment = clsFrmt.LaserAlignment;
			this.UseClearLaserBackground = clsFrmt.UseClearLaserBackground;
			this.PrintCopyForNewOwner = clsFrmt.PrintCopyForNewOwner;
			this.BillAsOfDate = clsFrmt.BillAsOfDate;
			this.HasDefaultMargin = clsFrmt.HasDefaultMargin;
			this.PrintCopyForMultiOwners = clsFrmt.PrintCopyForMultiOwners;
		}
	}
}
