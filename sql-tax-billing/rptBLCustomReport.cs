﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBLCustomReport.
	/// </summary>
	public partial class rptBLCustomReport : BaseSectionReport
	{
		public rptBLCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Report";
		}

		public static rptBLCustomReport InstancePtr
		{
			get
			{
				return (rptBLCustomReport)Sys.GetInstance(typeof(rptBLCustomReport));
			}
		}

		protected rptBLCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				clsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBLCustomReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		modCustomReport.CustomReportData[] udtAryReportFields = null;
		string[] aryTots = null;
		clsDRWrapper clsReport = new clsDRWrapper();
		private int lngStartYear;
		private int lngEndYear;
		const int CNSTROWWHEREACCOUNT = 0;
		const int CNSTROWWHERENAME = 1;
		const int CNSTROWWHERENAME2 = 2;
		const int CNSTROWWHEREADDRESS1 = 3;
		const int CNSTROWWHEREADDRESS2 = 4;
		const int CNSTROWWHEREADDRESS3 = 5;
		const int CNSTROWWHERELOCATION = 6;
		const int CNSTROWWHEREMAPLOT = 7;
		const int CNSTROWWHEREBOOKPAGE = 8;
		const int CNSTROWWHEREBILLTYPE = 9;
		const int CNSTROWWHEREBILLYEAR = 10;
		const int CNSTROWWHEREORIGINALTAX = 11;
		const int CNSTROWWHEREPAIDTODATE = 12;
		const int CNSTROWWHERECURRENTDUE = 13;
		const int CNSTROWWHEREOUTSTANDING = 32;
		const int CNSTROWWHEREOUTSTANDINGPRIN = 33;
		const int CNSTROWWHERELANDVALUE = 14;
		const int CNSTROWWHEREBLDGVALUE = 15;
		const int CNSTROWWHEREEXEMPTVALUE = 16;
		const int CNSTROWWHEREBILLABLE = 17;
		const int CNSTROWWHERETRANCODE = 18;
		const int CNSTROWWHERELANDCODE = 19;
		const int CNSTROWWHEREBLDGCODE = 20;
		const int CNSTROWWHEREACRES = 21;
		const int CNSTROWWHEREPPBILLABLE = 22;
		const int CNSTROWWHERECAT1 = 23;
		const int CNSTROWWHERECAT2 = 24;
		const int CNSTROWWHERECAT3 = 25;
		const int CNSTROWWHERECAT4 = 26;
		const int CNSTROWWHERECAT5 = 27;
		const int CNSTROWWHERECAT6 = 28;
		const int CNSTROWWHERECAT7 = 29;
		const int CNSTROWWHERECAT8 = 30;
		const int CNSTROWWHERECAT9 = 31;
		// vbPorter upgrade warning: lngAutoID As int	OnWrite(int, string)
		public void Init(int lngAutoID, bool boolPreview = true)
		{
			string strSQL;
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                bool boolDup = false;
                lngStartYear = 0;
                lngEndYear = 0;
                clsLoad.OpenRecordset("select * from customreports where ID = " + FCConvert.ToString(lngAutoID),
                    "twbl0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    Label9.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Title")));
                }

                // have to build the sql statement
                strSQL = MakeSQLStatement(ref lngAutoID);
                CreateLabelsAndTextboxes(lngAutoID);
                clsReport.OpenRecordset(strSQL, "twcl0000.vb1");
                if (clsReport.EndOfFile())
                {
                    MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (boolPreview)
                {
                    boolDup = false;
                    // have to force if the printer doesn't support duplex

                    if (this.PageSettings.Orientation ==
                        GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape)
                    {
                        frmReportViewer.InstancePtr.Init(this, "", 0, boolDup, false, "Pages", true, "",
                            "TRIO Software", false, true, "Custom");
                    }
                    else
                    {
                        frmReportViewer.InstancePtr.Init(this, "", 0, boolDup, false, "Pages", false, "",
                            "TRIO Software", false, true, "Custom");
                    }
                }
                else
                {
                    this.PrintReport(true);
                }
            }
        }

		private string MakeSQLStatement(ref int lngAutoID)
		{
			string MakeSQLStatement = "";
			string strSQL;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strWhere = "";
			string strOrderBy;
			string strMin = "";
			string strMax = "";
            try
            {
                // On Error GoTo ErrorHandler
                strSQL = "select * from billingmaster ";
                MakeSQLStatement = "Select * from billingmaster where billingyear = -2";
                strOrderBy = "";
                clsLoad.OpenRecordset(
                    "select * from reportparameters where sortby = 1 and reportnumber = " +
                    FCConvert.ToString(lngAutoID) + " order by line", "twbl0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    while (!clsLoad.EndOfFile())
                    {
                        if (!FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("specialcase")))
                        {
                            strOrderBy += clsLoad.Get_Fields_String("fieldname") + ",";
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                            switch (FCConvert.ToInt32(clsLoad.Get_Fields("code")))
                            {
                                case CNSTROWWHEREBILLYEAR:
                                {
                                    strOrderBy += "substring(STR(billingyear,5) + '    ',1,4),";
                                    break;
                                }
                                case CNSTROWWHERELOCATION:
                                {
                                    strOrderBy += "STREETname,streetnumber,";
                                    break;
                                }
                                case CNSTROWWHEREBILLABLE:
                                {
                                    strOrderBy +=
                                        "convert(int, isnull(landvalue, 0)) + convert(int, isnull(buildingvalue, 0)) - (convert(int, isnull(otherexempt1, 0)) + convert(int, isnull(homesteadexemption, 0))),";
                                    break;
                                }
                                case CNSTROWWHERECURRENTDUE:
                                {
                                    // shouldn't order by this
                                    break;
                                }
                                case CNSTROWWHEREEXEMPTVALUE:
                                {
                                    strOrderBy +=
                                        "convert(int, isnull(otherexempt1, 0)) + convert(int, isnull(homesteadexemption, 0)),";
                                    break;
                                }
                                case CNSTROWWHEREORIGINALTAX:
                                {
                                    strOrderBy += "taxdue1 + taxdue2 + taxdue3 + taxdue4,";
                                    break;
                                }
                                case CNSTROWWHEREPAIDTODATE:
                                {
                                    strOrderBy += "principalpaid,";
                                    break;
                                }
                                case CNSTROWWHEREPPBILLABLE:
                                {
                                    strOrderBy +=
                                        "convert(int, isnull(ppassessment, 0)) - convert(int, isnull(exemptvalue, 0)),";
                                    break;
                                }
                            }

                            //end switch
                        }

                        clsLoad.MoveNext();
                    }

                    if (strOrderBy != string.Empty)
                    {
                        strOrderBy = Strings.Mid(strOrderBy, 1, strOrderBy.Length - 1);
                        strOrderBy = " order by " + strOrderBy;
                    }
                }
                else
                {
                    strOrderBy = "";
                }

                // strWhere = " (convert(int, isnull(landvalue, 0)) > 0 or convert(int, isnull(buildingvalue, 0)) > 0) and "
                clsLoad.OpenRecordset(
                    "select * from reportparameters where (rtrim(isnull(min, '')) <> '' or rtrim(isnull(max, '')) <> '') and reportnumber = " +
                    FCConvert.ToString(lngAutoID), "twbl0000.vb1");
                while (!clsLoad.EndOfFile())
                {
                    strMin = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("min")));
                    strMax = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("max")));
                    if (!FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("specialcase")))
                    {
                        // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                        switch (FCConvert.ToInt32(clsLoad.Get_Fields("type")))
                        {
                            case modCustomReport.GRIDBOOLEAN:
                            {
                                strWhere += clsLoad.Get_Fields_String("Fieldname") + " = " + strMin;
                                break;
                            }
                            case modCustomReport.GRIDCOMBOIDNUM:
                            {
                                strWhere += clsLoad.Get_Fields_String("fieldname") + " = " +
                                            FCConvert.ToString(Conversion.Val(strMin));
                                break;
                            }
                            case modCustomReport.GRIDCOMBOIDTEXT:
                            case modCustomReport.GRIDCOMBOTEXT:
                            {
                                strWhere += clsLoad.Get_Fields_String("fieldname") + " = '" + strMin + "'";
                                break;
                            }
                            case modCustomReport.GRIDDATE:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere += clsLoad.Get_Fields_String("fieldname") + " between '" + strMin +
                                                    "' and '" + strMax + "'";
                                    }
                                    else
                                    {
                                        strWhere += clsLoad.Get_Fields_String("fieldname") + " >= '" + strMin + "'";
                                    }
                                }
                                else
                                {
                                    strWhere += clsLoad.Get_Fields_String("fieldname") + " >= '" + strMax + "'";
                                }

                                break;
                            }
                            case modCustomReport.GRIDNUMRANGE:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere += clsLoad.Get_Fields_String("fieldname") + " between " +
                                                    FCConvert.ToString(Conversion.Val(strMin)) + " and " +
                                                    FCConvert.ToString(Conversion.Val(strMax));
                                    }
                                    else
                                    {
                                        strWhere += clsLoad.Get_Fields_String("fieldname") + " >= " +
                                                    FCConvert.ToString(Conversion.Val(strMin));
                                    }
                                }
                                else
                                {
                                    strWhere += clsLoad.Get_Fields_String("fieldname") + " >= " +
                                                FCConvert.ToString(Conversion.Val(strMax));
                                }

                                break;
                            }
                            case modCustomReport.GRIDTEXT:
                            case modCustomReport.GRIDTEXTRANGE:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere += clsLoad.Get_Fields_String("fieldname") + " between '" + strMin +
                                                    "' and '" + strMax + "zzz'";
                                    }
                                    else
                                    {
                                        strWhere += clsLoad.Get_Fields_String("fieldname") + " >= '" + strMin + "'";
                                    }
                                }
                                else
                                {
                                    strWhere += clsLoad.Get_Fields_String("Fieldname") + " <= '" + strMax + "'";
                                }

                                break;
                            }
                        }

                        //end switch
                        strWhere += " and ";
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        switch (FCConvert.ToInt32(clsLoad.Get_Fields("code")))
                        {
                            case CNSTROWWHEREBILLABLE:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere +=
                                            "(convert(int, isnull(landvalue, 0)) + convert(int, isnull(buildingvalue, 0)) - (convert(int, isnull(otherexempt1, 0)) + convert(int, isnull(homesteadexemption, 0)))) between " +
                                            FCConvert.ToString(Conversion.Val(strMin)) + " and " +
                                            FCConvert.ToString(Conversion.Val(strMax));
                                    }
                                    else
                                    {
                                        strWhere +=
                                            "(convert(int, isnull(landvalue, 0)) + convert(int, isnull(buildingvalue, 0)) - (convert(int, isnull(otherexempt1, 0)) + convert(int, isnull(homesteadexemption, 0)))) >= " +
                                            FCConvert.ToString(Conversion.Val(strMin));
                                    }
                                }
                                else
                                {
                                    strWhere +=
                                        "(convert(int, isnull(landvalue, 0)) + convert(int, isnull(buildingvalue, 0)) - (convert(int, isnull(otherexempt1, 0)) + convert(int, isnull(homesteadexemption, 0)))) <= " +
                                        FCConvert.ToString(Conversion.Val(strMax));
                                }

                                break;
                            }
                            case CNSTROWWHEREBILLYEAR:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    lngStartYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strMin)));
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        lngEndYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strMax)));
                                        strWhere +=
                                            "convert(int, substring(STR(billingyear,5) + '    ',1,4)) between " +
                                            FCConvert.ToString(Conversion.Val(strMin)) + " and " +
                                            FCConvert.ToString(Conversion.Val(strMax));
                                    }
                                    else
                                    {
                                        strWhere += "convert(int, substring(STR(billingyear,5) + '    ',1,4)) >= " +
                                                    FCConvert.ToString(Conversion.Val(strMin));
                                    }
                                }
                                else
                                {
                                    lngEndYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strMax)));
                                    strWhere += "convert(int, substring(STR(billingyear,5) + '    ',1,4)) <= " +
                                                FCConvert.ToString(Conversion.Val(strMax));
                                }

                                break;
                            }
                            case CNSTROWWHEREEXEMPTVALUE:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere +=
                                            "(convert(int, isnull(homesteadexemption, 0)) + convert(int, isnull(otherexempt1, 0))) between " +
                                            FCConvert.ToString(Conversion.Val(strMin)) + " and " +
                                            FCConvert.ToString(Conversion.Val(strMax));
                                    }
                                    else
                                    {
                                        strWhere +=
                                            "(convert(int, isnull(homesteadexemption, 0)) + convert(int, isnull(otherexempt1, 0))) >= " +
                                            FCConvert.ToString(Conversion.Val(strMin));
                                    }
                                }
                                else
                                {
                                    strWhere +=
                                        "(convert(int, isnull(homesteadexemption, 0)) + convert(int, isnull(otherexempt1, 0))) <= " +
                                        FCConvert.ToString(Conversion.Val(strMax));
                                }

                                break;
                            }
                            case CNSTROWWHERELOCATION:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere += "streetname between '" + strMin + "' and '" + strMax + "zzz'";
                                    }
                                    else
                                    {
                                        strWhere += "streetname >= '" + strMin + "'";
                                    }
                                }
                                else
                                {
                                    strWhere += "streetname <= '" + strMax + "'";
                                }

                                break;
                            }
                            case CNSTROWWHEREORIGINALTAX:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere += "(taxdue1 + taxdue2 + taxdue3 + taxdue4) between " +
                                                    FCConvert.ToString(Conversion.Val(strMin)) + " and " +
                                                    FCConvert.ToString(Conversion.Val(strMax));
                                    }
                                    else
                                    {
                                        strWhere += "(taxdue1 + taxdue2 + taxdue3 + taxdue4) >= " +
                                                    FCConvert.ToString(Conversion.Val(strMin));
                                    }
                                }
                                else
                                {
                                    strWhere += "(taxdue1 + taxdue2 + taxdue3 + taxdue4) <= " +
                                                FCConvert.ToString(Conversion.Val(strMax));
                                }

                                break;
                            }
                            case CNSTROWWHEREPAIDTODATE:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere += "principalpaid between " +
                                                    FCConvert.ToString(Conversion.Val(strMin)) + " and " +
                                                    FCConvert.ToString(Conversion.Val(strMax));
                                    }
                                    else
                                    {
                                        strWhere += "principalpaid >= " + FCConvert.ToString(Conversion.Val(strMin));
                                    }
                                }
                                else
                                {
                                    strWhere += "pricipalpaid <= " + FCConvert.ToString(Conversion.Val(strMax));
                                }

                                break;
                            }
                            case CNSTROWWHEREPPBILLABLE:
                            {
                                if (Strings.Trim(strMin) != string.Empty)
                                {
                                    if (Strings.Trim(strMax) != string.Empty)
                                    {
                                        strWhere +=
                                            "(convert(int, isnull(ppassessment, 0)) - convert(int, isnull(exemptvalue, 0))) between " +
                                            FCConvert.ToString(Conversion.Val(strMin)) + " and " +
                                            FCConvert.ToString(Conversion.Val(strMax));
                                    }
                                    else
                                    {
                                        strWhere +=
                                            "(convert(int, isnull(ppassessment, 0)) - convert(int, isnull(exemptvalue, 0))) >= " +
                                            FCConvert.ToString(Conversion.Val(strMin));
                                    }
                                }
                                else
                                {
                                    strWhere +=
                                        "(convert(int, isnull(ppassessment, 0)) - convert(int, isnull(exemptvalue, 0))) <= " +
                                        FCConvert.ToString(Conversion.Val(strMax));
                                }

                                break;
                            }
                        }

                        //end switch
                        strWhere += " and ";
                    }

                    clsLoad.MoveNext();
                }

                if (strWhere != string.Empty)
                {
                    strWhere = Strings.Mid(strWhere, 1, strWhere.Length - 4);
                    strWhere = " where " + strWhere;
                }

                strSQL += strWhere + strOrderBy;
                MakeSQLStatement = strSQL;
                return MakeSQLStatement;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In MakeSQLStatement", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.Dispose();
            }
			
            return MakeSQLStatement;
		}

		private bool CreateLabelsAndTextboxes(int lngAutoID)
		{
			bool CreateLabelsAndTextboxes = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			int lngCol = 0;
			float lngCurrentLabelX;
			float lngCurrentLabelY;
			float lngCurrentBoxX;
			float lngCurrentBoxY;
			float lngHeight;
			float lngWidth = 0;
			int intDetailSpace = 0;
			// vbPorter upgrade warning: intReturn As short, int --> As DialogResult
			DialogResult intReturn;
			string strTemp = "";
			GrapeCity.ActiveReports.SectionReportModel.Label label;
			GrapeCity.ActiveReports.SectionReportModel.TextBox textBox;
			int lngFields;
            try
            {
                // On Error GoTo ErrorHandler
                CreateLabelsAndTextboxes = false;
                lngFields = 0;
                Array.Resize(ref aryTots, 1 + 1);
                clsLoad.OpenRecordset(
                    "select max(row) as maxrow from customreportfields where reportnumber = " +
                    FCConvert.ToString(lngAutoID), "twbl0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Field [maxrow] not found!! (maybe it is an alias?)
                    if (Conversion.Val(clsLoad.Get_Fields("maxrow")) > 1)
                    {
                        // make this greater than 0 to put extra space between records in multiline reports
                        // because of possible printer fonts, this could add too much space
                        // when using printer fonts, the extra space should be a multiple of 240 (line height)
                        intDetailSpace = 0;
                    }
                    else
                    {
                        intDetailSpace = 0;
                    }
                }

                clsLoad.OpenRecordset(
                    "select * from customreportfields where reportnumber = " + FCConvert.ToString(lngAutoID) +
                    " order by row,col", "twbl0000.vb1");
                if (clsLoad.EndOfFile())
                    return CreateLabelsAndTextboxes;
                lngCurrentLabelX = 0;
                lngCurrentLabelY = 720 / 1440f;
                lngCurrentBoxX = 0;
                lngCurrentBoxY = intDetailSpace;
                lngHeight = 240 / 1440f;
                lngRow = 1;
                while (!clsLoad.EndOfFile())
                {
                    // create a label for the header and a textbox for the detail section
                    // the labels tops will be 720 + (label height * row)
                    // boxes will be 240 + (box height * col)
                    lngFields += 1;
                    Array.Resize(ref udtAryReportFields, lngFields + 1);
                    //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                    udtAryReportFields[lngFields] = new TWBL0000.modCustomReport.CustomReportData(0);
                    if (clsLoad.Get_Fields_Int32("row") > lngRow)
                    {
                        lngCurrentLabelX = 0;
                        lngCurrentLabelY += lngHeight;
                        // + 720
                        lngCurrentBoxX = 0;
                        lngCurrentBoxY += 240 / 1440f;
                    }

                    lngRow = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("row"));
                    // TODO Get_Fields: Check the table for the column [Width] and replace with corresponding Get_Field method
                    lngWidth = FCConvert.ToSingle(clsLoad.Get_Fields("Width") / 1440F);
                    lngCol = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("COl"));
                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                    udtAryReportFields[lngFields].Code = FCConvert.ToInt32(clsLoad.Get_Fields("Code"));
                    udtAryReportFields[lngFields].Row = lngRow;
                    udtAryReportFields[lngFields].Col = lngCol;
                    udtAryReportFields[lngFields].Field = FCConvert.ToString(clsLoad.Get_Fields_String("FieldName"));
                    udtAryReportFields[lngFields].SpecialCase =
                        FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("specialcase"));
                    // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                    udtAryReportFields[lngFields].Type =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("type"))));
                    // now that we have the info we need to create the controls
                    label = new GrapeCity.ActiveReports.SectionReportModel.Label();
                    label.Top = lngCurrentLabelY;
                    label.Width = lngWidth;
                    label.Left = lngCurrentLabelX;
                    label.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                    label.Height = lngHeight;
                    label.Text = clsLoad.Get_Fields_String("description");
                    label.Font = new System.Drawing.Font(label.Font, System.Drawing.FontStyle.Bold);
                    label.Name = "lblRow" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol);
                    PageHeader.Controls.Add(label);
                    lngCurrentLabelX += lngWidth;
                    // now the textbox
                    textBox = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                    textBox.Top = lngCurrentBoxY;
                    textBox.Width = lngWidth;
                    textBox.Left = lngCurrentBoxX;
                    textBox.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                    textBox.Height = lngHeight;
                    textBox.Name = "txtRow" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol);
                    strTemp = textBox.Name;
                    Detail.Controls.Add(textBox);
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("totalfield")))
                    {
                        ReportFooter.Visible = true;
                        textBox = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                        textBox.Height = lngHeight;
                        // total
                        Array.Resize(ref aryTots, Information.UBound(aryTots, 1) + 1 + 1);
                        aryTots[Information.UBound(aryTots)] = strTemp;
                        textBox.Top = (lngRow * lngHeight) + Line1.Y1 + 120 / 1440f;
                        textBox.Left = lngCurrentBoxX;
                        textBox.Width = lngWidth;
                        textBox.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                        textBox.Name = "TOTALFOR" + strTemp;
                        if (ReportFooter.Height < textBox.Top + textBox.Height)
                        {
                            ReportFooter.Height = textBox.Top + textBox.Height + 20 / 1440f;
                        }

                        ReportFooter.Controls.Add(textBox);
                    }

                    lngCurrentBoxX += lngWidth;
                    clsLoad.MoveNext();
                }

                Detail.Height = lngCurrentBoxY + intDetailSpace;
                if (lngCurrentBoxX > 7.5F)
                {
                    // this must be landscape or to a wide printer
                    intReturn = MessageBox.Show(
                        "This report is wider than normal. Do you wish to print landscape?" + "\r\n" +
                        "If you answer no the page will be formatted for a wide printer.", "Print Landscape?",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (intReturn == DialogResult.Yes)
                    {
                        this.PageSettings.Orientation =
                            GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                        txtPage.Left = 10 - txtPage.Width - 20 / 1440f;
                        txtDate.Left = 10 - txtDate.Width - 20 / 1440f;
                        Label9.Width = txtDate.Left - Label9.Left;
                        //FC:FINAL:CHN - issue #1416: Not changed width to printing on changing Orientation.
                        this.PrintWidth = this.PageSettings.PaperHeight;
                    }
                    else
                    {
                        this.PageSettings.PaperWidth = 13.5F;
                        // 14.5 inches
                    }
                }

                //FC:FINAL:CHN - issue #1423: Missing converting size to C# version.
                // PageHeader.Height = lngCurrentLabelY + 1;
                PageHeader.Height = lngCurrentLabelY + 1 / 1440f;
                PageHeader.CanShrink = false;
                CreateLabelsAndTextboxes = true;
                return CreateLabelsAndTextboxes;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In CreateLabelsAndTextboxes", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                clsLoad.Dispose();
            }

			return CreateLabelsAndTextboxes;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			int lngRow = 0;
			int lngCol = 0;
			int y;
			for (x = 1; x <= Information.UBound(udtAryReportFields, 1); x++)
			{
				lngRow = udtAryReportFields[x].Row;
				lngCol = udtAryReportFields[x].Col;
				if (!udtAryReportFields[x].SpecialCase)
				{
					// Detail.Controls("txtRow" & lngRow & "Col" & lngCol).Text = clsReport.Fields(udtAryReportFields(x).Field)
					(Detail.Controls["txtRow" + lngRow + "Col" + lngCol] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = HandleRegularCase_2(x);
				}
				else
				{
					(Detail.Controls["txtRow" + lngRow + "Col" + lngCol] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = HandleSpecialCase(ref udtAryReportFields[x].Code);
				}
				for (y = 1; y <= Information.UBound(aryTots, 1); y++)
				{
					if (Strings.UCase(aryTots[y]) == Strings.UCase("txtROW" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol)))
					{
						//FC:FINAL:CHN - issue #1429: Incorrect converting.
						//(ReportFooter.Controls["TOTALFOR" + aryTots[y]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Conversion.Val((ReportFooter.Controls["TOTALFOR" + aryTots[y]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text) + FCConvert.ToDouble((Detail.Controls["txtRow" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text).ToString();
						(ReportFooter.Controls["TOTALFOR" + aryTots[y]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((ReportFooter.Controls["TOTALFOR" + aryTots[y]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text) + Conversion.CDbl((Detail.Controls["txtRow" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text));
						break;
					}
				}
				// y
			}
			// x
			clsReport.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private string HandleRegularCase_2(int lngIndex)
		{
			return HandleRegularCase(ref lngIndex);
		}

		private string HandleRegularCase(ref int lngIndex)
		{
			string HandleRegularCase = "";
			int lngType;
			// vbPorter upgrade warning: dblTemp As double	OnRead(string)
			double dblTemp = 0;
			// vbPorter upgrade warning: strReturn As string	OnWrite(string, double)
			string strReturn;
			try
			{
				// On Error GoTo ErrorHandler
				lngType = udtAryReportFields[lngIndex].Type;
				strReturn = "";
				switch (lngType)
				{
					case modCustomReport.GRIDBOOLEAN:
						{
							if (FCConvert.ToBoolean(clsReport.Get_Fields(udtAryReportFields[lngIndex].Field)))
							{
								strReturn = "Yes";
							}
							else
							{
								strReturn = "No";
							}
							break;
						}
					case modCustomReport.GRIDDATE:
						{
							if (Information.IsDate(clsReport.Get_Fields(udtAryReportFields[lngIndex].Field)))
							{
								if (clsReport.Get_Fields_DateTime(udtAryReportFields[lngIndex].Field).ToOADate() == 0)
								{
									strReturn = "";
								}
								else
								{
									strReturn = Strings.Format(clsReport.Get_Fields(udtAryReportFields[lngIndex].Field), "MM/dd/yyyy");
								}
							}
							else
							{
								strReturn = "";
							}
							break;
						}
					case modCustomReport.GRIDNUMRANGE:
					case modCustomReport.GRIDCOMBOIDNUM:
						{
							// determine if is is a double or not
							dblTemp = Conversion.Val(clsReport.Get_Fields(udtAryReportFields[lngIndex].Field));
							if (udtAryReportFields[lngIndex].Code != CNSTROWWHEREACCOUNT)
							{
								if (Conversion.Int(dblTemp) == dblTemp)
								{
									strReturn = Strings.Format(dblTemp, "#,###,###,###,##0");
								}
								else
								{
									strReturn = Strings.Format(dblTemp, "#,###,###,###,##0.00");
								}
							}
							else
							{
								strReturn = FCConvert.ToString(dblTemp);
							}
							break;
						}
					case modCustomReport.GRIDTEXT:
					case modCustomReport.GRIDTEXTRANGE:
					case modCustomReport.GRIDCOMBOTEXT:
					case modCustomReport.GRIDCOMBOIDTEXT:
						{
							strReturn = FCConvert.ToString(clsReport.Get_Fields(udtAryReportFields[lngIndex].Field));
							break;
						}
				}
				//end switch
				HandleRegularCase = strReturn;
				return HandleRegularCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleRegularCase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleRegularCase;
		}

		private string HandleSpecialCase(ref int lngCode)
		{
			string HandleSpecialCase = "";
			string strReturn = "";
			bool boolREBills = false;
			clsDRWrapper rsLien = new clsDRWrapper();
			clsDRWrapper rsCL = new clsDRWrapper();
			string strSQL = "";
			double dblTotal = 0;
			double dblXtraInt = 0;
            try
            {
                // On Error GoTo ErrorHandler
                HandleSpecialCase = "";
                switch (lngCode)
                {
                    case CNSTROWWHERELOCATION:
                    {
                        // TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
                        if (Conversion.Val(clsReport.Get_Fields("streetnumber")) > 0)
                        {
                            // TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
                            strReturn = FCConvert.ToString(clsReport.Get_Fields("streetnumber"));
                        }

                        strReturn = Strings.Trim(strReturn + clsReport.Get_Fields_String("apt") + " " +
                                                 clsReport.Get_Fields_String("streetname"));
                        break;
                    }
                    case CNSTROWWHEREBILLYEAR:
                    {
                        strReturn = Strings.Mid(clsReport.Get_Fields_Int32("billingyear") + "    ", 1, 4);
                        break;
                    }
                    case CNSTROWWHEREBILLABLE:
                    {
                        strReturn = Strings.Format(
                            Conversion.Val(clsReport.Get_Fields_Int32("landvalue")) +
                            Conversion.Val(clsReport.Get_Fields_Int32("BuildingValue")) -
                            (Conversion.Val(clsReport.Get_Fields_Double("OtherExempt1")) +
                             Conversion.Val(clsReport.Get_Fields_Double("HomesteadExemption"))), "#,###,###,##0");
                        break;
                    }
                    case CNSTROWWHEREPPBILLABLE:
                    {
                        strReturn = Strings.Format(
                            Conversion.Val(clsReport.Get_Fields_Int32("PPAssessment")) -
                            Conversion.Val(clsReport.Get_Fields_Int32("ExemptValue")), "#,###,###,##0");
                        break;
                    }
                    case CNSTROWWHEREORIGINALTAX:
                    {
                        strReturn = Strings.Format(
                            clsReport.Get_Fields_Decimal("taxdue1") + clsReport.Get_Fields_Decimal("taxdue2") +
                            clsReport.Get_Fields_Decimal("taxdue3") + clsReport.Get_Fields_Decimal("taxdue4"),
                            "#,###,###,##0.00");
                        break;
                    }
                    case CNSTROWWHERECURRENTDUE:
                    {
                        if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("billingtype"))) == "PP")
                        {
                            boolREBills = false;
                        }
                        else
                        {
                            boolREBills = true;
                        }

                        // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                        strReturn = Strings.Format(
                            modCLCalculations.CalculateAccountTotal(FCConvert.ToInt32(clsReport.Get_Fields("account")),
                                boolREBills), "#,###,###,##0.00");
                        if (FCConvert.ToDouble(strReturn) < 0)
                        {
                            strReturn = "Overpaid";
                        }

                        break;
                    }
                    case CNSTROWWHEREOUTSTANDINGPRIN:
                    {
                        if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("billingtype"))) == "PP")
                        {
                            boolREBills = false;
                        }
                        else
                        {
                            boolREBills = true;
                        }

                        double dblCurPrin = 0;
                        if (boolREBills)
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            strSQL = "SELECT * FROM BillingMaster WHERE Account = " + clsReport.Get_Fields("account") +
                                     " AND BillingType = 'RE' ";
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            strSQL = "SELECT * FROM BillingMaster WHERE Account = " + clsReport.Get_Fields("account") +
                                     " AND BillingType = 'PP' ";
                        }

                        if (lngStartYear > 0 || lngEndYear > 0)
                        {
                            if (lngEndYear > 0)
                            {
                                strSQL += " and billINGyear >= " + FCConvert.ToString(lngStartYear) +
                                          "1 and billingyear <= " + FCConvert.ToString(lngEndYear) + "9 ";
                            }
                            else
                            {
                                strSQL += " and billingyear >= " + FCConvert.ToString(lngStartYear) + "1 ";
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            strReturn = Strings.Format(
                                modCollectionsRelated.CalculateAccountTotalPrincipal_2(clsReport.Get_Fields("account"),
                                    boolREBills), "#,###,###,##0.00");
                            if (FCConvert.ToDouble(strReturn) < 0)
                            {
                                strReturn = "Overpaid";
                            }

                            HandleSpecialCase = strReturn;
                            return HandleSpecialCase;
                        }

                        rsCL.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
                        if (!rsCL.EndOfFile())
                        {
                            while (!rsCL.EndOfFile())
                            {
                                // calculate each year
                                if (FCConvert.ToInt32(rsCL.Get_Fields_Int32("LienRecordNumber")) == 0)
                                {
                                    // non-lien
                                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                                    double dblTemp = FCConvert.ToDouble(
                                        rsCL.Get_Fields_Decimal("DemandFees") -
                                        rsCL.Get_Fields_Decimal("DemandFeesPaid"));
                                    modCLCalculations.CalculateAccountCL3(ref rsCL, clsReport.Get_Fields("account"),
                                        DateTime.Today, ref dblXtraInt, ref dblCurPrin, ref dblTemp);
                                    dblTotal += dblCurPrin;
                                }
                                else
                                {
                                    // lien
                                    rsLien.OpenRecordset(
                                        "select * from lienrec where ID = " + rsCL.Get_Fields_Int32("lienrecordnumber"),
                                        modGlobalVariables.strCLDatabase);
                                    if (!rsLien.EndOfFile())
                                    {
                                        double dblTemp = FCConvert.ToDouble(
                                            rsCL.Get_Fields_Decimal("DemandFees") -
                                            rsCL.Get_Fields_Decimal("DemandFeesPaid"));
                                        modCLCalculations.CalculateAccountCLLien3(rsLien, DateTime.Today,
                                            ref dblXtraInt, ref dblCurPrin, ref dblTemp);
                                        dblTotal += dblCurPrin;
                                    }
                                }

                                rsCL.MoveNext();
                            }
                        }
                        else
                        {
                            // nothing found
                            dblTotal = 0;
                        }

                        strReturn = Strings.Format(dblTotal, "#,###,###,##0.00");
                        if (dblTotal < 0)
                        {
                            strReturn = "Overpaid";
                        }

                        break;
                    }
                    case CNSTROWWHEREOUTSTANDING:
                    {
                        if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("billingtype"))) == "PP")
                        {
                            boolREBills = false;
                        }
                        else
                        {
                            boolREBills = true;
                        }

                        if (boolREBills)
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            strSQL = "SELECT * FROM BillingMaster WHERE Account = " + clsReport.Get_Fields("account") +
                                     " AND BillingType = 'RE' ";
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            strSQL = "SELECT * FROM BillingMaster WHERE Account = " + clsReport.Get_Fields("account") +
                                     " AND BillingType = 'PP' ";
                        }

                        if (lngStartYear > 0 || lngEndYear > 0)
                        {
                            if (lngEndYear > 0)
                            {
                                strSQL += " and billINGyear >= " + FCConvert.ToString(lngStartYear) +
                                          "1 and billingyear <= " + FCConvert.ToString(lngEndYear) + "9 ";
                            }
                            else
                            {
                                strSQL += " and billingyear >= " + FCConvert.ToString(lngStartYear) + "1 ";
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            strReturn = Strings.Format(
                                modCLCalculations.CalculateAccountTotal(
                                    FCConvert.ToInt32(clsReport.Get_Fields("account")), boolREBills),
                                "#,###,###,##0.00");
                            if (FCConvert.ToDouble(strReturn) < 0)
                            {
                                strReturn = "Overpaid";
                            }

                            return HandleSpecialCase;
                        }

                        rsLien.OpenRecordset("SELECT * FROM LienRec", modGlobalVariables.strCLDatabase);
                        rsCL.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
                        if (!rsCL.EndOfFile())
                        {
                            while (!rsCL.EndOfFile())
                            {
                                // calculate each year
                                if (FCConvert.ToInt32(rsCL.Get_Fields_Int32("LienRecordNumber")) == 0)
                                {
                                    // non-lien
                                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                                    dblTotal += modCLCalculations.CalculateAccountCL2(ref rsCL,
                                        clsReport.Get_Fields("account"), DateTime.Today, ref dblXtraInt,
                                        FCConvert.ToDouble(rsCL.Get_Fields_Decimal("DemandFees") -
                                                           rsCL.Get_Fields_Decimal("DemandFeesPaid")));
                                }
                                else
                                {
                                    // lien
                                    rsLien.FindFirstRecord("ID", rsCL.Get_Fields_Int32("LienrecordNumber"));
                                    if (rsLien.NoMatch)
                                    {
                                        // no match has been found
                                        // do nothing
                                    }
                                    else
                                    {
                                        // calculate the lien record
                                        dblTotal += modCLCalculations.CalculateAccountCLLien(rsLien, DateTime.Today,
                                            ref dblXtraInt);
                                    }
                                }

                                rsCL.MoveNext();
                            }
                        }
                        else
                        {
                            // nothing found
                            dblTotal = 0;
                        }

                        strReturn = Strings.Format(dblTotal, "#,###,###,##0.00");
                        if (dblTotal < 0)
                        {
                            strReturn = "Overpaid";
                        }

                        break;
                    }
                    case CNSTROWWHEREPAIDTODATE:
                    {
                        strReturn = Strings.Format(clsReport.Get_Fields_Decimal("principalpaid"), "#,###,###,##0.00");
                        break;
                    }
                    case CNSTROWWHEREEXEMPTVALUE:
                    {
                        //FC:FINAL:CHN - issue #1438: Incorrect converting.
                        // strReturn = Strings.Format(FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("Otherexempt1")))) + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("HomesteadEXEMPTION")))), "#,###,###,##0");
                        strReturn = Strings.Format(
                            (Conversion.Val(clsReport.Get_Fields_Double("Otherexempt1"))) +
                            (Conversion.Val(clsReport.Get_Fields_Double("HomesteadEXEMPTION"))), "#,###,###,##0");
                        break;
                    }
                }

                //end switch
                HandleSpecialCase = strReturn;
                return HandleSpecialCase;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In HandleSpecialCase", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
                rsCL.Dispose();
                rsLien.Dispose();
            }
			return HandleSpecialCase;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			int x;
			for (x = 1; x <= Information.UBound(aryTots, 1); x++)
			{
				if (Strings.Trim(aryTots[x]) != string.Empty)
				{
					if (Strings.InStr(1, (ReportFooter.Controls["TOTALFOR" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, ".", CompareConstants.vbTextCompare) > 0)
					{
						(ReportFooter.Controls["TOTALFOR" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format((ReportFooter.Controls["TOTALFOR" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, "#,###,###,##0.00");
					}
					else
					{
						(ReportFooter.Controls["TOTALFOR" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format((ReportFooter.Controls["TOTALFOR" + aryTots[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, "#,###,###,##0");
					}
				}
			}
			// x
		}

		
	}
}
