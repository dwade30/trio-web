﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPickREorPP.
	/// </summary>
	public partial class frmPickREorPP : BaseForm
	{
		public frmPickREorPP()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPickREorPP InstancePtr
		{
			get
			{
				return (frmPickREorPP)Sys.GetInstance(typeof(frmPickREorPP));
			}
		}

		protected frmPickREorPP _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int lngRKey;
		/// <summary>
		/// ratekey
		/// </summary>
		int intFromWhere;
		/// <summary>
		/// tells what made the call to this form
		/// </summary>
		bool boolRE;
		bool boolPP;
		// vbPorter upgrade warning: intWhereFrom As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngRateKey, int intWhereFrom)
		{
			intFromWhere = intWhereFrom;
			lngRKey = lngRateKey;
			switch (intWhereFrom)
			{
				case 1:
				case 2:
					{
						this.Text = "Select tax files to audit";
						break;
					}
				case 3:
				case 4:
					{
						this.Text = "Select tax files to transfer";
						break;
					}
			}
			//end switch
			//FC:FINAL:MSH - issue #1281: show form in a small window 
			//this.Show(App.MainForm);
			this.Show(FormShowEnum.Modeless);
		}

		private void FillCmbDB()
		{
			cmbDB.Clear();
			cmbDB.AddItem("Live");
		}

		private void frmPickREorPP_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuContinue_Click();
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
			}
		}

		private void frmPickREorPP_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPickREorPP properties;
			//frmPickREorPP.ScaleWidth	= 5880;
			//frmPickREorPP.ScaleHeight	= 4200;
			//frmPickREorPP.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			clsRateRecord clsRateRec = new clsRateRecord();
			if (!clsRateRec.LoadRate(lngRKey))
			{
				MessageBox.Show("Error Loading raterecord " + FCConvert.ToString(lngRKey) + " cannot continue ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Close();
				return;
			}
			if (cmbModule.SelectedIndex == 0)
			{
				// real estate
				boolRE = true;
				boolPP = false;
			}
			else if (cmbModule.SelectedIndex == 1)
			{
				// personal property
				boolRE = false;
				boolPP = true;
			}
			else if (cmbModule.SelectedIndex == 2)
			{
				// both
				boolRE = true;
				boolPP = true;
			}
			switch (intFromWhere)
			{
				case 1:
				case 2:
					{
						// audit
						this.Hide();
						if (boolRE)
						{
							frmREAudit.InstancePtr.Init(ref clsRateRec, 1, boolPP);
						}
						else
						{
							frmPPAudit.InstancePtr.Init(ref clsRateRec, 2);
						}
						break;
					}
				case 3:
				case 4:
					{
						// xfer
						if (modGlobalConstants.Statics.gboolBD)
						{
							modREPPBudStuff.LoadREPPAccountInfo_2(clsRateRec.TaxYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
							if (boolRE)
							{
								if (Strings.Trim(modGlobalVariables.Statics.REPPAccountInfo.RECommitment) == string.Empty)
								{
									MessageBox.Show("You must set up a valid Real Estate Commitment account in Budgetary before proceeding", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									Close();
									return;
								}
								if (Strings.Trim(modGlobalVariables.Statics.REPPAccountInfo.REReceivable) == string.Empty)
								{
									MessageBox.Show("You must set up a valid Real Estate Receivable account in Budgetary before proceeding", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									Close();
									return;
								}
							}
							if (boolPP)
							{
								if (Strings.Trim(modGlobalVariables.Statics.REPPAccountInfo.PPCommitment) == string.Empty)
								{
									MessageBox.Show("You must set up a valid Personal Property Commitment account in Budgetary before proceeding", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									Close();
									return;
								}
								if (Strings.Trim(modGlobalVariables.Statics.REPPAccountInfo.PPReceivable) == string.Empty)
								{
									MessageBox.Show("You must set up a valid Personal Property Receivable account in Budgetary before proceeding", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									Close();
									return;
								}
							}
						}
						if (boolRE)
						{
							frmREAudit.InstancePtr.Init(ref clsRateRec, 3, boolPP);
						}
						else
						{
							frmPPAudit.InstancePtr.Init(ref clsRateRec, 4);
						}
						break;
					}
			}
			//end switch
			Close();
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
