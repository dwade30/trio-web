﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmReturnAddress.
	/// </summary>
	public partial class frmReturnAddress : BaseForm
	{
		public frmReturnAddress()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReturnAddress InstancePtr
		{
			get
			{
				return (frmReturnAddress)Sys.GetInstance(typeof(frmReturnAddress));
			}
		}

		protected frmReturnAddress _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolModal;
		bool boolLoading;
		int lastTown;

		private struct AddrType
		{
			public string[] strAddress;
			//= new string[4];
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public AddrType(int unusedParam)
			{
				this.strAddress = new string[4] {
					string.Empty,
					string.Empty,
					string.Empty,
					string.Empty
				};
			}
		};

		private AddrType[] aryAddress = null;

		private void cmbTown_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				aryAddress[lastTown].strAddress[0] = txtAddress1.Text;
				aryAddress[lastTown].strAddress[1] = txtAddress2.Text;
				aryAddress[lastTown].strAddress[2] = txtAddress3.Text;
				aryAddress[lastTown].strAddress[3] = txtAddress4.Text;
				LoadReturnAddress();
			}
		}

		private void frmReturnAddress_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
				return;
			}
		}

		private void frmReturnAddress_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmReturnAddress_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReturnAddress properties;
			//frmReturnAddress.ScaleWidth	= 9390;
			//frmReturnAddress.ScaleHeight	= 7440;
			//frmReturnAddress.LinkTopic	= "Form1";
			//frmReturnAddress.LockControls	= true;
			//End Unmaped Properties
			boolLoading = true;
			cmbTown.Clear();
			int x = 0;
			if (!modRegionalTown.IsRegionalTown())
			{
				cmbTown.AddItem(modGlobalConstants.Statics.MuniName);
				cmbTown.ItemData(0, 0);
				cmbTown.Visible = false;
				//FC:FINAL:CHN: Fix difference between initialization of arrays.
				// aryAddress = new AddrType[1];
				aryAddress = new AddrType[2];
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				aryAddress[0] = new AddrType(0);
				aryAddress[1] = new AddrType(0);
				lastTown = 0;
			}
			else
			{
				x = 0;
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from TBLREgions where townnumber > 0 order by townname", "CentralData");
				while (!rsLoad.EndOfFile())
				{
					cmbTown.AddItem(rsLoad.Get_Fields_String("townname"));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					cmbTown.ItemData(cmbTown.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields("townnumber")));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(rsLoad.Get_Fields("townnumber")) > x)
					{
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						x = FCConvert.ToInt32(rsLoad.Get_Fields("townnumber"));
						Array.Resize(ref aryAddress, x);
						for (int i = 0; i < x; i++)
						{
							//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
							aryAddress[i] = new AddrType(0);
						}
					}
					rsLoad.MoveNext();
				}
				cmbTown.Visible = true;
			}
			cmbTown.SelectedIndex = 0;
			lastTown = cmbTown.ItemData(cmbTown.SelectedIndex);
			LoadAddresses();
			LoadReturnAddress();
			modGlobalFunctions.SetFixedSize(this);
			boolLoading = false;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (!boolModal)
			{
				//MDIParent.InstancePtr.Show();
			}
		}

		public void Init()
		{
			boolModal = true;
			this.Show(FormShowEnum.Modal);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			int x;
			int intStart = 0;
			int intEnd = 0;
			aryAddress[lastTown].strAddress[0] = txtAddress1.Text;
			aryAddress[lastTown].strAddress[1] = txtAddress2.Text;
			aryAddress[lastTown].strAddress[2] = txtAddress3.Text;
			aryAddress[lastTown].strAddress[3] = txtAddress4.Text;
			if (!modRegionalTown.IsRegionalTown())
			{
				intStart = 0;
				intEnd = 0;
			}
			else
			{
				intStart = 1;
				intEnd = Information.UBound(aryAddress, 1);
			}
			clsSave.OpenRecordset("select * from returnaddress", "twbl0000.vb1");
			for (x = intStart; x <= intEnd; x++)
			{
				if (!clsSave.FindFirstRecord("townnumber", x))
				{
					clsSave.AddNew();
				}
				else
				{
					clsSave.Edit();
				}
				clsSave.Set_Fields("address1", aryAddress[x].strAddress[0]);
				clsSave.Set_Fields("address2", aryAddress[x].strAddress[1]);
				clsSave.Set_Fields("address3", aryAddress[x].strAddress[2]);
				clsSave.Set_Fields("address4", aryAddress[x].strAddress[3]);
				clsSave.Set_Fields("townnumber", x);
				clsSave.Update();
			}
			// x
			Close();
		}

		private void LoadAddresses()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from returnaddress order by townnumber", modGlobalVariables.strBLDatabase);
			while (!rsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				aryAddress[rsLoad.Get_Fields("townnumber")].strAddress[0] = FCConvert.ToString(rsLoad.Get_Fields_String("address1"));
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				aryAddress[rsLoad.Get_Fields("townnumber")].strAddress[1] = FCConvert.ToString(rsLoad.Get_Fields_String("address2"));
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				aryAddress[rsLoad.Get_Fields("townnumber")].strAddress[2] = FCConvert.ToString(rsLoad.Get_Fields_String("address3"));
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				aryAddress[rsLoad.Get_Fields("townnumber")].strAddress[3] = FCConvert.ToString(rsLoad.Get_Fields_String("address4"));
				rsLoad.MoveNext();
			}
		}

		private void LoadReturnAddress()
		{
			lastTown = cmbTown.ItemData(cmbTown.SelectedIndex);
			txtAddress1.Text = aryAddress[cmbTown.ItemData(cmbTown.SelectedIndex)].strAddress[0];
			txtAddress2.Text = aryAddress[cmbTown.ItemData(cmbTown.SelectedIndex)].strAddress[1];
			txtAddress3.Text = aryAddress[cmbTown.ItemData(cmbTown.SelectedIndex)].strAddress[2];
			txtAddress4.Text = aryAddress[cmbTown.ItemData(cmbTown.SelectedIndex)].strAddress[3];
		}
	}
}
