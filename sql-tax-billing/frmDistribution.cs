﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmDistribution.
	/// </summary>
	public partial class frmDistribution : BaseForm
	{
		public frmDistribution()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtName = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtPercent = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtName.AddControlArrayElement(txtName_5, 5);
			this.txtName.AddControlArrayElement(txtName_6, 6);
			this.txtName.AddControlArrayElement(txtName_4, 4);
			this.txtName.AddControlArrayElement(txtName_3, 3);
			this.txtName.AddControlArrayElement(txtName_2, 2);
			this.txtName.AddControlArrayElement(txtName_1, 1);
			this.txtName.AddControlArrayElement(txtName_0, 0);
			this.txtPercent.AddControlArrayElement(txtPercent_5, 5);
			this.txtPercent.AddControlArrayElement(txtPercent_6, 6);
			this.txtPercent.AddControlArrayElement(txtPercent_4, 4);
			this.txtPercent.AddControlArrayElement(txtPercent_3, 3);
			this.txtPercent.AddControlArrayElement(txtPercent_2, 2);
			this.txtPercent.AddControlArrayElement(txtPercent_1, 1);
			this.txtPercent.AddControlArrayElement(txtPercent_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDistribution InstancePtr
		{
			get
			{
				return (frmDistribution)Sys.GetInstance(typeof(frmDistribution));
			}
		}

		protected frmDistribution _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolModal;
		int intNumLines;
		bool boolMustVerify;
		double dblSavedPercentage;

		private void frmDistribution_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmDistribution_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// return key
				KeyAscii = (Keys)0;
				if (this.ActiveControl is FCTextBox)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDistribution_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDistribution properties;
			//frmDistribution.ScaleWidth	= 9045;
			//frmDistribution.ScaleHeight	= 7110;
			//frmDistribution.LinkTopic	= "Form1";
			//frmDistribution.LockControls	= true;
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x;
			double dblTotal;
			dblSavedPercentage = 0;
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			Label7.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			clsLoad.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
			x = 1;
			dblTotal = 0;
			while (!clsLoad.EndOfFile() && x < intNumLines + 1)
			{
				txtName[FCConvert.ToInt16(x - 1)].Text = FCConvert.ToString(clsLoad.Get_Fields_String("distributionname"));
				txtPercent[FCConvert.ToInt16(x - 1)].Text = FCConvert.ToString(clsLoad.Get_Fields_Double("distributionpercent"));
				dblTotal += clsLoad.Get_Fields_Double("distributionpercent");
				x += 1;
				clsLoad.MoveNext();
			}
			dblSavedPercentage = dblTotal;
			while (!(x > 7))
			{
				txtName[FCConvert.ToInt16(x - 1)].Text = "";
				txtPercent[FCConvert.ToInt16(x - 1)].Text = "";
				x += 1;
			}
			txtTotal.Text = Strings.Format(dblTotal, "##0.00");
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolMustVerify)
			{
				if (Conversion.Val(dblSavedPercentage) != 100)
				{
					MessageBox.Show("Distributions must equal 100 percent", "Invalid Percentage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					return;
				}
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (!boolModal)
			{
				//MDIParent.InstancePtr.Show();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			double dblTotal;
			int x;
			clsDRWrapper clsCheck = new clsDRWrapper();
			// check for legit values in the database since they could have screwed them
			// up on the screen but not saved them
			clsCheck.OpenRecordset("select sum(distributionpercent) as thesum from distributiontable", "twbl0000.vb1");
			if (boolMustVerify)
			{
				if (clsCheck.EndOfFile())
				{
					MessageBox.Show("There are no values saved.  You must save before exiting.", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
					if (Conversion.Val(clsCheck.Get_Fields("thesum")) != 100)
					{
						MessageBox.Show("The distribution percents saved in the database do not add up to 100", "Invalid Distribution", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			modGlobalVariables.Statics.gboolCancelForm = true;
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			int x;
			decimal dblTotal;
			string strTemp = "";
			// vbPorter upgrade warning: intTemp As string	OnWriteFCConvert.ToInt32(
			string intTemp = "";
			// check for legit values
			dblTotal = 0;
			for (x = 0; x <= 6; x++)
			{
				dblTotal += FCConvert.ToDecimal(Conversion.Val(txtPercent[FCConvert.ToInt16(x)].Text));
				if (Conversion.Val(txtPercent[FCConvert.ToInt16(x)].Text) == 0)
				{
					if (Strings.Trim(txtName[FCConvert.ToInt16(x)].Text) == string.Empty)
					{
						txtPercent[FCConvert.ToInt16(x)].Text = "";
					}
				}
			}
			// x
			if (dblTotal != 100)
			{
				MessageBox.Show("The percentages must add up to 100 percent before this information can be saved.", "Invalid Percentage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			clsSave.Execute("delete from distributiontable", "twbl0000.vb1");
			for (x = 0; x <= 6; x++)
			{
				if (txtPercent[FCConvert.ToInt16(x)].Text != string.Empty)
				{
					strTemp = txtPercent[FCConvert.ToInt16(x)].Text;
					intTemp = FCConvert.ToString(Strings.InStr(1, strTemp, "%", CompareConstants.vbTextCompare));
					if (FCConvert.ToDouble(intTemp) > 0)
					{
						strTemp = Strings.Mid(strTemp, 1, FCConvert.ToInt32(intTemp) - 1);
					}
					clsSave.Execute("insert into distributiontable (distributionname,distributionpercent,distributionnumber) values ('" + txtName[FCConvert.ToInt16(x)].Text + "'," + strTemp + "," + FCConvert.ToString(x + 1) + ")", "twbl0000.vb1");
				}
			}
			// x
			dblSavedPercentage = FCConvert.ToDouble(dblTotal);
			modGlobalVariables.Statics.gboolCancelForm = false;
			Close();
		}

		private void txtPercent_Validating(short Index, object sender, EventArgs e)
		{
			double dblNumber;
			int x;
			string strTemp;
			strTemp = txtPercent[Index].Text;
			x = Strings.InStr(1, strTemp, "%", CompareConstants.vbTextCompare);
			if (x > 0)
			{
				strTemp = Strings.Mid(strTemp, 1, x - 1);
			}
			txtPercent[Index].Text = Strings.Format(Conversion.Val(strTemp), "##0.00");
			dblNumber = 0;
			for (x = 0; x <= 6; x++)
			{
				dblNumber += Conversion.Val(txtPercent[FCConvert.ToInt16(x)].Text);
			}
			// x
			txtTotal.Text = Strings.Format(dblNumber, "##0.00");
		}

		private void txtPercent_Validating(object sender, System.EventArgs e)
		{
			short index = txtPercent.GetIndex((FCTextBox)sender);
			txtPercent_Validating(index, sender, e);
		}
		// vbPorter upgrade warning: intHowManyLines As short	OnWriteFCConvert.ToInt32(
		public void Init(int intHowManyLines = 7, bool boolFromBills = true)
		{
			boolMustVerify = boolFromBills;
			intNumLines = intHowManyLines;
			if (intNumLines == 5)
			{
				txtPercent[5].Enabled = false;
				txtPercent[6].Enabled = false;
				txtName[5].Enabled = false;
				txtName[6].Enabled = false;
			}
			boolModal = true;
			this.Show(FormShowEnum.Modal, App.MainForm);
		}

		private void cmdSaveAndExit_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}
	}
}
