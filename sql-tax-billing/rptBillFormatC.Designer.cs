﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormatC.
	/// </summary>
	partial class rptBillFormatC
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillFormatC));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtMailing1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCatOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInterest1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInterest2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaidLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterest1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterest2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMailing1,
				this.txtMailing2,
				this.txtMailing3,
				this.txtMailing4,
				this.txtAccount,
				this.txtLand,
				this.txtBldg,
				this.txtCat1,
				this.txtCat2,
				this.txtCatOther,
				this.txtOtherLabel,
				this.txtExemption,
				this.txtAssessment,
				this.txtRate,
				this.txtTaxDue,
				this.txtMapLot,
				this.txtLot,
				this.txtBookPage,
				this.txtPage,
				this.txtLocation,
				this.txtMessage1,
				this.txtMessage4,
				this.txtMessage5,
				this.txtMessage2,
				this.txtMessage3,
				this.txtAmount1,
				this.txtAcct1,
				this.txtInterest1,
				this.txtAmount2,
				this.txtAcct2,
				this.txtInterest2,
				this.txtPrePaidLabel,
				this.txtPrePaid,
				this.txtYear1,
				this.txtYear2,
				this.txtMailing5
			});
			this.Detail.Height = 5.104167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtMailing1
			// 
			this.txtMailing1.CanGrow = false;
			this.txtMailing1.Height = 0.19F;
			this.txtMailing1.Left = 0.25F;
			this.txtMailing1.MultiLine = false;
			this.txtMailing1.Name = "txtMailing1";
			this.txtMailing1.Tag = "textbox";
			this.txtMailing1.Text = "Field1";
			this.txtMailing1.Top = 2.78125F;
			this.txtMailing1.Width = 3.625F;
			// 
			// txtMailing2
			// 
			this.txtMailing2.CanGrow = false;
			this.txtMailing2.Height = 0.19F;
			this.txtMailing2.Left = 0.25F;
			this.txtMailing2.MultiLine = false;
			this.txtMailing2.Name = "txtMailing2";
			this.txtMailing2.Tag = "textbox";
			this.txtMailing2.Text = "Field1";
			this.txtMailing2.Top = 2.9375F;
			this.txtMailing2.Width = 3.625F;
			// 
			// txtMailing3
			// 
			this.txtMailing3.CanGrow = false;
			this.txtMailing3.Height = 0.19F;
			this.txtMailing3.Left = 0.25F;
			this.txtMailing3.MultiLine = false;
			this.txtMailing3.Name = "txtMailing3";
			this.txtMailing3.Tag = "textbox";
			this.txtMailing3.Text = "Field1";
			this.txtMailing3.Top = 3.09375F;
			this.txtMailing3.Width = 3.625F;
			// 
			// txtMailing4
			// 
			this.txtMailing4.CanGrow = false;
			this.txtMailing4.Height = 0.19F;
			this.txtMailing4.Left = 0.25F;
			this.txtMailing4.MultiLine = false;
			this.txtMailing4.Name = "txtMailing4";
			this.txtMailing4.Tag = "textbox";
			this.txtMailing4.Text = "Field1";
			this.txtMailing4.Top = 3.25F;
			this.txtMailing4.Width = 3.625F;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 1.0625F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 2.46875F;
			this.txtAccount.Width = 1.5625F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 5.25F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 0.75F;
			this.txtLand.Width = 1.4375F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 5.25F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0.90625F;
			this.txtBldg.Width = 1.4375F;
			// 
			// txtCat1
			// 
			this.txtCat1.CanGrow = false;
			this.txtCat1.Height = 0.19F;
			this.txtCat1.Left = 5.25F;
			this.txtCat1.MultiLine = false;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.Style = "text-align: right";
			this.txtCat1.Tag = "textbox";
			this.txtCat1.Text = null;
			this.txtCat1.Top = 1.0625F;
			this.txtCat1.Width = 1.4375F;
			// 
			// txtCat2
			// 
			this.txtCat2.CanGrow = false;
			this.txtCat2.Height = 0.19F;
			this.txtCat2.Left = 5.25F;
			this.txtCat2.MultiLine = false;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.Style = "text-align: right";
			this.txtCat2.Tag = "textbox";
			this.txtCat2.Text = null;
			this.txtCat2.Top = 1.21875F;
			this.txtCat2.Width = 1.4375F;
			// 
			// txtCatOther
			// 
			this.txtCatOther.CanGrow = false;
			this.txtCatOther.Height = 0.19F;
			this.txtCatOther.Left = 5.25F;
			this.txtCatOther.MultiLine = false;
			this.txtCatOther.Name = "txtCatOther";
			this.txtCatOther.Style = "text-align: right";
			this.txtCatOther.Tag = "textbox";
			this.txtCatOther.Text = null;
			this.txtCatOther.Top = 1.375F;
			this.txtCatOther.Width = 1.4375F;
			// 
			// txtOtherLabel
			// 
			this.txtOtherLabel.CanGrow = false;
			this.txtOtherLabel.Height = 0.19F;
			this.txtOtherLabel.Left = 4.375F;
			this.txtOtherLabel.MultiLine = false;
			this.txtOtherLabel.Name = "txtOtherLabel";
			this.txtOtherLabel.Tag = "textbox";
			this.txtOtherLabel.Text = "Other";
			this.txtOtherLabel.Top = 1.375F;
			this.txtOtherLabel.Width = 0.75F;
			// 
			// txtExemption
			// 
			this.txtExemption.CanGrow = false;
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 5.25F;
			this.txtExemption.MultiLine = false;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Tag = "textbox";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 1.53125F;
			this.txtExemption.Width = 1.4375F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.CanGrow = false;
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 5.25F;
			this.txtAssessment.MultiLine = false;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Tag = "textbox";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 2F;
			this.txtAssessment.Width = 1.4375F;
			// 
			// txtRate
			// 
			this.txtRate.CanGrow = false;
			this.txtRate.Height = 0.19F;
			this.txtRate.Left = 5.25F;
			this.txtRate.MultiLine = false;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "text-align: right";
			this.txtRate.Tag = "textbox";
			this.txtRate.Text = null;
			this.txtRate.Top = 2.46875F;
			this.txtRate.Width = 1.4375F;
			// 
			// txtTaxDue
			// 
			this.txtTaxDue.CanGrow = false;
			this.txtTaxDue.Height = 0.19F;
			this.txtTaxDue.Left = 5.25F;
			this.txtTaxDue.MultiLine = false;
			this.txtTaxDue.Name = "txtTaxDue";
			this.txtTaxDue.Style = "text-align: right";
			this.txtTaxDue.Tag = "textbox";
			this.txtTaxDue.Text = null;
			this.txtTaxDue.Top = 2.78125F;
			this.txtTaxDue.Width = 1.4375F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 4.625F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 3.25F;
			this.txtMapLot.Width = 0.875F;
			// 
			// txtLot
			// 
			this.txtLot.CanGrow = false;
			this.txtLot.Height = 0.19F;
			this.txtLot.Left = 5.8125F;
			this.txtLot.MultiLine = false;
			this.txtLot.Name = "txtLot";
			this.txtLot.Tag = "textbox";
			this.txtLot.Text = null;
			this.txtLot.Top = 3.25F;
			this.txtLot.Width = 1.125F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.CanGrow = false;
			this.txtBookPage.Height = 0.19F;
			this.txtBookPage.Left = 4.625F;
			this.txtBookPage.MultiLine = false;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 3.40625F;
			this.txtBookPage.Width = 0.875F;
			// 
			// txtPage
			// 
			this.txtPage.CanGrow = false;
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 5.8125F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Tag = "textbox";
			this.txtPage.Text = null;
			this.txtPage.Top = 3.40625F;
			this.txtPage.Width = 0.875F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 7.625F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 3.40625F;
			this.txtLocation.Width = 1.8125F;
			// 
			// txtMessage1
			// 
			this.txtMessage1.CanGrow = false;
			this.txtMessage1.Height = 0.19F;
			this.txtMessage1.Left = 0.25F;
			this.txtMessage1.MultiLine = false;
			this.txtMessage1.Name = "txtMessage1";
			this.txtMessage1.Tag = "textbox";
			this.txtMessage1.Text = null;
			this.txtMessage1.Top = 4.03125F;
			this.txtMessage1.Width = 8.875F;
			// 
			// txtMessage4
			// 
			this.txtMessage4.CanGrow = false;
			this.txtMessage4.Height = 0.19F;
			this.txtMessage4.Left = 0.25F;
			this.txtMessage4.MultiLine = false;
			this.txtMessage4.Name = "txtMessage4";
			this.txtMessage4.Tag = "textbox";
			this.txtMessage4.Text = null;
			this.txtMessage4.Top = 4.5F;
			this.txtMessage4.Width = 8.875F;
			// 
			// txtMessage5
			// 
			this.txtMessage5.CanGrow = false;
			this.txtMessage5.Height = 0.19F;
			this.txtMessage5.Left = 0.25F;
			this.txtMessage5.MultiLine = false;
			this.txtMessage5.Name = "txtMessage5";
			this.txtMessage5.Tag = "textbox";
			this.txtMessage5.Text = null;
			this.txtMessage5.Top = 4.65625F;
			this.txtMessage5.Width = 8.875F;
			// 
			// txtMessage2
			// 
			this.txtMessage2.CanGrow = false;
			this.txtMessage2.Height = 0.19F;
			this.txtMessage2.Left = 0.25F;
			this.txtMessage2.MultiLine = false;
			this.txtMessage2.Name = "txtMessage2";
			this.txtMessage2.Tag = "textbox";
			this.txtMessage2.Text = null;
			this.txtMessage2.Top = 4.1875F;
			this.txtMessage2.Width = 8.875F;
			// 
			// txtMessage3
			// 
			this.txtMessage3.CanGrow = false;
			this.txtMessage3.Height = 0.19F;
			this.txtMessage3.Left = 0.25F;
			this.txtMessage3.MultiLine = false;
			this.txtMessage3.Name = "txtMessage3";
			this.txtMessage3.Tag = "textbox";
			this.txtMessage3.Text = null;
			this.txtMessage3.Top = 4.34375F;
			this.txtMessage3.Width = 8.875F;
			// 
			// txtAmount1
			// 
			this.txtAmount1.CanGrow = false;
			this.txtAmount1.Height = 0.19F;
			this.txtAmount1.Left = 8.0625F;
			this.txtAmount1.MultiLine = false;
			this.txtAmount1.Name = "txtAmount1";
			this.txtAmount1.Style = "text-align: left";
			this.txtAmount1.Tag = "textbox";
			this.txtAmount1.Text = null;
			this.txtAmount1.Top = 2.9375F;
			this.txtAmount1.Width = 1.3125F;
			// 
			// txtAcct1
			// 
			this.txtAcct1.CanGrow = false;
			this.txtAcct1.Height = 0.19F;
			this.txtAcct1.Left = 8.125F;
			this.txtAcct1.MultiLine = false;
			this.txtAcct1.Name = "txtAcct1";
			this.txtAcct1.Tag = "textbox";
			this.txtAcct1.Text = null;
			this.txtAcct1.Top = 1.0625F;
			this.txtAcct1.Width = 1.25F;
			// 
			// txtInterest1
			// 
			this.txtInterest1.CanGrow = false;
			this.txtInterest1.Height = 0.19F;
			this.txtInterest1.Left = 8.0625F;
			this.txtInterest1.MultiLine = false;
			this.txtInterest1.Name = "txtInterest1";
			this.txtInterest1.Style = "text-align: left";
			this.txtInterest1.Tag = "textbox";
			this.txtInterest1.Text = null;
			this.txtInterest1.Top = 2.46875F;
			this.txtInterest1.Width = 1.3125F;
			// 
			// txtAmount2
			// 
			this.txtAmount2.CanGrow = false;
			this.txtAmount2.Height = 0.19F;
			this.txtAmount2.Left = 6.8125F;
			this.txtAmount2.MultiLine = false;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Style = "text-align: left";
			this.txtAmount2.Tag = "textbox";
			this.txtAmount2.Text = null;
			this.txtAmount2.Top = 2.9375F;
			this.txtAmount2.Width = 1.1875F;
			// 
			// txtAcct2
			// 
			this.txtAcct2.CanGrow = false;
			this.txtAcct2.Height = 0.19F;
			this.txtAcct2.Left = 6.8125F;
			this.txtAcct2.MultiLine = false;
			this.txtAcct2.Name = "txtAcct2";
			this.txtAcct2.Tag = "textbox";
			this.txtAcct2.Text = null;
			this.txtAcct2.Top = 1.0625F;
			this.txtAcct2.Width = 1.1875F;
			// 
			// txtInterest2
			// 
			this.txtInterest2.CanGrow = false;
			this.txtInterest2.Height = 0.19F;
			this.txtInterest2.Left = 6.8125F;
			this.txtInterest2.MultiLine = false;
			this.txtInterest2.Name = "txtInterest2";
			this.txtInterest2.Style = "text-align: left";
			this.txtInterest2.Tag = "textbox";
			this.txtInterest2.Text = null;
			this.txtInterest2.Top = 2.46875F;
			this.txtInterest2.Width = 1.1875F;
			// 
			// txtPrePaidLabel
			// 
			this.txtPrePaidLabel.CanGrow = false;
			this.txtPrePaidLabel.Height = 0.19F;
			this.txtPrePaidLabel.Left = 4.625F;
			this.txtPrePaidLabel.MultiLine = false;
			this.txtPrePaidLabel.Name = "txtPrePaidLabel";
			this.txtPrePaidLabel.Tag = "textbox";
			this.txtPrePaidLabel.Text = "Paid to Date";
			this.txtPrePaidLabel.Top = 3.09375F;
			this.txtPrePaidLabel.Width = 1.25F;
			// 
			// txtPrePaid
			// 
			this.txtPrePaid.CanGrow = false;
			this.txtPrePaid.Height = 0.19F;
			this.txtPrePaid.Left = 6.0625F;
			this.txtPrePaid.MultiLine = false;
			this.txtPrePaid.Name = "txtPrePaid";
			this.txtPrePaid.Tag = "textbox";
			this.txtPrePaid.Text = null;
			this.txtPrePaid.Top = 3.09375F;
			this.txtPrePaid.Width = 1.5625F;
			// 
			// txtYear1
			// 
			this.txtYear1.CanGrow = false;
			this.txtYear1.Height = 0.19F;
			this.txtYear1.Left = 0.9375F;
			this.txtYear1.MultiLine = false;
			this.txtYear1.Name = "txtYear1";
			this.txtYear1.Tag = "textbox";
			this.txtYear1.Text = null;
			this.txtYear1.Top = 1.21875F;
			this.txtYear1.Width = 0.4375F;
			// 
			// txtYear2
			// 
			this.txtYear2.CanGrow = false;
			this.txtYear2.Height = 0.19F;
			this.txtYear2.Left = 2.9375F;
			this.txtYear2.MultiLine = false;
			this.txtYear2.Name = "txtYear2";
			this.txtYear2.Tag = "textbox";
			this.txtYear2.Text = null;
			this.txtYear2.Top = 1.21875F;
			this.txtYear2.Width = 0.5F;
			// 
			// txtMailing5
			// 
			this.txtMailing5.CanGrow = false;
			this.txtMailing5.Height = 0.19F;
			this.txtMailing5.Left = 0.25F;
			this.txtMailing5.MultiLine = false;
			this.txtMailing5.Name = "txtMailing5";
			this.txtMailing5.Tag = "textbox";
			this.txtMailing5.Text = "Field1";
			this.txtMailing5.Top = 3.40625F;
			this.txtMailing5.Width = 3.625F;
			// 
			// rptBillFormatC
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.4375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterest1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterest2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInterest1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInterest2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaidLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing5;
	}
}
