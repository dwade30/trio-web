﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptMortgageHolderMaster.
	/// </summary>
	public partial class rptMortgageHolderMaster : BaseSectionReport
	{
		public static rptMortgageHolderMaster InstancePtr
		{
			get
			{
				return (rptMortgageHolderMaster)Sys.GetInstance(typeof(rptMortgageHolderMaster));
			}
		}

		protected rptMortgageHolderMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsMortgage.Dispose();
            }
			base.Dispose(disposing);
		}

		public rptMortgageHolderMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
			{
				_InstancePtr = this;
			}
			//FC:FINAL:RPU: #i1570 - Add report title
			this.Name = "Mortgage Holder List";
		}
		// nObj = 1
		//   0	rptMortgageHolderMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsMortgage = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsMortgage.EndOfFile();
		}

		public void Init(bool boolNameOrder, string strStart = "", string strEnd = "")
		{
			string strWhere = "";
			if (boolNameOrder)
			{
				if (strEnd == string.Empty)
				{
					if (strStart == string.Empty)
					{
						strWhere = "";
					}
					else
					{
						strWhere = " where name >= '" + strStart + "' ";
					}
				}
				else
				{
					strWhere = " where name between '" + strStart + "' and '" + strEnd + "' ";
				}
				clsMortgage.OpenRecordset("select * from mortgageholders " + strWhere + " order by name", "CentralData");
			}
			else
			{
				if (strEnd == string.Empty)
				{
					if (strStart == string.Empty)
					{
						strWhere = "";
					}
					else
					{
						strWhere = " where ID >= " + FCConvert.ToString(Conversion.Val(strStart)) + " ";
					}
				}
				else
				{
					strWhere = " where ID between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
				}
				clsMortgage.OpenRecordset("select * from mortgageholders " + strWhere + " order by ID", "CentralData");
			}
			//FC:FINAL:CHN - i.issue #1544: error when printing errors.
			frmReportViewer.InstancePtr.Init(this, strAttachmentName: "MortgageHolder");
			// frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "MortgageHolder");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int intNumLines = 0;
			int GridWidth = 0;
			int intRow = 0;
			if (!clsMortgage.EndOfFile())
			{
				intNumLines = 0;
				// find out how big to make the grid
				if (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("address1"))) != string.Empty)
					intNumLines += 1;
				if (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("address2"))) != string.Empty)
					intNumLines += 1;
				if (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("address3"))) != string.Empty)
					intNumLines += 1;
				if (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("city"))) != string.Empty || (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("state"))) != string.Empty))
					intNumLines += 1;
				// If Trim(clsMortgage.Fields("address4")) <> vbNullString Then intNumLines = intNumLines + 1
				if (intNumLines < 1)
				{
					intNumLines = 1;
				}
				if (intNumLines == 1 || intNumLines == 2)
				{
					textBoxDetail33.Visible = false;
					textBoxDetail43.Visible = false;
				}
				else if (intNumLines == 3)
				{
					textBoxDetail33.Visible = true;
					textBoxDetail43.Visible = false;
				}
				else
				{
					textBoxDetail33.Visible = true;
					textBoxDetail43.Visible = true;
				}
				//FC:FINAL:AM:#i1570 - use textboxes instead of the grid
				// setup the grid
				//GridWidth = FCConvert.ToInt32(DetailGrid.Width;
				//DetailGrid.Row = 1;
				//DetailGrid.Rows = intNumLines + 1;
				//DetailGrid.Left = 0;
				//DetailGrid.Cols = 3;
				//// size the grid
				//DetailGrid.Height = DetailGrid.Rows * DetailGrid.RowHeight(0);
				//// now size the columns
				//DetailGrid.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth)); // number
				//DetailGrid.ColWidth(1, FCConvert.ToInt32(0.4 * GridWidth)); // name
				//DetailGrid.ColWidth(2, FCConvert.ToInt32(0.45 * GridWidth)); // address
				//                                                           // now fill in the grid
				//DetailGrid.TextMatrix(0, 0, clsMortgage.Get_Fields_Int32("ID"));
				//DetailGrid.TextMatrix(0, 1, clsMortgage.Get_Fields_String("name"));
				textBoxDetail11.Text = clsMortgage.Get_Fields_Int32("ID").ToString();
				//FC:FINAL:MSH - change alignment of the text, because in original for displaying data is used Grid, which automatically aligns all numeric 
				// data to the right side
				textBoxDetail11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				textBoxDetail12.Text = clsMortgage.Get_Fields_String("name");
				intRow = 0;
				if (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("address1"))) != string.Empty)
				{
					//DetailGrid.TextMatrix(intRow, 2, clsMortgage.Get_Fields_String("address1"));
					(this.Detail.Controls[String.Format("textBoxDetail{0}3", intRow + 1)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = clsMortgage.Get_Fields_String("address1");
					intRow += 1;
				}
				if (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("address2"))) != string.Empty)
				{
					//DetailGrid.TextMatrix(intRow, 2, clsMortgage.Get_Fields_String("address2"));
					(this.Detail.Controls[String.Format("textBoxDetail{0}3", intRow + 1)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = clsMortgage.Get_Fields_String("address2");
					intRow += 1;
				}
				if (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("address3"))) != string.Empty)
				{
					//DetailGrid.TextMatrix(intRow, 2, Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("address3"))));
					(this.Detail.Controls[String.Format("textBoxDetail{0}3", intRow + 1)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = clsMortgage.Get_Fields_String("address3");
					intRow += 1;
				}
				if (Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("city"))) != string.Empty || Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("state"))) != string.Empty)
				{
					//DetailGrid.TextMatrix(intRow, 2, Strings.Trim(clsMortgage.Get_Fields_String("city") + " " + Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("state"))) + " " + clsMortgage.Get_Fields_String("zip") + " " + clsMortgage.Get_Fields_String("zip4")));
					(this.Detail.Controls[String.Format("textBoxDetail{0}3", intRow + 1)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Trim(clsMortgage.Get_Fields_String("city") + " " + Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("state"))) + " " + clsMortgage.Get_Fields_String("zip") + " " + clsMortgage.Get_Fields_String("zip4"));
				}
				//FC:FINAL:CHN - issue #1372: Set Detail Height according to original Grid (missing margin after redesign).
				this.Detail.Height = 0.2f * (intNumLines + 1);
				clsMortgage.MoveNext();
			}
		}

		private void rptMortgageHolderMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMortgageHolderMaster properties;
			//rptMortgageHolderMaster.Caption	= "Mortgage Holder List";
			//rptMortgageHolderMaster.Icon	= "rptMortgageHolderMaster.dsx":0000";
			//rptMortgageHolderMaster.Left	= 0;
			//rptMortgageHolderMaster.Top	= 0;
			//rptMortgageHolderMaster.Width	= 15240;
			//rptMortgageHolderMaster.Height	= 11115;
			//rptMortgageHolderMaster.StartUpPosition	= 3;
			//rptMortgageHolderMaster.SectionData	= "rptMortgageHolderMaster.dsx":058A;
			//End Unmaped Properties
		}
	}
}
