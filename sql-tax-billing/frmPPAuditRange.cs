﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPPAuditRange.
	/// </summary>
	public partial class frmPPAuditRange : BaseForm
	{
		public frmPPAuditRange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPAuditRange InstancePtr
		{
			get
			{
				return (frmPPAuditRange)Sys.GetInstance(typeof(frmPPAuditRange));
			}
		}

		protected frmPPAuditRange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public bool boolFromSupplemental;

		private void frmPPAuditRange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuCancel_Click();
			}
		}

		private void frmPPAuditRange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPAuditRange properties;
			//frmPPAuditRange.ScaleWidth	= 5880;
			//frmPPAuditRange.ScaleHeight	= 4260;
			//frmPPAuditRange.LinkTopic	= "Form1";
			//frmPPAuditRange.LockControls	= true;
			//End Unmaped Properties
			clsDRWrapper clsTemp = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			lblTo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			clsTemp.OpenRecordset("select * from PPRATIOOPENS", "twpp0000.vb1");
			// FC:FINAL:CHN - issue #1282: Incorrect indexes at insert items to listbox. 
			// if (cmbRange.Items.Contains("Open1"))
			// {
			// 	cmbRange.Items.Remove("Open1");
			//  cmbRange.Items.Insert(3, Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield1"))));
			// }
			// if (cmbRange.Items.Contains("Open2"))
			// {
			// 	cmbRange.Items.Remove("Open2");
			// 	cmbRange.Items.Add(Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield2"))));
			// }
			cmbRange.Items[3] = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield1")));
			cmbRange.Items[4] = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield2")));
			modGlobalVariables.Statics.CancelledIt = false;
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			Close();
		}

		public void mnuCancel_Click()
		{
			mnuCancel_Click(mnuCancel, new System.EventArgs());
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			string strBegin = "";
			string strEnd = "";
			int intWhich = 0;
			if (!boolFromSupplemental)
			{
				if (cmbDisplay.SelectedIndex == 0)
				{
					frmPPAudit.InstancePtr.boolDisplayFirst = true;
					frmPPAudit.InstancePtr.strSequence = "NAME";
				}
				else
				{
					frmPPAudit.InstancePtr.boolDisplayFirst = false;
					if (cmbSequence.SelectedIndex == 0)
					{
						frmPPAudit.InstancePtr.strSequence = "NAME";
					}
					else if (cmbSequence.SelectedIndex == 1)
					{
						frmPPAudit.InstancePtr.strSequence = "ACCOUNT";
					}
					else if (cmbSequence.SelectedIndex == 2)
					{
						frmPPAudit.InstancePtr.strSequence = "TAX";
					}
					else
					{
						frmPPAudit.InstancePtr.strSequence = "LOCATION";
					}
				}
				if (cmbAccounts.SelectedIndex == 0)
				{
					frmPPAudit.InstancePtr.intWhich = -1;
				}
				else
				{
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						MessageBox.Show("You must specify a start range", "Specify Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						frmPPAudit.InstancePtr.intWhich = 0;
						if (Conversion.Val(txtStart.Text) > 0)
						{
							frmPPAudit.InstancePtr.strBegin = txtStart.Text;
							if (Conversion.Val(txtEnd.Text) > 0)
							{
								frmPPAudit.InstancePtr.strEnd = txtEnd.Text;
							}
							else
							{
								frmPPAudit.InstancePtr.strEnd = txtStart.Text;
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						if (cmbRange.SelectedIndex == 1)
						{
							frmPPAudit.InstancePtr.intWhich = 1;
						}
						else if (cmbRange.SelectedIndex == 2)
						{
							frmPPAudit.InstancePtr.intWhich = 2;
						}
						else if (cmbRange.SelectedIndex == 3)
						{
							frmPPAudit.InstancePtr.intWhich = 3;
						}
						else if (cmbRange.SelectedIndex == 4)
						{
							frmPPAudit.InstancePtr.intWhich = 4;
						}
						frmPPAudit.InstancePtr.strBegin = txtStart.Text;
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							frmPPAudit.InstancePtr.strEnd = txtStart.Text;
						}
						else
						{
							frmPPAudit.InstancePtr.strEnd = txtEnd.Text;
						}
					}
				}
			}
			else
			{
				// from supplemental
				if (cmbAccounts.SelectedIndex == 0)
				{
					// all
					frmSupplementalBill.InstancePtr.TransferPP(-1);
				}
				else
				{
					// range
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						MessageBox.Show("You must specify a start range", "Specify Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						if (Conversion.Val(txtStart.Text) > 0)
						{
							strBegin = txtStart.Text;
							if (Conversion.Val(txtEnd.Text) > 0)
							{
								strEnd = txtEnd.Text;
							}
							else
							{
								strEnd = txtStart.Text;
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							MessageBox.Show("You must specify an end range", "Specify End", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						strBegin = txtStart.Text;
						strEnd = txtEnd.Text;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						// account
						intWhich = 0;
					}
					else if (cmbRange.SelectedIndex == 1)
					{
						// name
						intWhich = 1;
					}
					else if (cmbRange.SelectedIndex == 2)
					{
						// location
						intWhich = 2;
					}
					else if (cmbRange.SelectedIndex == 3)
					{
						// open1
						intWhich = 3;
					}
					else if (cmbRange.SelectedIndex == 4)
					{
						// open2
						intWhich = 4;
					}
					frmSupplementalBill.InstancePtr.TransferPP(intWhich, strBegin, strEnd);
				}
			}
			Close();
		}

		private void optAccounts_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						lblTo.Visible = false;
						txtStart.Visible = false;
						txtEnd.Visible = false;
						cmbRange.Visible = false;
						//FC:FINAL:MSH - issue #1318: change visibility of the label, too
						lblRange.Visible = false;
						break;
					}
				case 1:
					{
						lblTo.Visible = true;
						txtStart.Visible = true;
						txtEnd.Visible = true;
						cmbRange.Visible = true;
						//FC:FINAL:MSH - issue #1318: change visibility of the label, too
						lblRange.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbAccounts.SelectedIndex);
			optAccounts_CheckedChanged(index, sender, e);
		}

		private void optDisplay_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				cmbSequence.Visible = false;
				//FC:FINAL:MSH - issue #1318: change visibility of the label, too
				lblSequence.Visible = false;
			}
			else
			{
				cmbSequence.Visible = true;
				//FC:FINAL:MSH - issue #1318: change visibility of the label, too
				lblSequence.Visible = true;
			}
		}

		private void optDisplay_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbDisplay.SelectedIndex);
			optDisplay_CheckedChanged(index, sender, e);
		}

		private void cmbAccounts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - issue #1318: wrong comparing(won't work correctly with a different number of elements in list)
			//if (cmbAccounts.SelectedIndex == 0)
			//{
			//	optAccounts_CheckedChanged(sender, e);
			//}
			//else if (cmbAccounts.SelectedIndex == 1)
			//{
			//	optAccounts_CheckedChanged(sender, e);
			//}
			if (cmbAccounts.SelectedIndex > -1)
			{
				optAccounts_CheckedChanged(sender, e);
			}
		}

		private void cmbDisplay_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - issue #1318: wrong comparing(won't work correctly with a different number of elements in list)
			//if (cmbDisplay.SelectedIndex == 0)
			if (cmbDisplay.SelectedIndex > -1)
			{
				optDisplay_CheckedChanged(sender, e);
			}
		}
	}
}
