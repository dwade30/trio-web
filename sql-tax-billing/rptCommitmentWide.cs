﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptCommitmentWide.
	/// </summary>
	public partial class rptCommitmentWide : BaseSectionReport
	{
		public rptCommitmentWide()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Commitment Book";
		}

		public static rptCommitmentWide InstancePtr
		{
			get
			{
				return (rptCommitmentWide)Sys.GetInstance(typeof(rptCommitmentWide));
			}
		}

		protected rptCommitmentWide _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCommit.Dispose();
				clsRef1.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCommitmentWide	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int TaxYear;
		clsDRWrapper clsCommit = new clsDRWrapper();
		string Addr1 = "";
		string Addr2 = "";
		string addr3 = "";
		private string strMailingAddress3 = "";
		string strField10 = "";
		int intPage;
		int intStartPage;
		clsExempt[] exemptarray = null;
		double lngLandSub;
		double lngBldgSub;
		double lngExemptsub;
		double lngAssessSub;
		double lngTaxSub;
		clsDRWrapper clsRef1 = new clsDRWrapper();
		bool boolFirstRec;
		bool boolLandscape;
		int lngTotalCount;
		const int CNSTACCOUNTORDER = 1;
		const int CNSTNAMEORDER = 2;
		const int CNSTMAPLOTORDER = 3;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                // vbPorter upgrade warning: x As short --> As int	OnWrite(short, double)
                int x;
                lngTotalCount = 0;
                lngLandSub = 0;
                lngBldgSub = 0;
                lngExemptsub = 0;
                lngAssessSub = 0;
                lngTaxSub = 0;
                // add these fields so the report can do the subtotals for us
                Fields.Add("landval");
                Fields.Add("bldgval");
                Fields.Add("exemptval");
                Fields.Add("Totalval");
                Fields.Add("TaxVal");
                this.Fields.Add("TheBinder");
                txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                // open the costrecord table and get all the exemption records
                // Set up an array of exemption records so we don't hit the database for every bill record
                clsTemp.OpenRecordset("select max(code) as maxcode from exemptcode", "twre0000.vb1");
                // TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
                exemptarray = new clsExempt[FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxcode"))) + 1];
                // TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
                for (x = 0; x <= FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxcode"))); x++)
                {
                    exemptarray[x] = new clsExempt();
                    exemptarray[x].Description = "Invalid Code";
                    exemptarray[x].ShortDescription = "Bad Code";
                    exemptarray[x].Unit = 0;
                }

                // x
                clsTemp.OpenRecordset("select * from exemptcode WHERE CODE > 0 ORDER BY CODE", "twre0000.vb1");
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    x = FCConvert.ToInt32(clsTemp.Get_Fields("code"));
                    exemptarray[x].Description =
                        Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    exemptarray[x].ShortDescription =
                        Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("shortdescription")));
                    // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                    //FC:FINAL:CHN - issue #1328: Incorrect types casting. 
                    // exemptarray[x].Unit = Conversion.Val(clsTemp.Get_Fields("amount")));
                    exemptarray[x].Unit = FCConvert.ToInt32(clsTemp.Get_Fields_Double("amount"));
                    clsTemp.MoveNext();
                }
            }
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// if no more records, then end the report
			eArgs.EOF = clsCommit.EndOfFile();
			if (!eArgs.EOF)
			{
				FillValueFields();
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			boolFirstRec = true;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			int intTemp = 0;
			bool boolAlreadyFound;
			string strTemp = "";
			// fill in all the fields
			if (!clsCommit.EndOfFile())
			{
				// If boolFirstRec Then
				// Line1.Visible = False
				// boolFirstRec = False
				// Else
				// Line1.Visible = True
				// End If
				// boolAlreadyFound = False
				// If Not clsRef1.EndOfFile Then
				// If clsRef1.Fields("rsaccount") = clsCommit.Fields("account") Then boolAlreadyFound = True
				// End If
				// 
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = clsCommit.Get_Fields_String("account");
				txtName.Text = clsCommit.Get_Fields_String("name1");
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				if (Conversion.Val(clsCommit.Get_Fields("streetnumber")) > 0)
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtLocation.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
				}
				else
				{
					txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("apt"))) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
				}
				txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("maplot")));
				Addr1 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name2")));
				Addr2 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address1")));
				addr3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address2")));
				strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("MailingAddress3")));
				strField10 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address3")));
				if (Addr1 == string.Empty && Addr2 == string.Empty && addr3 == string.Empty && strField10 == string.Empty)
				{
					// If Not boolAlreadyFound Then
					// If clsRef1.FindFirstRecord("rsaccount", clsCommit.Fields("account")) Then
					// boolAlreadyFound = True
					// End If
					// End If
					clsRef1.OpenRecordset("select rsaccount,p.FullNameLF as OwnerFullNameLF, q.FullNameLF as SecOwnerFullNameLF ,rsref1,Address1,Address2,Address3, City,PartyState,Zip,RSLOCnumalph,rslocstreet,rsmaplot from master as c CROSS APPLY " + clsRef1.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.OwnerPartyID,null,null,null) as p CROSS APPLY " + clsRef1.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.SecOwnerPartyID) as q where rsdeleted <> 1 and rscard = 1 order by p.FullNameLF", "twre0000.vb1");
					if (!clsRef1.EndOfFile())
					{
						// TODO Get_Fields: Field [SecOwnerFullNameLF] not found!! (maybe it is an alias?)
						Addr1 = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields("SecOwnerFullNameLF")));
						Addr2 = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("Address1")));
						addr3 = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("Address2")));
						strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("Address3")));
						// TODO Get_Fields: Field [PartyState] not found!! (maybe it is an alias?)
						strField10 = Strings.Trim(Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("City"))) + " " + Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields("PartyState"))) + " " + Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("Zip"))));
						txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("rslocnumalph"))) + " " + Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("rslocstreet"))));
						txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("rsmaplot")));
					}
				}
				// if there are blank address lines condense them
				if (Strings.Trim(Addr1) == string.Empty)
				{
					Addr1 = Addr2;
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = strField10;
					strField10 = "";
					if (Strings.Trim(Addr1) == string.Empty)
					{
						Addr1 = Addr2;
						Addr2 = strMailingAddress3;
						strMailingAddress3 = addr3;
						addr3 = "";
					}
				}
				if (Strings.Trim(Addr2) == string.Empty)
				{
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = strField10;
					strField10 = "";
					if (Strings.Trim(Addr2) == string.Empty)
					{
						Addr2 = addr3;
						addr3 = "";
					}
				}
				if (Strings.Trim(strMailingAddress3) == string.Empty)
				{
					strMailingAddress3 = addr3;
					addr3 = strField10;
					strField10 = "";
				}
				if (Strings.Trim(addr3) == string.Empty)
				{
					addr3 = strField10;
					strField10 = "";
				}
				txtAddress1.Text = Addr1;
				txtAddress2.Text = Addr2;
				txtAddress3.Text = addr3;
				txtMailingAddress3.Text = strMailingAddress3;
				Field10.Text = strField10;
				txtBookPage.Text = "";
				if (Strings.Trim(clsCommit.Get_Fields_String("bookpage")) != string.Empty)
				{
					txtBookPage.Text = Strings.Trim(clsCommit.Get_Fields_String("bookpage"));
					intTemp = Strings.InStr(1, txtBookPage.Text, Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"), CompareConstants.vbTextCompare);
					if (intTemp > 0)
					{
						txtBookPage.Text = Strings.Mid(txtBookPage.Text, 1, intTemp - 1);
					}
					// Else
					// If Not clsRef1.EndOfFile Then
					// If Not boolAlreadyFound Then
					// If clsRef1.FindNextRecord("rsaccount", clsCommit.Fields("account")) Then
					// txtBookPage.Text = Mid(Trim(clsRef1.Fields("rsref1")) & " ", 1, 26)
					// boolAlreadyFound = True
					// End If
					// Else
					// txtBookPage.Text = Mid(Trim(clsRef1.Fields("rsref1")) & " ", 1, 26)
					// End If
					// End If
				}
				// txtBookPage.Text = ""
				// If Trim(clsCommit.Fields("bookpage")) <> vbNullString Then
				// txtBookPage.Text = Trim(clsCommit.Fields("bookpage"))
				// intTemp = InStr(1, txtBookPage.Text, "12:00:00 AM", vbTextCompare)
				// If intTemp > 0 Then
				// txtBookPage.Text = Mid(txtBookPage.Text, 1, intTemp - 1)
				// End If
				// 
				// Else
				// If Not clsRef1.EndOfFile Then
				// If clsRef1.FindNextRecord("rsaccount", clsCommit.Fields("account")) Then
				// txtBookPage.Text = Mid(Trim(clsRef1.Fields("rsref1")) & " ", 1, 26)
				// End If
				// End If
				// End If
				txtAcres.Text = "";
				if (Conversion.Val(clsCommit.Get_Fields_Double("acres")) > 0)
				{
					// txtAcres.Text = Val(clsCommit.Fields("acres")) & " Acres"
					txtAcres.Text = Strings.Format(Conversion.Val(clsCommit.Get_Fields_Double("acres")), "0.00");
					lblAcres.Visible = true;
					if (modGlobalVariables.Statics.CustomizedInfo.ShowTreeGrowth)
					{
						lblSoft.Visible = true;
						lblMixed.Visible = true;
						lblHard.Visible = true;
						txtSoft.Visible = true;
						txtMixed.Visible = true;
						txtHard.Visible = true;
						txtSoftValue.Visible = true;
						txtMixedValue.Visible = true;
						txtHardValue.Visible = true;
						txtSoft.Text = Strings.Format(Conversion.Val(clsCommit.Get_Fields_Double("TGSoftAcres")), "0.00");
						txtMixed.Text = Strings.Format(Conversion.Val(clsCommit.Get_Fields_Double("TGMixedAcres")), "0.00");
						txtHard.Text = Strings.Format(Conversion.Val(clsCommit.Get_Fields_Double("TGHardAcres")), "0.00");
						txtSoftValue.Text = Strings.Format(Conversion.Val(clsCommit.Get_Fields_Int32("TGSoftValue")), "#,###,##0");
						txtMixedValue.Text = Strings.Format(Conversion.Val(clsCommit.Get_Fields_Int32("TGMixedValue")), "#,###,##0");
						txtHardValue.Text = Strings.Format(Conversion.Val(clsCommit.Get_Fields_Int32("TGHardValue")), "#,###,##0");
					}
					else
					{
						lblSoft.Visible = false;
						lblMixed.Visible = false;
						lblHard.Visible = false;
						txtSoft.Visible = false;
						txtMixed.Visible = false;
						txtHard.Visible = false;
						txtSoftValue.Visible = false;
						txtMixedValue.Visible = false;
						txtHardValue.Visible = false;
					}
				}
				else
				{
					lblAcres.Visible = false;
					lblSoft.Visible = false;
					lblMixed.Visible = false;
					lblHard.Visible = false;
					txtSoft.Visible = false;
					txtMixed.Visible = false;
					txtHard.Visible = false;
					txtSoftValue.Visible = false;
					txtMixedValue.Visible = false;
					txtHardValue.Visible = false;
				}
				txtRef1.Text = "";
				if (modGlobalVariables.Statics.CustomizedInfo.ShowRef1)
				{
					txtRef1.Visible = true;
					txtRef1.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("ref1")));
				}
				else
				{
					txtRef1.Visible = false;
				}
				txtRef2.Text = "";
				if (modGlobalVariables.Statics.CustomizedInfo.ShowRef2)
				{
					txtRef2.Visible = true;
					txtRef2.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("ref2")));
				}
				else
				{
					txtRef2.Visible = false;
				}
				// zero out the exemption fields and then fill them if there are exemptions
				txtExempt1.Text = "";
				txtExempt2.Text = "";
				txtExempt3.Text = "";
				if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt1")) > 0)
				{
					strTemp = Strings.Format(clsCommit.Get_Fields_Int32("exempt1"), "00") + " ";
					if (Information.UBound(exemptarray, 1) >= Conversion.Val(clsCommit.Get_Fields_Int32("exempt1")))
					{
						strTemp += exemptarray[clsCommit.Get_Fields_Int32("exempt1")].Description;
					}
					else
					{
						strTemp += exemptarray[0].Description;
					}
					txtExempt1.Text = strTemp;
				}
				// If exemptarray(clsCommit.Fields("exempt1")).Unit = 0 Then
				// Else
				if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt2")) > 0)
				{
					strTemp = Strings.Format(clsCommit.Get_Fields_Int32("exempt2"), "00") + " ";
					if (Information.UBound(exemptarray, 1) >= Conversion.Val(clsCommit.Get_Fields_Int32("exempt2")))
					{
						strTemp += exemptarray[clsCommit.Get_Fields_Int32("exempt2")].Description;
					}
					else
					{
						strTemp += exemptarray[0].Description;
					}
					txtExempt2.Text = strTemp;
				}
				// End If
				if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt3")) > 0)
				{
					strTemp = Strings.Format(clsCommit.Get_Fields_Int32("exempt3"), "00") + " ";
					if (Information.UBound(exemptarray, 1) >= Conversion.Val(clsCommit.Get_Fields_Int32("exempt2")))
					{
						strTemp += exemptarray[clsCommit.Get_Fields_Int32("exempt3")].Description;
					}
					else
					{
						strTemp += exemptarray[0];
					}
					txtExempt3.Text = strTemp;
				}
				// End If
				txtPayment1.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue1"), "#,##0.00") + " (1)";
				txtPayment2.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue2"), "#,##0.00") + " (2)";
				txtPayment3.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue3"), "#,##0.00") + " (3)";
				txtPayment4.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue4"), "#,##0.00") + " (4)";
				if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) > 0)
				{
					txtPayment1.Visible = true;
					txtPayment2.Visible = true;
					if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) > 0)
					{
						txtPayment3.Visible = true;
						if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4")) > 0)
						{
							txtPayment4.Visible = true;
						}
						else
						{
							txtPayment4.Visible = false;
						}
					}
					else
					{
						txtPayment3.Visible = false;
						txtPayment4.Visible = false;
					}
				}
				else
				{
					txtPayment1.Visible = false;
					txtPayment2.Visible = false;
					txtPayment3.Visible = false;
					txtPayment4.Visible = false;
				}
				// If Val(txtLandTot.Text) > 0 Then
				// lngLandSub = lngLandSub + CLng(txtLandTot.Text)
				// End If
				// If Val(txtBldgTot.Text) > 0 Then
				// lngBldgSub = lngBldgSub + CLng(txtBldgTot.Text)
				// End If
				// If Val(txtExemptTot.Text) > 0 Then
				// lngExemptsub = lngExemptsub + CLng(txtExemptTot.Text)
				// End If
				// If Val(txtTotTot.Text) > 0 Then
				// lngAssessSub = lngAssessSub + CLng(txtTotTot.Text)
				// End If
				// If Val(txtTaxTot.Text) > 0 Then
				// lngTaxSub = lngTaxSub + CDbl(txtTaxTot.Text)
				// End If
				// txtLandSub.Text = Format(lngLandSub, "#,###,###,##0")
				// txtBldgSub.Text = Format(lngBldgSub, "#,###,###,##0")
				// txtExemptSub.Text = Format(lngExemptsub, "#,###,###,##0")
				// txtTotalSub.Text = Format(lngAssessSub, "#,###,###,##0")
				// txtTaxSub.Text = Format(lngTaxSub, "#,###,###,##0.00")
				clsCommit.MoveNext();
			}
		}
		// vbPorter upgrade warning: intWhichReport As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intPageStart As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intWhichOrder As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intBindOption As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngTaxYear, bool boolSetFont, string strFontName, string strPrinterName, int intWhichReport, int intPageStart, bool boolNewPage, int lngRateKey, bool boolFromSupplemental = false, int intWhichOrder = 2, string strStart = "", string strEnd = "", int intBindOption = 0)
		{
			bool boolFirst;
			int x;
			string strInClause = "";
			string strWhereClause;
			clsRateRecord clsrate = new clsRateRecord();
			clsDRWrapper clsLoad = new clsDRWrapper();
			bool boolDup;
			string strOrderBy = "";
			string strRange;
            try
            {
                // On Error GoTo ErrorHandler
                strRange = "";
                switch ((intWhichOrder))
                {
                    case CNSTACCOUNTORDER:
                    {
                        strOrderBy = "Account";
                        if (Strings.Trim(strStart) != string.Empty || Strings.Trim(strEnd) != string.Empty)
                        {
                            if (Strings.Trim(strStart) == string.Empty)
                            {
                                strRange = " and account <= " + FCConvert.ToString(Conversion.Val(strEnd));
                            }
                            else if (Strings.Trim(strEnd) == string.Empty)
                            {
                                strRange = " and account >= " + FCConvert.ToString(Conversion.Val(strStart));
                            }
                            else
                            {
                                strRange = " and account between " + FCConvert.ToString(Conversion.Val(strStart)) +
                                           " and " + FCConvert.ToString(Conversion.Val(strEnd));
                            }
                        }

                        break;
                    }
                    case CNSTNAMEORDER:
                    {
                        strOrderBy = "Name1";
                        if (Strings.Trim(strStart) != string.Empty || Strings.Trim(strEnd) != string.Empty)
                        {
                            if (Strings.Trim(strStart) == string.Empty)
                            {
                                strRange = " and name1 <= '" + Strings.Trim(strEnd) + "' ";
                            }
                            else if (Strings.Trim(strEnd) == string.Empty)
                            {
                                strRange = " and name1 >= '" + Strings.Trim(strStart) + "'";
                            }
                            else
                            {
                                strRange = " and name1 between '" + Strings.Trim(strStart) + "' and '" +
                                           Strings.Trim(strEnd) + "' ";
                            }
                        }

                        break;
                    }
                    case CNSTMAPLOTORDER:
                    {
                        strOrderBy = "MapLot";
                        if (Strings.Trim(strStart) != string.Empty || Strings.Trim(strEnd) != string.Empty)
                        {
                            if (Strings.Trim(strStart) == string.Empty)
                            {
                                strRange = " and maplot <= '" + Strings.Trim(strEnd) + "' ";
                            }
                            else if (Strings.Trim(strEnd) == string.Empty)
                            {
                                strRange = " and maplot >= '" + Strings.Trim(strStart) + "' ";
                            }
                            else
                            {
                                strRange = " and maplot between '" + Strings.Trim(strStart) + "' and '" +
                                           Strings.Trim(strEnd) + "' ";
                            }
                        }

                        break;
                    }
                }

                //end switch
                boolDup = false;
                clsLoad.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("duplexcommitment")))
                        boolDup = true;
                }

                // take in the printer and font to use as well as the taxyear to make the bill for
                TaxYear = lngTaxYear;
                intPage = intPageStart;
                intStartPage = intPageStart;
                if (boolNewPage)
                {
                    this.GroupHeader1.DataField = "TheBinder";
                }

                clsrate.LoadRate(lngRateKey);
                lblDescription.Text = clsrate.Description;
                strWhereClause = "";
                if (boolFromSupplemental)
                {
                    for (x = 1; x <= frmSupplementalBill.InstancePtr.ListGrid.Rows - 1; x++)
                    {
                        if (Strings.UCase(Strings.Trim(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 0))) ==
                            "RE")
                        {
                            if (Conversion.Val(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 4)) == lngRateKey)
                            {
                                if (strWhereClause != "")
                                {
                                    strWhereClause += ",";
                                }
                                else
                                {
                                    strWhereClause = " ID in (";
                                }

                                strWhereClause +=
                                    FCConvert.ToString(
                                        Conversion.Val(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 6)));
                            }
                        }
                    }

                    // x
                    strWhereClause += ")";
                    clsCommit.OpenRecordset(
                        "select  * from billingmaster where " + strWhereClause + " and billingtype = 'RE'  order by " +
                        strOrderBy, "twcl0000.vb1");
                }
                else
                {
                    clsCommit.OpenRecordset(
                        "select * from billingmaster where ratekey  = " + FCConvert.ToString(lngRateKey) +
                        " and billingtype = 'RE' " + strRange + " order by " + strOrderBy, "twcl0000.vb1");
                }

                // Call clsRef1.OpenRecordset("select rsaccount,p.FullNameLF as OwnerFullNameLF, q.FullNameLF as SecOwnerFullNameLF ,rsref1,Address1,Address2,Address3, City,PartyState,Zip,RSLOCnumalph,rslocstreet,rsmaplot from master as c CROSS APPLY " & clsRef1.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(c.OwnerPartyID) as p CROSS APPLY " & clsRef1.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(c.SecOwnerPartyID) as q where rsdeleted <> 1 and rscard = 1 order by p.FullNameLF", "twre0000.vb1")
                if (!clsCommit.EndOfFile())
                {
                    lblTitle.Text = lblTitle.Text + FCConvert.ToString(TaxYear) + "  " +
                                    Strings.Format(clsrate.TaxRate * 1000, "0.000");
                    lblMuniname.Text = modGlobalConstants.Statics.MuniName;
                    if (boolSetFont)
                    {
                        // now set each box and label to the correct printer font
                        this.Detail.CanGrow = true;
                        // Line1.Visible = False
                        for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
                        {
                            // any field I marked with textbox must have its font changed. Bold is a field that also has bold font
                            bool setFont = false;
                            bool bold = false;
                            if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
                            {
                                setFont = true;
                            }
                            else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
                            {
                                setFont = true;
                                bold = true;
                            }

                            if (setFont)
                            {
                                GrapeCity.ActiveReports.SectionReportModel.TextBox textBox =
                                    this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                                if (textBox != null)
                                {
                                    textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
                                    textBox.CanGrow = true;
                                }
                                else
                                {
                                    GrapeCity.ActiveReports.SectionReportModel.Label label =
                                        this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                    if (label != null)
                                    {
                                        label.Font = new Font(strFontName, 10,
                                            bold ? FontStyle.Bold : FontStyle.Regular);
                                    }
                                }
                            }
                        }

                        // x
                        for (x = 0; x <= this.PageFooter.Controls.Count - 1; x++)
                        {
                            // any field I marked with textbox must have its font changed. Bold is a field that also has bold font
                            bool setFont = false;
                            bool bold = false;
                            if (FCConvert.ToString(this.PageFooter.Controls[x].Tag) == "textbox")
                            {
                                setFont = true;
                            }
                            else if (FCConvert.ToString(this.PageFooter.Controls[x].Tag) == "bold")
                            {
                                setFont = true;
                                bold = true;
                            }

                            if (setFont)
                            {
                                GrapeCity.ActiveReports.SectionReportModel.TextBox textBox =
                                    this.PageFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                                if (textBox != null)
                                {
                                    textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
                                }
                                else
                                {
                                    GrapeCity.ActiveReports.SectionReportModel.Label label =
                                        this.PageFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                    if (label != null)
                                    {
                                        label.Font = new Font(strFontName, 10,
                                            bold ? FontStyle.Bold : FontStyle.Regular);
                                    }
                                }
                            }
                        }

                        // x
                        //this.Printer.RenderMode = 1;
                    }

                    // change to the selected printer
                    this.Document.Printer.PrinterName = strPrinterName;
                    switch (intWhichReport)
                    {
                        case 2:
                        {
                            // landscape regular
                            this.Document.Printer.DefaultPageSettings.Landscape = true;
                            boolLandscape = true;
                            break;
                        }
                        case 3:
                        {
                            // wide printer
                            //this.Document.Printer.PaperSize = 39;
                            this.Document.Printer.PaperKind = System.Drawing.Printing.PaperKind.USStandardFanfold;
                            boolLandscape = false;
                            // Me.PageSettings.PaperHeight = Me.Printer.PaperHeight
                            // Me.PageSettings.PaperWidth = Me.Printer.PaperWidth
                            break;
                        }
                    }

                    //end switch
                    switch (intBindOption)
                    {
                        case 1:
                        {
                            // left bind
                            switch (intWhichReport)
                            {
                                case 2:
                                {
                                    this.PageSettings.Margins.Left = 1080 / 1440f;
                                    this.PageSettings.Margins.Right = 360 / 1440f;
                                    this.PrintWidth = 14400 / 1440f;
                                    break;
                                }
                                case 3:
                                {
                                    this.PageSettings.Margins.Left = 2880 / 1440f;
                                    this.PrintWidth = 15840 / 1440f;
                                    break;
                                }
                            }

                            //end switch
                            break;
                        }
                        case 2:
                        {
                            // top bind
                            this.PageSettings.Margins.Top = 2160 / 1440f;
                            break;
                        }
                    }

                    //end switch
                    if (boolFromSupplemental)
                    {
                        //FC:FINAL:MSH - issue #1526: show 'Wide' report in modal window 
                        frmReportViewer.InstancePtr.Init(rptObj: this, strPrinter: FCGlobal.Printer.DeviceName,
                            intModal: 1, boolDuplex: boolDup, boolLandscape: boolLandscape, boolAllowEmail: true,
                            strAttachmentName: "Commitment", showModal: true);
                    }
                    else
                    {
                        // Me.Show , MDIParent
                        //FC:FINAL:MSH - issue #1526: show 'Wide' report in modal window 
                        frmReportViewer.InstancePtr.Init(rptObj: this, strPrinter: FCGlobal.Printer.DeviceName,
                            intModal: 1, boolDuplex: boolDup, boolLandscape: boolLandscape, boolAllowEmail: true,
                            strAttachmentName: "Commitment", showModal: true);
                    }
                }
                else
                {
                    // not records so leave
                    // If Not boolFromSupplemental Then
                    // MDIParent.Show
                    // End If
                    this.Close();
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In rptCommitmentWide.init", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.Dispose();
            }
		}

		private void PageFooter_Format(object sender, EventArgs e)
		{
			if (Conversion.Val(txtLandTot.Text) > 0)
			{
				//FC:FINAL:CHN - issue #1328: Incorrect types casting. 
				// lngLandSub += FCConvert.ToInt32(txtLandTot.Text);
				lngLandSub += Conversion.Val(txtLandTot.Text);
			}
			if (Conversion.Val(txtBldgTot.Text) > 0)
			{
				//FC:FINAL:CHN - issue #1328: Incorrect types casting. 
				// lngBldgSub += FCConvert.ToInt32(txtBldgTot.Text);
				lngBldgSub += Conversion.Val(txtBldgTot.Text);
			}
			if (Conversion.Val(txtExemptTot.Text) > 0)
			{
				//FC:FINAL:CHN - issue #1328: Incorrect types casting. 
				// lngExemptsub += FCConvert.ToInt32(txtExemptTot.Text);
				lngExemptsub += Conversion.Val(txtExemptTot.Text);
			}
			if (Conversion.Val(txtTotTot.Text) > 0)
			{
				//FC:FINAL:CHN - issue #1328: Incorrect types casting. 
				// lngAssessSub += FCConvert.ToInt32(txtTotTot.Text);
				lngAssessSub += Conversion.Val(txtTotTot.Text);
			}
			if (Conversion.Val(txtTaxTot.Text) > 0)
			{
				//FC:FINAL:CHN - issue #1328: Incorrect types casting. 
				// lngTaxSub += FCConvert.ToDouble(txtTaxTot.Text);
				lngTaxSub += Conversion.Val(txtTaxTot.Text);
			}
			txtLandSub.Text = Strings.Format(lngLandSub, "#,###,###,##0");
			txtBldgSub.Text = Strings.Format(lngBldgSub, "#,###,###,##0");
			txtExemptSub.Text = Strings.Format(lngExemptsub, "#,###,###,##0");
			txtTotalSub.Text = Strings.Format(lngAssessSub, "#,###,###,##0");
			txtTaxSub.Text = Strings.Format(lngTaxSub, "#,###,###,##0.00");
			if (clsCommit.EndOfFile())
			{
				lblSubTotals.Text = "Final Totals:";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = intPage.ToString();
			intPage += 1;
		}

		private void FillValueFields()
		{
			if (Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name1"))) != string.Empty)
			{
				this.Fields["TheBinder"].Value = Strings.Left(FCConvert.ToString(clsCommit.Get_Fields_String("name1")), 1);
			}
			else
			{
				this.Fields["TheBinder"].Value = "";
			}
			// filling these fields will fill the textboxes and the summary fields in the page footer
			this.Fields["landval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("landvalue"));
			this.Fields["bldgval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue"));
			this.Fields["exemptval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
			this.Fields["totalval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("landvalue")) + Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
			this.Fields["taxval"].Value = Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
		}

		
	}
}
