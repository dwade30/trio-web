﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormatC.
	/// </summary>
	public partial class rptBillFormatC : FCSectionReport
	{
		public rptBillFormatC()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Format C";
		}

		public static rptBillFormatC InstancePtr
		{
			get
			{
				return (rptBillFormatC)Sys.GetInstance(typeof(rptBillFormatC));
			}
		}

		protected rptBillFormatC _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillFormatC	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// WRITTEN BY     :               Corey Gray              *
		/// </summary>
		/// <summary>
		/// DATE           :                                       *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// MODIFIED BY    :               Jim Bertolino           *
		/// </summary>
		/// <summary>
		/// LAST UPDATED   :               07/29/2004              *
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		bool boolFirstBill;
		clsDRWrapper clsMortgageHolders = new clsDRWrapper();
		clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
		clsDRWrapper clsRateRecs = new clsDRWrapper();
		clsBillFormat clsTaxBillFormat = new clsBillFormat();
		clsRateRecord clsTaxRate = new clsRateRecord();
		double TaxRate;
		int intStartPage;
		clsDRWrapper clsBills = new clsDRWrapper();
		bool boolRE;
		bool boolPP;
		double Tax1;
		double Tax2;
		double Tax3;
		double Tax4;
		// vbPorter upgrade warning: Prepaid As double	OnWrite(double, string)
		double Prepaid;
		double DistPerc1;
		double DistPerc2;
		double DistPerc3;
		double DistPerc4;
		double DistPerc5;
		int intDistCats;
		double dblDiscountPercent;
		string strCat1 = "";
		string strCat2 = "";
		string strCat3 = "";
		int lngExempt;
		// vbPorter upgrade warning: lngTotAssess As int	OnWrite(short, double)
		int lngTotAssess;
		string strMap = "";
		string strLot = "";
		string strSub = "";
		string strType = "";
		public string strYear1 = "";
		public string strYear2 = "";
		public int intCat1;
		public int intCat2;
		bool boolShownModally;
		bool boolNotJustUnloading;
		bool boolReprintNewOwnerCopy;
		/// <summary>
		/// this will be set to true if there is a new owner and the recordset will not be advanced to create a copy for him
		/// </summary>
		bool boolPrintRecipientCopy;
		clsDRWrapper rsMultiRecipients = new clsDRWrapper();
		// vbPorter upgrade warning: intYear As short	OnWrite(double, int)
		public void Init(clsBillFormat clsTaxFormat, int intYear, string strFontName, bool boolShow = true, string strPrinterN = "", bool boolModal = false)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			string strSQL;
			boolShownModally = boolModal;
			boolNotJustUnloading = true;
			if (strPrinterN != string.Empty)
			{
				this.Document.Printer.PrinterName = strPrinterN;
			}
			if (clsTaxFormat.BillsFrom == 0)
			{
				boolRE = true;
				if (clsTaxFormat.Combined)
				{
					boolPP = true;
				}
				else
				{
					boolPP = false;
				}
			}
			else
			{
				boolRE = false;
				boolPP = true;
			}
			if (!boolPP)
			{
				txtCat1.Visible = false;
				txtCat2.Visible = false;
				txtCatOther.Visible = false;
				txtOtherLabel.Visible = false;
			}
			// 
			if (!boolRE)
			{
				txtLand.Visible = false;
				txtBldg.Visible = false;
			}
			clsTaxBillFormat.CopyFormat(ref clsTaxFormat);
			if (!boolRE)
			{
				txtBookPage.Visible = false;
				txtPage.Visible = false;
			}
			if (boolRE && clsTaxBillFormat.PrintCopyForMortgageHolders)
			{
				boolFirstBill = true;
				clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
				clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT", "CentralData");
				if (!clsMortgageAssociations.EndOfFile())
				{
					clsMortgageAssociations.MoveFirst();
					clsMortgageAssociations.MovePrevious();
				}
			}
			else
			{
				boolFirstBill = true;
			}
			strSQL = clsTaxBillFormat.SQLStatement;
			clsTaxRate.TaxYear = intYear;
			clsRateRecs.OpenRecordset("select * from raterec where year = " + FCConvert.ToString(intYear), "twcl0000.vb1");
			if (!clsRateRecs.EndOfFile())
			{
				clsTaxRate.LoadRate(clsRateRecs.Get_Fields_Int32("ID"));
			}
			clsTemp.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
			while (!clsTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				switch (FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("line"))))
				{
					case 0:
						{
							txtMessage1.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 1:
						{
							txtMessage2.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 2:
						{
							txtMessage3.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 3:
						{
							txtMessage4.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 4:
						{
							txtMessage5.Text = clsTemp.Get_Fields_String("message");
							break;
						}
				}
				//end switch
				clsTemp.MoveNext();
			}
			if ((clsTaxRate.NumberOfPeriods < 2) || (clsTaxBillFormat.Billformat == 9))
			{
				txtAmount2.Visible = false;
				txtAcct2.Visible = false;
				txtInterest2.Visible = false;
			}
			else
			{
				txtAcct2.Visible = true;
				txtAmount2.Visible = true;
				txtInterest2.Visible = true;
				txtInterest2.Text = clsTaxRate.Get_InterestStartDate(2).ToShortDateString();
			}
			txtInterest1.Text = clsTaxRate.Get_InterestStartDate(1).ToShortDateString();
			if (strFontName != string.Empty)
			{
				// now set each box and label to the correct printer font
				for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
				{
					// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
					bool setFont = false;
					bool bold = false;
					if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
					{
						setFont = true;
					}
					else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
					{
						setFont = true;
						bold = true;
					}
					if (setFont)
					{
						GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
						if (textBox != null)
						{
							textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
						}
						else
						{
							GrapeCity.ActiveReports.SectionReportModel.Label label = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
							if (label != null)
							{
								label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
						}
					}
				}
				// x
				//this.Printer.RenderMode = 1;
			}
			frmCMessage.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            txtYear1.Text = strYear1;
			txtYear2.Text = strYear2;
			GetBills(ref strSQL);
			if (boolShow)
			{
				// Me.Show , MDIParent
			}
			else
			{
				this.PrintReport(false);
			}
		}

		private void GetBills(ref string strSQL)
		{
			clsBills.OpenRecordset(strSQL, "twcl0000.vb1");
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int intReturn = 0;
			
			modCustomPageSize.CheckDefaultPrinter(this.Document.Printer.PrinterName);
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("BillFormatC", 1000, 550);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsBills.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			clsDRWrapper clsAddress = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
			if (clsTaxBillFormat.HasDefaultMargin)
			{
				this.PageSettings.Margins.Left = 0;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			modCustomPageSize.Statics.boolChangedDefault = true;
			modCustomPageSize.ResetDefaultPrinter();
			if (!boolShownModally && boolNotJustUnloading)
			{
				//MDIParent.InstancePtr.Show();
			}
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	// do this since we already chose the printer and don't want to choose again
		//	// now we have to handle printing the pages ourselves though.
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "Print...")
		//	{
		//		clsPrint.ReportPrint(this);
		//		// Call frmNumPages.Init(Me.Pages.Count, 1)
		//		// frmNumPages.Show vbModal, MDIParent
		//		// Me.Printer.FromPage = gintStartPage
		//		// Me.Printer.ToPage = gintEndPage
		//		// Me.PrintReport (False)
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			double Dist1;
			double Dist2;
			double Dist3;
			double Dist4;
			double Dist5;
			// vbPorter upgrade warning: TotTax As double	OnWrite(double, string)
			double TotTax = 0;
			string strTemp = "";
			string strAcct = "";
			DateTime dtLastDate;
			string strOldName = "";
			string strSecOwner = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			int intTemp = 0;
			if (!clsBills.EndOfFile())
			{
				if (FCConvert.ToString(clsBills.Get_Fields_String("Billingtype")) == "RE")
				{
					boolRE = true;
					boolPP = false;
				}
				else if (clsBills.Get_Fields_String("Billingtype") == "PP")
				{
					boolRE = false;
					boolPP = true;
				}
				else
				{
					// combined
					boolRE = true;
					boolPP = true;
				}
				if (!boolPP)
				{
					txtCat1.Visible = false;
					txtCat2.Visible = false;
					txtCatOther.Visible = false;
					txtOtherLabel.Visible = false;
				}
				else
				{
					txtCat1.Visible = true;
					txtCat2.Visible = true;
					txtCatOther.Visible = true;
					txtOtherLabel.Visible = true;
				}
				// 
				if (!boolRE)
				{
					txtLand.Visible = false;
					txtBldg.Visible = false;
					txtBookPage.Visible = false;
					txtPage.Visible = false;
				}
				else
				{
					txtLand.Visible = true;
					txtBldg.Visible = true;
					txtBookPage.Visible = true;
					txtPage.Visible = true;
				}
				if (clsRateRecs.FindFirstRecord("ID", clsBills.Get_Fields_Int32("ratekey")))
				{
					clsTaxRate.LoadRate(clsBills.Get_Fields_Int32("ratekey"), clsRateRecs);
					if ((clsTaxRate.NumberOfPeriods < 2) || (clsTaxBillFormat.Billformat == 9))
					{
						// framStub2.Visible = False
						// txtPayment2Label.Visible = False
						txtAmount2.Visible = false;
						txtAcct2.Visible = false;
						// txtDueDate2.Visible = False
						txtInterest2.Visible = false;
					}
					else
					{
						// framStub2.Visible = True
						txtAcct2.Visible = true;
						// txtDueDate2.Text = clsTaxRate.Get_DueDate(2)
						txtAmount2.Visible = true;
						// txtDueDate2.Visible = True
						txtInterest2.Visible = true;
						txtInterest2.Text = clsTaxRate.Get_InterestStartDate(2).ToShortDateString();
						// txtPayment2Label.Visible = True
					}
					// 
					// End If
					// 
					// txtDueDate1.Text = clsTaxRate.Get_DueDate(1)
					txtInterest1.Text = clsTaxRate.Get_InterestStartDate(1).ToShortDateString();
				}
				lngTotAssess = 0;
				lngExempt = 0;
				Tax1 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue1"));
				Tax2 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue2"));
				Tax3 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue3"));
				Tax4 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue4"));
				TotTax = Tax1 + Tax2 + Tax3 + Tax4;
				txtTaxDue.Text = Strings.Format(TotTax, "#,###,###,##0.00");
				Prepaid = Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid"));
				double dblAbated = 0;
				double dblAbate1 = 0;
				double dblAbate2 = 0;
				double dblAbate3 = 0;
				double dblAbate4 = 0;
				int intPer;
				double[] dblTAbate = new double[4 + 1];
				dblAbated = 0;
				if ((Tax2 > 0 || Tax3 > 0 || Tax4 > 0) && Prepaid > 0)
				{
					dblAbated = modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields_Int32("ID"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
					dblAbate1 = dblTAbate[1];
					dblAbate2 = dblTAbate[2];
					dblAbate3 = dblTAbate[3];
					dblAbate4 = dblTAbate[4];
					if (boolRE && boolPP)
					{
						// TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
						dblAbated += modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields("ppbillkey"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
						dblAbate1 += dblTAbate[1];
						dblAbate2 += dblTAbate[2];
						dblAbate3 += dblTAbate[3];
						dblAbate4 += dblTAbate[4];
					}
					Prepaid -= dblAbated;
				}
				if (dblAbated > 0)
				{
					Tax1 -= dblAbate1;
					Tax2 -= dblAbate2;
					Tax3 -= dblAbate3;
					Tax4 -= dblAbate4;
				}
				if (Prepaid > 0)
				{
					if (Prepaid > Tax1)
					{
						Prepaid -= Tax1;
						Tax1 = 0;
						if (Prepaid > Tax2)
						{
							Prepaid -= Tax2;
							Tax2 = 0;
							if (Prepaid > Tax3)
							{
								Prepaid -= Tax3;
								Tax3 = 0;
								if (Prepaid > Tax4)
								{
									Prepaid -= Tax4;
									Tax4 = 0;
								}
								else
								{
									Tax4 -= Prepaid;
								}
							}
							else
							{
								Tax3 -= Prepaid;
							}
						}
						else
						{
							Tax2 -= Prepaid;
						}
					}
					else
					{
						Tax1 -= Prepaid;
					}
				}
				Prepaid = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid")), "0.00"));
				TotTax = FCConvert.ToDouble(Strings.Format(TotTax, "0.00"));
				txtPrePaid.Text = Strings.Format(Prepaid, "#,###,##0.00");
				if (Prepaid == 0)
				{
					txtPrePaid.Visible = false;
					txtPrePaidLabel.Visible = false;
				}
				else
				{
					txtPrePaid.Visible = true;
					txtPrePaidLabel.Visible = true;
					if (Prepaid > TotTax)
					{
						txtTaxDue.Text = "Overpaid";
					}
					else
					{
						txtTaxDue.Text = Strings.Format(TotTax - Prepaid, "#,###,###,##0.00");
					}
				}
				txtRate.Text = Strings.Format(clsTaxRate.TaxRate * 1000, "#0.000");
				txtAmount1.Text = Strings.Format(Tax1, "#,###,##0.00");
				txtAmount2.Text = Strings.Format(Tax2, "#,###,##0.00");
				// txtDueDate1.Text = clsTaxRate.Get_DueDate(1)
				// txtDueDate2.Text = clsTaxRate.Get_DueDate(2)
				txtInterest1.Text = clsTaxRate.Get_InterestStartDate(1).ToShortDateString();
				txtInterest2.Text = clsTaxRate.Get_InterestStartDate(2).ToShortDateString();
				// txtAmount3.Text = Format(Tax3, "#,###,##0.00")
				// txtAmount4.Text = Format(Tax4, "#,###,##0.00")
				// txtinterest3.Text = clsTaxRate.Get_InterestStartDate(3)
				// txtinterest4.Text = clsTaxRate.Get_InterestStartDate(4)
				// 
				// If clsTaxBillFormat.Discount Then
				// txtDiscountAmount.Text = Format(dblDiscountPercent * (Tax1 + Tax2 + Tax3 + Tax4), "#,###,##0.00")
				// txtDiscountAmount.Text = Format((Tax1 + Tax2 + Tax3 + Tax4), "#,###,##0.00")
				// End If
				// 
				strAcct = "";
				txtMailing5.Text = "";
				if (boolRE)
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					strAcct = "R" + clsBills.Get_Fields("account");
					if (Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage"))) != string.Empty)
					{
						strTemp = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage")));
						strTemp = Strings.Mid(strTemp, 2);
						txtBookPage.Text = FCConvert.ToString(Conversion.Val(strTemp));
						intTemp = Strings.InStr(1, strTemp, "P", CompareConstants.vbTextCompare);
						if (intTemp > 0)
						{
							strTemp = FCConvert.ToString(Conversion.Val(Strings.Mid(strTemp, intTemp + 1)));
							txtPage.Text = strTemp;
						}
						else
						{
							txtPage.Text = "";
						}
					}
					else
					{
						txtBookPage.Visible = false;
						txtPage.Visible = false;
					}
					txtLand.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")), "#,###,###,##0");
					txtBldg.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")), "#,###,###,##0");
					lngTotAssess = FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")) + Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")));
					if (boolPrintRecipientCopy)
					{
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						txtMailing2.Text = "C/O " + rsMultiRecipients.Get_Fields_String("Name");
						txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address1");
						txtMailing4.Text = rsMultiRecipients.Get_Fields_String("address2");
						txtMailing5.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
					}
					else if (!boolFirstBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
					{
						clsMortgageHolders.FindFirstRecord("ID", clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"));
						if (!clsMortgageHolders.NoMatch)
						{
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing2.Text = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
							txtMailing5.Text = Strings.Trim(clsMortgageHolders.Get_Fields_String("city") + " " + clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
						}
						else
						{
							txtMailing1.Text = "Mortgage Holder Not Found";
							txtMailing2.Text = "";
							txtMailing3.Text = "";
							txtMailing4.Text = "";
							txtMailing5.Text = "";
						}
					}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (boolReprintNewOwnerCopy && modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3))
					{
						txtMailing3.Text = strAddr1;
						txtMailing4.Text = strAddr2;
						txtMailing5.Text = strAddr3;
						txtMailing1.Text = strOldName;
						txtMailing2.Text = strSecOwner;
					}
					else
					{
						txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
						txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
						txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						// strName = Trim(clsBills.Fields("name1"))
						txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name2")));
					}
					if (txtMailing4.Text == "")
					{
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing3.Text == "")
					{
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing2.Text == "")
					{
						txtMailing2.Text = txtMailing3.Text;
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						txtLocation.Text = Strings.Trim(Strings.Trim(clsBills.Get_Fields("streetnumber") + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
					}
					else
					{
						txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("apt"))) + " " + clsBills.Get_Fields_String("streetname"));
					}
					modGlobalRoutines.ParseMapLot(clsBills.Get_Fields_String("maplot"), ref strMap, ref strLot, ref strSub, ref strType);
					if (Strings.Trim(strMap + strLot) != string.Empty)
					{
						txtMapLot.Text = Strings.Trim(strMap);
						if (Strings.Trim(strLot + strSub + strType) != string.Empty)
						{
							txtLot.Text = Strings.Trim(strLot + " " + strSub + " " + strType);
						}
					}
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				}
				if (boolPP)
				{
					// pp
					if (boolRE)
					{
						// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBills.Get_Fields("ppac")) > 0)
						{
							strAcct += " P";
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							strAcct += clsBills.Get_Fields("ppac");
						}
					}
					else
					{
						strAcct = "P";
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						strAcct += clsBills.Get_Fields("account");
					}
					switch (intCat1)
					{
						case 0:
							{
								strCat1 = "";
								break;
							}
						case 1:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category1"))), "#,###,###,##0");
								break;
							}
						case 2:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category2"))), "#,###,###,##0");
								break;
							}
						case 3:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category3"))), "#,###,###,##0");
								break;
							}
						case 4:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category4"))), "#,###,###,##0");
								break;
							}
						case 5:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category5"))), "#,###,###,##0");
								break;
							}
						case 6:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category6"))), "#,###,###,##0");
								break;
							}
						case 7:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category7"))), "#,###,###,##0");
								break;
							}
						case 8:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category8"))), "#,###,###,##0");
								break;
							}
						case 9:
							{
								strCat1 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category9"))), "#,###,###,##0");
								break;
							}
					}
					//end switch
					switch (intCat2)
					{
						case 0:
							{
								strCat2 = "";
								break;
							}
						case 1:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category1"))), "#,###,###,##0");
								break;
							}
						case 2:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category2"))), "#,###,###,##0");
								break;
							}
						case 3:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category3"))), "#,###,###,##0");
								break;
							}
						case 4:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category4"))), "#,###,###,##0");
								break;
							}
						case 5:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category5"))), "#,###,###,##0");
								break;
							}
						case 6:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category6"))), "#,###,###,##0");
								break;
							}
						case 7:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category7"))), "#,###,###,##0");
								break;
							}
						case 8:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category8"))), "#,###,###,##0");
								break;
							}
						case 9:
							{
								strCat2 = Strings.Format(FCConvert.ToString(Conversion.Val(clsBills.Get_Fields_Int32("category9"))), "#,###,###,##0");
								break;
							}
					}
					//end switch
					txtCat1.Text = strCat1;
					txtCat2.Text = strCat2;
					// txtCat2.Text = Format(Val(clsBills.Fields("cat2")), "#,###,###,##0")
					// txtCat3.Text = Format(Val(clsBills.Fields("cat3")), "#,###,###,##0")
					txtCatOther.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category3")) + Conversion.Val(clsBills.Get_Fields_Int32("category4")) + Conversion.Val(clsBills.Get_Fields_Int32("category5")) + Conversion.Val(clsBills.Get_Fields_Int32("category6")) + Conversion.Val(clsBills.Get_Fields_Int32("category7")) + Conversion.Val(clsBills.Get_Fields_Int32("category8")) + Conversion.Val(clsBills.Get_Fields_Int32("category9")), "#,###,###,##0");
					lngTotAssess += FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("ppassessment")));
					if (!boolRE)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						txtLocation.Text = Strings.Trim(clsBills.Get_Fields("streetnumber") + " " + clsBills.Get_Fields_String("apt") + "  " + clsBills.Get_Fields_String("streetNAME"));
						// used for pp location
						if (boolPrintRecipientCopy)
						{
							txtMailing1.Text = rsMultiRecipients.Get_Fields_String("Name");
							txtMailing2.Text = rsMultiRecipients.Get_Fields_String("address1");
							txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address2");
							txtMailing4.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
							txtMailing5.Text = "";
						}
						else
						{
							txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
							// strTemp = Trim(clsBills.Fields("zip"))
							// txtMailing4.Text = Trim(clsBills.Fields("city")) & "  " & clsBills.Fields("state") & "  " & strTemp
							// If Trim(clsBills.Fields("zip4")) <> vbNullString Then
							// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("zip4"))
							// End If
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing5.Text = "";
						}
						if (txtMailing3.Text == "")
						{
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = "";
						}
						if (txtMailing2.Text == "")
						{
							txtMailing2.Text = txtMailing3.Text;
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = "";
						}
					}
					else
					{
					}
				}
				lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				// txtValue.Text = Format(lngTotAssess, "#,###,###,##0")
				txtExemption.Text = Strings.Format(lngExempt, "#,###,###,##0");
				txtAssessment.Text = Strings.Format(lngTotAssess - lngExempt, "#,###,###,##0");
				txtAccount.Text = strAcct;
				txtAcct1.Text = strAcct;
				txtAcct2.Text = strAcct;
				if (boolRE)
				{
					if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
					{
						boolReprintNewOwnerCopy = false;
						// if the copy for a new owner needs to be sent, then check it here
						if (clsTaxBillFormat.PrintCopyForNewOwner)
						{
							// if this is the main bill, only check for the new owner on the mail bill
							if (boolFirstBill)
							{
								if (DateTime.Today.Month < 4)
								{
									// if the month is before april then use last years commitment date
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1));
								}
								else
								{
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));
								}
								// If NewOwner(clsBills.Fields("Account"), dtLastDate) Then     'if there has been a new owner
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1")))
								{
									// if there has been a new owner
									boolReprintNewOwnerCopy = true;
								}
							}
						}
					}
					else if (!boolPrintRecipientCopy && boolFirstBill)
					{
						// turn off the copy so that the record can advance
						boolReprintNewOwnerCopy = false;
					}
					if (!boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							if (boolFirstBill)
							{
								rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
								if (!rsMultiRecipients.EndOfFile())
								{
									boolPrintRecipientCopy = true;
								}
							}
						}
					}
					else if (boolPrintRecipientCopy && boolFirstBill)
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (clsTaxBillFormat.PrintCopyForMortgageHolders && !boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (!clsMortgageAssociations.EndOfFile())
						{
							if (boolFirstBill)
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindFirstRecord("account", clsBills.Get_Fields("account"));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindNextRecord("account", clsBills.Get_Fields("account"));
							}
							// Call clsMortgageAssociations.FindNextRecord("account", clsBills.Fields("ac"))
							if (clsMortgageAssociations.NoMatch)
							{
								boolFirstBill = true;
								if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
									clsBills.MoveNext();
							}
							else
							{
								boolFirstBill = false;
							}
							// End If
						}
						else
						{
							boolFirstBill = true;
							if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
								clsBills.MoveNext();
						}
					}
					else
					{
						if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
							clsBills.MoveNext();
					}
				}
				else
				{
					if (!boolPrintRecipientCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
							if (!rsMultiRecipients.EndOfFile())
							{
								boolPrintRecipientCopy = true;
							}
						}
					}
					else
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (!boolPrintRecipientCopy)
					{
						clsBills.MoveNext();
					}
				}
			}
		}

		private void rptBillFormatC_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBillFormatC properties;
			//rptBillFormatC.Caption	= "Format C";
			//rptBillFormatC.Icon	= "rptBillFormatC.dsx":0000";
			//rptBillFormatC.Left	= 0;
			//rptBillFormatC.Top	= 0;
			//rptBillFormatC.Width	= 15240;
			//rptBillFormatC.Height	= 11115;
			//rptBillFormatC.StartUpPosition	= 3;
			//rptBillFormatC.SectionData	= "rptBillFormatC.dsx":058A;
			//End Unmaped Properties
		}
	}
}
