﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPCollectionSimple.
	/// </summary>
	public partial class rptPPCollectionSimple : BaseSectionReport
	{
		public rptPPCollectionSimple()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Personal Property Collection List";
		}

		public static rptPPCollectionSimple InstancePtr
		{
			get
			{
				return (rptPPCollectionSimple)Sys.GetInstance(typeof(rptPPCollectionSimple));
			}
		}

		protected rptPPCollectionSimple _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCommit.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPCollectionSimple	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int TaxYear;
		clsDRWrapper clsCommit = new clsDRWrapper();
		string Addr1 = "";
		string Addr2 = "";
		string addr3 = "";
		string strMailingAddress3 = "";
		int intStartPage;
		int intPage;
		bool boolIncludeAddress;
		// vbPorter upgrade warning: lngAssessSub As int	OnWrite(short, double)
		int lngAssessSub;
		int lngPage;
		double lngTaxSub;
		double dblDisc;
		double dblTax;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			lngPage = 1;
			lngAssessSub = 0;
			lngTaxSub = 0;
			// These fields will be filled with values that will then automatically
			// go to the textboxes and the summary textboxes in the page footer
			this.Fields.Add("TheBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsCommit.EndOfFile();
			if (!eArgs.EOF)
			{
				FillValueFields();
			}
		}

		private void ActiveReport_QueryClose(ref short Cancel, ref short CloseMode)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so we handle the event
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
			intPage = intStartPage;
			txtDate.Text = DateTime.Today.ToShortDateString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	string vbPorterVar = Tool.Caption;
		//	// if they click the print button
		//	if (vbPorterVar == "Print...")
		//	{
		//		// get the page range they want to print
		//		modGlobalVariables.Statics.gintStartPage = intStartPage;
		//		modGlobalVariables.Statics.gintEndPage = this.Pages.Count;
		//		frmNumPages.InstancePtr.Init(ref modGlobalVariables.Statics.gintEndPage, ref modGlobalVariables.Statics.gintStartPage);
		//		frmNumPages.InstancePtr.ShowDialog();
		//		// set the printers printrange
		//		this.Printer.FromPage = modGlobalVariables.Statics.gintStartPage;
		//		this.Printer.ToPage = modGlobalVariables.Statics.gintEndPage;
		//		this.PrintReport(false);
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			double dblDiscount = 0;
			double dblTax = 0;
			if (!clsCommit.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = clsCommit.Get_Fields_String("account");
				txtName.Text = clsCommit.Get_Fields_String("name1");
				// txtAssessment.DataValue = Val(clsCommit.Fields("ppassessment"))
				// lngValSub = lngValSub + Val(clsCommit.Fields("ppassessment"))
				// txtExempt.DataValue = Val(clsCommit.Fields("exemptvalue"))
				// lngExemptsub = lngExemptsub + Val(clsCommit.Fields("exemptvalue"))
				lngAssessSub += FCConvert.ToInt32(Conversion.Val(clsCommit.Get_Fields_Int32("ppassessment")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue")));
				txtTotal.Value = Conversion.Val(clsCommit.Get_Fields_Int32("ppassessment")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
				dblTax = Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
				txtTax.Value = dblTax;
				lngTaxSub += dblTax;
				dblDiscount = modMain.Round(dblDisc * dblTax, 2);
				txtDiscount.Text = Strings.Format(dblDiscount, "#,###,###,##0.00");
				// get the address fields and then condense them to get rid of gaps
				if (boolIncludeAddress)
				{
					Addr1 = FCConvert.ToString(clsCommit.Get_Fields_String("address1"));
					Addr2 = FCConvert.ToString(clsCommit.Get_Fields_String("address2"));
					addr3 = FCConvert.ToString(clsCommit.Get_Fields_String("address3"));
					strMailingAddress3 = FCConvert.ToString(clsCommit.Get_Fields_String("MailingAddress3"));
				}
				if (Strings.Trim(Addr1) == string.Empty)
				{
					Addr1 = Addr2;
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = "";
					if (Strings.Trim(Addr1) == string.Empty)
					{
						Addr1 = Addr2;
						Addr2 = strMailingAddress3;
						strMailingAddress3 = addr3;
						addr3 = "";
					}
				}
				if (Strings.Trim(Addr2) == string.Empty)
				{
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = "";
				}
				if (Strings.Trim(strMailingAddress3) == string.Empty)
				{
					strMailingAddress3 = addr3;
					addr3 = "";
				}
				if (boolIncludeAddress)
				{
					txtAddress1.Text = Addr1;
					txtAddress2.Text = Addr2;
					txtAddress3.Text = addr3;
					txtMailingAddress3.Text = strMailingAddress3;
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtLocation.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtAddress1.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
				}
				clsCommit.MoveNext();
			}
		}
		// vbPorter upgrade warning: lngTaxYear As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngTaxYear, bool boolSetFont, string strFontName, string strPrinterName, double dblDiscount, int intOrder, bool boolPrintAddress = true)
		{
			string strOrder = "";
			boolIncludeAddress = boolPrintAddress;
			if (!boolIncludeAddress)
			{
				this.Detail.Height -= (225 * 3 / 1440f);
			}
			TaxYear = lngTaxYear;
			// set the initial page number
			intStartPage = 1;
			if (intOrder == 2)
			{
				strOrder = "name1";
			}
			else
			{
				strOrder = "account";
			}
			if (dblDiscount == 0)
			{
				txtDiscount.Visible = false;
				lblDiscount.Visible = false;
			}
			else
			{
				dblDisc = modMain.Round((dblDiscount / 100), 4);
			}
			clsCommit.OpenRecordset("select * from billingmaster where billingyear = " + FCConvert.ToString(TaxYear) + "1 and billingtype = 'PP' order by " + strOrder, "twcl0000.vb1");
			if (!clsCommit.EndOfFile())
			{
				lblTitle.Text = lblTitle.Text + FCConvert.ToString(TaxYear);
				lblMuniname.Text = modGlobalConstants.Statics.MuniName;
				// if using a printer font, then set all textboxes to it, and make captions bold
				if (boolSetFont)
				{
					foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl fld in this.GetAllControls())
					{
						bool setFont = false;
						bool bold = false;
						if (FCConvert.ToString(fld.Tag) == "textbox")
						{
							setFont = true;
						}
						else if (FCConvert.ToString(fld.Tag) == "bold")
						{
							setFont = true;
							bold = true;
						}
						if (setFont)
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = fld as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textBox != null)
							{
								textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = fld as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
								}
							}
						}
					}
					// fld
					//this.Printer.RenderMode = 1;
				}
				// set the printer to the one they picked
				this.Document.Printer.PrinterName = strPrinterName;
				// Me.Show , MDIParent
				//FC:FINAL:MSH - i.issue #1586: restore missing call of the report
				frmReportViewer.InstancePtr.Init(this, strPrinterName, boolAllowEmail: true, strAttachmentName: "PPCollectionList");
			}
			else
			{
				// nothing to print so exit
				MessageBox.Show("No records found.", "Nothing to print", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//MDIParent.InstancePtr.Show();
				this.Close();
			}
			return;
		}

		private void FillValueFields()
		{
			// must do this or the values will not fill in correctly
			int x;
			// loop counter
			int CurrCategory;
			int lngOther;
			// accumulate category values
			int lngCategory;
			// current category's value
			if (Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name1"))) != string.Empty)
			{
				this.Fields["TheBinder"].Value = Strings.Left(FCConvert.ToString(clsCommit.Get_Fields_String("name1")), 1);
			}
			else
			{
				this.Fields["TheBinder"].Value = "";
			}
			// fill the monetary fields
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotTotal.Text = Strings.Format(lngAssessSub, "#,###,###,##0");
			txtTotTax.Text = Strings.Format(lngTaxSub, "#,###,###,##0.00");
		}

		private void rptPPCollectionSimple_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPPCollectionSimple properties;
			//rptPPCollectionSimple.Caption	= "Personal Property Collection List";
			//rptPPCollectionSimple.Icon	= "rptPPCollectionSimple.dsx":0000";
			//rptPPCollectionSimple.Left	= 0;
			//rptPPCollectionSimple.Top	= 0;
			//rptPPCollectionSimple.Width	= 13425;
			//rptPPCollectionSimple.Height	= 8490;
			//rptPPCollectionSimple.StartUpPosition	= 3;
			//rptPPCollectionSimple.SectionData	= "rptPPCollectionSimple.dsx":058A;
			//End Unmaped Properties
		}
	}
}
