﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmListings.
	/// </summary>
	public partial class frmListings : BaseForm
	{
		public frmListings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmListings InstancePtr
		{
			get
			{
				return (frmListings)Sys.GetInstance(typeof(frmListings));
			}
		}

		protected frmListings _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intTaxYear;

		private void frmListings_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmListings_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmListings properties;
			//frmListings.ScaleWidth	= 9225;
			//frmListings.ScaleHeight	= 8115;
			//frmListings.LinkTopic	= "Form1";
			//frmListings.LockControls	= true;
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intOption;
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			//lblStartPage.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//Label4.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//lblPOStart.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//lblPOEnd.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			if (clsLoad.OpenRecordset("select basis,OVERPAYINTERESTRATE from COLLECTIONS", modGlobalVariables.strCLDatabase))
			{
				if (!clsLoad.EndOfFile())
				{
					modGlobalVariables.Statics.gintBasis = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("Basis"))));
					modGlobalVariables.Statics.dblOverPayRate = Conversion.Val(clsLoad.Get_Fields_Double("overpayinterestrate"));
				}
			}
			intOption = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CommitmentBindOption", "BL"))));
			if (intOption == 0)
			{
				cmbBind.SelectedIndex = 2;
			}
			else if (intOption == 1)
			{
				cmbBind.SelectedIndex = 0;
			}
			else if (intOption == 2)
			{
				cmbBind.SelectedIndex = 1;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuRun_Click(object sender, System.EventArgs e)
		{
			if (cmbReport.SelectedIndex == 0)
			{
				CommitmentBooks();
			}
			else if (cmbReport.SelectedIndex == 1)
			{
				CommitmentBooks();
			}
			else if (cmbReport.SelectedIndex == 2)
			{
				CollectionLists();
			}
			else if (cmbReport.SelectedIndex == 3)
			{
				CollectionLists();
			}
			else if (cmbReport.SelectedIndex == 4)
			{
				NotBilled();
			}
			else if (cmbReport.SelectedIndex == 5)
			{
				NotBilled();
			}
			else if (cmbReport.Text == "Mortgage Holder List")
			{
				MortgageHolderList();
			}
			else if (cmbReport.Text == "Real Estate Accounts by Mortgage Holder")
			{
				REByMortgageHolder();
			}
			else if (cmbReport.Text == "Real Estate List with Mortgage Holders")
			{
				rptREMortgage.InstancePtr.Init(cmbOrder.SelectedIndex == 1);
			}
			else if (cmbReport.Text == "Real Estate & Personal Property Association")
			{
				rptREPPAssociation.InstancePtr.Init(cmbOrder.SelectedIndex == 1);
			}
			else if (cmbReport.Text == "Real Estate Out-Print Report")
			{
				OutPrinting();
			}
			else if (cmbReport.Text == "Personal Property Out-Print Report")
			{
				OutPrinting();
			}
			else if (cmbReport.Text == "Real Estate Previous Owners")
			{
				PreviousOwnerReport();
			}
			else if (cmbReport.Text == "Real Estate Transfer Report")
			{
				frmViewer.InstancePtr.Init("Real Estate Transfer Report", "RETRANSFER", true, "RETransfer");
			}
			else if (cmbReport.Text == "Personal Property Transfer Report")
			{
				frmViewer.InstancePtr.Init("Personal Property Transfer Report", "PPTRANSFER", true, "PPTransfer");
			}
			else if (cmbReport.Text == "Custom Report")
			{
				frmCustomReport.InstancePtr.Show(App.MainForm);
			}
			else if (cmbReport.Text == "Certificate of Assessment, Commitment etc.")
			{
				if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					modGlobalVariables.Statics.CustomizedInfo.intCurrentTown = frmPickRegionalTown.InstancePtr.Init(-1, false, "Choose the town to print certificates for");
					if (modGlobalVariables.Statics.CustomizedInfo.intCurrentTown < 0)
					{
						MessageBox.Show("Cannot proceed without choosing a town", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (MessageBox.Show("The certificates and warrants are based on data from the tax rate calculator" + "\r\n" + "If this has not been run the certificates and warrants will be incorrect", "Continue?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
				{
					frmCertificateOfAssessment.InstancePtr.Init(0, modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
				}
			}
		}

		private void PreviousOwnerReport()
		{
			int intOrder = 0;
			string strStart = "";
			string strEnd = "";
			if (cmbOrder.SelectedIndex == 0)
			{
				intOrder = 0;
			}
			else if (cmbOrder.SelectedIndex == 1)
			{
				intOrder = 1;
			}
			else
			{
				intOrder = 2;
			}
			if (cmbtPreviousOwner.SelectedIndex == 0)
			{
				if (Conversion.Val(txtPOStart.Text) <= 0)
				{
					MessageBox.Show("You must enter a valid account to print this report.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				rptPreviousOwners.InstancePtr.Init(intOrder, true, FCConvert.ToInt32(Conversion.Val(txtPOStart.Text)));
			}
			else
			{
				if (!Information.IsDate(txtPOStart.Text))
				{
					MessageBox.Show("You must enter a valid start date to print this report.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					strStart = Strings.Format(txtPOStart.Text, "MM/dd/yyyy");
				}
				if (!Information.IsDate(txtPOEnd.Text))
				{
					strEnd = strStart;
				}
				else
				{
					strEnd = Strings.Format(txtPOEnd.Text, "MM/dd/yyyy");
				}
				rptPreviousOwners.InstancePtr.Init(intOrder, false, 0, strStart, strEnd);
			}
		}

		private void REByMortgageHolder()
		{
			int lngYear;
			if (Conversion.Val(txtYear.Text) < 1990 || Conversion.Val(txtYear.Text) > 2030)
			{
				MessageBox.Show("You must enter a valid tax year.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
			//FC:FINAL:RPU:#i1571 - Compare with index 0 because the first item was temporary removed till will be maked enabled again
			//rptREbyHolder.InstancePtr.Init(cmbOrder.SelectedIndex == 1, cmbOptions.SelectedIndex == 0, lngYear, Strings.Trim(txtMortStart.Text), Strings.Trim(txtMortEnd.Text));
			rptREbyHolder.InstancePtr.Init(cmbOrder.SelectedIndex == 0, cmbOptions.SelectedIndex == 0, lngYear, Strings.Trim(txtMortStart.Text), Strings.Trim(txtMortEnd.Text));
		}

		private void MortgageHolderList()
		{
			//FC:FINAL:CHN - issue #1371: Fix changed selected index after removing "Account" from selecting.
			// rptMortgageHolderMaster.InstancePtr.Init(cmbOrder.SelectedIndex == 1, Strings.Trim(txtMortStart.Text), Strings.Trim(txtMortEnd.Text));
			rptMortgageHolderMaster.InstancePtr.Init(cmbOrder.SelectedIndex == 0, Strings.Trim(txtMortStart.Text), Strings.Trim(txtMortEnd.Text));
		}

		private void CommitmentBooks()
		{
			int x;
			// loop counter
			int NumFonts;
			// number of fonts this printer has
			bool boolUseFont;
			// did we find a printer font?
			string strFont = "";
			// font name
			string strPrinterName = "";
			// devicename
			int intWhichReport = 0;
			// reg,landscape, or wide
			int intCPI;
			// how many cpi we are looking for
			int intPageStart = 0;
			// number to start page numbers at
			bool boolNewPage;
			// true if chknewpage is checked
			int intOrder = 0;
			int intBindOption = 0;
			if (Conversion.Val(txtStart.Text) > 0)
			{
				intPageStart = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStart.Text)));
			}
			else
			{
				intPageStart = 1;
			}
			if (cmbOrder.SelectedIndex == 0)
			{
				intOrder = 1;
			}
			else if (cmbOrder.SelectedIndex == 1)
			{
				intOrder = 2;
			}
			else
			{
				intOrder = 3;
			}
			if (Conversion.Val(txtYear.Text) < 1990 || Conversion.Val(txtYear.Text) > 2030)
			{
				MessageBox.Show("You must pick a valid year.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			boolNewPage = chkNewPage.CheckState == Wisej.Web.CheckState.Checked;
			// make them choose the printer so we can set the correct printer font
			// MDIParent.CommonDialog1.ShowPrinter
			// 
			// strPrinterName = FCGlobal.Printer.DeviceName
			// 
			// NumFonts = Printer.FontCount
			// boolUseFont = False
			if (cmbBind.SelectedIndex == 0)
			{
				intBindOption = 1;
			}
			else if (cmbBind.SelectedIndex == 1)
			{
				intBindOption = 2;
			}
			else
			{
				intBindOption = 0;
			}
			if (cmbPrint.SelectedIndex == 0)
			{
				// regular
				intWhichReport = 1;
			}
			else if (cmbPrint.SelectedIndex == 1)
			{
				// landscape
				intWhichReport = 2;
			}
			else
			{
				// wide printer
				intWhichReport = 3;
			}
			modRegistry.SaveRegistryKey("CommitmentBindOption", FCConvert.ToString(intBindOption), "BL");
			frmPickRate.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), cmbReport.SelectedIndex == 0, intPageStart, intWhichReport, boolNewPage, intOrder, Strings.Trim(txtMortStart.Text), Strings.Trim(txtMortEnd.Text), intBindOption);
			// If intWhichReport = 1 Then
			// get a 17 cpi printer font if possible
			// For X = 0 To NumFonts - 1
			// strFont = Printer.Fonts(X)
			// If UCase(Right(strFont, 3)) = "CPI" Then
			// strFont = Mid(strFont, 1, Len(strFont) - 3)
			// If Right(strFont, 2) = "17" Or Right(strFont, 3) = "17 " Then
			// boolUseFont = True
			// strFont = Printer.Fonts(X)
			// Exit For
			// End If
			// End If
			// Next X
			// 
			// which commitment book are we printing?
			// If optReport(0).Value Then
			// Call rptCommitment.Init(Val(Trim(txtYear.Text)), boolUseFont, strFont, strPrinterName, intPageStart, boolNewPage)
			// Else
			// Call rptPPCommitment.Init(Val(Trim(txtYear.Text)), boolUseFont, strFont, strPrinterName, intPageStart, boolNewPage)
			// End If
			// Else
			// look for a 12 cpi font
			// If intWhichReport = 2 Then
			// intCPI = 12
			// Else
			// intCPI = 12
			// End If
			// 
			// For X = 0 To NumFonts - 1
			// strFont = Printer.Fonts(X)
			// If UCase(Right(strFont, 3)) = "CPI" Then
			// strFont = Mid(strFont, 1, Len(strFont) - 3)
			// If Val(Right(strFont, 2)) = intCPI Or Val(Right(strFont, 3)) = intCPI Then
			// boolUseFont = True
			// strFont = Printer.Fonts(X)
			// Exit For
			// End If
			// End If
			// Next X
			// 
			// 
			// If optReport(0).Value Then
			// Call rptCommitmentWide.Init(Val(Trim(txtYear.Text)), boolUseFont, strFont, strPrinterName, intWhichReport, intPageStart, boolNewPage)
			// Else
			// Call rptPPCommitmentWide.Init(Val(Trim(txtYear.Text)), boolUseFont, strFont, strPrinterName, intWhichReport, intPageStart, boolNewPage)
			// End If
			// End If
		}

		private void CollectionLists()
		{
			double dblDiscount = 0;
			int intOrder = 0;
			int intWhichReport = 0;
			int intCPI = 0;
			string strFont = "";
			int NumFonts;
			string strPrinterName;
			bool boolUseFont;
			int x;
			if (Conversion.Val(txtYear.Text) < 1990 || Conversion.Val(txtYear.Text) > 2030)
			{
				MessageBox.Show("You must enter a valid tax year.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkOptions.CheckState == Wisej.Web.CheckState.Checked)
			{
				dblDiscount = Conversion.Val(txtStart.Text);
			}
			else
			{
				dblDiscount = 0;
			}
			if (cmbOrder.SelectedIndex == 0)
			{
				intOrder = 1;
			}
			else if (cmbOrder.SelectedIndex == 1)
			{
				intOrder = 2;
			}
			else
			{
				intOrder = 3;
			}
			// make them choose the printer so we can set the correct printer font
			//FC:FINAL:CHN - i.issue #1544: error when printing errors.
			//MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
			//MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
			strPrinterName = FCGlobal.Printer.DeviceName;
			NumFonts = FCGlobal.Printer.Fonts.Count;
			boolUseFont = false;
			if (cmbPrint.SelectedIndex == 0)
			{
				// regular
				intWhichReport = 1;
			}
			else if (cmbPrint.SelectedIndex == 1)
			{
				// landscape
				intWhichReport = 2;
			}
			else
			{
				// wide printer
				intWhichReport = 3;
			}
			if (intWhichReport == 1)
			{
				if (cmbOptions.SelectedIndex == 0)
				{
					// get a 10 cpi printer font if possible
					for (x = 0; x <= NumFonts - 1; x++)
					{
						strFont = FCGlobal.Printer.Fonts[x];
						if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
						{
							strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
							if (Strings.Right(strFont, 2) == "10" || Strings.Right(strFont, 3) == "10 ")
							{
								boolUseFont = true;
								strFont = FCGlobal.Printer.Fonts[x];
								break;
							}
						}
					}
					// x
					// which commitment book are we printing?
					if (cmbReport.SelectedIndex == 2)
					{
						rptRECollectionSimple.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, dblDiscount, intOrder, chkAddress.CheckState == Wisej.Web.CheckState.Checked);
					}
					else
					{
						rptPPCollectionSimple.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, dblDiscount, intOrder, chkAddress.CheckState == Wisej.Web.CheckState.Checked);
					}
				}
				else
				{
					// get a 17 cpi printer font if possible
					for (x = 0; x <= NumFonts - 1; x++)
					{
						strFont = FCGlobal.Printer.Fonts[x];
						if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
						{
							strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
							if (Strings.Right(strFont, 2) == "17" || Strings.Right(strFont, 3) == "17 ")
							{
								boolUseFont = true;
								strFont = FCGlobal.Printer.Fonts[x];
								break;
							}
						}
					}
					// x
					// which commitment book are we printing?
					if (cmbReport.SelectedIndex == 2)
					{
						rptRECollectionList.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, dblDiscount, intOrder, chkAddress.CheckState == Wisej.Web.CheckState.Checked);
					}
					else
					{
						rptPPCollectionList.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, dblDiscount, intOrder, chkAddress.CheckState == Wisej.Web.CheckState.Checked);
					}
				}
			}
			else
			{
				// look for a 12 cpi font
				if (intWhichReport == 2)
				{
					intCPI = 12;
				}
				else
				{
					intCPI = 12;
				}
				for (x = 0; x <= NumFonts - 1; x++)
				{
					strFont = FCGlobal.Printer.Fonts[x];
					if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
					{
						strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
						if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
						{
							boolUseFont = true;
							strFont = FCGlobal.Printer.Fonts[x];
							break;
						}
					}
				}
				// x
				if (cmbReport.SelectedIndex == 2)
				{
					rptRECollectionWide.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, intWhichReport, dblDiscount, intOrder, chkAddress.CheckState == Wisej.Web.CheckState.Checked);
				}
				else
				{
					rptPPCollectionWide.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtYear.Text))), boolUseFont, strFont, strPrinterName, intWhichReport, dblDiscount, intOrder, chkAddress.CheckState == Wisej.Web.CheckState.Checked);
				}
			}
		}

		private void NotBilled()
		{
			if (Conversion.Val(txtYear.Text) < 1990 || Conversion.Val(txtYear.Text) > 2030)
			{
				MessageBox.Show("You must enter a valid tax year.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbReport.SelectedIndex == 4)
			{
				// real estate
				rptRENotBilled.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtYear.Text)));
			}
			else
			{
				// personal property
				rptPPNotBilled.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtYear.Text)));
			}
		}

		private void Mortgages()
		{
		}

		private void OutPrinting()
		{
			int intYear = 0;
			int intOrder = 0;
			bool boolUseFont;
			int NumFonts;
			string strPrinterName;
			int intWhichReport = 0;
			int x;
			string strFont = "";
			int intCPI = 0;
			// If Val(txtYear.Text) < 1990 Or Val(txtYear.Text) > 2030 Then
			// MsgBox "You must enter a valid tax year.", vbExclamation, "Invalid Year"
			// Exit Sub
			// End If
			// intYear = Val(txtYear.Text)
			// If optOrder(0).Value Then
			// intOrder = 1
			// ElseIf optOrder(1).Value Then
			// intOrder = 2
			// Else
			// intOrder = 3
			// End If
			// 
			if (cmbPrint.SelectedIndex == 0)
			{
				intWhichReport = 1;
			}
			else if (cmbPrint.SelectedIndex == 1)
			{
				intWhichReport = 2;
			}
			else
			{
				intWhichReport = 3;
			}
			// make them choose the printer so we can set the correct printer font
			//FC:FINAL:DDU:#i1563 - initialize CommonDialog1
			//MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
			//MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
			strPrinterName = FCGlobal.Printer.DeviceName;
			NumFonts = FCGlobal.Printer.Fonts.Count;
			boolUseFont = false;
			if (intWhichReport == 1)
			{
				// get a 17 cpi printer font if possible
				for (x = 0; x <= NumFonts - 1; x++)
				{
					strFont = FCGlobal.Printer.Fonts[x];
					if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
					{
						strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
						if (Strings.Right(strFont, 2) == "17" || Strings.Right(strFont, 3) == "17 ")
						{
							boolUseFont = true;
							strFont = FCGlobal.Printer.Fonts[x];
							break;
						}
					}
				}
				// x
				// which commitment book are we printing?
				if (cmbReport.Text == "Real Estate Out-Print Report")
				{
					rptRENormalOutprint.InstancePtr.Init(intOrder, intYear, boolUseFont, strFont, strPrinterName);
				}
				else
				{
					rptPPOutPrint.InstancePtr.Init(intOrder, intYear, boolUseFont, strFont, strPrinterName);
				}
			}
			else
			{
				// look for a 12 cpi font
				if (intWhichReport == 2)
				{
					intCPI = 12;
				}
				else
				{
					intCPI = 12;
				}
				for (x = 0; x <= NumFonts - 1; x++)
				{
					strFont = FCGlobal.Printer.Fonts[x];
					if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
					{
						strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
						if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
						{
							boolUseFont = true;
							strFont = FCGlobal.Printer.Fonts[x];
							break;
						}
					}
				}
				// x
				if (cmbReport.Text == "Real Estate Out-Print Report")
				{
					rptREOutPrint.InstancePtr.Init(intOrder, intYear, boolUseFont, strFont, strPrinterName, intWhichReport);
				}
				else
				{
					rptPPOutPrintWide.InstancePtr.Init(intOrder, intYear, boolUseFont, strFont, strPrinterName, intWhichReport);
				}
			}
		}

		private void OptPreviousOwner_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// account
						lblPOEnd.Visible = false;
						lblPOStart.Visible = false;
						txtPOEnd.Visible = false;
						if (cmbOrder.SelectedIndex == 2)
						{
							cmbOrder.SelectedIndex = 0;
						}
						cmbOrder.Items.RemoveAt(2);
						break;
					}
				case 1:
					{
						// saledate
						lblPOEnd.Visible = true;
						lblPOStart.Visible = true;
						txtPOEnd.Visible = true;
						cmbOrder.Items.Insert(2, "Account");
						break;
					}
			}
			//end switch
		}

		private void OptPreviousOwner_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbtPreviousOwner.SelectedIndex);
			OptPreviousOwner_CheckedChanged(index, sender, e);
		}

		private void optPrint_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 2:
					{
						cmbBind.Visible = true;
						lblBind.Visible = true;
						break;
					}
				default:
					{
						cmbBind.Visible = false;
						lblBind.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void optPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbPrint.SelectedIndex);
			optPrint_CheckedChanged(index, sender, e);
		}

		private void optReport_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			string strMessage = "";
			framMortgageHolder.Visible = false;
			framCommitOptions.Visible = false;
			switch (cmbReport.Text)
			{
				case "Real Estate Commitment Book":
				case "Personal Property Commitment Book":
					{
						// commitment books
						framOptions.Visible = false;
						lblPrint.Visible = true;
						cmbPrint.Visible = true;
						framTaxYear.Visible = true;
						strMessage = "Page Number to assign to first page of report. Not the first page to print.";
						txtStart.Visible = true;
						lblStartPage.Text = "Start Page";
						ToolTip1.SetToolTip(txtStart, strMessage);
						ToolTip1.SetToolTip(lblStartPage, strMessage);
						chkNewPage.Visible = true;
						framCommitOptions.Visible = true;
						Frame2.Visible = false;
						framMortgageHolder.Visible = true;
						break;
					}
				case "Real Estate Collection List":
				case "Personal Property Collection List":
					{
						// collection lists
						framOptions.Visible = true;
						lblPrint.Visible = true;
						cmbPrint.Visible = true;
						framTaxYear.Visible = true;
						txtStart.Visible = true;
						lblStartPage.Text = "Discount";
						ToolTip1.SetToolTip(txtStart, "");
						ToolTip1.SetToolTip(lblStartPage, "");
						chkNewPage.Visible = false;
						Frame2.Visible = false;
						chkAddress.Visible = true;
						break;
					}
				case "Real Estate Accounts Not Billed":
				case "Personal Property Accounts Not Billed":
					{
						framOptions.Visible = false;
						lblPrint.Visible = false;
						cmbPrint.Visible = false;
						framTaxYear.Visible = true;
						lblStartPage.Text = "";
						ToolTip1.SetToolTip(lblStartPage, "");
						chkNewPage.Visible = false;
						txtStart.Visible = false;
						Frame2.Visible = false;
						break;
					}
				case "Real Estate Accounts by Mortgage Holder":
					{
						framOptions.Visible = true;
						lblPrint.Visible = false;
						cmbPrint.Visible = false;
						framTaxYear.Visible = true;
						lblStartPage.Text = "";
						ToolTip1.SetToolTip(lblStartPage, "");
						chkNewPage.Visible = false;
						txtStart.Visible = false;
						if (cmbOptions.Items.Contains("Show Just Total and Tax"))
						{
							cmbOptions.Items.Remove("Show Just Total and Tax");
							cmbOptions.Items.Insert(0, "Show Tax");
						}
						else if (!cmbOptions.Items.Contains("Show Tax"))
						{
							cmbOptions.Items.Insert(0, "Show Tax");
						}
						cmbOptions.SelectedIndex = 0;
						if (cmbOptions.Items.Contains("Show Land/Building/Exempt"))
						{
							cmbOptions.Items.Remove("Show Land/Building/Exempt");
							cmbOptions.Items.Add("Don't Show Tax");
						}
						else if (!cmbOptions.Items.Contains("Don't Show Tax"))
						{
							cmbOptions.Items.Insert(1, "Don't Show Tax");
						}
						chkOptions.Text = "";
						chkOptions.Enabled = false;
						Frame2.Visible = false;
						framMortgageHolder.Visible = true;
						break;
					}
				case "Real Estate Out-Print Report":
				case "Personal Property Out-Print Report":
					{
						framOptions.Visible = false;
						lblPrint.Visible = true;
						cmbPrint.Visible = true;
						framTaxYear.Visible = false;
						lblStartPage.Text = "";
						ToolTip1.SetToolTip(lblStartPage, "");
						chkNewPage.Visible = false;
						txtStart.Visible = false;
						Frame2.Visible = false;
						break;
					}
				case "Real Estate Previous Owners":
					{
						framOptions.Visible = false;
						lblPrint.Visible = false;
						cmbPrint.Visible = false;
						framTaxYear.Visible = false;
						Frame2.Visible = true;
						break;
					}
				case "Certificate of Assessment, Commitment etc.":
					{
						framOptions.Visible = false;
						lblPrint.Visible = false;
						cmbPrint.Visible = false;
						framTaxYear.Visible = false;
						chkNewPage.Visible = false;
						lblOrder.Visible = false;
						cmbOrder.Visible = false;
						Frame2.Visible = false;
						break;
					}
				default:
					{
						framOptions.Visible = false;
						lblPrint.Visible = false;
						cmbPrint.Visible = false;
						framTaxYear.Visible = false;
						chkNewPage.Visible = false;
						lblOrder.Visible = true;
						cmbOrder.Visible = true;
						Frame2.Visible = false;
						break;
					}
			}
			//end switch
			switch (cmbReport.Text)
			{
				case "Real Estate Accounts Not Billed":
				case "Personal Property Accounts Not Billed":
				case "Real Estate Transfer Report":
				case "Personal Property Transfer Report":
				case "Real Estate Out-Print Report":
				case "Personal Property Out-Print Report":
				case "Custom Report":
					{
						lblOrder.Visible = false;
						cmbOrder.Visible = false;
						break;
					}
				case "Real Estate Commitment Book":
				case "Real Estate Collection List":
                    {
                        lblOrder.Visible = true;
                        cmbOrder.Visible = true;
                        cmbOrder.Items.Clear();
                        cmbOrder.Items.AddRange(new object[] {
                        "Account",
                        "Name",
                        "Map / Lot" });
                        cmbOrder.SelectedIndex = 1;
						break;
					}
				case "Mortgage Holder List":
				case "Real Estate Accounts by Mortgage Holder":
					{
						lblOrder.Visible = true;
						cmbOrder.Visible = true;						
                        cmbOrder.Items.Clear();
                        cmbOrder.Items.AddRange(new object[] {
                        "Name",
                        "Mortgage Holder ID"});
                        // Case 3
                        // framOrderBy.Visible = True
                        // optOrder(2).Enabled = True
                        // optOrder(0).Enabled = True
                        // optOrder(2).Caption = "Map / Lot"
                        //FC:FINAL:CHN - i.issue 1541, issue #1371 : Delete option "Account" from selected "Mortgage Holder List". 
                        cmbOrder.SelectedIndex = 0;
						break;
					}
				case "Personal Property Commitment Book":
				case "Personal Property Collection List":
				case "Real Estate List with Mortgage Holders":
				case "Real Estate & Personal Property Association":
					{
						lblOrder.Visible = true;
						cmbOrder.Visible = true;
                        // no map/lot
                        cmbOrder.Items.Clear();
                        cmbOrder.Items.AddRange(new object[] {
                        "Account",
                        "Name"});
                        cmbOrder.SelectedIndex = 1;
                        break;
					}
				case "Real Estate Previous Owners":
					{
                        lblOrder.Visible = true;
                        cmbOrder.Visible = true;
                        cmbOrder.Items.Clear();
                        cmbOrder.Items.AddRange(new object[] {
                        "Sale Date",
                        "Name",
                        "Account"});
						if (cmbtPreviousOwner.SelectedIndex == 0)
						{
							if (cmbOrder.SelectedIndex == 2)
							{
								cmbOrder.SelectedIndex = 0;
							}
							cmbOrder.Items.RemoveAt(2);
						}
                        cmbOrder.SelectedIndex = 0;
                        break;
					}
			}
			//end switch
			if (cmbReport.Text != "Real Estate Accounts by Mortgage Holder")
			{
				chkOptions.Enabled = true;
				//FC:FINAL:CHN - issue #1371: Delete option "Account" from selected "Mortgage Holder List". 
				// if (((ComboBox)sender).SelectedItem != "Real Estate Previous Owners")
				if (cmbReport.Text != "Real Estate Previous Owners" && cmbReport.Text != "Mortgage Holder List")
				{
					//optOrder[0].Text = "Account";
					if (cmbOrder.Items.Contains("Sale Date"))
					{
						cmbOrder.Items.Remove("Sale Date");
						cmbOrder.Items.Insert(0, "Account");
					}
					else if (!cmbOrder.Items.Contains("Account"))
					{
						cmbOrder.Items.Insert(0, "Account");
					}
				}
				if (cmbOptions.Items.Contains("Show Tax"))
				{
					cmbOptions.Items.Remove("Show Tax");
					cmbOptions.Items.Insert(0, "Show Just Total and Tax");
				}
				else if (!cmbOptions.Items.Contains("Show Just Total and Tax"))
				{
					cmbOptions.Items.Insert(0, "Show Just Total and Tax");
				}
				if (cmbOptions.Items.Contains("Don't Show Tax"))
				{
					cmbOptions.Items.Remove("Don't Show Tax");
					cmbOptions.Items.Add("Show Land/Building/Exempt");
				}
				else if (!cmbOptions.Items.Contains("Show Land/Building/Exempt"))
				{
					cmbOptions.Items.Insert(1, "Show Land/Building/Exempt");
				}
                cmbOptions.SelectedIndex = 1;
				chkOptions.Text = "Show Discount";
			}
			if (cmbReport.Text != "Real Estate Collection List" && cmbReport.Text != "Personal Property Collection List")
			{
				chkAddress.Visible = false;
			}
			if (cmbReport.Text == "Mortgage Holder List")
			{
				framMortgageHolder.Visible = true;
			}
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbReport.SelectedIndex);
			optReport_CheckedChanged(index, sender, e);
		}

		private void cmbReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			optReport_CheckedChanged(sender, e);
		}

		private void cmbtPreviousOwner_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbtPreviousOwner.SelectedIndex == 0)
			{
				OptPreviousOwner_CheckedChanged(sender, e);
			}
			else if (cmbtPreviousOwner.SelectedIndex == 1)
			{
				OptPreviousOwner_CheckedChanged(sender, e);
			}
		}

		private void cmbPrint_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:CHN - i.issue #1541: incorrect options for different types of reports.
			// if (cmbPrint.SelectedIndex == 0)
			// {
			optPrint_CheckedChanged(sender, e);
			// }
		}
	}
}
