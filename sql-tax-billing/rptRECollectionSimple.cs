﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptRECollectionSimple.
	/// </summary>
	public partial class rptRECollectionSimple : BaseSectionReport
	{
		public rptRECollectionSimple()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate Collection List";
		}

		public static rptRECollectionSimple InstancePtr
		{
			get
			{
				return (rptRECollectionSimple)Sys.GetInstance(typeof(rptRECollectionSimple));
			}
		}

		protected rptRECollectionSimple _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCommit.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRECollectionSimple	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int TaxYear;
		clsDRWrapper clsCommit = new clsDRWrapper();
		string Addr1 = "";
		string Addr2 = "";
		string addr3 = "";
		private string strMailingAddress3 = "";
		int intPage;
		int intStartPage;
		double lngAssessSub;
		double lngTaxSub;
		double dblDisc;
		int lngPage;
		bool boolIncludeAddress;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
		//	clsDRWrapper clsTemp = new clsDRWrapper();
			int x;
			lngAssessSub = 0;
			lngTaxSub = 0;
			lngPage = 1;
			// add these fields so the report can do the subtotals for us
			this.Fields.Add("TheBinder");
			txtDate.Text = DateTime.Today.ToShortDateString();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// if no more records, then end the report
			eArgs.EOF = clsCommit.EndOfFile();
			if (!eArgs.EOF)
			{
				FillValueFields();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			// fill in all the fields
			if (!clsCommit.EndOfFile())
			{
				double dblDiscount = 0;
				double dblTax = 0;
				txtLocation.Text = "";
				txtMapLot.Text = "";
				txtBookPage.Text = "";
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = clsCommit.Get_Fields_String("account");
				txtName.Text = clsCommit.Get_Fields_String("name1");
				dblTax = Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
				lngTaxSub += dblTax;
				txtTotal.Value = Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue")) + Conversion.Val(clsCommit.Get_Fields_Int32("landvalue"));
				txtTax.Value = dblTax;
				lngAssessSub += Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue")) + Conversion.Val(clsCommit.Get_Fields_Int32("landvalue"));
				dblDiscount = modMain.Round(dblDisc * dblTax, 2);
				txtDiscount.Text = Strings.Format(dblDiscount, "#,###,###,##0.00");
				if (boolIncludeAddress)
				{
					Addr1 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address1")));
					Addr2 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address2")));
					addr3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address3")));
					strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("MailingAddress3")));
				}
				else
				{
					Addr1 = "";
					Addr2 = "";
					addr3 = "";
					strMailingAddress3 = "";
				}
				// if there are blank address lines condense them
				if (Strings.Trim(Addr1) == string.Empty)
				{
					Addr1 = Addr2;
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = "";
					if (Strings.Trim(Addr1) == string.Empty)
					{
						Addr1 = Addr2;
						Addr2 = strMailingAddress3;
						strMailingAddress3 = addr3;
						addr3 = "";
					}
				}
				if (Strings.Trim(Addr2) == string.Empty)
				{
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = "";
				}
				txtAddress1.Text = Addr1;
				txtAddress2.Text = Addr2;
				txtAddress3.Text = addr3;
				txtMailingAddress3.Text = strMailingAddress3;
				if (boolIncludeAddress)
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtLocation.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
					txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("maplot")));
					txtBookPage.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("bookpage")));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtAddress1.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
					txtAddress2.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("maplot")));
					txtMailingAddress3.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("bookpage")));
					txtAddress3.Text = "";
				}
				clsCommit.MoveNext();
			}
		}
		// vbPorter upgrade warning: lngTaxYear As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngTaxYear, bool boolSetFont, string strFontName, string strPrinterName, double dblDiscount, int intOrder, bool boolPrintAddress = true)
		{
			string strOrder = "";
			int x;
			// take in the printer and font to use as well as the taxyear to make the bill for
			TaxYear = lngTaxYear;
			boolIncludeAddress = boolPrintAddress;
			if (!boolIncludeAddress)
			{
				this.Detail.Height -= (225 * 4 / 1440f);
			}
			if (dblDiscount == 0)
			{
				txtDiscount.Visible = false;
				lblDiscount.Visible = false;
			}
			else
			{
				dblDisc = modMain.Round((dblDiscount / 100), 4);
			}
			switch (intOrder)
			{
				case 2:
					{
						strOrder = "name1";
						break;
					}
				case 1:
					{
						strOrder = "account";
						break;
					}
				case 3:
					{
						strOrder = "maplot";
						break;
					}
			}
			//end switch
			intStartPage = 1;
			clsCommit.OpenRecordset("select * from billingmaster where billingyear = " + FCConvert.ToString(TaxYear) + "1 and billingtype = 'RE' order by " + strOrder, "twcl0000.vb1");
			if (!clsCommit.EndOfFile())
			{
				lblTitle.Text = lblTitle.Text + FCConvert.ToString(TaxYear);
				lblMuniname.Text = modGlobalConstants.Statics.MuniName;
				if (boolSetFont)
				{
					// now set each box and label to the correct printer font
					for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
					{
						// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
						bool setFont = false;
						bool bold = false;
						if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
						{
							setFont = true;
						}
						else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
						{
							setFont = true;
							bold = true;
						}
						if (setFont)
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textBox != null)
							{
								textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
								}
							}
						}
					}
					// x
					for (x = 0; x <= this.ReportFooter.Controls.Count - 1; x++)
					{
						// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
						bool setFont = false;
						bool bold = false;
						if (FCConvert.ToString(this.ReportFooter.Controls[x].Tag) == "textbox")
						{
							setFont = true;
						}
						else if (FCConvert.ToString(this.ReportFooter.Controls[x].Tag) == "bold")
						{
							setFont = true;
							bold = true;
						}
						if (setFont)
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.ReportFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textBox != null)
							{
								textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = this.ReportFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
								}
							}
						}
					}
					// x
					//this.Printer.RenderMode = 1;
				}
				// change to the selected printer
				this.Document.Printer.PrinterName = strPrinterName;
				// Me.Show , MDIParent
				//FC:FINAL:MSH - i.issue #1586: restore missing report call
				frmReportViewer.InstancePtr.Init(this, strPrinterName, boolAllowEmail: true, strAttachmentName: "RECollectionList");
			}
			else
			{
				// not records so leave
				MessageBox.Show("No records found.", "Nothing to print", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//MDIParent.InstancePtr.Show();
				this.Close();
			}
			return;
		}

		private void FillValueFields()
		{
			if (Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name1"))) != string.Empty)
			{
				this.Fields["TheBinder"].Value = Strings.Left(FCConvert.ToString(clsCommit.Get_Fields_String("name1")), 1);
			}
			else
			{
				this.Fields["TheBinder"].Value = "";
			}
			// filling these fields will fill the textboxes and the summary fields in the page footer
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalSub.Text = Strings.Format(lngAssessSub, "#,###,###,##0");
			txtTaxSub.Text = Strings.Format(lngTaxSub, "#,###,###,##0.00");
		}

		
	}
}
