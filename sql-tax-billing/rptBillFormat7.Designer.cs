﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormat7.
	/// </summary>
	partial class rptBillFormat7
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillFormat7));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPrePaidLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaidLabel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaid1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCatOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLateDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPercent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReducedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLateDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReducedBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMapLot,
				this.txtBookPage,
				this.txtAddress1,
				this.txtAddress2,
				this.txtAddress3,
				this.txtAddress4,
				this.txtRate,
				this.txtAccount,
				this.txtAcct1,
				this.lblTitle,
				this.txtPrePaidLabel,
				this.txtPrePaid,
				this.txtPrePaidLabel1,
				this.txtPrePaid1,
				this.lblTitle1,
				this.txtTaxDue,
				this.txtMailing1,
				this.txtMailing2,
				this.txtMailing3,
				this.txtMailing4,
				this.txtAssessment,
				this.txtExemption,
				this.txtExemptLabel,
				this.txtLand,
				this.txtBldg,
				this.lblLand,
				this.txtBldgLabel,
				this.txtLocation,
				this.txtCatOther,
				this.txtOtherLabel,
				this.txtName1,
				this.Field1,
				this.txtLateDate,
				this.txtPercent,
				this.txtReducedBy,
				this.txtMailing5
			});
			this.Detail.Height = 3.083333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 0F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.15625F;
			this.txtMapLot.Width = 1.9375F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.CanGrow = false;
			this.txtBookPage.Height = 0.19F;
			this.txtBookPage.Left = 2.5F;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.15625F;
			this.txtBookPage.Width = 1.5F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.CanGrow = false;
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 0.8125F;
			this.txtAddress1.MultiLine = false;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 2.3125F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.CanGrow = false;
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 0.8125F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 2.3125F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.CanGrow = false;
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.Left = 0.8125F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.46875F;
			this.txtAddress3.Width = 2.3125F;
			// 
			// txtAddress4
			// 
			this.txtAddress4.CanGrow = false;
			this.txtAddress4.Height = 0.19F;
			this.txtAddress4.Left = 0.8125F;
			this.txtAddress4.MultiLine = false;
			this.txtAddress4.Name = "txtAddress4";
			this.txtAddress4.Tag = "textbox";
			this.txtAddress4.Text = null;
			this.txtAddress4.Top = 0.625F;
			this.txtAddress4.Width = 2.3125F;
			// 
			// txtRate
			// 
			this.txtRate.CanGrow = false;
			this.txtRate.Height = 0.19F;
			this.txtRate.Left = 4F;
			this.txtRate.MultiLine = false;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "text-align: right";
			this.txtRate.Tag = "textbox";
			this.txtRate.Text = null;
			this.txtRate.Top = 0.3125F;
			this.txtRate.Width = 0.9375F;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 5.5F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.3125F;
			this.txtAccount.Width = 1.125F;
			// 
			// txtAcct1
			// 
			this.txtAcct1.CanGrow = false;
			this.txtAcct1.Height = 0.19F;
			this.txtAcct1.Left = 7.625F;
			this.txtAcct1.MultiLine = false;
			this.txtAcct1.Name = "txtAcct1";
			this.txtAcct1.Tag = "textbox";
			this.txtAcct1.Text = null;
			this.txtAcct1.Top = 0.3125F;
			this.txtAcct1.Width = 1.125F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.19F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 4.5625F;
			this.lblTitle.MultiLine = false;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "";
			this.lblTitle.Tag = "textbox";
			this.lblTitle.Text = null;
			this.lblTitle.Top = 0.625F;
			this.lblTitle.Width = 0.5625F;
			// 
			// txtPrePaidLabel
			// 
			this.txtPrePaidLabel.CanGrow = false;
			this.txtPrePaidLabel.Height = 0.19F;
			this.txtPrePaidLabel.Left = 5.1875F;
			this.txtPrePaidLabel.MultiLine = false;
			this.txtPrePaidLabel.Name = "txtPrePaidLabel";
			this.txtPrePaidLabel.Tag = "textbox";
			this.txtPrePaidLabel.Text = "Paid:";
			this.txtPrePaidLabel.Top = 0.625F;
			this.txtPrePaidLabel.Width = 0.5625F;
			// 
			// txtPrePaid
			// 
			this.txtPrePaid.CanGrow = false;
			this.txtPrePaid.Height = 0.19F;
			this.txtPrePaid.Left = 5.75F;
			this.txtPrePaid.MultiLine = false;
			this.txtPrePaid.Name = "txtPrePaid";
			this.txtPrePaid.Tag = "textbox";
			this.txtPrePaid.Text = null;
			this.txtPrePaid.Top = 0.625F;
			this.txtPrePaid.Width = 0.875F;
			// 
			// txtPrePaidLabel1
			// 
			this.txtPrePaidLabel1.CanGrow = false;
			this.txtPrePaidLabel1.Height = 0.19F;
			this.txtPrePaidLabel1.Left = 7F;
			this.txtPrePaidLabel1.MultiLine = false;
			this.txtPrePaidLabel1.Name = "txtPrePaidLabel1";
			this.txtPrePaidLabel1.Tag = "textbox";
			this.txtPrePaidLabel1.Text = "Paid:";
			this.txtPrePaidLabel1.Top = 0.625F;
			this.txtPrePaidLabel1.Width = 0.5625F;
			// 
			// txtPrePaid1
			// 
			this.txtPrePaid1.CanGrow = false;
			this.txtPrePaid1.Height = 0.19F;
			this.txtPrePaid1.Left = 7.5625F;
			this.txtPrePaid1.MultiLine = false;
			this.txtPrePaid1.Name = "txtPrePaid1";
			this.txtPrePaid1.Tag = "textbox";
			this.txtPrePaid1.Text = null;
			this.txtPrePaid1.Top = 0.625F;
			this.txtPrePaid1.Width = 0.875F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.19F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 8.5625F;
			this.lblTitle1.MultiLine = false;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "";
			this.lblTitle1.Tag = "textbox";
			this.lblTitle1.Text = null;
			this.lblTitle1.Top = 0.625F;
			this.lblTitle1.Width = 0.5F;
			// 
			// txtTaxDue
			// 
			this.txtTaxDue.CanGrow = false;
			this.txtTaxDue.Height = 0.19F;
			this.txtTaxDue.Left = 5.5625F;
			this.txtTaxDue.MultiLine = false;
			this.txtTaxDue.Name = "txtTaxDue";
			this.txtTaxDue.Style = "text-align: right";
			this.txtTaxDue.Tag = "textbox";
			this.txtTaxDue.Text = null;
			this.txtTaxDue.Top = 2.71875F;
			this.txtTaxDue.Width = 1.25F;
			// 
			// txtMailing1
			// 
			this.txtMailing1.CanGrow = false;
			this.txtMailing1.Height = 0.19F;
			this.txtMailing1.Left = 1F;
			this.txtMailing1.MultiLine = false;
			this.txtMailing1.Name = "txtMailing1";
			this.txtMailing1.Tag = "textbox";
			this.txtMailing1.Text = null;
			this.txtMailing1.Top = 1.78125F;
			this.txtMailing1.Width = 3F;
			// 
			// txtMailing2
			// 
			this.txtMailing2.CanGrow = false;
			this.txtMailing2.Height = 0.19F;
			this.txtMailing2.Left = 1F;
			this.txtMailing2.MultiLine = false;
			this.txtMailing2.Name = "txtMailing2";
			this.txtMailing2.Tag = "textbox";
			this.txtMailing2.Text = null;
			this.txtMailing2.Top = 1.9375F;
			this.txtMailing2.Width = 3F;
			// 
			// txtMailing3
			// 
			this.txtMailing3.CanGrow = false;
			this.txtMailing3.Height = 0.19F;
			this.txtMailing3.Left = 1F;
			this.txtMailing3.MultiLine = false;
			this.txtMailing3.Name = "txtMailing3";
			this.txtMailing3.Tag = "textbox";
			this.txtMailing3.Text = null;
			this.txtMailing3.Top = 2.09375F;
			this.txtMailing3.Width = 3F;
			// 
			// txtMailing4
			// 
			this.txtMailing4.CanGrow = false;
			this.txtMailing4.Height = 0.19F;
			this.txtMailing4.Left = 1F;
			this.txtMailing4.MultiLine = false;
			this.txtMailing4.Name = "txtMailing4";
			this.txtMailing4.Tag = "textbox";
			this.txtMailing4.Text = null;
			this.txtMailing4.Top = 2.25F;
			this.txtMailing4.Width = 3F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.CanGrow = false;
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 4.875F;
			this.txtAssessment.MultiLine = false;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Tag = "textbox";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 1.9375F;
			this.txtAssessment.Width = 1.3125F;
			// 
			// txtExemption
			// 
			this.txtExemption.CanGrow = false;
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 5.3125F;
			this.txtExemption.MultiLine = false;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Tag = "textbox";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 1.46875F;
			this.txtExemption.Width = 1.3125F;
			// 
			// txtExemptLabel
			// 
			this.txtExemptLabel.CanGrow = false;
			this.txtExemptLabel.Height = 0.19F;
			this.txtExemptLabel.Left = 4.1875F;
			this.txtExemptLabel.MultiLine = false;
			this.txtExemptLabel.Name = "txtExemptLabel";
			this.txtExemptLabel.Tag = "textbox";
			this.txtExemptLabel.Text = "Exemption";
			this.txtExemptLabel.Top = 1.46875F;
			this.txtExemptLabel.Width = 1.0625F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 5.3125F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 1.15625F;
			this.txtLand.Width = 1.3125F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 5.3125F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 1.3125F;
			this.txtBldg.Width = 1.3125F;
			// 
			// lblLand
			// 
			this.lblLand.CanGrow = false;
			this.lblLand.Height = 0.19F;
			this.lblLand.Left = 4.1875F;
			this.lblLand.MultiLine = false;
			this.lblLand.Name = "lblLand";
			this.lblLand.Tag = "textbox";
			this.lblLand.Text = "Land";
			this.lblLand.Top = 1.15625F;
			this.lblLand.Width = 0.9375F;
			// 
			// txtBldgLabel
			// 
			this.txtBldgLabel.CanGrow = false;
			this.txtBldgLabel.Height = 0.19F;
			this.txtBldgLabel.Left = 4.1875F;
			this.txtBldgLabel.MultiLine = false;
			this.txtBldgLabel.Name = "txtBldgLabel";
			this.txtBldgLabel.Tag = "textbox";
			this.txtBldgLabel.Text = "Buildings";
			this.txtBldgLabel.Top = 1.3125F;
			this.txtBldgLabel.Width = 1.0625F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 2.6875F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 2.71875F;
			this.txtLocation.Width = 2.3125F;
			// 
			// txtCatOther
			// 
			this.txtCatOther.CanGrow = false;
			this.txtCatOther.Height = 0.19F;
			this.txtCatOther.Left = 5.3125F;
			this.txtCatOther.MultiLine = false;
			this.txtCatOther.Name = "txtCatOther";
			this.txtCatOther.Style = "text-align: right";
			this.txtCatOther.Tag = "textbox";
			this.txtCatOther.Text = null;
			this.txtCatOther.Top = 1.625F;
			this.txtCatOther.Width = 1.3125F;
			// 
			// txtOtherLabel
			// 
			this.txtOtherLabel.CanGrow = false;
			this.txtOtherLabel.Height = 0.19F;
			this.txtOtherLabel.Left = 4.1875F;
			this.txtOtherLabel.MultiLine = false;
			this.txtOtherLabel.Name = "txtOtherLabel";
			this.txtOtherLabel.Tag = "textbox";
			this.txtOtherLabel.Text = "Other";
			this.txtOtherLabel.Top = 1.625F;
			this.txtOtherLabel.Width = 1.0625F;
			// 
			// txtName1
			// 
			this.txtName1.CanGrow = false;
			this.txtName1.Height = 0.19F;
			this.txtName1.Left = 7F;
			this.txtName1.MultiLine = false;
			this.txtName1.Name = "txtName1";
			this.txtName1.Tag = "textbox";
			this.txtName1.Text = null;
			this.txtName1.Top = 2.09375F;
			this.txtName1.Width = 2.0625F;
			// 
			// Field1
			// 
			this.Field1.CanGrow = false;
			this.Field1.Height = 0.19F;
			this.Field1.Left = 4.25F;
			this.Field1.MultiLine = false;
			this.Field1.Name = "Field1";
			this.Field1.Tag = "textbox";
			this.Field1.Text = "Close of business";
			this.Field1.Top = 2.40625F;
			this.Field1.Width = 1.6875F;
			// 
			// txtLateDate
			// 
			this.txtLateDate.CanGrow = false;
			this.txtLateDate.Height = 0.19F;
			this.txtLateDate.Left = 5.9375F;
			this.txtLateDate.MultiLine = false;
			this.txtLateDate.Name = "txtLateDate";
			this.txtLateDate.Tag = "textbox";
			this.txtLateDate.Text = null;
			this.txtLateDate.Top = 2.40625F;
			this.txtLateDate.Width = 0.875F;
			// 
			// txtPercent
			// 
			this.txtPercent.CanGrow = false;
			this.txtPercent.Height = 0.19F;
			this.txtPercent.Left = 5.9375F;
			this.txtPercent.MultiLine = false;
			this.txtPercent.Name = "txtPercent";
			this.txtPercent.Tag = "textbox";
			this.txtPercent.Text = null;
			this.txtPercent.Top = 2.25F;
			this.txtPercent.Width = 0.5625F;
			// 
			// txtReducedBy
			// 
			this.txtReducedBy.CanGrow = false;
			this.txtReducedBy.Height = 0.19F;
			this.txtReducedBy.Left = 2.6875F;
			this.txtReducedBy.MultiLine = false;
			this.txtReducedBy.Name = "txtReducedBy";
			this.txtReducedBy.Tag = "textbox";
			this.txtReducedBy.Text = null;
			this.txtReducedBy.Top = 2.875F;
			this.txtReducedBy.Width = 0.875F;
			// 
			// txtMailing5
			// 
			this.txtMailing5.CanGrow = false;
			this.txtMailing5.Height = 0.19F;
			this.txtMailing5.Left = 1F;
			this.txtMailing5.MultiLine = false;
			this.txtMailing5.Name = "txtMailing5";
			this.txtMailing5.Tag = "textbox";
			this.txtMailing5.Text = null;
			this.txtMailing5.Top = 2.40625F;
			this.txtMailing5.Width = 3F;
			// 
			// rptBillFormat7
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.0625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLateDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReducedBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaidLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaidLabel1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaid1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLateDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPercent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReducedBy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing5;
	}
}
