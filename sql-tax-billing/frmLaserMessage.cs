﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmLaserMessage.
	/// </summary>
	public partial class frmLaserMessage : BaseForm
	{
		public frmLaserMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtRemittance = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtMessage = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtRemittance.AddControlArrayElement(txtRemittance_6, 6);
			this.txtRemittance.AddControlArrayElement(txtRemittance_5, 5);
			this.txtRemittance.AddControlArrayElement(txtRemittance_4, 4);
			this.txtRemittance.AddControlArrayElement(txtRemittance_3, 3);
			this.txtRemittance.AddControlArrayElement(txtRemittance_2, 2);
			this.txtRemittance.AddControlArrayElement(txtRemittance_1, 1);
			this.txtRemittance.AddControlArrayElement(txtRemittance_0, 0);
			this.txtMessage.AddControlArrayElement(txtMessage_11, 11);
			this.txtMessage.AddControlArrayElement(txtMessage_10, 10);
			this.txtMessage.AddControlArrayElement(txtMessage_9, 9);
			this.txtMessage.AddControlArrayElement(txtMessage_8, 8);
			this.txtMessage.AddControlArrayElement(txtMessage_7, 7);
			this.txtMessage.AddControlArrayElement(txtMessage_6, 6);
			this.txtMessage.AddControlArrayElement(txtMessage_5, 5);
			this.txtMessage.AddControlArrayElement(txtMessage_4, 4);
			this.txtMessage.AddControlArrayElement(txtMessage_3, 3);
			this.txtMessage.AddControlArrayElement(txtMessage_2, 2);
			this.txtMessage.AddControlArrayElement(txtMessage_1, 1);
			this.txtMessage.AddControlArrayElement(txtMessage_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLaserMessage InstancePtr
		{
			get
			{
				return (frmLaserMessage)Sys.GetInstance(typeof(frmLaserMessage));
			}
		}

		protected frmLaserMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmLaserMessage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
			}
		}

		private void frmLaserMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLaserMessage properties;
			//frmLaserMessage.ScaleWidth	= 9225;
			//frmLaserMessage.ScaleHeight	= 7680;
			//frmLaserMessage.LinkTopic	= "Form1";
			//frmLaserMessage.LockControls	= true;
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			bool boolMessageNotEmpty;
			modGlobalFunctions.SetFixedSize(this);
			boolMessageNotEmpty = false;
			clsLoad.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("message"));
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				if (Conversion.Val(clsLoad.Get_Fields("line")) > 11)
					break;
				if (Strings.Trim(strTemp) != string.Empty)
					boolMessageNotEmpty = true;
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				txtMessage[FCConvert.ToInt16(clsLoad.Get_Fields("line"))].Text = strTemp;
				clsLoad.MoveNext();
			}
			if (!boolMessageNotEmpty)
			{
				modGlobalRoutines.SaveDefaultTaxMessage();
				clsLoad.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("message"));
					// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
					if (Conversion.Val(clsLoad.Get_Fields("line")) > 11)
						break;
					// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
					txtMessage[FCConvert.ToInt16(clsLoad.Get_Fields("line"))].Text = strTemp;
					clsLoad.MoveNext();
				}
			}
			clsLoad.OpenRecordset("select * from remittancemessage order by line", "twbl0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("message"));
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				txtRemittance[FCConvert.ToInt16(clsLoad.Get_Fields("line"))].Text = strTemp;
				clsLoad.MoveNext();
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			// save and continue
			clsDRWrapper clsSave = new clsDRWrapper();
			int x;
			string strTemp = "";

            try
            {
                clsSave.Execute("delete from taxmessage", "twbl0000.vb1");
                clsSave.Execute("delete from remittancemessage", "twbl0000.vb1");
                for (x = 0; x <= 11; x++)
                {
                    strTemp = txtMessage[FCConvert.ToInt16(x)].Text;
                    strTemp = modGlobalRoutines.escapequote(strTemp);
                    clsSave.Execute("insert into taxmessage (message,line) values ('" + strTemp + "'," + FCConvert.ToString(x) + ")", "twbl0000.vb1");
                }
                // x
                for (x = 0; x <= 6; x++)
                {
                    strTemp = txtRemittance[FCConvert.ToInt16(x)].Text;
                    strTemp = modGlobalRoutines.escapequote(strTemp);
                    clsSave.Execute("insert into remittancemessage (message,line) values ('" + strTemp + "'," + FCConvert.ToString(x) + ")", "twbl0000.vb1");
                }
            }
            finally
            {
                clsSave.DisposeOf();
            }

			// x
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void txtMessage_KeyDown(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode != Keys.F10 && KeyCode != Keys.Escape && KeyCode != Keys.Back && KeyCode != Keys.Delete)
			{
				if (KeyCode == Keys.Return)
				{
					KeyCode = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
				else if (txtMessage[Index].MaxLength <= txtMessage[Index].Text.Length)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void txtMessage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txtMessage.GetIndex((FCTextBox)sender);
			txtMessage_KeyDown(index, sender, e);
		}
		/// <summary>
		/// Private Sub txtMessage_KeyPress(Index As Integer, KeyAscii As Integer)
		/// </summary>
		/// <summary>
		/// If KeyAscii = 39 Or KeyAscii = 34 Then
		/// </summary>
		/// <summary>
		/// KeyAscii = 0
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void txtRemittance_KeyDown(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode != Keys.F10 && KeyCode != Keys.Escape && KeyCode != Keys.Back && KeyCode != Keys.Delete)
			{
				if (KeyCode == Keys.Return)
				{
					KeyCode = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
				else if (txtRemittance[Index].MaxLength <= txtRemittance[Index].Text.Length)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void txtRemittance_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txtRemittance.GetIndex((FCTextBox)sender);
			txtRemittance_KeyDown(index, sender, e);
		}

		private void cmdSaveAndContinue_Click(object sender, EventArgs e)
		{
			mnuContinue_Click(mnuContinue, EventArgs.Empty);
		}
		/// <summary>
		/// Private Sub txtRemittance_KeyPress(Index As Integer, KeyAscii As Integer)
		/// </summary>
		/// <summary>
		/// If KeyAscii = 39 Or KeyAscii = 34 Then
		/// </summary>
		/// <summary>
		/// KeyAscii = 0
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
	}
}
