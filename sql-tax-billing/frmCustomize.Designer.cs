﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	partial class frmCustomize : BaseForm
	{
		public fecherFoundation.FCComboBox cmbBillSplit;
		public fecherFoundation.FCLabel lblBillSplit;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCCheckBox chkShowDollarSigns;
		public fecherFoundation.FCCheckBox chkCreateZeroBills;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid GridAlternate;
		public fecherFoundation.FCCheckBox chkApplyDiscounts;
		public fecherFoundation.FCCheckBox chkPayInterestOnPrepayments;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkShowTreeGrowth;
		public fecherFoundation.FCCheckBox chkShowRef2;
		public fecherFoundation.FCCheckBox chkShowRef1;
		public fecherFoundation.FCCheckBox chkDuplexCommitment;
		public fecherFoundation.FCCheckBox chkPrinterFonts;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomize));
            this.cmbBillSplit = new fecherFoundation.FCComboBox();
            this.lblBillSplit = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.chkShowDollarSigns = new fecherFoundation.FCCheckBox();
            this.chkCreateZeroBills = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.GridAlternate = new fecherFoundation.FCGrid();
            this.chkApplyDiscounts = new fecherFoundation.FCCheckBox();
            this.chkPayInterestOnPrepayments = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkShowTreeGrowth = new fecherFoundation.FCCheckBox();
            this.chkShowRef2 = new fecherFoundation.FCCheckBox();
            this.chkShowRef1 = new fecherFoundation.FCCheckBox();
            this.chkDuplexCommitment = new fecherFoundation.FCCheckBox();
            this.chkPrinterFonts = new fecherFoundation.FCCheckBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDollarSigns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateZeroBills)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridAlternate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApplyDiscounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayInterestOnPrepayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowTreeGrowth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowRef2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowRef1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDuplexCommitment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrinterFonts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 416);
            this.BottomPanel.Size = new System.Drawing.Size(1105, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.chkPrinterFonts);
            this.ClientArea.Size = new System.Drawing.Size(1125, 516);
            this.ClientArea.Controls.SetChildIndex(this.chkPrinterFonts, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1125, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Customize";
            // 
            // cmbBillSplit
            // 
            this.cmbBillSplit.Items.AddRange(new object[] {
            "If bill < 100 apply to period 1",
            "If bill < 100 split between all periods"});
            this.cmbBillSplit.Location = new System.Drawing.Point(716, 30);
            this.cmbBillSplit.Name = "cmbBillSplit";
            this.cmbBillSplit.Size = new System.Drawing.Size(320, 40);
            this.cmbBillSplit.TabIndex = 3;
            this.cmbBillSplit.Text = "If bill < 100 apply to period 1";
            // 
            // lblBillSplit
            // 
            this.lblBillSplit.AutoSize = true;
            this.lblBillSplit.Location = new System.Drawing.Point(590, 44);
            this.lblBillSplit.Name = "lblBillSplit";
            this.lblBillSplit.Size = new System.Drawing.Size(79, 17);
            this.lblBillSplit.TabIndex = 2;
            this.lblBillSplit.Text = "BILL SPLIT";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.chkShowDollarSigns);
            this.Frame3.Controls.Add(this.cmbBillSplit);
            this.Frame3.Controls.Add(this.lblBillSplit);
            this.Frame3.Controls.Add(this.chkCreateZeroBills);
            this.Frame3.Location = new System.Drawing.Point(30, 297);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(1057, 119);
            this.Frame3.TabIndex = 3;
            this.Frame3.Text = "Bills";
            // 
            // chkShowDollarSigns
            // 
            this.chkShowDollarSigns.Location = new System.Drawing.Point(20, 30);
            this.chkShowDollarSigns.Name = "chkShowDollarSigns";
            this.chkShowDollarSigns.Size = new System.Drawing.Size(406, 24);
            this.chkShowDollarSigns.Text = "Show dollar signs for all monetary amounts on custom bills";
            this.ToolTip1.SetToolTip(this.chkShowDollarSigns, "When creating bill records if a prepayment exists that is greater than or equal t" +
        "o the bill amount, this option will charge the discount amount and create a pend" +
        "ing payment record");
            // 
            // chkCreateZeroBills
            // 
            this.chkCreateZeroBills.Location = new System.Drawing.Point(20, 70);
            this.chkCreateZeroBills.Name = "chkCreateZeroBills";
            this.chkCreateZeroBills.Size = new System.Drawing.Size(445, 24);
            this.chkCreateZeroBills.TabIndex = 1;
            this.chkCreateZeroBills.Text = "Create bills for all accounts, including accounts with 0 tax liability";
            this.ToolTip1.SetToolTip(this.chkCreateZeroBills, "When creating bill records, create records for all accounts even if they have a 0" +
        " net assessment");
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.GridAlternate);
            this.Frame2.Controls.Add(this.chkApplyDiscounts);
            this.Frame2.Controls.Add(this.chkPayInterestOnPrepayments);
            this.Frame2.Location = new System.Drawing.Point(30, 70);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(570, 206);
            this.Frame2.TabIndex = 1;
            this.Frame2.Text = "Prepayments";
            // 
            // GridAlternate
            // 
            this.GridAlternate.Cols = 1;
            this.GridAlternate.ExtendLastCol = true;
            this.GridAlternate.FixedCols = 0;
            this.GridAlternate.Location = new System.Drawing.Point(20, 110);
            this.GridAlternate.Name = "GridAlternate";
            this.GridAlternate.RowHeadersVisible = false;
            this.GridAlternate.Size = new System.Drawing.Size(303, 73);
            this.GridAlternate.TabIndex = 2;
            // 
            // chkApplyDiscounts
            // 
            this.chkApplyDiscounts.Location = new System.Drawing.Point(20, 30);
            this.chkApplyDiscounts.Name = "chkApplyDiscounts";
            this.chkApplyDiscounts.Size = new System.Drawing.Size(467, 24);
            this.chkApplyDiscounts.TabIndex = 3;
            this.chkApplyDiscounts.Text = "Automatically apply discounts when pre-payments are >= bill amount";
            this.ToolTip1.SetToolTip(this.chkApplyDiscounts, "When creating bill records if a prepayment exists that is greater than or equal t" +
        "o the bill amount, this option will charge the discount amount and create a pend" +
        "ing payment record");
            // 
            // chkPayInterestOnPrepayments
            // 
            this.chkPayInterestOnPrepayments.Location = new System.Drawing.Point(20, 70);
            this.chkPayInterestOnPrepayments.Name = "chkPayInterestOnPrepayments";
            this.chkPayInterestOnPrepayments.Size = new System.Drawing.Size(214, 24);
            this.chkPayInterestOnPrepayments.TabIndex = 1;
            this.chkPayInterestOnPrepayments.Text = "Pay Interest on Pre-payments";
            this.ToolTip1.SetToolTip(this.chkPayInterestOnPrepayments, "When creating bill records if a prepayment exists that is greater than or equal t" +
        "o the bill amount, this option will charge the discount amount and create a pend" +
        "ing payment record");
            this.chkPayInterestOnPrepayments.CheckedChanged += new System.EventHandler(this.chkPayInterestOnPrepayments_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkShowTreeGrowth);
            this.Frame1.Controls.Add(this.chkShowRef2);
            this.Frame1.Controls.Add(this.chkShowRef1);
            this.Frame1.Controls.Add(this.chkDuplexCommitment);
            this.Frame1.Location = new System.Drawing.Point(632, 70);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(455, 206);
            this.Frame1.TabIndex = 2;
            this.Frame1.Text = "Commitment Book";
            // 
            // chkShowTreeGrowth
            // 
            this.chkShowTreeGrowth.Location = new System.Drawing.Point(20, 110);
            this.chkShowTreeGrowth.Name = "chkShowTreeGrowth";
            this.chkShowTreeGrowth.Size = new System.Drawing.Size(216, 24);
            this.chkShowTreeGrowth.TabIndex = 3;
            this.chkShowTreeGrowth.Text = "Show tree growth information";
            // 
            // chkShowRef2
            // 
            this.chkShowRef2.Location = new System.Drawing.Point(20, 70);
            this.chkShowRef2.Name = "chkShowRef2";
            this.chkShowRef2.Size = new System.Drawing.Size(141, 24);
            this.chkShowRef2.TabIndex = 2;
            this.chkShowRef2.Text = "Show Reference 2";
            // 
            // chkShowRef1
            // 
            this.chkShowRef1.Location = new System.Drawing.Point(20, 30);
            this.chkShowRef1.Name = "chkShowRef1";
            this.chkShowRef1.Size = new System.Drawing.Size(141, 24);
            this.chkShowRef1.TabIndex = 1;
            this.chkShowRef1.Text = "Show Reference 1";
            // 
            // chkDuplexCommitment
            // 
            this.chkDuplexCommitment.Location = new System.Drawing.Point(272, 30);
            this.chkDuplexCommitment.Name = "chkDuplexCommitment";
            this.chkDuplexCommitment.Size = new System.Drawing.Size(162, 24);
            this.chkDuplexCommitment.TabIndex = 4;
            this.chkDuplexCommitment.Text = "Allow duplex printing";
            this.chkDuplexCommitment.Visible = false;
            // 
            // chkPrinterFonts
            // 
            this.chkPrinterFonts.Location = new System.Drawing.Point(30, 30);
            this.chkPrinterFonts.Name = "chkPrinterFonts";
            this.chkPrinterFonts.Size = new System.Drawing.Size(154, 24);
            this.chkPrinterFonts.TabIndex = 1001;
            this.chkPrinterFonts.Text = "Ignore Printer Fonts";
            this.ToolTip1.SetToolTip(this.chkPrinterFonts, "Printer Fonts are used to print quickly to dot matrix printers. If the bills or c" +
        "ommitment book have text that overtypes or drops to another line, check this opt" +
        "ion.");
            this.chkPrinterFonts.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 2;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(506, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(93, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmCustomize
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1125, 576);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomize";
            this.Text = "Customize";
            this.Load += new System.EventHandler(this.frmCustomize_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomize_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDollarSigns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateZeroBills)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridAlternate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApplyDiscounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayInterestOnPrepayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowTreeGrowth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowRef2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowRef1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDuplexCommitment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrinterFonts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
