﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormatF.
	/// </summary>
	public partial class rptBillFormatF : FCSectionReport
	{
		public rptBillFormatF()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Format F";
		}

		public static rptBillFormatF InstancePtr
		{
			get
			{
				return (rptBillFormatF)Sys.GetInstance(typeof(rptBillFormatF));
			}
		}

		protected rptBillFormatF _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillFormatF	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// WRITTEN BY     :               Corey Gray              *
		/// </summary>
		/// <summary>
		/// DATE           :                                       *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// MODIFIED BY    :               Jim Bertolino           *
		/// </summary>
		/// <summary>
		/// LAST UPDATED   :               07/29/2004              *
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		bool boolFirstBill;
		clsDRWrapper clsMortgageHolders = new clsDRWrapper();
		clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
		clsDRWrapper clsRateRecs = new clsDRWrapper();
		clsBillFormat clsTaxBillFormat = new clsBillFormat();
		clsRateRecord clsTaxRate = new clsRateRecord();
		double TaxRate;
		int intStartPage;
		clsDRWrapper clsBills = new clsDRWrapper();
		bool boolRE;
		bool boolPP;
		double Tax1;
		double Tax2;
		double Tax3;
		double Tax4;
		// vbPorter upgrade warning: Prepaid As double	OnWrite(double, string)
		double Prepaid;
		double DistPerc1;
		double DistPerc2;
		double DistPerc3;
		double DistPerc4;
		double DistPerc5;
		int intDistCats;
		double dblDiscountPercent;
		string strCat1 = "";
		string strCat2 = "";
		string strCat3 = "";
		int lngExempt;
		// vbPorter upgrade warning: lngTotAssess As int	OnWrite(short, double)
		int lngTotAssess;
		public string strPercent = "";
		public string strPaidDate = "";
		public string strReducedBy = "";
		bool boolShownModally;
		bool boolNotJustUnloading;
		bool boolReprintNewOwnerCopy;
		/// <summary>
		/// this will be set to true if there is a new owner and the recordset will not be advanced to create a copy for him
		/// </summary>
		bool boolPrintRecipientCopy;
		clsDRWrapper rsMultiRecipients = new clsDRWrapper();
		bool boolIsRegional;
		// vbPorter upgrade warning: intYear As short	OnWrite(double, int)
		public void Init(clsBillFormat clsTaxFormat, int intYear, string strFontName, bool boolShow = true, string strPrinterN = "", bool boolModal = false)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			string strSQL;
			double dblTotalTax;
			boolIsRegional = modRegionalTown.IsRegionalTown();
			boolShownModally = boolModal;
			boolNotJustUnloading = true;
			if (strPrinterN != string.Empty)
			{
				this.Document.Printer.PrinterName = strPrinterN;
			}
			if (clsTaxFormat.BillsFrom == 0)
			{
				boolRE = true;
				if (clsTaxFormat.Combined)
				{
					boolPP = true;
				}
				else
				{
					boolPP = false;
				}
			}
			else
			{
				boolRE = false;
				boolPP = true;
			}
			if (boolPP)
			{
				clsTemp.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
				strCat1 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				lblLand.Text = strCat1;
				clsTemp.MoveNext();
				strCat2 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				txtBldgLabel.Text = strCat2;
				clsTemp.MoveNext();
				strCat3 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				txtExemptLabel.Text = strCat3;
			}
			if (!boolPP)
			{
				txtCatOther.Visible = false;
				txtOtherLabel.Visible = false;
			}
			clsTaxBillFormat.CopyFormat(ref clsTaxFormat);
			if (boolRE && clsTaxBillFormat.PrintCopyForMortgageHolders)
			{
				boolFirstBill = true;
				clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
				clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT", "CentralData");
				if (!clsMortgageAssociations.EndOfFile())
				{
					clsMortgageAssociations.MoveFirst();
					clsMortgageAssociations.MovePrevious();
				}
			}
			else
			{
				boolFirstBill = true;
			}
			strSQL = clsTaxBillFormat.SQLStatement;
			clsTaxRate.TaxYear = intYear;
			lblTitle.Text = FCConvert.ToString(intYear);
			lblTitle1.Text = FCConvert.ToString(intYear);
			clsRateRecs.OpenRecordset("select * from raterec where year = " + FCConvert.ToString(intYear), "twcl0000.vb1");
			if (!clsRateRecs.EndOfFile())
			{
				clsTaxRate.LoadRate(clsRateRecs.Get_Fields_Int32("ID"));
			}
			if (clsTaxBillFormat.Breakdown)
			{
				// Frame1.Visible = True
				clsTemp.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					DistPerc1 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
					txtDistPerc1.Visible = true;
					txtDistPerc1.Text = Strings.Format(DistPerc1, "#0.00");
					clsTemp.MoveNext();
					if (!clsTemp.EndOfFile())
					{
						txtDistPerc2.Visible = true;
						DistPerc2 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
						txtDistPerc2.Text = Strings.Format(DistPerc2, "#0.00");
						clsTemp.MoveNext();
						if (!clsTemp.EndOfFile())
						{
							txtDistPerc3.Visible = true;
							DistPerc3 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
							txtDistPerc3.Text = Strings.Format(DistPerc3, "#0.00");
							clsTemp.MoveNext();
							if (!clsTemp.EndOfFile())
							{
								txtDistPerc4.Visible = true;
								DistPerc4 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
								txtDistPerc4.Text = Strings.Format(DistPerc4, "#0.00");
								clsTemp.MoveNext();
								if (!clsTemp.EndOfFile())
								{
									txtDistPerc5.Visible = true;
									DistPerc5 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
									txtDistPerc5.Text = Strings.Format(DistPerc5, "#0.00");
								}
							}
						}
					}
				}
			}
			else
			{
				// Frame1.Visible = False
				txtDistPerc1.Visible = false;
				txtDistPerc2.Visible = false;
				txtDistPerc3.Visible = false;
				txtDistPerc4.Visible = false;
				txtDistPerc5.Visible = false;
			}
			if (strFontName != string.Empty)
			{
				// now set each box and label to the correct printer font
				for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
				{
					// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
					bool setFont = false;
					bool bold = false;
					if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
					{
						setFont = true;
					}
					else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
					{
						setFont = true;
						bold = true;
					}
					if (setFont)
					{
						GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
						if (textBox != null)
						{
							textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
						}
						else
						{
							GrapeCity.ActiveReports.SectionReportModel.Label label = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
							if (label != null)
							{
								label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
						}
					}
				}
				// x
				//this.Printer.RenderMode = 1;
			}
			GetBills(ref strSQL);
			// frm7Message.Show vbModal, MDIParent
			frm7Message.InstancePtr.Init(7, clsTaxRate.InterestRate, clsTaxRate.Get_InterestStartDate(1));
			txtLateDate.Text = modGlobalVariables.Statics.gstrPaidDate;
			txtPercent.Text = modGlobalVariables.Statics.gstrPercent;
			txtReducedBy.Text = modGlobalVariables.Statics.gstrReducedBy;
			if (boolShow)
			{
				// Me.Show , MDIParent
			}
			else
			{
				this.PrintReport(false);
			}
		}

		private void GetBills(ref string strSQL)
		{
			clsBills.OpenRecordset(strSQL, "twcl0000.vb1");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsBills.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			// Dim clsAddress As New clsDRWrapper
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
			if (clsTaxBillFormat.HasDefaultMargin)
			{
				this.PageSettings.Margins.Left = 0;
			}
			if (clsTaxBillFormat.ReturnAddress)
			{
				if (!boolIsRegional)
				{
					string[] strAdd = new string[4 + 1];
					modBLCustomBill.GetReturnAddressForBills(0, ref strAdd[0], ref strAdd[1], ref strAdd[2], ref strAdd[3]);
					txtAddress1.Text = strAdd[0];
					txtAddress2.Text = strAdd[1];
					txtAddress3.Text = strAdd[2];
					txtAddress4.Text = strAdd[3];
				}
			}
			else
			{
				txtAddress1.Visible = false;
				txtAddress2.Visible = false;
				txtAddress3.Visible = false;
				txtAddress4.Visible = false;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			modCustomPageSize.Statics.boolChangedDefault = true;
			modCustomPageSize.ResetDefaultPrinter();
			if (!boolShownModally && boolNotJustUnloading)
			{
				//MDIParent.InstancePtr.Show();
			}
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	// do this since we already chose the printer and don't want to choose again
		//	// now we have to handle printing the pages ourselves though.
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "Print...")
		//	{
		//		// Call frmNumPages.Init(Me.Pages.Count, 1)
		//		// frmNumPages.Show vbModal, MDIParent
		//		// Me.Printer.FromPage = gintStartPage
		//		// Me.Printer.ToPage = gintEndPage
		//		// Me.PrintReport (False)
		//		clsPrint.ReportPrint(this);
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			double Dist1;
			double Dist2;
			double Dist3;
			double Dist4;
			double Dist5;
			// vbPorter upgrade warning: TotTax As double	OnWrite(double, string)
			double TotTax = 0;
			string strTemp = "";
			string strAcct = "";
			DateTime dtLastDate;
			string strOldName = "";
			string strSecOwner = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			if (!clsBills.EndOfFile())
			{
				if (clsRateRecs.FindFirstRecord("ID", clsBills.Get_Fields_Int32("ratekey")))
				{
					clsTaxRate.LoadRate(clsBills.Get_Fields_Int32("ratekey"), clsRateRecs);
				}
				lngTotAssess = 0;
				lngExempt = 0;
				if (clsTaxBillFormat.ReturnAddress)
				{
					if (boolIsRegional)
					{
						string[] strReturn = new string[4 + 1];
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						modBLCustomBill.GetReturnAddressForBills(FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields("trancode"))), ref strReturn[0], ref strReturn[1], ref strReturn[2], ref strReturn[3]);
						txtAddress1.Text = strReturn[0];
						txtAddress2.Text = strReturn[1];
						txtAddress3.Text = strReturn[2];
						txtAddress4.Text = strReturn[3];
					}
				}
				Tax1 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue1"));
				Tax2 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue2"));
				Tax3 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue3"));
				Tax4 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue4"));
				TotTax = Tax1 + Tax2 + Tax3 + Tax4;
				txtTaxDue.Text = Strings.Format(TotTax, "#,###,###,##0.00");
				// If clsTaxBillFormat.BreakdownDollars Then
				// Dist1 = TotTax * DistPerc1
				// Dist2 = TotTax * DistPerc2
				// Dist3 = TotTax * DistPerc3
				// Dist4 = TotTax * DistPerc4
				// Dist5 = TotTax * DistPerc5
				// 
				// txtDistAmount1.Text = Format(Dist1, "##,###,##0.00")
				// If intDistCats > 1 Then
				// txtDistAmount2.Text = Format(Dist2, "##,###,##0.00")
				// End If
				// If intDistCats > 2 Then
				// txtDistAmount3.Text = Format(Dist3, "##,###,##0.00")
				// End If
				// If intDistCats > 3 Then
				// txtDistAmount4.Text = Format(Dist4, "##,###,##0.00")
				// End If
				// If intDistCats > 4 Then
				// txtDistAmount5.Text = Format(Dist5, "##,###,##0.00")
				// End If
				// End If
				Prepaid = Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid"));
				double dblAbated = 0;
				double dblAbate1 = 0;
				double dblAbate2 = 0;
				double dblAbate3 = 0;
				double dblAbate4 = 0;
				int intPer;
				double[] dblTAbate = new double[4 + 1];
				dblAbated = 0;
				if ((Tax2 > 0 || Tax3 > 0 || Tax4 > 0) && Prepaid > 0)
				{
					dblAbated = modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields_Int32("ID"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
					dblAbate1 = dblTAbate[1];
					dblAbate2 = dblTAbate[2];
					dblAbate3 = dblTAbate[3];
					dblAbate4 = dblTAbate[4];
					if (boolRE && boolPP)
					{
						// TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
						dblAbated += modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields("ppbillkey"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
						dblAbate1 += dblTAbate[1];
						dblAbate2 += dblTAbate[2];
						dblAbate3 += dblTAbate[3];
						dblAbate4 += dblTAbate[4];
					}
					Prepaid -= dblAbated;
				}
				if (dblAbated > 0)
				{
					Tax1 -= dblAbate1;
					Tax2 -= dblAbate2;
					Tax3 -= dblAbate3;
					Tax4 -= dblAbate4;
				}
				if (Prepaid > 0)
				{
					if (Prepaid > Tax1)
					{
						Prepaid -= Tax1;
						Tax1 = 0;
						if (Prepaid > Tax2)
						{
							Prepaid -= Tax2;
							Tax2 = 0;
							if (Prepaid > Tax3)
							{
								Prepaid -= Tax3;
								Tax3 = 0;
								if (Prepaid > Tax4)
								{
									Prepaid -= Tax4;
									Tax4 = 0;
								}
								else
								{
									Tax4 -= Prepaid;
								}
							}
							else
							{
								Tax3 -= Prepaid;
							}
						}
						else
						{
							Tax2 -= Prepaid;
						}
					}
					else
					{
						Tax1 -= Prepaid;
					}
				}
				Prepaid = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid")), "0.00"));
				TotTax = FCConvert.ToDouble(Strings.Format(TotTax, "0.00"));
				txtPrePaid.Text = Strings.Format(Prepaid, "#,###,##0.00");
				// txtPrePaid1.Text = Format(Prepaid, "#,###,##0.00")
				if (Prepaid == 0)
				{
					txtPrePaid.Visible = false;
					txtPrePaidLabel.Visible = false;
					// txtPrePaid1.Visible = False
					// txtPrePaidLabel1.Visible = False
				}
				else
				{
					txtPrePaid.Visible = true;
					txtPrePaidLabel.Visible = true;
					if (Prepaid > TotTax)
					{
						txtTaxDue.Text = "Overpaid";
					}
					else
					{
						txtTaxDue.Text = Strings.Format(TotTax - Prepaid, "#,###,##0.00");
					}
					// txtPrePaid1.Visible = True
					// txtPrePaidLabel1.Visible = True
				}
				txtRate.Text = Strings.Format(clsTaxRate.TaxRate * 1000, "#0.000");
				// txtAmount1.Text = Format(Tax1, "#,###,##0.00")
				// txtAmount2.Text = Format(Tax2, "#,###,##0.00")
				// txtInterest1.Text = clsTaxRate.Get_InterestStartDate(1)
				// txtInterest2.Text = clsTaxRate.Get_InterestStartDate(2)
				// txtAmount3.Text = Format(Tax3, "#,###,##0.00")
				// txtAmount4.Text = Format(Tax4, "#,###,##0.00")
				// txtinterest3.Text = clsTaxRate.Get_InterestStartDate(3)
				// txtinterest4.Text = clsTaxRate.Get_InterestStartDate(4)
				// 
				// If clsTaxBillFormat.Discount Then
				// txtDiscountAmount.Text = Format(dblDiscountPercent * (Tax1 + Tax2 + Tax3 + Tax4), "#,###,##0.00")
				// txtDiscountAmount.Text = Format((Tax1 + Tax2 + Tax3 + Tax4), "#,###,##0.00")
				// End If
				// 
				strAcct = "";
				txtMailing5.Text = "";
				if (boolRE)
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					strAcct = "R" + clsBills.Get_Fields("account");
					if (Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage"))) != string.Empty)
					{
						txtBookPage.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage")));
						txtBookPage.Visible = true;
					}
					else
					{
						txtBookPage.Visible = false;
						// txtBookPageLabel.Visible = False
					}
					txtLand.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")), "#,###,###,##0");
					txtBldg.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")), "#,###,###,##0");
					lngTotAssess = FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")) + Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")));
					if (boolPrintRecipientCopy)
					{
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						txtMailing2.Text = "C/O " + rsMultiRecipients.Get_Fields_String("Name");
						txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address1");
						txtMailing4.Text = rsMultiRecipients.Get_Fields_String("address2");
						txtMailing5.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
					}
					else if (!boolFirstBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
					{
						clsMortgageHolders.FindFirstRecord("ID", clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"));
						if (!clsMortgageHolders.NoMatch)
						{
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing2.Text = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
							txtMailing5.Text = Strings.Trim(clsMortgageHolders.Get_Fields_String("city") + " " + clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
						}
						else
						{
							txtMailing1.Text = "Mortgage Holder Not Found";
							txtMailing2.Text = "";
							txtMailing3.Text = "";
							txtMailing4.Text = "";
							txtMailing5.Text = "";
						}
					}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (boolReprintNewOwnerCopy && modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3))
					{
						txtMailing3.Text = strAddr1;
						txtMailing4.Text = strAddr2;
						txtMailing5.Text = strAddr3;
						txtMailing1.Text = strOldName;
						txtMailing2.Text = strSecOwner;
					}
					else
					{
						txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
						txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
						txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						// strName = Trim(clsBills.Fields("name1"))
						txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name2")));
					}
					if (txtMailing4.Text == "")
					{
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing3.Text == "")
					{
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing2.Text == "")
					{
						txtMailing2.Text = txtMailing3.Text;
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						txtLocation.Text = Strings.Trim(Strings.Trim(clsBills.Get_Fields("streetnumber") + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
					}
					else
					{
						txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("apt"))) + " " + clsBills.Get_Fields_String("streetname"));
					}
					txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("maplot")));
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
					txtExemption.Text = Strings.Format(lngExempt, "#,###,###,##0");
				}
				if (boolPP)
				{
					// pp
					if (boolRE)
					{
						// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBills.Get_Fields("ppac")) > 0)
						{
							strAcct += " P";
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							strAcct += clsBills.Get_Fields("ppac");
						}
					}
					else
					{
						strAcct = "P";
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						strAcct += clsBills.Get_Fields("account");
					}
					txtLand.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category1")), "#,###,###,##0");
					txtBldg.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category2")), "#,###,###,##0");
					// txtCat1.Text = Format(Val(clsBills.Fields("cat1")), "#,###,###,##0")
					// txtCat2.Text = Format(Val(clsBills.Fields("cat2")), "#,###,###,##0")
					// txtCat3.Text = Format(Val(clsBills.Fields("cat3")), "#,###,###,##0")
					txtExemption.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category3")), "#,###,###,##0");
					txtCatOther.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category4")) + Conversion.Val(clsBills.Get_Fields_Int32("category5")) + Conversion.Val(clsBills.Get_Fields_Int32("category6")) + Conversion.Val(clsBills.Get_Fields_Int32("category7")) + Conversion.Val(clsBills.Get_Fields_Int32("category8")) + Conversion.Val(clsBills.Get_Fields_Int32("category9")), "#,###,###,##0");
					lngTotAssess += FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("ppassessment")));
					if (!boolRE)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						txtLocation.Text = Strings.Trim(clsBills.Get_Fields("streetnumber") + " " + clsBills.Get_Fields_String("apt") + "  " + clsBills.Get_Fields_String("streetNAME"));
						// used for pp location
						if (boolPrintRecipientCopy)
						{
							txtMailing1.Text = rsMultiRecipients.Get_Fields_String("Name");
							txtMailing2.Text = rsMultiRecipients.Get_Fields_String("address1");
							txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address2");
							txtMailing4.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
							txtMailing5.Text = "";
						}
						else
						{
							txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
							// strTemp = Trim(clsBills.Fields("zip"))
							// txtMailing4.Text = Trim(clsBills.Fields("city")) & "  " & clsBills.Fields("state") & "  " & strTemp
							// If Trim(clsBills.Fields("zip4")) <> vbNullString Then
							// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("zip4"))
							// End If
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing5.Text = "";
						}
						// txtName1.Text = Trim(clsBills.Fields("name"))
						if (txtMailing3.Text == "")
						{
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = "";
						}
						if (txtMailing2.Text == "")
						{
							txtMailing2.Text = txtMailing3.Text;
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = "";
						}
					}
					else
					{
					}
				}
				lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				// txtValue.Text = Format(lngTotAssess, "#,###,###,##0")
				// txtExemption.Text = Format(lngExempt, "#,###,###,##0")
				txtAssessment.Text = Strings.Format(lngTotAssess - lngExempt, "#,###,###,##0");
				txtAccount.Text = strAcct;
				txtAcct1.Text = strAcct;
				// txtAcct2.Text = strAcct
				if (boolRE)
				{
					if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
					{
						boolReprintNewOwnerCopy = false;
						// if the copy for a new owner needs to be sent, then check it here
						if (clsTaxBillFormat.PrintCopyForNewOwner)
						{
							// if this is the main bill, only check for the new owner on the mail bill
							if (boolFirstBill)
							{
								if (DateTime.Today.Month < 4)
								{
									// if the month is before april then use last years commitment date
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1));
								}
								else
								{
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));
								}
								// If NewOwner(clsBills.Fields("Account"), dtLastDate) Then     'if there has been a new owner
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1")))
								{
									// if there has been a new owner
									boolReprintNewOwnerCopy = true;
								}
							}
						}
					}
					else if (!boolPrintRecipientCopy && boolFirstBill)
					{
						// turn off the copy so that the record can advance
						boolReprintNewOwnerCopy = false;
					}
					if (!boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							if (boolFirstBill)
							{
								rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
								if (!rsMultiRecipients.EndOfFile())
								{
									boolPrintRecipientCopy = true;
								}
							}
						}
					}
					else if (boolPrintRecipientCopy && boolFirstBill)
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (clsTaxBillFormat.PrintCopyForMortgageHolders && !boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (!clsMortgageAssociations.EndOfFile())
						{
							if (boolFirstBill)
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindFirstRecord("account", clsBills.Get_Fields("account"));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindNextRecord("account", clsBills.Get_Fields("account"));
							}
							// Call clsMortgageAssociations.FindNextRecord("account", clsBills.Fields("ac"))
							if (clsMortgageAssociations.NoMatch)
							{
								boolFirstBill = true;
								if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
									clsBills.MoveNext();
							}
							else
							{
								boolFirstBill = false;
							}
							// End If
						}
						else
						{
							boolFirstBill = true;
							if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
								clsBills.MoveNext();
						}
					}
					else
					{
						if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
							clsBills.MoveNext();
					}
				}
				else
				{
					if (!boolPrintRecipientCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
							if (!rsMultiRecipients.EndOfFile())
							{
								boolPrintRecipientCopy = true;
							}
						}
					}
					else
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (!boolPrintRecipientCopy)
					{
						clsBills.MoveNext();
					}
				}
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int intReturn = 0;
			
			modCustomPageSize.CheckDefaultPrinter(this.Document.Printer.PrinterName);
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("BillFormatF", 1475, FCConvert.ToInt32( (5220 / 1440f * 100)));
		}
	}
}
