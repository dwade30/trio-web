﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBL0000
{
	public class cBangorValuations
	{
		//=========================================================
		private string strConnectionName = "";
		private string strGroupName = string.Empty;

		public delegate void LabelUpdateEventHandler(string strMessage);

		public event LabelUpdateEventHandler LabelUpdate;

		public delegate void ActionCompleteEventHandler(string strMessage);

		public event ActionCompleteEventHandler ActionComplete;

		public string ConnectionName
		{
			get
			{
				string ConnectionName = "";
				if (strConnectionName != "")
				{
					ConnectionName = strConnectionName;
				}
				else
				{
					ConnectionName = "RealEstate";
				}
				return ConnectionName;
			}
			set
			{
				strConnectionName = value;
			}
		}

		public string GroupName
		{
			set
			{
				strGroupName = value;
			}
			get
			{
				string GroupName = "";
				if (strGroupName != "")
				{
					GroupName = strGroupName;
				}
				else
				{
					GroupName = "Live";
				}
				return GroupName;
			}
		}

		public void ClearDistrict(int intDistrictCode)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.GroupName = GroupName;
			rsSave.Execute("update master set lastlandval = 0,lastbldgval = 0,rlexemption = 0 where ritrancode = " + FCConvert.ToString(intDistrictCode) + " and isnull(rsdeleted,0) <> 1", ConnectionName);
			if (this.ActionComplete != null)
				this.ActionComplete("Values cleared");
		}

		public void CopyDistrict(int intDistrictCode)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsSave = new clsDRWrapper();
			int lngAct = 0;
			string strTemp = "";
			string strMessage = "";
			rsSave.GroupName = GroupName;
			rsSave.OpenRecordset("select * from master where ritrancode = " + FCConvert.ToString(intDistrictCode) + " and rscard = 1 and isnull(rsdeleted,0) <> 1", ConnectionName);
			while (!rsSave.EndOfFile())
			{
				strMessage = "Filling Account " + rsSave.Get_Fields_Int32("rsaccount");
				if (this.LabelUpdate != null)
					this.LabelUpdate(strMessage);
				strTemp = Strings.Trim(FCConvert.ToString(rsSave.Get_Fields_String("rsref2")));
				strTemp = Strings.Trim(Strings.Replace(strTemp, "RE:", "", 1, -1, CompareConstants.vbTextCompare));
				strTemp = Strings.Trim(Strings.Replace(strTemp, "REG:", "", 1, -1, CompareConstants.vbTextCompare));
				lngAct = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				if (lngAct > 0)
				{
					rsLoad.OpenRecordset("select sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum from master where rsaccount = " + FCConvert.ToString(lngAct) + " group by rsaccount", ConnectionName);
					if (!rsLoad.EndOfFile())
					{
						rsSave.Edit();
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("lastlandval", rsLoad.Get_Fields("landsum"));
						// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("lastbldgval", rsLoad.Get_Fields("bldgsum"));
						rsSave.Set_Fields("rlexemption", 0);
						rsSave.Update();
					}
				}
				rsSave.MoveNext();
			}
			if (this.ActionComplete != null)
				this.ActionComplete("Values Copied");
		}

		public cBangorValuations() : base()
		{
			strConnectionName = "RealEstate";
		}
	}
}
