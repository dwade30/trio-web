﻿//Fecher vbPorter - Version 1.0.0.35
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using fecherFoundation.Extensions;
using fecherFoundation;
using Global;
using System;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmREAuditRange.
	/// </summary>
	public partial class frmREAuditRange : BaseForm
	{
		public frmREAuditRange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmREAuditRange InstancePtr
		{
			get
			{
				return (frmREAuditRange)Sys.GetInstance(typeof(frmREAuditRange));
			}
		}

		protected frmREAuditRange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public bool boolFromSupplemental;

		private void frmREAuditRange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuContinue_Click();
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuCancel_Click();
			}
		}

		private void frmREAuditRange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmREAuditRange properties;
			//frmREAuditRange.ScaleWidth	= 5880;
			//frmREAuditRange.ScaleHeight	= 4215;
			//frmREAuditRange.LinkTopic	= "Form1";
			//frmREAuditRange.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			lblTo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			modGlobalVariables.Statics.CancelledIt = false;
			//FC:FINAL:CHN - i.issue #1558: real estate acct-sequense option visibility.
			optDisplay_CheckedChanged(null, null);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// MDIParent.Show
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			Close();
		}

		public void mnuCancel_Click()
		{
			mnuCancel_Click(mnuCancel, new System.EventArgs());
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			int intWhich = 0;
			string strBegin = "";
			string strEnd = "";
			if (!boolFromSupplemental)
			{
				if (cmbDisplay.SelectedIndex == 0)
				{
					frmREAudit.InstancePtr.boolDisplayFirst = true;
					frmREAudit.InstancePtr.strSequence = "NAME";
				}
				else
				{
					frmREAudit.InstancePtr.boolDisplayFirst = false;
					if (cmbSequence.SelectedIndex == 0)
					{
						// name
						frmREAudit.InstancePtr.strSequence = "NAME";
					}
					else if (cmbSequence.SelectedIndex == 1)
					{
						// account
						frmREAudit.InstancePtr.strSequence = "ACCOUNT";
					}
					else if (cmbSequence.SelectedIndex == 2)
					{
						// maplot
						frmREAudit.InstancePtr.strSequence = "MAPLOT";
					}
					else if (cmbSequence.SelectedIndex == 3)
					{
						// tax
						frmREAudit.InstancePtr.strSequence = "TAX";
					}
					else
					{
						// location
						frmREAudit.InstancePtr.strSequence = "LOCATION";
					}
				}
				if (cmbAccounts.SelectedIndex == 0)
				{
					frmREAudit.InstancePtr.intWhich = -1;
				}
				else
				{
					if (cmbAccounts.SelectedIndex == 2)
					{
						txtEnd.Text = "";
					}
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						if (cmbAccounts.SelectedIndex == 2)
						{
							MessageBox.Show("You must specify a value", "Specify Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							MessageBox.Show("You must specify a start range", "Specify Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						return;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						frmREAudit.InstancePtr.intWhich = 0;
						if (Conversion.Val(txtStart.Text) > 0)
						{
							frmREAudit.InstancePtr.strBegin = txtStart.Text;
							if (Conversion.Val(txtEnd.Text) > 0)
							{
								frmREAudit.InstancePtr.strEnd = txtEnd.Text;
							}
							else
							{
								frmREAudit.InstancePtr.strEnd = txtStart.Text;
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						if (cmbRange.SelectedIndex == 1)
						{
							frmREAudit.InstancePtr.intWhich = 1;
						}
						else if (cmbRange.SelectedIndex == 2)
						{
							frmREAudit.InstancePtr.intWhich = 2;
						}
						else if (cmbRange.SelectedIndex == 3)
						{
							frmREAudit.InstancePtr.intWhich = 3;
						}
						else if (cmbRange.SelectedIndex == 4)
						{
							frmREAudit.InstancePtr.intWhich = 4;
						}
						else if (cmbRange.SelectedIndex == 5)
						{
							frmREAudit.InstancePtr.intWhich = 5;
						}
						else if (cmbRange.SelectedIndex == 6)
						{
							frmREAudit.InstancePtr.intWhich = 6;
						}
						frmREAudit.InstancePtr.strBegin = txtStart.Text;
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							if (cmbRange.SelectedIndex == 1)
							{
								frmREAudit.InstancePtr.strEnd = txtStart.Text + "zzz";
							}
							else
							{
								frmREAudit.InstancePtr.strEnd = txtStart.Text;
							}
						}
						else
						{
							if (cmbRange.SelectedIndex == 1)
							{
								frmREAudit.InstancePtr.strEnd = txtEnd.Text + "zzz";
							}
							else
							{
								frmREAudit.InstancePtr.strEnd = txtEnd.Text;
							}
						}
					}
				}
			}
			else
			{
				// from supplemental
				if (cmbAccounts.SelectedIndex == 0)
				{
					frmSupplementalBill.InstancePtr.TransferRE(-1);
				}
				else
				{
					if (cmbAccounts.SelectedIndex == 2)
						txtEnd.Text = "";
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						MessageBox.Show("You must specify a start range", "Specify Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						if (Conversion.Val(txtStart.Text) > 0)
						{
							strBegin = txtStart.Text;
							if (Conversion.Val(txtEnd.Text) > 0)
							{
								strEnd = txtEnd.Text;
							}
							else
							{
								strEnd = txtStart.Text;
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							MessageBox.Show("You must specify an end range", "Specify End", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						strBegin = txtStart.Text;
						strEnd = txtEnd.Text;
						if (cmbRange.SelectedIndex == 1)
						{
							strEnd += "zzz";
						}
					}
					if (cmbRange.SelectedIndex == 0)
					{
						intWhich = 0;
					}
					else if (cmbRange.SelectedIndex == 1)
					{
						intWhich = 1;
					}
					else if (cmbRange.SelectedIndex == 2)
					{
						intWhich = 2;
					}
					else if (cmbRange.SelectedIndex == 3)
					{
						intWhich = 3;
					}
					else if (cmbRange.SelectedIndex == 4)
					{
						intWhich = 4;
					}
					else if (cmbRange.SelectedIndex == 5)
					{
						intWhich = 5;
					}
					else if (cmbRange.SelectedIndex == 6)
					{
						intWhich = 6;
					}
					frmSupplementalBill.InstancePtr.TransferRE(intWhich, strBegin, strEnd);
				}
			}
			Close();
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, new System.EventArgs());
		}

		private void optAccounts_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// all
						txtStart.Visible = false;
						txtEnd.Visible = false;
						lblTo.Visible = false;
						cmbRange.Visible = false;
						ToolTip1.SetToolTip(txtStart, "");
						//FC:FINAL:CHN - issue #1283: Incorrect range option appearing. 
						lblRange.Text = "";
						break;
					}
				case 1:
					{
						// range
						txtStart.Visible = true;
						txtEnd.Visible = true;
						lblTo.Visible = true;
						cmbRange.Visible = true;
						lblRange.Text = "Range of";
						ToolTip1.SetToolTip(txtStart, "");
						break;
					}
				case 2:
					{
						// individual
						txtStart.Visible = true;
						txtEnd.Visible = false;
						lblTo.Visible = false;
						cmbRange.Visible = true;
						lblRange.Text = "Specific";
						ToolTip1.SetToolTip(txtStart, "All choices except for Name may also be entered as a list separated by commas.  Ex: 1,3,5,27 or Main Street,South Rd,First St");
						break;
					}
			}
			//end switch
		}

		private void optAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbAccounts.SelectedIndex);
			optAccounts_CheckedChanged(index, sender, e);
		}

		private void optDisplay_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						cmbSequence.Visible = false;
						//FC:FINAL:CHN - i.issue #1558: real estate acct-sequense option visibility.
						lblSequence.Visible = false;
						break;
					}
				case 1:
					{
						cmbSequence.Visible = true;
						//FC:FINAL:CHN - i.issue #1558: real estate acct-sequense option visibility.
						lblSequence.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optDisplay_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbDisplay.SelectedIndex);
			optDisplay_CheckedChanged(index, sender, e);
		}

		private void cmbAccounts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAccounts.SelectedIndex == 0)
			{
				optAccounts_CheckedChanged(sender, e);
			}
			else if (cmbAccounts.SelectedIndex == 1)
			{
				optAccounts_CheckedChanged(sender, e);
			}
			else if (cmbAccounts.SelectedIndex == 2)
			{
				optAccounts_CheckedChanged(sender, e);
			}
		}

		private void cmbDisplay_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbDisplay.SelectedIndex == 0)
			{
				optDisplay_CheckedChanged(sender, e);
			}
			else if (cmbDisplay.SelectedIndex == 1)
			{
				optDisplay_CheckedChanged(sender, e);
			}
		}
	}
}
