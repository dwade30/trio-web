//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	partial class frmCustomReport : BaseForm
	{
		public Wisej.Web.Panel Frame1;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCListBox lstTotal;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCFrame fraReports;
		public fecherFoundation.FCTextBox txtTitle;
		public FCGrid GridReport;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCListBox lstFields;
		public Wisej.Web.ImageList ImageList1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteReport;
		public fecherFoundation.FCButton cmdAddRow;
		public fecherFoundation.FCButton cmdAddColumn;
		public fecherFoundation.FCButton cmdDeleteRow;
		public fecherFoundation.FCButton cmdDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSaveNew;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCButton cmdSaveContinue;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomReport));
			Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.Frame1 = new Wisej.Web.Panel();
			this.line1 = new Wisej.Web.Line();
			this.vsLayout = new fecherFoundation.FCGrid();
			this.fraMessage = new fecherFoundation.FCFrame();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.lstTotal = new fecherFoundation.FCListBox();
			this.fraWhere = new fecherFoundation.FCFrame();
			this.WhereCriteriaPanel = new Wisej.Web.FlexLayoutPanel();
			this.AccountPanel = new Wisej.Web.TableLayoutPanel();
			this.txtAccountStart = new Wisej.Web.TextBox();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.txtAccountEnd = new Wisej.Web.TextBox();
			this.NamePanel = new Wisej.Web.TableLayoutPanel();
			this.txtNameStart = new Wisej.Web.TextBox();
			this.lblName = new fecherFoundation.FCLabel();
			this.txtNameEnd = new Wisej.Web.TextBox();
			this.SecondNamePanel = new Wisej.Web.TableLayoutPanel();
			this.txtSecondNameStart = new Wisej.Web.TextBox();
			this.lblSecondName = new fecherFoundation.FCLabel();
			this.txtSecondNameEnd = new Wisej.Web.TextBox();
			this.MapLotPanel = new Wisej.Web.TableLayoutPanel();
			this.txtMapLotEnd = new Wisej.Web.TextBox();
			this.txtMapLotStart = new Wisej.Web.TextBox();
			this.lblMapLot = new fecherFoundation.FCLabel();
			this.BillingTypePanel = new Wisej.Web.TableLayoutPanel();
			this.cmbBillingType = new Wisej.Web.ComboBox();
			this.lblBillingType = new fecherFoundation.FCLabel();
			this.BillingYearPanel = new Wisej.Web.TableLayoutPanel();
			this.cmbBillingYearEnd = new Wisej.Web.ComboBox();
			this.cmbBillingYearStart = new Wisej.Web.ComboBox();
			this.lblBillignYear = new fecherFoundation.FCLabel();
			this.OriginalTaxPanel = new Wisej.Web.TableLayoutPanel();
			this.txtOriginalTaxStart = new Wisej.Web.TextBox();
			this.lblOriginalTax = new fecherFoundation.FCLabel();
			this.txtOriginalTaxEnd = new Wisej.Web.TextBox();
			this.PaidToDatePanel = new Wisej.Web.TableLayoutPanel();
			this.txtPaidToDateEnd = new Wisej.Web.TextBox();
			this.txtPaidToDateStart = new Wisej.Web.TextBox();
			this.lblPaidToDate = new fecherFoundation.FCLabel();
			this.LandValuePanel = new Wisej.Web.TableLayoutPanel();
			this.txtLandValueStart = new Wisej.Web.TextBox();
			this.lblLandValue = new fecherFoundation.FCLabel();
			this.txtLandValueEnd = new Wisej.Web.TextBox();
			this.BuildingValuePanel = new Wisej.Web.TableLayoutPanel();
			this.txtBuildingValueStart = new Wisej.Web.TextBox();
			this.lblBuildingValue = new fecherFoundation.FCLabel();
			this.txtBuildingValueEnd = new Wisej.Web.TextBox();
			this.ExemptValuePanel = new Wisej.Web.TableLayoutPanel();
			this.txtExemptValueStart = new Wisej.Web.TextBox();
			this.lblExemptValue = new fecherFoundation.FCLabel();
			this.txtExemptValueEnd = new Wisej.Web.TextBox();
			this.REBillableValuePanel = new Wisej.Web.TableLayoutPanel();
			this.txtREBillableValueStart = new Wisej.Web.TextBox();
			this.lblREBillableValue = new fecherFoundation.FCLabel();
			this.txtREBillableValueEnd = new Wisej.Web.TextBox();
			this.TranCodePanel = new Wisej.Web.TableLayoutPanel();
			this.txtTranCodeStart = new Wisej.Web.TextBox();
			this.lblTranCode = new fecherFoundation.FCLabel();
			this.txtTranCodeEnd = new Wisej.Web.TextBox();
			this.LandCodePanel = new Wisej.Web.TableLayoutPanel();
			this.txtLandCodeStart = new Wisej.Web.TextBox();
			this.lblLandCode = new fecherFoundation.FCLabel();
			this.txtLandCodeEnd = new Wisej.Web.TextBox();
			this.BuildingCodePanel = new Wisej.Web.TableLayoutPanel();
			this.txtBuildingCodeStart = new Wisej.Web.TextBox();
			this.lblBuildingCode = new fecherFoundation.FCLabel();
			this.txtBuildingCodeEnd = new Wisej.Web.TextBox();
			this.AcresPanel = new Wisej.Web.TableLayoutPanel();
			this.txtAcresStart = new Wisej.Web.TextBox();
			this.lblAcres = new fecherFoundation.FCLabel();
			this.txtAcresEnd = new Wisej.Web.TextBox();
			this.PPBillableValuePanel = new Wisej.Web.TableLayoutPanel();
			this.txtPPBillableValueStart = new Wisej.Web.TextBox();
			this.lblPPBillableValue = new fecherFoundation.FCLabel();
			this.txtPPBillableValueEnd = new Wisej.Web.TextBox();
			this.Category1Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory1Start = new Wisej.Web.TextBox();
			this.lblCategory1 = new fecherFoundation.FCLabel();
			this.txtCategory1End = new Wisej.Web.TextBox();
			this.Category2Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory2Start = new Wisej.Web.TextBox();
			this.lblCategory2 = new fecherFoundation.FCLabel();
			this.txtCategory2End = new Wisej.Web.TextBox();
			this.Category3Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory3Start = new Wisej.Web.TextBox();
			this.lblCategory3 = new fecherFoundation.FCLabel();
			this.txtCategory3End = new Wisej.Web.TextBox();
			this.Category4Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory4Start = new Wisej.Web.TextBox();
			this.lblCategory4 = new fecherFoundation.FCLabel();
			this.txtCategory4End = new Wisej.Web.TextBox();
			this.Category5Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory5Start = new Wisej.Web.TextBox();
			this.lblCategory5 = new fecherFoundation.FCLabel();
			this.txtCategory5End = new Wisej.Web.TextBox();
			this.Category6Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory6Start = new Wisej.Web.TextBox();
			this.lblCategory6 = new fecherFoundation.FCLabel();
			this.txtCategory6End = new Wisej.Web.TextBox();
			this.Category7Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory7Start = new Wisej.Web.TextBox();
			this.lblCategory7 = new fecherFoundation.FCLabel();
			this.txtCategory7End = new Wisej.Web.TextBox();
			this.Category8Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory8Start = new Wisej.Web.TextBox();
			this.lblCategory8 = new fecherFoundation.FCLabel();
			this.txtCategory8End = new Wisej.Web.TextBox();
			this.Category9Panel = new Wisej.Web.TableLayoutPanel();
			this.txtCategory9Start = new Wisej.Web.TextBox();
			this.lblCategory9 = new fecherFoundation.FCLabel();
			this.txtCategory9End = new Wisej.Web.TextBox();
			this.fraReports = new fecherFoundation.FCFrame();
			this.txtTitle = new fecherFoundation.FCTextBox();
			this.GridReport = new fecherFoundation.FCGrid();
			this.Label4 = new fecherFoundation.FCLabel();
			this.fraSort = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCDraggableListBox();
			this.fraFields = new fecherFoundation.FCFrame();
			this.lstFields = new fecherFoundation.FCListBox();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuDeleteReport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmdAddRow = new fecherFoundation.FCButton();
			this.cmdAddColumn = new fecherFoundation.FCButton();
			this.cmdDeleteRow = new fecherFoundation.FCButton();
			this.cmdDeleteColumn = new fecherFoundation.FCButton();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdDeleteReport = new fecherFoundation.FCButton();
			this.cmdSaveAsNew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
			this.fraMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
			this.fraWhere.SuspendLayout();
			this.WhereCriteriaPanel.SuspendLayout();
			this.AccountPanel.SuspendLayout();
			this.NamePanel.SuspendLayout();
			this.SecondNamePanel.SuspendLayout();
			this.MapLotPanel.SuspendLayout();
			this.BillingTypePanel.SuspendLayout();
			this.BillingYearPanel.SuspendLayout();
			this.OriginalTaxPanel.SuspendLayout();
			this.PaidToDatePanel.SuspendLayout();
			this.LandValuePanel.SuspendLayout();
			this.BuildingValuePanel.SuspendLayout();
			this.ExemptValuePanel.SuspendLayout();
			this.REBillableValuePanel.SuspendLayout();
			this.TranCodePanel.SuspendLayout();
			this.LandCodePanel.SuspendLayout();
			this.BuildingCodePanel.SuspendLayout();
			this.AcresPanel.SuspendLayout();
			this.PPBillableValuePanel.SuspendLayout();
			this.Category1Panel.SuspendLayout();
			this.Category2Panel.SuspendLayout();
			this.Category3Panel.SuspendLayout();
			this.Category4Panel.SuspendLayout();
			this.Category5Panel.SuspendLayout();
			this.Category6Panel.SuspendLayout();
			this.Category7Panel.SuspendLayout();
			this.Category8Panel.SuspendLayout();
			this.Category9Panel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReports)).BeginInit();
			this.fraReports.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridReport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
			this.fraSort.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
			this.fraFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteReport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAsNew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 956);
			this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.fraWhere);
			this.ClientArea.Controls.Add(this.fraReports);
			this.ClientArea.Controls.Add(this.fraSort);
			this.ClientArea.Controls.Add(this.fraFields);
			this.ClientArea.Size = new System.Drawing.Size(1078, 606);
			this.ClientArea.Scroll += new Wisej.Web.ScrollEventHandler(this.ClientArea_Scroll);
			this.ClientArea.Controls.SetChildIndex(this.fraFields, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraSort, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraReports, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraWhere, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSaveAsNew);
			this.TopPanel.Controls.Add(this.cmdDeleteReport);
			this.TopPanel.Controls.Add(this.cmdAddRow);
			this.TopPanel.Controls.Add(this.cmdAddColumn);
			this.TopPanel.Controls.Add(this.cmdDeleteRow);
			this.TopPanel.Controls.Add(this.cmdDeleteColumn);
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteColumn, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteRow, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddColumn, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddRow, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteReport, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSaveAsNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(177, 30);
			this.HeaderText.Text = "Custom Report";
			// 
			// Frame1
			// 
			this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Frame1.AutoScroll = true;
			this.Frame1.Controls.Add(this.line1);
			this.Frame1.Controls.Add(this.vsLayout);
			this.Frame1.Controls.Add(this.fraMessage);
			this.Frame1.Controls.Add(this.Image1);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.ScrollBars = Wisej.Web.ScrollBars.Horizontal;
			this.Frame1.Size = new System.Drawing.Size(1018, 253);
			this.Frame1.TabIndex = 0;
			this.Frame1.TabStop = true;
			// 
			// line1
			// 
			this.line1.LineColor = System.Drawing.Color.Red;
			this.line1.LineSize = 2;
			this.line1.Location = new System.Drawing.Point(720, 15);
			this.line1.Name = "line1";
			this.line1.Orientation = Wisej.Web.Orientation.Vertical;
			this.line1.Size = new System.Drawing.Size(2, 217);
			// 
			// vsLayout
			// 
			this.vsLayout.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
			this.vsLayout.AllowUserToOrderColumns = true;
			this.vsLayout.AllowUserToResizeColumns = true;
			this.vsLayout.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Both;
			this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.vsLayout.FixedCols = 0;
			this.vsLayout.Location = new System.Drawing.Point(0, 45);
			this.vsLayout.Name = "vsLayout";
			this.vsLayout.ReadOnly = false;
			this.vsLayout.RowHeadersVisible = false;
			this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.EnableResizing;
			this.vsLayout.Rows = 1;
			this.vsLayout.ShowFocusCell = false;
			this.vsLayout.Size = new System.Drawing.Size(984, 187);
			this.vsLayout.TabIndex = 1;
			this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
			this.vsLayout.ColumnAdded += new Wisej.Web.DataGridViewColumnEventHandler(this.VsLayout_ColumnAdded);
			this.vsLayout.ColumnRemoved += new Wisej.Web.DataGridViewColumnEventHandler(this.VsLayout_ColumnRemoved);
			this.vsLayout.ColumnWidthChanged += new Wisej.Web.DataGridViewColumnEventHandler(this.vsLayout_ColumnWidthChanged);
			this.vsLayout.RowHeightChanged += new Wisej.Web.DataGridViewRowEventHandler(this.vsLayout_RowHeightChanged);
			this.vsLayout.ColumnStateChanged += new Wisej.Web.DataGridViewColumnStateChangedEventHandler(this.VsLayout_ColumnStateChanged);
			this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
			// 
			// fraMessage
			// 
			this.fraMessage.Controls.Add(this.Label3);
			this.fraMessage.Location = new System.Drawing.Point(41, 45);
			this.fraMessage.Name = "fraMessage";
			this.fraMessage.Size = new System.Drawing.Size(543, 88);
			this.fraMessage.TabIndex = 1;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(28, 23);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(488, 48);
			this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Image1
			// 
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(0, 15);
			this.Image1.Name = "Image1";
			this.Image1.Size = new System.Drawing.Size(984, 18);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.lstTotal);
			this.Frame2.Location = new System.Drawing.Point(722, 290);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(326, 300);
			this.Frame2.TabIndex = 3;
			this.Frame2.Text = "Fields To Total";
			// 
			// lstTotal
			// 
			this.lstTotal.BackColor = System.Drawing.SystemColors.Window;
			this.lstTotal.CheckBoxes = true;
			this.lstTotal.Location = new System.Drawing.Point(20, 30);
			this.lstTotal.Name = "lstTotal";
			this.lstTotal.Size = new System.Drawing.Size(286, 250);
			this.lstTotal.Style = 1;
			// 
			// fraWhere
			// 
			this.fraWhere.Controls.Add(this.WhereCriteriaPanel);
			this.fraWhere.Location = new System.Drawing.Point(30, 611);
			this.fraWhere.Name = "fraWhere";
			this.fraWhere.Size = new System.Drawing.Size(624, 345);
			this.fraWhere.TabIndex = 4;
			this.fraWhere.Text = "Select Search Criteria";
			this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
			// 
			// WhereCriteriaPanel
			// 
			this.WhereCriteriaPanel.AutoScroll = true;
			this.WhereCriteriaPanel.Controls.Add(this.AccountPanel);
			this.WhereCriteriaPanel.Controls.Add(this.NamePanel);
			this.WhereCriteriaPanel.Controls.Add(this.SecondNamePanel);
			this.WhereCriteriaPanel.Controls.Add(this.MapLotPanel);
			this.WhereCriteriaPanel.Controls.Add(this.BillingTypePanel);
			this.WhereCriteriaPanel.Controls.Add(this.BillingYearPanel);
			this.WhereCriteriaPanel.Controls.Add(this.OriginalTaxPanel);
			this.WhereCriteriaPanel.Controls.Add(this.PaidToDatePanel);
			this.WhereCriteriaPanel.Controls.Add(this.LandValuePanel);
			this.WhereCriteriaPanel.Controls.Add(this.BuildingValuePanel);
			this.WhereCriteriaPanel.Controls.Add(this.ExemptValuePanel);
			this.WhereCriteriaPanel.Controls.Add(this.REBillableValuePanel);
			this.WhereCriteriaPanel.Controls.Add(this.TranCodePanel);
			this.WhereCriteriaPanel.Controls.Add(this.LandCodePanel);
			this.WhereCriteriaPanel.Controls.Add(this.BuildingCodePanel);
			this.WhereCriteriaPanel.Controls.Add(this.AcresPanel);
			this.WhereCriteriaPanel.Controls.Add(this.PPBillableValuePanel);
			this.WhereCriteriaPanel.Controls.Add(this.Category1Panel);
			this.WhereCriteriaPanel.Controls.Add(this.Category2Panel);
			this.WhereCriteriaPanel.Controls.Add(this.Category3Panel);
			this.WhereCriteriaPanel.Controls.Add(this.Category4Panel);
			this.WhereCriteriaPanel.Controls.Add(this.Category5Panel);
			this.WhereCriteriaPanel.Controls.Add(this.Category6Panel);
			this.WhereCriteriaPanel.Controls.Add(this.Category7Panel);
			this.WhereCriteriaPanel.Controls.Add(this.Category8Panel);
			this.WhereCriteriaPanel.Controls.Add(this.Category9Panel);
			this.WhereCriteriaPanel.Location = new System.Drawing.Point(3, 21);
			this.WhereCriteriaPanel.Name = "WhereCriteriaPanel";
			this.WhereCriteriaPanel.ScrollBars = Wisej.Web.ScrollBars.Vertical;
			this.WhereCriteriaPanel.Size = new System.Drawing.Size(618, 315);
			this.WhereCriteriaPanel.TabIndex = 0;
			this.WhereCriteriaPanel.TabStop = true;
			this.WhereCriteriaPanel.Scroll += new Wisej.Web.ScrollEventHandler(this.WhereCriteriaPanel_Scroll);
			// 
			// AccountPanel
			// 
			this.AccountPanel.AutoSize = true;
			this.AccountPanel.ColumnCount = 3;
			this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountPanel.Controls.Add(this.txtAccountStart, 0, 0);
			this.AccountPanel.Controls.Add(this.lblAccount, 0, 0);
			this.AccountPanel.Controls.Add(this.txtAccountEnd, 2, 0);
			this.AccountPanel.Location = new System.Drawing.Point(0, 0);
			this.AccountPanel.Name = "AccountPanel";
			this.AccountPanel.RowCount = 1;
			this.AccountPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountPanel.Size = new System.Drawing.Size(590, 46);
			this.AccountPanel.TabIndex = 4;
			this.AccountPanel.TabStop = true;
			// 
			// txtAccountStart
			// 
			this.txtAccountStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtAccountStart.Location = new System.Drawing.Point(166, 3);
			this.txtAccountStart.Name = "txtAccountStart";
			this.txtAccountStart.Size = new System.Drawing.Size(207, 22);
			this.txtAccountStart.TabIndex = 5;
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(3, 3);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(123, 40);
			this.lblAccount.TabIndex = 1;
			this.lblAccount.Text = "ACCOUNT";
			this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtAccountEnd
			// 
			this.txtAccountEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtAccountEnd.Location = new System.Drawing.Point(379, 3);
			this.txtAccountEnd.Name = "txtAccountEnd";
			this.txtAccountEnd.Size = new System.Drawing.Size(208, 22);
			this.txtAccountEnd.TabIndex = 6;
			// 
			// NamePanel
			// 
			this.NamePanel.AutoSize = true;
			this.NamePanel.ColumnCount = 3;
			this.NamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.NamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.NamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.NamePanel.Controls.Add(this.txtNameStart, 0, 0);
			this.NamePanel.Controls.Add(this.lblName, 0, 0);
			this.NamePanel.Controls.Add(this.txtNameEnd, 2, 0);
			this.NamePanel.Location = new System.Drawing.Point(0, 52);
			this.NamePanel.Name = "NamePanel";
			this.NamePanel.RowCount = 1;
			this.NamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.NamePanel.Size = new System.Drawing.Size(590, 46);
			this.NamePanel.TabIndex = 5;
			this.NamePanel.TabStop = true;
			// 
			// txtNameStart
			// 
			this.txtNameStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtNameStart.Location = new System.Drawing.Point(166, 3);
			this.txtNameStart.Name = "txtNameStart";
			this.txtNameStart.Size = new System.Drawing.Size(207, 22);
			this.txtNameStart.TabIndex = 3;
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(3, 3);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(132, 40);
			this.lblName.TabIndex = 1;
			this.lblName.Text = "NAME";
			this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtNameEnd
			// 
			this.txtNameEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtNameEnd.Location = new System.Drawing.Point(379, 3);
			this.txtNameEnd.Name = "txtNameEnd";
			this.txtNameEnd.Size = new System.Drawing.Size(208, 22);
			this.txtNameEnd.TabIndex = 4;
			// 
			// SecondNamePanel
			// 
			this.SecondNamePanel.AutoSize = true;
			this.SecondNamePanel.ColumnCount = 3;
			this.SecondNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.SecondNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.SecondNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.SecondNamePanel.Controls.Add(this.txtSecondNameStart, 0, 0);
			this.SecondNamePanel.Controls.Add(this.lblSecondName, 0, 0);
			this.SecondNamePanel.Controls.Add(this.txtSecondNameEnd, 2, 0);
			this.SecondNamePanel.Location = new System.Drawing.Point(0, 104);
			this.SecondNamePanel.Name = "SecondNamePanel";
			this.SecondNamePanel.RowCount = 1;
			this.SecondNamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.SecondNamePanel.Size = new System.Drawing.Size(590, 46);
			this.SecondNamePanel.TabIndex = 6;
			this.SecondNamePanel.TabStop = true;
			// 
			// txtSecondNameStart
			// 
			this.txtSecondNameStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtSecondNameStart.Location = new System.Drawing.Point(166, 3);
			this.txtSecondNameStart.Name = "txtSecondNameStart";
			this.txtSecondNameStart.Size = new System.Drawing.Size(207, 22);
			this.txtSecondNameStart.TabIndex = 3;
			// 
			// lblSecondName
			// 
			this.lblSecondName.Location = new System.Drawing.Point(3, 3);
			this.lblSecondName.Name = "lblSecondName";
			this.lblSecondName.Size = new System.Drawing.Size(157, 40);
			this.lblSecondName.TabIndex = 1;
			this.lblSecondName.Text = "SECOND NAME";
			this.lblSecondName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSecondNameEnd
			// 
			this.txtSecondNameEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtSecondNameEnd.Location = new System.Drawing.Point(379, 3);
			this.txtSecondNameEnd.Name = "txtSecondNameEnd";
			this.txtSecondNameEnd.Size = new System.Drawing.Size(208, 22);
			this.txtSecondNameEnd.TabIndex = 4;
			// 
			// MapLotPanel
			// 
			this.MapLotPanel.AutoSize = true;
			this.MapLotPanel.ColumnCount = 3;
			this.MapLotPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.MapLotPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.MapLotPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.MapLotPanel.Controls.Add(this.txtMapLotEnd, 0, 0);
			this.MapLotPanel.Controls.Add(this.txtMapLotStart, 0, 0);
			this.MapLotPanel.Controls.Add(this.lblMapLot, 0, 0);
			this.MapLotPanel.Location = new System.Drawing.Point(0, 156);
			this.MapLotPanel.Name = "MapLotPanel";
			this.MapLotPanel.RowCount = 1;
			this.MapLotPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.MapLotPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Absolute, 20F));
			this.MapLotPanel.Size = new System.Drawing.Size(590, 46);
			this.MapLotPanel.TabIndex = 9;
			this.MapLotPanel.TabStop = true;
			// 
			// txtMapLotEnd
			// 
			this.txtMapLotEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtMapLotEnd.Location = new System.Drawing.Point(379, 3);
			this.txtMapLotEnd.Name = "txtMapLotEnd";
			this.txtMapLotEnd.Size = new System.Drawing.Size(208, 22);
			this.txtMapLotEnd.TabIndex = 6;
			// 
			// txtMapLotStart
			// 
			this.txtMapLotStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtMapLotStart.Location = new System.Drawing.Point(166, 3);
			this.txtMapLotStart.Name = "txtMapLotStart";
			this.txtMapLotStart.Size = new System.Drawing.Size(207, 22);
			this.txtMapLotStart.TabIndex = 5;
			// 
			// lblMapLot
			// 
			this.lblMapLot.Location = new System.Drawing.Point(3, 3);
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Size = new System.Drawing.Size(81, 40);
			this.lblMapLot.TabIndex = 1;
			this.lblMapLot.Text = "MAP LOT";
			this.lblMapLot.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// BillingTypePanel
			// 
			this.BillingTypePanel.AutoSize = true;
			this.BillingTypePanel.ColumnCount = 3;
			this.BillingTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.BillingTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BillingTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BillingTypePanel.Controls.Add(this.cmbBillingType, 0, 0);
			this.BillingTypePanel.Controls.Add(this.lblBillingType, 0, 0);
			this.BillingTypePanel.Location = new System.Drawing.Point(0, 208);
			this.BillingTypePanel.Name = "BillingTypePanel";
			this.BillingTypePanel.RowCount = 1;
			this.BillingTypePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BillingTypePanel.Size = new System.Drawing.Size(590, 46);
			this.BillingTypePanel.TabIndex = 10;
			this.BillingTypePanel.TabStop = true;
			// 
			// cmbBillingType
			// 
			this.cmbBillingType.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.cmbBillingType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBillingType.Location = new System.Drawing.Point(166, 3);
			this.cmbBillingType.Name = "cmbBillingType";
			this.cmbBillingType.Size = new System.Drawing.Size(207, 22);
			this.cmbBillingType.TabIndex = 5;
			this.cmbBillingType.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBillingType_KeyDown);
			// 
			// lblBillingType
			// 
			this.lblBillingType.Location = new System.Drawing.Point(3, 3);
			this.lblBillingType.Name = "lblBillingType";
			this.lblBillingType.Size = new System.Drawing.Size(157, 40);
			this.lblBillingType.TabIndex = 1;
			this.lblBillingType.Text = "BILLING TYPE";
			this.lblBillingType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// BillingYearPanel
			// 
			this.BillingYearPanel.AutoSize = true;
			this.BillingYearPanel.ColumnCount = 3;
			this.BillingYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.BillingYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BillingYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BillingYearPanel.Controls.Add(this.cmbBillingYearEnd, 2, 0);
			this.BillingYearPanel.Controls.Add(this.cmbBillingYearStart, 1, 0);
			this.BillingYearPanel.Controls.Add(this.lblBillignYear, 0, 0);
			this.BillingYearPanel.Location = new System.Drawing.Point(0, 260);
			this.BillingYearPanel.Name = "BillingYearPanel";
			this.BillingYearPanel.RowCount = 1;
			this.BillingYearPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BillingYearPanel.Size = new System.Drawing.Size(590, 46);
			this.BillingYearPanel.TabIndex = 11;
			this.BillingYearPanel.TabStop = true;
			// 
			// cmbBillingYearEnd
			// 
			this.cmbBillingYearEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.cmbBillingYearEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBillingYearEnd.Location = new System.Drawing.Point(379, 3);
			this.cmbBillingYearEnd.Name = "cmbBillingYearEnd";
			this.cmbBillingYearEnd.Size = new System.Drawing.Size(208, 22);
			this.cmbBillingYearEnd.TabIndex = 5;
			this.cmbBillingYearEnd.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBillingYearEnd_KeyDown);
			// 
			// cmbBillingYearStart
			// 
			this.cmbBillingYearStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.cmbBillingYearStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBillingYearStart.Location = new System.Drawing.Point(166, 3);
			this.cmbBillingYearStart.Name = "cmbBillingYearStart";
			this.cmbBillingYearStart.Size = new System.Drawing.Size(207, 22);
			this.cmbBillingYearStart.TabIndex = 4;
			this.cmbBillingYearStart.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBillingYearStart_KeyDown);
			// 
			// lblBillignYear
			// 
			this.lblBillignYear.Location = new System.Drawing.Point(3, 3);
			this.lblBillignYear.Name = "lblBillignYear";
			this.lblBillignYear.Size = new System.Drawing.Size(119, 40);
			this.lblBillignYear.TabIndex = 1;
			this.lblBillignYear.Text = "BILLING YEAR";
			this.lblBillignYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// OriginalTaxPanel
			// 
			this.OriginalTaxPanel.AutoSize = true;
			this.OriginalTaxPanel.ColumnCount = 3;
			this.OriginalTaxPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.OriginalTaxPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.OriginalTaxPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.OriginalTaxPanel.Controls.Add(this.txtOriginalTaxStart, 0, 0);
			this.OriginalTaxPanel.Controls.Add(this.lblOriginalTax, 0, 0);
			this.OriginalTaxPanel.Controls.Add(this.txtOriginalTaxEnd, 2, 0);
			this.OriginalTaxPanel.Location = new System.Drawing.Point(0, 312);
			this.OriginalTaxPanel.Name = "OriginalTaxPanel";
			this.OriginalTaxPanel.RowCount = 1;
			this.OriginalTaxPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.OriginalTaxPanel.Size = new System.Drawing.Size(590, 46);
			this.OriginalTaxPanel.TabIndex = 12;
			this.OriginalTaxPanel.TabStop = true;
			// 
			// txtOriginalTaxStart
			// 
			this.txtOriginalTaxStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtOriginalTaxStart.Location = new System.Drawing.Point(166, 3);
			this.txtOriginalTaxStart.Name = "txtOriginalTaxStart";
			this.txtOriginalTaxStart.Size = new System.Drawing.Size(207, 22);
			this.txtOriginalTaxStart.TabIndex = 3;
			// 
			// lblOriginalTax
			// 
			this.lblOriginalTax.Location = new System.Drawing.Point(3, 3);
			this.lblOriginalTax.Name = "lblOriginalTax";
			this.lblOriginalTax.Size = new System.Drawing.Size(132, 40);
			this.lblOriginalTax.TabIndex = 1;
			this.lblOriginalTax.Text = "ORIGINAL TAX DUE";
			this.lblOriginalTax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtOriginalTaxEnd
			// 
			this.txtOriginalTaxEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtOriginalTaxEnd.Location = new System.Drawing.Point(379, 3);
			this.txtOriginalTaxEnd.Name = "txtOriginalTaxEnd";
			this.txtOriginalTaxEnd.Size = new System.Drawing.Size(208, 22);
			this.txtOriginalTaxEnd.TabIndex = 4;
			// 
			// PaidToDatePanel
			// 
			this.PaidToDatePanel.AutoSize = true;
			this.PaidToDatePanel.ColumnCount = 3;
			this.PaidToDatePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.PaidToDatePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PaidToDatePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PaidToDatePanel.Controls.Add(this.txtPaidToDateEnd, 0, 0);
			this.PaidToDatePanel.Controls.Add(this.txtPaidToDateStart, 0, 0);
			this.PaidToDatePanel.Controls.Add(this.lblPaidToDate, 0, 0);
			this.PaidToDatePanel.Location = new System.Drawing.Point(0, 364);
			this.PaidToDatePanel.Name = "PaidToDatePanel";
			this.PaidToDatePanel.RowCount = 1;
			this.PaidToDatePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PaidToDatePanel.Size = new System.Drawing.Size(590, 46);
			this.PaidToDatePanel.TabIndex = 13;
			this.PaidToDatePanel.TabStop = true;
			// 
			// txtPaidToDateEnd
			// 
			this.txtPaidToDateEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtPaidToDateEnd.Location = new System.Drawing.Point(379, 3);
			this.txtPaidToDateEnd.Name = "txtPaidToDateEnd";
			this.txtPaidToDateEnd.Size = new System.Drawing.Size(208, 22);
			this.txtPaidToDateEnd.TabIndex = 5;
			// 
			// txtPaidToDateStart
			// 
			this.txtPaidToDateStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtPaidToDateStart.Location = new System.Drawing.Point(166, 3);
			this.txtPaidToDateStart.Name = "txtPaidToDateStart";
			this.txtPaidToDateStart.Size = new System.Drawing.Size(207, 22);
			this.txtPaidToDateStart.TabIndex = 4;
			// 
			// lblPaidToDate
			// 
			this.lblPaidToDate.Location = new System.Drawing.Point(3, 3);
			this.lblPaidToDate.Name = "lblPaidToDate";
			this.lblPaidToDate.Size = new System.Drawing.Size(157, 40);
			this.lblPaidToDate.TabIndex = 1;
			this.lblPaidToDate.Text = "PAID TO DATE";
			this.lblPaidToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// LandValuePanel
			// 
			this.LandValuePanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.LandValuePanel.AutoSize = true;
			this.LandValuePanel.ColumnCount = 3;
			this.LandValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.LandValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.LandValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.LandValuePanel.Controls.Add(this.txtLandValueStart, 0, 0);
			this.LandValuePanel.Controls.Add(this.lblLandValue, 0, 0);
			this.LandValuePanel.Controls.Add(this.txtLandValueEnd, 2, 0);
			this.LandValuePanel.Location = new System.Drawing.Point(3, 416);
			this.LandValuePanel.Name = "LandValuePanel";
			this.LandValuePanel.RowCount = 1;
			this.LandValuePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.LandValuePanel.Size = new System.Drawing.Size(590, 46);
			this.LandValuePanel.TabIndex = 14;
			this.LandValuePanel.TabStop = true;
			// 
			// txtLandValueStart
			// 
			this.txtLandValueStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtLandValueStart.Location = new System.Drawing.Point(166, 3);
			this.txtLandValueStart.Name = "txtLandValueStart";
			this.txtLandValueStart.Size = new System.Drawing.Size(207, 22);
			this.txtLandValueStart.TabIndex = 5;
			// 
			// lblLandValue
			// 
			this.lblLandValue.Location = new System.Drawing.Point(3, 3);
			this.lblLandValue.Name = "lblLandValue";
			this.lblLandValue.Size = new System.Drawing.Size(123, 40);
			this.lblLandValue.TabIndex = 1;
			this.lblLandValue.Text = "LAND VALUE";
			this.lblLandValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtLandValueEnd
			// 
			this.txtLandValueEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtLandValueEnd.Location = new System.Drawing.Point(379, 3);
			this.txtLandValueEnd.Name = "txtLandValueEnd";
			this.txtLandValueEnd.Size = new System.Drawing.Size(208, 22);
			this.txtLandValueEnd.TabIndex = 6;
			// 
			// BuildingValuePanel
			// 
			this.BuildingValuePanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.BuildingValuePanel.AutoSize = true;
			this.BuildingValuePanel.ColumnCount = 3;
			this.BuildingValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.BuildingValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BuildingValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BuildingValuePanel.Controls.Add(this.txtBuildingValueStart, 0, 0);
			this.BuildingValuePanel.Controls.Add(this.lblBuildingValue, 0, 0);
			this.BuildingValuePanel.Controls.Add(this.txtBuildingValueEnd, 2, 0);
			this.BuildingValuePanel.Location = new System.Drawing.Point(3, 468);
			this.BuildingValuePanel.Name = "BuildingValuePanel";
			this.BuildingValuePanel.RowCount = 1;
			this.BuildingValuePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BuildingValuePanel.Size = new System.Drawing.Size(590, 46);
			this.BuildingValuePanel.TabIndex = 15;
			this.BuildingValuePanel.TabStop = true;
			// 
			// txtBuildingValueStart
			// 
			this.txtBuildingValueStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtBuildingValueStart.Location = new System.Drawing.Point(166, 3);
			this.txtBuildingValueStart.Name = "txtBuildingValueStart";
			this.txtBuildingValueStart.Size = new System.Drawing.Size(207, 22);
			this.txtBuildingValueStart.TabIndex = 5;
			// 
			// lblBuildingValue
			// 
			this.lblBuildingValue.Location = new System.Drawing.Point(3, 3);
			this.lblBuildingValue.Name = "lblBuildingValue";
			this.lblBuildingValue.Size = new System.Drawing.Size(123, 40);
			this.lblBuildingValue.TabIndex = 1;
			this.lblBuildingValue.Text = "BUILDING VALUE";
			this.lblBuildingValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBuildingValueEnd
			// 
			this.txtBuildingValueEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtBuildingValueEnd.Location = new System.Drawing.Point(379, 3);
			this.txtBuildingValueEnd.Name = "txtBuildingValueEnd";
			this.txtBuildingValueEnd.Size = new System.Drawing.Size(208, 22);
			this.txtBuildingValueEnd.TabIndex = 6;
			// 
			// ExemptValuePanel
			// 
			this.ExemptValuePanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.ExemptValuePanel.AutoSize = true;
			this.ExemptValuePanel.ColumnCount = 3;
			this.ExemptValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.ExemptValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.ExemptValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.ExemptValuePanel.Controls.Add(this.txtExemptValueStart, 0, 0);
			this.ExemptValuePanel.Controls.Add(this.lblExemptValue, 0, 0);
			this.ExemptValuePanel.Controls.Add(this.txtExemptValueEnd, 2, 0);
			this.ExemptValuePanel.Location = new System.Drawing.Point(3, 520);
			this.ExemptValuePanel.Name = "ExemptValuePanel";
			this.ExemptValuePanel.RowCount = 1;
			this.ExemptValuePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.ExemptValuePanel.Size = new System.Drawing.Size(590, 46);
			this.ExemptValuePanel.TabIndex = 16;
			this.ExemptValuePanel.TabStop = true;
			// 
			// txtExemptValueStart
			// 
			this.txtExemptValueStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtExemptValueStart.Location = new System.Drawing.Point(166, 3);
			this.txtExemptValueStart.Name = "txtExemptValueStart";
			this.txtExemptValueStart.Size = new System.Drawing.Size(207, 22);
			this.txtExemptValueStart.TabIndex = 5;
			// 
			// lblExemptValue
			// 
			this.lblExemptValue.Location = new System.Drawing.Point(3, 3);
			this.lblExemptValue.Name = "lblExemptValue";
			this.lblExemptValue.Size = new System.Drawing.Size(123, 40);
			this.lblExemptValue.TabIndex = 1;
			this.lblExemptValue.Text = "EXEMPT VALUE";
			this.lblExemptValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtExemptValueEnd
			// 
			this.txtExemptValueEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtExemptValueEnd.Location = new System.Drawing.Point(379, 3);
			this.txtExemptValueEnd.Name = "txtExemptValueEnd";
			this.txtExemptValueEnd.Size = new System.Drawing.Size(208, 22);
			this.txtExemptValueEnd.TabIndex = 6;
			// 
			// REBillableValuePanel
			// 
			this.REBillableValuePanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.REBillableValuePanel.AutoSize = true;
			this.REBillableValuePanel.ColumnCount = 3;
			this.REBillableValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.REBillableValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.REBillableValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.REBillableValuePanel.Controls.Add(this.txtREBillableValueStart, 0, 0);
			this.REBillableValuePanel.Controls.Add(this.lblREBillableValue, 0, 0);
			this.REBillableValuePanel.Controls.Add(this.txtREBillableValueEnd, 2, 0);
			this.REBillableValuePanel.Location = new System.Drawing.Point(3, 572);
			this.REBillableValuePanel.Name = "REBillableValuePanel";
			this.REBillableValuePanel.RowCount = 1;
			this.REBillableValuePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.REBillableValuePanel.Size = new System.Drawing.Size(590, 46);
			this.REBillableValuePanel.TabIndex = 17;
			this.REBillableValuePanel.TabStop = true;
			// 
			// txtREBillableValueStart
			// 
			this.txtREBillableValueStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtREBillableValueStart.Location = new System.Drawing.Point(166, 3);
			this.txtREBillableValueStart.Name = "txtREBillableValueStart";
			this.txtREBillableValueStart.Size = new System.Drawing.Size(207, 22);
			this.txtREBillableValueStart.TabIndex = 5;
			// 
			// lblREBillableValue
			// 
			this.lblREBillableValue.Location = new System.Drawing.Point(3, 3);
			this.lblREBillableValue.Name = "lblREBillableValue";
			this.lblREBillableValue.Size = new System.Drawing.Size(154, 40);
			this.lblREBillableValue.TabIndex = 1;
			this.lblREBillableValue.Text = "RE BILLABLE VALUE";
			this.lblREBillableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtREBillableValueEnd
			// 
			this.txtREBillableValueEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtREBillableValueEnd.Location = new System.Drawing.Point(379, 3);
			this.txtREBillableValueEnd.Name = "txtREBillableValueEnd";
			this.txtREBillableValueEnd.Size = new System.Drawing.Size(208, 22);
			this.txtREBillableValueEnd.TabIndex = 6;
			// 
			// TranCodePanel
			// 
			this.TranCodePanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.TranCodePanel.AutoSize = true;
			this.TranCodePanel.ColumnCount = 3;
			this.TranCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.TranCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.TranCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.TranCodePanel.Controls.Add(this.txtTranCodeStart, 0, 0);
			this.TranCodePanel.Controls.Add(this.lblTranCode, 0, 0);
			this.TranCodePanel.Controls.Add(this.txtTranCodeEnd, 2, 0);
			this.TranCodePanel.Location = new System.Drawing.Point(3, 624);
			this.TranCodePanel.Name = "TranCodePanel";
			this.TranCodePanel.RowCount = 1;
			this.TranCodePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.TranCodePanel.Size = new System.Drawing.Size(590, 46);
			this.TranCodePanel.TabIndex = 18;
			this.TranCodePanel.TabStop = true;
			// 
			// txtTranCodeStart
			// 
			this.txtTranCodeStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtTranCodeStart.Location = new System.Drawing.Point(166, 3);
			this.txtTranCodeStart.Name = "txtTranCodeStart";
			this.txtTranCodeStart.Size = new System.Drawing.Size(207, 22);
			this.txtTranCodeStart.TabIndex = 5;
			// 
			// lblTranCode
			// 
			this.lblTranCode.Location = new System.Drawing.Point(3, 3);
			this.lblTranCode.Name = "lblTranCode";
			this.lblTranCode.Size = new System.Drawing.Size(123, 40);
			this.lblTranCode.TabIndex = 1;
			this.lblTranCode.Text = "TRAN CODE";
			this.lblTranCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTranCodeEnd
			// 
			this.txtTranCodeEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtTranCodeEnd.Location = new System.Drawing.Point(379, 3);
			this.txtTranCodeEnd.Name = "txtTranCodeEnd";
			this.txtTranCodeEnd.Size = new System.Drawing.Size(208, 22);
			this.txtTranCodeEnd.TabIndex = 6;
			// 
			// LandCodePanel
			// 
			this.LandCodePanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.LandCodePanel.AutoSize = true;
			this.LandCodePanel.ColumnCount = 3;
			this.LandCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.LandCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.LandCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.LandCodePanel.Controls.Add(this.txtLandCodeStart, 0, 0);
			this.LandCodePanel.Controls.Add(this.lblLandCode, 0, 0);
			this.LandCodePanel.Controls.Add(this.txtLandCodeEnd, 2, 0);
			this.LandCodePanel.Location = new System.Drawing.Point(3, 676);
			this.LandCodePanel.Name = "LandCodePanel";
			this.LandCodePanel.RowCount = 1;
			this.LandCodePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.LandCodePanel.Size = new System.Drawing.Size(590, 46);
			this.LandCodePanel.TabIndex = 19;
			this.LandCodePanel.TabStop = true;
			// 
			// txtLandCodeStart
			// 
			this.txtLandCodeStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtLandCodeStart.Location = new System.Drawing.Point(166, 3);
			this.txtLandCodeStart.Name = "txtLandCodeStart";
			this.txtLandCodeStart.Size = new System.Drawing.Size(207, 22);
			this.txtLandCodeStart.TabIndex = 5;
			// 
			// lblLandCode
			// 
			this.lblLandCode.Location = new System.Drawing.Point(3, 3);
			this.lblLandCode.Name = "lblLandCode";
			this.lblLandCode.Size = new System.Drawing.Size(123, 40);
			this.lblLandCode.TabIndex = 1;
			this.lblLandCode.Text = "LAND CODE";
			this.lblLandCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtLandCodeEnd
			// 
			this.txtLandCodeEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtLandCodeEnd.Location = new System.Drawing.Point(379, 3);
			this.txtLandCodeEnd.Name = "txtLandCodeEnd";
			this.txtLandCodeEnd.Size = new System.Drawing.Size(208, 22);
			this.txtLandCodeEnd.TabIndex = 6;
			// 
			// BuildingCodePanel
			// 
			this.BuildingCodePanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.BuildingCodePanel.AutoSize = true;
			this.BuildingCodePanel.ColumnCount = 3;
			this.BuildingCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.BuildingCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BuildingCodePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BuildingCodePanel.Controls.Add(this.txtBuildingCodeStart, 0, 0);
			this.BuildingCodePanel.Controls.Add(this.lblBuildingCode, 0, 0);
			this.BuildingCodePanel.Controls.Add(this.txtBuildingCodeEnd, 2, 0);
			this.BuildingCodePanel.Location = new System.Drawing.Point(3, 728);
			this.BuildingCodePanel.Name = "BuildingCodePanel";
			this.BuildingCodePanel.RowCount = 1;
			this.BuildingCodePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.BuildingCodePanel.Size = new System.Drawing.Size(590, 46);
			this.BuildingCodePanel.TabIndex = 20;
			this.BuildingCodePanel.TabStop = true;
			// 
			// txtBuildingCodeStart
			// 
			this.txtBuildingCodeStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtBuildingCodeStart.Location = new System.Drawing.Point(166, 3);
			this.txtBuildingCodeStart.Name = "txtBuildingCodeStart";
			this.txtBuildingCodeStart.Size = new System.Drawing.Size(207, 22);
			this.txtBuildingCodeStart.TabIndex = 5;
			// 
			// lblBuildingCode
			// 
			this.lblBuildingCode.Location = new System.Drawing.Point(3, 3);
			this.lblBuildingCode.Name = "lblBuildingCode";
			this.lblBuildingCode.Size = new System.Drawing.Size(123, 40);
			this.lblBuildingCode.TabIndex = 1;
			this.lblBuildingCode.Text = "BUILDING CODE";
			this.lblBuildingCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtBuildingCodeEnd
			// 
			this.txtBuildingCodeEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtBuildingCodeEnd.Location = new System.Drawing.Point(379, 3);
			this.txtBuildingCodeEnd.Name = "txtBuildingCodeEnd";
			this.txtBuildingCodeEnd.Size = new System.Drawing.Size(208, 22);
			this.txtBuildingCodeEnd.TabIndex = 6;
			// 
			// AcresPanel
			// 
			this.AcresPanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.AcresPanel.AutoSize = true;
			this.AcresPanel.ColumnCount = 3;
			this.AcresPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.AcresPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AcresPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AcresPanel.Controls.Add(this.txtAcresStart, 0, 0);
			this.AcresPanel.Controls.Add(this.lblAcres, 0, 0);
			this.AcresPanel.Controls.Add(this.txtAcresEnd, 2, 0);
			this.AcresPanel.Location = new System.Drawing.Point(3, 780);
			this.AcresPanel.Name = "AcresPanel";
			this.AcresPanel.RowCount = 1;
			this.AcresPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AcresPanel.Size = new System.Drawing.Size(590, 46);
			this.AcresPanel.TabIndex = 21;
			this.AcresPanel.TabStop = true;
			// 
			// txtAcresStart
			// 
			this.txtAcresStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtAcresStart.Location = new System.Drawing.Point(166, 3);
			this.txtAcresStart.Name = "txtAcresStart";
			this.txtAcresStart.Size = new System.Drawing.Size(207, 22);
			this.txtAcresStart.TabIndex = 5;
			// 
			// lblAcres
			// 
			this.lblAcres.Location = new System.Drawing.Point(3, 3);
			this.lblAcres.Name = "lblAcres";
			this.lblAcres.Size = new System.Drawing.Size(123, 40);
			this.lblAcres.TabIndex = 1;
			this.lblAcres.Text = "ACRES";
			this.lblAcres.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtAcresEnd
			// 
			this.txtAcresEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtAcresEnd.Location = new System.Drawing.Point(379, 3);
			this.txtAcresEnd.Name = "txtAcresEnd";
			this.txtAcresEnd.Size = new System.Drawing.Size(208, 22);
			this.txtAcresEnd.TabIndex = 6;
			// 
			// PPBillableValuePanel
			// 
			this.PPBillableValuePanel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.PPBillableValuePanel.AutoSize = true;
			this.PPBillableValuePanel.ColumnCount = 3;
			this.PPBillableValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.PPBillableValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PPBillableValuePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PPBillableValuePanel.Controls.Add(this.txtPPBillableValueStart, 0, 0);
			this.PPBillableValuePanel.Controls.Add(this.lblPPBillableValue, 0, 0);
			this.PPBillableValuePanel.Controls.Add(this.txtPPBillableValueEnd, 2, 0);
			this.PPBillableValuePanel.Location = new System.Drawing.Point(3, 832);
			this.PPBillableValuePanel.Name = "PPBillableValuePanel";
			this.PPBillableValuePanel.RowCount = 1;
			this.PPBillableValuePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PPBillableValuePanel.Size = new System.Drawing.Size(590, 46);
			this.PPBillableValuePanel.TabIndex = 22;
			this.PPBillableValuePanel.TabStop = true;
			// 
			// txtPPBillableValueStart
			// 
			this.txtPPBillableValueStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtPPBillableValueStart.Location = new System.Drawing.Point(166, 3);
			this.txtPPBillableValueStart.Name = "txtPPBillableValueStart";
			this.txtPPBillableValueStart.Size = new System.Drawing.Size(207, 22);
			this.txtPPBillableValueStart.TabIndex = 5;
			// 
			// lblPPBillableValue
			// 
			this.lblPPBillableValue.Location = new System.Drawing.Point(3, 3);
			this.lblPPBillableValue.Name = "lblPPBillableValue";
			this.lblPPBillableValue.Size = new System.Drawing.Size(146, 40);
			this.lblPPBillableValue.TabIndex = 1;
			this.lblPPBillableValue.Text = "PP BILLABLE VALUE";
			this.lblPPBillableValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtPPBillableValueEnd
			// 
			this.txtPPBillableValueEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtPPBillableValueEnd.Location = new System.Drawing.Point(379, 3);
			this.txtPPBillableValueEnd.Name = "txtPPBillableValueEnd";
			this.txtPPBillableValueEnd.Size = new System.Drawing.Size(208, 22);
			this.txtPPBillableValueEnd.TabIndex = 6;
			// 
			// Category1Panel
			// 
			this.Category1Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category1Panel.AutoSize = true;
			this.Category1Panel.ColumnCount = 3;
			this.Category1Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category1Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category1Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category1Panel.Controls.Add(this.txtCategory1Start, 0, 0);
			this.Category1Panel.Controls.Add(this.lblCategory1, 0, 0);
			this.Category1Panel.Controls.Add(this.txtCategory1End, 2, 0);
			this.Category1Panel.Location = new System.Drawing.Point(3, 884);
			this.Category1Panel.Name = "Category1Panel";
			this.Category1Panel.RowCount = 1;
			this.Category1Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category1Panel.Size = new System.Drawing.Size(590, 46);
			this.Category1Panel.TabIndex = 23;
			this.Category1Panel.TabStop = true;
			// 
			// txtCategory1Start
			// 
			this.txtCategory1Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory1Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory1Start.Name = "txtCategory1Start";
			this.txtCategory1Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory1Start.TabIndex = 5;
			// 
			// lblCategory1
			// 
			this.lblCategory1.Location = new System.Drawing.Point(3, 3);
			this.lblCategory1.Name = "lblCategory1";
			this.lblCategory1.Size = new System.Drawing.Size(146, 40);
			this.lblCategory1.TabIndex = 1;
			this.lblCategory1.Text = "CATEGORY 1";
			this.lblCategory1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory1End
			// 
			this.txtCategory1End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory1End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory1End.Name = "txtCategory1End";
			this.txtCategory1End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory1End.TabIndex = 6;
			// 
			// Category2Panel
			// 
			this.Category2Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category2Panel.AutoSize = true;
			this.Category2Panel.ColumnCount = 3;
			this.Category2Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category2Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category2Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category2Panel.Controls.Add(this.txtCategory2Start, 0, 0);
			this.Category2Panel.Controls.Add(this.lblCategory2, 0, 0);
			this.Category2Panel.Controls.Add(this.txtCategory2End, 2, 0);
			this.Category2Panel.Location = new System.Drawing.Point(3, 936);
			this.Category2Panel.Name = "Category2Panel";
			this.Category2Panel.RowCount = 1;
			this.Category2Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category2Panel.Size = new System.Drawing.Size(590, 46);
			this.Category2Panel.TabIndex = 24;
			this.Category2Panel.TabStop = true;
			// 
			// txtCategory2Start
			// 
			this.txtCategory2Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory2Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory2Start.Name = "txtCategory2Start";
			this.txtCategory2Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory2Start.TabIndex = 5;
			// 
			// lblCategory2
			// 
			this.lblCategory2.Location = new System.Drawing.Point(3, 3);
			this.lblCategory2.Name = "lblCategory2";
			this.lblCategory2.Size = new System.Drawing.Size(146, 40);
			this.lblCategory2.TabIndex = 1;
			this.lblCategory2.Text = "CATEGORY 2";
			this.lblCategory2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory2End
			// 
			this.txtCategory2End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory2End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory2End.Name = "txtCategory2End";
			this.txtCategory2End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory2End.TabIndex = 6;
			// 
			// Category3Panel
			// 
			this.Category3Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category3Panel.AutoSize = true;
			this.Category3Panel.ColumnCount = 3;
			this.Category3Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category3Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category3Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category3Panel.Controls.Add(this.txtCategory3Start, 0, 0);
			this.Category3Panel.Controls.Add(this.lblCategory3, 0, 0);
			this.Category3Panel.Controls.Add(this.txtCategory3End, 2, 0);
			this.Category3Panel.Location = new System.Drawing.Point(3, 988);
			this.Category3Panel.Name = "Category3Panel";
			this.Category3Panel.RowCount = 1;
			this.Category3Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category3Panel.Size = new System.Drawing.Size(590, 46);
			this.Category3Panel.TabIndex = 25;
			this.Category3Panel.TabStop = true;
			// 
			// txtCategory3Start
			// 
			this.txtCategory3Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory3Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory3Start.Name = "txtCategory3Start";
			this.txtCategory3Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory3Start.TabIndex = 5;
			// 
			// lblCategory3
			// 
			this.lblCategory3.Location = new System.Drawing.Point(3, 3);
			this.lblCategory3.Name = "lblCategory3";
			this.lblCategory3.Size = new System.Drawing.Size(146, 40);
			this.lblCategory3.TabIndex = 1;
			this.lblCategory3.Text = "CATEGORY 3";
			this.lblCategory3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory3End
			// 
			this.txtCategory3End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory3End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory3End.Name = "txtCategory3End";
			this.txtCategory3End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory3End.TabIndex = 6;
			// 
			// Category4Panel
			// 
			this.Category4Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category4Panel.AutoSize = true;
			this.Category4Panel.ColumnCount = 3;
			this.Category4Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category4Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category4Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category4Panel.Controls.Add(this.txtCategory4Start, 0, 0);
			this.Category4Panel.Controls.Add(this.lblCategory4, 0, 0);
			this.Category4Panel.Controls.Add(this.txtCategory4End, 2, 0);
			this.Category4Panel.Location = new System.Drawing.Point(3, 1040);
			this.Category4Panel.Name = "Category4Panel";
			this.Category4Panel.RowCount = 1;
			this.Category4Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category4Panel.Size = new System.Drawing.Size(590, 46);
			this.Category4Panel.TabIndex = 26;
			this.Category4Panel.TabStop = true;
			// 
			// txtCategory4Start
			// 
			this.txtCategory4Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory4Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory4Start.Name = "txtCategory4Start";
			this.txtCategory4Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory4Start.TabIndex = 5;
			// 
			// lblCategory4
			// 
			this.lblCategory4.Location = new System.Drawing.Point(3, 3);
			this.lblCategory4.Name = "lblCategory4";
			this.lblCategory4.Size = new System.Drawing.Size(146, 40);
			this.lblCategory4.TabIndex = 1;
			this.lblCategory4.Text = "CATEGORY 4";
			this.lblCategory4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory4End
			// 
			this.txtCategory4End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory4End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory4End.Name = "txtCategory4End";
			this.txtCategory4End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory4End.TabIndex = 6;
			// 
			// Category5Panel
			// 
			this.Category5Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category5Panel.AutoSize = true;
			this.Category5Panel.ColumnCount = 3;
			this.Category5Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category5Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category5Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category5Panel.Controls.Add(this.txtCategory5Start, 0, 0);
			this.Category5Panel.Controls.Add(this.lblCategory5, 0, 0);
			this.Category5Panel.Controls.Add(this.txtCategory5End, 2, 0);
			this.Category5Panel.Location = new System.Drawing.Point(3, 1092);
			this.Category5Panel.Name = "Category5Panel";
			this.Category5Panel.RowCount = 1;
			this.Category5Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category5Panel.Size = new System.Drawing.Size(590, 46);
			this.Category5Panel.TabIndex = 27;
			this.Category5Panel.TabStop = true;
			// 
			// txtCategory5Start
			// 
			this.txtCategory5Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory5Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory5Start.Name = "txtCategory5Start";
			this.txtCategory5Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory5Start.TabIndex = 5;
			// 
			// lblCategory5
			// 
			this.lblCategory5.Location = new System.Drawing.Point(3, 3);
			this.lblCategory5.Name = "lblCategory5";
			this.lblCategory5.Size = new System.Drawing.Size(146, 40);
			this.lblCategory5.TabIndex = 1;
			this.lblCategory5.Text = "CATEGORY 5";
			this.lblCategory5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory5End
			// 
			this.txtCategory5End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory5End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory5End.Name = "txtCategory5End";
			this.txtCategory5End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory5End.TabIndex = 6;
			// 
			// Category6Panel
			// 
			this.Category6Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category6Panel.AutoSize = true;
			this.Category6Panel.ColumnCount = 3;
			this.Category6Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category6Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category6Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category6Panel.Controls.Add(this.txtCategory6Start, 0, 0);
			this.Category6Panel.Controls.Add(this.lblCategory6, 0, 0);
			this.Category6Panel.Controls.Add(this.txtCategory6End, 2, 0);
			this.Category6Panel.Location = new System.Drawing.Point(3, 1144);
			this.Category6Panel.Name = "Category6Panel";
			this.Category6Panel.RowCount = 1;
			this.Category6Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category6Panel.Size = new System.Drawing.Size(590, 46);
			this.Category6Panel.TabIndex = 28;
			this.Category6Panel.TabStop = true;
			// 
			// txtCategory6Start
			// 
			this.txtCategory6Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory6Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory6Start.Name = "txtCategory6Start";
			this.txtCategory6Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory6Start.TabIndex = 5;
			// 
			// lblCategory6
			// 
			this.lblCategory6.Location = new System.Drawing.Point(3, 3);
			this.lblCategory6.Name = "lblCategory6";
			this.lblCategory6.Size = new System.Drawing.Size(146, 40);
			this.lblCategory6.TabIndex = 1;
			this.lblCategory6.Text = "CATEGORY 6";
			this.lblCategory6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory6End
			// 
			this.txtCategory6End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory6End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory6End.Name = "txtCategory6End";
			this.txtCategory6End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory6End.TabIndex = 6;
			// 
			// Category7Panel
			// 
			this.Category7Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category7Panel.AutoSize = true;
			this.Category7Panel.ColumnCount = 3;
			this.Category7Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category7Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category7Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category7Panel.Controls.Add(this.txtCategory7Start, 0, 0);
			this.Category7Panel.Controls.Add(this.lblCategory7, 0, 0);
			this.Category7Panel.Controls.Add(this.txtCategory7End, 2, 0);
			this.Category7Panel.Location = new System.Drawing.Point(3, 1196);
			this.Category7Panel.Name = "Category7Panel";
			this.Category7Panel.RowCount = 1;
			this.Category7Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category7Panel.Size = new System.Drawing.Size(590, 46);
			this.Category7Panel.TabIndex = 29;
			this.Category7Panel.TabStop = true;
			// 
			// txtCategory7Start
			// 
			this.txtCategory7Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory7Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory7Start.Name = "txtCategory7Start";
			this.txtCategory7Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory7Start.TabIndex = 5;
			// 
			// lblCategory7
			// 
			this.lblCategory7.Location = new System.Drawing.Point(3, 3);
			this.lblCategory7.Name = "lblCategory7";
			this.lblCategory7.Size = new System.Drawing.Size(146, 40);
			this.lblCategory7.TabIndex = 1;
			this.lblCategory7.Text = "CATEGORY 7";
			this.lblCategory7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory7End
			// 
			this.txtCategory7End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory7End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory7End.Name = "txtCategory7End";
			this.txtCategory7End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory7End.TabIndex = 6;
			// 
			// Category8Panel
			// 
			this.Category8Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category8Panel.AutoSize = true;
			this.Category8Panel.ColumnCount = 3;
			this.Category8Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category8Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category8Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category8Panel.Controls.Add(this.txtCategory8Start, 0, 0);
			this.Category8Panel.Controls.Add(this.lblCategory8, 0, 0);
			this.Category8Panel.Controls.Add(this.txtCategory8End, 2, 0);
			this.Category8Panel.Location = new System.Drawing.Point(3, 1248);
			this.Category8Panel.Name = "Category8Panel";
			this.Category8Panel.RowCount = 1;
			this.Category8Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category8Panel.Size = new System.Drawing.Size(590, 46);
			this.Category8Panel.TabIndex = 30;
			this.Category8Panel.TabStop = true;
			// 
			// txtCategory8Start
			// 
			this.txtCategory8Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory8Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory8Start.Name = "txtCategory8Start";
			this.txtCategory8Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory8Start.TabIndex = 5;
			// 
			// lblCategory8
			// 
			this.lblCategory8.Location = new System.Drawing.Point(3, 3);
			this.lblCategory8.Name = "lblCategory8";
			this.lblCategory8.Size = new System.Drawing.Size(146, 40);
			this.lblCategory8.TabIndex = 1;
			this.lblCategory8.Text = "CATEGORY 8";
			this.lblCategory8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory8End
			// 
			this.txtCategory8End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory8End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory8End.Name = "txtCategory8End";
			this.txtCategory8End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory8End.TabIndex = 6;
			// 
			// Category9Panel
			// 
			this.Category9Panel.Anchor = Wisej.Web.AnchorStyles.Left;
			this.Category9Panel.AutoSize = true;
			this.Category9Panel.ColumnCount = 3;
			this.Category9Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 163F));
			this.Category9Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category9Panel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category9Panel.Controls.Add(this.txtCategory9Start, 0, 0);
			this.Category9Panel.Controls.Add(this.lblCategory9, 0, 0);
			this.Category9Panel.Controls.Add(this.txtCategory9End, 2, 0);
			this.Category9Panel.Location = new System.Drawing.Point(3, 1300);
			this.Category9Panel.Name = "Category9Panel";
			this.Category9Panel.RowCount = 1;
			this.Category9Panel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.Category9Panel.Size = new System.Drawing.Size(590, 46);
			this.Category9Panel.TabIndex = 31;
			this.Category9Panel.TabStop = true;
			// 
			// txtCategory9Start
			// 
			this.txtCategory9Start.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory9Start.Location = new System.Drawing.Point(166, 3);
			this.txtCategory9Start.Name = "txtCategory9Start";
			this.txtCategory9Start.Size = new System.Drawing.Size(207, 22);
			this.txtCategory9Start.TabIndex = 5;
			// 
			// lblCategory9
			// 
			this.lblCategory9.Location = new System.Drawing.Point(3, 3);
			this.lblCategory9.Name = "lblCategory9";
			this.lblCategory9.Size = new System.Drawing.Size(146, 40);
			this.lblCategory9.TabIndex = 1;
			this.lblCategory9.Text = "CATEGORY 9";
			this.lblCategory9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtCategory9End
			// 
			this.txtCategory9End.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtCategory9End.Location = new System.Drawing.Point(379, 3);
			this.txtCategory9End.Name = "txtCategory9End";
			this.txtCategory9End.Size = new System.Drawing.Size(208, 22);
			this.txtCategory9End.TabIndex = 6;
			// 
			// fraReports
			// 
			this.fraReports.Controls.Add(this.txtTitle);
			this.fraReports.Controls.Add(this.GridReport);
			this.fraReports.Controls.Add(this.Label4);
			this.fraReports.Location = new System.Drawing.Point(676, 611);
			this.fraReports.Name = "fraReports";
			this.fraReports.Size = new System.Drawing.Size(372, 341);
			this.fraReports.TabIndex = 5;
			this.fraReports.Text = "Report";
			// 
			// txtTitle
			// 
			this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle.Location = new System.Drawing.Point(89, 280);
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Size = new System.Drawing.Size(264, 40);
			this.txtTitle.TabIndex = 2;
			// 
			// GridReport
			// 
			this.GridReport.ExtendLastCol = true;
			this.GridReport.FixedCols = 0;
			this.GridReport.Location = new System.Drawing.Point(20, 30);
			this.GridReport.Name = "GridReport";
			this.GridReport.RowHeadersVisible = false;
			this.GridReport.Rows = 1;
			this.GridReport.ShowFocusCell = false;
			this.GridReport.Size = new System.Drawing.Size(333, 232);
			this.GridReport.TabIndex = 3;
			this.GridReport.Click += new System.EventHandler(this.GridReport_ClickEvent);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 294);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(70, 18);
			this.Label4.TabIndex = 1;
			this.Label4.Text = "TITLE";
			// 
			// fraSort
			// 
			this.fraSort.Controls.Add(this.lstSort);
			this.fraSort.Location = new System.Drawing.Point(375, 290);
			this.fraSort.Name = "fraSort";
			this.fraSort.Size = new System.Drawing.Size(326, 300);
			this.fraSort.TabIndex = 2;
			this.fraSort.Text = "Fields To Sort By";
			this.ToolTip1.SetToolTip(this.fraSort, "Click and drag items in the Sort by column to change their order.");
			this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
			// 
			// lstSort
			// 
			this.lstSort.BackColor = System.Drawing.SystemColors.Window;
			this.lstSort.CheckBoxes = true;
			this.lstSort.Location = new System.Drawing.Point(20, 30);
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(286, 250);
			this.lstSort.Style = 1;
			this.ToolTip1.SetToolTip(this.lstSort, "Click and drag items in the Sort by column to change their order.");
			// 
			// fraFields
			// 
			this.fraFields.Controls.Add(this.lstFields);
			this.fraFields.Location = new System.Drawing.Point(30, 290);
			this.fraFields.Name = "fraFields";
			this.fraFields.Size = new System.Drawing.Size(326, 300);
			this.fraFields.TabIndex = 1;
			this.fraFields.Text = "Fields To Display On Report";
			// 
			// lstFields
			// 
			this.lstFields.BackColor = System.Drawing.SystemColors.Window;
			this.lstFields.Location = new System.Drawing.Point(20, 30);
			this.lstFields.Name = "lstFields";
			this.lstFields.Size = new System.Drawing.Size(286, 250);
			this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry2});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDeleteReport,
            this.mnuSP21,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP1,
            this.mnuSaveNew,
            this.mnuSave});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuDeleteReport
			// 
			this.mnuDeleteReport.Index = 0;
			this.mnuDeleteReport.Name = "mnuDeleteReport";
			this.mnuDeleteReport.Text = "Delete Report";
			this.mnuDeleteReport.Click += new System.EventHandler(this.mnuDeleteReport_Click);
			// 
			// mnuSP21
			// 
			this.mnuSP21.Index = 1;
			this.mnuSP21.Name = "mnuSP21";
			this.mnuSP21.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 2;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Index = 3;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Text = "Print/Preview";
			this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 4;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuSaveNew
			// 
			this.mnuSaveNew.Index = 5;
			this.mnuSaveNew.Name = "mnuSaveNew";
			this.mnuSaveNew.Text = "Save as New";
			this.mnuSaveNew.Click += new System.EventHandler(this.mnuSaveNew_Click);
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 6;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(959, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(143, 24);
			this.cmdClear.TabIndex = 16;
			this.cmdClear.Text = "Clear Search Criteria";
			this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// cmdAddRow
			// 
			this.cmdAddRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddRow.Location = new System.Drawing.Point(583, 29);
			this.cmdAddRow.Name = "cmdAddRow";
			this.cmdAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.cmdAddRow.Size = new System.Drawing.Size(71, 24);
			this.cmdAddRow.TabIndex = 10;
			this.cmdAddRow.Text = "Add Row";
			this.cmdAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
			// 
			// cmdAddColumn
			// 
			this.cmdAddColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddColumn.Location = new System.Drawing.Point(658, 29);
			this.cmdAddColumn.Name = "cmdAddColumn";
			this.cmdAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdAddColumn.Size = new System.Drawing.Size(94, 24);
			this.cmdAddColumn.TabIndex = 11;
			this.cmdAddColumn.Text = "Add Column";
			this.cmdAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
			// 
			// cmdDeleteRow
			// 
			this.cmdDeleteRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteRow.Location = new System.Drawing.Point(756, 29);
			this.cmdDeleteRow.Name = "cmdDeleteRow";
			this.cmdDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdDeleteRow.Size = new System.Drawing.Size(88, 24);
			this.cmdDeleteRow.TabIndex = 12;
			this.cmdDeleteRow.Text = "Delete Row";
			this.cmdDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
			// 
			// cmdDeleteColumn
			// 
			this.cmdDeleteColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteColumn.Location = new System.Drawing.Point(848, 29);
			this.cmdDeleteColumn.Name = "cmdDeleteColumn";
			this.cmdDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdDeleteColumn.Size = new System.Drawing.Size(107, 24);
			this.cmdDeleteColumn.TabIndex = 13;
			this.cmdDeleteColumn.Text = "Delete Column";
			this.cmdDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.Anchor = Wisej.Web.AnchorStyles.Top;
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(367, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(186, 48);
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// cmdDeleteReport
			// 
			this.cmdDeleteReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteReport.Location = new System.Drawing.Point(472, 29);
			this.cmdDeleteReport.Name = "cmdDeleteReport";
			this.cmdDeleteReport.Size = new System.Drawing.Size(105, 24);
			this.cmdDeleteReport.TabIndex = 14;
			this.cmdDeleteReport.Text = "Delete Report";
			this.cmdDeleteReport.Click += new System.EventHandler(this.mnuDeleteReport_Click);
			// 
			// cmdSaveAsNew
			// 
			this.cmdSaveAsNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSaveAsNew.Location = new System.Drawing.Point(361, 29);
			this.cmdSaveAsNew.Name = "cmdSaveAsNew";
			this.cmdSaveAsNew.Size = new System.Drawing.Size(105, 24);
			this.cmdSaveAsNew.TabIndex = 15;
			this.cmdSaveAsNew.Text = "Save As New";
			this.cmdSaveAsNew.Click += new System.EventHandler(this.mnuSaveNew_Click);
			// 
			// frmCustomReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmCustomReport";
			this.Text = "Custom Report";
			this.Load += new System.EventHandler(this.frmCustomReport_Load);
			this.Activated += new System.EventHandler(this.frmCustomReport_Activated);
			this.Resize += new System.EventHandler(this.frmCustomReport_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomReport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
			this.fraMessage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
			this.fraWhere.ResumeLayout(false);
			this.WhereCriteriaPanel.ResumeLayout(false);
			this.WhereCriteriaPanel.PerformLayout();
			this.AccountPanel.ResumeLayout(false);
			this.AccountPanel.PerformLayout();
			this.NamePanel.ResumeLayout(false);
			this.NamePanel.PerformLayout();
			this.SecondNamePanel.ResumeLayout(false);
			this.SecondNamePanel.PerformLayout();
			this.MapLotPanel.ResumeLayout(false);
			this.MapLotPanel.PerformLayout();
			this.BillingTypePanel.ResumeLayout(false);
			this.BillingTypePanel.PerformLayout();
			this.BillingYearPanel.ResumeLayout(false);
			this.BillingYearPanel.PerformLayout();
			this.OriginalTaxPanel.ResumeLayout(false);
			this.OriginalTaxPanel.PerformLayout();
			this.PaidToDatePanel.ResumeLayout(false);
			this.PaidToDatePanel.PerformLayout();
			this.LandValuePanel.ResumeLayout(false);
			this.LandValuePanel.PerformLayout();
			this.BuildingValuePanel.ResumeLayout(false);
			this.BuildingValuePanel.PerformLayout();
			this.ExemptValuePanel.ResumeLayout(false);
			this.ExemptValuePanel.PerformLayout();
			this.REBillableValuePanel.ResumeLayout(false);
			this.REBillableValuePanel.PerformLayout();
			this.TranCodePanel.ResumeLayout(false);
			this.TranCodePanel.PerformLayout();
			this.LandCodePanel.ResumeLayout(false);
			this.LandCodePanel.PerformLayout();
			this.BuildingCodePanel.ResumeLayout(false);
			this.BuildingCodePanel.PerformLayout();
			this.AcresPanel.ResumeLayout(false);
			this.AcresPanel.PerformLayout();
			this.PPBillableValuePanel.ResumeLayout(false);
			this.PPBillableValuePanel.PerformLayout();
			this.Category1Panel.ResumeLayout(false);
			this.Category1Panel.PerformLayout();
			this.Category2Panel.ResumeLayout(false);
			this.Category2Panel.PerformLayout();
			this.Category3Panel.ResumeLayout(false);
			this.Category3Panel.PerformLayout();
			this.Category4Panel.ResumeLayout(false);
			this.Category4Panel.PerformLayout();
			this.Category5Panel.ResumeLayout(false);
			this.Category5Panel.PerformLayout();
			this.Category6Panel.ResumeLayout(false);
			this.Category6Panel.PerformLayout();
			this.Category7Panel.ResumeLayout(false);
			this.Category7Panel.PerformLayout();
			this.Category8Panel.ResumeLayout(false);
			this.Category8Panel.PerformLayout();
			this.Category9Panel.ResumeLayout(false);
			this.Category9Panel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReports)).EndInit();
			this.fraReports.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridReport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
			this.fraSort.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
			this.fraFields.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteReport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAsNew)).EndInit();
			this.ResumeLayout(false);

		}




        #endregion

        private System.ComponentModel.IContainer components;
		public FCButton cmdSaveAsNew;
		public FCButton cmdDeleteReport;
        private Line line1;
		private FlexLayoutPanel WhereCriteriaPanel;
		private TableLayoutPanel AccountPanel;
		private TextBox txtAccountStart;
		private FCLabel lblAccount;
		private TextBox txtAccountEnd;
		private TableLayoutPanel NamePanel;
		private TextBox txtNameStart;
		private FCLabel lblName;
		private TextBox txtNameEnd;
		private TableLayoutPanel SecondNamePanel;
		private TextBox txtSecondNameStart;
		private FCLabel lblSecondName;
		private TextBox txtSecondNameEnd;
		private TableLayoutPanel MapLotPanel;
		private FCLabel lblMapLot;
		private TextBox txtMapLotStart;
		private TableLayoutPanel BillingTypePanel;
		private ComboBox cmbBillingType;
		private FCLabel lblBillingType;
		private TableLayoutPanel BillingYearPanel;
		private ComboBox cmbBillingYearEnd;
		private ComboBox cmbBillingYearStart;
		private FCLabel lblBillignYear;
		private TableLayoutPanel OriginalTaxPanel;
		private TextBox txtOriginalTaxStart;
		private FCLabel lblOriginalTax;
		private TextBox txtOriginalTaxEnd;
		private TextBox txtMapLotEnd;
		private TableLayoutPanel PaidToDatePanel;
		private FCLabel lblPaidToDate;
		private TableLayoutPanel LandValuePanel;
		private TextBox txtLandValueStart;
		private FCLabel lblLandValue;
		private TextBox txtLandValueEnd;
		private TableLayoutPanel BuildingValuePanel;
		private TextBox txtBuildingValueStart;
		private FCLabel lblBuildingValue;
		private TextBox txtBuildingValueEnd;
		private TableLayoutPanel ExemptValuePanel;
		private TextBox txtExemptValueStart;
		private FCLabel lblExemptValue;
		private TextBox txtExemptValueEnd;
		private TableLayoutPanel REBillableValuePanel;
		private TextBox txtREBillableValueStart;
		private FCLabel lblREBillableValue;
		private TextBox txtREBillableValueEnd;
		private TableLayoutPanel TranCodePanel;
		private TextBox txtTranCodeStart;
		private FCLabel lblTranCode;
		private TextBox txtTranCodeEnd;
		private TableLayoutPanel LandCodePanel;
		private TextBox txtLandCodeStart;
		private FCLabel lblLandCode;
		private TextBox txtLandCodeEnd;
		private TableLayoutPanel BuildingCodePanel;
		private TextBox txtBuildingCodeStart;
		private FCLabel lblBuildingCode;
		private TextBox txtBuildingCodeEnd;
		private TableLayoutPanel AcresPanel;
		private TextBox txtAcresStart;
		private FCLabel lblAcres;
		private TextBox txtAcresEnd;
		private TableLayoutPanel PPBillableValuePanel;
		private TextBox txtPPBillableValueStart;
		private FCLabel lblPPBillableValue;
		private TextBox txtPPBillableValueEnd;
		private TableLayoutPanel Category1Panel;
		private TextBox txtCategory1Start;
		private FCLabel lblCategory1;
		private TextBox txtCategory1End;
		private TableLayoutPanel Category2Panel;
		private TextBox txtCategory2Start;
		private FCLabel lblCategory2;
		private TextBox txtCategory2End;
		private TableLayoutPanel Category3Panel;
		private TextBox txtCategory3Start;
		private FCLabel lblCategory3;
		private TextBox txtCategory3End;
		private TableLayoutPanel Category4Panel;
		private TextBox txtCategory4Start;
		private FCLabel lblCategory4;
		private TextBox txtCategory4End;
		private TableLayoutPanel Category5Panel;
		private TextBox txtCategory5Start;
		private FCLabel lblCategory5;
		private TextBox txtCategory5End;
		private TableLayoutPanel Category6Panel;
		private TextBox txtCategory6Start;
		private FCLabel lblCategory6;
		private TextBox txtCategory6End;
		private TableLayoutPanel Category7Panel;
		private TextBox txtCategory7Start;
		private FCLabel lblCategory7;
		private TextBox txtCategory7End;
		private TableLayoutPanel Category8Panel;
		private TextBox txtCategory8Start;
		private FCLabel lblCategory8;
		private TextBox txtCategory8End;
		private TableLayoutPanel Category9Panel;
		private TextBox txtCategory9Start;
		private FCLabel lblCategory9;
		private TextBox txtCategory9End;
		private TextBox txtPaidToDateEnd;
		private TextBox txtPaidToDateStart;
	}
}