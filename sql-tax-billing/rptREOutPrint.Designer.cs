﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREOutPrint.
	/// </summary>
	partial class rptREOutPrint
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptREOutPrint));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPaidToDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDist1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPeriod2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalPP = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalPP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblExemptPP = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExemptPP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAssessPP = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAssessPP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPPTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalPPTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidToDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExemptPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPPTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPPTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLand,
				this.txtAccount,
				this.txtName,
				this.txtBldg,
				this.txtPaidToDate,
				this.txtTax,
				this.txtMapLot,
				this.Label1,
				this.txtDist1,
				this.txtDist2,
				this.txtDist3,
				this.txtDist4,
				this.txtDist5,
				this.txtPeriod1,
				this.txtPeriod3,
				this.txtPeriod4,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.txtPeriod2,
				this.txtLocation,
				this.txtAddress1,
				this.txtAddress2,
				this.txtAddress3,
				this.Label20,
				this.txtHomestead,
				this.lblCat1,
				this.txtCat1,
				this.lblCat2,
				this.txtCat2,
				this.Label21,
				this.txtOther,
				this.lblCat3,
				this.txtCat3,
				this.lblCat49,
				this.txtCat49,
				this.lblTotalPP,
				this.txtTotalPP,
				this.lblExemptPP,
				this.txtExemptPP,
				this.lblAssessPP,
				this.txtAssessPP
			});
			this.Detail.Height = 1.145833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCount,
				this.Label10,
				this.Label22,
				this.txtTotalLand,
				this.Label23,
				this.txtTotalHomestead,
				this.Label25,
				this.txtTotalBuilding,
				this.Label26,
				this.txtTotalOther,
				this.Label27,
				this.txtTotalAssessment,
				this.Label28,
				this.txtTotalTax,
				this.Label29,
				this.txtTotalExempt,
				this.lblPPTotal,
				this.txtTotalPPTotal
			});
			this.ReportFooter.Height = 0.53125F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblMuniname,
				this.lblTitle,
				this.txtPage,
				this.lblPage,
				this.txtDate,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label17,
				this.Label18,
				this.Label19,
				this.txtTime
			});
			this.PageHeader.Height = 0.4791667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.19F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "text-align: left";
			this.lblMuniname.Tag = "textbox";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
            this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 3F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Tag = "bold";
			this.lblTitle.Text = "Real Estate Out-Print Report";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 4F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 9.3125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Tag = "textbox";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 0.625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.19F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 8.4375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "text-align: right";
			this.lblPage.Tag = "textbox";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.15625F;
			this.lblPage.Width = 0.875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 8.625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Tag = "textbox";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Tag = "bold";
			this.Label11.Text = "Account";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 0.625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold";
			this.Label12.Tag = "bold";
			this.Label12.Text = "Name / Address";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 1.6875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.5625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Tag = "bold";
			this.Label13.Text = "Land";
			this.Label13.Top = 0.3125F;
			this.Label13.Width = 0.875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 6.5625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Tag = "bold";
			this.Label14.Text = "Building";
			this.Label14.Top = 0.3125F;
			this.Label14.Width = 1.0625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 7.625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Tag = "bold";
			this.Label15.Text = "Paid To Date";
			this.Label15.Top = 0.3125F;
			this.Label15.Width = 1.125F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 8.9375F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold; text-align: right";
			this.Label17.Tag = "bold";
			this.Label17.Text = "Tax";
			this.Label17.Top = 0.3125F;
			this.Label17.Width = 1F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold; text-align: left";
			this.Label18.Tag = "bold";
			this.Label18.Text = "Map/Lot";
			this.Label18.Top = 0.3125F;
			this.Label18.Width = 1.0625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2.4375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold";
			this.Label19.Tag = "bold";
			this.Label19.Text = "Location";
			this.Label19.Top = 0.3125F;
			this.Label19.Width = 0.875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0.6875F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "text-align: left";
			this.txtTime.Tag = "textbox";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.DataField = "landval";
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 5.4375F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.OutputFormat = resources.GetString("txtLand.OutputFormat");
			this.txtLand.Style = "font-size: 10pt; text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 0F;
			this.txtLand.Width = 1F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 10pt; text-align: right";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.625F;
			// 
			// txtName
			// 
			this.txtName.CanShrink = true;
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.6875F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 10pt";
			this.txtName.Tag = "textbox";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 1.6875F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.DataField = "bldgval";
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 6.5F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.OutputFormat = resources.GetString("txtBldg.OutputFormat");
			this.txtBldg.Style = "font-size: 10pt; text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0F;
			this.txtBldg.Width = 1.125F;
			// 
			// txtPaidToDate
			// 
			this.txtPaidToDate.CanGrow = false;
			this.txtPaidToDate.DataField = "exemptval";
			this.txtPaidToDate.Height = 0.19F;
			this.txtPaidToDate.Left = 7.6875F;
			this.txtPaidToDate.MultiLine = false;
			this.txtPaidToDate.Name = "txtPaidToDate";
			this.txtPaidToDate.OutputFormat = resources.GetString("txtPaidToDate.OutputFormat");
			this.txtPaidToDate.Style = "font-size: 10pt; text-align: right";
			this.txtPaidToDate.Tag = "textbox";
			this.txtPaidToDate.Text = null;
			this.txtPaidToDate.Top = 0F;
			this.txtPaidToDate.Width = 1.0625F;
			// 
			// txtTax
			// 
			this.txtTax.CanGrow = false;
			this.txtTax.DataField = "taxval";
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 8.8125F;
			this.txtTax.Name = "txtTax";
			this.txtTax.OutputFormat = resources.GetString("txtTax.OutputFormat");
			this.txtTax.Style = "font-size: 10pt; text-align: right";
			this.txtTax.Tag = "textbox";
			this.txtTax.Text = null;
			this.txtTax.Top = 0F;
			this.txtTax.Width = 1.1875F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 3.875F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.5F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.1875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Tag = "bold";
			this.Label1.Text = "Dist. 1";
			this.Label1.Top = 0.65625F;
			this.Label1.Width = 0.8125F;
			// 
			// txtDist1
			// 
			this.txtDist1.Height = 0.19F;
			this.txtDist1.Left = 0F;
			this.txtDist1.Name = "txtDist1";
			this.txtDist1.Style = "text-align: right";
			this.txtDist1.Tag = "textbox";
			this.txtDist1.Text = null;
			this.txtDist1.Top = 0.8125F;
			this.txtDist1.Width = 1F;
			// 
			// txtDist2
			// 
			this.txtDist2.Height = 0.19F;
			this.txtDist2.Left = 1.0625F;
			this.txtDist2.Name = "txtDist2";
			this.txtDist2.Style = "text-align: right";
			this.txtDist2.Tag = "textbox";
			this.txtDist2.Text = null;
			this.txtDist2.Top = 0.8125F;
			this.txtDist2.Width = 0.9375F;
			// 
			// txtDist3
			// 
			this.txtDist3.Height = 0.19F;
			this.txtDist3.Left = 2.0625F;
			this.txtDist3.Name = "txtDist3";
			this.txtDist3.Style = "text-align: right";
			this.txtDist3.Tag = "textbox";
			this.txtDist3.Text = null;
			this.txtDist3.Top = 0.8125F;
			this.txtDist3.Width = 0.9375F;
			// 
			// txtDist4
			// 
			this.txtDist4.Height = 0.19F;
			this.txtDist4.Left = 3.0625F;
			this.txtDist4.Name = "txtDist4";
			this.txtDist4.Style = "text-align: right";
			this.txtDist4.Tag = "textbox";
			this.txtDist4.Text = null;
			this.txtDist4.Top = 0.8125F;
			this.txtDist4.Width = 0.9375F;
			// 
			// txtDist5
			// 
			this.txtDist5.Height = 0.19F;
			this.txtDist5.Left = 4.0625F;
			this.txtDist5.Name = "txtDist5";
			this.txtDist5.Style = "text-align: right";
			this.txtDist5.Tag = "textbox";
			this.txtDist5.Text = null;
			this.txtDist5.Top = 0.8125F;
			this.txtDist5.Width = 0.9375F;
			// 
			// txtPeriod1
			// 
			this.txtPeriod1.Height = 0.19F;
			this.txtPeriod1.Left = 5.0625F;
			this.txtPeriod1.MultiLine = false;
			this.txtPeriod1.Name = "txtPeriod1";
			this.txtPeriod1.Style = "text-align: right";
			this.txtPeriod1.Tag = "textbox";
			this.txtPeriod1.Text = null;
			this.txtPeriod1.Top = 0.8125F;
			this.txtPeriod1.Width = 1.1875F;
			// 
			// txtPeriod3
			// 
			this.txtPeriod3.Height = 0.19F;
			this.txtPeriod3.Left = 7.5625F;
			this.txtPeriod3.MultiLine = false;
			this.txtPeriod3.Name = "txtPeriod3";
			this.txtPeriod3.Style = "text-align: right";
			this.txtPeriod3.Tag = "textbox";
			this.txtPeriod3.Text = null;
			this.txtPeriod3.Top = 0.8125F;
			this.txtPeriod3.Width = 1.1875F;
			// 
			// txtPeriod4
			// 
			this.txtPeriod4.Height = 0.19F;
			this.txtPeriod4.Left = 8.8125F;
			this.txtPeriod4.MultiLine = false;
			this.txtPeriod4.Name = "txtPeriod4";
			this.txtPeriod4.Style = "text-align: right";
			this.txtPeriod4.Tag = "textbox";
			this.txtPeriod4.Text = null;
			this.txtPeriod4.Top = 0.8125F;
			this.txtPeriod4.Width = 1.1875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: right";
			this.Label2.Tag = "bold";
			this.Label2.Text = "Dist. 2";
			this.Label2.Top = 0.65625F;
			this.Label2.Width = 0.9375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Tag = "bold";
			this.Label3.Text = "Dist. 3";
			this.Label3.Top = 0.65625F;
			this.Label3.Width = 0.9375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Tag = "bold";
			this.Label4.Text = "Dist. 4";
			this.Label4.Top = 0.65625F;
			this.Label4.Width = 0.9375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Tag = "bold";
			this.Label5.Text = "Dist. 5";
			this.Label5.Top = 0.65625F;
			this.Label5.Width = 0.9375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.3125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Tag = "bold";
			this.Label6.Text = "Period 1";
			this.Label6.Top = 0.65625F;
			this.Label6.Width = 0.9375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.5625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Tag = "bold";
			this.Label7.Text = "Period 2";
			this.Label7.Top = 0.65625F;
			this.Label7.Width = 0.9375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 7.75F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Tag = "bold";
			this.Label8.Text = "Period 3";
			this.Label8.Top = 0.65625F;
			this.Label8.Width = 1F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 9.125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Period 4";
			this.Label9.Top = 0.65625F;
			this.Label9.Width = 0.875F;
			// 
			// txtPeriod2
			// 
			this.txtPeriod2.Height = 0.19F;
			this.txtPeriod2.Left = 6.3125F;
			this.txtPeriod2.MultiLine = false;
			this.txtPeriod2.Name = "txtPeriod2";
			this.txtPeriod2.Style = "text-align: right";
			this.txtPeriod2.Tag = "textbox";
			this.txtPeriod2.Text = null;
			this.txtPeriod2.Top = 0.8125F;
			this.txtPeriod2.Width = 1.1875F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 2.4375F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0F;
			this.txtLocation.Width = 1.375F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.CanGrow = false;
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 0.6875F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-size: 10pt";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 1.6875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.CanGrow = false;
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 0.6875F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-size: 10pt";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 1.6875F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.CanGrow = false;
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.Left = 0.6875F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Style = "font-size: 10pt";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.46875F;
			this.txtAddress3.Width = 1.6875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.4375F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold; text-align: left";
			this.Label20.Tag = "bold";
			this.Label20.Text = "Homestead";
			this.Label20.Top = 0.15625F;
			this.Label20.Width = 1.0625F;
			// 
			// txtHomestead
			// 
			this.txtHomestead.CanGrow = false;
			this.txtHomestead.Height = 0.19F;
			this.txtHomestead.Left = 3.5625F;
			this.txtHomestead.Name = "txtHomestead";
			this.txtHomestead.Style = "text-align: right";
			this.txtHomestead.Tag = "textbox";
			this.txtHomestead.Text = null;
			this.txtHomestead.Top = 0.15625F;
			this.txtHomestead.Width = 1.125F;
			// 
			// lblCat1
			// 
			this.lblCat1.Height = 0.19F;
			this.lblCat1.HyperLink = null;
			this.lblCat1.Left = 2.4375F;
			this.lblCat1.Name = "lblCat1";
			this.lblCat1.Style = "font-weight: bold; text-align: left";
			this.lblCat1.Tag = "bold";
			this.lblCat1.Text = "Category 1";
			this.lblCat1.Top = 0.3125F;
			this.lblCat1.Width = 1.0625F;
			// 
			// txtCat1
			// 
			this.txtCat1.CanGrow = false;
			this.txtCat1.Height = 0.19F;
			this.txtCat1.Left = 3.5625F;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.Style = "text-align: right";
			this.txtCat1.Tag = "textbox";
			this.txtCat1.Text = null;
			this.txtCat1.Top = 0.3125F;
			this.txtCat1.Width = 1.125F;
			// 
			// lblCat2
			// 
			this.lblCat2.Height = 0.19F;
			this.lblCat2.HyperLink = null;
			this.lblCat2.Left = 2.4375F;
			this.lblCat2.Name = "lblCat2";
			this.lblCat2.Style = "font-weight: bold; text-align: left";
			this.lblCat2.Tag = "bold";
			this.lblCat2.Text = "Category 2";
			this.lblCat2.Top = 0.46875F;
			this.lblCat2.Width = 1.0625F;
			// 
			// txtCat2
			// 
			this.txtCat2.CanGrow = false;
			this.txtCat2.Height = 0.19F;
			this.txtCat2.Left = 3.5625F;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.Style = "text-align: right";
			this.txtCat2.Tag = "textbox";
			this.txtCat2.Text = null;
			this.txtCat2.Top = 0.46875F;
			this.txtCat2.Width = 1.125F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold; text-align: left";
			this.Label21.Tag = "bold";
			this.Label21.Text = "Other Exempt";
			this.Label21.Top = 0.15625F;
			this.Label21.Width = 1.125F;
			// 
			// txtOther
			// 
			this.txtOther.CanGrow = false;
			this.txtOther.Height = 0.19F;
			this.txtOther.Left = 6.0625F;
			this.txtOther.Name = "txtOther";
			this.txtOther.Style = "text-align: right";
			this.txtOther.Tag = "textbox";
			this.txtOther.Text = null;
			this.txtOther.Top = 0.15625F;
			this.txtOther.Width = 1.0625F;
			// 
			// lblCat3
			// 
			this.lblCat3.Height = 0.19F;
			this.lblCat3.HyperLink = null;
			this.lblCat3.Left = 4.875F;
			this.lblCat3.Name = "lblCat3";
			this.lblCat3.Style = "font-weight: bold; text-align: left";
			this.lblCat3.Tag = "bold";
			this.lblCat3.Text = "Category 3";
			this.lblCat3.Top = 0.3125F;
			this.lblCat3.Width = 1.125F;
			// 
			// txtCat3
			// 
			this.txtCat3.CanGrow = false;
			this.txtCat3.Height = 0.19F;
			this.txtCat3.Left = 6.0625F;
			this.txtCat3.Name = "txtCat3";
			this.txtCat3.Style = "text-align: right";
			this.txtCat3.Tag = "textbox";
			this.txtCat3.Text = null;
			this.txtCat3.Top = 0.3125F;
			this.txtCat3.Width = 1.0625F;
			// 
			// lblCat49
			// 
			this.lblCat49.Height = 0.19F;
			this.lblCat49.HyperLink = null;
			this.lblCat49.Left = 4.875F;
			this.lblCat49.Name = "lblCat49";
			this.lblCat49.Style = "font-weight: bold; text-align: left";
			this.lblCat49.Tag = "bold";
			this.lblCat49.Text = "Category 4-9";
			this.lblCat49.Top = 0.46875F;
			this.lblCat49.Width = 1.125F;
			// 
			// txtCat49
			// 
			this.txtCat49.CanGrow = false;
			this.txtCat49.Height = 0.19F;
			this.txtCat49.Left = 6.0625F;
			this.txtCat49.Name = "txtCat49";
			this.txtCat49.Style = "text-align: right";
			this.txtCat49.Tag = "textbox";
			this.txtCat49.Text = null;
			this.txtCat49.Top = 0.46875F;
			this.txtCat49.Width = 1.0625F;
			// 
			// lblTotalPP
			// 
			this.lblTotalPP.Height = 0.19F;
			this.lblTotalPP.HyperLink = null;
			this.lblTotalPP.Left = 7.3125F;
			this.lblTotalPP.Name = "lblTotalPP";
			this.lblTotalPP.Style = "font-weight: bold; text-align: left";
			this.lblTotalPP.Tag = "bold";
			this.lblTotalPP.Text = "Total PP";
			this.lblTotalPP.Top = 0.15625F;
			this.lblTotalPP.Width = 1.125F;
			// 
			// txtTotalPP
			// 
			this.txtTotalPP.CanGrow = false;
			this.txtTotalPP.Height = 0.19F;
			this.txtTotalPP.Left = 8.5F;
			this.txtTotalPP.Name = "txtTotalPP";
			this.txtTotalPP.Style = "text-align: right";
			this.txtTotalPP.Tag = "textbox";
			this.txtTotalPP.Text = null;
			this.txtTotalPP.Top = 0.15625F;
			this.txtTotalPP.Width = 1.0625F;
			// 
			// lblExemptPP
			// 
			this.lblExemptPP.Height = 0.19F;
			this.lblExemptPP.HyperLink = null;
			this.lblExemptPP.Left = 7.3125F;
			this.lblExemptPP.Name = "lblExemptPP";
			this.lblExemptPP.Style = "font-weight: bold; text-align: left";
			this.lblExemptPP.Tag = "bold";
			this.lblExemptPP.Text = "Exempt PP";
			this.lblExemptPP.Top = 0.3125F;
			this.lblExemptPP.Width = 1.125F;
			// 
			// txtExemptPP
			// 
			this.txtExemptPP.CanGrow = false;
			this.txtExemptPP.Height = 0.19F;
			this.txtExemptPP.Left = 8.5F;
			this.txtExemptPP.Name = "txtExemptPP";
			this.txtExemptPP.Style = "text-align: right";
			this.txtExemptPP.Tag = "textbox";
			this.txtExemptPP.Text = null;
			this.txtExemptPP.Top = 0.3125F;
			this.txtExemptPP.Width = 1.0625F;
			// 
			// lblAssessPP
			// 
			this.lblAssessPP.Height = 0.19F;
			this.lblAssessPP.HyperLink = null;
			this.lblAssessPP.Left = 7.3125F;
			this.lblAssessPP.Name = "lblAssessPP";
			this.lblAssessPP.Style = "font-weight: bold; text-align: left";
			this.lblAssessPP.Tag = "bold";
			this.lblAssessPP.Text = "Assessment";
			this.lblAssessPP.Top = 0.46875F;
			this.lblAssessPP.Width = 1.125F;
			// 
			// txtAssessPP
			// 
			this.txtAssessPP.CanGrow = false;
			this.txtAssessPP.Height = 0.19F;
			this.txtAssessPP.Left = 8.5F;
			this.txtAssessPP.Name = "txtAssessPP";
			this.txtAssessPP.Style = "text-align: right";
			this.txtAssessPP.Tag = "textbox";
			this.txtAssessPP.Text = null;
			this.txtAssessPP.Top = 0.46875F;
			this.txtAssessPP.Width = 1.0625F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 1.25F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "text-align: right";
			this.txtCount.Tag = "textbox";
			this.txtCount.Text = null;
			this.txtCount.Top = 0.34375F;
			this.txtCount.Width = 1.125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Tag = "bold";
			this.Label10.Text = "Count";
			this.Label10.Top = 0.34375F;
			this.Label10.Width = 0.6875F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.19F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-weight: bold; text-align: left";
			this.Label22.Tag = "bold";
			this.Label22.Text = "Land";
			this.Label22.Top = 0.03125F;
			this.Label22.Width = 1.0625F;
			// 
			// txtTotalLand
			// 
			this.txtTotalLand.CanGrow = false;
			this.txtTotalLand.Height = 0.19F;
			this.txtTotalLand.Left = 1.25F;
			this.txtTotalLand.Name = "txtTotalLand";
			this.txtTotalLand.Style = "text-align: right";
			this.txtTotalLand.Tag = "textbox";
			this.txtTotalLand.Text = null;
			this.txtTotalLand.Top = 0.03125F;
			this.txtTotalLand.Width = 1.125F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.125F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-weight: bold; text-align: left";
			this.Label23.Tag = "bold";
			this.Label23.Text = "Homestead";
			this.Label23.Top = 0.1875F;
			this.Label23.Width = 1.0625F;
			// 
			// txtTotalHomestead
			// 
			this.txtTotalHomestead.CanGrow = false;
			this.txtTotalHomestead.Height = 0.19F;
			this.txtTotalHomestead.Left = 1.25F;
			this.txtTotalHomestead.Name = "txtTotalHomestead";
			this.txtTotalHomestead.Style = "text-align: right";
			this.txtTotalHomestead.Tag = "textbox";
			this.txtTotalHomestead.Text = null;
			this.txtTotalHomestead.Top = 0.1875F;
			this.txtTotalHomestead.Width = 1.125F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 2.4375F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-weight: bold; text-align: left";
			this.Label25.Tag = "bold";
			this.Label25.Text = "Building";
			this.Label25.Top = 0.03125F;
			this.Label25.Width = 1.0625F;
			// 
			// txtTotalBuilding
			// 
			this.txtTotalBuilding.CanGrow = false;
			this.txtTotalBuilding.Height = 0.19F;
			this.txtTotalBuilding.Left = 3.5625F;
			this.txtTotalBuilding.Name = "txtTotalBuilding";
			this.txtTotalBuilding.Style = "text-align: right";
			this.txtTotalBuilding.Tag = "textbox";
			this.txtTotalBuilding.Text = null;
			this.txtTotalBuilding.Top = 0.03125F;
			this.txtTotalBuilding.Width = 1.125F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.19F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 2.4375F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-weight: bold; text-align: left";
			this.Label26.Tag = "bold";
			this.Label26.Text = "Other";
			this.Label26.Top = 0.1875F;
			this.Label26.Width = 1.0625F;
			// 
			// txtTotalOther
			// 
			this.txtTotalOther.CanGrow = false;
			this.txtTotalOther.Height = 0.19F;
			this.txtTotalOther.Left = 3.5625F;
			this.txtTotalOther.Name = "txtTotalOther";
			this.txtTotalOther.Style = "text-align: right";
			this.txtTotalOther.Tag = "textbox";
			this.txtTotalOther.Text = null;
			this.txtTotalOther.Top = 0.1875F;
			this.txtTotalOther.Width = 1.125F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.19F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 7.625F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-weight: bold; text-align: left";
			this.Label27.Tag = "bold";
			this.Label27.Text = "Assessment";
			this.Label27.Top = 0.03125F;
			this.Label27.Width = 1.0625F;
			// 
			// txtTotalAssessment
			// 
			this.txtTotalAssessment.CanGrow = false;
			this.txtTotalAssessment.Height = 0.19F;
			this.txtTotalAssessment.Left = 8.75F;
			this.txtTotalAssessment.Name = "txtTotalAssessment";
			this.txtTotalAssessment.Style = "text-align: right";
			this.txtTotalAssessment.Tag = "textbox";
			this.txtTotalAssessment.Text = null;
			this.txtTotalAssessment.Top = 0.03125F;
			this.txtTotalAssessment.Width = 1.125F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.19F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 7.625F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-weight: bold; text-align: left";
			this.Label28.Tag = "bold";
			this.Label28.Text = "Tax";
			this.Label28.Top = 0.1875F;
			this.Label28.Width = 1.0625F;
			// 
			// txtTotalTax
			// 
			this.txtTotalTax.CanGrow = false;
			this.txtTotalTax.Height = 0.19F;
			this.txtTotalTax.Left = 8.75F;
			this.txtTotalTax.Name = "txtTotalTax";
			this.txtTotalTax.Style = "text-align: right";
			this.txtTotalTax.Tag = "textbox";
			this.txtTotalTax.Text = null;
			this.txtTotalTax.Top = 0.1875F;
			this.txtTotalTax.Width = 1.125F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.19F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 4.75F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-weight: bold; text-align: left";
			this.Label29.Tag = "bold";
			this.Label29.Text = "Exempt";
			this.Label29.Top = 0.03125F;
			this.Label29.Width = 1.0625F;
			// 
			// txtTotalExempt
			// 
			this.txtTotalExempt.CanGrow = false;
			this.txtTotalExempt.Height = 0.19F;
			this.txtTotalExempt.Left = 6.4375F;
			this.txtTotalExempt.Name = "txtTotalExempt";
			this.txtTotalExempt.Style = "text-align: right";
			this.txtTotalExempt.Tag = "textbox";
			this.txtTotalExempt.Text = null;
			this.txtTotalExempt.Top = 0.03125F;
			this.txtTotalExempt.Width = 1.125F;
			// 
			// lblPPTotal
			// 
			this.lblPPTotal.Height = 0.19F;
			this.lblPPTotal.HyperLink = null;
			this.lblPPTotal.Left = 4.75F;
			this.lblPPTotal.Name = "lblPPTotal";
			this.lblPPTotal.Style = "font-weight: bold; text-align: left";
			this.lblPPTotal.Tag = "bold";
			this.lblPPTotal.Text = "Personal Property";
			this.lblPPTotal.Top = 0.1875F;
			this.lblPPTotal.Width = 1.625F;
			// 
			// txtTotalPPTotal
			// 
			this.txtTotalPPTotal.CanGrow = false;
			this.txtTotalPPTotal.Height = 0.19F;
			this.txtTotalPPTotal.Left = 6.4375F;
			this.txtTotalPPTotal.Name = "txtTotalPPTotal";
			this.txtTotalPPTotal.Style = "text-align: right";
			this.txtTotalPPTotal.Tag = "textbox";
			this.txtTotalPPTotal.Text = null;
			this.txtTotalPPTotal.Top = 0.1875F;
			this.txtTotalPPTotal.Width = 1.125F;
			// 
			// rptREOutPrint
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidToDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExemptPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPPTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPPTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaidToDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat49;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalPP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPP;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExemptPP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptPP;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessPP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessPP;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPPTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPPTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
