﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormat1_4.
	/// </summary>
	public partial class rptBillFormat1_4 : FCSectionReport
	{
		public static rptBillFormat1_4 InstancePtr
		{
			get
			{
				return (rptBillFormat1_4)Sys.GetInstance(typeof(rptBillFormat1_4));
			}
		}

		protected rptBillFormat1_4 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptBillFormat1_4()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
			{
				_InstancePtr = this;
			}
		}
		
		bool boolShownModally;
		clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		bool boolFirstBill;
		/// <summary>
		/// true if this is not a mortgage holder copy
		/// </summary>
		clsBillFormat clsTaxBillFormat = new clsBillFormat();
		clsRateRecord clsTaxRate = new clsRateRecord();
		clsDRWrapper clsRateRecs = new clsDRWrapper();
		double TaxRate;
		int intStartPage;
		clsDRWrapper clsBills = new clsDRWrapper();
		clsDRWrapper clsMortgageHolders = new clsDRWrapper();
		clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
		bool boolRE;
		bool boolPP;
		double Tax1;
		double Tax2;
		double Tax3;
		double Tax4;
		double Prepaid;
		double DistPerc1;
		double DistPerc2;
		double DistPerc3;
		double DistPerc4;
		double DistPerc5;
		int intDistCats;
		double dblDiscountPercent;
		string strCat1 = "";
		string strCat2 = "";
		string strCat3 = "";
		bool boolNotJustUnloading;
		bool boolReprintNewOwnerCopy;
		/// <summary>
		/// this will be set to true if there is a new owner and the recordset will not be advanced to create a copy for him
		/// </summary>
		bool boolPrintRecipientCopy;
		clsDRWrapper rsMultiRecipients = new clsDRWrapper();
		bool boolIsRegional;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			modCustomPageSize.SIZEL myform = new modCustomPageSize.SIZEL(0);
			string strReturn = "";
			string PrinterName = "";
			int PrinterHandle;
			int intReturn = 0;
			
			modCustomPageSize.CheckDefaultPrinter(this.Document.Printer.PrinterName);
            //FC:TODO:AM
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("BillFormat1", 900, 550);
		}

		public void Init(clsBillFormat clsTaxFormat, int intYear, string strFontName, bool boolShow = true, string strPrinterN = "", bool boolModal = false)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			string strSql;
			boolIsRegional = modRegionalTown.IsRegionalTown();
			boolNotJustUnloading = true;
			boolShownModally = boolModal;
			if (strPrinterN != string.Empty)
			{
				this.Document.Printer.PrinterName = strPrinterN;
			}
			if (clsTaxFormat.BillsFrom == 0)
			{
				boolRE = true;
				if (clsTaxFormat.Combined)
				{
					boolPP = true;
				}
				else
				{
					boolPP = false;
				}
			}
			else
			{
				boolRE = false;
				boolPP = true;
			}
			if (boolPP && !boolRE)
			{
				clsTemp.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
				strCat1 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				txtAccountLabel.Text = strCat1;
				clsTemp.MoveNext();
				strCat2 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				lblLand.Text = strCat2;
				clsTemp.MoveNext();
				strCat3 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				txtBldgLabel.Text = strCat3;
			}
			clsTaxBillFormat.CopyFormat(ref clsTaxFormat);
			if (!boolRE)
			{
				txtBookPage.Visible = false;
				txtBookPageLabel.Visible = false;
				// must use the land,bldg,account,exemption labels for pp categories, then move the account to where the location is and the location to maplot
				txtAccount.Width = txtLand.Width;
				// using this for a category, not the account number
				txtLocLabel.Left = txtBookPageLabel.Left;
				// using this for account
				txtLocLabel.Text = "Account";
				txtMapLotLabel.Text = "Location";
				txtLocation.Width = txtBookPage.Width;
				txtLocation.Left = txtBookPage.Left;
				txtAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				txtExemptLabel.Text = "Other";
			}
			if (boolRE && clsTaxBillFormat.PrintCopyForMortgageHolders)
			{
				boolFirstBill = true;
				clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
				clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT", "CentralData");
				if (!clsMortgageAssociations.EndOfFile())
				{
					clsMortgageAssociations.MoveFirst();
					clsMortgageAssociations.MovePrevious();
				}
			}
			else
			{
				boolFirstBill = true;
			}
			strSql = clsTaxBillFormat.SQLStatement;
			clsTaxRate.TaxYear = intYear;
			lblTitle.Text = lblTitle.Text + " " + FCConvert.ToString(intYear);
			clsRateRecs.OpenRecordset("select * from raterec where year = " + FCConvert.ToString(intYear), "twcl0000.vb1");
			if (!clsRateRecs.EndOfFile())
			{
				clsTaxRate.LoadRate(clsRateRecs.Get_Fields_Int32("ID"));
			}
			if (clsTaxBillFormat.Discount)
			{
                //FC:TODO:AM
                //Frame1.Visible = true;
                //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                // for same Functionality
                {
                    txtDiscount.Visible = true;
                    txtDiscountAmount.Visible = true;
                    txtDiscountDate.Visible = true;
                    Field16.Visible = true;
                    Field17.Visible = true;
                    Field18.Visible = true;
                }
			}
			else
			{
                //FC:TODO:AM
                //Frame1.Visible = false;
                //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                // for same Functionality
                {
                    txtDiscount.Visible = false;
                    txtDiscountAmount.Visible = false;
                    txtDiscountDate.Visible = false;
                    Field16.Visible = false;
                    Field17.Visible = false;
                    Field18.Visible = false;
                }
			}
            //FC:TODO:AM
            //DistFrame.Visible = false;
            //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
            // for same Functionality
            {
                Field20.Visible = false;
                txtDist1.Visible = false;
                txtDist2.Visible = false;
                txtDist3.Visible = false;
                txtDist4.Visible = false;
                txtDist5.Visible = false;
                txtDistPerc1.Visible = false;
                txtDistPerc2.Visible = false;
                txtDistPerc3.Visible = false;
                txtDistPerc4.Visible = false;
                txtDistPerc5.Visible = false;
                txtDistAmount1.Visible = false;
                txtDistAmount2.Visible = false;
                txtDistAmount3.Visible = false;
                txtDistAmount4.Visible = false;
                txtDistAmount5.Visible = false;
            }
			DistPerc1 = 0;
			DistPerc2 = 0;
			DistPerc3 = 0;
			DistPerc4 = 0;
			DistPerc5 = 0;
			clsTemp.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
			while (!clsTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				switch (FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("line"))))
				{
					case 0:
						{
							txtMessage1.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 1:
						{
							txtMessage2.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 2:
						{
							txtMessage3.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 3:
						{
							txtMessage4.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 4:
						{
							txtMessage5.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 5:
						{
							txtMessage6.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 6:
						{
							txtmessage7.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 7:
						{
							txtMessage8.Text = clsTemp.Get_Fields_String("message");
							break;
						}
				}
				//end switch
				clsTemp.MoveNext();
			}
			if (clsTaxBillFormat.Breakdown)
			{
                //FC:TODO:AM
                //DistFrame.Visible = true;
                //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                // for same Functionality
                {
                    Field20.Visible = true;
                    txtDist1.Visible = true;
                    txtDistPerc1.Visible = true;
                }
				clsTemp.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					intDistCats = clsTemp.RecordCount();
					DistPerc1 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
					txtDistPerc1.Text = Strings.Format(DistPerc1, "#0.00") + "%";
					DistPerc1 /= 100;
					strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
					txtDist1.Text = Strings.Mid(strTemp, 1, 11);
					if (intDistCats > 1)
					{
						clsTemp.MoveNext();
						txtDistPerc2.Visible = true;
						txtDist2.Visible = true;
						DistPerc2 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
						txtDistPerc2.Text = Strings.Format(DistPerc2, "#0.00") + "%";
						DistPerc2 /= 100;
						strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
						txtDist2.Text = Strings.Mid(strTemp, 1, 11);
						if (intDistCats > 2)
						{
							clsTemp.MoveNext();
							txtDistPerc3.Visible = true;
							txtDist3.Visible = true;
							DistPerc3 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
							txtDistPerc3.Text = Strings.Format(DistPerc3, "#0.00") + "%";
							DistPerc3 /= 100;
							strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
							txtDist3.Text = Strings.Mid(strTemp, 1, 11);
							if (intDistCats > 3)
							{
								clsTemp.MoveNext();
								txtDistPerc4.Visible = true;
								txtDist4.Visible = true;
								DistPerc4 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
								txtDistPerc4.Text = Strings.Format(DistPerc4, "#0.00") + "%";
								DistPerc4 /= 100;
								strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
								txtDist4.Text = Strings.Mid(strTemp, 1, 11);
								if (intDistCats > 4)
								{
									clsTemp.MoveNext();
									txtDistPerc5.Visible = true;
									txtDist5.Visible = true;
									DistPerc5 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
									txtDistPerc5.Text = Strings.Format(DistPerc5, "#0.00") + "%";
									DistPerc5 /= 100;
									strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
									txtDist5.Text = Strings.Mid(strTemp, 1, 11);
								}
							}
						}
					}
				}
				else
				{
					intDistCats = 0;
				}
			}
			if (clsTaxBillFormat.BreakdownDollars)
			{
				txtDistAmount1.Visible = true;
				if (intDistCats > 1)
				{
					txtDistAmount2.Visible = true;
				}
				if (intDistCats > 2)
				{
					txtDistAmount3.Visible = true;
				}
				if (intDistCats > 3)
				{
					txtDistAmount4.Visible = true;
				}
				if (intDistCats > 4)
				{
					txtDistAmount5.Visible = true;
				}
			}
			else
			{
				txtDistAmount1.Visible = false;
				txtDistAmount2.Visible = false;
				txtDistAmount3.Visible = false;
				txtDistAmount4.Visible = false;
				txtDistAmount5.Visible = false;
			}
			if (clsTaxBillFormat.Discount)
			{
				clsTemp.OpenRecordset("select * from discount", "twbl0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
					dblDiscountPercent = Conversion.Val(clsTemp.Get_Fields("discount")) / 100;
					// TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
					txtDiscount.Text = Strings.Format(clsTemp.Get_Fields("discount"), "#0.00");
					// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
					txtDiscountDate.Text = clsTemp.Get_Fields("duedate");
				}
			}
			if (clsTaxRate.NumberOfPeriods > 2)
			{
                //FC:TODO:AM
                //Frame1.Visible = false;
                //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                // for same Functionality
                {
                    txtDiscount.Visible = false;
                    txtDiscountAmount.Visible = false;
                    txtDiscountDate.Visible = false;
                    Field16.Visible = false;
                    Field17.Visible = false;
                    Field18.Visible = false;
                }
                //FC:TODO:AM
                //Frame2.Visible = true;
                //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                // for same Functionality
                {
                    Field19.Visible = true;
                    txtPayment4Label.Visible = true;
                    txtDueDate3.Visible = true;
                    txtAmount3.Visible = true;
                }
                txtDueDate2.Visible = true;
				txtAmount2.Visible = true;
				txtPayment2Label.Visible = true;
				txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToString();
				txtDueDate3.Text = clsTaxRate.Get_DueDate(3).ToString();
				if (clsTaxRate.NumberOfPeriods > 3)
				{
					txtDueDate4.Text = clsTaxRate.Get_DueDate(4).ToString();
					txtAmount4.Visible = true;
					txtDueDate4.Visible = true;
				}
			}
			else
			{
				if (clsTaxRate.NumberOfPeriods < 2)
				{
					txtPayment2Label.Visible = false;
					txtAmount2.Visible = false;
					txtDueDate2.Visible = false;
				}
				else
				{
					txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToString();
					txtAmount2.Visible = true;
					txtDueDate2.Visible = true;
					txtPayment2Label.Visible = true;
				}
                //FC:TODO:AM
                //Frame2.Visible = false;
                //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                // for same Functionality
                {
                    Field19.Visible = false;
                    txtPayment4Label.Visible = false;
                    txtDueDate3.Visible = false;
                    txtAmount3.Visible = false;
                }
            }
			txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToString();
			//this.Document.Printer.RenderMode = 1;
			if (strFontName != string.Empty)
			{
				//this.Document.Printer.PrintQuality = ddPQDraft;
				// now set each box and label to the correct printer font
				for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
				{

					bool setFont = false;
					bool bold = false;
					if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
					{
						setFont = true;
					}
					else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
					{
						setFont = true;
						bold = true;
					}
					if (setFont)
					{
						GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
						if (textBox != null)
						{
							textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
						}
						else
						{
							GrapeCity.ActiveReports.SectionReportModel.Label label = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
							if (label != null)
							{
								label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
						}
					}
				}
				// x
			}
			GetBills(ref strSql);
			if (boolShow)
			{
				// Me.Show , MDIParent
			}
			else
			{
				this.PrintReport(false);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsBills.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;

			if (clsTaxBillFormat.HasDefaultMargin)
			{
				this.PageSettings.Margins.Left = 0;
			}
			if (!boolIsRegional)
			{
				string[] strAdd = new string[4 + 1];
				modBLCustomBill.GetReturnAddressForBills(0, ref strAdd[0], ref strAdd[1], ref strAdd[2], ref strAdd[3]);
				txtAddress1.Text = strAdd[0];
				txtAddress2.Text = strAdd[1];
				txtAddress3.Text = strAdd[2];
				txtAddress4.Text = strAdd[3];
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			modCustomPageSize.Statics.boolChangedDefault = true;
			modCustomPageSize.ResetDefaultPrinter();
			if (!boolShownModally && boolNotJustUnloading)
			{
				//MDIParent.InstancePtr.Show();
			}
		}
	
		private void Detail_Format(object sender, EventArgs e)
		{
			double Dist1 = 0;
			double Dist2 = 0;
			double Dist3 = 0;
			double Dist4 = 0;
			double Dist5 = 0;
			double TotTax = 0;
			string strTemp = "";
			DateTime dtLastDate;
			string strOldName = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			string strSecOwner = "";
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string, double, short)
			double dblTemp = 0;
			string strLocation = "";
			string[] strReturn = new string[4 + 1];
			if (!clsBills.EndOfFile())
			{
				if (clsTaxBillFormat.ReturnAddress)
				{
					if (boolIsRegional)
					{
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						modBLCustomBill.GetReturnAddressForBills(FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields("trancode"))), ref strReturn[0], ref strReturn[1], ref strReturn[2], ref strReturn[3]);
						txtAddress1.Text = strReturn[0];
						txtAddress2.Text = strReturn[1];
						txtAddress3.Text = strReturn[2];
						txtAddress4.Text = strReturn[3];
					}
				}
				if (FCConvert.ToString(clsBills.Get_Fields_String("Billingtype")) == "RE")
				{
					boolRE = true;
					boolPP = false;
					lblTitle.Text = "R E A L  E S T A T E  T A X  B I L L";
					txtAccountLabel.Text = "Account";
					lblLand.Text = "Land";
					txtBldgLabel.Text = "Buildings";
					txtBookPage.Visible = true;
					txtBookPageLabel.Visible = true;
					txtLocLabel.Text = "Location";
					txtMapLotLabel.Text = "Map/Lot";
					txtAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
					txtExemptLabel.Text = "Exemption";
					//FC:FINAL:CHN: Incorrect using sizes (Missing convert).
					// txtAccount.Width = 990;
					// txtLocLabel.Left = 2610;
					// txtLocation.Width = 2970;
					// txtLocation.Left = 3960;
					txtAccount.Width = 990 / 1440f;
					txtLocLabel.Left = 2610 / 1440f;
					txtLocation.Width = 2970 / 1440f;
					txtLocation.Left = 3960 / 1440f;
				}
				else if (clsBills.Get_Fields_String("Billingtype") == "PP")
				{
					boolRE = false;
					boolPP = true;
					lblTitle.Text = "P E R S O N A L  P R O P E R T Y  T A X  B I L L";
					txtAccountLabel.Text = strCat1;
					lblLand.Text = strCat2;
					txtBldgLabel.Text = strCat3;
					txtBookPage.Visible = false;
					txtBookPageLabel.Visible = false;
					txtLocLabel.Text = "Account";
					txtMapLotLabel.Text = "Location";
					txtAccount.Width = txtLand.Width;
					// using this for a category, not the account number
					txtLocLabel.Left = txtBookPageLabel.Left;
					// using this for account
					txtLocation.Width = txtBookPage.Width;
					txtLocation.Left = txtBookPage.Left;
				}
				else
				{
					// combined
					boolRE = true;
					boolPP = true;
					lblTitle.Text = "C O M B I N E D  T A X  B I L L";
					txtAccountLabel.Text = "Account";
					lblLand.Text = "Land";
					txtBldgLabel.Text = "Buildings";
					txtBookPage.Visible = true;
					txtBookPageLabel.Visible = true;
					txtLocLabel.Text = "Location";
					txtMapLotLabel.Text = "Map/Lot";
					txtAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
					txtExemptLabel.Text = "Exemption";

					txtAccount.Width = 990 / 1440f;
					txtLocLabel.Left = 2610 / 1440f;
					txtLocation.Width = 2970 / 1440f;
					txtLocation.Left = 3960 / 1440f;
				}
				if (clsRateRecs.FindFirstRecord("ID", clsBills.Get_Fields_Int32("ratekey")))
				{
					clsTaxRate.LoadRate(clsBills.Get_Fields_Int32("ratekey"), clsRateRecs);
					if (clsTaxRate.NumberOfPeriods > 2)
					{
                        //FC:TODO:AM
                        //Frame1.Visible = false;
                        //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                        // for same Functionality
                        {
                            txtDiscount.Visible = false;
                            txtDiscountAmount.Visible = false;
                            txtDiscountDate.Visible = false;
                            Field16.Visible = false;
                            Field17.Visible = false;
                            Field18.Visible = false;
                        }
                        //FC:TODO:AM
                        //Frame2.Visible = true;
                        //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                        // for same Functionality
                        {
                            Field19.Visible = true;
                            txtPayment4Label.Visible = true;
                            txtDueDate3.Visible = true;
                            txtAmount3.Visible = true;
                        }
                        txtDueDate2.Visible = true;
						txtAmount2.Visible = true;
						txtPayment2Label.Visible = true;
						txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToString();
						txtDueDate3.Text = clsTaxRate.Get_DueDate(3).ToString();
						if (clsTaxRate.NumberOfPeriods > 3)
						{
							txtDueDate4.Text = clsTaxRate.Get_DueDate(4).ToString();
							txtAmount4.Visible = true;
							txtDueDate4.Visible = true;
						}
					}
					else
					{
						if (clsTaxRate.NumberOfPeriods < 2)
						{
							txtPayment2Label.Visible = false;
							txtAmount2.Visible = false;
							txtDueDate2.Visible = false;
						}
						else
						{
							txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToString();
							txtAmount2.Visible = true;
							txtDueDate2.Visible = true;
							txtPayment2Label.Visible = true;
						}
                        //FC:TODO:AM
                        //Frame2.Visible = false;
                        //FC:FINAL:MSH - frame doesn't exist in .NET version of GrapeCity, so change visibility of fields, which are located on frame in original app
                        // for same Functionality
                        {
                            Field19.Visible = false;
                            txtPayment4Label.Visible = false;
                            txtDueDate3.Visible = false;
                            txtAmount3.Visible = false;
                        }
                    }
					txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToString();
				}
				Tax1 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue1"));
				Tax2 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue2"));
				Tax3 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue3"));
				Tax4 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue4"));
				TotTax = Tax1 + Tax2 + Tax3 + Tax4;
				txtTaxDue.Text = Strings.Format(TotTax, "#,###,###,##0.00");
				if (clsTaxBillFormat.BreakdownDollars)
				{
					Dist1 = TotTax * DistPerc1;
					Dist2 = TotTax * DistPerc2;
					Dist3 = TotTax * DistPerc3;
					Dist4 = TotTax * DistPerc4;
					Dist5 = TotTax * DistPerc5;
					txtDistAmount1.Text = Strings.Format(Dist1, "##,###,##0.00");
					if (intDistCats > 1)
					{
						txtDistAmount2.Text = Strings.Format(Dist2, "##,###,##0.00");
					}
					if (intDistCats > 2)
					{
						txtDistAmount3.Text = Strings.Format(Dist3, "##,###,##0.00");
					}
					if (intDistCats > 3)
					{
						txtDistAmount4.Text = Strings.Format(Dist4, "##,###,##0.00");
					}
					if (intDistCats > 4)
					{
						txtDistAmount5.Text = Strings.Format(Dist5, "##,###,##0.00");
					}
				}
				Prepaid = Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid"));
				double dblAbated = 0;
				double dblAbate1 = 0;
				double dblAbate2 = 0;
				double dblAbate3 = 0;
				double dblAbate4 = 0;
				int intPer;
				double[] dblTAbate = new double[4 + 1];
				dblAbated = 0;
				if ((Tax2 > 0 || Tax3 > 0 || Tax4 > 0) && Prepaid > 0)
				{
					dblAbated = modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields_Int32("ID"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
					dblAbate1 = dblTAbate[1];
					dblAbate2 = dblTAbate[2];
					dblAbate3 = dblTAbate[3];
					dblAbate4 = dblTAbate[4];
					if (boolRE && boolPP)
					{
						// TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
						dblAbated += modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields("ppbillkey"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
						dblAbate1 += dblTAbate[1];
						dblAbate2 += dblTAbate[2];
						dblAbate3 += dblTAbate[3];
						dblAbate4 += dblTAbate[4];
					}
					Prepaid -= dblAbated;
				}
				if (dblAbated > 0)
				{
					Tax1 -= dblAbate1;
					Tax2 -= dblAbate2;
					Tax3 -= dblAbate3;
					Tax4 -= dblAbate4;
				}
				if (Prepaid > 0)
				{
					if (Prepaid > Tax1)
					{
						Prepaid -= Tax1;
						Tax1 = 0;
						if (Prepaid > Tax2)
						{
							Prepaid -= Tax2;
							Tax2 = 0;
							if (Prepaid > Tax3)
							{
								Prepaid -= Tax3;
								Tax3 = 0;
								if (Prepaid > Tax4)
								{
									Prepaid -= Tax4;
									Tax4 = 0;
								}
								else
								{
									Tax4 -= Prepaid;
								}
							}
							else
							{
								Tax3 -= Prepaid;
							}
						}
						else
						{
							Tax2 -= Prepaid;
						}
					}
					else
					{
						Tax1 -= Prepaid;
					}
				}
				Prepaid = Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid"));
				txtPrepaid.Text = Strings.Format(Prepaid, "#,###,##0.00");
				if (Prepaid == 0)
				{
					txtPrepaid.Visible = false;
					txtPrepaidLabel.Visible = false;
				}
				else
				{
					txtPrepaid.Visible = true;
					txtPrepaidLabel.Visible = true;

				}
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = clsBills.Get_Fields_String("account");
				txtRate.Text = Strings.Format(clsTaxRate.TaxRate * 1000, "#0.000");
				txtAmount1.Text = Strings.Format(Tax1, "#,###,##0.00");
				txtAmount2.Text = Strings.Format(Tax2, "#,###,##0.00");
				txtAmount3.Text = Strings.Format(Tax3, "#,###,##0.00");
				txtAmount4.Text = Strings.Format(Tax4, "#,###,##0.00");
				// If clsTaxBillFormat.Discount Then
				// txtDiscountAmount.Text = Format((Tax1 + Tax2 + Tax3 + Tax4) - (dblDiscountPercent * (Tax1 + Tax2 + Tax3 + Tax4)), "#,###,##0.00")
				// End If
				if (clsTaxBillFormat.Discount)
				{
					double dblDisc = 0;
					dblDisc = modMain.Round(TotTax * dblDiscountPercent, 2);
					txtDiscountAmount.Text = Strings.Format(TotTax - FCConvert.ToDouble(Strings.Format(dblDisc, "0.00")), "#,###,##0.00");
					dblTemp = FCConvert.ToDouble(Strings.Format(TotTax - FCConvert.ToDouble(Strings.Format(dblDisc, "0.00")), "0.00"));
					if (Prepaid > TotTax)
					{
						txtDiscountAmount.Text = "0.00";
					}
					else
					{
						if (Prepaid > 0)
						{
							dblTemp -= Prepaid;
							if (dblTemp < 0)
								dblTemp = 0;
							txtDiscountAmount.Text = Strings.Format(dblTemp, "#,###,##0.00");
						}
						else
						{
							txtDiscountAmount.Text = Strings.Format(dblTemp, "#,###,##0.00");
						}
					}
				}
				if (boolRE)
				{
					if (Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage"))) != string.Empty)
					{
						txtBookPage.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage")));
						txtBookPage.Visible = true;
						txtBookPageLabel.Visible = true;
					}
					else
					{
						txtBookPage.Visible = false;
						txtBookPageLabel.Visible = false;
					}
			
					txtLand.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")), "#,###,###,##0");
					txtBldg.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")), "#,###,###,##0");
					txtAssessment.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")) + Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")), "#,###,###,##0");
					if (boolPrintRecipientCopy)
					{
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						txtMailing2.Text = "C/O " + rsMultiRecipients.Get_Fields_String("Name");
						txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address1");
						txtMailing4.Text = rsMultiRecipients.Get_Fields_String("address2");
						txtMailing5.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
						// ElseIf clsTaxBillFormat.UsePreviousOwnerInfo And NewOwner(clsBills.Fields("Account"), clsTaxBillFormat.BillAsOfDate, strOldName, strAddr1, strAddr2, strAddr3) Then
					}
					else if (!boolFirstBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
					{
						clsMortgageHolders.FindFirstRecord("ID", clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"));
						if (!clsMortgageHolders.NoMatch)
						{
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing2.Text = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
							txtMailing5.Text = Strings.Trim(clsMortgageHolders.Get_Fields_String("city") + " " + clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
						}
						else
						{
							txtMailing1.Text = "Mortgage Holder Not Found";
							txtMailing2.Text = "";
							txtMailing3.Text = "";
							txtMailing4.Text = "";
							txtMailing5.Text = "";
						}
					}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (boolReprintNewOwnerCopy && modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3))
					{

						txtMailing3.Text = strAddr1;
						txtMailing4.Text = strAddr2;
						txtMailing5.Text = strAddr3;
						txtMailing1.Text = strOldName;
						txtMailing2.Text = strSecOwner;
						// End If
					}
					else
					{
						txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
						txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
						txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));

						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						// strName = Trim(clsBills.Fields("name1"))
						txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name2")));
					}
					if (txtMailing4.Text == "")
					{
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing3.Text == "")
					{
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing2.Text == "")
					{
						txtMailing2.Text = txtMailing3.Text;
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						strLocation = FCConvert.ToString(clsBills.Get_Fields("streetnumber"));
					}
					else
					{
						strLocation = "";
					}
					strLocation = Strings.Trim(Strings.Trim(strLocation + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
					txtLocation.Text = strLocation;
					txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("maplot")));
					txtExemption.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue")), "#,###,###,##0");
				}
				else
				{
					// pp
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					//FC:FINAL:CHN: Incorrect Converting.
					// txtLocation.Text = Conversion.Val(clsBills.Get_Fields("account")));
					txtLocation.Text = FCConvert.ToString(Conversion.Val(clsBills.Get_Fields("account")));
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					//FC:FINAL:CHN: Missing using property.
					// txtMapLot = Strings.Trim(clsBills.Get_Fields("streetnumber") + " " + clsBills.Get_Fields_String("apt") + " " + clsBills.Get_Fields_String("streetname"));
					txtMapLot.Text = Strings.Trim(clsBills.Get_Fields("streetnumber") + " " + clsBills.Get_Fields_String("apt") + " " + clsBills.Get_Fields_String("streetname"));
					// used for pp location
					txtAccount.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category1")), "#,###,###,##0");
					txtLand.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category2")), "#,###,###,##0");
					txtBldg.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category3")), "#,###,###,##0");
					txtExemption.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category4")) + Conversion.Val(clsBills.Get_Fields_Int32("category5")) + Conversion.Val(clsBills.Get_Fields_Int32("category6")) + Conversion.Val(clsBills.Get_Fields_Int32("category7")) + Conversion.Val(clsBills.Get_Fields_Int32("category8")) + Conversion.Val(clsBills.Get_Fields_Int32("category9")), "#,###,###,##0");
					txtAssessment.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("ppassessment")), "#,###,###,##0");
					if (boolPrintRecipientCopy)
					{
						txtMailing1.Text = rsMultiRecipients.Get_Fields_String("Name");
						txtMailing2.Text = rsMultiRecipients.Get_Fields_String("address1");
						txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address2");
						txtMailing4.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
						txtMailing5.Text = "";
					}
					else
					{
						txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
						txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
						txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));

						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						txtMailing5.Text = "";
					}
					if (txtMailing3.Text == "")
					{
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = "";
					}
					if (txtMailing2.Text == "")
					{
						txtMailing2.Text = txtMailing3.Text;
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = "";
					}
				}
				if (boolRE)
				{
					if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
					{
						boolReprintNewOwnerCopy = false;
						// if the copy for a new owner needs to be sent, then check it here
						if (clsTaxBillFormat.PrintCopyForNewOwner)
						{
							// if this is the main bill, only check for the new owner on the mail bill
							if (boolFirstBill)
							{
								if (DateTime.Today.Month < 4)
								{
									// if the month is before april then use last years commitment date
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1));
								}
								else
								{
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));
								}
								// 
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1")))
								{
									// if there has been a new owner
									// If NewOwner(clsBills.Fields("Account"), dtLastDate) Then     'if there has been a new owner
									boolReprintNewOwnerCopy = true;
									// End If
								}
							}
						}
					}
					else if (!boolPrintRecipientCopy && boolFirstBill)
					{
						// turn off the copy so that the record can advance
						boolReprintNewOwnerCopy = false;
					}
					if (!boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							if (boolFirstBill)
							{
								rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
								if (!rsMultiRecipients.EndOfFile())
								{
									boolPrintRecipientCopy = true;
								}
							}
						}
					}
					else if (boolPrintRecipientCopy && boolFirstBill)
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (clsTaxBillFormat.PrintCopyForMortgageHolders && !boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (!clsMortgageAssociations.EndOfFile())
						{
							if (boolFirstBill)
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindFirstRecord("account", clsBills.Get_Fields("account"));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindNextRecord("account", clsBills.Get_Fields("account"));
							}
							// Call clsMortgageAssociations.FindNextRecord("account", clsBills.Fields("ac"))
							if (clsMortgageAssociations.NoMatch)
							{
								boolFirstBill = true;
								if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
									clsBills.MoveNext();
							}
							else
							{
								boolFirstBill = false;
							}
							// End If
						}
						else
						{
							boolFirstBill = true;
							if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
								clsBills.MoveNext();
						}
					}
					else
					{
						if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
							clsBills.MoveNext();
					}
				}
				else
				{
					if (!boolPrintRecipientCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
							if (!rsMultiRecipients.EndOfFile())
							{
								boolPrintRecipientCopy = true;
							}
						}
					}
					else
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (!boolPrintRecipientCopy)
					{
						clsBills.MoveNext();
					}
				}
			}
		}

		private void GetBills(ref string strSql)
		{
			clsBills.OpenRecordset(strSql, "twcl0000.vb1");
		}

		private void rptBillFormat1_4_Load(object sender, System.EventArgs e)
		{

		}
	}
}
