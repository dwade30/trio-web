﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPCollectionWide.
	/// </summary>
	partial class rptPPCollectionWide
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPPCollectionWide));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCategory1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCategory2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCategory3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCategoryOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1Val = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2Val = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3Val = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCatOtherVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDiscount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPageAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPageExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPageTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategoryOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1Val)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2Val)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3Val)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOtherVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtCategory1,
				this.txtAssessment,
				this.txtExempt,
				this.txtTotal,
				this.txtTax,
				this.txtAddress1,
				this.txtAddress2,
				this.txtMailingAddress3,
				this.txtAddress3,
				this.txtCategory2,
				this.txtCategory3,
				this.txtCategoryOther,
				this.txtCat1Val,
				this.txtCat2Val,
				this.txtCat3Val,
				this.txtCatOtherVal,
				this.lblDiscount,
				this.txtDiscount,
				this.Line1,
				this.txtLocation
			});
			this.Detail.Height = 1.0625F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.Visible = false;
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label20,
				this.txtTotAssess,
				this.txtTotExempt,
				this.txtTotTotal,
				this.txtTotTax
			});
			this.ReportFooter.Height = 0.46875F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.txtDate,
				this.lblMuniname,
				this.lblTitle,
				this.Label9,
				this.Field11,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.txtTime
			});
			this.PageHeader.Height = 0.4791667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.CanGrow = false;
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label19,
				this.txtPageAssess,
				this.txtPageExempt,
				this.txtPageTotal,
				this.txtPageTax
			});
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			this.PageFooter.Visible = false;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 8.5625F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-size: 10pt; text-align: right";
			this.txtPage.Tag = "textbox";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 8.875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-size: 10pt; text-align: right";
			this.txtDate.Tag = "textbox";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.19F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "font-size: 10pt; text-align: left";
			this.lblMuniname.Tag = "textbox";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.4375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
            this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.625F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Tag = "bold";
			this.lblTitle.Text = "Personal Property Tax Commitment Book - ";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 4.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Account";
			this.Label9.Top = 0.3125F;
			this.Label9.Width = 0.625F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 0.625F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-weight: bold";
			this.Field11.Tag = "bold";
			this.Field11.Text = "Name & Address";
			this.Field11.Top = 0.3125F;
			this.Field11.Width = 1.5625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: left";
			this.Label10.Tag = "bold";
			this.Label10.Text = "Category Breakdown";
			this.Label10.Top = 0.3125F;
			this.Label10.Width = 1.4375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Tag = "bold";
			this.Label11.Text = "Assessment";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 0.875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 6.625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: right";
			this.Label12.Tag = "bold";
			this.Label12.Text = "Exempt";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 0.875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 7.8125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Tag = "bold";
			this.Label13.Text = "Total";
			this.Label13.Top = 0.3125F;
			this.Label13.Width = 0.9375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 9.1875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Tag = "bold";
			this.Label14.Text = "Tax";
			this.Label14.Top = 0.3125F;
			this.Label14.Width = 0.8125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-size: 10pt; text-align: left";
			this.txtTime.Tag = "textbox";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.375F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "white-space: nowrap";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = " ";
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.5625F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.625F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Tag = "textbox";
			this.txtName.Text = " ";
			this.txtName.Top = 0F;
			this.txtName.Width = 2.375F;
			// 
			// txtCategory1
			// 
			this.txtCategory1.CanGrow = false;
			this.txtCategory1.Height = 0.19F;
			this.txtCategory1.Left = 3.0625F;
			this.txtCategory1.MultiLine = false;
			this.txtCategory1.Name = "txtCategory1";
			this.txtCategory1.Tag = "textbox";
			this.txtCategory1.Text = " ";
			this.txtCategory1.Top = 0.15625F;
			this.txtCategory1.Width = 2F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 5.125F;
			this.txtAssessment.MultiLine = false;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.OutputFormat = resources.GetString("txtAssessment.OutputFormat");
			this.txtAssessment.Style = "text-align: right; white-space: nowrap";
			this.txtAssessment.Tag = "textbox";
			this.txtAssessment.Text = " ";
			this.txtAssessment.Top = 0F;
			this.txtAssessment.Width = 1.125F;
			// 
			// txtExempt
			// 
			this.txtExempt.Height = 0.19F;
			this.txtExempt.Left = 6.3125F;
			this.txtExempt.MultiLine = false;
			this.txtExempt.Name = "txtExempt";
			this.txtExempt.OutputFormat = resources.GetString("txtExempt.OutputFormat");
			this.txtExempt.Style = "text-align: right; white-space: nowrap";
			this.txtExempt.Tag = "textbox";
			this.txtExempt.Text = " ";
			this.txtExempt.Top = 0F;
			this.txtExempt.Width = 1.1875F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.19F;
			this.txtTotal.Left = 7.5625F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat");
			this.txtTotal.Style = "text-align: right; white-space: nowrap";
			this.txtTotal.Tag = "textbox";
			this.txtTotal.Text = " ";
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 1.1875F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 8.8125F;
			this.txtTax.MultiLine = false;
			this.txtTax.Name = "txtTax";
			this.txtTax.OutputFormat = resources.GetString("txtTax.OutputFormat");
			this.txtTax.Style = "text-align: right; white-space: nowrap";
			this.txtTax.Tag = "textbox";
			this.txtTax.Text = " ";
			this.txtTax.Top = 0F;
			this.txtTax.Width = 1.1875F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.CanGrow = false;
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 0.625F;
			this.txtAddress1.MultiLine = false;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = " ";
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 2.375F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.CanGrow = false;
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 0.625F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = " ";
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 2.375F;
			// 
			// txtMailingAddress3
			// 
			this.txtMailingAddress3.CanGrow = false;
			this.txtMailingAddress3.Height = 0.19F;
			this.txtMailingAddress3.Left = 0.625F;
			this.txtMailingAddress3.MultiLine = false;
			this.txtMailingAddress3.Name = "txtMailingAddress3";
			this.txtMailingAddress3.Tag = "textbox";
			this.txtMailingAddress3.Text = " ";
			this.txtMailingAddress3.Top = 0.46875F;
			this.txtMailingAddress3.Width = 2.375F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.CanGrow = false;
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.Left = 0.625F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = " ";
			this.txtAddress3.Top = 0.625F;
			this.txtAddress3.Width = 2.375F;
			// 
			// txtCategory2
			// 
			this.txtCategory2.CanGrow = false;
			this.txtCategory2.Height = 0.19F;
			this.txtCategory2.Left = 3.0625F;
			this.txtCategory2.MultiLine = false;
			this.txtCategory2.Name = "txtCategory2";
			this.txtCategory2.Tag = "textbox";
			this.txtCategory2.Text = null;
			this.txtCategory2.Top = 0.3125F;
			this.txtCategory2.Width = 2F;
			// 
			// txtCategory3
			// 
			this.txtCategory3.CanGrow = false;
			this.txtCategory3.Height = 0.19F;
			this.txtCategory3.Left = 3.0625F;
			this.txtCategory3.MultiLine = false;
			this.txtCategory3.Name = "txtCategory3";
			this.txtCategory3.Tag = "textbox";
			this.txtCategory3.Text = null;
			this.txtCategory3.Top = 0.46875F;
			this.txtCategory3.Width = 2F;
			// 
			// txtCategoryOther
			// 
			this.txtCategoryOther.CanGrow = false;
			this.txtCategoryOther.Height = 0.19F;
			this.txtCategoryOther.Left = 3.0625F;
			this.txtCategoryOther.MultiLine = false;
			this.txtCategoryOther.Name = "txtCategoryOther";
			this.txtCategoryOther.Tag = "textbox";
			this.txtCategoryOther.Text = null;
			this.txtCategoryOther.Top = 0.625F;
			this.txtCategoryOther.Width = 2F;
			// 
			// txtCat1Val
			// 
			this.txtCat1Val.CanGrow = false;
			this.txtCat1Val.DataField = "Cat1Val";
			this.txtCat1Val.Height = 0.19F;
			this.txtCat1Val.Left = 5.125F;
			this.txtCat1Val.MultiLine = false;
			this.txtCat1Val.Name = "txtCat1Val";
			this.txtCat1Val.OutputFormat = resources.GetString("txtCat1Val.OutputFormat");
			this.txtCat1Val.Style = "text-align: right";
			this.txtCat1Val.Tag = "textbox";
			this.txtCat1Val.Text = null;
			this.txtCat1Val.Top = 0.15625F;
			this.txtCat1Val.Width = 1.125F;
			// 
			// txtCat2Val
			// 
			this.txtCat2Val.CanGrow = false;
			this.txtCat2Val.DataField = "Cat2Val";
			this.txtCat2Val.Height = 0.19F;
			this.txtCat2Val.Left = 5.125F;
			this.txtCat2Val.MultiLine = false;
			this.txtCat2Val.Name = "txtCat2Val";
			this.txtCat2Val.OutputFormat = resources.GetString("txtCat2Val.OutputFormat");
			this.txtCat2Val.Style = "text-align: right";
			this.txtCat2Val.Tag = "textbox";
			this.txtCat2Val.Text = null;
			this.txtCat2Val.Top = 0.3125F;
			this.txtCat2Val.Width = 1.125F;
			// 
			// txtCat3Val
			// 
			this.txtCat3Val.CanGrow = false;
			this.txtCat3Val.DataField = "Cat3Val";
			this.txtCat3Val.Height = 0.19F;
			this.txtCat3Val.Left = 5.125F;
			this.txtCat3Val.MultiLine = false;
			this.txtCat3Val.Name = "txtCat3Val";
			this.txtCat3Val.OutputFormat = resources.GetString("txtCat3Val.OutputFormat");
			this.txtCat3Val.Style = "text-align: right";
			this.txtCat3Val.Tag = "textbox";
			this.txtCat3Val.Text = null;
			this.txtCat3Val.Top = 0.46875F;
			this.txtCat3Val.Width = 1.125F;
			// 
			// txtCatOtherVal
			// 
			this.txtCatOtherVal.CanGrow = false;
			this.txtCatOtherVal.DataField = "CatOtherVal";
			this.txtCatOtherVal.Height = 0.19F;
			this.txtCatOtherVal.Left = 5.125F;
			this.txtCatOtherVal.MultiLine = false;
			this.txtCatOtherVal.Name = "txtCatOtherVal";
			this.txtCatOtherVal.OutputFormat = resources.GetString("txtCatOtherVal.OutputFormat");
			this.txtCatOtherVal.Style = "text-align: right";
			this.txtCatOtherVal.Tag = "textbox";
			this.txtCatOtherVal.Text = null;
			this.txtCatOtherVal.Top = 0.625F;
			this.txtCatOtherVal.Width = 1.125F;
			// 
			// lblDiscount
			// 
			this.lblDiscount.Height = 0.19F;
			this.lblDiscount.HyperLink = null;
			this.lblDiscount.Left = 8.8125F;
			this.lblDiscount.Name = "lblDiscount";
			this.lblDiscount.Style = "font-weight: bold; text-align: right";
			this.lblDiscount.Tag = "bold";
			this.lblDiscount.Text = "Discount";
			this.lblDiscount.Top = 0.1875F;
			this.lblDiscount.Width = 1.1875F;
			// 
			// txtDiscount
			// 
			this.txtDiscount.Height = 0.19F;
			this.txtDiscount.Left = 8.8125F;
			this.txtDiscount.Name = "txtDiscount";
			this.txtDiscount.Style = "text-align: right";
			this.txtDiscount.Tag = "textbox";
			this.txtDiscount.Text = null;
			this.txtDiscount.Top = 0.34375F;
			this.txtDiscount.Width = 1.1875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 9.9375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 9.9375F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 0.625F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = " ";
			this.txtLocation.Top = 0.7569444F;
			this.txtLocation.Width = 2.375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold";
			this.Label19.Tag = "bold";
			this.Label19.Text = "Page Totals:";
			this.Label19.Top = 0.15625F;
			this.Label19.Width = 1.125F;
			// 
			// txtPageAssess
			// 
			this.txtPageAssess.DataField = "AssessVal";
			this.txtPageAssess.Height = 0.19F;
			this.txtPageAssess.Left = 3.1875F;
			this.txtPageAssess.MultiLine = false;
			this.txtPageAssess.Name = "txtPageAssess";
			this.txtPageAssess.OutputFormat = resources.GetString("txtPageAssess.OutputFormat");
			this.txtPageAssess.Style = "text-align: right; white-space: nowrap";
			this.txtPageAssess.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtPageAssess.Tag = "textbox";
			this.txtPageAssess.Text = null;
			this.txtPageAssess.Top = 0.15625F;
			this.txtPageAssess.Width = 1.6875F;
			// 
			// txtPageExempt
			// 
			this.txtPageExempt.DataField = "ExemptVal";
			this.txtPageExempt.Height = 0.19F;
			this.txtPageExempt.Left = 4.9375F;
			this.txtPageExempt.MultiLine = false;
			this.txtPageExempt.Name = "txtPageExempt";
			this.txtPageExempt.OutputFormat = resources.GetString("txtPageExempt.OutputFormat");
			this.txtPageExempt.Style = "text-align: right; white-space: nowrap";
			this.txtPageExempt.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtPageExempt.Tag = "textbox";
			this.txtPageExempt.Text = null;
			this.txtPageExempt.Top = 0.15625F;
			this.txtPageExempt.Width = 1.625F;
			// 
			// txtPageTotal
			// 
			this.txtPageTotal.DataField = "TotVal";
			this.txtPageTotal.Height = 0.19F;
			this.txtPageTotal.Left = 6.625F;
			this.txtPageTotal.MultiLine = false;
			this.txtPageTotal.Name = "txtPageTotal";
			this.txtPageTotal.OutputFormat = resources.GetString("txtPageTotal.OutputFormat");
			this.txtPageTotal.Style = "text-align: right; white-space: nowrap";
			this.txtPageTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtPageTotal.Tag = "textbox";
			this.txtPageTotal.Text = null;
			this.txtPageTotal.Top = 0.15625F;
			this.txtPageTotal.Width = 1.8125F;
			// 
			// txtPageTax
			// 
			this.txtPageTax.DataField = "TaxVal";
			this.txtPageTax.Height = 0.19F;
			this.txtPageTax.Left = 8.5F;
			this.txtPageTax.MultiLine = false;
			this.txtPageTax.Name = "txtPageTax";
			this.txtPageTax.OutputFormat = resources.GetString("txtPageTax.OutputFormat");
			this.txtPageTax.Style = "text-align: right; white-space: nowrap";
			this.txtPageTax.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtPageTax.Tag = "textbox";
			this.txtPageTax.Text = null;
			this.txtPageTax.Top = 0.15625F;
			this.txtPageTax.Width = 1.4375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 8.5F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Tag = "bold";
			this.Label15.Text = "Tax";
			this.Label15.Top = 0.15625F;
			this.Label15.Width = 1.4375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 6.625F;
			this.Label16.MultiLine = false;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold; text-align: right";
			this.Label16.Tag = "bold";
			this.Label16.Text = "Total";
			this.Label16.Top = 0.15625F;
			this.Label16.Width = 1.8125F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.9375F;
			this.Label17.MultiLine = false;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold; text-align: right";
			this.Label17.Tag = "bold";
			this.Label17.Text = "Exempt";
			this.Label17.Top = 0.15625F;
			this.Label17.Width = 1.625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.1875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold; text-align: right";
			this.Label18.Tag = "bold";
			this.Label18.Text = "Assessment";
			this.Label18.Top = 0.15625F;
			this.Label18.Width = 1.6875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold";
			this.Label20.Tag = "bold";
			this.Label20.Text = "Totals:";
			this.Label20.Top = 0.3125F;
			this.Label20.Width = 1.125F;
			// 
			// txtTotAssess
			// 
			this.txtTotAssess.Height = 0.19F;
			this.txtTotAssess.Left = 3.1875F;
			this.txtTotAssess.MultiLine = false;
			this.txtTotAssess.Name = "txtTotAssess";
			this.txtTotAssess.OutputFormat = resources.GetString("txtTotAssess.OutputFormat");
			this.txtTotAssess.Style = "text-align: right; white-space: nowrap";
			this.txtTotAssess.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtTotAssess.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTotAssess.Tag = "textbox";
			this.txtTotAssess.Text = null;
			this.txtTotAssess.Top = 0.3125F;
			this.txtTotAssess.Width = 1.6875F;
			// 
			// txtTotExempt
			// 
			this.txtTotExempt.Height = 0.19F;
			this.txtTotExempt.Left = 4.9375F;
			this.txtTotExempt.MultiLine = false;
			this.txtTotExempt.Name = "txtTotExempt";
			this.txtTotExempt.OutputFormat = resources.GetString("txtTotExempt.OutputFormat");
			this.txtTotExempt.Style = "text-align: right; white-space: nowrap";
			this.txtTotExempt.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtTotExempt.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTotExempt.Tag = "textbox";
			this.txtTotExempt.Text = null;
			this.txtTotExempt.Top = 0.3125F;
			this.txtTotExempt.Width = 1.625F;
			// 
			// txtTotTotal
			// 
			this.txtTotTotal.Height = 0.19F;
			this.txtTotTotal.Left = 6.625F;
			this.txtTotTotal.MultiLine = false;
			this.txtTotTotal.Name = "txtTotTotal";
			this.txtTotTotal.OutputFormat = resources.GetString("txtTotTotal.OutputFormat");
			this.txtTotTotal.Style = "text-align: right; white-space: nowrap";
			this.txtTotTotal.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtTotTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTotTotal.Tag = "textbox";
			this.txtTotTotal.Text = null;
			this.txtTotTotal.Top = 0.3125F;
			this.txtTotTotal.Width = 1.8125F;
			// 
			// txtTotTax
			// 
			this.txtTotTax.Height = 0.19F;
			this.txtTotTax.Left = 8.5F;
			this.txtTotTax.MultiLine = false;
			this.txtTotTax.Name = "txtTotTax";
			this.txtTotTax.OutputFormat = resources.GetString("txtTotTax.OutputFormat");
			this.txtTotTax.Style = "text-align: right; white-space: nowrap";
			this.txtTotTax.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtTotTax.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTotTax.Tag = "textbox";
			this.txtTotTax.Text = null;
			this.txtTotTax.Top = 0.3125F;
			this.txtTotTax.Width = 1.4375F;
			// 
			// rptPPCollectionWide
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategoryOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1Val)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2Val)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3Val)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOtherVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailingAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategoryOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1Val;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2Val;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3Val;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatOtherVal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTax;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageTax;
	}
}
