﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmREAudit.
	/// </summary>
	partial class frmREAudit : BaseForm
	{
		public fecherFoundation.FCFrame framSummary;
		public FCGrid GridSummary;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCTextBox txtNotify;
		public fecherFoundation.FCTextBox txtCommunicate;
		public FCGrid TotalGrid;
		public FCGrid AuditGrid;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblDoubleClick;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuView;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSingleLine;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmREAudit));
            this.framSummary = new fecherFoundation.FCFrame();
            this.GridSummary = new fecherFoundation.FCGrid();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.txtNotify = new fecherFoundation.FCTextBox();
            this.txtCommunicate = new fecherFoundation.FCTextBox();
            this.TotalGrid = new fecherFoundation.FCGrid();
            this.AuditGrid = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblDoubleClick = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuView = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintSingleLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdPrintSingleLine = new fecherFoundation.FCButton();
            this.cmdView = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framSummary)).BeginInit();
            this.framSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AuditGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSingleLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdView)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 667);
            this.BottomPanel.Size = new System.Drawing.Size(946, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framSummary);
            this.ClientArea.Controls.Add(this.txtNotify);
            this.ClientArea.Controls.Add(this.txtCommunicate);
            this.ClientArea.Controls.Add(this.TotalGrid);
            this.ClientArea.Controls.Add(this.AuditGrid);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblDoubleClick);
            this.ClientArea.Size = new System.Drawing.Size(966, 703);
            this.ClientArea.Controls.SetChildIndex(this.lblDoubleClick, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.AuditGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.TotalGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCommunicate, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtNotify, 0);
            this.ClientArea.Controls.SetChildIndex(this.framSummary, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdView);
            this.TopPanel.Controls.Add(this.cmdPrintSingleLine);
            this.TopPanel.Size = new System.Drawing.Size(966, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintSingleLine, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdView, 0);
            // 
            // framSummary
            // 
            this.framSummary.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framSummary.AppearanceKey = "groupBoxNoBorders";
            this.framSummary.BackColor = System.Drawing.Color.White;
            this.framSummary.Controls.Add(this.GridSummary);
            this.framSummary.Controls.Add(this.Label3);
            this.framSummary.Controls.Add(this.Label1);
            this.framSummary.Location = new System.Drawing.Point(30, 30);
            this.framSummary.Name = "framSummary";
            this.framSummary.Size = new System.Drawing.Size(887, 637);
            this.framSummary.TabIndex = 0;
            this.framSummary.Text = "Real Estate Billing Transfer Summary";
            this.framSummary.Visible = false;
            // 
            // GridSummary
            // 
            this.GridSummary.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridSummary.Cols = 10;
            this.GridSummary.ExtendLastCol = true;
            this.GridSummary.FixedCols = 0;
            this.GridSummary.Location = new System.Drawing.Point(0, 30);
            this.GridSummary.Name = "GridSummary";
            this.GridSummary.RowHeadersVisible = false;
            this.GridSummary.Rows = 50;
            this.GridSummary.ShowFocusCell = false;
            this.GridSummary.Size = new System.Drawing.Size(862, 150);
            this.GridSummary.TabIndex = 0;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 200);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(142, 18);
            this.Label3.TabIndex = 1;
            this.Label3.Text = "***  IMPORTANT  ***";
            this.Label3.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 240);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(458, 201);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "IF THE DISPLAYED TOTALS ARE INCORRECT, STOP HERE.  SELECT THE VIEW DETAILS OPTION" +
    " IN THE FILE MENU TO LIST INDIVIDUAL ACCOUNTS";
            this.Label1.Visible = false;
            // 
            // txtNotify
            // 
            this.txtNotify.BackColor = System.Drawing.SystemColors.Window;
            this.txtNotify.Location = new System.Drawing.Point(562, 28);
            this.txtNotify.Name = "txtNotify";
            this.txtNotify.Size = new System.Drawing.Size(80, 40);
            this.txtNotify.TabIndex = 5;
            this.txtNotify.Visible = false;
            // 
            // txtCommunicate
            // 
            this.txtCommunicate.BackColor = System.Drawing.SystemColors.Window;
            this.txtCommunicate.Location = new System.Drawing.Point(2, 32);
            this.txtCommunicate.Name = "txtCommunicate";
            this.txtCommunicate.Size = new System.Drawing.Size(29, 40);
            this.txtCommunicate.TabIndex = 4;
            this.txtCommunicate.Visible = false;
            // 
            // TotalGrid
            // 
            this.TotalGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.TotalGrid.Cols = 10;
            this.TotalGrid.ColumnHeadersVisible = false;
            this.TotalGrid.ExtendLastCol = true;
            this.TotalGrid.FixedCols = 0;
            this.TotalGrid.FixedRows = 0;
            this.TotalGrid.Location = new System.Drawing.Point(30, 517);
            this.TotalGrid.Name = "TotalGrid";
            this.TotalGrid.RowHeadersVisible = false;
            this.TotalGrid.Rows = 3;
            this.TotalGrid.ShowFocusCell = false;
            this.TotalGrid.Size = new System.Drawing.Size(866, 150);
            this.TotalGrid.TabIndex = 3;
            // 
            // AuditGrid
            // 
            this.AuditGrid.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
            this.AuditGrid.AllowUserToResizeColumns = true;
            this.AuditGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.AuditGrid.Cols = 8;
            this.AuditGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.AuditGrid.Location = new System.Drawing.Point(30, 90);
            this.AuditGrid.Name = "AuditGrid";
            this.AuditGrid.Rows = 50;
            this.AuditGrid.ShowFocusCell = false;
            this.AuditGrid.Size = new System.Drawing.Size(866, 405);
            this.AuditGrid.TabIndex = 0;
            this.AuditGrid.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.AuditGrid_MouseMoveEvent);
            this.AuditGrid.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.AuditGrid_BeforeSort);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 60);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(529, 18);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "DRAG COLUMNS WITH THE LEFT MOUSE BUTTON TO MAKE THEM WIDER OR NARROWER";
            // 
            // lblDoubleClick
            // 
            this.lblDoubleClick.Location = new System.Drawing.Point(30, 30);
            this.lblDoubleClick.Name = "lblDoubleClick";
            this.lblDoubleClick.Size = new System.Drawing.Size(527, 18);
            this.lblDoubleClick.TabIndex = 1;
            this.lblDoubleClick.Text = "DOUBLE-CLICK ON A ROW TO EDIT THE ACCOUNT IN THE REAL ESTATE APPLICATION";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuView,
            this.mnuPrintSingleLine,
            this.mnuPrint,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuView
            // 
            this.mnuView.Index = 0;
            this.mnuView.Name = "mnuView";
            this.mnuView.Text = "View Details";
            this.mnuView.Visible = false;
            this.mnuView.Click += new System.EventHandler(this.mnuView_Click);
            // 
            // mnuPrintSingleLine
            // 
            this.mnuPrintSingleLine.Index = 1;
            this.mnuPrintSingleLine.Name = "mnuPrintSingleLine";
            this.mnuPrintSingleLine.Text = "Print Single Line Version";
            this.mnuPrintSingleLine.Click += new System.EventHandler(this.mnuPrintSingleLine_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 2;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 3;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(439, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(100, 48);
            this.cmdPrint.TabIndex = 0;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdPrintSingleLine
            // 
            this.cmdPrintSingleLine.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintSingleLine.Location = new System.Drawing.Point(774, 29);
            this.cmdPrintSingleLine.Name = "cmdPrintSingleLine";
            this.cmdPrintSingleLine.Size = new System.Drawing.Size(164, 24);
            this.cmdPrintSingleLine.TabIndex = 1;
            this.cmdPrintSingleLine.Text = "Print Single Line Version";
            this.cmdPrintSingleLine.Click += new System.EventHandler(this.mnuPrintSingleLine_Click);
            // 
            // cmdView
            // 
            this.cmdView.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdView.Location = new System.Drawing.Point(674, 29);
            this.cmdView.Name = "cmdView";
            this.cmdView.Size = new System.Drawing.Size(94, 24);
            this.cmdView.TabIndex = 2;
            this.cmdView.Text = "View Details";
            this.cmdView.Visible = false;
            this.cmdView.Click += new System.EventHandler(this.mnuView_Click);
            // 
            // frmREAudit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(966, 763);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmREAudit";
            this.ShowInTaskbar = false;
            this.Text = "Real Estate Audit";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmREAudit_Load);
            this.Resize += new System.EventHandler(this.frmREAudit_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmREAudit_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framSummary)).EndInit();
            this.framSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AuditGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSingleLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdView)).EndInit();
            this.ResumeLayout(false);

		}

        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdPrint;
		private FCButton cmdPrintSingleLine;
		private FCButton cmdView;
	}
}
