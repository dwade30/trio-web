﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmSupplementalBill.
	/// </summary>
	public partial class frmSupplementalBill : BaseForm
	{
		public frmSupplementalBill()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSupplementalBill InstancePtr
		{
			get
			{
				return (frmSupplementalBill)Sys.GetInstance(typeof(frmSupplementalBill));
			}
		}

		protected frmSupplementalBill _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int CNSTLISTGRIDCOLTYPE = 0;
		const int CNSTLISTGRIDCOLACCOUNT = 1;
		const int CNSTLISTGRIDCOLAMOUNT = 2;
		const int CNSTLISTGRIDCOLENTERED = 3;
		const int CNSTLISTGRIDCOLRATEKEY = 4;
		const int CNSTLISTGRIDCOLNAME = 5;
		const int CNSTLISTGRIDCOLBILLNUMBER = 6;
		const int CNSTLISTGRIDCOLTAXYEAR = 7;
		const int CNSTLISTGRIDCOLTAXACQUIRED = 8;
		clsRateRecord RERate = new clsRateRecord();
		clsRateRecord PPRate = new clsRateRecord();
		cPartyController pCont = new cPartyController();

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			if (cmbSupplemental.SelectedIndex != 0 && cmbSupplemental.SelectedIndex != 1)
			{
				MessageBox.Show("You must choose either Real Estate or Personal Property Supplemental", "Choose Supplemental Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbTaxFrom.SelectedIndex == 1 && Conversion.Val(txtTax.Text) <= 0)
			{
				MessageBox.Show("You have chosen to use the entered tax, but didn't enter a valid tax.", "Invalid Tax", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Conversion.Val(txtAccount.Text) < 1)
			{
				MessageBox.Show("You must enter a valid account number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (modGlobalConstants.Statics.gboolBD)
			{
				if (cmbSupplemental.SelectedIndex == 0)
				{
					if (RERate.RateKey > 0)
					{
						modREPPBudStuff.LoadREPPAccountInfo_2(RERate.TaxYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.RESupplemental))
							{
								MessageBox.Show("You must set up a valid real estate supplemental account in budgetary before adding this bill", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.REReceivable))
							{
								MessageBox.Show("You must set up a valid real estate receivable account in budgetary before adding this bill", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
				}
				if (cmbSupplemental.SelectedIndex == 1)
				{
					if (PPRate.RateKey > 0)
					{
						modREPPBudStuff.LoadREPPAccountInfo_2(PPRate.TaxYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental))
							{
								MessageBox.Show("You must set up a valid personal property supplemental account in budgetary before adding this bill", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.PPReceivable))
							{
								MessageBox.Show("You must set up a valid personal property receivable account in budgetary before adding this bill", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
				}
			}
			if (cmbSupplemental.SelectedIndex == 0)
			{
				MakeRESupplemental();
			}
			else
			{
				MakePPSupplemental();
			}
		}

		private void frmSupplementalBill_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmSupplementalBill_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSupplementalBill properties;
			//frmSupplementalBill.ScaleWidth	= 9045;
			//frmSupplementalBill.ScaleHeight	= 7230;
			//frmSupplementalBill.LinkTopic	= "Form1";
			//frmSupplementalBill.LockControls	= true;
			//End Unmaped Properties
			RERate = new clsRateRecord();
			PPRate = new clsRateRecord();
			SetupListGrid();
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			if (modGlobalConstants.Statics.gboolBD)
			{
			}
		}

		private void frmSupplementalBill_Resize(object sender, System.EventArgs e)
		{
			ResizeListGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void ListGrid_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (ListGrid.MouseRow < 1 || ListGrid.MouseRow > ListGrid.Rows - 1)
					return;
				ListGrid.Row = ListGrid.MouseRow;
				this.PopupMenu(mnuPop);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupListGrid()
		{
			ListGrid.Cols = 9;
			ListGrid.Rows = 1;
			ListGrid.TextMatrix(0, 0, "Type");
			ListGrid.TextMatrix(0, 1, "Account");
			ListGrid.TextMatrix(0, 2, "Tax");
			ListGrid.TextMatrix(0, 3, "Entered");
			ListGrid.ExtendLastCol = true;
			ListGrid.ColHidden(4, true);
			// ratekey
			ListGrid.ColHidden(5, true);
			// name   so we can sort by name
			ListGrid.ColHidden(6, true);
			// billnumber for the commitement book sql statement
			ListGrid.ColHidden(7, true);
			// tax year so we can sort by year
			ListGrid.ColHidden(8, true);
			// TAX ACQUIRED
			ListGrid.ColDataType(8, FCGrid.DataTypeSettings.flexDTBoolean);
			ListGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			ListGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeListGrid()
		{
			int GridWidth = 0;
			GridWidth = ListGrid.WidthOriginal;
			ListGrid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.15));
			ListGrid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.21));
			ListGrid.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.35));
			ListGrid.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.14));
		}

		public void MakeRESupplemental()
		{
			clsDRWrapper clsre = new clsDRWrapper();
			double dblTaxAmount = 0;
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			// response for msgbox
			try
			{
				// On Error GoTo ErrorHandler
				if (RERate.RateKey == 0)
				{
					// haven't chosen or created a rate record yet so make them
					//! Load frmAuditInfo;
					frmAuditInfo.InstancePtr.Init(5, 1);
					// 5 is supplemental and 1 is RE
					frmAuditInfo.InstancePtr.Show(App.MainForm);
					return;
				}
				
				if (cmbTaxFrom.SelectedIndex == 1)
				{					
					clsre.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)) + " and rscard = 1 and isnull(rsdeleted,0) = 0", "RealEstate");
					if (clsre.EndOfFile())
					{
						MessageBox.Show("This Real Estate account doesn't exist or is deleted.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					dblTaxAmount = FCConvert.ToDouble(txtTax.Text);
					intResponse = MessageBox.Show("Account " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)))) + " Name " + clsre.Get_Fields_String("DeedName1") + "\r\n" + "Tax " + Strings.Format(dblTaxAmount, "#,###,###,##0.00") + "\r\n" + "Is this Correct?", "Correct?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intResponse == DialogResult.No)
						return;
					if (modGlobalConstants.Statics.gboolBD)
					{
						if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable) && clsre.Get_Fields_Boolean("taxacquired"))
						{
							MessageBox.Show("You must set up a valid tax acquired receivable account in budgetary before adding this account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					ListGrid.AddItem("RE" + "\t" + Strings.Trim(txtAccount.Text) + "\t" + Strings.Format(Strings.Trim(txtTax.Text), "#,###,##0.00") + "\t" + "Yes" + "\t" + FCConvert.ToString(RERate.RateKey) + "\t" + clsre.Get_Fields_String("DeedName1"));
					ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXYEAR, FCConvert.ToString(RERate.TaxYear));
					ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXACQUIRED, FCConvert.ToString(clsre.Get_Fields_Boolean("taxacquired")));
				}
				else
				{
					// get the account information
					clsre.OpenRecordset("select sum(isnull(lastlandval, 0)) as landsum, sum(isnull(lastbldgval, 0)) as bldgsum, sum(isnull(rlexemption, 0)) as exemptsum,rsaccount from master where rsaccount = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)) + " and isnull(rsdeleted,0) <> 1 group by rsaccount", "twre0000.vb1");
					if (clsre.EndOfFile())
					{
						MessageBox.Show("This Real Estate account doesn't exist or is deleted.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					dblTaxAmount = clsre.Get_Fields("landsum") + clsre.Get_Fields("bldgsum") - clsre.Get_Fields("exemptsum");
					if (dblTaxAmount > 0)
					{
						dblTaxAmount *= RERate.TaxRate;
						dblTaxAmount = modMain.Round(dblTaxAmount, 2);
					}
					else
					{
						dblTaxAmount = 0;
					}
					
					clsre.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)) + " and rscard = 1", "RealEstate");

					intResponse = MessageBox.Show("Account " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)))) + " Name " + clsre.Get_Fields_String("DeedName1") + "\r\n" + "Tax " + Strings.Format(dblTaxAmount, "#,###,###,##0.00") + "\r\n" + "Is this Correct?", "Correct?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intResponse == DialogResult.No)
						return;
					// Now save the info in the list
					if (modGlobalConstants.Statics.gboolBD)
					{
						if (Strings.Trim(modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable) == string.Empty && clsre.Get_Fields_Boolean("taxacquired"))
						{
							MessageBox.Show("You must set up a valid tax acquired receivable account in budgetary before adding this account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					ListGrid.AddItem("RE" + "\t" + Strings.Trim(txtAccount.Text) + "\t" + Strings.Format(dblTaxAmount, "#,###,###,##0.00") + "\t" + "No" + "\t" + FCConvert.ToString(RERate.RateKey) + "\t" + clsre.Get_Fields_String("DeedName1"));
					ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXYEAR, FCConvert.ToString(RERate.TaxYear));
					ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXACQUIRED, FCConvert.ToString(clsre.Get_Fields_Boolean("taxacquired")));
				}
				txtTax.Text = "";
				txtAccount.Text = "";
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "In Make RE Supplemental", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void MakePPSupplemental()
		{
			clsDRWrapper clsPP = new clsDRWrapper();
			double dblTaxAmount = 0;
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			cParty tParty;
			try
			{
				// On Error GoTo ErrorHandler
				if (PPRate.RateKey == 0)
				{
					// they haven't chosen a rate record so make them
					//! Load frmAuditInfo;
					frmAuditInfo.InstancePtr.Init(5, 2);
					frmAuditInfo.InstancePtr.Show(App.MainForm);
					return;
				}
				if (cmbTaxFrom.SelectedIndex == 1)
				{
					// Call clsPP.OpenRecordset("Select * from ppmaster CROSS APPLY " & clsPP.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(PartyID) as p where account = " & Val(txtAccount.Text) & " and deleted <> 1 ", "twpp0000.vb1")
					clsPP.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)) + " and isnull(deleted,0) = 0", "PersonalProperty");
					if (clsPP.EndOfFile())
					{
						MessageBox.Show(("This Personal Property account either doesn't exist or is deleted."), "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					tParty = pCont.GetParty(FCConvert.ToInt32(Conversion.Val(clsPP.Get_Fields_Int32("PartyID"))));
					if (tParty == null)
					{
						tParty = new cParty();
					}
					dblTaxAmount = FCConvert.ToDouble(txtTax.Text);
					intResponse = MessageBox.Show("Account " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)))) + " Name " + Strings.Trim(tParty.FullNameLastFirst) + "\r\n" + "Tax " + Strings.Format(dblTaxAmount, "#,###,###,##0.00") + "\r\n" + "Is this correct?", "Correct?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intResponse == DialogResult.No)
						return;
					ListGrid.AddItem("PP" + "\t" + Strings.Trim(txtAccount.Text) + "\t" + Strings.Format(Strings.Trim(txtTax.Text), "#,###,##0.00") + "\t" + "Yes" + "\t" + FCConvert.ToString(PPRate.RateKey) + "\t" + Strings.Trim(tParty.FullNameLastFirst) + "\t");
					ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXYEAR, FCConvert.ToString(PPRate.TaxYear));
					ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXACQUIRED, FCConvert.ToString(false));
				}
				else
				{
					// get the account information
					// Call clsPP.OpenRecordset("Select * from ppmaster CROSS APPLY " & clsPP.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(PartyID) as p where account = " & Val(txtAccount.Text) & " and deleted <> 1 ", "twpp0000.vb1")
					clsPP.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)) + " and isnull(deleted,0) = 0", "PersonalProperty");
					if (clsPP.EndOfFile())
					{
						MessageBox.Show("This Personal Property account either doesn't exist or is deleted.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					tParty = pCont.GetParty(FCConvert.ToInt32(Conversion.Val(clsPP.Get_Fields_Int32("PartyID"))));
					if (tParty == null)
					{
						tParty = new cParty();
					}
					// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
					dblTaxAmount = Conversion.Val(clsPP.Get_Fields("value")) - Conversion.Val(clsPP.Get_Fields_Int32("exemption"));
					if (dblTaxAmount < 0)
						dblTaxAmount = 0;
					dblTaxAmount *= PPRate.TaxRate;
					dblTaxAmount = modMain.Round(dblTaxAmount, 2);
					intResponse = MessageBox.Show("Account " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)))) + " Name " + Strings.Trim(tParty.FullNameLastFirst) + "\r\n" + "Tax " + Strings.Format(dblTaxAmount, "#,###,###,##0.00") + "\r\n" + "Is this correct?", "Correct?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intResponse == DialogResult.No)
						return;
					ListGrid.AddItem("PP" + "\t" + Strings.Trim(txtAccount.Text) + "\t" + Strings.Format(dblTaxAmount, "#,###,###,##0.00") + "\t" + "No" + "\t" + FCConvert.ToString(PPRate.RateKey));
					ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXYEAR, FCConvert.ToString(PPRate.TaxYear));
					ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXACQUIRED, FCConvert.ToString(false));
				}
				txtTax.Text = "";
				txtAccount.Text = "";
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "In Make PP Supplemental", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void FillRERate(clsRateRecord clsRERateRec)
		{
			RERate.BillingDate = clsRERateRec.BillingDate;
			RERate.Set_DueDate(1, clsRERateRec.Get_DueDate(1));
			RERate.Set_DueDate(2, clsRERateRec.Get_DueDate(2));
			RERate.Set_DueDate(3, clsRERateRec.Get_DueDate(3));
			RERate.Set_DueDate(4, clsRERateRec.Get_DueDate(4));
			RERate.InterestRate = clsRERateRec.InterestRate;
			RERate.Set_InterestStartDate(1, clsRERateRec.Get_InterestStartDate(1));
			RERate.Set_InterestStartDate(2, clsRERateRec.Get_InterestStartDate(2));
			RERate.Set_InterestStartDate(3, clsRERateRec.Get_InterestStartDate(3));
			RERate.Set_InterestStartDate(4, clsRERateRec.Get_InterestStartDate(4));
			RERate.NumberOfPeriods = clsRERateRec.NumberOfPeriods;
			RERate.RateKey = clsRERateRec.RateKey;
			RERate.RateType = clsRERateRec.RateType;
			RERate.TaxRate = clsRERateRec.TaxRate;
			RERate.TaxYear = clsRERateRec.TaxYear;
			RERate.Set_BillWeight(1, clsRERateRec.Get_BillWeight(1));
			RERate.Set_BillWeight(2, clsRERateRec.Get_BillWeight(2));
			RERate.Set_BillWeight(3, clsRERateRec.Get_BillWeight(3));
			RERate.Set_BillWeight(4, clsRERateRec.Get_BillWeight(4));
		}

		public void FillPPRate(clsRateRecord clsPPRateRec)
		{
			PPRate.BillingDate = clsPPRateRec.BillingDate;
			PPRate.Set_DueDate(1, clsPPRateRec.Get_DueDate(1));
			PPRate.Set_DueDate(2, clsPPRateRec.Get_DueDate(2));
			PPRate.Set_DueDate(3, clsPPRateRec.Get_DueDate(3));
			PPRate.Set_DueDate(4, clsPPRateRec.Get_DueDate(4));
			PPRate.InterestRate = clsPPRateRec.InterestRate;
			PPRate.Set_InterestStartDate(1, clsPPRateRec.Get_InterestStartDate(1));
			PPRate.Set_InterestStartDate(2, clsPPRateRec.Get_InterestStartDate(2));
			PPRate.Set_InterestStartDate(3, clsPPRateRec.Get_InterestStartDate(3));
			PPRate.Set_InterestStartDate(4, clsPPRateRec.Get_InterestStartDate(4));
			PPRate.NumberOfPeriods = clsPPRateRec.NumberOfPeriods;
			PPRate.RateKey = clsPPRateRec.RateKey;
			PPRate.RateType = clsPPRateRec.RateType;
			PPRate.TaxRate = clsPPRateRec.TaxRate;
			PPRate.TaxYear = clsPPRateRec.TaxYear;
			PPRate.Set_BillWeight(1, clsPPRateRec.Get_BillWeight(1));
			PPRate.Set_BillWeight(2, clsPPRateRec.Get_BillWeight(2));
			PPRate.Set_BillWeight(3, clsPPRateRec.Get_BillWeight(3));
			PPRate.Set_BillWeight(4, clsPPRateRec.Get_BillWeight(4));
		}

		private void mnuPPRate_Click(object sender, System.EventArgs e)
		{
			//! Load frmAuditInfo;
			frmAuditInfo.InstancePtr.Init(5, 4);
			frmAuditInfo.InstancePtr.Show(App.MainForm);
			return;
		}

		private void mnuProcessBill_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			double dblRETaxTotal;
			double dblPPTaxTotal;
			double dblRETaxAcquiredTotal = 0;
			double dblRERegTaxTotal = 0;
			//FileSystemObject fso = new FileSystemObject();
			bool boolREBills;
			// if true, there were re bills
			bool boolPPBills;
			// if true, there were pp bills
			int lngYear = 0;
			string strREList;
			string strPPList;
			modBudgetaryAccounting.FundType[] FundRec = null;
			int intCurFundRec;
			bool boolTaxAcquired;
			int lngCurRateKey;
			string strCurType;
			try
			{
				// On Error GoTo ErrorHandler
				int x;
				// loop counter
				clsDRWrapper clsTemp = new clsDRWrapper();
				intCurFundRec = -1;
				boolTaxAcquired = false;
				strCurType = "";
				lngCurRateKey = -1;
				if (ListGrid.Rows < 2)
				{
					MessageBox.Show("There are no supplementals in the list to process", "Empty List", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				intResponse = MessageBox.Show("This will process the list of bills and clear the list." + "\r\n" + "Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResponse == DialogResult.No)
					return;

				ListGrid.Col = CNSTLISTGRIDCOLNAME;
				ListGrid.Sort = FCGrid.SortSettings.flexSortStringNoCaseAscending;
				ListGrid.Col = CNSTLISTGRIDCOLTAXACQUIRED;
				ListGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
				ListGrid.Col = CNSTLISTGRIDCOLRATEKEY;
				ListGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				ListGrid.Col = CNSTLISTGRIDCOLTYPE;
				ListGrid.Sort = FCGrid.SortSettings.flexSortStringNoCaseAscending;
				// build supplemental bills
				// Keep a total so we can add this to
				dblRETaxTotal = 0;
				dblPPTaxTotal = 0;
				//! Load frmWait;
				frmWait.InstancePtr.Init("Please wait while bills are processed.");
				// frmWait.lblMessage.Caption = "Please wait while bills are processed."
				boolREBills = false;
				boolPPBills = false;
				strREList = "";
				strPPList = "";
				for (x = 1; x <= ListGrid.Rows - 1; x++)
				{
					//Application.DoEvents();
					if (modGlobalConstants.Statics.gboolBD)
					{
						if ((lngCurRateKey != Conversion.Val(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLRATEKEY))) || (boolTaxAcquired != FCConvert.CBool(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLTAXACQUIRED))) || (Strings.UCase(strCurType) != Strings.UCase(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLTYPE))))
						{
							if (lngCurRateKey != Conversion.Val(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLRATEKEY)) || strCurType != Strings.UCase(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLTYPE)))
							{
								if (lngCurRateKey > 0)
								{
									// make sure this isn't different because its the first record
									modREPPBudStuff.LoadREPPAccountInfo(ref lngYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
									if (strCurType == "RE" && dblRETaxTotal != 0)
									{
										intCurFundRec += 1;
										Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
										FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.RESupplemental;
										FundRec[intCurFundRec].AcctType = "A";
										FundRec[intCurFundRec].RCB = "R";
										FundRec[intCurFundRec].UseDueToFrom = false;
										FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Supplemental";
										if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
										{
											FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
										}
										FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(-dblRETaxTotal);
										dblRETaxTotal = 0;
										dblRERegTaxTotal = 0;
										dblRETaxAcquiredTotal = 0;
									}
									else if (dblPPTaxTotal != 0)
									{
										intCurFundRec += 1;
										Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
										FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental;
										FundRec[intCurFundRec].AcctType = "A";
										FundRec[intCurFundRec].RCB = "R";
										FundRec[intCurFundRec].UseDueToFrom = false;
										FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Supplemental";
										if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
										{
											FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
										}
										FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(-dblPPTaxTotal);
										dblPPTaxTotal = 0;
									}
								}
							}
							strCurType = Strings.UCase(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLTYPE));
							boolTaxAcquired = FCConvert.CBool(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLTAXACQUIRED));
							lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLTAXYEAR))));
							lngCurRateKey = FCConvert.ToInt32(Math.Round(Conversion.Val(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLRATEKEY))));
							modREPPBudStuff.LoadREPPAccountInfo(ref lngYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
							intCurFundRec += 1;
							Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
							FundRec[intCurFundRec].AcctType = "A";
							FundRec[intCurFundRec].RCB = "R";
							FundRec[intCurFundRec].UseDueToFrom = false;
							FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Supplemental";
							if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
							{
								FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
							}
							modREPPBudStuff.LoadREPPAccountInfo(ref lngYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
							if (strCurType == "RE")
							{
								if (!boolTaxAcquired)
								{
									FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.REReceivable;
								}
								else
								{
									FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable;
								}
							}
							else
							{
								FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.PPReceivable;
							}
						}
					}
					if (ListGrid.TextMatrix(x, 0) == "RE")
					{
						// if the bill is successfully made then add this to the total
						// to be transferred to budgetary
						if (MakeREBill(ref x))
						{
							boolREBills = true;
							dblRETaxTotal += FCConvert.ToDouble(ListGrid.TextMatrix(x, 2));
							if (!FCConvert.CBool(ListGrid.TextMatrix(x, CNSTLISTGRIDCOLTAXACQUIRED)))
							{
								dblRERegTaxTotal += FCConvert.ToDouble(ListGrid.TextMatrix(x, 2));
							}
							else
							{
								dblRETaxAcquiredTotal += FCConvert.ToDouble(ListGrid.TextMatrix(x, 2));
							}
							if (modGlobalConstants.Statics.gboolBD)
							{
								FundRec[intCurFundRec].Amount += FCConvert.ToDecimal(FCConvert.ToDouble(ListGrid.TextMatrix(x, 2)));
							}
							strREList += ListGrid.TextMatrix(x, 6) + ",";
						}
						else
						{
							ListGrid.RemoveItem(x);
							x -= 1;
						}
					}
					else
					{
						if (MakePPBill(ref x))
						{
							boolPPBills = true;
							dblPPTaxTotal += FCConvert.ToDouble(ListGrid.TextMatrix(x, 2));
							if (modGlobalConstants.Statics.gboolBD)
							{
								FundRec[intCurFundRec].Amount += FCConvert.ToDecimal(FCConvert.ToDouble(ListGrid.TextMatrix(x, 2)));
							}
							strPPList += ListGrid.TextMatrix(x, 6) + ",";
						}
						else
						{
							ListGrid.RemoveItem(x);
							x -= 1;
						}
					}
				}
				// x
				if (intCurFundRec >= 0 && modGlobalConstants.Statics.gboolBD)
				{
					if (lngCurRateKey > 0)
					{
						// make sure this isn't different because its the first record
						if (strCurType == "RE" && dblRETaxTotal != 0)
						{
							modREPPBudStuff.LoadREPPAccountInfo(ref lngYear, FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
							intCurFundRec += 1;
							Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
							FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.RESupplemental;
							FundRec[intCurFundRec].AcctType = "A";
							FundRec[intCurFundRec].RCB = "R";
							FundRec[intCurFundRec].UseDueToFrom = false;
							FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Supplemental";
							if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
							{
								FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
							}
							FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(-dblRETaxTotal);
							dblRETaxTotal = 0;
							dblRERegTaxTotal = 0;
							dblRETaxAcquiredTotal = 0;
						}
						else if (dblPPTaxTotal != 0)
						{
							intCurFundRec += 1;
							Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
							FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental;
							FundRec[intCurFundRec].AcctType = "A";
							FundRec[intCurFundRec].RCB = "R";
							FundRec[intCurFundRec].UseDueToFrom = false;
							FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Supplemental";
							if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
							{
								FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
							}
							FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(-dblPPTaxTotal);
							dblPPTaxTotal = 0;
						}
					}
				}
				if (modGlobalConstants.Statics.gboolBD)
				{
					int lngReturn = 0;
					if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
					{
						lngReturn = modBudgetaryAccounting.AddToJournal(ref FundRec, "GJ", 0, DateTime.Today, Strings.Right(FCConvert.ToString(lngYear), 2) + " Supplemental Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
					}
					else
					{
						lngReturn = modBudgetaryAccounting.AddToJournal(ref FundRec, "GJ", 0, DateTime.Today, FCConvert.ToString(lngYear) + " Supplemental");
					}
					if (lngReturn > 0)
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Data added to budgetary database in Journal " + FCConvert.ToString(lngReturn), "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
						if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
						{
							modGlobalFunctions.AddCYAEntry_26("CL", "Tax Supplement Journal" + FCConvert.ToString(lngReturn), "Year " + FCConvert.ToString(lngYear));
						}
						else
						{
							modGlobalFunctions.AddCYAEntry_80("CL", "Tax Supplement Journal" + FCConvert.ToString(lngReturn), "Year " + FCConvert.ToString(lngYear), "Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
						}
					}
					else
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("The journal entry was not created in budgetary", "No Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
						{
							modGlobalFunctions.AddCYAEntry_26("CL", "Tax Supp Journal Failed", "Year " + FCConvert.ToString(lngYear));
						}
						else
						{
							modGlobalFunctions.AddCYAEntry_80("CL", "Tax Supp Journal Failed", "Year " + FCConvert.ToString(lngYear), "Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
						}
					}
				}
				else
				{
					frmWait.InstancePtr.Unload();
				}
				if (strREList != string.Empty)
				{
					strREList = Strings.Mid(strREList, 1, strREList.Length - 1);
					// get rid of the extra comma
				}
				if (strPPList != string.Empty)
				{
					strPPList = Strings.Mid(strPPList, 1, strPPList.Length - 1);
				}
				// print the commitment book(s)
				// 
				if (boolREBills || boolPPBills)
				{
					frmPickCommitmentYear.InstancePtr.Show(FormShowEnum.Modal);
					if (boolREBills)
					{
						strREList = FCConvert.ToString(RERate.TaxYear) + "," + strREList;
					}
					if (boolPPBills)
					{
						strPPList = FCConvert.ToString(PPRate.TaxYear) + "," + strPPList;
					}
					//FC:FINAL:MSH - issue #1526: reverted changes from revision 5656, because reports will be showed as modal window(as in original) and in ReportEnd bool values won't be correct 
					if (MessageBox.Show("Do you want to print the supplemental bill(s) now?", "Print Supplemental(s)?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (boolREBills)
						{
							frmChooseBillFormat.InstancePtr.Init(true, strREList);
						}
						if (boolPPBills)
						{
							frmChooseBillFormat.InstancePtr.Init(true, strPPList);
						}
					}
				}
				// now clear the list so they can't be processed twice
				ListGrid.Rows = 1;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Process Bill", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuRemove_Click(object sender, System.EventArgs e)
		{
			ListGrid.RemoveItem(ListGrid.Row);
		}

		private void mnuRERate_Click(object sender, System.EventArgs e)
		{
			//! Load frmAuditInfo;
			frmAuditInfo.InstancePtr.Init(5, 3);
			frmAuditInfo.InstancePtr.Show(App.MainForm);
			return;
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private bool MakeREBill(ref int intRow)
		{
			bool MakeREBill = false;
			clsDRWrapper clsre = new clsDRWrapper();
			clsDRWrapper clsBill = new clsDRWrapper();
			clsDRWrapper clsexempt = new clsDRWrapper();
			int lngAccount;
			// vbPorter upgrade warning: intBillNumber As short --> As int	OnWrite(int, double)
			int intBillNumber = 0;
			int lngLand = 0;
			// need land,bldg,exempt because these will be returned
			int lngBldg = 0;
			// by an aggregate function and the rest will not
			int lngExempt = 0;
			double dblAcres = 0;
			double dblTaxperPeriod = 0;
			// the next 5 variable are for calc. the tax per period
			int intNumPeriods = 0;
			double dblOrigTaxVal = 0;
			double[] dblTax = new double[4 + 1];
			double dblTemp = 0;
			int x;
			// loop counter
			int y;
			clsDRWrapper clsBookPage = new clsDRWrapper();
			// Dim HomesteadArray(5, 2) As Long
			int lngTemp;
			string strTemp = "";
			string strBookPage = "";
			cParty tParty;
			cParty sParty;
			// vbPorter upgrade warning: tAddress As cPartyAddress	OnWrite(Collection, cPartyAddress)
			cPartyAddress tAddress;
			try
			{
				// On Error GoTo ErrorHandler
				MakeREBill = false;
				if (RERate.RateKey != FCConvert.ToDouble(ListGrid.TextMatrix(intRow, 4)))
				{
					RERate.LoadRate(FCConvert.ToInt32(ListGrid.TextMatrix(intRow, 4)));
				}
				lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(ListGrid.TextMatrix(intRow, 1))));
				clsBill.OpenRecordset("select max(billingyear) as maxbill from billingmaster where billingtype = 'RE' and account = " + FCConvert.ToString(Conversion.Val(ListGrid.TextMatrix(intRow, 1))) + " and billingyear between " + FCConvert.ToString(RERate.TaxYear * 10) + " and " + FCConvert.ToString(RERate.TaxYear * 10 + 9), "twcl0000.vb1");
				intBillNumber = 2;
				// don't want any supplementals as bill 1
				if (!clsBill.EndOfFile())
				{
					// find out the max and then add 1 to it
					// if there are already 9 then warn the user that this one can't be added
					// TODO Get_Fields: Field [maxbill] not found!! (maybe it is an alias?)
					intBillNumber = FCConvert.ToInt32(Conversion.Val(clsBill.Get_Fields("maxbill")) - (RERate.TaxYear * 10));
					if (intBillNumber > 8)
					{
						MessageBox.Show("This will make too many supplemental bills for Real Estate account " + FCConvert.ToString(Conversion.Val(ListGrid.TextMatrix(intRow, 1))) + "\r\n" + "This bill will not be processed and will be removed from the list.", "Unable to Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						// .RemoveItem (intRow)
						return MakeREBill;
					}
					if (intBillNumber <= 0)
					{
						intBillNumber = 2;
					}
					else
					{
						intBillNumber += 1;
					}
				}
				else
				{
					intBillNumber = 2;
				}
				// .TextMatrix(intRow, 6) = intBillNumber    'save for the processing part
				// make sure we don't return a record
				// do this because it's easier than an insert
				clsBill.OpenRecordset("select * from billingmaster where id = -1", "twcl0000.vb1");
				clsBill.AddNew();
				clsBill.Update();
				ListGrid.TextMatrix(intRow, 6, FCConvert.ToString(clsBill.Get_Fields_Int32("ID")));
				clsre.OpenRecordset("select sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum, sum(rlexemption) as exemptsum, sum(piacres) as acressum from master where rsaccount = " + ListGrid.TextMatrix(intRow, 1), "twre0000.vb1");
				// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
				lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsre.Get_Fields("landsum"))));
				// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
				lngBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsre.Get_Fields("bldgsum"))));
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsre.Get_Fields("exemptsum"))));
				// TODO Get_Fields: Field [acresSUM] not found!! (maybe it is an alias?)
				dblAcres = Conversion.Val(clsre.Get_Fields("acresSUM"));
				clsre.OpenRecordset("select * from master where rsaccount = " + ListGrid.TextMatrix(intRow, 1) + " and rscard = 1", "RealEstate");
				tParty = pCont.GetParty(clsre.Get_Fields_Int32("OwnerPartyID"), false);
				if (tParty == null)
				{
					tParty = new cParty();
				}
				// now build the bill
				clsBill.Set_Fields("account", FCConvert.ToString(Conversion.Val(ListGrid.TextMatrix(intRow, 1))));
				clsBill.Set_Fields("whetherbilledbefore", "A");
				clsBill.Set_Fields("billingtype", "RE");
				clsBill.Set_Fields("Billingyear", (RERate.TaxYear * 10) + intBillNumber);
				clsBill.Set_Fields("trancode", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("ritrancode"))));
				clsBill.Set_Fields("buildingcode", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("ribldgcode"))));
				clsBill.Set_Fields("name1", clsre.Get_Fields_String("DeedName1"));
				clsBill.Set_Fields("name2", clsre.Get_Fields_String("DeedName2"));
				if (tParty.Addresses.Count > 0)
				{
					tAddress = tParty.Addresses[1];
				}
				else
				{
					tAddress = new cPartyAddress();
				}
				clsBill.Set_Fields("address1", Strings.Trim(tAddress.Address1));
				clsBill.Set_Fields("address2", Strings.Trim(tAddress.Address2));
				clsBill.Set_Fields("mailingaddress3", Strings.Trim(tAddress.Address3));
				if (Strings.Trim(tAddress.City) == string.Empty)
				{
					clsBill.Set_Fields("address3", " ");
				}
				else
				{
					clsBill.Set_Fields("address3", Strings.Trim(tAddress.City));
				}
				clsBill.Set_Fields("address3", clsBill.Get_Fields_String("address3") + " ");
				clsBill.Set_Fields("address3", clsBill.Get_Fields_String("address3") + Strings.Trim(tAddress.State));
				clsBill.Set_Fields("address3", clsBill.Get_Fields_String("address3") + "  " + tAddress.Zip);
				if (Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rsmaplot"))) != string.Empty)
				{
					clsBill.Set_Fields("Maplot", Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rsmaplot"))));
				}
				if (Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocnumalph"))) != string.Empty)
				{
					clsBill.Set_Fields("streetnumber", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_String("rslocnumalph"))));
				}
				if (Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocapt"))) != string.Empty)
				{
					clsBill.Set_Fields("apt", Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocapt"))));
				}
				if (Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocstreet"))) != string.Empty)
				{
					clsBill.Set_Fields("streetname", Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocstreet"))));
				}
				if (Strings.UCase(ListGrid.TextMatrix(0, 3)) == "YES")
				{
					// amount is entered
					clsBill.Set_Fields("landvalue", 0);
					clsBill.Set_Fields("buildingvalue", 0);
					clsBill.Set_Fields("exemptvalue", 0);
					clsBill.Set_Fields("acres", 0);
					clsBill.Set_Fields("homesteadexemption", 0);
					clsBill.Set_Fields("otherexempt1", 0);
				}
				else
				{
					// amount is calculated
					clsBill.Set_Fields("landvalue", lngLand);
					clsBill.Set_Fields("buildingvalue", lngBldg);
					clsBill.Set_Fields("exemptvalue", lngExempt);
					clsBill.Set_Fields("acres", dblAcres);
					clsBill.Set_Fields("exempt1", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("riexemptcd1"))));
					clsBill.Set_Fields("exempt2", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("riexemptcd2"))));
					clsBill.Set_Fields("exempt3", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("riexemptcd3"))));
					clsBill.Set_Fields("landcode", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("rilandcode"))));
					clsBill.Set_Fields("trancode", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("ritrancode"))));
					clsBill.Set_Fields("buildingcode", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("ribldgcode"))));
					if (Conversion.Val(clsre.Get_Fields_Int32("riexemptcd1")) > 0)
					{
						clsBill.Set_Fields("homesteadexemption", FCConvert.ToString(Conversion.Val(clsre.Get_Fields_Int32("homesteadvalue"))));
						clsBill.Set_Fields("otherexempt1", Conversion.Val(clsre.Get_Fields_Int32("rlexemption")) - Conversion.Val(clsre.Get_Fields_Int32("homesteadvalue")));
					}
				}
				for (x = 1; x <= 4; x++)
				{
					dblTax[x] = 0;
				}
				// x
				intNumPeriods = RERate.NumberOfPeriods;
				dblOrigTaxVal = FCConvert.ToDouble(ListGrid.TextMatrix(intRow, 2));
				dblTaxperPeriod = dblOrigTaxVal / intNumPeriods;
				dblTaxperPeriod = modMain.Round(dblTaxperPeriod, 2);
				modGlobalFunctions.AddCYAEntry_728("CL", "Added RE Supplemental Bill", "Account " + ListGrid.TextMatrix(intRow, 1), "Bill key " + clsBill.Get_Fields_Int32("ID"), "Rate Key " + FCConvert.ToString(RERate.RateKey), "Amount " + FCConvert.ToString(dblOrigTaxVal));
				bool boolAllEqual = false;
				// Dim Y As Integer
				double dblLeftOver = 0;
				if (dblOrigTaxVal >= 100 || intNumPeriods < 4 || modGlobalVariables.Statics.CustomizedInfo.SplitSmallAmounts)
				{
					boolAllEqual = true;
					for (y = 1; y <= intNumPeriods; y++)
					{
						if (RERate.Get_BillWeight(y) != RERate.Get_BillWeight(1))
						{
							boolAllEqual = false;
						}
					}
					// y
					if (boolAllEqual)
					{
						for (y = 1; y <= intNumPeriods; y++)
						{
							dblTax[y] = modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(dblTaxperPeriod)), 2);
						}
						// y
						dblLeftOver = modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(dblOrigTaxVal - (dblTaxperPeriod * intNumPeriods))), 2);
						if (dblLeftOver < 0)
						{
							// make the last bill the smaller
							dblTax[intNumPeriods] += dblLeftOver;
						}
						else
						{
							// make the first bill the largest
							dblTax[1] += dblLeftOver;
						}
						// if dblleftover
					}
					else
					{
						dblTemp = 0;
						dblLeftOver = dblOrigTaxVal;
						for (y = 1; y <= intNumPeriods - 1; y++)
						{
							if (dblLeftOver > 0)
							{
								dblTemp = modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(RERate.Get_BillWeight(y) * dblOrigTaxVal)), 2);
								if (dblTemp <= dblLeftOver)
								{
									dblTax[y] = dblTemp;
									dblLeftOver -= dblTemp;
								}
								else
								{
									dblTax[y] = dblLeftOver;
									dblLeftOver = 0;
								}
							}
						}
						// y
						if (dblLeftOver > 0)
						{
							dblTax[intNumPeriods] = dblLeftOver;
						}
					}
				}
				else
				{
					dblTax[1] = dblOrigTaxVal;
					dblTax[2] = 0;
					dblTax[3] = 0;
					dblTax[4] = 0;
				}
				// 
				clsBill.Set_Fields("taxdue1", dblTax[1]);
				clsBill.Set_Fields("taxdue2", dblTax[2]);
				clsBill.Set_Fields("taxdue3", dblTax[3]);
				clsBill.Set_Fields("taxdue4", dblTax[4]);
				clsBill.Set_Fields("ratekey", RERate.RateKey);
				clsBill.Set_Fields("transferfrombillingdatefirst", DateTime.Today);
				clsBill.Set_Fields("Interestappliedthroughdate", DateAndTime.DateAdd("d", -1, RERate.Get_InterestStartDate(1)));
				strBookPage = modGlobalFunctions.GetCurrentBookPageString(FCConvert.ToInt32(ListGrid.TextMatrix(intRow, 1)));
				while (strBookPage.Length >= 255)
				{
					y = Strings.InStrRev(strBookPage, " ", -1, CompareConstants.vbTextCompare);
					if (y < 1)
					{
						strBookPage = Strings.Left(strBookPage, 254);
					}
					else
					{
						strBookPage = Strings.Trim(Strings.Left(strBookPage, y));
					}
				}
				clsBill.Set_Fields("bookpage", Strings.Trim(strBookPage));
				clsBill.Update();
				MakeREBill = true;
				return MakeREBill;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Make RE Bill", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeREBill;
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private bool MakePPBill(ref int intRow)
		{
			bool MakePPBill = false;
			clsDRWrapper clsPP = new clsDRWrapper();
			clsDRWrapper clsBill = new clsDRWrapper();
			int lngAccount;
			int intBillNumber = 0;
			double dblTaxperPeriod = 0;
			// the next 5 variable are for calc. the tax per period
			int intNumPeriods = 0;
			double dblOrigTaxVal = 0;
			double[] dblTax = new double[4 + 1];
			double dblTemp = 0;
			int x;
			cParty tParty;
			// vbPorter upgrade warning: tAddress As cPartyAddress	OnWrite(Collection, cPartyAddress)
			cPartyAddress tAddress;
			try
			{
				// On Error GoTo ErrorHandler
				MakePPBill = false;
				if (PPRate.RateKey != FCConvert.ToDouble(ListGrid.TextMatrix(intRow, 4)))
				{
					PPRate.LoadRate(FCConvert.ToInt32(ListGrid.TextMatrix(intRow, 4)));
				}
				lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(ListGrid.TextMatrix(intRow, 1))));
				clsBill.OpenRecordset("select max(billingyear) as maxbill from billingmaster where billingtype = 'PP' and account = " + FCConvert.ToString(Conversion.Val(ListGrid.TextMatrix(intRow, 1))) + " and billingyear between " + FCConvert.ToString(PPRate.TaxYear * 10) + " and " + FCConvert.ToString(PPRate.TaxYear * 10 + 9), "twcl0000.vb1");
				intBillNumber = 2;
				// don't want any supplementals as bill 1
				if (!clsBill.EndOfFile())
				{
					// TODO Get_Fields: Field [maxbill] not found!! (maybe it is an alias?)
					if (!fecherFoundation.FCUtils.IsNull(clsBill.Get_Fields("maxbill")))
					{
						// TODO Get_Fields: Field [maxbill] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBill.Get_Fields("maxbill")) > 0)
						{
							// find out the max and then add 1 to it
							// if there are already 9 then warn the user that this one can't be added
							// TODO Get_Fields: Field [maxbill] not found!! (maybe it is an alias?)
							intBillNumber = clsBill.Get_Fields("maxbill") - (PPRate.TaxYear * 10);
							if (intBillNumber > 8)
							{
								MessageBox.Show("This will make too many supplemental bills for Personal Property account " + FCConvert.ToString(Conversion.Val(ListGrid.TextMatrix(intRow, 1))) + "\r\n" + "This bill will not be processed and will be removed from the list.", "Unable to Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								// .RemoveItem (intRow)
								return MakePPBill;
							}
							intBillNumber += 1;
						}
						else
						{
							intBillNumber = 2;
						}
					}
					else
					{
						intBillNumber = 2;
					}
				}
				else
				{
					intBillNumber = 2;
				}
				// .TextMatrix(intRow, 6) = intBillNumber    'save for the processing part
				// make sure we don't return a record
				// do this because it's easier than an insert
				clsBill.OpenRecordset("select * from billingmaster where account = 0 and id = -1", "twcl0000.vb1");
				clsBill.AddNew();
				clsBill.Update();
				ListGrid.TextMatrix(intRow, 6, FCConvert.ToString(clsBill.Get_Fields_Int32("ID")));
				clsPP.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey))where ppmaster.account = " + ListGrid.TextMatrix(intRow, 1), "twpp0000.vb1");
				if (clsPP.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Account " + ListGrid.TextMatrix(intRow, 1) + " must be calculated. There is no entry in the valuations table for it.", "Calculate Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return MakePPBill;
				}
				// now fill in the bill information
				clsBill.Set_Fields("account", ListGrid.TextMatrix(intRow, 1));
				clsBill.Set_Fields("billingtype", "PP");
				clsBill.Set_Fields("WhetherBilledBefore", "A");
				clsBill.Set_Fields("BillingYear", PPRate.TaxYear * 10 + intBillNumber);
				tParty = pCont.GetParty(FCConvert.ToInt32(Conversion.Val(clsPP.Get_Fields_Int32("partyid"))));
				if (tParty == null)
				{
					tParty = new cParty();
				}
				if (tParty.Addresses.Count > 0)
				{
					tAddress = tParty.Addresses[1];
				}
				else
				{
					tAddress = new cPartyAddress();
				}
				clsBill.Set_Fields("name1", tParty.FullNameLastFirst);
				clsBill.Set_Fields("address1", tAddress.Address1);
				clsBill.Set_Fields("address2", tAddress.Address2);
				clsBill.Set_Fields("address3", Strings.Trim(tAddress.City) + "  " + Strings.Trim(tAddress.State) + " " + Strings.Trim(tAddress.Zip));
				clsBill.Set_Fields("MailingAddress3", tAddress.Address3);
				// If Trim(clsPP.Fields("zip4")) <> vbNullString Then
				// clsBill.Fields("address3") = clsBill.Fields("address3") & "-" & Trim(clsPP.Fields("zip4"))
				// End If  'if trim zip4
				if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					clsBill.Set_Fields("trancode", modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
				}
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				clsBill.Set_Fields("streetnumber", clsPP.Get_Fields("streetnumber"));
				clsBill.Set_Fields("apt", "");
				clsBill.Set_Fields("streetname", Strings.Trim(FCConvert.ToString(clsPP.Get_Fields_String("street"))));
				// TODO Get_Fields: Field [[value]] not found!! (maybe it is an alias?)
				clsBill.Set_Fields("ppassessment", Conversion.Val(clsPP.Get_Fields("[value]")) + Conversion.Val(clsPP.Get_Fields_Int32("cat1exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat2exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat3exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat4exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat5exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat6exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat7exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat8exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat9exempt")));
				clsBill.Set_Fields("exemptvalue", Conversion.Val(clsPP.Get_Fields_Int32("exemption")) + Conversion.Val(clsPP.Get_Fields_Int32("cat1exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat2exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat3exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat4exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat5exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat6exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat7exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat8exempt")) + Conversion.Val(clsPP.Get_Fields_Int32("cat9exempt")));
				// clsBill.Fields("PPAssessment") = Val(clsPP.Fields("[value]"))
				// clsBill.Fields("exemptvalue") = Val(clsPP.Fields("EXEMPTION"))
				clsBill.Set_Fields("interestappliedthroughdate", DateAndTime.DateAdd("d", -1, PPRate.Get_InterestStartDate(1)));
				for (x = 1; x <= 4; x++)
				{
					dblTax[x] = 0;
				}
				// x
				intNumPeriods = PPRate.NumberOfPeriods;
				dblOrigTaxVal = FCConvert.ToDouble(ListGrid.TextMatrix(intRow, 2));
				dblTaxperPeriod = dblOrigTaxVal / intNumPeriods;
				dblTaxperPeriod = modMain.Round(dblTaxperPeriod, 2);
				modGlobalFunctions.AddCYAEntry_728("CL", "Added PP Supplemental Bill", "Account " + ListGrid.TextMatrix(intRow, 1), "Bill key " + clsBill.Get_Fields_Int32("ID"), "Rate Key " + FCConvert.ToString(PPRate.RateKey), "Amount " + FCConvert.ToString(dblOrigTaxVal));
				bool boolAllEqual = false;
				int y;
				double dblLeftOver = 0;
				if (dblOrigTaxVal >= 100 || intNumPeriods < 4 || modGlobalVariables.Statics.CustomizedInfo.SplitSmallAmounts)
				{
					boolAllEqual = true;
					for (y = 1; y <= intNumPeriods; y++)
					{
						if (RERate.Get_BillWeight(y) != RERate.Get_BillWeight(1))
						{
							boolAllEqual = false;
						}
					}
					// y
					if (boolAllEqual)
					{
						for (y = 1; y <= intNumPeriods; y++)
						{
							dblTax[y] = modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(dblTaxperPeriod)), 2);
						}
						// y
						dblLeftOver = modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(dblOrigTaxVal - (dblTaxperPeriod * intNumPeriods))), 2);
						if (dblLeftOver < 0)
						{
							// make the last bill the smaller
							dblTax[intNumPeriods] += dblLeftOver;
						}
						else
						{
							// make the first bill the largest
							dblTax[1] += dblLeftOver;
						}
						// if dblleftover
					}
					else
					{
						dblTemp = 0;
						dblLeftOver = dblOrigTaxVal;
						for (y = 1; y <= intNumPeriods - 1; y++)
						{
							if (dblLeftOver > 0)
							{
								dblTemp = modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(RERate.Get_BillWeight(y) * dblOrigTaxVal)), 2);
								if (dblTemp <= dblLeftOver)
								{
									dblTax[y] = dblTemp;
									dblLeftOver -= dblTemp;
								}
								else
								{
									dblTax[y] = dblLeftOver;
									dblLeftOver = 0;
								}
							}
						}
						// y
						if (dblLeftOver > 0)
						{
							dblTax[intNumPeriods] = dblLeftOver;
						}
					}
				}
				else
				{
					dblTax[1] = dblOrigTaxVal;
					dblTax[2] = 0;
					dblTax[3] = 0;
					dblTax[4] = 0;
				}

				clsBill.Set_Fields("taxdue1", dblTax[1]);
				clsBill.Set_Fields("taxdue2", dblTax[2]);
				clsBill.Set_Fields("taxdue3", dblTax[3]);
				clsBill.Set_Fields("taxdue4", dblTax[4]);
				clsBill.Set_Fields("ratekey", PPRate.RateKey);
				clsBill.Set_Fields("transferfrombillingdatefirst", DateTime.Today);
				clsBill.Set_Fields("category1", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category1"))));
				clsBill.Set_Fields("category2", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category2"))));
				clsBill.Set_Fields("category3", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category3"))));
				clsBill.Set_Fields("category4", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category4"))));
				clsBill.Set_Fields("category5", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category5"))));
				clsBill.Set_Fields("category6", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category6"))));
				clsBill.Set_Fields("category7", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category7"))));
				clsBill.Set_Fields("category8", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category8"))));
				clsBill.Set_Fields("category9", FCConvert.ToString(Conversion.Val(clsPP.Get_Fields_Int32("category9"))));
				clsBill.Update();
				MakePPBill = true;
				return MakePPBill;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Make PP Bill", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakePPBill;
		}
		// vbPorter upgrade warning: intRange As short	OnWriteFCConvert.ToInt32(
		public void TransferPP(int intRange, string strBegin = "", string strEnd = "")
		{
			clsDRWrapper clsPP = new clsDRWrapper();
			string strWhere = "";
			double dblTaxAmount = 0;
			switch (intRange)
			{
				case -1:
					{
						// all
						strWhere = "";
						break;
					}
				case 0:
					{
						// account
						strWhere = " and account between " + strBegin + " and " + strEnd;
						break;
					}
				case 1:
					{
						// name
						strWhere = " and name between '" + strBegin + "' and '" + strEnd + "'";
						break;
					}
				case 2:
					{
						// location
						strWhere = " and street between '" + strBegin + "' and '" + strEnd + "'";
						break;
					}
				case 3:
					{
						// open1
						strWhere = " and open1 between '" + strBegin + "' and '" + strEnd + "'";
						break;
					}
				case 4:
					{
						// open2
						strWhere = " and open2 between '" + strBegin + "' and '" + strEnd + "'";
						break;
					}
			}
			//end switch
			if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
			{
				strWhere += " and trancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown) + " ";
			}
			clsPP.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey)) where (ppmaster.value - ppmaster.exemption > 0) and ppmaster.deleted <> 1 and ppmaster.account > 0 " + strWhere + " order by ppmaster.account", "twpp0000.vb1");
			while (!clsPP.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				dblTaxAmount = Conversion.Val(clsPP.Get_Fields("value")) - Conversion.Val(clsPP.Get_Fields_Int32("exemption"));
				if (dblTaxAmount < 0)
					dblTaxAmount = 0;
				dblTaxAmount *= PPRate.TaxRate;
				dblTaxAmount = modMain.Round(dblTaxAmount, 2);
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				ListGrid.AddItem("PP" + "\t" + clsPP.Get_Fields("account") + "\t" + Strings.Format(dblTaxAmount, "#,###,###,##0.00") + "\t" + "No" + "\t" + FCConvert.ToString(PPRate.RateKey));
				ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXYEAR, FCConvert.ToString(PPRate.TaxYear));
				ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXACQUIRED, FCConvert.ToString(false));
				clsPP.MoveNext();
			}
		}
		// vbPorter upgrade warning: intRange As short	OnWriteFCConvert.ToInt32(
		public void TransferRE(int intRange, string strBegin = "", string strEnd = "")
		{
			clsDRWrapper clsre = new clsDRWrapper();
			string strWhere = "";
			double dblTaxAmount = 0;
			string strTemp = "";
			string[] strAry = null;
			int x;
			switch (intRange)
			{
				case -1:
					{
						// all
						strWhere = "";
						break;
					}
				case 0:
					{
						// account
						if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
						{
							strWhere = " where rsaccount between " + strBegin + " and " + strEnd;
						}
						else
						{
							strTemp = " where (";
							strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								strTemp += "(rsaccount = " + strAry[x] + ") or";
							}
							// x
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
							// get rid of last or
							strTemp += ") ";
							strWhere = strTemp;
						}
						break;
					}
				case 1:
					{
						// name
						strWhere = " where FullNameLF between '" + strBegin + "' and '" + strEnd + "'";
						break;
					}
				case 2:
					{
						// maplot
						if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
						{
							strWhere = " where rsmaplot between '" + strBegin + "' and '" + strEnd + "'";
						}
						else
						{
							strTemp = " where (";
							strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								strTemp += "(rsmaplot = '" + strAry[x] + "') or";
							}
							// x
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
							// get rid of last or
							strTemp += ") ";
							strWhere = strTemp;
						}
						break;
					}
				case 3:
					{
						// location
						if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
						{
							strWhere = " where rslocstreet between '" + strBegin + "' and '" + strEnd + "'";
						}
						else
						{
							strTemp = " where (";
							strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								strTemp += "(rslocstreet = '" + strAry[x] + "') or";
							}
							// x
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
							// get rid of last or
							strTemp += ") ";
							strWhere = strTemp;
						}
						break;
					}
				case 4:
					{
						// tran code
						if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
						{
							strWhere = " where ritrancode between " + strBegin + " and " + strEnd;
						}
						else
						{
							strTemp = " where (";
							strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								strTemp += "(ritrancode = " + strAry[x] + ") or";
							}
							// x
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
							// get rid of last or
							strTemp += ") ";
							strWhere = strTemp;
						}
						break;
					}
				case 5:
					{
						// land code
						if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
						{
							strWhere = " where rilandcode between " + strBegin + " and " + strEnd;
						}
						else
						{
							strTemp = " where (";
							strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								strTemp += "(rilandcode = " + strAry[x] + ") or";
							}
							// x
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
							// get rid of last or
							strTemp += ") ";
							strWhere = strTemp;
						}
						break;
					}
				case 6:
					{
						// bldg code
						if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
						{
							strWhere = " where ribldgcode between " + strBegin + " and " + strEnd;
						}
						else
						{
							strTemp = " where (";
							strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								strTemp += "(ribldgcode = " + strAry[x] + ") or";
							}
							// x
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
							// get rid of last or
							strTemp += ") ";
							strWhere = strTemp;
						}
						break;
					}
			}
			//end switch
			if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
			{
				strWhere += " and ritrancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown) + " ";
			}
            clsre.OpenRecordset("select * from (select rsaccount,sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum, sum(rlexemption) as exemptsum from master where rsdeleted <> 1 and rsaccount > 0 group by rsaccount having (sum(lastlandval) + sum(lastbldgval) - sum(rlexemption) > 0) order by rsaccount) as tbl1 inner join (select rsaccount,deedname1,deedname2,taxacquired from master where rsdeleted <> 1 and rscard = 1) as tbl2 on (tbl1.rsaccount = tbl2.rsaccount) CROSS APPLY " + clsre.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(OwnerPartyID) as p " + strWhere, "twre0000.vb1");
            while (!clsre.EndOfFile())
			{
				// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				dblTaxAmount = clsre.Get_Fields("landsum") + clsre.Get_Fields("bldgsum") - clsre.Get_Fields("exemptsum");
				if (dblTaxAmount > 0)
				{
					dblTaxAmount *= RERate.TaxRate;
					dblTaxAmount = modMain.Round(dblTaxAmount, 2);
				}
				else
				{
					dblTaxAmount = 0;
				}
				// Now save the info in the list
				// TODO Get_Fields: Field [tbl1.rsaccount] not found!! (maybe it is an alias?)
                ListGrid.AddItem("RE" + "\t" + clsre.Get_Fields("tbl1.rsaccount") + "\t" +
                                 Strings.Format(dblTaxAmount, "#,###,###,##0.00") + "\t" + "No" + "\t" +
                                 FCConvert.ToString(RERate.RateKey) + "\t" + clsre.Get_Fields_String("DeedName1"));
				ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXYEAR, FCConvert.ToString(RERate.TaxYear));
				ListGrid.TextMatrix(ListGrid.Rows - 1, CNSTLISTGRIDCOLTAXACQUIRED, FCConvert.ToString(clsre.Get_Fields_Boolean("taxacquired")));
				clsre.MoveNext();
			}
		}

		public void mnuTranPP_Click(object sender, System.EventArgs e)
		{
			if (PPRate.RateKey == 0)
			{
				//! Load frmAuditInfo;
				frmAuditInfo.InstancePtr.Init(5, 6);
				frmAuditInfo.InstancePtr.Show(App.MainForm);
				return;
			}
			//! Load frmPPRange;
			frmPPRange.InstancePtr.boolFromSupplemental = true;
			frmPPRange.InstancePtr.Show(App.MainForm);
		}

		public void mnuTranPP_Click()
		{
			mnuTranPP_Click(mnuTranPP, new System.EventArgs());
		}

		public void mnuTranRE_Click(object sender, System.EventArgs e)
		{
			if (RERate.RateKey == 0)
			{
				//! Load frmAuditInfo;
				frmAuditInfo.InstancePtr.Init(5, 5);
				frmAuditInfo.InstancePtr.Show(App.MainForm);
				return;
			}
			//! Load frmRERange;
			frmRERange.InstancePtr.boolFromSupplemental = true;
			frmRERange.InstancePtr.Show(App.MainForm);
		}

		public void mnuTranRE_Click()
		{
			mnuTranRE_Click(mnuTranRE, new System.EventArgs());
		}
	}
}
