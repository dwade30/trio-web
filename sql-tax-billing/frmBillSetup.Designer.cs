﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmBillSetup.
	/// </summary>
	partial class frmBillSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrderBills;
		public fecherFoundation.FCLabel lblOrderBills;
		public fecherFoundation.FCComboBox cmbDiscount;
		public fecherFoundation.FCLabel lblDiscount;
		public fecherFoundation.FCComboBox cmbBillType;
		public fecherFoundation.FCLabel lblBillType;
		public fecherFoundation.FCComboBox cmbReturnAddress;
		public fecherFoundation.FCLabel lblReturnAddress;
		public fecherFoundation.FCComboBox cmbBillsToPrint;
		public fecherFoundation.FCLabel lblBillsToPrint;
		public fecherFoundation.FCComboBox cmbDistribution;
		public fecherFoundation.FCLabel lblDistribution;
		public fecherFoundation.FCCheckBox chkEliminateZeroBalance;
		public fecherFoundation.FCCheckBox chkMultiRecipients;
		public fecherFoundation.FCCheckBox chkNewOwner;
		public fecherFoundation.FCCheckBox chkMortgage;
		public fecherFoundation.FCFrame Frame8;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame FramAccountList;
		public FCGrid AccountsGrid;
		public FCGrid PrintGrid;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCCheckBox chkOwner;
		public Global.T2KDateBox T2KOwnerDate;
		public fecherFoundation.FCFrame framAdjustment;
		public fecherFoundation.FCCheckBox chkTransparentBoxes;
		public fecherFoundation.FCTextBox txtAdjustment;
		public fecherFoundation.FCCheckBox chkAdjustment;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame framDotMatrixAdjust;
		public fecherFoundation.FCCheckBox chkDotMatrixAdjust;
		public fecherFoundation.FCLabel lblDescription;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintLabels;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillSetup));
			this.cmbOrderBills = new fecherFoundation.FCComboBox();
			this.lblOrderBills = new fecherFoundation.FCLabel();
			this.cmbDiscount = new fecherFoundation.FCComboBox();
			this.lblDiscount = new fecherFoundation.FCLabel();
			this.cmbBillType = new fecherFoundation.FCComboBox();
			this.lblBillType = new fecherFoundation.FCLabel();
			this.cmbReturnAddress = new fecherFoundation.FCComboBox();
			this.lblReturnAddress = new fecherFoundation.FCLabel();
			this.cmbBillsToPrint = new fecherFoundation.FCComboBox();
			this.lblBillsToPrint = new fecherFoundation.FCLabel();
			this.cmbDistribution = new fecherFoundation.FCComboBox();
			this.lblDistribution = new fecherFoundation.FCLabel();
			this.chkEliminateZeroBalance = new fecherFoundation.FCCheckBox();
			this.chkMultiRecipients = new fecherFoundation.FCCheckBox();
			this.chkNewOwner = new fecherFoundation.FCCheckBox();
			this.chkMortgage = new fecherFoundation.FCCheckBox();
			this.Frame8 = new fecherFoundation.FCFrame();
			this.txtYear = new fecherFoundation.FCTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.FramAccountList = new fecherFoundation.FCFrame();
			this.AccountsGrid = new fecherFoundation.FCGrid();
			this.PrintGrid = new fecherFoundation.FCGrid();
			this.Frame9 = new fecherFoundation.FCFrame();
			this.chkOwner = new fecherFoundation.FCCheckBox();
			this.T2KOwnerDate = new Global.T2KDateBox();
			this.framAdjustment = new fecherFoundation.FCFrame();
			this.chkTransparentBoxes = new fecherFoundation.FCCheckBox();
			this.txtAdjustment = new fecherFoundation.FCTextBox();
			this.chkAdjustment = new fecherFoundation.FCCheckBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.framDotMatrixAdjust = new fecherFoundation.FCFrame();
			this.chkDotMatrixAdjust = new fecherFoundation.FCCheckBox();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.mnuPrintLabels = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSaveAndContinue = new fecherFoundation.FCButton();
			this.cmdPrintLabels = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEliminateZeroBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMultiRecipients)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNewOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMortgage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame8)).BeginInit();
			this.Frame8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.FramAccountList)).BeginInit();
			this.FramAccountList.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.AccountsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PrintGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
			this.Frame9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KOwnerDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framAdjustment)).BeginInit();
			this.framAdjustment.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkTransparentBoxes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framDotMatrixAdjust)).BeginInit();
			this.framDotMatrixAdjust.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDotMatrixAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabels)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAndContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framDotMatrixAdjust);
			this.ClientArea.Controls.Add(this.chkMortgage);
			this.ClientArea.Controls.Add(this.chkNewOwner);
			this.ClientArea.Controls.Add(this.chkMultiRecipients);
			this.ClientArea.Controls.Add(this.chkEliminateZeroBalance);
			this.ClientArea.Controls.Add(this.lblDistribution);
			this.ClientArea.Controls.Add(this.Frame8);
			this.ClientArea.Controls.Add(this.cmbOrderBills);
			this.ClientArea.Controls.Add(this.lblOrderBills);
			this.ClientArea.Controls.Add(this.cmbDiscount);
			this.ClientArea.Controls.Add(this.lblDiscount);
			this.ClientArea.Controls.Add(this.cmbBillType);
			this.ClientArea.Controls.Add(this.lblBillType);
			this.ClientArea.Controls.Add(this.cmbReturnAddress);
			this.ClientArea.Controls.Add(this.lblReturnAddress);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.cmbDistribution);
			this.ClientArea.Controls.Add(this.Frame9);
			this.ClientArea.Controls.Add(this.framAdjustment);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrintLabels);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintLabels, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(163, 30);
			this.HeaderText.Text = "Billing Format";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbOrderBills
			// 
			this.cmbOrderBills.AutoSize = false;
			this.cmbOrderBills.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOrderBills.FormattingEnabled = true;
			this.cmbOrderBills.Items.AddRange(new object[] {
				"Name",
				"Zip Code",
				"Account"
			});
			this.cmbOrderBills.Location = new System.Drawing.Point(226, 320);
			this.cmbOrderBills.Name = "cmbOrderBills";
			this.cmbOrderBills.Size = new System.Drawing.Size(234, 40);
			this.cmbOrderBills.TabIndex = 7;
			this.cmbOrderBills.Text = "Name";
			this.ToolTip1.SetToolTip(this.cmbOrderBills, null);
			// 
			// lblOrderBills
			// 
			this.lblOrderBills.AutoSize = true;
			this.lblOrderBills.Location = new System.Drawing.Point(30, 334);
			this.lblOrderBills.Name = "lblOrderBills";
			this.lblOrderBills.Size = new System.Drawing.Size(109, 15);
			this.lblOrderBills.TabIndex = 6;
			this.lblOrderBills.Text = "ORDER BILLS BY";
			this.ToolTip1.SetToolTip(this.lblOrderBills, null);
			// 
			// cmbDiscount
			// 
			this.cmbDiscount.AutoSize = false;
			this.cmbDiscount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDiscount.FormattingEnabled = true;
			this.cmbDiscount.Items.AddRange(new object[] {
				"Don\'t Show Discount",
				"Show Discount"
			});
			this.cmbDiscount.Location = new System.Drawing.Point(226, 260);
			this.cmbDiscount.Name = "cmbDiscount";
			this.cmbDiscount.Size = new System.Drawing.Size(236, 40);
			this.cmbDiscount.TabIndex = 11;
			this.cmbDiscount.Text = "Don\'t Show Discount";
			this.ToolTip1.SetToolTip(this.cmbDiscount, null);
			// 
			// lblDiscount
			// 
			this.lblDiscount.AutoSize = true;
			this.lblDiscount.Location = new System.Drawing.Point(30, 274);
			this.lblDiscount.Name = "lblDiscount";
			this.lblDiscount.Size = new System.Drawing.Size(72, 15);
			this.lblDiscount.TabIndex = 10;
			this.lblDiscount.Text = "DISCOUNT";
			this.lblDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.ToolTip1.SetToolTip(this.lblDiscount, null);
			// 
			// cmbBillType
			// 
			this.cmbBillType.AutoSize = false;
			this.cmbBillType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBillType.FormattingEnabled = true;
			this.cmbBillType.Items.AddRange(new object[] {
				"Real Estate   ",
				"Personal Property",
				"Real Estate Combined with Personal Property"
			});
			this.cmbBillType.Location = new System.Drawing.Point(226, 693);
			this.cmbBillType.Name = "cmbBillType";
			this.cmbBillType.Size = new System.Drawing.Size(386, 40);
			this.cmbBillType.TabIndex = 9;
			this.cmbBillType.Text = "Real Estate   ";
			this.ToolTip1.SetToolTip(this.cmbBillType, null);
			this.cmbBillType.SelectedIndexChanged += new System.EventHandler(this.cmbBillType_SelectedIndexChanged);
			// 
			// lblBillType
			// 
			this.lblBillType.AutoSize = true;
			this.lblBillType.Location = new System.Drawing.Point(30, 704);
			this.lblBillType.Name = "lblBillType";
			this.lblBillType.Size = new System.Drawing.Size(97, 15);
			this.lblBillType.TabIndex = 8;
			this.lblBillType.Text = "TYPE OF BILLS";
			this.ToolTip1.SetToolTip(this.lblBillType, null);
			// 
			// cmbReturnAddress
			// 
			this.cmbReturnAddress.AutoSize = false;
			this.cmbReturnAddress.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbReturnAddress.FormattingEnabled = true;
			this.cmbReturnAddress.Items.AddRange(new object[] {
				"Don\'t Show Return Address",
				"Show Return Address"
			});
			this.cmbReturnAddress.Location = new System.Drawing.Point(226, 200);
			this.cmbReturnAddress.Name = "cmbReturnAddress";
			this.cmbReturnAddress.Size = new System.Drawing.Size(307, 40);
			this.cmbReturnAddress.TabIndex = 5;
			this.cmbReturnAddress.Text = "Don\'t Show Return Address";
			this.ToolTip1.SetToolTip(this.cmbReturnAddress, null);
			// 
			// lblReturnAddress
			// 
			this.lblReturnAddress.AutoSize = true;
			this.lblReturnAddress.Location = new System.Drawing.Point(30, 214);
			this.lblReturnAddress.Name = "lblReturnAddress";
			this.lblReturnAddress.Size = new System.Drawing.Size(122, 15);
			this.lblReturnAddress.TabIndex = 4;
			this.lblReturnAddress.Text = "RETURN ADDRESS";
			this.ToolTip1.SetToolTip(this.lblReturnAddress, null);
			// 
			// cmbBillsToPrint
			// 
			this.cmbBillsToPrint.AutoSize = false;
			this.cmbBillsToPrint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBillsToPrint.FormattingEnabled = true;
			this.cmbBillsToPrint.Items.AddRange(new object[] {
				"All",
				"Individual",
				"Range"
			});
			this.cmbBillsToPrint.Location = new System.Drawing.Point(226, 6);
			this.cmbBillsToPrint.Name = "cmbBillsToPrint";
			this.cmbBillsToPrint.Size = new System.Drawing.Size(208, 40);
			this.cmbBillsToPrint.TabIndex = 1;
			this.cmbBillsToPrint.Text = "All";
			this.ToolTip1.SetToolTip(this.cmbBillsToPrint, null);
			this.cmbBillsToPrint.SelectedIndexChanged += new System.EventHandler(this.cmbBillsToPrint_SelectedIndexChanged);
			// 
			// lblBillsToPrint
			// 
			this.lblBillsToPrint.AutoSize = true;
			this.lblBillsToPrint.Location = new System.Drawing.Point(30, 20);
			this.lblBillsToPrint.Name = "lblBillsToPrint";
			this.lblBillsToPrint.Size = new System.Drawing.Size(102, 15);
			this.lblBillsToPrint.TabIndex = 0;
			this.lblBillsToPrint.Text = "BILLS TO PRINT";
			this.ToolTip1.SetToolTip(this.lblBillsToPrint, null);
			// 
			// cmbDistribution
			// 
			this.cmbDistribution.AutoSize = false;
			this.cmbDistribution.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDistribution.FormattingEnabled = true;
			this.cmbDistribution.Items.AddRange(new object[] {
				"Don\'t Show Breakdown",
				"Show Breakdown by Percentages",
				"Show Breakdown by Dollars and %"
			});
			this.cmbDistribution.Location = new System.Drawing.Point(226, 140);
			this.cmbDistribution.Name = "cmbDistribution";
			this.cmbDistribution.Size = new System.Drawing.Size(307, 40);
			this.cmbDistribution.TabIndex = 3;
			this.cmbDistribution.Text = "Don\'t Show Breakdown";
			this.ToolTip1.SetToolTip(this.cmbDistribution, null);
			// 
			// lblDistribution
			// 
			this.lblDistribution.AutoSize = true;
			this.lblDistribution.Location = new System.Drawing.Point(30, 154);
			this.lblDistribution.Name = "lblDistribution";
			this.lblDistribution.Size = new System.Drawing.Size(179, 15);
			this.lblDistribution.TabIndex = 2;
			this.lblDistribution.Text = "DISTRIBUTION BREAKDOWN";
			this.ToolTip1.SetToolTip(this.lblDistribution, null);
			// 
			// chkEliminateZeroBalance
			// 
			this.chkEliminateZeroBalance.Location = new System.Drawing.Point(30, 646);
			this.chkEliminateZeroBalance.Name = "chkEliminateZeroBalance";
			this.chkEliminateZeroBalance.Size = new System.Drawing.Size(366, 27);
			this.chkEliminateZeroBalance.TabIndex = 17;
			this.chkEliminateZeroBalance.Text = "Don\'t print bills with a negative or zero balance";
			this.ToolTip1.SetToolTip(this.chkEliminateZeroBalance, null);
			// 
			// chkMultiRecipients
			// 
			this.chkMultiRecipients.Location = new System.Drawing.Point(30, 474);
			this.chkMultiRecipients.Name = "chkMultiRecipients";
			this.chkMultiRecipients.Size = new System.Drawing.Size(288, 27);
			this.chkMultiRecipients.TabIndex = 41;
			this.chkMultiRecipients.Text = "Send a copy to all interested parties";
			this.ToolTip1.SetToolTip(this.chkMultiRecipients, "Print copies for each address set up in Billing as a bill recipient");
			// 
			// chkNewOwner
			// 
			this.chkNewOwner.Location = new System.Drawing.Point(30, 427);
			this.chkNewOwner.Name = "chkNewOwner";
			this.chkNewOwner.Size = new System.Drawing.Size(247, 27);
			this.chkNewOwner.TabIndex = 40;
			this.chkNewOwner.Text = "Send a copy to the new owner";
			this.ToolTip1.SetToolTip(this.chkNewOwner, null);
			// 
			// chkMortgage
			// 
			this.chkMortgage.Location = new System.Drawing.Point(30, 380);
			this.chkMortgage.Name = "chkMortgage";
			this.chkMortgage.Size = new System.Drawing.Size(301, 27);
			this.chkMortgage.TabIndex = 13;
			this.chkMortgage.Text = "Send a copy to each mortgage holder";
			this.ToolTip1.SetToolTip(this.chkMortgage, null);
			// 
			// Frame8
			// 
			this.Frame8.Controls.Add(this.txtYear);
			this.Frame8.Location = new System.Drawing.Point(30, 30);
			this.Frame8.Name = "Frame8";
			this.Frame8.Size = new System.Drawing.Size(124, 90);
			this.Frame8.TabIndex = 1;
			this.Frame8.Text = "Tax Year";
			this.ToolTip1.SetToolTip(this.Frame8, null);
			// 
			// txtYear
			// 
			this.txtYear.AutoSize = false;
			this.txtYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtYear.LinkItem = null;
			this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYear.LinkTopic = null;
			this.txtYear.Location = new System.Drawing.Point(20, 30);
			this.txtYear.MaxLength = 4;
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(83, 40);
			this.txtYear.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.txtYear, null);
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = " groupBoxLeftBorder";
			this.Frame2.Controls.Add(this.FramAccountList);
			this.Frame2.Controls.Add(this.cmbBillsToPrint);
			this.Frame2.Controls.Add(this.lblBillsToPrint);
			this.Frame2.Controls.Add(this.PrintGrid);
			this.Frame2.Location = new System.Drawing.Point(0, 753);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(808, 330);
			this.Frame2.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.Frame2, null);
			// 
			// FramAccountList
			// 
			this.FramAccountList.Controls.Add(this.AccountsGrid);
			this.FramAccountList.Location = new System.Drawing.Point(30, 66);
			this.FramAccountList.Name = "FramAccountList";
			this.FramAccountList.Size = new System.Drawing.Size(191, 256);
			this.FramAccountList.TabIndex = 2;
			this.FramAccountList.Text = "Accounts To Bill";
			this.ToolTip1.SetToolTip(this.FramAccountList, null);
			this.FramAccountList.Visible = false;
			// 
			// AccountsGrid
			// 
			this.AccountsGrid.AllowSelection = false;
			this.AccountsGrid.AllowUserToResizeColumns = false;
			this.AccountsGrid.AllowUserToResizeRows = false;
			this.AccountsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.AccountsGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.AccountsGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.AccountsGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.AccountsGrid.BackColorSel = System.Drawing.Color.Empty;
			this.AccountsGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.AccountsGrid.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.AccountsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.AccountsGrid.ColumnHeadersHeight = 30;
			this.AccountsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.AccountsGrid.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.AccountsGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.AccountsGrid.DragIcon = null;
			this.AccountsGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.AccountsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.AccountsGrid.ExtendLastCol = true;
			this.AccountsGrid.FixedCols = 0;
			this.AccountsGrid.FixedRows = 0;
			this.AccountsGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.AccountsGrid.FrozenCols = 0;
			this.AccountsGrid.GridColor = System.Drawing.Color.Empty;
			this.AccountsGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.AccountsGrid.Location = new System.Drawing.Point(20, 30);
			this.AccountsGrid.Name = "AccountsGrid";
			this.AccountsGrid.OutlineCol = 0;
			this.AccountsGrid.RowHeadersVisible = false;
			this.AccountsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.AccountsGrid.RowHeightMin = 0;
			this.AccountsGrid.Rows = 1;
			this.AccountsGrid.ScrollTipText = null;
			this.AccountsGrid.ShowColumnVisibilityMenu = false;
			this.AccountsGrid.ShowFocusCell = false;
			this.AccountsGrid.Size = new System.Drawing.Size(154, 205);
			this.AccountsGrid.StandardTab = true;
			this.AccountsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.AccountsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.AccountsGrid.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.AccountsGrid, null);
			this.AccountsGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.AccountsGrid_ValidateEdit);
			this.AccountsGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.AccountsGrid_KeyDownEvent);
			// 
			// PrintGrid
			// 
			this.PrintGrid.AllowSelection = false;
			this.PrintGrid.AllowUserToResizeColumns = false;
			this.PrintGrid.AllowUserToResizeRows = false;
			this.PrintGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.PrintGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.PrintGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.PrintGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.PrintGrid.BackColorSel = System.Drawing.Color.Empty;
			this.PrintGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.PrintGrid.Cols = 3;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.PrintGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.PrintGrid.ColumnHeadersHeight = 30;
			this.PrintGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.PrintGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.PrintGrid.DragIcon = null;
			this.PrintGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.PrintGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.PrintGrid.ExtendLastCol = true;
			this.PrintGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.PrintGrid.FrozenCols = 0;
			this.PrintGrid.GridColor = System.Drawing.Color.Empty;
			this.PrintGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.PrintGrid.Location = new System.Drawing.Point(30, 66);
			this.PrintGrid.Name = "PrintGrid";
			this.PrintGrid.OutlineCol = 0;
			this.PrintGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.PrintGrid.RowHeightMin = 0;
			this.PrintGrid.Rows = 8;
			this.PrintGrid.ScrollTipText = null;
			this.PrintGrid.ShowColumnVisibilityMenu = false;
			this.PrintGrid.ShowFocusCell = false;
			this.PrintGrid.Size = new System.Drawing.Size(565, 256);
			this.PrintGrid.StandardTab = true;
			this.PrintGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.PrintGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.PrintGrid.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.PrintGrid, null);
			this.PrintGrid.Visible = false;
			this.PrintGrid.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.PrintGrid_BeforeRowColChange);
			this.PrintGrid.CurrentCellChanged += new System.EventHandler(this.PrintGrid_RowColChange);
			// 
			// Frame9
			// 
			this.Frame9.Controls.Add(this.chkOwner);
			this.Frame9.Controls.Add(this.T2KOwnerDate);
			this.Frame9.Location = new System.Drawing.Point(626, 315);
			this.Frame9.Name = "Frame9";
			this.Frame9.Size = new System.Drawing.Size(342, 84);
			this.Frame9.TabIndex = 12;
			this.Frame9.Text = "Owner Information";
			this.ToolTip1.SetToolTip(this.Frame9, null);
			this.Frame9.Visible = false;
			// 
			// chkOwner
			// 
			this.chkOwner.Location = new System.Drawing.Point(20, 30);
			this.chkOwner.Name = "chkOwner";
			this.chkOwner.Size = new System.Drawing.Size(145, 27);
			this.chkOwner.TabIndex = 0;
			this.chkOwner.Text = "Use owner as of";
			this.ToolTip1.SetToolTip(this.chkOwner, null);
			// 
			// T2KOwnerDate
			// 
			this.T2KOwnerDate.Location = new System.Drawing.Point(202, 30);
			this.T2KOwnerDate.Mask = "##/##/####";
			this.T2KOwnerDate.MaxLength = 10;
			this.T2KOwnerDate.Name = "T2KOwnerDate";
			this.T2KOwnerDate.Size = new System.Drawing.Size(115, 40);
			this.T2KOwnerDate.TabIndex = 1;
			this.T2KOwnerDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.T2KOwnerDate, null);
			// 
			// framAdjustment
			// 
			this.framAdjustment.AppearanceKey = "groupBoxNoBorders";
			this.framAdjustment.Controls.Add(this.chkTransparentBoxes);
			this.framAdjustment.Controls.Add(this.txtAdjustment);
			this.framAdjustment.Controls.Add(this.chkAdjustment);
			this.framAdjustment.Controls.Add(this.Label1);
			this.framAdjustment.Location = new System.Drawing.Point(10, 501);
			this.framAdjustment.Name = "framAdjustment";
			this.framAdjustment.Size = new System.Drawing.Size(342, 145);
			this.framAdjustment.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.framAdjustment, null);
			// 
			// chkTransparentBoxes
			// 
			this.chkTransparentBoxes.Location = new System.Drawing.Point(20, 98);
			this.chkTransparentBoxes.Name = "chkTransparentBoxes";
			this.chkTransparentBoxes.Size = new System.Drawing.Size(166, 27);
			this.chkTransparentBoxes.TabIndex = 3;
			this.chkTransparentBoxes.Text = "Transparent Boxes";
			this.ToolTip1.SetToolTip(this.chkTransparentBoxes, null);
			this.chkTransparentBoxes.CheckedChanged += new System.EventHandler(this.chkTransparentBoxes_CheckedChanged);
			// 
			// txtAdjustment
			// 
			this.txtAdjustment.AutoSize = false;
			this.txtAdjustment.BackColor = System.Drawing.SystemColors.Window;
			this.txtAdjustment.LinkItem = null;
			this.txtAdjustment.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAdjustment.LinkTopic = null;
			this.txtAdjustment.Location = new System.Drawing.Point(269, 54);
			this.txtAdjustment.Name = "txtAdjustment";
			this.txtAdjustment.Size = new System.Drawing.Size(73, 40);
			this.txtAdjustment.TabIndex = 2;
			this.txtAdjustment.Text = "0";
			this.ToolTip1.SetToolTip(this.txtAdjustment, null);
			this.txtAdjustment.TextChanged += new System.EventHandler(this.txtAdjustment_TextChanged);
			// 
			// chkAdjustment
			// 
			this.chkAdjustment.Location = new System.Drawing.Point(20, 20);
			this.chkAdjustment.Name = "chkAdjustment";
			this.chkAdjustment.Size = new System.Drawing.Size(189, 27);
			this.chkAdjustment.TabIndex = 0;
			this.chkAdjustment.Text = "Use Laser Adjustment";
			this.ToolTip1.SetToolTip(this.chkAdjustment, null);
			this.chkAdjustment.CheckedChanged += new System.EventHandler(this.chkAdjustment_CheckedChanged);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 68);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(200, 20);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "ADJUSTMENT IN LINES. EX. 1,-1,1.5";
			this.ToolTip1.SetToolTip(this.Label1, null);
			this.Label1.Click += new System.EventHandler(this.Label1_Click);
			// 
			// framDotMatrixAdjust
			// 
			this.framDotMatrixAdjust.AppearanceKey = "groupBoxNoBorders";
			this.framDotMatrixAdjust.Controls.Add(this.chkDotMatrixAdjust);
			this.framDotMatrixAdjust.Location = new System.Drawing.Point(10, 594);
			this.framDotMatrixAdjust.Name = "framDotMatrixAdjust";
			this.framDotMatrixAdjust.Size = new System.Drawing.Size(342, 47);
			this.framDotMatrixAdjust.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.framDotMatrixAdjust, null);
			this.framDotMatrixAdjust.Visible = false;
			this.framDotMatrixAdjust.Enter += new System.EventHandler(this.framDotMatrixAdjust_Enter);
			// 
			// chkDotMatrixAdjust
			// 
			this.chkDotMatrixAdjust.Location = new System.Drawing.Point(20, 5);
			this.chkDotMatrixAdjust.Name = "chkDotMatrixAdjust";
			this.chkDotMatrixAdjust.Size = new System.Drawing.Size(215, 27);
			this.chkDotMatrixAdjust.TabIndex = 0;
			this.chkDotMatrixAdjust.Text = "Printer has default margin";
			this.ToolTip1.SetToolTip(this.chkDotMatrixAdjust, null);
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(30, 32);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(953, 37);
			this.lblDescription.TabIndex = 0;
			this.lblDescription.Text = "BILLS WILL BE IN FORMAT 1, WILL SHOW THE DISTRIBUTION BREAKDOWN BY DOLLARS, WILL " + "SHOW THE RETURN ADDRESS AND WILL COMBINE PERSONAL PROPERTY AND REAL ESTATE BILLS" + ".  ALL BILLS WILL BE PRINTED";
			this.ToolTip1.SetToolTip(this.lblDescription, null);
			this.lblDescription.Visible = false;
			// 
			// mnuPrintLabels
			// 
			this.mnuPrintLabels.Index = 0;
			this.mnuPrintLabels.Name = "mnuPrintLabels";
			this.mnuPrintLabels.Text = "Print Labels";
			this.mnuPrintLabels.Click += new System.EventHandler(this.mnuPrintLabels_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrintLabels,
				this.mnuSepar2,
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 2;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Save && Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 3;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveAndContinue
			// 
			this.cmdSaveAndContinue.AppearanceKey = "acceptButton";
			this.cmdSaveAndContinue.Location = new System.Drawing.Point(514, 30);
			this.cmdSaveAndContinue.Name = "cmdSaveAndContinue";
			this.cmdSaveAndContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAndContinue.Size = new System.Drawing.Size(184, 48);
			this.cmdSaveAndContinue.TabIndex = 0;
			this.cmdSaveAndContinue.Text = "Save & Continue";
			this.ToolTip1.SetToolTip(this.cmdSaveAndContinue, null);
			this.cmdSaveAndContinue.Click += new System.EventHandler(this.cmdSaveAndContinue_Click);
			// 
			// cmdPrintLabels
			// 
			this.cmdPrintLabels.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintLabels.AppearanceKey = "toolbarButton";
			this.cmdPrintLabels.Location = new System.Drawing.Point(948, 29);
			this.cmdPrintLabels.Name = "cmdPrintLabels";
			this.cmdPrintLabels.Size = new System.Drawing.Size(93, 24);
			this.cmdPrintLabels.TabIndex = 1;
			this.cmdPrintLabels.Text = "Print Labels";
			this.ToolTip1.SetToolTip(this.cmdPrintLabels, null);
			this.cmdPrintLabels.Click += new System.EventHandler(this.mnuPrintLabels_Click);
			// 
			// frmBillSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmBillSetup";
			this.Text = "Billing Format";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmBillSetup_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBillSetup_KeyDown);
			this.Resize += new System.EventHandler(this.frmBillSetup_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEliminateZeroBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMultiRecipients)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNewOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMortgage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame8)).EndInit();
			this.Frame8.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.FramAccountList)).EndInit();
			this.FramAccountList.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.AccountsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PrintGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
			this.Frame9.ResumeLayout(false);
			this.Frame9.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KOwnerDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framAdjustment)).EndInit();
			this.framAdjustment.ResumeLayout(false);
			this.framAdjustment.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkTransparentBoxes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framDotMatrixAdjust)).EndInit();
			this.framDotMatrixAdjust.ResumeLayout(false);
			this.framDotMatrixAdjust.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDotMatrixAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabels)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndContinue;
		private FCButton cmdPrintLabels;
	}
}
