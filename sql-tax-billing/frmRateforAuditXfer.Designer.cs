﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmRateforAuditXfer.
	/// </summary>
	partial class frmRateforAuditXfer : BaseForm
	{
		public fecherFoundation.FCFrame Frame2;
		public FCGrid Grid;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuShow;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRateforAuditXfer));
			this.Frame2 = new fecherFoundation.FCFrame();
			this.Grid = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuShow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdShow = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdShow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdShow);
			this.BottomPanel.Location = new System.Drawing.Point(0, 541);
			this.BottomPanel.Size = new System.Drawing.Size(948, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(948, 531);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Size = new System.Drawing.Size(948, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(225, 30);
			this.HeaderText.Text = "Select Rate Record";
			// 
			// Frame2
			// 
			this.Frame2.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Frame2.Controls.Add(this.Grid);
			this.Frame2.Location = new System.Drawing.Point(30, 180);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(887, 340);
			this.Frame2.TabIndex = 3;
			this.Frame2.Text = "Select Rate Record";
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid.Cols = 9;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(20, 30);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.ShowFocusCell = false;
			this.Grid.Size = new System.Drawing.Size(847, 290);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 0;
			this.Grid.Click += new System.EventHandler(this.Grid_ClickEvent);
			this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cmbYear);
			this.Frame1.Location = new System.Drawing.Point(30, 70);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(142, 90);
			this.Frame1.TabIndex = 1;
			this.Frame1.Text = "Year";
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(20, 30);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(96, 40);
			this.cmbYear.TabIndex = 0;
			this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(484, 30);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "SELECT TAX YEAR AND THE APPROPRIATE RATE RECORD BELOW";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuNew,
				this.mnuSepar2,
				this.mnuShow,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 0;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuShow
			// 
			this.mnuShow.Index = 2;
			this.mnuShow.Name = "mnuShow";
			this.mnuShow.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuShow.Text = "Continue";
			this.mnuShow.Click += new System.EventHandler(this.mnuShow_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 3;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdShow
			// 
			this.cmdShow.AppearanceKey = "acceptButton";
			this.cmdShow.Location = new System.Drawing.Point(451, 30);
			this.cmdShow.Name = "cmdShow";
			this.cmdShow.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdShow.Size = new System.Drawing.Size(100, 48);
			this.cmdShow.TabIndex = 0;
			this.cmdShow.Text = "Continue";
			this.cmdShow.Click += new System.EventHandler(this.mnuShow_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(729, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(41, 24);
			this.cmdNew.TabIndex = 1;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// frmRateforAuditXfer
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(948, 699);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmRateforAuditXfer";
			this.Text = "Select Rate Record";
			this.Load += new System.EventHandler(this.frmRateforAuditXfer_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRateforAuditXfer_KeyDown);
			this.Resize += new System.EventHandler(this.frmRateforAuditXfer_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdShow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdShow;
		private FCButton cmdNew;
	}
}
