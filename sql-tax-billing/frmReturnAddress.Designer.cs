﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmReturnAddress.
	/// </summary>
	partial class frmReturnAddress : BaseForm
	{
		public fecherFoundation.FCComboBox cmbTown;
		public fecherFoundation.FCTextBox txtAddress4;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress3;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReturnAddress));
			this.cmbTown = new fecherFoundation.FCComboBox();
			this.txtAddress4 = new fecherFoundation.FCTextBox();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtAddress3 = new fecherFoundation.FCTextBox();
			this.txtAddress2 = new fecherFoundation.FCTextBox();
			this.txtAddress1 = new fecherFoundation.FCTextBox();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 365);
			this.BottomPanel.Size = new System.Drawing.Size(633, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbTown);
			this.ClientArea.Controls.Add(this.txtAddress4);
			this.ClientArea.Controls.Add(this.txtZip4);
			this.ClientArea.Controls.Add(this.txtZip);
			this.ClientArea.Controls.Add(this.txtState);
			this.ClientArea.Controls.Add(this.txtCity);
			this.ClientArea.Controls.Add(this.txtAddress3);
			this.ClientArea.Controls.Add(this.txtAddress2);
			this.ClientArea.Controls.Add(this.txtAddress1);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(633, 305);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(633, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(183, 30);
			this.HeaderText.Text = "Return Address";
			// 
			// cmbTown
			// 
			this.cmbTown.AutoSize = false;
			this.cmbTown.BackColor = System.Drawing.SystemColors.Window;
			this.cmbTown.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTown.FormattingEnabled = true;
			this.cmbTown.Location = new System.Drawing.Point(30, 0);
			this.cmbTown.Name = "cmbTown";
			this.cmbTown.Size = new System.Drawing.Size(191, 40);
			this.cmbTown.TabIndex = 0;
			this.cmbTown.Visible = false;
			this.cmbTown.SelectedIndexChanged += new System.EventHandler(this.cmbTown_SelectedIndexChanged);
			// 
			// txtAddress4
			// 
			this.txtAddress4.AutoSize = false;
			this.txtAddress4.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress4.LinkItem = null;
			this.txtAddress4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress4.LinkTopic = null;
			this.txtAddress4.Location = new System.Drawing.Point(160, 210);
			this.txtAddress4.Name = "txtAddress4";
			this.txtAddress4.Size = new System.Drawing.Size(442, 40);
			this.txtAddress4.TabIndex = 8;
			// 
			// txtZip4
			// 
			this.txtZip4.MaxLength = 4;
			this.txtZip4.AutoSize = false;
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip4.LinkItem = null;
			this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip4.LinkTopic = null;
			this.txtZip4.Location = new System.Drawing.Point(535, 270);
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(68, 40);
			this.txtZip4.TabIndex = 16;
			this.txtZip4.Visible = false;
			// 
			// txtZip
			// 
			this.txtZip.MaxLength = 5;
			this.txtZip.AutoSize = false;
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.LinkItem = null;
			this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip.LinkTopic = null;
			this.txtZip.Location = new System.Drawing.Point(433, 270);
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(84, 40);
			this.txtZip.TabIndex = 14;
			this.txtZip.Visible = false;
			// 
			// txtState
			// 
			this.txtState.MaxLength = 2;
			this.txtState.AutoSize = false;
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.LinkItem = null;
			this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtState.LinkTopic = null;
			this.txtState.Location = new System.Drawing.Point(352, 270);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(41, 40);
			this.txtState.TabIndex = 12;
			this.txtState.Visible = false;
			// 
			// txtCity
			// 
			this.txtCity.AutoSize = false;
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.LinkItem = null;
			this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCity.LinkTopic = null;
			this.txtCity.Location = new System.Drawing.Point(79, 270);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(205, 40);
			this.txtCity.TabIndex = 10;
			this.txtCity.Visible = false;
			// 
			// txtAddress3
			// 
			this.txtAddress3.AutoSize = false;
			this.txtAddress3.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress3.LinkItem = null;
			this.txtAddress3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress3.LinkTopic = null;
			this.txtAddress3.Location = new System.Drawing.Point(160, 150);
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Size = new System.Drawing.Size(442, 40);
			this.txtAddress3.TabIndex = 6;
			// 
			// txtAddress2
			// 
			this.txtAddress2.AutoSize = false;
			this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress2.LinkItem = null;
			this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress2.LinkTopic = null;
			this.txtAddress2.Location = new System.Drawing.Point(160, 90);
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Size = new System.Drawing.Size(442, 40);
			this.txtAddress2.TabIndex = 4;
			// 
			// txtAddress1
			// 
			this.txtAddress1.AutoSize = false;
			this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress1.LinkItem = null;
			this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress1.LinkTopic = null;
			this.txtAddress1.Location = new System.Drawing.Point(160, 30);
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Size = new System.Drawing.Size(442, 40);
			this.txtAddress1.TabIndex = 2;
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(30, 224);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(98, 18);
			this.Label8.TabIndex = 7;
			this.Label8.Text = "ADDRESS 4";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(525, 284);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(17, 30);
			this.Label7.TabIndex = 15;
			this.Label7.Text = "-";
			this.Label7.Visible = false;
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(400, 284);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(29, 26);
			this.Label6.TabIndex = 13;
			this.Label6.Text = "ZIP";
			this.Label6.Visible = false;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(297, 284);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(54, 22);
			this.Label5.TabIndex = 11;
			this.Label5.Text = "STATE";
			this.Label5.Visible = false;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 284);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(74, 20);
			this.Label4.TabIndex = 9;
			this.Label4.Text = "CITY";
			this.Label4.Visible = false;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(28, 164);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(98, 18);
			this.Label3.TabIndex = 5;
			this.Label3.Text = "ADDRESS 3";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(96, 20);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "ADDRESS 2";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(98, 20);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "ADDRESS 1";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSave,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 0;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit ";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(266, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(85, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmReturnAddress
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(633, 473);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmReturnAddress";
			this.Text = "Return Address";
			this.Load += new System.EventHandler(this.frmReturnAddress_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReturnAddress_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReturnAddress_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
