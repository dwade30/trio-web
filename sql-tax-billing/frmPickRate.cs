﻿//Fecher vbPorter - Version 1.0.0.35
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBL0000
{
    /// <summary>
    /// Summary description for frmPickRate.
    /// </summary>
    public partial class frmPickRate : BaseForm
    {
        public frmPickRate()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmPickRate InstancePtr
        {
            get
            {
                return (frmPickRate)Sys.GetInstance(typeof(frmPickRate));
            }
        }

        protected frmPickRate _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        int intTaxYear;
        bool boolREBook;
        int intStartPage;
        int intWhichOrder;
        int intWhichReport;
        bool boolUseNewPage;
        // vbPorter upgrade warning: lngRateKey As int	OnWrite(string)
        int lngRateKey;
        string strStart;
        string strEnd;
        int intBindingOption;

        private void frmPickRate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            var KeyCode = e.KeyCode;
            var Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Escape)
            {
                Close();
                return;
            }
        }

        private void frmPickRate_Load(object sender, System.EventArgs e)
        {
            modGlobalFunctions.SetFixedSize(this);
            SetupGrid();
        }

        private void frmPickRate_Resize(object sender, System.EventArgs e)
        {
            ResizeGrid();
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
        }

        private void Grid_ClickEvent(object sender, System.EventArgs e)
        {
            if (Grid.MouseRow > 0 && Grid.MouseRow < Grid.Rows)
            {
                Grid.Row = Grid.MouseRow;
            }
        }

        private void Grid_DblClick(object sender, System.EventArgs e)
        {
            if (Grid.MouseRow > 0 && Grid.MouseRow < Grid.Rows)
            {
                mnuShow_Click();
            }
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void SetupGrid()
        {
            //FC:FINAL:CHN - i.issue #1524: Commitment Book: Empty grid
            if (Grid.Rows < 1)
            {
                Grid.Rows = 1;
            }
            Grid.Cols = 8;
            Grid.ExtendLastCol = true;
            Grid.TextMatrix(0, 0, "Tax Rate");
            Grid.TextMatrix(0, 1, "Bill Date");
            Grid.TextMatrix(0, 2, "Rate Key");
            Grid.TextMatrix(0, 3, "Interest Rate");
            Grid.TextMatrix(0, 4, "Pay Periods");
            Grid.TextMatrix(0, 5, "Type");
            // regular or supplemental
            Grid.TextMatrix(0, 6, "Bill Count");
            Grid.TextMatrix(0, 7, "Description");

            Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void ResizeGrid()
        {
            var GridWidth = 0;
            GridWidth = Grid.WidthOriginal;
            Grid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.1));
            Grid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.1));
            Grid.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.09));
            Grid.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.14));
            Grid.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.11));
            Grid.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.05));
            Grid.ColWidth(6, FCConvert.ToInt32(GridWidth * 0.09));
            Grid.ColWidth(7, FCConvert.ToInt32(GridWidth * 0.28));
        }

        public void Init(int intYear, bool boolRE, int intStart, int intReport, bool boolNewPage, int intOrder = 1, string strRangeStart = "", string strRangeEnd = "", int intBindOption = 0)
        {
            var clsrate = new clsDRWrapper();
            var strModule = "";
            int RateCount;
            intTaxYear = intYear;
            boolREBook = boolRE;
            strModule = boolRE ? "RE" : "PP";
            intBindingOption = intBindOption;
            strStart = strRangeStart;
            strEnd = strRangeEnd;
            intWhichOrder = intOrder;
            intStartPage = intStart;
            boolUseNewPage = boolNewPage;
            intWhichReport = intReport;

            try
            {
                clsrate.OpenRecordset($"select raterec.ID as RateRecID, * from raterec inner join (select ratekey,count(ratekey) as numrecs from billingmaster where billingtype = '{strModule}' and billingyear between {FCConvert.ToString(intTaxYear)}0 and {FCConvert.ToString(intTaxYear)}9 group by ratekey)as TBL1 on (tbl1.ratekey = raterec.ID) order by raterec.ID", "twcl0000.vb1");
                if (clsrate.EndOfFile())
                {
                    MessageBox.Show("There are no rate records for the year " + FCConvert.ToString(intTaxYear), "No Rate Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                    return;
                }
                RateCount = clsrate.RecordCount();
                while (!clsrate.EndOfFile())
                {
                    Grid.AddItem(Strings.Format(Conversion.Val(clsrate.Get_Fields_Double("taxrate")) * 1000, "##0.000") + "\t" + Strings.Format(clsrate.Get_Fields_DateTime("billingdate"), "MM/dd/yyyy") + "\t" + clsrate.Get_Fields("raterecID") + "\t" + FCConvert.ToString(Conversion.Val(clsrate.Get_Fields_Double("interestrate")) * 100) + "\t" + clsrate.Get_Fields_Int16("numberofperiods") + "\t" + clsrate.Get_Fields_String("ratetype") + "\t" + clsrate.Get_Fields("numrecs") + "\t" + clsrate.Get_Fields_String("description"));
                    clsrate.MoveNext();
                }
                if (RateCount == 1)
                {
                    // only one so no sense in showing a choice
                    Grid.Row = 1;
                    mnuShow_Click();
                }
                else
                {
                    Show(App.MainForm);
                }
            }
            finally
            {
                clsrate.DisposeOf();
            }
        }

        private void mnuShow_Click(object sender, System.EventArgs e)
        {
            int x;
            int NumFonts;
            var strFont = "";
            string strPrinterName;
            bool boolUseFont;
            var intCPI = 0;

            try
            {
                // On Error GoTo ErrorHandler
                if (Grid.Row < 1 || Grid.Row > (Grid.Rows - 1))
                {
                    MessageBox.Show("You must select a Tax Rate Record before continuing.", "Select Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                lngRateKey = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, 2));

                MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();

                //FC:FINAL:CHN - issue #1359: Not cancelled print after cancelling select printer. 
                MDIParent.InstancePtr.CommonDialog1.CancelError = true;

                //FC:FINAL:DDU:#1358 - don't choose a printer
                //MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
                strPrinterName = FCGlobal.Printer.DeviceName;
                NumFonts = FCGlobal.Printer.Fonts.Count;
                boolUseFont = false;

                if (intWhichReport == 1)
                {
                    // get a 17 cpi printer font if possible
                    for (x = 0; x <= NumFonts - 1; x++)
                    {
                        strFont = FCGlobal.Printer.Fonts[x];

                        var right3 = Strings.Right(strFont, 3);

                        if (Strings.UCase(right3) != "CPI") 
                            continue;

                        strFont = Strings.Mid(strFont, 1, strFont.Length - 3);

                        if (Strings.Right(strFont, 2) != "17" && right3 != "17 ") 
                            continue;

                        boolUseFont = true;
                        strFont = FCGlobal.Printer.Fonts[x];

                        break;
                    }

                    // x
                    // which commitment book are we printing?
                    Close();

                    if (boolREBook)
                    {
                        rptCommitment.InstancePtr.Init(intTaxYear, boolUseFont, strFont, strPrinterName, intStartPage, boolUseNewPage, lngRateKey, false, intWhichOrder, strStart, strEnd);
                    }
                    else
                    {
                        rptPPCommitment.InstancePtr.Init(intTaxYear, boolUseFont, strFont, strPrinterName, intStartPage, boolUseNewPage, lngRateKey, false, intWhichOrder, strStart, strEnd);
                    }
                }
                else
                {
                    // look for a 12 cpi font
                    intCPI = 12;

                    for (x = 0; x <= NumFonts - 1; x++)
                    {
                        strFont = FCGlobal.Printer.Fonts[x];

                        var right3 = Strings.Right(strFont, 3);

                        if (Strings.UCase(right3) != "CPI") 
                            continue;

                        strFont = Strings.Mid(strFont, 1, strFont.Length - 3);

                        if (Conversion.Val(Strings.Right(strFont, 2)) != intCPI && Conversion.Val(right3) != intCPI) 
                            continue;

                        boolUseFont = true;
                        strFont = FCGlobal.Printer.Fonts[x];

                        break;
                    }

                    Close();

                    // x
                    if (boolREBook)
                    {
                        rptCommitmentWide.InstancePtr.Init(intTaxYear, boolUseFont, strFont, strPrinterName, intWhichReport, intStartPage, boolUseNewPage, lngRateKey, false, intWhichOrder, strStart, strEnd, intBindingOption);
                    }
                    else
                    {
                        rptPPCommitmentWide.InstancePtr.Init(intTaxYear, boolUseFont, strFont, strPrinterName, intWhichReport, intStartPage, boolUseNewPage, lngRateKey, false, intWhichOrder, strStart, strEnd, intBindingOption);
                    }
                }

            }
            catch (Exception ex)
            {
                // ErrorHandler:
                if (Information.Err(ex).Number == 32755)
                {
                    // user hit cancel on the printer dialog box
                }
                else
                {
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "in mnuShow_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }

        }

        public void mnuShow_Click()
        {
            mnuShow_Click(mnuShow, new System.EventArgs());
        }
    }
}
