﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	public class clsRateRecord
	{
		//=========================================================
		private int Txyear;
		private double TxRate;
		private int NofPeriods;
		private string RatType = string.Empty;
		private DateTime BillDate;
		private DateTime[] InterestStart = new DateTime[4 + 1];
		private DateTime[] DueD = new DateTime[4 + 1];
		private float IntRate;
		private int RatKey;
		private DateTime CreateDate;
		private string strDescription = string.Empty;
		private DateTime CommitDate;
		private double[] dblWeight = new double[4 + 1];

		public void Set_BillWeight(int PayPeriod, double dblPerWeight)
		{
			dblWeight[PayPeriod] = dblPerWeight;
		}

		public double Get_BillWeight(int PayPeriod)
		{
			double BillWeight = 0;
			BillWeight = dblWeight[PayPeriod];
			return BillWeight;
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public DateTime CreationDate
		{
			set
			{
				CreateDate = value;
			}
			get
			{
				DateTime CreationDate = System.DateTime.Now;
				CreationDate = CreateDate;
				return CreationDate;
			}
		}

		public int RateKey
		{
			set
			{
				RatKey = value;
			}
			get
			{
				int RateKey = 0;
				RateKey = RatKey;
				return RateKey;
			}
		}

		public float InterestRate
		{
			set
			{
				IntRate = value;
			}
			get
			{
				float InterestRate = 0;
				InterestRate = IntRate;
				return InterestRate;
			}
		}

		public int TaxYear
		{
			set
			{
				Txyear = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int TaxYear = 0;
				TaxYear = Txyear;
				return TaxYear;
			}
		}

		public double TaxRate
		{
			set
			{
				TxRate = value;
			}
			get
			{
				double TaxRate = 0;
				TaxRate = TxRate;
				return TaxRate;
			}
		}

		public short NumberOfPeriods
		{
			set
			{
				NofPeriods = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short NumberOfPeriods = 0;
				NumberOfPeriods = FCConvert.ToInt16(NofPeriods);
				return NumberOfPeriods;
			}
		}

		public string RateType
		{
			set
			{
				RatType = value;
			}
			get
			{
				string RateType = "";
				RateType = RatType;
				return RateType;
			}
		}

		public DateTime BillingDate
		{
			set
			{
				BillDate = value;
			}
			get
			{
				DateTime BillingDate = System.DateTime.Now;
				BillingDate = BillDate;
				return BillingDate;
			}
		}

		public DateTime CommitmentDate
		{
			set
			{
				CommitDate = value;
			}
			get
			{
				DateTime CommitmentDate = System.DateTime.Now;
				CommitmentDate = CommitDate;
				return CommitmentDate;
			}
		}

		public void Set_InterestStartDate(int PayPeriod, DateTime StartDate)
		{
			InterestStart[PayPeriod] = StartDate;
		}

		public DateTime Get_InterestStartDate(int PayPeriod)
		{
			DateTime InterestStartDate = System.DateTime.Now;
			InterestStartDate = InterestStart[PayPeriod];
			return InterestStartDate;
		}

		public void Set_DueDate(int PayPeriod, DateTime DDate)
		{
			DueD[PayPeriod] = DDate;
		}

		public DateTime Get_DueDate(int PayPeriod)
		{
			DateTime DueDate = System.DateTime.Now;
			DueDate = DueD[PayPeriod];
			return DueDate;
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWrite(int, string)
		// vbPorter upgrade warning: RateRecordset As object	OnWrite(clsDRWrapper)
		public bool LoadRate(int lngRateKey, clsDRWrapper RateRecordset = null)
		{
			bool LoadRate = false;
			clsDRWrapper clsrate = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				LoadRate = false;
				if (!(RateRecordset == null))
				{
					if (RateRecordset.FindFirstRecord("ID", lngRateKey))
					{
						strDescription = Strings.Trim(RateRecordset.Get_Fields_String("description"));
						if (strDescription == string.Empty)
							strDescription = " ";
						RatKey = RateRecordset.Get_Fields_Int32("ID");
						// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
						TaxYear = RateRecordset.Get_Fields("year");
						TaxRate = RateRecordset.Get_Fields_Double("taxrate");
						NumberOfPeriods = RateRecordset.Get_Fields_Int16("numberofperiods");
						RateType = RateRecordset.Get_Fields_String("ratetype");
						BillingDate = RateRecordset.Get_Fields_DateTime("billingdate");
						CreationDate = RateRecordset.Get_Fields_DateTime("creationdate");
						if (!fecherFoundation.FCUtils.IsEmptyDateTime(RateRecordset.Get_Fields_DateTime("commitmentdate")))
						{
							if (FCConvert.ToString(RateRecordset.Get_Fields_DateTime("commitmentdate")) != string.Empty)
							{
								if (FCConvert.ToString(RateRecordset.Get_Fields_DateTime("commitmentdate")) == string.Empty)
								{
									CommitDate = BillingDate;
								}
								else
								{
									CommitDate = (DateTime)RateRecordset.Get_Fields_DateTime("CommitmentDate");
								}
							}
						}
						if (Information.IsDate(clsrate.Get_Fields("intereststartdate1")))
						{
							Set_InterestStartDate(1, clsrate.Get_Fields_DateTime("intereststartdate1"));
						}
						if (Information.IsDate(clsrate.Get_Fields("intereststartdate2")))
						{
							Set_InterestStartDate(2, clsrate.Get_Fields_DateTime("intereststartdate2"));
						}
						if (Information.IsDate(clsrate.Get_Fields("intereststartdate3")))
						{
							Set_InterestStartDate(3, clsrate.Get_Fields_DateTime("intereststartdate3"));
						}
						if (Information.IsDate(clsrate.Get_Fields("intereststartdate4")))
						{
							Set_InterestStartDate(4, clsrate.Get_Fields_DateTime("intereststartdate4"));
						}
						dblWeight[1] = Conversion.Val(RateRecordset.Get_Fields_Double("Weight1"));
						dblWeight[2] = Conversion.Val(RateRecordset.Get_Fields_Double("Weight2"));
						dblWeight[3] = Conversion.Val(RateRecordset.Get_Fields_Double("Weight3"));
						dblWeight[4] = Conversion.Val(RateRecordset.Get_Fields_Double("Weight4"));
						Set_DueDate(1, RateRecordset.Get_Fields_DateTime("duedate1"));
						if (!Information.IsDate(RateRecordset.Get_Fields("duedate2")))
						{
							Set_DueDate(2, FCConvert.ToDateTime(0));
						}
						else
						{
							Set_DueDate(2, RateRecordset.Get_Fields_DateTime("duedate2"));
						}
						if (!Information.IsDate(RateRecordset.Get_Fields("duedate3")))
						{
							Set_DueDate(3, FCConvert.ToDateTime(0));
						}
						else
						{
							Set_DueDate(3, RateRecordset.Get_Fields_DateTime("duedate3"));
						}
						if (!Information.IsDate(RateRecordset.Get_Fields("duedate4")))
						{
							Set_DueDate(4, FCConvert.ToDateTime(0));
						}
						else
						{
							Set_DueDate(4, RateRecordset.Get_Fields_DateTime("duedate4"));
						}
						InterestRate = FCConvert.ToSingle(RateRecordset.Get_Fields_Double("interestrate"));
					}
					else
					{
						return LoadRate;
					}
				}
				else
				{
					clsrate.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngRateKey), "twcl0000.vb1");
					strDescription = Strings.Trim(FCConvert.ToString(clsrate.Get_Fields_String("description")));
					if (strDescription == string.Empty)
						strDescription = " ";
					RatKey = FCConvert.ToInt32(clsrate.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
					TaxYear = FCConvert.ToInt16(clsrate.Get_Fields("year"));
					TaxRate = clsrate.Get_Fields_Double("taxrate");
					NumberOfPeriods = FCConvert.ToInt16(clsrate.Get_Fields_Int16("numberofperiods"));
					RateType = FCConvert.ToString(clsrate.Get_Fields_String("ratetype"));
					BillingDate = (DateTime)clsrate.Get_Fields_DateTime("billingdate");
					CreationDate = (DateTime)clsrate.Get_Fields_DateTime("creationdate");
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(clsrate.Get_Fields_DateTime("commitmentdate")))
					{
						if (FCConvert.ToString(clsrate.Get_Fields_DateTime("commitmentdate")) != string.Empty)
						{
							if (FCConvert.ToString(clsrate.Get_Fields_DateTime("commitmentdate")) == string.Empty)
							{
								CommitDate = BillingDate;
							}
							else
							{
								CommitDate = (DateTime)clsrate.Get_Fields_DateTime("CommitmentDate");
							}
						}
					}
					if (Information.IsDate(clsrate.Get_Fields("intereststartdate1")))
					{
						Set_InterestStartDate(1, clsrate.Get_Fields_DateTime("intereststartdate1"));
					}
					if (Information.IsDate(clsrate.Get_Fields("intereststartdate2")))
					{
						Set_InterestStartDate(2, clsrate.Get_Fields_DateTime("intereststartdate2"));
					}
					if (Information.IsDate(clsrate.Get_Fields("intereststartdate3")))
					{
						Set_InterestStartDate(3, clsrate.Get_Fields_DateTime("intereststartdate3"));
					}
					if (Information.IsDate(clsrate.Get_Fields("intereststartdate4")))
					{
						Set_InterestStartDate(4, clsrate.Get_Fields_DateTime("intereststartdate4"));
					}
					dblWeight[1] = clsrate.Get_Fields_Double("Weight1");
					dblWeight[2] = clsrate.Get_Fields_Double("Weight2");
					dblWeight[3] = clsrate.Get_Fields_Double("Weight3");
					dblWeight[4] = clsrate.Get_Fields_Double("Weight4");
					Set_DueDate(1, clsrate.Get_Fields_DateTime("duedate1"));
					if (!Information.IsDate(clsrate.Get_Fields("duedate2")))
					{
						Set_DueDate(2, FCConvert.ToDateTime(0));
					}
					else
					{
						Set_DueDate(2, clsrate.Get_Fields_DateTime("duedate2"));
					}
					if (!Information.IsDate(clsrate.Get_Fields("duedate3")))
					{
						Set_DueDate(3, FCConvert.ToDateTime(0));
					}
					else
					{
						Set_DueDate(3, clsrate.Get_Fields_DateTime("duedate3"));
					}
					if (!Information.IsDate(clsrate.Get_Fields("duedate4")))
					{
						Set_DueDate(4, FCConvert.ToDateTime(0));
					}
					else
					{
						Set_DueDate(4, clsrate.Get_Fields_DateTime("duedate4"));
					}
					InterestRate = FCConvert.ToSingle(clsrate.Get_Fields_Double("interestrate"));
				}
				LoadRate = true;
				return LoadRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Load Rate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadRate;
		}

		public object CopyRate(ref clsRateRecord clsRateRec)
		{
			object CopyRate = null;
			this.BillingDate = clsRateRec.BillingDate;
			this.CreationDate = clsRateRec.CreationDate;
			this.Description = clsRateRec.Description;
			this.Set_DueDate(1, clsRateRec.Get_DueDate(1));
			this.Set_DueDate(2, clsRateRec.Get_DueDate(2));
			this.Set_DueDate(3, clsRateRec.Get_DueDate(3));
			this.Set_DueDate(4, clsRateRec.Get_DueDate(4));
			this.Set_BillWeight(1, clsRateRec.Get_BillWeight(1));
			this.Set_BillWeight(2, clsRateRec.Get_BillWeight(2));
			this.Set_BillWeight(3, clsRateRec.Get_BillWeight(3));
			this.Set_BillWeight(4, clsRateRec.Get_BillWeight(4));
			this.CommitmentDate = clsRateRec.CommitmentDate;
			this.InterestRate = clsRateRec.InterestRate;
			this.Set_InterestStartDate(1, clsRateRec.Get_InterestStartDate(1));
			this.Set_InterestStartDate(2, clsRateRec.Get_InterestStartDate(2));
			this.Set_InterestStartDate(3, clsRateRec.Get_InterestStartDate(3));
			this.Set_InterestStartDate(4, clsRateRec.Get_InterestStartDate(4));
			this.NumberOfPeriods = clsRateRec.NumberOfPeriods;
			this.RateKey = clsRateRec.RateKey;
			this.RateType = clsRateRec.RateType;
			this.TaxRate = clsRateRec.TaxRate;
			this.TaxYear = clsRateRec.TaxYear;
			return CopyRate;
		}
	}
}
