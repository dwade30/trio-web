﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormat2_3.
	/// </summary>
	partial class rptBillFormat2_3
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptBillFormat2_3));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPageLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCatOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDiscountAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDiscountDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDiscount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat1label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaidLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMailing1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMailing2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMailing3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMailing4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMailing5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMessage9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNameStub2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNameStub4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNameStub3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscountAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscountDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameStub2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameStub4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameStub3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Height = 0F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanGrow = false;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMapLot,
				this.txtLand,
				this.txtBldg,
				this.txtValue,
				this.txtExemption,
				this.txtAssessment,
				this.txtRate,
				this.txtTaxDue,
				this.txtLocLabel,
				this.txtBookPageLabel,
				this.txtBookPage,
				this.txtDist1,
				this.txtDistPerc1,
				this.txtDist2,
				this.txtDist3,
				this.txtDist4,
				this.txtDist5,
				this.txtDistPerc2,
				this.txtDistPerc3,
				this.txtDistPerc4,
				this.txtDistPerc5,
				this.txtDistAmount1,
				this.txtDistAmount2,
				this.txtDistAmount3,
				this.txtDistAmount4,
				this.txtDistAmount5,
				this.txtCat1,
				this.txtCat2,
				this.txtCat3,
				this.txtCatOther,
				this.lblTitle,
				this.Field16,
				this.Field17,
				this.txtDiscountAmount,
				this.Field18,
				this.txtDiscountDate,
				this.Field25,
				this.txtDiscount,
				this.txtCat1label,
				this.txtCat2Label,
				this.txtCat3Label,
				this.txtOtherLabel,
				this.txtDueDate2,
				this.txtAmount2,
				this.txtAcct2,
				this.txtDueDate3,
				this.txtAmount3,
				this.txtAccount3,
				this.txtDueDate1,
				this.txtAmount1,
				this.txtAcct1,
				this.txtDueDate4,
				this.txtAmount4,
				this.txtAcct4,
				this.txtAddress1,
				this.txtAddress2,
				this.txtAddress3,
				this.txtAddress4,
				this.txtPrePaidLabel,
				this.txtPrePaid,
				this.txtAccount,
				this.txtMailing1,
				this.txtMailing2,
				this.txtMailing3,
				this.txtMailing4,
				this.txtMessage1,
				this.txtMessage2,
				this.txtMessage3,
				this.txtMessage4,
				this.txtMessage5,
				this.txtMessage6,
				this.txtMessage7,
				this.txtMessage8,
				this.txtMailing5,
				this.txtMessage9,
				this.lblLocation,
				this.txtName,
				this.txtNameStub2,
				this.txtNameStub4,
				this.txtNameStub3
			});
			this.GroupHeader1.DataField = "Gvalue";
			this.GroupHeader1.Height = 4.8125F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 0.825F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-family: \'Courier New\'";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.21875F;
			this.txtMapLot.Width = 1.75F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 4.75F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 0.375F;
			this.txtLand.Width = 1.0625F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 4.75F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0.71875F;
			this.txtBldg.Width = 1.0625F;
			// 
			// txtValue
			// 
			this.txtValue.CanGrow = false;
			this.txtValue.Height = 0.19F;
			this.txtValue.Left = 4.75F;
			this.txtValue.MultiLine = false;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "text-align: right";
			this.txtValue.Tag = "textbox";
			this.txtValue.Text = null;
			this.txtValue.Top = 2.09375F;
			this.txtValue.Width = 1.0625F;
			// 
			// txtExemption
			// 
			this.txtExemption.CanGrow = false;
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 4.75F;
			this.txtExemption.MultiLine = false;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtExemption.Tag = "textbox";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 2.40625F;
			this.txtExemption.Width = 1.0625F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.CanGrow = false;
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 4.75F;
			this.txtAssessment.MultiLine = false;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtAssessment.Tag = "textbox";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 2.75F;
			this.txtAssessment.Width = 1.0625F;
			// 
			// txtRate
			// 
			this.txtRate.CanGrow = false;
			this.txtRate.Height = 0.19F;
			this.txtRate.Left = 4.75F;
			this.txtRate.MultiLine = false;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtRate.Tag = "textbox";
			this.txtRate.Text = null;
			this.txtRate.Top = 3.09375F;
			this.txtRate.Width = 1.0625F;
			// 
			// txtTaxDue
			// 
			this.txtTaxDue.CanGrow = false;
			this.txtTaxDue.Height = 0.19F;
			this.txtTaxDue.Left = 4.75F;
			this.txtTaxDue.MultiLine = false;
			this.txtTaxDue.Name = "txtTaxDue";
			this.txtTaxDue.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtTaxDue.Tag = "textbox";
			this.txtTaxDue.Text = null;
			this.txtTaxDue.Top = 3.40625F;
			this.txtTaxDue.Width = 1.0625F;
			// 
			// txtLocLabel
			// 
			this.txtLocLabel.CanGrow = false;
			this.txtLocLabel.Height = 0.19F;
			this.txtLocLabel.Left = 0.0625F;
			this.txtLocLabel.MultiLine = false;
			this.txtLocLabel.Name = "txtLocLabel";
			this.txtLocLabel.Tag = "textbox";
			this.txtLocLabel.Text = "Location";
			this.txtLocLabel.Top = 1.625F;
			this.txtLocLabel.Width = 1F;
			// 
			// txtBookPageLabel
			// 
			this.txtBookPageLabel.CanGrow = false;
			this.txtBookPageLabel.Height = 0.19F;
			this.txtBookPageLabel.Left = 0.0625F;
			this.txtBookPageLabel.MultiLine = false;
			this.txtBookPageLabel.Name = "txtBookPageLabel";
			this.txtBookPageLabel.Tag = "textbox";
			this.txtBookPageLabel.Text = "Book & Page";
			this.txtBookPageLabel.Top = 1.78125F;
			this.txtBookPageLabel.Width = 1.125F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.CanGrow = false;
			this.txtBookPage.Height = 0.19F;
			this.txtBookPage.Left = 1.1875F;
			this.txtBookPage.MultiLine = false;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.78125F;
			this.txtBookPage.Width = 2.5625F;
			// 
			// txtDist1
			// 
			this.txtDist1.CanGrow = false;
			this.txtDist1.Height = 0.19F;
			this.txtDist1.Left = 3.25F;
			this.txtDist1.MultiLine = false;
			this.txtDist1.Name = "txtDist1";
			this.txtDist1.Tag = "textbox";
			this.txtDist1.Text = null;
			this.txtDist1.Top = 4F;
			this.txtDist1.Width = 1F;
			// 
			// txtDistPerc1
			// 
			this.txtDistPerc1.CanGrow = false;
			this.txtDistPerc1.Height = 0.19F;
			this.txtDistPerc1.Left = 4.25F;
			this.txtDistPerc1.MultiLine = false;
			this.txtDistPerc1.Name = "txtDistPerc1";
			this.txtDistPerc1.Style = "text-align: right";
			this.txtDistPerc1.Tag = "textbox";
			this.txtDistPerc1.Text = null;
			this.txtDistPerc1.Top = 4F;
			this.txtDistPerc1.Width = 0.6875F;
			// 
			// txtDist2
			// 
			this.txtDist2.CanGrow = false;
			this.txtDist2.Height = 0.19F;
			this.txtDist2.Left = 3.25F;
			this.txtDist2.MultiLine = false;
			this.txtDist2.Name = "txtDist2";
			this.txtDist2.Tag = "textbox";
			this.txtDist2.Text = " ";
			this.txtDist2.Top = 4.15625F;
			this.txtDist2.Visible = false;
			this.txtDist2.Width = 1F;
			// 
			// txtDist3
			// 
			this.txtDist3.CanGrow = false;
			this.txtDist3.Height = 0.19F;
			this.txtDist3.Left = 3.25F;
			this.txtDist3.MultiLine = false;
			this.txtDist3.Name = "txtDist3";
			this.txtDist3.Tag = "textbox";
			this.txtDist3.Text = " ";
			this.txtDist3.Top = 4.3125F;
			this.txtDist3.Visible = false;
			this.txtDist3.Width = 1F;
			// 
			// txtDist4
			// 
			this.txtDist4.CanGrow = false;
			this.txtDist4.Height = 0.19F;
			this.txtDist4.Left = 3.25F;
			this.txtDist4.MultiLine = false;
			this.txtDist4.Name = "txtDist4";
			this.txtDist4.Tag = "textbox";
			this.txtDist4.Text = " ";
			this.txtDist4.Top = 4.46875F;
			this.txtDist4.Visible = false;
			this.txtDist4.Width = 1F;
			// 
			// txtDist5
			// 
			this.txtDist5.CanGrow = false;
			this.txtDist5.Height = 0.19F;
			this.txtDist5.Left = 3.25F;
			this.txtDist5.MultiLine = false;
			this.txtDist5.Name = "txtDist5";
			this.txtDist5.Tag = "textbox";
			this.txtDist5.Text = " ";
			this.txtDist5.Top = 4.625F;
			this.txtDist5.Visible = false;
			this.txtDist5.Width = 1F;
			// 
			// txtDistPerc2
			// 
			this.txtDistPerc2.CanGrow = false;
			this.txtDistPerc2.Height = 0.19F;
			this.txtDistPerc2.Left = 4.25F;
			this.txtDistPerc2.MultiLine = false;
			this.txtDistPerc2.Name = "txtDistPerc2";
			this.txtDistPerc2.Style = "text-align: right";
			this.txtDistPerc2.Tag = "textbox";
			this.txtDistPerc2.Text = " ";
			this.txtDistPerc2.Top = 4.15625F;
			this.txtDistPerc2.Visible = false;
			this.txtDistPerc2.Width = 0.6875F;
			// 
			// txtDistPerc3
			// 
			this.txtDistPerc3.CanGrow = false;
			this.txtDistPerc3.Height = 0.19F;
			this.txtDistPerc3.Left = 4.25F;
			this.txtDistPerc3.MultiLine = false;
			this.txtDistPerc3.Name = "txtDistPerc3";
			this.txtDistPerc3.Style = "text-align: right";
			this.txtDistPerc3.Tag = "textbox";
			this.txtDistPerc3.Text = " ";
			this.txtDistPerc3.Top = 4.3125F;
			this.txtDistPerc3.Visible = false;
			this.txtDistPerc3.Width = 0.6875F;
			// 
			// txtDistPerc4
			// 
			this.txtDistPerc4.CanGrow = false;
			this.txtDistPerc4.Height = 0.19F;
			this.txtDistPerc4.Left = 4.25F;
			this.txtDistPerc4.MultiLine = false;
			this.txtDistPerc4.Name = "txtDistPerc4";
			this.txtDistPerc4.Style = "text-align: right";
			this.txtDistPerc4.Tag = "textbox";
			this.txtDistPerc4.Text = " ";
			this.txtDistPerc4.Top = 4.46875F;
			this.txtDistPerc4.Visible = false;
			this.txtDistPerc4.Width = 0.6875F;
			// 
			// txtDistPerc5
			// 
			this.txtDistPerc5.CanGrow = false;
			this.txtDistPerc5.Height = 0.19F;
			this.txtDistPerc5.Left = 4.25F;
			this.txtDistPerc5.MultiLine = false;
			this.txtDistPerc5.Name = "txtDistPerc5";
			this.txtDistPerc5.Style = "text-align: right";
			this.txtDistPerc5.Tag = "textbox";
			this.txtDistPerc5.Text = " ";
			this.txtDistPerc5.Top = 4.625F;
			this.txtDistPerc5.Visible = false;
			this.txtDistPerc5.Width = 0.6875F;
			// 
			// txtDistAmount1
			// 
			this.txtDistAmount1.CanGrow = false;
			this.txtDistAmount1.Height = 0.19F;
			this.txtDistAmount1.Left = 4.9375F;
			this.txtDistAmount1.MultiLine = false;
			this.txtDistAmount1.Name = "txtDistAmount1";
			this.txtDistAmount1.Style = "text-align: right";
			this.txtDistAmount1.Tag = "textbox";
			this.txtDistAmount1.Text = null;
			this.txtDistAmount1.Top = 4F;
			this.txtDistAmount1.Visible = false;
			this.txtDistAmount1.Width = 0.875F;
			// 
			// txtDistAmount2
			// 
			this.txtDistAmount2.CanGrow = false;
			this.txtDistAmount2.Height = 0.19F;
			this.txtDistAmount2.Left = 4.9375F;
			this.txtDistAmount2.MultiLine = false;
			this.txtDistAmount2.Name = "txtDistAmount2";
			this.txtDistAmount2.Style = "text-align: right";
			this.txtDistAmount2.Tag = "textbox";
			this.txtDistAmount2.Text = " ";
			this.txtDistAmount2.Top = 4.15625F;
			this.txtDistAmount2.Visible = false;
			this.txtDistAmount2.Width = 0.875F;
			// 
			// txtDistAmount3
			// 
			this.txtDistAmount3.CanGrow = false;
			this.txtDistAmount3.Height = 0.19F;
			this.txtDistAmount3.Left = 4.9375F;
			this.txtDistAmount3.MultiLine = false;
			this.txtDistAmount3.Name = "txtDistAmount3";
			this.txtDistAmount3.Style = "text-align: right";
			this.txtDistAmount3.Tag = "textbox";
			this.txtDistAmount3.Text = " ";
			this.txtDistAmount3.Top = 4.3125F;
			this.txtDistAmount3.Visible = false;
			this.txtDistAmount3.Width = 0.875F;
			// 
			// txtDistAmount4
			// 
			this.txtDistAmount4.CanGrow = false;
			this.txtDistAmount4.Height = 0.19F;
			this.txtDistAmount4.Left = 4.9375F;
			this.txtDistAmount4.MultiLine = false;
			this.txtDistAmount4.Name = "txtDistAmount4";
			this.txtDistAmount4.Style = "text-align: right";
			this.txtDistAmount4.Tag = "textbox";
			this.txtDistAmount4.Text = " ";
			this.txtDistAmount4.Top = 4.46875F;
			this.txtDistAmount4.Visible = false;
			this.txtDistAmount4.Width = 0.875F;
			// 
			// txtDistAmount5
			// 
			this.txtDistAmount5.CanGrow = false;
			this.txtDistAmount5.Height = 0.19F;
			this.txtDistAmount5.Left = 4.9375F;
			this.txtDistAmount5.MultiLine = false;
			this.txtDistAmount5.Name = "txtDistAmount5";
			this.txtDistAmount5.Style = "text-align: right";
			this.txtDistAmount5.Tag = "textbox";
			this.txtDistAmount5.Text = " ";
			this.txtDistAmount5.Top = 4.625F;
			this.txtDistAmount5.Visible = false;
			this.txtDistAmount5.Width = 0.875F;
			// 
			// txtCat1
			// 
			this.txtCat1.CanGrow = false;
			this.txtCat1.Height = 0.19F;
			this.txtCat1.Left = 4.75F;
			this.txtCat1.MultiLine = false;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.Style = "text-align: right";
			this.txtCat1.Tag = "textbox";
			this.txtCat1.Text = null;
			this.txtCat1.Top = 1.3125F;
			this.txtCat1.Width = 1.0625F;
			// 
			// txtCat2
			// 
			this.txtCat2.CanGrow = false;
			this.txtCat2.Height = 0.19F;
			this.txtCat2.Left = 4.75F;
			this.txtCat2.MultiLine = false;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.Style = "text-align: right";
			this.txtCat2.Tag = "textbox";
			this.txtCat2.Text = null;
			this.txtCat2.Top = 1.46875F;
			this.txtCat2.Width = 1.0625F;
			// 
			// txtCat3
			// 
			this.txtCat3.CanGrow = false;
			this.txtCat3.Height = 0.19F;
			this.txtCat3.Left = 4.75F;
			this.txtCat3.MultiLine = false;
			this.txtCat3.Name = "txtCat3";
			this.txtCat3.Style = "text-align: right";
			this.txtCat3.Tag = "textbox";
			this.txtCat3.Text = null;
			this.txtCat3.Top = 1.625F;
			this.txtCat3.Width = 1.0625F;
			// 
			// txtCatOther
			// 
			this.txtCatOther.CanGrow = false;
			this.txtCatOther.Height = 0.19F;
			this.txtCatOther.Left = 4.75F;
			this.txtCatOther.MultiLine = false;
			this.txtCatOther.Name = "txtCatOther";
			this.txtCatOther.Style = "text-align: right";
			this.txtCatOther.Tag = "textbox";
			this.txtCatOther.Text = null;
			this.txtCatOther.Top = 1.78125F;
			this.txtCatOther.Width = 1.0625F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.19F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 5.25F;
			this.lblTitle.MultiLine = false;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "";
			this.lblTitle.Tag = "textbox";
			this.lblTitle.Text = null;
			this.lblTitle.Top = 0.0625F;
			this.lblTitle.Width = 0.5625F;
			// 
			// Field16
			// 
			this.Field16.CanGrow = false;
			this.Field16.Height = 0.19F;
			this.Field16.Left = 6.4375F;
			this.Field16.MultiLine = false;
			this.Field16.Name = "Field16";
			this.Field16.Tag = "textbox";
			this.Field16.Text = "% discount available.";
			this.Field16.Top = 0.375F;
			this.Field16.Width = 1.0625F;
			// 
			// Field17
			// 
			this.Field17.CanGrow = false;
			this.Field17.Height = 0.19F;
			this.Field17.Left = 5.8125F;
			this.Field17.MultiLine = false;
			this.Field17.Name = "Field17";
			this.Field17.Tag = "textbox";
			this.Field17.Text = "available.To";
			this.Field17.Top = 0.5625F;
			this.Field17.Width = 1.5625F;
			// 
			// txtDiscountAmount
			// 
			this.txtDiscountAmount.CanGrow = false;
			this.txtDiscountAmount.Height = 0.19F;
			this.txtDiscountAmount.Left = 5.8125F;
			this.txtDiscountAmount.MultiLine = false;
			this.txtDiscountAmount.Name = "txtDiscountAmount";
			this.txtDiscountAmount.Tag = "textbox";
			this.txtDiscountAmount.Text = "000,000.00";
			this.txtDiscountAmount.Top = 0.875F;
			this.txtDiscountAmount.Width = 1.5625F;
			// 
			// Field18
			// 
			this.Field18.CanGrow = false;
			this.Field18.Height = 0.19F;
			this.Field18.Left = 5.8125F;
			this.Field18.MultiLine = false;
			this.Field18.Name = "Field18";
			this.Field18.Tag = "textbox";
			this.Field18.Text = "in full by";
			this.Field18.Top = 1.03125F;
			this.Field18.Width = 1.5625F;
			// 
			// txtDiscountDate
			// 
			this.txtDiscountDate.CanGrow = false;
			this.txtDiscountDate.Height = 0.19F;
			this.txtDiscountDate.Left = 5.8125F;
			this.txtDiscountDate.MultiLine = false;
			this.txtDiscountDate.Name = "txtDiscountDate";
			this.txtDiscountDate.Tag = "textbox";
			this.txtDiscountDate.Text = "00/00/00.";
			this.txtDiscountDate.Top = 1.1875F;
			this.txtDiscountDate.Width = 1.5625F;
			// 
			// Field25
			// 
			this.Field25.CanGrow = false;
			this.Field25.Height = 0.19F;
			this.Field25.Left = 5.8125F;
			this.Field25.MultiLine = false;
			this.Field25.Name = "Field25";
			this.Field25.Tag = "textbox";
			this.Field25.Text = "obtain pay";
			this.Field25.Top = 0.71875F;
			this.Field25.Width = 1.5625F;
			// 
			// txtDiscount
			// 
			this.txtDiscount.Height = 0.19F;
			this.txtDiscount.HyperLink = null;
			this.txtDiscount.Left = 5.8125F;
			this.txtDiscount.MultiLine = false;
			this.txtDiscount.Name = "txtDiscount";
			this.txtDiscount.Style = "";
			this.txtDiscount.Tag = "textbox";
			this.txtDiscount.Text = "00.00%";
			this.txtDiscount.Top = 0.375F;
			this.txtDiscount.Width = 0.625F;
			// 
			// txtCat1label
			// 
			this.txtCat1label.CanGrow = false;
			this.txtCat1label.Height = 0.19F;
			this.txtCat1label.Left = 3.8125F;
			this.txtCat1label.MultiLine = false;
			this.txtCat1label.Name = "txtCat1label";
			this.txtCat1label.Tag = "textbox";
			this.txtCat1label.Text = null;
			this.txtCat1label.Top = 1.3125F;
			this.txtCat1label.Width = 0.9375F;
			// 
			// txtCat2Label
			// 
			this.txtCat2Label.CanGrow = false;
			this.txtCat2Label.Height = 0.19F;
			this.txtCat2Label.Left = 3.8125F;
			this.txtCat2Label.MultiLine = false;
			this.txtCat2Label.Name = "txtCat2Label";
			this.txtCat2Label.Tag = "textbox";
			this.txtCat2Label.Text = null;
			this.txtCat2Label.Top = 1.46875F;
			this.txtCat2Label.Width = 0.9375F;
			// 
			// txtCat3Label
			// 
			this.txtCat3Label.CanGrow = false;
			this.txtCat3Label.Height = 0.19F;
			this.txtCat3Label.Left = 3.8125F;
			this.txtCat3Label.MultiLine = false;
			this.txtCat3Label.Name = "txtCat3Label";
			this.txtCat3Label.Tag = "textbox";
			this.txtCat3Label.Text = null;
			this.txtCat3Label.Top = 1.625F;
			this.txtCat3Label.Width = 0.9375F;
			// 
			// txtOtherLabel
			// 
			this.txtOtherLabel.CanGrow = false;
			this.txtOtherLabel.Height = 0.19F;
			this.txtOtherLabel.Left = 3.8125F;
			this.txtOtherLabel.MultiLine = false;
			this.txtOtherLabel.Name = "txtOtherLabel";
			this.txtOtherLabel.Tag = "textbox";
			this.txtOtherLabel.Text = "Other";
			this.txtOtherLabel.Top = 1.78125F;
			this.txtOtherLabel.Width = 0.9375F;
			// 
			// txtDueDate2
			// 
			this.txtDueDate2.CanGrow = false;
			this.txtDueDate2.Height = 0.19F;
			this.txtDueDate2.Left = 7.541667F;
			this.txtDueDate2.MultiLine = false;
			this.txtDueDate2.Name = "txtDueDate2";
			this.txtDueDate2.Tag = "textbox";
			this.txtDueDate2.Text = null;
			this.txtDueDate2.Top = 2.9375F;
			this.txtDueDate2.Width = 1.333333F;
			// 
			// txtAmount2
			// 
			this.txtAmount2.CanGrow = false;
			this.txtAmount2.Height = 0.19F;
			this.txtAmount2.Left = 7.541667F;
			this.txtAmount2.MultiLine = false;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Tag = "textbox";
			this.txtAmount2.Text = null;
			this.txtAmount2.Top = 3.5625F;
			this.txtAmount2.Width = 1.25F;
			// 
			// txtAcct2
			// 
			this.txtAcct2.CanGrow = false;
			this.txtAcct2.Height = 0.19F;
			this.txtAcct2.Left = 7.541667F;
			this.txtAcct2.MultiLine = false;
			this.txtAcct2.Name = "txtAcct2";
			this.txtAcct2.Tag = "textbox";
			this.txtAcct2.Text = null;
			this.txtAcct2.Top = 4.15625F;
			this.txtAcct2.Width = 1.333333F;
			// 
			// txtDueDate3
			// 
			this.txtDueDate3.CanGrow = false;
			this.txtDueDate3.Height = 0.19F;
			this.txtDueDate3.Left = 5.979167F;
			this.txtDueDate3.MultiLine = false;
			this.txtDueDate3.Name = "txtDueDate3";
			this.txtDueDate3.Tag = "textbox";
			this.txtDueDate3.Text = null;
			this.txtDueDate3.Top = 0.40625F;
			this.txtDueDate3.Width = 1.333333F;
			// 
			// txtAmount3
			// 
			this.txtAmount3.CanGrow = false;
			this.txtAmount3.Height = 0.19F;
			this.txtAmount3.Left = 5.979167F;
			this.txtAmount3.MultiLine = false;
			this.txtAmount3.Name = "txtAmount3";
			this.txtAmount3.Tag = "textbox";
			this.txtAmount3.Text = null;
			this.txtAmount3.Top = 1.03125F;
			this.txtAmount3.Width = 1.333333F;
			// 
			// txtAccount3
			// 
			this.txtAccount3.CanGrow = false;
			this.txtAccount3.Height = 0.19F;
			this.txtAccount3.Left = 5.979167F;
			this.txtAccount3.MultiLine = false;
			this.txtAccount3.Name = "txtAccount3";
			this.txtAccount3.Tag = "textbox";
			this.txtAccount3.Text = null;
			this.txtAccount3.Top = 1.625F;
			this.txtAccount3.Width = 1.333333F;
			// 
			// txtDueDate1
			// 
			this.txtDueDate1.CanGrow = false;
			this.txtDueDate1.Height = 0.19F;
			this.txtDueDate1.Left = 7.541667F;
			this.txtDueDate1.MultiLine = false;
			this.txtDueDate1.Name = "txtDueDate1";
			this.txtDueDate1.Tag = "textbox";
			this.txtDueDate1.Text = null;
			this.txtDueDate1.Top = 0.375F;
			this.txtDueDate1.Width = 1.3125F;
			// 
			// txtAmount1
			// 
			this.txtAmount1.CanGrow = false;
			this.txtAmount1.Height = 0.19F;
			this.txtAmount1.Left = 7.541667F;
			this.txtAmount1.MultiLine = false;
			this.txtAmount1.Name = "txtAmount1";
			this.txtAmount1.Tag = "textbox";
			this.txtAmount1.Text = null;
			this.txtAmount1.Top = 1F;
			this.txtAmount1.Width = 1.25F;
			// 
			// txtAcct1
			// 
			this.txtAcct1.CanGrow = false;
			this.txtAcct1.Height = 0.19F;
			this.txtAcct1.Left = 7.541667F;
			this.txtAcct1.MultiLine = false;
			this.txtAcct1.Name = "txtAcct1";
			this.txtAcct1.Tag = "textbox";
			this.txtAcct1.Text = null;
			this.txtAcct1.Top = 1.625F;
			this.txtAcct1.Width = 1.333333F;
			// 
			// txtDueDate4
			// 
			this.txtDueDate4.CanGrow = false;
			this.txtDueDate4.Height = 0.19F;
			this.txtDueDate4.Left = 5.979167F;
			this.txtDueDate4.MultiLine = false;
			this.txtDueDate4.Name = "txtDueDate4";
			this.txtDueDate4.Tag = "textbox";
			this.txtDueDate4.Text = "dfadsf";
			this.txtDueDate4.Top = 2.90625F;
			this.txtDueDate4.Width = 1.333333F;
			// 
			// txtAmount4
			// 
			this.txtAmount4.CanGrow = false;
			this.txtAmount4.Height = 0.19F;
			this.txtAmount4.Left = 5.979167F;
			this.txtAmount4.MultiLine = false;
			this.txtAmount4.Name = "txtAmount4";
			this.txtAmount4.Tag = "textbox";
			this.txtAmount4.Text = "adsfadf";
			this.txtAmount4.Top = 3.53125F;
			this.txtAmount4.Width = 1.375F;
			// 
			// txtAcct4
			// 
			this.txtAcct4.CanGrow = false;
			this.txtAcct4.Height = 0.19F;
			this.txtAcct4.Left = 5.979167F;
			this.txtAcct4.MultiLine = false;
			this.txtAcct4.Name = "txtAcct4";
			this.txtAcct4.Tag = "textbox";
			this.txtAcct4.Text = null;
			this.txtAcct4.Top = 4.15625F;
			this.txtAcct4.Width = 1.333333F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.CanGrow = false;
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 0.0625F;
			this.txtAddress1.MultiLine = false;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.0625F;
			this.txtAddress1.Width = 2.3125F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.CanGrow = false;
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 0.0625F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.21875F;
			this.txtAddress2.Width = 2.3125F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.CanGrow = false;
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.Left = 0.0625F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.375F;
			this.txtAddress3.Width = 2.3125F;
			// 
			// txtAddress4
			// 
			this.txtAddress4.CanGrow = false;
			this.txtAddress4.Height = 0.19F;
			this.txtAddress4.Left = 0.0625F;
			this.txtAddress4.MultiLine = false;
			this.txtAddress4.Name = "txtAddress4";
			this.txtAddress4.Tag = "textbox";
			this.txtAddress4.Text = null;
			this.txtAddress4.Top = 0.53125F;
			this.txtAddress4.Width = 2.3125F;
			// 
			// txtPrePaidLabel
			// 
			this.txtPrePaidLabel.CanGrow = false;
			this.txtPrePaidLabel.Height = 0.19F;
			this.txtPrePaidLabel.Left = 5.8125F;
			this.txtPrePaidLabel.MultiLine = false;
			this.txtPrePaidLabel.Name = "txtPrePaidLabel";
			this.txtPrePaidLabel.Tag = "textbox";
			this.txtPrePaidLabel.Text = "Paid to Date";
			this.txtPrePaidLabel.Top = 4.625F;
			this.txtPrePaidLabel.Width = 1.25F;
			// 
			// txtPrePaid
			// 
			this.txtPrePaid.CanGrow = false;
			this.txtPrePaid.Height = 0.19F;
			this.txtPrePaid.Left = 7.0625F;
			this.txtPrePaid.MultiLine = false;
			this.txtPrePaid.Name = "txtPrePaid";
			this.txtPrePaid.Tag = "textbox";
			this.txtPrePaid.Text = null;
			this.txtPrePaid.Top = 4.625F;
			this.txtPrePaid.Width = 1.5625F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.HyperLink = null;
			this.txtAccount.Left = 1.575F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.875F;
			this.txtAccount.Width = 1F;
			// 
			// txtMailing1
			// 
			this.txtMailing1.Height = 0.19F;
			this.txtMailing1.HyperLink = null;
			this.txtMailing1.Left = 0.125F;
			this.txtMailing1.MultiLine = false;
			this.txtMailing1.Name = "txtMailing1";
			this.txtMailing1.Style = "";
			this.txtMailing1.Tag = "textbox";
			this.txtMailing1.Text = null;
			this.txtMailing1.Top = 3.6875F;
			this.txtMailing1.Width = 3F;
			// 
			// txtMailing2
			// 
			this.txtMailing2.Height = 0.19F;
			this.txtMailing2.HyperLink = null;
			this.txtMailing2.Left = 0.125F;
			this.txtMailing2.MultiLine = false;
			this.txtMailing2.Name = "txtMailing2";
			this.txtMailing2.Style = "";
			this.txtMailing2.Tag = "textbox";
			this.txtMailing2.Text = null;
			this.txtMailing2.Top = 3.84375F;
			this.txtMailing2.Width = 3F;
			// 
			// txtMailing3
			// 
			this.txtMailing3.Height = 0.19F;
			this.txtMailing3.HyperLink = null;
			this.txtMailing3.Left = 0.125F;
			this.txtMailing3.MultiLine = false;
			this.txtMailing3.Name = "txtMailing3";
			this.txtMailing3.Style = "";
			this.txtMailing3.Tag = "textbox";
			this.txtMailing3.Text = null;
			this.txtMailing3.Top = 4F;
			this.txtMailing3.Width = 3F;
			// 
			// txtMailing4
			// 
			this.txtMailing4.Height = 0.19F;
			this.txtMailing4.HyperLink = null;
			this.txtMailing4.Left = 0.125F;
			this.txtMailing4.MultiLine = false;
			this.txtMailing4.Name = "txtMailing4";
			this.txtMailing4.Style = "";
			this.txtMailing4.Tag = "textbox";
			this.txtMailing4.Text = null;
			this.txtMailing4.Top = 4.15625F;
			this.txtMailing4.Width = 3F;
			// 
			// txtMessage1
			// 
			this.txtMessage1.Height = 0.19F;
			this.txtMessage1.HyperLink = null;
			this.txtMessage1.Left = 0.0625F;
			this.txtMessage1.MultiLine = false;
			this.txtMessage1.Name = "txtMessage1";
			this.txtMessage1.Style = "";
			this.txtMessage1.Tag = "textbox";
			this.txtMessage1.Text = null;
			this.txtMessage1.Top = 2.09375F;
			this.txtMessage1.Width = 3.6875F;
			// 
			// txtMessage2
			// 
			this.txtMessage2.Height = 0.19F;
			this.txtMessage2.HyperLink = null;
			this.txtMessage2.Left = 0.0625F;
			this.txtMessage2.MultiLine = false;
			this.txtMessage2.Name = "txtMessage2";
			this.txtMessage2.Style = "";
			this.txtMessage2.Tag = "textbox";
			this.txtMessage2.Text = null;
			this.txtMessage2.Top = 2.25F;
			this.txtMessage2.Width = 3.6875F;
			// 
			// txtMessage3
			// 
			this.txtMessage3.Height = 0.19F;
			this.txtMessage3.HyperLink = null;
			this.txtMessage3.Left = 0.0625F;
			this.txtMessage3.MultiLine = false;
			this.txtMessage3.Name = "txtMessage3";
			this.txtMessage3.Style = "";
			this.txtMessage3.Tag = "textbox";
			this.txtMessage3.Text = null;
			this.txtMessage3.Top = 2.40625F;
			this.txtMessage3.Width = 3.6875F;
			// 
			// txtMessage4
			// 
			this.txtMessage4.Height = 0.19F;
			this.txtMessage4.HyperLink = null;
			this.txtMessage4.Left = 0.0625F;
			this.txtMessage4.MultiLine = false;
			this.txtMessage4.Name = "txtMessage4";
			this.txtMessage4.Style = "";
			this.txtMessage4.Tag = "textbox";
			this.txtMessage4.Text = null;
			this.txtMessage4.Top = 2.5625F;
			this.txtMessage4.Width = 3.6875F;
			// 
			// txtMessage5
			// 
			this.txtMessage5.Height = 0.19F;
			this.txtMessage5.HyperLink = null;
			this.txtMessage5.Left = 0.0625F;
			this.txtMessage5.MultiLine = false;
			this.txtMessage5.Name = "txtMessage5";
			this.txtMessage5.Style = "";
			this.txtMessage5.Tag = "textbox";
			this.txtMessage5.Text = null;
			this.txtMessage5.Top = 2.71875F;
			this.txtMessage5.Width = 3.6875F;
			// 
			// txtMessage6
			// 
			this.txtMessage6.Height = 0.19F;
			this.txtMessage6.HyperLink = null;
			this.txtMessage6.Left = 0.0625F;
			this.txtMessage6.MultiLine = false;
			this.txtMessage6.Name = "txtMessage6";
			this.txtMessage6.Style = "";
			this.txtMessage6.Tag = "textbox";
			this.txtMessage6.Text = null;
			this.txtMessage6.Top = 2.875F;
			this.txtMessage6.Width = 3.6875F;
			// 
			// txtMessage7
			// 
			this.txtMessage7.Height = 0.19F;
			this.txtMessage7.HyperLink = null;
			this.txtMessage7.Left = 0.0625F;
			this.txtMessage7.MultiLine = false;
			this.txtMessage7.Name = "txtMessage7";
			this.txtMessage7.Style = "";
			this.txtMessage7.Tag = "textbox";
			this.txtMessage7.Text = null;
			this.txtMessage7.Top = 3.03125F;
			this.txtMessage7.Width = 3.6875F;
			// 
			// txtMessage8
			// 
			this.txtMessage8.Height = 0.19F;
			this.txtMessage8.HyperLink = null;
			this.txtMessage8.Left = 0.0625F;
			this.txtMessage8.MultiLine = false;
			this.txtMessage8.Name = "txtMessage8";
			this.txtMessage8.Style = "";
			this.txtMessage8.Tag = "textbox";
			this.txtMessage8.Text = null;
			this.txtMessage8.Top = 3.1875F;
			this.txtMessage8.Width = 3.6875F;
			// 
			// txtMailing5
			// 
			this.txtMailing5.Height = 0.19F;
			this.txtMailing5.HyperLink = null;
			this.txtMailing5.Left = 0.125F;
			this.txtMailing5.MultiLine = false;
			this.txtMailing5.Name = "txtMailing5";
			this.txtMailing5.Style = "";
			this.txtMailing5.Tag = "textbox";
			this.txtMailing5.Text = null;
			this.txtMailing5.Top = 4.3125F;
			this.txtMailing5.Width = 3F;
			// 
			// txtMessage9
			// 
			this.txtMessage9.Height = 0.19F;
			this.txtMessage9.HyperLink = null;
			this.txtMessage9.Left = 0.0625F;
			this.txtMessage9.MultiLine = false;
			this.txtMessage9.Name = "txtMessage9";
			this.txtMessage9.Style = "";
			this.txtMessage9.Tag = "textbox";
			this.txtMessage9.Text = null;
			this.txtMessage9.Top = 3.34375F;
			this.txtMessage9.Width = 3.6875F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.19F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 1.1875F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "";
			this.lblLocation.Tag = "textbox";
			this.lblLocation.Text = null;
			this.lblLocation.Top = 1.625F;
			this.lblLocation.Width = 2.5625F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.19F;
			this.txtName.Left = 7.541667F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Tag = "textbox";
			this.txtName.Text = null;
			this.txtName.Top = 1.78125F;
			this.txtName.Width = 1.333333F;
			// 
			// txtNameStub2
			// 
			this.txtNameStub2.Height = 0.19F;
			this.txtNameStub2.HyperLink = null;
			this.txtNameStub2.Left = 7.541667F;
			this.txtNameStub2.Name = "txtNameStub2";
			this.txtNameStub2.Style = "";
			this.txtNameStub2.Tag = "textbox";
			this.txtNameStub2.Text = null;
			this.txtNameStub2.Top = 4.3125F;
			this.txtNameStub2.Width = 1.333333F;
			// 
			// txtNameStub4
			// 
			this.txtNameStub4.Height = 0.19F;
			this.txtNameStub4.HyperLink = null;
			this.txtNameStub4.Left = 5.979167F;
			this.txtNameStub4.Name = "txtNameStub4";
			this.txtNameStub4.Style = "";
			this.txtNameStub4.Tag = "textbox";
			this.txtNameStub4.Text = null;
			this.txtNameStub4.Top = 1.78125F;
			this.txtNameStub4.Width = 1.333333F;
			// 
			// txtNameStub3
			// 
			this.txtNameStub3.Height = 0.19F;
			this.txtNameStub3.HyperLink = null;
			this.txtNameStub3.Left = 5.979167F;
			this.txtNameStub3.Name = "txtNameStub3";
			this.txtNameStub3.Style = "";
			this.txtNameStub3.Tag = "textbox";
			this.txtNameStub3.Text = null;
			this.txtNameStub3.Top = 4.3125F;
			this.txtNameStub3.Width = 1.333333F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// rptBillFormat2_3
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.75F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.958333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.rptBillFormat2_3_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscountAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscountDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameStub2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameStub4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameStub3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPageLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatOther;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscountAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscountDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaidLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaid;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMailing1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMailing2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMailing3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMailing4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage3;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage6;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage7;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage8;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMailing5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMessage9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtNameStub2;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtNameStub4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtNameStub3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
