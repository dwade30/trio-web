﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptRENotBilled.
	/// </summary>
	public partial class rptRENotBilled : BaseSectionReport
	{
		public rptRENotBilled()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate Not Billed";
		}

		public static rptRENotBilled InstancePtr
		{
			get
			{
				return (rptRENotBilled)Sys.GetInstance(typeof(rptRENotBilled));
			}
		}

		protected rptRENotBilled _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsBill.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRENotBilled	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsBill = new clsDRWrapper();
		int lngYear;
		cPartyController tPCont = new cPartyController();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsBill.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strResponse = "";
			// Dim lngYear As Long
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "Report Start";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = DateTime.Today.ToString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			// get the tax year to run this report on
			lblTitle.Text = lblTitle.Text + FCConvert.ToString(lngYear);
			// must build a querydef to handle this sql statement
			clsBill.OpenRecordset("select * from master left join (SELECT * From " + clsBill.CurrentPrefix + "Collections.dbo.billingmaster WHERE billingtype = 'RE' and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ") as REbillingquery on (REbillingquery.account = master.rsaccount)  where master.rscard = 1 and master.rsdeleted <> 1 and isnull(REbillingquery.account, -1) = -1 order by master.rsaccount", "twre0000.vb1");
			if (clsBill.EndOfFile())
			{
				MessageBox.Show("No accounts found.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsBill.EndOfFile())
			{
				cParty tParty;
				tParty = tPCont.GetParty(FCConvert.ToInt32(Conversion.Val(clsBill.Get_Fields_Int32("Ownerpartyid"))));
				txtAccount.Text = FCConvert.ToString(clsBill.Get_Fields_Int32("rsaccount"));
				txtName.Text = "";
				//if (!(tParty == null))
				//{
				//	txtName.Text = tParty.FullNameLastFirst;
				//}

				txtName.Text = clsBill.Get_Fields_String("DeedName1");
				txtAssessment.Text = Strings.Format(Conversion.Val(clsBill.Get_Fields_Int32("lastlandval")) + Conversion.Val(clsBill.Get_Fields_Int32("lastbldgval") - Conversion.Val(clsBill.Get_Fields_Int32("rlexemption"))), "###,###,###,##0");
				clsBill.MoveNext();
			}
		}
		// vbPorter upgrade warning: intYear As short	OnWriteFCConvert.ToDouble(
		public void Init(int intYear)
		{
			lngYear = intYear;
			// Me.Show , MDIParent
			;
			//FC:FINAL:CHN - i.issue #1544: error when printing errors.
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "RENotBilled");
		}

		
	}
}
