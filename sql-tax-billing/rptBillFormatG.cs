﻿//Fecher vbPorter - Version 1.0.0.35
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using System.IO;
using TWSharedLibrary;

namespace TWBL0000
{
    /// <summary>
    /// Summary description for rptBillFormatG.
    /// </summary>
    public partial class rptBillFormatG : BaseSectionReport
    {
        public rptBillFormatG()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            Name = "Laser Format";
        }

        public static rptBillFormatG InstancePtr
        {
            get
            {
                return (rptBillFormatG)Sys.GetInstance(typeof(rptBillFormatG));
            }
        }

        protected rptBillFormatG _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                clsMortgageAssociations.Dispose();
                clsMortgageHolders.Dispose();
                clsRateRecs.Dispose();
                clsBills.Dispose();
                rsMultiRecipients.Dispose();
            }
            base.Dispose(disposing);
        }

        string strDefaultDeviceName = "";
       // clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
        bool boolFirstBill;
        clsDRWrapper clsMortgageHolders = new clsDRWrapper();
        clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
        clsDRWrapper clsRateRecs = new clsDRWrapper();
        bool boolUsePreviousOwner;
        clsBillFormat clsTaxBillFormat = new clsBillFormat();
        clsRateRecord clsTaxRate = new clsRateRecord();
        double TaxRate;
        int intStartPage;
        clsDRWrapper clsBills = new clsDRWrapper();
        bool boolREBill;
        bool boolPPBill;
        double Tax1;
        double Tax2;
        double Tax3;
        double Tax4;
        // vbPorter upgrade warning: Prepaid As double	OnWrite(string, double)
        double Prepaid;
        double DistPerc1;
        double DistPerc2;
        double DistPerc3;
        double DistPerc4;
        double DistPerc5;
        double DistPerc6;
        double DistPerc7;
        int intDistCats;
        double dblDiscountPercent;
        string strCat1 = "";
        string strCat2 = "";
        string strCat3 = "";
        int lngExempt;
        int lngTotAssess;
        bool boolBlank;

        bool boolNotJustUnloading;
        bool boolReprintNewOwnerCopy;
        /// <summary>
        /// this will be set to true if there is a new owner and the recordset will not be advanced to create a copy for him
        /// </summary>
        bool boolPrintRecipientCopy;
        bool boolPP;
        clsDRWrapper rsMultiRecipients = new clsDRWrapper();
        bool boolIsRegional;
        private bool boolExport;
        private struct AddrType
        {
            public string[] strAddress/*?  = new string[4 + 1] */;
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public AddrType(int unusedParam)
            {
                strAddress = new string[5] {
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty
                };
            }
        };

        AddrType[] aryReturnAddress = null;

        public void LoadReturnAddresses()
        {
            using (var rsLoad = new clsDRWrapper())
            {
                if (!modRegionalTown.IsRegionalTown())
                {
                    aryReturnAddress = new AddrType[1 + 1];
                    //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                    aryReturnAddress[0] = new AddrType(0);
                    boolIsRegional = false;
                    rsLoad.OpenRecordset("select * from returnaddress", modGlobalVariables.strBLDatabase);
                    aryReturnAddress[0].strAddress[0] = FCConvert.ToString(rsLoad.Get_Fields_String("address1"));
                    aryReturnAddress[0].strAddress[1] = FCConvert.ToString(rsLoad.Get_Fields_String("address2"));
                    aryReturnAddress[0].strAddress[2] = FCConvert.ToString(rsLoad.Get_Fields_String("address3"));
                    aryReturnAddress[0].strAddress[3] = FCConvert.ToString(rsLoad.Get_Fields_String("address4"));
                    if (Strings.Trim(aryReturnAddress[0].strAddress[2]) == "")
                    {
                        aryReturnAddress[0].strAddress[2] = Strings.Trim(aryReturnAddress[0].strAddress[3]);
                        aryReturnAddress[0].strAddress[3] = "";
                    }

                    if (Strings.Trim(aryReturnAddress[0].strAddress[1]) == "")
                    {
                        aryReturnAddress[0].strAddress[1] = Strings.Trim(aryReturnAddress[0].strAddress[2]);
                        aryReturnAddress[0].strAddress[2] = Strings.Trim(aryReturnAddress[0].strAddress[3]);
                        aryReturnAddress[0].strAddress[3] = "";
                    }

                    if (Strings.Trim(aryReturnAddress[0].strAddress[0]) == "")
                    {
                        aryReturnAddress[0].strAddress[0] = Strings.Trim(aryReturnAddress[0].strAddress[1]);
                        aryReturnAddress[0].strAddress[1] = Strings.Trim(aryReturnAddress[0].strAddress[2]);
                        aryReturnAddress[0].strAddress[2] = Strings.Trim(aryReturnAddress[0].strAddress[3]);
                        aryReturnAddress[0].strAddress[3] = "";
                    }
                }
                else
                {
                    var x = 0;
                    x = 0;
                    boolIsRegional = true;
                    rsLoad.OpenRecordset("select * from TBLREgions where townnumber > 0 order by townname",
                        "CentralData");
                    var townNumber = rsLoad.Get_Fields("townnumber");

                    while (!rsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        if (Conversion.Val(townNumber) > x)
                        {
                            var oldX = x;
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            x = FCConvert.ToInt32(Math.Round(Conversion.Val(townNumber)));
                            Array.Resize(ref aryReturnAddress, x + 1);
                            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                            for (var i = oldX; i < x; i++)
                            {
                                aryReturnAddress[i] = new AddrType(0);
                            }
                        }

                        rsLoad.MoveNext();
                    }

                    rsLoad.OpenRecordset("select * from returnaddress order by townnumber",
                        modGlobalVariables.strBLDatabase);
                    while (!rsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        aryReturnAddress[townNumber].strAddress[0] =
                            FCConvert.ToString(rsLoad.Get_Fields_String("address1"));
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        aryReturnAddress[townNumber].strAddress[1] =
                            FCConvert.ToString(rsLoad.Get_Fields_String("address2"));
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        aryReturnAddress[townNumber].strAddress[2] =
                            FCConvert.ToString(rsLoad.Get_Fields_String("address3"));
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        aryReturnAddress[townNumber].strAddress[3] =
                            FCConvert.ToString(rsLoad.Get_Fields_String("address4"));
                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        if (Strings.Trim(aryReturnAddress[FCConvert.ToString(townNumber)].strAddress[2]) == "")
                        {
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[2] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(townNumber))].strAddress[3]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[3] = "";
                        }

                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        if (Strings.Trim(aryReturnAddress[FCConvert.ToString(townNumber)].strAddress[1]) == "")
                        {
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[1] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(townNumber))].strAddress[2]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[2] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(townNumber))].strAddress[3]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[3] = "";
                        }

                        // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                        if (Strings.Trim(aryReturnAddress[FCConvert.ToString(townNumber)].strAddress[0]) == "")
                        {
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[0] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(townNumber))].strAddress[1]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[1] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(townNumber))].strAddress[2]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[2] = Strings.Trim(
                                aryReturnAddress[FCConvert.ToString(Conversion.Val(townNumber))].strAddress[3]);
                            // TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
                            aryReturnAddress[townNumber].strAddress[3] = "";
                        }

                        rsLoad.MoveNext();
                    }
                }
            }
        }
        // vbPorter upgrade warning: intYear As short	OnWrite(double, int)
        public void Init(clsBillFormat clsTaxFormat, int intYear, string strFontName, bool boolShow = true, string strPrinterN = "", bool boolModal = false, bool boolExportOnly = false, string strExportFile = "")
        {
            int x;
            var clsTemp = new clsDRWrapper();
            var strTemp = "";
            string strSql;

            boolExport = boolExportOnly;
            boolNotJustUnloading = true;
            boolBlank = clsTaxFormat.Billformat == 15;
            if (strPrinterN != string.Empty)
            {
                Document.Printer.PrinterName = strPrinterN;
            }
            if (clsTaxFormat.BillsFrom == 0)
            {
                boolREBill = true;
                boolPPBill = clsTaxFormat.Combined;
            }
            else
            {
                boolREBill = false;
                boolPPBill = true;
            }

            try
            {
                if (boolBlank)
                {
                    txtFirstPayment.Visible = true;
                    txtRemit1.Visible = true;
                    txtFirstHalfDue.Visible = true;
                    txtLocLabel.Visible = true;
                    txtMapLotLabel.Visible = true;
                    shapeInformation.Visible = true;
                    Line1.Visible = true;
                    Line2.Visible = true;
                    txtTaxBillYear.Visible = true;
                    txtTaxBillYear.Text = FCConvert.ToString(intYear) + " Tax Bill";
                    if (clsTaxFormat.UseClearLaserBackground)
                    {

                        txtStub11.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub11.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub12.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub12.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub13.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub13.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub21.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub21.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub22.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub22.BackColor = Color.FromArgb(255, 255, 255);
                        txtStub23.ForeColor = Color.FromArgb(1, 1, 1);
                        txtStub23.BackColor = Color.FromArgb(255, 255, 255);
                    }
                    else
                    {
                        //FC:FINAL:MSH - issue #1534: set default color values for textboxes
                        txtStub11.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub11.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub12.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub12.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub13.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub13.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub21.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub21.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub22.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub22.BackColor = Color.FromArgb(1, 1, 1);
                        txtStub23.ForeColor = Color.FromArgb(255, 255, 255);
                        txtStub23.BackColor = Color.FromArgb(1, 1, 1);
                    }
                }
                clsTemp.OpenRecordset("select * from billsetup", "twbl0000.vb1");
                if (!clsTemp.EndOfFile())
                {
                    if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("showpicture")))
                    {
                        if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicture"))) != string.Empty)
                        {
                            if (File.Exists(Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicturepath"))) + Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicture")))))
                            {
                                Image1.Image = FCUtils.LoadPicture(Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicturepath"))) + Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("billpicture"))));
                                Image1.Visible = true;
                                if (clsTaxFormat.ReturnAddress == false)
                                {
                                    // there is room to expand the logo to the right
                                    double dblPicRatio = 0;
                                    if (Image1.Image.Width > Image1.Image.Height)
                                    {
                                        if (Image1.Image.Height > 0)
                                        {
                                            dblPicRatio = Image1.Image.Width / Image1.Image.Height;
                                            Image1.Width = FCConvert.ToSingle(dblPicRatio * Image1.Width);
                                            Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Image1.Image = null;
                        }
                    }
                    else
                    {
                        Image1.Image = null;
                    }
                }
                else
                {
                    Image1.Image = null;
                }

                clsTaxBillFormat.CopyFormat(ref clsTaxFormat);

                SetupGrids();
                if (!boolREBill)
                {
                    txtBookPage.Visible = false;
                }
                if (boolREBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
                {
                    boolFirstBill = true;
                    clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
                    clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT", "CentralData");
                    if (!clsMortgageAssociations.EndOfFile())
                    {
                        clsMortgageAssociations.MoveFirst();
                        clsMortgageAssociations.MovePrevious();
                    }
                }
                else
                {
                    boolFirstBill = true;
                }
                strSql = clsTaxBillFormat.SQLStatement;
                clsTaxRate.TaxYear = intYear;
                // lblTitle.Caption = intYear
                clsRateRecs.OpenRecordset("select * from raterec where year = " + FCConvert.ToString(intYear), "twcl0000.vb1");
                if (!clsRateRecs.EndOfFile())
                {
                    clsTaxRate.LoadRate(FCConvert.ToInt32(clsRateRecs.Get_Fields_Int32("ID")));
                }
                clsTemp.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
                    switch (FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("line"))))
                    {
                        case 0:
                            {
                                // GridInformation.TextMatrix(1, 0) = clsTemp.Fields("message")
                                txtInfoLbl1.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 1:
                            {
                                // GridInformation.TextMatrix(2, 0) = clsTemp.Fields("message")
                                txtInfoLbl2.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 2:
                            {
                                // GridInformation.TextMatrix(3, 0) = clsTemp.Fields("message")
                                txtInfoLbl3.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 3:
                            {
                                // GridInformation.TextMatrix(4, 0) = clsTemp.Fields("message")
                                txtInfoLbl4.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 4:
                            {
                                // GridInformation.TextMatrix(5, 0) = clsTemp.Fields("message")
                                txtInfoLbl5.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 5:
                            {
                                // GridInformation.TextMatrix(6, 0) = clsTemp.Fields("message")
                                txtInfoLbl6.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 6:
                            {
                                // GridInformation.TextMatrix(7, 0) = clsTemp.Fields("message")
                                txtInfoLbl7.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 7:
                            {
                                // GridInformation.TextMatrix(8, 0) = clsTemp.Fields("message")
                                txtInfoLbl8.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 8:
                            {
                                // GridInformation.TextMatrix(9, 0) = clsTemp.Fields("message")
                                txtInfoLbl9.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 9:
                            {
                                // GridInformation.TextMatrix(10, 0) = clsTemp.Fields("message")
                                txtInfoLbl10.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 10:
                            {
                                // GridInformation.TextMatrix(11, 0) = clsTemp.Fields("message")
                                txtInfoLbl11.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 11:
                            {
                                // GridInformation.TextMatrix(12, 0) = clsTemp.Fields("message")
                                txtInfoLbl12.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                    }
                    //end switch
                    clsTemp.MoveNext();
                }
                clsTemp.OpenRecordset("select * from remittancemessage order by line", "twbl0000.vb1");
                while (!clsTemp.EndOfFile())
                {
                    // GridRemittance.TextMatrix(Val(clsTemp.Fields("line")) + 1, 0) = clsTemp.Fields("message")
                    // TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
                    switch (FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("line"))))
                    {
                        case 0:
                            {
                                txtRemitLbl1.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 1:
                            {
                                txtRemitLbl2.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 2:
                            {
                                txtRemitLbl3.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 3:
                            {
                                txtRemitLbl4.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 4:
                            {
                                txtRemitLbl5.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 5:
                            {
                                txtRemitLbl6.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                        case 6:
                            {
                                txtRemitLbl7.Text = FCConvert.ToString(clsTemp.Get_Fields_String("message"));
                                break;
                            }
                    }
                    //end switch
                    clsTemp.MoveNext();
                }
                if (clsTaxBillFormat.Discount)
                {
                    clsTemp.OpenRecordset("select * from discount", "twbl0000.vb1");
                    if (!clsTemp.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
                        dblDiscountPercent = Conversion.Val(clsTemp.Get_Fields("discount")) / 100;
                        // TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
                        txtDiscount.Text = Strings.Format(clsTemp.Get_Fields("discount"), "#0.00") + "%";
                        // TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
                        txtDiscountDate.Text = FCConvert.ToString(clsTemp.Get_Fields("duedate"));
                        txtDiscountMsg1.Visible = true;
                        txtDiscountMsg2.Visible = true;
                        txtDiscount.Visible = true;
                        txtDiscountDate.Visible = true;
                        txtDiscountAmount.Visible = true;
                    }
                }

                if (clsTaxRate.NumberOfPeriods < 2)
                {
                    // framStub2.Visible = False
                    // txtPayment2Label.Visible = False
                    txtAmount2.Visible = false;
                    txtDueDate2.Visible = false;
                    if (boolBlank)
                    {
                        txtNA.Visible = true;
                    }
                }
                else
                {
                    // framStub2.Visible = True
                    txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
                    txtBillYearStub2.Visible = true;
                    //CustomGridStubInfo2.Visible = true;
                    txtAmount2.Visible = true;
                    txtDueDate2.Visible = true;
                    // txtPayment2Label.Visible = True
                    txtSecondDue.Visible = true;
                    txtSecondDue.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
                    txtSecondAmountDue.Visible = true;
                    if (boolBlank)
                    {
                        txtSecondPayment.Visible = true;
                        txtRemit2.Visible = true;
                        txtSecondHalfDue.Visible = true;
                        txtFirstHalfDue.Text = "First Half Due";
                    }
                }
                // End If
                txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
                txtFirstDue.Text = txtDueDate1.Text;

                if (clsTaxBillFormat.UseLaserAlignment && Conversion.Val(clsTaxBillFormat.LaserAlignment) != 0)
                {
                    for (x = 0; x <= Detail.Controls.Count - 1; x++)
                    {
                        if (FCConvert.ToString(Detail.Controls[x].Tag) == "Line")
                        {
                            (Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 += FCConvert.ToSingle(240 * clsTaxBillFormat.LaserAlignment) / 1440F;
                            (Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Line).Y2 += FCConvert.ToSingle(240 * clsTaxBillFormat.LaserAlignment) / 1440F;
                        }
                        else
                        {
                            Detail.Controls[x].Top += FCConvert.ToSingle(240 * clsTaxBillFormat.LaserAlignment) / 1440F;
                        }
                    }
                }

                GetBills(ref strSql);

                if (!boolShow)
                {
                    if (boolExport)
                    {
                        var pdf = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();

                        //frmWait.InstancePtr.Init(pdf.FileName + " is being created", false);
                        Run(false);
                        this.ExportToPDF(strExportFile);
                        frmWait.InstancePtr.Unload();

                        //MessageBox.Show("Created pdf file " + strExportFileName, "File Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        this.PrintReport(false);
                    }
                }
            }
            finally
            {
                clsTemp.DisposeOf();
            }
        }

        private void GetBills(ref string strSql)
        {
            clsBills.OpenRecordset(strSql, "twcl0000.vb1");
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = clsBills.EndOfFile();
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            //Application.DoEvents();
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            int const_printtoolid;

            const_printtoolid = 9950;

            if (clsTaxBillFormat.ReturnAddress)
            {
                LoadReturnAddresses();

                if (!boolIsRegional)
                {
                    txtAddress1.Text = aryReturnAddress[0].strAddress[0];
                    txtAddress2.Text = aryReturnAddress[0].strAddress[1];
                    txtAddress3.Text = aryReturnAddress[0].strAddress[2];
                    txtAddress4.Text = aryReturnAddress[0].strAddress[3];
                }

                if (txtAddress3.Text == "")
                {
                    txtAddress3.Text = txtAddress4.Text;
                    txtAddress4.Text = "";
                }
                if (txtAddress2.Text == "")
                {
                    txtAddress2.Text = txtAddress3.Text;
                    txtAddress3.Text = txtAddress4.Text;
                    txtAddress4.Text = "";
                }

            }
            else
            {
                txtAddress1.Visible = false;
                txtAddress2.Visible = false;
                txtAddress3.Visible = false;
                txtAddress4.Visible = false;
            }
        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {

        }

        private void Detail_Format(object sender, EventArgs e)
        {
            double Dist1 = 0;
            double Dist2 = 0;
            double Dist3 = 0;
            double Dist4 = 0;
            double Dist5 = 0;
            double Dist6 = 0;
            double Dist7 = 0;
            // vbPorter upgrade warning: TotTax As double	OnWrite(double, string)
            double TotTax = 0;
            var strTemp = "";
            var strAcct = "";
            var strMapLot = "";
            var strName = "";
            var strLocation = "";
            DateTime dtLastDate;
            var strOldName = "";
            var strSecOwner = "";
            var strAddr1 = "";
            var strAddr2 = "";
            var strAddr3 = "";
            var strMailingAddress3 = "";
            // vbPorter upgrade warning: dblTemp As double	OnWrite(string, double, short)
            double dblTemp = 0;
            string strCountry;
            strCountry = "";
            txtMailing6.Text = "";
            txtValLbl10.Text = "";

            if (clsBills.EndOfFile()) return;

            var billingType = FCConvert.ToString(clsBills.Get_Fields_String("Billingtype"));

            switch (billingType)
            {
                case "RE":
                    modGlobalVariables.Statics.boolRE = true;
                    boolREBill = true;
                    boolPP = false;
                    boolPPBill = false;

                    break;
                case "PP":
                    modGlobalVariables.Statics.boolRE = false;
                    boolREBill = false;
                    boolPP = true;
                    boolPPBill = true;

                    break;
                default:
                    // combined
                    modGlobalVariables.Statics.boolRE = true;
                    boolREBill = true;
                    boolPP = true;
                    boolPPBill = true;

                    break;
            }

            var rateKey = FCConvert.ToInt32(clsBills.Get_Fields_Int32("ratekey"));

            if (clsRateRecs.FindFirstRecord("ID", rateKey))
            {
                clsTaxRate.LoadRate(rateKey, clsRateRecs);
                if (clsTaxRate.NumberOfPeriods < 2)
                {
                    // framStub2.Visible = False
                    // txtPayment2Label.Visible = False
                    txtAmount2.Visible = false;
                    txtDueDate2.Visible = false;
                    txtSecondDue.Visible = false;
                    if (boolBlank)
                    {
                        txtNA.Visible = true;
                    }
                    else
                    {
                        // these towns seem to be setup a little different
                        txtBookPageLabel.Visible = false;
                        txtFirstDue.Top = txtMailing5.Top + 563 / 1440F;
                        txtFirstAmountDue.Top = txtFirstDue.Top;
                        txtAcres.Top = txtMailing5.Top + 563 / 1440F;
                        lblAcres.Top = txtAcres.Top;
                        txtMapLot.Top = txtAcres.Top + 225 / 1440F;
                        txtBookPage.Top = txtMapLot.Top;
                        txtLocation.Top = txtMapLot.Top + 225 / 1440F;
                        txtBillYearStub1.Top = txtRemit1.Top;
                        //CustomGridStubInfo1.Top = txtBillYearStub1.Top + 270 / 1440F;
                        txtStubInfo111.Top = txtBillYearStub1.Top + 270 / 1440F;
                        txtStubInfo112.Top = txtBillYearStub1.Top + 270 / 1440F;
                        txtStubInfo121.Top = txtStubInfo111.Top + txtStubInfo111.Height;
                        txtStubInfo122.Top = txtStubInfo112.Top + txtStubInfo112.Height;
                        txtStubInfo131.Top = txtStubInfo121.Top + txtStubInfo121.Height;
                        txtStubInfo132.Top = txtStubInfo122.Top + txtStubInfo122.Height;
                        txtStubInfo141.Top = txtStubInfo131.Top + txtStubInfo131.Height;
                        txtStubInfo142.Top = txtStubInfo132.Top + txtStubInfo132.Height;
                        txtDueDate1.Top = txtBillYearStub1.Top + 180 / 1440F;
                        txtAmount1.Top = txtDueDate1.Top;
                    }
                    //CustomGridStubInfo2.Visible = false;
                    txtStubInfo211.Visible = false;
                    txtStubInfo221.Visible = false;
                    txtStubInfo231.Visible = false;
                    txtStubInfo241.Visible = false;
                    txtStubInfo212.Visible = false;
                    txtStubInfo222.Visible = false;
                    txtStubInfo232.Visible = false;
                    txtStubInfo242.Visible = false;
                    txtSecondPayment.Visible = false;
                    txtRemit2.Visible = false;
                    txtSecondAmountDue.Visible = false;
                    txtSecondHalfDue.Visible = false;
                    txtFirstHalfDue.Text = "Payment Due";
                }
                else
                {
                    // framStub2.Visible = True
                    txtNA.Visible = false;
                    txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
                    txtBillYearStub2.Visible = true;
                    //CustomGridStubInfo2.Visible = true;
                    txtAmount2.Visible = true;
                    txtDueDate2.Visible = true;
                    // txtPayment2Label.Visible = True
                    txtSecondDue.Visible = true;
                    txtSecondDue.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
                    if (boolBlank)
                    {
                        txtSecondPayment.Visible = true;
                        txtRemit2.Visible = true;
                        txtSecondHalfDue.Visible = true;
                        txtFirstHalfDue.Text = "First Half Due";
                        txtSecondAmountDue.Visible = true;
                    }
                }
                // End If
                txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
                txtFirstDue.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
            }
            lngTotAssess = 0;
            lngExempt = 0;
            Tax1 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue1"));
            Tax2 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue2"));
            Tax3 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue3"));
            Tax4 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue4"));
            TotTax = Tax1 + Tax2 + Tax3 + Tax4;
            // GridValuation.TextMatrix(13, 1) = Format(TotTax, "#,###,###,##0.00")
            txtValData13.Text = Strings.Format(TotTax, "#,###,###,##0.00");
            const string longCurrencyFormat = "##,###,##0.00";

            if (clsTaxBillFormat.BreakdownDollars)
            {
                Dist1 = TotTax * DistPerc1;
                Dist2 = TotTax * DistPerc2;
                Dist3 = TotTax * DistPerc3;
                Dist4 = TotTax * DistPerc4;
                Dist5 = TotTax * DistPerc5;
                Dist6 = TotTax * DistPerc6;
                Dist7 = TotTax * DistPerc7;
                var distSum = Dist1 + Dist2 + Dist3 + Dist4 + Dist5 + Dist6 + Dist7;

                if (distSum != TotTax)
                {
                    // correct it so the sum of the dists is equal to the total tax
                    var difference = TotTax - distSum;

                    if (Dist7 > 0)
                        Dist7 += difference;
                    else if (Dist6 > 0)
                            Dist6 += difference;
                        else if (Dist5 > 0)
                                Dist5 += difference;
                            else if (Dist4 > 0)
                                    Dist4 += difference;
                                else if (Dist3 > 0)
                                        Dist3 += difference;
                                    else if (Dist2 > 0)
                                            Dist2 += difference;
                                        else if (Dist1 > 0) Dist1 += difference;
                }

                txtDistAm1.Text = Strings.Format(Dist1, longCurrencyFormat);
                if (intDistCats > 1) txtDistAm2.Text = Strings.Format(Dist2, longCurrencyFormat);

                if (intDistCats > 2)
                    txtDistAm3.Text = Strings.Format(Dist3, longCurrencyFormat);
                if (intDistCats > 3)
                    txtDistAm4.Text = Strings.Format(Dist4, longCurrencyFormat);
                if (intDistCats > 4)
                    txtDistAm5.Text = Strings.Format(Dist5, longCurrencyFormat);
                if (intDistCats > 5)
                    txtDistAm6.Text = Strings.Format(Dist6, longCurrencyFormat);
                if (intDistCats > 6)
                    txtDistAm7.Text = Strings.Format(Dist7, longCurrencyFormat);
            }

            const string twoDecimalFormat = "0.00";
            Prepaid = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid")), twoDecimalFormat));
            TotTax = FCConvert.ToDouble(Strings.Format(TotTax, twoDecimalFormat));
            double dblAbated = 0;
            double dblAbate1 = 0;
            double dblAbate2 = 0;
            double dblAbate3 = 0;
            double dblAbate4 = 0;
            var dblTAbate = new double[4 + 1];
            dblAbated = 0;
            if ((Tax2 > 0 || Tax3 > 0 || Tax4 > 0) && Prepaid > 0)
            {
                dblAbated = modCollectionsRelated.AutoAbatementAmount_8(FCConvert.ToInt32(clsBills.Get_Fields_Int32("ID")), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
                dblAbate1 = dblTAbate[1];
                dblAbate2 = dblTAbate[2];
                dblAbate3 = dblTAbate[3];
                dblAbate4 = dblTAbate[4];
                if (boolREBill && boolPPBill)
                {
                    // TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
                    dblAbated += modCollectionsRelated.AutoAbatementAmount_8(FCConvert.ToInt32(clsBills.Get_Fields("ppbillkey")), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
                    dblAbate1 += dblTAbate[1];
                    dblAbate2 += dblTAbate[2];
                    dblAbate3 += dblTAbate[3];
                    dblAbate4 += dblTAbate[4];
                }
                Prepaid -= dblAbated;
            }
            if (dblAbated > 0)
            {
                Tax1 -= dblAbate1;
                Tax2 -= dblAbate2;
                Tax3 -= dblAbate3;
                Tax4 -= dblAbate4;
            }
            if (Prepaid > 0)
            {
                if (Prepaid > Tax1)
                {
                    Prepaid -= Tax1;
                    Tax1 = 0;
                    if (Prepaid > Tax2)
                    {
                        Prepaid -= Tax2;
                        Tax2 = 0;
                        if (Prepaid > Tax3)
                        {
                            Prepaid -= Tax3;
                            Tax3 = 0;
                            if (Prepaid > Tax4)
                            {
                                Prepaid -= Tax4;
                                Tax4 = 0;
                            }
                            else
                            {
                                Tax4 -= Prepaid;
                            }
                        }
                        else
                        {
                            Tax3 -= Prepaid;
                        }
                    }
                    else
                    {
                        Tax2 -= Prepaid;
                    }
                }
                else
                {
                    Tax1 -= Prepaid;
                }
            }
            Prepaid = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid")), twoDecimalFormat));
            if (Prepaid == 0)
            {
                txtValLbl12.Text = "";
                txtValData12.Text = "";
                txtValLbl10.Text = "";
                txtValData10.Text = "";
            }
            else
            {
                txtValLbl10.Text = "Original Bill";
                txtValData10.Text = Strings.Format(TotTax, "#,###,###,##0.00");
                txtValLbl12.Text = "Paid To Date";
                txtValData12.Text = Strings.Format(Prepaid, longCurrencyFormat);
                txtValData13.Text = Prepaid > TotTax ? "Overpaid" : Strings.Format(TotTax - Prepaid, longCurrencyFormat);
            }

            txtValData11.Text = Strings.Format(clsTaxRate.TaxRate * 1000, "#0.000");
            txtAmount1.Text = Strings.Format(Tax1, longCurrencyFormat);
            txtAmount2.Text = Strings.Format(Tax2, longCurrencyFormat);
            txtFirstAmountDue.Text = txtAmount1.Text;
            txtSecondAmountDue.Text = txtAmount2.Text;
            if (clsTaxBillFormat.Discount)
            {
                double dblDisc = 0;
                dblDisc = modMain.Round(TotTax * dblDiscountPercent, 2);
                txtDiscountAmount.Text = Strings.Format(TotTax - FCConvert.ToDouble(Strings.Format(dblDisc, twoDecimalFormat)), longCurrencyFormat);
                dblTemp = FCConvert.ToDouble(Strings.Format(TotTax - FCConvert.ToDouble(Strings.Format(dblDisc, twoDecimalFormat)), twoDecimalFormat));
                if (Prepaid > TotTax)
                    txtDiscountAmount.Text = twoDecimalFormat;
                else
                    if (Prepaid > 0)
                    {
                        dblTemp -= Prepaid;
                        if (dblTemp < 0)
                            dblTemp = 0;
                        txtDiscountAmount.Text = Strings.Format(dblTemp, longCurrencyFormat);
                    }
                    else
                    {
                        txtDiscountAmount.Text = Strings.Format(dblTemp, longCurrencyFormat);
                    }
            }
            // 
            strAcct = "";
            txtAcres.Visible = false;
            lblAcres.Visible = false;
            txtMailing5.Text = "";
            if (clsTaxBillFormat.ReturnAddress)
                if (boolIsRegional)
                {
                    var intTemp = 0;
                    // TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
                    intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields("trancode"))));
                    if (intTemp > 0)
                    {
                        txtAddress1.Text = aryReturnAddress[intTemp].strAddress[0];
                        txtAddress2.Text = aryReturnAddress[intTemp].strAddress[1];
                        txtAddress3.Text = aryReturnAddress[intTemp].strAddress[2];
                        txtAddress4.Text = aryReturnAddress[intTemp].strAddress[3];
                    }
                }

            var billsName1 = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));

            const string mediumNumericFormat = "#,###,###,##0";

            if (boolREBill)
            {
                txtTaxBillYear.Text = FCConvert.ToString(clsTaxRate.TaxYear) + " Real Estate Tax Bill";
                // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                strAcct = "R" + clsBills.Get_Fields("account");
                // If boolBlank Then
                txtAcres.Visible = true;
                lblAcres.Visible = true;
                txtAcres.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Double("acres")), "#,##0.00");
                // End If
                if (Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage"))) != string.Empty)
                {
                    txtBookPage.Visible = true;
                    if (clsTaxRate.NumberOfPeriods < 2 && !boolBlank)
                        txtBookPageLabel.Visible = false;
                    else
                        txtBookPageLabel.Visible = true;
                    txtBookPage.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage")));
                }
                else
                {
                    txtBookPage.Visible = false;
                    txtBookPageLabel.Visible = false;
                }
                txtValData1.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")), mediumNumericFormat);
                txtValData2.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")), "#,###,###,###,##0");
                lngTotAssess = FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")) + Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")));
                txtMailing6.Text = "";
                if (boolPrintRecipientCopy)
                {
                    txtMailing1.Text = billsName1;
                    txtMailing2.Text = "C/O " + rsMultiRecipients.Get_Fields_String("Name");
                    txtMailing3.Text = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("address1"));
                    txtMailing4.Text = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("address2"));
                    txtMailing5.Text = Strings.Trim($"{rsMultiRecipients.Get_Fields_String("city")} {rsMultiRecipients.Get_Fields_String("state")}  {rsMultiRecipients.Get_Fields_String("zip")} {rsMultiRecipients.Get_Fields_String("zip4")}");
                    // txtMailing5.Text = ""
                }
                else
                {
                    if (!boolFirstBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
                    {
                        clsMortgageHolders.FindFirstRecord("ID", clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"));
                        if (!clsMortgageHolders.NoMatch)
                        {
                            txtMailing1.Text = billsName1;
                            txtMailing2.Text = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
                            txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
                            txtMailing5.Text = Strings.Trim(clsMortgageHolders.Get_Fields_String("city") + " " + clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
                            txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
                        }
                        else
                        {
                            txtMailing1.Text = "Mortgage Holder Not Found";
                            txtMailing2.Text = "";
                            txtMailing3.Text = "";
                            txtMailing4.Text = "";
                            txtMailing5.Text = "";
                        }
                    }
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    else
                    {
                        if (boolReprintNewOwnerCopy && modMain.HasNewOwner_28439(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3, ref strCountry, ref strMailingAddress3))
                        {
                            txtMailing1.Text = strOldName;
                            txtMailing2.Text = strSecOwner;
                            txtMailing3.Text = strAddr1;
                            txtMailing4.Text = strAddr2;
                            if (strMailingAddress3 != "")
                            {
                                txtMailing5.Text = strMailingAddress3;
                                txtMailing6.Text = strAddr3;
                            }
                            else
                            {
                                txtMailing5.Text = strAddr3;
                                txtMailing6.Text = strCountry;
                            }
                        }
                        else
                        {
                            txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
                            txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
                            txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("mailingaddress3")));
                            txtMailing6.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
                            txtMailing1.Text = billsName1;
                            strName = billsName1;
                            txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name2")));
                        }
                    }
                }

                if (txtMailing5.Text == "")
                {
                    txtMailing5.Text = txtMailing6.Text;
                    txtMailing6.Text = "";
                }
                if (txtMailing4.Text == "")
                {
                    txtMailing4.Text = txtMailing5.Text;
                    txtMailing5.Text = txtMailing6.Text;
                    txtMailing6.Text = "";
                }
                if (txtMailing3.Text == "")
                {
                    txtMailing3.Text = txtMailing4.Text;
                    txtMailing4.Text = txtMailing5.Text;
                    txtMailing5.Text = txtMailing6.Text;
                    txtMailing6.Text = "";
                }
                if (txtMailing2.Text == "")
                {
                    txtMailing2.Text = txtMailing3.Text;
                    txtMailing3.Text = txtMailing4.Text;
                    txtMailing4.Text = txtMailing5.Text;
                    txtMailing5.Text = txtMailing6.Text;
                    txtMailing6.Text = "";
                }
                // TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
                strLocation = Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0
                    ? (string)FCConvert.ToString(clsBills.Get_Fields("streetnumber"))
                    : "";
                strLocation = Strings.Trim(Strings.Trim(strLocation + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
                txtLocation.Text = strLocation;
                txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("maplot")));
                strMapLot = txtMapLot.Text;
                lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
            }
            if (boolPPBill)
            {
                // pp
                if (boolREBill)
                {
                    var ppac = clsBills.Get_Fields("ppac");

                    if (Conversion.Val(ppac) > 0)
                    {
                        strAcct += " P";
                        strAcct += FCConvert.ToString(Conversion.Val(ppac));
                        txtTaxBillYear.Text = $"{FCConvert.ToString(clsTaxRate.TaxYear)} Combined Tax Bill";
                    }
                }
                else
                {
                    strAcct = "P";
                    strAcct += FCConvert.ToString(clsBills.Get_Fields("account"));
                    txtTaxBillYear.Text = FCConvert.ToString(clsTaxRate.TaxYear) + " Personal Property Tax Bill";
                }
                txtValData3.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category1")), mediumNumericFormat);
                txtValData4.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category2")), mediumNumericFormat);
                txtValData5.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category3")), mediumNumericFormat);
                txtValData6.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category4")) + Conversion.Val(clsBills.Get_Fields_Int32("category5")) + Conversion.Val(clsBills.Get_Fields_Int32("category6")) + Conversion.Val(clsBills.Get_Fields_Int32("category7")) + Conversion.Val(clsBills.Get_Fields_Int32("category8")) + Conversion.Val(clsBills.Get_Fields_Int32("category9")), mediumNumericFormat);
                lngTotAssess += FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("ppassessment")));
                if (!boolREBill)
                {
                    // TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
                    strLocation = Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0
                        ? (string)FCConvert.ToString(clsBills.Get_Fields("streetnumber"))
                        : "";
                    strLocation = Strings.Trim(Strings.Trim(strLocation + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
                    txtLocation.Text = strLocation;
                    // used for pp location
                    txtBookPage.Visible = false;
                    txtBookPageLabel.Visible = false;
                    strName = billsName1;
                    strMapLot = "";
                    txtMapLot.Text = "";
                    txtMailing6.Text = "";
                    if (boolPrintRecipientCopy)
                    {
                        txtMailing1.Text = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("Name"));
                        txtMailing2.Text = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("address1"));
                        txtMailing3.Text = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("address2"));
                        txtMailing4.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
                        txtMailing5.Text = "";
                    }
                    else
                    {
                        txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
                        txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
                        txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("mailingaddress3")));
                        txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
                        txtMailing1.Text = billsName1;
                        txtMailing6.Text = "";
                    }
                    if (txtMailing4.Text == "")
                    {
                        txtMailing4.Text = txtMailing5.Text;
                        txtMailing5.Text = txtMailing6.Text;
                        txtMailing6.Text = "";
                    }
                    if (txtMailing3.Text == "")
                    {
                        txtMailing3.Text = txtMailing4.Text;
                        txtMailing4.Text = txtMailing5.Text;
                        txtMailing5.Text = txtMailing6.Text;
                        txtMailing6.Text = "";
                    }
                    if (txtMailing2.Text == "")
                    {
                        txtMailing2.Text = txtMailing3.Text;
                        txtMailing3.Text = txtMailing4.Text;
                        txtMailing4.Text = txtMailing5.Text;
                        txtMailing5.Text = txtMailing6.Text;
                        txtMailing6.Text = "";
                    }
                }
            }

            if (boolREBill)
            {
                txtValLbl1.Text = "Land";
                txtValLbl2.Text = "Building";
            }
            else
            {
                txtValLbl1.Text = "";
                txtValLbl2.Text = "";
                txtValData1.Text = "";
                txtValData2.Text = "";
            }

            if (boolPPBill)
            {
                txtValLbl3.Text = strCat1;
                txtValLbl4.Text = strCat2;
                txtValLbl5.Text = strCat3;
                txtValLbl6.Text = "Other P/P";
            }
            else
            {
                txtValLbl3.Text = "";
                txtValLbl4.Text = "";
                txtValLbl5.Text = "";
                txtValLbl6.Text = "";
                txtValData3.Text = "";
                txtValData4.Text = "";
                txtValData5.Text = "";
                txtValData6.Text = "";
            }

            txtBillYearStub1.Text = txtTaxBillYear.Text;
            txtBillYearStub2.Text = txtTaxBillYear.Text;
            txtAccountAddress.Text = strAcct;

            txtStubInfo112.Text = strAcct;
            txtStubInfo122.Text = strName;
            txtStubInfo132.Text = strMapLot;
            txtStubInfo142.Text = strLocation;

            txtStubInfo212.Text = strAcct;
            txtStubInfo222.Text = strName;
            txtStubInfo232.Text = strMapLot;
            txtStubInfo242.Text = strLocation;
            lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));

            txtValData7.Text = Strings.Format(lngTotAssess, mediumNumericFormat);
            txtValData8.Text = Strings.Format(lngExempt, mediumNumericFormat);
            txtValData9.Text = Strings.Format(lngTotAssess - lngExempt, mediumNumericFormat);
            // txtAccount.Text = strAcct
            if (modGlobalVariables.Statics.boolRE)
            {
                if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
                {
                    boolReprintNewOwnerCopy = false;
                    // if the copy for a new owner needs to be sent, then check it here
                    if (clsTaxBillFormat.PrintCopyForNewOwner && boolFirstBill)
                    {
                        // if this is the main bill, only check for the new owner on the mail bill
                        dtLastDate = DateTime.Today.Month < 4
                            ? DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1))
                            : DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));

                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        if (modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1")))
                        // if there has been a new owner
                        // If NewOwner(clsBills.Fields("Account"), dtLastDate) Then     'if there has been a new owner
                            boolReprintNewOwnerCopy = true;
                        // End If
                    }
                }
                else
                {
                    if (!boolPrintRecipientCopy && boolFirstBill)
                    // turn off the copy so that the record can advance
                        boolReprintNewOwnerCopy = false;
                }

                if (!boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
                {
                    if (clsTaxBillFormat.PrintCopyForMultiOwners && boolFirstBill)
                    {
                        rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
                        if (!rsMultiRecipients.EndOfFile()) boolPrintRecipientCopy = true;
                    }
                }
                else
                {
                    if (boolPrintRecipientCopy && boolFirstBill)
                    {
                        rsMultiRecipients.MoveNext();
                        if (rsMultiRecipients.EndOfFile()) boolPrintRecipientCopy = false;
                    }
                }

                if (clsTaxBillFormat.PrintCopyForMortgageHolders && !boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
                {
                    if (!clsMortgageAssociations.EndOfFile())
                    {
                        if (boolFirstBill)
                            clsMortgageAssociations.FindFirstRecord("account", clsBills.Get_Fields("account"));
                        else
                            clsMortgageAssociations.FindNextRecord("account", clsBills.Get_Fields("account"));

                        if (clsMortgageAssociations.NoMatch)
                        {
                            boolFirstBill = true;
                            if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
                                clsBills.MoveNext();
                        }
                        else
                        {
                            boolFirstBill = false;
                        }
                    }
                    else
                    {
                        boolFirstBill = true;
                        if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
                            clsBills.MoveNext();
                    }
                }
                else
                {
                    if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
                        clsBills.MoveNext();
                }
            }
            else
            {
                if (!boolPrintRecipientCopy)
                {
                    if (clsTaxBillFormat.PrintCopyForMultiOwners)
                    {
                        rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
                        if (!rsMultiRecipients.EndOfFile()) boolPrintRecipientCopy = true;
                    }
                }
                else
                {
                    rsMultiRecipients.MoveNext();
                    if (rsMultiRecipients.EndOfFile()) boolPrintRecipientCopy = false;
                }
                if (!boolPrintRecipientCopy) clsBills.MoveNext();
            }
        }

        private void SetupGrids()
        {
            using (var clsTemp = new clsDRWrapper())
            {
                var strTemp = "";

                if (boolBlank)
                {
                    txtDistLbl0.Font = new Font(txtDistLbl0.Font, FontStyle.Bold);
                    // .MergeRow(0) = True
                    // .TextMatrix(0, 0) = "Current Billing Information"
                    // .TextMatrix(0, 1) = "Current Billing Information"
                    txtVallbl0.Text = "Current Billing Information";
                    // .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1) = flexAlignCenterCenter
                    // .Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1) = True
                    // .Cell(FCGrid.CellPropertySettings.flexcpFontName, 0, 0, 0, 1) = "Tahoma"
                    // show boxes too
                    shapeValuation.Visible = true;
                    ValuationLine1.Visible = true;
                    valuationLine2.Visible = true;
                    // Call GridValuation.Select(0, 0, 13, 1)
                    // Call GridValuation.CellBorder(RGB(1, 1, 1), 1, 1, 1, 1, 1, -1)
                    // .TextMatrix(13, 0) = "Total Due"
                    txtValLbl13.Text = "Total Due";
                    if (!clsTaxBillFormat.UseClearLaserBackground)
                    {
                        txtVallbl0.ForeColor = Color.FromArgb(255, 255, 255);
                        txtVallbl0.BackColor = Color.FromArgb(1, 1, 1);
                        //txtVallbl0.BackStyle = ddBKNormal;
                        txtDistLbl0.ForeColor = txtVallbl0.ForeColor;
                        txtDistLbl0.BackColor = txtVallbl0.BackColor;
                        //txtDistLbl0.BackStyle = ddBKNormal;
                        txtRemitLbl0.ForeColor = txtDistLbl0.ForeColor;
                        txtRemitLbl0.BackColor = txtDistLbl0.BackColor;
                        //txtRemitLbl0.BackStyle = ddBKNormal;
                        txtInfoLbl0.ForeColor = txtVallbl0.ForeColor;
                        txtInfoLbl0.BackColor = txtVallbl0.BackColor;
                        //txtInfoLbl0.BackStyle = ddBKNormal;
                        txtInfoLbl0.Font = new Font(txtInfoLbl0.Font, FontStyle.Bold);
                        // .Cell(FCGrid.CellPropertySettings.flexcpFontBold, 13, 0) = True
                        // .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 13, 0) = RGB(1, 1, 1)
                        // .Cell(FCGrid.CellPropertySettings.flexcpForeColor, 13, 0) = RGB(255, 255, 255)
                        txtValLbl13.Font = new Font(txtValLbl13.Font, FontStyle.Bold);
                        txtValLbl13.BackColor = Color.FromArgb(1, 1, 1);
                        //txtValLbl13.BackStyle = ddBKNormal;
                        txtValLbl13.ForeColor = Color.FromArgb(255, 255, 255);
                    }
                }

                txtValLbl7.Text = "Assessment";
                txtValLbl8.Text = "Exemption";
                txtValLbl9.Text = "Taxable";
                txtValLbl11.Text = "Rate Per $1000";
                txtValLbl12.Text = "Paid To Date";
                if (boolREBill)
                {
                    txtValLbl1.Text = "Land";
                    txtValLbl2.Text = "Building";
                }
                else
                {
                    // .TextMatrix(1, 0) = ""
                    // .TextMatrix(2, 0) = ""
                    txtValLbl1.Text = "";
                    txtValLbl2.Text = "";
                }

                if (boolPPBill)
                {
                    clsTemp.OpenRecordset("select * from RATIOTRENDS order by type", "PersonalProperty");
                    strCat1 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    // .TextMatrix(3, 0) = strCat1
                    txtValLbl3.Text = strCat1;
                    clsTemp.MoveNext();
                    strCat2 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    // .TextMatrix(4, 0) = strCat2
                    txtValLbl4.Text = strCat2;
                    clsTemp.MoveNext();
                    strCat3 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    // .TextMatrix(5, 0) = strCat3
                    txtValLbl5.Text = strCat3;
                    // .TextMatrix(6, 0) = "Other P/P"
                    txtValLbl6.Text = "Other P/P";
                }

                if (boolBlank)
                {
                    shapeDistribution.Visible = true;
                }

                // .Visible = False
                DistPerc1 = 0;
                DistPerc2 = 0;
                DistPerc3 = 0;
                DistPerc4 = 0;
                DistPerc5 = 0;
                if (clsTaxBillFormat.Breakdown)
                {
                    if (boolBlank)
                    {
                        txtDistLbl0.Text = "Current Billing Distribution";
                    }

                    // .Visible = True
                    clsTemp.OpenRecordset("select * from distributiontable order by distributionnumber",
                        "twbl0000.vb1");
                    if (!clsTemp.EndOfFile())
                    {
                        clsTemp.MoveLast();
                        clsTemp.MoveFirst();
                        intDistCats = clsTemp.RecordCount();

                        var distributionPercent = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
                        DistPerc1 = distributionPercent;
                        txtDistPerc1.Text = FormatAsPercent(DistPerc1);
                        DistPerc1 /= 100;

                        var distributionName = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
                        strTemp = distributionName;
                        txtDistLbl1.Text = strTemp;

                        if (intDistCats > 1)
                        {
                            clsTemp.MoveNext();
                            DistPerc2 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
                            txtDistPer2.Text = FormatAsPercent(DistPerc2);
                            DistPerc2 /= 100;
                            strTemp = clsTemp.Get_Fields_String("distributionname");
                            txtDistLbl2.Text = strTemp;
                            if (intDistCats > 2)
                            {
                                clsTemp.MoveNext();
                                DistPerc3 = clsTemp.Get_Fields_Double("distributionpercent");
                                txtDistPerc3.Text = FormatAsPercent(DistPerc3);
                                DistPerc3 /= 100;
                                strTemp = clsTemp.Get_Fields_String("distributionname");
                                // GridDistribution.TextMatrix(3, 0) = strTemp
                                txtDistLBL3.Text = strTemp;
                                if (intDistCats > 3)
                                {
                                    clsTemp.MoveNext();
                                    DistPerc4 = clsTemp.Get_Fields_Double("distributionpercent");
                                    txtDistPerc4.Text = FormatAsPercent(DistPerc4);
                                    DistPerc4 /= 100;
                                    strTemp = clsTemp.Get_Fields_String("distributionname");
                                    txtDistLbl4.Text = strTemp;
                                    if (intDistCats > 4)
                                    {
                                        clsTemp.MoveNext();
                                        DistPerc5 = clsTemp.Get_Fields_Double("distributionpercent");
                                        txtDistPerc5.Text = FormatAsPercent(DistPerc5);
                                        DistPerc5 /= 100;
                                        strTemp = clsTemp.Get_Fields_String("distributionname");
                                        txtDistLbl5.Text = strTemp;
                                        if (intDistCats > 5)
                                        {
                                            clsTemp.MoveNext();
                                            DistPerc6 = clsTemp.Get_Fields_Double("distributionpercent");
                                            txtDistPerc6.Text = FormatAsPercent(DistPerc6);
                                            DistPerc6 /= 100;
                                            strTemp = clsTemp.Get_Fields_String("distributionname");
                                            txtDistLbl6.Text = strTemp;
                                            if (intDistCats > 6)
                                            {
                                                clsTemp.MoveNext();
                                                DistPerc7 = clsTemp.Get_Fields_Double("distributionpercent");
                                                txtDistPerc7.Text = FormatAsPercent(DistPerc7);
                                                DistPerc7 /= 100;
                                                strTemp = clsTemp.Get_Fields_String("distributionname");
                                                txtDistLbl7.Text = strTemp;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        intDistCats = 0;
                    }
                }

                if (boolBlank)
                {
                    txtStub11.Text = "Due Date";
                    txtStub12.Text = "Amount Due";
                    txtStub13.Text = "Amount Paid";
                }
                else
                {
                    txtStub11.Visible = false;
                    txtStub12.Visible = false;
                    txtStub13.Visible = false;
                }

                if (boolBlank)
                {
                    txtStub21.Text = "Due Date";
                    txtStub22.Text = "Amount Due";
                    txtStub23.Text = "Amount Paid";
                }
                else
                {
                    txtStub21.Visible = false;
                    txtStub22.Visible = false;
                    txtStub23.Visible = false;
                }

                //GridStubInfo1.ColWidth(0, FCConvert.ToInt32(0.25 * GridStubInfo1.WidthOriginal));
                if (boolBlank)
                {
                    //GridStubInfo1.TextMatrix(0, 0, "Account:");
                    //GridStubInfo1.TextMatrix(1, 0, "Name:");
                    //GridStubInfo1.TextMatrix(2, 0, "Map/Lot:");
                    //GridStubInfo1.TextMatrix(3, 0, "Location:");
                    txtStubInfo111.Text = "Account:";
                    txtStubInfo121.Text = "Name:";
                    txtStubInfo131.Text = "Map/Lot:";
                    txtStubInfo141.Text = "Location:";
                }

                //GridStubInfo2.ColWidth(0, FCConvert.ToInt32(0.25 * GridStubInfo2.WidthOriginal));
                if (boolBlank)
                {
                    //GridStubInfo2.TextMatrix(0, 0, "Account:");
                    //GridStubInfo2.TextMatrix(1, 0, "Name:");
                    //GridStubInfo2.TextMatrix(2, 0, "Map/Lot:");
                    //GridStubInfo2.TextMatrix(3, 0, "Location:");
                    txtStubInfo211.Text = "Account:";
                    txtStubInfo221.Text = "Name:";
                    txtStubInfo231.Text = "Map/Lot:";
                    txtStubInfo241.Text = "Location:";
                }

                // With GridRemittance
                if (boolBlank)
                {
                    // .TextMatrix(0, 0) = "Remittance Instructions"
                    // .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0) = FCGrid.AlignmentSettings.flexAlignCenterCenter
                    // .Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0) = True
                    txtRemitLbl0.Text = "Remittance Instructions";
                    txtRemitLbl0.Font = new Font(txtRemitLbl0.Font, FontStyle.Bold);
                    shapeRemittance.Visible = true;
                }
                else
                {
                    // .BackColorFixed = RGB(255, 255, 255)
                }

                // End With
                // With GridInformation
                if (boolBlank)
                {
                    txtInfoLbl0.Text = "Information";
                    txtInfoLbl0.Font = new Font(txtInfoLbl0.Font, FontStyle.Bold);
                    // .TextMatrix(0, 0) = "Information"
                    // .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0) = FCGrid.AlignmentSettings.flexAlignCenterCenter
                    // .Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0) = True
                    shapeInformation.Visible = true;
                }
                else
                {
                    // .BackColorFixed = RGB(255, 255, 255)
                }

                // End With
            }
        }

        private string FormatAsPercent(double pct)
        {
            return Strings.Format(pct, "#0.00") + "%";
        }


    }
}
