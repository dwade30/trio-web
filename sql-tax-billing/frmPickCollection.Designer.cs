﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPickCollection.
	/// </summary>
	partial class frmPickCollection : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbOptions;
		public fecherFoundation.FCLabel lblOptions;
		public fecherFoundation.FCComboBox cmbFormat;
		public fecherFoundation.FCLabel lblFormat;
		public fecherFoundation.FCComboBox cmbCollection;
		public fecherFoundation.FCLabel lblCollection;
		public fecherFoundation.FCTextBox txtDiscount;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPickCollection));
			this.cmbOrder = new fecherFoundation.FCComboBox();
			this.lblOrder = new fecherFoundation.FCLabel();
			this.cmbOptions = new fecherFoundation.FCComboBox();
			this.lblOptions = new fecherFoundation.FCLabel();
			this.cmbFormat = new fecherFoundation.FCComboBox();
			this.lblFormat = new fecherFoundation.FCLabel();
			this.cmbCollection = new fecherFoundation.FCComboBox();
			this.lblCollection = new fecherFoundation.FCLabel();
			this.txtDiscount = new fecherFoundation.FCTextBox();
			this.txtYear = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 288);
			this.BottomPanel.Size = new System.Drawing.Size(969, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbOrder);
			this.ClientArea.Controls.Add(this.lblOrder);
			this.ClientArea.Controls.Add(this.txtDiscount);
			this.ClientArea.Controls.Add(this.cmbOptions);
			this.ClientArea.Controls.Add(this.lblOptions);
			this.ClientArea.Controls.Add(this.cmbFormat);
			this.ClientArea.Controls.Add(this.lblFormat);
			this.ClientArea.Controls.Add(this.cmbCollection);
			this.ClientArea.Controls.Add(this.lblCollection);
			this.ClientArea.Controls.Add(this.txtYear);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(969, 228);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(969, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(166, 30);
			this.HeaderText.Text = "Collection List";
			// 
			// cmbOrder
			// 
			this.cmbOrder.AutoSize = false;
			this.cmbOrder.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOrder.FormattingEnabled = true;
			this.cmbOrder.Items.AddRange(new object[] {
				"Name",
				"Account",
				"Map / Lot"
			});
			this.cmbOrder.Location = new System.Drawing.Point(623, 90);
			this.cmbOrder.Name = "cmbOrder";
			this.cmbOrder.Size = new System.Drawing.Size(185, 40);
			this.cmbOrder.TabIndex = 9;
			this.cmbOrder.Text = "Name";
			// 
			// lblOrder
			// 
			this.lblOrder.AutoSize = true;
			this.lblOrder.Location = new System.Drawing.Point(495, 104);
			this.lblOrder.Name = "lblOrder";
			this.lblOrder.Size = new System.Drawing.Size(52, 15);
			this.lblOrder.TabIndex = 8;
			this.lblOrder.Text = "ORDER";
			// 
			// cmbOptions
			// 
			this.cmbOptions.AutoSize = false;
			this.cmbOptions.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOptions.FormattingEnabled = true;
			this.cmbOptions.Items.AddRange(new object[] {
				"Show Just Total and Tax",
				"Show Land/Building/Exempt",
				"Show Discount"
			});
			this.cmbOptions.Location = new System.Drawing.Point(623, 30);
			this.cmbOptions.Name = "cmbOptions";
			this.cmbOptions.Size = new System.Drawing.Size(315, 40);
			this.cmbOptions.TabIndex = 7;
			this.cmbOptions.Text = "Show Land/Building/Exempt";
			this.cmbOptions.SelectedIndexChanged += new System.EventHandler(this.cmbOptions_SelectedIndexChanged);
			// 
			// lblOptions
			// 
			this.lblOptions.AutoSize = true;
			this.lblOptions.Location = new System.Drawing.Point(495, 44);
			this.lblOptions.Name = "lblOptions";
			this.lblOptions.Size = new System.Drawing.Size(63, 15);
			this.lblOptions.TabIndex = 6;
			this.lblOptions.Text = "OPTIONS";
			// 
			// cmbFormat
			// 
			this.cmbFormat.AutoSize = false;
			this.cmbFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFormat.FormattingEnabled = true;
			this.cmbFormat.Items.AddRange(new object[] {
				"8.5 X 11 Portrait",
				"8.5 X 11 Landscape",
				"Wide Printer"
			});
			this.cmbFormat.Location = new System.Drawing.Point(172, 90);
			this.cmbFormat.Name = "cmbFormat";
			this.cmbFormat.Size = new System.Drawing.Size(253, 40);
			this.cmbFormat.TabIndex = 3;
			this.cmbFormat.Text = "8.5 X 11 Portrait";
			// 
			// lblFormat
			// 
			this.lblFormat.AutoSize = true;
			this.lblFormat.Location = new System.Drawing.Point(30, 104);
			this.lblFormat.Name = "lblFormat";
			this.lblFormat.Size = new System.Drawing.Size(60, 15);
			this.lblFormat.TabIndex = 2;
			this.lblFormat.Text = "FORMAT";
			// 
			// cmbCollection
			// 
			this.cmbCollection.AutoSize = false;
			this.cmbCollection.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCollection.FormattingEnabled = true;
			this.cmbCollection.Items.AddRange(new object[] {
				"Real Estate",
				"Personal Property"
			});
			this.cmbCollection.Location = new System.Drawing.Point(172, 30);
			this.cmbCollection.Name = "cmbCollection";
			this.cmbCollection.Size = new System.Drawing.Size(253, 40);
			this.cmbCollection.TabIndex = 1;
			this.cmbCollection.Text = "Real Estate";
			this.cmbCollection.SelectedIndexChanged += new System.EventHandler(this.cmbCollection_SelectedIndexChanged);
			// 
			// lblCollection
			// 
			this.lblCollection.AutoSize = true;
			this.lblCollection.Location = new System.Drawing.Point(30, 44);
			this.lblCollection.Name = "lblCollection";
			this.lblCollection.Size = new System.Drawing.Size(86, 15);
			this.lblCollection.TabIndex = 0;
			this.lblCollection.Text = "COLLECTION";
			// 
			// txtDiscount
			// 
			this.txtDiscount.AutoSize = false;
			this.txtDiscount.BackColor = System.Drawing.SystemColors.Window;
			this.txtDiscount.LinkItem = null;
			this.txtDiscount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDiscount.LinkTopic = null;
			this.txtDiscount.Location = new System.Drawing.Point(623, 150);
			this.txtDiscount.Name = "txtDiscount";
			this.txtDiscount.Size = new System.Drawing.Size(75, 40);
			this.txtDiscount.TabIndex = 11;
			// 
			// txtYear
			// 
			this.txtYear.AutoSize = false;
			this.txtYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtYear.LinkItem = null;
			this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYear.LinkTopic = null;
			this.txtYear.Location = new System.Drawing.Point(172, 150);
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(75, 40);
			this.txtYear.TabIndex = 5;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(495, 164);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(79, 16);
			this.Label2.TabIndex = 10;
			this.Label2.Text = "DISCOUNT";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 164);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(80, 19);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "TAX YEAR";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Cancel    ";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(437, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(100, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmPickCollection
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(969, 396);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPickCollection";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Collection List";
			this.Load += new System.EventHandler(this.frmPickCollection_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPickCollection_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
