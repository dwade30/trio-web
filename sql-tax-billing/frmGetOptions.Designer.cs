﻿//Fecher vbPorter - Version 1.0.0.35
using Wisej.Web;
using Global;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmGetOptions.
	/// </summary>
	partial class frmGetOptions : BaseForm
	{
		public fecherFoundation.FCComboBox cmbtion1;
		public fecherFoundation.FCLabel lbltion1;
		public fecherFoundation.FCLabel lblDescription;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetOptions));
            this.cmbtion1 = new fecherFoundation.FCComboBox();
            this.lbltion1 = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveAndContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveAndContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 126);
            this.BottomPanel.Size = new System.Drawing.Size(426, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbtion1);
            this.ClientArea.Controls.Add(this.lbltion1);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(446, 247);
            this.ClientArea.Controls.SetChildIndex(this.lblDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.lbltion1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbtion1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(446, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(143, 30);
            this.HeaderText.Text = "Get Options";
            // 
            // cmbtion1
            // 
            this.cmbtion1.Location = new System.Drawing.Point(109, 86);
            this.cmbtion1.Name = "cmbtion1";
            this.cmbtion1.Size = new System.Drawing.Size(210, 40);
            this.cmbtion1.TabIndex = 2;
            // 
            // lbltion1
            // 
            this.lbltion1.AutoSize = true;
            this.lbltion1.Location = new System.Drawing.Point(30, 100);
            this.lbltion1.Name = "lbltion1";
            this.lbltion1.Size = new System.Drawing.Size(66, 17);
            this.lbltion1.TabIndex = 1;
            this.lbltion1.Text = "CHOOSE";
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 30);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(381, 36);
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveAndContinue
            // 
            this.cmdSaveAndContinue.AppearanceKey = "acceptButton";
            this.cmdSaveAndContinue.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdSaveAndContinue.Location = new System.Drawing.Point(108, 30);
            this.cmdSaveAndContinue.Name = "cmdSaveAndContinue";
            this.cmdSaveAndContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveAndContinue.Size = new System.Drawing.Size(210, 48);
            this.cmdSaveAndContinue.Text = "Continue";
            this.cmdSaveAndContinue.Click += new System.EventHandler(this.cmdSaveAndContinue_Click);
            // 
            // frmGetOptions
            // 
            this.AcceptButton = this.cmdSaveAndContinue;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(446, 307);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmGetOptions";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.Load += new System.EventHandler(this.frmGetOptions_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetOptions_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndContinue;
	}
}
