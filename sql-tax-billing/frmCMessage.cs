﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmCMessage.
	/// </summary>
	public partial class frmCMessage : BaseForm
	{
		public frmCMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCMessage InstancePtr
		{
			get
			{
				return (frmCMessage)Sys.GetInstance(typeof(frmCMessage));
			}
		}

		protected frmCMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmCMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCMessage properties;
			//frmCMessage.ScaleWidth	= 5880;
			//frmCMessage.ScaleHeight	= 4005;
			//frmCMessage.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(DialogResult)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				e.Cancel = true;
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtMach.Text) > 9 || Conversion.Val(txtFurn.Text) > 9)
			{
				MessageBox.Show("9 is the highest valid category", "Invalid Category", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rptBillFormatC.InstancePtr.intCat1 = FCConvert.ToInt32(Math.Round(Conversion.Val(txtMach.Text)));
			rptBillFormatC.InstancePtr.intCat2 = FCConvert.ToInt32(Math.Round(Conversion.Val(txtFurn.Text)));
			rptBillFormatC.InstancePtr.strYear1 = txtBegin.Text;
			rptBillFormatC.InstancePtr.strYear2 = txtEnd.Text;
			Close();
		}

		private void cmdContinue_Click(object sender, EventArgs e)
		{
			mnuContinue_Click(mnuContinue, EventArgs.Empty);
		}
	}
}
