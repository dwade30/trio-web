﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPreviousOwners.
	/// </summary>
	partial class rptPreviousOwners
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPreviousOwners));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSecOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtAddress2,
				this.txtCity,
				this.txtSaleDate,
				this.txtAddress1,
				this.txtSecOwner,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8,
				this.Field9,
				this.txtState,
				this.Field10,
				this.Field11,
				this.txtZip
			});
			this.Detail.Height = 0.9375F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Field2,
				this.Field3,
				this.Field4
			});
			this.PageHeader.Height = 0.6979167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 2F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Previous Owners Report";
			this.Field1.Top = 0F;
			this.Field1.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.21625F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.0625F;
			this.txtMuni.Width = 1.8125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.25875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.21625F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.4375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field2";
			this.txtPage.Top = 0.25875F;
			this.txtPage.Width = 1F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.19F;
			this.Field2.Left = 0F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-weight: bold; text-align: right";
			this.Field2.Text = "Account";
			this.Field2.Top = 0.53125F;
			this.Field2.Width = 0.75F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.19F;
			this.Field3.Left = 0.875F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-weight: bold";
			this.Field3.Text = "Name & Address";
			this.Field3.Top = 0.53125F;
			this.Field3.Width = 1.625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.19F;
			this.Field4.Left = 5.5625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-weight: bold; text-align: center";
			this.Field4.Text = "Sale Date";
			this.Field4.Top = 0.53125F;
			this.Field4.Width = 0.875F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "text-align: right";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.15625F;
			this.txtAccount.Width = 0.75F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 1.6875F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "No previous owners found.";
			this.txtName.Top = 0.15625F;
			this.txtName.Width = 3.8125F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 1.6875F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.625F;
			this.txtAddress2.Width = 3.8125F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.19F;
			this.txtCity.Left = 1.6875F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Text = null;
			this.txtCity.Top = 0.78125F;
			this.txtCity.Width = 1.6875F;
			// 
			// txtSaleDate
			// 
			this.txtSaleDate.Height = 0.19F;
			this.txtSaleDate.Left = 5.5625F;
			this.txtSaleDate.Name = "txtSaleDate";
			this.txtSaleDate.Style = "text-align: center";
			this.txtSaleDate.Text = null;
			this.txtSaleDate.Top = 0.15625F;
			this.txtSaleDate.Width = 0.875F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 1.6875F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.46875F;
			this.txtAddress1.Width = 3.8125F;
			// 
			// txtSecOwner
			// 
			this.txtSecOwner.Height = 0.19F;
			this.txtSecOwner.Left = 1.6875F;
			this.txtSecOwner.Name = "txtSecOwner";
			this.txtSecOwner.Text = null;
			this.txtSecOwner.Top = 0.3125F;
			this.txtSecOwner.Width = 3.8125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.19F;
			this.Field5.Left = 0.8125F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "text-align: left";
			this.Field5.Text = "Name";
			this.Field5.Top = 0.15625F;
			this.Field5.Width = 0.8125F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.19F;
			this.Field6.Left = 0.8125F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "text-align: left";
			this.Field6.Text = "2nd Owner";
			this.Field6.Top = 0.3125F;
			this.Field6.Width = 0.8125F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.19F;
			this.Field7.Left = 0.8125F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "text-align: left";
			this.Field7.Text = "Address 1";
			this.Field7.Top = 0.46875F;
			this.Field7.Width = 0.8125F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.19F;
			this.Field8.Left = 0.8125F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "text-align: left";
			this.Field8.Text = "Address 2";
			this.Field8.Top = 0.625F;
			this.Field8.Width = 0.8125F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.19F;
			this.Field9.Left = 0.8125F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "text-align: left";
			this.Field9.Text = "City";
			this.Field9.Top = 0.78125F;
			this.Field9.Width = 0.8125F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.19F;
			this.txtState.Left = 3.875F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "text-align: left";
			this.txtState.Text = null;
			this.txtState.Top = 0.78125F;
			this.txtState.Width = 0.375F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.19F;
			this.Field10.Left = 3.375F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "text-align: right";
			this.Field10.Text = "State";
			this.Field10.Top = 0.78125F;
			this.Field10.Width = 0.4375F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.19F;
			this.Field11.Left = 4.3125F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "text-align: right";
			this.Field11.Text = "Zip";
			this.Field11.Top = 0.78125F;
			this.Field11.Width = 0.3125F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.19F;
			this.txtZip.Left = 4.6875F;
			this.txtZip.Name = "txtZip";
			this.txtZip.Style = "text-align: left";
			this.txtZip.Text = null;
			this.txtZip.Top = 0.78125F;
			this.txtZip.Width = 0.8125F;
			// 
			// rptPreviousOwners
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
