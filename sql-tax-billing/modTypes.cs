﻿//Fecher vbPorter - Version 1.0.0.35
using fecherFoundation;
using System.Linq;
using System.Runtime.InteropServices;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	public class modTypes
	{
		//=========================================================
		public struct OutPrintCommaFormat
		{
			// vbPorter upgrade warning: BillType As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string BillType;
			// re pp or cm (combined)
			public string Account;
			public string Location;
			public string MapLot;
			public string Name;
			public string Address1;
			public string Address2;
			public string City;
			public string State;
			public string Zip;
			public string Land;
			public string Building;
			public string Total;
			public string REExempt;
			public string PPCode1;
			public string PPCode2;
			public string PPCode3;
			public string PPCode4;
			public string PPCode5;
			public string PPCode6;
			public string PPCode7;
			public string PPCode8;
			public string PPcode9;
			public string TotalPP;
			public string TotalExempt;
			public string NetAssessment;
			public string TotalTax;
			// total tax - amount paid
			public string TaxDue1;
			public string TaxDue2;
			public string TaxDue3;
			public string TaxDue4;
			public string BookPage;
			public string HomesteadExempt;
			// RE only
			public string OtherExempt;
			// other than homestead
			public string Distribution1;
			public string Distribution2;
			public string Distribution3;
			public string Distribution4;
			public string Distribution5;
			public string DistPercent1;
			// distribution percentage
			public string DistPercent2;
			public string DistPercent3;
			public string DistPercent4;
			public string DistPercent5;
			public string PaidToDate;
			// Amount paid to date
			public string NetREAssessment;
			public string Ref1;
			public string Ref2;
			public string SecondOwner;
			public string Acreage;
			public string MortgageHolderID;
			public string SoftAcres;
			public string MixedAcres;
			public string HardAcres;
			public string SoftValue;
			public string MixedValue;
			public string HardValue;
			public string DiscountAmount;
			public string[] REExemptAmounts/*?  = new string[3 + 1] */;
			public string[] REExemptDesc/*?  = new string[3 + 1] */;
			public string Address3;
			public string PastDue;

			public OutPrintCommaFormat(int unusedParam)
			{
				this.BillType = new string(' ', 2);
				this.Account = string.Empty;
				this.Acreage = string.Empty;
				this.Address1 = string.Empty;
				this.Address2 = string.Empty;
				this.Address3 = string.Empty;
				this.BookPage = string.Empty;
				this.Building = string.Empty;
				this.City = string.Empty;
				this.DiscountAmount = string.Empty;
				this.DistPercent1 = string.Empty;
				this.DistPercent2 = string.Empty;
				this.DistPercent3 = string.Empty;
				this.DistPercent4 = string.Empty;
				this.DistPercent5 = string.Empty;
				this.Distribution1 = string.Empty;
				this.Distribution2 = string.Empty;
				this.Distribution3 = string.Empty;
				this.Distribution4 = string.Empty;
				this.Distribution5 = string.Empty;
				this.HardAcres = string.Empty;
				this.HardValue = string.Empty;
				this.HomesteadExempt = string.Empty;
				this.Land = string.Empty;
				this.Location = string.Empty;
				this.MapLot = string.Empty;
				this.MixedAcres = string.Empty;
				this.MixedValue = string.Empty;
				this.MortgageHolderID = string.Empty;
				this.Name = string.Empty;
				this.NetAssessment = string.Empty;
				this.NetREAssessment = string.Empty;
				this.OtherExempt = string.Empty;
				this.PaidToDate = string.Empty;
				this.PastDue = string.Empty;
				this.PPCode1 = string.Empty;
				this.PPCode2 = string.Empty;
				this.PPCode3 = string.Empty;
				this.PPCode4 = string.Empty;
				this.PPCode5 = string.Empty;
				this.PPCode6 = string.Empty;
				this.PPCode7 = string.Empty;
				this.PPCode8 = string.Empty;
				this.PPcode9 = string.Empty;
				this.REExempt = string.Empty;
				this.REExemptAmounts = new string[4] {
					string.Empty,
					string.Empty,
					string.Empty,
					string.Empty
				};
				this.REExemptDesc = new string[4] {
					string.Empty,
					string.Empty,
					string.Empty,
					string.Empty
				};
				this.Ref1 = string.Empty;
				this.Ref2 = string.Empty;
				this.SecondOwner = string.Empty;
				this.SoftAcres = string.Empty;
				this.SoftValue = string.Empty;
				this.State = string.Empty;
				this.TaxDue1 = string.Empty;
				this.TaxDue2 = string.Empty;
				this.TaxDue3 = string.Empty;
				this.TaxDue4 = string.Empty;
				this.Total = string.Empty;
				this.TotalExempt = string.Empty;
				this.TotalPP = string.Empty;
				this.TotalTax = string.Empty;
				this.Zip = string.Empty;
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct OutPrintingFormat
		{
			// vbPorter upgrade warning: BillType As FixedString	OnWrite(string)	OnRead(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] BillTypeCharArray;
			public string BillType
			{
				get
				{
					return FCUtils.FixedStringFromArray(BillTypeCharArray);
				}
				set
				{
					BillTypeCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// re pp or cm (combined)
			// vbPorter upgrade warning: Account As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] AccountCharArray;
			public string Account
			{
				get
				{
					return FCUtils.FixedStringFromArray(AccountCharArray);
				}
				set
				{
					AccountCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: Location As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 31)]
			public char[] LocationCharArray;
			public string Location
			{
				get
				{
					return FCUtils.FixedStringFromArray(LocationCharArray);
				}
				set
				{
					LocationCharArray = FCUtils.FixedStringToArray(value, 31);
				}
			}
			// vbPorter upgrade warning: MapLot As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
			public char[] MapLotCharArray;
			public string MapLot
			{
				get
				{
					return FCUtils.FixedStringFromArray(MapLotCharArray);
				}
				set
				{
					MapLotCharArray = FCUtils.FixedStringToArray(value, 17);
				}
			}
			// vbPorter upgrade warning: Name As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 34)]
			public char[] NameCharArray;
			public string Name
			{
				get
				{
					return FCUtils.FixedStringFromArray(NameCharArray);
				}
				set
				{
					NameCharArray = FCUtils.FixedStringToArray(value, 34);
				}
			}
			// vbPorter upgrade warning: Address1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 34)]
			public char[] Address1CharArray;
			public string Address1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Address1CharArray);
				}
				set
				{
					Address1CharArray = FCUtils.FixedStringToArray(value, 34);
				}
			}
			// vbPorter upgrade warning: Address2 As FixedString	OnWrite(string, FixedString)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 34)]
			public char[] Address2CharArray;
			public string Address2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Address2CharArray);
				}
				set
				{
					Address2CharArray = FCUtils.FixedStringToArray(value, 34);
				}
			}
			// vbPorter upgrade warning: City As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
			public char[] CityCharArray;
			public string City
			{
				get
				{
					return FCUtils.FixedStringFromArray(CityCharArray);
				}
				set
				{
					CityCharArray = FCUtils.FixedStringToArray(value, 24);
				}
			}
			// vbPorter upgrade warning: State As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] StateCharArray;
			public string State
			{
				get
				{
					return FCUtils.FixedStringFromArray(StateCharArray);
				}
				set
				{
					StateCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// vbPorter upgrade warning: Zip As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
			public char[] ZipCharArray;
			public string Zip
			{
				get
				{
					return FCUtils.FixedStringFromArray(ZipCharArray);
				}
				set
				{
					ZipCharArray = FCUtils.FixedStringToArray(value, 5);
				}
			}
			// vbPorter upgrade warning: Land As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] LandCharArray;
			public string Land
			{
				get
				{
					return FCUtils.FixedStringFromArray(LandCharArray);
				}
				set
				{
					LandCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: Building As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] BuildingCharArray;
			public string Building
			{
				get
				{
					return FCUtils.FixedStringFromArray(BuildingCharArray);
				}
				set
				{
					BuildingCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: Total As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TotalCharArray;
			public string Total
			{
				get
				{
					return FCUtils.FixedStringFromArray(TotalCharArray);
				}
				set
				{
					TotalCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode1CharArray;
			public string PPCode1
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode1CharArray);
				}
				set
				{
					PPCode1CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode2CharArray;
			public string PPCode2
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode2CharArray);
				}
				set
				{
					PPCode2CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode3CharArray;
			public string PPCode3
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode3CharArray);
				}
				set
				{
					PPCode3CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode4CharArray;
			public string PPCode4
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode4CharArray);
				}
				set
				{
					PPCode4CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode5 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode5CharArray;
			public string PPCode5
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode5CharArray);
				}
				set
				{
					PPCode5CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TotalPP As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TotalPPCharArray;
			public string TotalPP
			{
				get
				{
					return FCUtils.FixedStringFromArray(TotalPPCharArray);
				}
				set
				{
					TotalPPCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TotalExempt As FixedString	OnWrite(FixedString, string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TotalExemptCharArray;
			public string TotalExempt
			{
				get
				{
					return FCUtils.FixedStringFromArray(TotalExemptCharArray);
				}
				set
				{
					TotalExemptCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: NetAssessment As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] NetAssessmentCharArray;
			public string NetAssessment
			{
				get
				{
					return FCUtils.FixedStringFromArray(NetAssessmentCharArray);
				}
				set
				{
					NetAssessmentCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TotalTax As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TotalTaxCharArray;
			public string TotalTax
			{
				get
				{
					return FCUtils.FixedStringFromArray(TotalTaxCharArray);
				}
				set
				{
					TotalTaxCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// total tax - amount paid
			// vbPorter upgrade warning: TaxDue1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TaxDue1CharArray;
			public string TaxDue1
			{
				get
				{
					return FCUtils.FixedStringFromArray(TaxDue1CharArray);
				}
				set
				{
					TaxDue1CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TaxDue2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TaxDue2CharArray;
			public string TaxDue2
			{
				get
				{
					return FCUtils.FixedStringFromArray(TaxDue2CharArray);
				}
				set
				{
					TaxDue2CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TaxDue3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TaxDue3CharArray;
			public string TaxDue3
			{
				get
				{
					return FCUtils.FixedStringFromArray(TaxDue3CharArray);
				}
				set
				{
					TaxDue3CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TaxDue4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TaxDue4CharArray;
			public string TaxDue4
			{
				get
				{
					return FCUtils.FixedStringFromArray(TaxDue4CharArray);
				}
				set
				{
					TaxDue4CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: BookPage As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
			public char[] BookPageCharArray;
			public string BookPage
			{
				get
				{
					return FCUtils.FixedStringFromArray(BookPageCharArray);
				}
				set
				{
					BookPageCharArray = FCUtils.FixedStringToArray(value, 12);
				}
			}
			// vbPorter upgrade warning: HomesteadExempt As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] HomesteadExemptCharArray;
			public string HomesteadExempt
			{
				get
				{
					return FCUtils.FixedStringFromArray(HomesteadExemptCharArray);
				}
				set
				{
					HomesteadExemptCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// RE only
			// vbPorter upgrade warning: OtherExempt As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] OtherExemptCharArray;
			public string OtherExempt
			{
				get
				{
					return FCUtils.FixedStringFromArray(OtherExemptCharArray);
				}
				set
				{
					OtherExemptCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// other than homestead
			// vbPorter upgrade warning: Distribution1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] Distribution1CharArray;
			public string Distribution1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution1CharArray);
				}
				set
				{
					Distribution1CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: Distribution2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] Distribution2CharArray;
			public string Distribution2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution2CharArray);
				}
				set
				{
					Distribution2CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: Distribution3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] Distribution3CharArray;
			public string Distribution3
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution3CharArray);
				}
				set
				{
					Distribution3CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: Distribution4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] Distribution4CharArray;
			public string Distribution4
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution4CharArray);
				}
				set
				{
					Distribution4CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: Distribution5 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] Distribution5CharArray;
			public string Distribution5
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution5CharArray);
				}
				set
				{
					Distribution5CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: DistPercent1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent1CharArray;
			public string DistPercent1
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent1CharArray);
				}
				set
				{
					DistPercent1CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// distribution percentage
			// vbPorter upgrade warning: DistPercent2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent2CharArray;
			public string DistPercent2
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent2CharArray);
				}
				set
				{
					DistPercent2CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: DistPercent3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent3CharArray;
			public string DistPercent3
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent3CharArray);
				}
				set
				{
					DistPercent3CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: DistPercent4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent4CharArray;
			public string DistPercent4
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent4CharArray);
				}
				set
				{
					DistPercent4CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: DistPercent5 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent5CharArray;
			public string DistPercent5
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent5CharArray);
				}
				set
				{
					DistPercent5CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: PaidToDate As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PaidToDateCharArray;
			public string PaidToDate
			{
				get
				{
					return FCUtils.FixedStringFromArray(PaidToDateCharArray);
				}
				set
				{
					PaidToDateCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// Amount paid to date
			// vbPorter upgrade warning: LineEnd As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] LineEndCharArray;
			public string LineEnd
			{
				get
				{
					return FCUtils.FixedStringFromArray(LineEndCharArray);
				}
				set
				{
					LineEndCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public OutPrintingFormat(int unusedParam)
			{
                this.BillTypeCharArray = new string(' ', 2).ToArray().ToArray();
				this.AccountCharArray = new string(' ', 6).ToArray().ToArray();
				this.LocationCharArray = new string(' ', 31).ToArray().ToArray();
				this.MapLotCharArray = new string(' ', 17).ToArray().ToArray();
				this.NameCharArray = new string(' ', 34).ToArray().ToArray();
				this.Address1CharArray = new string(' ', 34).ToArray().ToArray();
				this.Address2CharArray = new string(' ', 34).ToArray().ToArray();
				this.CityCharArray = new string(' ', 24).ToArray().ToArray();
				this.StateCharArray = new string(' ', 2).ToArray().ToArray();
				this.ZipCharArray = new string(' ', 5).ToArray().ToArray();
				this.LandCharArray = new string(' ', 10).ToArray().ToArray();
				this.BuildingCharArray = new string(' ', 10).ToArray().ToArray();
				this.TotalCharArray = new string(' ', 10).ToArray().ToArray();
				this.PPCode1CharArray = new string(' ', 10).ToArray().ToArray();
				this.PPCode2CharArray = new string(' ', 10).ToArray().ToArray();
				this.PPCode3CharArray = new string(' ', 10).ToArray().ToArray();
				this.PPCode4CharArray = new string(' ', 10).ToArray().ToArray();
				this.PPCode5CharArray = new string(' ', 10).ToArray().ToArray();
				this.TotalPPCharArray = new string(' ', 10).ToArray().ToArray();
				this.TotalExemptCharArray = new string(' ', 10).ToArray().ToArray();
				this.NetAssessmentCharArray = new string(' ', 10).ToArray().ToArray();
				this.TotalTaxCharArray = new string(' ', 10).ToArray().ToArray();
				this.TaxDue1CharArray = new string(' ', 10).ToArray().ToArray();
				this.TaxDue2CharArray = new string(' ', 10).ToArray().ToArray();
				this.TaxDue3CharArray = new string(' ', 10).ToArray().ToArray();
				this.TaxDue4CharArray = new string(' ', 10).ToArray().ToArray();
				this.BookPageCharArray = new string(' ', 12).ToArray().ToArray();
				this.HomesteadExemptCharArray = new string(' ', 10).ToArray().ToArray();
				this.OtherExemptCharArray = new string(' ', 10).ToArray().ToArray();
				this.Distribution1CharArray = new string(' ', 10).ToArray().ToArray();
				this.Distribution2CharArray = new string(' ', 10).ToArray().ToArray();
				this.Distribution3CharArray = new string(' ', 10).ToArray().ToArray();
				this.Distribution4CharArray = new string(' ', 10).ToArray().ToArray();
				this.Distribution5CharArray = new string(' ', 10).ToArray().ToArray();
				this.DistPercent1CharArray = new string(' ', 6).ToArray().ToArray();
				this.DistPercent2CharArray = new string(' ', 6).ToArray().ToArray();
				this.DistPercent3CharArray = new string(' ', 6).ToArray().ToArray();
				this.DistPercent4CharArray = new string(' ', 6).ToArray().ToArray();
				this.DistPercent5CharArray = new string(' ', 6).ToArray().ToArray();
				this.PaidToDateCharArray = new string(' ', 10).ToArray().ToArray();
				this.LineEndCharArray = new string(' ', 2).ToArray().ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct OutPrintingEnhancedFormat
		{
			// vbPorter upgrade warning: BillType As FixedString	OnWrite(string)	OnRead(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] BillTypeCharArray;
			public string BillType
			{
				get
				{
					return FCUtils.FixedStringFromArray(BillTypeCharArray);
				}
				set
				{
					BillTypeCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// re pp or cm (combined)
			// vbPorter upgrade warning: Account As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] AccountCharArray;
			public string Account
			{
				get
				{
					return FCUtils.FixedStringFromArray(AccountCharArray);
				}
				set
				{
					AccountCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: Location As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 31)]
			public char[] LocationCharArray;
			public string Location
			{
				get
				{
					return FCUtils.FixedStringFromArray(LocationCharArray);
				}
				set
				{
					LocationCharArray = FCUtils.FixedStringToArray(value, 31);
				}
			}
			// vbPorter upgrade warning: MapLot As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
			public char[] MapLotCharArray;
			public string MapLot
			{
				get
				{
					return FCUtils.FixedStringFromArray(MapLotCharArray);
				}
				set
				{
					MapLotCharArray = FCUtils.FixedStringToArray(value, 17);
				}
			}
			// vbPorter upgrade warning: Name As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 34)]
			public char[] NameCharArray;
			public string Name
			{
				get
				{
					return FCUtils.FixedStringFromArray(NameCharArray);
				}
				set
				{
					NameCharArray = FCUtils.FixedStringToArray(value, 34);
				}
			}
			// vbPorter upgrade warning: Address1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 34)]
			public char[] Address1CharArray;
			public string Address1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Address1CharArray);
				}
				set
				{
					Address1CharArray = FCUtils.FixedStringToArray(value, 34);
				}
			}
			// vbPorter upgrade warning: Address2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 34)]
			public char[] Address2CharArray;
			public string Address2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Address2CharArray);
				}
				set
				{
					Address2CharArray = FCUtils.FixedStringToArray(value, 34);
				}
			}
			// vbPorter upgrade warning: City As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
			public char[] CityCharArray;
			public string City
			{
				get
				{
					return FCUtils.FixedStringFromArray(CityCharArray);
				}
				set
				{
					CityCharArray = FCUtils.FixedStringToArray(value, 24);
				}
			}
			// vbPorter upgrade warning: State As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] StateCharArray;
			public string State
			{
				get
				{
					return FCUtils.FixedStringFromArray(StateCharArray);
				}
				set
				{
					StateCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// vbPorter upgrade warning: Zip As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
			public char[] ZipCharArray;
			public string Zip
			{
				get
				{
					return FCUtils.FixedStringFromArray(ZipCharArray);
				}
				set
				{
					ZipCharArray = FCUtils.FixedStringToArray(value, 5);
				}
			}
			// vbPorter upgrade warning: Land As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] LandCharArray;
			public string Land
			{
				get
				{
					return FCUtils.FixedStringFromArray(LandCharArray);
				}
				set
				{
					LandCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: Building As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] BuildingCharArray;
			public string Building
			{
				get
				{
					return FCUtils.FixedStringFromArray(BuildingCharArray);
				}
				set
				{
					BuildingCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: Total As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TotalCharArray;
			public string Total
			{
				get
				{
					return FCUtils.FixedStringFromArray(TotalCharArray);
				}
				set
				{
					TotalCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode1CharArray;
			public string PPCode1
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode1CharArray);
				}
				set
				{
					PPCode1CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode2CharArray;
			public string PPCode2
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode2CharArray);
				}
				set
				{
					PPCode2CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode3CharArray;
			public string PPCode3
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode3CharArray);
				}
				set
				{
					PPCode3CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode4CharArray;
			public string PPCode4
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode4CharArray);
				}
				set
				{
					PPCode4CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: PPCode5 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] PPCode5CharArray;
			public string PPCode5
			{
				get
				{
					return FCUtils.FixedStringFromArray(PPCode5CharArray);
				}
				set
				{
					PPCode5CharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TotalPP As FixedString	OnWrite(string, FixedString)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TotalPPCharArray;
			public string TotalPP
			{
				get
				{
					return FCUtils.FixedStringFromArray(TotalPPCharArray);
				}
				set
				{
					TotalPPCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TotalExempt As FixedString	OnWrite(FixedString, string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] TotalExemptCharArray;
			public string TotalExempt
			{
				get
				{
					return FCUtils.FixedStringFromArray(TotalExemptCharArray);
				}
				set
				{
					TotalExemptCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: NetAssessment As FixedString	OnWrite(short, string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] NetAssessmentCharArray;
			public string NetAssessment
			{
				get
				{
					return FCUtils.FixedStringFromArray(NetAssessmentCharArray);
				}
				set
				{
					NetAssessmentCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: TotalTax As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] TotalTaxCharArray;
			public string TotalTax
			{
				get
				{
					return FCUtils.FixedStringFromArray(TotalTaxCharArray);
				}
				set
				{
					TotalTaxCharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// total tax - amount paid
			// vbPorter upgrade warning: TaxDue1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] TaxDue1CharArray;
			public string TaxDue1
			{
				get
				{
					return FCUtils.FixedStringFromArray(TaxDue1CharArray);
				}
				set
				{
					TaxDue1CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: TaxDue2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] TaxDue2CharArray;
			public string TaxDue2
			{
				get
				{
					return FCUtils.FixedStringFromArray(TaxDue2CharArray);
				}
				set
				{
					TaxDue2CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: TaxDue3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] TaxDue3CharArray;
			public string TaxDue3
			{
				get
				{
					return FCUtils.FixedStringFromArray(TaxDue3CharArray);
				}
				set
				{
					TaxDue3CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: TaxDue4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] TaxDue4CharArray;
			public string TaxDue4
			{
				get
				{
					return FCUtils.FixedStringFromArray(TaxDue4CharArray);
				}
				set
				{
					TaxDue4CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: BookPage As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
			public char[] BookPageCharArray;
			public string BookPage
			{
				get
				{
					return FCUtils.FixedStringFromArray(BookPageCharArray);
				}
				set
				{
					BookPageCharArray = FCUtils.FixedStringToArray(value, 12);
				}
			}
			// vbPorter upgrade warning: HomesteadExempt As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] HomesteadExemptCharArray;
			public string HomesteadExempt
			{
				get
				{
					return FCUtils.FixedStringFromArray(HomesteadExemptCharArray);
				}
				set
				{
					HomesteadExemptCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// RE only
			// vbPorter upgrade warning: OtherExempt As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] OtherExemptCharArray;
			public string OtherExempt
			{
				get
				{
					return FCUtils.FixedStringFromArray(OtherExemptCharArray);
				}
				set
				{
					OtherExemptCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// other than homestead
			// vbPorter upgrade warning: Distribution1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] Distribution1CharArray;
			public string Distribution1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution1CharArray);
				}
				set
				{
					Distribution1CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: Distribution2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] Distribution2CharArray;
			public string Distribution2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution2CharArray);
				}
				set
				{
					Distribution2CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: Distribution3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] Distribution3CharArray;
			public string Distribution3
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution3CharArray);
				}
				set
				{
					Distribution3CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: Distribution4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] Distribution4CharArray;
			public string Distribution4
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution4CharArray);
				}
				set
				{
					Distribution4CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: Distribution5 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] Distribution5CharArray;
			public string Distribution5
			{
				get
				{
					return FCUtils.FixedStringFromArray(Distribution5CharArray);
				}
				set
				{
					Distribution5CharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// vbPorter upgrade warning: DistPercent1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent1CharArray;
			public string DistPercent1
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent1CharArray);
				}
				set
				{
					DistPercent1CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// distribution percentage
			// vbPorter upgrade warning: DistPercent2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent2CharArray;
			public string DistPercent2
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent2CharArray);
				}
				set
				{
					DistPercent2CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: DistPercent3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent3CharArray;
			public string DistPercent3
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent3CharArray);
				}
				set
				{
					DistPercent3CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: DistPercent4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent4CharArray;
			public string DistPercent4
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent4CharArray);
				}
				set
				{
					DistPercent4CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: DistPercent5 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] DistPercent5CharArray;
			public string DistPercent5
			{
				get
				{
					return FCUtils.FixedStringFromArray(DistPercent5CharArray);
				}
				set
				{
					DistPercent5CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: PaidToDate As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
			public char[] PaidToDateCharArray;
			public string PaidToDate
			{
				get
				{
					return FCUtils.FixedStringFromArray(PaidToDateCharArray);
				}
				set
				{
					PaidToDateCharArray = FCUtils.FixedStringToArray(value, 14);
				}
			}
			// Amount paid to date
			// vbPorter upgrade warning: RENet As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
			public char[] RENetCharArray;
			public string RENet
			{
				get
				{
					return FCUtils.FixedStringFromArray(RENetCharArray);
				}
				set
				{
					RENetCharArray = FCUtils.FixedStringToArray(value, 11);
				}
			}
			// vbPorter upgrade warning: Ref1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 36)]
			public char[] Ref1CharArray;
			public string Ref1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Ref1CharArray);
				}
				set
				{
					Ref1CharArray = FCUtils.FixedStringToArray(value, 36);
				}
			}
			// vbPorter upgrade warning: Ref2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 36)]
			public char[] Ref2CharArray;
			public string Ref2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Ref2CharArray);
				}
				set
				{
					Ref2CharArray = FCUtils.FixedStringToArray(value, 36);
				}
			}
			// vbPorter upgrade warning: SecondOwner As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 34)]
			public char[] SecondOwnerCharArray;
			public string SecondOwner
			{
				get
				{
					return FCUtils.FixedStringFromArray(SecondOwnerCharArray);
				}
				set
				{
					SecondOwnerCharArray = FCUtils.FixedStringToArray(value, 34);
				}
			}
			// vbPorter upgrade warning: Acreage As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] AcreageCharArray;
			public string Acreage
			{
				get
				{
					return FCUtils.FixedStringFromArray(AcreageCharArray);
				}
				set
				{
					AcreageCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: MortgageHolderID As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] MortgageHolderIDCharArray;
			public string MortgageHolderID
			{
				get
				{
					return FCUtils.FixedStringFromArray(MortgageHolderIDCharArray);
				}
				set
				{
					MortgageHolderIDCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: LineEnd As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] LineEndCharArray;
			public string LineEnd
			{
				get
				{
					return FCUtils.FixedStringFromArray(LineEndCharArray);
				}
				set
				{
					LineEndCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public OutPrintingEnhancedFormat(int unusedParam)
			{
				this.BillTypeCharArray = new string(' ', 2).ToArray();
				this.AccountCharArray = new string(' ', 6).ToArray();
				this.LocationCharArray = new string(' ', 31).ToArray();
				this.MapLotCharArray = new string(' ', 17).ToArray();
				this.NameCharArray = new string(' ', 34).ToArray();
				this.Address1CharArray = new string(' ', 34).ToArray();
				this.Address2CharArray = new string(' ', 34).ToArray();
				this.CityCharArray = new string(' ', 24).ToArray();
				this.StateCharArray = new string(' ', 2).ToArray();
				this.ZipCharArray = new string(' ', 5).ToArray();
				this.LandCharArray = new string(' ', 10).ToArray();
				this.BuildingCharArray = new string(' ', 10).ToArray();
				this.TotalCharArray = new string(' ', 10).ToArray();
				this.PPCode1CharArray = new string(' ', 10).ToArray();
				this.PPCode2CharArray = new string(' ', 10).ToArray();
				this.PPCode3CharArray = new string(' ', 10).ToArray();
				this.PPCode4CharArray = new string(' ', 10).ToArray();
				this.PPCode5CharArray = new string(' ', 10).ToArray();
				this.TotalPPCharArray = new string(' ', 10).ToArray();
				this.TotalExemptCharArray = new string(' ', 10).ToArray();
				this.NetAssessmentCharArray = new string(' ', 10).ToArray();
				this.TotalTaxCharArray = new string(' ', 14).ToArray();
				this.TaxDue1CharArray = new string(' ', 14).ToArray();
				this.TaxDue2CharArray = new string(' ', 14).ToArray();
				this.TaxDue3CharArray = new string(' ', 14).ToArray();
				this.TaxDue4CharArray = new string(' ', 14).ToArray();
				this.BookPageCharArray = new string(' ', 12).ToArray();
				this.HomesteadExemptCharArray = new string(' ', 10).ToArray();
				this.OtherExemptCharArray = new string(' ', 10).ToArray();
				this.Distribution1CharArray = new string(' ', 14).ToArray();
				this.Distribution2CharArray = new string(' ', 14).ToArray();
				this.Distribution3CharArray = new string(' ', 14).ToArray();
				this.Distribution4CharArray = new string(' ', 14).ToArray();
				this.Distribution5CharArray = new string(' ', 14).ToArray();
				this.DistPercent1CharArray = new string(' ', 6).ToArray();
				this.DistPercent2CharArray = new string(' ', 6).ToArray();
				this.DistPercent3CharArray = new string(' ', 6).ToArray();
				this.DistPercent4CharArray = new string(' ', 6).ToArray();
				this.DistPercent5CharArray = new string(' ', 6).ToArray();
				this.PaidToDateCharArray = new string(' ', 14).ToArray();
				this.RENetCharArray = new string(' ', 11).ToArray();
				this.Ref1CharArray = new string(' ', 36).ToArray();
				this.Ref2CharArray = new string(' ', 36).ToArray();
				this.SecondOwnerCharArray = new string(' ', 34).ToArray();
				this.AcreageCharArray = new string(' ', 10).ToArray();
				this.MortgageHolderIDCharArray = new string(' ', 6).ToArray();
				this.LineEndCharArray = new string(' ', 2).ToArray();
			}
		};
	}
}
