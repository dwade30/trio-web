﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREMortgage.
	/// </summary>
	partial class rptREMortgage
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptREMortgage));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtaccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEscrow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHolderNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHolderName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHolder = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEscrow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHolderNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHolderName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtaccount,
				this.txtName,
				this.txtEscrow,
				this.txtBill,
				this.txtHolderNumber,
				this.txtHolderName
			});
			this.Detail.Height = 0.19F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtaccount
			// 
			this.txtaccount.Height = 0.19F;
			this.txtaccount.Left = 0.0625F;
			this.txtaccount.MultiLine = false;
			this.txtaccount.Name = "txtaccount";
			this.txtaccount.Style = "font-family: \'Tahoma\'";
			this.txtaccount.Text = "Field1";
			this.txtaccount.Top = 0F;
			this.txtaccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.8125F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0F;
			this.txtName.Width = 2.1875F;
			// 
			// txtEscrow
			// 
			this.txtEscrow.Height = 0.19F;
			this.txtEscrow.Left = 3F;
			this.txtEscrow.MultiLine = false;
			this.txtEscrow.Name = "txtEscrow";
			this.txtEscrow.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtEscrow.Text = "Field1";
			this.txtEscrow.Top = 0F;
			this.txtEscrow.Width = 0.5F;
			// 
			// txtBill
			// 
			this.txtBill.Height = 0.19F;
			this.txtBill.Left = 3.625F;
			this.txtBill.MultiLine = false;
			this.txtBill.Name = "txtBill";
			this.txtBill.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtBill.Text = "Field2";
			this.txtBill.Top = 0F;
			this.txtBill.Width = 0.4375F;
			// 
			// txtHolderNumber
			// 
			this.txtHolderNumber.Height = 0.19F;
			this.txtHolderNumber.Left = 4.125F;
			this.txtHolderNumber.MultiLine = false;
			this.txtHolderNumber.Name = "txtHolderNumber";
			this.txtHolderNumber.Style = "font-family: \'Tahoma\'";
			this.txtHolderNumber.Text = "Field3";
			this.txtHolderNumber.Top = 0F;
			this.txtHolderNumber.Width = 0.625F;
			// 
			// txtHolderName
			// 
			this.txtHolderName.Height = 0.19F;
			this.txtHolderName.Left = 4.8125F;
			this.txtHolderName.MultiLine = false;
			this.txtHolderName.Name = "txtHolderName";
			this.txtHolderName.Style = "font-family: \'Tahoma\'";
			this.txtHolderName.Text = "Field4";
			this.txtHolderName.Top = 0F;
			this.txtHolderName.Width = 2.5625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniname,
				this.txtDate,
				this.txtTitle,
				this.txtPage,
				this.lblPage,
				this.lblAccount,
				this.lblName,
				this.lblHolder,
				this.Label1,
				this.Label2,
				this.lblNumber,
				this.txtTime
			});
			this.PageHeader.Height = 0.5F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.19F;
			this.txtMuniname.Left = 0.0625F;
			this.txtMuniname.MultiLine = false;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "font-family: \'Tahoma\'";
			this.txtMuniname.Text = null;
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 1.5F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.375F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.21875F;
			this.txtTitle.Left = 1.625F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Real Estate List with Mortgage Holders";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 4.3125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 7F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 0.4375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.191F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.15625F;
			this.lblPage.Width = 0.625F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.191F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.0625F;
			this.lblAccount.MultiLine = false;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.3125F;
			this.lblAccount.Width = 0.688F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.191F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.8125F;
			this.lblName.MultiLine = false;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.3125F;
			this.lblName.Width = 1.188F;
			// 
			// lblHolder
			// 
			this.lblHolder.Height = 0.191F;
			this.lblHolder.HyperLink = null;
			this.lblHolder.Left = 4.8125F;
			this.lblHolder.MultiLine = false;
			this.lblHolder.Name = "lblHolder";
			this.lblHolder.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblHolder.Text = "Mortgage Holder";
			this.lblHolder.Top = 0.3125F;
			this.lblHolder.Width = 1.313F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.191F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label1.Text = "Escrow";
			this.Label1.Top = 0.3125F;
			this.Label1.Width = 0.625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.191F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 3.625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Bill";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 0.438F;
			// 
			// lblNumber
			// 
			this.lblNumber.Height = 0.191F;
			this.lblNumber.HyperLink = null;
			this.lblNumber.Left = 4.125F;
			this.lblNumber.Name = "lblNumber";
			this.lblNumber.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblNumber.Text = "Number";
			this.lblNumber.Top = 0.3125F;
			this.lblNumber.Width = 0.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.5F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptREMortgage
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEscrow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHolderNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHolderName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEscrow;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHolderNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHolderName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHolder;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
