﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for MDIParent.
	/// </summary>
	partial class MDIParent : BaseForm
	{
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCPictureBox picArchive;
		public fecherFoundation.FCPictureBox imgArchive;
		public FCCommonDialog CommonDialog1;
		public FCGrid GRID;
		public Wisej.Web.StatusBar StatusBar1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel2;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel3;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel4;
		public Wisej.Web.ImageList ImageList1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFExit;
		public fecherFoundation.FCToolStripMenuItem mnuForms;
		public fecherFoundation.FCToolStripMenuItem mnuPrintForms;
		public fecherFoundation.FCToolStripMenuItem mnuOMaxForms;
		public fecherFoundation.FCToolStripMenuItem mnuBangor;
		public fecherFoundation.FCToolStripMenuItem mnuEditDistrictValues;
		public fecherFoundation.FCToolStripMenuItem mnuHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRedbookHelp;
		public fecherFoundation.FCToolStripMenuItem mnuBudgetaryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCashReceiptsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuClerkHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCodeEnforcementHelp;
		public fecherFoundation.FCToolStripMenuItem mnuEnhanced911Help;
		public fecherFoundation.FCToolStripMenuItem mnuFixedAssetsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuGeneralEntryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuMotorVehicleRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPayrollHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPersonalPropertyHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRealEstateHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxCollectionsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuUtilityBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuVoterRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuHAbout;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MDIParent));
			this.Picture1 = new fecherFoundation.FCPictureBox();
			this.picArchive = new fecherFoundation.FCPictureBox();
			this.imgArchive = new fecherFoundation.FCPictureBox();
			this.CommonDialog1 = new FCCommonDialog();
			this.GRID = new FCGrid();
			this.StatusBar1 = new Wisej.Web.StatusBar();
			this.StatusBar1_Panel1 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel2 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel3 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel4 = new Wisej.Web.StatusBarPanel();
			this.components = new System.ComponentModel.Container();
			this.ImageList1 = new Wisej.Web.ImageList();
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOMaxForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBangor = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditDistrictValues = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRedbookHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBudgetaryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCashReceiptsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClerkHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCodeEnforcementHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEnhanced911Help = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFixedAssetsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGeneralEntryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMotorVehicleRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPayrollHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPersonalPropertyHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRealEstateHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxCollectionsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUtilityBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuVoterRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHAbout = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// Picture1
			//
			this.Picture1.Controls.Add(this.picArchive);
			this.Picture1.Name = "Picture1";
			this.Picture1.TabIndex = 2;
			this.Picture1.Location = new System.Drawing.Point(169, 24);
			this.Picture1.Size = new System.Drawing.Size(349, 453);
			this.Picture1.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.Picture1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(255)));
			this.Picture1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Right));
			//
			// picArchive
			//
			this.picArchive.Controls.Add(this.imgArchive);
			this.picArchive.Name = "picArchive";
			this.picArchive.TabIndex = 3;
			this.picArchive.Location = new System.Drawing.Point(0, 0);
			this.picArchive.Size = new System.Drawing.Size(809, 452);
			this.picArchive.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.picArchive.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(255)));
			//
			// imgArchive
			//
			this.imgArchive.Name = "imgArchive";
			this.imgArchive.Visible = false;
			this.imgArchive.Location = new System.Drawing.Point(14, 40);
			this.imgArchive.Size = new System.Drawing.Size(291, 73);
			this.imgArchive.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.imgArchive.BackColor = System.Drawing.SystemColors.Control;
			this.imgArchive.Image = ((System.Drawing.Image)(resources.GetObject("imgArchive.Image")));
			this.imgArchive.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			//
			// CommonDialog1
			//
			//
			// GRID
			//
			this.GRID.Name = "GRID";
			this.GRID.Enabled = true;
			this.GRID.TabIndex = 1;
			this.GRID.Location = new System.Drawing.Point(0, 24);
			this.GRID.Size = new System.Drawing.Size(168, 453);
			this.GRID.Rows = 21;
			this.GRID.Cols = 2;
			this.GRID.FixedRows = 0;
			this.GRID.FixedCols = 0;
			this.GRID.ExplorerBar = 0;
			this.GRID.TabBehavior = 0;
			this.GRID.Editable = 0;
			this.GRID.FrozenRows = 0;
			this.GRID.FrozenCols = 0;
			this.GRID.KeyDown += new KeyEventHandler(this.GRID_KeyDownEvent);
			this.GRID.KeyPress += new KeyPressEventHandler(this.GRID_KeyPressEvent);
			this.GRID.CellMouseDown += new DataGridViewCellMouseEventHandler(this.GRID_MouseDownEvent);
			this.GRID.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GRID_MouseMoveEvent);
			//
			// StatusBar1
			//
			this.StatusBar1.Panels.AddRange(new Wisej.Web.StatusBarPanel[] {
				this.StatusBar1_Panel1,
				this.StatusBar1_Panel2,
				this.StatusBar1_Panel3,
				this.StatusBar1_Panel4
			});
			this.StatusBar1.Name = "StatusBar1";
			this.StatusBar1.TabIndex = 0;
			this.StatusBar1.Location = new System.Drawing.Point(0, 477);
			this.StatusBar1.Size = new System.Drawing.Size(518, 19);
			this.StatusBar1.ShowPanels = true;
			this.StatusBar1.SizingGrip = false;
			//this.StatusBar1.Font = new System.Drawing.Font("MS Sans Serif", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Panel1
			//
			this.StatusBar1_Panel1.Text = "TRIO Software Corporation";
			this.StatusBar1_Panel1.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel1.Width = 166;
			this.StatusBar1_Panel1.MinWidth = 142;
			//
			// Panel2
			//
			this.StatusBar1_Panel2.Text = "";
			this.StatusBar1_Panel2.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel2.BorderStyle = Wisej.Web.StatusBarPanelBorderStyle.None;
			this.StatusBar1_Panel2.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Spring;
			this.StatusBar1_Panel2.Width = 184;
			//
			// Panel3
			//
			this.StatusBar1_Panel3.Text = "";
			this.StatusBar1_Panel3.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel3.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel3.Width = 77;
			this.StatusBar1_Panel3.MinWidth = 67;
			//
			// Panel4
			//
			this.StatusBar1_Panel4.Text = "";
			this.StatusBar1_Panel4.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel4.Width = 67;
			this.StatusBar1_Panel4.MinWidth = 67;
			//
			// ImageList1
			//
			this.ImageList1.ImageSize = new System.Drawing.Size(17, 19);
			//this.ImageList1.ColorDepth = Wisej.Web.ColorDepth.Depth8Bit;
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
			this.ImageList1.Images.SetKeyName(0, "Clear");
			this.ImageList1.Images.SetKeyName(1, "Left");
			this.ImageList1.Images.SetKeyName(2, "Right");
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			//
			// mnuFExit
			//
			this.mnuFExit.Name = "mnuFExit";
			this.mnuFExit.Text = "Exit";
			this.mnuFExit.Click += new System.EventHandler(this.mnuFExit_Click);
			//
			// mnuForms
			//
			this.mnuForms.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrintForms,
				this.mnuOMaxForms
			});
			this.mnuForms.Name = "mnuForms";
			this.mnuForms.Text = "Forms";
			//
			// mnuPrintForms
			//
			this.mnuPrintForms.Name = "mnuPrintForms";
			this.mnuPrintForms.Text = "Print Form(s)";
			this.mnuPrintForms.Click += new System.EventHandler(this.mnuPrintForms_Click);
			//
			// mnuOMaxForms
			//
			this.mnuOMaxForms.Name = "mnuOMaxForms";
			this.mnuOMaxForms.Text = "Maximize Forms";
			this.mnuOMaxForms.Click += new System.EventHandler(this.mnuOMaxForms_Click);
			//
			// mnuBangor
			//
			this.mnuBangor.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuEditDistrictValues
			});
			this.mnuBangor.Name = "mnuBangor";
			this.mnuBangor.Visible = false;
			this.mnuBangor.Text = "Bangor";
			//
			// mnuEditDistrictValues
			//
			this.mnuEditDistrictValues.Name = "mnuEditDistrictValues";
			this.mnuEditDistrictValues.Text = "District Valuations";
			this.mnuEditDistrictValues.Click += new System.EventHandler(this.mnuEditDistrictValues_Click);
			//
			// mnuHelp
			//
			this.mnuHelp.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRedbookHelp,
				this.mnuBudgetaryHelp,
				this.mnuCashReceiptsHelp,
				this.mnuClerkHelp,
				this.mnuCodeEnforcementHelp,
				this.mnuEnhanced911Help,
				this.mnuFixedAssetsHelp,
				this.mnuGeneralEntryHelp,
				this.mnuMotorVehicleRegistrationHelp,
				this.mnuPayrollHelp,
				this.mnuPersonalPropertyHelp,
				this.mnuRealEstateHelp,
				this.mnuTaxBillingHelp,
				this.mnuTaxCollectionsHelp,
				this.mnuUtilityBillingHelp,
				this.mnuVoterRegistrationHelp,
				this.mnuSepar,
				this.mnuHAbout
			});
			this.mnuHelp.Name = "mnuHelp";
			this.mnuHelp.Text = "Help";
			//
			// mnuRedbookHelp
			//
			this.mnuRedbookHelp.Name = "mnuRedbookHelp";
			this.mnuRedbookHelp.Text = "Blue Book";
			this.mnuRedbookHelp.Click += new System.EventHandler(this.mnuRedbookHelp_Click);
			//
			// mnuBudgetaryHelp
			//
			this.mnuBudgetaryHelp.Name = "mnuBudgetaryHelp";
			this.mnuBudgetaryHelp.Text = "Budgetary";
			this.mnuBudgetaryHelp.Click += new System.EventHandler(this.mnuBudgetaryHelp_Click);
			//
			// mnuCashReceiptsHelp
			//
			this.mnuCashReceiptsHelp.Name = "mnuCashReceiptsHelp";
			this.mnuCashReceiptsHelp.Text = "Cash Receipts";
			this.mnuCashReceiptsHelp.Click += new System.EventHandler(this.mnuCashReceiptsHelp_Click);
			//
			// mnuClerkHelp
			//
			this.mnuClerkHelp.Name = "mnuClerkHelp";
			this.mnuClerkHelp.Text = "Clerk";
			this.mnuClerkHelp.Click += new System.EventHandler(this.mnuClerkHelp_Click);
			//
			// mnuCodeEnforcementHelp
			//
			this.mnuCodeEnforcementHelp.Name = "mnuCodeEnforcementHelp";
			this.mnuCodeEnforcementHelp.Text = "Code Enforcement";
			this.mnuCodeEnforcementHelp.Click += new System.EventHandler(this.mnuCodeEnforcementHelp_Click);
			//
			// mnuEnhanced911Help
			//
			this.mnuEnhanced911Help.Name = "mnuEnhanced911Help";
			this.mnuEnhanced911Help.Text = "Enhanced 911";
			this.mnuEnhanced911Help.Click += new System.EventHandler(this.mnuEnhanced911Help_Click);
			//
			// mnuFixedAssetsHelp
			//
			this.mnuFixedAssetsHelp.Name = "mnuFixedAssetsHelp";
			this.mnuFixedAssetsHelp.Text = "Fixed Assets";
			//
			// mnuGeneralEntryHelp
			//
			this.mnuGeneralEntryHelp.Name = "mnuGeneralEntryHelp";
			this.mnuGeneralEntryHelp.Text = "General Entry";
			this.mnuGeneralEntryHelp.Click += new System.EventHandler(this.mnuGeneralEntryHelp_Click);
			//
			// mnuMotorVehicleRegistrationHelp
			//
			this.mnuMotorVehicleRegistrationHelp.Name = "mnuMotorVehicleRegistrationHelp";
			this.mnuMotorVehicleRegistrationHelp.Text = "Motor Vehicle";
			this.mnuMotorVehicleRegistrationHelp.Click += new System.EventHandler(this.mnuMotorVehicleRegistrationHelp_Click);
			//
			// mnuPayrollHelp
			//
			this.mnuPayrollHelp.Name = "mnuPayrollHelp";
			this.mnuPayrollHelp.Text = "Payroll";
			this.mnuPayrollHelp.Click += new System.EventHandler(this.mnuPayrollHelp_Click);
			//
			// mnuPersonalPropertyHelp
			//
			this.mnuPersonalPropertyHelp.Name = "mnuPersonalPropertyHelp";
			this.mnuPersonalPropertyHelp.Text = "Personal Property";
			this.mnuPersonalPropertyHelp.Click += new System.EventHandler(this.mnuPersonalPropertyHelp_Click);
			//
			// mnuRealEstateHelp
			//
			this.mnuRealEstateHelp.Name = "mnuRealEstateHelp";
			this.mnuRealEstateHelp.Text = "Real Estate";
			this.mnuRealEstateHelp.Click += new System.EventHandler(this.mnuRealEstateHelp_Click);
			//
			// mnuTaxBillingHelp
			//
			this.mnuTaxBillingHelp.Name = "mnuTaxBillingHelp";
			this.mnuTaxBillingHelp.Text = "Tax Billing";
			this.mnuTaxBillingHelp.Click += new System.EventHandler(this.mnuTaxBillingHelp_Click);
			//
			// mnuTaxCollectionsHelp
			//
			this.mnuTaxCollectionsHelp.Name = "mnuTaxCollectionsHelp";
			this.mnuTaxCollectionsHelp.Text = "Tax Collections";
			this.mnuTaxCollectionsHelp.Click += new System.EventHandler(this.mnuTaxCollectionsHelp_Click);
			//
			// mnuUtilityBillingHelp
			//
			this.mnuUtilityBillingHelp.Name = "mnuUtilityBillingHelp";
			this.mnuUtilityBillingHelp.Text = "Utility Billing";
			this.mnuUtilityBillingHelp.Click += new System.EventHandler(this.mnuUtilityBillingHelp_Click);
			//
			// mnuVoterRegistrationHelp
			//
			this.mnuVoterRegistrationHelp.Name = "mnuVoterRegistrationHelp";
			this.mnuVoterRegistrationHelp.Text = "Voter Registration";
			this.mnuVoterRegistrationHelp.Click += new System.EventHandler(this.mnuVoterRegistrationHelp_Click);
			//
			// mnuSepar
			//
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			//
			// mnuHAbout
			//
			this.mnuHAbout.Name = "mnuHAbout";
			this.mnuHAbout.Text = "About";
			this.mnuHAbout.Click += new System.EventHandler(this.mnuHAbout_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFile,
				this.mnuForms,
				this.mnuBangor,
				this.mnuHelp
			});
			//
			// MDIParent
			//
			this.ClientSize = new System.Drawing.Size(518, 472);
			this.ClientArea.Controls.Add(this.Picture1);
			this.ClientArea.Controls.Add(this.GRID);
			this.ClientArea.Controls.Add(this.StatusBar1);
			this.Menu = this.MainMenu1;
			this.IsMdiContainer = true;
			this.Name = "MDIParent";
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("MDIParent.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.WindowState = Wisej.Web.FormWindowState.Maximized;
			this.Activated += new System.EventHandler(this.MDIParent_Activated);
			//this.Load += new System.EventHandler(this.MDIParent_Load);
			this.Resize += new System.EventHandler(this.MDIParent_Resize);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.MDIForm_Unload);
			this.Text = "Tax Billing";
			this.picArchive.ResumeLayout(false);
			this.Picture1.ResumeLayout(false);
			this.GRID.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GRID)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
