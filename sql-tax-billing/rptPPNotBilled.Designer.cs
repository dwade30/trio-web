﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPNotBilled.
	/// </summary>
	partial class rptPPNotBilled
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPPNotBilled));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtAssessment
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.lblDate,
				this.lblMuniName,
				this.Field1,
				this.Label2,
				this.Field3,
				this.Label3,
				this.Label4,
				this.txtTime
			});
			this.PageHeader.Height = 0.53125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.2175F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.75F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Personal Property Accounts Not Billed for ";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 4.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.18625F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.4375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "";
			this.lblDate.Text = "Label2";
			this.lblDate.Top = 0.03125F;
			this.lblDate.Width = 1F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.18625F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "";
			this.lblMuniName.Text = "Label2";
			this.lblMuniName.Top = 0.03125F;
			this.lblMuniName.Width = 1.4375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 6.8125F;
			this.Field1.Name = "Field1";
			this.Field1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.Field1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.Field1.Text = null;
			this.Field1.Top = 0.1875F;
			this.Field1.Width = 0.625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 6.25F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "";
			this.Label2.Text = "Page";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 0.5625F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.18625F;
			this.Field3.Left = 4.0625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-weight: bold; text-align: right";
			this.Field3.Text = "Taxable Assessment";
			this.Field3.Top = 0.375F;
			this.Field3.Width = 1.625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.18625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Name";
			this.Label3.Top = 0.375F;
			this.Label3.Width = 1.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.18625F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Account";
			this.Label4.Top = 0.375F;
			this.Label4.Width = 0.6875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0.1875F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Text = "Field2";
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 1.125F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field2";
			this.txtName.Top = 0F;
			this.txtName.Width = 3.125F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 4.3125F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Text = "Field3";
			this.txtAssessment.Top = 0F;
			this.txtAssessment.Width = 1.375F;
			// 
			// rptPPNotBilled
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
