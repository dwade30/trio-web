﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptRENormalOutprint.
	/// </summary>
	partial class rptRENormalOutprint
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRENormalOutprint));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPaidToDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDist1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPeriod2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Address1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblcat1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblcat2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblcat3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblcat49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCat49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptPP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessPP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalPP = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExemptPP = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessPP = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalPPTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPPTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidToDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Address1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblcat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblcat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblcat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblcat49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExemptPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPPTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPPTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLand,
				this.txtAccount,
				this.txtName,
				this.txtBldg,
				this.txtPaidToDate,
				this.txtTax,
				this.txtMapLot,
				this.Label1,
				this.txtDist1,
				this.txtDist2,
				this.txtDist3,
				this.txtDist4,
				this.txtDist5,
				this.txtPeriod1,
				this.txtPeriod3,
				this.txtPeriod4,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.txtPeriod2,
				this.Address1,
				this.txtAddress2,
				this.txtAddress3,
				this.Label20,
				this.txtHomestead,
				this.Label21,
				this.txtOther,
				this.lblcat1,
				this.txtCat1,
				this.lblcat2,
				this.txtCat2,
				this.lblcat3,
				this.txtCat3,
				this.lblcat49,
				this.txtCat49,
				this.txtTotalPP,
				this.txtExemptPP,
				this.txtAssessPP,
				this.lblTotalPP,
				this.lblExemptPP,
				this.lblAssessPP
			});
			this.Detail.Height = 1.083333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCount,
				this.Label10,
				this.txtTotalTax,
				this.Label22,
				this.txtTotalLand,
				this.Label24,
				this.txtTotalBuilding,
				this.Label25,
				this.Label26,
				this.txtTotalHomestead,
				this.txtTotalOther,
				this.Label27,
				this.txtTotalAssessment,
				this.Label28,
				this.txtTotalExempt,
				this.Label29,
				this.txtTotalPPTotal,
				this.lblPPTotal
			});
			this.ReportFooter.Height = 0.5208333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblMuniname,
				this.lblTitle,
				this.txtPage,
				this.lblPage,
				this.txtDate,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label11,
				this.txtTime
			});
			this.PageHeader.Height = 0.5F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.15625F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "text-align: left";
			this.lblMuniname.Tag = "textbox";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 1.75F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
            this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.8125F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Tag = "bold";
			this.lblTitle.Text = "Real Estate Out-Print Report";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 4F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.15625F;
			this.txtPage.Left = 7F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Tag = "textbox";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 0.4375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.15625F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.125F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "text-align: right";
			this.lblPage.Tag = "textbox";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.15625F;
			this.lblPage.Width = 0.875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.15625F;
			this.txtDate.Left = 6.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Tag = "textbox";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold";
			this.Label12.Tag = "bold";
			this.Label12.Text = "Name";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 1.6875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Tag = "bold";
			this.Label13.Text = "Land";
			this.Label13.Top = 0.3125F;
			this.Label13.Width = 0.875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.5625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Tag = "bold";
			this.Label14.Text = "Building";
			this.Label14.Top = 0.3125F;
			this.Label14.Width = 0.9375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.5625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Tag = "bold";
			this.Label15.Text = "Paid To Date";
			this.Label15.Top = 0.3125F;
			this.Label15.Width = 0.875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 6.5F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold; text-align: right";
			this.Label17.Tag = "bold";
			this.Label17.Text = "Tax";
			this.Label17.Top = 0.3125F;
			this.Label17.Width = 0.9375F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.375F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold; text-align: left";
			this.Label18.Tag = "bold";
			this.Label18.Text = "Map/Lot";
			this.Label18.Top = 0.3125F;
			this.Label18.Width = 1.1875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 1.875F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold";
			this.Label19.Tag = "bold";
			this.Label19.Text = "Location";
			this.Label19.Top = 0.15625F;
			this.Label19.Visible = false;
			this.Label19.Width = 0.9375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Tag = "bold";
			this.Label11.Text = "Account";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 0.5625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.15625F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "text-align: left";
			this.txtTime.Tag = "textbox";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.DataField = "landval";
			this.txtLand.Height = 0.15625F;
			this.txtLand.Left = 3.625F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.OutputFormat = resources.GetString("txtLand.OutputFormat");
			this.txtLand.Style = "font-size: 8pt; text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 0F;
			this.txtLand.Width = 0.875F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.15625F;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 8pt; text-align: right";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.5625F;
			// 
			// txtName
			// 
			this.txtName.CanShrink = true;
			this.txtName.Height = 0.15625F;
			this.txtName.Left = 0.625F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 8pt";
			this.txtName.Tag = "textbox";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 1.6875F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.DataField = "bldgval";
			this.txtBldg.Height = 0.15625F;
			this.txtBldg.Left = 4.5625F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.OutputFormat = resources.GetString("txtBldg.OutputFormat");
			this.txtBldg.Style = "font-size: 8pt; text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0F;
			this.txtBldg.Width = 0.9375F;
			// 
			// txtPaidToDate
			// 
			this.txtPaidToDate.CanGrow = false;
			this.txtPaidToDate.DataField = "exemptval";
			this.txtPaidToDate.Height = 0.15625F;
			this.txtPaidToDate.Left = 5.5625F;
			this.txtPaidToDate.MultiLine = false;
			this.txtPaidToDate.Name = "txtPaidToDate";
			this.txtPaidToDate.OutputFormat = resources.GetString("txtPaidToDate.OutputFormat");
			this.txtPaidToDate.Style = "font-size: 8pt; text-align: right";
			this.txtPaidToDate.Tag = "textbox";
			this.txtPaidToDate.Text = null;
			this.txtPaidToDate.Top = 0F;
			this.txtPaidToDate.Width = 0.875F;
			// 
			// txtTax
			// 
			this.txtTax.CanGrow = false;
			this.txtTax.DataField = "taxval";
			this.txtTax.Height = 0.15625F;
			this.txtTax.Left = 6.5F;
			this.txtTax.MultiLine = false;
			this.txtTax.Name = "txtTax";
			this.txtTax.OutputFormat = resources.GetString("txtTax.OutputFormat");
			this.txtTax.Style = "font-size: 8pt; text-align: right";
			this.txtTax.Tag = "textbox";
			this.txtTax.Text = null;
			this.txtTax.Top = 0F;
			this.txtTax.Width = 1F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.15625F;
			this.txtMapLot.Left = 2.375F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-size: 8pt";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.1875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.15625F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Tag = "bold";
			this.Label1.Text = "Dist. 1";
			this.Label1.Top = 0.625F;
			this.Label1.Width = 0.75F;
			// 
			// txtDist1
			// 
			this.txtDist1.CanGrow = false;
			this.txtDist1.Height = 0.15625F;
			this.txtDist1.Left = 0F;
			this.txtDist1.MultiLine = false;
			this.txtDist1.Name = "txtDist1";
			this.txtDist1.Style = "text-align: right";
			this.txtDist1.Tag = "textbox";
			this.txtDist1.Text = null;
			this.txtDist1.Top = 0.78125F;
			this.txtDist1.Width = 0.75F;
			// 
			// txtDist2
			// 
			this.txtDist2.CanGrow = false;
			this.txtDist2.Height = 0.15625F;
			this.txtDist2.Left = 0.8125F;
			this.txtDist2.MultiLine = false;
			this.txtDist2.Name = "txtDist2";
			this.txtDist2.Style = "text-align: right";
			this.txtDist2.Tag = "textbox";
			this.txtDist2.Text = null;
			this.txtDist2.Top = 0.78125F;
			this.txtDist2.Width = 0.75F;
			// 
			// txtDist3
			// 
			this.txtDist3.CanGrow = false;
			this.txtDist3.Height = 0.15625F;
			this.txtDist3.Left = 1.625F;
			this.txtDist3.MultiLine = false;
			this.txtDist3.Name = "txtDist3";
			this.txtDist3.Style = "text-align: right";
			this.txtDist3.Tag = "textbox";
			this.txtDist3.Text = null;
			this.txtDist3.Top = 0.78125F;
			this.txtDist3.Width = 0.75F;
			// 
			// txtDist4
			// 
			this.txtDist4.CanGrow = false;
			this.txtDist4.Height = 0.15625F;
			this.txtDist4.Left = 2.4375F;
			this.txtDist4.MultiLine = false;
			this.txtDist4.Name = "txtDist4";
			this.txtDist4.Style = "text-align: right";
			this.txtDist4.Tag = "textbox";
			this.txtDist4.Text = null;
			this.txtDist4.Top = 0.78125F;
			this.txtDist4.Width = 0.75F;
			// 
			// txtDist5
			// 
			this.txtDist5.CanGrow = false;
			this.txtDist5.Height = 0.15625F;
			this.txtDist5.Left = 3.25F;
			this.txtDist5.MultiLine = false;
			this.txtDist5.Name = "txtDist5";
			this.txtDist5.Style = "text-align: right";
			this.txtDist5.Tag = "textbox";
			this.txtDist5.Text = null;
			this.txtDist5.Top = 0.78125F;
			this.txtDist5.Width = 0.75F;
			// 
			// txtPeriod1
			// 
			this.txtPeriod1.CanGrow = false;
			this.txtPeriod1.Height = 0.15625F;
			this.txtPeriod1.Left = 4.0625F;
			this.txtPeriod1.MultiLine = false;
			this.txtPeriod1.Name = "txtPeriod1";
			this.txtPeriod1.Style = "text-align: right";
			this.txtPeriod1.Tag = "textbox";
			this.txtPeriod1.Text = null;
			this.txtPeriod1.Top = 0.78125F;
			this.txtPeriod1.Width = 0.8125F;
			// 
			// txtPeriod3
			// 
			this.txtPeriod3.CanGrow = false;
			this.txtPeriod3.Height = 0.15625F;
			this.txtPeriod3.Left = 5.8125F;
			this.txtPeriod3.MultiLine = false;
			this.txtPeriod3.Name = "txtPeriod3";
			this.txtPeriod3.Style = "text-align: right";
			this.txtPeriod3.Tag = "textbox";
			this.txtPeriod3.Text = null;
			this.txtPeriod3.Top = 0.78125F;
			this.txtPeriod3.Width = 0.8125F;
			// 
			// txtPeriod4
			// 
			this.txtPeriod4.CanGrow = false;
			this.txtPeriod4.Height = 0.15625F;
			this.txtPeriod4.Left = 6.6875F;
			this.txtPeriod4.MultiLine = false;
			this.txtPeriod4.Name = "txtPeriod4";
			this.txtPeriod4.Style = "text-align: right";
			this.txtPeriod4.Tag = "textbox";
			this.txtPeriod4.Text = null;
			this.txtPeriod4.Top = 0.78125F;
			this.txtPeriod4.Width = 0.8125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.15625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.8125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: right";
			this.Label2.Tag = "bold";
			this.Label2.Text = "Dist. 2";
			this.Label2.Top = 0.625F;
			this.Label2.Width = 0.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.15625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Tag = "bold";
			this.Label3.Text = "Dist. 3";
			this.Label3.Top = 0.625F;
			this.Label3.Width = 0.75F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.15625F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.4375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Tag = "bold";
			this.Label4.Text = "Dist. 4";
			this.Label4.Top = 0.625F;
			this.Label4.Width = 0.75F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.15625F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.25F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Tag = "bold";
			this.Label5.Text = "Dist. 5";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 0.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.15625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Tag = "bold";
			this.Label6.Text = "Period 1";
			this.Label6.Top = 0.625F;
			this.Label6.Width = 0.8125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.15625F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.9375F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Tag = "bold";
			this.Label7.Text = "Period 2";
			this.Label7.Top = 0.625F;
			this.Label7.Width = 0.8125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.15625F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 5.8125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Tag = "bold";
			this.Label8.Text = "Period 3";
			this.Label8.Top = 0.625F;
			this.Label8.Width = 0.8125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.15625F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 6.6875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Period 4";
			this.Label9.Top = 0.625F;
			this.Label9.Width = 0.8125F;
			// 
			// txtPeriod2
			// 
			this.txtPeriod2.CanGrow = false;
			this.txtPeriod2.Height = 0.15625F;
			this.txtPeriod2.Left = 4.9375F;
			this.txtPeriod2.MultiLine = false;
			this.txtPeriod2.Name = "txtPeriod2";
			this.txtPeriod2.Style = "text-align: right";
			this.txtPeriod2.Tag = "textbox";
			this.txtPeriod2.Text = null;
			this.txtPeriod2.Top = 0.78125F;
			this.txtPeriod2.Width = 0.8125F;
			// 
			// Address1
			// 
			this.Address1.CanGrow = false;
			this.Address1.Height = 0.15625F;
			this.Address1.Left = 0.625F;
			this.Address1.Name = "Address1";
			this.Address1.Style = "font-size: 8pt";
			this.Address1.Tag = "textbox";
			this.Address1.Text = null;
			this.Address1.Top = 0.15625F;
			this.Address1.Width = 1.6875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.CanGrow = false;
			this.txtAddress2.Height = 0.15625F;
			this.txtAddress2.Left = 0.625F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-size: 8pt";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 1.6875F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.CanGrow = false;
			this.txtAddress3.Height = 0.15625F;
			this.txtAddress3.Left = 0.625F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Style = "font-size: 8pt";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.46875F;
			this.txtAddress3.Width = 1.6875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.15625F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.375F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold; text-align: left";
			this.Label20.Tag = "bold";
			this.Label20.Text = "Homestead";
			this.Label20.Top = 0.15625F;
			this.Label20.Width = 0.75F;
			// 
			// txtHomestead
			// 
			this.txtHomestead.CanGrow = false;
			this.txtHomestead.DataField = "landval";
			this.txtHomestead.Height = 0.15625F;
			this.txtHomestead.Left = 3.1875F;
			this.txtHomestead.MultiLine = false;
			this.txtHomestead.Name = "txtHomestead";
			this.txtHomestead.OutputFormat = resources.GetString("txtHomestead.OutputFormat");
			this.txtHomestead.Style = "font-size: 8pt; text-align: right";
			this.txtHomestead.Tag = "textbox";
			this.txtHomestead.Text = null;
			this.txtHomestead.Top = 0.15625F;
			this.txtHomestead.Width = 0.875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.15625F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.125F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold; text-align: left";
			this.Label21.Tag = "bold";
			this.Label21.Text = "Other Exempt";
			this.Label21.Top = 0.15625F;
			this.Label21.Width = 0.875F;
			// 
			// txtOther
			// 
			this.txtOther.CanGrow = false;
			this.txtOther.DataField = "exemptval";
			this.txtOther.Height = 0.15625F;
			this.txtOther.Left = 5.0625F;
			this.txtOther.MultiLine = false;
			this.txtOther.Name = "txtOther";
			this.txtOther.OutputFormat = resources.GetString("txtOther.OutputFormat");
			this.txtOther.Style = "font-size: 8pt; text-align: right";
			this.txtOther.Tag = "textbox";
			this.txtOther.Text = null;
			this.txtOther.Top = 0.15625F;
			this.txtOther.Width = 0.875F;
			// 
			// lblcat1
			// 
			this.lblcat1.Height = 0.15625F;
			this.lblcat1.HyperLink = null;
			this.lblcat1.Left = 2.375F;
			this.lblcat1.Name = "lblcat1";
			this.lblcat1.Style = "font-weight: bold; text-align: left";
			this.lblcat1.Tag = "bold";
			this.lblcat1.Text = "Category 1";
			this.lblcat1.Top = 0.3125F;
			this.lblcat1.Width = 0.75F;
			// 
			// txtCat1
			// 
			this.txtCat1.CanGrow = false;
			this.txtCat1.DataField = "landval";
			this.txtCat1.Height = 0.15625F;
			this.txtCat1.Left = 3.1875F;
			this.txtCat1.MultiLine = false;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.OutputFormat = resources.GetString("txtCat1.OutputFormat");
			this.txtCat1.Style = "font-size: 8pt; text-align: right";
			this.txtCat1.Tag = "textbox";
			this.txtCat1.Text = null;
			this.txtCat1.Top = 0.3125F;
			this.txtCat1.Width = 0.875F;
			// 
			// lblcat2
			// 
			this.lblcat2.Height = 0.15625F;
			this.lblcat2.HyperLink = null;
			this.lblcat2.Left = 2.375F;
			this.lblcat2.Name = "lblcat2";
			this.lblcat2.Style = "font-weight: bold; text-align: left";
			this.lblcat2.Tag = "bold";
			this.lblcat2.Text = "Category 2";
			this.lblcat2.Top = 0.46875F;
			this.lblcat2.Width = 0.75F;
			// 
			// txtCat2
			// 
			this.txtCat2.CanGrow = false;
			this.txtCat2.DataField = "landval";
			this.txtCat2.Height = 0.15625F;
			this.txtCat2.Left = 3.1875F;
			this.txtCat2.MultiLine = false;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.OutputFormat = resources.GetString("txtCat2.OutputFormat");
			this.txtCat2.Style = "font-size: 8pt; text-align: right";
			this.txtCat2.Tag = "textbox";
			this.txtCat2.Text = null;
			this.txtCat2.Top = 0.46875F;
			this.txtCat2.Width = 0.875F;
			// 
			// lblcat3
			// 
			this.lblcat3.Height = 0.15625F;
			this.lblcat3.HyperLink = null;
			this.lblcat3.Left = 4.125F;
			this.lblcat3.Name = "lblcat3";
			this.lblcat3.Style = "font-weight: bold; text-align: left";
			this.lblcat3.Tag = "bold";
			this.lblcat3.Text = "Category 3";
			this.lblcat3.Top = 0.3125F;
			this.lblcat3.Width = 0.875F;
			// 
			// txtCat3
			// 
			this.txtCat3.CanGrow = false;
			this.txtCat3.DataField = "landval";
			this.txtCat3.Height = 0.15625F;
			this.txtCat3.Left = 5.0625F;
			this.txtCat3.MultiLine = false;
			this.txtCat3.Name = "txtCat3";
			this.txtCat3.OutputFormat = resources.GetString("txtCat3.OutputFormat");
			this.txtCat3.Style = "font-size: 8pt; text-align: right";
			this.txtCat3.Tag = "textbox";
			this.txtCat3.Text = null;
			this.txtCat3.Top = 0.3125F;
			this.txtCat3.Width = 0.875F;
			// 
			// lblcat49
			// 
			this.lblcat49.Height = 0.15625F;
			this.lblcat49.HyperLink = null;
			this.lblcat49.Left = 4.125F;
			this.lblcat49.Name = "lblcat49";
			this.lblcat49.Style = "font-weight: bold; text-align: left";
			this.lblcat49.Tag = "bold";
			this.lblcat49.Text = "Category 4-9";
			this.lblcat49.Top = 0.46875F;
			this.lblcat49.Width = 0.875F;
			// 
			// txtCat49
			// 
			this.txtCat49.CanGrow = false;
			this.txtCat49.DataField = "landval";
			this.txtCat49.Height = 0.15625F;
			this.txtCat49.Left = 5.0625F;
			this.txtCat49.MultiLine = false;
			this.txtCat49.Name = "txtCat49";
			this.txtCat49.OutputFormat = resources.GetString("txtCat49.OutputFormat");
			this.txtCat49.Style = "font-size: 8pt; text-align: right";
			this.txtCat49.Tag = "textbox";
			this.txtCat49.Text = null;
			this.txtCat49.Top = 0.46875F;
			this.txtCat49.Width = 0.875F;
			// 
			// txtTotalPP
			// 
			this.txtTotalPP.CanGrow = false;
			this.txtTotalPP.DataField = "exemptval";
			this.txtTotalPP.Height = 0.15625F;
			this.txtTotalPP.Left = 6.625F;
			this.txtTotalPP.MultiLine = false;
			this.txtTotalPP.Name = "txtTotalPP";
			this.txtTotalPP.OutputFormat = resources.GetString("txtTotalPP.OutputFormat");
			this.txtTotalPP.Style = "font-size: 8pt; text-align: right";
			this.txtTotalPP.Tag = "textbox";
			this.txtTotalPP.Text = null;
			this.txtTotalPP.Top = 0.15625F;
			this.txtTotalPP.Width = 0.875F;
			// 
			// txtExemptPP
			// 
			this.txtExemptPP.CanGrow = false;
			this.txtExemptPP.DataField = "landval";
			this.txtExemptPP.Height = 0.15625F;
			this.txtExemptPP.Left = 6.6875F;
			this.txtExemptPP.MultiLine = false;
			this.txtExemptPP.Name = "txtExemptPP";
			this.txtExemptPP.OutputFormat = resources.GetString("txtExemptPP.OutputFormat");
			this.txtExemptPP.Style = "font-size: 8pt; text-align: right";
			this.txtExemptPP.Tag = "textbox";
			this.txtExemptPP.Text = null;
			this.txtExemptPP.Top = 0.3125F;
			this.txtExemptPP.Width = 0.8125F;
			// 
			// txtAssessPP
			// 
			this.txtAssessPP.CanGrow = false;
			this.txtAssessPP.DataField = "landval";
			this.txtAssessPP.Height = 0.15625F;
			this.txtAssessPP.Left = 6.625F;
			this.txtAssessPP.MultiLine = false;
			this.txtAssessPP.Name = "txtAssessPP";
			this.txtAssessPP.OutputFormat = resources.GetString("txtAssessPP.OutputFormat");
			this.txtAssessPP.Style = "font-size: 8pt; text-align: right";
			this.txtAssessPP.Tag = "textbox";
			this.txtAssessPP.Text = null;
			this.txtAssessPP.Top = 0.46875F;
			this.txtAssessPP.Width = 0.875F;
			// 
			// lblTotalPP
			// 
			this.lblTotalPP.Height = 0.15625F;
			this.lblTotalPP.HyperLink = null;
			this.lblTotalPP.Left = 6F;
			this.lblTotalPP.Name = "lblTotalPP";
			this.lblTotalPP.Style = "font-weight: bold; text-align: left";
			this.lblTotalPP.Tag = "bold";
			this.lblTotalPP.Text = "Total PP";
			this.lblTotalPP.Top = 0.15625F;
			this.lblTotalPP.Width = 0.625F;
			// 
			// lblExemptPP
			// 
			this.lblExemptPP.Height = 0.15625F;
			this.lblExemptPP.HyperLink = null;
			this.lblExemptPP.Left = 6F;
			this.lblExemptPP.Name = "lblExemptPP";
			this.lblExemptPP.Style = "font-weight: bold; text-align: left";
			this.lblExemptPP.Tag = "bold";
			this.lblExemptPP.Text = "Exempt PP";
			this.lblExemptPP.Top = 0.3125F;
			this.lblExemptPP.Width = 0.6875F;
			// 
			// lblAssessPP
			// 
			this.lblAssessPP.Height = 0.15625F;
			this.lblAssessPP.HyperLink = null;
			this.lblAssessPP.Left = 6F;
			this.lblAssessPP.Name = "lblAssessPP";
			this.lblAssessPP.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.lblAssessPP.Tag = "bold";
			this.lblAssessPP.Text = "Assess";
			this.lblAssessPP.Top = 0.46875F;
			this.lblAssessPP.Width = 0.625F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.15625F;
			this.txtCount.Left = 0.8125F;
			this.txtCount.MultiLine = false;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "text-align: right";
			this.txtCount.Tag = "textbox";
			this.txtCount.Text = null;
			this.txtCount.Top = 0.3125F;
			this.txtCount.Width = 1F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.15625F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.0625F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: left";
			this.Label10.Tag = "bold";
			this.Label10.Text = "Count";
			this.Label10.Top = 0.3125F;
			this.Label10.Width = 0.6875F;
			// 
			// txtTotalTax
			// 
			this.txtTotalTax.Height = 0.15625F;
			this.txtTotalTax.Left = 6.3125F;
			this.txtTotalTax.MultiLine = false;
			this.txtTotalTax.Name = "txtTotalTax";
			this.txtTotalTax.Style = "text-align: right";
			this.txtTotalTax.Tag = "textbox";
			this.txtTotalTax.Text = null;
			this.txtTotalTax.Top = 0.15625F;
			this.txtTotalTax.Width = 1F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.15625F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 5.375F;
			this.Label22.MultiLine = false;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-weight: bold; text-align: left";
			this.Label22.Tag = "bold";
			this.Label22.Text = "Tax";
			this.Label22.Top = 0.15625F;
			this.Label22.Width = 0.375F;
			// 
			// txtTotalLand
			// 
			this.txtTotalLand.Height = 0.15625F;
			this.txtTotalLand.Left = 0.8125F;
			this.txtTotalLand.MultiLine = false;
			this.txtTotalLand.Name = "txtTotalLand";
			this.txtTotalLand.Style = "text-align: right";
			this.txtTotalLand.Tag = "textbox";
			this.txtTotalLand.Text = null;
			this.txtTotalLand.Top = 0F;
			this.txtTotalLand.Width = 1F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.15625F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.0625F;
			this.Label24.MultiLine = false;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-weight: bold; text-align: left";
			this.Label24.Tag = "bold";
			this.Label24.Text = "Land";
			this.Label24.Top = 0F;
			this.Label24.Width = 0.6875F;
			// 
			// txtTotalBuilding
			// 
			this.txtTotalBuilding.Height = 0.15625F;
			this.txtTotalBuilding.Left = 2.625F;
			this.txtTotalBuilding.MultiLine = false;
			this.txtTotalBuilding.Name = "txtTotalBuilding";
			this.txtTotalBuilding.Style = "text-align: right";
			this.txtTotalBuilding.Tag = "textbox";
			this.txtTotalBuilding.Text = null;
			this.txtTotalBuilding.Top = 0F;
			this.txtTotalBuilding.Width = 1F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.15625F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 1.875F;
			this.Label25.MultiLine = false;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-weight: bold; text-align: left";
			this.Label25.Tag = "bold";
			this.Label25.Text = "Building";
			this.Label25.Top = 0F;
			this.Label25.Width = 0.6875F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.15625F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.0625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-weight: bold; text-align: left";
			this.Label26.Tag = "bold";
			this.Label26.Text = "Homestead";
			this.Label26.Top = 0.15625F;
			this.Label26.Width = 0.6875F;
			// 
			// txtTotalHomestead
			// 
			this.txtTotalHomestead.CanGrow = false;
			this.txtTotalHomestead.DataField = "landval";
			this.txtTotalHomestead.Height = 0.15625F;
			this.txtTotalHomestead.Left = 0.8125F;
			this.txtTotalHomestead.MultiLine = false;
			this.txtTotalHomestead.Name = "txtTotalHomestead";
			this.txtTotalHomestead.OutputFormat = resources.GetString("txtTotalHomestead.OutputFormat");
			this.txtTotalHomestead.Style = "font-size: 8pt; text-align: right";
			this.txtTotalHomestead.Tag = "textbox";
			this.txtTotalHomestead.Text = null;
			this.txtTotalHomestead.Top = 0.15625F;
			this.txtTotalHomestead.Width = 1F;
			// 
			// txtTotalOther
			// 
			this.txtTotalOther.Height = 0.15625F;
			this.txtTotalOther.Left = 2.625F;
			this.txtTotalOther.MultiLine = false;
			this.txtTotalOther.Name = "txtTotalOther";
			this.txtTotalOther.Style = "text-align: right";
			this.txtTotalOther.Tag = "textbox";
			this.txtTotalOther.Text = null;
			this.txtTotalOther.Top = 0.15625F;
			this.txtTotalOther.Width = 1F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.15625F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 1.875F;
			this.Label27.MultiLine = false;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-weight: bold; text-align: left";
			this.Label27.Tag = "bold";
			this.Label27.Text = "Other";
			this.Label27.Top = 0.15625F;
			this.Label27.Width = 0.6875F;
			// 
			// txtTotalAssessment
			// 
			this.txtTotalAssessment.Height = 0.15625F;
			this.txtTotalAssessment.Left = 6.3125F;
			this.txtTotalAssessment.MultiLine = false;
			this.txtTotalAssessment.Name = "txtTotalAssessment";
			this.txtTotalAssessment.Style = "text-align: right";
			this.txtTotalAssessment.Tag = "textbox";
			this.txtTotalAssessment.Text = null;
			this.txtTotalAssessment.Top = 0F;
			this.txtTotalAssessment.Width = 1F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.15625F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 5.375F;
			this.Label28.MultiLine = false;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-weight: bold; text-align: left";
			this.Label28.Tag = "bold";
			this.Label28.Text = "Assessment";
			this.Label28.Top = 0F;
			this.Label28.Width = 0.875F;
			// 
			// txtTotalExempt
			// 
			this.txtTotalExempt.Height = 0.15625F;
			this.txtTotalExempt.Left = 4.3125F;
			this.txtTotalExempt.MultiLine = false;
			this.txtTotalExempt.Name = "txtTotalExempt";
			this.txtTotalExempt.Style = "text-align: right";
			this.txtTotalExempt.Tag = "textbox";
			this.txtTotalExempt.Text = null;
			this.txtTotalExempt.Top = 0F;
			this.txtTotalExempt.Width = 1F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.15625F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 3.6875F;
			this.Label29.MultiLine = false;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-weight: bold; text-align: left";
			this.Label29.Tag = "bold";
			this.Label29.Text = "Exempt";
			this.Label29.Top = 0F;
			this.Label29.Width = 0.5625F;
			// 
			// txtTotalPPTotal
			// 
			this.txtTotalPPTotal.Height = 0.15625F;
			this.txtTotalPPTotal.Left = 4.3125F;
			this.txtTotalPPTotal.MultiLine = false;
			this.txtTotalPPTotal.Name = "txtTotalPPTotal";
			this.txtTotalPPTotal.Style = "text-align: right";
			this.txtTotalPPTotal.Tag = "textbox";
			this.txtTotalPPTotal.Text = null;
			this.txtTotalPPTotal.Top = 0.15625F;
			this.txtTotalPPTotal.Visible = false;
			this.txtTotalPPTotal.Width = 1F;
			// 
			// lblPPTotal
			// 
			this.lblPPTotal.Height = 0.15625F;
			this.lblPPTotal.HyperLink = null;
			this.lblPPTotal.Left = 3.6875F;
			this.lblPPTotal.MultiLine = false;
			this.lblPPTotal.Name = "lblPPTotal";
			this.lblPPTotal.Style = "font-weight: bold; text-align: left";
			this.lblPPTotal.Tag = "bold";
			this.lblPPTotal.Text = "PP";
			this.lblPPTotal.Top = 0.15625F;
			this.lblPPTotal.Visible = false;
			this.lblPPTotal.Width = 0.5F;
			// 
			// rptRENormalOutprint
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaidToDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Address1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblcat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblcat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblcat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblcat49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExemptPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPPTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPPTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPaidToDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Address1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblcat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblcat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblcat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblcat49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptPP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessPP;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalPP;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExemptPP;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessPP;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPPTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPPTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
