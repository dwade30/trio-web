﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormatD.
	/// </summary>
	partial class rptBillFormatD
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillFormatD));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCatOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePaidLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmessage7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInterestRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmessage7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterestRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.lblTitle,
				this.txtRate,
				this.txtCatOther,
				this.txtLocation,
				this.txtLand,
				this.txtBldg,
				this.txtValue,
				this.txtExemption,
				this.txtAssessment,
				this.txtTaxDue,
				this.txtMessage1,
				this.txtMessage4,
				this.txtMessage2,
				this.txtMessage3,
				this.txtPrePaid,
				this.txtAcct1,
				this.txtMailing1,
				this.txtMailing2,
				this.txtMailing3,
				this.txtMailing4,
				this.txtMapLot,
				this.txtSubType,
				this.txtPrePaidLabel,
				this.txtDueDate1,
				this.txtAmount1,
				this.txtDueDate2,
				this.txtAmount2,
				this.txtAcct2,
				this.txtDueDate3,
				this.txtAmount3,
				this.txtAcct3,
				this.txtAmount4,
				this.txtAcct4,
				this.txtDueDate4,
				this.txtMessage5,
				this.txtTotalDue,
				this.txtMessage6,
				this.txtmessage7,
				this.txtInterestRate,
				this.txtMailing5
			});
			this.Detail.Height = 5.083333F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 1.625F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 3.03125F;
			this.txtAccount.Width = 1.875F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.19F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 0.0625F;
			this.lblTitle.MultiLine = false;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "text-align: center";
			this.lblTitle.Tag = "textbox";
			this.lblTitle.Text = null;
			this.lblTitle.Top = 0.84375F;
			this.lblTitle.Width = 0.5625F;
			// 
			// txtRate
			// 
			this.txtRate.CanGrow = false;
			this.txtRate.Height = 0.19F;
			this.txtRate.Left = 3.625F;
			this.txtRate.MultiLine = false;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "text-align: right";
			this.txtRate.Tag = "textbox";
			this.txtRate.Text = null;
			this.txtRate.Top = 0.84375F;
			this.txtRate.Width = 0.9375F;
			// 
			// txtCatOther
			// 
			this.txtCatOther.CanGrow = false;
			this.txtCatOther.Height = 0.19F;
			this.txtCatOther.Left = 3.625F;
			this.txtCatOther.MultiLine = false;
			this.txtCatOther.Name = "txtCatOther";
			this.txtCatOther.Style = "text-align: right";
			this.txtCatOther.Tag = "textbox";
			this.txtCatOther.Text = null;
			this.txtCatOther.Top = 1.46875F;
			this.txtCatOther.Width = 1F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 0F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1.625F;
			this.txtLocation.Width = 2.375F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 0F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 2.09375F;
			this.txtLand.Width = 1F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 1.0625F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 2.09375F;
			this.txtBldg.Width = 1.0625F;
			// 
			// txtValue
			// 
			this.txtValue.CanGrow = false;
			this.txtValue.Height = 0.19F;
			this.txtValue.Left = 2.25F;
			this.txtValue.MultiLine = false;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "text-align: right";
			this.txtValue.Tag = "textbox";
			this.txtValue.Text = null;
			this.txtValue.Top = 2.09375F;
			this.txtValue.Width = 1.25F;
			// 
			// txtExemption
			// 
			this.txtExemption.CanGrow = false;
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 2.5625F;
			this.txtExemption.MultiLine = false;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Tag = "textbox";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 2.25F;
			this.txtExemption.Width = 1F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.CanGrow = false;
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 2.625F;
			this.txtAssessment.MultiLine = false;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Tag = "textbox";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 2.40625F;
			this.txtAssessment.Width = 1F;
			// 
			// txtTaxDue
			// 
			this.txtTaxDue.CanGrow = false;
			this.txtTaxDue.Height = 0.19F;
			this.txtTaxDue.Left = 3.6875F;
			this.txtTaxDue.MultiLine = false;
			this.txtTaxDue.Name = "txtTaxDue";
			this.txtTaxDue.Style = "text-align: right";
			this.txtTaxDue.Tag = "textbox";
			this.txtTaxDue.Text = null;
			this.txtTaxDue.Top = 2.40625F;
			this.txtTaxDue.Width = 1F;
			// 
			// txtMessage1
			// 
			this.txtMessage1.CanGrow = false;
			this.txtMessage1.Height = 0.19F;
			this.txtMessage1.Left = 0F;
			this.txtMessage1.MultiLine = false;
			this.txtMessage1.Name = "txtMessage1";
			this.txtMessage1.Tag = "textbox";
			this.txtMessage1.Text = " WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
			this.txtMessage1.Top = 1.3125F;
			this.txtMessage1.Width = 2.375F;
			// 
			// txtMessage4
			// 
			this.txtMessage4.CanGrow = false;
			this.txtMessage4.Height = 0.19F;
			this.txtMessage4.Left = 0.125F;
			this.txtMessage4.MultiLine = false;
			this.txtMessage4.Name = "txtMessage4";
			this.txtMessage4.Tag = "textbox";
			this.txtMessage4.Text = " ";
			this.txtMessage4.Top = 4.28125F;
			this.txtMessage4.Width = 4.3125F;
			// 
			// txtMessage2
			// 
			this.txtMessage2.CanGrow = false;
			this.txtMessage2.Height = 0.19F;
			this.txtMessage2.Left = 0F;
			this.txtMessage2.MultiLine = false;
			this.txtMessage2.Name = "txtMessage2";
			this.txtMessage2.Tag = "textbox";
			this.txtMessage2.Text = " ";
			this.txtMessage2.Top = 1.46875F;
			this.txtMessage2.Width = 2.375F;
			// 
			// txtMessage3
			// 
			this.txtMessage3.CanGrow = false;
			this.txtMessage3.Height = 0.19F;
			this.txtMessage3.Left = 0.125F;
			this.txtMessage3.MultiLine = false;
			this.txtMessage3.Name = "txtMessage3";
			this.txtMessage3.Tag = "textbox";
			this.txtMessage3.Text = " ";
			this.txtMessage3.Top = 4.125F;
			this.txtMessage3.Width = 4.3125F;
			// 
			// txtPrePaid
			// 
			this.txtPrePaid.CanGrow = false;
			this.txtPrePaid.Height = 0.19F;
			this.txtPrePaid.Left = 0.6875F;
			this.txtPrePaid.MultiLine = false;
			this.txtPrePaid.Name = "txtPrePaid";
			this.txtPrePaid.Tag = "textbox";
			this.txtPrePaid.Text = null;
			this.txtPrePaid.Top = 2.40625F;
			this.txtPrePaid.Width = 1.25F;
			// 
			// txtAcct1
			// 
			this.txtAcct1.CanGrow = false;
			this.txtAcct1.Height = 0.19F;
			this.txtAcct1.Left = 7.625F;
			this.txtAcct1.MultiLine = false;
			this.txtAcct1.Name = "txtAcct1";
			this.txtAcct1.Tag = "textbox";
			this.txtAcct1.Text = null;
			this.txtAcct1.Top = 1F;
			this.txtAcct1.Width = 1.1875F;
			// 
			// txtMailing1
			// 
			this.txtMailing1.CanGrow = false;
			this.txtMailing1.Height = 0.19F;
			this.txtMailing1.Left = 1.625F;
			this.txtMailing1.MultiLine = false;
			this.txtMailing1.Name = "txtMailing1";
			this.txtMailing1.Tag = "textbox";
			this.txtMailing1.Text = null;
			this.txtMailing1.Top = 3.1875F;
			this.txtMailing1.Width = 2.8125F;
			// 
			// txtMailing2
			// 
			this.txtMailing2.CanGrow = false;
			this.txtMailing2.Height = 0.19F;
			this.txtMailing2.Left = 1.625F;
			this.txtMailing2.MultiLine = false;
			this.txtMailing2.Name = "txtMailing2";
			this.txtMailing2.Tag = "textbox";
			this.txtMailing2.Text = null;
			this.txtMailing2.Top = 3.34375F;
			this.txtMailing2.Width = 2.8125F;
			// 
			// txtMailing3
			// 
			this.txtMailing3.CanGrow = false;
			this.txtMailing3.Height = 0.19F;
			this.txtMailing3.Left = 1.625F;
			this.txtMailing3.MultiLine = false;
			this.txtMailing3.Name = "txtMailing3";
			this.txtMailing3.Tag = "textbox";
			this.txtMailing3.Text = null;
			this.txtMailing3.Top = 3.5F;
			this.txtMailing3.Width = 2.8125F;
			// 
			// txtMailing4
			// 
			this.txtMailing4.CanGrow = false;
			this.txtMailing4.Height = 0.19F;
			this.txtMailing4.Left = 1.625F;
			this.txtMailing4.MultiLine = false;
			this.txtMailing4.Name = "txtMailing4";
			this.txtMailing4.Tag = "textbox";
			this.txtMailing4.Text = null;
			this.txtMailing4.Top = 3.65625F;
			this.txtMailing4.Width = 2.8125F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 2.5F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.46875F;
			this.txtMapLot.Width = 1F;
			// 
			// txtSubType
			// 
			this.txtSubType.CanGrow = false;
			this.txtSubType.Height = 0.19F;
			this.txtSubType.Left = 2.5F;
			this.txtSubType.MultiLine = false;
			this.txtSubType.Name = "txtSubType";
			this.txtSubType.Tag = "textbox";
			this.txtSubType.Text = null;
			this.txtSubType.Top = 1.78125F;
			this.txtSubType.Width = 1F;
			// 
			// txtPrePaidLabel
			// 
			this.txtPrePaidLabel.CanGrow = false;
			this.txtPrePaidLabel.Height = 0.19F;
			this.txtPrePaidLabel.Left = 0.0625F;
			this.txtPrePaidLabel.MultiLine = false;
			this.txtPrePaidLabel.Name = "txtPrePaidLabel";
			this.txtPrePaidLabel.Tag = "textbox";
			this.txtPrePaidLabel.Text = "Paid";
			this.txtPrePaidLabel.Top = 2.40625F;
			this.txtPrePaidLabel.Width = 0.5625F;
			// 
			// txtDueDate1
			// 
			this.txtDueDate1.CanGrow = false;
			this.txtDueDate1.Height = 0.19F;
			this.txtDueDate1.Left = 7.625F;
			this.txtDueDate1.MultiLine = false;
			this.txtDueDate1.Name = "txtDueDate1";
			this.txtDueDate1.Tag = "textbox";
			this.txtDueDate1.Text = "dfadsf";
			this.txtDueDate1.Top = 0.53125F;
			this.txtDueDate1.Width = 1.1875F;
			// 
			// txtAmount1
			// 
			this.txtAmount1.CanGrow = false;
			this.txtAmount1.Height = 0.19F;
			this.txtAmount1.Left = 7.625F;
			this.txtAmount1.MultiLine = false;
			this.txtAmount1.Name = "txtAmount1";
			this.txtAmount1.Tag = "textbox";
			this.txtAmount1.Text = "adsfadf";
			this.txtAmount1.Top = 0.6875F;
			this.txtAmount1.Width = 1.1875F;
			// 
			// txtDueDate2
			// 
			this.txtDueDate2.CanGrow = false;
			this.txtDueDate2.Height = 0.19F;
			this.txtDueDate2.Left = 7.625F;
			this.txtDueDate2.MultiLine = false;
			this.txtDueDate2.Name = "txtDueDate2";
			this.txtDueDate2.Tag = "textbox";
			this.txtDueDate2.Text = "dfadsf";
			this.txtDueDate2.Top = 3.1875F;
			this.txtDueDate2.Visible = false;
			this.txtDueDate2.Width = 1.25F;
			// 
			// txtAmount2
			// 
			this.txtAmount2.CanGrow = false;
			this.txtAmount2.Height = 0.19F;
			this.txtAmount2.Left = 7.625F;
			this.txtAmount2.MultiLine = false;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Tag = "textbox";
			this.txtAmount2.Text = "adsfadf";
			this.txtAmount2.Top = 3.34375F;
			this.txtAmount2.Visible = false;
			this.txtAmount2.Width = 1.3125F;
			// 
			// txtAcct2
			// 
			this.txtAcct2.CanGrow = false;
			this.txtAcct2.Height = 0.19F;
			this.txtAcct2.Left = 7.625F;
			this.txtAcct2.MultiLine = false;
			this.txtAcct2.Name = "txtAcct2";
			this.txtAcct2.Tag = "textbox";
			this.txtAcct2.Text = "adsfasdfsad";
			this.txtAcct2.Top = 3.8125F;
			this.txtAcct2.Visible = false;
			this.txtAcct2.Width = 1.3125F;
			// 
			// txtDueDate3
			// 
			this.txtDueDate3.CanGrow = false;
			this.txtDueDate3.Height = 0.19F;
			this.txtDueDate3.Left = 5.375F;
			this.txtDueDate3.MultiLine = false;
			this.txtDueDate3.Name = "txtDueDate3";
			this.txtDueDate3.Tag = "textbox";
			this.txtDueDate3.Text = "dfadsf";
			this.txtDueDate3.Top = 0.53125F;
			this.txtDueDate3.Visible = false;
			this.txtDueDate3.Width = 1.375F;
			// 
			// txtAmount3
			// 
			this.txtAmount3.CanGrow = false;
			this.txtAmount3.Height = 0.19F;
			this.txtAmount3.Left = 5.375F;
			this.txtAmount3.MultiLine = false;
			this.txtAmount3.Name = "txtAmount3";
			this.txtAmount3.Tag = "textbox";
			this.txtAmount3.Text = "adsfadf";
			this.txtAmount3.Top = 0.6875F;
			this.txtAmount3.Visible = false;
			this.txtAmount3.Width = 1.375F;
			// 
			// txtAcct3
			// 
			this.txtAcct3.CanGrow = false;
			this.txtAcct3.Height = 0.19F;
			this.txtAcct3.Left = 5.375F;
			this.txtAcct3.MultiLine = false;
			this.txtAcct3.Name = "txtAcct3";
			this.txtAcct3.Tag = "textbox";
			this.txtAcct3.Text = "adsfasdfsad";
			this.txtAcct3.Top = 1F;
			this.txtAcct3.Visible = false;
			this.txtAcct3.Width = 1.4375F;
			// 
			// txtAmount4
			// 
			this.txtAmount4.CanGrow = false;
			this.txtAmount4.Height = 0.19F;
			this.txtAmount4.Left = 5.375F;
			this.txtAmount4.MultiLine = false;
			this.txtAmount4.Name = "txtAmount4";
			this.txtAmount4.Tag = "textbox";
			this.txtAmount4.Text = "adsfadf";
			this.txtAmount4.Top = 3.34375F;
			this.txtAmount4.Visible = false;
			this.txtAmount4.Width = 1.375F;
			// 
			// txtAcct4
			// 
			this.txtAcct4.CanGrow = false;
			this.txtAcct4.Height = 0.19F;
			this.txtAcct4.Left = 5.375F;
			this.txtAcct4.MultiLine = false;
			this.txtAcct4.Name = "txtAcct4";
			this.txtAcct4.Tag = "textbox";
			this.txtAcct4.Text = "adsfasdfsad";
			this.txtAcct4.Top = 3.8125F;
			this.txtAcct4.Visible = false;
			this.txtAcct4.Width = 1.4375F;
			// 
			// txtDueDate4
			// 
			this.txtDueDate4.CanGrow = false;
			this.txtDueDate4.Height = 0.19F;
			this.txtDueDate4.Left = 5.375F;
			this.txtDueDate4.MultiLine = false;
			this.txtDueDate4.Name = "txtDueDate4";
			this.txtDueDate4.Tag = "textbox";
			this.txtDueDate4.Text = "dfadsf";
			this.txtDueDate4.Top = 3.1875F;
			this.txtDueDate4.Visible = false;
			this.txtDueDate4.Width = 1.3125F;
			// 
			// txtMessage5
			// 
			this.txtMessage5.CanGrow = false;
			this.txtMessage5.Height = 0.19F;
			this.txtMessage5.Left = 0.125F;
			this.txtMessage5.MultiLine = false;
			this.txtMessage5.Name = "txtMessage5";
			this.txtMessage5.Tag = "textbox";
			this.txtMessage5.Text = null;
			this.txtMessage5.Top = 4.4375F;
			this.txtMessage5.Width = 4.3125F;
			// 
			// txtTotalDue
			// 
			this.txtTotalDue.CanGrow = false;
			this.txtTotalDue.Height = 0.19F;
			this.txtTotalDue.Left = 3.1875F;
			this.txtTotalDue.MultiLine = false;
			this.txtTotalDue.Name = "txtTotalDue";
			this.txtTotalDue.Style = "text-align: right";
			this.txtTotalDue.Tag = "textbox";
			this.txtTotalDue.Text = null;
			this.txtTotalDue.Top = 2.71875F;
			this.txtTotalDue.Width = 1.4375F;
			// 
			// txtMessage6
			// 
			this.txtMessage6.CanGrow = false;
			this.txtMessage6.Height = 0.19F;
			this.txtMessage6.Left = 0.125F;
			this.txtMessage6.MultiLine = false;
			this.txtMessage6.Name = "txtMessage6";
			this.txtMessage6.Tag = "textbox";
			this.txtMessage6.Text = null;
			this.txtMessage6.Top = 4.59375F;
			this.txtMessage6.Width = 4.3125F;
			// 
			// txtmessage7
			// 
			this.txtmessage7.CanGrow = false;
			this.txtmessage7.Height = 0.19F;
			this.txtmessage7.Left = 0.125F;
			this.txtmessage7.MultiLine = false;
			this.txtmessage7.Name = "txtmessage7";
			this.txtmessage7.Tag = "textbox";
			this.txtmessage7.Text = null;
			this.txtmessage7.Top = 4.75F;
			this.txtmessage7.Width = 4.3125F;
			// 
			// txtInterestRate
			// 
			this.txtInterestRate.CanGrow = false;
			this.txtInterestRate.Height = 0.19F;
			this.txtInterestRate.Left = 0.875F;
			this.txtInterestRate.MultiLine = false;
			this.txtInterestRate.Name = "txtInterestRate";
			this.txtInterestRate.Style = "text-align: right";
			this.txtInterestRate.Tag = "textbox";
			this.txtInterestRate.Text = null;
			this.txtInterestRate.Top = 0.84375F;
			this.txtInterestRate.Width = 0.9375F;
			// 
			// txtMailing5
			// 
			this.txtMailing5.CanGrow = false;
			this.txtMailing5.Height = 0.19F;
			this.txtMailing5.Left = 1.625F;
			this.txtMailing5.MultiLine = false;
			this.txtMailing5.Name = "txtMailing5";
			this.txtMailing5.Tag = "textbox";
			this.txtMailing5.Text = null;
			this.txtMailing5.Top = 3.8125F;
			this.txtMailing5.Width = 2.8125F;
			// 
			// rptBillFormatD
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePaidLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmessage7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterestRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePaidLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmessage7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInterestRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing5;
	}
}
