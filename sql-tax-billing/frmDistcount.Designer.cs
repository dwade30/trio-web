﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmDiscount.
	/// </summary>
	partial class frmDiscount : BaseForm
	{
		public T2KDateBox txtDate;
		public fecherFoundation.FCTextBox txtPercent;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSP;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDiscount));
            this.txtDate = new T2KDateBox();
			this.txtPercent = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveAndExit = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAndExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 209);
			this.BottomPanel.Size = new System.Drawing.Size(393, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.txtPercent);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(393, 149);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(393, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// txtDate
			// 
			this.txtDate.AutoSize = false;
			this.txtDate.BackColor = System.Drawing.SystemColors.Window;
			this.txtDate.Location = new System.Drawing.Point(241, 90);
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(124, 40);
			this.txtDate.TabIndex = 3;
			this.txtDate.Text = "00/00/0000";
			//this.txtDate.Enter += new System.EventHandler(this.txtDate_Enter);
			//this.txtDate.TextChanged += new System.EventHandler(this.txtDate_TextChanged);
			// 
			// txtPercent
			// 
			this.txtPercent.AutoSize = false;
			this.txtPercent.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent.LinkItem = null;
			this.txtPercent.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent.LinkTopic = null;
			this.txtPercent.Location = new System.Drawing.Point(30, 30);
			this.txtPercent.Name = "txtPercent";
			this.txtPercent.Size = new System.Drawing.Size(65, 40);
			this.txtPercent.TabIndex = 0;
			this.txtPercent.Text = "0.00";
			this.txtPercent.Validating += new System.ComponentModel.CancelEventHandler(this.txtPercent_Validating);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(162, 20);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "AMOUNT DUE IN FULL BY";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(108, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(253, 21);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "% DISCOUNT AVAILABLE. TO OBTAIN, PAY";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSave,
				this.mnuSP,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 0;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSave.Text = "Save & Exit";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSP
			// 
			this.mnuSP.Index = 1;
			this.mnuSP.Name = "mnuSP";
			this.mnuSP.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveAndExit
			// 
			this.cmdSaveAndExit.AppearanceKey = "acceptButton";
			this.cmdSaveAndExit.Location = new System.Drawing.Point(151, 30);
			this.cmdSaveAndExit.Name = "cmdSaveAndExit";
			this.cmdSaveAndExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAndExit.Size = new System.Drawing.Size(94, 48);
			this.cmdSaveAndExit.TabIndex = 0;
			this.cmdSaveAndExit.Text = "Save";
			this.cmdSaveAndExit.Click += new System.EventHandler(this.cmdSaveAndExit_Click);
			// 
			// frmDiscount
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(393, 317);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmDiscount";
			this.Text = "Discount";
			this.Load += new System.EventHandler(this.frmDiscount_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDiscount_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndExit)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndExit;
	}
}
