﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmRateforAuditXfer.
	/// </summary>
	public partial class frmRateforAuditXfer : BaseForm
	{
		public frmRateforAuditXfer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRateforAuditXfer InstancePtr
		{
			get
			{
				return (frmRateforAuditXfer)Sys.GetInstance(typeof(frmRateforAuditXfer));
			}
		}

		protected frmRateforAuditXfer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intTaxYear;
		// vbPorter upgrade warning: lngRateKey As int	OnWrite(short, string)
		int lngRateKey;
		int intFromWhere;
		bool boolNotLoading;

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolNotLoading)
			{
				if (Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString()) > 0)
				{
					FillGrid_2(FCConvert.ToInt32(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
				}
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				modGlobalVariables.Statics.CancelledIt = true;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			Close();
		}

		private void frmRateforAuditXfer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				Close();
				return;
			}
		}

		private void frmRateforAuditXfer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRateforAuditXfer properties;
			//frmRateforAuditXfer.ScaleWidth	= 8880;
			//frmRateforAuditXfer.ScaleHeight	= 7410;
			//frmRateforAuditXfer.LinkTopic	= "Form1";
			//frmRateforAuditXfer.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
		}

		private void frmRateforAuditXfer_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// MDIParent.Show
		}

		private void Grid_ClickEvent(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow > 0 && Grid.MouseRow < Grid.Rows)
			{
				Grid.Row = Grid.MouseRow;
			}
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow > 0 && Grid.MouseRow < Grid.Rows)
			{
				mnuShow_Click();
			}
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.Cols = 9;
			Grid.ColHidden(8, true);
			Grid.ExtendLastCol = true;
			Grid.TextMatrix(0, 0, "Tax Year");
			Grid.TextMatrix(0, 1, "Tax Rate");
			Grid.TextMatrix(0, 2, "Description");
			Grid.TextMatrix(0, 3, "Periods");
			Grid.TextMatrix(0, 4, "Type");
			Grid.TextMatrix(0, 5, "Bill Date");
			// regular or supplemental
			Grid.TextMatrix(0, 6, "Commit Date");
			Grid.TextMatrix(0, 7, "Int. Rate");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.1));
			Grid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.1));
			//FC:FINAL:MSH - issue #1272: increase size of description column
			//Grid.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.3));
			Grid.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.33));
			Grid.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.08));
			Grid.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.06));
			Grid.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.1));
			//Grid.ColWidth(6, FCConvert.ToInt32(GridWidth * 0.13));
			Grid.ColWidth(6, FCConvert.ToInt32(GridWidth * 0.1));
			Grid.ColWidth(7, FCConvert.ToInt32(GridWidth * 0.07));
            //FC:FINAL:BSE #1879 align header to match column alignmnet 
            Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}
		// vbPorter upgrade warning: intWhereFrom As short	OnWriteFCConvert.ToInt32(
		public void Init(int intWhereFrom)
		{
			clsDRWrapper clsrate = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strModule = "";
			int RateCount;
			int CurrentRateKey;
			string strCommit = "";
			int intYear;
			intFromWhere = intWhereFrom;
			clsTemp.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
			if (clsTemp.EndOfFile())
			{
				clsTemp.AddNew();
			}
			intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("defaultyear"))));
			//FC:FINAL:MSH - i.issue #1443: SetupGrid moved here to avoid removing data from table after initializing(Form_Load will be called on calling FillGrid in original)
			SetupGrid();
			FillGrid(intYear);
			if (intWhereFrom != 5)
			{
				// regular
				clsrate.OpenRecordset("select year from raterec  where ratetype = 'R'  group by year order by year desc ", "twcl0000.vb1");
			}
			else
			{
				// supplemental
				clsrate.OpenRecordset("select year from raterec where ratetype = 'S' group by year order by year desc", "twcl0000.vb1");
			}
			boolNotLoading = true;
			if (!clsrate.EndOfFile())
			{
				while (!clsrate.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
					cmbYear.AddItem(FCConvert.ToString(clsrate.Get_Fields("year")));
					// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(clsrate.Get_Fields("year")) == intYear)
						cmbYear.SelectedIndex = cmbYear.NewIndex;
					clsrate.MoveNext();
				}
			}
			else
			{
				cmbYear.AddItem(DateTime.Today.Year.ToString());
				cmbYear.SelectedIndex = 0;
			}
			boolNotLoading = false;
			// 
			// Grid.AddItem (vbTab & vbTab & "Add New")
			this.Show(FormShowEnum.Modal);
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			frmAuditInfo.InstancePtr.lngRKey = 0;
			frmAuditInfo.InstancePtr.t2kDueDate[0].Text = "";
			frmAuditInfo.InstancePtr.t2kDueDate[1].Text = "";
			frmAuditInfo.InstancePtr.t2kDueDate[2].Text = "";
			frmAuditInfo.InstancePtr.t2kDueDate[3].Text = "";
			frmAuditInfo.InstancePtr.t2kInterestDate[0].Text = "";
			frmAuditInfo.InstancePtr.t2kInterestDate[1].Text = "";
			frmAuditInfo.InstancePtr.t2kInterestDate[2].Text = "";
			frmAuditInfo.InstancePtr.t2kInterestDate[3].Text = "";
			frmAuditInfo.InstancePtr.t2kBilldate.Text = "";
			frmAuditInfo.InstancePtr.t2kCommitment.Text = "";
			frmAuditInfo.InstancePtr.txtTaxRate.Text = "1.00";
			frmAuditInfo.InstancePtr.txtInterestRate.Text = "";
			frmAuditInfo.InstancePtr.txtDescription.Text = "";
			//FC:FINAL:MSH - i.issue #1443: Unload the form without reinitializing AuditInfo
			this.Unload();
			//frmAuditInfo.InstancePtr.Show(App.MainForm);
		}

		private void mnuShow_Click(object sender, System.EventArgs e)
		{
			int x;
			int NumFonts;
			string strFont = "";
			string strPrinterName = "";
			bool boolUseFont;
			int intCPI;
			if (Grid.Row < 1 || Grid.Row > (Grid.Rows - 1))
			{
				if (Grid.Rows == 2)
				{
					// only one to choose from
					Grid.Row = 1;
				}
				else
				{
					MessageBox.Show("You must select a Tax Rate Record before continuing.", "Select Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			lngRateKey = 0;
			// If Grid.Row = Grid.Rows - 1 Then
			// adding a new one
			// If Not ValidPermissions(Me, CNSTEDITTAXRATES, False) Then
			// MsgBox "Your permission setting to add a new tax rate is set to none or is missing.", vbExclamation
			// Exit Sub
			// End If
			// Else
			lngRateKey = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, 8));
			// End If
			frmAuditInfo.InstancePtr.lngRKey = lngRateKey;
			// Call frmAuditInfo.Init(intFromWhere, , lngRateKey)
			//FC:FINAL:MSH - i.issue #1443: Unload the form without reinitializing AuditInfo
			this.Unload();
			//frmAuditInfo.InstancePtr.Show(App.MainForm);
		}

		public void mnuShow_Click()
		{
			mnuShow_Click(mnuShow, new System.EventArgs());
		}
		// vbPorter upgrade warning: intYear As short	OnWrite(double, int)
		private void FillGrid_2(int intYear)
		{
			FillGrid(intYear);
		}

		private void FillGrid(int intYear)
		{
			clsDRWrapper clsrate = new clsDRWrapper();
			string strCommit = "";
			Grid.Rows = 1;
			// 
			if (intFromWhere != 5)
			{
				// regular
				clsrate.OpenRecordset("select * from raterec  where ratetype = 'R' and year = " + FCConvert.ToString(intYear) + " order by year desc,billingdate desc ,ID desc", "twcl0000.vb1");
			}
			else
			{
				// supplemental
				clsrate.OpenRecordset("select * from raterec where year = " + FCConvert.ToString(intYear) + " and ratetype = 'S' order by year desc,billingdate desc,ID desc", "twcl0000.vb1");
			}
			while (!clsrate.EndOfFile())
			{
				strCommit = Strings.Format(clsrate.Get_Fields_DateTime("commitmentdate"), "MM/dd/yyyy");
				if (strCommit != string.Empty)
				{
					if (FCConvert.ToDateTime(strCommit).Year == 1899)
					{
						strCommit = "";
					}
					else
					{
						strCommit = Strings.Format(clsrate.Get_Fields_DateTime("commitmentdate"), "MM/dd/yy");
					}
				}
				// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
				//FC:FINAL:CHN - issue #1275: Delete timestamp from date. 
				// Grid.AddItem(FCConvert.ToString(Conversion.Val(clsrate.Get_Fields("year")))) + "\t" + Strings.Format(Conversion.Val(clsrate.Get_Fields_Double("taxrate"))) * 1000, "##0.000") + "\t" + clsrate.Get_Fields_String("description") + "\t" + clsrate.Get_Fields_Int16("numberofperiods") + "\t" + clsrate.Get_Fields_String("ratetype") + "\t" + clsrate.Get_Fields_DateTime("billingdate") + "\t" + strCommit + "\t" + FCConvert.ToString(Conversion.Val(clsrate.Get_Fields_Double("interestrate"))) * 100) + "\t" + clsrate.Get_Fields_Int32("ID"));
				Grid.AddItem(FCConvert.ToString(Conversion.Val(clsrate.Get_Fields("year"))) + "\t" + Strings.Format(Conversion.Val(clsrate.Get_Fields_Double("taxrate")) * 1000, "##0.000") + "\t" + clsrate.Get_Fields_String("description") + "\t" + clsrate.Get_Fields_Int16("numberofperiods") + "\t" + clsrate.Get_Fields_String("ratetype") + "\t" + Strings.Format(clsrate.Get_Fields_DateTime("billingdate"), "MM/dd/yyyy") + "\t" + strCommit + "\t" + FCConvert.ToString(Conversion.Val(clsrate.Get_Fields_Double("interestrate")) * 100) + "\t" + clsrate.Get_Fields_Int32("ID"));
				clsrate.MoveNext();
			}
			// Grid.AddItem (vbTab & vbTab & "Add New")
		}
	}
}
