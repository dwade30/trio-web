﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPCollectionSimple.
	/// </summary>
	partial class rptPPCollectionSimple
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPPCollectionSimple));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPageTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPageTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtTotal,
				this.txtTax,
				this.txtAddress1,
				this.txtAddress2,
				this.txtMailingAddress3,
				this.txtAddress3,
				this.txtDiscount,
				this.lblDiscount,
				this.txtLocation
			});
			this.Detail.Height = 0.9895833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.Visible = false;
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label15,
				this.Label16,
				this.Label20,
				this.txtTotTotal,
				this.txtTotTax
			});
			this.ReportFooter.Height = 0.46875F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.txtDate,
				this.lblMuniname,
				this.lblTitle,
				this.Label9,
				this.Field11,
				this.Label13,
				this.Label14,
				this.txtTime
			});
			this.PageHeader.Height = 0.55F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.CanGrow = false;
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label19,
				this.txtPageTotal,
				this.txtPageTax
			});
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			this.PageFooter.Visible = false;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.125F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-size: 10pt; text-align: right";
			this.txtPage.Tag = "textbox";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 1.3125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-size: 10pt; text-align: right";
			this.txtDate.Tag = "textbox";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 0.8125F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.19F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "font-size: 8.5pt; text-align: left";
			this.lblMuniname.Tag = "textbox";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.4375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.lblTitle.Tag = "bold";
			this.lblTitle.Text = "Personal Property Collection List - ";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Account";
			this.Label9.Top = 0.34375F;
			this.Label9.Width = 0.6875F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 0.75F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-weight: bold";
			this.Field11.Tag = "bold";
			this.Field11.Text = "Name & Address";
			this.Field11.Top = 0.34375F;
			this.Field11.Width = 1.5625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.75F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Tag = "bold";
			this.Label13.Text = "Total";
			this.Label13.Top = 0.34375F;
			this.Label13.Width = 0.9375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 6.3125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Tag = "bold";
			this.Label14.Text = "Tax";
			this.Label14.Top = 0.34375F;
			this.Label14.Width = 1.125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-size: 10pt; text-align: left";
			this.txtTime.Tag = "textbox";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "white-space: nowrap";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = " ";
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.75F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Tag = "textbox";
			this.txtName.Text = " ";
			this.txtName.Top = 0F;
			this.txtName.Width = 3.375F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.19F;
			this.txtTotal.Left = 4.1875F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat");
			this.txtTotal.Style = "text-align: right; white-space: nowrap";
			this.txtTotal.Tag = "textbox";
			this.txtTotal.Text = " ";
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 1.5F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 5.875F;
			this.txtTax.MultiLine = false;
			this.txtTax.Name = "txtTax";
			this.txtTax.OutputFormat = resources.GetString("txtTax.OutputFormat");
			this.txtTax.Style = "text-align: right; white-space: nowrap";
			this.txtTax.Tag = "textbox";
			this.txtTax.Text = " ";
			this.txtTax.Top = 0F;
			this.txtTax.Width = 1.5625F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.CanGrow = false;
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 0.75F;
			this.txtAddress1.MultiLine = false;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = " ";
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 3.375F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.CanGrow = false;
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 0.75F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = " ";
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 3.375F;
			// 
			// txtMailingAddress3
			// 
			this.txtMailingAddress3.CanGrow = false;
			this.txtMailingAddress3.Height = 0.19F;
			this.txtMailingAddress3.Left = 0.75F;
			this.txtMailingAddress3.MultiLine = false;
			this.txtMailingAddress3.Name = "txtMailingAddress3";
			this.txtMailingAddress3.Tag = "textbox";
			this.txtMailingAddress3.Text = " ";
			this.txtMailingAddress3.Top = 0.46875F;
			this.txtMailingAddress3.Width = 3.375F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.CanGrow = false;
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.Left = 0.75F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = " ";
			this.txtAddress3.Top = 0.625F;
			this.txtAddress3.Width = 3.375F;
			// 
			// txtDiscount
			// 
			this.txtDiscount.DataField = "taxval";
			this.txtDiscount.Height = 0.19F;
			this.txtDiscount.Left = 6.3125F;
			this.txtDiscount.MultiLine = false;
			this.txtDiscount.Name = "txtDiscount";
			this.txtDiscount.OutputFormat = resources.GetString("txtDiscount.OutputFormat");
			this.txtDiscount.Style = "font-size: 10pt; text-align: right";
			this.txtDiscount.Tag = "textbox";
			this.txtDiscount.Text = null;
			this.txtDiscount.Top = 0.15625F;
			this.txtDiscount.Width = 1.125F;
			// 
			// lblDiscount
			// 
			this.lblDiscount.Height = 0.19F;
			this.lblDiscount.Left = 5.3125F;
			this.lblDiscount.MultiLine = false;
			this.lblDiscount.Name = "lblDiscount";
			this.lblDiscount.OutputFormat = resources.GetString("lblDiscount.OutputFormat");
			this.lblDiscount.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.lblDiscount.Tag = "bold";
			this.lblDiscount.Text = "Discount";
			this.lblDiscount.Top = 0.15625F;
			this.lblDiscount.Width = 0.9375F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 0.75F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = " ";
			this.txtLocation.Top = 0.7569444F;
			this.txtLocation.Width = 3.375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold";
			this.Label19.Tag = "bold";
			this.Label19.Text = "Page Totals:";
			this.Label19.Top = 0.15625F;
			this.Label19.Width = 0.9375F;
			// 
			// txtPageTotal
			// 
			this.txtPageTotal.DataField = "TotVal";
			this.txtPageTotal.Height = 0.19F;
			this.txtPageTotal.Left = 4.1875F;
			this.txtPageTotal.MultiLine = false;
			this.txtPageTotal.Name = "txtPageTotal";
			this.txtPageTotal.OutputFormat = resources.GetString("txtPageTotal.OutputFormat");
			this.txtPageTotal.Style = "text-align: right; white-space: nowrap";
			this.txtPageTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtPageTotal.Tag = "textbox";
			this.txtPageTotal.Text = null;
			this.txtPageTotal.Top = 0.15625F;
			this.txtPageTotal.Width = 1.5F;
			// 
			// txtPageTax
			// 
			this.txtPageTax.DataField = "TaxVal";
			this.txtPageTax.Height = 0.19F;
			this.txtPageTax.Left = 5.875F;
			this.txtPageTax.MultiLine = false;
			this.txtPageTax.Name = "txtPageTax";
			this.txtPageTax.OutputFormat = resources.GetString("txtPageTax.OutputFormat");
			this.txtPageTax.Style = "text-align: right; white-space: nowrap";
			this.txtPageTax.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtPageTax.Tag = "textbox";
			this.txtPageTax.Text = null;
			this.txtPageTax.Top = 0.15625F;
			this.txtPageTax.Width = 1.5625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.875F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Tag = "bold";
			this.Label15.Text = "Tax";
			this.Label15.Top = 0.15625F;
			this.Label15.Width = 1.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 4.1875F;
			this.Label16.MultiLine = false;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold; text-align: right";
			this.Label16.Tag = "bold";
			this.Label16.Text = "Total";
			this.Label16.Top = 0.15625F;
			this.Label16.Width = 1.5F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 3.125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold";
			this.Label20.Tag = "bold";
			this.Label20.Text = "Totals:";
			this.Label20.Top = 0.3125F;
			this.Label20.Width = 0.9375F;
			// 
			// txtTotTotal
			// 
			this.txtTotTotal.Height = 0.19F;
			this.txtTotTotal.Left = 4.1875F;
			this.txtTotTotal.MultiLine = false;
			this.txtTotTotal.Name = "txtTotTotal";
			this.txtTotTotal.OutputFormat = resources.GetString("txtTotTotal.OutputFormat");
			this.txtTotTotal.Style = "text-align: right; white-space: nowrap";
			this.txtTotTotal.Tag = "textbox";
			this.txtTotTotal.Text = null;
			this.txtTotTotal.Top = 0.3125F;
			this.txtTotTotal.Width = 1.5F;
			// 
			// txtTotTax
			// 
			this.txtTotTax.Height = 0.19F;
			this.txtTotTax.Left = 5.875F;
			this.txtTotTax.MultiLine = false;
			this.txtTotTax.Name = "txtTotTax";
			this.txtTotTax.OutputFormat = resources.GetString("txtTotTax.OutputFormat");
			this.txtTotTax.Style = "text-align: right; white-space: nowrap";
			this.txtTotTax.Tag = "textbox";
			this.txtTotTax.Text = null;
			this.txtTotTax.Top = 0.3125F;
			this.txtTotTax.Width = 1.5625F;
			// 
			// rptPPCollectionSimple
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailingAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTax;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageTax;
	}
}
