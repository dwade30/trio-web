﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using System.IO;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmFileList.
	/// </summary>
	public partial class frmFileList : BaseForm
	{
		public frmFileList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmFileList InstancePtr
		{
			get
			{
				return (frmFileList)Sys.GetInstance(typeof(frmFileList));
			}
		}

		protected frmFileList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		DriveInfo dr1;
		//FileSystemObject fso = new FileSystemObject();
		string OldPath = "";

		private void Dir1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			File1.Path = Dir1.Path;
		}

		private void Drive1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dr1 = new DriveInfo(Directory.GetDirectoryRoot(Strings.Left(Drive1.Drive, 1)));
			if (dr1.IsReady)
			{
				Dir1.Path = Drive1.Drive;
			}
			else
			{
				MessageBox.Show("Drive isn't ready.", "Drive", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmFileList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuContinue_Click();
			}
		}

		private void frmFileList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFileList properties;
			//frmFileList.ScaleWidth	= 9045;
			//frmFileList.ScaleHeight	= 7200;
			//frmFileList.LinkTopic	= "Form1";
			//End Unmaped Properties
			// OldPath = CurDir
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			lblMessage.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			lblDrive.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			lblFile.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			lblDirectory.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
		}

		private void mnuExit_Click()
		{
			Close();
		}
		// vbPorter upgrade warning: strTitle As Variant --> As string
		public void Init(object strDriveName = null, object strDirName = null, string strTitle = "", object strDrive = null, object strDir = null, object strFile = null)
		{
			if (!Information.IsNothing(strTitle))
			{
				lblMessage.Text = strTitle;
			}
			if (!Information.IsNothing(strDrive))
			{
				lblDrive.Text = FCConvert.ToString(strDrive);
			}
			if (!Information.IsNothing(strDir))
			{
				lblDirectory.Text = FCConvert.ToString(strDir);
			}
			if (!Information.IsNothing(strFile))
			{
				lblFile.Text = FCConvert.ToString(strFile);
			}
			if (!Information.IsNothing(strDriveName))
			{
				Drive1.Drive = FCConvert.ToString(strDriveName);
			}
			if (!Information.IsNothing(strDirName))
			{
				Dir1.Path = FCConvert.ToString(strDirName);
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// ChDir (OldPath)
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			dr1 = new DriveInfo(Directory.GetDirectoryRoot(Strings.Left(Drive1.Drive, 1)));
			if (!dr1.IsReady)
			{
				MessageBox.Show("Drive " + Drive1.Drive + " is not ready.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			else
			{
				//modGlobalVariables.Statics.gstrDriveName = Drive1.Drive;
				modGlobalVariables.Statics.gstrFolderName = Dir1.Path;
				//modGlobalVariables.Statics.gstrFileName = File1.FileName;
				Close();
			}
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, new System.EventArgs());
		}

		private void cmdContinue_Click(object sender, EventArgs e)
		{
			mnuContinue_Click(mnuContinue, EventArgs.Empty);
		}
	}
}
