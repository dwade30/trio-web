﻿using fecherFoundation;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormatI.
	/// </summary>
	partial class rptBillFormatI
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillFormatI));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDiscountDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMailing1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMailing2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMailing3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMailing4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBookPageLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMapLotLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLocLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDueDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDueDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSecondPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFirstPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDiscountMsg1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDiscountAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDiscountMsg2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemit2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemit1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.txtNA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTaxBillYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAccountAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBillYearStub1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBillYearStub2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shapeInformation = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtInfoLbl0 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtInfoLbl12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shapeDistribution = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shapeRemittance = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtDistLbl0 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistLbl7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistLbl1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistLbl2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistLBL3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistLbl4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistLbl5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistLbl6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistAm7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistAm1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistAm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistAm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistAm4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistAm5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistAm6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistPerc7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistPerc1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistPer2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistPerc3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistPerc4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistPerc5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDistPerc6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemitLbl0 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemitLbl1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemitLbl2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemitLbl3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemitLbl4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemitLbl5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemitLbl6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRemitLbl7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ValuationLine1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtValLbl1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVallbl0 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValLbl17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValData17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.valuationLine2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shapeValuation = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtMailing5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMailing6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStub11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStub12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStub13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStub21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStub22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStub23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo122 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo131 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo132 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo141 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo142 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo211 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo212 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo221 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo222 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo231 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo232 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo241 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStubInfo242 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLotLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecondPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountMsg1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountMsg2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxBillYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillYearStub1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillYearStub2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLBL3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVallbl0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo211)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo212)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo221)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo222)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo231)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo232)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo241)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo242)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line1,
            this.Line2,
            this.txtDiscountDate,
            this.txtDiscount,
            this.txtBookPage,
            this.txtAddress1,
            this.txtAddress2,
            this.txtAddress3,
            this.txtAddress4,
            this.txtMailing1,
            this.txtMailing2,
            this.txtMailing3,
            this.txtMailing4,
            this.txtBookPageLabel,
            this.txtMapLot,
            this.txtMapLotLabel,
            this.txtLocation,
            this.txtLocLabel,
            this.txtDueDate1,
            this.txtAmount1,
            this.txtDueDate2,
            this.txtAmount2,
            this.txtSecondPayment,
            this.txtFirstPayment,
            this.txtDiscountMsg1,
            this.txtDiscountAmount,
            this.txtDiscountMsg2,
            this.txtRemit2,
            this.txtRemit1,
            this.Image1,
            this.txtNA,
            this.txtTaxBillYear,
            this.lblAcres,
            this.txtAcres,
            this.txtAccountAddress,
            this.txtBillYearStub1,
            this.txtBillYearStub2,
            this.shapeInformation,
            this.txtInfoLbl0,
            this.txtInfoLbl1,
            this.txtInfoLbl2,
            this.txtInfoLbl3,
            this.txtInfoLbl4,
            this.txtInfoLbl5,
            this.txtInfoLbl6,
            this.txtInfoLbl7,
            this.txtInfoLbl8,
            this.txtInfoLbl9,
            this.txtInfoLbl10,
            this.txtInfoLbl11,
            this.txtInfoLbl12,
            this.shapeDistribution,
            this.shapeRemittance,
            this.txtDistLbl0,
            this.txtDistLbl7,
            this.txtDistLbl1,
            this.txtDistLbl2,
            this.txtDistLBL3,
            this.txtDistLbl4,
            this.txtDistLbl5,
            this.txtDistLbl6,
            this.txtDistAm7,
            this.txtDistAm1,
            this.txtDistAm2,
            this.txtDistAm3,
            this.txtDistAm4,
            this.txtDistAm5,
            this.txtDistAm6,
            this.txtDistPerc7,
            this.txtDistPerc1,
            this.txtDistPer2,
            this.txtDistPerc3,
            this.txtDistPerc4,
            this.txtDistPerc5,
            this.txtDistPerc6,
            this.txtRemitLbl0,
            this.txtRemitLbl1,
            this.txtRemitLbl2,
            this.txtRemitLbl3,
            this.txtRemitLbl4,
            this.txtRemitLbl5,
            this.txtRemitLbl6,
            this.txtRemitLbl7,
            this.ValuationLine1,
            this.txtValLbl1,
            this.txtValLbl2,
            this.txtValLbl3,
            this.txtValLbl4,
            this.txtValLbl5,
            this.txtValLbl6,
            this.txtValLbl7,
            this.txtValLbl8,
            this.txtValLbl9,
            this.txtValLbl10,
            this.txtValLbl11,
            this.txtValLbl12,
            this.txtValLbl13,
            this.txtVallbl0,
            this.txtValData1,
            this.txtValData2,
            this.txtValData3,
            this.txtValData4,
            this.txtValData5,
            this.txtValData6,
            this.txtValData7,
            this.txtValData8,
            this.txtValData9,
            this.txtValData10,
            this.txtValData11,
            this.txtValData12,
            this.txtValData13,
            this.txtValLbl14,
            this.txtValData14,
            this.txtValLbl15,
            this.txtValData15,
            this.txtValLbl16,
            this.txtValData16,
            this.txtValLbl17,
            this.txtValData17,
            this.valuationLine2,
            this.shapeValuation,
            this.txtMailing5,
            this.txtMailing6,
            this.txtStub11,
            this.txtStub12,
            this.txtStub13,
            this.txtStub21,
            this.txtStub22,
            this.txtStub23,
            this.txtStubInfo111,
            this.txtStubInfo112,
            this.txtStubInfo121,
            this.txtStubInfo122,
            this.txtStubInfo131,
            this.txtStubInfo132,
            this.txtStubInfo141,
            this.txtStubInfo142,
            this.txtStubInfo211,
            this.txtStubInfo212,
            this.txtStubInfo221,
            this.txtStubInfo222,
            this.txtStubInfo231,
            this.txtStubInfo232,
            this.txtStubInfo241,
            this.txtStubInfo242});
            this.Detail.Height = 10.46875F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.0625F;
            this.Line1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Tag = "Line";
            this.Line1.Top = 7.75F;
            this.Line1.Visible = false;
            this.Line1.Width = 7.5625F;
            this.Line1.X1 = 0.0625F;
            this.Line1.X2 = 7.625F;
            this.Line1.Y1 = 7.75F;
            this.Line1.Y2 = 7.75F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.0625F;
            this.Line2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Tag = "Line";
            this.Line2.Top = 9.25F;
            this.Line2.Visible = false;
            this.Line2.Width = 7.5625F;
            this.Line2.X1 = 0.0625F;
            this.Line2.X2 = 7.625F;
            this.Line2.Y1 = 9.25F;
            this.Line2.Y2 = 9.25F;
            // 
            // txtDiscountDate
            // 
            this.txtDiscountDate.Height = 0.19F;
            this.txtDiscountDate.Left = 5.9375F;
            this.txtDiscountDate.Name = "txtDiscountDate";
            this.txtDiscountDate.Text = null;
            this.txtDiscountDate.Top = 3.65625F;
            this.txtDiscountDate.Visible = false;
            this.txtDiscountDate.Width = 1.125F;
            // 
            // txtDiscount
            // 
            this.txtDiscount.Height = 0.19F;
            this.txtDiscount.Left = 0F;
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDiscount.Text = null;
            this.txtDiscount.Top = 3.65625F;
            this.txtDiscount.Visible = false;
            this.txtDiscount.Width = 0.625F;
            // 
            // txtBookPage
            // 
            this.txtBookPage.CanGrow = false;
            this.txtBookPage.Height = 0.19F;
            this.txtBookPage.Left = 3.125F;
            this.txtBookPage.Name = "txtBookPage";
            this.txtBookPage.Style = "font-family: \'Courier New\'";
            this.txtBookPage.Text = null;
            this.txtBookPage.Top = 3.34375F;
            this.txtBookPage.Width = 4.5F;
            // 
            // txtAddress1
            // 
            this.txtAddress1.CanGrow = false;
            this.txtAddress1.Height = 0.19F;
            this.txtAddress1.Left = 1.25F;
            this.txtAddress1.MultiLine = false;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Style = "font-family: \'Courier New\'";
            this.txtAddress1.Tag = "textbox";
            this.txtAddress1.Text = "Field1";
            this.txtAddress1.Top = 0.53125F;
            this.txtAddress1.Width = 3.125F;
            // 
            // txtAddress2
            // 
            this.txtAddress2.CanGrow = false;
            this.txtAddress2.Height = 0.19F;
            this.txtAddress2.Left = 1.25F;
            this.txtAddress2.MultiLine = false;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Style = "font-family: \'Courier New\'";
            this.txtAddress2.Tag = "textbox";
            this.txtAddress2.Text = "Field1";
            this.txtAddress2.Top = 0.6875F;
            this.txtAddress2.Width = 3.125F;
            // 
            // txtAddress3
            // 
            this.txtAddress3.CanGrow = false;
            this.txtAddress3.Height = 0.19F;
            this.txtAddress3.Left = 1.25F;
            this.txtAddress3.MultiLine = false;
            this.txtAddress3.Name = "txtAddress3";
            this.txtAddress3.Style = "font-family: \'Courier New\'";
            this.txtAddress3.Tag = "textbox";
            this.txtAddress3.Text = "Field1";
            this.txtAddress3.Top = 0.84375F;
            this.txtAddress3.Width = 3.125F;
            // 
            // txtAddress4
            // 
            this.txtAddress4.CanGrow = false;
            this.txtAddress4.Height = 0.19F;
            this.txtAddress4.Left = 1.25F;
            this.txtAddress4.MultiLine = false;
            this.txtAddress4.Name = "txtAddress4";
            this.txtAddress4.Style = "font-family: \'Courier New\'";
            this.txtAddress4.Tag = "textbox";
            this.txtAddress4.Text = "Field1";
            this.txtAddress4.Top = 1F;
            this.txtAddress4.Width = 3.125F;
            // 
            // txtMailing1
            // 
            this.txtMailing1.CanGrow = false;
            this.txtMailing1.Height = 0.19F;
            this.txtMailing1.Left = 0.5625F;
            this.txtMailing1.MultiLine = false;
            this.txtMailing1.Name = "txtMailing1";
            this.txtMailing1.Style = "font-family: \'Courier New\'";
            this.txtMailing1.Tag = "textbox";
            this.txtMailing1.Text = null;
            this.txtMailing1.Top = 2.09375F;
            this.txtMailing1.Width = 3.625F;
            // 
            // txtMailing2
            // 
            this.txtMailing2.CanGrow = false;
            this.txtMailing2.Height = 0.19F;
            this.txtMailing2.Left = 0.5625F;
            this.txtMailing2.MultiLine = false;
            this.txtMailing2.Name = "txtMailing2";
            this.txtMailing2.Style = "font-family: \'Courier New\'";
            this.txtMailing2.Tag = "textbox";
            this.txtMailing2.Text = null;
            this.txtMailing2.Top = 2.25F;
            this.txtMailing2.Width = 3.625F;
            // 
            // txtMailing3
            // 
            this.txtMailing3.CanGrow = false;
            this.txtMailing3.Height = 0.19F;
            this.txtMailing3.Left = 0.5625F;
            this.txtMailing3.MultiLine = false;
            this.txtMailing3.Name = "txtMailing3";
            this.txtMailing3.Style = "font-family: \'Courier New\'";
            this.txtMailing3.Tag = "textbox";
            this.txtMailing3.Text = null;
            this.txtMailing3.Top = 2.40625F;
            this.txtMailing3.Width = 3.625F;
            // 
            // txtMailing4
            // 
            this.txtMailing4.CanGrow = false;
            this.txtMailing4.Height = 0.19F;
            this.txtMailing4.Left = 0.5625F;
            this.txtMailing4.MultiLine = false;
            this.txtMailing4.Name = "txtMailing4";
            this.txtMailing4.Style = "font-family: \'Courier New\'";
            this.txtMailing4.Tag = "textbox";
            this.txtMailing4.Text = null;
            this.txtMailing4.Top = 2.5625F;
            this.txtMailing4.Width = 3.625F;
            // 
            // txtBookPageLabel
            // 
            this.txtBookPageLabel.CanGrow = false;
            this.txtBookPageLabel.Height = 0.19F;
            this.txtBookPageLabel.Left = 2.1875F;
            this.txtBookPageLabel.MultiLine = false;
            this.txtBookPageLabel.Name = "txtBookPageLabel";
            this.txtBookPageLabel.Style = "font-family: \'Courier New\'; font-weight: bold";
            this.txtBookPageLabel.Tag = "textbox";
            this.txtBookPageLabel.Text = "Book/Page";
            this.txtBookPageLabel.Top = 3.34375F;
            this.txtBookPageLabel.Width = 0.9375F;
            // 
            // txtMapLot
            // 
            this.txtMapLot.CanGrow = false;
            this.txtMapLot.Height = 0.19F;
            this.txtMapLot.Left = 0.6875F;
            this.txtMapLot.MultiLine = false;
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Style = "font-family: \'Courier New\'";
            this.txtMapLot.Tag = "textbox";
            this.txtMapLot.Text = null;
            this.txtMapLot.Top = 3.34375F;
            this.txtMapLot.Width = 1.5F;
            // 
            // txtMapLotLabel
            // 
            this.txtMapLotLabel.CanGrow = false;
            this.txtMapLotLabel.Height = 0.19F;
            this.txtMapLotLabel.Left = 0F;
            this.txtMapLotLabel.MultiLine = false;
            this.txtMapLotLabel.Name = "txtMapLotLabel";
            this.txtMapLotLabel.Style = "font-family: \'Courier New\'; font-weight: bold";
            this.txtMapLotLabel.Tag = "textbox";
            this.txtMapLotLabel.Text = "Map/Lot";
            this.txtMapLotLabel.Top = 3.34375F;
            this.txtMapLotLabel.Visible = false;
            this.txtMapLotLabel.Width = 0.6875F;
            // 
            // txtLocation
            // 
            this.txtLocation.CanGrow = false;
            this.txtLocation.Height = 0.19F;
            this.txtLocation.Left = 0.8125F;
            this.txtLocation.MultiLine = false;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Tag = "textbox";
            this.txtLocation.Text = null;
            this.txtLocation.Top = 3.5F;
            this.txtLocation.Width = 2.3125F;
            // 
            // txtLocLabel
            // 
            this.txtLocLabel.CanGrow = false;
            this.txtLocLabel.Height = 0.19F;
            this.txtLocLabel.Left = 0F;
            this.txtLocLabel.MultiLine = false;
            this.txtLocLabel.Name = "txtLocLabel";
            this.txtLocLabel.Style = "font-weight: bold";
            this.txtLocLabel.Tag = "textbox";
            this.txtLocLabel.Text = "Location";
            this.txtLocLabel.Top = 3.5F;
            this.txtLocLabel.Visible = false;
            this.txtLocLabel.Width = 0.8125F;
            // 
            // txtDueDate1
            // 
            this.txtDueDate1.CanGrow = false;
            this.txtDueDate1.Height = 0.19F;
            this.txtDueDate1.Left = 4.3125F;
            this.txtDueDate1.MultiLine = false;
            this.txtDueDate1.Name = "txtDueDate1";
            this.txtDueDate1.Style = "text-align: center";
            this.txtDueDate1.Tag = "textbox";
            this.txtDueDate1.Text = null;
            this.txtDueDate1.Top = 9.5625F;
            this.txtDueDate1.Width = 0.9375F;
            // 
            // txtAmount1
            // 
            this.txtAmount1.CanGrow = false;
            this.txtAmount1.Height = 0.19F;
            this.txtAmount1.Left = 5.3125F;
            this.txtAmount1.MultiLine = false;
            this.txtAmount1.Name = "txtAmount1";
            this.txtAmount1.Style = "text-align: center";
            this.txtAmount1.Tag = "textbox";
            this.txtAmount1.Text = null;
            this.txtAmount1.Top = 9.5625F;
            this.txtAmount1.Width = 1.0625F;
            // 
            // txtDueDate2
            // 
            this.txtDueDate2.CanGrow = false;
            this.txtDueDate2.Height = 0.19F;
            this.txtDueDate2.Left = 4.3125F;
            this.txtDueDate2.MultiLine = false;
            this.txtDueDate2.Name = "txtDueDate2";
            this.txtDueDate2.Style = "text-align: center";
            this.txtDueDate2.Tag = "textbox";
            this.txtDueDate2.Text = null;
            this.txtDueDate2.Top = 8.0625F;
            this.txtDueDate2.Visible = false;
            this.txtDueDate2.Width = 0.9375F;
            // 
            // txtAmount2
            // 
            this.txtAmount2.CanGrow = false;
            this.txtAmount2.Height = 0.19F;
            this.txtAmount2.Left = 5.3125F;
            this.txtAmount2.MultiLine = false;
            this.txtAmount2.Name = "txtAmount2";
            this.txtAmount2.Style = "text-align: center";
            this.txtAmount2.Tag = "textbox";
            this.txtAmount2.Text = null;
            this.txtAmount2.Top = 8.0625F;
            this.txtAmount2.Visible = false;
            this.txtAmount2.Width = 1.0625F;
            // 
            // txtSecondPayment
            // 
            this.txtSecondPayment.CanGrow = false;
            this.txtSecondPayment.Height = 0.21875F;
            this.txtSecondPayment.Left = 5.125F;
            this.txtSecondPayment.Name = "txtSecondPayment";
            this.txtSecondPayment.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtSecondPayment.Text = "Second Payment";
            this.txtSecondPayment.Top = 8.6875F;
            this.txtSecondPayment.Visible = false;
            this.txtSecondPayment.Width = 1.5F;
            // 
            // txtFirstPayment
            // 
            this.txtFirstPayment.CanGrow = false;
            this.txtFirstPayment.Height = 0.21875F;
            this.txtFirstPayment.Left = 5.1875F;
            this.txtFirstPayment.Name = "txtFirstPayment";
            this.txtFirstPayment.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtFirstPayment.Text = "First Payment";
            this.txtFirstPayment.Top = 10.1875F;
            this.txtFirstPayment.Visible = false;
            this.txtFirstPayment.Width = 1.5F;
            // 
            // txtDiscountMsg1
            // 
            this.txtDiscountMsg1.Height = 0.19F;
            this.txtDiscountMsg1.Left = 0.6875F;
            this.txtDiscountMsg1.Name = "txtDiscountMsg1";
            this.txtDiscountMsg1.Text = "discount available. To obtain, pay";
            this.txtDiscountMsg1.Top = 3.65625F;
            this.txtDiscountMsg1.Visible = false;
            this.txtDiscountMsg1.Width = 3.0625F;
            // 
            // txtDiscountAmount
            // 
            this.txtDiscountAmount.CanGrow = false;
            this.txtDiscountAmount.Height = 0.19F;
            this.txtDiscountAmount.Left = 3.75F;
            this.txtDiscountAmount.Name = "txtDiscountAmount";
            this.txtDiscountAmount.Style = "text-align: right";
            this.txtDiscountAmount.Text = null;
            this.txtDiscountAmount.Top = 3.65625F;
            this.txtDiscountAmount.Visible = false;
            this.txtDiscountAmount.Width = 1.125F;
            // 
            // txtDiscountMsg2
            // 
            this.txtDiscountMsg2.CanGrow = false;
            this.txtDiscountMsg2.Height = 0.19F;
            this.txtDiscountMsg2.Left = 4.9375F;
            this.txtDiscountMsg2.Name = "txtDiscountMsg2";
            this.txtDiscountMsg2.Text = "in full by";
            this.txtDiscountMsg2.Top = 3.65625F;
            this.txtDiscountMsg2.Visible = false;
            this.txtDiscountMsg2.Width = 1F;
            // 
            // txtRemit2
            // 
            this.txtRemit2.Height = 0.19F;
            this.txtRemit2.Left = 1.5625F;
            this.txtRemit2.Name = "txtRemit2";
            this.txtRemit2.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
            this.txtRemit2.Text = "Please remit this portion with your second payment";
            this.txtRemit2.Top = 7.78125F;
            this.txtRemit2.Visible = false;
            this.txtRemit2.Width = 4.375F;
            // 
            // txtRemit1
            // 
            this.txtRemit1.Height = 0.19F;
            this.txtRemit1.Left = 1.5625F;
            this.txtRemit1.Name = "txtRemit1";
            this.txtRemit1.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
            this.txtRemit1.Text = "Please remit this portion with your first payment";
            this.txtRemit1.Top = 9.3125F;
            this.txtRemit1.Visible = false;
            this.txtRemit1.Width = 4.375F;
            // 
            // Image1
            // 
            this.Image1.Height = 1.1875F;
            this.Image1.HyperLink = null;
            this.Image1.ImageData = null;
            this.Image1.Left = 0F;
            this.Image1.LineWeight = 1F;
            this.Image1.Name = "Image1";
            this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.Image1.Top = 0.375F;
            this.Image1.Width = 1.1875F;
            // 
            // txtNA
            // 
            this.txtNA.Height = 0.46875F;
            this.txtNA.Left = 0.5625F;
            this.txtNA.Name = "txtNA";
            this.txtNA.Style = "font-family: \'Tahoma\'; font-size: 26.5pt";
            this.txtNA.Text = "N/A";
            this.txtNA.Top = 8.28125F;
            this.txtNA.Visible = false;
            this.txtNA.Width = 0.875F;
            // 
            // txtTaxBillYear
            // 
            this.txtTaxBillYear.CanGrow = false;
            this.txtTaxBillYear.Height = 0.19F;
            this.txtTaxBillYear.Left = 1.125F;
            this.txtTaxBillYear.MultiLine = false;
            this.txtTaxBillYear.Name = "txtTaxBillYear";
            this.txtTaxBillYear.Style = "font-family: \'Courier New\'; text-align: center";
            this.txtTaxBillYear.Tag = "textbox";
            this.txtTaxBillYear.Text = null;
            this.txtTaxBillYear.Top = 0.125F;
            this.txtTaxBillYear.Visible = false;
            this.txtTaxBillYear.Width = 3.125F;
            // 
            // lblAcres
            // 
            this.lblAcres.CanGrow = false;
            this.lblAcres.Height = 0.19F;
            this.lblAcres.Left = 0F;
            this.lblAcres.MultiLine = false;
            this.lblAcres.Name = "lblAcres";
            this.lblAcres.Style = "font-family: \'Courier New\'; font-weight: bold";
            this.lblAcres.Tag = "textbox";
            this.lblAcres.Text = "Acres:";
            this.lblAcres.Top = 3.1875F;
            this.lblAcres.Visible = false;
            this.lblAcres.Width = 0.6875F;
            // 
            // txtAcres
            // 
            this.txtAcres.CanGrow = false;
            this.txtAcres.Height = 0.19F;
            this.txtAcres.Left = 0.6875F;
            this.txtAcres.MultiLine = false;
            this.txtAcres.Name = "txtAcres";
            this.txtAcres.Style = "font-family: \'Courier New\'";
            this.txtAcres.Tag = "textbox";
            this.txtAcres.Text = null;
            this.txtAcres.Top = 3.1875F;
            this.txtAcres.Visible = false;
            this.txtAcres.Width = 0.9375F;
            // 
            // txtAccountAddress
            // 
            this.txtAccountAddress.CanGrow = false;
            this.txtAccountAddress.Height = 0.19F;
            this.txtAccountAddress.Left = 1.1875F;
            this.txtAccountAddress.MultiLine = false;
            this.txtAccountAddress.Name = "txtAccountAddress";
            this.txtAccountAddress.Style = "font-family: \'Courier New\'";
            this.txtAccountAddress.Tag = "textbox";
            this.txtAccountAddress.Text = null;
            this.txtAccountAddress.Top = 1.9375F;
            this.txtAccountAddress.Width = 1.625F;
            // 
            // txtBillYearStub1
            // 
            this.txtBillYearStub1.CanGrow = false;
            this.txtBillYearStub1.Height = 0.19F;
            this.txtBillYearStub1.Left = 0.0625F;
            this.txtBillYearStub1.MultiLine = false;
            this.txtBillYearStub1.Name = "txtBillYearStub1";
            this.txtBillYearStub1.Style = "text-align: center";
            this.txtBillYearStub1.Tag = "textbox";
            this.txtBillYearStub1.Text = null;
            this.txtBillYearStub1.Top = 9.4375F;
            this.txtBillYearStub1.Width = 3F;
            // 
            // txtBillYearStub2
            // 
            this.txtBillYearStub2.CanGrow = false;
            this.txtBillYearStub2.Height = 0.19F;
            this.txtBillYearStub2.Left = 0.0625F;
            this.txtBillYearStub2.MultiLine = false;
            this.txtBillYearStub2.Name = "txtBillYearStub2";
            this.txtBillYearStub2.Style = "text-align: center";
            this.txtBillYearStub2.Tag = "textbox";
            this.txtBillYearStub2.Text = null;
            this.txtBillYearStub2.Top = 7.96875F;
            this.txtBillYearStub2.Visible = false;
            this.txtBillYearStub2.Width = 3F;
            // 
            // shapeInformation
            // 
            this.shapeInformation.Height = 2.3125F;
            this.shapeInformation.Left = 0F;
            this.shapeInformation.Name = "shapeInformation";
            this.shapeInformation.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.shapeInformation.Top = 3.875F;
            this.shapeInformation.Visible = false;
            this.shapeInformation.Width = 7.625F;
            // 
            // txtInfoLbl0
            // 
            this.txtInfoLbl0.CanGrow = false;
            this.txtInfoLbl0.Height = 0.1777778F;
            this.txtInfoLbl0.Left = 0F;
            this.txtInfoLbl0.MultiLine = false;
            this.txtInfoLbl0.Name = "txtInfoLbl0";
            this.txtInfoLbl0.Style = "font-family: \'Courier New\'; text-align: center";
            this.txtInfoLbl0.Tag = "textbox";
            this.txtInfoLbl0.Text = null;
            this.txtInfoLbl0.Top = 3.875F;
            this.txtInfoLbl0.Width = 7.625F;
            // 
            // txtInfoLbl1
            // 
            this.txtInfoLbl1.CanGrow = false;
            this.txtInfoLbl1.Height = 0.1777778F;
            this.txtInfoLbl1.Left = 0F;
            this.txtInfoLbl1.MultiLine = false;
            this.txtInfoLbl1.Name = "txtInfoLbl1";
            this.txtInfoLbl1.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl1.Tag = "textbox";
            this.txtInfoLbl1.Text = null;
            this.txtInfoLbl1.Top = 4.052778F;
            this.txtInfoLbl1.Width = 7.625F;
            // 
            // txtInfoLbl2
            // 
            this.txtInfoLbl2.CanGrow = false;
            this.txtInfoLbl2.Height = 0.1777778F;
            this.txtInfoLbl2.Left = 0F;
            this.txtInfoLbl2.MultiLine = false;
            this.txtInfoLbl2.Name = "txtInfoLbl2";
            this.txtInfoLbl2.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl2.Tag = "textbox";
            this.txtInfoLbl2.Text = null;
            this.txtInfoLbl2.Top = 4.230556F;
            this.txtInfoLbl2.Width = 7.625F;
            // 
            // txtInfoLbl3
            // 
            this.txtInfoLbl3.CanGrow = false;
            this.txtInfoLbl3.Height = 0.1777778F;
            this.txtInfoLbl3.Left = 0F;
            this.txtInfoLbl3.MultiLine = false;
            this.txtInfoLbl3.Name = "txtInfoLbl3";
            this.txtInfoLbl3.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl3.Tag = "textbox";
            this.txtInfoLbl3.Text = null;
            this.txtInfoLbl3.Top = 4.408333F;
            this.txtInfoLbl3.Width = 7.625F;
            // 
            // txtInfoLbl4
            // 
            this.txtInfoLbl4.CanGrow = false;
            this.txtInfoLbl4.Height = 0.1777778F;
            this.txtInfoLbl4.Left = 0F;
            this.txtInfoLbl4.MultiLine = false;
            this.txtInfoLbl4.Name = "txtInfoLbl4";
            this.txtInfoLbl4.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl4.Tag = "textbox";
            this.txtInfoLbl4.Text = null;
            this.txtInfoLbl4.Top = 4.586111F;
            this.txtInfoLbl4.Width = 7.625F;
            // 
            // txtInfoLbl5
            // 
            this.txtInfoLbl5.CanGrow = false;
            this.txtInfoLbl5.Height = 0.1777778F;
            this.txtInfoLbl5.Left = 0F;
            this.txtInfoLbl5.MultiLine = false;
            this.txtInfoLbl5.Name = "txtInfoLbl5";
            this.txtInfoLbl5.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl5.Tag = "textbox";
            this.txtInfoLbl5.Text = null;
            this.txtInfoLbl5.Top = 4.763889F;
            this.txtInfoLbl5.Width = 7.625F;
            // 
            // txtInfoLbl6
            // 
            this.txtInfoLbl6.CanGrow = false;
            this.txtInfoLbl6.Height = 0.1777778F;
            this.txtInfoLbl6.Left = 0F;
            this.txtInfoLbl6.MultiLine = false;
            this.txtInfoLbl6.Name = "txtInfoLbl6";
            this.txtInfoLbl6.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl6.Tag = "textbox";
            this.txtInfoLbl6.Text = null;
            this.txtInfoLbl6.Top = 4.941667F;
            this.txtInfoLbl6.Width = 7.625F;
            // 
            // txtInfoLbl7
            // 
            this.txtInfoLbl7.CanGrow = false;
            this.txtInfoLbl7.Height = 0.1777778F;
            this.txtInfoLbl7.Left = 0F;
            this.txtInfoLbl7.MultiLine = false;
            this.txtInfoLbl7.Name = "txtInfoLbl7";
            this.txtInfoLbl7.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl7.Tag = "textbox";
            this.txtInfoLbl7.Text = null;
            this.txtInfoLbl7.Top = 5.119444F;
            this.txtInfoLbl7.Width = 7.625F;
            // 
            // txtInfoLbl8
            // 
            this.txtInfoLbl8.CanGrow = false;
            this.txtInfoLbl8.Height = 0.1777778F;
            this.txtInfoLbl8.Left = 0F;
            this.txtInfoLbl8.MultiLine = false;
            this.txtInfoLbl8.Name = "txtInfoLbl8";
            this.txtInfoLbl8.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl8.Tag = "textbox";
            this.txtInfoLbl8.Text = null;
            this.txtInfoLbl8.Top = 5.297222F;
            this.txtInfoLbl8.Width = 7.625F;
            // 
            // txtInfoLbl9
            // 
            this.txtInfoLbl9.CanGrow = false;
            this.txtInfoLbl9.Height = 0.1777778F;
            this.txtInfoLbl9.Left = 0F;
            this.txtInfoLbl9.MultiLine = false;
            this.txtInfoLbl9.Name = "txtInfoLbl9";
            this.txtInfoLbl9.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl9.Tag = "textbox";
            this.txtInfoLbl9.Text = null;
            this.txtInfoLbl9.Top = 5.475F;
            this.txtInfoLbl9.Width = 7.625F;
            // 
            // txtInfoLbl10
            // 
            this.txtInfoLbl10.CanGrow = false;
            this.txtInfoLbl10.Height = 0.1777778F;
            this.txtInfoLbl10.Left = 0F;
            this.txtInfoLbl10.MultiLine = false;
            this.txtInfoLbl10.Name = "txtInfoLbl10";
            this.txtInfoLbl10.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl10.Tag = "textbox";
            this.txtInfoLbl10.Text = null;
            this.txtInfoLbl10.Top = 5.652778F;
            this.txtInfoLbl10.Width = 7.625F;
            // 
            // txtInfoLbl11
            // 
            this.txtInfoLbl11.CanGrow = false;
            this.txtInfoLbl11.Height = 0.1777778F;
            this.txtInfoLbl11.Left = 0F;
            this.txtInfoLbl11.MultiLine = false;
            this.txtInfoLbl11.Name = "txtInfoLbl11";
            this.txtInfoLbl11.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl11.Tag = "textbox";
            this.txtInfoLbl11.Text = null;
            this.txtInfoLbl11.Top = 5.830555F;
            this.txtInfoLbl11.Width = 7.625F;
            // 
            // txtInfoLbl12
            // 
            this.txtInfoLbl12.CanGrow = false;
            this.txtInfoLbl12.Height = 0.1777778F;
            this.txtInfoLbl12.Left = 0F;
            this.txtInfoLbl12.MultiLine = false;
            this.txtInfoLbl12.Name = "txtInfoLbl12";
            this.txtInfoLbl12.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtInfoLbl12.Tag = "textbox";
            this.txtInfoLbl12.Text = null;
            this.txtInfoLbl12.Top = 6.008333F;
            this.txtInfoLbl12.Width = 7.625F;
            // 
            // shapeDistribution
            // 
            this.shapeDistribution.Height = 1.40625F;
            this.shapeDistribution.Left = 0F;
            this.shapeDistribution.Name = "shapeDistribution";
            this.shapeDistribution.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.shapeDistribution.Top = 6.25F;
            this.shapeDistribution.Visible = false;
            this.shapeDistribution.Width = 3.6875F;
            // 
            // shapeRemittance
            // 
            this.shapeRemittance.Height = 1.40625F;
            this.shapeRemittance.Left = 3.8125F;
            this.shapeRemittance.Name = "shapeRemittance";
            this.shapeRemittance.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.shapeRemittance.Top = 6.25F;
            this.shapeRemittance.Visible = false;
            this.shapeRemittance.Width = 3.8125F;
            // 
            // txtDistLbl0
            // 
            this.txtDistLbl0.CanGrow = false;
            this.txtDistLbl0.Height = 0.1756945F;
            this.txtDistLbl0.Left = 0F;
            this.txtDistLbl0.MultiLine = false;
            this.txtDistLbl0.Name = "txtDistLbl0";
            this.txtDistLbl0.Style = "font-family: \'Courier New\'; text-align: center";
            this.txtDistLbl0.Tag = "textbox";
            this.txtDistLbl0.Text = null;
            this.txtDistLbl0.Top = 6.25F;
            this.txtDistLbl0.Width = 3.6875F;
            // 
            // txtDistLbl7
            // 
            this.txtDistLbl7.CanGrow = false;
            this.txtDistLbl7.Height = 0.1756945F;
            this.txtDistLbl7.Left = 0.0625F;
            this.txtDistLbl7.MultiLine = false;
            this.txtDistLbl7.Name = "txtDistLbl7";
            this.txtDistLbl7.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtDistLbl7.Tag = "textbox";
            this.txtDistLbl7.Text = null;
            this.txtDistLbl7.Top = 7.479861F;
            this.txtDistLbl7.Width = 1.4375F;
            // 
            // txtDistLbl1
            // 
            this.txtDistLbl1.CanGrow = false;
            this.txtDistLbl1.Height = 0.1756945F;
            this.txtDistLbl1.Left = 0.0625F;
            this.txtDistLbl1.MultiLine = false;
            this.txtDistLbl1.Name = "txtDistLbl1";
            this.txtDistLbl1.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtDistLbl1.Tag = "textbox";
            this.txtDistLbl1.Text = null;
            this.txtDistLbl1.Top = 6.425694F;
            this.txtDistLbl1.Width = 1.4375F;
            // 
            // txtDistLbl2
            // 
            this.txtDistLbl2.CanGrow = false;
            this.txtDistLbl2.Height = 0.1756945F;
            this.txtDistLbl2.Left = 0.0625F;
            this.txtDistLbl2.MultiLine = false;
            this.txtDistLbl2.Name = "txtDistLbl2";
            this.txtDistLbl2.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtDistLbl2.Tag = "textbox";
            this.txtDistLbl2.Text = null;
            this.txtDistLbl2.Top = 6.601389F;
            this.txtDistLbl2.Width = 1.4375F;
            // 
            // txtDistLBL3
            // 
            this.txtDistLBL3.CanGrow = false;
            this.txtDistLBL3.Height = 0.1756945F;
            this.txtDistLBL3.Left = 0.0625F;
            this.txtDistLBL3.MultiLine = false;
            this.txtDistLBL3.Name = "txtDistLBL3";
            this.txtDistLBL3.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtDistLBL3.Tag = "textbox";
            this.txtDistLBL3.Text = null;
            this.txtDistLBL3.Top = 6.777083F;
            this.txtDistLBL3.Width = 1.4375F;
            // 
            // txtDistLbl4
            // 
            this.txtDistLbl4.CanGrow = false;
            this.txtDistLbl4.Height = 0.1756945F;
            this.txtDistLbl4.Left = 0.0625F;
            this.txtDistLbl4.MultiLine = false;
            this.txtDistLbl4.Name = "txtDistLbl4";
            this.txtDistLbl4.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtDistLbl4.Tag = "textbox";
            this.txtDistLbl4.Text = null;
            this.txtDistLbl4.Top = 6.952778F;
            this.txtDistLbl4.Width = 1.4375F;
            // 
            // txtDistLbl5
            // 
            this.txtDistLbl5.CanGrow = false;
            this.txtDistLbl5.Height = 0.1756945F;
            this.txtDistLbl5.Left = 0.0625F;
            this.txtDistLbl5.MultiLine = false;
            this.txtDistLbl5.Name = "txtDistLbl5";
            this.txtDistLbl5.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtDistLbl5.Tag = "textbox";
            this.txtDistLbl5.Text = null;
            this.txtDistLbl5.Top = 7.128472F;
            this.txtDistLbl5.Width = 1.4375F;
            // 
            // txtDistLbl6
            // 
            this.txtDistLbl6.CanGrow = false;
            this.txtDistLbl6.Height = 0.1756945F;
            this.txtDistLbl6.Left = 0.0625F;
            this.txtDistLbl6.MultiLine = false;
            this.txtDistLbl6.Name = "txtDistLbl6";
            this.txtDistLbl6.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtDistLbl6.Tag = "textbox";
            this.txtDistLbl6.Text = null;
            this.txtDistLbl6.Top = 7.304167F;
            this.txtDistLbl6.Width = 1.4375F;
            // 
            // txtDistAm7
            // 
            this.txtDistAm7.CanGrow = false;
            this.txtDistAm7.Height = 0.1756945F;
            this.txtDistAm7.Left = 2.4375F;
            this.txtDistAm7.MultiLine = false;
            this.txtDistAm7.Name = "txtDistAm7";
            this.txtDistAm7.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistAm7.Tag = "textbox";
            this.txtDistAm7.Text = null;
            this.txtDistAm7.Top = 7.479861F;
            this.txtDistAm7.Width = 1.25F;
            // 
            // txtDistAm1
            // 
            this.txtDistAm1.CanGrow = false;
            this.txtDistAm1.Height = 0.1756945F;
            this.txtDistAm1.Left = 2.4375F;
            this.txtDistAm1.MultiLine = false;
            this.txtDistAm1.Name = "txtDistAm1";
            this.txtDistAm1.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistAm1.Tag = "textbox";
            this.txtDistAm1.Text = null;
            this.txtDistAm1.Top = 6.425694F;
            this.txtDistAm1.Width = 1.25F;
            // 
            // txtDistAm2
            // 
            this.txtDistAm2.CanGrow = false;
            this.txtDistAm2.Height = 0.1756945F;
            this.txtDistAm2.Left = 2.4375F;
            this.txtDistAm2.MultiLine = false;
            this.txtDistAm2.Name = "txtDistAm2";
            this.txtDistAm2.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistAm2.Tag = "textbox";
            this.txtDistAm2.Text = null;
            this.txtDistAm2.Top = 6.601389F;
            this.txtDistAm2.Width = 1.25F;
            // 
            // txtDistAm3
            // 
            this.txtDistAm3.CanGrow = false;
            this.txtDistAm3.Height = 0.1756945F;
            this.txtDistAm3.Left = 2.4375F;
            this.txtDistAm3.MultiLine = false;
            this.txtDistAm3.Name = "txtDistAm3";
            this.txtDistAm3.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistAm3.Tag = "textbox";
            this.txtDistAm3.Text = null;
            this.txtDistAm3.Top = 6.777083F;
            this.txtDistAm3.Width = 1.25F;
            // 
            // txtDistAm4
            // 
            this.txtDistAm4.CanGrow = false;
            this.txtDistAm4.Height = 0.1756945F;
            this.txtDistAm4.Left = 2.4375F;
            this.txtDistAm4.MultiLine = false;
            this.txtDistAm4.Name = "txtDistAm4";
            this.txtDistAm4.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistAm4.Tag = "textbox";
            this.txtDistAm4.Text = null;
            this.txtDistAm4.Top = 6.952778F;
            this.txtDistAm4.Width = 1.25F;
            // 
            // txtDistAm5
            // 
            this.txtDistAm5.CanGrow = false;
            this.txtDistAm5.Height = 0.1756945F;
            this.txtDistAm5.Left = 2.4375F;
            this.txtDistAm5.MultiLine = false;
            this.txtDistAm5.Name = "txtDistAm5";
            this.txtDistAm5.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistAm5.Tag = "textbox";
            this.txtDistAm5.Text = null;
            this.txtDistAm5.Top = 7.128472F;
            this.txtDistAm5.Width = 1.25F;
            // 
            // txtDistAm6
            // 
            this.txtDistAm6.CanGrow = false;
            this.txtDistAm6.Height = 0.1756945F;
            this.txtDistAm6.Left = 2.4375F;
            this.txtDistAm6.MultiLine = false;
            this.txtDistAm6.Name = "txtDistAm6";
            this.txtDistAm6.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistAm6.Tag = "textbox";
            this.txtDistAm6.Text = null;
            this.txtDistAm6.Top = 7.304167F;
            this.txtDistAm6.Width = 1.25F;
            // 
            // txtDistPerc7
            // 
            this.txtDistPerc7.CanGrow = false;
            this.txtDistPerc7.Height = 0.1756945F;
            this.txtDistPerc7.Left = 1.5625F;
            this.txtDistPerc7.MultiLine = false;
            this.txtDistPerc7.Name = "txtDistPerc7";
            this.txtDistPerc7.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistPerc7.Tag = "textbox";
            this.txtDistPerc7.Text = null;
            this.txtDistPerc7.Top = 7.479861F;
            this.txtDistPerc7.Width = 0.875F;
            // 
            // txtDistPerc1
            // 
            this.txtDistPerc1.CanGrow = false;
            this.txtDistPerc1.Height = 0.1756945F;
            this.txtDistPerc1.Left = 1.5625F;
            this.txtDistPerc1.MultiLine = false;
            this.txtDistPerc1.Name = "txtDistPerc1";
            this.txtDistPerc1.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistPerc1.Tag = "textbox";
            this.txtDistPerc1.Text = null;
            this.txtDistPerc1.Top = 6.425694F;
            this.txtDistPerc1.Width = 0.875F;
            // 
            // txtDistPer2
            // 
            this.txtDistPer2.CanGrow = false;
            this.txtDistPer2.Height = 0.1756945F;
            this.txtDistPer2.Left = 1.5625F;
            this.txtDistPer2.MultiLine = false;
            this.txtDistPer2.Name = "txtDistPer2";
            this.txtDistPer2.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistPer2.Tag = "textbox";
            this.txtDistPer2.Text = null;
            this.txtDistPer2.Top = 6.601389F;
            this.txtDistPer2.Width = 0.875F;
            // 
            // txtDistPerc3
            // 
            this.txtDistPerc3.CanGrow = false;
            this.txtDistPerc3.Height = 0.1756945F;
            this.txtDistPerc3.Left = 1.5625F;
            this.txtDistPerc3.MultiLine = false;
            this.txtDistPerc3.Name = "txtDistPerc3";
            this.txtDistPerc3.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistPerc3.Tag = "textbox";
            this.txtDistPerc3.Text = null;
            this.txtDistPerc3.Top = 6.777083F;
            this.txtDistPerc3.Width = 0.875F;
            // 
            // txtDistPerc4
            // 
            this.txtDistPerc4.CanGrow = false;
            this.txtDistPerc4.Height = 0.1756945F;
            this.txtDistPerc4.Left = 1.5625F;
            this.txtDistPerc4.MultiLine = false;
            this.txtDistPerc4.Name = "txtDistPerc4";
            this.txtDistPerc4.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistPerc4.Tag = "textbox";
            this.txtDistPerc4.Text = null;
            this.txtDistPerc4.Top = 6.952778F;
            this.txtDistPerc4.Width = 0.875F;
            // 
            // txtDistPerc5
            // 
            this.txtDistPerc5.CanGrow = false;
            this.txtDistPerc5.Height = 0.1756945F;
            this.txtDistPerc5.Left = 1.5625F;
            this.txtDistPerc5.MultiLine = false;
            this.txtDistPerc5.Name = "txtDistPerc5";
            this.txtDistPerc5.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistPerc5.Tag = "textbox";
            this.txtDistPerc5.Text = null;
            this.txtDistPerc5.Top = 7.128472F;
            this.txtDistPerc5.Width = 0.875F;
            // 
            // txtDistPerc6
            // 
            this.txtDistPerc6.CanGrow = false;
            this.txtDistPerc6.Height = 0.1756945F;
            this.txtDistPerc6.Left = 1.5625F;
            this.txtDistPerc6.MultiLine = false;
            this.txtDistPerc6.Name = "txtDistPerc6";
            this.txtDistPerc6.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtDistPerc6.Tag = "textbox";
            this.txtDistPerc6.Text = null;
            this.txtDistPerc6.Top = 7.304167F;
            this.txtDistPerc6.Width = 0.875F;
            // 
            // txtRemitLbl0
            // 
            this.txtRemitLbl0.CanGrow = false;
            this.txtRemitLbl0.Height = 0.1756945F;
            this.txtRemitLbl0.Left = 3.8125F;
            this.txtRemitLbl0.MultiLine = false;
            this.txtRemitLbl0.Name = "txtRemitLbl0";
            this.txtRemitLbl0.Style = "font-family: \'Courier New\'; text-align: center";
            this.txtRemitLbl0.Tag = "textbox";
            this.txtRemitLbl0.Text = null;
            this.txtRemitLbl0.Top = 6.25F;
            this.txtRemitLbl0.Width = 3.8125F;
            // 
            // txtRemitLbl1
            // 
            this.txtRemitLbl1.CanGrow = false;
            this.txtRemitLbl1.Height = 0.1756945F;
            this.txtRemitLbl1.Left = 3.8125F;
            this.txtRemitLbl1.MultiLine = false;
            this.txtRemitLbl1.Name = "txtRemitLbl1";
            this.txtRemitLbl1.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtRemitLbl1.Tag = "textbox";
            this.txtRemitLbl1.Text = null;
            this.txtRemitLbl1.Top = 6.425694F;
            this.txtRemitLbl1.Width = 3.8125F;
            // 
            // txtRemitLbl2
            // 
            this.txtRemitLbl2.CanGrow = false;
            this.txtRemitLbl2.Height = 0.1756945F;
            this.txtRemitLbl2.Left = 3.8125F;
            this.txtRemitLbl2.MultiLine = false;
            this.txtRemitLbl2.Name = "txtRemitLbl2";
            this.txtRemitLbl2.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtRemitLbl2.Tag = "textbox";
            this.txtRemitLbl2.Text = null;
            this.txtRemitLbl2.Top = 6.601389F;
            this.txtRemitLbl2.Width = 3.8125F;
            // 
            // txtRemitLbl3
            // 
            this.txtRemitLbl3.CanGrow = false;
            this.txtRemitLbl3.Height = 0.1756945F;
            this.txtRemitLbl3.Left = 3.8125F;
            this.txtRemitLbl3.MultiLine = false;
            this.txtRemitLbl3.Name = "txtRemitLbl3";
            this.txtRemitLbl3.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtRemitLbl3.Tag = "textbox";
            this.txtRemitLbl3.Text = null;
            this.txtRemitLbl3.Top = 6.777083F;
            this.txtRemitLbl3.Width = 3.8125F;
            // 
            // txtRemitLbl4
            // 
            this.txtRemitLbl4.CanGrow = false;
            this.txtRemitLbl4.Height = 0.1756945F;
            this.txtRemitLbl4.Left = 3.8125F;
            this.txtRemitLbl4.MultiLine = false;
            this.txtRemitLbl4.Name = "txtRemitLbl4";
            this.txtRemitLbl4.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtRemitLbl4.Tag = "textbox";
            this.txtRemitLbl4.Text = null;
            this.txtRemitLbl4.Top = 6.952778F;
            this.txtRemitLbl4.Width = 3.8125F;
            // 
            // txtRemitLbl5
            // 
            this.txtRemitLbl5.CanGrow = false;
            this.txtRemitLbl5.Height = 0.1756945F;
            this.txtRemitLbl5.Left = 3.8125F;
            this.txtRemitLbl5.MultiLine = false;
            this.txtRemitLbl5.Name = "txtRemitLbl5";
            this.txtRemitLbl5.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtRemitLbl5.Tag = "textbox";
            this.txtRemitLbl5.Text = null;
            this.txtRemitLbl5.Top = 7.128472F;
            this.txtRemitLbl5.Width = 3.8125F;
            // 
            // txtRemitLbl6
            // 
            this.txtRemitLbl6.CanGrow = false;
            this.txtRemitLbl6.Height = 0.1756945F;
            this.txtRemitLbl6.Left = 3.8125F;
            this.txtRemitLbl6.MultiLine = false;
            this.txtRemitLbl6.Name = "txtRemitLbl6";
            this.txtRemitLbl6.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtRemitLbl6.Tag = "textbox";
            this.txtRemitLbl6.Text = null;
            this.txtRemitLbl6.Top = 7.304167F;
            this.txtRemitLbl6.Width = 3.8125F;
            // 
            // txtRemitLbl7
            // 
            this.txtRemitLbl7.CanGrow = false;
            this.txtRemitLbl7.Height = 0.1756945F;
            this.txtRemitLbl7.Left = 3.8125F;
            this.txtRemitLbl7.MultiLine = false;
            this.txtRemitLbl7.Name = "txtRemitLbl7";
            this.txtRemitLbl7.Style = "font-family: \'Courier New\'; text-align: left";
            this.txtRemitLbl7.Tag = "textbox";
            this.txtRemitLbl7.Text = null;
            this.txtRemitLbl7.Top = 7.479861F;
            this.txtRemitLbl7.Width = 3.8125F;
            // 
            // ValuationLine1
            // 
            this.ValuationLine1.Height = 3.0625F;
            this.ValuationLine1.Left = 6.0625F;
            this.ValuationLine1.LineWeight = 1F;
            this.ValuationLine1.Name = "ValuationLine1";
            this.ValuationLine1.Tag = "Line";
            this.ValuationLine1.Top = 0.25F;
            this.ValuationLine1.Visible = false;
            this.ValuationLine1.Width = 0F;
            this.ValuationLine1.X1 = 6.0625F;
            this.ValuationLine1.X2 = 6.0625F;
            this.ValuationLine1.Y1 = 0.25F;
            this.ValuationLine1.Y2 = 3.3125F;
            // 
            // txtValLbl1
            // 
            this.txtValLbl1.CanGrow = false;
            this.txtValLbl1.Height = 0.1875F;
            this.txtValLbl1.Left = 4.5F;
            this.txtValLbl1.MultiLine = false;
            this.txtValLbl1.Name = "txtValLbl1";
            this.txtValLbl1.Style = "font-family: \'Courier New\'";
            this.txtValLbl1.Tag = "textbox";
            this.txtValLbl1.Text = null;
            this.txtValLbl1.Top = 0.25F;
            this.txtValLbl1.Width = 1.5625F;
            // 
            // txtValLbl2
            // 
            this.txtValLbl2.CanGrow = false;
            this.txtValLbl2.Height = 0.1875F;
            this.txtValLbl2.Left = 4.5F;
            this.txtValLbl2.MultiLine = false;
            this.txtValLbl2.Name = "txtValLbl2";
            this.txtValLbl2.Style = "font-family: \'Courier New\'";
            this.txtValLbl2.Tag = "textbox";
            this.txtValLbl2.Text = null;
            this.txtValLbl2.Top = 0.375F;
            this.txtValLbl2.Width = 1.5625F;
            // 
            // txtValLbl3
            // 
            this.txtValLbl3.CanGrow = false;
            this.txtValLbl3.Height = 0.1875F;
            this.txtValLbl3.Left = 4.5F;
            this.txtValLbl3.MultiLine = false;
            this.txtValLbl3.Name = "txtValLbl3";
            this.txtValLbl3.Style = "font-family: \'Courier New\'";
            this.txtValLbl3.Tag = "textbox";
            this.txtValLbl3.Text = null;
            this.txtValLbl3.Top = 0.5625F;
            this.txtValLbl3.Width = 1.5625F;
            // 
            // txtValLbl4
            // 
            this.txtValLbl4.CanGrow = false;
            this.txtValLbl4.Height = 0.1875F;
            this.txtValLbl4.Left = 4.5F;
            this.txtValLbl4.MultiLine = false;
            this.txtValLbl4.Name = "txtValLbl4";
            this.txtValLbl4.Style = "font-family: \'Courier New\'";
            this.txtValLbl4.Tag = "textbox";
            this.txtValLbl4.Text = null;
            this.txtValLbl4.Top = 0.75F;
            this.txtValLbl4.Width = 1.5625F;
            // 
            // txtValLbl5
            // 
            this.txtValLbl5.CanGrow = false;
            this.txtValLbl5.Height = 0.1875F;
            this.txtValLbl5.Left = 4.5F;
            this.txtValLbl5.MultiLine = false;
            this.txtValLbl5.Name = "txtValLbl5";
            this.txtValLbl5.Style = "font-family: \'Courier New\'";
            this.txtValLbl5.Tag = "textbox";
            this.txtValLbl5.Text = null;
            this.txtValLbl5.Top = 0.9375F;
            this.txtValLbl5.Width = 1.5625F;
            // 
            // txtValLbl6
            // 
            this.txtValLbl6.CanGrow = false;
            this.txtValLbl6.Height = 0.1875F;
            this.txtValLbl6.Left = 4.5F;
            this.txtValLbl6.MultiLine = false;
            this.txtValLbl6.Name = "txtValLbl6";
            this.txtValLbl6.Style = "font-family: \'Courier New\'";
            this.txtValLbl6.Tag = "textbox";
            this.txtValLbl6.Text = null;
            this.txtValLbl6.Top = 1.125F;
            this.txtValLbl6.Width = 1.5625F;
            // 
            // txtValLbl7
            // 
            this.txtValLbl7.CanGrow = false;
            this.txtValLbl7.Height = 0.1875F;
            this.txtValLbl7.Left = 4.5F;
            this.txtValLbl7.MultiLine = false;
            this.txtValLbl7.Name = "txtValLbl7";
            this.txtValLbl7.Style = "font-family: \'Courier New\'";
            this.txtValLbl7.Tag = "textbox";
            this.txtValLbl7.Text = null;
            this.txtValLbl7.Top = 1.3125F;
            this.txtValLbl7.Width = 1.5625F;
            // 
            // txtValLbl8
            // 
            this.txtValLbl8.CanGrow = false;
            this.txtValLbl8.Height = 0.1875F;
            this.txtValLbl8.Left = 4.5F;
            this.txtValLbl8.MultiLine = false;
            this.txtValLbl8.Name = "txtValLbl8";
            this.txtValLbl8.Style = "font-family: \'Courier New\'";
            this.txtValLbl8.Tag = "textbox";
            this.txtValLbl8.Text = null;
            this.txtValLbl8.Top = 1.5F;
            this.txtValLbl8.Width = 1.5625F;
            // 
            // txtValLbl9
            // 
            this.txtValLbl9.CanGrow = false;
            this.txtValLbl9.Height = 0.1875F;
            this.txtValLbl9.Left = 4.5F;
            this.txtValLbl9.MultiLine = false;
            this.txtValLbl9.Name = "txtValLbl9";
            this.txtValLbl9.Style = "font-family: \'Courier New\'";
            this.txtValLbl9.Tag = "textbox";
            this.txtValLbl9.Text = null;
            this.txtValLbl9.Top = 1.6875F;
            this.txtValLbl9.Width = 1.5625F;
            // 
            // txtValLbl10
            // 
            this.txtValLbl10.CanGrow = false;
            this.txtValLbl10.Height = 0.1875F;
            this.txtValLbl10.Left = 4.5F;
            this.txtValLbl10.MultiLine = false;
            this.txtValLbl10.Name = "txtValLbl10";
            this.txtValLbl10.Style = "font-family: \'Courier New\'";
            this.txtValLbl10.Tag = "textbox";
            this.txtValLbl10.Text = null;
            this.txtValLbl10.Top = 1.875F;
            this.txtValLbl10.Width = 1.5625F;
            // 
            // txtValLbl11
            // 
            this.txtValLbl11.CanGrow = false;
            this.txtValLbl11.Height = 0.1875F;
            this.txtValLbl11.Left = 4.5F;
            this.txtValLbl11.MultiLine = false;
            this.txtValLbl11.Name = "txtValLbl11";
            this.txtValLbl11.Style = "font-family: \'Courier New\'";
            this.txtValLbl11.Tag = "textbox";
            this.txtValLbl11.Text = null;
            this.txtValLbl11.Top = 2F;
            this.txtValLbl11.Width = 1.5625F;
            // 
            // txtValLbl12
            // 
            this.txtValLbl12.CanGrow = false;
            this.txtValLbl12.Height = 0.1875F;
            this.txtValLbl12.Left = 4.5F;
            this.txtValLbl12.MultiLine = false;
            this.txtValLbl12.Name = "txtValLbl12";
            this.txtValLbl12.Style = "font-family: \'Courier New\'";
            this.txtValLbl12.Tag = "textbox";
            this.txtValLbl12.Text = null;
            this.txtValLbl12.Top = 2.1875F;
            this.txtValLbl12.Width = 1.5625F;
            // 
            // txtValLbl13
            // 
            this.txtValLbl13.CanGrow = false;
            this.txtValLbl13.Height = 0.1875F;
            this.txtValLbl13.Left = 4.5F;
            this.txtValLbl13.MultiLine = false;
            this.txtValLbl13.Name = "txtValLbl13";
            this.txtValLbl13.Style = "font-family: \'Courier New\'";
            this.txtValLbl13.Tag = "textbox";
            this.txtValLbl13.Text = null;
            this.txtValLbl13.Top = 2.375F;
            this.txtValLbl13.Width = 1.5625F;
            // 
            // txtVallbl0
            // 
            this.txtVallbl0.CanGrow = false;
            this.txtVallbl0.Height = 0.1875F;
            this.txtVallbl0.Left = 4.5F;
            this.txtVallbl0.MultiLine = false;
            this.txtVallbl0.Name = "txtVallbl0";
            this.txtVallbl0.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: center";
            this.txtVallbl0.Tag = "textbox";
            this.txtVallbl0.Text = null;
            this.txtVallbl0.Top = 0.0625F;
            this.txtVallbl0.Width = 3.125F;
            // 
            // txtValData1
            // 
            this.txtValData1.CanGrow = false;
            this.txtValData1.Height = 0.1875F;
            this.txtValData1.Left = 6F;
            this.txtValData1.MultiLine = false;
            this.txtValData1.Name = "txtValData1";
            this.txtValData1.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData1.Tag = "textbox";
            this.txtValData1.Text = null;
            this.txtValData1.Top = 0.25F;
            this.txtValData1.Width = 1.5625F;
            // 
            // txtValData2
            // 
            this.txtValData2.CanGrow = false;
            this.txtValData2.Height = 0.1875F;
            this.txtValData2.Left = 6F;
            this.txtValData2.MultiLine = false;
            this.txtValData2.Name = "txtValData2";
            this.txtValData2.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData2.Tag = "textbox";
            this.txtValData2.Text = null;
            this.txtValData2.Top = 0.375F;
            this.txtValData2.Width = 1.5625F;
            // 
            // txtValData3
            // 
            this.txtValData3.CanGrow = false;
            this.txtValData3.Height = 0.1875F;
            this.txtValData3.Left = 6F;
            this.txtValData3.MultiLine = false;
            this.txtValData3.Name = "txtValData3";
            this.txtValData3.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData3.Tag = "textbox";
            this.txtValData3.Text = null;
            this.txtValData3.Top = 0.5625F;
            this.txtValData3.Width = 1.5625F;
            // 
            // txtValData4
            // 
            this.txtValData4.CanGrow = false;
            this.txtValData4.Height = 0.1875F;
            this.txtValData4.Left = 6F;
            this.txtValData4.MultiLine = false;
            this.txtValData4.Name = "txtValData4";
            this.txtValData4.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData4.Tag = "textbox";
            this.txtValData4.Text = null;
            this.txtValData4.Top = 0.75F;
            this.txtValData4.Width = 1.5625F;
            // 
            // txtValData5
            // 
            this.txtValData5.CanGrow = false;
            this.txtValData5.Height = 0.1875F;
            this.txtValData5.Left = 6F;
            this.txtValData5.MultiLine = false;
            this.txtValData5.Name = "txtValData5";
            this.txtValData5.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData5.Tag = "textbox";
            this.txtValData5.Text = null;
            this.txtValData5.Top = 0.9375F;
            this.txtValData5.Width = 1.5625F;
            // 
            // txtValData6
            // 
            this.txtValData6.CanGrow = false;
            this.txtValData6.Height = 0.1875F;
            this.txtValData6.Left = 6F;
            this.txtValData6.MultiLine = false;
            this.txtValData6.Name = "txtValData6";
            this.txtValData6.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData6.Tag = "textbox";
            this.txtValData6.Text = null;
            this.txtValData6.Top = 1.125F;
            this.txtValData6.Width = 1.5625F;
            // 
            // txtValData7
            // 
            this.txtValData7.CanGrow = false;
            this.txtValData7.Height = 0.1875F;
            this.txtValData7.Left = 6F;
            this.txtValData7.MultiLine = false;
            this.txtValData7.Name = "txtValData7";
            this.txtValData7.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData7.Tag = "textbox";
            this.txtValData7.Text = null;
            this.txtValData7.Top = 1.3125F;
            this.txtValData7.Width = 1.5625F;
            // 
            // txtValData8
            // 
            this.txtValData8.CanGrow = false;
            this.txtValData8.Height = 0.1875F;
            this.txtValData8.Left = 6F;
            this.txtValData8.MultiLine = false;
            this.txtValData8.Name = "txtValData8";
            this.txtValData8.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData8.Tag = "textbox";
            this.txtValData8.Text = null;
            this.txtValData8.Top = 1.5F;
            this.txtValData8.Width = 1.5625F;
            // 
            // txtValData9
            // 
            this.txtValData9.CanGrow = false;
            this.txtValData9.Height = 0.1875F;
            this.txtValData9.Left = 6F;
            this.txtValData9.MultiLine = false;
            this.txtValData9.Name = "txtValData9";
            this.txtValData9.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData9.Tag = "textbox";
            this.txtValData9.Text = null;
            this.txtValData9.Top = 1.6875F;
            this.txtValData9.Width = 1.5625F;
            // 
            // txtValData10
            // 
            this.txtValData10.CanGrow = false;
            this.txtValData10.Height = 0.1875F;
            this.txtValData10.Left = 6F;
            this.txtValData10.MultiLine = false;
            this.txtValData10.Name = "txtValData10";
            this.txtValData10.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData10.Tag = "textbox";
            this.txtValData10.Text = null;
            this.txtValData10.Top = 1.875F;
            this.txtValData10.Width = 1.5625F;
            // 
            // txtValData11
            // 
            this.txtValData11.CanGrow = false;
            this.txtValData11.Height = 0.1875F;
            this.txtValData11.Left = 6F;
            this.txtValData11.MultiLine = false;
            this.txtValData11.Name = "txtValData11";
            this.txtValData11.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData11.Tag = "textbox";
            this.txtValData11.Text = null;
            this.txtValData11.Top = 2F;
            this.txtValData11.Width = 1.5625F;
            // 
            // txtValData12
            // 
            this.txtValData12.CanGrow = false;
            this.txtValData12.Height = 0.1875F;
            this.txtValData12.Left = 6F;
            this.txtValData12.MultiLine = false;
            this.txtValData12.Name = "txtValData12";
            this.txtValData12.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData12.Tag = "textbox";
            this.txtValData12.Text = null;
            this.txtValData12.Top = 2.1875F;
            this.txtValData12.Width = 1.5625F;
            // 
            // txtValData13
            // 
            this.txtValData13.CanGrow = false;
            this.txtValData13.Height = 0.1875F;
            this.txtValData13.Left = 6F;
            this.txtValData13.MultiLine = false;
            this.txtValData13.Name = "txtValData13";
            this.txtValData13.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData13.Tag = "textbox";
            this.txtValData13.Text = null;
            this.txtValData13.Top = 2.375F;
            this.txtValData13.Width = 1.5625F;
            // 
            // txtValLbl14
            // 
            this.txtValLbl14.CanGrow = false;
            this.txtValLbl14.Height = 0.1875F;
            this.txtValLbl14.Left = 4.5F;
            this.txtValLbl14.MultiLine = false;
            this.txtValLbl14.Name = "txtValLbl14";
            this.txtValLbl14.Style = "font-family: \'Courier New\'";
            this.txtValLbl14.Tag = "textbox";
            this.txtValLbl14.Text = null;
            this.txtValLbl14.Top = 2.5625F;
            this.txtValLbl14.Width = 1.5625F;
            // 
            // txtValData14
            // 
            this.txtValData14.CanGrow = false;
            this.txtValData14.Height = 0.1875F;
            this.txtValData14.Left = 6F;
            this.txtValData14.MultiLine = false;
            this.txtValData14.Name = "txtValData14";
            this.txtValData14.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData14.Tag = "textbox";
            this.txtValData14.Text = null;
            this.txtValData14.Top = 2.5625F;
            this.txtValData14.Width = 1.5625F;
            // 
            // txtValLbl15
            // 
            this.txtValLbl15.CanGrow = false;
            this.txtValLbl15.Height = 0.1875F;
            this.txtValLbl15.Left = 4.5F;
            this.txtValLbl15.MultiLine = false;
            this.txtValLbl15.Name = "txtValLbl15";
            this.txtValLbl15.Style = "font-family: \'Courier New\'";
            this.txtValLbl15.Tag = "textbox";
            this.txtValLbl15.Text = null;
            this.txtValLbl15.Top = 2.75F;
            this.txtValLbl15.Width = 1.5625F;
            // 
            // txtValData15
            // 
            this.txtValData15.CanGrow = false;
            this.txtValData15.Height = 0.1875F;
            this.txtValData15.Left = 6F;
            this.txtValData15.MultiLine = false;
            this.txtValData15.Name = "txtValData15";
            this.txtValData15.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData15.Tag = "textbox";
            this.txtValData15.Text = null;
            this.txtValData15.Top = 2.75F;
            this.txtValData15.Width = 1.5625F;
            // 
            // txtValLbl16
            // 
            this.txtValLbl16.CanGrow = false;
            this.txtValLbl16.Height = 0.1875F;
            this.txtValLbl16.Left = 4.5F;
            this.txtValLbl16.MultiLine = false;
            this.txtValLbl16.Name = "txtValLbl16";
            this.txtValLbl16.Style = "font-family: \'Courier New\'";
            this.txtValLbl16.Tag = "textbox";
            this.txtValLbl16.Text = null;
            this.txtValLbl16.Top = 2.9375F;
            this.txtValLbl16.Width = 1.5625F;
            // 
            // txtValData16
            // 
            this.txtValData16.CanGrow = false;
            this.txtValData16.Height = 0.1875F;
            this.txtValData16.Left = 6F;
            this.txtValData16.MultiLine = false;
            this.txtValData16.Name = "txtValData16";
            this.txtValData16.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData16.Tag = "textbox";
            this.txtValData16.Text = null;
            this.txtValData16.Top = 2.9375F;
            this.txtValData16.Width = 1.5625F;
            // 
            // txtValLbl17
            // 
            this.txtValLbl17.CanGrow = false;
            this.txtValLbl17.Height = 0.1875F;
            this.txtValLbl17.Left = 4.5F;
            this.txtValLbl17.MultiLine = false;
            this.txtValLbl17.Name = "txtValLbl17";
            this.txtValLbl17.Style = "font-family: \'Courier New\'";
            this.txtValLbl17.Tag = "textbox";
            this.txtValLbl17.Text = null;
            this.txtValLbl17.Top = 3.125F;
            this.txtValLbl17.Width = 1.5625F;
            // 
            // txtValData17
            // 
            this.txtValData17.CanGrow = false;
            this.txtValData17.Height = 0.1875F;
            this.txtValData17.Left = 6F;
            this.txtValData17.MultiLine = false;
            this.txtValData17.Name = "txtValData17";
            this.txtValData17.Style = "font-family: \'Courier New\'; text-align: right";
            this.txtValData17.Tag = "textbox";
            this.txtValData17.Text = null;
            this.txtValData17.Top = 3.125F;
            this.txtValData17.Width = 1.5625F;
            // 
            // valuationLine2
            // 
            this.valuationLine2.Height = 0F;
            this.valuationLine2.Left = 4.5F;
            this.valuationLine2.LineWeight = 1F;
            this.valuationLine2.Name = "valuationLine2";
            this.valuationLine2.Tag = "Line";
            this.valuationLine2.Top = 3.125F;
            this.valuationLine2.Visible = false;
            this.valuationLine2.Width = 3.125F;
            this.valuationLine2.X1 = 4.5F;
            this.valuationLine2.X2 = 7.625F;
            this.valuationLine2.Y1 = 3.125F;
            this.valuationLine2.Y2 = 3.125F;
            // 
            // shapeValuation
            // 
            this.shapeValuation.Height = 3.25F;
            this.shapeValuation.Left = 4.5F;
            this.shapeValuation.Name = "shapeValuation";
            this.shapeValuation.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.shapeValuation.Top = 0.0625F;
            this.shapeValuation.Visible = false;
            this.shapeValuation.Width = 3.125F;
            // 
            // txtMailing5
            // 
            this.txtMailing5.CanGrow = false;
            this.txtMailing5.Height = 0.19F;
            this.txtMailing5.Left = 0.5625F;
            this.txtMailing5.MultiLine = false;
            this.txtMailing5.Name = "txtMailing5";
            this.txtMailing5.Style = "font-family: \'Courier New\'";
            this.txtMailing5.Tag = "textbox";
            this.txtMailing5.Text = null;
            this.txtMailing5.Top = 2.71875F;
            this.txtMailing5.Width = 3.625F;
            // 
            // txtMailing6
            // 
            this.txtMailing6.CanGrow = false;
            this.txtMailing6.Height = 0.1875F;
            this.txtMailing6.Left = 0.5625F;
            this.txtMailing6.MultiLine = false;
            this.txtMailing6.Name = "txtMailing6";
            this.txtMailing6.Style = "font-family: \'Courier New\'";
            this.txtMailing6.Tag = "textbox";
            this.txtMailing6.Text = null;
            this.txtMailing6.Top = 2.875F;
            this.txtMailing6.Width = 3.625F;
            // 
            // txtStub11
            // 
            this.txtStub11.Height = 0.28125F;
            this.txtStub11.Left = 4.313F;
            this.txtStub11.Name = "txtStub11";
            this.txtStub11.Style = "font-family: Courier New; font-size: 10.2pt; text-align: center; vertical-align: " +
    "middle; ddo-char-set: 0";
            this.txtStub11.Text = null;
            this.txtStub11.Top = 9.78125F;
            this.txtStub11.Width = 1.04F;
            // 
            // txtStub12
            // 
            this.txtStub12.Height = 0.28125F;
            this.txtStub12.Left = 5.353F;
            this.txtStub12.Name = "txtStub12";
            this.txtStub12.Style = "font-family: Courier New; font-size: 10.2pt; text-align: center; vertical-align: " +
    "middle; ddo-char-set: 0";
            this.txtStub12.Text = null;
            this.txtStub12.Top = 9.781F;
            this.txtStub12.Width = 1.04F;
            // 
            // txtStub13
            // 
            this.txtStub13.Height = 0.28125F;
            this.txtStub13.Left = 6.393F;
            this.txtStub13.Name = "txtStub13";
            this.txtStub13.Style = "font-family: Courier New; font-size: 10.2pt; text-align: center; vertical-align: " +
    "middle; ddo-char-set: 0";
            this.txtStub13.Text = null;
            this.txtStub13.Top = 9.781F;
            this.txtStub13.Width = 1.04F;
            // 
            // txtStub21
            // 
            this.txtStub21.Height = 0.28125F;
            this.txtStub21.Left = 4.3125F;
            this.txtStub21.Name = "txtStub21";
            this.txtStub21.Style = "font-family: Courier New; font-size: 10.2pt; text-align: center; vertical-align: " +
    "middle; ddo-char-set: 0";
            this.txtStub21.Text = null;
            this.txtStub21.Top = 8.28125F;
            this.txtStub21.Width = 1.04F;
            // 
            // txtStub22
            // 
            this.txtStub22.Height = 0.28125F;
            this.txtStub22.Left = 5.353F;
            this.txtStub22.Name = "txtStub22";
            this.txtStub22.Style = "font-family: Courier New; font-size: 10.2pt; text-align: center; vertical-align: " +
    "middle; ddo-char-set: 0";
            this.txtStub22.Text = null;
            this.txtStub22.Top = 8.28125F;
            this.txtStub22.Width = 1.04F;
            // 
            // txtStub23
            // 
            this.txtStub23.Height = 0.28125F;
            this.txtStub23.Left = 6.393F;
            this.txtStub23.Name = "txtStub23";
            this.txtStub23.Style = "font-family: Courier New; font-size: 10.2pt; text-align: center; vertical-align: " +
    "middle; ddo-char-set: 0";
            this.txtStub23.Text = null;
            this.txtStub23.Top = 8.28125F;
            this.txtStub23.Width = 1.04F;
            // 
            // txtStubInfo111
            // 
            this.txtStubInfo111.Height = 0.18F;
            this.txtStubInfo111.Left = 0.125F;
            this.txtStubInfo111.Name = "txtStubInfo111";
            this.txtStubInfo111.Style = "font-family: Courier New; font-size: 10.2pt; ddo-char-set: 0";
            this.txtStubInfo111.Text = null;
            this.txtStubInfo111.Top = 9.625F;
            this.txtStubInfo111.Width = 0.9800001F;
            // 
            // txtStubInfo112
            // 
            this.txtStubInfo112.Height = 0.18F;
            this.txtStubInfo112.Left = 1.105F;
            this.txtStubInfo112.Name = "txtStubInfo112";
            this.txtStubInfo112.Style = "font-family: Courier New; font-size: 10.2pt; white-space: nowrap; ddo-char-set: 0" +
    "; ddo-wrap-mode: nowrap";
            this.txtStubInfo112.Text = null;
            this.txtStubInfo112.Top = 9.625F;
            this.txtStubInfo112.Width = 2.88F;
            // 
            // txtStubInfo121
            // 
            this.txtStubInfo121.Height = 0.18F;
            this.txtStubInfo121.Left = 0.125F;
            this.txtStubInfo121.Name = "txtStubInfo121";
            this.txtStubInfo121.Style = "font-family: Courier New; font-size: 10.2pt; ddo-char-set: 0";
            this.txtStubInfo121.Text = null;
            this.txtStubInfo121.Top = 9.805F;
            this.txtStubInfo121.Width = 0.9800001F;
            // 
            // txtStubInfo122
            // 
            this.txtStubInfo122.Height = 0.18F;
            this.txtStubInfo122.Left = 1.105F;
            this.txtStubInfo122.Name = "txtStubInfo122";
            this.txtStubInfo122.Style = "font-family: Courier New; font-size: 10.2pt; white-space: nowrap; ddo-char-set: 0" +
    "; ddo-wrap-mode: nowrap";
            this.txtStubInfo122.Text = null;
            this.txtStubInfo122.Top = 9.805F;
            this.txtStubInfo122.Width = 2.88F;
            // 
            // txtStubInfo131
            // 
            this.txtStubInfo131.Height = 0.18F;
            this.txtStubInfo131.Left = 0.125F;
            this.txtStubInfo131.Name = "txtStubInfo131";
            this.txtStubInfo131.Style = "font-family: Courier New; font-size: 10.2pt; ddo-char-set: 0";
            this.txtStubInfo131.Text = null;
            this.txtStubInfo131.Top = 10.007F;
            this.txtStubInfo131.Width = 0.9800001F;
            // 
            // txtStubInfo132
            // 
            this.txtStubInfo132.Height = 0.18F;
            this.txtStubInfo132.Left = 1.105F;
            this.txtStubInfo132.Name = "txtStubInfo132";
            this.txtStubInfo132.Style = "font-family: Courier New; font-size: 10.2pt; white-space: nowrap; ddo-char-set: 0" +
    "; ddo-wrap-mode: nowrap";
            this.txtStubInfo132.Text = null;
            this.txtStubInfo132.Top = 10.007F;
            this.txtStubInfo132.Width = 2.88F;
            // 
            // txtStubInfo141
            // 
            this.txtStubInfo141.Height = 0.18F;
            this.txtStubInfo141.Left = 0.125F;
            this.txtStubInfo141.Name = "txtStubInfo141";
            this.txtStubInfo141.Style = "font-family: Courier New; font-size: 10.2pt; ddo-char-set: 0";
            this.txtStubInfo141.Text = null;
            this.txtStubInfo141.Top = 10.226F;
            this.txtStubInfo141.Width = 0.9800001F;
            // 
            // txtStubInfo142
            // 
            this.txtStubInfo142.Height = 0.18F;
            this.txtStubInfo142.Left = 1.105F;
            this.txtStubInfo142.Name = "txtStubInfo142";
            this.txtStubInfo142.Style = "font-family: Courier New; font-size: 10.2pt; white-space: nowrap; ddo-char-set: 0" +
    "; ddo-wrap-mode: nowrap";
            this.txtStubInfo142.Text = null;
            this.txtStubInfo142.Top = 10.226F;
            this.txtStubInfo142.Width = 2.88F;
            // 
            // txtStubInfo211
            // 
            this.txtStubInfo211.Height = 0.18F;
            this.txtStubInfo211.Left = 0.125F;
            this.txtStubInfo211.Name = "txtStubInfo211";
            this.txtStubInfo211.Style = "font-family: Courier New; font-size: 10.2pt; ddo-char-set: 0";
            this.txtStubInfo211.Text = null;
            this.txtStubInfo211.Top = 8.125F;
            this.txtStubInfo211.Visible = false;
            this.txtStubInfo211.Width = 0.98F;
            // 
            // txtStubInfo212
            // 
            this.txtStubInfo212.Height = 0.18F;
            this.txtStubInfo212.Left = 1.105F;
            this.txtStubInfo212.Name = "txtStubInfo212";
            this.txtStubInfo212.Style = "font-family: Courier New; font-size: 10.2pt; white-space: nowrap; ddo-char-set: 0" +
    "; ddo-wrap-mode: nowrap";
            this.txtStubInfo212.Text = null;
            this.txtStubInfo212.Top = 8.125F;
            this.txtStubInfo212.Visible = false;
            this.txtStubInfo212.Width = 2.88F;
            // 
            // txtStubInfo221
            // 
            this.txtStubInfo221.Height = 0.18F;
            this.txtStubInfo221.Left = 0.125F;
            this.txtStubInfo221.Name = "txtStubInfo221";
            this.txtStubInfo221.Style = "font-family: Courier New; font-size: 10.2pt; ddo-char-set: 0";
            this.txtStubInfo221.Text = null;
            this.txtStubInfo221.Top = 8.305F;
            this.txtStubInfo221.Visible = false;
            this.txtStubInfo221.Width = 0.98F;
            // 
            // txtStubInfo222
            // 
            this.txtStubInfo222.Height = 0.18F;
            this.txtStubInfo222.Left = 1.105F;
            this.txtStubInfo222.Name = "txtStubInfo222";
            this.txtStubInfo222.Style = "font-family: Courier New; font-size: 10.2pt; white-space: nowrap; ddo-char-set: 0" +
    "; ddo-wrap-mode: nowrap";
            this.txtStubInfo222.Text = null;
            this.txtStubInfo222.Top = 8.305F;
            this.txtStubInfo222.Visible = false;
            this.txtStubInfo222.Width = 2.88F;
            // 
            // txtStubInfo231
            // 
            this.txtStubInfo231.Height = 0.18F;
            this.txtStubInfo231.Left = 0.125F;
            this.txtStubInfo231.Name = "txtStubInfo231";
            this.txtStubInfo231.Style = "font-family: Courier New; font-size: 10.2pt; ddo-char-set: 0";
            this.txtStubInfo231.Text = null;
            this.txtStubInfo231.Top = 8.507F;
            this.txtStubInfo231.Visible = false;
            this.txtStubInfo231.Width = 0.98F;
            // 
            // txtStubInfo232
            // 
            this.txtStubInfo232.Height = 0.18F;
            this.txtStubInfo232.Left = 1.105F;
            this.txtStubInfo232.Name = "txtStubInfo232";
            this.txtStubInfo232.Style = "font-family: Courier New; font-size: 10.2pt; white-space: nowrap; ddo-char-set: 0" +
    "; ddo-wrap-mode: nowrap";
            this.txtStubInfo232.Text = null;
            this.txtStubInfo232.Top = 8.507F;
            this.txtStubInfo232.Visible = false;
            this.txtStubInfo232.Width = 2.88F;
            // 
            // txtStubInfo241
            // 
            this.txtStubInfo241.Height = 0.18F;
            this.txtStubInfo241.Left = 0.125F;
            this.txtStubInfo241.Name = "txtStubInfo241";
            this.txtStubInfo241.Style = "font-family: Courier New; font-size: 10.2pt; ddo-char-set: 0";
            this.txtStubInfo241.Text = null;
            this.txtStubInfo241.Top = 8.687F;
            this.txtStubInfo241.Visible = false;
            this.txtStubInfo241.Width = 0.98F;
            // 
            // txtStubInfo242
            // 
            this.txtStubInfo242.Height = 0.18F;
            this.txtStubInfo242.Left = 1.105F;
            this.txtStubInfo242.Name = "txtStubInfo242";
            this.txtStubInfo242.Style = "font-family: Courier New; font-size: 10.2pt; white-space: nowrap; ddo-char-set: 0" +
    "; ddo-wrap-mode: nowrap";
            this.txtStubInfo242.Text = null;
            this.txtStubInfo242.Top = 8.687F;
            this.txtStubInfo242.Visible = false;
            this.txtStubInfo242.Width = 2.88F;
            // 
            // rptBillFormatI
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.71875F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLotLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecondPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountMsg1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountMsg2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxBillYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillYearStub1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillYearStub2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInfoLbl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLBL3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistLbl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistAm6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistPerc6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemitLbl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVallbl0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValLbl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValData17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailing6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStub23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo211)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo212)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo221)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo222)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo231)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo232)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo241)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStubInfo242)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscountDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPageLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLotLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		//private GrapeCity.ActiveReports.SectionReportModel.CustomControl CustomGridStub1;
		//private GrapeCity.ActiveReports.SectionReportModel.CustomControl CustomGridStub2;
		//private GrapeCity.ActiveReports.SectionReportModel.CustomControl CustomGridStubInfo1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirstPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscountMsg1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscountAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscountMsg2;
		//private GrapeCity.ActiveReports.SectionReportModel.CustomControl CustomGridStubInfo2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemit2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemit1;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxBillYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccountAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBillYearStub1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBillYearStub2;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shapeInformation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl0;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoLbl12;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shapeDistribution;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shapeRemittance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistLbl0;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistLbl7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistLbl1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistLbl2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistLBL3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistLbl4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistLbl5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistLbl6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAm7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAm1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAm4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAm5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAm6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPer2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemitLbl0;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemitLbl1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemitLbl2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemitLbl3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemitLbl4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemitLbl5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemitLbl6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemitLbl7;
		private GrapeCity.ActiveReports.SectionReportModel.Line ValuationLine1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVallbl0;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValLbl17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValData17;
		private GrapeCity.ActiveReports.SectionReportModel.Line valuationLine2;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shapeValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing6;
		//FC:FINAL:CHN - issue #1443: Fix showing report. Change Grid type.
		// public FCReportGrid GridStub1;
		// public FCReportGrid GridStub2;
		// public FCReportGrid GridStubInfo1;
		// public FCReportGrid GridStubInfo2;
		//public FCGrid GridStub1;
		//public FCGrid GridStub2;
		//public FCGrid GridStubInfo1;
		//public FCGrid GridStubInfo2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStub11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStub12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStub13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStub21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStub22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStub23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo111;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo112;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo121;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo122;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo131;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo132;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo141;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo142;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo211;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo212;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo221;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo222;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo231;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo232;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo241;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStubInfo242;
	}
}
