﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmListings.
	/// </summary>
	partial class frmListings : BaseForm
	{
		public fecherFoundation.FCComboBox cmbBind;
		public fecherFoundation.FCLabel lblBind;
		public fecherFoundation.FCComboBox cmbOptions;
		public fecherFoundation.FCLabel lblOptions;
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbtPreviousOwner;
		public fecherFoundation.FCLabel lbltPreviousOwner;
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCLabel lblReport;
		public fecherFoundation.FCFrame framCommitOptions;
		public fecherFoundation.FCCheckBox chkNewPage;
		public fecherFoundation.FCFrame framTaxYear;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblStartPage;
		public fecherFoundation.FCFrame framOptions;
		public fecherFoundation.FCCheckBox chkAddress;
		public fecherFoundation.FCCheckBox chkOptions;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame framMortgageHolder;
		public fecherFoundation.FCTextBox txtMortStart;
		public fecherFoundation.FCTextBox txtMortEnd;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtPOEnd;
		public fecherFoundation.FCTextBox txtPOStart;
		public fecherFoundation.FCLabel lblPOEnd;
		public fecherFoundation.FCLabel lblPOStart;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuRun;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmListings));
            this.cmbBind = new fecherFoundation.FCComboBox();
            this.lblBind = new fecherFoundation.FCLabel();
            this.cmbOptions = new fecherFoundation.FCComboBox();
            this.lblOptions = new fecherFoundation.FCLabel();
            this.cmbPrint = new fecherFoundation.FCComboBox();
            this.lblPrint = new fecherFoundation.FCLabel();
            this.cmbOrder = new fecherFoundation.FCComboBox();
            this.lblOrder = new fecherFoundation.FCLabel();
            this.cmbtPreviousOwner = new fecherFoundation.FCComboBox();
            this.lbltPreviousOwner = new fecherFoundation.FCLabel();
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.lblReport = new fecherFoundation.FCLabel();
            this.framCommitOptions = new fecherFoundation.FCFrame();
            this.chkNewPage = new fecherFoundation.FCCheckBox();
            this.framTaxYear = new fecherFoundation.FCFrame();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.txtStart = new fecherFoundation.FCTextBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblStartPage = new fecherFoundation.FCLabel();
            this.framOptions = new fecherFoundation.FCFrame();
            this.chkAddress = new fecherFoundation.FCCheckBox();
            this.chkOptions = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.framMortgageHolder = new fecherFoundation.FCFrame();
            this.txtMortStart = new fecherFoundation.FCTextBox();
            this.txtMortEnd = new fecherFoundation.FCTextBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtPOEnd = new fecherFoundation.FCTextBox();
            this.txtPOStart = new fecherFoundation.FCTextBox();
            this.lblPOEnd = new fecherFoundation.FCLabel();
            this.lblPOStart = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRun = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdRun = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framCommitOptions)).BeginInit();
            this.framCommitOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framTaxYear)).BeginInit();
            this.framTaxYear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framOptions)).BeginInit();
            this.framOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framMortgageHolder)).BeginInit();
            this.framMortgageHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRun)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdRun);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(868, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framCommitOptions);
            this.ClientArea.Controls.Add(this.framTaxYear);
            this.ClientArea.Controls.Add(this.framOptions);
            this.ClientArea.Controls.Add(this.cmbPrint);
            this.ClientArea.Controls.Add(this.lblPrint);
            this.ClientArea.Controls.Add(this.cmbOrder);
            this.ClientArea.Controls.Add(this.lblOrder);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(868, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(868, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(98, 30);
            this.HeaderText.Text = "Reports";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbBind
            // 
            this.cmbBind.Items.AddRange(new object[] {
            "Bind on Left",
            "Bind on Top",
            "Don\'t Adjust for Binding"});
            this.cmbBind.Location = new System.Drawing.Point(90, 77);
            this.cmbBind.Name = "cmbBind";
            this.cmbBind.Size = new System.Drawing.Size(238, 40);
            this.cmbBind.TabIndex = 2;
            this.cmbBind.Text = "Don\'t Adjust for Binding";
            this.ToolTip1.SetToolTip(this.cmbBind, null);
            this.cmbBind.Visible = false;
            // 
            // lblBind
            // 
            this.lblBind.AutoSize = true;
            this.lblBind.Location = new System.Drawing.Point(20, 91);
            this.lblBind.Name = "lblBind";
            this.lblBind.Size = new System.Drawing.Size(37, 15);
            this.lblBind.TabIndex = 1;
            this.lblBind.Text = "BIND";
            this.ToolTip1.SetToolTip(this.lblBind, null);
            this.lblBind.Visible = false;
            // 
            // cmbOptions
            // 
            this.cmbOptions.Items.AddRange(new object[] {
            "Show Just Total and Tax",
            "Show Land/Building/Exempt"});
            this.cmbOptions.Location = new System.Drawing.Point(140, 30);
            this.cmbOptions.Name = "cmbOptions";
            this.cmbOptions.Size = new System.Drawing.Size(264, 40);
            this.cmbOptions.TabIndex = 1;
            this.cmbOptions.Text = "Show Land/Building/Exempt";
            this.ToolTip1.SetToolTip(this.cmbOptions, null);
            // 
            // lblOptions
            // 
            this.lblOptions.AutoSize = true;
            this.lblOptions.Location = new System.Drawing.Point(20, 44);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(63, 15);
            this.lblOptions.TabIndex = 4;
            this.lblOptions.Text = "OPTIONS";
            this.ToolTip1.SetToolTip(this.lblOptions, null);
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "8.5 x 11 Portrait",
            "8.5 x 11 Landscape",
            "Wide Printer"});
            this.cmbPrint.Location = new System.Drawing.Point(503, 320);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(216, 40);
            this.cmbPrint.TabIndex = 3;
            this.cmbPrint.Text = "8.5 x 11 Portrait";
            this.ToolTip1.SetToolTip(this.cmbPrint, null);
            this.cmbPrint.SelectedIndexChanged += new System.EventHandler(this.cmbPrint_SelectedIndexChanged);
            // 
            // lblPrint
            // 
            this.lblPrint.AutoSize = true;
            this.lblPrint.Location = new System.Drawing.Point(314, 334);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Size = new System.Drawing.Size(142, 15);
            this.lblPrint.TabIndex = 2;
            this.lblPrint.Text = "PAPER SIZE / FORMAT";
            this.ToolTip1.SetToolTip(this.lblPrint, null);
            // 
            // cmbOrder
            // 
            this.cmbOrder.Items.AddRange(new object[] {
            "Account",
            "Name",
            "Map / Lot"});
            this.cmbOrder.Location = new System.Drawing.Point(160, 30);
            this.cmbOrder.Name = "cmbOrder";
            this.cmbOrder.Size = new System.Drawing.Size(243, 40);
            this.cmbOrder.TabIndex = 1;
            this.cmbOrder.Text = "Name";
            this.ToolTip1.SetToolTip(this.cmbOrder, null);
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(30, 44);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(97, 15);
            this.lblOrder.TabIndex = 8;
            this.lblOrder.Text = "SEQUENCE BY";
            this.ToolTip1.SetToolTip(this.lblOrder, null);
            // 
            // cmbtPreviousOwner
            // 
            this.cmbtPreviousOwner.Items.AddRange(new object[] {
            "Account",
            "Sale Date"});
            this.cmbtPreviousOwner.Location = new System.Drawing.Point(143, 0);
            this.cmbtPreviousOwner.Name = "cmbtPreviousOwner";
            this.cmbtPreviousOwner.Size = new System.Drawing.Size(353, 40);
            this.cmbtPreviousOwner.TabIndex = 1;
            this.cmbtPreviousOwner.Text = "Account";
            this.ToolTip1.SetToolTip(this.cmbtPreviousOwner, null);
            this.cmbtPreviousOwner.SelectedIndexChanged += new System.EventHandler(this.cmbtPreviousOwner_SelectedIndexChanged);
            // 
            // lbltPreviousOwner
            // 
            this.lbltPreviousOwner.AutoSize = true;
            this.lbltPreviousOwner.Location = new System.Drawing.Point(15, 14);
            this.lbltPreviousOwner.Name = "lbltPreviousOwner";
            this.lbltPreviousOwner.Size = new System.Drawing.Size(122, 15);
            this.lbltPreviousOwner.TabIndex = 6;
            this.lbltPreviousOwner.Text = "PREVIOUS OWNER";
            this.ToolTip1.SetToolTip(this.lbltPreviousOwner, null);
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Real Estate Commitment Book",
            "Personal Property Commitment Book",
            "Real Estate Collection List",
            "Personal Property Collection List",
            "Real Estate Accounts Not Billed",
            "Personal Property Accounts Not Billed",
            "Real Estate Transfer Report",
            "Personal Property Transfer Report",
            "Real Estate Previous Owners",
            "Custom Report",
            "Certificate of Assessment, Commitment etc.",
            "Mortgage Holder List",
            "Real Estate Accounts by Mortgage Holder",
            "Real Estate List with Mortgage Holders",
            "Real Estate & Personal Property Association",
            "Real Estate Out-Print Report",
            "Personal Property Out-Print Report"});
            this.cmbReport.Location = new System.Drawing.Point(128, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(421, 40);
            this.cmbReport.TabIndex = 1;
            this.cmbReport.Text = "Real Estate Collection List";
            this.ToolTip1.SetToolTip(this.cmbReport, null);
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.cmbReport_SelectedIndexChanged);
            // 
            // lblReport
            // 
            this.lblReport.AutoSize = true;
            this.lblReport.Location = new System.Drawing.Point(20, 44);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(59, 15);
            this.lblReport.TabIndex = 3;
            this.lblReport.Text = "REPORT";
            this.ToolTip1.SetToolTip(this.lblReport, null);
            // 
            // framCommitOptions
            // 
            this.framCommitOptions.Controls.Add(this.chkNewPage);
            this.framCommitOptions.Controls.Add(this.cmbBind);
            this.framCommitOptions.Controls.Add(this.lblBind);
            this.framCommitOptions.Location = new System.Drawing.Point(30, 440);
            this.framCommitOptions.Name = "framCommitOptions";
            this.framCommitOptions.Size = new System.Drawing.Size(344, 174);
            this.framCommitOptions.TabIndex = 7;
            this.framCommitOptions.Text = "Options";
            this.ToolTip1.SetToolTip(this.framCommitOptions, null);
            this.framCommitOptions.Visible = false;
            // 
            // chkNewPage
            // 
            this.chkNewPage.Location = new System.Drawing.Point(20, 30);
            this.chkNewPage.Name = "chkNewPage";
            this.chkNewPage.Size = new System.Drawing.Size(270, 27);
            this.chkNewPage.Text = "New Page on New Starting Letter";
            this.ToolTip1.SetToolTip(this.chkNewPage, null);
            this.chkNewPage.Visible = false;
            // 
            // framTaxYear
            // 
            this.framTaxYear.AppearanceKey = "groupBoxNoBorders";
            this.framTaxYear.Controls.Add(this.txtYear);
            this.framTaxYear.Controls.Add(this.txtStart);
            this.framTaxYear.Controls.Add(this.Label4);
            this.framTaxYear.Controls.Add(this.lblStartPage);
            this.framTaxYear.Location = new System.Drawing.Point(30, 320);
            this.framTaxYear.Name = "framTaxYear";
            this.framTaxYear.Size = new System.Drawing.Size(255, 100);
            this.framTaxYear.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.framTaxYear, null);
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.Location = new System.Drawing.Point(127, 0);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(128, 40);
            this.txtYear.TabIndex = 1;
            this.txtYear.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtYear, null);
            // 
            // txtStart
            // 
            this.txtStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtStart.Location = new System.Drawing.Point(127, 60);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(128, 40);
            this.txtStart.TabIndex = 3;
            this.txtStart.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtStart, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(0, 14);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(84, 18);
            this.Label4.TabIndex = 4;
            this.Label4.Text = "TAX YEAR";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // lblStartPage
            // 
            this.lblStartPage.Location = new System.Drawing.Point(0, 74);
            this.lblStartPage.Name = "lblStartPage";
            this.lblStartPage.Size = new System.Drawing.Size(73, 19);
            this.lblStartPage.TabIndex = 2;
            this.lblStartPage.Text = "DISCOUNT";
            this.ToolTip1.SetToolTip(this.lblStartPage, null);
            // 
            // framOptions
            // 
            this.framOptions.Controls.Add(this.chkAddress);
            this.framOptions.Controls.Add(this.cmbOptions);
            this.framOptions.Controls.Add(this.lblOptions);
            this.framOptions.Controls.Add(this.chkOptions);
            this.framOptions.Location = new System.Drawing.Point(30, 440);
            this.framOptions.Name = "framOptions";
            this.framOptions.Size = new System.Drawing.Size(412, 174);
            this.framOptions.TabIndex = 6;
            this.framOptions.Text = "Options";
            this.ToolTip1.SetToolTip(this.framOptions, null);
            // 
            // chkAddress
            // 
            this.chkAddress.Location = new System.Drawing.Point(20, 137);
            this.chkAddress.Name = "chkAddress";
            this.chkAddress.Size = new System.Drawing.Size(133, 27);
            this.chkAddress.TabIndex = 3;
            this.chkAddress.Text = "Show Address";
            this.ToolTip1.SetToolTip(this.chkAddress, null);
            // 
            // chkOptions
            // 
            this.chkOptions.Location = new System.Drawing.Point(20, 90);
            this.chkOptions.Name = "chkOptions";
            this.chkOptions.Size = new System.Drawing.Size(137, 27);
            this.chkOptions.TabIndex = 2;
            this.chkOptions.Text = "Show Discount";
            this.ToolTip1.SetToolTip(this.chkOptions, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.framMortgageHolder);
            this.Frame1.Controls.Add(this.cmbReport);
            this.Frame1.Controls.Add(this.lblReport);
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Location = new System.Drawing.Point(30, 90);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(801, 210);
            this.Frame1.TabIndex = 4;
            this.Frame1.Text = "Report";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // framMortgageHolder
            // 
            this.framMortgageHolder.AppearanceKey = "groupBoxNoBorders";
            this.framMortgageHolder.Controls.Add(this.txtMortStart);
            this.framMortgageHolder.Controls.Add(this.txtMortEnd);
            this.framMortgageHolder.Controls.Add(this.Label5);
            this.framMortgageHolder.Controls.Add(this.Label3);
            this.framMortgageHolder.Location = new System.Drawing.Point(20, 90);
            this.framMortgageHolder.Name = "framMortgageHolder";
            this.framMortgageHolder.Size = new System.Drawing.Size(235, 100);
            this.framMortgageHolder.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.framMortgageHolder, null);
            this.framMortgageHolder.Visible = false;
            // 
            // txtMortStart
            // 
            this.txtMortStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtMortStart.Location = new System.Drawing.Point(107, 0);
            this.txtMortStart.Name = "txtMortStart";
            this.txtMortStart.Size = new System.Drawing.Size(128, 40);
            this.txtMortStart.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtMortStart, null);
            // 
            // txtMortEnd
            // 
            this.txtMortEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtMortEnd.Location = new System.Drawing.Point(107, 60);
            this.txtMortEnd.Name = "txtMortEnd";
            this.txtMortEnd.Size = new System.Drawing.Size(128, 40);
            this.txtMortEnd.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtMortEnd, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(0, 14);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(48, 18);
            this.Label5.TabIndex = 4;
            this.Label5.Text = "START";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(0, 74);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(48, 18);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "END";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.txtPOEnd);
            this.Frame2.Controls.Add(this.cmbtPreviousOwner);
            this.Frame2.Controls.Add(this.lbltPreviousOwner);
            this.Frame2.Controls.Add(this.txtPOStart);
            this.Frame2.Controls.Add(this.lblPOEnd);
            this.Frame2.Controls.Add(this.lblPOStart);
            this.Frame2.Location = new System.Drawing.Point(285, 90);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(496, 100);
            this.Frame2.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.Frame2, null);
            this.Frame2.Visible = false;
            // 
            // txtPOEnd
            // 
            this.txtPOEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtPOEnd.Location = new System.Drawing.Point(368, 60);
            this.txtPOEnd.Name = "txtPOEnd";
            this.txtPOEnd.Size = new System.Drawing.Size(128, 40);
            this.txtPOEnd.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtPOEnd, null);
            this.txtPOEnd.Visible = false;
            // 
            // txtPOStart
            // 
            this.txtPOStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtPOStart.Location = new System.Drawing.Point(143, 60);
            this.txtPOStart.Name = "txtPOStart";
            this.txtPOStart.Size = new System.Drawing.Size(128, 40);
            this.txtPOStart.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtPOStart, null);
            // 
            // lblPOEnd
            // 
            this.lblPOEnd.Anchor = Wisej.Web.AnchorStyles.Top;
            this.lblPOEnd.Location = new System.Drawing.Point(325, 74);
            this.lblPOEnd.Name = "lblPOEnd";
            this.lblPOEnd.Size = new System.Drawing.Size(40, 18);
            this.lblPOEnd.TabIndex = 4;
            this.lblPOEnd.Text = "END";
            this.ToolTip1.SetToolTip(this.lblPOEnd, null);
            this.lblPOEnd.Visible = false;
            // 
            // lblPOStart
            // 
            this.lblPOStart.Location = new System.Drawing.Point(89, 74);
            this.lblPOStart.Name = "lblPOStart";
            this.lblPOStart.Size = new System.Drawing.Size(48, 18);
            this.lblPOStart.TabIndex = 2;
            this.lblPOStart.Text = "START";
            this.ToolTip1.SetToolTip(this.lblPOStart, null);
            this.lblPOStart.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRun,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuRun
            // 
            this.mnuRun.Index = 0;
            this.mnuRun.Name = "mnuRun";
            this.mnuRun.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuRun.Text = "Print Preview";
            this.mnuRun.Click += new System.EventHandler(this.mnuRun_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdRun
            // 
            this.cmdRun.AppearanceKey = "acceptButton";
            this.cmdRun.Location = new System.Drawing.Point(362, 30);
            this.cmdRun.Name = "cmdRun";
            this.cmdRun.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdRun.Size = new System.Drawing.Size(131, 48);
            this.cmdRun.Text = "Print Preview";
            this.cmdRun.Click += new System.EventHandler(this.mnuRun_Click);
            // 
            // frmListings
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(868, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmListings";
            this.Text = "Reports";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmListings_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmListings_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framCommitOptions)).EndInit();
            this.framCommitOptions.ResumeLayout(false);
            this.framCommitOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framTaxYear)).EndInit();
            this.framTaxYear.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.framOptions)).EndInit();
            this.framOptions.ResumeLayout(false);
            this.framOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framMortgageHolder)).EndInit();
            this.framMortgageHolder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRun)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdRun;
	}
}
