﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	public class modCollectionsRelated
	{
		//=========================================================
		public struct IntPaymentRecord
		{
			public int Account;
			public int Year;
			public int BillKey;
			// vbPorter upgrade warning: ActualSystemDate As DateTime	OnWrite(string)
			public DateTime ActualSystemDate;
			public DateTime EffectiveInterestDate;
			public DateTime RecordedTransactionDate;
			public string Teller;
			public string Period;
			public double Principal;
			public string BillCode;
			// vbPorter upgrade warning: CurrentInterest As Decimal	OnWrite(short, double)
			public Decimal CurrentInterest;
			// vbPorter upgrade warning: PreLienInterest As Decimal	OnWriteFCConvert.ToInt16(
			public Decimal PreLienInterest;
			public string PaidBy;
			public string Comments;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public IntPaymentRecord(int unusedParam)
			{
				this.Account = 0;
				this.Year = 0;
				this.BillKey = 0;
				this.ActualSystemDate = DateTime.FromOADate(0);
				this.EffectiveInterestDate = DateTime.FromOADate(0);
				this.RecordedTransactionDate = DateTime.FromOADate(0);
				this.Teller = string.Empty;
				this.Period = string.Empty;
				this.Principal = 0;
				this.BillCode = string.Empty;
				this.CurrentInterest = 0;
				this.PreLienInterest = 0;
				this.PaidBy = string.Empty;
				this.Comments = string.Empty;
			}
		};

		public static double CalculateAccountTotalPrincipal_2(int lngAccountNumber, bool boolREAccount)
		{
			return CalculateAccountTotalPrincipal(ref lngAccountNumber, ref boolREAccount);
		}

		public static double CalculateAccountTotalPrincipal(ref int lngAccountNumber, ref bool boolREAccount)
		{
			double CalculateAccountTotalPrincipal = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the total remaining balance and
				// return it as a double for the account and type passed in
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				string strSQL = "";
				double dblTotal = 0;
				double dblXtraInt = 0;
				double dblTotalInt;
				double dblCurPrin = 0;
				double dblTotPrin;
				if (boolREAccount)
				{
					strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'RE'";
				}
				else
				{
					strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'PP'";
				}
				rsLien.OpenRecordset("SELECT * FROM LienRec", modGlobalVariables.strCLDatabase);
				dblTotPrin = 0;
				rsCL.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
				dblTotalInt = 0;
				if (!rsCL.EndOfFile())
				{
					while (!rsCL.EndOfFile())
					{
						// calculate each year
						if (FCConvert.ToInt32(rsCL.Get_Fields_Int32("LienRecordNumber")) == 0)
						{
							// non-lien
							dblTotal += modCLCalculations.CalculateAccountCL3(ref rsCL, lngAccountNumber, DateTime.Today, ref dblXtraInt, ref dblCurPrin);
							dblTotPrin += dblCurPrin;
						}
						else
						{
							// lien
							if (rsLien.FindFirstRecord("ID", rsCL.Get_Fields_Int32("LienrecordNumber")))
							{
								dblTotal += modCLCalculations.CalculateAccountCLLien3(rsLien, DateTime.Today, ref dblXtraInt, ref dblCurPrin);
								dblTotPrin += dblCurPrin;
							}
						}
						rsCL.MoveNext();
					}
				}
				else
				{
					// nothing found
					dblTotal = 0;
				}
				CalculateAccountTotalPrincipal = dblTotPrin;
				return CalculateAccountTotalPrincipal;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CalculateAccountTotalPrincipal = 0;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Calculate Account Total Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateAccountTotalPrincipal;
		}

		public static bool CreatePrePaymentInterestRecord_6(IntPaymentRecord IRec, bool boolUpdateBillRec = true)
		{
			return CreatePrePaymentInterestRecord(ref IRec, boolUpdateBillRec);
		}

		public static bool CreatePrePaymentInterestRecord(ref IntPaymentRecord IRec, bool boolUpdateBillRec = true)
		{
			bool CreatePrePaymentInterestRecord = false;
			// this adds a record to the database that offsets the charged interest line and
			// adjusts the PaidThroughDate
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngR;
			clsDRWrapper rsCreatePy = new clsDRWrapper();
			try
			{
				// On Error GoTo END_OF_PAYMENT
				// this will save one payment record to the database
				rsCreatePy.OpenRecordset("select * from PaymentRec where id = -1", modGlobalVariables.strCLDatabase);
				rsCreatePy.AddNew();
				// .ID = rsCreatePy.Fields("ID")
				rsCreatePy.Set_Fields("Account", IRec.Account);
				rsCreatePy.Set_Fields("Year", IRec.Year);
				rsCreatePy.Set_Fields("Reversal", false);
				rsCreatePy.Set_Fields("SetEIDate", IRec.EffectiveInterestDate);
				rsCreatePy.Set_Fields("BillKey", IRec.BillKey);
				rsCreatePy.Set_Fields("ReceiptNumber", 0);
				// rsTemp.OpenRecordset "SELECT * FROM BillingMaster WHERE Billkey = " & .BillKey, strCLDATABASE
				rsCreatePy.Set_Fields("CHGINTNumber", 0);
				rsCreatePy.Set_Fields("CHGINTDate", 0);
				rsCreatePy.Set_Fields("ActualSystemDate", IRec.ActualSystemDate);
				rsCreatePy.Set_Fields("EffectiveInterestDate", IRec.EffectiveInterestDate);
				rsCreatePy.Set_Fields("RecordedTransactionDate", IRec.RecordedTransactionDate);
				rsCreatePy.Set_Fields("Teller", IRec.Teller);
				rsCreatePy.Set_Fields("Reference", "EARNINT");
				rsCreatePy.Set_Fields("Period", IRec.Period);
				rsCreatePy.Set_Fields("Code", "P");
				rsCreatePy.Set_Fields("Principal", IRec.Principal);
				rsCreatePy.Set_Fields("PreLienInterest", IRec.PreLienInterest);
				rsCreatePy.Set_Fields("CurrentInterest", IRec.CurrentInterest);
				rsCreatePy.Set_Fields("LienCost", 0);
				rsCreatePy.Set_Fields("TransNumber", 0);
				rsCreatePy.Set_Fields("PaidBy", IRec.PaidBy);
				rsCreatePy.Set_Fields("Comments", IRec.Comments);
				rsCreatePy.Set_Fields("CashDrawer", "Y");
				rsCreatePy.Set_Fields("GeneralLedger", "Y");
				// rsCreatePy.Fields("BudgetaryAccountNumber") = .BudgetaryAccountNumber
				rsCreatePy.Set_Fields("BillCode", IRec.BillCode);
				rsCreatePy.Set_Fields("dailycloseout", 1);
				rsCreatePy.Update();
				if (boolUpdateBillRec)
				{
					rsTemp.OpenRecordset("SELECT * FROM billingmaster WHERE ID = " + FCConvert.ToString(IRec.BillKey), modGlobalVariables.strCLDatabase);
					if (!rsTemp.EndOfFile())
					{
						rsTemp.Edit();
						rsTemp.Set_Fields("PrincipalPaid", rsTemp.Get_Fields_Decimal("PrincipalPaid") + FCConvert.ToDecimal(IRec.Principal));
						rsTemp.Set_Fields("DemandFeesPaid", 0);
						// rsTemp.Fields("PrincipalPaid") = 0
						rsTemp.Update();
					}
				}
				CreatePrePaymentInterestRecord = true;
				rsTemp.Reset();
				return CreatePrePaymentInterestRecord;
			}
			catch (Exception ex)
			{
				// END_OF_PAYMENT:
				MessageBox.Show("Account " + FCConvert.ToString(IRec.Account) + " has had an error while attempting to apply interest to a prepayment.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				rsTemp.Reset();
			}
			return CreatePrePaymentInterestRecord;
		}
		// vbPorter upgrade warning: intPeriods As short	OnWriteFCConvert.ToInt32(
		public static double AutoAbatementAmount_8(int lngBillKey, short intPeriods, ref double dblAbate1, ref double dblAbate2, ref double dblAbate3, ref double dblAbate4)
		{
			return AutoAbatementAmount(ref lngBillKey, ref intPeriods, ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4);
		}

		public static double AutoAbatementAmount(ref int lngBillKey, ref short intPeriods, ref double dblAbate1, ref double dblAbate2, ref double dblAbate3, ref double dblAbate4)
		{
			double AutoAbatementAmount = 0;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				double dblReturn;
				double[] dblAbate = new double[4 + 1];
				// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
				double dblTemp = 0;
				double dblTemp2 = 0;
				dblReturn = 0;
				dblAbate[1] = 0;
				dblAbate[2] = 0;
				dblAbate[3] = 0;
				dblAbate[4] = 0;
				dblAbate1 = 0;
				dblAbate2 = 0;
				dblAbate3 = 0;
				dblAbate4 = 0;
				AutoAbatementAmount = 0;
				// Call rsLoad.OpenRecordset("select sum(principal) as totAmount from paymentrec where period = 'A' and code = 'A' and billkey = " & lngBillKey, strCLDatabase)
				// If Not rsLoad.EndOfFile Then
				// dblReturn = Val(rsLoad.Fields("totamount"))
				// End If
				rsLoad.OpenRecordset("select sum(principal) as totAmount,period from paymentrec where period <> '1' and code = 'A' and billkey = " + FCConvert.ToString(lngBillKey) + " group by period", modGlobalVariables.strCLDatabase);
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [period] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsLoad.Get_Fields("period")) == "A")
					{
						// TODO Get_Fields: Field [totamount] not found!! (maybe it is an alias?)
						dblTemp = FCConvert.ToDouble(Strings.Format(Conversion.Val(rsLoad.Get_Fields("totamount")) / intPeriods, "0.00"));
						// TODO Get_Fields: Field [totamount] not found!! (maybe it is an alias?)
						dblTemp2 = Conversion.Val(rsLoad.Get_Fields("totamount"));
						dblAbate[1] = dblTemp;
						dblTemp2 -= dblAbate[1];
						if (intPeriods >= 2)
						{
							dblAbate[2] = dblTemp;
							dblTemp2 -= dblAbate[2];
							if (dblTemp2 < 0)
							{
								dblAbate[2] += dblTemp2;
								dblTemp2 = 0;
							}
							if (intPeriods >= 3)
							{
								dblAbate[3] = dblTemp;
								dblTemp2 -= dblAbate[3];
								if (dblTemp2 < 0)
								{
									dblAbate[3] += dblTemp2;
									dblTemp2 = 0;
								}
								if (intPeriods >= 4)
								{
									dblAbate[4] = dblTemp2;
								}
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [period] and replace with corresponding Get_Field method
						// TODO Get_Fields: Field [totamount] not found!! (maybe it is an alias?)
						dblAbate[rsLoad.Get_Fields("period")] = Conversion.Val(rsLoad.Get_Fields("totamount"));
					}
					dblAbate1 += dblAbate[1];
					dblAbate2 += dblAbate[2];
					dblAbate3 += dblAbate[3];
					dblAbate4 += dblAbate[4];
					rsLoad.MoveNext();
				}
				dblReturn = dblAbate[1] + dblAbate[2] + dblAbate[3] + dblAbate[4];
				AutoAbatementAmount = dblReturn;
				return AutoAbatementAmount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In AutoAbatementAmount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AutoAbatementAmount;
		}

		public class StaticVariables
		{
			public string gstrLastYearBilled = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
