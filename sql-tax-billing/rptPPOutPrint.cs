﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPOutPrint.
	/// </summary>
	public partial class rptPPOutPrint : BaseSectionReport
	{
		public rptPPOutPrint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Personal Property Out-Print Report";
		}

		public static rptPPOutPrint InstancePtr
		{
			get
			{
				return (rptPPOutPrint)Sys.GetInstance(typeof(rptPPOutPrint));
			}
		}

		protected rptPPOutPrint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPOutPrint	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		//clsDRWrapper clsOutPrint = new clsDRWrapper();
		//[] DistArray = new double[5 + 1];
		//clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		//FileSystemObject fso = new FileSystemObject();
		StreamReader txtStream;
		modTypes.OutPrintingFormat OPF = new modTypes.OutPrintingFormat(0);
		modTypes.OutPrintingEnhancedFormat OPFEnh = new modTypes.OutPrintingEnhancedFormat(0);
		bool boolCommaDelimited;
		int lngRecordCount;
		/// <summary>
		/// keeps track of how many records have been printed
		/// </summary>
		double lngTotalValue;
		double lngTotalExempt;
		double dblTotalTax;
		bool boolUsingEnhanced;

		private bool IsFileEnded_2(short intFile)
		{
			return IsFileEnded(ref intFile);
		}

		private bool IsFileEnded(ref short intFile)
		{
			bool IsFileEnded = false;
			IsFileEnded = FCFileSystem.EOF(intFile);
			return IsFileEnded;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// EOF = clsOutPrint.EndOfFile
			if (boolCommaDelimited)
			{
				eArgs.EOF = txtStream.EndOfStream;
			}
			else
			{
				eArgs.EOF = IsFileEnded_2(42);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCFileSystem.FileClose();
            //FC:FINAL:MSH - issue #1628: close StreamReader after finishing work with file
            if(txtStream != null)
            {
                txtStream.Close();
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			int const_printtoolid;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngTotalValue = 0;
			lngTotalExempt = 0;
			dblTotalTax = 0;
			lngRecordCount = 0;
			lblMuniname.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			txtDate.Text = DateTime.Today.ToShortDateString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			// override the print button so the print dialog doesn't come up again
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	// do this since we already chose the printer and don't want to choose again
		//	// now we have to handle printing the pages ourselves though.
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "Print...")
		//	{
		//		clsPrint.ReportPrint(this);
		//		// Call frmNumPages.Init(Me.Pages.Count, 1)
		//		// frmNumPages.Show vbModal, MDIParent
		//		// Me.Printer.FromPage = gintStartPage
		//		// Me.Printer.ToPage = gintEndPage
		//		// Me.PrintReport (False)
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			string strData = "";
			string[] strDataArray = null;
			int lngTemp = 0;
			int x;
			if (!boolCommaDelimited)
			{
				if (!FCFileSystem.EOF(42))
				{
					lngRecordCount += 1;
					if (boolUsingEnhanced)
					{
						GetEnhancedFixed();
					}
					else
					{
						GetRegularFixed();
					}
				}
				else
				{
					txtValue.Text = "0";
					txtExempt.Text = "0";
					txtTax.Text = "0";
				}
			}
			else
			{
				if (!txtStream.EndOfStream)
				{
					strData = txtStream.ReadLine();
					// one bill record
					lngRecordCount += 1;
					// the next two lines will allow us to split the comma delimted record without splitting on commas in text
					strData = Strings.Replace(strData, FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)), "^", 1, -1, CompareConstants.vbTextCompare);
					strData = Strings.Replace(strData, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
					strDataArray = Strings.Split(strData, "^", -1, CompareConstants.vbTextCompare);
					txtAccount.Text = FCConvert.ToString(Conversion.Val(strDataArray[1]));
					txtName.Text = Strings.Trim(strDataArray[4]);
					txtLocation.Text = Strings.Trim(strDataArray[2]);
					txtValue.Text = Strings.Trim(strDataArray[23]);
					txtExempt.Text = Strings.Trim(strDataArray[24]);
					txtTax.Text = Strings.Trim(strDataArray[26]);
					txtPeriod1.Text = Strings.Trim(strDataArray[27]);
					txtPeriod2.Text = Strings.Trim(strDataArray[28]);
					txtPeriod3.Text = Strings.Trim(strDataArray[29]);
					txtPeriod4.Text = Strings.Trim(strDataArray[30]);
					txtDist1.Text = Strings.Trim(strDataArray[34]);
					txtDist2.Text = Strings.Trim(strDataArray[35]);
					txtDist3.Text = Strings.Trim(strDataArray[36]);
					txtDist4.Text = Strings.Trim(strDataArray[37]);
					txtDist5.Text = Strings.Trim(strDataArray[38]);
					txtCat1.Text = Strings.Trim(strDataArray[14]);
					txtCat2.Text = Strings.Trim(strDataArray[15]);
					txtCat3.Text = Strings.Trim(strDataArray[16]);
					// txtCat49.Text = Trim(strDataArray(17))
					lngTemp = 0;
					for (x = 4; x <= 9; x++)
					{
						if (Strings.Trim(strDataArray[13 + x]) != string.Empty)
						{
							lngTemp += FCConvert.ToInt32(FCConvert.ToDouble(strDataArray[13 + x]));
						}
					}
					// x
					txtCat49.Text = Strings.Format(lngTemp, "#,###,##0");
				}
			}
			//FC:FINAL:CHN - issue #1383: Change C# converting on methods as at VB6 version.
			if (txtValue.Text != string.Empty)
			{
				// lngTotalValue += FCConvert.ToInt32(txtValue.Text);
				lngTotalValue += Conversion.CLng(txtValue.Text);
			}
			if (txtExempt.Text != string.Empty)
			{
				// lngTotalExempt += FCConvert.ToInt32(txtExempt.Text);
				lngTotalExempt += Conversion.CLng(txtExempt.Text);
			}
			if (txtTax.Text != string.Empty)
			{
				// dblTotalTax += FCConvert.ToDouble(txtTax.Text);
				dblTotalTax += Conversion.CDbl(txtTax.Text);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = lngRecordCount.ToString();
			txtTotalValue.Text = Strings.Format(lngTotalValue, "#,###,###,##0");
			txtTotalExempt.Text = Strings.Format(lngTotalExempt, "#,###,###,##0");
			txtTotalAssessment.Text = Strings.Format(lngTotalValue - lngTotalExempt, "#,###,###,##0");
			txtTotalTax.Text = Strings.Format(dblTotalTax, "#,###,###,##0.00");
		}
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intYear As short	OnWriteFCConvert.ToInt32(
		public void Init(int intOrder, int intYear, bool boolSetFont, string strFontName, string strPrinterName)
		{
			int x;
			string strSQL = "";
			string strOrder = "";
			string strTemp = "";
			if (boolSetFont)
			{
				// now set each box and label to the correct printer font
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl fld in this.GetAllControls())
				{
					bool setFont = false;
					bool bold = false;
					if (FCConvert.ToString(fld.Tag) == "textbox")
					{
						setFont = true;
					}
					else if (FCConvert.ToString(fld.Tag) == "bold")
					{
						setFont = true;
						bold = true;
					}
					if (setFont)
					{
						GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = fld as GrapeCity.ActiveReports.SectionReportModel.TextBox;
						if (textBox != null)
						{
							textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
						}
						else
						{
							GrapeCity.ActiveReports.SectionReportModel.Label label = fld as GrapeCity.ActiveReports.SectionReportModel.Label;
							if (label != null)
							{
								label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
						}
					}
				}
				//this.Printer.RenderMode = 1;
			}
			// change to the selected printer
			this.Document.Printer.PrinterName = strPrinterName;
			//FC:FINAL:CHN - issue #1383: Change File class on FCFileSystem to use same paths.
			// if (File.Exists("TSPPBILL.FIL"))
			if (FCFileSystem.FileExists("TSPPBILL.FIL"))
			{
				// txtStream = File.OpenText("TSPPBILL.FIL");
				txtStream = FCFileSystem.OpenText("TSPPBILL.FIL");
				if (txtStream == null)
				{
					MessageBox.Show("Cannot open Personal Property outprinting file", "No file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
				}
				if (txtStream.EndOfStream)
				{
					MessageBox.Show("Cannot open Personal Property outprinting file", "No file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
				}
			}
			else
			{
				MessageBox.Show("Cannot find Personal Property outprinting file", "No file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
				return;
			}
			lngRecordCount = 0;
			for (int i = 0; i < 8; i++)
			{
				strTemp += (char)txtStream.Read();
			}
			if (FCConvert.ToBoolean(Strings.InStr(1, strTemp, ",", CompareConstants.vbTextCompare)))
			{
				boolCommaDelimited = true;
				txtStream.Close();
				//FC:FINAL:CHN - issue #1383: Change File class on FCFileSystem to use same paths.
				// txtStream = File.OpenText("TSPPBILL.FIL");
				txtStream = FCFileSystem.OpenText("TSPPBILL.FIL");
				txtStream.ReadLine();
				// skip header record
			}
			else
			{
				boolCommaDelimited = false;
				txtStream.Close();
				FCFileSystem.FileClose();
				FCFileSystem.FileOpen(42, "TSPPBILL.FIL", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(OPF));
				FCFileSystem.FileGet(42, ref OPF, -1/*? 1 */);
				if (OPF.LineEnd == "\r\n")
				{
					boolUsingEnhanced = false;
				}
				else
				{
					boolUsingEnhanced = true;
					FCFileSystem.FileClose();
					FCFileSystem.FileOpen(42, "TSPPBILL.FIL", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(OPFEnh));
				}
			}
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this, strPrinterName, 0, false, false, "Pages", false, "", "TRIO Software", false, true, "PPOutprint");
			// the distributions are set up
		}

		private void GetRegularFixed()
		{
			FCFileSystem.FileGet(42, ref OPF, lngRecordCount);
			if (FCFileSystem.EOF(42))
			{
				lngRecordCount -= 1;
				Detail.Visible = false;
				return;
			}
			txtAccount.Text = FCConvert.ToString(Conversion.Val(OPF.Account));
			txtName.Text = Strings.Trim(OPF.Name);
			txtLocation.Text = Strings.Trim(OPF.Location);
			txtValue.Text = Strings.Trim(OPF.TotalPP);
			txtExempt.Text = Strings.Trim(OPF.TotalExempt);
			txtTax.Text = Strings.Trim(OPF.TotalTax);
			txtPeriod1.Text = Strings.Trim(OPF.TaxDue1);
			txtPeriod2.Text = Strings.Trim(OPF.TaxDue2);
			txtPeriod3.Text = Strings.Trim(OPF.TaxDue3);
			txtPeriod4.Text = Strings.Trim(OPF.TaxDue4);
			txtDist1.Text = Strings.Trim(OPF.Distribution1);
			txtDist2.Text = Strings.Trim(OPF.Distribution2);
			txtDist3.Text = Strings.Trim(OPF.Distribution3);
			txtDist4.Text = Strings.Trim(OPF.Distribution4);
			txtDist5.Text = Strings.Trim(OPF.Distribution5);
			txtCat1.Text = Strings.Trim(OPF.PPCode1);
			txtCat2.Text = Strings.Trim(OPF.PPCode2);
			txtCat3.Text = Strings.Trim(OPF.PPCode3);
			txtCat49.Text = Strings.Trim(OPF.PPCode5);
		}

		private void GetEnhancedFixed()
		{
			FCFileSystem.FileGet(42, ref OPFEnh, lngRecordCount);
			if (FCFileSystem.EOF(42))
			{
				txtValue.Text = "0";
				txtExempt.Text = "0";
				txtTax.Text = "0";
				lngRecordCount -= 1;
				Detail.Visible = false;
				return;
			}
			txtAccount.Text = FCConvert.ToString(Conversion.Val(OPFEnh.Account));
			txtName.Text = Strings.Trim(OPFEnh.Name);
			txtLocation.Text = Strings.Trim(OPFEnh.Location);
			txtValue.Text = Strings.Trim(OPFEnh.TotalPP);
			txtExempt.Text = Strings.Trim(OPFEnh.TotalExempt);
			txtTax.Text = Strings.Trim(OPFEnh.TotalTax);
			txtPeriod1.Text = Strings.Trim(OPFEnh.TaxDue1);
			txtPeriod2.Text = Strings.Trim(OPFEnh.TaxDue2);
			txtPeriod3.Text = Strings.Trim(OPFEnh.TaxDue3);
			txtPeriod4.Text = Strings.Trim(OPFEnh.TaxDue4);
			txtDist1.Text = Strings.Trim(OPFEnh.Distribution1);
			txtDist2.Text = Strings.Trim(OPFEnh.Distribution2);
			txtDist3.Text = Strings.Trim(OPFEnh.Distribution3);
			txtDist4.Text = Strings.Trim(OPFEnh.Distribution4);
			txtDist5.Text = Strings.Trim(OPFEnh.Distribution5);
			txtCat1.Text = Strings.Trim(OPFEnh.PPCode1);
			txtCat2.Text = Strings.Trim(OPFEnh.PPCode2);
			txtCat3.Text = Strings.Trim(OPFEnh.PPCode3);
			txtCat49.Text = Strings.Trim(OPFEnh.PPCode5);
		}

		private void rptPPOutPrint_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPPOutPrint properties;
			//rptPPOutPrint.Caption	= "Personal Property Out-Print Report";
			//rptPPOutPrint.Icon	= "rptPPOutPrint.dsx":0000";
			//rptPPOutPrint.Left	= 0;
			//rptPPOutPrint.Top	= 0;
			//rptPPOutPrint.Width	= 13395;
			//rptPPOutPrint.Height	= 6225;
			//rptPPOutPrint.StartUpPosition	= 3;
			//rptPPOutPrint.SectionData	= "rptPPOutPrint.dsx":058A;
			//End Unmaped Properties
		}
	}
}
