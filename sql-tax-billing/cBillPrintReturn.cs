﻿//Fecher vbPorter - Version 1.0.0.35
using fecherFoundation;

namespace TWBL0000
{
	public class cBillPrintReturn
	{
		//=========================================================
		private int intPreviewLimit;
		private string strFileName = "";
		private int intType;
		/// <summary>
		/// 0 is print
		/// </summary>
		/// <summary>
		/// 1 is preview
		/// </summary>
		/// <summary>
		/// 2 is pdf
		/// </summary>
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short PreviewLimit
		{
			get
			{
				short PreviewLimit = 0;
				PreviewLimit = FCConvert.ToInt16(intPreviewLimit);
				return PreviewLimit;
			}
			set
			{
				intPreviewLimit = value;
			}
		}

		public string Filename
		{
			get
			{
				string Filename = "";
				Filename = strFileName;
				return Filename;
			}
			set
			{
				strFileName = value;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short OutputType
		{
			get
			{
				short OutputType = 0;
				OutputType = FCConvert.ToInt16(intType);
				return OutputType;
			}
			set
			{
				intType = value;
			}
		}
	}
}
