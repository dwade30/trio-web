﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmRERange.
	/// </summary>
	public partial class frmRERange : BaseForm
	{
		public frmRERange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRERange InstancePtr
		{
			get
			{
				return (frmRERange)Sys.GetInstance(typeof(frmRERange));
			}
		}

		protected frmRERange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public bool boolFromSupplemental;
		private bool isContinued = false;

		private void frmRERange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuContinue_Click();
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuCancel_Click();
			}
		}

		private void frmRERange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRERange properties;
			//frmRERange.ScaleWidth	= 5880;
			//frmRERange.ScaleHeight	= 4245;
			//frmRERange.LinkTopic	= "Form1";
			//frmRERange.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			//lblTo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			modGlobalVariables.Statics.CancelledIt = false;
			isContinued = false;
			if (!boolFromSupplemental)
			{
				chkUseOwnerAsOf.Visible = true;
				t2kUseOwnerAsOf.Visible = true;
				if (DateTime.Today.Month < 4)
				{
					t2kUseOwnerAsOf.Text = "04/01/" + FCConvert.ToString(DateTime.Today.Year - 1);
				}
				else
				{
					t2kUseOwnerAsOf.Text = "04/01/" + FCConvert.ToString(DateTime.Today.Year);
				}
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// MDIParent.Show
			//FC:FINAL:MSH - issue #1313: closing form by using 'X' toolbar button will threw an exception, because CancelledIt won't be changed
			if (!isContinued)
			{
				modGlobalVariables.Statics.CancelledIt = true;
			}
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			Close();
		}

		public void mnuCancel_Click()
		{
			mnuCancel_Click(mnuCancel, new System.EventArgs());
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			int intWhich = 0;
			string strBegin = "";
			string strEnd = "";
			if (!boolFromSupplemental)
			{
				if (chkUseOwnerAsOf.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (!Information.IsDate(t2kUseOwnerAsOf.Text))
					{
						MessageBox.Show("You must enter a valid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						isContinued = false;
						return;
					}
					else
					{
						frmREAudit.InstancePtr.boolUseOwnerAsOf = true;
						frmREAudit.InstancePtr.strAsOfDate = Strings.Format(t2kUseOwnerAsOf.Text, "MM/dd/yyyy");
					}
				}
				if (cmbAccounts.SelectedIndex == 0)
				{
					frmREAudit.InstancePtr.intWhich = -1;
				}
				else
				{
					if (cmbAccounts.SelectedIndex == 2)
					{
						txtEnd.Text = "";
					}
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						if (cmbAccounts.SelectedIndex == 2)
						{
							MessageBox.Show("You must specify a value", "Specify Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							MessageBox.Show("You must specify a start range", "Specify Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						isContinued = false;
						return;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						frmREAudit.InstancePtr.intWhich = 0;
						if (Conversion.Val(txtStart.Text) > 0)
						{
							frmREAudit.InstancePtr.strBegin = txtStart.Text;
							if (Conversion.Val(txtEnd.Text) > 0)
							{
								frmREAudit.InstancePtr.strEnd = txtEnd.Text;
							}
							else
							{
								frmREAudit.InstancePtr.strEnd = txtStart.Text;
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							isContinued = false;
							return;
						}
					}
					else
					{
						if (cmbRange.SelectedIndex == 1)
						{
							frmREAudit.InstancePtr.intWhich = 1;
						}
						else if (cmbRange.SelectedIndex == 2)
						{
							frmREAudit.InstancePtr.intWhich = 2;
						}
						else if (cmbRange.SelectedIndex == 3)
						{
							frmREAudit.InstancePtr.intWhich = 3;
						}
						else if (cmbRange.SelectedIndex == 4)
						{
							frmREAudit.InstancePtr.intWhich = 4;
						}
						else if (cmbRange.SelectedIndex == 5)
						{
							frmREAudit.InstancePtr.intWhich = 5;
						}
						else if (cmbRange.SelectedIndex == 6)
						{
							frmREAudit.InstancePtr.intWhich = 6;
						}
						frmREAudit.InstancePtr.strBegin = txtStart.Text;
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							frmREAudit.InstancePtr.strEnd = txtStart.Text;
						}
						else
						{
							if (cmbRange.SelectedIndex != 1 && cmbRange.SelectedIndex != 3)
							{
								frmREAudit.InstancePtr.strEnd = txtEnd.Text;
							}
							else
							{
								frmREAudit.InstancePtr.strEnd = txtEnd.Text + "zzz";
							}
						}
					}
				}
				//FC:FINAL:MSH - issue #1313: in original form will be closed only in the end of method to avoid missing data
				//Close();
			}
			else
			{
				//FC:FINAL:MSH - issue #1313: in original form will be closed only in the end of method to avoid missing data
				//Close();
				// from supplemental
				if (cmbAccounts.SelectedIndex == 0)
				{
					frmSupplementalBill.InstancePtr.TransferRE(-1);
				}
				else
				{
					if (cmbAccounts.SelectedIndex == 2)
						txtEnd.Text = "";
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						MessageBox.Show("You must specify a start range", "Specify Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						isContinued = false;
						return;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						if (Conversion.Val(txtStart.Text) > 0)
						{
							strBegin = txtStart.Text;
							if (Conversion.Val(txtEnd.Text) > 0)
							{
								strEnd = txtEnd.Text;
							}
							else
							{
								strEnd = txtStart.Text;
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							isContinued = false;
							return;
						}
					}
					else
					{
						if (cmbAccounts.SelectedIndex == 2)
						{
							txtEnd.Text = txtStart.Text;
							strEnd = strBegin;
						}
						else
						{
							if (Strings.Trim(txtEnd.Text) == string.Empty)
							{
								MessageBox.Show("You must specify an end range", "Specify End", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								isContinued = false;
								return;
							}
							strBegin = txtStart.Text;
							if (cmbRange.SelectedIndex != 1 && cmbRange.SelectedIndex != 3)
							{
								strEnd = txtEnd.Text;
							}
							else
							{
								strEnd = txtEnd.Text + "zzz";
							}
						}
					}
					if (cmbRange.SelectedIndex == 0)
					{
						intWhich = 0;
					}
					else if (cmbRange.SelectedIndex == 1)
					{
						intWhich = 1;
					}
					else if (cmbRange.SelectedIndex == 2)
					{
						intWhich = 2;
					}
					else if (cmbRange.SelectedIndex == 3)
					{
						intWhich = 3;
					}
					else if (cmbRange.SelectedIndex == 4)
					{
						intWhich = 4;
					}
					else if (cmbRange.SelectedIndex == 5)
					{
						intWhich = 5;
					}
					else if (cmbRange.SelectedIndex == 6)
					{
						intWhich = 6;
					}
					frmSupplementalBill.InstancePtr.TransferRE(intWhich, strBegin, strEnd);
				}
			}
			//FC:FINAL:MSH - issue #1313: closing form by using 'X' toolbar button will threw an exception, because CancelledIt won't be changed.
			//Proccess will be continued only after successfull work of method
			isContinued = true;
			//FC:FINAL:MSH - issue #1313: in original form will be closed only in the end of method to avoid missing data
			this.Close();
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, new System.EventArgs());
		}

		private void optAccounts_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// all
						txtStart.Visible = false;
						txtEnd.Visible = false;
						lblTo.Visible = false;
						cmbRange.Visible = false;
						lblRange.Visible = false;
						ToolTip1.SetToolTip(txtStart, "");
						break;
					}
				case 1:
					{
						// range
						txtStart.Visible = true;
						txtEnd.Visible = true;
						lblTo.Visible = true;
						cmbRange.Visible = true;
						lblRange.Visible = true;
						lblRange.Text = "Range of";
						ToolTip1.SetToolTip(txtStart, "");
						break;
					}
				case 2:
					{
						// individual
						txtStart.Visible = true;
						txtEnd.Visible = false;
						lblTo.Visible = false;
						cmbRange.Visible = true;
						lblRange.Visible = true;
						lblRange.Text = "Specific";
						ToolTip1.SetToolTip(txtStart, "All choices except for Name may also be entered as a list separated by commas.  Ex: 1,3,5,27 or Main Street,South Rd,First St");
						break;
					}
			}
			//end switch
		}

		private void optAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbAccounts.SelectedIndex);
			optAccounts_CheckedChanged(index, sender, e);
		}

		private void cmbAccounts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAccounts.SelectedIndex == 0)
			{
				optAccounts_CheckedChanged(sender, e);
			}
			else if (cmbAccounts.SelectedIndex == 1)
			{
				optAccounts_CheckedChanged(sender, e);
			}
			else if (cmbAccounts.SelectedIndex == 2)
			{
				optAccounts_CheckedChanged(sender, e);
			}
		}
	}
}
