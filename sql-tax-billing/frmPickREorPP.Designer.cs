﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPickREorPP.
	/// </summary>
	partial class frmPickREorPP : BaseForm
	{
		public fecherFoundation.FCComboBox cmbModule;
		public fecherFoundation.FCLabel lblModule;
		public fecherFoundation.FCComboBox cmbDB;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPickREorPP));
			this.cmbModule = new fecherFoundation.FCComboBox();
			this.lblModule = new fecherFoundation.FCLabel();
			this.cmbDB = new fecherFoundation.FCComboBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 188);
			this.BottomPanel.Size = new System.Drawing.Size(436, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbDB);
			this.ClientArea.Controls.Add(this.cmbModule);
			this.ClientArea.Controls.Add(this.lblModule);
			this.ClientArea.Size = new System.Drawing.Size(436, 128);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(436, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// cmbModule
			// 
			this.cmbModule.AutoSize = false;
			this.cmbModule.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbModule.FormattingEnabled = true;
			this.cmbModule.Items.AddRange(new object[] {
				"Real Estate",
				"Personal Property"
			});
			this.cmbModule.Location = new System.Drawing.Point(146, 30);
			this.cmbModule.Name = "cmbModule";
			this.cmbModule.Size = new System.Drawing.Size(257, 40);
			this.cmbModule.TabIndex = 1;
			this.cmbModule.Text = "Real Estate";
			// 
			// lblModule
			// 
			this.lblModule.AutoSize = true;
			this.lblModule.Location = new System.Drawing.Point(30, 44);
			this.lblModule.Name = "lblModule";
			this.lblModule.Size = new System.Drawing.Size(60, 15);
			this.lblModule.TabIndex = 0;
			this.lblModule.Text = "Tax Files";
			// 
			// cmbDB
			// 
			this.cmbDB.AutoSize = false;
			this.cmbDB.BackColor = System.Drawing.SystemColors.Window;
			this.cmbDB.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDB.FormattingEnabled = true;
			this.cmbDB.Location = new System.Drawing.Point(146, 85);
			this.cmbDB.Name = "cmbDB";
			this.cmbDB.Size = new System.Drawing.Size(184, 40);
			this.cmbDB.TabIndex = 2;
			this.cmbDB.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(173, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(100, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmPickREorPP
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(436, 296);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPickREorPP";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "";
			this.Load += new System.EventHandler(this.frmPickREorPP_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPickREorPP_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
