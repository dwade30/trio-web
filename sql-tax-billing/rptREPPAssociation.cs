﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREPPAssociation.
	/// </summary>
	public partial class rptREPPAssociation : BaseSectionReport
	{
		public static rptREPPAssociation InstancePtr
		{
			get
			{
				return (rptREPPAssociation)Sys.GetInstance(typeof(rptREPPAssociation));
			}
		}

		protected rptREPPAssociation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre.Dispose();
				clsPP.Dispose();
            }
			base.Dispose(disposing);
		}

		public rptREPPAssociation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
			{
				_InstancePtr = this;
			}
			this.Name = "Mortgage Holder List";
		}
		// nObj = 1
		//   0	rptREPPAssociation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsre = new clsDRWrapper();
		clsDRWrapper clsPP = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
		}

		public void Init(bool boolNameOrder)
		{
			string strSQL = "";
			string strOrder = "";
			if (boolNameOrder)
			{
				strOrder = " order by DeedName1 ";
			}
			else
			{
				strOrder = " order by master.rsaccount ";
			}
			clsre.OpenRecordset("select * from master inner join " + clsre.CurrentPrefix + "CentralData.dbo.moduleassociation on (moduleassociation.remasteracct = master.rsaccount) CROSS APPLY " + clsre.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(master.OwnerPartyID) as p where moduleassociation.module = 'PP' and master.rsdeleted <> 1 and master.rscard = 1" + strOrder, "twre0000.vb1");
			clsPP.OpenRecordset("select *, FullNameLF as name from ppmaster inner join " + clsPP.CurrentPrefix + "CentralParties.dbo.PartyNameView on (PPmaster.Partyid = PartyNameView.ID) order by account", "twpp0000.vb1");
			// Me.Show , MDIParent
			//FC:FINAL:MSH - i.issue #1543: restore missing report call
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "REPPAssociation");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtDate.Text = DateTime.Today.ToString();
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsre.EndOfFile())
			{
				txtAccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
				txtName.Text = clsre.Get_Fields_String("DeedName1");
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtPPAccount.Text = clsre.Get_Fields_String("account");
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				clsPP.FindFirstRecord("account", clsre.Get_Fields("account"));
				if (clsPP.NoMatch)
				{
					txtPPName.Text = "Error PP record not found.";
				}
				else
				{
					txtPPName.Text = clsPP.Get_Fields_String("name");
				}
				clsre.MoveNext();
			}
		}

		
	}
}
