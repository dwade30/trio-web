﻿using fecherFoundation;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptMortgageHolderMaster.
	/// </summary>
	partial class rptMortgageHolderMaster
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMortgageHolderMaster));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.textBoxDetail11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBoxDetail12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBoxDetail13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBoxDetail23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBoxDetail33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBoxDetail43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			//FC:FINAL:CHN - issue #1372: Set Detail Height according to original Grid (missing margin after redesign).
			// this.Detail.CanShrink = true;
			this.Detail.CanShrink = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.textBoxDetail11,
				this.textBoxDetail12,
				this.textBoxDetail13,
				this.textBoxDetail23,
				this.textBoxDetail33,
				this.textBoxDetail43
			});
			//FC:FINAL:CHN - issue #1372: Set Detail Height according to original Grid (missing margin after redesign).
			// this.Detail.Height = 0.8166666F;
			this.Detail.Height = 0.6f;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// textBoxDetail11
			// 
			this.textBoxDetail11.Height = 0.2F;
			this.textBoxDetail11.Left = 0.062F;
			this.textBoxDetail11.Name = "textBoxDetail11";
			this.textBoxDetail11.Style = "font-family: Tahoma; font-size: 10.2pt";
			this.textBoxDetail11.Text = null;
			this.textBoxDetail11.Top = 0F;
			this.textBoxDetail11.Width = 0.688F;
			// 
			// textBoxDetail12
			// 
			this.textBoxDetail12.Height = 0.2F;
			this.textBoxDetail12.Left = 0.7500001F;
			this.textBoxDetail12.Name = "textBoxDetail12";
			this.textBoxDetail12.Style = "font-family: Tahoma; font-size: 10.2pt";
			this.textBoxDetail12.Text = null;
			this.textBoxDetail12.Top = 0F;
			this.textBoxDetail12.Width = 3.0F;
			// 
			// textBoxDetail13
			// 
			this.textBoxDetail13.Height = 0.2F;
			this.textBoxDetail13.Left = 3.75F;
			this.textBoxDetail13.Name = "textBoxDetail13";
			this.textBoxDetail13.Style = "font-family: Tahoma; font-size: 10.2pt";
			this.textBoxDetail13.Text = null;
			this.textBoxDetail13.Top = 0F;
			this.textBoxDetail13.Width = 3.428F;
			// 
			// textBoxDetail23
			// 
			this.textBoxDetail23.Height = 0.2F;
			this.textBoxDetail23.Left = 3.75F;
			this.textBoxDetail23.Name = "textBoxDetail23";
			this.textBoxDetail23.Style = "font-family: Tahoma; font-size: 10.2pt";
			this.textBoxDetail23.Text = null;
			this.textBoxDetail23.Top = 0.2F;
			this.textBoxDetail23.Width = 3.428F;
			// 
			// textBoxDetail33
			// 
			this.textBoxDetail33.Height = 0.2F;
			this.textBoxDetail33.Left = 3.75F;
			this.textBoxDetail33.Name = "textBoxDetail33";
			this.textBoxDetail33.Style = "font-family: Tahoma; font-size: 10.2pt";
			this.textBoxDetail33.Text = null;
			this.textBoxDetail33.Top = 0.4F;
			this.textBoxDetail33.Width = 3.428F;
			// 
			// textBoxDetail43
			// 
			this.textBoxDetail43.Height = 0.2F;
			this.textBoxDetail43.Left = 3.75F;
			this.textBoxDetail43.Name = "textBoxDetail43";
			this.textBoxDetail43.Style = "font-family: Tahoma; font-size: 10.2pt";
			this.textBoxDetail43.Text = null;
			this.textBoxDetail43.Top = 0.6F;
			this.textBoxDetail43.Width = 3.428F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniname,
				this.txtDate,
				this.txtTitle,
				this.txtPage,
				this.lblPage,
				this.lblNo,
				this.lblName,
				this.lblAddress,
				this.txtTime
			});
			this.PageHeader.Height = 0.6145833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.19F;
			this.txtMuniname.Left = 0.0625F;
			this.txtMuniname.MultiLine = false;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "font-family: \'Tahoma\'";
			this.txtMuniname.Text = null;
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 1.5F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.375F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.21875F;
			this.txtTitle.Left = 2F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Mortgage Holder Master";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 3.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 7F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 0.4375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.19625F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.15625F;
			this.lblPage.Width = 0.625F;
			// 
			// lblNo
			// 
			this.lblNo.Height = 0.19F;
			this.lblNo.HyperLink = null;
			this.lblNo.Left = 0.1875F;
			this.lblNo.MultiLine = false;
			this.lblNo.Name = "lblNo";
			this.lblNo.Style = "font-weight: bold";
			this.lblNo.Text = "Number";
			this.lblNo.Top = 0.46875F;
			this.lblNo.Width = 0.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.18625F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.75F;
			this.lblName.MultiLine = false;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.45875F;
			this.lblName.Width = 1.1875F;
			// 
			// lblAddress
			// 
			this.lblAddress.Height = 0.18625F;
			this.lblAddress.HyperLink = null;
			this.lblAddress.Left = 3.75F;
			this.lblAddress.MultiLine = false;
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblAddress.Text = "Address";
			this.lblAddress.Top = 0.45875F;
			this.lblAddress.Width = 1.3125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.25F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptMortgageHolderMaster
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.49F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBoxDetail43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.CustomControl DetailCustomGrid;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBoxDetail11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBoxDetail12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBoxDetail13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBoxDetail23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBoxDetail33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBoxDetail43;
	}
}
