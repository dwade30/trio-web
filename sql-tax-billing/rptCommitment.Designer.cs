﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptCommitment.
	/// </summary>
	partial class rptCommitment
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCommitment));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayment1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayment2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayment3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayment4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblSoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoft = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoftValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRef1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRef2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtLandTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBldgTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSubtotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRef1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRef2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubtotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLand,
				this.txtAccount,
				this.txtName,
				this.txtBldg,
				this.txtExempt,
				this.txtTotal,
				this.txtAddress1,
				this.txtAddress2,
				this.txtMailingAddress3,
				this.txtAddress3,
				this.Field10,
				this.txtLocation,
				this.txtBookPage,
				this.txtTax,
				this.txtAcres,
				this.txtExempt1,
				this.txtExempt2,
				this.txtExempt3,
				this.txtPayment1,
				this.txtPayment2,
				this.txtPayment3,
				this.txtPayment4,
				this.Line1,
				this.lblSoft,
				this.lblAcres,
				this.lblMixed,
				this.lblHard,
				this.txtSoft,
				this.txtMixed,
				this.txtHard,
				this.txtSoftValue,
				this.txtMixedValue,
				this.txtHardValue,
				this.txtRef1,
				this.txtRef2,
				this.txtMapLot
			});
			this.Detail.Height = 1.385417F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtLand
			// 
			this.txtLand.DataField = "landval";
			this.txtLand.Height = 0.15625F;
			this.txtLand.Left = 2.375F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.OutputFormat = resources.GetString("txtLand.OutputFormat");
			this.txtLand.Style = "font-size: 8.5pt; text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 0F;
			this.txtLand.Width = 0.875F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.15625F;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 8.5pt; text-align: right";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.5625F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.15625F;
			this.txtName.Left = 0.625F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-size: 8.5pt";
			this.txtName.Tag = "textbox";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 1.6875F;
			// 
			// txtBldg
			// 
			this.txtBldg.DataField = "bldgval";
			this.txtBldg.Height = 0.15625F;
			this.txtBldg.Left = 3.3125F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.OutputFormat = resources.GetString("txtBldg.OutputFormat");
			this.txtBldg.Style = "font-size: 8.5pt; text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0F;
			this.txtBldg.Width = 1.0625F;
			// 
			// txtExempt
			// 
			this.txtExempt.DataField = "exemptval";
			this.txtExempt.Height = 0.15625F;
			this.txtExempt.Left = 4.4375F;
			this.txtExempt.MultiLine = false;
			this.txtExempt.Name = "txtExempt";
			this.txtExempt.OutputFormat = resources.GetString("txtExempt.OutputFormat");
			this.txtExempt.Style = "font-size: 8.5pt; text-align: right";
			this.txtExempt.Tag = "textbox";
			this.txtExempt.Text = null;
			this.txtExempt.Top = 0F;
			this.txtExempt.Width = 1F;
			// 
			// txtTotal
			// 
			this.txtTotal.DataField = "totalval";
			this.txtTotal.Height = 0.15625F;
			this.txtTotal.Left = 5.5F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat");
			this.txtTotal.Style = "font-size: 8.5pt; text-align: right";
			this.txtTotal.Tag = "textbox";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 1.0625F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.15625F;
			this.txtAddress1.Left = 0.625F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-size: 8.5pt";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = null;
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 1.6875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.15625F;
			this.txtAddress2.Left = 0.625F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-size: 8.5pt";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Width = 1.6875F;
			// 
			// txtMailingAddress3
			// 
			this.txtMailingAddress3.Height = 0.15625F;
			this.txtMailingAddress3.Left = 0.625F;
			this.txtMailingAddress3.Name = "txtMailingAddress3";
			this.txtMailingAddress3.Style = "font-size: 8.5pt";
			this.txtMailingAddress3.Tag = "textbox";
			this.txtMailingAddress3.Text = null;
			this.txtMailingAddress3.Top = 0.46875F;
			this.txtMailingAddress3.Width = 1.6875F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.Height = 0.15625F;
			this.txtAddress3.Left = 0.625F;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Style = "font-size: 8.5pt";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = null;
			this.txtAddress3.Top = 0.625F;
			this.txtAddress3.Width = 1.6875F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.15625F;
			this.Field10.Left = 0.625F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-size: 8.5pt";
			this.Field10.Tag = "textbox";
			this.Field10.Text = null;
			this.Field10.Top = 0.78125F;
			this.Field10.Width = 1.6875F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.15625F;
			this.txtLocation.Left = 0.625F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-size: 8.5pt";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.9375F;
			this.txtLocation.Width = 1.6875F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.CanShrink = true;
			this.txtBookPage.Height = 0.125F;
			this.txtBookPage.Left = 0.625F;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Style = "font-size: 8.5pt";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.25F;
			this.txtBookPage.Width = 3.75F;
			// 
			// txtTax
			// 
			this.txtTax.DataField = "taxval";
			this.txtTax.Height = 0.15625F;
			this.txtTax.Left = 6.625F;
			this.txtTax.MultiLine = false;
			this.txtTax.Name = "txtTax";
			this.txtTax.OutputFormat = resources.GetString("txtTax.OutputFormat");
			this.txtTax.Style = "font-size: 8.5pt; text-align: right";
			this.txtTax.Tag = "textbox";
			this.txtTax.Text = null;
			this.txtTax.Top = 0F;
			this.txtTax.Width = 0.875F;
			// 
			// txtAcres
			// 
			this.txtAcres.Height = 0.15625F;
			this.txtAcres.Left = 3F;
			this.txtAcres.MultiLine = false;
			this.txtAcres.Name = "txtAcres";
			this.txtAcres.Style = "font-size: 8.5pt; text-align: right";
			this.txtAcres.Tag = "textbox";
			this.txtAcres.Text = null;
			this.txtAcres.Top = 0.15625F;
			this.txtAcres.Width = 0.625F;
			// 
			// txtExempt1
			// 
			this.txtExempt1.Height = 0.15625F;
			this.txtExempt1.Left = 4.4375F;
			this.txtExempt1.MultiLine = false;
			this.txtExempt1.Name = "txtExempt1";
			this.txtExempt1.Style = "font-size: 8.5pt";
			this.txtExempt1.Tag = "textbox";
			this.txtExempt1.Text = null;
			this.txtExempt1.Top = 0.15625F;
			this.txtExempt1.Width = 2.125F;
			// 
			// txtExempt2
			// 
			this.txtExempt2.Height = 0.15625F;
			this.txtExempt2.Left = 4.4375F;
			this.txtExempt2.Name = "txtExempt2";
			this.txtExempt2.Style = "font-size: 8.5pt";
			this.txtExempt2.Tag = "textbox";
			this.txtExempt2.Text = null;
			this.txtExempt2.Top = 0.3125F;
			this.txtExempt2.Width = 2.125F;
			// 
			// txtExempt3
			// 
			this.txtExempt3.Height = 0.15625F;
			this.txtExempt3.Left = 4.4375F;
			this.txtExempt3.Name = "txtExempt3";
			this.txtExempt3.Style = "font-size: 8.5pt";
			this.txtExempt3.Tag = "textbox";
			this.txtExempt3.Text = null;
			this.txtExempt3.Top = 0.46875F;
			this.txtExempt3.Width = 2.125F;
			// 
			// txtPayment1
			// 
			this.txtPayment1.DataField = "taxval";
			this.txtPayment1.Height = 0.15625F;
			this.txtPayment1.Left = 6.5625F;
			this.txtPayment1.MultiLine = false;
			this.txtPayment1.Name = "txtPayment1";
			this.txtPayment1.OutputFormat = resources.GetString("txtPayment1.OutputFormat");
			this.txtPayment1.Style = "font-size: 8.5pt; text-align: right";
			this.txtPayment1.Tag = "textbox";
			this.txtPayment1.Text = null;
			this.txtPayment1.Top = 0.3125F;
			this.txtPayment1.Width = 0.9375F;
			// 
			// txtPayment2
			// 
			this.txtPayment2.DataField = "taxval";
			this.txtPayment2.Height = 0.15625F;
			this.txtPayment2.Left = 6.5625F;
			this.txtPayment2.MultiLine = false;
			this.txtPayment2.Name = "txtPayment2";
			this.txtPayment2.OutputFormat = resources.GetString("txtPayment2.OutputFormat");
			this.txtPayment2.Style = "font-size: 8.5pt; text-align: right";
			this.txtPayment2.Tag = "textbox";
			this.txtPayment2.Text = null;
			this.txtPayment2.Top = 0.46875F;
			this.txtPayment2.Width = 0.9375F;
			// 
			// txtPayment3
			// 
			this.txtPayment3.DataField = "taxval";
			this.txtPayment3.Height = 0.15625F;
			this.txtPayment3.Left = 6.5625F;
			this.txtPayment3.MultiLine = false;
			this.txtPayment3.Name = "txtPayment3";
			this.txtPayment3.OutputFormat = resources.GetString("txtPayment3.OutputFormat");
			this.txtPayment3.Style = "font-size: 8.5pt; text-align: right";
			this.txtPayment3.Tag = "textbox";
			this.txtPayment3.Text = null;
			this.txtPayment3.Top = 0.625F;
			this.txtPayment3.Width = 0.9375F;
			// 
			// txtPayment4
			// 
			this.txtPayment4.DataField = "taxval";
			this.txtPayment4.Height = 0.15625F;
			this.txtPayment4.Left = 6.5625F;
			this.txtPayment4.MultiLine = false;
			this.txtPayment4.Name = "txtPayment4";
			this.txtPayment4.OutputFormat = resources.GetString("txtPayment4.OutputFormat");
			this.txtPayment4.Style = "font-size: 8.5pt; text-align: right";
			this.txtPayment4.Tag = "textbox";
			this.txtPayment4.Text = null;
			this.txtPayment4.Top = 0.78125F;
			this.txtPayment4.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 7.4375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// lblSoft
			// 
			this.lblSoft.Height = 0.15625F;
			this.lblSoft.Left = 2.375F;
			this.lblSoft.MultiLine = false;
			this.lblSoft.Name = "lblSoft";
			this.lblSoft.Style = "font-size: 8.5pt";
			this.lblSoft.Tag = "textbox";
			this.lblSoft.Text = "Soft:";
			this.lblSoft.Top = 0.3125F;
			this.lblSoft.Visible = false;
			this.lblSoft.Width = 0.625F;
			// 
			// lblAcres
			// 
			this.lblAcres.Height = 0.15625F;
			this.lblAcres.Left = 2.375F;
			this.lblAcres.MultiLine = false;
			this.lblAcres.Name = "lblAcres";
			this.lblAcres.Style = "font-size: 8.5pt";
			this.lblAcres.Tag = "textbox";
			this.lblAcres.Text = "Acres";
			this.lblAcres.Top = 0.15625F;
			this.lblAcres.Visible = false;
			this.lblAcres.Width = 0.625F;
			// 
			// lblMixed
			// 
			this.lblMixed.Height = 0.15625F;
			this.lblMixed.Left = 2.375F;
			this.lblMixed.MultiLine = false;
			this.lblMixed.Name = "lblMixed";
			this.lblMixed.Style = "font-size: 8.5pt";
			this.lblMixed.Tag = "textbox";
			this.lblMixed.Text = "Mixed:";
			this.lblMixed.Top = 0.46875F;
			this.lblMixed.Visible = false;
			this.lblMixed.Width = 0.625F;
			// 
			// lblHard
			// 
			this.lblHard.Height = 0.15625F;
			this.lblHard.Left = 2.375F;
			this.lblHard.MultiLine = false;
			this.lblHard.Name = "lblHard";
			this.lblHard.Style = "font-size: 8.5pt";
			this.lblHard.Tag = "textbox";
			this.lblHard.Text = "Hard:";
			this.lblHard.Top = 0.625F;
			this.lblHard.Visible = false;
			this.lblHard.Width = 0.625F;
			// 
			// txtSoft
			// 
			this.txtSoft.Height = 0.15625F;
			this.txtSoft.Left = 3F;
			this.txtSoft.MultiLine = false;
			this.txtSoft.Name = "txtSoft";
			this.txtSoft.Style = "font-size: 8.5pt; text-align: right";
			this.txtSoft.Tag = "textbox";
			this.txtSoft.Text = null;
			this.txtSoft.Top = 0.3125F;
			this.txtSoft.Visible = false;
			this.txtSoft.Width = 0.625F;
			// 
			// txtMixed
			// 
			this.txtMixed.Height = 0.15625F;
			this.txtMixed.Left = 3F;
			this.txtMixed.MultiLine = false;
			this.txtMixed.Name = "txtMixed";
			this.txtMixed.Style = "font-size: 8.5pt; text-align: right";
			this.txtMixed.Tag = "textbox";
			this.txtMixed.Text = null;
			this.txtMixed.Top = 0.46875F;
			this.txtMixed.Visible = false;
			this.txtMixed.Width = 0.625F;
			// 
			// txtHard
			// 
			this.txtHard.Height = 0.15625F;
			this.txtHard.Left = 3F;
			this.txtHard.MultiLine = false;
			this.txtHard.Name = "txtHard";
			this.txtHard.Style = "font-size: 8.5pt; text-align: right";
			this.txtHard.Tag = "textbox";
			this.txtHard.Text = null;
			this.txtHard.Top = 0.625F;
			this.txtHard.Visible = false;
			this.txtHard.Width = 0.625F;
			// 
			// txtSoftValue
			// 
			this.txtSoftValue.Height = 0.15625F;
			this.txtSoftValue.Left = 3.625F;
			this.txtSoftValue.MultiLine = false;
			this.txtSoftValue.Name = "txtSoftValue";
			this.txtSoftValue.Style = "font-size: 8.5pt; text-align: right";
			this.txtSoftValue.Tag = "textbox";
			this.txtSoftValue.Text = null;
			this.txtSoftValue.Top = 0.3125F;
			this.txtSoftValue.Visible = false;
			this.txtSoftValue.Width = 0.75F;
			// 
			// txtMixedValue
			// 
			this.txtMixedValue.Height = 0.15625F;
			this.txtMixedValue.Left = 3.625F;
			this.txtMixedValue.MultiLine = false;
			this.txtMixedValue.Name = "txtMixedValue";
			this.txtMixedValue.Style = "font-size: 8.5pt; text-align: right";
			this.txtMixedValue.Tag = "textbox";
			this.txtMixedValue.Text = null;
			this.txtMixedValue.Top = 0.46875F;
			this.txtMixedValue.Visible = false;
			this.txtMixedValue.Width = 0.75F;
			// 
			// txtHardValue
			// 
			this.txtHardValue.Height = 0.15625F;
			this.txtHardValue.Left = 3.625F;
			this.txtHardValue.MultiLine = false;
			this.txtHardValue.Name = "txtHardValue";
			this.txtHardValue.Style = "font-size: 8.5pt; text-align: right";
			this.txtHardValue.Tag = "textbox";
			this.txtHardValue.Text = null;
			this.txtHardValue.Top = 0.625F;
			this.txtHardValue.Visible = false;
			this.txtHardValue.Width = 0.75F;
			// 
			// txtRef1
			// 
			this.txtRef1.CanShrink = true;
			this.txtRef1.Height = 0.15625F;
			this.txtRef1.Left = 2.375F;
			this.txtRef1.Name = "txtRef1";
			this.txtRef1.Style = "font-size: 8.5pt";
			this.txtRef1.Tag = "textbox";
			this.txtRef1.Text = null;
			this.txtRef1.Top = 0.78125F;
			this.txtRef1.Width = 3.125F;
			// 
			// txtRef2
			// 
			this.txtRef2.CanShrink = true;
			this.txtRef2.Height = 0.15625F;
			this.txtRef2.Left = 2.375F;
			this.txtRef2.MultiLine = false;
			this.txtRef2.Name = "txtRef2";
			this.txtRef2.Style = "font-size: 8.5pt";
			this.txtRef2.Tag = "textbox";
			this.txtRef2.Text = null;
			this.txtRef2.Top = 0.9375F;
			this.txtRef2.Width = 3.125F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.15625F;
			this.txtMapLot.Left = 0.625F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-size: 8.5pt";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.09375F;
			this.txtMapLot.Width = 1.6875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.Label8,
				this.txtDate,
				this.lblMuniname,
				this.lblTitle,
				this.Label9,
				this.Field11,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.lblDescription,
				this.txtTime
			});
			this.PageHeader.Height = 0.4583333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 6.96875F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-size: 8.5pt; text-align: right";
			this.txtPage.Tag = "textbox";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1666667F;
			this.txtPage.Width = 0.46875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1666667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6.09375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 8.5pt; text-align: right";
			this.Label8.Tag = "textbox";
			this.Label8.Text = "Page";
			this.Label8.Top = 0.1666667F;
			this.Label8.Width = 0.875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.15625F;
			this.txtDate.Left = 6.5F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-size: 8.5pt; text-align: right";
			this.txtDate.Tag = "textbox";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 0.9375F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.15625F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "font-size: 8.5pt; text-align: left";
			this.lblMuniname.Tag = "textbox";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 1.625F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.17F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.625F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Tag = "bold";
			this.lblTitle.Text = "Real Estate Tax Commitment Book - ";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 4.625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.165F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Account";
			this.Label9.Top = 0.3125F;
			this.Label9.Width = 0.625F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.165F;
			this.Field11.Left = 0.625F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-weight: bold";
			this.Field11.Tag = "bold";
			this.Field11.Text = "Name & Address";
			this.Field11.Top = 0.3125F;
			this.Field11.Width = 1.6875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.165F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: right";
			this.Label10.Tag = "bold";
			this.Label10.Text = "Land";
			this.Label10.Top = 0.3125F;
			this.Label10.Width = 0.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.165F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.3125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Tag = "bold";
			this.Label11.Text = "Building";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 1.0625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.165F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.4375F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: right";
			this.Label12.Tag = "bold";
			this.Label12.Text = "Exemption";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 1F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.165F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.5F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Tag = "bold";
			this.Label13.Text = "Assessment";
			this.Label13.Top = 0.3125F;
			this.Label13.Width = 1.0625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.165F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 6.625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Tag = "bold";
			this.Label14.Text = "Tax";
			this.Label14.Top = 0.3125F;
			this.Label14.Width = 0.8125F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.15625F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 2.4375F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.lblDescription.Tag = "bold";
			this.lblDescription.Text = null;
			this.lblDescription.Top = 0.15625F;
			this.lblDescription.Width = 3.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.15625F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-size: 8.5pt; text-align: left";
			this.txtTime.Tag = "textbox";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.4375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLandTot,
				this.Label1,
				this.txtBldgTot,
				this.txtExemptTot,
				this.txtTotTot,
				this.txtTaxTot,
				this.lblSubtotals,
				this.txtLandSub,
				this.txtBldgSub,
				this.txtExemptSub,
				this.txtTotalSub,
				this.txtTaxSub,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Line2
			});
			this.PageFooter.Height = 0.46875F;
			this.PageFooter.Name = "PageFooter";
			this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
			// 
			// txtLandTot
			// 
			this.txtLandTot.DataField = "landval";
			this.txtLandTot.Height = 0.15625F;
			this.txtLandTot.Left = 1.3125F;
			this.txtLandTot.Name = "txtLandTot";
			this.txtLandTot.OutputFormat = resources.GetString("txtLandTot.OutputFormat");
			this.txtLandTot.Style = "font-size: 8.5pt; text-align: right";
			this.txtLandTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtLandTot.Tag = "textbox";
			this.txtLandTot.Text = null;
			this.txtLandTot.Top = 0.15625F;
			this.txtLandTot.Width = 1.125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.15625F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label1.Tag = "bold";
			this.Label1.Text = "Page Totals:";
			this.Label1.Top = 0.15625F;
			this.Label1.Width = 1.25F;
			// 
			// txtBldgTot
			// 
			this.txtBldgTot.DataField = "bldgval";
			this.txtBldgTot.Height = 0.15625F;
			this.txtBldgTot.Left = 2.5F;
			this.txtBldgTot.Name = "txtBldgTot";
			this.txtBldgTot.OutputFormat = resources.GetString("txtBldgTot.OutputFormat");
			this.txtBldgTot.Style = "font-size: 8.5pt; text-align: right";
			this.txtBldgTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtBldgTot.Tag = "textbox";
			this.txtBldgTot.Text = null;
			this.txtBldgTot.Top = 0.15625F;
			this.txtBldgTot.Width = 1.1875F;
			// 
			// txtExemptTot
			// 
			this.txtExemptTot.DataField = "exemptval";
			this.txtExemptTot.Height = 0.15625F;
			this.txtExemptTot.Left = 3.75F;
			this.txtExemptTot.Name = "txtExemptTot";
			this.txtExemptTot.OutputFormat = resources.GetString("txtExemptTot.OutputFormat");
			this.txtExemptTot.Style = "font-size: 8.5pt; text-align: right";
			this.txtExemptTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtExemptTot.Tag = "textbox";
			this.txtExemptTot.Text = null;
			this.txtExemptTot.Top = 0.15625F;
			this.txtExemptTot.Width = 1.1875F;
			// 
			// txtTotTot
			// 
			this.txtTotTot.DataField = "totalval";
			this.txtTotTot.Height = 0.15625F;
			this.txtTotTot.Left = 5F;
			this.txtTotTot.Name = "txtTotTot";
			this.txtTotTot.OutputFormat = resources.GetString("txtTotTot.OutputFormat");
			this.txtTotTot.Style = "font-size: 8.5pt; text-align: right";
			this.txtTotTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTotTot.Tag = "textbox";
			this.txtTotTot.Text = null;
			this.txtTotTot.Top = 0.15625F;
			this.txtTotTot.Width = 1.1875F;
			// 
			// txtTaxTot
			// 
			this.txtTaxTot.DataField = "taxval";
			this.txtTaxTot.Height = 0.15625F;
			this.txtTaxTot.Left = 6.25F;
			this.txtTaxTot.Name = "txtTaxTot";
			this.txtTaxTot.OutputFormat = resources.GetString("txtTaxTot.OutputFormat");
			this.txtTaxTot.Style = "font-size: 8.5pt; text-align: right";
			this.txtTaxTot.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageTotal;
			this.txtTaxTot.Tag = "textbox";
			this.txtTaxTot.Text = null;
			this.txtTaxTot.Top = 0.15625F;
			this.txtTaxTot.Width = 1.25F;
			// 
			// lblSubtotals
			// 
			this.lblSubtotals.Height = 0.15625F;
			this.lblSubtotals.HyperLink = null;
			this.lblSubtotals.Left = 0.0625F;
			this.lblSubtotals.Name = "lblSubtotals";
			this.lblSubtotals.Style = "font-size: 8.5pt; font-weight: bold";
			this.lblSubtotals.Tag = "bold";
			this.lblSubtotals.Text = "Subtotals:";
			this.lblSubtotals.Top = 0.3125F;
			this.lblSubtotals.Width = 1.25F;
			// 
			// txtLandSub
			// 
			this.txtLandSub.Height = 0.15625F;
			this.txtLandSub.Left = 1.3125F;
			this.txtLandSub.MultiLine = false;
			this.txtLandSub.Name = "txtLandSub";
			this.txtLandSub.OutputFormat = resources.GetString("txtLandSub.OutputFormat");
			this.txtLandSub.Style = "font-size: 8.5pt; text-align: right";
			this.txtLandSub.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtLandSub.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtLandSub.Tag = "textbox";
			this.txtLandSub.Text = null;
			this.txtLandSub.Top = 0.3125F;
			this.txtLandSub.Width = 1.125F;
			// 
			// txtBldgSub
			// 
			this.txtBldgSub.Height = 0.15625F;
			this.txtBldgSub.Left = 2.5F;
			this.txtBldgSub.MultiLine = false;
			this.txtBldgSub.Name = "txtBldgSub";
			this.txtBldgSub.OutputFormat = resources.GetString("txtBldgSub.OutputFormat");
			this.txtBldgSub.Style = "font-size: 8.5pt; text-align: right";
			this.txtBldgSub.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtBldgSub.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtBldgSub.Tag = "textbox";
			this.txtBldgSub.Text = null;
			this.txtBldgSub.Top = 0.3125F;
			this.txtBldgSub.Width = 1.1875F;
			// 
			// txtExemptSub
			// 
			this.txtExemptSub.Height = 0.15625F;
			this.txtExemptSub.Left = 3.75F;
			this.txtExemptSub.MultiLine = false;
			this.txtExemptSub.Name = "txtExemptSub";
			this.txtExemptSub.OutputFormat = resources.GetString("txtExemptSub.OutputFormat");
			this.txtExemptSub.Style = "font-size: 8.5pt; text-align: right";
			this.txtExemptSub.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtExemptSub.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtExemptSub.Tag = "textbox";
			this.txtExemptSub.Text = null;
			this.txtExemptSub.Top = 0.3125F;
			this.txtExemptSub.Width = 1.1875F;
			// 
			// txtTotalSub
			// 
			this.txtTotalSub.Height = 0.15625F;
			this.txtTotalSub.Left = 5F;
			this.txtTotalSub.MultiLine = false;
			this.txtTotalSub.Name = "txtTotalSub";
			this.txtTotalSub.OutputFormat = resources.GetString("txtTotalSub.OutputFormat");
			this.txtTotalSub.Style = "font-size: 8.5pt; text-align: right";
			this.txtTotalSub.Tag = "textbox";
			this.txtTotalSub.Text = null;
			this.txtTotalSub.Top = 0.3125F;
			this.txtTotalSub.Width = 1.1875F;
			// 
			// txtTaxSub
			// 
			this.txtTaxSub.Height = 0.15625F;
			this.txtTaxSub.Left = 6.25F;
			this.txtTaxSub.MultiLine = false;
			this.txtTaxSub.Name = "txtTaxSub";
			this.txtTaxSub.OutputFormat = resources.GetString("txtTaxSub.OutputFormat");
			this.txtTaxSub.Style = "font-size: 8.5pt; text-align: right";
			this.txtTaxSub.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtTaxSub.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtTaxSub.Tag = "textbox";
			this.txtTaxSub.Text = null;
			this.txtTaxSub.Top = 0.3125F;
			this.txtTaxSub.Width = 1.25F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.15625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.5625F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label3.Tag = "bold";
			this.Label3.Text = "Land";
			this.Label3.Top = 0F;
			this.Label3.Width = 0.875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.15625F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.5F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label4.Tag = "bold";
			this.Label4.Text = "Building";
			this.Label4.Top = 0F;
			this.Label4.Width = 1.1875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.15625F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.75F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label5.Tag = "bold";
			this.Label5.Text = "Exempt";
			this.Label5.Top = 0F;
			this.Label5.Width = 1.1875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.15625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label6.Tag = "bold";
			this.Label6.Text = "Total";
			this.Label6.Top = 0F;
			this.Label6.Width = 1.1875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.15625F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.25F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label7.Tag = "bold";
			this.Label7.Text = "Tax";
			this.Label7.Top = 0F;
			this.Label7.Width = 1.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 7.4375F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// rptCommitment
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRef1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRef2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubtotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailingAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayment1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayment2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayment3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayment4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblSoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblHard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRef1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRef2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandTot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxTot;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSubtotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSub;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxSub;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
