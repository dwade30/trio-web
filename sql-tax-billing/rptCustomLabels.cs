﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using GrapeCity.ActiveReports.Document.Section;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptCustomLabels.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
		}

		public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		/// <summary>
		/// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		/// </summary>
		/// <summary>
		/// MODCUSTOMREPORT.MOD AND frmCustomLabels.FRM THEN THERE
		/// </summary>
		/// <summary>
		/// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		/// </summary>
		//clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		clsDRWrapper rsData = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(double, short)
		int intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		int intBlankLabelsLeftToPrint;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
		}
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intLabelStartPosition As short	OnWriteFCConvert.ToInt32(
		public void Init(string strSQL, string strReportType, int intLabelType, string strPrinterName, string strFonttoUse, int intLabelStartPosition = 1)
		{
			string strDBName = "";
			int intReturn;
			int x;
			bool boolUseFont;
			bool boolIsLaser = false;
			intBlankLabelsLeftToPrint = intLabelStartPosition - 1;
			strFont = strFonttoUse;
			this.Document.Printer.PrinterName = strPrinterName;
			boolPP = false;
			boolMort = false;
			switch (strReportType)
            {
                case "MORTGAGEHOLDER":
                    strDBName = "CentralData";
                    boolMort = true;

                    break;
                case "REALESTATE":
                    strDBName = "twcl0000.vb1";

                    break;
                case "PERSONALPROPERTY":
                    strDBName = "twcl0000.vb1";
                    boolPP = true;

                    break;
            }
			rsData.OpenRecordset(strSQL, strDBName);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			// make them choose the printer first if you have to use a custom form
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			boolDifferentPageSize = false;
			switch (intLabelType)
			{
				case 0:
					{
						// 4013
						boolDifferentPageSize = true;
						PageSettings.Margins.Top = 0;
						PageSettings.Margins.Left = 0;
						PageSettings.Margins.Right = 0;
						//PageSettings.PaperSize = 255;
						PageSettings.PaperHeight = 1;
						PageSettings.PaperWidth = FCConvert.ToSingle(3.5);
						PageSettings.Margins.Bottom = 0;
                        //FC:FINAL:MSH - replace wrong size calculations
                        //PrintWidth = 3.5f - 1;
                        PrintWidth = 3.5f - (1 / 1440f);
						this.Detail.Height = 1439 / 1440f;
						boolIsLaser = false;
						break;
					}
				case 1:
					{
						// 4014
						boolDifferentPageSize = true;
						PageSettings.Margins.Top = 0;
						PageSettings.Margins.Left = 0;
						PageSettings.Margins.Right = 0;
						PageSettings.Margins.Bottom = 0;
						//PageSettings.PaperSize = 255;
						PageSettings.PaperHeight = FCConvert.ToSingle(1.5);
						PageSettings.PaperWidth = 4;
                        //FC:FINAL:MSH - replace wrong size calculations
                        //PrintWidth = 4 - 1;
                        PrintWidth = 4 - (1 / 1440f);
                        //FC:FINAL:MSH - replace wrong size calculations
                        //this.Detail.Height = 1.5f - 1;
                        this.Detail.Height = 1.5f - (1 / 1440f);
						boolIsLaser = false;
						break;
					}
				case 2:
					{
						// Avery 5260 (3 X 10)  Height = 1" Width = 2.63"
						PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
						PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
						PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
						PrintWidth = 8.5f - PageSettings.Margins.Left - PageSettings.Margins.Right;
						intLabelWidth = FCConvert.ToInt32((2.625f));
						Detail.Height = 1;
						Detail.ColumnCount = 3;
                        Detail.ColumnSpacing = (0.125f);
						boolIsLaser = true;
						break;
					}
				case 3:
					{
						// Avery 5261 (2 X 10)  Height = 1" Width = 4"
						PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
						PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
						PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
						PrintWidth = (8.5f) - PageSettings.Margins.Left - PageSettings.Margins.Right;
						intLabelWidth = 4;
						Detail.Height = 1;
						Detail.ColumnCount = 2;
						Detail.ColumnSpacing = 0.1875f;
						boolIsLaser = true;
						break;
					}
				case 4:
					{
						// Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
						PageSettings.Margins.Top = FCConvert.ToSingle(0.8125);
						PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
						PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
						PrintWidth = (8.5f) - PageSettings.Margins.Left - PageSettings.Margins.Right;
						intLabelWidth = 4;
						Detail.Height = 1.33f;
						Detail.ColumnCount = 2;
						Detail.ColumnSpacing = 0.1875f;
						boolIsLaser = true;
						break;
					}
				case 5:
					{
						// Avery 5163 (2 X 5)  Height = 2" Width = 4"
						PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
						PageSettings.Margins.Left = FCConvert.ToSingle(0.1875);
						PageSettings.Margins.Right = FCConvert.ToSingle(0.1875);
						PrintWidth = (8.5f) - PageSettings.Margins.Left - PageSettings.Margins.Right;
						intLabelWidth = 4;
						Detail.Height = 2;
						Detail.ColumnCount = 2;
						Detail.ColumnSpacing = 0.15625f;
						boolIsLaser = true;
						break;
					}
			}
			//end switch
			if (boolDifferentPageSize)
			{
				Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			if (boolIsLaser)
			{
				double dblAdjust = 0;
				// vbPorter upgrade warning: dblMargin As double	OnWrite(float, short)
				double dblMargin = 0;
				dblAdjust = Conversion.Val(modRegistry.GetRegistryKey("BLLabelLaserAdjustment"));
				if (dblAdjust != 0)
				{
					//FC:FINAL:CHN - issue #1618: Delete convert to get correct size.
					// dblMargin = PageSettings.Margins.Top + Convert.ToInt64((dblAdjust * (240)) / 1440f);
					dblMargin = PageSettings.Margins.Top + (dblAdjust * (240)) / 1440f;
					if (dblMargin < 0)
						dblMargin = 0;
					PageSettings.Margins.Top = FCConvert.ToSingle(dblMargin);
				}
			}
			// End If
			CreateDataFields();
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName, 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Labels");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomLabel", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
		}

		private void CreateDataFields()
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intNumber;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 5;
			if (boolMort)
				intNumber = 5;
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
				NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				NewField.CanGrow = false;
				NewField.Name = "txtData" + FCConvert.ToString(intRow);
				NewField.Top = (intRow - 1) * 225 / 1440f;
				NewField.Left = 144 / 1440f;
				// one space
                //FC:FINAL:MSH - replace wrong width calculation to avoid overlapping data
				//NewField.Width = (FCConvert.ToInt32(PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
				NewField.Width = ((PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
				NewField.Height = 225 / 1440f;
				NewField.Text = string.Empty;
				NewField.MultiLine = false;
                NewField.WrapMode = WrapMode.WordWrap;
				if (Strings.Trim(strFont) != string.Empty)
				{
					NewField.Font = new Font(strFont, NewField.Font.Size);
				}
				Detail.Controls.Add(NewField);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			int intControl;
			int intRow;
			string[] straddr = new string[6 + 1];
			int x = 0;
			if (intBlankLabelsLeftToPrint <= 0)
			{
				straddr[1] = "";
				straddr[2] = "";
				straddr[3] = "";
				straddr[4] = "";
				straddr[5] = "";
				straddr[6] = "";
				x = 1;
				straddr[x] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("name")));
				x += 1;
				straddr[x] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("secowner")));
				x += 1;
				straddr[x] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("address1")));
				x += 1;
				straddr[x] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("address2")));
				x += 1;
				if (boolMort)
				{
					straddr[x] = Strings.Trim(rsData.Get_Fields_String("City") + "  " + rsData.Get_Fields_String("state"));
				}
				if (boolPP)
				{
					x += 1;
					straddr[x] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("address3")));
					// straddr(x) = straddr(x) & " " & Trim(rsData.Fields("zip"))
					// If Trim(rsData.Fields("zip4")) <> vbNullString Then
					// straddr(x) = straddr(x) & "-" & Trim(rsData.Fields("zip4"))
					// End If
				}
				else if (boolMort)
				{
					straddr[x] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("address3")));
					x += 1;
					straddr[x] = Strings.Trim(rsData.Get_Fields_String("City") + "  " + rsData.Get_Fields_String("state"));
					straddr[x] += " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("zip")));
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("zip4"))) != string.Empty)
					{
						straddr[x] += "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("zip4")));
					}
				}
				else
				{
					straddr[x] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("MailingAddress3")));
					x += 1;
					straddr[x] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("address3")));
					// straddr(x) = straddr(x) & " " & Trim(rsData.Fields("zip"))
					// If Trim(rsData.Fields("zip4")) <> vbNullString Then
					// straddr(x) = straddr(x) & "-" & Trim(rsData.Fields("zip4"))
					// End If
				}
				// condense
				if (Strings.Trim(straddr[4]) == string.Empty)
				{
					straddr[4] = straddr[5];
					straddr[5] = straddr[6];
					straddr[6] = "";
				}
				if (Strings.Trim(straddr[3]) == string.Empty)
				{
					straddr[3] = straddr[4];
					if (boolMort)
					{
						straddr[4] = straddr[5];
						straddr[5] = straddr[6];
						straddr[6] = "";
					}
					else
					{
						straddr[4] = "";
					}
				}
				if (Strings.Trim(straddr[2]) == string.Empty)
				{
					straddr[2] = straddr[3];
					straddr[3] = straddr[4];
					if (boolMort)
					{
						straddr[4] = straddr[5];
						straddr[5] = straddr[6];
						straddr[6] = "";
					}
					else
					{
						straddr[4] = "";
					}
				}
				if (Strings.Trim(straddr[1]) == string.Empty)
				{
					straddr[1] = straddr[2];
					straddr[2] = straddr[3];
					straddr[3] = straddr[4];
					if (boolMort)
					{
						straddr[4] = straddr[5];
						straddr[5] = straddr[6];
						straddr[6] = "";
					}
					else
					{
						straddr[4] = "";
					}
				}
				for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						switch (intRow)
						{
							case 1:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[1];
									break;
								}
							case 2:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[2];
									break;
								}
							case 3:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[3];
									break;
								}
							case 4:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[4];
									break;
								}
							case 5:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = straddr[5];
									break;
								}
						}
						//end switch
					}
				}
				// intControl
				rsData.MoveNext();
			}
			else
			{
				intBlankLabelsLeftToPrint -= 1;
			}
		}

		
	}
}
