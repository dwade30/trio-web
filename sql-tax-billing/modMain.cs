﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using System.Data.SqlClient;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Models;
using SharedApplication.TaxBilling;
using TWSharedLibrary;

namespace TWBL0000
{
	public class modMain
	{

		//[DllImport("kernel32")]
		//public static extern int OpenProcess(uint dwDesiredAccess, int bInheritHandle, int dwProcessId);

		//[DllImport("kernel32")]
		//public static extern int WaitForSingleObject(int hHandle, int dwMilliseconds);

		//[DllImport("kernel32")]
		//public static extern int CloseHandle(int hObject);

		public static void Main()
		{
			//FileSystemObject fso = new FileSystemObject();
			// kk04132015 troges-39  Change to single config file
			if (!modGlobalFunctions.LoadSQLConfig("TWBL0000.VB1"))
			{
				MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			clsDRWrapper clsTemp = new clsDRWrapper();
			// Dim s                           As New SettingsInfo
			// If Not s.LoadSettingsAndArchives(CurDir & "\" & s.GetDefaultSettingsFileName) Then
			// MsgBox "Error loading connection information", vbCritical, "Error"
			// End If
			clsTemp.SharedDefaultDB = "TWMV0000.vb1";
			// clsTemp.DefaultMegaGroup = clsTemp.AvailableMegaGroups(0).GroupName
			// clsTemp.DefaultGroup = "Live"
			try
			{
				// On Error GoTo ErrorHandler
				modGlobalFunctions.UpdateUsedModules();
				modGlobalVariables.Statics.boolShortRE = !modGlobalConstants.Statics.gboolRE;
				modGlobalVariables.Statics.boolShortPP = !modGlobalConstants.Statics.gboolPP;
				modGlobalConstants.Statics.gboolRE = true;
				modGlobalConstants.Statics.gboolPP = true;
				modGlobalVariables.Statics.boolRE = true;
				modReplaceWorkFiles.EntryFlagFile(true, "");
				//FC:FINAL:BBE: remove check
				//if (App.PrevInstance())
				//{
				//    MessageBox.Show("Warning. There is already a Tax Billing running on this computer." + "\r\n" + "If they are both using the same database this is probably unintended and one should be closed.", "Already Running", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//}
				//if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\twbl0000.hlp"))
				//{
				//	App.HelpFile = FCFileSystem.Statics.UserDataFolder + "\\twbl0000.hlp";
				//}
				//FC:FINAL:SBE - we cannot store userid as a global settings, because webapp is used by multiple users at the same time
				//modReplaceWorkFiles.Statics.gintSecurityID = (modRegistry.GetRegistryKey("SecurityID") != string.Empty ? FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn)) : 0);
				modReplaceWorkFiles.Statics.gintSecurityID = TWSharedLibrary.Variables.Statics.IntUserID;
                CheckDatabaseStructure();
				modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
				modGlobalConstants.Statics.clsSecurityClass.Init("BL");
				modReplaceWorkFiles.GetGlobalVariables();
				modNewAccountBox.GetFormats();
				if (modGlobalConstants.Statics.gboolBD)
				{
					modValidateAccount.SetBDFormats();
				}
				if (!modGlobalConstants.Statics.gboolBL)
				{
					MessageBox.Show("Billing is not enabled or has expired", "Not Enabled", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Application.Exit();
					return;
				}
				LoadCustomizedInfo();
				MDIParent.InstancePtr.Init();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Main", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void LoadCustomizedInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					modGlobalVariables.Statics.CustomizedInfo.ApplyDiscounts = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ApplyDiscounts"));
					modGlobalVariables.Statics.CustomizedInfo.DuplexCommitment = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("DuplexCommitment"));
					modGlobalVariables.Statics.CustomizedInfo.IgnorePrinterFonts = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("IgnorePrinterFonts"));
					modGlobalVariables.Statics.CustomizedInfo.ShowRef1 = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ShowRef1"));
					modGlobalVariables.Statics.CustomizedInfo.ShowRef2 = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ShowRef2"));
					modGlobalVariables.Statics.CustomizedInfo.ShowTreeGrowth = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ShowTreeGrowth"));
					modGlobalVariables.Statics.CustomizedInfo.ShowDollarSigns = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ShowDollarSigns"));
					modGlobalVariables.Statics.CustomizedInfo.boolIsRegional = modRegionalTown.IsRegionalTown();
					modGlobalVariables.Statics.CustomizedInfo.boolZeroLiabilityOption = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ZeroLiabilityOption"));
					modGlobalVariables.Statics.CustomizedInfo.AlternateAccount = FCConvert.ToString(clsLoad.Get_Fields_String("AlternateAccount"));
					modGlobalVariables.Statics.CustomizedInfo.SplitSmallAmounts = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("splitsmallamounts"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadCustomizedInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static bool CheckVersion()
		{ 
           var dbInfo = new SQLConfigInfo()
            {
				ServerInstance =  StaticSettings.gGlobalSettings.DataSource,
				UserName = StaticSettings.gGlobalSettings.UserName,
				Password = StaticSettings.gGlobalSettings.Password
            };
           return StaticSettings.GlobalCommandDispatcher.Send(new UpdateTaxBillingDatabase(dbInfo,
               StaticSettings.gGlobalSettings.DataEnvironment)).Result;
            
		}

		public static bool CheckDatabaseStructure()
		{
			bool boolReturn;
			boolReturn = CheckVersion();
            var ppDb = new cPPDatabase();
            ppDb.CheckVersion();
            cCollectionsDB collDB = new cCollectionsDB();
			boolReturn = boolReturn && collDB.CheckVersion();
            var reDB = new cRealEstateDB();
            boolReturn = boolReturn & reDB.CheckVersion();

			return boolReturn;
        }

		//public static void WaitForTerm(ref int pID)
		//{
		//	int pHND;
		//	int pCheck = 0;
		//	pHND = OpenProcess(modGlobalVariables.SYNCHRONIZE, 0, pID);
		//	if (pHND != 0)
		//	{
		//		foreach (Form ff in Application.OpenForms)
		//		{
		//			ff.Enabled = false;
		//		}
		//		// ff
		//		do
		//		{
		//			pCheck = WaitForSingleObject(pHND, 0);
		//			if (pCheck != 0x102)
		//				break;
		//			//Application.DoEvents();
		//		}
		//		while (true);
		//		foreach (Form ff in Application.OpenForms)
		//		{
		//			ff.Enabled = true;
		//		}
		//		// ff
		//		CloseHandle(pHND);
		//	}
		//}

		public static double Round(double dValue, short iDigits)
		{
			double Round = 0;
			Round = Conversion.Int(FCConvert.ToDouble(dValue * (Math.Pow(10, iDigits)) + 0.5)) / (Math.Pow(10, iDigits));
			return Round;
		}

		public static double RoundDown_8(double dValue, short iDigits)
		{
			return RoundDown(ref dValue, ref iDigits);
		}

		public static double RoundDown(ref double dValue, ref short iDigits)
		{
			double RoundDown = 0;
			RoundDown = Conversion.Int(dValue * (Math.Pow(10, iDigits))) / (Math.Pow(10, iDigits));
			return RoundDown;
		}
		// vbPorter upgrade warning: lngNumber As int	OnWrite(double, int, string)
		public static string NumtoString_6(long lngNumber, short intLength)
		{
			return NumtoString(lngNumber, intLength);
		}

		public static string NumtoString_8(long lngNumber, short intLength)
		{
			return NumtoString(lngNumber, intLength);
		}

		public static string NumtoString(long lngNumber, short intLength)
		{
			string NumtoString = "";
			// makes a string (with commas) from lngnumber that is intlength characters
			string strNum = "";
			string strTemp = "";
			bool boolUseCommas;
			if (intLength < 1)
			{
				NumtoString = "";
				return NumtoString;
			}
			boolUseCommas = true;
			switch (intLength)
			{
				case 4:
					{
						if (lngNumber > 999)
							boolUseCommas = false;
						break;
					}
				case 5:
					{
						if (lngNumber > 9999)
							boolUseCommas = false;
						break;
					}
				case 6:
					{
						if (lngNumber > 99999)
							boolUseCommas = false;
						break;
					}
				case 7:
				case 8:
					{
						if (lngNumber > 999999)
							boolUseCommas = false;
						break;
					}
				case 9:
					{
						if (lngNumber > 9999999)
							boolUseCommas = false;
						break;
					}
				case 10:
					{
						if (lngNumber > 99999999)
							boolUseCommas = false;
						break;
					}
				case 11:
				case 12:
					{
						if (lngNumber > 999999999)
							boolUseCommas = false;
						break;
					}
			}
			//end switch
			// THIS WILL PREVENT THE COMMAS FROM SHOWING IN THE OUTPRINT FILE
			if (boolUseCommas)
			{
				strNum = Strings.Format(lngNumber, "###,###,###,###,##0");
			}
			else
			{
				strNum = Strings.Format(lngNumber, "0");
			}
			if (strNum.Length >= intLength)
			{
				NumtoString = Strings.Right(strNum, intLength);
			}
			else
			{
				NumtoString = Strings.StrDup(intLength - strNum.Length, " ") + strNum;
			}
			return NumtoString;
		}
		// vbPorter upgrade warning: dblNumber As double	OnWrite(double, short, string)
		// vbPorter upgrade warning: intPlaces As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		public static string DblToString_6(double dblNumber, short intLength, int intPlaces = 2)
		{
			return DblToString(ref dblNumber, ref intLength, intPlaces);
		}

		public static string DblToString_8(double dblNumber, short intLength, int intPlaces = 2)
		{
			return DblToString(ref dblNumber, ref intLength, intPlaces);
		}

		public static string DblToString(ref double dblNumber, ref short intLength, int intPlaces = 2)
		{
			string DblToString = "";
			// makes a string (with commas) from dblnumber that is intlength characters
			string strNum;
			string strFormat = "";
			bool boolUseCommas;
			if (intLength < 1)
			{
				DblToString = "";
				return DblToString;
			}
			boolUseCommas = true;
			switch (intLength)
			{
				case 7:
					{
						if (dblNumber > 999.99)
							boolUseCommas = false;
						break;
					}
				case 8:
					{
						if (dblNumber > 9999.99)
							boolUseCommas = false;
						break;
					}
				case 9:
				case 10:
					{
						if (dblNumber > 99999.99)
							boolUseCommas = false;
						break;
					}
				case 11:
					{
						if (dblNumber > 999999.99)
							boolUseCommas = false;
						break;
					}
				case 12:
				case 13:
					{
						if (dblNumber > 9999999.99)
							boolUseCommas = false;
						break;
					}
			}
			//end switch
			if (boolUseCommas)
			{
				strFormat = "###,###,###,###,##0.";
			}
			else
			{
				strFormat = "0.";
			}
			strFormat += Strings.StrDup(intPlaces, "0");
			strNum = Strings.Format(dblNumber, strFormat);
			if (strNum.Length >= intLength)
			{
				DblToString = Strings.Right(strNum, intLength);
			}
			else
			{
				DblToString = Strings.StrDup(intLength - strNum.Length, " ") + strNum;
			}
			return DblToString;
		}
		//FC:FINAL:MSH - issue #1501: add an extra overload for returning data after initializing(in vb6 string is ref type and can be used as optional)
		public static bool HasNewOwner_8(int lngAccount, string strOrigName, ref string strNewName, ref string strSecOwner, ref string strAddr1, ref string strAddr2, ref string strAddr3)
		{
			string temp = "";
			return HasNewOwner(ref lngAccount, ref strOrigName, ref strNewName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3, ref temp, ref temp, ref temp, ref temp, ref temp);
		}
		//FC:FINAL:MSH - issue #1501: add an extra overload for returning data after initializing(in vb6 string is ref type and can be used as optional)
		public static bool HasNewOwner_8(int lngAccount, string strOrigName)
		{
			string temp = "";
			return HasNewOwner(ref lngAccount, ref strOrigName, ref temp, ref temp, ref temp, ref temp, ref temp, ref temp, ref temp, ref temp, ref temp, ref temp);
		}
		//FC:FINAL:MSH - issue #1501: add 'ref' keyword for returning data after initializing (in vb6 string is reference type and data will be returned)
		public static bool HasNewOwner_8(int lngAccount, string strOrigName, ref string strNewName, ref string strSecOwner, ref string strAddr1, ref string strAddr2, ref string strAddr3, ref string strCity, ref string strState, ref string strZip, ref string strCountry, ref string strMailingAddress3)
		{
			return HasNewOwner(ref lngAccount, ref strOrigName, ref strNewName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3, ref strCity, ref strState, ref strZip, ref strCountry, ref strMailingAddress3);
		}
		//FC:FINAL:MSH - issue #1501: add 'ref' keyword for returning data after initializing (in vb6 string is reference type and data will be returned)
		public static bool HasNewOwner_28439(int lngAccount, string strOrigName, ref string strNewName, ref string strSecOwner, ref string strAddr1, ref string strAddr2, ref string strAddr3, ref string strCountry, ref string strMailingAddress3)
		{
			string temp = "";
			return HasNewOwner(ref lngAccount, ref strOrigName, ref strNewName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3, ref temp, ref temp, ref temp, ref strCountry, ref strMailingAddress3);
		}
		//FC:FINAL:MSH - issue #1501: add 'ref' keyword for returning data after initializing (in vb6 string is reference type and data will be returned)
		public static bool HasNewOwner(ref int lngAccount, ref string strOrigName, ref string strNewName, ref string strSecOwner, ref string strAddr1, ref string strAddr2, ref string strAddr3, ref string strCity, ref string strState, ref string strZip, ref string strCountry, ref string strMailingAddress3)
		{
			bool HasNewOwner = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				HasNewOwner = false;
				// Call clsLoad.OpenRecordset("select p.FullNameLF as OwnerFullNameLF, q.FullNameLF as SecOwnerFullNameLF ,Address1, Address2,City,Partystate,zip from master CROSS APPLY " & clsLoad.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(master.OwnerPartyID, NULL, 'RE', master.rsAccount) as p CROSS APPLY " & clsLoad.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(master.SecOwnerPartyID) as q where rsaccount = " & lngAccount & " and rscard = 1", strREDatabase)
				clsLoad.OpenRecordset("select ownerpartyid ,secownerpartyid from master  where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = 1", modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					cParty tParty;
					cParty sParty;
					// vbPorter upgrade warning: tPAddr As cPartyAddress	OnWrite(Collection)
					cPartyAddress tPAddr;
					cPartyController tPCont = new cPartyController();
					tParty = tPCont.GetParty(clsLoad.Get_Fields_Int32("ownerpartyid"), true);
					if (!(tParty == null))
					{
						strSecOwner = "";
                        if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("deedname1"))).Replace(",", "")) != fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(strOrigName).Replace(",", "")))
                        {
							HasNewOwner = true;
                            strNewName = clsLoad.Get_Fields_String("DeedName1").Replace("\n", "");
							strSecOwner = FCConvert.ToString(clsLoad.Get_Fields("deedname2")).Replace("\n", ""); 

							tParty = tPCont.GetParty(clsLoad.Get_Fields_Int32("ownerpartyid"), false);
							if (tParty.Addresses.Count > 0)
							{
								tPAddr = tParty.Addresses[1];
								strAddr1 = tPAddr.Address1.Replace("\r\n", "");
								strAddr2 = tPAddr.Address2.Replace("\r\n", "");
								strMailingAddress3 = tPAddr.Address3.Replace("\r\n", "");
								strAddr3 = tPAddr.City.Replace("\r\n", "") + " " + tPAddr.State + " " + tPAddr.Zip;
								strCity = tPAddr.City;
								strState = tPAddr.State;
								strZip = tPAddr.Zip;
								strCountry = tPAddr.Country;
								if (Strings.LCase(Strings.Trim(strCountry)) == "united states")
								{
									strCountry = "";
								}
							}
						}
					}
				}
				return HasNewOwner;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HasNewOwner", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HasNewOwner;
		}

		public static bool NewOwner(ref int lngAccount, ref DateTime dtDate, string strName = "", string strAddr1 = "", string strAddr2 = "", string strAddr3 = "", string strSecOwner = "", bool boolGetNewOwner = false, string strCountry = "")
		{
			bool NewOwner = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return true if there is a record in the previous owner table since the date passed in
				// for the account that is passed in, it will return false otherwise
				clsDRWrapper rsNew = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				strCountry = "";
				// check the previous owner table to find out if there has been a sale of that account since the date
				rsNew.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = "+FCConvert.ToString(lngAccount)+" AND SaleDate >= '"+fecherFoundation.Strings.Format(dtDate, "MM/DD/YYYY")+"'", modGlobalVariables.strREDatabase);               
                if (!rsNew.EndOfFile())
				{
					// found a match
					NewOwner = true;
					strName = FCConvert.ToString(rsNew.Get_Fields_String("Name"));
					strSecOwner = FCConvert.ToString(rsNew.Get_Fields_String("SecOwner"));
					strAddr1 = FCConvert.ToString(rsNew.Get_Fields_String("Address1"));
					strAddr2 = FCConvert.ToString(rsNew.Get_Fields_String("Address2"));
					strAddr3 = rsNew.Get_Fields_String("City") + ", " + rsNew.Get_Fields_String("State") + " " + rsNew.Get_Fields_String("Zip");
					if (Conversion.Val(rsNew.Get_Fields_String("Zip4")) != 0)
					{
						strAddr3 += "-" + rsNew.Get_Fields_String("Zip4");
					}
					if (boolGetNewOwner)
					{
						cPartyController tPCont = new cPartyController();
						cParty tParty;
						// vbPorter upgrade warning: tAddr As cPartyAddress	OnWrite(Collection)
						cPartyAddress tAddr;
                        // Call rsTemp.OpenRecordset("select p.FullNameLF as OwnerFullNameLF, q.FullNameLF as SecOwnerFullNameLF ,Address1,Address2,Address3, City,PartyState,Zip from master as c CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(c.OwnerPartyID) as p CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(c.SecOwnerPartyID) as q from master where rsaccount = " & lngAccount & " and rscard = 1", "twre0000.vb1")
                        rsTemp.OpenRecordset("select ownerpartyid,secownerpartyid,deedname1,deedname2 from master  where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = 1", "twre0000.vb1");
                        if (!rsTemp.EndOfFile())
						{
							tParty = tPCont.GetParty(rsTemp.Get_Fields_Int32("ownerpartyid"));
							if (!(tParty == null))
							{
								strName = rsTemp.Get_Fields_String("Deedname1").Trim();
								if (tParty.Addresses.Count > 0)
								{
									tAddr = tParty.Addresses[1];
									if (!(tAddr == null))
									{
										strAddr1 = tAddr.Address1;
										strAddr2 = tAddr.Address2;
										strAddr3 = tAddr.City + ", " + tAddr.State + " " + tAddr.Zip;
										strCountry = Strings.Trim(tAddr.Country);
										if (Strings.LCase(strCountry) == "united states")
										{
											strCountry = "";
										}
									}
								}
							}
							strSecOwner = rsTemp.Get_Fields_String("DeedName2").Trim();
						}
					}
				}
				else
				{
					NewOwner = false;
					strName = "";
					strAddr1 = "";
					strAddr2 = "";
					strAddr3 = "";
				}
				return NewOwner;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				NewOwner = false;
			}
			return NewOwner;
		}

		private static bool CheckMessage()
		{
			bool CheckMessage = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			bool boolChangeIt;
			string strMessage = "";
			string strTemp = "";
			int intFormat;
			try
			{
				// On Error GoTo ErrorHandler
				CheckMessage = false;
				boolChangeIt = false;
				clsLoad.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (clsLoad.FindFirstRecord("Line", 0))
					{
						if (Strings.Left(Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("Message"))) + Strings.StrDup(30, " "), 30) == "WITHOUT STATE AID TO EDUCATION")
						{
							if (clsLoad.FindFirstRecord("Line", 1))
							{
								if (Strings.Left(Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("Message"))) + Strings.StrDup(30, " "), 30) == "REVENUE SHARING, YOUR TAX BILL")
								{
									if (clsLoad.FindFirstRecord("Line", 2))
									{
										if (Strings.Trim(Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("Message")))) == "BEEN XX.X% HIGHER.")
										{
											if (clsLoad.FindFirstRecord("Line", 3))
											{
												if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Message"))) == string.Empty)
												{
													boolChangeIt = true;
												}
											}
											else
											{
												boolChangeIt = true;
											}
										}
									}
								}
							}
						}
					}
				}
				else
				{
					boolChangeIt = true;
				}
				if (boolChangeIt)
				{
					clsLoad.Execute("update taxmessage set message = ''", "twbl0000.vb1");
				}
				CheckMessage = true;
				return CheckMessage;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CheckMessage", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckMessage;
		}
	}
}
