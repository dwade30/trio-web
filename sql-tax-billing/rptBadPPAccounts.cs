﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBadPPAccounts.
	/// </summary>
	public partial class rptBadPPAccounts : BaseSectionReport
	{
		public rptBadPPAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static rptBadPPAccounts InstancePtr
		{
			get
			{
				return (rptBadPPAccounts)Sys.GetInstance(typeof(rptBadPPAccounts));
			}
		}

		protected rptBadPPAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBadPPAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport;
		cPartyController tPCont = new cPartyController();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
				txtPage.Text = "Page 1";
				txtMuni.Text = modGlobalConstants.Statics.MuniName;
				if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					txtMuni.Text = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void Init(ref clsDRWrapper tRS, bool modalDialog)
		{
			try
			{
				// On Error GoTo ErrorHandler
				rsReport = tRS;
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!rsReport.EndOfFile())
				{
					cParty tParty;
					double dblCatTotal = 0;
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					txtAccount.Text = rsReport.Get_Fields_String("account");
					tParty = tPCont.GetParty(rsReport.Get_Fields_Int32("Partyid"), true);
					if (!(tParty == null))
					{
						txtName.Text = tParty.FullNameLastFirst;
					}
					else
					{
						txtName.Text = "";
					}
					dblCatTotal = Conversion.Val(rsReport.Get_Fields_Int32("Category1")) + Conversion.Val(rsReport.Get_Fields_Int32("category2")) + Conversion.Val(rsReport.Get_Fields_Int32("Category3")) + Conversion.Val(rsReport.Get_Fields_Int32("Category4")) + Conversion.Val(rsReport.Get_Fields_Int32("Category5")) + Conversion.Val(rsReport.Get_Fields_Int32("Category6")) + Conversion.Val(rsReport.Get_Fields_Int32("Category7")) + Conversion.Val(rsReport.Get_Fields_Int32("Category8")) + Conversion.Val(rsReport.Get_Fields_Int32("Category9"));
					txtCategories.Text = Strings.Format(dblCatTotal, "#,###,###,##0");
					// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
					txtTotal.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("Value")), "#,###,###,##0");
					rsReport.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Detail_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
