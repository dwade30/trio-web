﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmChooseBillFormat.
	/// </summary>
	public partial class frmChooseBillFormat : BaseForm
	{
		public frmChooseBillFormat()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseBillFormat InstancePtr
		{
			get
			{
				return (frmChooseBillFormat)Sys.GetInstance(typeof(frmChooseBillFormat));
			}
		}

		protected frmChooseBillFormat _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int CNSTCUSTOMFORMAT = 17;
		clsBillFormat clsTaxFormat = new clsBillFormat();
		int LastBillType;
		bool boolFromSupplemental;
		string strList;

		public void Init(bool boolFromSupp, string strSupplementList = "")
		{
			// if from supplemental
			boolFromSupplemental = boolFromSupp;
			strList = strSupplementList;
			if (boolFromSupp)
			{
				// Me.Show vbModal, MDIParent
				this.LoadForm();
				mnuContinue_Click();
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void cmbBillFormat_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// If cmbBillFormat.ListIndex = 13 Then 'Type E which isn't used yet
			// cmbBillFormat.ListIndex = LastBillType
			// Else
			// LastBillType = cmbBillFormat.ListIndex
			// End If
			//SizeThePic_2(FCConvert.ToInt16(cmbBillFormat.SelectedIndex));
            SizeThePic_2(Convert.ToInt16(cmbBillFormat.ItemData(cmbBillFormat.SelectedIndex)));
			//FillDesc_2(FCConvert.ToInt16(cmbBillFormat.SelectedIndex));
            FillDesc_2(Convert.ToInt16(cmbBillFormat.ItemData(cmbBillFormat.SelectedIndex)));
		}
		// vbPorter upgrade warning: intFormat As short	OnWriteFCConvert.ToInt32(
		private void FillDesc_2(short intFormat)
		{
			FillDesc(intFormat);
		}

		private void FillDesc(short intFormat)
		{
			string strDesc = "";
			//intFormat += 1;
			// If intFormat > 13 Then intFormat = intFormat + 1 'cause we took out E
			switch (intFormat)
			{
				//case 1:
				//	{
				//		strDesc = "Similar to 4.  With or without discount showing and with or without a breakdown of taxes by percentage or by dollars. Cannot combine Real Estate and Personal Property bills.";
				//		imgBreakdown.Visible = true;
				//		imgCombination.Visible = false;
				//		imgDiscounts.Visible = true;
				//		break;
				//	}
				//case 2:
				//	{
				//		strDesc = "Similar to 3.  This is a mailer format with 2 stubs.  Real Estate and Personal Property can be combined on this bill.  With or without a discount and with or without a breakdown of taxes by percentage or by dollars.";
				//		imgBreakdown.Visible = true;
				//		imgCombination.Visible = true;
				//		imgDiscounts.Visible = true;
				//		break;
				//	}
				//case 3:
				//	{
				//		strDesc = "Similar to 2.  This is a mailer format with 4 stubs.  Real Estate and Personal Property can be combined on this bill.  With or without a discount and with or without a breakdown of taxes by percentage or by dollars.";
				//		imgBreakdown.Visible = true;
				//		imgCombination.Visible = true;
				//		imgDiscounts.Visible = true;
				//		break;
				//	}
				//case 4:
				//	{
				//		strDesc = "Similar to 1.  With or without discount showing and with or without a breakdown of taxes by percentage or by dollars.";
				//		imgBreakdown.Visible = true;
				//		imgCombination.Visible = false;
				//		imgDiscounts.Visible = true;
				//		break;
				//	}
				//case 5:
				//	{
				//		strDesc = "No discount is available with this format. With or without a breakdown of taxes by percentage or by dollars. With 2 stubs. Real Estate and Personal Property can be combined on this bill.";
				//		imgBreakdown.Visible = true;
				//		imgCombination.Visible = true;
				//		imgDiscounts.Visible = false;
				//		break;
				//	}
				//case 6:
				//	{
				//		strDesc = "No discount is available with this format. With or without a breakdown of taxes by percentage or by dollars. With 1 stub. Real Estate and Personal Property can be combined on this bill.";
				//		imgBreakdown.Visible = true;
				//		imgCombination.Visible = true;
				//		imgDiscounts.Visible = false;
				//		break;
				//	}
				//case 7:
				//	{
				//		strDesc = "No discount is available with this format. Real Estate and Personal Property cannot be combined on this bill. No breakdown of taxes is available.";
				//		imgBreakdown.Visible = false;
				//		imgCombination.Visible = false;
				//		imgDiscounts.Visible = false;
				//		break;
				//	}
				//case 8:
				//	{
				//		strDesc = "With or without a discount and with or without a breakdown of taxes by percentage or by dollars.";
				//		imgBreakdown.Visible = true;
				//		imgCombination.Visible = false;
				//		imgDiscounts.Visible = true;
				//		break;
				//	}
				//case 9:
				//	{
				//		// A
				//		strDesc = "No discount is available with this format. Real Estate and Personal Property can be combined with this format. No breakdown of taxes is available.";
				//		imgBreakdown.Visible = false;
				//		imgCombination.Visible = true;
				//		imgDiscounts.Visible = false;
				//		break;
				//	}
				//case 10:
				//	{
				//		// B
				//		strDesc = "No discount is available with this format. Real Estate and Personal Property can be combined with this format. No breakdown of taxes is available.";
				//		imgBreakdown.Visible = false;
				//		imgCombination.Visible = true;
				//		imgDiscounts.Visible = false;
				//		break;
				//	}
				//case 11:
				//	{
				//		// C
				//		strDesc = "No discount is available with this format. Real Estate and Personal Property can be combined with this format. No breakdown of taxes is available.";
				//		imgBreakdown.Visible = false;
				//		imgCombination.Visible = true;
				//		imgDiscounts.Visible = false;
				//		break;
				//	}
				//case 12:
				//	{
				//		// D
				//		strDesc = "No discount is available with this format. Real Estate and Personal Property can be combined with this format. No breakdown of taxes is available.";
				//		imgBreakdown.Visible = false;
				//		imgCombination.Visible = true;
				//		imgDiscounts.Visible = false;
				//		// Case 13
				//		// E
				//		// strDesc = "Non-mailer format wide. No discount is available with this format. Real estate and Personal Property can be combined with this format. With or without breakdown of taxes by percentage or by dollars."
				//		// boolClickyClicky = True
				//		// chkBreakdown.Value = vbChecked
				//		// boolClickyClicky = True
				//		// chkCombination.Value = vbChecked
				//		// boolClickyClicky = True
				//		// chkDiscounts.Value = vbUnchecked
				//		break;
				//	}
				//case 13:
				//	{
				//		// F
				//		strDesc = "No discount is available with this format. Real Estate and Personal Property cannot be combined on this bill. Breakdown of taxes by percentage is available.";
				//		imgBreakdown.Visible = true;
				//		imgCombination.Visible = false;
				//		imgDiscounts.Visible = false;
				//		break;
				//	}
				case 14:
					{
						// G
						strDesc = "Pre-printed Laser Format. With or without a breakdown of taxes by percentage or by dollars. Real Estate and Personal Property can be combined with this format. Discount is available.";
						imgBreakdown.Visible = true;
						imgCombination.Visible = true;
						imgDiscounts.Visible = true;
						break;
					}
				case 15:
					{
						strDesc = "Same as G except it is not pre-printed.  With or without a breakdown by percentage or by dollars.  Real Estate and Personal Property can be combined with this format. Discount is available.";
						imgBreakdown.Visible = true;
						imgCombination.Visible = true;
						imgDiscounts.Visible = true;
						break;
					}
				case 16:
					{
						strDesc = "Same as H except Exemption is broken down and assessment and tax due amounts are placed differently.  With or without a breakdown by percentage or by dollars.  Real Estate and Personal Property can be combined with this format.  Discount is available.";
						imgBreakdown.Visible = true;
						imgCombination.Visible = true;
						imgDiscounts.Visible = true;
						break;
					}
				case 17:
					{
						strDesc = "This format is completely user defined";
						imgBreakdown.Visible = true;
						imgCombination.Visible = true;
						imgDiscounts.Visible = true;
						break;
					}
			}
			//end switch
			lblFormatDescription.Text = strDesc;
		}

		private void frmChooseBillFormat_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
				return;
			}
		}

		private void frmChooseBillFormat_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			BillPic.Image = BillImages.Images[0];
			SizeThePic_2(1);
			FillDesc_2(1);
			FillCombo();
			clsTemp.OpenRecordset("select * from billsetup", "twbl0000.vb1");
			if (clsTemp.EndOfFile())
			{
				cmbBillFormat.SelectedIndex = 0;
				LastBillType = 0;
			}
			else
			{
				//cmbBillFormat.SelectedIndex = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("billformat")) - 1);
                for (int x = 0; x < cmbBillFormat.Items.Count; x++)
                {
                    if (cmbBillFormat.ItemData(x) == clsTemp.Get_Fields_Int32("billformat"))
                    {
                        cmbBillFormat.SelectedIndex = x;
                        LastBillType = x;
                        break;
                    }
                }
			}
		}

		private void frmChooseBillFormat_Resize(object sender, System.EventArgs e)
		{
			//FC:FINAL:RPU:#1434 - Don't need to recalculate the size of image
			//ResizeThePic();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (!boolFromSupplemental)
			{
				//MDIParent.InstancePtr.Show();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
		// vbPorter upgrade warning: intpic As short	OnWriteFCConvert.ToInt32(
		private void SizeThePic_2(short intpic)
		{
			SizeThePic(ref intpic);
		}

		private void SizeThePic(ref short intpic)
		{
			double dblRatio;
			//intpic += 1;
			if (intpic == 4)
				intpic = 1;
			// If intpic = 16 Then intpic = 15
			if (intpic > 15)
				intpic -= 1;
			// If intpic = 3 Then intpic = 2
			if ((intpic > 4) && intpic < 13)
				intpic -= 1;
			BillPic.Image = BillImages.Images[intpic - 1];
			BillPic.Height = BillImages.Images[intpic - 1].Size.Height;
			BillPic.Width = BillImages.Images[intpic - 1].Size.Width;			
			Frame1.Height = BillPic.Height + 36;
		}

		//private void ResizeThePic()
		//{
		//	double dblRatio = 0;
		//	int intpic;
		//	//intpic = cmbBillFormat.SelectedIndex + 1;
  //          intpic = cmbBillFormat.ItemData(cmbBillFormat.SelectedIndex);
		//	if (intpic == 4)
		//		intpic = 1;
		//	if (intpic > 15)
		//		intpic -= 1;
		//	// If intpic = 3 Then intpic = 2
		//	if (intpic > 4 && intpic < 13)
		//		intpic -= 1;
		//	// we reuse a pic for one of them
		//	BillPic.Width = Frame1.Width - 120;
		//	BillPic.Height = Frame1.Height - 180;
		//	if (intpic == 0)
		//	{
		//	}
		//	else
		//	{
		//		dblRatio = BillImages.Images[intpic - 1].Size.Width / BillImages.Images[intpic - 1].Size.Height;
		//		if (BillImages.Images[intpic - 1].Size.Height > BillImages.Images[intpic - 1].Size.Width)
		//		{
		//			BillPic.Width = FCConvert.ToInt32((BillPic.Height * dblRatio));
		//		}
		//		else
		//		{
		//			BillPic.Height = FCConvert.ToInt32((1 / dblRatio) * BillPic.Width);
		//		}
		//	}
		//	//FC:FINAL:MSH - i.issue #1538: change Height of the Frame with picture for fully displaying of the image
		//	Frame1.Height = BillPic.Height;
		//	//FC:FINAL:MSH - i.issue #1538: replace frame with combobox and label to avoid overlapping after recalculating image size
		//	Frame3.Top = Frame1.Bottom + 22;
		//}

		private void FillCombo()
		{
			//cmbBillFormat.AddItem("1 Mailer without Stubs");
			//cmbBillFormat.AddItem("2 Mailer with 2 stubs");
			//cmbBillFormat.AddItem("3 Mailer with 4 stubs");
			//cmbBillFormat.AddItem("4 Non-mailer format - Narrow");
			//cmbBillFormat.AddItem("5 Alternate format G with 2 stubs");
			//cmbBillFormat.AddItem("6 Alternate format G with 1 stub");
			//cmbBillFormat.AddItem("7 Alternate format P");
			//cmbBillFormat.AddItem("8 Alternate format V");
			//cmbBillFormat.AddItem("A Alternate format C");
			//cmbBillFormat.AddItem("B Alternate format H");
			//cmbBillFormat.AddItem("C Alternate format I");
			//cmbBillFormat.AddItem("D Alternate format C/V");
			// .AddItem ("E Non-mailer Wide")
			//cmbBillFormat.AddItem("F Alternate format P Wide");
			cmbBillFormat.AddItem("G Laser/Inkjet Format Pre-Printed");
            cmbBillFormat.ItemData(0, 14);
			cmbBillFormat.AddItem("H Laser/Inkjet Format Blank");
            cmbBillFormat.ItemData(1, 15);
			cmbBillFormat.AddItem("I Laser/Inkjet Format Blank 2");
            cmbBillFormat.ItemData(2, 16);
			cmbBillFormat.AddItem("J Custom Bill Format");
            cmbBillFormat.ItemData(3, 17);
        }

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			int lngReturn = 0;
			
			SetupFormat();
			if (clsTaxFormat.Billformat == CNSTCUSTOMFORMAT)
			{
				// choose which customformat
				lngReturn = frmChooseCustomBillType.InstancePtr.Init("BL", "TWBL0000.vb1");
				if (lngReturn <= 0)
				{
					return;
				}
				clsTaxFormat.CustomBillID = lngReturn;
			}
			frmBillSetup.InstancePtr.Init(ref clsTaxFormat, boolFromSupplemental, strList);
			Close();
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, new System.EventArgs());
		}

		private void SetupFormat()
		{
			// If cmbBillFormat.ListIndex > 13 Then
			// we took 13 out
			// clsTaxFormat.Billformat = cmbBillFormat.ListIndex + 2
			// Else

            clsTaxFormat.Billformat = Convert.ToInt16( cmbBillFormat.ItemData(cmbBillFormat.SelectedIndex));
            // End If
            clsTaxFormat.UseLaserAlignment = false;
			switch (clsTaxFormat.Billformat)
			{
				//case 1:
				//case 4:
				//	{
				//		clsTaxFormat.Breakdown = true;
				//		clsTaxFormat.BreakdownDollars = true;
				//		clsTaxFormat.Combined = false;
				//		clsTaxFormat.Discount = true;
				//		clsTaxFormat.ReturnAddress = true;
				//		break;
				//	}
				//case 2:
				//case 3:
				//	{
				//		clsTaxFormat.Breakdown = true;
				//		clsTaxFormat.BreakdownDollars = true;
				//		clsTaxFormat.Combined = true;
				//		clsTaxFormat.Discount = true;
				//		clsTaxFormat.ReturnAddress = true;
				//		break;
				//	}
				//case 5:
				//	{
				//		clsTaxFormat.Breakdown = true;
				//		clsTaxFormat.BreakdownDollars = true;
				//		clsTaxFormat.Combined = true;
				//		clsTaxFormat.Discount = false;
				//		clsTaxFormat.ReturnAddress = true;
				//		break;
				//	}
				//case 6:
				//	{
				//		clsTaxFormat.Breakdown = true;
				//		clsTaxFormat.BreakdownDollars = true;
				//		clsTaxFormat.Combined = true;
				//		clsTaxFormat.Discount = false;
				//		clsTaxFormat.ReturnAddress = true;
				//		break;
				//	}
				//case 7:
				//	{
				//		clsTaxFormat.Breakdown = false;
				//		clsTaxFormat.BreakdownDollars = false;
				//		clsTaxFormat.Combined = false;
				//		clsTaxFormat.Discount = false;
				//		clsTaxFormat.ReturnAddress = true;
				//		break;
				//	}
				//case 8:
				//	{
				//		clsTaxFormat.Breakdown = true;
				//		clsTaxFormat.BreakdownDollars = true;
				//		clsTaxFormat.Combined = false;
				//		clsTaxFormat.Discount = true;
				//		clsTaxFormat.ReturnAddress = true;
				//		break;
				//	}
				//case 9:
				//	{
				//		clsTaxFormat.Breakdown = false;
				//		clsTaxFormat.BreakdownDollars = false;
				//		clsTaxFormat.Combined = true;
				//		clsTaxFormat.Discount = false;
				//		clsTaxFormat.ReturnAddress = false;
				//		break;
				//	}
				//case 10:
				//	{
				//		// B
				//		clsTaxFormat.Breakdown = true;
				//		clsTaxFormat.BreakdownDollars = false;
				//		clsTaxFormat.Combined = true;
				//		clsTaxFormat.Discount = false;
				//		clsTaxFormat.ReturnAddress = false;
				//		break;
				//	}
				//case 11:
				//	{
				//		// C
				//		clsTaxFormat.Breakdown = false;
				//		clsTaxFormat.BreakdownDollars = false;
				//		clsTaxFormat.Combined = true;
				//		clsTaxFormat.Discount = false;
				//		clsTaxFormat.ReturnAddress = false;
				//		break;
				//	}
				//case 12:
				//	{
				//		// D
				//		clsTaxFormat.Breakdown = false;
				//		clsTaxFormat.BreakdownDollars = false;
				//		clsTaxFormat.Combined = true;
				//		clsTaxFormat.Discount = false;
				//		clsTaxFormat.ReturnAddress = false;
				//		// Case 13
				//		// E
				//		// clsTaxFormat.Breakdown = True
				//		// clsTaxFormat.BreakdownDollars = True
				//		// clsTaxFormat.Combined = True
				//		// clsTaxFormat.Discount = False
				//		// clsTaxFormat.ReturnAddress = True
				//		break;
				//	}
				//case 13:
				//	{
				//		// F
				//		clsTaxFormat.Breakdown = true;
				//		clsTaxFormat.BreakdownDollars = false;
				//		clsTaxFormat.Combined = false;
				//		clsTaxFormat.Discount = false;
				//		clsTaxFormat.ReturnAddress = true;
				//		break;
				//	}
				case 14:
				case 15:
				case 16:
					{
						// G,H
						clsTaxFormat.Breakdown = true;
						clsTaxFormat.BreakdownDollars = true;
						clsTaxFormat.Combined = true;
						clsTaxFormat.Discount = true;
						clsTaxFormat.ReturnAddress = true;
						clsTaxFormat.UseLaserAlignment = true;
						break;
					}
				case 17:
					{
						clsTaxFormat.Breakdown = false;
						clsTaxFormat.BreakdownDollars = false;
						clsTaxFormat.Combined = true;
						clsTaxFormat.Discount = false;
						clsTaxFormat.ReturnAddress = false;
						clsTaxFormat.UseLaserAlignment = false;
						break;
					}
			}
			//end switch
		}

		private void cmdContinue_Click(object sender, EventArgs e)
		{
			mnuContinue_Click(mnuContinue, EventArgs.Empty);
		}
	}
}
