﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPickCommitmentYear.
	/// </summary>
	public partial class frmPickCommitmentYear : BaseForm
	{
		public frmPickCommitmentYear()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPickCommitmentYear InstancePtr
		{
			get
			{
				return (frmPickCommitmentYear)Sys.GetInstance(typeof(frmPickCommitmentYear));
			}
		}

		protected frmPickCommitmentYear _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolREBook;
		int intYear;
		bool boolNotFromSupplemental;
		int lngRKey;
		bool boolFromRE;

		private void frmPickCommitmentYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmPickCommitmentYear_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPickCommitmentYear properties;
			//frmPickCommitmentYear.ScaleWidth	= 5880;
			//frmPickCommitmentYear.ScaleHeight	= 4290;
			//frmPickCommitmentYear.LinkTopic	= "Form1";
			//frmPickCommitmentYear.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			int intOption;
			intOption = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CommitmentBindOption", "BL"))));
			if (intOption == 0)
			{
				cmbBind.SelectedIndex = 2;
			}
			else if (intOption == 1)
			{
				cmbBind.SelectedIndex = 0;
			}
			else if (intOption == 2)
			{
				cmbBind.SelectedIndex = 1;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// MDIParent.Show
		}

		private void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, EventArgs.Empty);
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			int x;
			// loop counter
			int y;
			int NumFonts;
			// number of fonts this printer has
			bool boolUseFont;
			// did we find a printer font?
			string strFont = "";
			// font name
			string strPrinterName;
			// devicename
			int intWhichReport = 0;
			// reg,landscape, or wide
			int intCPI = 0;
			// how many cpi we are looking for
			int intPageStart = 0;
			// number to start page numbers at
			bool boolNewPage;
			// true if chknewpage is checked
			// vbPorter upgrade warning: lngRateKey As int	OnWrite(short, string)
			int lngRateKey;
			clsRateRecord clsrate = new clsRateRecord();
			string strTemp = "";
			string OldPrinter;
			int intBindOption = 0;
			// make sure we have valid data before continuing
			try
			{
				// On Error GoTo ErrorHandler
				OldPrinter = FCGlobal.Printer.DeviceName;
				if (boolNotFromSupplemental)
                {
                    intPageStart = Conversion.Val(txtStart.Text) > 0 
                        ? FCConvert.ToInt32(Math.Round(Conversion.Val(txtStart.Text))) 
                        : 1;
                }

				switch (cmbBind.SelectedIndex)
                {
                    case 0:
                        intBindOption = 1;

                        break;
                    case 1:
                        intBindOption = 2;

                        break;
                    default:
                        intBindOption = 0;

                        break;
                }
				boolNewPage = false;
				
                strPrinterName = FCGlobal.Printer.DeviceName;
				NumFonts = FCGlobal.Printer.Fonts.Count;
				boolUseFont = false;
				
                switch (cmbPrint.SelectedIndex)
                {
                    case 0:
                        // regular
                        intWhichReport = 1;

                        break;
                    case 1:
                        // landscape
                        intWhichReport = 2;

                        break;
                    default:
                        // wide printer
                        intWhichReport = 3;

                        break;
                }

				if (intWhichReport == 1)
				{
					// get a 17 cpi printer font if possible
					for (x = 0; x <= NumFonts - 1; x++)
					{
						strFont = FCGlobal.Printer.Fonts[x];

                        var right3 = Strings.Right(strFont, 3);

                        if (Strings.UCase(right3) != "CPI") 
                            continue;

                        strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
                        if (Strings.Right(strFont, 2) != "17" && right3 != "17 ") 
                            continue;

                        boolUseFont = true;
                        strFont = FCGlobal.Printer.Fonts[x];
                        break;
                    }
					// x
				}
				else
				{
					// look for a 12 cpi font
					intCPI = 12;
					
                    for (x = 0; x <= NumFonts - 1; x++)
					{
						strFont = FCGlobal.Printer.Fonts[x];

                        var right3 = Strings.Right(strFont, 3);

                        if (Strings.UCase(right3) != "CPI") 
                            continue;

                        strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
                        if (Conversion.Val(Strings.Right(strFont, 2)) != intCPI && Conversion.Val(right3) != intCPI) 
                            continue;

                        boolUseFont = true;
                        strFont = FCGlobal.Printer.Fonts[x];
                        break;
                    }
					// x
				}
				
                boolREBook = true;
				if (boolNotFromSupplemental)
				{
					// 
					clsrate.LoadRate(lngRKey);
					intYear = clsrate.TaxYear;
					boolNewPage = chkNewPage.CheckState == Wisej.Web.CheckState.Checked;
					
                    if (intWhichReport == 1)
					{
						if (boolFromRE)
						{
							rptCommitment.InstancePtr.Init(intYear, boolUseFont, strFont, strPrinterName, intPageStart, boolNewPage, lngRKey, false);
						}
						else
						{
							rptPPCommitment.InstancePtr.Init(intYear, boolUseFont, strFont, strPrinterName, intPageStart, boolNewPage, lngRKey, false);
						}
					}
					else
					{
						if (boolFromRE)
						{
							rptCommitmentWide.InstancePtr.Init(intYear, boolUseFont, strFont, strPrinterName, intWhichReport, intPageStart, boolNewPage, lngRKey, false);
						}
						else
						{
							rptPPCommitmentWide.InstancePtr.Init(intYear, boolUseFont, strFont, strPrinterName, intWhichReport, intPageStart, boolNewPage, lngRKey, false);
						}
					}

				}
				else
				{
					for (y = 1; y <= 2; y++)
					{
						// do twice so it will do a commitment book for re and one for pp even when
						// they use the same ratekey
						lngRateKey = 0;
						strTemp = y == 1 ? "RE" : "PP";

						for (x = 1; x <= frmSupplementalBill.InstancePtr.ListGrid.Rows - 1; x++)
						{
							// go through supplemental grid and print commitment book for each ratekey
                            if (FCConvert.ToDouble(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 4)) == lngRateKey || frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 0) != strTemp) 
                                continue;
                            
                            lngRateKey = FCConvert.ToInt32(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 4));
                            clsrate.LoadRate(lngRateKey);
                            intYear = clsrate.TaxYear;
                            intPageStart = 1;
                            if (intWhichReport == 1)
                            {
                                // which commitment book are we printing?
                                if (frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 0) == "RE")
                                {
                                    rptCommitment.InstancePtr.Init(intYear, boolUseFont, strFont, strPrinterName, intPageStart, boolNewPage, lngRateKey, true);
                                }
                                else
                                {
                                    rptPPCommitment.InstancePtr.Init(intYear, boolUseFont, strFont, strPrinterName, intPageStart, boolNewPage, lngRateKey, true);
                                }
                            }
                            else
                            {
                                modRegistry.SaveRegistryKey("CommitmentBindOption", FCConvert.ToString(intBindOption), "BL");
                                if (frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 0) == "RE")
                                {
                                    rptCommitmentWide.InstancePtr.Init(intYear, boolUseFont, strFont, strPrinterName, intWhichReport, intPageStart, boolNewPage, lngRateKey, true, intBindOption: intBindOption);
                                }
                                else
                                {
                                    rptPPCommitmentWide.InstancePtr.Init(intYear, boolUseFont, strFont, strPrinterName, intWhichReport, intPageStart, boolNewPage, lngRateKey, true, intBindOption: intBindOption);
                                }
                            }
                        }
						// x
					}
					// y
				}
				
				Close();
			}
			catch (Exception ex)
            {
                if (Information.Err(ex).Number != 32755) // real error, not a cancelled print job
                {
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuContinue_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
               
            }
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void txtYear_KeyPress(ref short KeyAscii)
		{
            if (KeyAscii != 13) return;

            KeyAscii = 0;
            mnuContinue_Click();
        }

		public void Init(int lngRateKey, bool boolFromRealEstate, int intModal = 0)
		{
			// not used by supplemental screen
			boolNotFromSupplemental = true;
			txtStart.Visible = true;
			chkNewPage.Visible = true;
			lblStartPage.Visible = true;
			boolFromRE = boolFromRealEstate;
			// If boolFromRE Then
			lblCommitment.Text = "Commitment Book for Initial Transfer";
			// Else
			// End If
			lngRKey = lngRateKey;
			if (intModal != FCConvert.ToInt32(FormShowEnum.Modal))
			{
				this.Show(App.MainForm);
			}
			else
			{
				this.Show(FormShowEnum.Modal);
			}
		}

		private void optPrint_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 2:
					{
						cmbBind.Visible = true;
						lblBind.Visible = true;
						break;
					}
				default:
					{
						cmbBind.Visible = false;
						lblBind.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void optPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbPrint.SelectedIndex);
			optPrint_CheckedChanged(index, sender, e);
		}

		private void cmbPrint_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            switch (cmbPrint.SelectedIndex)
            {
                case 0:
                case 1:
                case 2:
                    optPrint_CheckedChanged(sender, e);

                    break;
            }
        }
	}
}
