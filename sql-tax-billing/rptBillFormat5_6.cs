﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormat5_6.
	/// </summary>
	public partial class rptBillFormat5_6 : FCSectionReport
	{
		public rptBillFormat5_6()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBillFormat5_6 InstancePtr
		{
			get
			{
				return (rptBillFormat5_6)Sys.GetInstance(typeof(rptBillFormat5_6));
			}
		}

		protected rptBillFormat5_6 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		
		bool boolFirstBill;
		clsDRWrapper clsMortgageHolders = new clsDRWrapper();
		clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
		clsBillFormat clsTaxBillFormat = new clsBillFormat();
		clsRateRecord clsTaxRate = new clsRateRecord();
		clsDRWrapper clsRateRecs = new clsDRWrapper();
		double TaxRate;
		int intStartPage;
		clsDRWrapper clsBills = new clsDRWrapper();
		bool boolRE;
		bool boolPP;
		double Tax1;
		double Tax2;
		double Tax3;
		double Tax4;
		double Prepaid;
		double DistPerc1;
		double DistPerc2;
		double DistPerc3;
		double DistPerc4;
		double DistPerc5;
		int intDistCats;
		double dblDiscountPercent;
		string strCat1 = "";
		string strCat2 = "";
		string strCat3 = "";
		int lngExempt;
		// vbPorter upgrade warning: lngTotAssess As int	OnWrite(short, double)
		int lngTotAssess;
		clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		bool boolShownModally;
		bool boolNotJustUnloading;
		bool boolReprintNewOwnerCopy;
		/// <summary>
		/// this will be set to true if there is a new owner and the recordset will not be advanced to create a copy for him
		/// </summary>
		clsDRWrapper rsMultiRecipients = new clsDRWrapper();
		bool boolPrintRecipientCopy;
		bool boolIsRegional;
		// vbPorter upgrade warning: intYear As short	OnWrite(double, int)
		public void Init(clsBillFormat clsTaxFormat, int intYear, string strFontName, bool boolShow = true, string strPrinterN = "", bool boolModal = false)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			string strSQL;
			boolIsRegional = modRegionalTown.IsRegionalTown();
			boolShownModally = boolModal;
			boolNotJustUnloading = true;
			if (strPrinterN != string.Empty)
			{
				this.Document.Printer.PrinterName = strPrinterN;
			}
			if (clsTaxFormat.BillsFrom == 0)
			{
				boolRE = true;
				if (clsTaxFormat.Combined)
				{
					boolPP = true;
				}
				else
				{
					boolPP = false;
				}
			}
			else
			{
				boolRE = false;
				boolPP = true;
			}
			if (boolPP)
			{
				clsTemp.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
				strCat1 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				txtCat1label.Text = strCat1;
				clsTemp.MoveNext();
				strCat2 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				txtCat2Label.Text = strCat2;
				clsTemp.MoveNext();
				strCat3 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				txtCat3Label.Text = strCat3;
			}
			if (!boolPP)
			{
				txtCat1.Visible = false;
				txtCat2.Visible = false;
				txtCat3.Visible = false;
				txtCatOther.Visible = false;
				txtCat1label.Visible = false;
				txtCat2Label.Visible = false;
				txtCat3Label.Visible = false;
				txtOtherLabel.Visible = false;
			}
			// 
			if (!boolRE)
			{
				txtLand.Visible = false;
				txtBldg.Visible = false;
				lblLand.Visible = false;
				txtBldgLabel.Visible = false;
			}
			clsTaxBillFormat.CopyFormat(ref clsTaxFormat);
			// If clsTaxBillFormat.Discount Then
			// Frame1.Visible = True
			// Else
			// Frame1.Visible = False
			// End If
			if (!boolRE)
			{
				txtBookPage.Visible = false;
				txtBookPageLabel.Visible = false;
			}
			if (boolRE && clsTaxBillFormat.PrintCopyForMortgageHolders)
			{
				boolFirstBill = true;
				clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
				clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT", "CentralData");
				if (!clsMortgageAssociations.EndOfFile())
				{
					clsMortgageAssociations.MoveFirst();
					clsMortgageAssociations.MovePrevious();
				}
			}
			else
			{
				boolFirstBill = true;
			}
			strSQL = clsTaxBillFormat.SQLStatement;
			clsTaxRate.TaxYear = intYear;
			// lblTitle.Caption = intYear
			clsRateRecs.OpenRecordset("select * from raterec where year = " + FCConvert.ToString(intYear), "twcl0000.vb1");
			if (!clsRateRecs.EndOfFile())
			{
				clsTaxRate.LoadRate(clsRateRecs.Get_Fields_Int32("ID"));
			}
			// If clsTaxBillFormat.Discount Then
			// Frame1.Visible = True
			// Else
			// Frame1.Visible = False
			// End If
			//FC:TODO:AM
			//DistFrame.Visible = false;
			DistPerc1 = 0;
			DistPerc2 = 0;
			DistPerc3 = 0;
			DistPerc4 = 0;
			DistPerc5 = 0;
			clsTemp.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
			while (!clsTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				switch (FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("line"))))
				{
					case 0:
						{
							txtMessage1.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 1:
						{
							txtMessage2.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 2:
						{
							txtMessage3.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 3:
						{
							txtMessage4.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 4:
						{
							txtMessage5.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 5:
						{
							txtMessage6.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 6:
						{
							txtmessage7.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 7:
						{
							txtMessage8.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 8:
						{
							txtMessage9.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 9:
						{
							txtMessage10.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 10:
						{
							txtMessage11.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 11:
						{
							txtMessage12.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 12:
						{
							txtMessage13.Text = clsTemp.Get_Fields_String("message");
							break;
						}
				}
				//end switch
				clsTemp.MoveNext();
			}
			if (clsTaxBillFormat.Breakdown)
			{
				//FC:TODO:AM
				//DistFrame.Visible = true;
				clsTemp.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					intDistCats = clsTemp.RecordCount();
					DistPerc1 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
					txtDistPerc1.Text = Strings.Format(DistPerc1, "#0.00") + "%";
					DistPerc1 /= 100;
					strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
					txtDist1.Text = Strings.Mid(strTemp, 1, 11);
					if (intDistCats > 1)
					{
						clsTemp.MoveNext();
						txtDistPerc2.Visible = true;
						txtDist2.Visible = true;
						DistPerc2 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
						txtDistPerc2.Text = Strings.Format(DistPerc2, "#0.00") + "%";
						DistPerc2 /= 100;
						strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
						txtDist2.Text = Strings.Mid(strTemp, 1, 11);
						if (intDistCats > 2)
						{
							clsTemp.MoveNext();
							txtDistPerc3.Visible = true;
							txtDist3.Visible = true;
							DistPerc3 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
							txtDistPerc3.Text = Strings.Format(DistPerc3, "#0.00") + "%";
							DistPerc3 /= 100;
							strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
							txtDist3.Text = Strings.Mid(strTemp, 1, 11);
							if (intDistCats > 3)
							{
								clsTemp.MoveNext();
								txtDistPerc4.Visible = true;
								txtDist4.Visible = true;
								DistPerc4 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
								txtDistPerc4.Text = Strings.Format(DistPerc4, "#0.00") + "%";
								DistPerc4 /= 100;
								strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
								txtDist4.Text = Strings.Mid(strTemp, 1, 11);
								if (intDistCats > 4)
								{
									clsTemp.MoveNext();
									txtDistPerc5.Visible = true;
									txtDist5.Visible = true;
									DistPerc5 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
									txtDistPerc5.Text = Strings.Format(DistPerc5, "#0.00") + "%";
									DistPerc5 /= 100;
									strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
									txtDist5.Text = Strings.Mid(strTemp, 1, 11);
								}
							}
						}
					}
				}
				else
				{
					intDistCats = 0;
				}
			}
			if (clsTaxBillFormat.BreakdownDollars)
			{
				txtDistAmount1.Visible = true;
				if (intDistCats > 1)
				{
					txtDistAmount2.Visible = true;
				}
				if (intDistCats > 2)
				{
					txtDistAmount3.Visible = true;
				}
				if (intDistCats > 3)
				{
					txtDistAmount4.Visible = true;
				}
				if (intDistCats > 4)
				{
					txtDistAmount5.Visible = true;
				}
			}
			else
			{
				txtDistAmount1.Visible = false;
				txtDistAmount2.Visible = false;
				txtDistAmount3.Visible = false;
				txtDistAmount4.Visible = false;
				txtDistAmount5.Visible = false;
			}
			// If clsTaxBillFormat.Discount Then
			// Call clsTemp.OpenRecordset("select * from discount", "twbl0000.vb1")
			// If Not clsTemp.EndOfFile Then
			// dblDiscountPercent = Val(clsTemp.Fields("discount")) / 100
			// txtDiscount.Text = Format(clsTemp.Fields("discount"), "#0.00")
			// txtDiscountDate.Text = clsTemp.Fields("duedate")
			// End If
			// End If
			// 
			// If clsTaxRate.NumberOfPeriods > 2 Then
			// framStub3.Visible = True
			// framStub2.Visible = True
			// txtDueDate2.Visible = True
			// txtAmount2.Visible = True
			// 
			// txtDueDate2.Text = clsTaxRate.Get_DueDate(2)
			// txtDueDate3.Text = clsTaxRate.Get_DueDate(3)
			// If clsTaxRate.NumberOfPeriods > 3 Then
			// Frame1.Visible = False ' can't show stub 4 and discount
			// framStub4.Visible = True
			// txtDueDate4.Text = clsTaxRate.Get_DueDate(4)
			// txtAmount4.Visible = True
			// txtDueDate4.Visible = True
			// End If
			// Else
			if (clsTaxRate.NumberOfPeriods < 2)
			{
				//FC:TODO:AM
				//framStub2.Visible = false;
				// txtPayment2Label.Visible = False
				// txtAmount2.Visible = False
				// txtDueDate2.Visible = False
			}
			else
			{
				//FC:TODO:AM
				//framStub2.Visible = true;
				txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
				// txtAmount2.Visible = True
				// txtDueDate2.Visible = True
				// txtPayment2Label.Visible = True
			}
			// 
			// End If
			// 
			txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
			if (strFontName != string.Empty)
			{
				// now set each box and label to the correct printer font
				for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
				{
					// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
					bool setFont = false;
					bool bold = false;
					if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
					{
						setFont = true;
					}
					else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
					{
						setFont = true;
						bold = true;
					}
					if (setFont)
					{
						GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
						if (textBox != null)
						{
							textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
						}
						else
						{
							GrapeCity.ActiveReports.SectionReportModel.Label label = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
							if (label != null)
							{
								label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
						}
					}
				}
				// x
				//this.Printer.RenderMode = 1;
			}
			GetBills(ref strSQL);
			if (boolShow)
			{
				// Me.Show , MDIParent
			}
			else
			{
				this.PrintReport(false);
			}
		}

		private void GetBills(ref string strSQL)
		{
			// If boolREBill Then
			clsBills.OpenRecordset(strSQL, "twcl0000.vb1");
			// Else
			// Call clsBills.OpenRecordset(strSQL, "twpp0000.vb1")
			// End If
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int intReturn = 0;
			
			modCustomPageSize.CheckDefaultPrinter(this.Document.Printer.PrinterName);
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("BillFormat5", 1000, 550);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsBills.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			// Dim clsAddress As New clsDRWrapper
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
			if (clsTaxBillFormat.HasDefaultMargin)
			{
				this.PageSettings.Margins.Left = 0;
			}
			if (clsTaxBillFormat.ReturnAddress)
			{
				if (!boolIsRegional)
				{
					string[] strAdd = new string[4 + 1];
					modBLCustomBill.GetReturnAddressForBills(0, ref strAdd[0], ref strAdd[1], ref strAdd[2], ref strAdd[3]);
					txtAddress1.Text = strAdd[0];
					txtAddress2.Text = strAdd[1];
					txtAddress3.Text = strAdd[2];
					txtAddress4.Text = strAdd[3];
				}
			}
			else
			{
				txtAddress1.Visible = false;
				txtAddress2.Visible = false;
				txtAddress3.Visible = false;
				txtAddress4.Visible = false;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			modCustomPageSize.Statics.boolChangedDefault = true;
			modCustomPageSize.ResetDefaultPrinter();
			if (!boolShownModally && boolNotJustUnloading)
			{
				//MDIParent.InstancePtr.Show();
			}
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	// do this since we already chose the printer and don't want to choose again
		//	// now we have to handle printing the pages ourselves though.
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "Print...")
		//	{
		//		clsPrint.ReportPrint(this);
		//		// Call frmNumPages.Init(Me.Pages.Count, 1)
		//		// frmNumPages.Show vbModal, MDIParent
		//		// Me.Printer.FromPage = gintStartPage
		//		// Me.Printer.ToPage = gintEndPage
		//		// Me.PrintReport (False)
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			double Dist1 = 0;
			double Dist2 = 0;
			double Dist3 = 0;
			double Dist4 = 0;
			double Dist5 = 0;
			double TotTax = 0;
			string strTemp = "";
			string strAcct = "";
			DateTime dtLastDate;
			string strOldName = "";
			string strSecOwner = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			string strLocation = "";
			if (!clsBills.EndOfFile())
			{
				if (FCConvert.ToString(clsBills.Get_Fields_String("Billingtype")) == "RE")
				{
					boolRE = true;
					boolPP = false;
				}
				else if (clsBills.Get_Fields_String("Billingtype") == "PP")
				{
					boolRE = false;
					boolPP = true;
				}
				else
				{
					// combined
					boolRE = true;
					boolPP = true;
				}
				if (clsTaxBillFormat.ReturnAddress)
				{
					if (boolIsRegional)
					{
						string[] strReturn = new string[4 + 1];
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						modBLCustomBill.GetReturnAddressForBills(FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields("trancode"))), ref strReturn[0], ref strReturn[1], ref strReturn[2], ref strReturn[3]);
						txtAddress1.Text = strReturn[0];
						txtAddress2.Text = strReturn[1];
						txtAddress3.Text = strReturn[2];
						txtAddress4.Text = strReturn[3];
					}
				}
				if (boolPP)
				{
					txtCat1.Visible = true;
					txtCat2.Visible = true;
					txtCat3.Visible = true;
					txtCatOther.Visible = true;
					txtCat1label.Visible = true;
					txtCat2Label.Visible = true;
					txtCat3Label.Visible = true;
					txtOtherLabel.Visible = true;
				}
				else
				{
					txtCat1.Visible = false;
					txtCat2.Visible = false;
					txtCat3.Visible = false;
					txtCatOther.Visible = false;
					txtCat1label.Visible = false;
					txtCat2Label.Visible = false;
					txtCat3Label.Visible = false;
					txtOtherLabel.Visible = false;
				}
				// 
				if (!boolRE)
				{
					txtLand.Visible = false;
					txtBldg.Visible = false;
					lblLand.Visible = false;
					txtBldgLabel.Visible = false;
					txtBookPage.Visible = false;
					txtBookPageLabel.Visible = false;
				}
				else
				{
					txtLand.Visible = true;
					txtBldg.Visible = true;
					lblLand.Visible = true;
					txtBldgLabel.Visible = true;
					txtBookPage.Visible = true;
					txtBookPageLabel.Visible = true;
				}
				if (clsRateRecs.FindFirstRecord("ID", clsBills.Get_Fields_Int32("ratekey")))
				{
					clsTaxRate.LoadRate(clsBills.Get_Fields_Int32("ratekey"), clsRateRecs);
					if (clsTaxRate.NumberOfPeriods < 2)
					{
						//FC:TODO:AM
						//framStub2.Visible = false;
					}
					else
					{
						if (clsTaxBillFormat.Billformat == 6)
						{
							//FC:TODO:AM
							//framStub2.Visible = false;
						}
						else
						{
							//FC:TODO:AM
							//framStub2.Visible = true;
						}
						txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
						txtIRate2.Text = Strings.Format(clsTaxRate.InterestRate * 100, "0.000") + "%";
					}
					txtIRate1.Text = Strings.Format(clsTaxRate.InterestRate * 100, "0.000") + "%";
					txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
				}
				lngTotAssess = 0;
				lngExempt = 0;
				Tax1 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue1"));
				Tax2 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue2"));
				Tax3 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue3"));
				Tax4 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue4"));
				TotTax = Tax1 + Tax2 + Tax3 + Tax4;
				txtTaxDue.Text = Strings.Format(TotTax, "#,###,###,##0.00");
				if (clsTaxBillFormat.BreakdownDollars)
				{
					Dist1 = TotTax * DistPerc1;
					Dist2 = TotTax * DistPerc2;
					Dist3 = TotTax * DistPerc3;
					Dist4 = TotTax * DistPerc4;
					Dist5 = TotTax * DistPerc5;
					txtDistAmount1.Text = Strings.Format(Dist1, "##,###,##0.00");
					if (intDistCats > 1)
					{
						txtDistAmount2.Text = Strings.Format(Dist2, "##,###,##0.00");
					}
					if (intDistCats > 2)
					{
						txtDistAmount3.Text = Strings.Format(Dist3, "##,###,##0.00");
					}
					if (intDistCats > 3)
					{
						txtDistAmount4.Text = Strings.Format(Dist4, "##,###,##0.00");
					}
					if (intDistCats > 4)
					{
						txtDistAmount5.Text = Strings.Format(Dist5, "##,###,##0.00");
					}
				}
				Prepaid = Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid"));
				double dblAbated = 0;
				double dblAbate1 = 0;
				double dblAbate2 = 0;
				double dblAbate3 = 0;
				double dblAbate4 = 0;
				int intPer;
				double[] dblTAbate = new double[4 + 1];
				dblAbated = 0;
				if ((Tax2 > 0 || Tax3 > 0 || Tax4 > 0) && Prepaid > 0)
				{
					dblAbated = modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields_Int32("ID"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
					dblAbate1 = dblTAbate[1];
					dblAbate2 = dblTAbate[2];
					dblAbate3 = dblTAbate[3];
					dblAbate4 = dblTAbate[4];
					if (boolRE && boolPP)
					{
						// TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
						dblAbated += modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields("ppbillkey"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
						dblAbate1 += dblTAbate[1];
						dblAbate2 += dblTAbate[2];
						dblAbate3 += dblTAbate[3];
						dblAbate4 += dblTAbate[4];
					}
					Prepaid -= dblAbated;
				}
				if (dblAbated > 0)
				{
					Tax1 -= dblAbate1;
					Tax2 -= dblAbate2;
					Tax3 -= dblAbate3;
					Tax4 -= dblAbate4;
				}
				if (Prepaid > 0)
				{
					if (Prepaid > Tax1)
					{
						Prepaid -= Tax1;
						Tax1 = 0;
						if (Prepaid > Tax2)
						{
							Prepaid -= Tax2;
							Tax2 = 0;
							if (Prepaid > Tax3)
							{
								Prepaid -= Tax3;
								Tax3 = 0;
								if (Prepaid > Tax4)
								{
									Prepaid -= Tax4;
									Tax4 = 0;
								}
								else
								{
									Tax4 -= Prepaid;
								}
							}
							else
							{
								Tax3 -= Prepaid;
							}
						}
						else
						{
							Tax2 -= Prepaid;
						}
					}
					else
					{
						Tax1 -= Prepaid;
					}
				}
				Prepaid = Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid"));
				txtPrePaid.Text = Strings.Format(Prepaid, "#,###,##0.00");
				if (Prepaid == 0)
				{
					txtPrePaid.Visible = false;
					txtPrePaidLabel.Visible = false;
				}
				else
				{
					txtPrePaid.Visible = true;
					txtPrePaidLabel.Visible = true;
					// 09/28/2005 Ron had me change to taxdue is always original due
					// If Prepaid > TotTax Then
					// txtTaxDue.Text = "Overpaid"
					// Else
					// 
					// txtTaxDue.Text = Format(TotTax - Prepaid, "#,###,###,##0.00")
					// End If
				}
				txtRate.Text = Strings.Format(clsTaxRate.TaxRate * 1000, "#0.000");
				txtAmount1.Text = Strings.Format(Tax1, "#,###,##0.00");
				txtAmount2.Text = Strings.Format(Tax2, "#,###,##0.00");
				txtInterest1.Text = clsTaxRate.Get_InterestStartDate(1).ToShortDateString();
				txtInterest2.Text = clsTaxRate.Get_InterestStartDate(2).ToShortDateString();
				// txtAmount3.Text = Format(Tax3, "#,###,##0.00")
				// txtAmount4.Text = Format(Tax4, "#,###,##0.00")
				// txtinterest3.Text = clsTaxRate.Get_InterestStartDate(3)
				// txtinterest4.Text = clsTaxRate.Get_InterestStartDate(4)
				// 
				// If clsTaxBillFormat.Discount Then
				// txtDiscountAmount.Text = Format(dblDiscountPercent * (Tax1 + Tax2 + Tax3 + Tax4), "#,###,##0.00")
				// txtDiscountAmount.Text = Format((Tax1 + Tax2 + Tax3 + Tax4), "#,###,##0.00")
				// End If
				// 
				strAcct = "";
				if (boolRE)
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					strAcct = "R" + clsBills.Get_Fields("account");
					if (Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage"))) != string.Empty)
					{
						txtBookPage.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage")));
						txtBookPage.Visible = true;
						txtBookPageLabel.Visible = true;
					}
					else
					{
						txtBookPage.Visible = false;
						txtBookPageLabel.Visible = false;
					}
					txtLand.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")), "#,###,###,##0");
					txtBldg.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")), "#,###,###,##0");
					lngTotAssess = FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")) + Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")));
					if (boolPrintRecipientCopy)
					{
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						txtMailing2.Text = "C/O " + rsMultiRecipients.Get_Fields_String("Name");
						txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address1");
						txtMailing4.Text = rsMultiRecipients.Get_Fields_String("address2");
						txtMailing5.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
						// ElseIf clsTaxBillFormat.UsePreviousOwnerInfo And NewOwner(clsBills.Fields("Account"), clsTaxBillFormat.BillAsOfDate, strOldName, strAddr1, strAddr2, strAddr3) Then
					}
					else if (!boolFirstBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
					{
						clsMortgageHolders.FindFirstRecord("ID", clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"));
						if (!clsMortgageHolders.NoMatch)
						{
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing2.Text = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
							txtMailing5.Text = Strings.Trim(clsMortgageHolders.Get_Fields_String("city") + " " + clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
						}
						else
						{
							txtMailing1.Text = "Mortgage Holder Not Found";
							txtMailing2.Text = "";
							txtMailing3.Text = "";
							txtMailing4.Text = "";
							txtMailing5.Text = "";
						}
					}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (boolReprintNewOwnerCopy && modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3))
					{
						// If boolReprintNewOwnerCopy Then
						// txtMailing2.Text = Trim(clsBills.Fields("rsaddr1"))
						// txtMailing3.Text = Trim(clsBills.Fields("rsaddr2"))
						// txtMailing4.Text = Trim(clsBills.Fields("rsaddr3")) & "  " & clsBills.Fields("rsstate") & "  " & clsBills.Fields("rszip")
						// If Trim(clsBills.Fields("rszip4")) <> vbNullString Then
						// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("rszip4"))
						// End If
						// txtMailing1.Text = Trim(clsBills.Fields("rsname"))
						// Else
						txtMailing3.Text = strAddr1;
						txtMailing4.Text = strAddr2;
						txtMailing5.Text = strAddr3;
						txtMailing1.Text = strOldName;
						txtMailing2.Text = strSecOwner;
						// End If
					}
					else
					{
						txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
						txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
						txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
						// If Trim(clsBills.Fields("rssecowner")) <> vbNullString Then
						// If Trim(txtMailing2.Text) = vbNullString Or Trim(txtMailing3.Text) = vbNullString Then
						// If Trim(txtMailing2.Text) <> vbNullString Then
						// txtMailing3.Text = txtMailing2.Text
						// End If
						// txtMailing2.Text = Trim(clsBills.Fields("rssecowner"))
						// 
						// End If
						// End If
						// txtMailing4.Text = Trim(clsBills.Fields("rsaddr3")) & "  " & clsBills.Fields("rsstate") & "  " & clsBills.Fields("rszip")
						// If Trim(clsBills.Fields("rszip4")) <> vbNullString Then
						// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("rszip4"))
						// End If
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						// strName = Trim(clsBills.Fields("name1"))
						txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name2")));
					}
					if (txtMailing4.Text == "")
					{
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing3.Text == "")
					{
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing2.Text == "")
					{
						txtMailing2.Text = txtMailing3.Text;
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						strLocation = FCConvert.ToString(clsBills.Get_Fields("streetnumber"));
					}
					else
					{
						strLocation = "";
					}
					strLocation = Strings.Trim(Strings.Trim(strLocation + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
					txtLocation.Text = strLocation;
					txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("maplot")));
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				}
				if (boolPP)
				{
					// pp
					if (boolRE)
					{
						// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBills.Get_Fields("ppac")) > 0)
						{
							strAcct += " P";
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							strAcct += clsBills.Get_Fields("ppac");
						}
					}
					else
					{
						strAcct = "P";
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						strAcct += clsBills.Get_Fields("account");
					}
					txtCat1.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category1")), "#,###,###,##0");
					txtCat2.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category2")), "#,###,###,##0");
					txtCat3.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category3")), "#,###,###,##0");
					txtCatOther.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("category4")) + Conversion.Val(clsBills.Get_Fields_Int32("category5")) + Conversion.Val(clsBills.Get_Fields_Int32("category6")) + Conversion.Val(clsBills.Get_Fields_Int32("category7")) + Conversion.Val(clsBills.Get_Fields_Int32("category8")) + Conversion.Val(clsBills.Get_Fields_Int32("category9")), "#,###,###,##0");
					lngTotAssess += FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("ppassessment")));
					if (!boolRE)
					{
						txtMailing5.Text = "";
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						txtLocation.Text = Strings.Trim(clsBills.Get_Fields("streetnumber") + " " + clsBills.Get_Fields_String("apt") + " " + clsBills.Get_Fields_String("streetNAME"));
						// used for pp location
						if (boolPrintRecipientCopy)
						{
							txtMailing1.Text = rsMultiRecipients.Get_Fields_String("Name");
							txtMailing2.Text = rsMultiRecipients.Get_Fields_String("address1");
							txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address2");
							txtMailing4.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
							txtMailing5.Text = "";
						}
						else
						{
							txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
							// strTemp = Trim(clsBills.Fields("zip"))
							// txtMailing4.Text = Trim(clsBills.Fields("city")) & "  " & clsBills.Fields("state") & "  " & strTemp
							// If Trim(clsBills.Fields("zip4")) <> vbNullString Then
							// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("zip4"))
							// End If
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing5.Text = "";
						}
						if (txtMailing3.Text == "")
						{
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = "";
						}
						if (txtMailing2.Text == "")
						{
							txtMailing2.Text = txtMailing3.Text;
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = "";
						}
					}
					else
					{
					}
				}
				lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				// txtValue.Text = Format(lngTotAssess, "#,###,###,##0")
				txtExemption.Text = Strings.Format(lngExempt, "#,###,###,##0");
				txtAssessment.Text = Strings.Format(lngTotAssess - lngExempt, "#,###,###,##0");
				txtAccount.Text = strAcct;
				txtAcct1.Text = strAcct;
				txtAcct2.Text = strAcct;
				if (boolRE)
				{
					if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
					{
						boolReprintNewOwnerCopy = false;
						// if the copy for a new owner needs to be sent, then check it here
						if (clsTaxBillFormat.PrintCopyForNewOwner)
						{
							// if this is the main bill, only check for the new owner on the mail bill
							if (boolFirstBill)
							{
								if (DateTime.Today.Month < 4)
								{
									// if the month is before april then use last years commitment date
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1));
								}
								else
								{
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));
								}
								// If NewOwner(clsBills.Fields("Account"), dtLastDate) Then     'if there has been a new owner
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1")))
								{
									// if there has been a new owner
									boolReprintNewOwnerCopy = true;
								}
							}
						}
					}
					else if (!boolPrintRecipientCopy && boolFirstBill)
					{
						// turn off the copy so that the record can advance
						boolReprintNewOwnerCopy = false;
					}
					if (!boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							if (boolFirstBill)
							{
								rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
								if (!rsMultiRecipients.EndOfFile())
								{
									boolPrintRecipientCopy = true;
								}
							}
						}
					}
					else if (boolPrintRecipientCopy && boolFirstBill)
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (clsTaxBillFormat.PrintCopyForMortgageHolders && !boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (!clsMortgageAssociations.EndOfFile())
						{
							if (boolFirstBill)
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindFirstRecord("account", clsBills.Get_Fields("account"));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindNextRecord("account", clsBills.Get_Fields("account"));
							}
							// Call clsMortgageAssociations.FindNextRecord("account", clsBills.Fields("ac"))
							if (clsMortgageAssociations.NoMatch)
							{
								boolFirstBill = true;
								if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
									clsBills.MoveNext();
							}
							else
							{
								boolFirstBill = false;
							}
							// End If
						}
						else
						{
							boolFirstBill = true;
							if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
								clsBills.MoveNext();
						}
					}
					else
					{
						if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
							clsBills.MoveNext();
					}
				}
				else
				{
					if (!boolPrintRecipientCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
							if (!rsMultiRecipients.EndOfFile())
							{
								boolPrintRecipientCopy = true;
							}
						}
					}
					else
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (!boolPrintRecipientCopy)
					{
						clsBills.MoveNext();
					}
				}
			}
		}

		private void rptBillFormat5_6_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBillFormat5_6 properties;
			//rptBillFormat5_6.Icon	= "rptBillFormat5_6.dsx":0000";
			//rptBillFormat5_6.Left	= 0;
			//rptBillFormat5_6.Top	= 0;
			//rptBillFormat5_6.Width	= 15240;
			//rptBillFormat5_6.Height	= 11115;
			//rptBillFormat5_6.StartUpPosition	= 3;
			//rptBillFormat5_6.SectionData	= "rptBillFormat5_6.dsx":058A;
			//End Unmaped Properties
		}
	}
}
