﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptCommitment.
	/// </summary>
	public partial class rptCommitment : BaseSectionReport
	{
		public rptCommitment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Commitment Book";
		}

		public static rptCommitment InstancePtr
		{
			get
			{
				return (rptCommitment)Sys.GetInstance(typeof(rptCommitment));
			}
		}

		protected rptCommitment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCommit.Dispose();
				clsRef1.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCommitment	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int TaxYear;
		clsDRWrapper clsCommit = new clsDRWrapper();
		string Addr1 = "";
		string Addr2 = "";
		string addr3 = "";
		private string strMailingAddress3 = "";
		string strField10 = "";
		int intStartPage;
		int intPage;
		clsExempt[] exemptarray = null;
		double lngLandSub;
		double lngBldgSub;
		double lngExemptsub;
		double lngAssessSub;
		double lngTaxSub;
		clsDRWrapper clsRef1 = new clsDRWrapper();
		const int CNSTACCOUNTORDER = 1;
		const int CNSTNAMEORDER = 2;
		const int CNSTMAPLOTORDER = 3;

        private void ActiveReport_DataInitialize(object sender, EventArgs e)
        {
			using (clsDRWrapper clsTemp = new clsDRWrapper())
			{ 
				int x;
				lngLandSub = 0;
				lngBldgSub = 0;
				lngExemptsub = 0;
				lngAssessSub = 0;
				lngTaxSub = 0;
				Fields.Add("landval");
				Fields.Add("bldgval");
				Fields.Add("exemptval");
				Fields.Add("Totalval");
				Fields.Add("TaxVal");
				this.Fields.Add("TheBinder");
				// Set up an array of exemption records so we don't hit the database for every bill record
				clsTemp.OpenRecordset("select max(code) as maxcode from exemptcode", "twre0000.vb1");
				// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
				exemptarray = new clsExempt[FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxcode"))) + 1];
				// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
				for (x = 0; x <= FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxcode"))); x++)
				{
					exemptarray[x] = new clsExempt();
					exemptarray[x].Description = "Invalid Code";
					exemptarray[x].ShortDescription = "Bad Code";
					exemptarray[x].Unit = 0;
				}

				// x
				clsTemp.OpenRecordset("select * from exemptcode WHERE CODE > 0 ORDER BY CODE", "twre0000.vb1");
				while (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					x = FCConvert.ToInt32(clsTemp.Get_Fields("code"));
					exemptarray[x] = new clsExempt();
					exemptarray[x].Description = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
					exemptarray[x].ShortDescription =
						Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("shortdescription")));
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					exemptarray[x].Unit = clsTemp.Get_Fields_Int32("amount");
					clsTemp.MoveNext();
				}
			}
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsCommit.EndOfFile();
			if (!eArgs.EOF)
			{
				FillValueFields();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
			intPage = intStartPage;
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			int intTemp = 0;
			bool boolAlreadyFound = false;
			string strTemp = "";
			if (!clsCommit.EndOfFile())
			{
				// boolAlreadyFound = False
				// If Not clsRef1.EndOfFile Then
				// If clsRef1.Fields("rsaccount") = clsCommit.Fields("account") Then boolAlreadyFound = True
				// End If
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				if (Conversion.Val(clsCommit.Get_Fields("streetnumber")) > 0)
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtLocation.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
				}
				else
				{
					txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("apt"))) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
				}
				txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("maplot")));
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = clsCommit.Get_Fields_String("account");
				txtName.Text = clsCommit.Get_Fields_String("name1");
				Addr1 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name2")));
				Addr2 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address1")));
				addr3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address2")));
				strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("MailingAddress3")));
				strField10 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address3")));
				if (Addr1 == string.Empty && Addr2 == string.Empty && addr3 == string.Empty && strField10 == string.Empty)
				{
					if (!boolAlreadyFound)
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						if (clsRef1.FindFirstRecord("rsaccount", clsCommit.Get_Fields("account")))
						{
							boolAlreadyFound = true;
						}
					}
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					clsRef1.OpenRecordset("select rsaccount,p.FullNameLF as OwnerFullNameLF, q.FullNameLF as SecOwnerFullNameLF ,rsref1,Address1,Address2,Address3, City,PartyState,Zip,RSLOCnumalph,rslocstreet,rsmaplot from master as c CROSS APPLY " + clsRef1.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.OwnerPartyID,null,null,null) as p CROSS APPLY " + clsRef1.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.SecOwnerPartyID) as q where rsaccount = " + clsCommit.Get_Fields("account") + " and rsdeleted <> 1 and rscard = 1 order by p.FullNameLF", "twre0000.vb1");
					if (!clsRef1.EndOfFile())
					{
						// TODO Get_Fields: Field [SecOwnerFullNameLF] not found!! (maybe it is an alias?)
						Addr1 = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields("SecOwnerFullNameLF")));
						Addr2 = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("Address1")));
						addr3 = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("Address2")));
						strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("address3")));
						// TODO Get_Fields: Field [PartyState] not found!! (maybe it is an alias?)
						strField10 = Strings.Trim(Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields("PartyState"))) + " " + Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("Zip"))));
						txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("rslocnumalph"))) + " " + Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("rslocstreet"))));
						txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsRef1.Get_Fields_String("rsmaplot")));
					}
				}
				if (Strings.Trim(Addr1) == string.Empty)
				{
					Addr1 = Addr2;
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = strField10;
					strField10 = "";
					if (Strings.Trim(Addr1) == string.Empty)
					{
						Addr1 = Addr2;
						Addr2 = addr3;
						addr3 = "";
					}
				}
				if (Strings.Trim(Addr2) == string.Empty)
				{
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = strField10;
					strField10 = "";
				}
				if (Strings.Trim(strMailingAddress3) == string.Empty)
				{
					strMailingAddress3 = addr3;
					addr3 = strField10;
					strField10 = "";
				}
				if (Strings.Trim(addr3) == string.Empty)
				{
					addr3 = strField10;
					strField10 = "";
				}
				txtAddress1.Text = Addr1;
				txtAddress2.Text = Addr2;
				txtMailingAddress3.Text = strMailingAddress3;
				txtAddress3.Text = addr3;
				Field10.Text = strField10;
				txtBookPage.Text = "";
				if (Strings.Trim(clsCommit.Get_Fields_String("bookpage")) != string.Empty)
				{
					txtBookPage.Text = Strings.Trim(clsCommit.Get_Fields_String("bookpage"));
					intTemp = Strings.InStr(1, txtBookPage.Text, Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"), CompareConstants.vbTextCompare);
					if (intTemp > 0)
					{
						txtBookPage.Text = Strings.Mid(txtBookPage.Text, 1, intTemp - 1);
					}
					// Else
					// If Not clsRef1.EndOfFile Then
					// If Not boolAlreadyFound Then
					// If clsRef1.FindNextRecord("rsaccount", clsCommit.Fields("account")) Then
					// txtBookPage.Text = Mid(Trim(clsRef1.Fields("rsref1")) & " ", 1, 26)
					// boolAlreadyFound = True
					// End If
					// Else
					// txtBookPage.Text = Mid(Trim(clsRef1.Fields("rsref1")) & " ", 1, 26)
					// End If
					// End If
				}
				txtAcres.Text = "";
				if (Conversion.Val(clsCommit.Get_Fields_Double("acres")) > 0)
				{
					// txtAcres.Text = Val(clsCommit.Fields("acres")) & " Acres"
					txtAcres.Text = Strings.Format(Conversion.Val(clsCommit.Get_Fields_Double("acres")), "0.00");
					lblAcres.Visible = true;
					if (modGlobalVariables.Statics.CustomizedInfo.ShowTreeGrowth)
					{
						lblSoft.Visible = true;
						lblMixed.Visible = true;
						lblHard.Visible = true;
						txtSoft.Visible = true;
						txtMixed.Visible = true;
						txtHard.Visible = true;
						txtSoftValue.Visible = true;
						txtMixedValue.Visible = true;
						txtHardValue.Visible = true;
						txtSoft.Text = Strings.Format(clsCommit.Get_Fields_Double("TGSoftAcres"), "0.00");
						txtMixed.Text = Strings.Format(clsCommit.Get_Fields_Double("TGMixedAcres"), "0.00");
						txtHard.Text = Strings.Format(clsCommit.Get_Fields_Double("TGHardAcres"), "0.00");
						txtSoftValue.Text = Strings.Format(clsCommit.Get_Fields_Int32("TGSoftValue"), "#,###,##0");
						txtMixedValue.Text = Strings.Format(clsCommit.Get_Fields_Int32("TGMixedValue"), "#,###,##0");
						txtHardValue.Text = Strings.Format(clsCommit.Get_Fields_Int32("TGHardValue"), "#,###,##0");
					}
					else
					{
						lblSoft.Visible = false;
						lblMixed.Visible = false;
						lblHard.Visible = false;
						txtSoft.Visible = false;
						txtMixed.Visible = false;
						txtHard.Visible = false;
						txtSoftValue.Visible = false;
						txtMixedValue.Visible = false;
						txtHardValue.Visible = false;
					}
				}
				else
				{
					lblAcres.Visible = false;
					lblSoft.Visible = false;
					lblMixed.Visible = false;
					lblHard.Visible = false;
					txtSoft.Visible = false;
					txtMixed.Visible = false;
					txtHard.Visible = false;
					txtSoftValue.Visible = false;
					txtMixedValue.Visible = false;
					txtHardValue.Visible = false;
				}
				txtRef1.Text = "";
				if (modGlobalVariables.Statics.CustomizedInfo.ShowRef1)
				{
					txtRef1.Visible = true;
					txtRef1.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("ref1")));
				}
				else
				{
					txtRef1.Visible = false;
				}
				txtRef2.Text = "";
				if (modGlobalVariables.Statics.CustomizedInfo.ShowRef2)
				{
					txtRef2.Visible = true;
					txtRef2.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("ref2")));
				}
				else
				{
					txtRef2.Visible = false;
				}
				txtPayment1.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue1"), "#,##0.00") + " (1)";
				txtPayment2.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue2"), "#,##0.00") + " (2)";
				txtPayment3.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue3"), "#,##0.00") + " (3)";
				txtPayment4.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue4"), "#,##0.00") + " (4)";
				if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) > 0)
				{
					txtPayment1.Visible = true;
					txtPayment2.Visible = true;
					if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) > 0)
					{
						txtPayment3.Visible = true;
						if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4")) > 0)
						{
							txtPayment4.Visible = true;
						}
						else
						{
							txtPayment4.Visible = false;
						}
					}
					else
					{
						txtPayment3.Visible = false;
						txtPayment4.Visible = false;
					}
				}
				else
				{
					txtPayment1.Visible = false;
					txtPayment2.Visible = false;
					txtPayment3.Visible = false;
					txtPayment4.Visible = false;
				}
				txtExempt1.Text = "";
				txtExempt2.Text = "";
				txtExempt3.Text = "";
				if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt1")) > 0)
				{
					strTemp = Strings.Format(clsCommit.Get_Fields_Int32("exempt1"), "00") + " ";
					if (Information.UBound(exemptarray, 1) >= Conversion.Val(clsCommit.Get_Fields_Int32("exempt1")))
					{
						strTemp += exemptarray[clsCommit.Get_Fields_Int32("exempt1")].Description;
					}
					else
					{
						strTemp += exemptarray[0].Description;
					}
					txtExempt1.Text = strTemp;
				}
				// If exemptarray(clsCommit.Fields("exempt1")).Unit = 0 Then
				// Else
				if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt2")) > 0)
				{
					strTemp = Strings.Format(clsCommit.Get_Fields_Int32("exempt2"), "00") + " ";
					if (Information.UBound(exemptarray, 1) >= Conversion.Val(clsCommit.Get_Fields_Int32("exempt2")))
					{
						strTemp += exemptarray[clsCommit.Get_Fields_Int32("exempt2")].Description;
					}
					else
					{
						strTemp += exemptarray[0].Description;
					}
					txtExempt2.Text = strTemp;
				}
				// End If
				if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt3")) > 0)
				{
					strTemp = Strings.Format(clsCommit.Get_Fields_Int32("exempt3"), "00") + " ";
					if (Information.UBound(exemptarray, 1) >= Conversion.Val(clsCommit.Get_Fields_Int32("exempt3")))
					{
						strTemp += exemptarray[clsCommit.Get_Fields_Int32("exempt3")].Description;
					}
					else
					{
						strTemp += exemptarray[0].Description;
					}
					txtExempt3.Text = strTemp;
				}
				// End If
				// to fix a bizarre printing problem, going to add spaces to fix problems
				// If Len(txtExempt.Text) < 10 Then
				// txtExempt.Text = "  " & txtExempt.Text
				// End If
				// If Len(txtBldg.Text) < 10 Then
				// txtBldg.Text = "  " & txtBldg.Text
				// End If
				// If Len(txtLand.Text) < 10 Then
				// txtLand.Text = "  " & txtLand.Text
				// End If
				// If Len(txtTotal.Text) < 10 Then
				// txtTotal.Text = "  " & txtTotal.Text
				// End If
				// If Len(txtTax.Text) < 10 Then
				// txtTax.Text = "  " & txtTax.Text
				// End If
				// If Len(txtPayment1.Text) < 10 Then
				// txtPayment1.Text = "  " & txtPayment1.Text
				// End If
				// If Len(txtPayment2.Text) < 10 Then
				// txtPayment2.Text = "  " & txtPayment2.Text
				// End If
				// If Len(txtPayment3.Text) < 10 Then
				// txtPayment3.Text = "  " & txtPayment3.Text
				// End If
				// If Len(txtPayment4.Text) < 10 Then
				// txtPayment4.Text = "  " & txtPayment4.Text
				// End If
				clsCommit.MoveNext();
			}
		}
		// vbPorter upgrade warning: intPageStart As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intWhichOrder As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngTaxYear, bool boolSetFont, string strFontName, string strPrinterName, int intPageStart, bool boolNewPage, int lngRateKey, bool boolFromSupplemental = false, int intWhichOrder = 2, string strStart = "", string strEnd = "")
		{
			int x;
			string strWhereClause;
			clsRateRecord clsrate = new clsRateRecord();
			bool boolDup;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strOrderBy = "";
			string strRange;
			strRange = "";
			switch ((intWhichOrder))
			{
				case CNSTACCOUNTORDER:
					{
						strOrderBy = "account";
						if (Strings.Trim(strStart) != string.Empty || Strings.Trim(strEnd) != string.Empty)
						{
							if (Strings.Trim(strStart) == string.Empty)
							{
								strRange = " and account <= " + FCConvert.ToString(Conversion.Val(strEnd));
							}
							else if (Strings.Trim(strEnd) == string.Empty)
							{
								strRange = " and account >= " + FCConvert.ToString(Conversion.Val(strStart));
							}
							else
							{
								strRange = " and account between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd));
							}
						}
						break;
					}
				case CNSTNAMEORDER:
					{
						strOrderBy = "name1";
						if (Strings.Trim(strStart) != string.Empty || Strings.Trim(strEnd) != string.Empty)
						{
							if (Strings.Trim(strStart) == string.Empty)
							{
								strRange = " and name1 <= '" + Strings.Trim(strEnd) + "' ";
							}
							else if (Strings.Trim(strEnd) == string.Empty)
							{
								strRange = " and name1 >= '" + Strings.Trim(strStart) + "'";
							}
							else
							{
								strRange = " and name1 between '" + Strings.Trim(strStart) + "' and '" + Strings.Trim(strEnd) + "' ";
							}
						}
						break;
					}
				case CNSTMAPLOTORDER:
					{
						strOrderBy = "maplot";
						if (Strings.Trim(strStart) != string.Empty || Strings.Trim(strEnd) != string.Empty)
						{
							if (Strings.Trim(strStart) == string.Empty)
							{
								strRange = " and maplot <= '" + Strings.Trim(strEnd) + "' ";
							}
							else if (Strings.Trim(strEnd) == string.Empty)
							{
								strRange = " and maplot >= '" + Strings.Trim(strStart) + "' ";
							}
							else
							{
								strRange = " and maplot between '" + Strings.Trim(strStart) + "' and '" + Strings.Trim(strEnd) + "' ";
							}
						}
						break;
					}
			}
			//end switch
			boolDup = false;
			clsLoad.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("duplexcommitment")))
					boolDup = true;
			}
			TaxYear = lngTaxYear;
			intStartPage = intPageStart;
			if (boolNewPage)
			{
				this.GroupHeader1.DataField = "TheBinder";
			}
			clsrate.LoadRate(lngRateKey);
			lblDescription.Text = clsrate.Description;
			strWhereClause = "";
			if (boolFromSupplemental)
			{
				for (x = 1; x <= frmSupplementalBill.InstancePtr.ListGrid.Rows - 1; x++)
				{
					if (Strings.UCase(Strings.Trim(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 0))) == "RE")
					{
						if (Conversion.Val(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 4)) == lngRateKey)
						{
							if (strWhereClause != "")
							{
								strWhereClause += ",";
							}
							else
							{
								strWhereClause = " ID in (";
							}
							strWhereClause += FCConvert.ToString(Conversion.Val(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 6)));
						}
					}
				}
				// x
				strWhereClause += ")";
				clsCommit.OpenRecordset("select  * from billingmaster where " + strWhereClause + " and billingtype = 'RE'  order by " + strOrderBy, "twcl0000.vb1");
			}
			else
			{
				clsCommit.OpenRecordset("select * from billingmaster where ratekey  = " + FCConvert.ToString(lngRateKey) + " and billingtype = 'RE' " + strRange + " order by " + strOrderBy, "twcl0000.vb1");
			}
			// Call clsRef1.OpenRecordset("select rsaccount,p.FullNameLF as OwnerFullNameLF, q.FullNameLF as SecOwnerFullNameLF ,rsref1,Address1,Address2,Address3, City,PartyState,Zip,RSLOCnumalph,rslocstreet,rsmaplot from master as c CROSS APPLY " & clsRef1.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(c.OwnerPartyID,null,null,null) as p CROSS APPLY " & clsRef1.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(c.SecOwnerPartyID) as q where rsdeleted <> 1 and rscard = 1 order by p.FullNameLF", "twre0000.vb1")
			if (!clsCommit.EndOfFile())
			{
				// CHANGE BY REQUEST OF RON BUG ID 4260 8/24/2004
				lblTitle.Text = lblTitle.Text + Strings.Format(clsrate.TaxRate * 1000, "0.000");
				lblMuniname.Text = modGlobalConstants.Statics.MuniName;
				if (boolSetFont)
				{
					for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
					{
						// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
						bool setFont = false;
						bool bold = false;
						if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
						{
							setFont = true;
						}
						else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
						{
							setFont = true;
							bold = true;
						}
						if (setFont)
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textBox != null)
							{
								textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
								}
							}
						}
					}
					// x
					for (x = 0; x <= this.PageFooter.Controls.Count - 1; x++)
					{
						// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
						bool setFont = false;
						bool bold = false;
						if (FCConvert.ToString(this.PageFooter.Controls[x].Tag) == "textbox")
						{
							setFont = true;
						}
						else if (FCConvert.ToString(this.PageFooter.Controls[x].Tag) == "bold")
						{
							setFont = true;
							bold = true;
						}
						if (setFont)
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.PageFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textBox != null)
							{
								textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = this.PageFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
								}
							}
						}
					}
					// x
					//this.Printer.RenderMode = 1;
				}
				this.Document.Printer.PrinterName = strPrinterName;
				if (!boolFromSupplemental)
				{
					// Me.Show , MDIParent
					frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName, boolDuplex: boolDup, strAttachmentName: "Commitment");
				}
				else
				{
					frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName, boolDuplex: boolDup, strAttachmentName: "Commitment");
				}
			}
			else
			{
				// If Not boolFromSupplemental Then
				// MDIParent.Show
				// End If
				this.Close();
			}
			clsLoad.Dispose();
            return;
		}

		private void PageFooter_Format(object sender, EventArgs e)
		{
			if (Conversion.Val(txtLandTot.Text) > 0)
			{
				lngLandSub += Conversion.Val(txtLandTot.Text);
			}
			if (Conversion.Val(txtBldgTot.Text) > 0)
			{
				lngBldgSub += Conversion.Val(txtBldgTot.Text);
			}
			if (Conversion.Val(txtExemptTot.Text) > 0)
			{
				lngExemptsub += Conversion.Val(txtExemptTot.Text);
			}
			if (Conversion.Val(txtTotTot.Text) > 0)
			{
				lngAssessSub += Conversion.Val(txtTotTot.Text);
			}
			if (Conversion.Val(txtTaxTot.Text) > 0)
			{
				lngTaxSub += FCConvert.ToDouble(txtTaxTot.Text);
			}
			txtLandTot.Text = Strings.Format(txtLandTot.Text, "#,###,###,##0");
			txtBldgTot.Text = Strings.Format(txtBldgTot.Text, "#,###,###,##0");
			txtExemptTot.Text = Strings.Format(txtExemptTot.Text, "#,###,###,##0");
			txtTotTot.Text = Strings.Format(txtTotTot.Text, "#,###,###,##0");
			txtTaxTot.Text = Strings.Format(txtTaxTot.Text, "#,###,###,##0.00");
			txtLandSub.Text = Strings.Format(lngLandSub, "#,###,###,##0");
			txtBldgSub.Text = Strings.Format(lngBldgSub, "#,###,###,##0");
			txtExemptSub.Text = Strings.Format(lngExemptsub, "#,###,###,##0");
			txtTotalSub.Text = Strings.Format(lngAssessSub, "#,###,###,##0");
			txtTaxSub.Text = Strings.Format(lngTaxSub, "#,###,###,##0.00");
			if (clsCommit.EndOfFile())
			{
				lblSubtotals.Text = "Final Totals:";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = intPage.ToString();
			intPage += 1;
		}

		private void FillValueFields()
		{
			if (Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name1"))) != string.Empty)
			{
				this.Fields["TheBinder"].Value = Strings.Left(FCConvert.ToString(clsCommit.Get_Fields_String("name1")), 1);
			}
			else
			{
				this.Fields["TheBinder"].Value = "";
			}
			this.Fields["landval"].Value = Strings.Format(Conversion.Val(clsCommit.Get_Fields("landvalue")), "#,###,###,##0");
			this.Fields["bldgval"].Value = Strings.Format(Conversion.Val(clsCommit.Get_Fields("buildingvalue")), "#,###,###,##0");
			this.Fields["exemptval"].Value = Strings.Format(Conversion.Val(clsCommit.Get_Fields("exemptvalue")), "#,###,###,##0");
			this.Fields["totalval"].Value = Strings.Format(Conversion.Val(clsCommit.Get_Fields("landvalue")) + Conversion.Val(clsCommit.Get_Fields("buildingvalue")) - Conversion.Val(clsCommit.Get_Fields("exemptvalue")), "#,##0");
			// Me.Fields("totalval") = Val(clsCommit.Fields("landvalue")) + Val(clsCommit.Fields("buildingvalue"))
			this.Fields["taxval"].Value = Strings.Format(Conversion.Val(clsCommit.Get_Fields("taxdue1")) + Conversion.Val(clsCommit.Get_Fields("taxdue2")) + Conversion.Val(clsCommit.Get_Fields("taxdue3")) + Conversion.Val(clsCommit.Get_Fields("taxdue4")), "#,##0.00");
		}

		
	}
}
