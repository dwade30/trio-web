﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBLCustomReport.
	/// </summary>
	partial class rptBLCustomReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBLCustomReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Height = 0.1458333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line1
			});
			this.ReportFooter.Height = 0.1770833F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Visible = false;
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.txtMuniname,
				this.Label9,
				this.txtTime,
				this.txtDate
			});
			this.PageHeader.Height = 0.4166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			this.PageFooter.Visible = false;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.3125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 1.0625F;
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.19F;
			this.txtMuniname.Left = 0F;
			this.txtMuniname.MultiLine = false;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "text-align: left";
			this.txtMuniname.Text = "Field1";
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 1.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.21875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.6875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label9.Text = "Custom Report";
			this.Label9.Top = 0F;
			this.Label9.Width = 4F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "text-align: left";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.3125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.08333334F;
			this.Line1.LineWeight = 2F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.08333334F;
			this.Line1.Width = 7.333333F;
			this.Line1.X1 = 0.08333334F;
			this.Line1.X2 = 7.416667F;
			this.Line1.Y1 = 0.08333334F;
			this.Line1.Y2 = 0.08333334F;
			// 
			// rptBLCustomReport
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
