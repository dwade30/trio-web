﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormatA.
	/// </summary>
	public partial class rptBillFormatA : FCSectionReport
	{
		public rptBillFormatA()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Format A";
		}

		public static rptBillFormatA InstancePtr
		{
			get
			{
				return (rptBillFormatA)Sys.GetInstance(typeof(rptBillFormatA));
			}
		}

		protected rptBillFormatA _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillFormatA	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// WRITTEN BY     :               Corey Gray              *
		/// </summary>
		/// <summary>
		/// DATE           :                                       *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// MODIFIED BY    :               Jim Bertolino           *
		/// </summary>
		/// <summary>
		/// LAST UPDATED   :               07/29/2004              *
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		bool boolFirstBill;
		clsDRWrapper clsMortgageHolders = new clsDRWrapper();
		clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
		clsDRWrapper clsRateRecs = new clsDRWrapper();
		clsBillFormat clsTaxBillFormat = new clsBillFormat();
		clsRateRecord clsTaxRate = new clsRateRecord();
		double TaxRate;
		int intStartPage;
		clsDRWrapper clsBills = new clsDRWrapper();
		bool boolRE;
		bool boolPP;
		double Tax1;
		double Tax2;
		double Tax3;
		double Tax4;
		// vbPorter upgrade warning: Prepaid As double	OnWrite(double, string)
		double Prepaid;
		double DistPerc1;
		double DistPerc2;
		double DistPerc3;
		double DistPerc4;
		double DistPerc5;
		int intDistCats;
		double dblDiscountPercent;
		string strCat1 = "";
		string strCat2 = "";
		string strCat3 = "";
		int lngExempt;
		// vbPorter upgrade warning: lngTotAssess As int	OnWrite(short, double)
		int lngTotAssess;
		string strMap = "";
		string strLot = "";
		string strSub = "";
		string strType = "";
		bool boolNotJustUnloading;
		bool boolShownModally;
		bool boolReprintNewOwnerCopy;
		/// <summary>
		/// this will be set to true if there is a new owner and the recordset will not be advanced to create a copy for him
		/// </summary>
		int intCutoff;
		bool boolPrintRecipientCopy;
		clsDRWrapper rsMultiRecipients = new clsDRWrapper();
		// vbPorter upgrade warning: intYear As short	OnWrite(double, int)
		public void Init(clsBillFormat clsTaxFormat, int intYear, string strFontName, bool boolShow = true, string strPrinterN = "", bool boolModal = false)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			string strSQL;
			boolShownModally = boolModal;
			boolNotJustUnloading = true;
			if (strPrinterN != string.Empty)
			{
				this.Document.Printer.PrinterName = strPrinterN;
			}
			if (clsTaxFormat.BillsFrom == 0)
			{
				boolRE = true;
				if (clsTaxFormat.Combined)
				{
					boolPP = true;
				}
				else
				{
					boolPP = false;
				}
			}
			else
			{
				boolRE = false;
				boolPP = true;
			}
			if (!boolPP)
			{
				txtCatOther.Visible = false;
			}
			if (!boolRE)
			{
				txtBookPage.Visible = false;
				txtBookPageLabel.Visible = false;
				txtLand.Visible = false;
				txtBldg.Visible = false;
			}
			clsTaxBillFormat.CopyFormat(ref clsTaxFormat);
			if (boolRE && clsTaxBillFormat.PrintCopyForMortgageHolders)
			{
				boolFirstBill = true;
				clsMortgageHolders.OpenRecordset("select * from mortgageholders order by ID", "CentralData");
				clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' order by ACCOUNT", "CentralData");
				if (!clsMortgageAssociations.EndOfFile())
				{
					clsMortgageAssociations.MoveFirst();
					clsMortgageAssociations.MovePrevious();
				}
			}
			else
			{
				boolFirstBill = true;
			}
			strSQL = clsTaxBillFormat.SQLStatement;
			clsTaxRate.TaxYear = intYear;
			lblTitle.Text = FCConvert.ToString(intYear);
			lblTitle1.Text = FCConvert.ToString(intYear);
			clsRateRecs.OpenRecordset("select * from raterec where year = " + FCConvert.ToString(intYear), "twcl0000.vb1");
			if (!clsRateRecs.EndOfFile())
			{
				clsTaxRate.LoadRate(clsRateRecs.Get_Fields_Int32("ID"));
			}
			clsTemp.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
			while (!clsTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				switch (FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("line"))))
				{
					case 0:
						{
							txtMessage1.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 1:
						{
							txtMessage2.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 2:
						{
							txtMessage3.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 3:
						{
							txtMessage4.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 4:
						{
							txtMessage5.Text = clsTemp.Get_Fields_String("message");
							break;
						}
					case 5:
						{
							txtMessage6.Text = clsTemp.Get_Fields_String("message");
							break;
						}
				}
				//end switch
				clsTemp.MoveNext();
			}
			if (clsTaxBillFormat.Breakdown)
			{
				// DistFrame.Visible = True
				txtAccount.Top = 1890 / 1440f;
				// move out of the way of distperc1
				txtDist1.Visible = true;
				txtDist2.Visible = true;
				txtDist3.Visible = true;
				txtDist4.Visible = true;
				txtDistPerc1.Visible = true;
				txtDistPerc2.Visible = true;
				txtDistPerc3.Visible = true;
				txtDistPerc4.Visible = true;
				clsTemp.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					intDistCats = clsTemp.RecordCount();
					DistPerc1 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
					txtDistPerc1.Text = Strings.Format(DistPerc1, "#0.00") + "%";
					DistPerc1 /= 100;
					strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
					txtDist1.Text = Strings.Mid(strTemp, 1, 11);
					if (intDistCats > 1)
					{
						clsTemp.MoveNext();
						txtDistPerc2.Visible = true;
						txtDist2.Visible = true;
						DistPerc2 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
						txtDistPerc2.Text = Strings.Format(DistPerc2, "#0.00") + "%";
						DistPerc2 /= 100;
						strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
						txtDist2.Text = Strings.Mid(strTemp, 1, 11);
						if (intDistCats > 2)
						{
							clsTemp.MoveNext();
							txtDistPerc3.Visible = true;
							txtDist3.Visible = true;
							DistPerc3 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
							txtDistPerc3.Text = Strings.Format(DistPerc3, "#0.00") + "%";
							DistPerc3 /= 100;
							strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
							txtDist3.Text = Strings.Mid(strTemp, 1, 11);
							if (intDistCats > 3)
							{
								clsTemp.MoveNext();
								txtDistPerc4.Visible = true;
								txtDist4.Visible = true;
								DistPerc4 = Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent"));
								txtDistPerc4.Text = Strings.Format(DistPerc4, "#0.00") + "%";
								DistPerc4 /= 100;
								strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("distributionname"));
								txtDist4.Text = Strings.Mid(strTemp, 1, 11);
							}
						}
					}
				}
				else
				{
					intDistCats = 0;
				}
			}
			if ((clsTaxRate.NumberOfPeriods < 2) || (clsTaxBillFormat.Billformat == 9))
			{
				txtAmount2.Visible = false;
				txtDueDate2.Visible = false;
				txtTaxDue2Copy.Visible = false;
			}
			else
			{
				txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
				txtAmount2.Visible = true;
				txtDueDate2.Visible = true;
				if (clsTaxRate.NumberOfPeriods > 2 && (Strings.UCase(modGlobalConstants.Statics.MuniName) == "CAMDEN" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "RUMFORD"))
				{
					txtTaxDue2Copy.Visible = true;
				}
				else
				{
					txtTaxDue2Copy.Visible = false;
				}
			}
			txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
			intCutoff = 0;
			if (strFontName != string.Empty)
			{
				// now set each box and label to the correct printer font
				for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
				{
					// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
					bool setFont = false;
					bool bold = false;
					if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
					{
						setFont = true;
					}
					else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
					{
						setFont = true;
						bold = true;
					}
					if (setFont)
					{
						GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
						if (textBox != null)
						{
							textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
						}
						else
						{
							GrapeCity.ActiveReports.SectionReportModel.Label label = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
							if (label != null)
							{
								label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
						}
					}
				}
				// x
				//this.Printer.RenderMode = 1;
				intCutoff = 23;
			}
			frm7Message.InstancePtr.Init(clsTaxBillFormat.Billformat, clsTaxRate.InterestRate, clsTaxRate.Get_InterestStartDate(1));
			txtLateDate.Text = modGlobalVariables.Statics.gstrPaidDate;
			txtPercent.Text = modGlobalVariables.Statics.gstrPercent;
			GetBills(ref strSQL);
			if (boolShow)
			{
				// Me.Show , MDIParent
			}
			else
			{
				this.PrintReport(false);
			}
		}

		private void GetBills(ref string strSQL)
		{
			clsBills.OpenRecordset(strSQL, "twcl0000.vb1");
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int intReturn = 0;
			
			modCustomPageSize.CheckDefaultPrinter(this.Document.Printer.PrinterName);
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("BillFormatA", 850, 550);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsBills.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			clsDRWrapper clsAddress = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
			if (clsTaxBillFormat.HasDefaultMargin)
			{
				this.PageSettings.Margins.Left = 0;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			modCustomPageSize.Statics.boolChangedDefault = true;
			modCustomPageSize.ResetDefaultPrinter();
			if (!boolShownModally && boolNotJustUnloading)
			{
				//MDIParent.InstancePtr.Show();
			}
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	// do this since we already chose the printer and don't want to choose again
		//	// now we have to handle printing the pages ourselves though.
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "Print...")
		//	{
		//		// Call frmNumPages.Init(Me.Pages.Count, 1)
		//		// frmNumPages.Show vbModal, MDIParent
		//		// Me.Printer.FromPage = gintStartPage
		//		// Me.Printer.ToPage = gintEndPage
		//		// Me.PrintReport (False)
		//		clsPrint.ReportPrint(this);
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			double Dist1;
			double Dist2;
			double Dist3;
			double Dist4;
			double Dist5;
			// vbPorter upgrade warning: TotTax As double	OnWrite(string)
			double TotTax = 0;
			string strTemp = "";
			string strAcct = "";
			DateTime dtLastDate;
			string strOldName = "";
			string strSecOwner = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			string strLocation = "";
			if (!clsBills.EndOfFile())
			{
				strMap = "";
				strLot = "";
				strSub = "";
				strType = "";
				if (FCConvert.ToString(clsBills.Get_Fields_String("Billingtype")) == "RE")
				{
					boolRE = true;
					boolPP = false;
				}
				else if (clsBills.Get_Fields_String("Billingtype") == "PP")
				{
					boolRE = false;
					boolPP = true;
				}
				else
				{
					// combined
					boolRE = true;
					boolPP = true;
				}
				if (!boolPP)
				{
					txtCatOther.Visible = false;
				}
				if (!boolRE)
				{
					txtBookPage.Visible = false;
					txtBookPageLabel.Visible = false;
					txtLand.Visible = false;
					txtBldg.Visible = false;
				}
				if (clsRateRecs.FindFirstRecord("ID", clsBills.Get_Fields_Int32("ratekey")))
				{
					clsTaxRate.LoadRate(clsBills.Get_Fields_Int32("ratekey"), clsRateRecs);
					if ((clsTaxRate.NumberOfPeriods < 2) || (clsTaxBillFormat.Billformat == 9))
					{
						// framStub2.Visible = False
						// txtPayment2Label.Visible = False
						txtAmount2.Visible = false;
						txtDueDate2.Visible = false;
						txtTaxDue2Copy.Visible = false;
					}
					else
					{
						// framStub2.Visible = True
						txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
						txtAmount2.Visible = true;
						txtDueDate2.Visible = true;
						// txtPayment2Label.Visible = True
						if (clsTaxRate.NumberOfPeriods > 1 && Strings.UCase(modGlobalConstants.Statics.MuniName) == "CAMDEN")
						{
							txtTaxDue2Copy.Visible = true;
						}
						else
						{
							txtTaxDue2Copy.Visible = false;
						}
					}
					if (clsTaxBillFormat.Billformat == 9)
					{
						// can move the tax message higher and show all 6 lines
						txtMessage2.Top = 6120 / 1440f;
						txtMessage6.Visible = true;
					}
					else
					{
						txtMessage2.Top = 6615 / 1440f;
						txtMessage6.Visible = false;
					}
					txtMessage3.Top = txtMessage2.Top + 225;
					txtMessage4.Top = txtMessage3.Top + 225;
					txtMessage5.Top = txtMessage4.Top + 225;
					txtMessage6.Top = txtMessage5.Top + 225;
					txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
				}
				lngTotAssess = 0;
				lngExempt = 0;
				Tax1 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue1"));
				Tax2 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue2"));
				Tax3 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue3"));
				Tax4 = Conversion.Val(clsBills.Get_Fields_Decimal("taxdue4"));
				TotTax = FCConvert.ToDouble(Strings.Format(Tax1 + Tax2 + Tax3 + Tax4, "0.00"));
				txtTaxDue.Text = Strings.Format(TotTax, "#,###,###,##0.00");
				txtTaxDueCopy.Text = txtTaxDue.Text;
				Prepaid = Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid"));
				double dblAbated = 0;
				double dblAbate1 = 0;
				double dblAbate2 = 0;
				double dblAbate3 = 0;
				double dblAbate4 = 0;
				int intPer;
				double[] dblTAbate = new double[4 + 1];
				dblAbated = 0;
				if ((Tax2 > 0 || Tax3 > 0 || Tax4 > 0) && Prepaid > 0)
				{
					dblAbated = modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields_Int32("ID"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
					dblAbate1 = dblTAbate[1];
					dblAbate2 = dblTAbate[2];
					dblAbate3 = dblTAbate[3];
					dblAbate4 = dblTAbate[4];
					if (boolRE && boolPP)
					{
						// TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
						dblAbated += modCollectionsRelated.AutoAbatementAmount_8(clsBills.Get_Fields("ppbillkey"), clsTaxRate.NumberOfPeriods, ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
						dblAbate1 += dblTAbate[1];
						dblAbate2 += dblTAbate[2];
						dblAbate3 += dblTAbate[3];
						dblAbate4 += dblTAbate[4];
					}
					Prepaid -= dblAbated;
				}
				if (dblAbated > 0)
				{
					Tax1 -= dblAbate1;
					Tax2 -= dblAbate2;
					Tax3 -= dblAbate3;
					Tax4 -= dblAbate4;
				}
				if (Prepaid > 0)
				{
					if (Prepaid > Tax1)
					{
						Prepaid -= Tax1;
						Tax1 = 0;
						if (Prepaid > Tax2)
						{
							Prepaid -= Tax2;
							Tax2 = 0;
							if (Prepaid > Tax3)
							{
								Prepaid -= Tax3;
								Tax3 = 0;
								if (Prepaid > Tax4)
								{
									Prepaid -= Tax4;
									Tax4 = 0;
								}
								else
								{
									Tax4 -= Prepaid;
								}
							}
							else
							{
								Tax3 -= Prepaid;
							}
						}
						else
						{
							Tax2 -= Prepaid;
						}
					}
					else
					{
						Tax1 -= Prepaid;
					}
				}
				Prepaid = FCConvert.ToDouble(Strings.Format(Conversion.Val(clsBills.Get_Fields_Decimal("principalpaid")), "0.00"));
				txtPrePaid.Text = Strings.Format(Prepaid, "#,###,##0.00");
				txtTaxDue2Copy.Text = "";
				if (Prepaid == 0)
				{
					txtPrePaid.Visible = false;
					txtPrePaidLabel.Visible = false;
				}
				else
				{
					txtPrePaid.Visible = true;
					txtPrePaidLabel.Visible = true;
					if (Prepaid > TotTax)
					{
						// txtTaxDue.Text = "Overpaid"
						txtTaxDueCopy.Text = "Overpaid";
						txtTaxDue2Copy.Text = "OverPaid";
					}
					else
					{
						// txtTaxDue.Text = Format(TotTax - Prepaid, "#,###,###,##0.00")
						txtTaxDueCopy.Text = Strings.Format(TotTax - Prepaid, "#,###,###,##0.00");
					}
				}
				txtRate.Text = Strings.Format(clsTaxRate.TaxRate * 1000, "#0.000");
				txtAmount1.Text = Strings.Format(Tax1, "#,###,##0.00");
				if (Strings.UCase(txtTaxDueCopy.Text) != "OVERPAID")
				{
					txtTaxDueCopy.Text = txtAmount1.Text;
				}
				txtAmount2.Text = Strings.Format(Tax2, "#,###,##0.00");
				if (Strings.UCase(txtTaxDue2Copy.Text) != "OVERPAID")
				{
					txtTaxDue2Copy.Text = txtAmount2.Text;
				}
				txtDueDate1.Text = clsTaxRate.Get_DueDate(1).ToShortDateString();
				txtDueDate2.Text = clsTaxRate.Get_DueDate(2).ToShortDateString();
				// txtInterest1.Text = clsTaxRate.Get_InterestStartDate(1)
				// txtInterest2.Text = clsTaxRate.Get_InterestStartDate(2)
				// txtAmount3.Text = Format(Tax3, "#,###,##0.00")
				// txtAmount4.Text = Format(Tax4, "#,###,##0.00")
				// txtinterest3.Text = clsTaxRate.Get_InterestStartDate(3)
				// txtinterest4.Text = clsTaxRate.Get_InterestStartDate(4)
				// 
				// If clsTaxBillFormat.Discount Then
				// txtDiscountAmount.Text = Format(dblDiscountPercent * (Tax1 + Tax2 + Tax3 + Tax4), "#,###,##0.00")
				// txtDiscountAmount.Text = Format((Tax1 + Tax2 + Tax3 + Tax4), "#,###,##0.00")
				// End If
				// 
				strAcct = "";
				int intTemp = 0;
				if (boolRE)
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					strAcct = "R" + clsBills.Get_Fields("account");
					if (Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage"))) != string.Empty)
					{
						txtBookPage.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("bookpage")));
						txtBookPage.Visible = true;
						txtBookPageLabel.Visible = true;
					}
					else
					{
						txtBookPage.Visible = false;
						txtBookPageLabel.Visible = false;
					}
					// If Val(clsBills.Fields("book")) > 0 Then
					// txtBookPage.Visible = True
					// txtBookPageLabel.Visible = True
					// txtBookPage.Text = "B" & clsBills.Fields("book") & " P" & clsBills.Fields("page")
					// Else
					// If Trim(clsBills.Fields("rsref1")) <> vbNullString Then
					// If InStr(1, clsBills.Fields("rsref1"), "B", vbTextCompare) Then
					// txtBookPage.Visible = True
					// txtBookPageLabel.Visible = True
					// txtBookPage.Text = Trim(clsBills.Fields("rsref1"))
					// Else
					// txtBookPage.Visible = False
					// txtBookPageLabel.Visible = False
					// End If
					// Else
					// txtBookPage.Visible = False
					// txtBookPageLabel.Visible = False
					// End If
					// End If
					txtLand.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")), "#,###,###,##0");
					txtBldg.Text = Strings.Format(Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")), "#,###,###,##0");
					lngTotAssess = FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("landvalue")) + Conversion.Val(clsBills.Get_Fields_Int32("buildingvalue")));
					if (boolPrintRecipientCopy)
					{
						txtMailing1.Text = rsMultiRecipients.Get_Fields_String("Name");
						txtMailing2.Text = rsMultiRecipients.Get_Fields_String("address1");
						txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address2");
						txtMailing4.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
						txtMailing5.Text = "";
						if (intCutoff > 0 && txtMailing4.Text.Length > intCutoff)
						{
							txtMailing5.Text = rsMultiRecipients.Get_Fields_String("state") + " " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4");
							// intTemp = intCutoff - Len(txtMailing4.Text)
							// intTemp = intTemp - 1
							// txtMailing4.Text = Mid(rsMultiRecipients.Fields("city"), 1, intTemp) & " " & txtMailing4.Text
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(rsMultiRecipients.Get_Fields_String("city")));
						}
						// txtMailing5.Text = ""
						// ElseIf clsTaxBillFormat.UsePreviousOwnerInfo And NewOwner(clsBills.Fields("Account"), clsTaxBillFormat.BillAsOfDate, strOldName, strAddr1, strAddr2, strAddr3) Then
					}
					else if (!boolFirstBill && clsTaxBillFormat.PrintCopyForMortgageHolders)
					{
						clsMortgageHolders.FindFirstRecord("ID", clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"));
						if (!clsMortgageHolders.NoMatch)
						{
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing2.Text = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
							txtMailing5.Text = Strings.Trim(clsMortgageHolders.Get_Fields_String("city") + " " + clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
							if (intCutoff > 0 && txtMailing5.Text.Length > intCutoff)
							{
								if (Strings.Trim(txtMailing4.Text) == string.Empty)
								{
									txtMailing4.Text = rsMultiRecipients.Get_Fields_String("city");
									txtMailing5.Text = Strings.Trim(clsMortgageHolders.Get_Fields_String("state") + "  " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
								}
								else
								{
									intTemp = intCutoff - txtMailing5.Text.Length;
									intTemp -= 1;
									txtMailing5.Text = Strings.Mid(FCConvert.ToString(rsMultiRecipients.Get_Fields_String("city")), 1, intTemp) + " " + txtMailing5.Text;
								}
							}
						}
						else
						{
							txtMailing1.Text = "Mortgage Holder Not Found";
							txtMailing2.Text = "";
							txtMailing3.Text = "";
							txtMailing4.Text = "";
							txtMailing5.Text = "";
						}
					}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (boolReprintNewOwnerCopy && modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strAddr3))
					{
						// If boolReprintNewOwnerCopy Then
						// txtMailing2.Text = Trim(clsBills.Fields("rsaddr1"))
						// txtMailing3.Text = Trim(clsBills.Fields("rsaddr2"))
						// txtMailing4.Text = Trim(clsBills.Fields("rsaddr3")) & "  " & clsBills.Fields("rsstate") & "  " & clsBills.Fields("rszip")
						// If Trim(clsBills.Fields("rszip4")) <> vbNullString Then
						// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("rszip4"))
						// End If
						// txtMailing1.Text = Trim(clsBills.Fields("rsname"))
						// Else
						txtMailing3.Text = strAddr1;
						txtMailing4.Text = strAddr2;
						txtMailing5.Text = strAddr3;
						txtMailing1.Text = strOldName;
						txtMailing2.Text = strSecOwner;
						if (intCutoff > 0 && strAddr3.Length > intCutoff)
						{
							if (Strings.Trim(txtMailing4.Text) == string.Empty)
							{
								txtMailing4.Text = SplitCityState_8(Strings.Trim(strAddr3), false);
								txtMailing5.Text = SplitCityState_8(Strings.Trim(strAddr3), true);
							}
							else if (Strings.Trim(txtMailing3.Text) == string.Empty)
							{
								txtMailing3.Text = txtMailing4.Text;
								txtMailing4.Text = SplitCityState_8(Strings.Trim(strAddr3), false);
								txtMailing5.Text = SplitCityState_8(Strings.Trim(strAddr3), true);
							}
							else if (Strings.Trim(txtMailing2.Text) == string.Empty)
							{
								txtMailing2.Text = txtMailing3.Text;
								txtMailing3.Text = txtMailing4.Text;
								txtMailing4.Text = SplitCityState_8(Strings.Trim(strAddr3), false);
								txtMailing5.Text = SplitCityState_8(Strings.Trim(strAddr3), true);
							}
							else
							{
								txtMailing5.Text = CutOffAddress(ref strAddr3, ref intCutoff);
							}
						}
						// End If
					}
					else
					{
						txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
						txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name2")));
						txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
						txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
						txtMailing5.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
						if (intCutoff > 0 && Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))).Length > intCutoff)
						{
							if (Strings.Trim(txtMailing4.Text) == string.Empty)
							{
								txtMailing4.Text = SplitCityState_8(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), false);
								txtMailing5.Text = SplitCityState_8(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), true);
							}
							else if (Strings.Trim(txtMailing3.Text) == string.Empty)
							{
								txtMailing3.Text = txtMailing4.Text;
								txtMailing4.Text = SplitCityState_8(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), false);
								txtMailing5.Text = SplitCityState_8(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), true);
							}
							else if (Strings.Trim(txtMailing2.Text) == string.Empty)
							{
								txtMailing2.Text = txtMailing3.Text;
								txtMailing3.Text = txtMailing4.Text;
								txtMailing4.Text = SplitCityState_8(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), false);
								txtMailing5.Text = SplitCityState_8(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), true);
							}
							else
							{
								txtMailing5.Text = CutOffAddress_2(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), intCutoff);
							}
						}
						// If Trim(clsBills.Fields("rssecowner")) <> vbNullString Then
						// If Trim(txtMailing2.Text) = vbNullString Or Trim(txtMailing3.Text) = vbNullString Then
						// If Trim(txtMailing2.Text) <> vbNullString Then
						// txtMailing3.Text = txtMailing2.Text
						// End If
						// txtMailing2.Text = Trim(clsBills.Fields("rssecowner"))
						// 
						// End If
						// End If
						// txtMailing4.Text = Trim(clsBills.Fields("rsaddr3")) & "  " & clsBills.Fields("rsstate") & "  " & clsBills.Fields("rszip")
						// If Trim(clsBills.Fields("rszip4")) <> vbNullString Then
						// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("rszip4"))
						// End If
						// txtMailing1.Text = Trim(clsBills.Fields("name1"))
						// strName = Trim(clsBills.Fields("name1"))
						// txtMailing2.Text = Trim(clsBills.Fields("name2"))
					}
					if (txtMailing4.Text == "")
					{
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing3.Text == "")
					{
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					if (txtMailing2.Text == "")
					{
						txtMailing2.Text = txtMailing3.Text;
						txtMailing3.Text = txtMailing4.Text;
						txtMailing4.Text = txtMailing5.Text;
						txtMailing5.Text = "";
					}
					// txtLocation.Text = Trim(Trim(clsBills.Fields("rslocnumalph") & " " & clsBills.Fields("rslocapt")) & " " & clsBills.Fields("rslocstreet"))
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						strLocation = FCConvert.ToString(clsBills.Get_Fields("streetnumber"));
					}
					else
					{
						strLocation = "";
					}
					strLocation = Strings.Trim(Strings.Trim(strLocation + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
					txtLocation.Text = strLocation;
					modGlobalRoutines.ParseMapLot(clsBills.Get_Fields_String("maplot"), ref strMap, ref strLot, ref strSub, ref strType);
					if (Strings.Trim(strMap + strLot) != string.Empty)
					{
						txtMapLot.Text = Strings.Trim(strMap + "-" + strLot);
						if (Strings.Trim(strSub + strType) != string.Empty)
						{
							txtSubType.Text = Strings.Trim(strSub + "-" + strType);
						}
					}
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				}
				if (boolPP)
				{
					// pp
					if (boolRE)
					{
						// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBills.Get_Fields("ppac")) > 0)
						{
							strAcct += " P";
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							strAcct += clsBills.Get_Fields("ppac");
						}
					}
					else
					{
						strAcct = "P";
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						strAcct += clsBills.Get_Fields("account");
					}
					// txtCat1.Text = Format(Val(clsBills.Fields("cat1")), "#,###,###,##0")
					// txtCat2.Text = Format(Val(clsBills.Fields("cat2")), "#,###,###,##0")
					// txtCat3.Text = Format(Val(clsBills.Fields("cat3")), "#,###,###,##0")
					txtCatOther.Text = Strings.Format(clsBills.Get_Fields_Int32("ppaSSESSMENT"), "#,###,###,##0");
					lngTotAssess += FCConvert.ToInt32(Conversion.Val(clsBills.Get_Fields_Int32("ppassessment")));
					if (!boolRE)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						if (Conversion.Val(clsBills.Get_Fields("streetnumber")) > 0)
						{
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							strLocation = FCConvert.ToString(clsBills.Get_Fields("streetnumber"));
						}
						else
						{
							strLocation = "";
						}
						strLocation = Strings.Trim(Strings.Trim(strLocation + " " + clsBills.Get_Fields_String("apt")) + " " + clsBills.Get_Fields_String("streetname"));
						txtLocation.Text = strLocation;
						if (boolPrintRecipientCopy)
						{
							txtMailing1.Text = rsMultiRecipients.Get_Fields_String("Name");
							txtMailing2.Text = rsMultiRecipients.Get_Fields_String("address1");
							txtMailing3.Text = rsMultiRecipients.Get_Fields_String("address2");
							txtMailing4.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
							txtMailing5.Text = "";
							if (intCutoff > 0 && txtMailing4.Text.Length > intCutoff)
							{
								// txtMailing4.Text = rsMultiRecipients.Fields("state") & " " & rsMultiRecipients.Fields("zip") & " " & rsMultiRecipients.Fields("zip4")
								// intTemp = intCutoff - Len(txtMailing4.Text)
								// intTemp = intTemp - 1
								// txtMailing4.Text = Mid(rsMultiRecipients.Fields("city"), 1, intTemp) & " " & txtMailing4.Text
								txtMailing4.Text = Strings.Trim(FCConvert.ToString(rsMultiRecipients.Get_Fields_String("city")));
								txtMailing5.Text = Strings.Trim(rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
							}
						}
						else
						{
							txtMailing2.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address1")));
							txtMailing3.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address2")));
							txtMailing4.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3")));
							// strTemp = Trim(clsBills.Fields("zip"))
							// txtMailing4.Text = Trim(clsBills.Fields("city")) & "  " & clsBills.Fields("state") & "  " & strTemp
							// If Trim(clsBills.Fields("zip4")) <> vbNullString Then
							// txtMailing4.Text = txtMailing4.Text & "-" & Trim(clsBills.Fields("zip4"))
							// End If
							txtMailing1.Text = Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("name1")));
							txtMailing5.Text = "";
							if (intCutoff > 0 && Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))).Length > intCutoff)
							{
								txtMailing4.Text = SplitCityState_8(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), false);
								txtMailing5.Text = SplitCityState_8(Strings.Trim(FCConvert.ToString(clsBills.Get_Fields_String("address3"))), true);
								// txtMailing4.Text = CutOffAddress(Trim(clsBills.Fields("address3")), intCutoff)
							}
						}
						if (txtMailing3.Text == "")
						{
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = "";
						}
						if (txtMailing2.Text == "")
						{
							txtMailing2.Text = txtMailing3.Text;
							txtMailing3.Text = txtMailing4.Text;
							txtMailing4.Text = "";
						}
					}
					else
					{
					}
				}
				lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBills.Get_Fields_Int32("exemptvalue"))));
				txtValue.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
				txtExemption.Text = Strings.Format(lngExempt, "#,###,###,##0");
				txtAssessment.Text = Strings.Format(lngTotAssess - lngExempt, "#,###,###,##0");
				txtAccount.Text = strAcct;
				txtAcct1.Text = strAcct;
				// txtAcct2.Text = strAcct
				if (intCutoff > 0)
				{
					if (txtMailing1.Text.Length > intCutoff)
						txtMailing1.Text = Strings.Left(txtMailing1.Text, intCutoff);
					if (txtMailing2.Text.Length > intCutoff)
						txtMailing2.Text = Strings.Left(txtMailing2.Text, intCutoff);
					if (txtMailing3.Text.Length > intCutoff)
						txtMailing3.Text = Strings.Left(txtMailing3.Text, intCutoff);
					if (txtMailing4.Text.Length > intCutoff)
						txtMailing4.Text = Strings.Left(txtMailing4.Text, intCutoff);
					if (txtMailing5.Text.Length > intCutoff)
						txtMailing5.Text = Strings.Left(txtMailing5.Text, intCutoff);
				}
				if (boolRE)
				{
					if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
					{
						boolReprintNewOwnerCopy = false;
						// if the copy for a new owner needs to be sent, then check it here
						if (clsTaxBillFormat.PrintCopyForNewOwner)
						{
							// if this is the main bill, only check for the new owner on the mail bill
							if (boolFirstBill)
							{
								if (DateTime.Today.Month < 4)
								{
									// if the month is before april then use last years commitment date
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1));
								}
								else
								{
									dtLastDate = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));
								}
								// If NewOwner(clsBills.Fields("Account"), dtLastDate) Then     'if there has been a new owner
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modMain.HasNewOwner_8(clsBills.Get_Fields("Account"), clsBills.Get_Fields_String("name1")))
								{
									// if there has been a new owner
									boolReprintNewOwnerCopy = true;
								}
							}
						}
					}
					else if (!boolPrintRecipientCopy && boolFirstBill)
					{
						// turn off the copy so that the record can advance
						boolReprintNewOwnerCopy = false;
					}
					if (!boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							if (boolFirstBill)
							{
								rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
								if (!rsMultiRecipients.EndOfFile())
								{
									boolPrintRecipientCopy = true;
								}
							}
						}
					}
					else if (boolPrintRecipientCopy && boolFirstBill)
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (clsTaxBillFormat.PrintCopyForMortgageHolders && !boolPrintRecipientCopy && !boolReprintNewOwnerCopy)
					{
						if (!clsMortgageAssociations.EndOfFile())
						{
							if (boolFirstBill)
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindFirstRecord("account", clsBills.Get_Fields("account"));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsMortgageAssociations.FindNextRecord("account", clsBills.Get_Fields("account"));
							}
							// Call clsMortgageAssociations.FindNextRecord("account", clsBills.Fields("ac"))
							if (clsMortgageAssociations.NoMatch)
							{
								boolFirstBill = true;
								if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
									clsBills.MoveNext();
							}
							else
							{
								boolFirstBill = false;
							}
							// End If
						}
						else
						{
							boolFirstBill = true;
							if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
								clsBills.MoveNext();
						}
					}
					else
					{
						if (!boolReprintNewOwnerCopy && !boolPrintRecipientCopy)
							clsBills.MoveNext();
					}
				}
				else
				{
					if (!boolPrintRecipientCopy)
					{
						if (clsTaxBillFormat.PrintCopyForMultiOwners)
						{
							rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsBills.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
							if (!rsMultiRecipients.EndOfFile())
							{
								boolPrintRecipientCopy = true;
							}
						}
					}
					else
					{
						rsMultiRecipients.MoveNext();
						if (rsMultiRecipients.EndOfFile())
						{
							boolPrintRecipientCopy = false;
						}
					}
					if (!boolPrintRecipientCopy)
					{
						clsBills.MoveNext();
					}
				}
			}
		}
		// vbPorter upgrade warning: intCut As short	OnWriteFCConvert.ToInt32(
		private string CutOffAddress_2(string straddr, int intCut)
		{
			return CutOffAddress(ref straddr, ref intCut);
		}

		private string CutOffAddress(ref string straddr, ref int intCut)
		{
			string CutOffAddress = "";
			string strReturn;
			string strTemp;
			int intMaxCity;
			string strCity = "";
			CutOffAddress = straddr;
			strReturn = "";
			string[] strAry = null;
			int x;
			if (straddr.Length < intCut || intCut < 1)
				return CutOffAddress;
			strTemp = Strings.Replace(straddr, "  ", " ", 1, -1, CompareConstants.vbTextCompare);
			strAry = Strings.Split(strTemp, " ", -1, CompareConstants.vbTextCompare);
			if (Information.UBound(strAry, 1) < 2)
				return CutOffAddress;
			strReturn = strAry[Information.UBound(strAry, 1) - 1] + " " + strAry[Information.UBound(strAry, 1)];
			for (x = (Information.UBound(strAry, 1) - 2); x >= 0; x--)
			{
				strCity = strAry[x] + " " + strCity;
			}
			// x
			strCity = Strings.Trim(strCity);
			intMaxCity = intCut - strReturn.Length - 1;
			strCity = Strings.Mid(strCity, 1, intMaxCity);
			CutOffAddress = strCity + " " + strReturn;
			return CutOffAddress;
		}
		// vbPorter upgrade warning: straddr As object	OnWrite(string)
		private string SplitCityState_8(object straddr, bool boolGetStateZip)
		{
			return SplitCityState(ref straddr, ref boolGetStateZip);
		}

		private string SplitCityState(ref object straddr, ref bool boolGetStateZip)
		{
			string SplitCityState = "";
			string strReturn;
			int x;
			string strTemp;
			string strCity = "";
			string[] strAry = null;
			strReturn = "";
			SplitCityState = FCConvert.ToString(straddr);
			strTemp = Strings.Replace(FCConvert.ToString(straddr), "  ", " ", 1, -1, CompareConstants.vbTextCompare);
			strAry = Strings.Split(strTemp, " ", -1, CompareConstants.vbTextCompare);
			if (Information.UBound(strAry, 1) < 2)
			{
				if (!boolGetStateZip)
				{
					return SplitCityState;
				}
				else
				{
					SplitCityState = "";
					return SplitCityState;
				}
			}
			strReturn = strAry[Information.UBound(strAry, 1) - 1] + " " + strAry[Information.UBound(strAry, 1)];
			for (x = (Information.UBound(strAry, 1) - 2); x >= 0; x--)
			{
				strCity = strAry[x] + " " + strCity;
			}
			// x
			strCity = Strings.Trim(strCity);
			if (boolGetStateZip)
			{
				SplitCityState = strReturn;
			}
			else
			{
				SplitCityState = strCity;
			}
			return SplitCityState;
		}

		private void rptBillFormatA_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBillFormatA properties;
			//rptBillFormatA.Caption	= "Format A";
			//rptBillFormatA.Icon	= "rptBillFormatA.dsx":0000";
			//rptBillFormatA.Left	= 0;
			//rptBillFormatA.Top	= 0;
			//rptBillFormatA.Width	= 15240;
			//rptBillFormatA.Height	= 11115;
			//rptBillFormatA.StartUpPosition	= 3;
			//rptBillFormatA.SectionData	= "rptBillFormatA.dsx":058A;
			//End Unmaped Properties
		}
	}
}
