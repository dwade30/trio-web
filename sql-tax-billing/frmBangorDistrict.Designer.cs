﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmBangorDistrict.
	/// </summary>
	partial class frmBangorDistrict : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCComboBox cmbDistrict;
		public fecherFoundation.FCButton cmdCopy;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBangorDistrict));
			this.Frame1 = new fecherFoundation.FCFrame();
			this.lblMessage = new fecherFoundation.FCLabel();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmbDistrict = new fecherFoundation.FCComboBox();
			this.cmdCopy = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveAndContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCopy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAndContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 338);
			this.BottomPanel.Size = new System.Drawing.Size(448, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmdClear);
			this.ClientArea.Controls.Add(this.cmbDistrict);
			this.ClientArea.Controls.Add(this.cmdCopy);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(448, 278);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(448, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(170, 30);
			this.HeaderText.Text = "District Values";
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.lblMessage);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(378, 103);
			this.Frame1.TabIndex = 0;
			// 
			// lblMessage
			// 
			this.lblMessage.Location = new System.Drawing.Point(0, 0);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(350, 82);
			this.lblMessage.TabIndex = 0;
			// 
			// cmdClear
			// 
			this.cmdClear.AppearanceKey = "actionButton";
			this.cmdClear.ForeColor = System.Drawing.Color.White;
			this.cmdClear.Location = new System.Drawing.Point(181, 210);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(135, 40);
			this.cmdClear.TabIndex = 4;
			this.cmdClear.Text = "Clear Values";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// cmbDistrict
			// 
			this.cmbDistrict.AutoSize = false;
			this.cmbDistrict.BackColor = System.Drawing.SystemColors.Window;
			this.cmbDistrict.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDistrict.FormattingEnabled = true;
			this.cmbDistrict.Location = new System.Drawing.Point(151, 150);
			this.cmbDistrict.Name = "cmbDistrict";
			this.cmbDistrict.Size = new System.Drawing.Size(187, 40);
			this.cmbDistrict.TabIndex = 2;
			this.cmbDistrict.Text = "Combo1";
			// 
			// cmdCopy
			// 
			this.cmdCopy.AppearanceKey = "actionButton";
			this.cmdCopy.ForeColor = System.Drawing.Color.White;
			this.cmdCopy.Location = new System.Drawing.Point(30, 210);
			this.cmdCopy.Name = "cmdCopy";
			this.cmdCopy.Size = new System.Drawing.Size(136, 40);
			this.cmdCopy.TabIndex = 3;
			this.cmdCopy.Text = "Copy Values";
			this.cmdCopy.Click += new System.EventHandler(this.cmdCopy_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 164);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(101, 19);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "DISTRICT";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveAndContinue
			// 
			this.cmdSaveAndContinue.AppearanceKey = "acceptButton";
			this.cmdSaveAndContinue.Location = new System.Drawing.Point(97, 26);
			this.cmdSaveAndContinue.Name = "cmdSaveAndContinue";
			this.cmdSaveAndContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAndContinue.Size = new System.Drawing.Size(183, 48);
			this.cmdSaveAndContinue.TabIndex = 0;
			this.cmdSaveAndContinue.Text = "Save & Continue";
			this.cmdSaveAndContinue.Click += new System.EventHandler(this.cmdSaveAndContinue_Click);
			// 
			// frmBangorDistrict
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(448, 446);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBangorDistrict";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "District Values";
			this.Load += new System.EventHandler(this.frmBangorDistrict_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBangorDistrict_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCopy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndContinue;
	}
}
