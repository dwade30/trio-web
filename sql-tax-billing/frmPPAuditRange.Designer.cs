//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPPAuditRange.
	/// </summary>
	partial class frmPPAuditRange : BaseForm
	{
		public fecherFoundation.FCComboBox cmbDisplay;
		public fecherFoundation.FCLabel lblDisplay;
		public fecherFoundation.FCComboBox cmbSequence;
		public fecherFoundation.FCLabel lblSequence;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbAccounts;
		public fecherFoundation.FCLabel lblAccounts;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPAuditRange));
			this.cmbDisplay = new fecherFoundation.FCComboBox();
			this.lblDisplay = new fecherFoundation.FCLabel();
			this.cmbSequence = new fecherFoundation.FCComboBox();
			this.lblSequence = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbAccounts = new fecherFoundation.FCComboBox();
			this.lblAccounts = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 340);
			this.BottomPanel.Size = new System.Drawing.Size(846, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.txtEnd);
			this.ClientArea.Controls.Add(this.txtStart);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.cmbAccounts);
			this.ClientArea.Controls.Add(this.lblAccounts);
			this.ClientArea.Controls.Add(this.lblTo);
			this.ClientArea.Size = new System.Drawing.Size(846, 280);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(846, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(317, 30);
			this.HeaderText.Text = "Personal Property Accounts";
			// 
			// cmbDisplay
			// 
			this.cmbDisplay.AutoSize = false;
			this.cmbDisplay.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDisplay.FormattingEnabled = true;
			this.cmbDisplay.Items.AddRange(new object[] {
				"Display",
				"Print"
			});
			this.cmbDisplay.Location = new System.Drawing.Point(152, 30);
			this.cmbDisplay.Name = "cmbDisplay";
			this.cmbDisplay.Size = new System.Drawing.Size(141, 40);
			this.cmbDisplay.TabIndex = 1;
			this.cmbDisplay.Text = "Display";
			this.cmbDisplay.SelectedIndexChanged += new System.EventHandler(this.cmbDisplay_SelectedIndexChanged);
			// 
			// lblDisplay
			// 
			this.lblDisplay.AutoSize = true;
			this.lblDisplay.Location = new System.Drawing.Point(20, 44);
			this.lblDisplay.Name = "lblDisplay";
			this.lblDisplay.Size = new System.Drawing.Size(60, 15);
			this.lblDisplay.TabIndex = 0;
			this.lblDisplay.Text = "DISPLAY";
			// 
			// cmbSequence
			// 
			this.cmbSequence.AutoSize = false;
			this.cmbSequence.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSequence.FormattingEnabled = true;
			this.cmbSequence.Items.AddRange(new object[] {
				"Name",
				"Account",
				"Tax",
				"Location"
			});
			this.cmbSequence.Location = new System.Drawing.Point(152, 90);
			this.cmbSequence.Name = "cmbSequence";
			this.cmbSequence.Size = new System.Drawing.Size(141, 40);
			this.cmbSequence.TabIndex = 3;
			this.cmbSequence.Text = "Name";
			this.cmbSequence.Visible = false;
			// 
			// lblSequence
			// 
			this.lblSequence.AutoSize = true;
			this.lblSequence.Location = new System.Drawing.Point(20, 104);
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Size = new System.Drawing.Size(77, 15);
			this.lblSequence.TabIndex = 2;
			this.lblSequence.Text = "SEQUENCE";
			this.lblSequence.Visible = false;
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"Accounts",
				"Names",
				"Locations",
				"Open1",
				"Open2"
			});
			this.cmbRange.Location = new System.Drawing.Point(524, 29);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(154, 40);
			this.cmbRange.TabIndex = 4;
			this.cmbRange.Text = "Accounts";
			this.cmbRange.Visible = false;
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(411, 44);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 3;
			this.lblRange.Text = "RANGE";
			this.lblRange.Visible = false;
			// 
			// cmbAccounts
			// 
			this.cmbAccounts.AutoSize = false;
			this.cmbAccounts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAccounts.FormattingEnabled = true;
			this.cmbAccounts.Items.AddRange(new object[] {
				"All Accounts",
				"Range"
			});
			this.cmbAccounts.Location = new System.Drawing.Point(164, 30);
			this.cmbAccounts.Name = "cmbAccounts";
			this.cmbAccounts.Size = new System.Drawing.Size(178, 40);
			this.cmbAccounts.TabIndex = 1;
			this.cmbAccounts.Text = "All Accounts";
			this.cmbAccounts.SelectedIndexChanged += new System.EventHandler(this.cmbAccounts_SelectedIndexChanged);
			// 
			// lblAccounts
			// 
			this.lblAccounts.AutoSize = true;
			this.lblAccounts.Location = new System.Drawing.Point(30, 44);
			this.lblAccounts.Name = "lblAccounts";
			this.lblAccounts.Size = new System.Drawing.Size(77, 15);
			this.lblAccounts.TabIndex = 0;
			this.lblAccounts.Text = "ACCOUNTS";
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.cmbSequence);
			this.Frame3.Controls.Add(this.lblSequence);
			this.Frame3.Controls.Add(this.cmbDisplay);
			this.Frame3.Controls.Add(this.lblDisplay);
			this.Frame3.Location = new System.Drawing.Point(30, 90);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(317, 152);
			this.Frame3.TabIndex = 2;
			this.Frame3.Text = "Display / Print";
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(664, 120);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(173, 40);
			this.txtEnd.TabIndex = 7;
			this.txtEnd.Visible = false;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(403, 120);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(173, 40);
			this.txtStart.TabIndex = 5;
			this.txtStart.Visible = false;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(605, 134);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(29, 21);
			this.lblTo.TabIndex = 6;
			this.lblTo.Text = "TO";
			this.lblTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTo.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuCancel
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuCancel
			// 
			this.mnuCancel.Index = 2;
			this.mnuCancel.Name = "mnuCancel";
			this.mnuCancel.Text = "Exit";
			this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(364, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(124, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmPPAuditRange
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(846, 448);
			this.ControlBox = false;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPAuditRange";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Personal Property Accounts";
			this.Load += new System.EventHandler(this.frmPPAuditRange_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPAuditRange_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}