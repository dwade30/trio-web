﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPreviousOwners.
	/// </summary>
	public partial class rptPreviousOwners : BaseSectionReport
	{
		public rptPreviousOwners()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Previous Owners Report";
		}

		public static rptPreviousOwners InstancePtr
		{
			get
			{
				return (rptPreviousOwners)Sys.GetInstance(typeof(rptPreviousOwners));
			}
		}

		protected rptPreviousOwners _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPreviousOwners	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		int lngPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngAccount As int	OnWriteFCConvert.ToDouble(
		public void Init(int intOrder, bool boolofAccount, int lngAccount = 0, string strStartDate = "", string strEndDate = "")
		{
			string strOrder = "";
			if (boolofAccount)
			{
				if (intOrder == 0)
				{
					strOrder = "SaleDate";
				}
				else
				{
					strOrder = "Name";
				}
				clsReport.OpenRecordset("select * from previousowner where account = " + FCConvert.ToString(lngAccount) + " order by " + strOrder, "twre0000.vb1");
			}
			else
			{
				if (intOrder == 0)
				{
					strOrder = "SaleDate";
				}
				else if (intOrder == 1)
				{
					strOrder = "Name";
				}
				else
				{
					strOrder = "Account";
				}
				clsReport.OpenRecordset("select * from previousowner where saledate between '" + strStartDate + "' and '" + strEndDate + "' or saledate = 0 order by " + strOrder, "twre0000.vb1");
			}
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "PreviousOwners");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPage.Text = "Page 1";
			lngPage = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			DateTime dtTemp;
			if (!clsReport.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = FCConvert.ToString(clsReport.Get_Fields("account"));
				txtName.Text = FCConvert.ToString(clsReport.Get_Fields_String("name"));
				txtSecOwner.Text = FCConvert.ToString(clsReport.Get_Fields_String("secowner"));
				txtAddress1.Text = FCConvert.ToString(clsReport.Get_Fields_String("address1"));
				txtAddress2.Text = FCConvert.ToString(clsReport.Get_Fields_String("address2"));
				txtCity.Text = FCConvert.ToString(clsReport.Get_Fields_String("city"));
				txtState.Text = FCConvert.ToString(clsReport.Get_Fields_String("state"));
				txtZip.Text = FCConvert.ToString(clsReport.Get_Fields_String("zip") + " " + clsReport.Get_Fields_String("zip4"));
				dtTemp = (DateTime)clsReport.Get_Fields_DateTime("saledate");
				if (dtTemp.ToOADate() == 0)
				{
					txtSaleDate.Text = "";
				}
				else
				{
					txtSaleDate.Text = Strings.Format(dtTemp, "MM/dd/yyyy");
				}
				clsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

	
	}
}
