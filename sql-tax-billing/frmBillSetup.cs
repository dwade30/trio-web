﻿//Fecher vbPorter - Version 1.0.0.35
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using Wisej.Web;

namespace TWBL0000
{
    /// <summary>
    /// Summary description for frmBillSetup.
    /// </summary>
    public partial class frmBillSetup : BaseForm
    {
        public frmBillSetup()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmBillSetup InstancePtr
        {
            get
            {
                return (frmBillSetup)Sys.GetInstance(typeof(frmBillSetup));
            }
        }

        protected frmBillSetup _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>

        clsBillFormat clsTaxFormat = new clsBillFormat();
        clsRateRecord clsRateRec = new clsRateRecord();
        string strOpen1 = "";
        string strOpen2 = "";
        bool boolFromSupplemental;
        const int PRTROWACCOUNT = 1;
        const int PRTROWNAME = 2;
        const int PRTROWLOCATION = 3;
        /// <summary>
        /// RE stuff
        /// </summary>
        const int PRTROWMAPLOT = 4;
        const int PRTROWTRAN = 5;
        const int PRTROWLAND = 6;
        const int PRTROWBLDG = 7;
        /// <summary>
        /// PP stuff
        /// </summary>
        const int PRTROWBUSCODE = 4;
        const int PRTROWOPEN1 = 5;
        const int PRTROWOPEN2 = 6;
        private string strAutoList = "";

        private void AccountsGrid_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.Delete)
            {
                KeyCode = 0;
                if (AccountsGrid.Rows > 1)
                {
                    AccountsGrid.RemoveItem();
                }
                else
                {
                    AccountsGrid.Rows = 1;
                    AccountsGrid.TextMatrix(0, 0, "");
                }
            }
        }
        //FC:FINAL:CHN - issue #1444: Prevent CellValidating event on changing rows count at CellValidating method (Fix StackOverflow).
        private int AccountsGridRow = -1;
        private int AccountsGridCol = -1;
        private string AccountsGridText = null;

        private void AccountsGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = AccountsGrid.GetFlexRowIndex(e.RowIndex);
            int col = AccountsGrid.GetFlexColIndex(e.ColumnIndex);
            //FC:FINAL:CHN - issue #1444: Save values to display after validating.
            AccountsGridRow = row;
            AccountsGridCol = col;
            AccountsGridText = AccountsGrid.EditText;
            if (Conversion.Val(AccountsGrid.EditText) > 0)
            {
                //FC:FINAL:CHN - issue #1444: Prevent CellValidating event on changing rows count at CellValidating method (Fix StackOverflow).
                AccountsGrid.CellValidating -= AccountsGrid_ValidateEdit;
                // prevent call CellValidating
                if (Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, 0)) > 0)
                {
                    AccountsGrid.Rows += 1;
                    // was call CellValidating
                    AccountsGrid.Row += 1;
                    // was call CellValidating
                    if (AccountsGrid.Rows > 9)
                    {
                        //Application.DoEvents();
                        // AccountsGrid.TopRow = AccountsGrid.Rows - 7 'accountsgrid - 1 - (one less than visible rows)
                        AccountsGrid.ScrollCellIntoView(0, AccountsGrid.Rows - 1);
                    }
                }
                else if (row == AccountsGrid.Rows - 1)
                {
                    AccountsGrid.Rows += 1;
                    AccountsGrid.Row += 1;
                    if (AccountsGrid.Rows > 9)
                    {
                        //Application.DoEvents();
                        // AccountsGrid.TopRow = AccountsGrid.Rows - 7 'accountsgrid - 1 - (one less than visible rows)
                        AccountsGrid.ScrollCellIntoView(0, AccountsGrid.Rows - 1);
                    }
                }
                //FC:FINAL:CHN - issue #1444: Prevent CellValidating event on changing rows count at CellValidating method (Fix StackOverflow).
                AccountsGrid.CellValidating += AccountsGrid_ValidateEdit;
                // return validating
            }
            else
            {
                //FC:FINAL:CHN - i.issue #1539: Individual acct control is overlapping the groupbox
                AccountsGrid.EditText = "";
                // e.Cancel = true; // if we change focus on other component, e.Cancel will cancelling changing focus and entering number value.
            }
        }

        private void AccountsGrid_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (AccountsGridText != null)
                AccountsGrid.TextMatrix(AccountsGridRow, AccountsGridCol, AccountsGridText);
        }

        private void frmBillSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Escape)
            {
                KeyCode = (Keys)0;
                mnuExit_Click();
            }
        }

        private void frmBillSetup_Load(object sender, System.EventArgs e)
        {
            modGlobalFunctions.SetFixedSize(this);
            modGlobalFunctions.SetTRIOColors(this);
            clsDRWrapper clsLoad = new clsDRWrapper();
            if (clsLoad.OpenRecordset("select basis,OVERPAYINTERESTRATE from COLLECTIONS", modGlobalVariables.strCLDatabase))
            {
                if (!clsLoad.EndOfFile())
                {
                    modGlobalVariables.Statics.gintBasis = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("Basis"))));
                    modGlobalVariables.Statics.dblOverPayRate = Conversion.Val(clsLoad.Get_Fields_Double("overpayinterestrate"));
                }
            }
        }

        private void frmBillSetup_Resize(object sender, System.EventArgs e)
        {
            ResizePrintGrid();
            ResizeAccountsGrid();
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            if (!boolFromSupplemental)
            {
                //MDIParent.InstancePtr.Show();
            }
        }

        private void mnuContinue_Click(object sender, System.EventArgs e)
        {
            // bring up the tax message screen then go to print bills
            int NumFonts = 0;
            string strFont = "";
            int intCPI = 0;
            int x;
            string strPrinterName = "";
            bool boolUseFont = false;
            string strTemp = "";
            int intHowManyDistLines = 0;
            int intLimit = 0;
            string strDefPrtDevice = "";
            bool boolDefaultMargin;
            string strLastFont = "";
            clsDRWrapper clsTemp = new clsDRWrapper();
            clsDRWrapper clsSave = new clsDRWrapper();

            try
            {

                if (!IsValid()) return;

                clsSave.OpenRecordset("select * from billsetup", "twbl0000.vb1");

                if (clsSave.EndOfFile())
                {
                    clsSave.AddNew();
                }
                else
                {
                    clsSave.Edit();
                }

                clsTaxFormat.PrintCopyForMortgageHolders = chkMortgage.CheckState == Wisej.Web.CheckState.Checked;
                clsTaxFormat.PrintCopyForNewOwner = chkNewOwner.CheckState == Wisej.Web.CheckState.Checked;
                clsTaxFormat.PrintCopyForMultiOwners = chkMultiRecipients.CheckState == Wisej.Web.CheckState.Checked;
                clsTaxFormat.Discount = cmbDiscount.SelectedIndex != 0;
                clsSave.Set_Fields("discount", clsTaxFormat.Discount);

                if (clsTaxFormat.Discount == true)
                {
                    frmDiscount.InstancePtr.Init();

                    if (modGlobalVariables.Statics.gboolCancelForm)
                    {
                        modGlobalVariables.Statics.gboolCancelForm = false;

                        return;
                    }
                }

                switch (cmbDistribution.SelectedIndex)
                {
                    case 0:
                        clsTaxFormat.Breakdown = false;
                        clsTaxFormat.BreakdownDollars = false;

                        break;
                    case 1:
                        clsTaxFormat.Breakdown = true;
                        clsTaxFormat.BreakdownDollars = false;

                        break;
                    default:
                        clsTaxFormat.Breakdown = true;
                        clsTaxFormat.BreakdownDollars = true;

                        break;
                }

                clsSave.Set_Fields("distribution", clsTaxFormat.Breakdown);
                clsSave.Set_Fields("distributiontotals", clsTaxFormat.BreakdownDollars);

                if (clsTaxFormat.Breakdown)
                {
                    if (clsTaxFormat.Billformat > 13)
                    {
                        // laser formats
                        intHowManyDistLines = 7;
                    }
                    else
                        intHowManyDistLines = clsTaxFormat.Billformat == 10 ? 4 : 5;

                    frmDistribution.InstancePtr.Init(intHowManyDistLines);

                    if (modGlobalVariables.Statics.gboolCancelForm)
                    {
                        modGlobalVariables.Statics.gboolCancelForm = false;

                        return;
                    }
                }

                clsTaxFormat.ReturnAddress = cmbReturnAddress.SelectedIndex != 0;
                clsSave.Set_Fields("returnaddress", clsTaxFormat.ReturnAddress);

                if (clsTaxFormat.ReturnAddress)
                {
                    clsTemp.OpenRecordset(!modRegionalTown.IsRegionalTown()
                                              ? "select * from returnaddress"
                                              : "select * from returnaddress where convert(int, isnull(townnumber, 0)) > 0", "twbl0000.vb1");

                    if (clsTemp.EndOfFile())
                    {
                        frmReturnAddress.InstancePtr.Init();
                    }
                    else
                    {
                        strTemp = Strings.Trim(clsTemp.Get_Fields_String("Address1") + clsTemp.Get_Fields_String("address2") + clsTemp.Get_Fields_String("address3") + clsTemp.Get_Fields_String("address4"));

                        if (strTemp == string.Empty)
                        {
                            frmReturnAddress.InstancePtr.Init();
                        }
                    }
                }

                switch (cmbBillType.SelectedIndex)
                {
                    case 0:
                        // re
                        clsTaxFormat.BillsFrom = 0;
                        clsTaxFormat.Combined = false;
                        clsSave.Set_Fields("billtype", 0);

                        break;
                    case 1:
                        // pp
                        clsTaxFormat.BillsFrom = 1;
                        clsTaxFormat.Combined = false;
                        clsSave.Set_Fields("billtype", 1);

                        break;
                    default:
                        // combined
                        clsTaxFormat.BillsFrom = 0;
                        clsTaxFormat.Combined = true;
                        clsSave.Set_Fields("billtype", 2);

                        break;
                }

                switch (cmbOrderBills.SelectedIndex)
                {
                    case 0:
                        clsSave.Set_Fields("orderby", 0);

                        break;
                    case 1:
                        clsSave.Set_Fields("orderby", 1);

                        break;
                    default:
                        clsSave.Set_Fields("orderby", 2);

                        break;
                }

                clsSave.Set_Fields("billformat", clsTaxFormat.Billformat);
                clsSave.Set_Fields("printcopyformortgageholders", clsTaxFormat.PrintCopyForMortgageHolders);

                clsSave.Set_Fields("UseLaserAdjustment", chkAdjustment.CheckState == Wisej.Web.CheckState.Checked);
                clsTaxFormat.UseLaserAlignment = chkAdjustment.CheckState == Wisej.Web.CheckState.Checked;

                clsTaxFormat.HasDefaultMargin = chkDotMatrixAdjust.CheckState == Wisej.Web.CheckState.Checked;

                clsSave.Set_Fields("clearlaserboxes", chkTransparentBoxes.CheckState == Wisej.Web.CheckState.Checked);
                clsTaxFormat.UseClearLaserBackground = chkTransparentBoxes.CheckState == Wisej.Web.CheckState.Checked;

                clsSave.Set_Fields("LaserAdjustment", FCConvert.ToString(Conversion.Val(txtAdjustment.Text)));
                clsTaxFormat.LaserAlignment = Conversion.Val(txtAdjustment.Text);

                clsSave.Update();

                modGlobalVariables.Statics.CancelledIt = false;

                if (clsTaxFormat.Billformat != 7 && clsTaxFormat.Billformat < 13)
                {
                    frmTaxMessage.InstancePtr.Init(ref clsTaxFormat);
                }
                else
                    if (clsTaxFormat.Billformat >= 14 && clsTaxFormat.Billformat < 17)
                    {
                        frmLaserMessage.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
                    }

                if (modGlobalVariables.Statics.CancelledIt)
                {
                    return;
                }

                //strPrinterName = FCGlobal.Printer?.DeviceName;
                boolUseFont = true;
                strFont = "";
                clsTemp.OpenRecordset("select * from defaultstable", "twbl0000.vb1");

                if (!clsTemp.EndOfFile())
                {
                    if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("ignoreprinterfonts")))
                    {
                        boolUseFont = false;
                    }
                }

                // strFont = strLastFont
                strTemp = BuildSQLStatement();

                if (strTemp == string.Empty) return;

                clsTaxFormat.SQLStatement = strTemp;
                clsTaxFormat.BillAsOfDate = DateTime.Today.Month < 4
                    ? DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1))
                    : DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));

                cBillPrintReturn tReturn;

                switch (clsTaxFormat.Billformat)
                {
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                        tReturn = frmPrintorPreview.InstancePtr.Init(true);

                        break;
                    default:
                        tReturn = frmPrintorPreview.InstancePtr.Init(false);

                        break;
                }

                intLimit = tReturn.PreviewLimit;

                modBLCustomBill.LoadReturnAddressesForBills();

                if (tReturn.OutputType < 0) return;

                if (tReturn.OutputType == 1)
                {
                    switch (clsTaxFormat.Billformat)
                    {
                        case 14:
                        case 15:
                        {
                            rptBillFormatG.InstancePtr.Unload();

                            break;
                        }
                        case 16:
                        {
                            rptBillFormatI.InstancePtr.Unload();

                            break;
                        }
                        case 17:
                        {
                            rptCustomBill.InstancePtr.Unload();

                            break;
                        }
                    }

                    //end switch
                    frmBillViewer.InstancePtr.Init(ref clsTaxFormat, FCConvert.ToInt16(Conversion.Val(txtYear.Text)), ref strFont, ref intLimit, strPrinterName, boolFromSupplemental);
                }
                else
                {
                    clsBLCustomBill clsBill;

                    if (tReturn.OutputType == 2)
                    {
                        if (fecherFoundation.Strings.InStr(1, tReturn.Filename, ".", fecherFoundation.CompareConstants.vbBinaryCompare) < 1)
                        {
                            tReturn.Filename = tReturn.Filename + ".pdf";
                        }

                        switch (clsTaxFormat.Billformat)
                        {

                            case 14:
                            {
                                // G
                                var billFormatG = new rptBillFormatG();
                                billFormatG.Init(clsTaxFormat, Convert.ToInt16(fecherFoundation.Conversion.Val(txtYear.Text)), strFont, false, strPrinterName, boolFromSupplemental, true, tReturn.Filename);

                                break;
                            }
                            case 15:
                            {
                                rptBillFormatG.InstancePtr.Unload();
                                rptBillFormatG.InstancePtr.Init(clsTaxFormat, Convert.ToInt16(fecherFoundation.Conversion.Val(txtYear.Text)), strFont, false, strPrinterName, boolFromSupplemental, true, tReturn.Filename);

                                break;
                            }
                            case 16:
                            {
                                rptBillFormatI.InstancePtr.Unload();
                                rptBillFormatI.InstancePtr.Init(clsTaxFormat, Convert.ToInt16(fecherFoundation.Conversion.Val(txtYear.Text)), strFont, false, strPrinterName, boolFromSupplemental, true, tReturn.Filename);

                                break;
                            }
                            case 17:
                            {
                                rptCustomBill.InstancePtr.Unload();
                                clsBill = new clsBLCustomBill();
                                clsBill.SQL = clsTaxFormat.SQLStatement;
                                clsBill.AssignBillFormat(ref clsTaxFormat);
                                clsBill.UsePrinterFonts = true;
                                clsBill.FormatID = clsTaxFormat.CustomBillID;
                                clsBill.DataDBFile = "Collections";
                                clsBill.PrinterName = strPrinterName;
                                clsBill.DotMatrixFormat = false;
                                clsBill.DBFile = "TaxBilling";
                                rptCustomBill.InstancePtr.Init(clsBill, false, true, false, true, tReturn.Filename);

                                break;
                            }
                        } //end switch
                    }
                    else
                    {
                        // just print all
                        if (clsTaxFormat.Billformat < 14 && modPrinterFunctions.PrintXsForAlignment("The top of the X should align with the top of the bill", 40, 1, strPrinterName) == DialogResult.No)
                        {
                            return;
                        }

                        switch (clsTaxFormat.Billformat)
                        {
                            case 14:
                            {
                                rptBillFormatG.InstancePtr.Unload();
                                rptBillFormatG.InstancePtr.Init(clsTaxFormat, FCConvert.ToInt32(Conversion.Val(txtYear.Text)), strFont, false, strPrinterName, boolFromSupplemental);

                                break;
                            }
                            case 15:
                            {
                                rptBillFormatG.InstancePtr.Unload();
                                rptBillFormatG.InstancePtr.Init(clsTaxFormat, FCConvert.ToInt32(Conversion.Val(txtYear.Text)), strFont, false, strPrinterName, boolFromSupplemental);

                                break;
                            }
                            case 16:
                            {
                                rptBillFormatI.InstancePtr.Unload();
                                rptBillFormatI.InstancePtr.Init(clsTaxFormat, FCConvert.ToInt32(Conversion.Val(txtYear.Text)), strFont, false, strPrinterName, boolFromSupplemental);

                                break;
                            }
                            case 17:
                            {
                                rptCustomBill.InstancePtr.Unload();
                                clsBill = new clsBLCustomBill();
                                clsBill.SQL = clsTaxFormat.SQLStatement;
                                clsBill.AssignBillFormat(ref clsTaxFormat);
                                clsBill.UsePrinterFonts = true;
                                clsBill.FormatID = clsTaxFormat.CustomBillID;
                                clsBill.DataDBFile = "Twcl0000.vb1";
                                clsBill.PrinterName = strPrinterName;
                                clsTemp.OpenRecordset("select * from custombills where ID = " + FCConvert.ToString(clsBill.FormatID), "twbl0000.vb1");

                                if (!clsTemp.EndOfFile())
                                {
                                    clsBill.DotMatrixFormat = !FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("islaser"));
                                }

                                rptCustomBill.InstancePtr.Init(clsBill, false);

                                break;
                            }
                        }

                    }
                }

                Close();
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                if (Information.Err(ex).Number != 32755)
                {
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuContinue_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            finally
            {
                clsSave.DisposeOf();
                clsTemp.DisposeOf();
            }
        }

        private bool IsValid()
        {
            if (!HasValidYear())
            {
                MessageBox.Show("You must choose a billing year.", "Choose Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return false;
            }

            return DisplayOfDistBreakdown() || MessageBox.Show("You have not chosen to display the distribution breakdown"
                                                               + "\r\n"
                                                               + "Beginning in 2008 the state requires all bills to indicate the property tax distribution"
                                                               + "\r\n"
                                                               + "If this information is not pre-printed, or does not appear in some other way on the bill, you may be in violation"
                                                             , "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.Cancel;
        }

        private bool DisplayOfDistBreakdown()
        {
            return cmbDistribution.SelectedIndex != 0 || clsTaxFormat.Billformat == 17;
        }

        private bool HasValidYear()
        {
            return (Conversion.Val(txtYear.Text) >= 1990 && Conversion.Val(txtYear.Text) <= 2030);
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void FillPrintGrid()
        {
            // If .Row > 5 Then
            // .Row = 1
            // .RowSel = 1
            // End If
            PrintGrid.Rows = 4;
            PrintGrid.TextMatrix(0, 0, "Bills to print");
            PrintGrid.TextMatrix(0, 1, "Start");
            PrintGrid.TextMatrix(0, 2, "End");
            // .TextMatrix(1, 0) = "All"
            // .TextMatrix(2, 0) = "Individual"
            PrintGrid.TextMatrix(PRTROWACCOUNT, 0, "Account");
            PrintGrid.TextMatrix(PRTROWNAME, 0, "Name");
            PrintGrid.TextMatrix(PRTROWLOCATION, 0, "Location");
            if (cmbBillType.SelectedIndex == 0 || cmbBillType.SelectedIndex == 2)
            {
                // real estate or real estate combined
                PrintGrid.Rows = 8;
                PrintGrid.TextMatrix(PRTROWMAPLOT, 0, "Map / Lot");
                PrintGrid.TextMatrix(PRTROWTRAN, 0, "Tran Code");
                PrintGrid.TextMatrix(PRTROWLAND, 0, "Land Code");
                PrintGrid.TextMatrix(PRTROWBLDG, 0, "Bldg Code");
            }
            else
            {
                // personal property
                PrintGrid.Rows = 7;
                PrintGrid.TextMatrix(PRTROWBUSCODE, 0, "Business Code");
                PrintGrid.TextMatrix(PRTROWOPEN1, 0, strOpen1);
                PrintGrid.TextMatrix(PRTROWOPEN2, 0, strOpen2);
            }
            // .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 2, 2) = TRIOCOLORGRAYEDOUTTEXTBOX
            ResizePrintGrid();
        }

        private void ResizePrintGrid()
        {
            int GridWidth = 0;
            GridWidth = PrintGrid.WidthOriginal;
            PrintGrid.ColWidth(0, FCConvert.ToInt32(0.32 * GridWidth));
            PrintGrid.ColWidth(1, FCConvert.ToInt32(0.32 * GridWidth));
            PrintGrid.ColWidth(2, FCConvert.ToInt32(0.32 * GridWidth));
            //PrintGrid.HeightOriginal = PrintGrid.RowHeight(0) * PrintGrid.Rows + 60;
            //FC:FINAL:RPU: #1445 - Set the height of grid
            //PrintGrid.HeightOriginal = PrintGrid.RowHeight(1) * PrintGrid.Rows- 125;
        }

        private void mnuPrintLabels_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper clsSave = new clsDRWrapper();
            string strSQL;
            PrintGrid.Col = -1;
            AccountsGrid.Col = -1;
            //Application.DoEvents();
            if (Conversion.Val(txtYear.Text) < 1990 || Conversion.Val(txtYear.Text) > 2030)
            {
                MessageBox.Show("You must choose a billing year.", "Choose Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            clsSave.OpenRecordset("select * from billsetup", "twbl0000.vb1");
            if (clsSave.EndOfFile())
            {
                clsSave.AddNew();
            }
            else
            {
                clsSave.Edit();
            }
            clsTaxFormat.PrintCopyForMortgageHolders = chkMortgage.CheckState == Wisej.Web.CheckState.Checked;
            clsTaxFormat.PrintCopyForNewOwner = chkNewOwner.CheckState == Wisej.Web.CheckState.Checked;
            clsTaxFormat.PrintCopyForMultiOwners = chkMultiRecipients.CheckState == Wisej.Web.CheckState.Checked;
            clsTaxFormat.Discount = cmbDiscount.SelectedIndex != 0;
            clsSave.Set_Fields("discount", clsTaxFormat.Discount);
            switch (cmbDistribution.SelectedIndex)
            {
                case 0:
                    clsTaxFormat.Breakdown = false;
                    clsTaxFormat.BreakdownDollars = false;

                    break;
                case 1:
                    clsTaxFormat.Breakdown = true;
                    clsTaxFormat.BreakdownDollars = false;

                    break;
                default:
                    clsTaxFormat.Breakdown = true;
                    clsTaxFormat.BreakdownDollars = true;

                    break;
            }
            clsSave.Set_Fields("distribution", clsTaxFormat.Breakdown);
            clsSave.Set_Fields("distributiontotals", clsTaxFormat.BreakdownDollars);
            clsTaxFormat.ReturnAddress = cmbReturnAddress.SelectedIndex != 0;
            clsSave.Set_Fields("returnaddress", clsTaxFormat.ReturnAddress);
            switch (cmbBillType.SelectedIndex)
            {
                case 0:
                    // re
                    clsTaxFormat.BillsFrom = 0;
                    clsTaxFormat.Combined = false;
                    clsSave.Set_Fields("billtype", 0);

                    break;
                case 1:
                    // pp
                    clsTaxFormat.BillsFrom = 1;
                    clsTaxFormat.Combined = false;
                    clsSave.Set_Fields("billtype", 1);

                    break;
                default:
                    // combined
                    clsTaxFormat.BillsFrom = 0;
                    clsTaxFormat.Combined = true;
                    clsSave.Set_Fields("billtype", 2);

                    break;
            }
            switch (cmbOrderBills.SelectedIndex)
            {
                case 0:
                    clsSave.Set_Fields("orderby", 0);

                    break;
                case 1:
                    clsSave.Set_Fields("orderby", 1);

                    break;
                default:
                    clsSave.Set_Fields("orderby", 2);

                    break;
            }
            clsSave.Set_Fields("billformat", clsTaxFormat.Billformat);
            clsSave.Set_Fields("printcopyformortgageholders", clsTaxFormat.PrintCopyForMortgageHolders);

            clsSave.Set_Fields("UseLaserAdjustment", chkAdjustment.CheckState == Wisej.Web.CheckState.Checked);
            clsTaxFormat.UseLaserAlignment = chkAdjustment.CheckState == Wisej.Web.CheckState.Checked;

            clsTaxFormat.HasDefaultMargin = chkDotMatrixAdjust.CheckState == Wisej.Web.CheckState.Checked;

            clsSave.Set_Fields("clearlaserboxes", chkTransparentBoxes.CheckState == Wisej.Web.CheckState.Checked);
            clsTaxFormat.UseClearLaserBackground = chkTransparentBoxes.CheckState == Wisej.Web.CheckState.Checked;

            clsSave.Set_Fields("LaserAdjustment", FCConvert.ToString(Conversion.Val(txtAdjustment.Text)));
            clsTaxFormat.LaserAlignment = Conversion.Val(txtAdjustment.Text);
            clsSave.Update();

            clsTaxFormat.BillAsOfDate = DateTime.Today.Month < 4
                ? DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year - 1))
                : DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));

            strSQL = BuildSQLLabelStatement();
            if (strSQL == string.Empty)
                return;

            clsTaxFormat.SQLStatement = strSQL;
            frmCustomLabels.InstancePtr.Init(0, 0, true, clsTaxFormat);
        }

        private void optBillsToPrint_CheckedChanged(short Index, object sender, System.EventArgs e)
        {
            switch (Index)
            {
                case 0:
                    {
                        // all
                        PrintGrid.Visible = false;
                        FramAccountList.Visible = false;
                        break;
                    }
                case 1:
                    {
                        // individual
                        PrintGrid.Visible = false;
                        FramAccountList.Visible = true;
                        break;
                    }
                case 2:
                    {
                        // range
                        PrintGrid.Visible = true;
                        FramAccountList.Visible = false;
                        break;
                    }
            }
            //end switch
        }

        private void optBillsToPrint_CheckedChanged(object sender, System.EventArgs e)
        {
            short index = FCConvert.ToInt16(cmbBillsToPrint.SelectedIndex);
            optBillsToPrint_CheckedChanged(index, sender, e);
        }

        private void optBillType_CheckedChanged(short Index, object sender, System.EventArgs e)
        {
            switch (Index)
            {
                case 0:
                    {
                        chkOwner.Enabled = true;
                        T2KOwnerDate.Enabled = true;
                        break;
                    }
                case 1:
                    {
                        chkOwner.Enabled = false;
                        T2KOwnerDate.Enabled = false;
                        break;
                    }
                case 2:
                    {
                        chkOwner.Enabled = true;
                        T2KOwnerDate.Enabled = true;
                        if (!clsTaxFormat.Combined)
                        {
                            cmbBillType.SelectedIndex = 0;
                            MessageBox.Show("This bill format does not allow for combined bills.", "Invalid Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        break;
                    }
            }
            //end switch
            FillPrintGrid();
        }

        private void optBillType_CheckedChanged(object sender, System.EventArgs e)
        {
            short index = FCConvert.ToInt16(cmbBillType.SelectedIndex);
            optBillType_CheckedChanged(index, sender, e);
        }

        private void PrintGrid_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
        {
            if (e.NewRow != e.OldRow)
            {
                // clear out range information for all rows
                PrintGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 1, PrintGrid.Rows - 1, 2, "");
            }
        }

        private void PrintGrid_RowColChange(object sender, System.EventArgs e)
        {
            // With PrintGrid
            // .Editable = flexEDNone
            // 
            // If .Row > 0 Then
            // .Editable = flexEDKbdMouse
            // AccountsGrid.Editable = flexEDNone
            // AccountsGrid.Rows = 0
            // ElseIf .Row = 2 Then
            // individual accounts
            // AccountsGrid.Editable = flexEDKbdMouse
            // AccountsGrid.Rows = 0
            // AccountsGrid.Rows = 1
            // AccountsGrid.Row = 0
            // AccountsGrid.Col = 0
            // Else
            // AccountsGrid.Editable = flexEDNone
            // AccountsGrid.Rows = 0
            // End If
            // End With
        }

        public void Init(ref clsBillFormat clsFrmt, bool boolFromSupp = false, string strSuppList = "")
        {
            string[] strAry = null;
            int x;
            clsTaxFormat.CopyFormat(ref clsFrmt);
            FillOpens();
            SetupGrids();
            if (clsTaxFormat.Billformat == 17)
            {
                framAdjustment.Visible = false;
                framDotMatrixAdjust.Visible = false;
                cmbDistribution.Enabled = false;
                cmbReturnAddress.Enabled = false;
                cmbDiscount.Enabled = false;
                //optDistribution[0].Enabled = false;
                if (cmbDistribution.Items.Contains("Don\'t Show Breakdown"))
                {
                    cmbDistribution.Items.Remove("Don\'t Show Breakdown");
                }
                //optReturnAddress[0].Enabled = false;
                if (cmbReturnAddress.Items.Contains("Don\'t Show Return Address"))
                {
                    cmbReturnAddress.Items.Remove("Don\'t Show Return Address");
                }
                //optDiscount[0].Enabled = false;
                if (cmbDiscount.Items.Contains("Don\'t Show Discount"))
                {
                    cmbDiscount.Items.Remove("Don\'t Show Discount");
                }
            }
            boolFromSupplemental = boolFromSupp;
            if (boolFromSupp)
            {
                strAutoList = "";
                // optRegOrSup(1).Value = True
                // optRegOrSup(0).Enabled = False
                // optRegOrSup(2).Enabled = False
                if (Strings.Trim(strSuppList) != string.Empty)
                {
                    // parse list and see what to do
                    strAry = Strings.Split(strSuppList, ",", -1, CompareConstants.vbTextCompare);
                    txtYear.Text = strAry[0];
                    txtYear.Enabled = false;
                    // If strAry(1) = "RE" Then
                    // optBillType(0).Value = True
                    // optBillType(1).Enabled = False
                    // Else
                    // optBillType(1).Value = True
                    // optBillType(0).Enabled = False
                    // End If
                    cmbBillType.SelectedIndex = 0;
                    //FC:FINAL:RPU: #i1546 - Disable the Combobox and label
                    cmbBillType.Enabled = false;
                    //FC:FINAL:CHN - issue #1527: Label should be not grayed out.
                    // lblBillType.Enabled = false;
                    if (Information.UBound(strAry, 1) > 0)
                    {
                        cmbBillsToPrint.SelectedIndex = 0;
                        // PrintGrid.Row = 2
                        AccountsGrid.Rows = 0;
                        for (x = 1; x <= Information.UBound(strAry, 1); x++)
                        {
                            // AccountsGrid.AddItem strAry(x)
                            strAutoList += strAry[x] + ",";
                        }
                        // x
                        if (strAutoList != string.Empty)
                        {
                            strAutoList = Strings.Mid(strAutoList, 1, strAutoList.Length - 1);
                        }
                        AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
                        PrintGrid.Editable = FCGrid.EditableSettings.flexEDNone;
                        cmbBillsToPrint.Enabled = false;
                        // Frame2.Enabled = False
                    }
                }
            }
            if (boolFromSupp)
            {
                //FC:FINAL:AM:#1528 - don't show as modal
                //this.Show(FormShowEnum.Modal);
                Show(App.MainForm);
            }
            else
            {
                Show(App.MainForm);
            }
        }

        private void SetupGrids()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            DateTime dtTemp;
            clsLoad.OpenRecordset("select defaultyear from defaultstable", "twbl0000.vb1");
            if (!clsLoad.EndOfFile())
            {
                if (Conversion.Val(clsLoad.Get_Fields_Int16("defaultyear")) > 0)
                {
                    txtYear.Text = FCConvert.ToString(clsLoad.Get_Fields_Int16("defaultyear"));
                }
            }
            clsLoad.OpenRecordset("select * from billsetup", "twbl0000.vb1");
            if (clsLoad.EndOfFile())
            {
                clsLoad.AddNew();
                clsLoad.Set_Fields("billtype", 0);
                clsLoad.Set_Fields("orderby", 0);
                clsLoad.Set_Fields("billstoprint", 1);
                clsLoad.Set_Fields("discount", false);
                clsLoad.Set_Fields("distribution", false);
                clsLoad.Set_Fields("distributiontotals", false);
                clsLoad.Set_Fields("returnaddress", false);
                // clsLoad.Fields("usepreviousowner") = False
                // clsLoad.Fields("Previousownerasof") = 0
                clsLoad.Set_Fields("PrintCopyformortgageholders", false);
                clsLoad.Set_Fields("UseLaserAdjustment", false);
                clsLoad.Set_Fields("LaserAdjustment", 0);
                clsLoad.Set_Fields("ClearLaserBoxes", false);
            }
            if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("uselaseradjustment")))
            {
                chkAdjustment.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkAdjustment.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("clearlaserboxes")))
            {
                chkTransparentBoxes.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkTransparentBoxes.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            txtAdjustment.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("LaserAdjustment")));
            if (clsTaxFormat.Billformat >= 14)
            {
                framAdjustment.Visible = true;
                framDotMatrixAdjust.Visible = false;
                if (clsTaxFormat.Billformat < 15)
                {
                    chkTransparentBoxes.Enabled = false;
                }
            }
            else
            {
                framAdjustment.Visible = false;
                framDotMatrixAdjust.Visible = true;
            }
            if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("printcopyformortgageholders")))
            {
                chkMortgage.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkMortgage.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            // If clsLoad.Fields("usepreviousownerinfo") Then
            // chkOwner.Value = vbChecked
            // Else
            // chkOwner.Value = vbUnchecked
            // End If
            // dttemp = clsLoad.Fields("previousownerasof")
            // If dttemp <> 0 Then T2KOwnerDate.Text = Format(dttemp, "MM/dd/yyyy")
            // TODO Get_Fields: Check the table for the column [billtype] and replace with corresponding Get_Field method
            if (FCConvert.ToInt32(clsLoad.Get_Fields("billtype")) == 1)
            {
                chkOwner.Enabled = false;
                T2KOwnerDate.Enabled = false;
            }
            // TODO Get_Fields: Check the table for the column [billtype] and replace with corresponding Get_Field method
            if (FCConvert.ToInt32(clsLoad.Get_Fields("billtype")) == 2 && !clsTaxFormat.Combined)
            {
                cmbBillType.SelectedIndex = 0;
            }
            else
            {
                // TODO Get_Fields: Check the table for the column [billtype] and replace with corresponding Get_Field method
                cmbBillType.SelectedIndex = FCConvert.ToInt32(clsLoad.Get_Fields("billtype"));
            }
            if (!clsTaxFormat.Combined)
            {
                //optBillType[2].Enabled = false;
            }
            if (clsLoad.Get_Fields_Boolean("returnaddress") && clsTaxFormat.ReturnAddress)
            {
                cmbReturnAddress.SelectedIndex = 1;
            }
            else
            {
                cmbReturnAddress.SelectedIndex = 0;
            }
            if (!clsTaxFormat.ReturnAddress)
            {
                //optReturnAddress[1].Enabled = false;
                if (cmbReturnAddress.Items.Contains("Show Return Address"))
                {
                    cmbReturnAddress.Items.Remove("Show Return Address");
                }
            }
            // TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
            if (clsLoad.Get_Fields("discount") && clsTaxFormat.Discount)
            {
                cmbDiscount.SelectedIndex = 1;
            }
            else
            {
                cmbDiscount.SelectedIndex = 0;
            }
            if (!clsTaxFormat.Discount)
            {
                //optDiscount[1].Enabled = false;
                if (cmbDiscount.Items.Contains("Show Discount"))
                {
                    cmbDiscount.Items.Remove("Show Discount");
                }
            }
            if (clsLoad.Get_Fields_Boolean("distribution") && clsTaxFormat.Breakdown)
            {
                if (clsLoad.Get_Fields_Boolean("distributiontotals") && clsTaxFormat.BreakdownDollars)
                {
                    cmbDistribution.SelectedIndex = 2;
                }
                else
                {
                    cmbDistribution.SelectedIndex = 1;
                }
            }
            else
            {
                cmbDistribution.SelectedIndex = 0;
            }
            if (!clsTaxFormat.BreakdownDollars)
            {
                //optDistribution[2].Enabled = false;
                //FC:FINAL:CHN - issue #1494: Incorrect item text.
                // if (cmbDistribution.Items.Contains("Show Breakdown by Percentages"))
                // {
                // 	cmbDistribution.Items.Remove("Show Breakdown by Percentages");
                // }
                if (cmbDistribution.Items.Contains("Show Breakdown by Dollars and %"))
                {
                    cmbDistribution.Items.Remove("Show Breakdown by Dollars and %");
                }
            }
            if (!clsTaxFormat.Breakdown)
            {
                //optDistribution[1].Enabled = false;
                if (cmbDistribution.Items.Contains("Show Breakdown by Dollars and %"))
                {
                    cmbDistribution.Items.Remove("Show Breakdown by Dollars and %");
                }
            }
            cmbOrderBills.SelectedIndex = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("orderby")));
            FillPrintGrid();
            PrintGrid.Row = 1;
            PrintGrid.RowSel = 1;
        }

        private void ResizeAccountsGrid()
        {
            //FC:FINAL:DDU: height did at design time
            //if (AccountsGrid.Rows > 0)
            //{
            //    AccountsGrid.HeightOriginal = 9 * AccountsGrid.RowHeight(0) + 60;
            //}
        }

        private void FillOpens()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            clsLoad.OpenRecordset("select * from PPRatioOpens", "twpp0000.vb1");
            strOpen1 = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("openfield1")));
            strOpen2 = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("openfield2")));
            if (strOpen1 == string.Empty)
                strOpen1 = "Open 1";
            if (strOpen2 == string.Empty)
                strOpen2 = "Open 2";
        }

        private string BuildSQLStatement()
        {
            string BuildSQLStatement = "";
            string strWhere = "";
            string strRE = "";
            string strTbl1 = "";
            string strQry1 = "";
            string strQry2 = "";
            string strQry3 = "";
            string strTbl2 = "";
            string strSQL = "";
            int lngYear;
            string strOrderBy = "";
            clsDRWrapper clsBills = new clsDRWrapper();
            // vbPorter upgrade warning: intRes As short, int --> As DialogResult
            DialogResult intRes;
            string strPP = "";
            int x;
            bool boolValidAccounts = false;
            string strZeroBalWhere = "";
            int lngTemp;
            string strTemp = "";
            string strRateKey = "";
            string strWhereRateKey = "";
            string strBillingMaster = "";

            lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
            strBillingMaster = " billingmaster.ID,billingmaster.Account ,[BillingType] ,[BillingYear] ,[Name1] ,[Name2] ,[Address1] ,[Address2] ,[Address3] ,[MapLot]";
            strBillingMaster += ",[streetnumber] ,[Apt],[StreetName],[LandValue],[BuildingValue],[ExemptValue],[TranCode],[LandCode],[BuildingCode],[OtherCode1]";
            strBillingMaster += ",[OtherCode2],[TaxDue1],[TaxDue2],[TaxDue3],[TaxDue4],[LienRecordNumber],[PrincipalPaid],[InterestPaid] ,[InterestCharged],[DemandFees]";
            strBillingMaster += " ,[DemandFeesPaid],[InterestAppliedThroughDate],[RateKey],[TransferFromBillingDateFirst],[TransferFromBillingDateLast],[WhetherBilledBefore],[OwnerGroup]";
            strBillingMaster += " ,[Category1],[Category2],[Category3],[Category4],[Category5],[Category6],[Category7],[Category8],[Category9],[Acres]";
            strBillingMaster += " ,[HomesteadExemption],[OtherExempt1],[OtherExempt2],[BookPage],[PPAssessment],[Exempt1],[Exempt2],[Exempt3],[LienStatusEligibility]";
            strBillingMaster += ",[LienProcessStatus],[Copies] ,[CertifiedMailNumber] ,[AbatementPaymentMade] ,[TGMixedAcres] ,[TGSoftAcres] ,[TGHardAcres] ,[TGMixedValue]";
            strBillingMaster += ",[TGSoftValue],[TGHardValue],[Ref2],[Ref1],[ShowRef1],[Zip],[LienProcessExclusion],[IMPBTrackingNumber],[MailingAddress3]";

            if (boolFromSupplemental)
            {
                strZeroBalWhere = "";
                if (cmbOrderBills.SelectedIndex == 0)
                {
                    strOrderBy = " name1";
                }
                else if (cmbOrderBills.SelectedIndex == 1)
                {
                    strOrderBy = " zip";
                }
                else
                {
                    strOrderBy = " Account";
                }
                strWhere = " where ID in (" + strAutoList + ") ";
                strSQL = "Select * from billingmaster " + strWhere + " order by " + strOrderBy;
                BuildSQLStatement = strSQL;
                return BuildSQLStatement;
            }
            else if (cmbBillType.SelectedIndex == 0)
            {
                // re
                strZeroBalWhere = "";
                if (chkEliminateZeroBalance.CheckState == Wisej.Web.CheckState.Checked)
                {
                    // strZeroBalWhere = " where (taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid) > 0"
                    strZeroBalWhere = " and (taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid) > 0 ";
                }
                // use the twre link table so that we can join with the association table
                if (cmbOrderBills.SelectedIndex == 0)
                {
                    // name
                    strOrderBy = " name1";
                }
                else if (cmbOrderBills.SelectedIndex == 1)
                {
                    // zip
                    strOrderBy = "zip";
                }
                else
                {
                    // account
                    strOrderBy = "account";
                }
                // see what accounts are included
                if (cmbBillsToPrint.SelectedIndex == 0)
                {
                    // all
                    strWhere = "";
                }
                else if (cmbBillsToPrint.SelectedIndex == 1)
                {
                    // individual
                    if (AccountsGrid.Rows < 1)
                    {
                        MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        BuildSQLStatement = "";
                        return BuildSQLStatement;
                    }
                    else
                    {
                        if (AccountsGrid.Rows == 1)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(0, 0)) < 1)
                            {
                                MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                BuildSQLStatement = "";
                                return BuildSQLStatement;
                            }
                        }
                        strWhere = " and account in (";
                        boolValidAccounts = false;
                        for (x = 0; x <= AccountsGrid.Rows - 1; x++)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(x, 0)) > 0)
                            {
                                if (boolValidAccounts)
                                {
                                    strWhere += ",";
                                }
                                strWhere += FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, 0)));
                                boolValidAccounts = true;
                            }
                        }
                        // x
                        strWhere += ")";
                        if (!boolValidAccounts)
                        {
                            MessageBox.Show("You must specify valid accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            BuildSQLStatement = "";
                            return BuildSQLStatement;
                        }
                    }
                }
                else
                {
                    switch (PrintGrid.Row)
                    {
                        case PRTROWACCOUNT:
                            {
                                // account
                                if ((Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1)) == 0) || (Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)) == 0))
                                {
                                    MessageBox.Show("Invalid account range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere = " and account between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)));
                                break;
                            }
                        case PRTROWNAME:
                            {
                                // name
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid name range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) != string.Empty)
                                {
                                    strWhere = " and name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) + "zzz'";
                                }
                                else
                                {
                                    strWhere = " and name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWLOCATION:
                            {
                                // location
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid location range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) != string.Empty)
                                {
                                    strWhere = " and streetname between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) + "zzz'";
                                }
                                else
                                {
                                    strWhere = " and streetname between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWMAPLOT:
                            {
                                // maplot
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid MapLot range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 2)) != string.Empty)
                                {
                                    strWhere = " and maplot between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and maplot between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWTRAN:
                            {
                                // trancode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWTRAN, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWTRAN, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid trancode range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere = " and trancode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWTRAN, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWTRAN, 2)));
                                break;
                            }
                        case PRTROWLAND:
                            {
                                // landcode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWLAND, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWLAND, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid Land code range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere = " and landcode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWLAND, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWLAND, 2)));
                                break;
                            }
                        case PRTROWBLDG:
                            {
                                // bldgcode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWBLDG, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWBLDG, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid trancode range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere = " and buildingcode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWBLDG, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWBLDG, 2)));
                                break;
                            }
                    }
                    //end switch
                }
                strRE = "select * from master innerjoin [";
                strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, true, "RE");
                if (strTemp == "CANCEL")
                {
                    BuildSQLStatement = "";
                    return BuildSQLStatement;
                }
                strTbl1 = "select * from billingmaster  where billingtype = 'RE' and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9);
                if (strTemp != string.Empty)
                {
                    strTbl1 += " and " + strTemp;
                    // & " )"
                }
                strSQL = strTbl1 + " " + strZeroBalWhere + " " + strWhere + " order by " + strOrderBy;
                BuildSQLStatement = strSQL;
            }
            else if (cmbBillType.SelectedIndex == 1)
            {
                // pp
                // see if they are doing all pp or just those not combined with re
                strZeroBalWhere = "";
                if (chkEliminateZeroBalance.CheckState == Wisej.Web.CheckState.Checked)
                {
                    strZeroBalWhere = " and (taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid) > 0";
                }
                if (cmbOrderBills.SelectedIndex == 0)
                {
                    // name
                    strOrderBy = " order by name1";
                }
                else if (cmbOrderBills.SelectedIndex == 1)
                {
                    // zip
                    strOrderBy = " order by zip";
                }
                else
                {
                    // account
                    strOrderBy = " order by account";
                }
                if (cmbBillsToPrint.SelectedIndex == 0)
                {
                    // all
                    strWhere = "";
                }
                else if (cmbBillsToPrint.SelectedIndex == 1)
                {
                    // individual
                    if (AccountsGrid.Rows < 1)
                    {
                        MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        BuildSQLStatement = "";
                        return BuildSQLStatement;
                    }
                    else
                    {
                        if (AccountsGrid.Rows == 1)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(0, 0)) < 1)
                            {
                                MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                BuildSQLStatement = "";
                                return BuildSQLStatement;
                            }
                        }
                        strWhere = " and billingmaster.account in (";
                        boolValidAccounts = false;
                        for (x = 0; x <= AccountsGrid.Rows - 1; x++)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(x, 0)) > 0)
                            {
                                if (boolValidAccounts)
                                {
                                    strWhere += ",";
                                }
                                strWhere += FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, 0)));
                                boolValidAccounts = true;
                            }
                        }
                        // x
                        strWhere += ")";
                        if (!boolValidAccounts)
                        {
                            MessageBox.Show("You must specify valid accounts", "Specify Valid Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            BuildSQLStatement = "";
                            return BuildSQLStatement;
                        }
                    }
                }
                else
                {
                    // range
                    switch (PrintGrid.Row)
                    {
                        case PRTROWACCOUNT:
                            {
                                // account
                                if ((Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1)) == 0) || (Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)) == 0))
                                {
                                    MessageBox.Show("Invalid account range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere = " and billingmaster.account between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)));
                                break;
                            }
                        case PRTROWNAME:
                            {
                                // name
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid name range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) != string.Empty)
                                {
                                    strWhere = " and billingmaster.name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) + "zzz'";
                                }
                                else
                                {
                                    strWhere = " and billingmaster.name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWLOCATION:
                            {
                                // location
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid location range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) != string.Empty)
                                {
                                    strWhere = " and billingmaster.street between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) + "zzz'";
                                }
                                else
                                {
                                    strWhere = " and billingmaster.street between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWBUSCODE:
                            {
                                // business code
                                if ((Conversion.Val(PrintGrid.TextMatrix(PRTROWBUSCODE, 1)) == 0) || (Conversion.Val(PrintGrid.TextMatrix(PRTROWBUSCODE, 2)) == 0))
                                {
                                    MessageBox.Show("Invalid business code range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                // strWhere = " and billingmaster.businesscode between " & Val(PrintGrid.TextMatrix(PRTROWBUSCODE, 1)) & " and " & Val(PrintGrid.TextMatrix(PRTROWBUSCODE, 2))
                                break;
                            }
                        case PRTROWOPEN1:
                            {
                                // open1
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid Open1 range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 2)) != string.Empty)
                                {
                                    strWhere = " and billingmaster.open1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and billingmaster.open1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWOPEN2:
                            {
                                // open2
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid Open2 range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 2)) != string.Empty)
                                {
                                    strWhere = " and billingmaster .open2 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and billingmaster .open2 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 1)) + "zzz'";
                                }
                                break;
                            }
                    }
                    //end switch
                }
                if (!boolFromSupplemental)
                {
                    intRes = MessageBox.Show("Do you wish to exclude accounts that would be combined with Real Estate Bills?", "Exclude Accounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }
                else
                {
                    intRes = DialogResult.No;
                }
                if (intRes == DialogResult.Yes)
                {

                    strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, true, "PP");
                    if (strTemp == "CANCEL")
                    {
                        BuildSQLStatement = "";
                        return BuildSQLStatement;
                    }
                    // strTbl1 = "select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as exempt,ratekey from billingmaster left join [(select * from moduleassociation where module = 'PP' and primaryassociate = -1)]. as Tbl1 on (billingmaster.account = tbl1.account) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'PP' and billingmaster.billingyear between " & lngYear * 10 & " and " & lngYear * 10 + 9
                    strTbl1 = "select billingmaster.* from billingmaster left join (select * from " + clsBills.CurrentPrefix + "CentralData.dbo.moduleassociation where module = 'PP' and primaryassociate = -1) tbl1 on (billingmaster.account = tbl1.account) where  isnull(tbl1.primaryassociate,0) = 1 and  billingmaster.billingtype = 'PP' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9);
                    if (strTemp != string.Empty)
                    {
                        strTbl1 += " and (" + strTemp + ")";
                    }
                    // End If
                    // strTbl1 = strTbl1 & " " & strZeroBalWhere
                    strSQL = strTbl1 + " " + strZeroBalWhere + strWhere + strOrderBy;
                    BuildSQLStatement = strSQL;
                }
                else
                {
                    // both
                    // strTbl1 = "select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as exempt,ratekey from billingmaster  where   billingmaster.billingtype = 'PP' and billingmaster.billingyear between " & lngYear * 10 & " and " & lngYear * 10 + 9 & ""
                    strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, true, "PP");
                    if (strTemp == "CANCEL")
                    {
                        BuildSQLStatement = "";
                        return BuildSQLStatement;
                    }
                    strTbl1 = "select billingmaster.* from billingmaster  where   billingmaster.billingtype = 'PP' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9);
                    if (strTemp != string.Empty)
                    {
                        strTbl1 += " and (" + strTemp + ")";
                    }
                    // End If
                    strSQL = strTbl1 + " " + strWhere + " " + strZeroBalWhere + strOrderBy;
                    BuildSQLStatement = strSQL;
                }
            }
            else
            {
                // combined
                // use the twre link table so that we can join with the association table
                strZeroBalWhere = "";
                // If chkEliminateZeroBalance.Value = vbChecked Then
                // strZeroBalWhere = " having (sum(taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid)) > 0"
                // End If
                if (chkEliminateZeroBalance.CheckState == Wisej.Web.CheckState.Checked)
                {
                    strZeroBalWhere = " and ccur(taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid) > 0";
                }
                if (cmbOrderBills.SelectedIndex == 0)
                {
                    // name
                    strOrderBy = " order by name1";
                }
                else if (cmbOrderBills.SelectedIndex == 1)
                {
                    // zip
                    strOrderBy = " order by zip";
                }
                else
                {
                    // account
                    strOrderBy = " order by account";
                }
                strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, false);
                strWhere = " where " + strTemp;
                strRateKey = " and " + strTemp;
                strWhereRateKey = strWhere;
                // see what accounts are included
                if (cmbBillsToPrint.SelectedIndex == 0)
                {
                    // all
                    // strWhere = ""
                }
                else if (cmbBillsToPrint.SelectedIndex == 1)
                {
                    // individual
                    if (AccountsGrid.Rows < 1)
                    {
                        MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        BuildSQLStatement = "";
                        return BuildSQLStatement;
                    }
                    else
                    {
                        if (AccountsGrid.Rows == 1)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(0, 0)) < 1)
                            {
                                MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                BuildSQLStatement = "";
                                return BuildSQLStatement;
                            }
                        }
                        strWhere += " and account in (";
                        boolValidAccounts = false;
                        for (x = 0; x <= AccountsGrid.Rows - 1; x++)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(x, 0)) > 0)
                            {
                                if (boolValidAccounts)
                                {
                                    strWhere += ",";
                                }
                                strWhere += FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, 0)));
                                boolValidAccounts = true;
                            }
                        }
                        // x
                        strWhere += ")";
                        if (!boolValidAccounts)
                        {
                            MessageBox.Show("You must specify valid accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            BuildSQLStatement = "";
                            return BuildSQLStatement;
                        }
                    }
                }
                else
                {
                    // range
                    switch (PrintGrid.Row)
                    {
                        case PRTROWACCOUNT:
                            {
                                // account
                                if ((Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1)) == 0) || (Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)) == 0))
                                {
                                    MessageBox.Show("Invalid account range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere += " and account between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)));
                                break;
                            }
                        case PRTROWNAME:
                            {
                                // name
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid name range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) != string.Empty)
                                {
                                    strWhere += " and name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) + "'";
                                }
                                else
                                {
                                    strWhere += " and name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWLOCATION:
                            {
                                // location
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid location range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) != string.Empty)
                                {
                                    strWhere += " and streetname between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) + "'";
                                }
                                else
                                {
                                    strWhere += " and streetname between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWMAPLOT:
                            {
                                // maplot
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid MapLot range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 2)) != string.Empty)
                                {
                                    strWhere += " and maplot between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 2)) + "'";
                                }
                                else
                                {
                                    strWhere += " and maplot between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWTRAN:
                            {
                                // trancode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWTRAN, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWTRAN, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid trancode range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere += " and trancode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWTRAN, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWTRAN, 2)));
                                break;
                            }
                        case PRTROWLAND:
                            {
                                // landcode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWLAND, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWLAND, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid Land code range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere += " and landcode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWLAND, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWLAND, 2)));
                                break;
                            }
                        case PRTROWBLDG:
                            {
                                // bldgcode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWBLDG, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWBLDG, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid Building Code range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLStatement = "";
                                    return BuildSQLStatement;
                                }
                                strWhere += " and buildingcode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWBLDG, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWBLDG, 2)));
                                break;
                            }
                    }
                    //end switch
                }
                // all pp accounts that aren't combined				
                strQry1 = "(select " + strBillingMaster + " ,0 as ppac,0 as ppbillkey from billingmaster left join (select * from " + clsBills.CurrentPrefix + "CentralData.dbo.moduleassociation where module = 'PP' and primaryassociate = 1) qry1 on (billingmaster.account = qry1.account) where  qry1.primaryassociate is null and  billingmaster.billingtype = 'PP' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ") qrya ";
                // all RE accounts that aren't combined
                strQry2 = "(select" + strBillingMaster + " ,0 as ppac,0 as ppbillkey from billingmaster left join (select * from " + clsBills.CurrentPrefix + "CentralData.dbo.moduleassociation where module = 'PP' and primaryassociate = 1) qry2 on (billingmaster.account = qry2.REMASTERACCT) where  qry2.primaryassociate is null and  billingmaster.billingtype = 'RE' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ") qryc ";
                // now get all accounts that are combined
                strTbl1 = "(select * from billingmaster inner join (select REMasterAcct,Account as ac from " + clsBills.CurrentPrefix + "CentralData.dbo.moduleassociation where module = 'PP' and primaryassociate = 1)  tbl4  on (billingmaster.account = tbl4.remasteracct) " + strWhereRateKey + " AND BILLINGTYPE = 'RE') as tbl3 ";
                // This is all combined RE records
                strTbl2 = "(select * from billingmaster where billingtype = 'PP' and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " " + strRateKey + ")  tbl5";
                strQry3 = "(select tbl3.ID as ID,tbl3.account as account,'CM' as billingtype,tbl3.billingyear as billingyear,tbl3.name1 as name1";
                strQry3 += ",tbl3.name2 as name2,tbl3.address1 as address1,tbl3.address2 as address2,tbl3.address3 as address3,tbl3.maplot as maplot";
                strQry3 += ",tbl3.streetnumber as streetnumber,tbl3.apt as apt,tbl3.streetname as streetname,tbl3.landvalue as landvalue,tbl3.buildingvalue as buildingvalue";
                strQry3 += ",isnull(tbl3.exemptvalue,0) + isnull(tbl5.exemptvalue,0) as exemptvalue,tbl3.trancode as trancode,tbl3.landcode as landcode,tbl3.buildingcode as buildingcode,tbl3.othercode1 as othercode1";
                strQry3 += ",tbl3.othercode2 as othercode2";
                strQry3 += ",isnull(tbl5.taxdue1,0) + isnull(tbl3.taxdue1,0) as taxdue1,isnull(tbl5.taxdue2,0) + isnull(tbl3.taxdue2 ,0) as taxdue2,isnull(tbl5.taxdue3 ,0) + isnull(tbl3.taxdue3,0) as taxdue3,isnull(tbl5.taxdue4 ,0) + isnull(tbl3.taxdue4 ,0) as taxdue4,tbl3.lienrecordnumber as lienrecordnumber";
                strQry3 += ",isnull(tbl5.principalpaid ,0) + isnull(tbl3.principalpaid ,0) as principalpaid,isnull(tbl5.interestpaid ,0) + isnull(tbl3.interestpaid ,0) as interestpaid,isnull(TBL5.interestcharged ,0) + isnull(tbl3.interestcharged,0)as interestcharged";
                strQry3 += ",isnull(tbl5.demandfees,0) + isnull(tbl3.demandfees,0) as demandfees,isnull(tbl5.demandfeespaid,0) + isnull(tbl3.demandfeespaid ,0) as demandfeespaid";
                strQry3 += ",tbl3.interestappliedthroughdate as interestappliedthroughdate,tbl3.ratekey as ratekey";
                strQry3 += ",tbl3.transferfrombillingdatefirst as transferfrombillingdatefirst,tbl3.transferfrombillingdatelast as transferfrombillingdatelast";
                strQry3 += ",tbl3.whetherbilledbefore as whetherbilledbefore,tbl3.ownergroup as ownergroup";
                strQry3 += ",tbl5.category1 as category1,tbl5.category2 as category2,tbl5.category3 as category3,tbl5.category4 as category4,tbl5.category5 as category5";
                strQry3 += ",tbl5.category6 as category6,tbl5.category7 as category7,tbl5.category8 as category8,tbl5.category9 as category9";
                strQry3 += ",tbl3.acres as acres,tbl3.homesteadexemption as homesteadexemption,tbl3.otherexempt1 as otherexempt1,tbl3.otherexempt2 as otherexempt2";
                strQry3 += ",tbl3.bookpage as bookpage,tbl5.ppassessment as ppassessment,tbl3.exempt1 as exempt1,tbl3.exempt2 as exempt2,tbl3.exempt3 as exempt3";
                strQry3 += ",tbl3.lienstatuseligibility as lienstatuseligibility,tbl3.lienprocessstatus as lienprocessstatus,tbl3.copies as copies,tbl3.certifiedmailnumber as certifiedmailnumber";
                strQry3 += ",tbl3.abatementpaymentmade as abatementpaymentmade,tbl3.tgmixedacres as tgmixedacres,tbl3.tgsoftacres as tgsoftacres,tbl3.tghardacres as tghardacres";
                strQry3 += ",tbl3.tgmixedvalue as tgmixedvalue,tbl3.tgsoftvalue as tgsoftvalue,tbl3.tghardvalue as tghardvalue,tbl3.ref2 as ref2,tbl3.ref1 as ref1,tbl3.showref1 as showref1,tbl3.zip as zip,TBL3.LIENPROCESSEXCLUSION as lienprocessexclusion,tbl3.IMPBTrackingNumber as IMPBTrackingNumber,tbl3.mailingAddress3 as mailingAddress3 ,tbl5.account as ppac,tbl5.ID as ppbillkey";
                strQry3 += " from " + strTbl2 + " right join " + strTbl1 + " on (tbl5.account = tbl3.ac)  ) qryh ";
                // now get all three types of records together
                strSQL = "select * from (select * from (select * from " + strQry1 + ") qryb union all (select * from " + strQry2 + ") union all (select * from " + strQry3 + ")) qryl" + strWhere + " " + strZeroBalWhere + " " + strOrderBy;
                BuildSQLStatement = strSQL;
            }
            return BuildSQLStatement;
        }

        private string BuildSQLLabelStatement()
        {
            string BuildSQLLabelStatement = "";
            string strWhere = "";
            string strRE = "";
            string strTbl1 = "";
            string strQry1 = "";
            string strQry2 = "";
            string strQry3 = "";
            string strTbl2 = "";
            string strSQL = "";
            int lngYear;
            string strOrderBy = "";
            clsDRWrapper clsBills = new clsDRWrapper();
            // vbPorter upgrade warning: intRes As short, int --> As DialogResult
            DialogResult intRes;
            string strPP = "";
            int x;
            bool boolValidAccounts = false;
            string strZeroBalWhere = "";
            int lngTemp;
            string strTemp = "";
            string strRateKey = "";
            string strWhereRateKey = "";
            lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
            if (boolFromSupplemental)
            {
                strZeroBalWhere = "";
                if (cmbOrderBills.SelectedIndex == 0)
                {
                    strOrderBy = " name1";
                }
                else if (cmbOrderBills.SelectedIndex == 1)
                {
                    strOrderBy = " zip";
                }
                else
                {
                    strOrderBy = " Account";
                }
                strWhere = " where ID in (" + strAutoList + ") ";
                strSQL = "Select * from billingmaster " + strWhere + " order by " + strOrderBy;
                BuildSQLLabelStatement = strSQL;
                return BuildSQLLabelStatement;
            }
            else if (cmbBillType.SelectedIndex == 0)
            {
                // re
                strZeroBalWhere = "";
                if (chkEliminateZeroBalance.CheckState == Wisej.Web.CheckState.Checked)
                {
                    // strZeroBalWhere = " where (taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid) > 0"
                    strZeroBalWhere = " and (taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid) > 0 ";
                }
                // use the twre link table so that we can join with the association table
                if (cmbOrderBills.SelectedIndex == 0)
                {
                    // name
                    strOrderBy = " name1";
                }
                else if (cmbOrderBills.SelectedIndex == 1)
                {
                    // zip
                    strOrderBy = "zip";
                }
                else
                {
                    // account
                    strOrderBy = "account";
                }
                // see what accounts are included
                if (cmbBillsToPrint.SelectedIndex == 0)
                {
                    // all
                    strWhere = "";
                }
                else if (cmbBillsToPrint.SelectedIndex == 1)
                {
                    // individual
                    if (AccountsGrid.Rows < 1)
                    {
                        MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        BuildSQLLabelStatement = "";
                        return BuildSQLLabelStatement;
                    }
                    else
                    {
                        if (AccountsGrid.Rows == 1)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(0, 0)) < 1)
                            {
                                MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                BuildSQLLabelStatement = "";
                                return BuildSQLLabelStatement;
                            }
                        }
                        strWhere = " and account in (";
                        boolValidAccounts = false;
                        for (x = 0; x <= AccountsGrid.Rows - 1; x++)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(x, 0)) > 0)
                            {
                                if (boolValidAccounts)
                                {
                                    strWhere += ",";
                                }
                                strWhere += FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, 0)));
                                boolValidAccounts = true;
                            }
                        }
                        // x
                        strWhere += ")";
                        if (!boolValidAccounts)
                        {
                            MessageBox.Show("You must specify valid accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            BuildSQLLabelStatement = "";
                            return BuildSQLLabelStatement;
                        }
                    }
                }
                else
                {
                    switch (PrintGrid.Row)
                    {
                        case PRTROWACCOUNT:
                            {
                                // account
                                if ((Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1)) == 0) || (Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)) == 0))
                                {
                                    MessageBox.Show("Invalid account range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere = " and account between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)));
                                break;
                            }
                        case PRTROWNAME:
                            {
                                // name
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid name range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) != string.Empty)
                                {
                                    strWhere = " and name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWLOCATION:
                            {
                                // location
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid location range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) != string.Empty)
                                {
                                    strWhere = " and streetname between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and streetname between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWMAPLOT:
                            {
                                // maplot
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid MapLot range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 2)) != string.Empty)
                                {
                                    strWhere = " and maplot between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and maplot between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWTRAN:
                            {
                                // trancode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWTRAN, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWTRAN, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid trancode range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere = " and trancode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWTRAN, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWTRAN, 2)));
                                break;
                            }
                        case PRTROWLAND:
                            {
                                // landcode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWLAND, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWLAND, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid Land code range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere = " and landcode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWLAND, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWLAND, 2)));
                                break;
                            }
                        case PRTROWBLDG:
                            {
                                // bldgcode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWBLDG, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWBLDG, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid trancode range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere = " and buildingcode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWBLDG, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWBLDG, 2)));
                                break;
                            }
                    }
                    //end switch
                }
                strRE = "select * from master innerjoin [";
                strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, true, "RE");
                if (strTemp == "CANCEL" || strTemp == string.Empty)
                {
                    BuildSQLLabelStatement = "";
                    return BuildSQLLabelStatement;
                }
                strTbl1 = "select * from billingmaster  where billingtype = 'RE' and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9);
                if (strTemp != string.Empty)
                {
                    strTbl1 += " and " + strTemp;
                    // & " )"
                }
                else
                {
                }
                strSQL = strTbl1 + " " + strZeroBalWhere + " " + strWhere + " order by " + strOrderBy;
                BuildSQLLabelStatement = strSQL;
            }
            else if (cmbBillType.SelectedIndex == 1)
            {
                // pp
                // see if they are doing all pp or just those not combined with re
                strZeroBalWhere = "";
                if (chkEliminateZeroBalance.CheckState == Wisej.Web.CheckState.Checked)
                {
                    strZeroBalWhere = " and (taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid) > 0";
                }
                if (cmbOrderBills.SelectedIndex == 0)
                {
                    // name
                    // strOrderBy = " order by ppmaster.name"
                    strOrderBy = " order by name1";
                }
                else if (cmbOrderBills.SelectedIndex == 1)
                {
                    // zip
                    strOrderBy = " order by zip";
                }
                else
                {
                    // account
                    // strOrderBy = " order by ppmaster.account"
                    strOrderBy = " order by account";
                }
                if (cmbBillsToPrint.SelectedIndex == 0)
                {
                    // all
                    strWhere = "";
                }
                else if (cmbBillsToPrint.SelectedIndex == 1)
                {
                    // individual
                    if (AccountsGrid.Rows < 1)
                    {
                        MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        BuildSQLLabelStatement = "";
                        return BuildSQLLabelStatement;
                    }
                    else
                    {
                        if (AccountsGrid.Rows == 1)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(0, 0)) < 1)
                            {
                                MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                BuildSQLLabelStatement = "";
                                return BuildSQLLabelStatement;
                            }
                        }
                        strWhere = " and account in (";
                        boolValidAccounts = false;
                        for (x = 0; x <= AccountsGrid.Rows - 1; x++)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(x, 0)) > 0)
                            {
                                if (boolValidAccounts)
                                {
                                    strWhere += ",";
                                }
                                strWhere += FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, 0)));
                                boolValidAccounts = true;
                            }
                        }
                        // x
                        strWhere += ")";
                        if (!boolValidAccounts)
                        {
                            MessageBox.Show("You must specify valid accounts", "Specify Valid Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            BuildSQLLabelStatement = "";
                            return BuildSQLLabelStatement;
                        }
                    }
                }
                else
                {
                    // range
                    switch (PrintGrid.Row)
                    {
                        case PRTROWACCOUNT:
                            {
                                // account
                                if ((Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1)) == 0) || (Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)) == 0))
                                {
                                    MessageBox.Show("Invalid account range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere = " and billingmaster.account between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)));
                                break;
                            }
                        case PRTROWNAME:
                            {
                                // name
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid name range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) != string.Empty)
                                {
                                    strWhere = " and billingmaster.name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and billingmaster.name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWLOCATION:
                            {
                                // location
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid location range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) != string.Empty)
                                {
                                    strWhere = " and billingmaster.street between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and billingmaster.street between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWBUSCODE:
                            {
                                // business code
                                if ((Conversion.Val(PrintGrid.TextMatrix(PRTROWBUSCODE, 1)) == 0) || (Conversion.Val(PrintGrid.TextMatrix(PRTROWBUSCODE, 2)) == 0))
                                {
                                    MessageBox.Show("Invalid business code range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                // strWhere = " and billingmaster.businesscode between " & Val(PrintGrid.TextMatrix(PRTROWBUSCODE, 1)) & " and " & Val(PrintGrid.TextMatrix(PRTROWBUSCODE, 2))
                                break;
                            }
                        case PRTROWOPEN1:
                            {
                                // open1
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid Open1 range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 2)) != string.Empty)
                                {
                                    strWhere = " and billingmaster.open1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and billingmaster.open1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN1, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWOPEN2:
                            {
                                // open2
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid Open2 range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 2)) != string.Empty)
                                {
                                    strWhere = " and billingmaster .open2 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 2)) + "'";
                                }
                                else
                                {
                                    strWhere = " and billingmaster .open2 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWOPEN2, 1)) + "zzz'";
                                }
                                break;
                            }
                    }
                    //end switch
                }
                if (!boolFromSupplemental)
                {
                    intRes = MessageBox.Show("Do you wish to exclude accounts that would be combined with Real Estate Bills?", "Exclude Accounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }
                else
                {
                    intRes = DialogResult.No;
                }
                if (intRes == DialogResult.Yes)
                {
                    // both
                    strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, true, "PP");
                    if (strTemp == "CANCEL" || strTemp == string.Empty)
                    {
                        BuildSQLLabelStatement = "";
                        return BuildSQLLabelStatement;
                    }
                    // strTbl1 = "select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as exempt,ratekey from billingmaster left join [(select * from moduleassociation where module = 'PP' and primaryassociate = -1)]. as Tbl1 on (billingmaster.account = tbl1.account) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'PP' and billingmaster.billingyear between " & lngYear * 10 & " and " & lngYear * 10 + 9
                    strTbl1 = "select billingmaster.* from billingmaster left join [(select * from moduleassociation where module = 'PP' and primaryassociate = -1)]. as Tbl1 on (billingmaster.account = tbl1.account) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'PP' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9);
                    if (strTemp != string.Empty)
                    {
                        strTbl1 += " and (" + strTemp + ")";
                    }
                    // End If
                    // strTbl1 = strTbl1 & " " & strZeroBalWhere
                    strSQL = strTbl1 + " " + strZeroBalWhere + strWhere + strOrderBy;
                    BuildSQLLabelStatement = strSQL;
                }
                else
                {
                    // both
                    // strTbl1 = "select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as exempt,ratekey from billingmaster  where   billingmaster.billingtype = 'PP' and billingmaster.billingyear between " & lngYear * 10 & " and " & lngYear * 10 + 9 & ""
                    strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, true, "PP");
                    if (strTemp == "CANCEL")
                    {
                        BuildSQLLabelStatement = "";
                        return BuildSQLLabelStatement;
                    }
                    strTbl1 = "select billingmaster.* from billingmaster  where   billingmaster.billingtype = 'PP' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9);
                    if (strTemp != string.Empty)
                    {
                        strTbl1 += " and (" + strTemp + ")";
                    }
                    // End If
                    strSQL = strTbl1 + " " + strWhere + " " + strZeroBalWhere + strOrderBy;
                    BuildSQLLabelStatement = strSQL;
                }
            }
            else
            {
                // combined
                // use the twre link table so that we can join with the association table
                strZeroBalWhere = "";
                if (chkEliminateZeroBalance.CheckState == Wisej.Web.CheckState.Checked)
                {
                    strZeroBalWhere = " having (sum(taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid)) > 0";
                }
                if (cmbOrderBills.SelectedIndex == 0)
                {
                    // name
                    strOrderBy = " order by name1";
                }
                else if (cmbOrderBills.SelectedIndex == 1)
                {
                    // zip
                    strOrderBy = " order by zip";
                }
                else
                {
                    // account
                    strOrderBy = " order by account";
                }
                strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, false);
                if (strTemp == "CANCEL" || strTemp == string.Empty)
                {
                    BuildSQLLabelStatement = "";
                    return BuildSQLLabelStatement;
                }
                strWhere = " where " + strTemp;
                strRateKey = " and " + strTemp;
                strWhereRateKey = strWhere;
                // see what accounts are included
                if (cmbBillsToPrint.SelectedIndex == 0)
                {
                    // all
                    // strWhere = ""
                }
                else if (cmbBillsToPrint.SelectedIndex == 1)
                {
                    // individual
                    if (AccountsGrid.Rows < 1)
                    {
                        MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        BuildSQLLabelStatement = "";
                        return BuildSQLLabelStatement;
                    }
                    else
                    {
                        if (AccountsGrid.Rows == 1)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(0, 0)) < 1)
                            {
                                MessageBox.Show("You must specify the individual accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                BuildSQLLabelStatement = "";
                                return BuildSQLLabelStatement;
                            }
                        }
                        strWhere += " and account in (";
                        boolValidAccounts = false;
                        for (x = 0; x <= AccountsGrid.Rows - 1; x++)
                        {
                            if (Conversion.Val(AccountsGrid.TextMatrix(x, 0)) > 0)
                            {
                                if (boolValidAccounts)
                                {
                                    strWhere += ",";
                                }
                                strWhere += FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, 0)));
                                boolValidAccounts = true;
                            }
                        }
                        // x
                        strWhere += ")";
                        if (!boolValidAccounts)
                        {
                            MessageBox.Show("You must specify valid accounts", "Specify Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            BuildSQLLabelStatement = "";
                            return BuildSQLLabelStatement;
                        }
                    }
                }
                else
                {
                    // range
                    switch (PrintGrid.Row)
                    {
                        case PRTROWACCOUNT:
                            {
                                // account
                                if ((Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1)) == 0) || (Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)) == 0))
                                {
                                    MessageBox.Show("Invalid account range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere += " and account between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWACCOUNT, 2)));
                                break;
                            }
                        case PRTROWNAME:
                            {
                                // name
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid name range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) != string.Empty)
                                {
                                    strWhere += " and name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 2)) + "'";
                                }
                                else
                                {
                                    strWhere += " and name1 between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWNAME, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWLOCATION:
                            {
                                // location
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid location range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) != string.Empty)
                                {
                                    strWhere += " and streetname between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 2)) + "'";
                                }
                                else
                                {
                                    strWhere += " and streetname between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWLOCATION, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWMAPLOT:
                            {
                                // maplot
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) == string.Empty)
                                {
                                    MessageBox.Show("Invalid MapLot range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                if (Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 2)) != string.Empty)
                                {
                                    strWhere += " and maplot between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 2)) + "'";
                                }
                                else
                                {
                                    strWhere += " and maplot between '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "' and '" + Strings.Trim(PrintGrid.TextMatrix(PRTROWMAPLOT, 1)) + "zzz'";
                                }
                                break;
                            }
                        case PRTROWTRAN:
                            {
                                // trancode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWTRAN, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWTRAN, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid trancode range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere += " and trancode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWTRAN, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWTRAN, 2)));
                                break;
                            }
                        case PRTROWLAND:
                            {
                                // landcode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWLAND, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWLAND, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid Land code range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere += " and landcode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWLAND, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWLAND, 2)));
                                break;
                            }
                        case PRTROWBLDG:
                            {
                                // bldgcode
                                if ((Strings.Trim(PrintGrid.TextMatrix(PRTROWBLDG, 1)) == string.Empty) || (Strings.Trim(PrintGrid.TextMatrix(PRTROWBLDG, 2)) == string.Empty))
                                {
                                    MessageBox.Show("Invalid Building Code range.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    BuildSQLLabelStatement = "";
                                    return BuildSQLLabelStatement;
                                }
                                strWhere += " and buildingcode between " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWBLDG, 1))) + " and " + FCConvert.ToString(Conversion.Val(PrintGrid.TextMatrix(PRTROWBLDG, 2)));
                                break;
                            }
                    }
                    //end switch
                }
                // all pp accounts that aren't combined
                strQry1 = "(select billingmaster.* ,0 as ppac from billingmaster left join [select * from moduleassociation where module = 'PP' and primaryassociate = -1]. as qry1 on (billingmaster.account = qry1.account) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'PP' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ") ";
                // all RE accounts that aren't combined
                strQry2 = "(select billingmaster.* ,0 as ppac from billingmaster left join [select * from moduleassociation where module = 'PP' and primaryassociate = -1]. as qry2 on (billingmaster.account = qry2.REMASTERACCT) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'RE' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")  ";
                // now get all accounts that are combined
                strTbl1 = "(select * from billingmaster inner join [(select REMasterAcct,Account as ac from moduleassociation where module = 'PP' and primaryassociate = -1)]. as tbl4  on (billingmaster.account = tbl4.remasteracct) " + strWhereRateKey + " AND BILLINGTYPE = 'RE') as tbl3 ";
                // This is all combined RE records
                strTbl2 = "(select * from billingmaster where billingtype = 'PP' and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " " + strRateKey + ") as tbl5";
                strQry3 = "(select tbl3.ID as ID,tbl3.account as account,'CM' as billingtype,tbl3.billingyear as billingyear,tbl3.name1 as name1";
                strQry3 += ",tbl3.name2 as name2,tbl3.address1 as address1,tbl3.address2 as address2,tbl3.address3 as address3,tbl3.maplot as maplot";
                strQry3 += ",tbl3.streetnumber as streetnumber,tbl3.apt as apt,tbl3.streetname as streetname,tbl3.landvalue as landvalue,tbl3.buildingvalue as buildingvalue";
                strQry3 += ",tbl3.exemptvalue as exemptvalue,tbl3.trancode as trancode,tbl3.landcode as landcode,tbl3.buildingcode as buildingcode,tbl3.othercode1 as othercode1";
                strQry3 += ",tbl3.othercode2 as othercode2";
                strQry3 += ",val(tbl5.taxdue1 & '') + val(tbl3.taxdue1 & '') as taxdue1,val(tbl5.taxdue2 & '') + val(tbl3.taxdue2 & '') as taxdue2,val(tbl5.taxdue3 & '') + val(tbl3.taxdue3 & '') as taxdue3,val(tbl5.taxdue4 & '') + val(tbl3.taxdue4 & '') as taxdue4,tbl3.lienrecordnumber as lienrecordnumber";
                strQry3 += ",val(tbl5.principalpaid & '') + val(tbl3.principalpaid & '') as principalpaid,val(tbl5.interestpaid & '') + val(tbl3.interestpaid & '') as interestpaid,val(TBL5.interestcharged & '') + val(tbl3.interestcharged & '')as interestcharged";
                strQry3 += ",val(tbl5.demandfees & '') + val(tbl3.demandfees & '') as demandfees,val(tbl5.demandfeespaid & '') + val(tbl3.demandfeespaid & '') as demandfeespaid";
                strQry3 += ",tbl3.interestappliedthroughdate as interestappliedthroughdate,tbl3.ratekey as ratekey";
                strQry3 += ",tbl3.transferfrombillingdatefirst as transferfrombillingdatefirst,tbl3.transferfrombillingdatelast as transferfrombillingdatelast";
                strQry3 += ",tbl3.whetherbilledbefore as whetherbilledbefore,tbl3.ownergroup as ownergroup";
                strQry3 += ",tbl5.category1 as category1,tbl5.category2 as category2,tbl5.category3 as category3,tbl5.category4 as category4,tbl5.category5 as category5";
                strQry3 += ",tbl5.category6 as category6,tbl5.category7 as category7,tbl5.category8 as category8,tbl5.category9 as category9";
                strQry3 += ",tbl3.acres as acres,tbl3.homesteadexemption as homesteadexemption,tbl3.otherexempt1 as otherexempt1,tbl3.otherexempt2 as otherexempt2";
                strQry3 += ",tbl3.bookpage as bookpage,tbl5.ppassessment as ppassessment,tbl3.exempt1 as exempt1,tbl3.exempt2 as exempt2,tbl3.exempt3 as exempt3";
                strQry3 += ",tbl3.lienstatuseligibility as lienstatuseligibility,tbl3.lienprocessstatus as lienprocessstatus,tbl3.copies as copies,tbl3.certifiedmailnumber as certifiedmailnumber";
                strQry3 += ",tbl3.abatementpaymentmade as abatementpaymentmade,tbl3.tgmixedacres as tgmixedacres,tbl3.tgsoftacres as tgsoftacres,tbl3.tghardacres as tghardacres";
                strQry3 += ",tbl3.tgmixedvalue as tgmixedvalue,tbl3.tgsoftvalue as tgsoftvalue,tbl3.tghardvalue as tghardvalue,tbl3.ref2 as ref2,tbl3.ref1 as ref1,tbl3.zip as zip,TBL3.SHOWREF1 as showref1,TBL3.LIENPROCESSEXCLUSION,tbl5.account as ppac";
                strQry3 += " from " + strTbl2 + " right join " + strTbl1 + " on (tbl5.account = tbl3.ac)  ) ";
                // now get all three types of records together
                strSQL = "select * from (select * from (select * from " + strQry1 + ") union all (select * from " + strQry2 + ") union all (select * from " + strQry3 + ")) " + strWhere + " " + strOrderBy;
                BuildSQLLabelStatement = strSQL;
            }
            return BuildSQLLabelStatement;
        }

        private void cmbBillsToPrint_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cmbBillsToPrint.SelectedIndex == 0)
            {
                optBillsToPrint_CheckedChanged(sender, e);
            }
            else if (cmbBillsToPrint.SelectedIndex == 1)
            {
                optBillsToPrint_CheckedChanged(sender, e);
            }
            else if (cmbBillsToPrint.SelectedIndex == 2)
            {
                optBillsToPrint_CheckedChanged(sender, e);
            }
        }

        private void cmbBillType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cmbBillType.SelectedIndex == 0)
            {
                optBillType_CheckedChanged(sender, e);
            }
            else if (cmbBillType.SelectedIndex == 1)
            {
                optBillType_CheckedChanged(sender, e);
            }
            else if (cmbBillType.SelectedIndex == 2)
            {
                optBillType_CheckedChanged(sender, e);
            }
        }

        private void cmdSaveAndContinue_Click(object sender, EventArgs e)
        {
            mnuContinue_Click(mnuContinue, EventArgs.Empty);
        }

        private void chkAdjustment_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void Label1_Click(object sender, EventArgs e)
        {
        }

        private void chkTransparentBoxes_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void txtAdjustment_TextChanged(object sender, EventArgs e)
        {
        }

        private void framDotMatrixAdjust_Enter(object sender, EventArgs e)
        {
        }
    }
}
