﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmExport.
	/// </summary>
	public partial class frmExport : BaseForm
	{
		public frmExport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExport InstancePtr
		{
			get
			{
				return (frmExport)Sys.GetInstance(typeof(frmExport));
			}
		}

		protected frmExport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		string strYearList = "";
		string strRateRecList = "";
		// vbPorter upgrade warning: strDefaultYear As string	OnWrite(string, int)
		string strDefaultYear = "";
		int lngDefaultRateRec;
		int intLastIndex;
		string strREHeader;
		string strPPHeader;
		string strREFormat;
		string strPPFormat;
		string[] strTowns = null;

		public void Init()
		{
			LoadLists();
			LoadHeaders();
			LoadTowns();
			this.Show(App.MainForm);
		}

		private void LoadTowns()
		{
			if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from tblRegions where townnumber > 0 order by townnumber", "CentralData");
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					Array.Resize(ref strTowns, FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("townnumber"))) + 1);
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					strTowns[rsLoad.Get_Fields("townnumber")] = FCConvert.ToString(rsLoad.Get_Fields_String("TownName"));
					rsLoad.MoveNext();
				}
				strTowns[0] = "Unknown";
			}
		}

		private void LoadHeaders()
		{
			strREHeader = FCConvert.ToString(Convert.ToChar(34)) + "Type" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Account" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Bill Year" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Bill Sequence" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Name 1" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Name 2" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Address 1" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Address 2" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Address 3" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "MapLot" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Street Number" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Apt" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Street Name" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Land" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Building" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Exempt" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Exempt 1" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Exempt 2" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Exempt 3" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Homestead Exemption" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Town Code" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Book Page" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Total Acres" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Soft Acres" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Soft Value" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Mixed Acres" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Mixed Value" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Hard Acres" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Hard Value" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Reference 1" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Reference 2" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Tax Due 1" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Tax Due 2" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Tax Due 3" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Tax Due 4" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Principal Paid" + FCConvert.ToString(Convert.ToChar(34));
			strREHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Interest Paid" + FCConvert.ToString(Convert.ToChar(34));
			strREFormat = "Real Estate Bill Extract is in a comma delimited format in the following order:" + "\r\n" + "\r\n";
			strREFormat += "Type" + "\r\n" + "Account" + "\r\n" + "Bill Year" + "\r\n" + "Bill Sequence" + "\r\n";
			strREFormat += "Name 1" + "\r\n" + "Name 2" + "\r\n" + "Address 1" + "\r\n";
			strREFormat += "Address 2" + "\r\n" + "Address 3" + "\r\n" + "MapLot" + "\r\n";
			strREFormat += "Street Number" + "\r\n" + "Apt" + "\r\n" + "Street Name" + "\r\n";
			strREFormat += "Land" + "\r\n" + "Building" + "\r\n" + "Exempt" + "\r\n";
			strREFormat += "Exempt 1" + "\r\n" + "Exempt 2" + "\r\n" + "Exempt 3" + "\r\n";
			strREFormat += "Homestead Exemption" + "\r\n" + "Town Code" + "\r\n";
			strREFormat += "Book Page" + "\r\n" + "Total Acres" + "\r\n" + "Soft Acres" + "\r\n";
			strREFormat += "Soft Value" + "\r\n" + "Mixed Acres" + "\r\n" + "Mixed Value" + "\r\n";
			strREFormat += "Hard Acres" + "\r\n" + "Hard Values" + "\r\n" + "Reference 1" + "\r\n";
			strREFormat += "Reference 2" + "\r\n" + "Tax Due 1" + "\r\n" + "Tax Due 2" + "\r\n";
			strREFormat += "Tax Due 3" + "\r\n" + "Tax Due 4" + "\r\n" + "Principal Paid" + "\r\n" + "Interest Paid";
			strPPHeader = FCConvert.ToString(Convert.ToChar(34)) + "Type" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Account" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Bill Year" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Bill Sequence" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Name" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Address 1" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Address 2" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Address 3" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Street Number" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Apt" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Street Name" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Town Code" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 1" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 2" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 3" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 4" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 5" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 6" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 7" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 8" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Category 9" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Assessment" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Exempt Value" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Tax Due 1" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Tax Due 2" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Tax Due 3" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Tax Due 4" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Principal Paid" + FCConvert.ToString(Convert.ToChar(34));
			strPPHeader += "," + FCConvert.ToString(Convert.ToChar(34)) + "Interest Paid" + FCConvert.ToString(Convert.ToChar(34));
			strPPFormat = "Personal Property Bill Extract is in a comma delimited format in the following order:" + "\r\n" + "\r\n";
			strPPFormat += "Type" + "\r\n" + "Account" + "\r\n" + "Bill Year" + "\r\n" + "Bill Sequence" + "\r\n";
			strPPFormat += "Name" + "\r\n" + "Address 1" + "\r\n" + "Address 2" + "\r\n" + "Address 3" + "\r\n";
			strPPFormat += "Street Number" + "\r\n" + "Apt" + "\r\n" + "Street Name" + "\r\n" + "Town Code" + "\r\n";
			strPPFormat += "Category 1" + "\r\n" + "Category 2" + "\r\n" + "Category 3" + "\r\n" + "Category 4" + "\r\n";
			strPPFormat += "Category 5" + "\r\n" + "Category 6" + "\r\n" + "Category 7" + "\r\n" + "Category 8" + "\r\n";
			strPPFormat += "Category 9" + "\r\n" + "Assessment" + "\r\n" + "Exempt Value" + "\r\n" + "Tax Due 1" + "\r\n";
			strPPFormat += "Tax Due 2" + "\r\n" + "Tax Due 3" + "\r\n" + "Tax Due 4" + "\r\n" + "Principal Paid" + "\r\n";
			strPPFormat += "Interest Paid";
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			string strReturn;
			strReturn = frmGetDirectory.InstancePtr.Init("Choose the directory to save in", "", true, "Extract Directory");
			if (strReturn != string.Empty)
			{
				if (Strings.Right(strReturn, 1) != "\\")
					strReturn += "\\";
				//FC:FINAL:MSH - i.issue #1456: txtPath will not be needed on web 
				//txtPath.Text = strReturn;
			}
		}

		private void frmExport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmExport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExport properties;
			//frmExport.FillStyle	= 0;
			//frmExport.ScaleWidth	= 9300;
			//frmExport.ScaleHeight	= 7605;
			//frmExport.LinkTopic	= "Form2";
			//frmExport.LockControls	= true;
			//frmExport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupDropDown();
			strTemp = modRegistry.GetRegistryKey("BLLastExportPath", "BL");
			//FC:FINAL:MSH - i.issue #1456: txtPath will not be needed on web 
			//txtPath.Text = strTemp;
		}

		private void SetupDropDown()
		{
			if (cmbBills.SelectedIndex == 0)
			{
				// year
				if (GridGroup.ColComboList(0) != strYearList)
				{
					GridGroup.ColComboList(0, strYearList);
					GridGroup.TextMatrix(0, 0, strDefaultYear);
				}
			}
			else
			{
				if (GridGroup.ColComboList(0) != strRateRecList)
				{
					GridGroup.ColComboList(0, strRateRecList);
					GridGroup.TextMatrix(0, 0, FCConvert.ToString(lngDefaultRateRec));
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void LoadLists()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				strRateRecList = "";
				strYearList = "";
				strDefaultYear = "";
				lngDefaultRateRec = 0;
				strSQL = "select BILLINGYEAR FROM billingmaster group by billingyear order by billingyear desc";
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
				if (!rsLoad.EndOfFile())
				{
					while (!rsLoad.EndOfFile())
					{
						if (Strings.Right(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("billingyear"))), 1) == "1")
						{
							strYearList += FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("billingyear") / 10)) + "|";
							if (strDefaultYear == string.Empty)
							{
								strDefaultYear = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("Billingyear") / 10));
							}
						}
						rsLoad.MoveNext();
					}
					if (!(strYearList == string.Empty))
					{
						strYearList = Strings.Mid(strYearList, 1, strYearList.Length - 1);
					}
					else
					{
						strYearList = "None";
					}
				}
				else
				{
					strYearList = "None";
				}
				if (strDefaultYear == string.Empty)
					strDefaultYear = "None";
				strSQL = "select * from raterec order by [year] desc ,description";
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
				if (!rsLoad.EndOfFile())
				{
					while (!rsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
						strRateRecList += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "\t" + rsLoad.Get_Fields("year") + "\t" + Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("taxrate")) * 1000, "0.000") + "|";
						if (lngDefaultRateRec == 0)
						{
							lngDefaultRateRec = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
						}
						rsLoad.MoveNext();
					}
					if (!(strRateRecList == string.Empty))
					{
						strRateRecList = Strings.Mid(strRateRecList, 1, strRateRecList.Length - 1);
					}
					else
					{
						strRateRecList = "#0|None";
					}
				}
				else
				{
					strRateRecList = "#0|None";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadLists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			string strFName;
			GridGroup.Row = -1;
			strFName = txtFilename.Text;
			if (Strings.Trim(strFName) == string.Empty)
			{
				MessageBox.Show("You must specify a filename", "No Filename", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.UCase(Strings.Right("   " + strFName, 3)) == "VB1")
			{
				MessageBox.Show("You cannot use vb1 as a file extension", "Bad Extension", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.InStr(1, strFName, ".", CompareConstants.vbTextCompare) < 1)
			{
				strFName += ".csv";
				txtFilename.Text = txtFilename.Text + ".csv";
			}
			//FC:FINAL:MSH - i.issue #1456: saving file will be in user directory on server. This checking won't be needed on web
			//if (!Directory.Exists(txtPath.Text))
			//{
			//    MessageBox.Show("Directory " + txtPath.Text + " could not be found", "Directory Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			//    return;
			//}
			//modRegistry.SaveRegistryKey("BLLastExportPath", txtPath.Text, "BL");
			if (cmbBillType.SelectedIndex == 0 || cmbBillType.SelectedIndex == 1)
			{
				ExtractREPPFile(strFName);
			}
		}

		private void ExtractREPPFile(string strFName)
		{
			string strSQL;
			string strEndRange = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (cmbRange.SelectedIndex == 1)
				{
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						MessageBox.Show("You did not specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbOrder.SelectedIndex == 1)
					{
						if (Conversion.Val(txtStart.Text) == 0 || (Conversion.Val(txtEnd.Text) == 0 && Strings.Trim(txtEnd.Text) != string.Empty))
						{
							MessageBox.Show("You did not specify a valid range of accounts", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
				}
				if (cmbBills.SelectedIndex == 0)
				{
					// year
					if (Conversion.Val(GridGroup.TextMatrix(0, 0)) == 0)
					{
						MessageBox.Show("Not a valid year", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					// raterec
					if (Conversion.Val(GridGroup.TextMatrix(0, 0)) == 0)
					{
						MessageBox.Show("Not a valid tax rate record", "Invalid Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				strSQL = "Select * from billingmaster where billINGtype = '";
				if (cmbBillType.SelectedIndex == 0)
				{
					// Real Estate
					strSQL += "RE' ";
				}
				else
				{
					// Personal Property
					strSQL += "PP' ";
				}
				if (cmbBills.SelectedIndex == 0)
				{
					// by year
					strSQL += " and billingyear between " + FCConvert.ToString(Conversion.Val(GridGroup.TextMatrix(0, 0))) + "1 and " + FCConvert.ToString(Conversion.Val(GridGroup.TextMatrix(0, 0))) + "9 ";
				}
				else
				{
					// by rate rec
					strSQL += " and ratekey = " + FCConvert.ToString(Conversion.Val(GridGroup.TextMatrix(0, 0)));
				}
				if (cmbOrder.SelectedIndex == 0)
				{
					// name order
					if (cmbRange.SelectedIndex == 1)
					{
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							strEndRange = txtStart.Text + "zzzz";
						}
						else
						{
							strEndRange = txtEnd.Text;
						}
						strSQL += " and name1 between '" + txtStart.Text + "' and '" + strEndRange + "' ";
					}
					strSQL += " order by name1,account,billingyear ";
				}
				else if (cmbOrder.SelectedIndex == 1)
				{
					// account order
					if (cmbRange.SelectedIndex == 1)
					{
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							strEndRange = txtStart.Text;
						}
						else
						{
							strEndRange = txtEnd.Text;
						}
						strSQL += " and account between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(strEndRange));
					}
					strSQL += " order by account,billingyear ";
				}
				else if (cmbOrder.SelectedIndex == 2)
				{
					// zip order
					if (cmbRange.SelectedIndex == 1)
					{
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							strEndRange = txtStart.Text;
						}
						else
						{
							strEndRange = txtEnd.Text;
						}
						strSQL += " and zip between '" + txtStart.Text + "' and '" + strEndRange + "' ";
					}
					strSQL += " order by zip,name1,account,billingyear ";
				}
				if (cmbBillType.SelectedIndex == 0)
				{
					// real estate
					ExtractRealEstate(strSQL);
				}
				else
				{
					// personal property
					ExtractPersonalProperty(strSQL);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ExtractREPPFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ExtractRealEstate(string strSQL)
		{
			bool boolFileOpen = false;
			StreamWriter ts = null;
			try
			{
				// On Error GoTo ErrorHandler
				//string strFile;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strRec = "";
				string strTemp1 = "";
				string strTemp2 = "";
				clsDRWrapper rsExempt = new clsDRWrapper();
				boolFileOpen = false;
				//FC:FINAL:MSH - i.issue #1456: file will be saved in user directory on server. txtPath will not be needed on web
				//strFile = txtPath.Text + txtFilename.Text;
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
				if (rsLoad.EndOfFile())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				rsExempt.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
				//FC:FINAL:MSH - i.issue #1456: delete file if it exists
				if (FCFileSystem.FileExists(txtFilename.Text))
				{
					FCFileSystem.DeleteFile(txtFilename.Text);
				}
				//FC:FINAL:MSH - i.issue #1456: file will be saved in user directory on server. txtPath will not be needed on web
				//ts = File.CreateText(strFile);
				ts = FCFileSystem.CreateTextFile(txtFilename.Text);
				boolFileOpen = true;
				frmWait.InstancePtr.Init("Saving ");
				if (chkHeaders.CheckState == Wisej.Web.CheckState.Checked)
				{
					ts.WriteLine(strREHeader);
				}
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					frmWait.InstancePtr.lblMessage.Text = "Saving " + rsLoad.Get_Fields("Account");
					// frmWait.lblMessage.Refresh
					//Application.DoEvents();
					strRec = "";
					strRec += FCConvert.ToString(Convert.ToChar(34)) + "RE" + FCConvert.ToString(Convert.ToChar(34));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("account")));
					strTemp1 = FCConvert.ToString(rsLoad.Get_Fields_Int32("billingyear"));
					strTemp2 = Strings.Right(strTemp1, 1);
					strTemp1 = Strings.Left(strTemp1, 4);
					strRec += "," + strTemp1;
					strRec += "," + strTemp2;
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("name1") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("name2") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("address1") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("address2") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("address3") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("maplot") + FCConvert.ToString(Convert.ToChar(34));
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("streetnumber")));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("apt") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("streetname") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("landvalue")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("buildingvalue")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("Exemptvalue")));
					if (Conversion.Val(rsLoad.Get_Fields_Int32("exempt1")) == 0)
					{
						strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
					}
					else
					{
						if (rsExempt.FindFirstRecord("Code", Conversion.Val(rsLoad.Get_Fields_Int32("exempt1"))))
						{
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsExempt.Get_Fields_String("description") + FCConvert.ToString(Convert.ToChar(34));
						}
						else
						{
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
						}
					}
					if (Conversion.Val(rsLoad.Get_Fields_Int32("exempt2")) == 0)
					{
						strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
					}
					else
					{
						if (rsExempt.FindFirstRecord("Code", Conversion.Val(rsLoad.Get_Fields_Int32("exempt2"))))
						{
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsExempt.Get_Fields_String("description") + FCConvert.ToString(Convert.ToChar(34));
						}
						else
						{
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
						}
					}
					if (Conversion.Val(rsLoad.Get_Fields_Int32("exempt3")) == 0)
					{
						strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
					}
					else
					{
						if (rsExempt.FindFirstRecord("Code", Conversion.Val(rsLoad.Get_Fields_Int32("exempt3"))))
						{
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsExempt.Get_Fields_String("description") + FCConvert.ToString(Convert.ToChar(34));
						}
						else
						{
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
						}
					}
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("homesteadexemption")));
					if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
					{
						strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						if (Information.UBound(strTowns, 1) <= Conversion.Val(rsLoad.Get_Fields("trancode")))
						{
							// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + strTowns[rsLoad.Get_Fields("trancode")] + FCConvert.ToString(Convert.ToChar(34));
						}
						else
						{
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + "Unknown" + FCConvert.ToString(Convert.ToChar(34));
						}
					}
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("bookpage") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("acres")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("TGSoftAcres")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("TGSoftValue")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("TGMixedAcres")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("TGMixedValue")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("TGHardAcres")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("TGHardValue")));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("ref1") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("ref2") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("taxdue1")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("taxdue2")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("taxdue3")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("taxdue4")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("PrincipalPaid")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("InterestPaid")));
					ts.WriteLine(strRec);
					rsLoad.MoveNext();
				}
				boolFileOpen = false;
				ts.Close();
				frmWait.InstancePtr.Unload();
				MessageBox.Show("File " + txtFilename.Text + " saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//FC:FINAL:MSH - i.issue #1456 - Use Application.Download to download the file to client side
				FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, txtFilename.Text), txtFilename.Text);
				frmShowResults.InstancePtr.Init("File is named " + txtFilename.Text + "\r\n" + strREFormat);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				if (boolFileOpen)
				{
					ts.Close();
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ExtractRealEstate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			finally
			{
				if (ts != null)
				{
					ts.Close();
				}
			}
		}

		private void ExtractPersonalProperty(string strSQL)
		{
			bool boolFileOpen = false;
			StreamWriter ts = null;
			try
			{
				// On Error GoTo ErrorHandler
				//string strFile;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strRec = "";
				string strTemp1 = "";
				string strTemp2 = "";
				boolFileOpen = false;
				//FC:FINAL:MSH - i.issue #1456: file will be saved in user directory on server. txtPath will not be needed on web
				//strFile = txtPath.Text + txtFilename.Text;
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
				if (rsLoad.EndOfFile())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				//FC:FINAL:MSH - i.issue #1456: delete file if it exists
				if (FCFileSystem.FileExists(txtFilename.Text))
				{
					FCFileSystem.DeleteFile(txtFilename.Text);
				}
				//FC:FINAL:MSH - i.issue #1456: file will be saved in user directory on server. txtPath will not be needed on web
				//ts = File.CreateText(strFile);
				ts = FCFileSystem.CreateTextFile(txtFilename.Text);
				boolFileOpen = true;
				frmWait.InstancePtr.Init("Saving ");
				if (chkHeaders.CheckState == Wisej.Web.CheckState.Checked)
				{
					ts.WriteLine(strPPHeader);
				}
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					frmWait.InstancePtr.lblMessage.Text = "Saving " + rsLoad.Get_Fields("Account");
					// frmWait.lblMessage.Refresh
					//Application.DoEvents();
					strRec = "";
					strRec += FCConvert.ToString(Convert.ToChar(34)) + "PP" + FCConvert.ToString(Convert.ToChar(34));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("account")));
					strTemp1 = FCConvert.ToString(rsLoad.Get_Fields_Int32("billingyear"));
					strTemp2 = Strings.Right(strTemp1, 1);
					strTemp1 = Strings.Left(strTemp1, 4);
					strRec += "," + strTemp1;
					strRec += "," + strTemp2;
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("name1") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("address1") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("address2") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("address3") + FCConvert.ToString(Convert.ToChar(34));
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("streetnumber")));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("apt") + FCConvert.ToString(Convert.ToChar(34));
					strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + rsLoad.Get_Fields_String("streetname") + FCConvert.ToString(Convert.ToChar(34));
					if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
					{
						strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						if (Information.UBound(strTowns, 1) <= Conversion.Val(rsLoad.Get_Fields("trancode")))
						{
							// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + strTowns[rsLoad.Get_Fields("trancode")] + FCConvert.ToString(Convert.ToChar(34));
						}
						else
						{
							strRec += "," + FCConvert.ToString(Convert.ToChar(34)) + "Unknown" + FCConvert.ToString(Convert.ToChar(34));
						}
					}
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category1")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category2")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category3")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category4")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category5")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category6")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category7")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category8")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("category9")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ppassessment")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("exemptvalue")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("taxdue1")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("taxdue2")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("taxdue3")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("taxdue4")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("PrincipalPaid")));
					strRec += "," + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Decimal("InterestPaid")));
					ts.WriteLine(strRec);
					rsLoad.MoveNext();
				}
				boolFileOpen = false;
				ts.Close();
				frmWait.InstancePtr.Unload();
				MessageBox.Show("File " + txtFilename.Text + "saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//FC:FINAL:MSH - i.issue #1456 - Use Application.Download to download the file to client side
				FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, txtFilename.Text), txtFilename.Text);
				frmShowResults.InstancePtr.Init("File is named " + txtFilename.Text + "\r\n" + strPPFormat);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				if (boolFileOpen)
				{
					ts.Close();
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ExtractPersonalProperty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuViewFormat_Click(object sender, System.EventArgs e)
		{
			if (cmbBillType.SelectedIndex == 0)
			{
				// real estate
				frmShowResults.InstancePtr.Init(strREFormat);
			}
			else
			{
				// personal property
				frmShowResults.InstancePtr.Init(strPPFormat);
			}
		}

		private void optBills_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			SetupDropDown();
		}

		private void optBills_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbBills.SelectedIndex);
			optBills_CheckedChanged(index, sender, e);
		}

		private void optOrder_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index != intLastIndex)
			{
				txtStart.Text = "";
				txtEnd.Text = "";
				intLastIndex = Index;
			}
		}

		private void optOrder_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbOrder.SelectedIndex);
			optOrder_CheckedChanged(index, sender, e);
		}

		private void optRange_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// all
						txtStart.Visible = false;
						lblTo.Visible = false;
						txtEnd.Visible = false;
						break;
					}
				case 1:
					{
						// range
						txtStart.Visible = true;
						lblTo.Visible = true;
						txtEnd.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbRange.SelectedIndex);
			optRange_CheckedChanged(index, sender, e);
		}

		private void cmbBills_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbBills.SelectedIndex == 0)
			{
				optBills_CheckedChanged(sender, e);
			}
			else if (cmbBills.SelectedIndex == 1)
			{
				optBills_CheckedChanged(sender, e);
			}
		}

		private void cmbRange_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbRange.SelectedIndex == 0)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbRange.SelectedIndex == 1)
			{
				optRange_CheckedChanged(sender, e);
			}
		}

		private void cmbOrder_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbOrder.SelectedIndex == 0)
			{
				optOrder_CheckedChanged(sender, e);
			}
			else if (cmbOrder.SelectedIndex == 1)
			{
				optOrder_CheckedChanged(sender, e);
			}
			else if (cmbOrder.SelectedIndex == 2)
			{
				optOrder_CheckedChanged(sender, e);
			}
		}

		private void cmdSaveAndContinue_Click(object sender, EventArgs e)
		{
			mnuSaveExit_Click(mnuSaveExit, EventArgs.Empty);
		}
	}
}
