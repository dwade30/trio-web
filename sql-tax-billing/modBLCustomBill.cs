﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWBL0000
{
	public class modBLCustomBill
	{
		//=========================================================
		const int CNSTRECATEGORY = 5;
		const int CNSTPPCATEGORY = 6;

		public struct AddrType
		{
			public string[] strAddress;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public AddrType(int unusedParam)
			{
				this.strAddress = new string[4] {
					string.Empty,
					string.Empty,
					string.Empty,
					string.Empty
				};
			}
		};

		public static void LoadReturnAddressesForBills()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			if (!modRegionalTown.IsRegionalTown())
			{
				Statics.aryReturnAddress = new AddrType[1 + 1];
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				Statics.aryReturnAddress[0] = new AddrType(0);
				Statics.boolIsRegional = false;
				rsLoad.OpenRecordset("select * from returnaddress", modGlobalVariables.strBLDatabase);
				Statics.aryReturnAddress[0].strAddress[0] = FCConvert.ToString(rsLoad.Get_Fields_String("address1"));
				Statics.aryReturnAddress[0].strAddress[1] = FCConvert.ToString(rsLoad.Get_Fields_String("address2"));
				Statics.aryReturnAddress[0].strAddress[2] = FCConvert.ToString(rsLoad.Get_Fields_String("address3"));
				Statics.aryReturnAddress[0].strAddress[3] = FCConvert.ToString(rsLoad.Get_Fields_String("address4"));
				if (Strings.Trim(Statics.aryReturnAddress[0].strAddress[2]) == "")
				{
					Statics.aryReturnAddress[0].strAddress[2] = Strings.Trim(Statics.aryReturnAddress[0].strAddress[3]);
					Statics.aryReturnAddress[0].strAddress[3] = "";
				}
				if (Strings.Trim(Statics.aryReturnAddress[0].strAddress[1]) == "")
				{
					Statics.aryReturnAddress[0].strAddress[1] = Strings.Trim(Statics.aryReturnAddress[0].strAddress[2]);
					Statics.aryReturnAddress[0].strAddress[2] = Strings.Trim(Statics.aryReturnAddress[0].strAddress[3]);
					Statics.aryReturnAddress[0].strAddress[3] = "";
				}
				if (Strings.Trim(Statics.aryReturnAddress[0].strAddress[0]) == "")
				{
					Statics.aryReturnAddress[0].strAddress[0] = Strings.Trim(Statics.aryReturnAddress[0].strAddress[1]);
					Statics.aryReturnAddress[0].strAddress[1] = Strings.Trim(Statics.aryReturnAddress[0].strAddress[2]);
					Statics.aryReturnAddress[0].strAddress[2] = Strings.Trim(Statics.aryReturnAddress[0].strAddress[3]);
					Statics.aryReturnAddress[0].strAddress[3] = "";
				}
			}
			else
			{
				int x = 0;
				x = 0;
				Statics.boolIsRegional = true;
				rsLoad.OpenRecordset("select * from TBLREgions where townnumber > 0 order by townname", "CentralData");
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(rsLoad.Get_Fields("townnumber")) > x)
					{
						//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
						int oldX = x;
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						x = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("townnumber"))));
						Array.Resize(ref Statics.aryReturnAddress, x + 1);
						for (int i = oldX; i < x; i++)
						{
							Statics.aryReturnAddress[i] = new AddrType(0);
						}
					}
					rsLoad.MoveNext();
				}
				rsLoad.OpenRecordset("select * from returnaddress order by townnumber", modGlobalVariables.strBLDatabase);
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					Statics.aryReturnAddress[rsLoad.Get_Fields_Int32("townnumber")].strAddress[0] = FCConvert.ToString(rsLoad.Get_Fields_String("address1"));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					Statics.aryReturnAddress[rsLoad.Get_Fields_Int32("townnumber")].strAddress[1] = FCConvert.ToString(rsLoad.Get_Fields_String("address2"));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					Statics.aryReturnAddress[rsLoad.Get_Fields_Int32("townnumber")].strAddress[2] = FCConvert.ToString(rsLoad.Get_Fields_String("address3"));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					Statics.aryReturnAddress[rsLoad.Get_Fields_Int32("townnumber")].strAddress[3] = FCConvert.ToString(rsLoad.Get_Fields_String("address4"));
					rsLoad.MoveNext();
				}
			}
		}
		// vbPorter upgrade warning: intTown As short --> As int	OnWrite(short, double)
		public static void GetReturnAddressForBills(int intTown, ref string strAddress1, ref string strAddress2, ref string strAddress3, ref string strAddress4)
		{
			if (Information.UBound(Statics.aryReturnAddress, 1) >= intTown)
			{
				strAddress1 = Statics.aryReturnAddress[intTown].strAddress[0];
				strAddress2 = Statics.aryReturnAddress[intTown].strAddress[1];
				strAddress3 = Statics.aryReturnAddress[intTown].strAddress[2];
				strAddress4 = Statics.aryReturnAddress[intTown].strAddress[3];
			}
			else
			{
				strAddress1 = "";
				strAddress2 = "";
				strAddress3 = "";
				strAddress4 = "";
			}
		}
		// vbPorter upgrade warning: clsRep As object	OnWrite(clsDRWrapper)
		public static string HandleRegularCase(ref clsDRWrapper clsRep, ref modCustomBill.CustomBillCodeType clsCCode)
		{
			string HandleRegularCase = "";
			string strTemp;
			// Takes a code number and looks up the data based on the field name in the database
			try
			{
				// On Error GoTo ErrorHandler
				HandleRegularCase = "";
				// return a nullstring if this is re or pp only and the bill is of opposite type
				if (clsCCode.Category == CNSTRECATEGORY)
				{
					if (clsRep.Get_Fields_String("billingtype") == "PP")
					{
						HandleRegularCase = "";
						return HandleRegularCase;
					}
				}
				else if (clsCCode.Category == CNSTPPCATEGORY)
				{
					if (clsRep.Get_Fields_String("billingtype") == "RE")
					{
						HandleRegularCase = "";
						return HandleRegularCase;
					}
				}
				strTemp = clsRep.Get_Fields_String(clsCCode.FieldName);
				switch ((DataTypeEnum)clsCCode.Datatype)
				{
					case fecherFoundation.DataTypeEnum.dbDate:
						{
							if (clsRep.Get_Fields_Int32(clsCCode.FieldName) == 0)
							{
								strTemp = "";
							}
							else
							{
								if (clsCCode.FormatString != string.Empty)
								{
									strTemp = Strings.Format(strTemp, clsCCode.FormatString);
								}
								else
								{
									strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
								}
							}
							break;
						}
					default:
						{
							if (Strings.Trim(clsCCode.FormatString) != string.Empty)
							{
								strTemp = Strings.Format(strTemp, clsCCode.FormatString);
							}
							break;
						}
				}
				//end switch
				HandleRegularCase = strTemp;
				return HandleRegularCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleRegularCase with code " + FCConvert.ToString(clsCCode.FieldID), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleRegularCase;
		}
		// vbPorter upgrade warning: clsRep As object	OnWrite(clsDRWrapper)
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		public static string HandleSpecialCase(ref clsDRWrapper clsRep, ref modCustomBill.CustomBillCodeType clsCCode, ref string strExtraParameters)
		{
			string HandleSpecialCase = "";
			// always module specific.
			double dblTemp = 0;
			double dblTemp2 = 0;
			string strTemp = "";
			DateTime dtTemp;
			string[] strAry = null;
			DateTime dtTemp2;
			// vbPorter upgrade warning: lngTemp As int	OnWrite(int, double)
			int lngTemp = 0;
			int x;
			bool boolTemp;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strDSign;
			try
			{
				// On Error GoTo ErrorHandler
				strDSign = "";
				if (modGlobalVariables.Statics.CustomizedInfo.ShowDollarSigns)
				{
					strDSign = "$";
				}
				HandleSpecialCase = "";
				int vbPorterVar = clsCCode.FieldID;
				if (vbPorterVar == modCustomBill.CNSTCUSTOMBILLCUSTOMBARCODE)
				{
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "PP")
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						HandleSpecialCase = "R" + Strings.Format(Conversion.Val(clsRep.Get_Fields("account")), "0000000");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						HandleSpecialCase = "P" + clsRep.Get_Fields_String("account");
					}
				}
				else if (vbPorterVar == 1)
				{
					// RETURN address
					if (!Statics.boolIsRegional)
					{
						lngTemp = 0;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsRep.Get_Fields("trancode"))));
						if (lngTemp == 0)
						{
							HandleSpecialCase = "";
							return HandleSpecialCase;
						}
					}
					strTemp = "";
					if (Strings.Trim(Statics.aryReturnAddress[lngTemp].strAddress[0]) != string.Empty)
					{
						HandleSpecialCase = HandleSpecialCase + strTemp + Statics.aryReturnAddress[lngTemp].strAddress[0];
						strTemp = "\r\n";
					}
					if (Strings.Trim(Statics.aryReturnAddress[lngTemp].strAddress[1]) != "")
					{
						HandleSpecialCase = HandleSpecialCase + strTemp + Statics.aryReturnAddress[lngTemp].strAddress[1];
						strTemp = "\r\n";
					}
					if (Strings.Trim(Statics.aryReturnAddress[lngTemp].strAddress[2]) != "")
					{
						HandleSpecialCase = HandleSpecialCase + strTemp + Statics.aryReturnAddress[lngTemp].strAddress[2];
						strTemp = "\r\n";
					}
					if (Strings.Trim(Statics.aryReturnAddress[lngTemp].strAddress[3]) != "")
					{
						HandleSpecialCase = HandleSpecialCase + strTemp + Statics.aryReturnAddress[lngTemp].strAddress[3];
					}
				}
				else if (vbPorterVar == 2)
				{
					// tax rate
					lngTemp = clsRep.Get_Fields_Int32("RATEKEY");
					clsTemp.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngTemp), "twcl0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						dblTemp = Conversion.Val(clsTemp.Get_Fields_Double("taxrate")) * 1000;
						HandleSpecialCase = Strings.Format(dblTemp, "0.000");
					}
				}
				else if (vbPorterVar == 3)
				{
					// tax year
					lngTemp = clsRep.Get_Fields_Int32("ratekey");
					clsTemp.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngTemp), "twcl0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("year"))));
						HandleSpecialCase = FCConvert.ToString(lngTemp);
					}
				}
				else if (vbPorterVar == 4)
				{
					// due date
					lngTemp = clsRep.Get_Fields_Int32("ratekey");
					clsTemp.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngTemp), "twcl0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
						// the period
						if (lngTemp <= Conversion.Val(clsTemp.Get_Fields_Int16("numberofperiods")))
						{
							// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
							if (Information.IsDate(clsTemp.Get_Fields("duedate" + FCConvert.ToString(lngTemp))))
							{
								// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
								dtTemp = (DateTime)clsTemp.Get_Fields("duedate" + FCConvert.ToString(lngTemp));
								HandleSpecialCase = Strings.Format(dtTemp, strAry[1]);
							}
						}
					}
				}
				else if (vbPorterVar == 5)
				{
					// interest date
					lngTemp = clsRep.Get_Fields_Int32("ratekey");
					clsTemp.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngTemp), "twcl0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
						if (lngTemp <= Conversion.Val(clsTemp.Get_Fields_Int16("numberofperiods")))
						{
							if (Information.IsDate(clsTemp.Get_Fields("InterestSTARTdate" + FCConvert.ToString(lngTemp))))
							{
								dtTemp = DateTime.FromOADate(clsTemp.Get_Fields_Int32("interestSTARTdate" + FCConvert.ToString(lngTemp)));
								HandleSpecialCase = Strings.Format(dtTemp, strAry[1]);
							}
						}
					}
				}
				else if (vbPorterVar == 7)
				{
					// Distribution Label
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strExtraParameters)));
					clsTemp.OpenRecordset("select * from distributiontable where distributionnumber = " + FCConvert.ToString(lngTemp), "twbl0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						HandleSpecialCase = FCConvert.ToString(clsTemp.Get_Fields_String("DistributionName"));
					}
				}
				else if (vbPorterVar == 8)
				{
					// distribution Percent
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strExtraParameters)));
					clsTemp.OpenRecordset("select * from distributiontable where distributionnumber = " + FCConvert.ToString(lngTemp), "twbl0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						HandleSpecialCase = Strings.Format(Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent")), "0.00") + "%";
					}
				}
				else if (vbPorterVar == 9)
				{
					// distribution amount
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strExtraParameters)));
					dblTemp = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue4"));
					dblTemp2 = dblTemp;
					clsTemp.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						// vbPorter upgrade warning: dblDistAmount As double	OnWrite(short, string, double)
						double[] dblDistAmount = new double[20 + 1];
						x = 0;
						for (x = 1; x <= 20; x++)
						{
							// DEFAULT TO BLANK
							dblDistAmount[x] = -1;
						}
						// x
						x = 0;
						while (!clsTemp.EndOfFile() && dblTemp2 > 0)
						{
							x += 1;
							dblDistAmount[x] = FCConvert.ToDouble(Strings.Format(dblTemp * (Conversion.Val(clsTemp.Get_Fields_Double("distributionpercent")) / 100), "0.00"));
							if (dblDistAmount[x] < 0)
								dblDistAmount[x] = 0;
							if (dblDistAmount[x] > dblTemp2)
							{
								dblDistAmount[x] = dblTemp2;
								dblTemp2 = 0;
							}
							else
							{
								dblTemp2 -= dblDistAmount[x];
							}
							// If Val(clsTemp.Fields("distributionpercent")) <= 0 Then dblDistAmount(x) = -1
							clsTemp.MoveNext();
						}
						if (dblDistAmount[lngTemp] >= 0)
						{
							HandleSpecialCase = strDSign + Strings.Format(dblDistAmount[lngTemp], "#,###,##0.00");
						}
					}
				}
				else if (vbPorterVar == 10)
				{
					// billing address
					// handled directly in the class
				}
				else if (vbPorterVar == 12)
				{
					// location
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					strTemp = clsRep.Get_Fields_String("streetnumber") + " " + clsRep.Get_Fields_String("apt");
					strTemp = Strings.Trim(strTemp);
					strTemp += " " + clsRep.Get_Fields_String("streetname");
					HandleSpecialCase = Strings.Trim(strTemp);
				}
				else if (vbPorterVar == 13)
				{
					// account
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) == "RE")
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						HandleSpecialCase = "R" + clsRep.Get_Fields_String("account");
					}
					else if (Strings.UCase(clsRep.Get_Fields_String("Billingtype")) == "PP")
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						HandleSpecialCase = "P" + clsRep.Get_Fields_String("account");
					}
					else
					{
						// combined
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Field [PPAC] not found!! (maybe it is an alias?)
						HandleSpecialCase = "R" + clsRep.Get_Fields_String("account") + " " + "P" + clsRep.Get_Fields_String("PPAC");
					}
				}
				else if (vbPorterVar == 14)
				{
					// original tax due
					dblTemp = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue4"));
					HandleSpecialCase = strDSign + Strings.Format(dblTemp, "#,###,##0.00");
				}
				else if (vbPorterVar == 15)
				{
					// tax due
					dblTemp = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue4"));
					dblTemp -= Conversion.Val(clsRep.Get_Fields_Decimal("principalpaid"));
					if (dblTemp < 0)
					{
						HandleSpecialCase = "Overpaid";
					}
					else
					{
						HandleSpecialCase = strDSign + Strings.Format(dblTemp, "#,###,###,##0.00");
					}
				}
				else if (vbPorterVar == 16)
				{
					// paid to date
					dblTemp = Conversion.Val(clsRep.Get_Fields_Decimal("principalpaid"));
					HandleSpecialCase = strDSign + Strings.Format(dblTemp, "#,###,###,##0.00");
				}
				else if (vbPorterVar == 17)
				{
					// original due by period
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strExtraParameters)));
					if (lngTemp > 0 && lngTemp < 5)
					{
						// TODO Get_Fields: Field [taxdue] not found!! (maybe it is an alias?)
						dblTemp = Conversion.Val(clsRep.Get_Fields("taxdue" + FCConvert.ToString(lngTemp)));
						HandleSpecialCase = strDSign + Strings.Format(dblTemp, "#,###,###,##0.00");
					}
				}
				else if (vbPorterVar == 18)
				{
					// tax due by period
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strExtraParameters)));
					if (lngTemp > 0 && lngTemp < 5)
					{
						double[] dblTax = new double[4 + 1];
						dblTax[1] = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue1"));
						dblTax[2] = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue2"));
						dblTax[3] = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue3"));
						dblTax[4] = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue4"));
						dblTemp = Conversion.Val(clsRep.Get_Fields_Decimal("principalpaid"));
						int intPPeriods = 0;
						intPPeriods = 1;
						if (dblTax[2] > 0)
						{
							intPPeriods = 2;
						}
						if (dblTax[3] > 0)
						{
							intPPeriods = 3;
						}
						if (dblTax[4] > 0)
						{
							intPPeriods = 4;
						}
						double[] dblTAbate = new double[4 + 1];
						double dblAbated = 0;
						dblAbated = 0;
						double dblAbate1 = 0;
						double dblAbate2 = 0;
						double dblAbate3 = 0;
						double dblAbate4 = 0;
						if ((dblTax[2] > 0 || dblTax[3] > 0 || dblTax[4] > 0) && dblTemp > 0)
						{
							dblAbated = modCollectionsRelated.AutoAbatementAmount_8(clsRep.Get_Fields_Int32("ID"), FCConvert.ToInt16(intPPeriods), ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
							dblAbate1 = dblTAbate[1];
							dblAbate2 = dblTAbate[2];
							dblAbate3 = dblTAbate[3];
							dblAbate4 = dblTAbate[4];
							if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "PP" && Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
							{
								// TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
								dblAbated += modCollectionsRelated.AutoAbatementAmount_8(clsRep.Get_Fields_Int32("ppbillkey"), FCConvert.ToInt16(intPPeriods), ref dblTAbate[1], ref dblTAbate[2], ref dblTAbate[3], ref dblTAbate[4]);
								dblAbate1 += dblTAbate[1];
								dblAbate2 += dblTAbate[2];
								dblAbate3 += dblTAbate[3];
								dblAbate4 += dblTAbate[4];
							}
							dblTemp -= dblAbated;
						}
						if (dblAbated > 0)
						{
							dblTax[1] -= dblAbate1;
							dblTax[2] -= dblAbate2;
							dblTax[3] -= dblAbate3;
							dblTax[4] -= dblAbate4;
						}
						if (dblTemp > dblTax[1])
						{
							dblTemp -= dblTax[1];
							dblTax[1] = 0;
						}
						else
						{
							dblTax[1] -= dblTemp;
							dblTemp = 0;
						}
						if (dblTemp > dblTax[2])
						{
							dblTemp -= dblTax[2];
							dblTax[2] = 0;
						}
						else
						{
							dblTax[2] -= dblTemp;
							dblTemp = 0;
						}
						if (dblTemp > dblTax[3])
						{
							dblTemp -= dblTax[3];
							dblTax[3] = 0;
						}
						else
						{
							dblTax[3] -= dblTemp;
							dblTemp = 0;
						}
						if (dblTemp > dblTax[4])
						{
							dblTemp -= dblTax[4];
							dblTax[4] = 0;
						}
						else
						{
							dblTax[4] -= dblTemp;
							dblTemp = 0;
						}
						HandleSpecialCase = strDSign + Strings.Format(dblTax[lngTemp], "#,###,###,##0.00");
					}
				}
				else if (vbPorterVar == 19)
				{
					// discount amount
					dblTemp = Conversion.Val(strExtraParameters);
					// percentage
					dblTemp /= 100;
					dblTemp2 = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue4"));
					dblTemp2 -= (dblTemp2 * dblTemp);
					HandleSpecialCase = strDSign + Strings.Format(dblTemp2, "#,###,###,##0.00");
				}
				else if (vbPorterVar == 20)
				{
					// discount amount left to pay
					dblTemp = Conversion.Val(strExtraParameters);
					// percentage
					dblTemp /= 100;
					dblTemp2 = Conversion.Val(clsRep.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsRep.Get_Fields_Decimal("taxdue4"));
					dblTemp2 -= (dblTemp2 * dblTemp);
					dblTemp2 -= Conversion.Val(clsRep.Get_Fields_Decimal("principalpaid"));
					if (dblTemp2 < 0)
						dblTemp2 = 0;
					HandleSpecialCase = strDSign + Strings.Format(dblTemp2, "#,###,###,##0.00");
				}
				else if (vbPorterVar == 21)
				{
					// RE & PP Label
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) == "RE")
					{
						HandleSpecialCase = strAry[0];
					}
					else if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) == "PP")
					{
						HandleSpecialCase = strAry[1];
					}
					else
					{
						HandleSpecialCase = strAry[2];
					}
				}
				else if (vbPorterVar == 22)
				{
					// RE Label
					if (Strings.UCase(clsRep.Get_Fields_String("BillingType")) != "PP")
					{
						strTemp = strExtraParameters;
						HandleSpecialCase = strTemp;
					}
				}
				else if (vbPorterVar == 32)
				{
					// Real Estate
					if (Strings.UCase(clsRep.Get_Fields_String("BillingType")) != "PP")
					{
						// is re or combined
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("landvalue")) + Conversion.Val(clsRep.Get_Fields_Int32("buildingvalue")) - Conversion.Val(clsRep.Get_Fields_Int32("exemptvalue")));
						HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
					}
				}
				else if (vbPorterVar == 36)
				{
					if (Strings.UCase(clsRep.Get_Fields_String("BillingType")) != "PP")
					{
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("Exemptvalue")) - Conversion.Val(clsRep.Get_Fields_Double("HomesteadExemption")));
						HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
					}
				}
				else if (vbPorterVar == 37)
				{
					// PP Label
					if (Strings.UCase(clsRep.Get_Fields_String("BillingType")) != "RE")
					{
						strTemp = strExtraParameters;
						HandleSpecialCase = strTemp;
					}
				}
				else if (vbPorterVar == 40)
				{
					// taxable value
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("PPAssessment")) - Conversion.Val(clsRep.Get_Fields_Int32("ExemptValue")));
						HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
					}
				}
				else if (vbPorterVar == 41)
				{
					// category description
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strExtraParameters)));
						clsTemp.OpenRecordset("select * from RATIOTRENDS where type = " + FCConvert.ToString(lngTemp), "twpp0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							HandleSpecialCase = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
						}
					}
				}
				else if (vbPorterVar == 42)
				{
					// Category amount
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strExtraParameters)));
						if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) == "PP")
						{
							// just pp
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							clsTemp.OpenRecordset("select * from ppvaluations where valuekey = " + clsRep.Get_Fields("account"), "twpp0000.vb1");
						}
						else
						{
							// combined
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							clsTemp.OpenRecordset("select * from ppvaluations where valuekey = " + FCConvert.ToString(Conversion.Val(clsRep.Get_Fields("ppac"))), "twpp0000.vb1");
						}
						if (!clsTemp.EndOfFile())
						{
							if (lngTemp > 0 && lngTemp < 10)
							{
								// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
								lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("category" + FCConvert.ToString(lngTemp)))));
								HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
							}
						}
					}
				}
				else if (vbPorterVar == 43)
				{
					// open 1 desc
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						clsTemp.OpenRecordset("select * from ppratioopens", "twpp0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							HandleSpecialCase = FCConvert.ToString(clsTemp.Get_Fields_String("OpenField1"));
						}
					}
				}
				else if (vbPorterVar == 45)
				{
					// open 2 desc
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						clsTemp.OpenRecordset("select * from ppratioopens", "twpp0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							HandleSpecialCase = FCConvert.ToString(clsTemp.Get_Fields_String("OpenField2"));
						}
					}
				}
				else if (vbPorterVar == 47)
				{
					// category range amount
					int intStart = 0;
					int intEnd = 0;
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
						intStart = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
						intEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
						if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) == "PP")
						{
							// just pp
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							clsTemp.OpenRecordset("select * from ppvaluations where valuekey = " + clsRep.Get_Fields("account"), "twpp0000.vb1");
						}
						else
						{
							// combined
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							clsTemp.OpenRecordset("select * from ppvaluations where valuekey = " + FCConvert.ToString(Conversion.Val(clsRep.Get_Fields("ppac"))), "twpp0000.vb1");
						}
						if (!clsTemp.EndOfFile())
						{
							lngTemp = 0;
							for (x = intStart; x <= intEnd; x++)
							{
								// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
								lngTemp += FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("category" + FCConvert.ToString(x))));
							}
							// x
							HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
						}
					}
				}
				else if (vbPorterVar == 48)
				{
					// Assessment (land + building)
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "PP")
					{
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("landvalue")) + Conversion.Val(clsRep.Get_Fields_Int32("buildingvalue")));
						HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
					}
				}
				else if (vbPorterVar == 52)
				{
					// Tree Growth Acres
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "PP")
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("TGSoftAcres")) + Conversion.Val(clsRep.Get_Fields_Double("TGHardAcres")) + Conversion.Val(clsRep.Get_Fields_Double("TGMixedAcres"));
						HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
					}
				}
				else if (vbPorterVar == 53)
				{
					// Other Acres
					if (Strings.UCase(clsRep.Get_Fields_String("Billingtype")) != "PP")
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("Acres")) - (Conversion.Val(clsRep.Get_Fields_Double("TGSoftAcres")) + Conversion.Val(clsRep.Get_Fields_Double("TGHardAcres")) + Conversion.Val(clsRep.Get_Fields_Double("TGMixedAcres")));
						HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
					}
				}
				else if (vbPorterVar == 57)
				{
					// Tree Growth Value
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "PP")
					{
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("TGSoftvalue")) + Conversion.Val(clsRep.Get_Fields_Int32("TGHardValue")) + Conversion.Val(clsRep.Get_Fields_Int32("TGMixedValue")));
						HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,###,##0");
					}
				}
				else if (vbPorterVar == 58)
				{
					// Other Value
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "PP")
					{
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("LandValue")) - (Conversion.Val(clsRep.Get_Fields_Int32("TGSoftvalue")) + Conversion.Val(clsRep.Get_Fields_Int32("TGHardValue")) + Conversion.Val(clsRep.Get_Fields_Int32("TGMixedValue"))));
						HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
					}
				}
				else if (vbPorterVar == 59)
				{
					// RE + PP Assessment
					if (Strings.UCase(clsRep.Get_Fields_String("BillingType")) != "PP")
					{
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("LandValue")) + Conversion.Val(clsRep.Get_Fields_Int32("BuildingValue")));
					}
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("ppassessment")));
					}
					HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
				}
				else if (vbPorterVar == 60)
				{
					// RE + PP Taxable Value
					if (Strings.UCase(clsRep.Get_Fields_String("BillingType")) != "PP")
					{
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("LandValue")) + Conversion.Val(clsRep.Get_Fields_Int32("BuildingValue")));
					}
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("ppassessment")));
					}
					lngTemp -= FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("exemptvalue")));
					HandleSpecialCase = Strings.Format(lngTemp, "#,###,###,##0");
				}
				else if (vbPorterVar == 61)
				{
					// past due
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "PP")
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						dblTemp = modREPPBudStuff.CalculateAccountTotalDue_6(clsRep.Get_Fields("account"), true, clsRep.Get_Fields_Int32("ID"));
					}
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "RE")
					{
						if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) == "PP")
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							dblTemp += modREPPBudStuff.CalculateAccountTotalDue_6(clsRep.Get_Fields("account"), false, clsRep.Get_Fields_Int32("ID"));
						}
						else
						{
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [ppbillkey] not found!! (maybe it is an alias?)
							dblTemp += modREPPBudStuff.CalculateAccountTotalDue_26(FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields("ppac"))), false, FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields("ppbillkey"))));
						}
					}
					HandleSpecialCase = Strings.Format(dblTemp, "#,###,###,##0.00");
				}
				else if (vbPorterVar == 62)
				{
					// Account Due
					if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) != "PP")
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						dblTemp = modREPPBudStuff.CalculateAccountTotalDue_6(clsRep.Get_Fields("account"), true);
					}
					if (Strings.UCase(clsRep.Get_Fields_String("Billingtype")) != "RE")
					{
						if (Strings.UCase(clsRep.Get_Fields_String("billingtype")) == "PP")
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							dblTemp += modREPPBudStuff.CalculateAccountTotalDue_6(clsRep.Get_Fields("account"), false);
						}
						else
						{
							// TODO Get_Fields: Field [ppac] not found!! (maybe it is an alias?)
							dblTemp += modREPPBudStuff.CalculateAccountTotalDue_8(FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields("ppac"))), false);
						}
					}
					HandleSpecialCase = Strings.Format(dblTemp, "#,###,###,##0.00");
				}
				return HandleSpecialCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleSpecialCase with code " + FCConvert.ToString(clsCCode.FieldID), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleSpecialCase;
		}

		public class StaticVariables
		{
			public bool boolIsRegional;
			public AddrType[] aryReturnAddress = null;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
