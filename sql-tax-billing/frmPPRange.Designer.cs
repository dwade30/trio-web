﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPPRange.
	/// </summary>
	partial class frmPPRange : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbAccounts;
		public fecherFoundation.FCLabel lblAccounts;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPRange));
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbAccounts = new fecherFoundation.FCComboBox();
			this.lblAccounts = new fecherFoundation.FCLabel();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 313);
			this.BottomPanel.Size = new System.Drawing.Size(522, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtEnd);
			this.ClientArea.Controls.Add(this.txtStart);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.cmbAccounts);
			this.ClientArea.Controls.Add(this.lblAccounts);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(522, 253);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(522, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(317, 30);
			this.HeaderText.Text = "Personal Property Accounts";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"Accounts",
				"Names",
				"Locations",
				"Open1",
				"Open2"
			});
			this.cmbRange.Location = new System.Drawing.Point(164, 90);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(183, 40);
			this.cmbRange.TabIndex = 3;
			this.cmbRange.Text = "Accounts";
			this.cmbRange.Visible = false;
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(30, 104);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 2;
			this.lblRange.Text = "RANGE";
			this.lblRange.Visible = false;
			// 
			// cmbAccounts
			// 
			this.cmbAccounts.AutoSize = false;
			this.cmbAccounts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAccounts.FormattingEnabled = true;
			this.cmbAccounts.Items.AddRange(new object[] {
				"All Accounts",
				"Range"
			});
			this.cmbAccounts.Location = new System.Drawing.Point(164, 30);
			this.cmbAccounts.Name = "cmbAccounts";
			this.cmbAccounts.Size = new System.Drawing.Size(183, 40);
			this.cmbAccounts.TabIndex = 1;
			this.cmbAccounts.Text = "All Accounts";
			this.cmbAccounts.SelectedIndexChanged += new System.EventHandler(this.cmbAccounts_SelectedIndexChanged);
			// 
			// lblAccounts
			// 
			this.lblAccounts.AutoSize = true;
			this.lblAccounts.Location = new System.Drawing.Point(30, 44);
			this.lblAccounts.Name = "lblAccounts";
			this.lblAccounts.Size = new System.Drawing.Size(77, 15);
			this.lblAccounts.TabIndex = 0;
			this.lblAccounts.Text = "ACCOUNTS";
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(333, 150);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(149, 40);
			this.txtEnd.TabIndex = 6;
			this.txtEnd.Visible = false;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(30, 150);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(149, 40);
			this.txtStart.TabIndex = 4;
			this.txtStart.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(241, 164);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(29, 23);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.Label1.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuCancel
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue ";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuCancel
			// 
			this.mnuCancel.Index = 2;
			this.mnuCancel.Name = "mnuCancel";
			this.mnuCancel.Text = "Cancel";
			this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(176, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(118, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmPPRange
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(522, 421);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPRange";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Personal Property Accounts";
			this.Load += new System.EventHandler(this.frmPPRange_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPRange_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
