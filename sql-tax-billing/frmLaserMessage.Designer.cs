﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmLaserMessage.
	/// </summary>
	partial class frmLaserMessage : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtRemittance;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMessage;
		public fecherFoundation.FCTextBox txtRemittance_6;
		public fecherFoundation.FCTextBox txtRemittance_5;
		public fecherFoundation.FCTextBox txtRemittance_4;
		public fecherFoundation.FCTextBox txtRemittance_3;
		public fecherFoundation.FCTextBox txtRemittance_2;
		public fecherFoundation.FCTextBox txtRemittance_1;
		public fecherFoundation.FCTextBox txtRemittance_0;
		public fecherFoundation.FCTextBox txtMessage_11;
		public fecherFoundation.FCTextBox txtMessage_10;
		public fecherFoundation.FCTextBox txtMessage_9;
		public fecherFoundation.FCTextBox txtMessage_8;
		public fecherFoundation.FCTextBox txtMessage_7;
		public fecherFoundation.FCTextBox txtMessage_6;
		public fecherFoundation.FCTextBox txtMessage_5;
		public fecherFoundation.FCTextBox txtMessage_4;
		public fecherFoundation.FCTextBox txtMessage_3;
		public fecherFoundation.FCTextBox txtMessage_2;
		public fecherFoundation.FCTextBox txtMessage_1;
		public fecherFoundation.FCTextBox txtMessage_0;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLaserMessage));
			this.txtRemittance_6 = new fecherFoundation.FCTextBox();
			this.txtRemittance_5 = new fecherFoundation.FCTextBox();
			this.txtRemittance_4 = new fecherFoundation.FCTextBox();
			this.txtRemittance_3 = new fecherFoundation.FCTextBox();
			this.txtRemittance_2 = new fecherFoundation.FCTextBox();
			this.txtRemittance_1 = new fecherFoundation.FCTextBox();
			this.txtRemittance_0 = new fecherFoundation.FCTextBox();
			this.txtMessage_11 = new fecherFoundation.FCTextBox();
			this.txtMessage_10 = new fecherFoundation.FCTextBox();
			this.txtMessage_9 = new fecherFoundation.FCTextBox();
			this.txtMessage_8 = new fecherFoundation.FCTextBox();
			this.txtMessage_7 = new fecherFoundation.FCTextBox();
			this.txtMessage_6 = new fecherFoundation.FCTextBox();
			this.txtMessage_5 = new fecherFoundation.FCTextBox();
			this.txtMessage_4 = new fecherFoundation.FCTextBox();
			this.txtMessage_3 = new fecherFoundation.FCTextBox();
			this.txtMessage_2 = new fecherFoundation.FCTextBox();
			this.txtMessage_1 = new fecherFoundation.FCTextBox();
			this.txtMessage_0 = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveAndContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAndContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 670);
			this.BottomPanel.Size = new System.Drawing.Size(1240, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtRemittance_6);
			this.ClientArea.Controls.Add(this.txtRemittance_5);
			this.ClientArea.Controls.Add(this.txtRemittance_4);
			this.ClientArea.Controls.Add(this.txtRemittance_3);
			this.ClientArea.Controls.Add(this.txtRemittance_2);
			this.ClientArea.Controls.Add(this.txtRemittance_1);
			this.ClientArea.Controls.Add(this.txtRemittance_0);
			this.ClientArea.Controls.Add(this.txtMessage_11);
			this.ClientArea.Controls.Add(this.txtMessage_10);
			this.ClientArea.Controls.Add(this.txtMessage_9);
			this.ClientArea.Controls.Add(this.txtMessage_8);
			this.ClientArea.Controls.Add(this.txtMessage_7);
			this.ClientArea.Controls.Add(this.txtMessage_6);
			this.ClientArea.Controls.Add(this.txtMessage_5);
			this.ClientArea.Controls.Add(this.txtMessage_4);
			this.ClientArea.Controls.Add(this.txtMessage_3);
			this.ClientArea.Controls.Add(this.txtMessage_2);
			this.ClientArea.Controls.Add(this.txtMessage_1);
			this.ClientArea.Controls.Add(this.txtMessage_0);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1240, 610);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1240, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(434, 30);
			this.HeaderText.Text = "Information and Remittance Messages";
			// 
			// txtRemittance_6
			// 
			this.txtRemittance_6.AutoSize = false;
			this.txtRemittance_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtRemittance_6.LinkItem = null;
			this.txtRemittance_6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRemittance_6.LinkTopic = null;
			this.txtRemittance_6.Location = new System.Drawing.Point(784, 330);
			this.txtRemittance_6.MaxLength = 45;
			this.txtRemittance_6.Name = "txtRemittance_6";
			this.txtRemittance_6.Size = new System.Drawing.Size(433, 40);
			this.txtRemittance_6.TabIndex = 20;
			this.txtRemittance_6.KeyDown += new Wisej.Web.KeyEventHandler(this.txtRemittance_KeyDown);
			// 
			// txtRemittance_5
			// 
			this.txtRemittance_5.AutoSize = false;
			this.txtRemittance_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtRemittance_5.LinkItem = null;
			this.txtRemittance_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRemittance_5.LinkTopic = null;
			this.txtRemittance_5.Location = new System.Drawing.Point(784, 285);
			this.txtRemittance_5.MaxLength = 45;
			this.txtRemittance_5.Name = "txtRemittance_5";
			this.txtRemittance_5.Size = new System.Drawing.Size(433, 40);
			this.txtRemittance_5.TabIndex = 19;
			this.txtRemittance_5.KeyDown += new Wisej.Web.KeyEventHandler(this.txtRemittance_KeyDown);
			// 
			// txtRemittance_4
			// 
			this.txtRemittance_4.AutoSize = false;
			this.txtRemittance_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtRemittance_4.LinkItem = null;
			this.txtRemittance_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRemittance_4.LinkTopic = null;
			this.txtRemittance_4.Location = new System.Drawing.Point(784, 240);
			this.txtRemittance_4.MaxLength = 45;
			this.txtRemittance_4.Name = "txtRemittance_4";
			this.txtRemittance_4.Size = new System.Drawing.Size(433, 40);
			this.txtRemittance_4.TabIndex = 18;
			this.txtRemittance_4.KeyDown += new Wisej.Web.KeyEventHandler(this.txtRemittance_KeyDown);
			// 
			// txtRemittance_3
			// 
			this.txtRemittance_3.AutoSize = false;
			this.txtRemittance_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtRemittance_3.LinkItem = null;
			this.txtRemittance_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRemittance_3.LinkTopic = null;
			this.txtRemittance_3.Location = new System.Drawing.Point(784, 195);
			this.txtRemittance_3.MaxLength = 45;
			this.txtRemittance_3.Name = "txtRemittance_3";
			this.txtRemittance_3.Size = new System.Drawing.Size(433, 40);
			this.txtRemittance_3.TabIndex = 17;
			this.txtRemittance_3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtRemittance_KeyDown);
			// 
			// txtRemittance_2
			// 
			this.txtRemittance_2.AutoSize = false;
			this.txtRemittance_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtRemittance_2.LinkItem = null;
			this.txtRemittance_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRemittance_2.LinkTopic = null;
			this.txtRemittance_2.Location = new System.Drawing.Point(784, 150);
			this.txtRemittance_2.MaxLength = 45;
			this.txtRemittance_2.Name = "txtRemittance_2";
			this.txtRemittance_2.Size = new System.Drawing.Size(433, 40);
			this.txtRemittance_2.TabIndex = 16;
			this.txtRemittance_2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtRemittance_KeyDown);
			// 
			// txtRemittance_1
			// 
			this.txtRemittance_1.AutoSize = false;
			this.txtRemittance_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtRemittance_1.LinkItem = null;
			this.txtRemittance_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRemittance_1.LinkTopic = null;
			this.txtRemittance_1.Location = new System.Drawing.Point(784, 105);
			this.txtRemittance_1.MaxLength = 45;
			this.txtRemittance_1.Name = "txtRemittance_1";
			this.txtRemittance_1.Size = new System.Drawing.Size(433, 40);
			this.txtRemittance_1.TabIndex = 15;
			this.txtRemittance_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtRemittance_KeyDown);
			// 
			// txtRemittance_0
			// 
			this.txtRemittance_0.AutoSize = false;
			this.txtRemittance_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtRemittance_0.LinkItem = null;
			this.txtRemittance_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRemittance_0.LinkTopic = null;
			this.txtRemittance_0.Location = new System.Drawing.Point(784, 60);
			this.txtRemittance_0.MaxLength = 45;
			this.txtRemittance_0.Name = "txtRemittance_0";
			this.txtRemittance_0.Size = new System.Drawing.Size(433, 40);
			this.txtRemittance_0.TabIndex = 14;
			this.txtRemittance_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtRemittance_KeyDown);
			// 
			// txtMessage_11
			// 
			this.txtMessage_11.AutoSize = false;
			this.txtMessage_11.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_11.LinkItem = null;
			this.txtMessage_11.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_11.LinkTopic = null;
			this.txtMessage_11.Location = new System.Drawing.Point(30, 555);
			this.txtMessage_11.MaxLength = 88;
			this.txtMessage_11.Name = "txtMessage_11";
			this.txtMessage_11.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_11.TabIndex = 12;
			this.txtMessage_11.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_10
			// 
			this.txtMessage_10.AutoSize = false;
			this.txtMessage_10.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_10.LinkItem = null;
			this.txtMessage_10.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_10.LinkTopic = null;
			this.txtMessage_10.Location = new System.Drawing.Point(30, 510);
			this.txtMessage_10.MaxLength = 88;
			this.txtMessage_10.Name = "txtMessage_10";
			this.txtMessage_10.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_10.TabIndex = 11;
			this.txtMessage_10.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_9
			// 
			this.txtMessage_9.AutoSize = false;
			this.txtMessage_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_9.LinkItem = null;
			this.txtMessage_9.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_9.LinkTopic = null;
			this.txtMessage_9.Location = new System.Drawing.Point(30, 465);
			this.txtMessage_9.MaxLength = 88;
			this.txtMessage_9.Name = "txtMessage_9";
			this.txtMessage_9.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_9.TabIndex = 10;
			this.txtMessage_9.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_8
			// 
			this.txtMessage_8.AutoSize = false;
			this.txtMessage_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_8.LinkItem = null;
			this.txtMessage_8.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_8.LinkTopic = null;
			this.txtMessage_8.Location = new System.Drawing.Point(30, 420);
			this.txtMessage_8.MaxLength = 88;
			this.txtMessage_8.Name = "txtMessage_8";
			this.txtMessage_8.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_8.TabIndex = 9;
			this.txtMessage_8.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_7
			// 
			this.txtMessage_7.AutoSize = false;
			this.txtMessage_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_7.LinkItem = null;
			this.txtMessage_7.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_7.LinkTopic = null;
			this.txtMessage_7.Location = new System.Drawing.Point(30, 375);
			this.txtMessage_7.MaxLength = 88;
			this.txtMessage_7.Name = "txtMessage_7";
			this.txtMessage_7.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_7.TabIndex = 8;
			this.txtMessage_7.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_6
			// 
			this.txtMessage_6.AutoSize = false;
			this.txtMessage_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_6.LinkItem = null;
			this.txtMessage_6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_6.LinkTopic = null;
			this.txtMessage_6.Location = new System.Drawing.Point(30, 330);
			this.txtMessage_6.MaxLength = 88;
			this.txtMessage_6.Name = "txtMessage_6";
			this.txtMessage_6.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_6.TabIndex = 7;
			this.txtMessage_6.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_5
			// 
			this.txtMessage_5.AutoSize = false;
			this.txtMessage_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_5.LinkItem = null;
			this.txtMessage_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_5.LinkTopic = null;
			this.txtMessage_5.Location = new System.Drawing.Point(30, 285);
			this.txtMessage_5.MaxLength = 88;
			this.txtMessage_5.Name = "txtMessage_5";
			this.txtMessage_5.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_5.TabIndex = 6;
			this.txtMessage_5.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_4
			// 
			this.txtMessage_4.AutoSize = false;
			this.txtMessage_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_4.LinkItem = null;
			this.txtMessage_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_4.LinkTopic = null;
			this.txtMessage_4.Location = new System.Drawing.Point(30, 240);
			this.txtMessage_4.MaxLength = 88;
			this.txtMessage_4.Name = "txtMessage_4";
			this.txtMessage_4.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_4.TabIndex = 5;
			this.txtMessage_4.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_3
			// 
			this.txtMessage_3.AutoSize = false;
			this.txtMessage_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_3.LinkItem = null;
			this.txtMessage_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_3.LinkTopic = null;
			this.txtMessage_3.Location = new System.Drawing.Point(30, 195);
			this.txtMessage_3.MaxLength = 88;
			this.txtMessage_3.Name = "txtMessage_3";
			this.txtMessage_3.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_3.TabIndex = 4;
			this.txtMessage_3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_2
			// 
			this.txtMessage_2.AutoSize = false;
			this.txtMessage_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_2.LinkItem = null;
			this.txtMessage_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_2.LinkTopic = null;
			this.txtMessage_2.Location = new System.Drawing.Point(30, 150);
			this.txtMessage_2.MaxLength = 88;
			this.txtMessage_2.Name = "txtMessage_2";
			this.txtMessage_2.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_2.TabIndex = 3;
			this.txtMessage_2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_1
			// 
			this.txtMessage_1.AutoSize = false;
			this.txtMessage_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_1.LinkItem = null;
			this.txtMessage_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_1.LinkTopic = null;
			this.txtMessage_1.Location = new System.Drawing.Point(30, 105);
			this.txtMessage_1.MaxLength = 88;
			this.txtMessage_1.Name = "txtMessage_1";
			this.txtMessage_1.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_1.TabIndex = 2;
			this.txtMessage_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_0
			// 
			this.txtMessage_0.AutoSize = false;
			this.txtMessage_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_0.LinkItem = null;
			this.txtMessage_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage_0.LinkTopic = null;
			this.txtMessage_0.Location = new System.Drawing.Point(30, 60);
			this.txtMessage_0.MaxLength = 88;
			this.txtMessage_0.Name = "txtMessage_0";
			this.txtMessage_0.Size = new System.Drawing.Size(715, 40);
			this.txtMessage_0.TabIndex = 1;
			this.txtMessage_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(784, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(308, 20);
			this.Label2.TabIndex = 13;
			this.Label2.Text = "REMITTANCE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(147, 20);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "INFORMATION";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Save && Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveAndContinue
			// 
			this.cmdSaveAndContinue.AppearanceKey = "acceptButton";
			this.cmdSaveAndContinue.Location = new System.Drawing.Point(538, 30);
			this.cmdSaveAndContinue.Name = "cmdSaveAndContinue";
			this.cmdSaveAndContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAndContinue.Size = new System.Drawing.Size(180, 48);
			this.cmdSaveAndContinue.TabIndex = 0;
			this.cmdSaveAndContinue.Text = "Save & Continue";
			this.cmdSaveAndContinue.Click += new System.EventHandler(this.cmdSaveAndContinue_Click);
			// 
			// frmLaserMessage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1240, 778);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmLaserMessage";
			this.Text = "Information and Remittance messages";
			this.Load += new System.EventHandler(this.frmLaserMessage_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLaserMessage_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndContinue;
	}
}
