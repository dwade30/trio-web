﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPickRate.
	/// </summary>
	partial class frmPickRate : BaseForm
	{
		public FCGrid Grid;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuShow;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPickRate));
            this.Grid = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuShow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdShow = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdShow)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdShow);
            this.BottomPanel.Location = new System.Drawing.Point(0, 606);
            this.BottomPanel.Size = new System.Drawing.Size(880, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(900, 639);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(900, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(349, 30);
            this.HeaderText.Text = "Real Estate Commitment Book";
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 10;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(30, 90);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.Size = new System.Drawing.Size(841, 516);
            this.Grid.TabIndex = 1;
            this.Grid.Click += new System.EventHandler(this.Grid_ClickEvent);
            this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(841, 46);
            this.Label1.Text = "HIGHLIGHT A ROW TO CHOOSE THE RATE RECORD TO PRINT THE COMMITMENT BOOK FOR.  DOUB" +
    "LE-CLICK THAT ROW OR USE THE DROP DOWN MENU TO SHOW THE COMMITMENT BOOK";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuShow,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuShow
            // 
            this.mnuShow.Index = 0;
            this.mnuShow.Name = "mnuShow";
            this.mnuShow.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuShow.Text = "Show commitment book";
            this.mnuShow.Click += new System.EventHandler(this.mnuShow_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdShow
            // 
            this.cmdShow.AppearanceKey = "acceptButton";
            this.cmdShow.Location = new System.Drawing.Point(344, 30);
            this.cmdShow.Name = "cmdShow";
            this.cmdShow.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdShow.Size = new System.Drawing.Size(246, 48);
            this.cmdShow.Text = "Show commitment book";
            this.cmdShow.Click += new System.EventHandler(this.mnuShow_Click);
            // 
            // frmPickRate
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(900, 699);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPickRate";
            this.Text = "Real Estate Commitment Book";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPickRate_Load);
            this.Resize += new System.EventHandler(this.frmPickRate_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPickRate_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdShow)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdShow;
	}
}
