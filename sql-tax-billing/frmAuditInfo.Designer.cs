﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmAuditInfo.
	/// </summary>
	partial class frmAuditInfo : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCFrame> frPeriod;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtWeight;
		public System.Collections.Generic.List<T2KDateBox> t2kInterestDate;
		public System.Collections.Generic.List<T2KDateBox> t2kDueDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label5;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblWeight;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblIDate;
		public Global.T2KDateBox t2kBilldate;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCTextBox txtInterestRate;
		public fecherFoundation.FCFrame frPeriod_3;
		public fecherFoundation.FCTextBox txtWeight_3;
		public Global.T2KDateBox t2kInterestDate_3;
		public Global.T2KDateBox t2kDueDate_3;
		public fecherFoundation.FCLabel Label5_3;
		public fecherFoundation.FCLabel lblWeight_3;
		public fecherFoundation.FCLabel lblDDate_3;
		public fecherFoundation.FCLabel lblIDate_3;
		public fecherFoundation.FCFrame frPeriod_2;
		public fecherFoundation.FCTextBox txtWeight_2;
		public Global.T2KDateBox t2kInterestDate_2;
		public Global.T2KDateBox t2kDueDate_2;
		public fecherFoundation.FCLabel Label5_2;
		public fecherFoundation.FCLabel lblWeight_2;
		public fecherFoundation.FCLabel lblDDate_2;
		public fecherFoundation.FCLabel lblIDate_2;
		public fecherFoundation.FCFrame frPeriod_1;
		public fecherFoundation.FCTextBox txtWeight_1;
		public Global.T2KDateBox t2kInterestDate_1;
		public Global.T2KDateBox t2kDueDate_1;
		public fecherFoundation.FCLabel Label5_1;
		public fecherFoundation.FCLabel lblWeight_1;
		public fecherFoundation.FCLabel lblDDate_1;
		public fecherFoundation.FCLabel lblIDate_1;
		public fecherFoundation.FCFrame frPeriod_0;
		public fecherFoundation.FCTextBox txtWeight_0;
		public Global.T2KDateBox t2kInterestDate_0;
		public Global.T2KDateBox t2kDueDate_0;
		public fecherFoundation.FCLabel Label5_0;
		public fecherFoundation.FCLabel lblWeight_0;
		public fecherFoundation.FCLabel lblDDate_0;
		public fecherFoundation.FCLabel lblIDate_0;
		public fecherFoundation.FCTextBox txtTaxYear;
		public fecherFoundation.FCTextBox txtTaxRate;
		public fecherFoundation.FCComboBox cmbPeriods;
		public Global.T2KDateBox t2kCommitment;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuBack;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAuditInfo));
            this.t2kBilldate = new Global.T2KDateBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.txtInterestRate = new fecherFoundation.FCTextBox();
            this.frPeriod_3 = new fecherFoundation.FCFrame();
            this.txtWeight_3 = new fecherFoundation.FCTextBox();
            this.t2kInterestDate_3 = new Global.T2KDateBox();
            this.t2kDueDate_3 = new Global.T2KDateBox();
            this.Label5_3 = new fecherFoundation.FCLabel();
            this.lblWeight_3 = new fecherFoundation.FCLabel();
            this.lblDDate_3 = new fecherFoundation.FCLabel();
            this.lblIDate_3 = new fecherFoundation.FCLabel();
            this.frPeriod_2 = new fecherFoundation.FCFrame();
            this.txtWeight_2 = new fecherFoundation.FCTextBox();
            this.t2kInterestDate_2 = new Global.T2KDateBox();
            this.t2kDueDate_2 = new Global.T2KDateBox();
            this.Label5_2 = new fecherFoundation.FCLabel();
            this.lblWeight_2 = new fecherFoundation.FCLabel();
            this.lblDDate_2 = new fecherFoundation.FCLabel();
            this.lblIDate_2 = new fecherFoundation.FCLabel();
            this.frPeriod_1 = new fecherFoundation.FCFrame();
            this.txtWeight_1 = new fecherFoundation.FCTextBox();
            this.t2kInterestDate_1 = new Global.T2KDateBox();
            this.t2kDueDate_1 = new Global.T2KDateBox();
            this.Label5_1 = new fecherFoundation.FCLabel();
            this.lblWeight_1 = new fecherFoundation.FCLabel();
            this.lblDDate_1 = new fecherFoundation.FCLabel();
            this.lblIDate_1 = new fecherFoundation.FCLabel();
            this.frPeriod_0 = new fecherFoundation.FCFrame();
            this.txtWeight_0 = new fecherFoundation.FCTextBox();
            this.t2kInterestDate_0 = new Global.T2KDateBox();
            this.t2kDueDate_0 = new Global.T2KDateBox();
            this.Label5_0 = new fecherFoundation.FCLabel();
            this.lblWeight_0 = new fecherFoundation.FCLabel();
            this.lblDDate_0 = new fecherFoundation.FCLabel();
            this.lblIDate_0 = new fecherFoundation.FCLabel();
            this.txtTaxYear = new fecherFoundation.FCTextBox();
            this.txtTaxRate = new fecherFoundation.FCTextBox();
            this.cmbPeriods = new fecherFoundation.FCComboBox();
            this.t2kCommitment = new Global.T2KDateBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label24 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuBack = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSaveAndContinue = new fecherFoundation.FCButton();
            this.cmdBack = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kBilldate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPeriod_3)).BeginInit();
            this.frPeriod_3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPeriod_2)).BeginInit();
            this.frPeriod_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPeriod_1)).BeginInit();
            this.frPeriod_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPeriod_0)).BeginInit();
            this.frPeriod_0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kCommitment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBack)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveAndContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 540);
            this.BottomPanel.Size = new System.Drawing.Size(976, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.t2kBilldate);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.txtInterestRate);
            this.ClientArea.Controls.Add(this.frPeriod_3);
            this.ClientArea.Controls.Add(this.frPeriod_2);
            this.ClientArea.Controls.Add(this.frPeriod_1);
            this.ClientArea.Controls.Add(this.frPeriod_0);
            this.ClientArea.Controls.Add(this.txtTaxYear);
            this.ClientArea.Controls.Add(this.txtTaxRate);
            this.ClientArea.Controls.Add(this.cmbPeriods);
            this.ClientArea.Controls.Add(this.t2kCommitment);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label24);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(996, 651);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label7, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label24, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.t2kCommitment, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPeriods, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTaxRate, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTaxYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.frPeriod_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.frPeriod_1, 0);
            this.ClientArea.Controls.SetChildIndex(this.frPeriod_2, 0);
            this.ClientArea.Controls.SetChildIndex(this.frPeriod_3, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtInterestRate, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.t2kBilldate, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdBack);
            this.TopPanel.Size = new System.Drawing.Size(996, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdBack, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(320, 30);
            this.HeaderText.Text = "Rate and Period Information";
            // 
            // t2kBilldate
            // 
            this.t2kBilldate.Location = new System.Drawing.Point(132, 130);
            this.t2kBilldate.Mask = "##/##/####";
            this.t2kBilldate.MaxLength = 10;
            this.t2kBilldate.Name = "t2kBilldate";
            this.t2kBilldate.Size = new System.Drawing.Size(115, 22);
            this.t2kBilldate.TabIndex = 9;
            this.t2kBilldate.TextChanged += new System.EventHandler(this.t2kBilldate_Change);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(487, 30);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(441, 40);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // txtInterestRate
            // 
            this.txtInterestRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtInterestRate.Location = new System.Drawing.Point(487, 80);
            this.txtInterestRate.Name = "txtInterestRate";
            this.txtInterestRate.Size = new System.Drawing.Size(113, 40);
            this.txtInterestRate.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtInterestRate, "Enter the Interest Rate as a percentage.  Ex: 10% as 10 not .10");
            this.txtInterestRate.TextChanged += new System.EventHandler(this.txtInterestRate_TextChanged);
            // 
            // frPeriod_3
            // 
            this.frPeriod_3.Controls.Add(this.txtWeight_3);
            this.frPeriod_3.Controls.Add(this.t2kInterestDate_3);
            this.frPeriod_3.Controls.Add(this.t2kDueDate_3);
            this.frPeriod_3.Controls.Add(this.Label5_3);
            this.frPeriod_3.Controls.Add(this.lblWeight_3);
            this.frPeriod_3.Controls.Add(this.lblDDate_3);
            this.frPeriod_3.Controls.Add(this.lblIDate_3);
            this.frPeriod_3.Enabled = false;
            this.frPeriod_3.Location = new System.Drawing.Point(357, 369);
            this.frPeriod_3.Name = "frPeriod_3";
            this.frPeriod_3.Size = new System.Drawing.Size(295, 171);
            this.frPeriod_3.TabIndex = 17;
            this.frPeriod_3.Text = "Period 4";
            // 
            // txtWeight_3
            // 
            this.txtWeight_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtWeight_3.Location = new System.Drawing.Point(154, 120);
            this.txtWeight_3.Name = "txtWeight_3";
            this.txtWeight_3.Size = new System.Drawing.Size(65, 40);
            this.txtWeight_3.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtWeight_3, "Percentage (weight) of bill this installment represents");
            // 
            // t2kInterestDate_3
            // 
            this.t2kInterestDate_3.Location = new System.Drawing.Point(154, 20);
            this.t2kInterestDate_3.Mask = "##/##/####";
            this.t2kInterestDate_3.MaxLength = 10;
            this.t2kInterestDate_3.Name = "t2kInterestDate_3";
            this.t2kInterestDate_3.Size = new System.Drawing.Size(115, 22);
            this.t2kInterestDate_3.TabIndex = 1;
            this.t2kInterestDate_3.TextChanged += new System.EventHandler(this.t2kInterestDate_Change);
            this.t2kInterestDate_3.Validating += new System.ComponentModel.CancelEventHandler(this.t2kInterestDate_Validate);
            // 
            // t2kDueDate_3
            // 
            this.t2kDueDate_3.Location = new System.Drawing.Point(154, 70);
            this.t2kDueDate_3.Mask = "##/##/####";
            this.t2kDueDate_3.MaxLength = 10;
            this.t2kDueDate_3.Name = "t2kDueDate_3";
            this.t2kDueDate_3.Size = new System.Drawing.Size(115, 22);
            this.t2kDueDate_3.TabIndex = 3;
            this.t2kDueDate_3.TextChanged += new System.EventHandler(this.t2kDueDate_Change);
            this.t2kDueDate_3.Validating += new System.ComponentModel.CancelEventHandler(this.t2kDueDate_Validate);
            // 
            // Label5_3
            // 
            this.Label5_3.Location = new System.Drawing.Point(245, 134);
            this.Label5_3.Name = "Label5_3";
            this.Label5_3.Size = new System.Drawing.Size(26, 20);
            this.Label5_3.TabIndex = 6;
            this.Label5_3.Text = "%";
            // 
            // lblWeight_3
            // 
            this.lblWeight_3.Location = new System.Drawing.Point(30, 134);
            this.lblWeight_3.Name = "lblWeight_3";
            this.lblWeight_3.Size = new System.Drawing.Size(82, 20);
            this.lblWeight_3.TabIndex = 4;
            this.lblWeight_3.Text = "WEIGHT";
            this.ToolTip1.SetToolTip(this.lblWeight_3, "Percentage (weight) of bill this installment represents");
            // 
            // lblDDate_3
            // 
            this.lblDDate_3.Location = new System.Drawing.Point(30, 84);
            this.lblDDate_3.Name = "lblDDate_3";
            this.lblDDate_3.Size = new System.Drawing.Size(62, 15);
            this.lblDDate_3.TabIndex = 2;
            this.lblDDate_3.Text = "DUE DATE";
            // 
            // lblIDate_3
            // 
            this.lblIDate_3.Location = new System.Drawing.Point(30, 34);
            this.lblIDate_3.Name = "lblIDate_3";
            this.lblIDate_3.Size = new System.Drawing.Size(102, 17);
            this.lblIDate_3.Text = "INTEREST DATE";
            // 
            // frPeriod_2
            // 
            this.frPeriod_2.Controls.Add(this.txtWeight_2);
            this.frPeriod_2.Controls.Add(this.t2kInterestDate_2);
            this.frPeriod_2.Controls.Add(this.t2kDueDate_2);
            this.frPeriod_2.Controls.Add(this.Label5_2);
            this.frPeriod_2.Controls.Add(this.lblWeight_2);
            this.frPeriod_2.Controls.Add(this.lblDDate_2);
            this.frPeriod_2.Controls.Add(this.lblIDate_2);
            this.frPeriod_2.Enabled = false;
            this.frPeriod_2.Location = new System.Drawing.Point(30, 369);
            this.frPeriod_2.Name = "frPeriod_2";
            this.frPeriod_2.Size = new System.Drawing.Size(301, 171);
            this.frPeriod_2.TabIndex = 16;
            this.frPeriod_2.Text = "Period 3";
            // 
            // txtWeight_2
            // 
            this.txtWeight_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtWeight_2.Location = new System.Drawing.Point(159, 120);
            this.txtWeight_2.Name = "txtWeight_2";
            this.txtWeight_2.Size = new System.Drawing.Size(65, 40);
            this.txtWeight_2.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtWeight_2, "Percentage (weight) of bill this installment represents");
            // 
            // t2kInterestDate_2
            // 
            this.t2kInterestDate_2.Location = new System.Drawing.Point(159, 20);
            this.t2kInterestDate_2.Mask = "##/##/####";
            this.t2kInterestDate_2.MaxLength = 10;
            this.t2kInterestDate_2.Name = "t2kInterestDate_2";
            this.t2kInterestDate_2.Size = new System.Drawing.Size(115, 22);
            this.t2kInterestDate_2.TabIndex = 1;
            this.t2kInterestDate_2.TextChanged += new System.EventHandler(this.t2kInterestDate_Change);
            this.t2kInterestDate_2.Validating += new System.ComponentModel.CancelEventHandler(this.t2kInterestDate_Validate);
            // 
            // t2kDueDate_2
            // 
            this.t2kDueDate_2.Location = new System.Drawing.Point(159, 70);
            this.t2kDueDate_2.Mask = "##/##/####";
            this.t2kDueDate_2.MaxLength = 10;
            this.t2kDueDate_2.Name = "t2kDueDate_2";
            this.t2kDueDate_2.Size = new System.Drawing.Size(115, 22);
            this.t2kDueDate_2.TabIndex = 3;
            this.t2kDueDate_2.TextChanged += new System.EventHandler(this.t2kDueDate_Change);
            this.t2kDueDate_2.Validating += new System.ComponentModel.CancelEventHandler(this.t2kDueDate_Validate);
            // 
            // Label5_2
            // 
            this.Label5_2.Location = new System.Drawing.Point(245, 134);
            this.Label5_2.Name = "Label5_2";
            this.Label5_2.Size = new System.Drawing.Size(26, 20);
            this.Label5_2.TabIndex = 6;
            this.Label5_2.Text = "%";
            // 
            // lblWeight_2
            // 
            this.lblWeight_2.Location = new System.Drawing.Point(30, 134);
            this.lblWeight_2.Name = "lblWeight_2";
            this.lblWeight_2.Size = new System.Drawing.Size(82, 20);
            this.lblWeight_2.TabIndex = 4;
            this.lblWeight_2.Text = "WEIGHT";
            this.ToolTip1.SetToolTip(this.lblWeight_2, "Percentage (weight) of bill this installment represents");
            // 
            // lblDDate_2
            // 
            this.lblDDate_2.Location = new System.Drawing.Point(30, 84);
            this.lblDDate_2.Name = "lblDDate_2";
            this.lblDDate_2.Size = new System.Drawing.Size(80, 19);
            this.lblDDate_2.TabIndex = 2;
            this.lblDDate_2.Text = "DUE DATE";
            // 
            // lblIDate_2
            // 
            this.lblIDate_2.Location = new System.Drawing.Point(30, 34);
            this.lblIDate_2.Name = "lblIDate_2";
            this.lblIDate_2.Size = new System.Drawing.Size(104, 15);
            this.lblIDate_2.Text = "INTEREST DATE";
            // 
            // frPeriod_1
            // 
            this.frPeriod_1.Controls.Add(this.txtWeight_1);
            this.frPeriod_1.Controls.Add(this.t2kInterestDate_1);
            this.frPeriod_1.Controls.Add(this.t2kDueDate_1);
            this.frPeriod_1.Controls.Add(this.Label5_1);
            this.frPeriod_1.Controls.Add(this.lblWeight_1);
            this.frPeriod_1.Controls.Add(this.lblDDate_1);
            this.frPeriod_1.Controls.Add(this.lblIDate_1);
            this.frPeriod_1.Enabled = false;
            this.frPeriod_1.Location = new System.Drawing.Point(357, 176);
            this.frPeriod_1.Name = "frPeriod_1";
            this.frPeriod_1.Size = new System.Drawing.Size(295, 173);
            this.frPeriod_1.TabIndex = 15;
            this.frPeriod_1.Text = "Period 2";
            // 
            // txtWeight_1
            // 
            this.txtWeight_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtWeight_1.Location = new System.Drawing.Point(154, 120);
            this.txtWeight_1.Name = "txtWeight_1";
            this.txtWeight_1.Size = new System.Drawing.Size(65, 40);
            this.txtWeight_1.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtWeight_1, "Percentage (weight) of bill this installment represents");
            // 
            // t2kInterestDate_1
            // 
            this.t2kInterestDate_1.Location = new System.Drawing.Point(154, 20);
            this.t2kInterestDate_1.Mask = "##/##/####";
            this.t2kInterestDate_1.MaxLength = 10;
            this.t2kInterestDate_1.Name = "t2kInterestDate_1";
            this.t2kInterestDate_1.Size = new System.Drawing.Size(115, 22);
            this.t2kInterestDate_1.TabIndex = 1;
            this.t2kInterestDate_1.TextChanged += new System.EventHandler(this.t2kInterestDate_Change);
            this.t2kInterestDate_1.Validating += new System.ComponentModel.CancelEventHandler(this.t2kInterestDate_Validate);
            // 
            // t2kDueDate_1
            // 
            this.t2kDueDate_1.Location = new System.Drawing.Point(154, 70);
            this.t2kDueDate_1.Mask = "##/##/####";
            this.t2kDueDate_1.MaxLength = 10;
            this.t2kDueDate_1.Name = "t2kDueDate_1";
            this.t2kDueDate_1.Size = new System.Drawing.Size(115, 22);
            this.t2kDueDate_1.TabIndex = 3;
            this.t2kDueDate_1.TextChanged += new System.EventHandler(this.t2kDueDate_Change);
            this.t2kDueDate_1.Validating += new System.ComponentModel.CancelEventHandler(this.t2kDueDate_Validate);
            // 
            // Label5_1
            // 
            this.Label5_1.Location = new System.Drawing.Point(245, 134);
            this.Label5_1.Name = "Label5_1";
            this.Label5_1.Size = new System.Drawing.Size(26, 20);
            this.Label5_1.TabIndex = 6;
            this.Label5_1.Text = "%";
            // 
            // lblWeight_1
            // 
            this.lblWeight_1.Location = new System.Drawing.Point(30, 134);
            this.lblWeight_1.Name = "lblWeight_1";
            this.lblWeight_1.Size = new System.Drawing.Size(82, 20);
            this.lblWeight_1.TabIndex = 4;
            this.lblWeight_1.Text = "WEIGHT";
            this.ToolTip1.SetToolTip(this.lblWeight_1, "Percentage (weight) of bill this installment represents");
            // 
            // lblDDate_1
            // 
            this.lblDDate_1.Location = new System.Drawing.Point(30, 84);
            this.lblDDate_1.Name = "lblDDate_1";
            this.lblDDate_1.Size = new System.Drawing.Size(76, 14);
            this.lblDDate_1.TabIndex = 2;
            this.lblDDate_1.Text = "DUE DATE";
            // 
            // lblIDate_1
            // 
            this.lblIDate_1.Location = new System.Drawing.Point(30, 34);
            this.lblIDate_1.Name = "lblIDate_1";
            this.lblIDate_1.Size = new System.Drawing.Size(102, 23);
            this.lblIDate_1.Text = "INTEREST DATE";
            // 
            // frPeriod_0
            // 
            this.frPeriod_0.Controls.Add(this.txtWeight_0);
            this.frPeriod_0.Controls.Add(this.t2kInterestDate_0);
            this.frPeriod_0.Controls.Add(this.t2kDueDate_0);
            this.frPeriod_0.Controls.Add(this.Label5_0);
            this.frPeriod_0.Controls.Add(this.lblWeight_0);
            this.frPeriod_0.Controls.Add(this.lblDDate_0);
            this.frPeriod_0.Controls.Add(this.lblIDate_0);
            this.frPeriod_0.Location = new System.Drawing.Point(30, 176);
            this.frPeriod_0.Name = "frPeriod_0";
            this.frPeriod_0.Size = new System.Drawing.Size(301, 173);
            this.frPeriod_0.TabIndex = 14;
            this.frPeriod_0.Text = "Period 1";
            // 
            // txtWeight_0
            // 
            this.txtWeight_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtWeight_0.Location = new System.Drawing.Point(159, 120);
            this.txtWeight_0.Name = "txtWeight_0";
            this.txtWeight_0.Size = new System.Drawing.Size(65, 40);
            this.txtWeight_0.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtWeight_0, "Percentage (weight) of bill this installment represents");
            // 
            // t2kInterestDate_0
            // 
            this.t2kInterestDate_0.Location = new System.Drawing.Point(159, 20);
            this.t2kInterestDate_0.Mask = "##/##/####";
            this.t2kInterestDate_0.MaxLength = 10;
            this.t2kInterestDate_0.Name = "t2kInterestDate_0";
            this.t2kInterestDate_0.Size = new System.Drawing.Size(115, 22);
            this.t2kInterestDate_0.TabIndex = 1;
            this.t2kInterestDate_0.TextChanged += new System.EventHandler(this.t2kInterestDate_Change);
            this.t2kInterestDate_0.Validating += new System.ComponentModel.CancelEventHandler(this.t2kInterestDate_Validate);
            // 
            // t2kDueDate_0
            // 
            this.t2kDueDate_0.Location = new System.Drawing.Point(159, 70);
            this.t2kDueDate_0.Mask = "##/##/####";
            this.t2kDueDate_0.MaxLength = 10;
            this.t2kDueDate_0.Name = "t2kDueDate_0";
            this.t2kDueDate_0.Size = new System.Drawing.Size(115, 22);
            this.t2kDueDate_0.TabIndex = 3;
            this.t2kDueDate_0.TextChanged += new System.EventHandler(this.t2kDueDate_Change);
            this.t2kDueDate_0.Validating += new System.ComponentModel.CancelEventHandler(this.t2kDueDate_Validate);
            // 
            // Label5_0
            // 
            this.Label5_0.Location = new System.Drawing.Point(245, 134);
            this.Label5_0.Name = "Label5_0";
            this.Label5_0.Size = new System.Drawing.Size(26, 20);
            this.Label5_0.TabIndex = 6;
            this.Label5_0.Text = "%";
            // 
            // lblWeight_0
            // 
            this.lblWeight_0.Location = new System.Drawing.Point(30, 134);
            this.lblWeight_0.Name = "lblWeight_0";
            this.lblWeight_0.Size = new System.Drawing.Size(82, 20);
            this.lblWeight_0.TabIndex = 4;
            this.lblWeight_0.Text = "WEIGHT";
            this.ToolTip1.SetToolTip(this.lblWeight_0, "Percentage (weight) of bill this installment represents");
            // 
            // lblDDate_0
            // 
            this.lblDDate_0.Location = new System.Drawing.Point(30, 84);
            this.lblDDate_0.Name = "lblDDate_0";
            this.lblDDate_0.Size = new System.Drawing.Size(82, 20);
            this.lblDDate_0.TabIndex = 2;
            this.lblDDate_0.Text = "DUE DATE";
            // 
            // lblIDate_0
            // 
            this.lblIDate_0.Location = new System.Drawing.Point(30, 34);
            this.lblIDate_0.Name = "lblIDate_0";
            this.lblIDate_0.Size = new System.Drawing.Size(118, 17);
            this.lblIDate_0.Text = "INTEREST DATE";
            // 
            // txtTaxYear
            // 
            this.txtTaxYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxYear.Location = new System.Drawing.Point(132, 30);
            this.txtTaxYear.MaxLength = 4;
            this.txtTaxYear.Name = "txtTaxYear";
            this.txtTaxYear.Size = new System.Drawing.Size(113, 40);
            this.txtTaxYear.TabIndex = 1;
            this.txtTaxYear.Enter += new System.EventHandler(this.txtTaxYear_Enter);
            this.txtTaxYear.TextChanged += new System.EventHandler(this.txtTaxYear_TextChanged);
            // 
            // txtTaxRate
            // 
            this.txtTaxRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxRate.Location = new System.Drawing.Point(132, 80);
            this.txtTaxRate.Name = "txtTaxRate";
            this.txtTaxRate.Size = new System.Drawing.Size(113, 40);
            this.txtTaxRate.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtTaxRate, "Enter the Tax Rate as per 1000.  Ex: 16.50 not .0165");
            this.txtTaxRate.TextChanged += new System.EventHandler(this.txtTaxRate_TextChanged);
            // 
            // cmbPeriods
            // 
            this.cmbPeriods.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPeriods.Location = new System.Drawing.Point(838, 130);
            this.cmbPeriods.Name = "cmbPeriods";
            this.cmbPeriods.Size = new System.Drawing.Size(90, 40);
            this.cmbPeriods.Sorted = true;
            this.cmbPeriods.TabIndex = 13;
            this.cmbPeriods.SelectedIndexChanged += new System.EventHandler(this.cmbPeriods_SelectedIndexChanged);
            // 
            // t2kCommitment
            // 
            this.t2kCommitment.Location = new System.Drawing.Point(487, 130);
            this.t2kCommitment.Mask = "##/##/####";
            this.t2kCommitment.MaxLength = 10;
            this.t2kCommitment.Name = "t2kCommitment";
            this.t2kCommitment.Size = new System.Drawing.Size(115, 22);
            this.t2kCommitment.TabIndex = 11;
            this.t2kCommitment.TextChanged += new System.EventHandler(this.t2kCommitment_Change);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(334, 144);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(122, 19);
            this.Label4.TabIndex = 10;
            this.Label4.Text = "COMMITMENT DATE";
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(334, 44);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(141, 19);
            this.Label24.TabIndex = 2;
            this.Label24.Text = "DESCRIPTION / TITLE";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(334, 94);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(104, 19);
            this.Label7.TabIndex = 6;
            this.Label7.Text = "INTEREST RATE";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(72, 22);
            this.Label6.Text = "TAX YEAR";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 144);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(60, 19);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "BILL DATE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(654, 144);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(158, 17);
            this.Label2.TabIndex = 12;
            this.Label2.Text = "NUMBER OF PAY PERIODS";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 94);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(64, 20);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "TAX RATE";
            // 
            // mnuBack
            // 
            this.mnuBack.Index = 0;
            this.mnuBack.Name = "mnuBack";
            this.mnuBack.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuBack.Text = "Select Another Rate";
            this.mnuBack.Click += new System.EventHandler(this.mnuBack_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBack,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuContinue,
            this.mnuSepar,
            this.mnuCancel});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Enabled = false;
            this.mnuSave.Index = 2;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Visible = false;
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuContinue
            // 
            this.mnuContinue.Index = 3;
            this.mnuContinue.Name = "mnuContinue";
            this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuContinue.Text = "Save && Continue";
            this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 4;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuCancel
            // 
            this.mnuCancel.Index = 5;
            this.mnuCancel.Name = "mnuCancel";
            this.mnuCancel.Text = "Exit";
            this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
            // 
            // cmdSaveAndContinue
            // 
            this.cmdSaveAndContinue.AppearanceKey = "acceptButton";
            this.cmdSaveAndContinue.Location = new System.Drawing.Point(160, 24);
            this.cmdSaveAndContinue.Name = "cmdSaveAndContinue";
            this.cmdSaveAndContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveAndContinue.Size = new System.Drawing.Size(190, 48);
            this.cmdSaveAndContinue.TabIndex = 18;
            this.cmdSaveAndContinue.Text = "Save & Continue";
            this.cmdSaveAndContinue.Click += new System.EventHandler(this.cmdSaveAndContinue_Click);
            // 
            // cmdBack
            // 
            this.cmdBack.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdBack.Location = new System.Drawing.Point(813, 29);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdBack.Size = new System.Drawing.Size(143, 24);
            this.cmdBack.TabIndex = 1;
            this.cmdBack.Text = "Select Another Rate";
            this.cmdBack.Click += new System.EventHandler(this.mnuBack_Click);
            // 
            // frmAuditInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(996, 711);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmAuditInfo";
            this.Text = "Rate and Period Information";
            this.Load += new System.EventHandler(this.frmAuditInfo_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAuditInfo_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAuditInfo_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kBilldate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPeriod_3)).EndInit();
            this.frPeriod_3.ResumeLayout(false);
            this.frPeriod_3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPeriod_2)).EndInit();
            this.frPeriod_2.ResumeLayout(false);
            this.frPeriod_2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPeriod_1)).EndInit();
            this.frPeriod_1.ResumeLayout(false);
            this.frPeriod_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPeriod_0)).EndInit();
            this.frPeriod_0.ResumeLayout(false);
            this.frPeriod_0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kCommitment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBack)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndContinue;
		private FCButton cmdBack;
	}
}
