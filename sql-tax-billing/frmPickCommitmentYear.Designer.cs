﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPickCommitmentYear.
	/// </summary>
	partial class frmPickCommitmentYear : BaseForm
	{
		public fecherFoundation.FCComboBox cmbBind;
		public fecherFoundation.FCLabel lblBind;
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCFrame framCommitOptions;
		public fecherFoundation.FCCheckBox chkNewPage;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel lblStartPage;
		public fecherFoundation.FCLabel lblCommitment;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPickCommitmentYear));
			this.cmbBind = new fecherFoundation.FCComboBox();
			this.lblBind = new fecherFoundation.FCLabel();
			this.cmbPrint = new fecherFoundation.FCComboBox();
			this.lblPrint = new fecherFoundation.FCLabel();
			this.framCommitOptions = new fecherFoundation.FCFrame();
			this.chkNewPage = new fecherFoundation.FCCheckBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.lblStartPage = new fecherFoundation.FCLabel();
			this.lblCommitment = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framCommitOptions)).BeginInit();
			this.framCommitOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkNewPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 383);
			this.BottomPanel.Size = new System.Drawing.Size(474, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framCommitOptions);
			this.ClientArea.Controls.Add(this.txtStart);
			this.ClientArea.Controls.Add(this.cmbPrint);
			this.ClientArea.Controls.Add(this.lblPrint);
			this.ClientArea.Controls.Add(this.lblStartPage);
			this.ClientArea.Controls.Add(this.lblCommitment);
			this.ClientArea.Size = new System.Drawing.Size(474, 323);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(474, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(335, 30);
			//FC:FINAL:MSH - issue #1326: remove header text
			//this.HeaderText.Text = "Format for Commitment Book";
			// 
			// cmbBind
			// 
			this.cmbBind.AutoSize = false;
			this.cmbBind.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBind.FormattingEnabled = true;
			this.cmbBind.Items.AddRange(new object[] {
				"Bind on Left",
				"Bind on Top",
				"Don\'t Adjust for Binding"
			});
			this.cmbBind.Location = new System.Drawing.Point(116, 70);
			this.cmbBind.Name = "cmbBind";
			this.cmbBind.Size = new System.Drawing.Size(251, 40);
			this.cmbBind.TabIndex = 2;
			this.cmbBind.Text = "Don\'t Adjust for Binding";
			this.cmbBind.Visible = false;
			// 
			// lblBind
			// 
			this.lblBind.AutoSize = true;
			this.lblBind.Location = new System.Drawing.Point(20, 84);
			this.lblBind.Name = "lblBind";
			this.lblBind.Size = new System.Drawing.Size(37, 15);
			this.lblBind.TabIndex = 1;
			this.lblBind.Text = "BIND";
			this.lblBind.Visible = false;
			// 
			// cmbPrint
			// 
			this.cmbPrint.AutoSize = false;
			this.cmbPrint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPrint.FormattingEnabled = true;
			this.cmbPrint.Items.AddRange(new object[] {
				"8.5 x 11 Portrait",
				"8.5 x 11 Landscape",
				"Wide Printer"
			});
			this.cmbPrint.Location = new System.Drawing.Point(170, 70);
			this.cmbPrint.Name = "cmbPrint";
			this.cmbPrint.Size = new System.Drawing.Size(260, 40);
			this.cmbPrint.TabIndex = 2;
			this.cmbPrint.Text = "8.5 x 11 Portrait";
			this.cmbPrint.SelectedIndexChanged += new System.EventHandler(this.cmbPrint_SelectedIndexChanged);
			// 
			// lblPrint
			// 
			this.lblPrint.AutoSize = true;
			this.lblPrint.Location = new System.Drawing.Point(30, 84);
			this.lblPrint.Name = "lblPrint";
			this.lblPrint.Size = new System.Drawing.Size(45, 15);
			this.lblPrint.TabIndex = 1;
			this.lblPrint.Text = "PRINT";
			// 
			// framCommitOptions
			// 
			this.framCommitOptions.Controls.Add(this.chkNewPage);
			this.framCommitOptions.Controls.Add(this.cmbBind);
			this.framCommitOptions.Controls.Add(this.lblBind);
			this.framCommitOptions.Location = new System.Drawing.Point(30, 190);
			this.framCommitOptions.Name = "framCommitOptions";
			this.framCommitOptions.Size = new System.Drawing.Size(400, 120);
			this.framCommitOptions.TabIndex = 5;
			this.framCommitOptions.Text = "Options";
			// 
			// chkNewPage
			// 
			this.chkNewPage.Location = new System.Drawing.Point(20, 30);
			this.chkNewPage.Name = "chkNewPage";
			this.chkNewPage.Size = new System.Drawing.Size(270, 27);
			this.chkNewPage.TabIndex = 0;
			this.chkNewPage.Text = "New Page on New Starting Letter";
			this.chkNewPage.Visible = false;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(170, 130);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(95, 40);
			this.txtStart.TabIndex = 4;
			this.txtStart.Visible = false;
			// 
			// lblStartPage
			// 
			this.lblStartPage.Location = new System.Drawing.Point(30, 144);
			this.lblStartPage.Name = "lblStartPage";
			this.lblStartPage.Size = new System.Drawing.Size(72, 23);
			this.lblStartPage.TabIndex = 3;
			this.lblStartPage.Text = "START PAGE";
			this.lblStartPage.Visible = false;
			// 
			// lblCommitment
			// 
			this.lblCommitment.Location = new System.Drawing.Point(30, 30);
			this.lblCommitment.Name = "lblCommitment";
			this.lblCommitment.Size = new System.Drawing.Size(400, 27);
			this.lblCommitment.TabIndex = 0;
			this.lblCommitment.Text = "COMMITMENT BOOK(S) FOR SUPPLEMENTAL BILLS PROCESSED";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(184, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(100, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmPickCommitmentYear
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(474, 491);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPickCommitmentYear";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Format for Commitment Book";
			this.Load += new System.EventHandler(this.frmPickCommitmentYear_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPickCommitmentYear_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framCommitOptions)).EndInit();
			this.framCommitOptions.ResumeLayout(false);
			this.framCommitOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkNewPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
