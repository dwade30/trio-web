﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	public class modMDIParent
	{
		//=========================================================
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Public Sub SetFixedSize(FormName As Form, Optional UseRoutine As Boolean)
		/// </summary>
		/// <summary>
		/// Dim lngTempWidth As Long
		/// </summary>
		/// <summary>
		/// Dim lngTempHeight As Long
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// If Not UseRoutine Then Exit Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// If boolMaxForms Then
		/// </summary>
		/// <summary>
		/// FormName.WindowState = vbMaximized
		/// </summary>
		/// <summary>
		/// Else
		/// </summary>
		/// <summary>
		/// BW Change
		/// </summary>
		/// <summary>
		/// Form not wide enough for date & lines
		/// </summary>
		/// <summary>
		/// lngTempWidth = MDIParent.Width - mdiparent.grid.Width - 200
		/// </summary>
		/// <summary>
		/// If lngTempWidth < 1 Then Exit Sub
		/// </summary>
		/// <summary>
		/// lngTempHeight = mdiparent.grid.Height - 100
		/// </summary>
		/// <summary>
		/// If lngTempHeight < 1 Then Exit Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// FormName.Left = mdiparent.grid.Left + mdiparent.grid.Width + 50 ' from 100
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// FormName.Width = MDIParent.Width - mdiparent.grid.Width - 200  ' from 500
		/// </summary>
		/// <summary>
		/// FormName.Height = mdiparent.grid.Height - 100
		/// </summary>
		/// <summary>
		/// FormName.Top = MDIParent.Top + 660
		/// </summary>
		/// <summary>
		/// End BW Change
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmLoadValidAccounts, frmSelectBankNumber, frmSelectPostingJournal)
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			foreach (Form frm in Application.OpenForms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		
	}
}
