﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPCommitmentWide.
	/// </summary>
	public partial class rptPPCommitmentWide : BaseSectionReport
	{
		public rptPPCommitmentWide()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Personal Property Tax Commitment";
		}

		public static rptPPCommitmentWide InstancePtr
		{
			get
			{
				return (rptPPCommitmentWide)Sys.GetInstance(typeof(rptPPCommitmentWide));
			}
		}

		protected rptPPCommitmentWide _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsAddress.Dispose();
				clsCommit.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPCommitmentWide	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		/// <summary>
		/// This is the Wide and Landscape version of the Personal Property Commitment Book
		/// </summary>
		int TaxYear;
		clsDRWrapper clsCommit = new clsDRWrapper();
		string Addr1 = "";
		string Addr2 = "";
		string addr3 = "";
		private string strMailingAddress3 = "";
		int intStartPage;
		int intPage;
		string[] Categoryarray = new string[9 + 1];
		int lngValSub;
		int lngExemptsub;
		int lngAssessSub;
		double lngTaxSub;
		bool boolLandscape;
		clsDRWrapper clsAddress = new clsDRWrapper();
		const int CNSTACCOUNTORDER = 1;
		const int CNSTNAMEORDER = 2;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                lngValSub = 0;
                lngExemptsub = 0;
                lngAssessSub = 0;
                lngTaxSub = 0;
                // These fields will be filled with values that will then automatically
                // go to the textboxes and the summary textboxes in the page footer
                this.Fields.Add("ExemptVal");
                this.Fields.Add("TotVal");
                this.Fields.Add("TaxVal");
                this.Fields.Add("AssessVal");
                this.Fields.Add("Cat1Val");
                this.Fields.Add("Cat2Val");
                this.Fields.Add("Cat3Val");
                this.Fields.Add("CatOtherVal");
                this.Fields.Add("TheBinder");
                // fill the array with the 9 category descriptions
                clsTemp.OpenRecordset("select * from ratiotrends order by type", "twpp0000.vb1");
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                    Categoryarray[clsTemp.Get_Fields("type")] =
                        Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    clsTemp.MoveNext();
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In DataInitialize", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsTemp.Dispose();
            }
		}
		//private void ActiveReport_Error(int Number, DDActiveReports2.IReturnString Description, int Scode, string Source, string HelpFile, int HelpContext, DDActiveReports2.IReturnBool CancelDisplay)
		//{
		//	MessageBox.Show("Error Number " + FCConvert.ToString(Number) + " " + Description.Value + "\r\n" + "Source: " + Source, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
		//}
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsCommit.EndOfFile();
			if (!eArgs.EOF)
			{
				FillValueFields();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			try
			{
				// On Error GoTo ErrorHandler
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				// override the print button so we handle the event
				const_printtoolid = 9950;
				//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
				//{
				//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
				//	{
				//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
				//		this.Toolbar.Tools(cnt).Enabled = true;
				//	}
				//}
				// cnt
				clsAddress.OpenRecordset("select ACCOUNT,street,streetnumber,address1,address2,city,state,zip,zip4 from ppmaster order by account", "twpp0000.vb1");
				intPage = intStartPage;
				txtDate.Text = DateTime.Today.ToShortDateString();
				txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			bool boolAlreadyFound = false;
			try
			{
				// On Error GoTo ErrorHandler
				if (!clsCommit.EndOfFile())
				{
					boolAlreadyFound = false;
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					txtAccount.Text = clsCommit.Get_Fields_String("account");
					txtName.Text = clsCommit.Get_Fields_String("name1");
					if (!clsAddress.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						if (clsAddress.Get_Fields("account") == clsCommit.Get_Fields("account"))
						{
							boolAlreadyFound = true;
						}
					}
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					if (Conversion.Val(clsCommit.Get_Fields("streetnumber")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						txtLocation.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
					}
					else
					{
						txtLocation.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname")));
					}
					// get the address fields and then condense them to get rid of gaps
					Addr1 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address1")));
					Addr2 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address2")));
					addr3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address3")));
					strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("MailingAddress3")));
					if (Addr1 == string.Empty && Addr2 == string.Empty && addr3 == string.Empty)
					{
						if (!(clsAddress.EndOfFile() && clsAddress.BeginningOfFile()))
						{
							if (!boolAlreadyFound)
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								if (clsAddress.FindFirstRecord("account", clsCommit.Get_Fields("account")))
								{
									Addr1 = Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("address1")));
									Addr2 = Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("address2")));
									strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("address3")));
									addr3 = Strings.Trim(Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("city"))) + " " + Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("state"))) + " " + Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("zip"))));
									if (Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("zip4"))) != string.Empty)
									{
										addr3 += " " + Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("zip4")));
									}
									// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
									txtLocation.Text = Strings.Trim(clsAddress.Get_Fields("streetnumber") + " " + clsAddress.Get_Fields_String("street"));
									boolAlreadyFound = true;
								}
							}
							else
							{
								Addr1 = Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("address1")));
								Addr2 = Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("address2")));
								strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("address3")));
								addr3 = Strings.Trim(Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("city"))) + " " + Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("state"))) + " " + Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("zip"))));
								if (Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("zip4"))) != string.Empty)
								{
									addr3 += " " + Strings.Trim(FCConvert.ToString(clsAddress.Get_Fields_String("zip4")));
								}
								// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
								txtLocation.Text = Strings.Trim(clsAddress.Get_Fields("streetnumber") + " " + clsAddress.Get_Fields_String("street"));
							}
						}
					}
					if (Strings.Trim(Addr1) == string.Empty)
					{
						Addr1 = Addr2;
						Addr2 = strMailingAddress3;
						strMailingAddress3 = addr3;
						addr3 = "";
						if (Strings.Trim(Addr1) == string.Empty)
						{
							Addr1 = Addr2;
							Addr2 = strMailingAddress3;
							strMailingAddress3 = addr3;
							addr3 = "";
						}
					}
					if (Strings.Trim(Addr2) == string.Empty)
					{
						Addr2 = strMailingAddress3;
						strMailingAddress3 = addr3;
						addr3 = "";
					}
					txtAddress1.Text = Addr1;
					txtAddress2.Text = Addr2;
					txtAddress3.Text = addr3;
					txtMailingAddress3.Text = strMailingAddress3;
					txtPayment1.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue1"), "#,##0.00") + " (1)";
					txtPayment2.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue2"), "#,##0.00") + " (2)";
					txtPayment3.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue3"), "#,##0.00") + " (3)";
					txtPayment4.Text = Strings.Format(clsCommit.Get_Fields_Decimal("taxdue4"), "#,##0.00") + " (4)";
					if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) > 0)
					{
						txtPayment1.Visible = true;
						txtPayment2.Visible = true;
						if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) > 0)
						{
							txtPayment3.Visible = true;
							if (Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4")) > 0)
							{
								txtPayment4.Visible = true;
							}
							else
							{
								txtPayment4.Visible = false;
							}
						}
						else
						{
							txtPayment3.Visible = false;
							txtPayment4.Visible = false;
						}
					}
					else
					{
						txtPayment1.Visible = false;
						txtPayment2.Visible = false;
						txtPayment3.Visible = false;
						txtPayment4.Visible = false;
					}
					clsCommit.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In DetailFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageFooter_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(txtPageAssess.Text) > 0)
				{
					lngValSub += FCConvert.ToInt32(txtPageAssess.Text);
				}
				if (Conversion.Val(txtPageExempt.Text) > 0)
				{
					lngExemptsub += FCConvert.ToInt32(txtPageExempt.Text);
				}
				if (Conversion.Val(txtPageTotal.Text) > 0)
				{
					lngAssessSub += FCConvert.ToInt32(txtPageTotal.Text);
				}
				if (Conversion.Val(txtPageTax.Text) > 0)
				{
					lngTaxSub += FCConvert.ToDouble(txtPageTax.Text);
				}
				txtTotAssess.Text = Strings.Format(lngValSub, "#,###,###,##0");
				txtTotExempt.Text = Strings.Format(lngExemptsub, "#,###,###,##0");
				txtTotTotal.Text = Strings.Format(lngAssessSub, "#,###,###,##0");
				txtTotTax.Text = Strings.Format(lngTaxSub, "#,###,###,##0.00");
				if (clsCommit.EndOfFile())
				{
					lblSubtotals.Text = "Final Totals:";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In PageFooter Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// print the page number
			txtPage.Text = intPage.ToString();
			intPage += 1;
		}
		// vbPorter upgrade warning: intWhichReport As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intPageStart As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intWhichOrder As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intBindOption As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngTaxYear, bool boolSetFont, string strFontName, string strPrinterName, int intWhichReport, int intPageStart, bool boolNewPage, int lngRateKey, bool boolFromSupplemental = false, int intWhichOrder = 2, string strStart = "", string strEnd = "", int intBindOption = 0)
		{
			clsRateRecord clsrate = new clsRateRecord();
			int x;
			string strWhereClause;
			clsDRWrapper clsLoad = new clsDRWrapper();
			bool boolDup;
			string strOrderBy = "";
			string strRange;
            try
            {
                // On Error GoTo ErrorHandler
                strRange = "";
                switch (intWhichOrder)
                {
                    case CNSTACCOUNTORDER:
                    {
                        strOrderBy = "Account";
                        if (Strings.Trim(strStart) != string.Empty || Strings.Trim(strEnd) != string.Empty)
                        {
                            if (Strings.Trim(strStart) == string.Empty)
                            {
                                strRange = " and account <= " + FCConvert.ToString(Conversion.Val(strEnd));
                            }
                            else if (Strings.Trim(strEnd) == string.Empty)
                            {
                                strRange = " and account >= " + FCConvert.ToString(Conversion.Val(strStart));
                            }
                            else
                            {
                                strRange = " and account between " + FCConvert.ToString(Conversion.Val(strStart)) +
                                           " and " + FCConvert.ToString(Conversion.Val(strEnd));
                            }
                        }

                        break;
                    }
                    case CNSTNAMEORDER:
                    {
                        strOrderBy = "Name1";
                        if (Strings.Trim(strStart) != string.Empty || Strings.Trim(strEnd) != string.Empty)
                        {
                            if (Strings.Trim(strStart) == string.Empty)
                            {
                                strRange = " and name1 <= '" + Strings.Trim(strEnd) + "' ";
                            }
                            else if (Strings.Trim(strEnd) == string.Empty)
                            {
                                strRange = " and name1 >= '" + Strings.Trim(strStart) + "'";
                            }
                            else
                            {
                                strRange = " and name1 between '" + Strings.Trim(strStart) + "' and '" +
                                           Strings.Trim(strEnd) + "' ";
                            }
                        }

                        break;
                    }
                }

                //end switch
                boolDup = false;
                clsLoad.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("duplexcommitment")))
                        boolDup = true;
                }

                TaxYear = lngTaxYear;
                // set the initial page number
                intStartPage = intPageStart;
                if (boolNewPage)
                {
                    this.GroupHeader1.DataField = ("TheBinder");
                }

                clsrate.LoadRate(lngRateKey);
                lblDescription.Text = Strings.Trim(clsrate.Description);
                strWhereClause = "";
                if (boolFromSupplemental)
                {
                    for (x = 1; x <= frmSupplementalBill.InstancePtr.ListGrid.Rows - 1; x++)
                    {
                        if (Strings.UCase(Strings.Trim(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 0))) ==
                            "PP")
                        {
                            if (Conversion.Val(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 4)) == lngRateKey)
                            {
                                if (strWhereClause != "")
                                {
                                    strWhereClause += ",";
                                }
                                else
                                {
                                    strWhereClause = " ID in (";
                                }

                                strWhereClause +=
                                    FCConvert.ToString(
                                        Conversion.Val(frmSupplementalBill.InstancePtr.ListGrid.TextMatrix(x, 6)));
                            }
                        }
                    }

                    // x
                    strWhereClause += ")";
                    clsCommit.OpenRecordset(
                        "select  * from billingmaster where " + strWhereClause + " and billingtype = 'PP'  order by " +
                        strOrderBy, "twcl0000.vb1");
                }
                else
                {
                    clsCommit.OpenRecordset(
                        "select * from billingmaster where ratekey  = " + FCConvert.ToString(lngRateKey) +
                        " and billingtype = 'PP' " + strRange + " order by " + strOrderBy, "twcl0000.vb1");
                }

                if (!clsCommit.EndOfFile())
                {
                    lblTitle.Text = lblTitle.Text + FCConvert.ToString(TaxYear) + " " +
                                    Strings.Format(clsrate.TaxRate * 1000, "0.000");
                    lblMuniname.Text = modGlobalConstants.Statics.MuniName;
                    // if using a printer font, then set all textboxes to it, and make captions bold
                    if (boolSetFont)
                    {
                        for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
                        {
                            // any field I marked with textbox must have its font changed. Bold is a field that also has bold font
                            bool setFont = false;
                            bool bold = false;
                            if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
                            {
                                setFont = true;
                            }
                            else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
                            {
                                setFont = true;
                                bold = true;
                            }

                            if (setFont)
                            {
                                GrapeCity.ActiveReports.SectionReportModel.TextBox textBox =
                                    this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                                if (textBox != null)
                                {
                                    textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
                                }
                                else
                                {
                                    GrapeCity.ActiveReports.SectionReportModel.Label label =
                                        this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                    if (label != null)
                                    {
                                        label.Font = new Font(strFontName, 10,
                                            bold ? FontStyle.Bold : FontStyle.Regular);
                                    }
                                }
                            }
                        }

                        // x
                        for (x = 0; x <= this.PageFooter.Controls.Count - 1; x++)
                        {
                            // any field I marked with textbox must have its font changed. Bold is a field that also has bold font
                            bool setFont = false;
                            bool bold = false;
                            if (FCConvert.ToString(this.PageFooter.Controls[x].Tag) == "textbox")
                            {
                                setFont = true;
                            }
                            else if (FCConvert.ToString(this.PageFooter.Controls[x].Tag) == "bold")
                            {
                                setFont = true;
                                bold = true;
                            }

                            if (setFont)
                            {
                                GrapeCity.ActiveReports.SectionReportModel.TextBox textBox =
                                    this.PageFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                                if (textBox != null)
                                {
                                    textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
                                }
                                else
                                {
                                    GrapeCity.ActiveReports.SectionReportModel.Label label =
                                        this.PageFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                    if (label != null)
                                    {
                                        label.Font = new Font(strFontName, 10,
                                            bold ? FontStyle.Bold : FontStyle.Regular);
                                    }
                                }
                            }
                        }

                        // x
                        //this.Printer.RenderMode = 1;
                    }

                    // set the printer to the one they picked
                    this.Document.Printer.PrinterName = strPrinterName;
                    // set the orientation depending on which report they picked
                    switch (intWhichReport)
                    {
                        case 2:
                        {
                            // landscape regular
                            this.Document.Printer.DefaultPageSettings.Landscape = true;
                            boolLandscape = true;
                            break;
                        }
                        case 3:
                        {
                            // wide printer
                            //this.Document.Printer.PaperSize = 39;
                            this.Document.Printer.PaperKind = System.Drawing.Printing.PaperKind.USStandardFanfold;
                            boolLandscape = false;
                            // Me.PageSettings.PaperHeight = Me.Printer.PaperHeight
                            // Me.PageSettings.PaperWidth = Me.Printer.PaperWidth
                            break;
                        }
                    }

                    //end switch
                    switch (intBindOption)
                    {
                        case 1:
                        {
                            // left bind
                            this.PageSettings.Margins.Left = 2880 / 1440f;
                            this.PrintWidth = 15840 / 1440f;
                            break;
                        }
                        case 2:
                        {
                            // top bind
                            this.PageSettings.Margins.Top = 1;
                            break;
                        }
                    }

                    //end switch
                    if (!boolFromSupplemental)
                    {
                        // Me.Show , MDIParent
                        //FC:FINAL:MSH - issue #1526: show 'Wide' report in modal window 
                        frmReportViewer.InstancePtr.Init(rptObj: this, strPrinter: FCGlobal.Printer.DeviceName,
                            intModal: 1, boolDuplex: boolDup, boolLandscape: boolLandscape, boolAllowEmail: true,
                            strAttachmentName: "PPCommitment", showModal: true);
                    }
                    else
                    {
                        //FC:FINAL:MSH - issue #1526: show 'Wide' report in modal window 
                        frmReportViewer.InstancePtr.Init(rptObj: this, strPrinter: FCGlobal.Printer.DeviceName,
                            intModal: 1, boolDuplex: boolDup, boolLandscape: boolLandscape, boolAllowEmail: true,
                            strAttachmentName: "PPCommitment", showModal: true);
                    }
                }
                else
                {
                    // nothing to print so exit
                    // If Not boolFromSupplemental Then
                    // MDIParent.Show
                    // End If
                    this.Close();
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.Dispose();
            }
		}

		private void FillValueFields()
		{
			// must do this or the values will not fill in correctly
			int x;
			// loop counter
			int CurrCategory;
			int lngOther;
			// accumulate category values
			int lngCategory;
			// current category's value
			try
			{
				// On Error GoTo ErrorHandler
				if (Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name1"))) != string.Empty)
				{
					this.Fields["TheBinder"].Value = Strings.Left(FCConvert.ToString(clsCommit.Get_Fields_String("name1")), 1);
				}
				else
				{
					this.Fields["TheBinder"].Value = "";
				}
				// fill the monetary fields
				this.Fields["assessval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("ppassessment"));
				this.Fields["exemptval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
				this.Fields["totval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("ppassessment")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
				this.Fields["taxval"].Value = Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
				// fill category information, but initialize some things first
				CurrCategory = 0;
				lngOther = 0;
				lngCategory = 0;
				txtCategory1.Text = "";
				txtCategory2.Text = "";
				txtCategory3.Text = "";
				txtCategoryOther.Text = "";
				this.Fields["Cat1Val"].Value = "";
				this.Fields["Cat2Val"].Value = "";
				this.Fields["Cat3Val"].Value = "";
				this.Fields["CatOtherVal"].Value = "";
				txtCatOtherVal.Text = "";
				// just use first four categories that have values
				// if more than four then put 4+ together as other
				for (x = 1; x <= 9; x++)
				{
					// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
					lngCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCommit.Get_Fields("category" + FCConvert.ToString(x)))));
					if (lngCategory > 0)
					{
						CurrCategory += 1;
						if (CurrCategory < 5)
						{
							lngOther = lngCategory;
							switch (CurrCategory)
							{
								case 1:
									{
										txtCategory1.Text = Categoryarray[x];
										this.Fields["Cat1Val"].Value = lngCategory;
										break;
									}
								case 2:
									{
										txtCategory2.Text = Categoryarray[x];
										this.Fields["Cat2Val"].Value = lngCategory;
										break;
									}
								case 3:
									{
										txtCategory3.Text = Categoryarray[x];
										this.Fields["Cat3Val"].Value = lngCategory;
										break;
									}
								case 4:
									{
										txtCategoryOther.Text = Categoryarray[x];
										this.Fields["CatOtherVal"].Value = lngCategory;
										break;
									}
							}
							//end switch
						}
						else
						{
							lngOther += lngCategory;
							txtCategoryOther.Text = "OTHER";
							this.Fields["CatOtherVal"].Value = lngOther;
						}
					}
				}
				// x
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillValueFields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
