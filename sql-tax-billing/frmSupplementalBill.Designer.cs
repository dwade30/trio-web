//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmSupplementalBill.
	/// </summary>
	partial class frmSupplementalBill : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSupplemental;
		public fecherFoundation.FCLabel lblSupplemental;
		public fecherFoundation.FCComboBox cmbTaxFrom;
		public fecherFoundation.FCLabel lblTaxFrom;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtTax;
		public fecherFoundation.FCLabel Label1;
		public FCGrid ListGrid;
		public fecherFoundation.FCLabel Label2;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuTranRE;
		public fecherFoundation.FCToolStripMenuItem mnuTranPP;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuRERate;
		public fecherFoundation.FCToolStripMenuItem mnuPPRate;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuProcessBill;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuPop;
		public fecherFoundation.FCToolStripMenuItem mnuRemove;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbSupplemental = new fecherFoundation.FCComboBox();
			this.lblSupplemental = new fecherFoundation.FCLabel();
			this.cmbTaxFrom = new fecherFoundation.FCComboBox();
			this.lblTaxFrom = new fecherFoundation.FCLabel();
			this.cmdAdd = new fecherFoundation.FCButton();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtTax = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ListGrid = new fecherFoundation.FCGrid();
			this.Label2 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTranRE = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTranPP = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRERate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPPRate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessBill = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPop = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRemove = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessBill = new fecherFoundation.FCButton();
			this.cmdTranRE = new fecherFoundation.FCButton();
			this.cmdTranPP = new fecherFoundation.FCButton();
			this.fcButton3 = new fecherFoundation.FCButton();
			this.fcButton4 = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ListGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTranRE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTranPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton4)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessBill);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdAdd);
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.cmbSupplemental);
			this.ClientArea.Controls.Add(this.lblSupplemental);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.ListGrid);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.fcButton4);
			this.TopPanel.Controls.Add(this.fcButton3);
			this.TopPanel.Controls.Add(this.cmdTranPP);
			this.TopPanel.Controls.Add(this.cmdTranRE);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdTranRE, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdTranPP, 0);
			this.TopPanel.Controls.SetChildIndex(this.fcButton3, 0);
			this.TopPanel.Controls.SetChildIndex(this.fcButton4, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(236, 30);
			this.HeaderText.Text = "Supplemental Billing";
			// 
			// cmbSupplemental
			// 
			this.cmbSupplemental.AutoSize = false;
			this.cmbSupplemental.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSupplemental.FormattingEnabled = true;
			this.cmbSupplemental.Items.AddRange(new object[] {
				"Real Estate",
				"Personal Property"
			});
			this.cmbSupplemental.Location = new System.Drawing.Point(187, 30);
			this.cmbSupplemental.Name = "cmbSupplemental";
			this.cmbSupplemental.Size = new System.Drawing.Size(198, 40);
			this.cmbSupplemental.TabIndex = 1;
			this.cmbSupplemental.Text = "Real Estate";
			// 
			// lblSupplemental
			// 
			this.lblSupplemental.AutoSize = true;
			this.lblSupplemental.Location = new System.Drawing.Point(30, 44);
			this.lblSupplemental.Name = "lblSupplemental";
			this.lblSupplemental.Size = new System.Drawing.Size(106, 15);
			this.lblSupplemental.TabIndex = 0;
			this.lblSupplemental.Text = "SUPPLEMENTAL TYPE";
			// 
			// cmbTaxFrom
			// 
			this.cmbTaxFrom.AutoSize = false;
			this.cmbTaxFrom.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTaxFrom.FormattingEnabled = true;
			this.cmbTaxFrom.Items.AddRange(new object[] {
				"Calculated",
				"Entered"
			});
			this.cmbTaxFrom.Location = new System.Drawing.Point(157, 30);
			this.cmbTaxFrom.Name = "cmbTaxFrom";
			this.cmbTaxFrom.Size = new System.Drawing.Size(180, 40);
			this.cmbTaxFrom.TabIndex = 1;
			this.cmbTaxFrom.Text = "Entered";
			// 
			// lblTaxFrom
			// 
			this.lblTaxFrom.AutoSize = true;
			this.lblTaxFrom.Location = new System.Drawing.Point(20, 44);
			this.lblTaxFrom.Name = "lblTaxFrom";
			this.lblTaxFrom.Size = new System.Drawing.Size(71, 15);
			this.lblTaxFrom.TabIndex = 0;
			this.lblTaxFrom.Text = "TAX FROM";
			// 
			// cmdAdd
			// 
			this.cmdAdd.AppearanceKey = "actionButton";
			this.cmdAdd.ForeColor = System.Drawing.Color.White;
			this.cmdAdd.Location = new System.Drawing.Point(321, 249);
			this.cmdAdd.Name = "cmdAdd";
			this.cmdAdd.Size = new System.Drawing.Size(64, 40);
			this.cmdAdd.TabIndex = 5;
			this.cmdAdd.Text = "Add";
			this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.LinkItem = null;
			this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAccount.LinkTopic = null;
			this.txtAccount.Location = new System.Drawing.Point(187, 250);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(116, 40);
			this.txtAccount.TabIndex = 4;
			this.txtAccount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtTax);
			this.Frame1.Controls.Add(this.cmbTaxFrom);
			this.Frame1.Controls.Add(this.lblTaxFrom);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(30, 90);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(355, 141);
			this.Frame1.TabIndex = 2;
			this.Frame1.Text = "Tax From";
			// 
			// txtTax
			// 
			this.txtTax.AutoSize = false;
			this.txtTax.BackColor = System.Drawing.SystemColors.Window;
			this.txtTax.LinkItem = null;
			this.txtTax.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTax.LinkTopic = null;
			this.txtTax.Location = new System.Drawing.Point(157, 90);
			this.txtTax.Name = "txtTax";
			this.txtTax.Size = new System.Drawing.Size(148, 40);
			this.txtTax.TabIndex = 3;
			this.txtTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 104);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(42, 19);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "TAX";
			// 
			// ListGrid
			// 
			this.ListGrid.AllowSelection = false;
			this.ListGrid.AllowUserToResizeColumns = false;
			this.ListGrid.AllowUserToResizeRows = false;
			this.ListGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.ListGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.ListGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.ListGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.ListGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.ListGrid.BackColorSel = System.Drawing.Color.Empty;
			this.ListGrid.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.ListGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.ListGrid.ColumnHeadersHeight = 30;
			this.ListGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.ListGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.ListGrid.DragIcon = null;
			this.ListGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.ListGrid.ExtendLastCol = true;
			this.ListGrid.FixedCols = 0;
			this.ListGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.ListGrid.FrozenCols = 0;
			this.ListGrid.GridColor = System.Drawing.Color.Empty;
			this.ListGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.ListGrid.Location = new System.Drawing.Point(452, 30);
			this.ListGrid.Name = "ListGrid";
			this.ListGrid.ReadOnly = true;
			this.ListGrid.RowHeadersVisible = false;
			this.ListGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.ListGrid.RowHeightMin = 0;
			this.ListGrid.Rows = 50;
			this.ListGrid.ScrollTipText = null;
			this.ListGrid.ShowColumnVisibilityMenu = false;
			this.ListGrid.Size = new System.Drawing.Size(588, 460);
			this.ListGrid.StandardTab = true;
			this.ListGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.ListGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.ListGrid.TabIndex = 6;
			this.ListGrid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.ListGrid_MouseDownEvent);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 264);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(124, 22);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "ACCOUNT";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuTranRE,
				this.mnuTranPP,
				this.mnuSepar1,
				this.mnuRERate,
				this.mnuPPRate,
				this.mnuSepar,
				this.mnuProcessBill,
				this.mnuSepar2,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuTranRE
			// 
			this.mnuTranRE.Index = 0;
			this.mnuTranRE.Name = "mnuTranRE";
			this.mnuTranRE.Text = "Transfer Values from RE";
			this.mnuTranRE.Click += new System.EventHandler(this.mnuTranRE_Click);
			// 
			// mnuTranPP
			// 
			this.mnuTranPP.Index = 1;
			this.mnuTranPP.Name = "mnuTranPP";
			this.mnuTranPP.Text = "Transfer Values from PP";
			this.mnuTranPP.Click += new System.EventHandler(this.mnuTranPP_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 2;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuRERate
			// 
			this.mnuRERate.Index = 3;
			this.mnuRERate.Name = "mnuRERate";
			this.mnuRERate.Text = "Choose New RE Tax Rate";
			this.mnuRERate.Click += new System.EventHandler(this.mnuRERate_Click);
			// 
			// mnuPPRate
			// 
			this.mnuPPRate.Index = 4;
			this.mnuPPRate.Name = "mnuPPRate";
			this.mnuPPRate.Text = "Choose New PP Tax Rate";
			this.mnuPPRate.Click += new System.EventHandler(this.mnuPPRate_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 5;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuProcessBill
			// 
			this.mnuProcessBill.Index = 6;
			this.mnuProcessBill.Name = "mnuProcessBill";
			this.mnuProcessBill.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessBill.Text = "Process Bill List";
			this.mnuProcessBill.Click += new System.EventHandler(this.mnuProcessBill_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 7;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 8;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// mnuPop
			// 
			this.mnuPop.Index = -1;
			this.mnuPop.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRemove,
				this.mnuCancel
			});
			this.mnuPop.Name = "mnuPop";
			this.mnuPop.Text = "PopUp";
			this.mnuPop.Visible = false;
			// 
			// mnuRemove
			// 
			this.mnuRemove.Index = 0;
			this.mnuRemove.Name = "mnuRemove";
			this.mnuRemove.Text = "Remove this Entry";
			this.mnuRemove.Click += new System.EventHandler(this.mnuRemove_Click);
			// 
			// mnuCancel
			// 
			this.mnuCancel.Index = 1;
			this.mnuCancel.Name = "mnuCancel";
			this.mnuCancel.Text = "Cancel";
			// 
			// cmdProcessBill
			// 
			this.cmdProcessBill.AppearanceKey = "acceptButton";
			this.cmdProcessBill.Location = new System.Drawing.Point(333, 30);
			this.cmdProcessBill.Name = "cmdProcessBill";
			this.cmdProcessBill.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessBill.Size = new System.Drawing.Size(172, 48);
			this.cmdProcessBill.TabIndex = 0;
			this.cmdProcessBill.Text = "Process Bill List";
			this.cmdProcessBill.Click += new System.EventHandler(this.mnuProcessBill_Click);
			// 
			// cmdTranRE
			// 
			this.cmdTranRE.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdTranRE.AppearanceKey = "toolbarButton";
			this.cmdTranRE.Location = new System.Drawing.Point(872, 29);
			this.cmdTranRE.Name = "cmdTranRE";
			this.cmdTranRE.Size = new System.Drawing.Size(168, 24);
			this.cmdTranRE.TabIndex = 1;
			this.cmdTranRE.Text = "Transfer Values from RE";
			this.cmdTranRE.Click += new System.EventHandler(this.mnuTranRE_Click);
			// 
			// cmdTranPP
			// 
			this.cmdTranPP.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdTranPP.AppearanceKey = "toolbarButton";
			this.cmdTranPP.Location = new System.Drawing.Point(709, 29);
			this.cmdTranPP.Name = "cmdTranPP";
			this.cmdTranPP.Size = new System.Drawing.Size(158, 24);
			this.cmdTranPP.TabIndex = 2;
			this.cmdTranPP.Text = "Transfer Values from PP";
			this.cmdTranPP.Click += new System.EventHandler(this.mnuTranPP_Click);
			// 
			// fcButton3
			// 
			this.fcButton3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.fcButton3.AppearanceKey = "toolbarButton";
			this.fcButton3.Location = new System.Drawing.Point(527, 29);
			this.fcButton3.Name = "fcButton3";
			this.fcButton3.Size = new System.Drawing.Size(176, 24);
			this.fcButton3.TabIndex = 3;
			this.fcButton3.Text = "Choose New RE Tax Rate";
			this.fcButton3.Click += new System.EventHandler(this.mnuRERate_Click);
			// 
			// fcButton4
			// 
			this.fcButton4.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.fcButton4.AppearanceKey = "toolbarButton";
			this.fcButton4.Location = new System.Drawing.Point(351, 29);
			this.fcButton4.Name = "fcButton4";
			this.fcButton4.Size = new System.Drawing.Size(170, 24);
			this.fcButton4.TabIndex = 4;
			this.fcButton4.Text = "Choose New PP Tax Rate";
			this.fcButton4.Click += new System.EventHandler(this.mnuPPRate_Click);
			// 
			// frmSupplementalBill
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = null;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmSupplementalBill";
			this.ShowInTaskbar = false;
			this.Text = "Supplemental Billing";
			this.Load += new System.EventHandler(this.frmSupplementalBill_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSupplementalBill_KeyDown);
			this.Resize += new System.EventHandler(this.frmSupplementalBill_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ListGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTranRE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTranPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton4)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessBill;
		private FCButton fcButton4;
		private FCButton fcButton3;
		private FCButton cmdTranPP;
		private FCButton cmdTranRE;
	}
}