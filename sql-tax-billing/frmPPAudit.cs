﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.Drawing;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPPAudit.
	/// </summary>
	public partial class frmPPAudit : BaseForm
	{
		public frmPPAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPAudit InstancePtr
		{
			get
			{
				return (frmPPAudit)Sys.GetInstance(typeof(frmPPAudit));
			}
		}

		protected frmPPAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		double dblTaxRate;
		clsRateRecord clsRateInfo = new clsRateRecord();
		int lngTotvalue;
		int lngTotExempt;
		int lngTotAssess;
		double dblTotTax;
		public string strBegin = "";
		/// <summary>
		/// used for ranges
		/// </summary>
		public string strEnd = "";
		/// <summary>
		/// used for ranges
		/// </summary>
		public int intWhereFrom;
		public int intWhich;
		const int colAcct = 0;
		const int colName = 1;
		const int colSnum = 2;
		const int colLoc = 3;
		const int colValue = 4;
		const int colExempt = 5;
		const int colAssessment = 6;
		const int colTax = 7;
		const int colCat1 = 8;
		const int colCat2 = 9;
		const int colCat3 = 10;
		const int colCat4 = 11;
		const int colCat5 = 12;
		const int colCat6 = 13;
		const int colCat7 = 14;
		const int colCat8 = 15;
		const int colCat9 = 16;
		const int colBETECat1 = 17;
		const int colBETECat2 = 18;
		const int colBETECat3 = 19;
		const int colBETECat4 = 20;
		const int colBETECat5 = 21;
		const int colBETECat6 = 22;
		const int colBETECat7 = 23;
		const int colBETECat8 = 24;
		const int colBETECat9 = 25;
		public bool boolDisplayFirst;
		public string strSequence = "";
		int intSortOrder;
		bool boolPrintQuery;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cPartyController tPCont = new cPartyController();
		private cPartyController tPCont_AutoInitialized;

		private cPartyController tPCont
		{
			get
			{
				if (tPCont_AutoInitialized == null)
				{
					tPCont_AutoInitialized = new cPartyController();
				}
				return tPCont_AutoInitialized;
			}
			set
			{
				tPCont_AutoInitialized = value;
			}
		}

		private void AuditGrid_BeforeSort(object sender, DataGridViewCellMouseEventArgs e)
		{
			if ((AuditGrid.Col == colSnum) || (AuditGrid.Col == colLoc))
			{
				//e.order = 0;
				switch (intSortOrder)
				{
					case 0:
						{
							AuditGrid.Col = colSnum;
							AuditGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
							AuditGrid.Col = colLoc;
							AuditGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
							intSortOrder = 1;
							break;
						}
					case 1:
						{
							AuditGrid.Col = colSnum;
							AuditGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
							AuditGrid.Col = colLoc;
							AuditGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
							intSortOrder = 0;
							break;
						}
				}
				//end switch
			}
		}

		private void frmPPAudit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

       private void frmPPAudit_Load(object sender, System.EventArgs e)
		{
			SetupAuditGrid();
			SetupCategoryGrid();
			SetupTotalGrid();
			// Call FillAuditGrid
			if (modGlobalConstants.Statics.boolMaxForms)
			{
				this.WindowState = FormWindowState.Maximized;
			}

			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			boolPrintQuery = false;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			if (intWhereFrom == 4 && boolPrintQuery)
			{
				intResponse = MessageBox.Show("Print Commitment Book Now?", "Print Commitment Book?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResponse == DialogResult.Yes)
				{
					// print the commitment book for this year
					frmPickCommitmentYear.InstancePtr.Init(clsRateInfo.RateKey, false);
				}
			}
		}

		private void frmPPAudit_Resize(object sender, System.EventArgs e)
		{
			ResizeAuditGrid();
			ResizeCategoryGrid();
			ResizeTotalGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			int intResponse;
			// check to see if they want to print the commitment book now
			if (boolDisplayFirst)
			{
				modGlobalVariables.Statics.gboolDonePPXferReport = true;
			}
			else
			{
				if (!modGlobalVariables.Statics.gboolDonePPXferReport)
				{
					MessageBox.Show("Cannot exit until transfer information is done saving.  Please try again in a few moments.", "Saving", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			// If intWhereFrom = 4 Then
			// intResponse = MsgBox("Print Commitment Book Now?", vbYesNo + vbQuestion, "Print Commitment Book?")
			// If intResponse = vbYes Then
			// print the commitment book for this year
			// Call frmPickCommitmentYear.Init(clsRateInfo.RateKey, False)
			// End If
			// End If
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupTotalGrid()
		{
			TotalGrid.Rows = 1;
			TotalGrid.Cols = 6;
			TotalGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.TextMatrix(0, 0, "Total");
			TotalGrid.Font = new Font(TotalGrid.Font, FontStyle.Bold);
			GridSummary.Rows = 2;
			GridSummary.Cols = 6;
			GridSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridSummary.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.TextMatrix(0, 1, "Count");
			GridSummary.TextMatrix(0, 2, "Value");
			GridSummary.TextMatrix(0, 3, "Exemptions");
			GridSummary.TextMatrix(0, 4, "Assessment");
			GridSummary.TextMatrix(0, 5, "Tax");
			GridSummary.TextMatrix(1, 0, "Total");
			GridSummary.Font = new Font(GridSummary.Font, FontStyle.Bold);
		}

		private void SetupAuditGrid()
		{
			int x;
			AuditGrid.FixedCols = 0;
			AuditGrid.FixedRows = 1;
			AuditGrid.Rows = 1;
			AuditGrid.Cols = 26;
			for (x = 8; x <= 25; x++)
			{
				AuditGrid.ColHidden(x, true);
			}
			// x
			AuditGrid.ColAlignment(colValue, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ColAlignment(colExempt, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ColAlignment(colAssessment, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ColAlignment(colTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ExtendLastCol = true;
			AuditGrid.TextMatrix(0, 0, "Acct");
			AuditGrid.TextMatrix(0, 1, "Name");
			AuditGrid.TextMatrix(0, colSnum, "#");
			AuditGrid.TextMatrix(0, colLoc, "Location");
			AuditGrid.TextMatrix(0, colValue, "Value");
			AuditGrid.TextMatrix(0, colExempt, "Exempt");
			AuditGrid.TextMatrix(0, colAssessment, "Assessment");
			AuditGrid.TextMatrix(0, colTax, "Tax");
			//FC:FINAL:MSH - issue #1337: add date types to columns for correct sorting
			AuditGrid.ColDataType(colAcct, FCGrid.DataTypeSettings.flexDTLong);
			AuditGrid.ColDataType(colName, FCGrid.DataTypeSettings.flexDTString);
			AuditGrid.ColDataType(colSnum, FCGrid.DataTypeSettings.flexDTLong);
			AuditGrid.ColDataType(colLoc, FCGrid.DataTypeSettings.flexDTString);
			AuditGrid.ColDataType(colValue, FCGrid.DataTypeSettings.flexDTDouble);
			AuditGrid.ColDataType(colExempt, FCGrid.DataTypeSettings.flexDTDouble);
			AuditGrid.ColDataType(colAssessment, FCGrid.DataTypeSettings.flexDTDouble);
			AuditGrid.ColDataType(colTax, FCGrid.DataTypeSettings.flexDTDouble);
		}

		private void SetupCategoryGrid()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			CategoryGrid.FixedCols = 0;
			CategoryGrid.FixedRows = 0;
			CategoryGrid.Rows = 3;
			CategoryGrid.Cols = 6;
			CategoryGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			CategoryGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			CategoryGrid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			CategoryGrid.ExtendLastCol = true;
			clsTemp.OpenRecordset("select * from ratioTRENDS", "twpp0000.vb1");
			clsTemp.FindFirstRecord("type", 1);
			CategoryGrid.TextMatrix(0, 0, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 2);
			CategoryGrid.TextMatrix(1, 0, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 3);
			CategoryGrid.TextMatrix(2, 0, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 4);
			CategoryGrid.TextMatrix(0, 2, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 5);
			CategoryGrid.TextMatrix(1, 2, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 6);
			CategoryGrid.TextMatrix(2, 2, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 7);
			CategoryGrid.TextMatrix(0, 4, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 8);
			CategoryGrid.TextMatrix(1, 4, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 9);
			CategoryGrid.TextMatrix(2, 4, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			CategoryGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, true);
			CategoryGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 2, 2, 2, true);
			CategoryGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 4, 2, 4, true);
			GridSummaryCategory.FixedCols = 0;
			GridSummaryCategory.FixedRows = 0;
			GridSummaryCategory.Rows = 3;
			GridSummaryCategory.Cols = 6;
			GridSummaryCategory.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummaryCategory.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummaryCategory.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummaryCategory.ExtendLastCol = true;
			clsTemp.OpenRecordset("select * from ratioTRENDS", "twpp0000.vb1");
			clsTemp.FindFirstRecord("type", 1);
			GridSummaryCategory.TextMatrix(0, 0, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 2);
			GridSummaryCategory.TextMatrix(1, 0, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 3);
			GridSummaryCategory.TextMatrix(2, 0, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 4);
			GridSummaryCategory.TextMatrix(0, 2, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 5);
			GridSummaryCategory.TextMatrix(1, 2, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 6);
			GridSummaryCategory.TextMatrix(2, 2, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 7);
			GridSummaryCategory.TextMatrix(0, 4, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 8);
			GridSummaryCategory.TextMatrix(1, 4, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			clsTemp.FindFirstRecord("type", 9);
			GridSummaryCategory.TextMatrix(2, 4, FCConvert.ToString(clsTemp.Get_Fields_String("description")));
			GridSummaryCategory.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, true);
			GridSummaryCategory.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 2, 2, 2, true);
			GridSummaryCategory.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 4, 2, 4, true);
			GridBETEExempt.Rows = 3;
			GridBETEExempt.Cols = 6;
			GridBETEExempt.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridBETEExempt.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridBETEExempt.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridBETEExempt.ExtendLastCol = true;
			GridBETEExempt.TextMatrix(0, 0, CategoryGrid.TextMatrix(0, 0));
			GridBETEExempt.TextMatrix(1, 0, CategoryGrid.TextMatrix(1, 0));
			GridBETEExempt.TextMatrix(2, 0, CategoryGrid.TextMatrix(2, 0));
			GridBETEExempt.TextMatrix(0, 2, CategoryGrid.TextMatrix(0, 2));
			GridBETEExempt.TextMatrix(1, 2, CategoryGrid.TextMatrix(1, 2));
			GridBETEExempt.TextMatrix(2, 2, CategoryGrid.TextMatrix(2, 2));
			GridBETEExempt.TextMatrix(0, 4, CategoryGrid.TextMatrix(0, 4));
			GridBETEExempt.TextMatrix(1, 4, CategoryGrid.TextMatrix(1, 4));
			GridBETEExempt.TextMatrix(2, 4, CategoryGrid.TextMatrix(2, 4));
			GridBETEExempt.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, true);
			GridBETEExempt.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 2, 2, 2, true);
			GridBETEExempt.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 4, 2, 4, true);
			GridSummaryBETEExempt.FixedCols = 0;
			GridSummaryBETEExempt.FixedRows = 0;
			GridSummaryBETEExempt.Rows = 3;
			GridSummaryBETEExempt.Cols = 6;
			GridSummaryBETEExempt.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummaryBETEExempt.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummaryBETEExempt.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummaryBETEExempt.TextMatrix(0, 0, GridSummaryCategory.TextMatrix(0, 0));
			GridSummaryBETEExempt.TextMatrix(1, 0, GridSummaryCategory.TextMatrix(1, 0));
			GridSummaryBETEExempt.TextMatrix(2, 0, GridSummaryCategory.TextMatrix(2, 0));
			GridSummaryBETEExempt.TextMatrix(0, 2, GridSummaryCategory.TextMatrix(0, 2));
			GridSummaryBETEExempt.TextMatrix(1, 2, GridSummaryCategory.TextMatrix(1, 2));
			GridSummaryBETEExempt.TextMatrix(2, 2, GridSummaryCategory.TextMatrix(2, 2));
			GridSummaryBETEExempt.TextMatrix(0, 4, GridSummaryCategory.TextMatrix(0, 4));
			GridSummaryBETEExempt.TextMatrix(1, 4, GridSummaryCategory.TextMatrix(1, 4));
			GridSummaryBETEExempt.TextMatrix(2, 4, GridSummaryCategory.TextMatrix(2, 4));
			GridSummaryBETEExempt.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 2, 0, true);
			GridSummaryBETEExempt.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 2, 2, 2, true);
			GridSummaryBETEExempt.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 4, 2, 4, true);
		}

		private void ResizeCategoryGrid()
		{
			int GridWidth;
			GridWidth = CategoryGrid.WidthOriginal;
			CategoryGrid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.22));
			CategoryGrid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.11));
			CategoryGrid.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.22));
			CategoryGrid.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.11));
			CategoryGrid.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.22));
			CategoryGrid.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.11));
			//CategoryGrid.HeightOriginal = CategoryGrid.RowHeight(0) * 3;
			GridBETEExempt.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.22));
			GridBETEExempt.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.11));
			GridBETEExempt.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.22));
			GridBETEExempt.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.11));
			GridBETEExempt.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.22));
			GridBETEExempt.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.11));
			//GridBETEExempt.HeightOriginal = GridBETEExempt.RowHeight(0) * 3;
			GridWidth = GridSummaryCategory.WidthOriginal;
			GridSummaryCategory.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.22));
			GridSummaryCategory.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.11));
			GridSummaryCategory.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.22));
			GridSummaryCategory.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.11));
			GridSummaryCategory.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.22));
			GridSummaryCategory.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.11));
			//FC:FINAL:RPU:#1389 - The size of grid was set in designer
			//GridSummaryCategory.HeightOriginal = GridSummaryCategory.RowHeight(0) * 3;
			GridSummaryBETEExempt.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.22));
			GridSummaryBETEExempt.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.11));
			GridSummaryBETEExempt.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.22));
			GridSummaryBETEExempt.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.11));
			GridSummaryBETEExempt.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.22));
			GridSummaryBETEExempt.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.11));
			//FC:FINAL:RPU:#1389 - The size of grid was set in designer
			//GridSummaryBETEExempt.HeightOriginal = GridSummaryBETEExempt.RowHeight(0) * 3;
		}

		private void ResizeAuditGrid()
		{
			int GridWidth;
			GridWidth = AuditGrid.WidthOriginal;
			AuditGrid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.05));
			AuditGrid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.19));
			AuditGrid.ColWidth(colSnum, FCConvert.ToInt32(GridWidth * 0.05));
			AuditGrid.ColWidth(colLoc, FCConvert.ToInt32(GridWidth * 0.16));
			AuditGrid.ColWidth(colValue, FCConvert.ToInt32(GridWidth * 0.13));
			AuditGrid.ColWidth(colExempt, FCConvert.ToInt32(GridWidth * 0.13));
			AuditGrid.ColWidth(colAssessment, FCConvert.ToInt32(GridWidth * 0.13));
			AuditGrid.ColWidth(colTax, FCConvert.ToInt32(GridWidth * 0.13));
		}

		private void ResizeTotalGrid()
		{
			int GridWidth;
			GridWidth = TotalGrid.WidthOriginal;
			TotalGrid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.32));
			TotalGrid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.13));
			TotalGrid.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.13));
			TotalGrid.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.13));
			TotalGrid.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.13));
			TotalGrid.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.13));
			//TotalGrid.HeightOriginal = TotalGrid.RowHeight(0) + 60;
			GridWidth = GridSummary.WidthOriginal;
			GridSummary.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.15));
			GridSummary.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.13));
			GridSummary.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.18));
			GridSummary.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.18));
			GridSummary.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.18));
			GridSummary.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.18));
			//FC:FINAL:RPU:#1389 - The size of grid was set in designer
			//GridSummary.HeightOriginal = GridSummary.RowHeight(0) * 2;
		}

		private bool ValuesDontAgree()
		{
			bool ValuesDontAgree = false;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL = "";
			// returns true if they choose to stop
			if (modGlobalVariables.Statics.boolShortPP)
			{
				// if short screen
				// check categories against total
				rsLoad.OpenRecordset("select * from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where deleted <> 1 and ppmaster.value <> (category1 + category2 + category3 + category4 + category5 + category6 + category7 + category8 + category9)", modGlobalVariables.strPPDatabase);
				if (!rsLoad.EndOfFile())
				{
					// If Val(rsLoad.Fields("totcount")) > 0 Then
					if (rsLoad.RecordCount() > 0)
					{
						MessageBox.Show("There are account(s) with totals that do not match the sum of the categories" + "\r\n" + "Running Check Database Structure in personal property may fix this", "Amount Descrepancies", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						rptBadPPAccounts.InstancePtr.Init(ref rsLoad, this.Modal);
						if (MessageBox.Show("Do you wish to continue?", "Data Discrepancy", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
						{
							ValuesDontAgree = true;
							return ValuesDontAgree;
						}
					}
				}
			}
			else
			{
				// if full assessing
				strSQL = "select * from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where deleted <> 1 and (ppmaster.value <> (category1 + category2 + category3 + category4 + category5 + category6 + category7 + category8 + category9) and orcode <> 'Y')";
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
				if (!rsLoad.EndOfFile())
				{
					if (rsLoad.RecordCount() > 0)
					{
						// If Val(rsLoad.Fields("totcount")) > 0 Then
						MessageBox.Show("There are account(s) with totals that do not match the sum of the categories" + "\r\n" + "The Commit to Billing process may not have been run after changes were made", "Amount Discrepancies", MessageBoxButtons.OK, MessageBoxIcon.Information);
						rptBadPPAccounts.InstancePtr.Init(ref rsLoad, this.Modal);
						if (MessageBox.Show("Do you wish to continue?", "Data Discrepancy", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
						{
							ValuesDontAgree = true;
							return ValuesDontAgree;
						}
					}
				}
			}
			return ValuesDontAgree;
		}

		private bool FillAuditGrid()
		{
			bool FillAuditGrid = false;
			// VB6 Bad Scope Dim:
			int intCurFundRec = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(short, Decimal)
			double dblTemp = 0;
			double[] dblWeights = new double[4 + 1];
			double[] dblTempDue = new double[4 + 1];
			bool boolAllEqual = false;
			clsDRWrapper clsAudit = new clsDRWrapper();
			int lngValue = 0;
			int lngExempt = 0;
			int lngAssess = 0;
			double dblTax = 0;
			int x;
			int y;
			bool boolAlreadyAsked;
			bool boolOVAll = false;
			bool boolNewAll = false;
			bool boolNewBill = false;
			bool boolWriteBill = false;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp = 0;
			double dblTempTax = 0;
			double dblTaxperPeriod = 0;
			int intNumPeriods = 0;
			// vbPorter upgrade warning: dblLeftOver As double	OnWrite(Decimal, double, short)
			double dblLeftOver = 0;
			clsDRWrapper clsBill = new clsDRWrapper();
			int intResponse = 0;
			string strWhere = "";
			string strWhere2 = "";
			string strNameWhere;
			clsDRWrapper clsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: categorytotal As int	OnWrite(short, double)
			int[] categorytotal = new int[9 + 1];
			double[] CategoryBETETotal = new double[9 + 1];
			double[] CategoryBETEVal = new double[9 + 1];
			int[] CategoryVal = new int[9 + 1];
			// vbPorter upgrade warning: dblTotalTax As double	OnReadFCConvert.ToDecimal(
			double dblTotalTax = 0;
			//FileSystemObject fso = new FileSystemObject();
			string strSQLFix = "";
			bool boolCancelBill = false;
			double dblDiscount = 0;
			bool boolApplyDiscount = false;
			double dblReturnAmount = 0;
			int lngRateToReplace = 0;
			bool boolSkipAll;
			string strTemp = "";
			string strTemp2 = "";
			cParty tParty;
			// vbPorter upgrade warning: tAddr As cPartyAddress	OnWrite(cPartyAddress, Collection)
			cPartyAddress tAddr;
			try
			{
				// On Error GoTo ErrorHandler
				FillAuditGrid = true;
				if (ValuesDontAgree())
				{
					FillAuditGrid = false;
					return FillAuditGrid;
				}
				strNameWhere = "";
				switch (intWhich)
				{
					case -1:
						{
							// all
							strWhere = "";
							strWhere2 = "";
							strTemp = "All";
							strTemp2 = "";
							break;
						}
					case 0:
						{
							strWhere = " and account between " + strBegin + " and " + strEnd;
							strWhere2 = " and ppmaster.account between " + strBegin + " and " + strEnd;
							strTemp = " range of Account";
							strTemp2 = "between " + strBegin + " and " + strEnd;
							break;
						}
					case 1:
						{
							// strWhere = " and FullNameLF between '" & strBegin & "' and '" & strEnd & "'"
							// strWhere2 = strWhere
							strNameWhere = " and lastname + firstname between '" + strBegin + "' and '" + strEnd + "'";
							strTemp = " range of Name";
							strTemp2 = "between " + strBegin + " and " + strEnd;
							break;
						}
					case 2:
						{
							strWhere = " and street between '" + strBegin + "' and '" + strEnd + "'";
							strWhere2 = strWhere;
							strTemp = " range of Street";
							strTemp2 = "between " + strBegin + " and " + strEnd;
							break;
						}
					case 3:
						{
							strWhere = " and open1 between '" + strBegin + "' and '" + strEnd + "'";
							strWhere2 = strWhere;
							strTemp = " range of Open 1";
							strTemp2 = "between " + strBegin + " and " + strEnd;
							break;
						}
					case 4:
						{
							strWhere = " and open2 between '" + strBegin + "' and '" + strEnd + "'";
							strWhere2 = strWhere;
							strTemp = " range of Open 2";
							strTemp2 = "between " + strBegin + " and " + strEnd;
							break;
						}
				}
				//end switch
				if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					strWhere += " and trancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
				}
				if (intWhereFrom == 4)
				{
					modGlobalFunctions.AddCYAEntry_6("CL", "Created PP bills", "By " + strTemp, strTemp2, "Rate Key " + FCConvert.ToString(clsRateInfo.RateKey));
					lngRateToReplace = 0;
					clsAudit.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
					if (FCConvert.ToBoolean(clsAudit.Get_Fields_Boolean("applydiscounts")))
					{
						boolApplyDiscount = true;
						clsAudit.OpenRecordset("select top 1 * from pendingdiscount", "twcl0000.vb1");
						if (!clsAudit.EndOfFile())
						{
							if (MessageBox.Show("Unfinalized pending discounts found. Add to these?" + "\r\n" + "Answering no will clear the previous pending discounts.", "Pending Discounts Found", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
							{
								clsAudit.Execute("delete from pendingdiscount", "twcl0000.vb1");
								modGlobalFunctions.AddCYAEntry_26("CL", "Deleted pending discounts", "Bill Year " + FCConvert.ToString(clsRateInfo.TaxYear));
							}
						}
					}
					else
					{
						boolApplyDiscount = false;
					}
					// Call clsAudit.OpenRecordset("select ratekey from (select BILLINGMASTER.* from " & clsAudit.CurrentPrefix & "PersonalProperty.dbo.ppmaster inner join (billingmaster inner join RATEREC on (billingmaster.ratekey = raterec.ID) ) on (billingmaster.account = ppmaster.account) CROSS APPLY " & clsAudit.CurrentPrefix & "CentralParties.dbo.GetCentralPartyName(PartyID) as p where billingmaster.ratekey > 0 and ratetype = 'R' and billingyear between " & clsRateInfo.TaxYear & "0 and " & clsRateInfo.TaxYear & "9 AND billingmaster.billingtype = 'PP' " & strWhere2 & " ) as temp group by ratekey", "twcl0000.vb1")
					if (strNameWhere == "")
					{
						clsAudit.OpenRecordset("select ratekey from (select BILLINGMASTER.* from " + clsAudit.CurrentPrefix + "PersonalProperty.dbo.ppmaster inner join (billingmaster inner join RATEREC on (billingmaster.ratekey = raterec.ID) ) on (billingmaster.account = ppmaster.account)  where billingmaster.ratekey > 0 and ratetype = 'R' and billingyear between " + FCConvert.ToString(clsRateInfo.TaxYear) + "0 and " + FCConvert.ToString(clsRateInfo.TaxYear) + "9 AND billingmaster.billingtype = 'PP' " + strWhere2 + " ) as temp group by ratekey", "twcl0000.vb1");
					}
					else
					{
						clsAudit.OpenRecordset("select ratekey from (select BILLINGMASTER.* from " + clsAudit.CurrentPrefix + "PersonalProperty.dbo.ppmaster inner join (billingmaster inner join RATEREC on (billingmaster.ratekey = raterec.ID) ) on (billingmaster.account = ppmaster.account) inner join " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties on (" + clsAudit.CurrentPrefix + "PersonalProperty.dbo.ppmaster.partyid = " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties.id) where billingmaster.ratekey > 0 and ratetype = 'R' and billingyear between " + FCConvert.ToString(clsRateInfo.TaxYear) + "0 and " + FCConvert.ToString(clsRateInfo.TaxYear) + "9 AND billingmaster.billingtype = 'PP' " + strWhere2 + " " + strNameWhere + " ) as temp group by ratekey", "twcl0000.vb1");
					}
					if (!clsAudit.EndOfFile())
					{
						// there are already some bill records out there
						if (clsAudit.FindFirstRecord("ratekey", clsRateInfo.RateKey))
						{
							// already some of the same rate key
							// must overwrite,skip or cancel
							//FC:FINAL:CHN - Incorrect converting default values from VB6.
							// intResponse = frmGetOptions.InstancePtr.Init(1, "Bills Already Exist", "There are already bills with other rate key(s) with values for this tax year", "Skip these bills", "Overwrite these bills", "Create New Bills", "Cancel");
							intResponse = frmGetOptions.InstancePtr.Init(1, "Bills Already Exist", "There are already bills with other rate key(s) with values for this tax year", "Skip these bills", "", "Overwrite these bills", "", "Cancel");
							switch (intResponse)
							{
								case -1:
									{
										// cancel
										Close();
										return FillAuditGrid;
									}
								case 1:
									{
										// skip
										boolSkipAll = true;
										boolOVAll = false;
										boolNewAll = false;
										break;
									}
								case 3:
									{
										// overwrite
										boolOVAll = true;
										boolSkipAll = false;
										boolNewAll = false;
										lngRateToReplace = clsRateInfo.RateKey;
										modGlobalFunctions.AddCYAEntry_242("CL", "OV Bills", "Billing overwrote PP bills for year " + Strings.Left(FCConvert.ToString(clsRateInfo.TaxYear), 4), "Rate " + FCConvert.ToString(lngRateToReplace), "With rate " + FCConvert.ToString(lngRateToReplace));
										// If Not File.Exists("backup\bkcl" & Format(Month(Date), "00") & Format(Day(Date), "00") & ".WBL") Then
										// intResponse = MsgBox("You are about to overwrite existing bill records." & vbNewLine & "It is recommended that you create a backup of your current data before proceeding." & vbNewLine & "Would you like a back up made now?", vbQuestion + vbYesNoCancel, "Backup Data?")
										// Select Case intResponse
										// Case vbYes
										// If Not Directory.Exists("Backup") Then
										// fso.CreateFolder ("Backup")
										// End If
										// Call File.Copy("twcl0000.vb1", "backup\BKCL" & Format(Month(Date), "00") & Format(Day(Date), "00") & ".WBL", True)
										// Case vbNo
										// Call AddCYAEntry("CL", "OV BIlls", "Overwriting PP bills", "Declined to create backup")
										// Case vbCancel
										// Unload Me
										// Exit Function
										// End Select
										// End If
										break;
									}
								case 5:
									{
										// cancel
										Close();
										return FillAuditGrid;
									}
							}
							//end switch
						}
						else
						{
							// overwrite,add new, skip or cancel
							// if overwrite and more than one, must pick the ratekey to overwrite
							intResponse = frmGetOptions.InstancePtr.Init(1, "Bills Already Exist", "There are already bills with other rate key(s) with values for this tax year", "Skip these bills", "Overwrite these bills", "Create New Bills", "Cancel");
							switch (intResponse)
							{
								case -1:
									{
										// cancel
										Close();
										return FillAuditGrid;
									}
								case 1:
									{
										// skip
										boolSkipAll = true;
										boolOVAll = false;
										boolNewAll = false;
										break;
									}
								case 2:
									{
										// overwrite
										boolSkipAll = false;
										boolOVAll = true;
										boolNewAll = false;
										break;
									}
								case 3:
									{
										// add new
										boolSkipAll = false;
										boolOVAll = false;
										boolNewAll = true;
										modGlobalFunctions.AddCYAEntry_80("CL", "New Bills", "Billing made 2nd etc bills for year " + Strings.Left(FCConvert.ToString(clsRateInfo.TaxYear), 4), "Rate Key " + FCConvert.ToString(clsRateInfo.RateKey));
										break;
									}
								case 4:
									{
										// cancel
										Close();
										return FillAuditGrid;
									}
							}
							//end switch
							if (boolOVAll)
							{
								// check if there is more than one rate rec
								// if so, make them choose the one to overwrite
								if (clsAudit.RecordCount() > 1)
								{
									strTemp = frmPickRateToPrint.InstancePtr.Init(clsRateInfo.TaxYear, true, false, false, "PP", "Overwrite bills that use which rate record?");
									if (strTemp == "CANCEL" || strTemp == string.Empty)
									{
										Close();
										return FillAuditGrid;
									}
									// returns string of ratekey = x
									strTemp = Strings.Trim(Strings.Mid(strTemp, Strings.InStr(1, strTemp, "=", CompareConstants.vbTextCompare) + 1));
									// everything after the =
									lngRateToReplace = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
								}
								else
								{
									lngRateToReplace = FCConvert.ToInt32(clsAudit.Get_Fields_Int32("ratekey"));
								}
								modGlobalFunctions.AddCYAEntry_242("CL", "OV Bills", "Billing overwrote PP bills for year " + Strings.Left(FCConvert.ToString(clsRateInfo.TaxYear), 4), "Rate " + FCConvert.ToString(clsRateInfo.RateKey), "With rate " + FCConvert.ToString(lngRateToReplace));
								// If Not File.Exists("backup\bkcl" & Format(Month(Date), "00") & Format(Day(Date), "00") & ".WBL") Then
								// intResponse = MsgBox("You are about to overwrite existing bill records." & vbNewLine & "It is recommended that you create a backup of your current data before proceeding." & vbNewLine & "Would you like a back up made now?", vbQuestion + vbYesNoCancel, "Backup Data?")
								// Select Case intResponse
								// Case vbYes
								// If Not Directory.Exists("Backup") Then
								// fso.CreateFolder ("Backup")
								// End If
								// Call File.Copy("twcl0000.vb1", "backup\BKCL" & Format(Month(Date), "00") & Format(Day(Date), "00") & ".WBL", True)
								// Case vbNo
								// Call AddCYAEntry("CL", "OV BIlls", "Overwriting PP bills", "Declined to create backup")
								// Case vbCancel
								// Unload Me
								// Exit Function
								// End Select
								// End If
							}
						}
					}
				}
				//! Load frmWait;
				if (intWhereFrom == 4)
				{
					frmWait.InstancePtr.Init("Please wait while information is transferred.");
					// frmWait.lblMessage.Caption = "Please wait while information is transferred."
				}
				else
				{
					frmWait.InstancePtr.Init("Please wait while the Audit is being loaded.");
					// frmWait.lblMessage.Caption = "Please wait while the Audit is being loaded."
				}
				for (x = 1; x <= 9; x++)
				{
					categorytotal[x] = 0;
				}
				// x
				if (intWhereFrom == 4)
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolZeroLiabilityOption)
					{
						if (strNameWhere == "")
						{
							clsAudit.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey))  where (ppmaster.value - isnull(ppmaster.exemption, 0) > 0) and ppmaster.deleted <> 1  and ppmaster.account > 0 " + strWhere + " order by ppmaster.account", "twpp0000.vb1");
						}
						else
						{
							clsAudit.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey)) inner join " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties on (" + clsAudit.CurrentPrefix + "PersonalProperty.dbo.ppmaster.partyid = " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties.id) where (ppmaster.value - isnull(ppmaster.exemption, 0) > 0) and ppmaster.deleted <> 1  and ppmaster.account > 0 " + strWhere + " " + strNameWhere + " order by ppmaster.account", "twpp0000.vb1");
						}
						strSQLFix = "update billingmaster set ratekey = 0,taxdue1 = 0,taxdue2 = 0,taxdue3 = 0,taxdue4 = 0,transferfrombillingdatefirst = 0,transferfrombillingdatelast = 0";
						strSQLFix += ",category1 = 0,category2 = 0,category3 = 0,category4 = 0,category5 = 0,category6 = 0,category7 = 0,category8 = 0,category9 = 0,ppassessment = 0,otherexempt1 = 0,otherexempt2 = 0,exempt1 = 0,exempt2 = 0,exempt3 = 0";
						strSQLFix += " where billingtype = 'PP' and billingyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and account in ";
						if (strNameWhere == "")
						{
							strSQLFix += "(select account from " + clsTemp.CurrentPrefix + "PersonalProperty.dbo.ppmaster  where ppmaster.value - ppmaster.exemption <= 0 and deleted <> 1 and account > 0 " + strWhere + ")";
						}
						else
						{
							strSQLFix += "(select account from " + clsTemp.CurrentPrefix + "PersonalProperty.dbo.ppmaster inner join " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties on (" + clsAudit.CurrentPrefix + "PersonalProperty.dbo.ppmaster.partyid = " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties.id) where ppmaster.value - ppmaster.exemption <= 0 and deleted <> 1 and account > 0 " + strWhere + " " + strNameWhere + ")";
						}
						clsTemp.Execute(strSQLFix, "twcl0000.vb1");
						strSQLFix = "delete from billingmaster where billingtype = 'PP' and billingyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and taxdue1 = 0 and taxdue2 = 0 and taxdue3 = 0 and taxdue4 = 0 and principalpaid = 0 and interestpaid = 0";
						clsTemp.Execute(strSQLFix, "twcl0000.vb1");
					}
					else
					{
						if (strNameWhere == "")
						{
							clsAudit.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey)) where  ppmaster.deleted <> 1  and ppmaster.account > 0 " + strWhere + " order by ppmaster.account", "twpp0000.vb1");
						}
						else
						{
							clsAudit.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey)) inner join " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties on (" + clsAudit.CurrentPrefix + "PersonalProperty.dbo.ppmaster.partyid = " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties.id) where  ppmaster.deleted <> 1  and ppmaster.account > 0 " + strWhere + " " + strNameWhere + " order by ppmaster.account", "twpp0000.vb1");
						}
					}
				}
				else
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolZeroLiabilityOption)
					{
						if (strNameWhere == "")
						{
							clsAudit.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey))  where (ppmaster.value - isnull(ppmaster.exemption, 0) > 0) and ppmaster.deleted <> 1  and ppmaster.account > 0 " + strWhere + " order by ppmaster.account", "twpp0000.vb1");
						}
						else
						{
							clsAudit.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey)) inner join " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties on (" + clsAudit.CurrentPrefix + "PersonalProperty.dbo.ppmaster.partyid = " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties.id) where (ppmaster.value - isnull(ppmaster.exemption, 0) > 0) and ppmaster.deleted <> 1  and ppmaster.account > 0 " + strWhere + " " + strNameWhere + " order by ppmaster.account", "twpp0000.vb1");
						}
					}
					else
					{
						if (strNameWhere == "")
						{
							clsAudit.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey))  where ppmaster.deleted <> 1  and ppmaster.account > 0 " + strWhere + " order by ppmaster.account", "twpp0000.vb1");
						}
						else
						{
							clsAudit.OpenRecordset("select * from (ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey)) inner join " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties on (" + clsAudit.CurrentPrefix + "PersonalProperty.dbo.ppmaster.partyid = " + clsAudit.CurrentPrefix + "CentralParties.dbo.parties.id) where ppmaster.deleted <> 1  and ppmaster.account > 0 " + strWhere + " order by ppmaster.account", "twpp0000.vb1");
						}
					}
				}
				clsAudit.MoveLast();
				clsAudit.MoveFirst();
				//FC:FINAL:AM:#1298 - in VB6 the form is loaded when a property is accessed
				this.LoadForm();
				TotalGrid.TextMatrix(0, 1, "Count " + FCConvert.ToString(clsAudit.RecordCount()));
				GridSummary.TextMatrix(1, 1, FCConvert.ToString(clsAudit.RecordCount()));
				//Application.DoEvents();
				lngTotvalue = 0;
				lngTotExempt = 0;
				lngTotAssess = 0;
				dblTotTax = 0;
				boolAlreadyAsked = false;
				// boolOVAll = False
				// boolNewAll = False
				if (intWhereFrom == 4)
				{
					clsBill.Execute("update collections set lastbilledyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1", "twcl0000.vb1");
					if (lngRateToReplace > 0)
					{
						// get any with this ratekey or a dash one with only pre-payments on it
						clsBill.OpenRecordset("select * from billingmaster where billingtype = 'PP' and ((billingyear between " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and " + FCConvert.ToString(clsRateInfo.TaxYear) + "9 and ratekey = " + FCConvert.ToString(lngRateToReplace) + ") or (billingyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and (taxdue1 + taxdue2 + taxdue3 + taxdue4) <= 0 and ratekey = 0))", "twcl0000.vb1");
					}
					else
					{
						// no dash ones with tax due exist
						clsBill.OpenRecordset("select * from billingmaster where billingyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and billingtype = 'PP' order by account", "twcl0000.vb1");
					}
				}
				while (!clsAudit.EndOfFile())
				{
					//Application.DoEvents();
					// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
					lngValue = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAudit.Get_Fields("value"))));
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAudit.Get_Fields_Int32("exemption"))));
					// + Val(clsAudit.Fields("cat1exempt")) + Val(clsAudit.Fields("cat2exempt")) + Val(clsAudit.Fields("cat3exempt")) + Val(clsAudit.Fields("cat4exempt")) + Val(clsAudit.Fields("cat5exempt")) + Val(clsAudit.Fields("cat6exempt")) + Val(clsAudit.Fields("cat7exempt")) + Val(clsAudit.Fields("cat8exempt")) + Val(clsAudit.Fields("cat9exempt"))
					lngAssess = lngValue - lngExempt;
					dblTax = modMain.Round(lngAssess * clsRateInfo.TaxRate, 2);
					dblTax = modMain.Round(dblTax, 2);
					if (intWhereFrom != 4)
					{
						lngTotvalue += lngValue;
						lngTotExempt += lngExempt;
						lngTotAssess += lngAssess;
						dblTotTax += dblTax;
						for (x = 1; x <= 9; x++)
						{
							// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
							categorytotal[x] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields("category" + FCConvert.ToString(x))));
							// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
							CategoryVal[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAudit.Get_Fields("category" + FCConvert.ToString(x)))));
							// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
							CategoryBETEVal[x] += Conversion.Val(clsAudit.Get_Fields("cat" + FCConvert.ToString(x) + "exempt"));
							// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
							CategoryBETETotal[x] += Conversion.Val(clsAudit.Get_Fields("cat" + FCConvert.ToString(x) + "exempt"));
						}
						// x
					}
					tParty = tPCont.GetParty(clsAudit.Get_Fields_Int32("partyid"));
					if (tParty == null)
					{
						tParty = new cParty();
					}
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [STREETNUMBER] and replace with corresponding Get_Field method
					AuditGrid.AddItem(clsAudit.Get_Fields("account") + "\t" + tParty.FullNameLastFirst + "\t" + clsAudit.Get_Fields("STREETNUMBER") + "\t" + Strings.Trim(FCConvert.ToString(clsAudit.Get_Fields_String("street"))) + "\t" + Strings.Format(lngValue, "#,###,###,##0") + "\t" + Strings.Format(lngExempt, "#,###,###,##0") + "\t" + Strings.Format(lngAssess, "#,###,###,##0") + "\t" + Strings.Format(dblTax, "#,###,###,##0.00") + "\t" + FCConvert.ToString(CategoryVal[1]) + "\t" + FCConvert.ToString(CategoryVal[2]) + "\t" + FCConvert.ToString(CategoryVal[3]) + "\t" + FCConvert.ToString(CategoryVal[4]) + "\t" + FCConvert.ToString(CategoryVal[5]) + "\t" + FCConvert.ToString(CategoryVal[6]) + "\t" + FCConvert.ToString(CategoryVal[7]) + "\t" + FCConvert.ToString(CategoryVal[8]) + "\t" + FCConvert.ToString(CategoryVal[9]) + "\t" + FCConvert.ToString(CategoryBETEVal[1]) + "\t" + FCConvert.ToString(CategoryBETEVal[2]) + "\t" + FCConvert.ToString(CategoryBETEVal[3]) + "\t" + FCConvert.ToString(CategoryBETEVal[4]) + "\t" + FCConvert.ToString(CategoryBETEVal[5]) + "\t" + FCConvert.ToString(CategoryBETEVal[6]) + "\t" + FCConvert.ToString(CategoryBETEVal[7]) + "\t" + FCConvert.ToString(CategoryBETEVal[8]) + "\t" + FCConvert.ToString(CategoryBETEVal[9]));
					if (intWhereFrom == 4)
					{
						// We aren't doing an audit, we're building billing records
						boolWriteBill = false;
						boolNewBill = false;
						boolCancelBill = false;
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						clsBill.FindFirstRecord("account", clsAudit.Get_Fields("account"));
						if (!clsBill.NoMatch)
						{
							// make sure we aren't overwriting some bills
							if (clsBill.Get_Fields_Int32("RateKey") > 0 || (Conversion.Val(clsBill.Get_Fields_Decimal("taxdue1")) > 0) || (Conversion.Val(clsBill.Get_Fields_Decimal("taxdue2")) > 0) || (Conversion.Val(clsBill.Get_Fields_Decimal("taxdue3")) > 0) || (Conversion.Val(clsBill.Get_Fields_Decimal("taxdue4")) > 0))
							{
								if (boolOVAll)
								{
									boolWriteBill = true;
									boolNewBill = false;
								}
								else if (boolNewAll)
								{
									boolNewBill = true;
									boolWriteBill = true;
								}
								else
								{
									// skip all
									boolWriteBill = false;
								}
								// If Not (boolOVAll Or boolNewAll) Then
								// Unload frmWait
								// DoEvents
								// 
								// intResponse = frmGetOptions.init(1, "Bill Already Exists", "There is already a bill for account " & clsAudit.Fields("Account") & " with values for this tax year.", "Skip this bill", , "Overwrite this bill", , "Create a new bill")
								// Select Case intResponse
								// Case 1
								// skip
								// boolWriteBill = False
								// boolNewBill = False
								// Case 3
								// overwrite
								// If Not boolAlreadyAsked Then
								// Call AddCYAEntry("CL", "OV Bills", "Billing overwrote PP bills for year " & Left(clsRateInfo.TaxYear, 4))
								// intResponse = MsgBox("Do you want to overwrite all existing bills?" & vbNewLine & "If you choose no, for each existing bill you will be asked what action to take.", vbYesNo + vbQuestion, "Overwrite All Bills?")
								// If intResponse = vbYes Then boolOVAll = True
								// boolAlreadyAsked = True
								// End If   'if not boolalreadyasked
								// boolWriteBill = True
								// boolNewBill = False
								// Case 5
								// create -2 or -3 etc.
								// If Not boolAlreadyAsked Then
								// Call AddCYAEntry("CL", "New Bills", "Billing made 2nd etc bills for year " & Left(clsRateInfo.TaxYear, 4))
								// intResponse = MsgBox("Do you want to create new bills for all existing bills?" & vbNewLine & "If you choose no, for each existing bill you will be asked what action to take.", vbYesNo + vbQuestion, "Create New Bills?")
								// If intResponse = vbYes Then boolNewAll = True
								// boolAlreadyAsked = True
								// End If
								// boolNewBill = True
								// boolWriteBill = True
								// Case -1
								// cancelled
								// MsgBox "Bill Transfer interrupted.", vbExclamation, "Transfer Interrupted"
								// Unload Me
								// Exit Sub
								// End Select
								// 
								// 
								// 
								// intResponse = MsgBox("There is already a bill for account " & clsBill.Fields("account") & " with values for this tax year." & vbNewLine & "Do you want to overwrite this bill?" & vbNewLine & "Yes - Overwrite" & vbNewLine & "No - Don't overwrite, but continue" & vbNewLine & "Cancel - Cancel the bill making process.", vbCritical + vbYesNoCancel)
								// If intResponse = vbYes Then
								// 
								// If Not boolAlreadyAsked Then
								// Call clsTemp.Execute("insert into CYATable (user,actionDATE,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'OV Bills','Tax Billing overwrote PP bills for tax year " & Left(clsRateInfo.TaxYear, 4) & "')", "twbl0000.vb1")
								// Call AddCYAEntry("CL", "OV Bills", "Billing overwrote PP bills for year " & Left(clsRateInfo.TaxYear, 4))
								// intResponse = MsgBox("Do you want to overwrite all existing bills?" & vbNewLine & "If you choose NO you will be asked if you want to overwrite for each existing bill.", vbYesNo + vbQuestion, "Overwrite Bills?")
								// If intResponse = vbYes Then boolOVAll = True
								// boolAlreadyAsked = True
								// End If   'if not boolalreadyasked
								// boolWriteBill = True
								// ElseIf intResponse = vbNo Then
								// don't do anything special
								// boolWriteBill = False
								// Else
								// cancel bill writing
								// MsgBox "Bill Transfer interrupted.", vbExclamation, "Transfer Interrupted"
								// Unload Me
								// Exit Sub
								// End If      'if intresponse = vbyes
								// Load frmWait
								// If intWhereFrom <> 4 Then
								// frmWait.init ("Please wait while the Audit is being loaded.")
								// Else
								// frmWait.init ("Please wait while the information is transferred.")
								// End If
								// frmWait.lblMessage.Caption = "Please wait while the Audit is being loaded."
								// DoEvents
								// Else
								// boolWriteBill = True
								// If boolNewAll Then boolNewBill = True
								// End If      'if not boolovall
							}
							else
							{
								boolWriteBill = true;
								boolNewBill = false;
							}
							// if taxdue1,2,3,4 > 0
							if (boolWriteBill)
							{
								if (!boolNewBill)
								{
									clsBill.Edit();
								}
								else
								{
									clsBill.AddNew();
									clsBill.Update();
								}
							}
						}
						else
						{
							boolWriteBill = true;
							boolNewBill = true;
							clsBill.AddNew();
							clsBill.Update();
						}
						// if not nomatch
						if (boolWriteBill)
						{
							lngTotvalue += lngValue;
							lngTotExempt += lngExempt;
							lngTotAssess += lngAssess;
							dblTotTax += dblTax;
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							clsBill.Set_Fields("account", clsAudit.Get_Fields("account"));
							// pp account Number
							clsBill.Set_Fields("billingtype", "PP");
							// PP bill
							if (!boolNewBill)
							{
								// clsBill.Fields("billingyear") = clsRateInfo.TaxYear & "1"
							}
							else
							{
								// determine what one this is
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsTemp.OpenRecordset("select max(billingyear) as maxyear from billingmaster where account = " + clsAudit.Get_Fields("account") + " and billingtype = 'PP' and billingyear between " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and " + FCConvert.ToString(clsRateInfo.TaxYear) + "9", modGlobalVariables.strCLDatabase);
								if (!clsTemp.EndOfFile())
								{
									// TODO Get_Fields: Field [maxyear] not found!! (maybe it is an alias?)
									lngTemp = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxyear")) - (clsRateInfo.TaxYear * 10));
									if (lngTemp < 1)
									{
										clsBill.Set_Fields("billingyear", FCConvert.ToString(clsRateInfo.TaxYear) + "1");
									}
									else
									{
										if (lngTemp < 9)
										{
											clsBill.Set_Fields("billingyear", FCConvert.ToString(clsRateInfo.TaxYear) + FCConvert.ToString(lngTemp + 1));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
											MessageBox.Show("There is already the max number of bills for account " + clsAudit.Get_Fields("account") + "." + "\r\n" + "Cannot create a bill for this account");
											modGlobalFunctions.AddCYAEntry_26("CL", "Max Bills for " + AuditGrid.TextMatrix(x, 0), "unable to make new bill");
											boolCancelBill = true;
										}
									}
								}
								else
								{
									clsBill.Set_Fields("billingyear", FCConvert.ToString(clsRateInfo.TaxYear) + "1");
								}
								clsBill.Set_Fields("interestappliedthroughdate", DateAndTime.DateAdd("d", -1, clsRateInfo.Get_InterestStartDate(1)));
							}
							// clsBill.Fields("billingyear") = clsRateInfo.TaxYear & "1"
							if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
							{
								clsBill.Set_Fields("trancode", modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
							}
							clsBill.Set_Fields("name1", tParty.FullNameLastFirst);
							if (tParty.Addresses.Count > 0)
							{
								tAddr = tParty.GetPrimaryAddress();
								if (tAddr == null)
								{
									tAddr = tParty.Addresses[1];
								}
								clsBill.Set_Fields("address1", tAddr.Address1);
								clsBill.Set_Fields("address2", tAddr.Address2);
								clsBill.Set_Fields("address3", tAddr.City + "  " + tAddr.State + " " + tAddr.Zip);
								clsBill.Set_Fields("MailingAddress3", tAddr.Address3);
								clsBill.Set_Fields("zip", tAddr.Zip);
							}
							else
							{
								clsBill.Set_Fields("address1", "");
								clsBill.Set_Fields("address2", "");
								clsBill.Set_Fields("address3", "");
								clsBill.Set_Fields("MailingAddress3", "");
								clsBill.Set_Fields("zip", "");
							}
							// TODO Get_Fields: Check the table for the column [STREETNUMBER] and replace with corresponding Get_Field method
							clsBill.Set_Fields("streetnumber", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("STREETNUMBER"))));
							clsBill.Set_Fields("apt", "");
							clsBill.Set_Fields("streetname", Strings.Trim(FCConvert.ToString(clsAudit.Get_Fields_String("street"))));
							// clsBill.Fields("PPAssessment") = Val(clsAudit.Fields("[value]"))
							// clsBill.Fields("exemptvalue") = Val(clsAudit.Fields("EXEMPTION"))
							// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
							clsBill.Set_Fields("ppassessment", Conversion.Val(clsAudit.Get_Fields("value")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat1exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat2exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat3exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat4exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat5exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat6exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat7exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat8exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat9exempt")));
							clsBill.Set_Fields("exemptvalue", Conversion.Val(clsAudit.Get_Fields_Int32("exemption")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat1exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat2exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat3exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat4exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat5exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat6exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat7exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat8exempt")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat9exempt")));
							clsBill.Set_Fields("whetherbilledbefore", "A");
							if (fecherFoundation.FCUtils.IsEmptyDateTime(clsBill.Get_Fields_DateTime("transferfrombillingdatefirst")))
							{
								clsBill.Set_Fields("transferfrombillingdatefirst", DateTime.Now);
							}
							else
							{
								if (FCConvert.ToDateTime(clsBill.Get_Fields_DateTime("transferfrombillingdatefirst") as object).ToOADate() == 0)
								{
									clsBill.Set_Fields("transferfrombillingdatefirst", DateTime.Now);
								}
								else
								{
									clsBill.Set_Fields("transferfrombillingdatelast", DateTime.Now);
								}
							}
							clsBill.Set_Fields("taxdue1", 0);
							clsBill.Set_Fields("taxdue2", 0);
							clsBill.Set_Fields("taxdue3", 0);
							clsBill.Set_Fields("taxdue4", 0);
							dblTempDue[0] = 0;
							dblTempDue[1] = 0;
							dblTempDue[2] = 0;
							dblTempDue[3] = 0;
							dblTempTax = dblTax;
							dblTotalTax += dblTax;
							intNumPeriods = clsRateInfo.NumberOfPeriods;
							dblTaxperPeriod = dblTempTax / intNumPeriods;
							dblTaxperPeriod = modMain.Round(dblTaxperPeriod, 2);
							if (dblTempTax >= 100 || intNumPeriods < 4 || modGlobalVariables.Statics.CustomizedInfo.SplitSmallAmounts)
							{
								boolAllEqual = true;
								for (y = 1; y <= intNumPeriods; y++)
								{
									if (clsRateInfo.Get_BillWeight(y) != clsRateInfo.Get_BillWeight(1))
									{
										boolAllEqual = false;
									}
								}
								// y
								if (boolAllEqual)
								{
									for (y = 1; y <= intNumPeriods; y++)
									{
										clsBill.Set_Fields("taxdue" + y, FCConvert.ToDecimal(dblTaxperPeriod));
									}
									// y
									dblLeftOver = FCConvert.ToDouble(FCConvert.ToDecimal(dblTempTax - (dblTaxperPeriod * intNumPeriods)));
									if (dblLeftOver < 0)
									{
										// make the last bill the smaller
										clsBill.Set_Fields("taxdue" + intNumPeriods, clsBill.Get_Fields_Decimal("taxdue" + FCConvert.ToString(intNumPeriods)) + FCConvert.ToDecimal(dblLeftOver));
									}
									else
									{
										// make the first bill the largest
										clsBill.Set_Fields("taxdue1", clsBill.Get_Fields_Decimal("taxdue1") + FCConvert.ToDecimal(dblLeftOver));
									}
									// if dblleftover
								}
								else
								{
									dblTemp = 0;
									dblLeftOver = dblTempTax;
									for (y = 1; y <= intNumPeriods - 1; y++)
									{
										if (dblLeftOver > 0)
										{
											dblTemp = FCConvert.ToDouble(FCConvert.ToDecimal(clsRateInfo.Get_BillWeight(y) * dblTempTax));
											if (dblTemp <= dblLeftOver)
											{
												dblTempDue[y - 1] = dblTemp;
												dblLeftOver -= dblTemp;
											}
											else
											{
												dblTempDue[y - 1] = dblLeftOver;
												dblLeftOver = 0;
											}
										}
									}
									// y
									if (dblLeftOver > 0)
									{
										dblTempDue[intNumPeriods - 1] = dblLeftOver;
									}
									for (y = 1; y <= 4; y++)
									{
										clsBill.Set_Fields("TaxDue" + y, dblTempDue[y - 1]);
									}
									// y
								}
							}
							else
							{
								clsBill.Set_Fields("taxdue1", dblTempTax);
							}
							clsBill.Set_Fields("ratekey", clsRateInfo.RateKey);
							clsBill.Set_Fields("category1", Conversion.Val(clsAudit.Get_Fields_Int32("category1")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat1exempt")));
							categorytotal[1] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category1")));
							// + Val(clsAudit.Fields("cat1exempt"))
							CategoryBETETotal[1] += Conversion.Val(clsAudit.Get_Fields_Int32("cat1exempt"));
							clsBill.Set_Fields("category2", Conversion.Val(clsAudit.Get_Fields_Int32("category2")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat2exempt")));
							categorytotal[2] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category2")));
							// + Val(clsAudit.Fields("cat2exempt"))
							CategoryBETETotal[2] += Conversion.Val(clsAudit.Get_Fields_Int32("cat2exempt"));
							clsBill.Set_Fields("category3", Conversion.Val(clsAudit.Get_Fields_Int32("category3")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat3exempt")));
							categorytotal[3] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category3")));
							// + Val(clsAudit.Fields("cat3exempt"))
							CategoryBETETotal[3] += Conversion.Val(clsAudit.Get_Fields_Int32("cat3exempt"));
							clsBill.Set_Fields("category4", Conversion.Val(clsAudit.Get_Fields_Int32("category4")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat4exempt")));
							categorytotal[4] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category4")));
							// + Val(clsAudit.Fields("cat4exempt"))
							CategoryBETETotal[4] += Conversion.Val(clsAudit.Get_Fields_Int32("cat4exempt"));
							clsBill.Set_Fields("category5", Conversion.Val(clsAudit.Get_Fields_Int32("category5")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat5exempt")));
							categorytotal[5] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category5")));
							// + Val(clsAudit.Fields("cat5exempt"))
							CategoryBETETotal[5] += Conversion.Val(clsAudit.Get_Fields_Int32("cat5exempt"));
							clsBill.Set_Fields("category6", Conversion.Val(clsAudit.Get_Fields_Int32("category6")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat6exempt")));
							categorytotal[6] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category6")));
							// + Val(clsAudit.Fields("cat6exempt"))
							CategoryBETETotal[6] += Conversion.Val(clsAudit.Get_Fields_Int32("cat6exempt"));
							clsBill.Set_Fields("category7", Conversion.Val(clsAudit.Get_Fields_Int32("category7")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat7exempt")));
							categorytotal[7] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category7")));
							// + Val(clsAudit.Fields("cat7exempt"))
							CategoryBETETotal[7] += Conversion.Val(clsAudit.Get_Fields_Int32("cat7exempt"));
							clsBill.Set_Fields("category8", Conversion.Val(clsAudit.Get_Fields_Int32("category8")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat8exempt")));
							categorytotal[8] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category8")));
							// + Val(clsAudit.Fields("cat8exempt"))
							CategoryBETETotal[8] += Conversion.Val(clsAudit.Get_Fields_Int32("cat8exempt"));
							clsBill.Set_Fields("category9", Conversion.Val(clsAudit.Get_Fields_Int32("category9")) + Conversion.Val(clsAudit.Get_Fields_Int32("cat9exempt")));
							categorytotal[9] += FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields_Int32("Category9")));
							// + Val(clsAudit.Fields("cat9exempt"))
							CategoryBETETotal[9] += Conversion.Val(clsAudit.Get_Fields_Int32("cat9exempt"));
							if (boolApplyDiscount)
							{
								clsTemp.OpenRecordset("select * from paymentrec where code = 'D' and billkey = " + clsBill.Get_Fields_Int32("ID"), "twcl0000.vb1");
								if (clsTemp.EndOfFile())
								{
									dblDiscount = 0;
									dblReturnAmount = 0;
									if (Conversion.Val(clsBill.Get_Fields_Decimal("principalpaid")) > 0)
									{
										// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
										if (modCLDiscount.PrePaymentsPlusDiscountFinishesAccount(FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields("account"))), FCConvert.ToInt32(Conversion.Val(clsRateInfo.TaxYear) + "1"), "PP", ref dblReturnAmount, ref dblDiscount))
										{
											clsTemp.Execute("delete from pendingdiscount where billkey = " + clsBill.Get_Fields_Int32("ID"), "twcl0000.vb1");
											// TODO Get_Fields: Check the table for the column [ACCOUNT] and replace with corresponding Get_Field method
											modCLDiscount.AddDiscountAmountToPendingTable_78(dblDiscount, FCConvert.ToInt32(Conversion.Val(clsAudit.Get_Fields("ACCOUNT"))), FCConvert.ToInt32(Conversion.Val(clsRateInfo.TaxYear) + "1"), "PP");
											// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
											modGlobalFunctions.AddCYAEntry_242("CL", "Applied discount", "PP Acct " + FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("account"))), "Bill Year " + FCConvert.ToString(Conversion.Val(clsRateInfo.TaxYear) + "1"), "Discount " + FCConvert.ToString(dblReturnAmount));
										}
									}
								}
							}
							if (!boolCancelBill)
							{
								clsBill.Update();
							}
							else
							{
								if (boolNewBill)
								{
									clsBill.Delete();
								}
							}
						}
						else
						{
							AuditGrid.RowHidden(AuditGrid.Rows - 1, true);
						}
						// if boolwritebill
					}
					clsAudit.MoveNext();
				}
				if (intWhereFrom == 4)
				{
					for (x = AuditGrid.Rows - 1; x >= 1; x--)
					{
						if (AuditGrid.RowHidden(x))
						{
							AuditGrid.RemoveItem(x);
						}
					}
					// x
					clsTemp.OpenRecordset("select BillingMaster.ID as BillingMasterID, TaxClub.ID as TaxClubID, * from billingmaster inner join taxclub on (billingmaster.account = taxclub.account) and (billingmaster.billingtype = taxclub.type) and (billingmaster.billingyear = taxclub.year)  where taxclub.billkey <> billingmaster.ID", "Collections");
					if (clsTemp.EndOfFile() != true && clsTemp.BeginningOfFile() != true)
					{
						clsDRWrapper clsTemp20 = new clsDRWrapper();
						do
						{
							//Application.DoEvents();
							// TODO Get_Fields: Field [BillingMasterID] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TaxClubID] not found!! (maybe it is an alias?)
							clsTemp20.Execute("UPDATE TaxClue Set BillKey = " + clsTemp.Get_Fields("BillingMasterID") + " WHERE ID = " + clsTemp.Get_Fields("TaxClubID"), "Collections");
							clsTemp.MoveNext();
						}
						while (clsTemp.EndOfFile() != true);
					}
				}
				TotalGrid.TextMatrix(0, 2, Strings.Format(lngTotvalue, "###,###,###,##0"));
				TotalGrid.TextMatrix(0, 3, Strings.Format(lngTotExempt, "###,###,###,##0"));
				TotalGrid.TextMatrix(0, 4, Strings.Format(lngTotAssess, "###,###,###,##0"));
				TotalGrid.TextMatrix(0, 5, Strings.Format(dblTotTax, "###,###,##0.00"));
				GridSummary.TextMatrix(1, 2, Strings.Format(lngTotvalue, "###,###,###,##0"));
				GridSummary.TextMatrix(1, 3, Strings.Format(lngTotExempt, "###,###,###,##0"));
				GridSummary.TextMatrix(1, 4, Strings.Format(lngTotAssess, "###,###,###,##0"));
				GridSummary.TextMatrix(1, 5, Strings.Format(dblTotTax, "###,###,##0.00"));
				CategoryGrid.TextMatrix(0, 1, Strings.Format(categorytotal[1], "#,###,###,##0"));
				CategoryGrid.TextMatrix(1, 1, Strings.Format(categorytotal[2], "#,###,###,##0"));
				CategoryGrid.TextMatrix(2, 1, Strings.Format(categorytotal[3], "#,###,###,##0"));
				CategoryGrid.TextMatrix(0, 3, Strings.Format(categorytotal[4], "#,###,###,##0"));
				CategoryGrid.TextMatrix(1, 3, Strings.Format(categorytotal[5], "#,###,###,##0"));
				CategoryGrid.TextMatrix(2, 3, Strings.Format(categorytotal[6], "#,###,###,##0"));
				CategoryGrid.TextMatrix(0, 5, Strings.Format(categorytotal[7], "#,###,###,##0"));
				CategoryGrid.TextMatrix(1, 5, Strings.Format(categorytotal[8], "#,###,###,##0"));
				CategoryGrid.TextMatrix(2, 5, Strings.Format(categorytotal[9], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(0, 1, Strings.Format(CategoryBETETotal[1], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(1, 1, Strings.Format(CategoryBETETotal[2], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(2, 1, Strings.Format(CategoryBETETotal[3], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(0, 3, Strings.Format(CategoryBETETotal[4], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(1, 3, Strings.Format(CategoryBETETotal[5], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(2, 3, Strings.Format(CategoryBETETotal[6], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(0, 5, Strings.Format(CategoryBETETotal[7], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(1, 5, Strings.Format(CategoryBETETotal[8], "#,###,###,##0"));
				GridBETEExempt.TextMatrix(2, 5, Strings.Format(CategoryBETETotal[9], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(0, 1, Strings.Format(categorytotal[1], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(1, 1, Strings.Format(categorytotal[2], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(2, 1, Strings.Format(categorytotal[3], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(0, 3, Strings.Format(categorytotal[4], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(1, 3, Strings.Format(categorytotal[5], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(2, 3, Strings.Format(categorytotal[6], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(0, 5, Strings.Format(categorytotal[7], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(1, 5, Strings.Format(categorytotal[8], "#,###,###,##0"));
				GridSummaryCategory.TextMatrix(2, 5, Strings.Format(categorytotal[9], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(0, 1, Strings.Format(CategoryBETETotal[1], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(1, 1, Strings.Format(CategoryBETETotal[2], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(2, 1, Strings.Format(CategoryBETETotal[3], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(0, 3, Strings.Format(CategoryBETETotal[4], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(1, 3, Strings.Format(CategoryBETETotal[5], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(2, 3, Strings.Format(CategoryBETETotal[6], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(0, 5, Strings.Format(CategoryBETETotal[7], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(1, 5, Strings.Format(CategoryBETETotal[8], "#,###,###,##0"));
				GridSummaryBETEExempt.TextMatrix(2, 5, Strings.Format(CategoryBETETotal[9], "#,###,###,##0"));
				// AuditGrid.Row = AuditGrid.Rows - 1
				// AuditGrid.Col = 0
				// AuditGrid.ColSel = 6
				// Call AuditGrid.CellBorder(RGB(1, 1, 1), -1, 3, -1, -1, -1, -1)
				frmWait.InstancePtr.Unload();
				if (intWhereFrom == 4)
				{
					// send the total figure to budgetary
					if (modGlobalConstants.Statics.gboolBD && dblTotalTax != 0)
					{
						int lngYear = 0;
						int lngReturn = 0;
						modBudgetaryAccounting.FundType[] FundRec = null;
						lngYear = clsRateInfo.TaxYear;
						intCurFundRec = -1;
						frmWait.InstancePtr.Unload();
						intCurFundRec += 1;
						Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
						FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.PPReceivable;
						FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(dblTotalTax);
						FundRec[intCurFundRec].UseDueToFrom = false;
						FundRec[intCurFundRec].AcctType = "A";
						FundRec[intCurFundRec].RCB = "R";
						FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Tax Commitment";
						if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
						{
							FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
						}
						intCurFundRec += 1;
						Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
						FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.PPCommitment;
						FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(-(dblTotalTax));
						FundRec[intCurFundRec].UseDueToFrom = false;
						FundRec[intCurFundRec].AcctType = "A";
						FundRec[intCurFundRec].RCB = "R";
						FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Tax Commitment";
						if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
						{
							FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
							lngReturn = modBudgetaryAccounting.AddToJournal(ref FundRec, "GJ", 0, DateTime.Today, Strings.Right(FCConvert.ToString(lngYear), 2) + " Tax Commitment Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
						}
						else
						{
							lngReturn = modBudgetaryAccounting.AddToJournal(ref FundRec, "GJ", 0, DateTime.Today, FCConvert.ToString(lngYear) + " Tax Commitment");
						}
						if (lngReturn > 0)
						{
							MessageBox.Show("Data added to budgetary database in Journal " + FCConvert.ToString(lngReturn), "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
							if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
							{
								modGlobalFunctions.AddCYAEntry_80("CL", "Tax Commitment Journal" + FCConvert.ToString(lngReturn), "Year " + FCConvert.ToString(lngYear), "Tax " + FCConvert.ToString(dblTotalTax));
							}
							else
							{
								modGlobalFunctions.AddCYAEntry_242("CL", "Tax Commitment Journal" + FCConvert.ToString(lngReturn), "Year " + FCConvert.ToString(lngYear), "Tax " + FCConvert.ToString(dblTotalTax), "Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
							}
						}
						else
						{
							MessageBox.Show("The journal entry was not created in budgetary", "No Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
							{
								modGlobalFunctions.AddCYAEntry_80("CL", "Tax Comm Journal Failed", "Year " + FCConvert.ToString(lngYear), "Tax " + FCConvert.ToString(dblTotalTax));
							}
							else
							{
								modGlobalFunctions.AddCYAEntry_242("CL", "Tax Comm Journal Failed", "Year " + FCConvert.ToString(lngYear), "Tax " + FCConvert.ToString(dblTotalTax), "Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
							}
						}
						if (boolDisplayFirst)
						{
							rptPPAudit.InstancePtr.Run(true);
						}
						modGlobalVariables.Statics.gboolDonePPXferReport = true;
					}
				}
				if ((Strings.UCase(strSequence) == "NAME") || (Strings.UCase(strSequence) == ""))
				{
					AuditGrid.Col = colName;
					AuditGrid.Sort = FCGrid.SortSettings.flexSortStringAscending;
				}
				else if (Strings.UCase(strSequence) == "ACCOUNT")
				{
					AuditGrid.Col = colAcct;
					AuditGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				}
				else if (Strings.UCase(strSequence) == "TAX")
				{
					AuditGrid.Col = colTax;
					AuditGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				}
				else if (Strings.UCase(strSequence) == "LOCATION")
				{
					AuditGrid.Col = colSnum;
					AuditGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
					AuditGrid.Col = colLoc;
					AuditGrid.Sort = FCGrid.SortSettings.flexSortStringAscending;
				}
				if (!boolDisplayFirst)
				{
					if (intWhereFrom == 4)
					{
                        //FC:FINAL:MSH - restore missing report call
						rptPPAudit.InstancePtr.Init(false, true, false, true);
                    }
					else
					{
						rptPPAudit.InstancePtr.Init(false, false, true, true);
					}
				}
				else
				{
					boolPrintQuery = true;
				}
				return FillAuditGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In fill Audit Grid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillAuditGrid;
		}

		private void hack()
		{
			//Application.DoEvents();
			if (!modGlobalVariables.Statics.CancelledIt)
			{
				if (!FillAuditGrid())
				{
					Close();
					return;
				}
			}
			else
			{
				Close();
				return;
			}
			if (boolDisplayFirst)
			{
				this.Show(App.MainForm);
			}
		}
		// vbPorter upgrade warning: intCalledFrom As short	OnWriteFCConvert.ToInt32(
		public void Init(ref clsRateRecord RInfo, int intCalledFrom)
		{
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				intWhereFrom = intCalledFrom;
				//Application.DoEvents();
				if (intWhereFrom == 4)
				{
					lblDoubleClick.Visible = false;
					framSummary.Visible = true;
					//FC:FINAL:RPU:#1389 - Make Frame1 Visible false
					Frame1.Visible = false;
					cmdView.Visible = true;
				}
				clsRateInfo.BillingDate = RInfo.BillingDate;
				clsRateInfo.NumberOfPeriods = RInfo.NumberOfPeriods;
				for (x = 1; x <= 4; x++)
				{
					clsRateInfo.Set_DueDate(x, RInfo.Get_DueDate(x));
					clsRateInfo.Set_InterestStartDate(x, RInfo.Get_InterestStartDate(x));
					clsRateInfo.Set_BillWeight(x, RInfo.Get_BillWeight(x));
				}
				// x
				clsRateInfo.InterestRate = RInfo.InterestRate;
				clsRateInfo.RateType = RInfo.RateType;
				clsRateInfo.TaxRate = RInfo.TaxRate;
				clsRateInfo.TaxYear = RInfo.TaxYear;
				clsRateInfo.LoadRate(RInfo.RateKey);
                //Application.DoEvents();
                frmPPAuditRange.InstancePtr.Show(FormShowEnum.Modal);
				//Application.DoEvents();
				hack();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Audit Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// rptPPAudit.Show , MDIParent
			if (intWhereFrom == 4)
			{
				rptPPAudit.InstancePtr.Init(false, true);
			}
			else
			{
				rptPPAudit.InstancePtr.Init(false, false, true);
			}
		}

		private void mnuPrintSingleLine_Click(object sender, System.EventArgs e)
		{
			if (intWhereFrom == 4)
			{
				rptPPAudit.InstancePtr.Init(true, true);
			}
			else
			{
				rptPPAudit.InstancePtr.Init(true, false);
			}
		}

		private void mnuView_Click(object sender, System.EventArgs e)
		{
			if (framSummary.Visible == false)
			{
				//FC:FINAL:RPU:#1389 - Make Frame1 Visible false
				Frame1.Visible = false;
				framSummary.Visible = true;
				cmdView.Text = "View Detail";
				//FC:FINAL:RPU:#1389 - Change Size and Location of button also
				cmdView.Size = new Size(88, 25);
				cmdView.Location = new Point(cmdView.Location.X + 20, cmdView.Location.Y);
			}
			else
			{
				//FC:FINAL:RPU:#1389 - Make Frame1 Visible true
				Frame1.Visible = true;
				framSummary.Visible = false;
				cmdView.Text = "View Summary";
				//FC:FINAL:RPU:#1389 - Change Size and Location of button also
				cmdView.Size = new Size(108, 25);
				cmdView.Location = new Point(cmdView.Location.X - 20, cmdView.Location.Y);
			}
		}

		private void ReTotalGrids()
		{
			int x;
			int[] CategoryTotals = new int[9 + 1];
			double[] CategoryBETETotals = new double[9 + 1];
			int lngTotAssessment;
			int lngExemption;
			int lngValue;
			// vbPorter upgrade warning: lngAssessment As Variant --> As int
			int lngAssessment = 0;
			double dblTax;
			double dblTotTax;
			// recalcs the totals
			lngTotAssessment = 0;
			lngExemption = 0;
			lngValue = 0;
			for (x = 1; x <= 9; x++)
			{
				CategoryTotals[x] = 0;
			}
			// x
			dblTotTax = 0;
			dblTax = 0;
			for (x = 1; x <= AuditGrid.Rows - 1; x++)
			{
				lngValue += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colValue)));
				lngExemption += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colExempt)));
				lngAssessment += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colAssessment)));
				CategoryTotals[1] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat1)));
				CategoryTotals[2] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat2)));
				CategoryTotals[3] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat3)));
				CategoryTotals[4] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat4)));
				CategoryTotals[5] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat5)));
				CategoryTotals[6] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat6)));
				CategoryTotals[7] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat7)));
				CategoryTotals[8] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat8)));
				CategoryTotals[9] += FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colCat9)));
				CategoryBETETotals[1] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat1));
				CategoryBETETotals[2] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat2));
				CategoryBETETotals[3] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat3));
				CategoryBETETotals[4] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat4));
				CategoryBETETotals[5] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat5));
				CategoryBETETotals[6] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat6));
				CategoryBETETotals[7] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat7));
				CategoryBETETotals[8] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat8));
				CategoryBETETotals[9] += FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBETECat9));
				dblTax = FCConvert.ToDouble(AuditGrid.TextMatrix(x, colTax));
				dblTotTax += dblTax;
			}
			// x
			TotalGrid.TextMatrix(0, 2, Strings.Format(lngValue, "###,###,###,##0"));
			TotalGrid.TextMatrix(0, 3, Strings.Format(lngExemption, "###,###,###,##0"));
			TotalGrid.TextMatrix(0, 4, Strings.Format(lngAssessment, "###,###,###,##0"));
			TotalGrid.TextMatrix(0, 5, Strings.Format(dblTotTax, "###,###,##0.00"));
			GridSummary.TextMatrix(1, 2, Strings.Format(lngValue, "###,###,###,##0"));
			GridSummary.TextMatrix(1, 3, Strings.Format(lngExemption, "###,###,###,##0"));
			GridSummary.TextMatrix(1, 4, Strings.Format(lngAssessment, "###,###,###,##0"));
			GridSummary.TextMatrix(1, 5, Strings.Format(dblTotTax, "###,###,##0.00"));
			CategoryGrid.TextMatrix(0, 1, Strings.Format(CategoryTotals[1], "#,###,###,##0"));
			CategoryGrid.TextMatrix(1, 1, Strings.Format(CategoryTotals[2], "#,###,###,##0"));
			CategoryGrid.TextMatrix(2, 1, Strings.Format(CategoryTotals[3], "#,###,###,##0"));
			CategoryGrid.TextMatrix(0, 3, Strings.Format(CategoryTotals[4], "#,###,###,##0"));
			CategoryGrid.TextMatrix(1, 3, Strings.Format(CategoryTotals[5], "#,###,###,##0"));
			CategoryGrid.TextMatrix(2, 3, Strings.Format(CategoryTotals[6], "#,###,###,##0"));
			CategoryGrid.TextMatrix(0, 5, Strings.Format(CategoryTotals[7], "#,###,###,##0"));
			CategoryGrid.TextMatrix(1, 5, Strings.Format(CategoryTotals[8], "#,###,###,##0"));
			CategoryGrid.TextMatrix(2, 5, Strings.Format(CategoryTotals[9], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(0, 1, Strings.Format(CategoryBETETotals[1], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(1, 1, Strings.Format(CategoryBETETotals[2], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(2, 1, Strings.Format(CategoryBETETotals[3], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(0, 3, Strings.Format(CategoryBETETotals[4], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(1, 3, Strings.Format(CategoryBETETotals[5], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(2, 3, Strings.Format(CategoryBETETotals[6], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(0, 5, Strings.Format(CategoryBETETotals[7], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(1, 5, Strings.Format(CategoryBETETotals[8], "#,###,###,##0"));
			GridBETEExempt.TextMatrix(2, 5, Strings.Format(CategoryBETETotals[9], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(0, 1, Strings.Format(CategoryTotals[1], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(1, 1, Strings.Format(CategoryTotals[2], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(2, 1, Strings.Format(CategoryTotals[3], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(0, 3, Strings.Format(CategoryTotals[4], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(1, 3, Strings.Format(CategoryTotals[5], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(2, 3, Strings.Format(CategoryTotals[6], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(0, 5, Strings.Format(CategoryTotals[7], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(1, 5, Strings.Format(CategoryTotals[8], "#,###,###,##0"));
			GridSummaryCategory.TextMatrix(2, 5, Strings.Format(CategoryTotals[9], "#,###,###,##0"));
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			mnuPrint_Click(mnuPrint, EventArgs.Empty);
		}

		private void cmdPrintSingleLineVersion_Click(object sender, EventArgs e)
		{
		}
	}
}
