﻿//Fecher vbPorter - Version 1.0.0.35
using fecherFoundation;
using System;
using System.Runtime.InteropServices;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	public class modGlobalVariables
	{
		public const string strUTDatabase = "TWUT0000.vb1";
		public const string strREDatabase = "TWRE0000.vb1";
		public const string strPPDatabase = "TWPP0000.vb1";
		public const string strCLDatabase = "TWCL0000.vb1";
		public const string strCRDatabase = "TWCR0000.vb1";
		public const string strBDDatabase = "TWBD0000.vb1";
		public const string strBLDatabase = "TWBL0000.vb1";
		/// <summary>
		/// Public Const DATABASEPASSWORD = "TRIO2001"
		/// </summary>
		public const string DEFAULTDATABASE = "TWBL0000.vb1";
		
		
		public const int CNSTENTRYINTOBILLING = 1;
		public const int CNSTASSOCIATIONS = 11;
		public const int CNSTGROUPMAINT = 13;
		public const int CNSTMORTGAGEMAINT = 12;
		public const int CNSTAUDIT = 2;
		public const int CNSTBILLINGMENU = 4;
		public const int CNSTOUTPRINTING = 6;
		public const int CNSTPRINTBILLS = 5;
		public const int CNSTFILEMAINT = 14;
		public const int CNSTIMAGE = 18;
		public const int CNSTCUSTOMIZE = 16;
		public const int CNSTDISCOUNT = 17;
		public const int CNSTDISTPERCENTS = 15;
		public const int CNSTPRINTING = 8;
		public const int CNSTLABELS = 10;
		public const int CNSTLISTINGS = 9;
		public const int CNSTSUPPLEMENTAL = 7;
		public const int CNSTXFER = 3;
		public const int CNSTEDITGROUPS = 19;
		public const int CNSTEDITMORTGAGE = 20;
		public const int CNSTEDITTAXRATES = 21;

		public struct CustomizedStuff
		{
			public bool IgnorePrinterFonts;
			public bool ApplyDiscounts;
			public bool DuplexCommitment;
			public bool ShowRef1;
			public bool ShowRef2;
			public bool ShowTreeGrowth;
			public bool ShowDollarSigns;
			public bool boolIsRegional;
			public int intCurrentTown;
			public bool boolZeroLiabilityOption;
			public string AlternateAccount;
			public bool SplitSmallAmounts;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CustomizedStuff(int unusedParam)
			{
				this.IgnorePrinterFonts = false;
				this.ApplyDiscounts = false;
				this.DuplexCommitment = false;
				this.ShowDollarSigns = false;
				this.ShowRef1 = false;
				this.ShowRef2 = false;
				this.ShowTreeGrowth = false;
				this.intCurrentTown = 0;
				this.boolIsRegional = false;
				this.boolZeroLiabilityOption = false;
				this.AlternateAccount = string.Empty;
				this.SplitSmallAmounts = false;
			}
		};

		public struct REPPAccountInfoType
		{
			public string Fund;
			public string REReceivable;
			public string PPReceivable;
			public string TaxAcquiredReceivable;
			public string RECommitment;
			public string PPCommitment;
			public string RESupplemental;
			public string PPSupplemental;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public REPPAccountInfoType(int unusedParam)
			{
				this.Fund = string.Empty;
				this.REReceivable = string.Empty;
				this.PPReceivable = string.Empty;
				this.TaxAcquiredReceivable = string.Empty;
				this.RECommitment = string.Empty;
				this.PPCommitment = string.Empty;
				this.RESupplemental = string.Empty;
				this.PPSupplemental = string.Empty;
			}
		};

		public class StaticVariables
		{
            //=========================================================
            public bool moduleInitialized = false;
            public int gintBasis;
			public double dblOverPayRate;
			//public double dblDefaultInterestRate;
			//public bool gboolDefaultPaymentsToAuto;
			/// </summary>
			/// Public gboolRE As Boolean
			/// <summary>
			public bool boolRE;
			public int gintPassQuestion;
			public bool gboolCancelForm;
			//public bool boolShortRE;
			//public bool boolShortPP;
		
			public bool gboolDonePPXferReport;
			public bool CancelledIt;
			//public int gintStartPage;
			//public int gintEndPage;
			//public string gstrDriveName = "";
			public string gstrFolderName = "";
			//public string gstrFileName = "";
			//public string gstrPercent = "";
			/// </summary>
			/// these 3 are used by bills
			/// <summary>
			//public string gstrPaidDate = "";
			//public string gstrReducedBy = "";
			//public bool boolMessageShown;
			public CustomizedStuff CustomizedInfo = new CustomizedStuff(0);
			public REPPAccountInfoType REPPAccountInfo = new REPPAccountInfoType(0);
            public bool boolShortPP;
            public bool boolShortRE;
        }

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}

		public static double Round(double dValue, short iDigits)
		{
			double Round = 0;
			Round = Conversion.Int(FCConvert.ToDouble(dValue * (Math.Pow(10, iDigits)) + 0.5)) / (Math.Pow(10, iDigits));
			return Round;
		}
	}
}
