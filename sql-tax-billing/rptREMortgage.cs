﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREMortgage.
	/// </summary>
	public partial class rptREMortgage : BaseSectionReport
	{
		public rptREMortgage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Mortgage Holder List";
		}

		public static rptREMortgage InstancePtr
		{
			get
			{
				return (rptREMortgage)Sys.GetInstance(typeof(rptREMortgage));
			}
		}

		protected rptREMortgage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptREMortgage	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsre = new clsDRWrapper();
		int lngLastAccount;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
		}

		public void Init(bool boolNameOrder)
		{
			string strSQL = "";
			string strOrder = "";
			if (boolNameOrder)
			{
				strOrder = " order by DeedName1 ";
			}
			else
			{
				strOrder = " order by master.rsaccount ";
			}
			clsre.OpenRecordset("select * from master inner join (" + clsre.CurrentPrefix + "CentralData.dbo.mortgageassociation inner join " + clsre.CurrentPrefix + "CentralData.dbo.mortgageholders on (mortgageassociation.mortgageholderid = mortgageholders.id)) on (master.rsaccount = mortgageassociation.account) CROSS APPLY " + clsre.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(master.OwnerPartyID) as p where mortgageassociation.module = 'RE' and master.rsdeleted <> 1 and master.rscard = 1" + strOrder, "twre0000.vb1");
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Mortgage");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngLastAccount = 0;
			txtDate.Text = DateTime.Today.ToShortDateString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsre.EndOfFile())
			{
				if (Conversion.Val(clsre.Get_Fields_Int32("rsaccount")) != lngLastAccount)
				{
					txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
					txtName.Text = FCConvert.ToString(clsre.Get_Fields_String("DeedName1"));
					lngLastAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsre.Get_Fields_Int32("rsaccount"))));
				}
				else
				{
					txtaccount.Text = "";
					txtName.Text = "";
				}
				if (FCConvert.ToBoolean(clsre.Get_Fields_Boolean("escrowed")))
				{
					txtEscrow.Text = "Yes";
				}
				else
				{
					txtEscrow.Text = "No";
				}
				if (FCConvert.ToBoolean(clsre.Get_Fields_Boolean("receivebill")))
				{
					txtBill.Text = "Yes";
				}
				else
				{
					txtBill.Text = "No";
				}
				txtHolderNumber.Text = FCConvert.ToString(clsre.Get_Fields_Int32("id"));
				txtHolderName.Text = FCConvert.ToString(clsre.Get_Fields_String("name"));
				clsre.MoveNext();
			}
		}

		
	}
}
