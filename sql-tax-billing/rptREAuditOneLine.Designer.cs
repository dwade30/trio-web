﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREAuditOneLine.
	/// </summary>
	partial class rptREAuditOneLine
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptREAuditOneLine));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txthsteadLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHsteadBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHsteadCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txthsteadLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHsteadBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHsteadCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.txtLand,
				this.txtBldg,
				this.txtAssessment,
				this.txtTax,
				this.txtMapLot
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotLand,
				this.txtTotBldg,
				this.txtTotAssess,
				this.txtTotTax,
				this.Label12,
				this.txtCount,
				this.Field1,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.txthsteadLand,
				this.txtHsteadBldg,
				this.Field9,
				this.Field10,
				this.Label13,
				this.txtHsteadCount
			});
			this.ReportFooter.Height = 0.7708333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.Label3,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label9,
				this.Label10,
				this.txtDate,
				this.txtMuniName,
				this.txtTime,
				this.lblTitle
			});
			this.PageHeader.Height = 0.75F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.3125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 1F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Name";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 0.625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.5F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Land";
			this.Label5.Top = 0.5625F;
			this.Label5.Width = 0.875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Tax";
			this.Label6.Top = 0.5625F;
			this.Label6.Width = 0.625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.5625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Assessment";
			this.Label7.Top = 0.5625F;
			this.Label7.Width = 0.875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.4375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Building";
			this.Label9.Top = 0.5625F;
			this.Label9.Width = 0.875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Map/Lot";
			this.Label10.Top = 0.5625F;
			this.Label10.Width = 1.3125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.25F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.19F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Text = "Field1";
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2.572917F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.0625F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Real Estate Audit Report";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0F;
			this.txtName.Width = 1.8125F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.1875F;
			this.txtLand.Left = 3.4375F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Text = null;
			this.txtLand.Top = 0F;
			this.txtLand.Width = 0.9375F;
			// 
			// txtBldg
			// 
			this.txtBldg.Height = 0.1875F;
			this.txtBldg.Left = 4.4375F;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0F;
			this.txtBldg.Width = 0.875F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.1875F;
			this.txtAssessment.Left = 5.375F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 0F;
			this.txtAssessment.Width = 1.0625F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.1875F;
			this.txtTax.Left = 6.5F;
			this.txtTax.Name = "txtTax";
			this.txtTax.Style = "text-align: right";
			this.txtTax.Text = null;
			this.txtTax.Top = 0F;
			this.txtTax.Width = 1F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.1875F;
			this.txtMapLot.Left = 1.875F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.5F;
			// 
			// txtTotLand
			// 
			this.txtTotLand.Height = 0.1875F;
			this.txtTotLand.Left = 2.4375F;
			this.txtTotLand.Name = "txtTotLand";
			this.txtTotLand.Style = "font-weight: bold; text-align: right";
			this.txtTotLand.Text = null;
			this.txtTotLand.Top = 0.25F;
			this.txtTotLand.Width = 1.1875F;
			// 
			// txtTotBldg
			// 
			this.txtTotBldg.Height = 0.1875F;
			this.txtTotBldg.Left = 3.6875F;
			this.txtTotBldg.Name = "txtTotBldg";
			this.txtTotBldg.Style = "font-weight: bold; text-align: right";
			this.txtTotBldg.Text = null;
			this.txtTotBldg.Top = 0.25F;
			this.txtTotBldg.Width = 1.25F;
			// 
			// txtTotAssess
			// 
			this.txtTotAssess.Height = 0.1875F;
			this.txtTotAssess.Left = 5F;
			this.txtTotAssess.Name = "txtTotAssess";
			this.txtTotAssess.Style = "font-weight: bold; text-align: right";
			this.txtTotAssess.Text = null;
			this.txtTotAssess.Top = 0.25F;
			this.txtTotAssess.Width = 1.1875F;
			// 
			// txtTotTax
			// 
			this.txtTotTax.Height = 0.1875F;
			this.txtTotTax.Left = 6.25F;
			this.txtTotTax.Name = "txtTotTax";
			this.txtTotTax.Style = "font-weight: bold; text-align: right";
			this.txtTotTax.Text = null;
			this.txtTotTax.Top = 0.25F;
			this.txtTotTax.Width = 1.25F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold";
			this.Label12.Text = "Total";
			this.Label12.Top = 0.25F;
			this.Label12.Width = 0.5625F;
			// 
			// txtCount
			// 
			this.txtCount.CanGrow = false;
			this.txtCount.Height = 0.1875F;
			this.txtCount.Left = 0.8125F;
			this.txtCount.MultiLine = false;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-weight: bold; text-align: right";
			this.txtCount.Text = "Count";
			this.txtCount.Top = 0.25F;
			this.txtCount.Width = 0.8125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 3.6875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-weight: bold; text-align: right";
			this.Field1.Text = "Building";
			this.Field1.Top = 0.0625F;
			this.Field1.Width = 1.25F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 5F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-weight: bold; text-align: right";
			this.Field3.Text = "Assessment";
			this.Field3.Top = 0.0625F;
			this.Field3.Width = 1.1875F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 6.375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-weight: bold; text-align: right";
			this.Field4.Text = "Tax";
			this.Field4.Top = 0.0625F;
			this.Field4.Width = 1.125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 2.4375F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-weight: bold; text-align: right";
			this.Field5.Text = "Land";
			this.Field5.Top = 0.0625F;
			this.Field5.Width = 1.1875F;
			// 
			// Field6
			// 
			this.Field6.CanGrow = false;
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 0.8125F;
			this.Field6.MultiLine = false;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-weight: bold; text-align: right";
			this.Field6.Text = "Count";
			this.Field6.Top = 0.0625F;
			this.Field6.Width = 0.8125F;
			// 
			// txthsteadLand
			// 
			this.txthsteadLand.Height = 0.1875F;
			this.txthsteadLand.Left = 2.4375F;
			this.txthsteadLand.Name = "txthsteadLand";
			this.txthsteadLand.Style = "font-weight: bold; text-align: right";
			this.txthsteadLand.Text = null;
			this.txthsteadLand.Top = 0.5833333F;
			this.txthsteadLand.Width = 1.1875F;
			// 
			// txtHsteadBldg
			// 
			this.txtHsteadBldg.Height = 0.1875F;
			this.txtHsteadBldg.Left = 3.6875F;
			this.txtHsteadBldg.Name = "txtHsteadBldg";
			this.txtHsteadBldg.Style = "font-weight: bold; text-align: right";
			this.txtHsteadBldg.Text = null;
			this.txtHsteadBldg.Top = 0.5833333F;
			this.txtHsteadBldg.Width = 1.25F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 5F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-weight: bold; text-align: right";
			this.Field9.Text = "0";
			this.Field9.Top = 0.5833333F;
			this.Field9.Width = 1.1875F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 6.25F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-weight: bold; text-align: right";
			this.Field10.Text = "0.00";
			this.Field10.Top = 0.5833333F;
			this.Field10.Width = 1.25F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.3229167F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold";
			this.Label13.Text = "Exempt Homesteads";
			this.Label13.Top = 0.4375F;
			this.Label13.Width = 0.9270833F;
			// 
			// txtHsteadCount
			// 
			this.txtHsteadCount.CanGrow = false;
			this.txtHsteadCount.Height = 0.1875F;
			this.txtHsteadCount.Left = 0.8125F;
			this.txtHsteadCount.MultiLine = false;
			this.txtHsteadCount.Name = "txtHsteadCount";
			this.txtHsteadCount.Style = "font-weight: bold; text-align: right";
			this.txtHsteadCount.Text = "Count";
			this.txtHsteadCount.Top = 0.5833333F;
			this.txtHsteadCount.Width = 0.8125F;
			// 
			// rptREAuditOneLine
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txthsteadLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHsteadBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHsteadCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txthsteadLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHsteadBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHsteadCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
