﻿//Fecher vbPorter - Version 1.0.0.35
using Wisej.Web;
using Global;
using fecherFoundation;
using System;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmShowResults.
	/// </summary>
	partial class frmShowResults : BaseForm
	{
		public FCCommonDialog CommonDialog1_Save;
		public FCCommonDialog CommonDialog1;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCRichTextBox rtbText;
		public fecherFoundation.FCLabel lblKey;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmShowResults));
            this.CommonDialog1_Save = new fecherFoundation.FCCommonDialog();
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.rtbText = new fecherFoundation.FCRichTextBox();
            this.lblKey = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbText)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 483);
            this.BottomPanel.Size = new System.Drawing.Size(671, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.rtbText);
            this.ClientArea.Controls.Add(this.lblKey);
            this.ClientArea.Size = new System.Drawing.Size(671, 423);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(671, 60);
            // 
            // CommonDialog1_Save
            // 
            this.CommonDialog1_Save.Name = "CommonDialog1_Save";
            this.CommonDialog1_Save.Size = new System.Drawing.Size(0, 0);
            // 
            // CommonDialog1
            // 
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(290, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(80, 48);
            this.cmdPrint.TabIndex = 4;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // rtbText
            // 
            this.rtbText.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.rtbText.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rtbText.Location = new System.Drawing.Point(30, 20);
            this.rtbText.Name = "rtbText";
            this.rtbText.ReadOnly = true;
            this.rtbText.Size = new System.Drawing.Size(603, 387);
            this.rtbText.TabIndex = 1;
            this.rtbText.TabStop = false;
            // 
            // lblKey
            // 
            this.lblKey.Location = new System.Drawing.Point(6, 390);
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(38, 19);
            this.lblKey.TabIndex = 1;
            // 
            // MainMenu1
            // 
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmShowResults
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(671, 591);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmShowResults";
            this.Text = "";
            this.Load += new System.EventHandler(this.frmShowResults_Load);
            this.Activated += new System.EventHandler(this.frmShowResults_Activated);
            this.Resize += new System.EventHandler(this.frmShowResults_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmShowResults_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmShowResults_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbText)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
