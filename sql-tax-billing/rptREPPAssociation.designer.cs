﻿using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREPPAssociation.
	/// </summary>
	public partial class rptREPPAssociation : BaseSectionReport
	{
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptREPPAssociation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPPAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPPName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPPName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPPAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPPAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPPName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPPName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPPAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtPPAccount,
				this.txtPPName
			});
			this.Detail.Height = 0.19F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0.0625F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'";
			this.txtAccount.Text = "Field1";
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.8125F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0F;
			this.txtName.Width = 2.1875F;
			// 
			// txtPPAccount
			// 
			this.txtPPAccount.Height = 0.19F;
			this.txtPPAccount.Left = 4.125F;
			this.txtPPAccount.MultiLine = false;
			this.txtPPAccount.Name = "txtPPAccount";
			this.txtPPAccount.Style = "font-family: \'Tahoma\'";
			this.txtPPAccount.Text = "Field3";
			this.txtPPAccount.Top = 0F;
			this.txtPPAccount.Width = 0.625F;
			// 
			// txtPPName
			// 
			this.txtPPName.Height = 0.19F;
			this.txtPPName.Left = 4.8125F;
			this.txtPPName.MultiLine = false;
			this.txtPPName.Name = "txtPPName";
			this.txtPPName.Style = "font-family: \'Tahoma\'";
			this.txtPPName.Text = "Field4";
			this.txtPPName.Top = 0F;
			this.txtPPName.Width = 2.5625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniname,
				this.txtDate,
				this.txtTitle,
				this.txtPage,
				this.lblPage,
				this.lblAccount,
				this.lblName,
				this.lblPPName,
				this.lblPPAccount,
				this.txtTime
			});
			this.PageHeader.Height = 0.625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.19F;
			this.txtMuniname.Left = 0.0625F;
			this.txtMuniname.MultiLine = false;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "font-family: \'Tahoma\'";
			this.txtMuniname.Text = null;
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 1.5F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.25F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.1875F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.21875F;
			this.txtTitle.Left = 1.5625F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Real Estate and Personal Property Association List";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 4.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 7F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 0.4375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1975F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.15625F;
			this.lblPage.Width = 0.625F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.18625F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.0625F;
			this.lblAccount.MultiLine = false;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.46875F;
			this.lblAccount.Width = 0.6875F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.18625F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.8125F;
			this.lblName.MultiLine = false;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.46875F;
			this.lblName.Width = 1.1875F;
			// 
			// lblPPName
			// 
			this.lblPPName.Height = 0.18625F;
			this.lblPPName.HyperLink = null;
			this.lblPPName.Left = 4.8125F;
			this.lblPPName.MultiLine = false;
			this.lblPPName.Name = "lblPPName";
			this.lblPPName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblPPName.Text = "Personal Property Owner";
			this.lblPPName.Top = 0.46875F;
			this.lblPPName.Width = 2.4375F;
			// 
			// lblPPAccount
			// 
			this.lblPPAccount.Height = 0.18625F;
			this.lblPPAccount.HyperLink = null;
			this.lblPPAccount.Left = 4.125F;
			this.lblPPAccount.Name = "lblPPAccount";
			this.lblPPAccount.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblPPAccount.Text = "Account";
			this.lblPPAccount.Top = 0.46875F;
			this.lblPPAccount.Width = 0.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.0625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptREPPAssociation
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPPAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPPName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPPName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPPAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}

		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPPAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPPName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPPName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPPAccount;
	}
}
