﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	public class clsBLCustomBill
	{
		//=========================================================
		/// <summary>
		/// This is set up so that you can have as many classes
		/// </summary>
		/// <summary>
		/// as you want per module.  You do not have to handle
		/// </summary>
		/// <summary>
		/// all custom bill/reports the same way
		/// </summary>
		/// <summary>
		/// You can have multiple classes, or just one class
		/// </summary>
		/// <summary>
		/// that alters its behaviour based on a variable
		/// </summary>
		/// <summary>
		/// that determines which type you are doing.
		/// </summary>
		/// <summary>
		/// The properties and functions are set up to be used
		/// </summary>
		/// <summary>
		/// like recordsets from the report.  They do not have
		/// </summary>
		/// <summary>
		/// to actually work this way.  EndOfFile could return
		/// </summary>
		/// <summary>
		/// clsreport.endoffile, or it could return whether an
		/// </summary>
		/// <summary>
		/// index is pointing past the end of an array or grid
		/// </summary>
		/// <summary>
		/// Any functions or variables can be changed or added
		/// </summary>
		/// <summary>
		/// as long as the interface that the report expects
		/// </summary>
		/// <summary>
		/// is unchanged. I.E. add as much as you want or change an existing function that
		/// </summary>
		/// <summary>
		/// the report calls, but don't rename or delete it.
		/// </summary>
		private string strSQLReport = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper clsReport = new clsDRWrapper();
		private clsDRWrapper clsReport_AutoInitialized;

		private clsDRWrapper clsReport
		{
			get
			{
				if (clsReport_AutoInitialized == null)
				{
					clsReport_AutoInitialized = new clsDRWrapper();
				}
				return clsReport_AutoInitialized;
			}
			set
			{
				clsReport_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper clsMortgageHolders = new clsDRWrapper();
		private clsDRWrapper clsMortgageHolders_AutoInitialized;

		private clsDRWrapper clsMortgageHolders
		{
			get
			{
				if (clsMortgageHolders_AutoInitialized == null)
				{
					clsMortgageHolders_AutoInitialized = new clsDRWrapper();
				}
				return clsMortgageHolders_AutoInitialized;
			}
			set
			{
				clsMortgageHolders_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsMultiRecipients = new clsDRWrapper();
		private clsDRWrapper rsMultiRecipients_AutoInitialized;

		private clsDRWrapper rsMultiRecipients
		{
			get
			{
				if (rsMultiRecipients_AutoInitialized == null)
				{
					rsMultiRecipients_AutoInitialized = new clsDRWrapper();
				}
				return rsMultiRecipients_AutoInitialized;
			}
			set
			{
				rsMultiRecipients_AutoInitialized = value;
			}
		}

		private int lngCurrentFormatID;
		private string strDisplayTitle = string.Empty;
		private string strPrinterName = string.Empty;
		private string strThisModule = string.Empty;
		private string strThisDB = string.Empty;
		private string strDataDB = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper clsCustomCodes = new clsDRWrapper();
		private clsDRWrapper clsCustomCodes_AutoInitialized;

		private clsDRWrapper clsCustomCodes
		{
			get
			{
				if (clsCustomCodes_AutoInitialized == null)
				{
					clsCustomCodes_AutoInitialized = new clsDRWrapper();
				}
				return clsCustomCodes_AutoInitialized;
			}
			set
			{
				clsCustomCodes_AutoInitialized = value;
			}
		}

		private int lngVerticalAlignment;
		private bool boolUsePrinterFonts;
		private bool boolDotMatrix;
		private string strBillingName = "";
		private string strBillingName2 = "";
		private string[] strBillingAddress = new string[5 + 1];
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsBillFormat clsBillingParameters = new clsBillFormat();
		clsBillFormat clsBillingParameters_AutoInitialized;

		clsBillFormat clsBillingParameters
		{
			get
			{
				if (clsBillingParameters_AutoInitialized == null)
				{
					clsBillingParameters_AutoInitialized = new clsBillFormat();
				}
				return clsBillingParameters_AutoInitialized;
			}
			set
			{
				clsBillingParameters_AutoInitialized = value;
			}
		}

		private bool[] boolBillToPrint = new bool[4 + 1];
		const int CNSTPRINTREGBILL = 1;
		const int CNSTPRINTNEWOWNER = 2;
		const int CNSTPRINTMORTGAGEHOLDER = 3;
		const int CNSTPRINTMULTIRECIPIENT = 4;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cPartyController tPCont = new cPartyController();
		private cPartyController tPCont_AutoInitialized;

		private cPartyController tPCont
		{
			get
			{
				if (tPCont_AutoInitialized == null)
				{
					tPCont_AutoInitialized = new cPartyController();
				}
				return tPCont_AutoInitialized;
			}
			set
			{
				tPCont_AutoInitialized = value;
			}
		}

		public void AssignBillFormat(ref clsBillFormat clsBFormat)
		{
			clsBillingParameters.CopyFormat(ref clsBFormat);
		}

		public bool DotMatrixFormat
		{
			set
			{
				boolDotMatrix = value;
			}
			get
			{
				bool DotMatrixFormat = false;
				DotMatrixFormat = boolDotMatrix;
				return DotMatrixFormat;
			}
		}

		public bool UsePrinterFonts
		{
			set
			{
				boolUsePrinterFonts = value;
			}
			get
			{
				bool UsePrinterFonts = false;
				UsePrinterFonts = boolUsePrinterFonts;
				return UsePrinterFonts;
			}
		}

		public int VerticalAlignment
		{
			set
			{
				// adjust up or down by lngtwips
				lngVerticalAlignment = value;
			}
			get
			{
				int VerticalAlignment = 0;
				VerticalAlignment = lngVerticalAlignment;
				return VerticalAlignment;
			}
		}

		public string Module
		{
			set
			{
				// I.E. BL or UT etc.
				strThisModule = value;
			}
			get
			{
				string Module = "";
				Module = strThisModule;
				return Module;
			}
		}

		public string DBFile
		{
			set
			{
				// The filename.  Usually TW[Mod abbreviation]0000.vb1
				strThisDB = value;
			}
			get
			{
				string DBFile = "";
				DBFile = strThisDB;
				return DBFile;
			}
		}

		public string DataDBFile
		{
			set
			{
				// The filename of the db that has the data
				strDataDB = value;
			}
			get
			{
				string DataDBFile = "";
				if (strDataDB != string.Empty)
				{
					DataDBFile = strDataDB;
				}
				else
				{
					DataDBFile = strThisDB;
				}
				return DataDBFile;
			}
		}

		public string SQL
		{
			set
			{
				// The sql statement.  This is not accessed directly
				// by the report since the programmer might use an array of types or a grid
				// instead of a recordset
				strSQLReport = value;
			}
			get
			{
				string SQL = "";
				SQL = strSQLReport;
				return SQL;
			}
		}

		public int FormatID
		{
			set
			{
				// The ID of the report
				// Should be set before the report is run
				lngCurrentFormatID = value;
			}
			get
			{
				int FormatID = 0;
				FormatID = lngCurrentFormatID;
				return FormatID;
			}
		}

		public string ReportTitle
		{
			set
			{
				// What you want displayed if the report is previewed
				strDisplayTitle = value;
			}
			get
			{
				string ReportTitle = "";
				ReportTitle = strDisplayTitle;
				return ReportTitle;
			}
		}

		public string PrinterName
		{
			set
			{
				// If you need a specific printer and don't want the user to
				// be able to choose one, fill this in before calling the report
				strPrinterName = value;
			}
			get
			{
				string PrinterName = "";
				PrinterName = strPrinterName;
				return PrinterName;
			}
		}

		public bool EndOfFile
		{
			get
			{
				bool EndOfFile = false;
				// end of file, or more generally, end of the data
				EndOfFile = clsReport.EndOfFile();
				return EndOfFile;
			}
		}

		public bool BeginningOfFile
		{
			get
			{
				bool BeginningOfFile = false;
				BeginningOfFile = clsReport.BeginningOfFile();
				return BeginningOfFile;
			}
		}

		public void MoveNext()
		{
			DateTime dtDate;
			clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolMortHolders;
			bool boolFirst;
			cParty tParty;
			boolFirst = false;
			if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("billingtype"))) != "PP")
			{
				boolMortHolders = false;
				if (boolBillToPrint[CNSTPRINTNEWOWNER])
				{
					boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = true;
					boolFirst = true;
					boolBillToPrint[CNSTPRINTNEWOWNER] = false;
				}
				if (boolBillToPrint[CNSTPRINTREGBILL])
				{
					if (clsBillingParameters.PrintCopyForNewOwner)
					{
						// check if the owner is different
						// assume it is a different owner if the names are different and there is an entry in the
						// previous owners table
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						clsTemp.OpenRecordset("select ownerpartyid,rscard,rsaccount from master  where rscard = 1 and rsaccount = " + clsReport.Get_Fields("account"), "twre0000.vb1");
						if (!clsTemp.EndOfFile())
						{
                            if (clsTemp.Get_Fields_String("DeedName1").ToLower() !=
                                clsReport.Get_Fields_String("Name1").ToLower())
                            {
                                boolBillToPrint[CNSTPRINTREGBILL] = false;
                                boolBillToPrint[CNSTPRINTNEWOWNER] = true;
                                boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
                                boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
                            }
                            else
                            {
                                boolBillToPrint[CNSTPRINTREGBILL] = true;
                                boolFirst = true;
                                boolBillToPrint[CNSTPRINTNEWOWNER] = false;
                                boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
                                boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
                            }
						}
						else
						{
							boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = true;
							boolFirst = true;
							boolBillToPrint[CNSTPRINTREGBILL] = false;
							boolBillToPrint[CNSTPRINTNEWOWNER] = false;
							boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
						}
					}
					else
					{
						boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = true;
						boolFirst = true;
						boolBillToPrint[CNSTPRINTREGBILL] = false;
						boolBillToPrint[CNSTPRINTNEWOWNER] = false;
						boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
					}
				}
				if (boolBillToPrint[CNSTPRINTMULTIRECIPIENT])
				{
					if (clsBillingParameters.PrintCopyForMultiOwners)
					{
						if (boolFirst)
						{
							rsMultiRecipients.OpenRecordset("select * from owners where module = 'RE' and associd = " + clsReport.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
							if (rsMultiRecipients.EndOfFile())
							{
								boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
								boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = true;
								boolFirst = true;
							}
							else
							{
								boolFirst = false;
							}
						}
						else if (!rsMultiRecipients.EndOfFile())
						{
							rsMultiRecipients.MoveNext();
							if (rsMultiRecipients.EndOfFile())
							{
								boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
								boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = true;
								boolFirst = true;
							}
						}
						else
						{
							boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
							boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = true;
							boolFirst = true;
						}
					}
					else
					{
						boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
						boolBillToPrint[CNSTPRINTREGBILL] = false;
						boolBillToPrint[CNSTPRINTNEWOWNER] = false;
						boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = true;
						boolFirst = true;
					}
				}
				if (boolBillToPrint[CNSTPRINTMORTGAGEHOLDER])
				{
					if (clsBillingParameters.PrintCopyForMortgageHolders)
					{
						if (boolFirst)
						{
							// check if there are any mortgage holders
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							clsMortgageHolders.OpenRecordset("select * from mortgageholders inner join mortgageassociation on (mortgageassociation.mortgageholderid = mortgageholders.ID) where REceivebill = 1 and module = 'RE' and account = " + clsReport.Get_Fields("account"), "CentralData");
							if (!clsMortgageHolders.EndOfFile())
							{
								boolBillToPrint[CNSTPRINTREGBILL] = false;
								boolBillToPrint[CNSTPRINTNEWOWNER] = false;
								boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = true;
							}
							else
							{
								clsReport.MoveNext();
								boolBillToPrint[CNSTPRINTREGBILL] = true;
								boolBillToPrint[CNSTPRINTNEWOWNER] = false;
								boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
							}
						}
						else if (!clsMortgageHolders.EndOfFile())
						{
							clsMortgageHolders.MoveNext();
							if (clsMortgageHolders.EndOfFile())
							{
								clsReport.MoveNext();
								boolBillToPrint[CNSTPRINTREGBILL] = true;
								boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
							}
						}
						else
						{
							clsReport.MoveNext();
							boolBillToPrint[CNSTPRINTREGBILL] = true;
							boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
						}
					}
					else
					{
						boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
						boolBillToPrint[CNSTPRINTREGBILL] = true;
						clsReport.MoveNext();
					}
				}
			}
			else
			{
				// PP
				if (boolBillToPrint[CNSTPRINTREGBILL])
				{
					if (clsBillingParameters.PrintCopyForMultiOwners)
					{
						rsMultiRecipients.OpenRecordset("select * from owners where module = 'PP' and associd = " + clsReport.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
						if (rsMultiRecipients.EndOfFile())
						{
							boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
							boolBillToPrint[CNSTPRINTREGBILL] = true;
							clsReport.MoveNext();
						}
						else
						{
							boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = true;
							boolBillToPrint[CNSTPRINTREGBILL] = false;
						}
					}
					else
					{
						clsReport.MoveNext();
					}
				}
				else
				{
					// multirecipients
					rsMultiRecipients.MoveNext();
					if (rsMultiRecipients.EndOfFile())
					{
						boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
						boolBillToPrint[CNSTPRINTREGBILL] = true;
						clsReport.MoveNext();
					}
				}
			}
		}

		public void MoveLast()
		{
			clsReport.MoveLast();
		}

		public void MovePrevious()
		{
			// most likely not used
			clsReport.MovePrevious();
		}

		public void MoveFirst()
		{
			clsReport.MoveFirst();
		}

		public bool LoadData()
		{
			bool LoadData = false;
			// this function returns false if it fails
			// It loads the data.  In the example class, this loads a recordset
			// the function calls all assume a recordset, but the programmer can have movenext
			// add 1 to an index to an array.  The class controls how the data is loaded and read
			// The report will use it like a recordset, but the implementation is irrelevant
			try
			{
				// On Error GoTo ErrorHandler
				LoadData = false;
				// load the data
				clsReport.OpenRecordset(strSQLReport, DataDBFile);
				if (!clsReport.EndOfFile())
				{
					strBillingName = FCConvert.ToString(clsReport.Get_Fields_String("name1"));
					strBillingName2 = FCConvert.ToString(clsReport.Get_Fields_String("name2"));
					strBillingAddress[1] = FCConvert.ToString(clsReport.Get_Fields_String("address1"));
					strBillingAddress[2] = FCConvert.ToString(clsReport.Get_Fields_String("address2"));
					strBillingAddress[3] = FCConvert.ToString(clsReport.Get_Fields_String("address3"));
					strBillingAddress[4] = "";
				}
				else
				{
					strBillingName = "";
					strBillingName2 = "";
					strBillingAddress[1] = "";
					strBillingAddress[2] = "";
					strBillingAddress[3] = "";
					strBillingAddress[4] = "";
				}
				// load the codes
				clsCustomCodes.OpenRecordset("select * from custombillcodes order by fieldid", strThisDB);
				boolBillToPrint[CNSTPRINTREGBILL] = true;
				boolBillToPrint[CNSTPRINTMORTGAGEHOLDER] = false;
				boolBillToPrint[CNSTPRINTMULTIRECIPIENT] = false;
				boolBillToPrint[CNSTPRINTNEWOWNER] = false;
				LoadData = true;
				return LoadData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In clsCustomBill.LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadData;
		}

		public string GetDataByCode(int lngCode, string strExtraParameters)
		{
			string GetDataByCode = "";
			// the code corresponds to the data in the custombillcodes table
			bool boolSpecialCase = false;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			string[] strAry = null;
			GetDataByCode = "";
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			modCustomBill.CustomBillCodeType CurCode = new modCustomBill.CustomBillCodeType(0);
			cParty tParty;
			// vbPorter upgrade warning: tAddr As cPartyAddress	OnWrite(Collection)
			cPartyAddress tAddr;
			if (clsCustomCodes.FindFirstRecord("FieldID", lngCode))
			{
				boolSpecialCase = FCConvert.ToBoolean(clsCustomCodes.Get_Fields_Boolean("SpecialCase"));
				// TODO Get_Fields: Check the table for the column [Category] and replace with corresponding Get_Field method
				CurCode.Category = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCustomCodes.Get_Fields("Category"))));
				CurCode.Datatype = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCustomCodes.Get_Fields_Int16("datatype"))));
				CurCode.DBName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("dbname"));
				CurCode.FieldID = lngCode;
				CurCode.FieldName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("FieldName"));
				CurCode.FormatString = FCConvert.ToString(clsCustomCodes.Get_Fields_String("FormatString"));
				CurCode.SpecialCase = boolSpecialCase;
				CurCode.TableName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("tablename"));
				if (boolSpecialCase == true)
				{
					// it is a special case and can't be simply loaded straight from the database
					switch (lngCode)
					{
						case 10:
							{
								// billing address
								strBillingAddress[4] = "";
								strBillingAddress[5] = "";
								if (boolBillToPrint[CNSTPRINTREGBILL])
								{
									// regular bill
									strBillingName = FCConvert.ToString(clsReport.Get_Fields_String("name1"));
									strBillingName2 = FCConvert.ToString(clsReport.Get_Fields_String("name2"));
									strBillingAddress[1] = FCConvert.ToString(clsReport.Get_Fields_String("Address1"));
									strBillingAddress[2] = FCConvert.ToString(clsReport.Get_Fields_String("Address2"));
									strBillingAddress[3] = FCConvert.ToString(clsReport.Get_Fields_String("MailingAddress3"));
									strBillingAddress[4] = FCConvert.ToString(clsReport.Get_Fields_String("address3"));
								}
								else if (boolBillToPrint[CNSTPRINTNEWOWNER])
								{
									// new owner
									strBillingName = "";
									strBillingAddress[1] = "";
									strBillingAddress[2] = "";
									strBillingAddress[3] = "";
									// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
									clsTemp.OpenRecordset("select ownerpartyid,deedname1,deedname2,rsaccount,rscard from master  where rsaccount = " + clsReport.Get_Fields("account") + " and rscard = 1", "twre0000.vb1");
									if (!clsTemp.EndOfFile())
                                    {
                                        strBillingName = clsTemp.Get_Fields_String("Deedname1");
                                        strBillingName2 = clsTemp.Get_Fields_String("Deedname2");
										tParty = tPCont.GetParty(FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields_Int32("ownerpartyid"))));
										if (!(tParty == null))
										{											
											if (tParty.Addresses.Count > 0)
											{
												tAddr = tParty.Addresses[1];
												if (!(tAddr == null))
												{
													strBillingAddress[1] = Strings.Trim(tAddr.Address1);
													strBillingAddress[2] = Strings.Trim(tAddr.Address2);
													strBillingAddress[3] = Strings.Trim(tAddr.Address3);
													strBillingAddress[4] = Strings.Trim(tAddr.City + " " + tAddr.State + " " + tAddr.Zip);
												}
											}
										}
									}
									else
									{
										strBillingName = "";
										strBillingName2 = "";
										strBillingAddress[1] = "";
										strBillingAddress[2] = "";
										strBillingAddress[3] = "";
									}
								}
								else if (boolBillToPrint[CNSTPRINTMULTIRECIPIENT])
								{
									// multi recipient
									if (!rsMultiRecipients.EndOfFile())
									{
										strBillingName = FCConvert.ToString(clsReport.Get_Fields_String("name1"));
										strBillingName2 = "C/O " + rsMultiRecipients.Get_Fields_String("name");
										strBillingAddress[1] = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("address1"));
										strBillingAddress[2] = FCConvert.ToString(rsMultiRecipients.Get_Fields_String("address2"));
										strBillingAddress[3] = Strings.Trim(rsMultiRecipients.Get_Fields_String("city") + " " + rsMultiRecipients.Get_Fields_String("state") + "  " + rsMultiRecipients.Get_Fields_String("zip") + " " + rsMultiRecipients.Get_Fields_String("zip4"));
									}
									else
									{
										strBillingName = "";
										strBillingName2 = "";
										strBillingAddress[1] = "";
										strBillingAddress[2] = "";
										strBillingAddress[3] = "";
									}
								}
								else if (boolBillToPrint[CNSTPRINTMORTGAGEHOLDER])
								{
									// mortgageholder
									if (!clsMortgageHolders.NoMatch)
									{
										strBillingName = FCConvert.ToString(clsReport.Get_Fields_String("name1"));
										strBillingName2 = "C/O " + Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")));
										strBillingAddress[1] = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")));
										strBillingAddress[2] = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address2")));
										if (Strings.Trim(strExtraParameters) != string.Empty)
										{
											if (Conversion.Val(strExtraParameters) > 5)
											{
												strBillingAddress[3] = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address3")));
												strBillingAddress[4] = Strings.Trim(clsMortgageHolders.Get_Fields_String("City") + " " + clsMortgageHolders.Get_Fields_String("state") + " " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
											}
											else
											{
												strBillingAddress[3] = Strings.Trim(clsMortgageHolders.Get_Fields_String("City") + " " + clsMortgageHolders.Get_Fields_String("state") + " " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
												strBillingAddress[4] = "";
											}
										}
										else
										{
											strBillingAddress[3] = Strings.Trim(clsMortgageHolders.Get_Fields_String("City") + " " + clsMortgageHolders.Get_Fields_String("state") + " " + clsMortgageHolders.Get_Fields_String("zip") + " " + clsMortgageHolders.Get_Fields_String("zip4"));
											strBillingAddress[4] = "";
										}
									}
									else
									{
										strBillingName = "Mortgage Holder Not Found";
										strBillingName2 = "";
										strBillingAddress[1] = "";
										strBillingAddress[2] = "";
										strBillingAddress[3] = "";
										strBillingAddress[4] = "";
									}
								}
								if (strBillingAddress[3] == string.Empty)
								{
									strBillingAddress[3] = strBillingAddress[4];
									strBillingAddress[4] = "";
								}
								if (strBillingAddress[2] == string.Empty)
								{
									strBillingAddress[2] = strBillingAddress[3];
									strBillingAddress[3] = "";
								}
								if (strBillingAddress[1] == string.Empty)
								{
									strBillingAddress[1] = strBillingAddress[2];
									strBillingAddress[2] = strBillingAddress[3];
									strBillingAddress[3] = "";
								}
								if (strBillingName2 == string.Empty)
								{
									strBillingName2 = strBillingAddress[1];
									strBillingAddress[1] = strBillingAddress[2];
									strBillingAddress[2] = strBillingAddress[3];
									strBillingAddress[3] = strBillingAddress[4];
									strBillingAddress[4] = "";
								}
								if (strBillingName == string.Empty)
								{
									strBillingName = strBillingName2;
									strBillingName2 = strBillingAddress[1];
									strBillingAddress[1] = strBillingAddress[2];
									strBillingAddress[2] = strBillingAddress[3];
									strBillingAddress[3] = strBillingAddress[4];
									strBillingAddress[4] = "";
								}
								strTemp = "";
								if (strBillingName != string.Empty)
								{
									GetDataByCode = strBillingName;
									strTemp = "\r\n";
								}
								if (strBillingName2 != string.Empty)
								{
									GetDataByCode = GetDataByCode + strTemp + strBillingName2;
									strTemp = "\r\n";
								}
								if (strBillingAddress[1] != string.Empty)
								{
									GetDataByCode = GetDataByCode + strTemp + strBillingAddress[1];
									strTemp = "\r\n";
								}
								if (strBillingAddress[2] != string.Empty)
								{
									GetDataByCode = GetDataByCode + strTemp + strBillingAddress[2];
									strTemp = "\r\n";
								}
								if (strBillingAddress[3] != string.Empty)
								{
									GetDataByCode = GetDataByCode + strTemp + strBillingAddress[3];
								}
								if (strBillingAddress[4] != string.Empty)
								{
									GetDataByCode = GetDataByCode + strTemp + strBillingAddress[4];
								}
								break;
							}
						default:
							{
								//FC:FINAL:DSE A property cannot be passed as a ref parameter
								//GetDataByCode = modBLCustomBill.HandleSpecialCase(ref clsReport, ref CurCode, ref strExtraParameters);
								clsDRWrapper temp = clsReport;
								GetDataByCode = modBLCustomBill.HandleSpecialCase(ref temp, ref CurCode, ref strExtraParameters);
								clsReport = temp;
								break;
							}
					}
					//end switch
				}
				else
				{
					// is a simple code that just takes the data
					//FC:FINAL:DSE A property cannot be passed as a ref parameter
					//GetDataByCode = modBLCustomBill.HandleRegularCase(ref clsReport, ref CurCode);
					clsDRWrapper temp = clsReport;
					GetDataByCode = modBLCustomBill.HandleRegularCase(ref temp, ref CurCode);
					clsReport = temp;
				}
			}
			else if (lngCode < 0)
			{
				CurCode.Category = 0;
				CurCode.Datatype = 0;
				CurCode.DBName = "";
				CurCode.FieldID = lngCode;
				CurCode.FieldName = "";
				CurCode.FormatString = "";
				CurCode.SpecialCase = true;
				CurCode.TableName = "";
				//FC:FINAL:DSE A property cannot be passed as a ref parameter
				//GetDataByCode = modBLCustomBill.HandleSpecialCase(ref clsReport, ref CurCode, ref strExtraParameters);
				clsDRWrapper temp = clsReport;
				GetDataByCode = modBLCustomBill.HandleSpecialCase(ref temp, ref CurCode, ref strExtraParameters);
				clsReport = temp;
			}
			return GetDataByCode;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		public int CentimetersToTwips(ref double dblCentimeters)
		{
			int CentimetersToTwips = 0;
			// returns twips
			CentimetersToTwips = FCConvert.ToInt32(dblCentimeters * 567);
			return CentimetersToTwips;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		public int InchesToTwips(ref double dblInches)
		{
			int InchesToTwips = 0;
			// returns twips
			InchesToTwips = FCConvert.ToInt32(dblInches * 1440);
			return InchesToTwips;
		}

		public double TwipsToInches(ref int lngTwips)
		{
			double TwipsToInches = 0;
			TwipsToInches = lngTwips / 1440.0;
			return TwipsToInches;
		}

		public double TwipsToCentimeters(ref int lngTwips)
		{
			double TwipsToCentimeters = 0;
			TwipsToCentimeters = lngTwips / 567.0;
			return TwipsToCentimeters;
		}

		public string EmailTag()
		{
			string EmailTag = "";
			EmailTag = "";
			return EmailTag;
		}
	}
}
