﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmStartupWait.
	/// </summary>
	partial class frmStartupWait : BaseForm
	{
		public fecherFoundation.FCPictureBox Image2;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStartupWait));
			this.Image2 = new fecherFoundation.FCPictureBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 228);
			this.BottomPanel.Size = new System.Drawing.Size(480, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Image2);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(480, 168);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(480, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// Image2
			// 
			this.Image2.AllowDrop = true;
			this.Image2.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image2.DrawStyle = ((short)(0));
			this.Image2.DrawWidth = ((short)(1));
			this.Image2.FillStyle = ((short)(1));
			this.Image2.FontTransparent = true;
			this.Image2.Image = ((System.Drawing.Image)(resources.GetObject("Image2.Image")));
			this.Image2.Location = new System.Drawing.Point(158, 125);
			this.Image2.Name = "Image2";
			this.Image2.Picture = ((System.Drawing.Image)(resources.GetObject("Image2.Picture")));
			this.Image2.Size = new System.Drawing.Size(121, 140);
			this.Image2.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image2.TabIndex = 0;
			this.Image2.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 70);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(425, 42);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "CHANGES ARE BEING MADE TO YOUR DATABASE. MAKE SURE NO ONE ELSE RUNS THIS PROGRAM " + "UNTIL THIS MESSAGE DISAPPEARS";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(425, 26);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "PLEASE WAIT";
			// 
			// frmStartupWait
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(480, 336);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
			this.Name = "frmStartupWait";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.Activated += new System.EventHandler(this.frmStartupWait_Activated);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
