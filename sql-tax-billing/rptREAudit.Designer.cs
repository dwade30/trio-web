﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREAudit.
	/// </summary>
	partial class rptREAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptREAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHsteadLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHSteadbldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotHExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHsteadCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHsteadLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHSteadbldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHsteadCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAcct,
				this.txtName,
				this.txtLand,
				this.txtBldg,
				this.txtExemption,
				this.txtAssessment,
				this.txtTax,
				this.txtMapLot,
				this.txtLocation
			});
			this.Detail.Height = 0.3125F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotLand,
				this.txtTotBldg,
				this.txtTotExemption,
				this.txtTotAssess,
				this.txtTotTax,
				this.Label12,
				this.txtCount,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.txtHsteadLand,
				this.txtHSteadbldg,
				this.txtTotHExempt,
				this.Field10,
				this.Field11,
				this.txtHsteadCount,
				this.Label13
			});
			this.ReportFooter.Height = 0.6979167F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.Label2,
				this.Label3,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.txtDate,
				this.txtMuniName,
				this.txtTime,
				this.lblTitle
			});
			this.PageHeader.Height = 0.6458333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.3125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 1F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Acct";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Name";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 0.625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 1.6875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Land";
			this.Label5.Top = 0.46875F;
			this.Label5.Width = 0.875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Tax";
			this.Label6.Top = 0.46875F;
			this.Label6.Width = 0.625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.3125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Assessment";
			this.Label7.Top = 0.46875F;
			this.Label7.Width = 0.875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Text = "Exemption";
			this.Label8.Top = 0.46875F;
			this.Label8.Width = 0.8125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2.875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Building";
			this.Label9.Top = 0.46875F;
			this.Label9.Width = 0.9375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.5F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Map/Lot";
			this.Label10.Top = 0.3125F;
			this.Label10.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Text = "Location";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 0.9375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.25F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.19F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Text = "Field1";
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2.395833F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.0625F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Real Estate Audit Report";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtAcct
			// 
			this.txtAcct.Height = 0.19F;
			this.txtAcct.Left = 0F;
			this.txtAcct.Name = "txtAcct";
			this.txtAcct.Text = "Field1";
			this.txtAcct.Top = 0F;
			this.txtAcct.Width = 0.5625F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.625F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0F;
			this.txtName.Width = 1.8125F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 1.5F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Text = "Field1";
			this.txtLand.Top = 0.15625F;
			this.txtLand.Width = 1.0625F;
			// 
			// txtBldg
			// 
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 2.625F;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Text = "Field1";
			this.txtBldg.Top = 0.15625F;
			this.txtBldg.Width = 1.1875F;
			// 
			// txtExemption
			// 
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 3.875F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Text = "Field1";
			this.txtExemption.Top = 0.15625F;
			this.txtExemption.Width = 1.0625F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 5F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Text = "Field1";
			this.txtAssessment.Top = 0.15625F;
			this.txtAssessment.Width = 1.1875F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 6.25F;
			this.txtTax.Name = "txtTax";
			this.txtTax.Style = "text-align: right";
			this.txtTax.Text = "Field1";
			this.txtTax.Top = 0.15625F;
			this.txtTax.Width = 1.25F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 2.5F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Text = "Field1";
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.3125F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 3.875F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = "Field1";
			this.txtLocation.Top = 0F;
			this.txtLocation.Width = 2.125F;
			// 
			// txtTotLand
			// 
			this.txtTotLand.Height = 0.19F;
			this.txtTotLand.Left = 1.5F;
			this.txtTotLand.Name = "txtTotLand";
			this.txtTotLand.Style = "font-weight: bold; text-align: right";
			this.txtTotLand.Text = "Field1";
			this.txtTotLand.Top = 0.21875F;
			this.txtTotLand.Width = 1.0625F;
			// 
			// txtTotBldg
			// 
			this.txtTotBldg.Height = 0.19F;
			this.txtTotBldg.Left = 2.625F;
			this.txtTotBldg.Name = "txtTotBldg";
			this.txtTotBldg.Style = "font-weight: bold; text-align: right";
			this.txtTotBldg.Text = "Field2";
			this.txtTotBldg.Top = 0.21875F;
			this.txtTotBldg.Width = 1.1875F;
			// 
			// txtTotExemption
			// 
			this.txtTotExemption.Height = 0.19F;
			this.txtTotExemption.Left = 3.875F;
			this.txtTotExemption.Name = "txtTotExemption";
			this.txtTotExemption.Style = "font-weight: bold; text-align: right";
			this.txtTotExemption.Text = "Field3";
			this.txtTotExemption.Top = 0.21875F;
			this.txtTotExemption.Width = 1.0625F;
			// 
			// txtTotAssess
			// 
			this.txtTotAssess.Height = 0.19F;
			this.txtTotAssess.Left = 5F;
			this.txtTotAssess.Name = "txtTotAssess";
			this.txtTotAssess.Style = "font-weight: bold; text-align: right";
			this.txtTotAssess.Text = "Field4";
			this.txtTotAssess.Top = 0.21875F;
			this.txtTotAssess.Width = 1.1875F;
			// 
			// txtTotTax
			// 
			this.txtTotTax.Height = 0.19F;
			this.txtTotTax.Left = 6.25F;
			this.txtTotTax.Name = "txtTotTax";
			this.txtTotTax.Style = "font-weight: bold; text-align: right";
			this.txtTotTax.Text = "Field5";
			this.txtTotTax.Top = 0.21875F;
			this.txtTotTax.Width = 1.25F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold";
			this.Label12.Text = "Total";
			this.Label12.Top = 0.21875F;
			this.Label12.Width = 0.75F;
			// 
			// txtCount
			// 
			this.txtCount.CanGrow = false;
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 0.7916667F;
			this.txtCount.MultiLine = false;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-weight: bold; text-align: right";
			this.txtCount.Text = "Count";
			this.txtCount.Top = 0.21875F;
			this.txtCount.Width = 0.6458333F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.19F;
			this.Field1.Left = 2.625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-weight: bold; text-align: right";
			this.Field1.Text = "Building";
			this.Field1.Top = 0.0625F;
			this.Field1.Width = 1.1875F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.19F;
			this.Field2.Left = 3.875F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-weight: bold; text-align: right";
			this.Field2.Text = "Exemption";
			this.Field2.Top = 0.0625F;
			this.Field2.Width = 1.0625F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.19F;
			this.Field3.Left = 5F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-weight: bold; text-align: right";
			this.Field3.Text = "Assessment";
			this.Field3.Top = 0.0625F;
			this.Field3.Width = 1.1875F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.19F;
			this.Field4.Left = 6.25F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-weight: bold; text-align: right";
			this.Field4.Text = "Tax";
			this.Field4.Top = 0.0625F;
			this.Field4.Width = 1.25F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.19F;
			this.Field5.Left = 1.5F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-weight: bold; text-align: right";
			this.Field5.Text = "Land";
			this.Field5.Top = 0.0625F;
			this.Field5.Width = 1.0625F;
			// 
			// Field6
			// 
			this.Field6.CanGrow = false;
			this.Field6.Height = 0.19F;
			this.Field6.Left = 0.7916667F;
			this.Field6.MultiLine = false;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-weight: bold; text-align: right";
			this.Field6.Text = "Count";
			this.Field6.Top = 0.0625F;
			this.Field6.Width = 0.6458333F;
			// 
			// txtHsteadLand
			// 
			this.txtHsteadLand.Height = 0.19F;
			this.txtHsteadLand.Left = 1.5F;
			this.txtHsteadLand.Name = "txtHsteadLand";
			this.txtHsteadLand.Style = "font-weight: bold; text-align: right";
			this.txtHsteadLand.Text = "Field1";
			this.txtHsteadLand.Top = 0.5208333F;
			this.txtHsteadLand.Width = 1.0625F;
			// 
			// txtHSteadbldg
			// 
			this.txtHSteadbldg.Height = 0.19F;
			this.txtHSteadbldg.Left = 2.625F;
			this.txtHSteadbldg.Name = "txtHSteadbldg";
			this.txtHSteadbldg.Style = "font-weight: bold; text-align: right";
			this.txtHSteadbldg.Text = "Field2";
			this.txtHSteadbldg.Top = 0.5208333F;
			this.txtHSteadbldg.Width = 1.1875F;
			// 
			// txtTotHExempt
			// 
			this.txtTotHExempt.Height = 0.19F;
			this.txtTotHExempt.Left = 3.875F;
			this.txtTotHExempt.Name = "txtTotHExempt";
			this.txtTotHExempt.Style = "font-weight: bold; text-align: right";
			this.txtTotHExempt.Text = null;
			this.txtTotHExempt.Top = 0.5208333F;
			this.txtTotHExempt.Width = 1.0625F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.19F;
			this.Field10.Left = 5F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-weight: bold; text-align: right";
			this.Field10.Text = "0";
			this.Field10.Top = 0.5208333F;
			this.Field10.Width = 1.1875F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.19F;
			this.Field11.Left = 6.25F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-weight: bold; text-align: right";
			this.Field11.Text = "0.00";
			this.Field11.Top = 0.5208333F;
			this.Field11.Width = 1.25F;
			// 
			// txtHsteadCount
			// 
			this.txtHsteadCount.CanGrow = false;
			this.txtHsteadCount.Height = 0.19F;
			this.txtHsteadCount.Left = 0.8020833F;
			this.txtHsteadCount.MultiLine = false;
			this.txtHsteadCount.Name = "txtHsteadCount";
			this.txtHsteadCount.Style = "font-weight: bold; text-align: right";
			this.txtHsteadCount.Text = "Count";
			this.txtHsteadCount.Top = 0.5208333F;
			this.txtHsteadCount.Width = 0.6354167F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.3333333F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold";
			this.Label13.Text = "Exempt Homesteads";
			this.Label13.Top = 0.3645833F;
			this.Label13.Width = 0.8645833F;
			// 
			// rptREAudit
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHsteadLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHSteadbldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotHExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHsteadCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHsteadLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHSteadbldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotHExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHsteadCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
