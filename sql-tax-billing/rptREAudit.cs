﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREAudit.
	/// </summary>
	public partial class rptREAudit : BaseSectionReport
	{
		public rptREAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate Audit Report";
		}

		public static rptREAudit InstancePtr
		{
			get
			{
				return (rptREAudit)Sys.GetInstance(typeof(rptREAudit));
			}
		}

		protected rptREAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptREAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCurrRow;
		int intPage;
		int lngLastRow;
		bool boolSaveToFile;
		const int colAcct = 0;
		const int colName = 1;
		const int colMap = 2;
		const int colSnum = 3;
		const int colLoc = 4;
		const int colLand = 5;
		const int colBldg = 6;
		const int colExempt = 7;
		const int colTot = 8;
		const int colTax = 9;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = (lngCurrRow > lngLastRow);
		}

		public void Init()
		{
			// If init is being called, then this needs to be saved to a file
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			string strFileNameToUse = "";
			if (boolSaveToFile)
			{
				// save to a file
				// check to see what files are already saved so we know what to name this one
				strFileNameToUse = modGlobalRoutines.DetermineFileNameFromMask_8("REBLTR*.rdf", TWSharedLibrary.Variables.Statics.ReportPath);
				// now save it in the report directory
				while (!(this.State == ReportState.Completed))
				{
				}
				//FC:FINAL:DDU:#i1574 - prevent inexistent folder error
				if (!FCFileSystem.FileExists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					FCFileSystem.MkDir(TWSharedLibrary.Variables.Statics.ReportPath);
				}
				this.Document.Save(Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, strFileNameToUse));
			}
			//Application.DoEvents();
			if (!frmREAudit.InstancePtr.boolDisplayFirst)
			{
				this.Close();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (this.Visible)
			{
			    boolSaveToFile = false;
			}
			else
			{
				boolSaveToFile = true;
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			}
			if (frmREAudit.InstancePtr.intWhereFrom == 3)
			{
				lblTitle.Text = "Real Estate Billing Transfer Report";
				this.Name = "Real Estate Billing Transfer Report";
			}
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
			{
				txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				txtMuniName.Text = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
			}
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lngCurrRow = 1;
			lngLastRow = frmREAudit.InstancePtr.AuditGrid.Rows - 1;
			txtTotLand.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 2);
			txtTotBldg.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 3);
			txtTotExemption.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 4);
			txtTotAssess.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 5);
			txtTotTax.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 6);
			txtCount.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 1);
			txtHsteadCount.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(2, 1);
			txtTotHExempt.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(2, 4);
			txtHSteadbldg.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(2, 3);
			txtHsteadLand.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(2, 2);
			intPage = 1;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// Dim strFileNameToUse As String
			// 
			// If boolSaveToFile Then
			// save to a file
			// check to see what files are already saved so we know what to name this one
			// 
			// strFileNameToUse = DetermineFileNameFromMask("REBLTR*.rdf", CurDir & "\rpt\")
			// now save it in the report directory
			// 
			// rptREAudit.Pages.Save CurDir & "\rpt\" & strFileNameToUse
			// Unload Me
			// End If
			if (!frmREAudit.InstancePtr.boolDisplayFirst)
			{
				// And Not boolSaveToFile Then
				frmREAudit.InstancePtr.Unload();
				//Application.DoEvents();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCurrRow <= lngLastRow)
			{
				txtAcct.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, 0);
				// If UCase(txtAcct.Text) = "TOTAL" Then
				// txtAcct.Font.Bold = True
				// Else
				// txtAcct.Font.Bold = False
				// End If
				txtName.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colName);
				txtLand.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colLand);
				txtBldg.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colBldg);
				txtExemption.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colExempt);
				txtAssessment.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colTot);
				txtTax.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colTax);
				txtMapLot.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colMap);
				if (Conversion.Val(frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colSnum)) > 0)
				{
					txtLocation.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colSnum) + " " + frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colLoc);
				}
				else
				{
					txtLocation.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colLoc);
				}
			}
			lngCurrRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		
	}
}
