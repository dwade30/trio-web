//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.Drawing;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPPAudit.
	/// </summary>
	partial class frmPPAudit : BaseForm
	{
		public fecherFoundation.FCFrame framSummary;
		public FCGrid GridSummary;
		public FCGrid GridSummaryCategory;
		public FCGrid GridSummaryBETEExempt;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCPanel Frame1;
		public FCGrid AuditGrid;
		public FCGrid TotalGrid;
		public FCGrid CategoryGrid;
		public FCGrid GridBETEExempt;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCTextBox txtCommunicate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblDoubleClick;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuView;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSingleLine;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCButton cmdView;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle13 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle14 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPAudit));
			this.framSummary = new fecherFoundation.FCFrame();
			this.GridSummary = new fecherFoundation.FCGrid();
			this.GridSummaryCategory = new fecherFoundation.FCGrid();
			this.GridSummaryBETEExempt = new fecherFoundation.FCGrid();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCPanel();
			this.AuditGrid = new fecherFoundation.FCGrid();
			this.TotalGrid = new fecherFoundation.FCGrid();
			this.CategoryGrid = new fecherFoundation.FCGrid();
			this.GridBETEExempt = new fecherFoundation.FCGrid();
			this.Label4 = new fecherFoundation.FCLabel();
			this.txtCommunicate = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblDoubleClick = new fecherFoundation.FCLabel();
			this.mnuPrintSingleLine = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuView = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdPrintSingleLineVersion = new fecherFoundation.FCButton();
			this.cmdView = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framSummary)).BeginInit();
			this.framSummary.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSummaryCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSummaryBETEExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.AuditGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CategoryGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridBETEExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintSingleLineVersion)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdView)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 760);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.txtCommunicate);
			this.ClientArea.Controls.Add(this.framSummary);
			this.ClientArea.Controls.Add(this.lblDoubleClick);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Size = new System.Drawing.Size(1078, 700);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdView);
			this.TopPanel.Controls.Add(this.cmdPrintSingleLineVersion);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintSingleLineVersion, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdView, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(272, 30);
			this.HeaderText.Text = "Personal Property Audit";
			// 
			// framSummary
			// 
			this.framSummary.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.framSummary.BackColor = System.Drawing.Color.White;
			this.framSummary.Controls.Add(this.GridSummary);
			this.framSummary.Controls.Add(this.GridSummaryCategory);
			this.framSummary.Controls.Add(this.GridSummaryBETEExempt);
			this.framSummary.Controls.Add(this.Label5);
			this.framSummary.Controls.Add(this.Label1);
			this.framSummary.Controls.Add(this.Label3);
			this.framSummary.Location = new System.Drawing.Point(31, 30);
			this.framSummary.Name = "framSummary";
			this.framSummary.Size = new System.Drawing.Size(1019, 776);
			this.framSummary.TabIndex = 3;
			this.framSummary.Text = "Personal Property Billing Transfer Summary";
			this.framSummary.Visible = false;
			// 
			// GridSummary
			// 
			this.GridSummary.AllowSelection = false;
			this.GridSummary.AllowUserToResizeColumns = false;
			this.GridSummary.AllowUserToResizeRows = false;
			this.GridSummary.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridSummary.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridSummary.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridSummary.BackColorBkg = System.Drawing.Color.Empty;
			this.GridSummary.BackColorFixed = System.Drawing.Color.Empty;
			this.GridSummary.BackColorSel = System.Drawing.Color.Empty;
			this.GridSummary.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridSummary.Cols = 7;
			dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
			this.GridSummary.ColumnHeadersHeight = 30;
			this.GridSummary.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridSummary.DefaultCellStyle = dataGridViewCellStyle10;
			this.GridSummary.DragIcon = null;
			this.GridSummary.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridSummary.ExtendLastCol = true;
			this.GridSummary.FixedCols = 0;
			this.GridSummary.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridSummary.FrozenCols = 0;
			this.GridSummary.GridColor = System.Drawing.Color.Empty;
			this.GridSummary.GridColorFixed = System.Drawing.Color.Empty;
			this.GridSummary.Location = new System.Drawing.Point(20, 30);
			this.GridSummary.Name = "GridSummary";
			this.GridSummary.ReadOnly = true;
			this.GridSummary.RowHeadersVisible = false;
			this.GridSummary.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridSummary.RowHeightMin = 0;
			this.GridSummary.Rows = 50;
			this.GridSummary.ScrollTipText = null;
			this.GridSummary.ShowColumnVisibilityMenu = false;
			this.GridSummary.ShowFocusCell = false;
			this.GridSummary.Size = new System.Drawing.Size(984, 180);
			this.GridSummary.StandardTab = true;
			this.GridSummary.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridSummary.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridSummary.TabIndex = 0;
			// 
			// GridSummaryCategory
			// 
			this.GridSummaryCategory.AllowSelection = false;
			this.GridSummaryCategory.AllowUserToResizeColumns = false;
			this.GridSummaryCategory.AllowUserToResizeRows = false;
			this.GridSummaryCategory.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridSummaryCategory.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridSummaryCategory.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridSummaryCategory.BackColorBkg = System.Drawing.Color.Empty;
			this.GridSummaryCategory.BackColorFixed = System.Drawing.Color.Empty;
			this.GridSummaryCategory.BackColorSel = System.Drawing.Color.Empty;
			this.GridSummaryCategory.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridSummaryCategory.Cols = 10;
			dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridSummaryCategory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
			this.GridSummaryCategory.ColumnHeadersHeight = 30;
			this.GridSummaryCategory.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridSummaryCategory.ColumnHeadersVisible = false;
			dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridSummaryCategory.DefaultCellStyle = dataGridViewCellStyle12;
			this.GridSummaryCategory.DragIcon = null;
			this.GridSummaryCategory.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridSummaryCategory.ExtendLastCol = true;
			this.GridSummaryCategory.FixedCols = 0;
			this.GridSummaryCategory.FixedRows = 0;
			this.GridSummaryCategory.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridSummaryCategory.FrozenCols = 0;
			this.GridSummaryCategory.GridColor = System.Drawing.Color.Empty;
			this.GridSummaryCategory.GridColorFixed = System.Drawing.Color.Empty;
			this.GridSummaryCategory.Location = new System.Drawing.Point(20, 220);
			this.GridSummaryCategory.Name = "GridSummaryCategory";
			this.GridSummaryCategory.ReadOnly = true;
			this.GridSummaryCategory.RowHeadersVisible = false;
			this.GridSummaryCategory.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridSummaryCategory.RowHeightMin = 0;
			this.GridSummaryCategory.Rows = 3;
			this.GridSummaryCategory.ScrollTipText = null;
			this.GridSummaryCategory.ShowColumnVisibilityMenu = false;
			this.GridSummaryCategory.ShowFocusCell = false;
			this.GridSummaryCategory.Size = new System.Drawing.Size(984, 125);
			this.GridSummaryCategory.StandardTab = true;
			this.GridSummaryCategory.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridSummaryCategory.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridSummaryCategory.TabIndex = 1;
			// 
			// GridSummaryBETEExempt
			// 
			this.GridSummaryBETEExempt.AllowSelection = false;
			this.GridSummaryBETEExempt.AllowUserToResizeColumns = false;
			this.GridSummaryBETEExempt.AllowUserToResizeRows = false;
			this.GridSummaryBETEExempt.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridSummaryBETEExempt.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridSummaryBETEExempt.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridSummaryBETEExempt.BackColorBkg = System.Drawing.Color.Empty;
			this.GridSummaryBETEExempt.BackColorFixed = System.Drawing.Color.Empty;
			this.GridSummaryBETEExempt.BackColorSel = System.Drawing.Color.Empty;
			this.GridSummaryBETEExempt.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridSummaryBETEExempt.Cols = 10;
			dataGridViewCellStyle13.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridSummaryBETEExempt.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
			this.GridSummaryBETEExempt.ColumnHeadersHeight = 30;
			this.GridSummaryBETEExempt.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridSummaryBETEExempt.ColumnHeadersVisible = false;
			dataGridViewCellStyle14.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridSummaryBETEExempt.DefaultCellStyle = dataGridViewCellStyle14;
			this.GridSummaryBETEExempt.DragIcon = null;
			this.GridSummaryBETEExempt.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridSummaryBETEExempt.ExtendLastCol = true;
			this.GridSummaryBETEExempt.FixedCols = 0;
			this.GridSummaryBETEExempt.FixedRows = 0;
			this.GridSummaryBETEExempt.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridSummaryBETEExempt.FrozenCols = 0;
			this.GridSummaryBETEExempt.GridColor = System.Drawing.Color.Empty;
			this.GridSummaryBETEExempt.GridColorFixed = System.Drawing.Color.Empty;
			this.GridSummaryBETEExempt.Location = new System.Drawing.Point(20, 376);
			this.GridSummaryBETEExempt.Name = "GridSummaryBETEExempt";
			this.GridSummaryBETEExempt.ReadOnly = true;
			this.GridSummaryBETEExempt.RowHeadersVisible = false;
			this.GridSummaryBETEExempt.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridSummaryBETEExempt.RowHeightMin = 0;
			this.GridSummaryBETEExempt.Rows = 3;
			this.GridSummaryBETEExempt.ScrollTipText = null;
			this.GridSummaryBETEExempt.ShowColumnVisibilityMenu = false;
			this.GridSummaryBETEExempt.ShowFocusCell = false;
			this.GridSummaryBETEExempt.Size = new System.Drawing.Size(984, 125);
			this.GridSummaryBETEExempt.StandardTab = true;
			this.GridSummaryBETEExempt.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridSummaryBETEExempt.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridSummaryBETEExempt.TabIndex = 3;
			// 
			// Label5
			// 
			this.Label5.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Label5.Location = new System.Drawing.Point(20, 355);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(96, 16);
			this.Label5.TabIndex = 2;
			this.Label5.Text = "BETE EXEMPT";
			// 
			// Label1
			// 
			this.Label1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Label1.Location = new System.Drawing.Point(20, 571);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(608, 184);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "IF THE DISPLAYED TOTALS ARE INCORRECT, STOP HERE.  SELECT THE VIEW DETAILS OPTION" + " IN THE FILE MENU TO LIST INDIVIDUAL ACCOUNTS";
			this.Label1.Visible = false;
			// 
			// Label3
			// 
			this.Label3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Label3.Location = new System.Drawing.Point(20, 516);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(529, 33);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "***   IMPORTANT ***";
			this.Label3.Visible = false;
			// 
			// Frame1
			// 
			this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.AuditGrid);
			this.Frame1.Controls.Add(this.TotalGrid);
			this.Frame1.Controls.Add(this.CategoryGrid);
			this.Frame1.Controls.Add(this.GridBETEExempt);
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Location = new System.Drawing.Point(30, 90);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(1014, 640);
			this.Frame1.TabIndex = 2;
			// 
			// AuditGrid
			// 
			this.AuditGrid.AllowSelection = false;
			this.AuditGrid.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
			this.AuditGrid.AllowUserToResizeRows = false;
			this.AuditGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.AuditGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.AuditGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.AuditGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.AuditGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.AuditGrid.BackColorSel = System.Drawing.Color.Empty;
			this.AuditGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.AuditGrid.Cols = 8;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.AuditGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.AuditGrid.ColumnHeadersHeight = 30;
			this.AuditGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.AuditGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.AuditGrid.DragIcon = null;
			this.AuditGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.AuditGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.AuditGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.AuditGrid.FrozenCols = 0;
			this.AuditGrid.GridColor = System.Drawing.Color.Empty;
			this.AuditGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.AuditGrid.Location = new System.Drawing.Point(0, 0);
			this.AuditGrid.Name = "AuditGrid";
			this.AuditGrid.ReadOnly = true;
			this.AuditGrid.RowHeightMin = 0;
			this.AuditGrid.Rows = 50;
			this.AuditGrid.ScrollTipText = null;
			this.AuditGrid.ShowColumnVisibilityMenu = false;
			this.AuditGrid.ShowFocusCell = false;
			this.AuditGrid.Size = new System.Drawing.Size(1006, 312);
			this.AuditGrid.StandardTab = true;
			this.AuditGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.AuditGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.AuditGrid.TabIndex = 0;
			this.AuditGrid.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.AuditGrid_BeforeSort);
			// 
			// TotalGrid
			// 
			this.TotalGrid.AllowSelection = false;
			this.TotalGrid.AllowUserToResizeColumns = false;
			this.TotalGrid.AllowUserToResizeRows = false;
			this.TotalGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.TotalGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.TotalGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.TotalGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.TotalGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.TotalGrid.BackColorSel = System.Drawing.Color.Empty;
			this.TotalGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.TotalGrid.Cols = 7;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.TotalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.TotalGrid.ColumnHeadersHeight = 30;
			this.TotalGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.TotalGrid.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.TotalGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.TotalGrid.DragIcon = null;
			this.TotalGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.TotalGrid.ExtendLastCol = true;
			this.TotalGrid.FixedCols = 0;
			this.TotalGrid.FixedRows = 0;
			this.TotalGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.TotalGrid.FrozenCols = 0;
			this.TotalGrid.GridColor = System.Drawing.Color.Empty;
			this.TotalGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.TotalGrid.Location = new System.Drawing.Point(0, 312);
			this.TotalGrid.Name = "TotalGrid";
			this.TotalGrid.ReadOnly = true;
			this.TotalGrid.RowHeadersVisible = false;
			this.TotalGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.TotalGrid.RowHeightMin = 0;
			this.TotalGrid.Rows = 50;
			this.TotalGrid.ScrollTipText = null;
			this.TotalGrid.ShowColumnVisibilityMenu = false;
			this.TotalGrid.ShowFocusCell = false;
			this.TotalGrid.Size = new System.Drawing.Size(1006, 45);
			this.TotalGrid.StandardTab = true;
			this.TotalGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.TotalGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.TotalGrid.TabIndex = 1;
			// 
			// CategoryGrid
			// 
			this.CategoryGrid.AllowSelection = false;
			this.CategoryGrid.AllowUserToResizeColumns = false;
			this.CategoryGrid.AllowUserToResizeRows = false;
			this.CategoryGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.CategoryGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.CategoryGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.CategoryGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.CategoryGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.CategoryGrid.BackColorSel = System.Drawing.Color.Empty;
			this.CategoryGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.CategoryGrid.Cols = 10;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.CategoryGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.CategoryGrid.ColumnHeadersHeight = 30;
			this.CategoryGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.CategoryGrid.ColumnHeadersVisible = false;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.CategoryGrid.DefaultCellStyle = dataGridViewCellStyle6;
			this.CategoryGrid.DragIcon = null;
			this.CategoryGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.CategoryGrid.ExtendLastCol = true;
			this.CategoryGrid.FixedCols = 0;
			this.CategoryGrid.FixedRows = 0;
			this.CategoryGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.CategoryGrid.FrozenCols = 0;
			this.CategoryGrid.GridColor = System.Drawing.Color.Empty;
			this.CategoryGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.CategoryGrid.Location = new System.Drawing.Point(0, 360);
			this.CategoryGrid.Name = "CategoryGrid";
			this.CategoryGrid.ReadOnly = true;
			this.CategoryGrid.RowHeadersVisible = false;
			this.CategoryGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.CategoryGrid.RowHeightMin = 0;
			this.CategoryGrid.Rows = 3;
			this.CategoryGrid.ScrollTipText = null;
			this.CategoryGrid.ShowColumnVisibilityMenu = false;
			this.CategoryGrid.ShowFocusCell = false;
			this.CategoryGrid.Size = new System.Drawing.Size(1006, 125);
			this.CategoryGrid.StandardTab = true;
			this.CategoryGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.CategoryGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.CategoryGrid.TabIndex = 2;
			// 
			// GridBETEExempt
			// 
			this.GridBETEExempt.AllowSelection = false;
			this.GridBETEExempt.AllowUserToResizeColumns = false;
			this.GridBETEExempt.AllowUserToResizeRows = false;
			this.GridBETEExempt.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridBETEExempt.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridBETEExempt.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridBETEExempt.BackColorBkg = System.Drawing.Color.Empty;
			this.GridBETEExempt.BackColorFixed = System.Drawing.Color.Empty;
			this.GridBETEExempt.BackColorSel = System.Drawing.Color.Empty;
			this.GridBETEExempt.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridBETEExempt.Cols = 10;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridBETEExempt.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.GridBETEExempt.ColumnHeadersHeight = 30;
			this.GridBETEExempt.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridBETEExempt.ColumnHeadersVisible = false;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridBETEExempt.DefaultCellStyle = dataGridViewCellStyle8;
			this.GridBETEExempt.DragIcon = null;
			this.GridBETEExempt.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridBETEExempt.ExtendLastCol = true;
			this.GridBETEExempt.FixedCols = 0;
			this.GridBETEExempt.FixedRows = 0;
			this.GridBETEExempt.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridBETEExempt.FrozenCols = 0;
			this.GridBETEExempt.GridColor = System.Drawing.Color.Empty;
			this.GridBETEExempt.GridColorFixed = System.Drawing.Color.Empty;
			this.GridBETEExempt.Location = new System.Drawing.Point(0, 506);
			this.GridBETEExempt.Name = "GridBETEExempt";
			this.GridBETEExempt.ReadOnly = true;
			this.GridBETEExempt.RowHeadersVisible = false;
			this.GridBETEExempt.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridBETEExempt.RowHeightMin = 0;
			this.GridBETEExempt.Rows = 3;
			this.GridBETEExempt.ScrollTipText = null;
			this.GridBETEExempt.ShowColumnVisibilityMenu = false;
			this.GridBETEExempt.ShowFocusCell = false;
			this.GridBETEExempt.Size = new System.Drawing.Size(1006, 125);
			this.GridBETEExempt.StandardTab = true;
			this.GridBETEExempt.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridBETEExempt.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridBETEExempt.TabIndex = 4;
			// 
			// Label4
			// 
			this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.Label4.Location = new System.Drawing.Point(0, 490);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(95, 20);
			this.Label4.TabIndex = 3;
			this.Label4.Text = "BETE EXEMPT";
			// 
			// txtCommunicate
			// 
			this.txtCommunicate.AutoSize = false;
			this.txtCommunicate.BackColor = System.Drawing.SystemColors.Window;
			this.txtCommunicate.LinkItem = null;
			this.txtCommunicate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCommunicate.LinkTopic = null;
			this.txtCommunicate.Location = new System.Drawing.Point(0, 24);
			this.txtCommunicate.Name = "txtCommunicate";
			this.txtCommunicate.Size = new System.Drawing.Size(29, 40);
			this.txtCommunicate.TabIndex = 2;
			this.txtCommunicate.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 60);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(529, 18);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "DRAG COLUMNS WITH THE LEFT MOUSE BUTTON TO MAKE THEM WIDER OR NARROWER";
			// 
			// lblDoubleClick
			// 
			this.lblDoubleClick.Location = new System.Drawing.Point(30, 30);
			this.lblDoubleClick.Name = "lblDoubleClick";
			this.lblDoubleClick.Size = new System.Drawing.Size(527, 18);
			this.lblDoubleClick.TabIndex = 0;
			this.lblDoubleClick.Text = "DOUBLE-CLICK ON A ROW TO EDIT THE ACCOUNT IN PERSONAL PROPERTY";
			// 
			// mnuPrintSingleLine
			// 
			this.mnuPrintSingleLine.Index = 1;
			this.mnuPrintSingleLine.Name = "mnuPrintSingleLine";
			this.mnuPrintSingleLine.Text = "Print Single Line Version";
			this.mnuPrintSingleLine.Click += new System.EventHandler(this.mnuPrintSingleLine_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuView,
				this.mnuPrintSingleLine,
				this.mnuPrint,
				this.mnuSepar1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuView
			// 
			this.mnuView.Index = 0;
			this.mnuView.Name = "mnuView";
			this.mnuView.Text = "View Detail";
			this.mnuView.Visible = false;
			this.mnuView.Click += new System.EventHandler(this.mnuView_Click);
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 2;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 3;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(104, 35);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(89, 48);
			this.cmdPrint.TabIndex = 0;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdPrintSingleLineVersion
			// 
			this.cmdPrintSingleLineVersion.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintSingleLineVersion.AppearanceKey = "toolbarButton";
			this.cmdPrintSingleLineVersion.Location = new System.Drawing.Point(883, 29);
			this.cmdPrintSingleLineVersion.Name = "cmdPrintSingleLineVersion";
			this.cmdPrintSingleLineVersion.Size = new System.Drawing.Size(167, 25);
			this.cmdPrintSingleLineVersion.TabIndex = 1;
			this.cmdPrintSingleLineVersion.Text = "Print Single Line Version";
			this.cmdPrintSingleLineVersion.Click += new System.EventHandler(this.mnuPrintSingleLine_Click);
			// 
			// cmdView
			// 
			this.cmdView.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdView.AppearanceKey = "toolbarButton";
			this.cmdView.Location = new System.Drawing.Point(792, 29);
			this.cmdView.Name = "cmdView";
			this.cmdView.Size = new System.Drawing.Size(88, 25);
			this.cmdView.TabIndex = 2;
			this.cmdView.Text = "View Detail";
			this.cmdView.Visible = false;
			this.cmdView.Click += new System.EventHandler(this.mnuView_Click);
			// 
			// frmPPAudit
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 868);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPAudit";
			this.ShowInTaskbar = false;
			this.Text = "Personal Property Audit";
			this.Load += new System.EventHandler(this.frmPPAudit_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPAudit_KeyDown);
            this.Resize += new System.EventHandler(this.frmPPAudit_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framSummary)).EndInit();
			this.framSummary.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSummaryCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSummaryBETEExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.AuditGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TotalGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CategoryGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridBETEExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintSingleLineVersion)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdView)).EndInit();
			this.ResumeLayout(false);
		}

        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdPrint;
		private FCButton cmdPrintSingleLineVersion;
	}
}