﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	public class modCustomReport
	{
		public const int GRIDTEXT = 1;
		public const int GRIDDATE = 2;
		public const int GRIDCOMBOIDTEXT = 3;
		public const int GRIDNUMRANGE = 4;
		public const int GRIDCOMBOIDNUM = 5;
		public const int GRIDCOMBOTEXT = 6;
		public const int GRIDTEXTRANGE = 7;
		public const int GRIDBOOLEAN = 8;

		public struct CustomReportData
		{
			public int Code;
			public string Field;
			public bool SpecialCase;
			public int Row;
			public int Col;
			public int Type;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CustomReportData(int unusedParam)
			{
				this.Code = 0;
				this.Field = string.Empty;
				this.SpecialCase = false;
				this.Row = 0;
				this.Col = 0;
				this.Type = 0;
			}
		};

		public const string CUSTOMREPORTDATABASE = "Twck0000.vb1";
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string FixQuotes(string strValue)
		{
			string FixQuotes = "";
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void SetColumnCaptions(dynamic FormName)
		{
			// THIS ROUTINE GETS THE CAPTIONS THAT ARE TO BE DISPLAYED ON
			// THE REPORT ITSELF
			// THIS ARRAY (strFieldCaptions) HOLDS ONLY THE FIELD NAMES THAT ARE
			// TO BE DISPLAYED WHERE THE ARRAY (strCaptions) HOLDS ALL POSSIBLE
			// FIELD NAMES
			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				if (FormName.lstFields.Selected(intCounter))
				{
					Statics.strFieldCaptions[intCount] = Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}

		public static void GetNumberOfFields(string strSQL)
		{
			// THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
			// ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
			// TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
			// ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			object strTemp2;
			int intCount;
			int intCounter;
			if (Strings.Trim(strSQL) == string.Empty)
			{
				Statics.intNumberOfSQLFields = -1;
				return;
			}
			strTemp = Strings.Split(strSQL, "from", -1, CompareConstants.vbBinaryCompare);
			strSQL = Strings.Mid(strTemp[0], 7, FCConvert.ToString(strTemp[0]).Length - 6);
			strTemp = Strings.Split(strSQL, ",", -1, CompareConstants.vbBinaryCompare);
			Statics.intNumberOfSQLFields = Information.UBound(strTemp, 1);
			for (intCounter = 0; intCounter <= Statics.intNumberOfSQLFields; intCounter++)
			{
				if (Strings.Trim(strTemp[intCounter]) != string.Empty)
				{
					// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
					Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
					// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
					// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
					for (intCount = 0; intCount <= Statics.intNumberOfSQLFields; intCount++)
					{
						if (Statics.strFields[intCount] == Strings.Trim(strTemp[intCounter]))
						{
							Statics.strFieldCaptions[intCounter] = Statics.strCaptions[intCount];
							break;
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object CheckForAS(string strFieldName, short intSegment = 2)
		{
			object CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (Strings.UCase(strTemp[1]) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					if (intSegment == 1)
					{
						CheckForAS = strTemp[0];
					}
					else
					{
						CheckForAS = strTemp[2];
					}
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					CheckForAS = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				CheckForAS = Strings.Trim(strTemp[0]);
			}
			return CheckForAS;
		}
		/// <summary>
		/// Public Sub ClearComboListArray()
		/// </summary>
		/// <summary>
		/// CLEAR THE COMBO LIST ARRAY.
		/// </summary>
		/// <summary>
		/// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Dim intCount As Integer
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// For intCount = 0 To 50
		/// </summary>
		/// <summary>
		/// strComboList(intCount, 0) = vbNullString
		/// </summary>
		/// <summary>
		/// strComboList(intCount, 1) = vbNullString
		/// </summary>
		/// <summary>
		/// Next
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		/// Public Sub SetFormFieldCaptions(FormName As Form, ByVal ReportType As String)
		/// </summary>
		/// <summary>
		/// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// ****************************************************************
		/// </summary>
		/// <summary>
		/// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
		/// </summary>
		/// <summary>
		/// TO ADD A NEW REPORT TYPE.
		/// </summary>
		/// <summary>
		/// ****************************************************************
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// strReportType = UCase(ReportType)
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// CLEAR THE COMBO LIST ARRAY
		/// </summary>
		/// <summary>
		/// Call ClearComboListArray
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Select Case UCase(ReportType)
		/// </summary>
		/// <summary>
		/// Case "MORTGAGEHOLDER"
		/// </summary>
		/// <summary>
		/// Call SetMortgageHolderParameters(FormName)
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// End Select
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
		/// </summary>
		/// <summary>
		/// Call LoadSortList(FormName)
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
		/// </summary>
		/// <summary>
		/// Call LoadWhereGrid(FormName)
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// LOAD THE SAVED REPORT COMBO ON THE FORM
		/// </summary>
		/// <summary>
		/// CallByName FormName, "LoadCombo", VbMethod
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Public Sub SetHeaderGridProperties(GridName As Object)
		/// </summary>
		/// <summary>
		/// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
		/// </summary>
		/// <summary>
		/// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Select Case UCase(strReportType)
		/// </summary>
		/// <summary>
		/// Case "DOGS"
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Case "BIRTHS"
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Case Else
		/// </summary>
		/// <summary>
		/// GridName.BackColor = vbBlue
		/// </summary>
		/// <summary>
		/// GridName.ForeColor = vbWhite
		/// </summary>
		/// <summary>
		/// End Select
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// GridName.Select 0, 0, 0, GridName.Cols - 1
		/// </summary>
		/// <summary>
		/// GridName.CellFontBold = True
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Public Sub SetDataGridProperties(GridName As Object)
		/// </summary>
		/// <summary>
		/// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
		/// </summary>
		/// <summary>
		/// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Select Case UCase(strReportType)
		/// </summary>
		/// <summary>
		/// Case "DOGS"
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Case "BIRTHS"
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// End Select
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private static void LoadGridCellAsCombo(ref Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, "twre0000.vb1");
				while (!rsCombo.EndOfFile())
				{
					Statics.strComboList[intRowNumber, 0] += "|";
					Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
					Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
					rsCombo.MoveNext();
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool IsValidDate(string DateToCheck)
		{
			bool IsValidDate = false;
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As short --> As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As short --> As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							MessageBox.Show("Invalid Month on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							MessageBox.Show("Invalid day on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							MessageBox.Show("Invalid day on Date", "ErTRIO Softwarevror", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}

		public class StaticVariables
		{
			//=========================================================
			public int intNumberOfSQLFields;
			public int[] strFieldWidth = new int[50 + 1];
			public string strPreSetReport = string.Empty;
			public string strCustomSQL = "";
			public string strColumnCaptions = "";
			public string[] strFields = new string[50 + 1];
			public string[] strFieldNames = new string[50 + 1];
			public string[] strFieldCaptions = new string[50 + 1];
			public string[] strCaptions = new string[50 + 1];
			public string strCustomTitle = string.Empty;
			public string strReportType = "";
			public string[,] strComboList = new string[50 + 1, 50 + 1];
			public string[] strWhereType = new string[50 + 1];
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
