﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptOutprintSummary.
	/// </summary>
	partial class rptOutprintSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptOutprintSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOriginalRETax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalPPTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtREPrePay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPPPrePay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalREDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPPDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrePay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNetRE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNetPP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNewOwners = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMortgageHolders = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCopiesDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCopiesOrigDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtInterestedParties = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalRETax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalPPTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtREPrePay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPPPrePay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalREDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPPDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetRE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetPP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewOwners)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMortgageHolders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCopiesDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCopiesOrigDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterestedParties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.txtOriginalRETax,
				this.txtOriginalPPTax,
				this.txtREPrePay,
				this.txtPPPrePay,
				this.txtTotalREDue,
				this.txtTotalPPDue,
				this.txtOriginalTax,
				this.txtPrePay,
				this.txtTotalDue,
				this.Line1,
				this.Label6,
				this.txtMuniName,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.txtTitle,
				this.txtInfo,
				this.Label7,
				this.txtNetRE,
				this.txtNetPP,
				this.txtNet,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.txtNewOwners,
				this.txtMortgageHolders,
				this.txtTotalCopiesDue,
				this.txtTotalCopiesOrigDue,
				this.Label12,
				this.txtInterestedParties
			});
			this.Detail.Height = 3.416667F;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.01041667F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "color: Black; font-weight: bold; text-align: right";
			this.Label1.Text = "Real Estate file (RE and Comb. RE/PP Accounts)";
			this.Label1.Top = 1.364583F;
			this.Label1.Width = 3.427083F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.25F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.01041667F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "color: Black; font-weight: bold; text-align: right";
			this.Label2.Text = "Personal Property file (PP only Accounts)";
			this.Label2.Top = 1.635417F;
			this.Label2.Width = 3.427083F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.3333333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 4.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "color: Black; font-weight: bold; text-align: center";
			this.Label3.Text = "Original Tax Due";
			this.Label3.Top = 0.9375F;
			this.Label3.Width = 0.90625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.3333333F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 5.65625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "color: Black; font-weight: bold; text-align: center";
			this.Label4.Text = "Pre Payments";
			this.Label4.Top = 0.9375F;
			this.Label4.Width = 0.8229167F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.3333333F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 6.489583F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "color: Black; font-weight: bold; text-align: center";
			this.Label5.Text = "Total Tax Due";
			this.Label5.Top = 0.9375F;
			this.Label5.Width = 0.9895833F;
			// 
			// txtOriginalRETax
			// 
			this.txtOriginalRETax.Height = 0.1770833F;
			this.txtOriginalRETax.Left = 4.5625F;
			this.txtOriginalRETax.Name = "txtOriginalRETax";
			this.txtOriginalRETax.Style = "color: Black; text-align: right";
			this.txtOriginalRETax.Text = null;
			this.txtOriginalRETax.Top = 1.364583F;
			this.txtOriginalRETax.Width = 0.9791667F;
			// 
			// txtOriginalPPTax
			// 
			this.txtOriginalPPTax.Height = 0.1770833F;
			this.txtOriginalPPTax.Left = 4.5625F;
			this.txtOriginalPPTax.Name = "txtOriginalPPTax";
			this.txtOriginalPPTax.Style = "color: Black; text-align: right";
			this.txtOriginalPPTax.Text = "Field1";
			this.txtOriginalPPTax.Top = 1.625F;
			this.txtOriginalPPTax.Width = 0.9791667F;
			// 
			// txtREPrePay
			// 
			this.txtREPrePay.Height = 0.1770833F;
			this.txtREPrePay.Left = 5.572917F;
			this.txtREPrePay.Name = "txtREPrePay";
			this.txtREPrePay.Style = "color: Black; text-align: right";
			this.txtREPrePay.Text = "Field1";
			this.txtREPrePay.Top = 1.364583F;
			this.txtREPrePay.Width = 0.8958333F;
			// 
			// txtPPPrePay
			// 
			this.txtPPPrePay.Height = 0.1770833F;
			this.txtPPPrePay.Left = 5.572917F;
			this.txtPPPrePay.Name = "txtPPPrePay";
			this.txtPPPrePay.Style = "color: Black; text-align: right";
			this.txtPPPrePay.Text = "Field1";
			this.txtPPPrePay.Top = 1.625F;
			this.txtPPPrePay.Width = 0.8958333F;
			// 
			// txtTotalREDue
			// 
			this.txtTotalREDue.Height = 0.1770833F;
			this.txtTotalREDue.Left = 6.489583F;
			this.txtTotalREDue.Name = "txtTotalREDue";
			this.txtTotalREDue.Style = "color: Black; text-align: right";
			this.txtTotalREDue.Text = "Field1";
			this.txtTotalREDue.Top = 1.354167F;
			this.txtTotalREDue.Width = 0.9791667F;
			// 
			// txtTotalPPDue
			// 
			this.txtTotalPPDue.Height = 0.1770833F;
			this.txtTotalPPDue.Left = 6.489583F;
			this.txtTotalPPDue.Name = "txtTotalPPDue";
			this.txtTotalPPDue.Style = "color: Black; text-align: right";
			this.txtTotalPPDue.Text = "Field1";
			this.txtTotalPPDue.Top = 1.614583F;
			this.txtTotalPPDue.Width = 0.9791667F;
			// 
			// txtOriginalTax
			// 
			this.txtOriginalTax.Height = 0.1770833F;
			this.txtOriginalTax.Left = 4.5625F;
			this.txtOriginalTax.Name = "txtOriginalTax";
			this.txtOriginalTax.Style = "color: Black; text-align: right";
			this.txtOriginalTax.Text = "Field1";
			this.txtOriginalTax.Top = 1.958333F;
			this.txtOriginalTax.Width = 0.9791667F;
			// 
			// txtPrePay
			// 
			this.txtPrePay.Height = 0.1770833F;
			this.txtPrePay.Left = 5.572917F;
			this.txtPrePay.Name = "txtPrePay";
			this.txtPrePay.Style = "color: Black; text-align: right";
			this.txtPrePay.Text = "Field1";
			this.txtPrePay.Top = 1.958333F;
			this.txtPrePay.Width = 0.8958333F;
			// 
			// txtTotalDue
			// 
			this.txtTotalDue.Height = 0.1770833F;
			this.txtTotalDue.Left = 6.489583F;
			this.txtTotalDue.Name = "txtTotalDue";
			this.txtTotalDue.Style = "color: Black; text-align: right";
			this.txtTotalDue.Text = "Field1";
			this.txtTotalDue.Top = 1.947917F;
			this.txtTotalDue.Width = 0.9791667F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 4.15625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.90625F;
			this.Line1.Width = 3.28125F;
			this.Line1.X1 = 4.15625F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1.90625F;
			this.Line1.Y2 = 1.90625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.25F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.2604167F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "color: Black; font-weight: bold; text-align: right";
			this.Label6.Text = "Total";
			this.Label6.Top = 1.96875F;
			this.Label6.Width = 3.177083F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.02083333F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "color: Black";
			this.txtMuniName.Text = "Field1";
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2.96875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.02083333F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "color: Black";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.885417F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "color: Black; text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.21875F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "color: Black; text-align: right";
			this.txtPage.Text = "Page 1";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.21875F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.25F;
			this.txtTitle.Left = 2.416667F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "color: Black; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Outprint Summary";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 2.78125F;
			// 
			// txtInfo
			// 
			this.txtInfo.Height = 0.1875F;
			this.txtInfo.Left = 2.333333F;
			this.txtInfo.Name = "txtInfo";
			this.txtInfo.Style = "color: Black; text-align: center";
			this.txtInfo.Text = "Field1";
			this.txtInfo.Top = 0.3333333F;
			this.txtInfo.Width = 2.96875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.3333333F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3.583333F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "color: Black; font-weight: bold; text-align: center";
			this.Label7.Text = "Net Assessment";
			this.Label7.Top = 0.9375F;
			this.Label7.Width = 0.90625F;
			// 
			// txtNetRE
			// 
			this.txtNetRE.Height = 0.1770833F;
			this.txtNetRE.Left = 3.510417F;
			this.txtNetRE.Name = "txtNetRE";
			this.txtNetRE.Style = "color: Black; text-align: right";
			this.txtNetRE.Text = null;
			this.txtNetRE.Top = 1.364583F;
			this.txtNetRE.Width = 0.9791667F;
			// 
			// txtNetPP
			// 
			this.txtNetPP.Height = 0.1770833F;
			this.txtNetPP.Left = 3.510417F;
			this.txtNetPP.Name = "txtNetPP";
			this.txtNetPP.Style = "color: Black; text-align: right";
			this.txtNetPP.Text = null;
			this.txtNetPP.Top = 1.625F;
			this.txtNetPP.Width = 0.9791667F;
			// 
			// txtNet
			// 
			this.txtNet.Height = 0.1770833F;
			this.txtNet.Left = 3.510417F;
			this.txtNet.Name = "txtNet";
			this.txtNet.Style = "color: Black; text-align: right";
			this.txtNet.Text = null;
			this.txtNet.Top = 1.958333F;
			this.txtNet.Width = 0.9791667F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1979167F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 1.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "color: Black; font-weight: bold; text-align: right";
			this.Label8.Text = "New Owner Copies";
			this.Label8.Top = 2.53125F;
			this.Label8.Width = 2.010417F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1979167F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.25F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "color: Black; font-weight: bold; text-align: right";
			this.Label9.Text = "Mortgage Holder Copies";
			this.Label9.Top = 2.729167F;
			this.Label9.Width = 2.010417F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1979167F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.25F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "color: Black; font-weight: bold; text-align: right";
			this.Label10.Text = "Tax Due";
			this.Label10.Top = 2.927083F;
			this.Label10.Width = 2.010417F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1979167F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.08333334F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "color: Black; font-weight: bold; text-align: right";
			this.Label11.Text = "Total Tax Due in File (Copies and Originals)";
			this.Label11.Top = 3.125F;
			this.Label11.Width = 3.177083F;
			// 
			// txtNewOwners
			// 
			this.txtNewOwners.Height = 0.1770833F;
			this.txtNewOwners.Left = 3.510417F;
			this.txtNewOwners.Name = "txtNewOwners";
			this.txtNewOwners.Style = "color: Black; text-align: right";
			this.txtNewOwners.Text = null;
			this.txtNewOwners.Top = 2.541667F;
			this.txtNewOwners.Width = 0.9791667F;
			// 
			// txtMortgageHolders
			// 
			this.txtMortgageHolders.Height = 0.1770833F;
			this.txtMortgageHolders.Left = 3.510417F;
			this.txtMortgageHolders.Name = "txtMortgageHolders";
			this.txtMortgageHolders.Style = "color: Black; text-align: right";
			this.txtMortgageHolders.Text = null;
			this.txtMortgageHolders.Top = 2.739583F;
			this.txtMortgageHolders.Width = 0.9791667F;
			// 
			// txtTotalCopiesDue
			// 
			this.txtTotalCopiesDue.Height = 0.1770833F;
			this.txtTotalCopiesDue.Left = 3.510417F;
			this.txtTotalCopiesDue.Name = "txtTotalCopiesDue";
			this.txtTotalCopiesDue.Style = "color: Black; text-align: right";
			this.txtTotalCopiesDue.Text = null;
			this.txtTotalCopiesDue.Top = 2.9375F;
			this.txtTotalCopiesDue.Width = 0.9791667F;
			// 
			// txtTotalCopiesOrigDue
			// 
			this.txtTotalCopiesOrigDue.Height = 0.1770833F;
			this.txtTotalCopiesOrigDue.Left = 3.510417F;
			this.txtTotalCopiesOrigDue.Name = "txtTotalCopiesOrigDue";
			this.txtTotalCopiesOrigDue.Style = "color: Black; text-align: right";
			this.txtTotalCopiesOrigDue.Text = null;
			this.txtTotalCopiesOrigDue.Top = 3.135417F;
			this.txtTotalCopiesOrigDue.Width = 0.9791667F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1979167F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.25F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "color: Black; font-weight: bold; text-align: right";
			this.Label12.Text = "Interested Party Copies";
			this.Label12.Top = 2.336806F;
			this.Label12.Width = 2.010417F;
			// 
			// txtInterestedParties
			// 
			this.txtInterestedParties.Height = 0.1770833F;
			this.txtInterestedParties.Left = 3.510417F;
			this.txtInterestedParties.Name = "txtInterestedParties";
			this.txtInterestedParties.Style = "color: Black; text-align: right";
			this.txtInterestedParties.Text = null;
			this.txtInterestedParties.Top = 2.347222F;
			this.txtInterestedParties.Width = 0.9791667F;
			// 
			// rptOutprintSummary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Tahoma\'", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalRETax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalPPTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtREPrePay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPPPrePay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalREDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPPDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetRE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetPP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewOwners)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMortgageHolders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCopiesDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCopiesOrigDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterestedParties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalRETax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalPPTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtREPrePay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPPPrePay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalREDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPPDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrePay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfo;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetRE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetPP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewOwners;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMortgageHolders;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCopiesDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCopiesOrigDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInterestedParties;
	}
}
