﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptRECollectionList.
	/// </summary>
	public partial class rptRECollectionList : BaseSectionReport
	{
		public rptRECollectionList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate Collection List";
		}

		public static rptRECollectionList InstancePtr
		{
			get
			{
				return (rptRECollectionList)Sys.GetInstance(typeof(rptRECollectionList));
			}
		}

		protected rptRECollectionList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				clsCommit.Dispose();
            }

			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRECollectionList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		/// <summary>
		/// this code is cut and pasted from the commitment report.  The only difference is the addition of a discount
		/// </summary>
		int TaxYear;
		clsDRWrapper clsCommit = new clsDRWrapper();
		string Addr1 = "";
		string Addr2 = "";
		string addr3 = "";
		string strMailingAddress3 = "";
		int intPage;
		int intStartPage;
		clsExempt[] exemptarray = null;
		double lngLandSub;
		double lngBldgSub;
		double lngExemptsub;
		double lngAssessSub;
		double lngTaxSub;
		double dblDisc;
		/// <summary>
		/// discount percentage
		/// </summary>
		double dblTax;
		//clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		int lngPage;
		bool boolIncludeAddress;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: x As short --> As int	OnWrite(short, double)
			int x;
			lngPage = 1;
			lngLandSub = 0;
			lngBldgSub = 0;
			lngExemptsub = 0;
			lngAssessSub = 0;
			lngTaxSub = 0;
			// add these fields so the report can do the subtotals for us
			Fields.Add("landval");
			Fields.Add("bldgval");
			Fields.Add("exemptval");
			Fields.Add("Totalval");
			Fields.Add("TaxVal");
			this.Fields.Add("TheBinder");
			txtDate.Text = DateTime.Today.ToShortDateString();
			clsTemp.OpenRecordset("select max(code) as maxcode from exemptcode", "twre0000.vb1");
			// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
			exemptarray = new clsExempt[FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxcode"))) + 1];
			// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
			for (x = 1; x <= FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxcode"))); x++)
			{
				exemptarray[x] = new clsExempt();
				exemptarray[x].Description = "Invalid Code";
				exemptarray[x].ShortDescription = "Bad Code";
				exemptarray[x].Unit = 0;
			}
			// x
			clsTemp.OpenRecordset("select * from exemptcode ORDER BY CODE", "twre0000.vb1");
			// ReDim exemptarray(1)
			while (!clsTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				x = FCConvert.ToInt32(clsTemp.Get_Fields("code"));
				exemptarray[x].Description = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				exemptarray[x].ShortDescription = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("shortdescription")));
				// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
				//FC:FINAL:CHN - issue #1361: Missing explicit convert.
				// exemptarray[x].Unit = Conversion.Val(clsTemp.Get_Fields("amount")));
				exemptarray[x].Unit = FCConvert.ToInt32(clsTemp.Get_Fields_Double("amount"));
				clsTemp.MoveNext();
			}
			clsTemp.Dispose();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// if no more records, then end the report
			eArgs.EOF = clsCommit.EndOfFile();
			if (!eArgs.EOF)
			{
				FillValueFields();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
			
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			// fill in all the fields
			if (!clsCommit.EndOfFile())
			{
				double dblDiscount = 0;
				// vbPorter upgrade warning: lngAssess As int	OnWriteFCConvert.ToDouble(
				int lngAssess = 0;
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = clsCommit.Get_Fields_String("account");
				txtName.Text = clsCommit.Get_Fields_String("name1");
				txtLocation.Text = "";
				txtMapLot.Text = "";
				txtBookPage.Text = "";
				txtLand.Value = Conversion.Val(clsCommit.Get_Fields_Int32("landvalue"));
				txtBldg.Value = Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue"));
				txtExempt.Value = Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
				lngExemptsub += Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
				lngLandSub += Conversion.Val(clsCommit.Get_Fields_Int32("landvalue"));
				lngBldgSub += Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue"));
				lngAssess = FCConvert.ToInt32(Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue")) + Conversion.Val(clsCommit.Get_Fields_Int32("landvalue")));
				txtTotal.Value = lngAssess;
				txtTax.Value = Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
				lngTaxSub += Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
				if (boolIncludeAddress)
				{
					Addr1 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address1")));
					Addr2 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address2")));
					addr3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("address3")));
					strMailingAddress3 = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("MailingAddress3")));
				}
				else
				{
					Addr1 = "";
					Addr2 = "";
					addr3 = "";
					strMailingAddress3 = "";
				}
				// if there are blank address lines condense them
				if (Strings.Trim(Addr1) == string.Empty)
				{
					Addr1 = Addr2;
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = "";
					if (Strings.Trim(Addr1) == string.Empty)
					{
						Addr1 = Addr2;
						Addr2 = strMailingAddress3;
						strMailingAddress3 = addr3;
						addr3 = "";
					}
				}
				if (Strings.Trim(Addr2) == string.Empty)
				{
					Addr2 = strMailingAddress3;
					strMailingAddress3 = addr3;
					addr3 = "";
				}
				dblTax = Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
				dblDiscount = modMain.Round(dblDisc * dblTax, 2);
				txtDiscount.Text = Strings.Format(dblDiscount, "#,###,###,##0.00");
				if (boolIncludeAddress)
				{
					txtAddress1.Text = Addr1;
					txtAddress2.Text = Addr2;
					txtAddress3.Text = addr3;
					txtMailingAddress3.Text = strMailingAddress3;
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtLocation.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
					txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("maplot")));
					txtBookPage.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("bookpage")));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtAddress1.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
					txtAddress2.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("maplot")));
					txtMailingAddress3.Text = Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("bookpage")));
					txtAddress3.Text = "";
				}
				txtAcres.Text = "";
				if (Conversion.Val(clsCommit.Get_Fields_Double("acres")) > 0)
				{
					txtAcres.Text = FCConvert.ToString(Conversion.Val(clsCommit.Get_Fields_Double("acres"))) + " Acres";
				}
				// zero out the exemption fields and then fill them if there are exemptions
				txtExempt1.Text = "";
				txtExempt2.Text = "";
				txtExempt3.Text = "";
				if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt1")) > 0)
				{
					txtExempt1.Text = Strings.Format(clsCommit.Get_Fields_Int32("exempt1"), "00") + " " + exemptarray[clsCommit.Get_Fields_Int32("exempt1")].Description;
					if (exemptarray[clsCommit.Get_Fields_Int32("exempt1")].Unit == 0)
					{
					}
					else
					{
						if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt2")) > 0)
						{
							txtExempt2.Text = Strings.Format(clsCommit.Get_Fields_Int32("exempt2"), "00") + " " + exemptarray[clsCommit.Get_Fields_Int32("exempt2")].Description;
						}
					}
					if (Conversion.Val(clsCommit.Get_Fields_Int32("exempt3")) > 0)
					{
						txtExempt3.Text = Strings.Format(clsCommit.Get_Fields_Int32("exempt3"), "00") + " " + exemptarray[clsCommit.Get_Fields_Int32("exempt3")].Description;
					}
				}
				// If Val(txtLandTot.Text) > 0 Then
				// lngLandSub = lngLandSub + CLng(txtLandTot.Text)
				// End If
				// If Val(txtBldgTot.Text) > 0 Then
				// lngBldgSub = lngBldgSub + CLng(txtBldgTot.Text)
				// End If
				// If Val(txtExemptTot.Text) > 0 Then
				// lngExemptsub = lngExemptsub + CLng(txtExemptTot.Text)
				// End If
				// If Val(txtTotTot.Text) > 0 Then
				// lngAssessSub = lngAssessSub + CLng(txtTotTot.Text)
				// End If
				// If Val(txtTaxTot.Text) > 0 Then
				// lngTaxSub = lngTaxSub + CDbl(txtTaxTot.Text)
				// End If
				// txtLandSub.Text = Format(lngLandSub, "#,###,###,##0")
				// txtBldgSub.Text = Format(lngBldgSub, "#,###,###,##0")
				// txtExemptSub.Text = Format(lngExemptsub, "#,###,###,##0")
				// txtTotalSub.Text = Format(lngAssessSub, "#,###,###,##0")
				// txtTaxSub.Text = Format(lngTaxSub, "#,###,###,##0.00")
				clsCommit.MoveNext();
			}
		}
		// vbPorter upgrade warning: lngTaxYear As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngTaxYear, bool boolSetFont, string strFontName, string strPrinterName, double dblDiscount, int intOrder, bool boolPrintAddress = true)
		{
			string strOrder = "";
			int x;
			// take in the printer and font to use as well as the taxyear to make the bill for
			TaxYear = lngTaxYear;
			if (dblDiscount == 0)
			{
				txtDiscount.Visible = false;
				lblDiscount.Visible = false;
			}
			else
			{
				dblDisc = modMain.Round((dblDiscount / 100), 2);
			}
			boolIncludeAddress = boolPrintAddress;
			if (!boolIncludeAddress)
			{
				this.Detail.Height -= (225 * 4 / 1440f);
			}
			switch (intOrder)
			{
				case 2:
					{
						strOrder = "name1";
						break;
					}
				case 1:
					{
						strOrder = "account";
						break;
					}
				case 3:
					{
						strOrder = "maplot";
						break;
					}
			}
			//end switch
			intStartPage = 1;
			clsCommit.OpenRecordset("select * from billingmaster where billingyear = " + FCConvert.ToString(TaxYear) + "1 and billingtype = 'RE' order by " + strOrder, "twcl0000.vb1");
			if (!clsCommit.EndOfFile())
			{
				lblTitle.Text = lblTitle.Text + FCConvert.ToString(TaxYear);
				lblMuniname.Text = modGlobalConstants.Statics.MuniName;
				if (boolSetFont)
				{
					// now set each box and label to the correct printer font
					for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
					{
						// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
						bool setFont = false;
						bool bold = false;
						if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "textbox")
						{
							setFont = true;
						}
						else if (FCConvert.ToString(this.Detail.Controls[x].Tag) == "bold")
						{
							setFont = true;
							bold = true;
						}
						if (setFont)
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textBox != null)
							{
								textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
								}
							}
						}
					}
					// x
					for (x = 0; x <= this.ReportFooter.Controls.Count - 1; x++)
					{
						// any field I marked with textbox must have its font changed. Bold is a field that also has bold font
						bool setFont = false;
						bool bold = false;
						if (FCConvert.ToString(this.ReportFooter.Controls[x].Tag) == "textbox")
						{
							setFont = true;
						}
						else if (FCConvert.ToString(this.ReportFooter.Controls[x].Tag) == "bold")
						{
							setFont = true;
							bold = true;
						}
						if (setFont)
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = this.ReportFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textBox != null)
							{
								textBox.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = this.ReportFooter.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(strFontName, 10, bold ? FontStyle.Bold : FontStyle.Regular);
								}
							}
						}
					}
					// x
					//this.Printer.RenderMode = 1;
				}
				// change to the selected printer
				this.Document.Printer.PrinterName = strPrinterName;
				// Me.Show , MDIParent
				//FC:FINAL:MSH - restore missing report call
				frmReportViewer.InstancePtr.Init(this, strPrinterName, boolAllowEmail: true, strAttachmentName: "RECollectionList");
			}
			else
			{
				// not records so leave
				MessageBox.Show("No records found.", "Nothing to print", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//MDIParent.InstancePtr.Show();
				this.Close();
			}
			return;
		}

		private void FillValueFields()
		{
			if (Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name1"))) != string.Empty)
			{
				this.Fields["TheBinder"].Value = Strings.Left(FCConvert.ToString(clsCommit.Get_Fields_String("name1")), 1);
			}
			else
			{
				this.Fields["TheBinder"].Value = "";
			}
			// filling these fields will fill the textboxes and the summary fields in the page footer
			this.Fields["landval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("landvalue"));
			this.Fields["bldgval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue"));
			this.Fields["exemptval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
			this.Fields["totalval"].Value = Conversion.Val(clsCommit.Get_Fields_Int32("landvalue")) + Conversion.Val(clsCommit.Get_Fields_Int32("buildingvalue")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
			// Me.Fields("totalval") = Val(clsCommit.Fields("landvalue")) + Val(clsCommit.Fields("buildingvalue"))
			this.Fields["taxval"].Value = Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			lngAssessSub = lngBldgSub - lngExemptsub + lngLandSub;
			txtLandSub.Text = Strings.Format(lngLandSub, "#,###,###,##0");
			txtBldgSub.Text = Strings.Format(lngBldgSub, "#,###,###,##0");
			txtExemptSub.Text = Strings.Format(lngExemptsub, "#,###,###,##0");
			txtTotalSub.Text = Strings.Format(lngAssessSub, "#,###,###,##0");
			txtTaxSub.Text = Strings.Format(lngTaxSub, "#,###,###,##0.00");
		}

		
	}
}
