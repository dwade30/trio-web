//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.IO.Compression;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmOutPrinting.
	/// </summary>
	partial class frmOutPrinting : BaseForm
	{
		public fecherFoundation.FCComboBox cmbFormat;
		public fecherFoundation.FCLabel lblFormat;
		public fecherFoundation.FCComboBox cmbModule;
		public fecherFoundation.FCLabel lblModule;
		public fecherFoundation.FCTextBox txtStateAidReduction;
		public Global.T2KDateBox t2kIndebtednessDate;
		public fecherFoundation.FCTextBox txtIndebtednessAmount;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkDiscount;
		public fecherFoundation.FCTextBox txtPercent;
		public fecherFoundation.FCTextBox txtDate;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCCheckBox chkInterestedParties;
		public fecherFoundation.FCCheckBox chkNewOwnerCopy;
		public fecherFoundation.FCCheckBox chkMHCopy;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkEnhanced;
		public fecherFoundation.FCRichTextBox RichTextBox1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkPrint;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		//public AbaleZipLibrary.AbaleZip AbaleZip1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuCreate;
		public fecherFoundation.FCToolStripMenuItem mnuCopy;
		public fecherFoundation.FCToolStripMenuItem mnuPrintFormat;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOutPrinting));
			this.cmbFormat = new fecherFoundation.FCComboBox();
			this.lblFormat = new fecherFoundation.FCLabel();
			this.cmbModule = new fecherFoundation.FCComboBox();
			this.lblModule = new fecherFoundation.FCLabel();
			this.txtStateAidReduction = new fecherFoundation.FCTextBox();
			this.t2kIndebtednessDate = new Global.T2KDateBox();
			this.txtIndebtednessAmount = new fecherFoundation.FCTextBox();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.chkDiscount = new fecherFoundation.FCCheckBox();
			this.txtPercent = new fecherFoundation.FCTextBox();
			this.txtDate = new fecherFoundation.FCTextBox();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.chkInterestedParties = new fecherFoundation.FCCheckBox();
			this.chkNewOwnerCopy = new fecherFoundation.FCCheckBox();
			this.chkMHCopy = new fecherFoundation.FCCheckBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.chkEnhanced = new fecherFoundation.FCCheckBox();
			this.RichTextBox1 = new fecherFoundation.FCRichTextBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkPrint = new fecherFoundation.FCCheckBox();
			this.txtYear = new fecherFoundation.FCTextBox();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCreate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCopy = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintFormat = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdCreate = new fecherFoundation.FCButton();
			this.cmdCopy = new fecherFoundation.FCButton();
			this.cmdPrintFormat = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kIndebtednessDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInterestedParties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNewOwnerCopy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMHCopy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEnhanced)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCopy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintFormat)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdCreate);
			this.BottomPanel.Location = new System.Drawing.Point(0, 809);
			this.BottomPanel.Size = new System.Drawing.Size(1072, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtStateAidReduction);
			this.ClientArea.Controls.Add(this.t2kIndebtednessDate);
			this.ClientArea.Controls.Add(this.txtIndebtednessAmount);
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.RichTextBox1);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.txtYear);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1072, 749);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrintFormat);
			this.TopPanel.Controls.Add(this.cmdCopy);
			this.TopPanel.Size = new System.Drawing.Size(1072, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdCopy, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintFormat, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(142, 30);
			this.HeaderText.Text = "Out Printing";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbFormat
			// 
			this.cmbFormat.AutoSize = false;
			this.cmbFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFormat.FormattingEnabled = true;
			this.cmbFormat.Items.AddRange(new object[] {
				"Fixed Length (Original)",
				"Comma Delimited"
			});
			this.cmbFormat.Location = new System.Drawing.Point(138, 30);
			this.cmbFormat.Name = "cmbFormat";
			this.cmbFormat.Size = new System.Drawing.Size(187, 40);
			this.cmbFormat.TabIndex = 1;
			this.cmbFormat.Text = "Comma Delimited";
			this.ToolTip1.SetToolTip(this.cmbFormat, null);
			this.cmbFormat.SelectedIndexChanged += new System.EventHandler(this.cmbFormat_SelectedIndexChanged);
			// 
			// lblFormat
			// 
			this.lblFormat.AutoSize = true;
			this.lblFormat.Location = new System.Drawing.Point(20, 44);
			this.lblFormat.Name = "lblFormat";
			this.lblFormat.Size = new System.Drawing.Size(60, 15);
			this.lblFormat.TabIndex = 0;
			this.lblFormat.Text = "FORMAT";
			this.ToolTip1.SetToolTip(this.lblFormat, null);
			// 
			// cmbModule
			// 
			this.cmbModule.AutoSize = false;
			this.cmbModule.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbModule.FormattingEnabled = true;
			this.cmbModule.Items.AddRange(new object[] {
				"Real Estate",
				"Personal Property",
				//FC:FINAL:MSH - i.issue #1573: remove item from the combobox(in original this item is disabled by default)
				//"Both (Combined)",
				"Both (Not Combined)"
			});
			this.cmbModule.Location = new System.Drawing.Point(125, 30);
			this.cmbModule.Name = "cmbModule";
			this.cmbModule.Size = new System.Drawing.Size(207, 40);
			this.cmbModule.TabIndex = 1;
			this.cmbModule.Text = "Real Estate";
			this.ToolTip1.SetToolTip(this.cmbModule, null);
			this.cmbModule.SelectedIndexChanged += new System.EventHandler(this.cmbModule_SelectedIndexChanged);
			// 
			// lblModule
			// 
			this.lblModule.AutoSize = true;
			this.lblModule.Location = new System.Drawing.Point(20, 44);
			this.lblModule.Name = "lblModule";
			this.lblModule.Size = new System.Drawing.Size(60, 15);
			this.lblModule.TabIndex = 0;
			this.lblModule.Text = "MODULE";
			this.ToolTip1.SetToolTip(this.lblModule, null);
			// 
			// txtStateAidReduction
			// 
			this.txtStateAidReduction.AutoSize = false;
			this.txtStateAidReduction.BackColor = System.Drawing.SystemColors.Window;
			this.txtStateAidReduction.LinkItem = null;
			this.txtStateAidReduction.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStateAidReduction.LinkTopic = null;
			this.txtStateAidReduction.Location = new System.Drawing.Point(983, 317);
			this.txtStateAidReduction.Name = "txtStateAidReduction";
			this.txtStateAidReduction.Size = new System.Drawing.Size(61, 40);
			this.txtStateAidReduction.TabIndex = 11;
			this.txtStateAidReduction.Text = "0";
			this.ToolTip1.SetToolTip(this.txtStateAidReduction, "Enter the whole percentage amount of tax reduction as a result of state aid to ed" + "ucation and revenue sharing");
			// 
			// t2kIndebtednessDate
			// 
			this.t2kIndebtednessDate.Location = new System.Drawing.Point(204, 317);
			this.t2kIndebtednessDate.Mask = "00/00/0000";
			//this.t2kIndebtednessDate.PromptChar = '0';
			this.t2kIndebtednessDate.Name = "t2kIndebtednessDate";
			this.t2kIndebtednessDate.Size = new System.Drawing.Size(107, 40);
			this.t2kIndebtednessDate.TabIndex = 7;
			this.t2kIndebtednessDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.t2kIndebtednessDate, null);
			// 
			// txtIndebtednessAmount
			// 
			this.txtIndebtednessAmount.AutoSize = false;
			this.txtIndebtednessAmount.BackColor = System.Drawing.SystemColors.Window;
			this.txtIndebtednessAmount.LinkItem = null;
			this.txtIndebtednessAmount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtIndebtednessAmount.LinkTopic = null;
			this.txtIndebtednessAmount.Location = new System.Drawing.Point(562, 317);
			this.txtIndebtednessAmount.Name = "txtIndebtednessAmount";
			this.txtIndebtednessAmount.Size = new System.Drawing.Size(140, 40);
			this.txtIndebtednessAmount.TabIndex = 9;
			this.txtIndebtednessAmount.Text = "0.00";
			this.ToolTip1.SetToolTip(this.txtIndebtednessAmount, null);
			// 
			// Frame4
			// 
			this.Frame4.AppearanceKey = "groupBoxNoBorders";
			this.Frame4.Controls.Add(this.chkDiscount);
			this.Frame4.Controls.Add(this.txtPercent);
			this.Frame4.Controls.Add(this.txtDate);
			this.Frame4.Controls.Add(this.Label4);
			this.Frame4.Controls.Add(this.Label3);
			this.Frame4.Location = new System.Drawing.Point(313, 132);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(388, 162);
			this.Frame4.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.Frame4, null);
			// 
			// chkDiscount
			// 
			this.chkDiscount.Checked = true;
			this.chkDiscount.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkDiscount.Location = new System.Drawing.Point(30, 30);
			this.chkDiscount.Name = "chkDiscount";
			this.chkDiscount.Size = new System.Drawing.Size(133, 27);
			this.chkDiscount.TabIndex = 0;
			this.chkDiscount.Text = "Use Discounts";
			this.ToolTip1.SetToolTip(this.chkDiscount, null);
			this.chkDiscount.CheckedChanged += new System.EventHandler(this.chkDiscount_CheckedChanged);
			// 
			// txtPercent
			// 
			this.txtPercent.AutoSize = false;
			this.txtPercent.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent.LinkItem = null;
			this.txtPercent.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent.LinkTopic = null;
			this.txtPercent.Location = new System.Drawing.Point(30, 77);
			this.txtPercent.Name = "txtPercent";
			this.txtPercent.Size = new System.Drawing.Size(62, 40);
			this.txtPercent.TabIndex = 1;
			this.txtPercent.Text = "0.00";
			this.ToolTip1.SetToolTip(this.txtPercent, null);
			// 
			// txtDate
			// 
			this.txtDate.AutoSize = false;
			this.txtDate.BackColor = System.Drawing.SystemColors.Window;
			this.txtDate.LinkItem = null;
			this.txtDate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDate.LinkTopic = null;
			this.txtDate.Location = new System.Drawing.Point(250, 77);
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(115, 40);
			this.txtDate.TabIndex = 4;
			this.txtDate.Text = "00/00/0000";
			this.ToolTip1.SetToolTip(this.txtDate, null);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(137, 91);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(71, 16);
			this.Label4.TabIndex = 3;
			this.Label4.Text = "DUE";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(101, 91);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(18, 19);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "%";
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.chkInterestedParties);
			this.Frame3.Controls.Add(this.chkNewOwnerCopy);
			this.Frame3.Controls.Add(this.chkMHCopy);
			this.Frame3.Location = new System.Drawing.Point(30, 132);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(242, 162);
			this.Frame3.TabIndex = 3;
			this.Frame3.Text = "Send Copies To";
			this.ToolTip1.SetToolTip(this.Frame3, null);
			// 
			// chkInterestedParties
			// 
			this.chkInterestedParties.ForeColor = Color.Black;
			this.chkInterestedParties.Location = new System.Drawing.Point(20, 124);
			this.chkInterestedParties.Name = "chkInterestedParties";
			this.chkInterestedParties.Size = new System.Drawing.Size(156, 27);
			this.chkInterestedParties.TabIndex = 2;
			this.chkInterestedParties.Text = "Interested Parties";
			this.ToolTip1.SetToolTip(this.chkInterestedParties, null);
			// 
			// chkNewOwnerCopy
			// 
			this.chkNewOwnerCopy.ForeColor = Color.Black;
			this.chkNewOwnerCopy.Location = new System.Drawing.Point(20, 77);
			this.chkNewOwnerCopy.Name = "chkNewOwnerCopy";
			this.chkNewOwnerCopy.Size = new System.Drawing.Size(113, 27);
			this.chkNewOwnerCopy.TabIndex = 1;
			this.chkNewOwnerCopy.Text = "New Owner";
			this.ToolTip1.SetToolTip(this.chkNewOwnerCopy, null);
			// 
			// chkMHCopy
			// 
			this.chkMHCopy.ForeColor = Color.Black;
			this.chkMHCopy.Location = new System.Drawing.Point(20, 30);
			this.chkMHCopy.Name = "chkMHCopy";
			this.chkMHCopy.Size = new System.Drawing.Size(191, 27);
			this.chkMHCopy.TabIndex = 0;
			this.chkMHCopy.Text = "Each Mortgage Holder";
			this.ToolTip1.SetToolTip(this.chkMHCopy, null);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.chkEnhanced);
			this.Frame2.Controls.Add(this.cmbFormat);
			this.Frame2.Controls.Add(this.lblFormat);
			this.Frame2.Location = new System.Drawing.Point(313, 132);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(350, 162);
			this.Frame2.TabIndex = 4;
			this.Frame2.Text = "Format";
			this.ToolTip1.SetToolTip(this.Frame2, null);
			this.Frame2.Visible = false;
			// 
			// chkEnhanced
			// 
			this.chkEnhanced.Checked = true;
			this.chkEnhanced.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkEnhanced.Enabled = false;
			this.chkEnhanced.Location = new System.Drawing.Point(20, 90);
			this.chkEnhanced.Name = "chkEnhanced";
			this.chkEnhanced.Size = new System.Drawing.Size(116, 27);
			this.chkEnhanced.TabIndex = 2;
			this.chkEnhanced.Text = "New Format";
			this.ToolTip1.SetToolTip(this.chkEnhanced, "The enhanced version contains extra information at the end of the record.  Contac" + "t your printing service to see which format they accept.");
			this.chkEnhanced.CheckedChanged += new System.EventHandler(this.chkEnhanced_CheckedChanged);
			// 
			// RichTextBox1
			// 
			this.RichTextBox1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
            this.RichTextBox1.Font = new System.Drawing.Font("Courier New", 10);
            this.RichTextBox1.Location = new System.Drawing.Point(29, 414);
			this.RichTextBox1.Locked = true;
			this.RichTextBox1.MinimumSize = new System.Drawing.Size(300, 300);
			this.RichTextBox1.Multiline = true;
			this.RichTextBox1.Name = "RichTextBox1";
			this.RichTextBox1.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.RichTextBox1.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.RichTextBox1.SelTabCount = null;
			this.RichTextBox1.Size = new System.Drawing.Size(1015, 315);
			this.RichTextBox1.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.RichTextBox1, null);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkPrint);
			this.Frame1.Controls.Add(this.cmbModule);
			this.Frame1.Controls.Add(this.lblModule);
			this.Frame1.Location = new System.Drawing.Point(327, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(725, 87);
			this.Frame1.TabIndex = 2;
			this.Frame1.Text = "Tax";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// chkPrint
			// 
			this.chkPrint.ForeColor = Color.Black;
			this.chkPrint.Location = new System.Drawing.Point(351, 40);
			this.chkPrint.Name = "chkPrint";
			this.chkPrint.Size = new System.Drawing.Size(366, 27);
			this.chkPrint.TabIndex = 2;
			this.chkPrint.Text = "Don\'t print bills with a zero or negative balance";
			this.ToolTip1.SetToolTip(this.chkPrint, null);
			// 
			// txtYear
			// 
			this.txtYear.AutoSize = false;
			this.txtYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtYear.LinkItem = null;
			this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYear.LinkTopic = null;
			this.txtYear.Location = new System.Drawing.Point(165, 59);
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(83, 40);
			this.txtYear.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtYear, null);
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(801, 331);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(149, 19);
			this.Label7.TabIndex = 10;
			this.Label7.Text = "STATE AID REDUCTION %";
			this.ToolTip1.SetToolTip(this.Label7, null);
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(30, 331);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(148, 19);
			this.Label6.TabIndex = 6;
			this.Label6.Text = "INDEBTEDNESS DATE";
			this.ToolTip1.SetToolTip(this.Label6, null);
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(377, 331);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(152, 19);
			this.Label5.TabIndex = 8;
			this.Label5.Text = "INDEBTEDNESS AMOUNT";
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label2
			// 
			this.Label2.ForeColor = Color.Black;
			this.Label2.Location = new System.Drawing.Point(30, 378);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(199, 15);
			this.Label2.TabIndex = 12;
			this.Label2.Text = "FILE FORMAT";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Label1
			// 
			this.Label1.ForeColor = Color.Black;
			this.Label1.Location = new System.Drawing.Point(30, 73);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(89, 19);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "TAX YEAR";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuCreate,
				this.mnuCopy,
				this.mnuPrintFormat,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuCreate
			// 
			this.mnuCreate.Index = 0;
			this.mnuCreate.Name = "mnuCreate";
			this.mnuCreate.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuCreate.Text = "Create Out-Print File";
			this.mnuCreate.Click += new System.EventHandler(this.mnuCreate_Click);
			// 
			// mnuCopy
			// 
			this.mnuCopy.Index = 1;
			this.mnuCopy.Name = "mnuCopy";
			this.mnuCopy.Text = "Copy File to Disk";
			this.mnuCopy.Click += new System.EventHandler(this.mnuCopy_Click);
			// 
			// mnuPrintFormat
			// 
			this.mnuPrintFormat.Index = 2;
			this.mnuPrintFormat.Name = "mnuPrintFormat";
			this.mnuPrintFormat.Text = "Print Format";
			this.mnuPrintFormat.Click += new System.EventHandler(this.mnuPrintFormat_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 3;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdCreate
			// 
			this.cmdCreate.AppearanceKey = "acceptButton";
			this.cmdCreate.Location = new System.Drawing.Point(441, 30);
			this.cmdCreate.Name = "cmdCreate";
			this.cmdCreate.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdCreate.Size = new System.Drawing.Size(189, 48);
			this.cmdCreate.TabIndex = 0;
			this.cmdCreate.Text = "Create Out-Print File";
			this.ToolTip1.SetToolTip(this.cmdCreate, null);
			this.cmdCreate.Click += new System.EventHandler(this.mnuCreate_Click);
			// 
			// cmdCopy
			// 
			this.cmdCopy.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdCopy.AppearanceKey = "toolbarButton";
			this.cmdCopy.Location = new System.Drawing.Point(926, 29);
			this.cmdCopy.Name = "cmdCopy";
			this.cmdCopy.Size = new System.Drawing.Size(118, 24);
			this.cmdCopy.TabIndex = 1;
			this.cmdCopy.Text = "Copy File to Disk";
			this.ToolTip1.SetToolTip(this.cmdCopy, null);
			this.cmdCopy.Click += new System.EventHandler(this.mnuCopy_Click);
			// 
			// cmdPrintFormat
			// 
			this.cmdPrintFormat.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintFormat.AppearanceKey = "toolbarButton";
			this.cmdPrintFormat.Location = new System.Drawing.Point(830, 29);
			this.cmdPrintFormat.Name = "cmdPrintFormat";
			this.cmdPrintFormat.Size = new System.Drawing.Size(90, 24);
			this.cmdPrintFormat.TabIndex = 2;
			this.cmdPrintFormat.Text = "Print Format";
			this.ToolTip1.SetToolTip(this.cmdPrintFormat, null);
			this.cmdPrintFormat.Click += new System.EventHandler(this.mnuPrintFormat_Click);
			// 
			// frmOutPrinting
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1072, 917);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmOutPrinting";
			this.Text = "Out-Printing";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmOutPrinting_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmOutPrinting_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kIndebtednessDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInterestedParties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNewOwnerCopy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMHCopy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEnhanced)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCopy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintFormat)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdCreate;
		private FCButton cmdCopy;
		private FCButton cmdPrintFormat;
	}
}