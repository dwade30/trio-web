﻿//Fecher vbPorter - Version 1.0.0.35
using Wisej.Web;
using Global;
using fecherFoundation;
using fecherFoundation.Extensions;
using System;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmGetOptions.
	/// </summary>
	public partial class frmGetOptions : BaseForm
	{
		public frmGetOptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetOptions InstancePtr
		{
			get
			{
				return (frmGetOptions)Sys.GetInstance(typeof(frmGetOptions));
			}
		}

		protected frmGetOptions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Corey
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// 07/20/2004
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		int lngReturn;
        string opt1 = "", opt2 = "", opt3 = "", opt4 = "", opt5 = "", opt6 = "";

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			lngReturn = -1;
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			mnuSaveContinue_Click();
		}

		private void frmGetOptions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			lngReturn = -1;
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public int Init(short intDefaultChoice = 1, string strTitle = "", string strDescription = "", string strOption1 = "", string strOption2 = "", string strOption3 = "", string strOption4 = "", string strOption5 = "", string strOption6 = "")
		{
			int Init = 0;
			// returns negative if cancelled
			lngReturn = -1;
			Init = lngReturn;
			lblDescription.Text = strDescription;
			this.Text = strTitle;
			//FC:FINAL:MSH - issue #1314: replace HeaderText to text from form Caption
			this.HeaderText.Text = strTitle;
			if (!cmbtion1.Items.Contains(strOption1) && strOption1 != string.Empty)
			{
				cmbtion1.Items.Add(strOption1);
			}
			if (!cmbtion1.Items.Contains(strOption2) && strOption2 != string.Empty)
			{
				cmbtion1.Items.Add(strOption2);
			}
			if (!cmbtion1.Items.Contains(strOption3) && strOption3 != string.Empty)
			{
				cmbtion1.Items.Add(strOption3);
			}
			if (!cmbtion1.Items.Contains(strOption4) && strOption4 != string.Empty)
			{
				cmbtion1.Items.Add(strOption4);
			}
			if (!cmbtion1.Items.Contains(strOption5) && strOption5 != string.Empty)
			{
				cmbtion1.Items.Add(strOption5);
			}
			if (!cmbtion1.Items.Contains(strOption6) && strOption6 != string.Empty)
			{
				cmbtion1.Items.Add(strOption6);
			}
			// set default
			if (cmbtion1.Items.Count >= intDefaultChoice - 1)
			{
				cmbtion1.SelectedIndex = intDefaultChoice - 1;
			}
			else
			{
				int x;
				for (x = 0; x <= 5; x++)
				{
					if (cmbtion1.Items.Count >= x)
					{
						cmbtion1.SelectedIndex = x;
						break;
					}
				}
				// x
			}
            //FC:FINAL:MSH - save input options for use in comparison
            opt1 = strOption1;
            opt2 = strOption2;
            opt3 = strOption3;
            opt4 = strOption4;
            opt5 = strOption5;
            opt6 = strOption6;

            this.Show(FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:MSH - replace wrong comparing. In original always exist 6 options. In combobox number of items depend on number of input params in Init()
			//int x;
			//for (x = 0; x <= 5; x++)
			//{
			//	if (cmbtion1.SelectedIndex == x)
			//	{
			//		lngReturn = x + 1;
			//		break;
			//	}
			//}
			// x
            if(cmbtion1.Text == opt1)
            {
                lngReturn = 1;
            }
            else if(cmbtion1.Text == opt2)
            {
                lngReturn = 2;
            }
            else if(cmbtion1.Text == opt3)
            {
                lngReturn = 3;
            }
            else if(cmbtion1.Text == opt4)
            {
                lngReturn = 4;
            }
            else if(cmbtion1.Text == opt5)
            {
                lngReturn = 5;
            }
            else if(cmbtion1.Text == opt6)
            {
                lngReturn = 6;
            }

			Close();
		}

		public void mnuSaveContinue_Click()
		{
			mnuSaveContinue_Click(mnuSaveContinue, new System.EventArgs());
		}

		private void frmGetOptions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetOptions properties;
			//frmGetOptions.FillStyle	= 0;
			//frmGetOptions.ScaleWidth	= 5880;
			//frmGetOptions.ScaleHeight	= 4155;
			//frmGetOptions.LinkTopic	= "Form2";
			//frmGetOptions.LockControls	= true;
			//frmGetOptions.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void cmdSaveAndContinue_Click(object sender, System.EventArgs e)
		{
			mnuSaveContinue_Click(mnuSaveContinue, EventArgs.Empty);
		}
	}
}
