﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmViewer.
	/// </summary>
	public partial class frmViewer : FCForm
	{
		public frmViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmViewer InstancePtr
		{
			get
			{
				return (frmViewer)Sys.GetInstance(typeof(frmViewer));
			}
		}

		protected frmViewer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string[] arFileList = new string[9 + 1];
		bool boolEmail;
		string strEmailAttachmentName = "";

		private void mnuEmailPDF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				//ActiveReportsPDFExport.ARExportPDF a = new ActiveReportsPDFExport.ARExportPDF();
				//a = new ActiveReportsPDFExport.ARExportPDF();
				////FileSystemObject fso = new FileSystemObject();
				//string strList = "";
				//a.FileName = FCFileSystem.CurDir() + "\\rpt\\" + fso.GetTempName();
				//a.Export(ARViewer21.Pages);
				//if (!File.Exists(a.FileName))
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				string strList = "";
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
				string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Document.Name + ".pdf";
				a.Export(ARViewer21.ReportSource.Document, fileName);
				if (!File.Exists(fileName))
				{
					MessageBox.Show("Could not create report file to send as attachment", "Cannot E-mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (Strings.Trim(strEmailAttachmentName) != string.Empty)
					{
						strList = fileName + ";" + strEmailAttachmentName + ".pdf";
					}
					else
					{
						strList = fileName;
					}
					;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailPDF_Click");
			}
		}

		private void mnuEmailRTF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				//ActiveReportsRTFExport.ARExportRTF a = new ActiveReportsRTFExport.ARExportRTF();
				//a = new ActiveReportsRTFExport.ARExportRTF();
				////FileSystemObject fso = new FileSystemObject();
				//string strList = "";
				//a.FileName = FCFileSystem.CurDir() + "\\rpt\\" + fso.GetTempName();
				//a.Export(ARViewer21.Pages);
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				string strList = "";
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
				string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Document.Name + ".rtf";
				a.Export(ARViewer21.ReportSource.Document, fileName);
				if (!File.Exists(fileName))
				{
					MessageBox.Show("Could not create report file to send as attachment", "Cannot E-mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (Strings.Trim(strEmailAttachmentName) != string.Empty)
					{
						strList = fileName + ";" + strEmailAttachmentName + ".rtf";
					}
					else
					{
						strList = fileName;
					}
					;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailRTF_Click");
			}
		}

		private void mnuExcel_Click(object sender, System.EventArgs e)
		{
			try
			{
				//string strOldDir;
				//string strFileName = "";
				//string strFullName = "";
				//string strPathName = "";
				//Scripting.File fl;
				////FileSystemObject fso = new FileSystemObject();
				//// If ARViewer21.ReportSource.Status = ddVStatusReadingData Then
				//// MsgBox "Report not done", vbExclamation, "Report Loading"
				//// Exit Sub
				//// End If
				//strOldDir = FCFileSystem.CurDir();
				//Information.Err().Clear();
				//// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				///*? On Error Resume Next  */
				//MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.xls";
				//MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "xls";
				//MDIParent.InstancePtr.CommonDialog1_Save.InitDir = Application.StartupPath;
				//MDIParent.InstancePtr.CommonDialog1_Save.ShowSave();
				//if (Information.Err().Number == 0)
				//{
				//	strFileName = fso.GetBaseName(MDIParent.InstancePtr.CommonDialog1_Save.FileName);
				//	// name without extension
				//	strFullName = fso.GetFileName(MDIParent.InstancePtr.CommonDialog1_Save.FileName);
				//	// name with extension
				//	strPathName = fso.GetParentFolderName(MDIParent.InstancePtr.CommonDialog1_Save.FileName);
				//	if (Strings.Right(strPathName, 1) != "\\")
				//		strPathName += "\\";
				//	ActiveReportsExcelExport.ARExportExcel a = new ActiveReportsExcelExport.ARExportExcel();
				//	a = new ActiveReportsExcelExport.ARExportExcel();
				//	if (!Directory.Exists(strPathName + strFileName))
				//	{
				//		Directory.CreateDirectory(strPathName + strFileName);
				//		strPathName += strFileName + "\\";
				//	}
				//	// a.FileName = .FileName
				//	a.FileName = strPathName + strFullName;
				//	a.Version = 8;
				//	a.Export(ARViewer21.Pages);
				//	if (!File.Exists(a.FileName))
				//	{
				//		MessageBox.Show("No file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that the directory is not write protected and that there is enough free space to store the file", "Not Successful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//		// MsgBox "No file created" & vbNewLine & "Export not successful", vbExclamation, "Not Exported"
				//	}
				//	else
				//	{
				//		fl = fso.GetFile(a.FileName);
				//		if (FCConvert.ToInt32(fl.Size > 0)
				//		{
				//			MessageBox.Show("Export to file  " + MDIParent.InstancePtr.CommonDialog1_Save.FileName + "  was completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//		}
				//		else
				//		{
				//			MessageBox.Show("Empty file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that there is enough free space to create the file", "Not Exported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//		}
				//	}
				//}
				//else
				//{
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				///* On Error GoTo ErrorHandler */
				//ChDrive(strOldDir);
				//FCFileSystem.CurDir() = strOldDir;
				GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
				string fileName = ARViewer21.ReportSource.Name + ".xls";
				using (MemoryStream stream = new MemoryStream())
				{
					a.Export(ARViewer21.ReportSource.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Excel export.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}


		//FC:FINAL:CHN - issue #1594: Add missing print Button.
		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
            if (ARViewer21.ReportSource != null)
            {
                if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                {
                    FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                    return;
                }
            }
            try
            {
                FCSectionReport report;
                if (ARViewer21.ReportSource != null)
                {
                    report = ARViewer21.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document?.Load(ARViewer21.ReportName);
                }
                report.ExportToPDFAndPrint();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In mnuPrint", MsgBoxStyle.Critical, "Error");
            }
        }
		

		private void mnuHTML_Click(object sender, System.EventArgs e)
		{
			try
			{
				//string strFileName = "";
				////FileSystemObject fso = new FileSystemObject();
				//// vbPorter upgrade warning: intResp As short, int --> As DialogResult
				//DialogResult intResp;
				//string strOldDir;
				//string strFullName = "";
				//// If ARViewer21.ReportSource.Status = ddVStatusReadingData Then
				//// MsgBox "Report not done", vbExclamation, "Report Loading"
				//// Exit Sub
				//// End If
				//strOldDir = FCFileSystem.CurDir();
				//Information.Err().Clear();
				//// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				///*? On Error Resume Next  */
				//MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.htm";
				//MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "htm";
				//MDIParent.InstancePtr.CommonDialog1_Save.InitDir = Application.StartupPath;
				//MDIParent.InstancePtr.CommonDialog1_Save.ShowSave();
				//if (Information.Err().Number == 0)
				//{
				//	ActiveReportsHTMLExport.HTMLexport a = new ActiveReportsHTMLExport.HTMLexport();
				//	a = new ActiveReportsHTMLExport.HTMLexport();
				//	strFileName = fso.GetBaseName(MDIParent.InstancePtr.CommonDialog1_Save.FileName);
				//	a.FileNamePrefix = strFileName;
				//	strFullName = fso.GetFileName(MDIParent.InstancePtr.CommonDialog1_Save.FileName);
				//	a.HTMLOutputPath = fso.GetParentFolderName(MDIParent.InstancePtr.CommonDialog1_Save.FileName);
				//	a.MHTOutput = false;
				//	a.MultiPageOutput = false;
				//	if (ARViewer21.Pages.Count > 1)
				//	{
				//		intResp = MessageBox.Show("Do you want to save all pages as one HTML page?", "Single or multiple files?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				//		if (intResp == DialogResult.No)
				//			a.MultiPageOutput = true;
				//	}
				//	if (!Directory.Exists(a.HTMLOutputPath + strFileName))
				//	{
				//		Directory.CreateDirectory(a.HTMLOutputPath + strFileName);
				//		a.HTMLOutputPath = a.HTMLOutputPath + strFileName;
				//	}
				//	a.Export(ARViewer21.Pages);
				//	MessageBox.Show("Export to file  " + a.HTMLOutputPath + strFullName + "  was completed successfully." + "\r\n" + "Please note that supporting files may also have been created in the same directory.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//}
				//else
				//{
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
				///* On Error GoTo ErrorHandler */
				//ChDrive(strOldDir);
				//FCFileSystem.CurDir() = strOldDir;
				GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
				string fileName = ARViewer21.ReportSource.Name + ".html";
				using (MemoryStream stream = new MemoryStream())
				{
					a.Export(ARViewer21.ReportSource.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HTML export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuRTF_Click(object sender, System.EventArgs e)
		{
			try
			{
				//string strOldDir;
				////FileSystemObject fso = new FileSystemObject();
				//Scripting.File fl;
				//// If ARViewer21.ReportSource.Status = ddVStatusReadingData Then
				//// MsgBox "Report not done", vbExclamation, "Report Loading"
				//// Exit Sub
				//// End If
				//strOldDir = FCFileSystem.CurDir();
				//Information.Err().Clear();
				//// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				///*? On Error Resume Next  */
				//MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.rtf";
				//MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "rtf";
				//MDIParent.InstancePtr.CommonDialog1_Save.InitDir = Application.StartupPath;
				//MDIParent.InstancePtr.CommonDialog1_Save.ShowSave();
				//if (Information.Err().Number == 0)
				//{
				//	ActiveReportsRTFExport.ARExportRTF a = new ActiveReportsRTFExport.ARExportRTF();
				//	a = new ActiveReportsRTFExport.ARExportRTF();
				//	a.FileName = MDIParent.InstancePtr.CommonDialog1_Save.FileName;
				//	a.Export(ARViewer21.Pages);
				//	if (!File.Exists(a.FileName))
				//	{
				//		// If Left(UCase(fso.GetDriveName(a.FileName)), 1) = "A" Then
				//		MessageBox.Show("No file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that the directory is not write protected and that there is enough free space to store the file", "Not Exported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//		// Else
				//		// MsgBox "No file created" & vbNewLine & "Export not successful", vbExclamation, "Not Exported"
				//		// End If
				//	}
				//	else
				//	{
				//		fl = fso.GetFile(a.FileName);
				//		if (FCConvert.ToInt32(fl.Size > 0)
				//		{
				//			MessageBox.Show("Export to file  " + MDIParent.InstancePtr.CommonDialog1_Save.FileName + "  was completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//		}
				//		else
				//		{
				//			MessageBox.Show("Empty file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that there is enough free space to create the file", "Not Exported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//		}
				//	}
				//}
				//else
				//{
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
				///* On Error GoTo ErrorHandler */
				//ChDrive(strOldDir);
				//FCFileSystem.CurDir() = strOldDir;
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				string fileName = ARViewer21.ReportSource.Name + ".rtf";
				using (MemoryStream stream = new MemoryStream())
				{
					a.Export(ARViewer21.ReportSource.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In RTF export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExportPDF_Click(object sender, System.EventArgs e)
		{
			try
			{
				//string strOldDir;
				////FileSystemObject fso = new FileSystemObject();
				//Scripting.File fl;
				//// If ARViewer21.ReportSource.Status = ddVStatusReadingData Then
				//// MsgBox "Report not done", vbExclamation, "Report Loading"
				//// Exit Sub
				//// End If
				//strOldDir = FCFileSystem.CurDir();
				//Information.Err().Clear();
				//// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				///*? On Error Resume Next  */
				//MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.pdf";
				//MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "pdf";
				//MDIParent.InstancePtr.CommonDialog1_Save.InitDir = Application.StartupPath;
				//MDIParent.InstancePtr.CommonDialog1_Save.ShowSave();
				//if (Information.Err().Number == 0)
				//{
				//	ActiveReportsPDFExport.ARExportPDF a = new ActiveReportsPDFExport.ARExportPDF();
				//	a = new ActiveReportsPDFExport.ARExportPDF();
				//	a.FileName = MDIParent.InstancePtr.CommonDialog1_Save.FileName;
				//	a.Export(ARViewer21.Pages);
				//	if (!File.Exists(a.FileName))
				//	{
				//		// MsgBox "No file created" & vbNewLine & "Export not successful", vbExclamation, "Not Exported"
				//		MessageBox.Show("No file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that the directory is not write protected and that there is enough free space to store the file", "Not Exported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//	}
				//	else
				//	{
				//		fl = fso.GetFile(a.FileName);
				//		if (FCConvert.ToInt32(fl.Size > 0)
				//		{
				//			MessageBox.Show("Export to file  " + MDIParent.InstancePtr.CommonDialog1_Save.FileName + "  was completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//		}
				//		else
				//		{
				//			MessageBox.Show("Empty file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that there is enough free space to create the file", "Not Exported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//		}
				//	}
				//}
				//else
				//{
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				///* On Error GoTo ErrorHandler */
				//ChDrive(strOldDir);
				//FCFileSystem.CurDir() = strOldDir;
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				string fileName = ARViewer21.ReportSource.Name + ".pdf";
				using (MemoryStream stream = new MemoryStream())
				{
					a.Export(ARViewer21.ReportSource.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In PDF export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmViewer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmViewer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmViewer properties;
			//frmViewer.ScaleWidth	= 9330;
			//frmViewer.ScaleHeight	= 7995;
			//frmViewer.LinkTopic	= "Form1";
			//End Unmaped Properties
			// Call SetFixedSize(Me, TRIOWINDOWSIZEBIGGIE)
			this.WindowState = FormWindowState.Maximized;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public void Init(string strCaption, string strReportType, bool boolAllowEmail = false, string strAttachmentName = "")
		{
			string strFileMask = "";
			// the file name format used for a given report
			boolEmail = boolAllowEmail;
			strEmailAttachmentName = strAttachmentName;
			this.Text = strCaption;
			if (Strings.UCase(strReportType) == "RETRANSFER")
			{
				// billing transfer
				strFileMask = "REBLTR*.*";
			}
			else if (Strings.UCase(strReportType) == "PPTRANSFER")
			{
				// billing transfer
				strFileMask = "PPBLTR*.*";
			}
			if (LoadReports(ref strFileMask))
			{
				// 
				//FC:FINAL:RPU: #i1574 - Set the ARViewer visible false
				ARViewer21.Visible = false;
				this.Show(App.MainForm);
				framReports.Visible = true;
			}
			else
			{
				// if it comes back false then there were no saved reports
				return;
			}
		}

		private bool LoadReports(ref string strFileMask)
		{
			bool LoadReports = false;
			string strFile;
			int lngRow;
			// this routine fills the combo box
			LoadReports = false;
			strFile = FCFileSystem.Dir(Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, strFileMask), 0);
			GridList.Rows = 0;
			if (strFile != string.Empty)
			{
				// fill the grid with the choices of reports
				LoadReports = true;
				while (strFile != string.Empty)
				{
					GridList.Rows += 1;
					lngRow = GridList.Rows - 1;
					GridList.TextMatrix(lngRow, 0, File.GetLastWriteTime(Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, strFile)));
					GridList.TextMatrix(lngRow, 1, Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, strFile));
					strFile = FCFileSystem.Dir();
				}
				if (GridList.Rows > 0)
				{
					GridList.ColDataType(0, FCGrid.DataTypeSettings.flexDTDate);
					GridList.Sort = FCGrid.SortSettings.flexSortGenericDescending;
					cmbDate.Clear();
					for (lngRow = 0; lngRow <= GridList.Rows - 1; lngRow++)
					{
						cmbDate.AddItem(GridList.TextMatrix(lngRow, 0));
						arFileList[cmbDate.NewIndex] = GridList.TextMatrix(lngRow, 1);
					}
					// lngRow
				}
				cmbDate.SelectedIndex = 0;
			}
			else
			{
				MessageBox.Show("No reports to load.", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				LoadReports = false;
			}
			return LoadReports;
		}

		private void mnuLoad_Click(object sender, System.EventArgs e)
		{
			framReports.Visible = true;
		}

		private void mnuPreview_Click(object sender, System.EventArgs e)
		{
			if (cmbDate.Items.Count > 0)
			{
				if (cmbDate.SelectedIndex < 0)
				{
					MessageBox.Show("You haven't selected a date yet.", "Select a Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					framReports.Visible = false;
					//FC:FINAL:RPU: #i1574 - Load the report and make ARViewer visible again
					//ARViewer21.ReportSource.Document.Load(arFileList[cmbDate.SelectedIndex]);
					ARViewer21.Visible = true;
					ARViewer21.ReportName = arFileList[cmbDate.SelectedIndex];
				}
			}
			else
			{
				MessageBox.Show("There are no reports to preview", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
		{
			if (e.Button == toolBarButtonEmailPDF)
			{
				this.mnuEmailPDF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonEmailRTF)
			{
				this.mnuEmailRTF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportPDF)
			{
				this.mnuExportPDF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportRTF)
			{
				this.mnuRTF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportHTML)
			{
				this.mnuHTML_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportExcel)
			{
				this.mnuExcel_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonPrint)
			{
				this.mnuPrint_Click(e.Button, EventArgs.Empty);
			}
		}
	}
}
