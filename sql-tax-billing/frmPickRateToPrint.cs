﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPickRateToPrint.
	/// </summary>
	public partial class frmPickRateToPrint : BaseForm
	{
		public frmPickRateToPrint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPickRateToPrint InstancePtr
		{
			get
			{
				return (frmPickRateToPrint)Sys.GetInstance(typeof(frmPickRateToPrint));
			}
		}

		protected frmPickRateToPrint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// WRITTEN BY     :               Corey Gray              *
		/// </summary>
		/// <summary>
		/// DATE           :                                       *
		/// </summary>
		/// <summary>
		/// *
		/// </summary>
		/// <summary>
		/// MODIFIED BY    :               Jim Bertolino           *
		/// </summary>
		/// <summary>
		/// LAST UPDATED   :               07/29/2004              *
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		string strReturn;
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void frmPickRateToPrint_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPickRateToPrint_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPickRateToPrint properties;
			//frmPickRateToPrint.FillStyle	= 0;
			//frmPickRateToPrint.ScaleWidth	= 5880;
			//frmPickRateToPrint.ScaleHeight	= 3960;
			//frmPickRateToPrint.LinkTopic	= "Form2";
			//frmPickRateToPrint.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPickRateToPrint_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			mnuSaveContinue_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			strReturn = "CANCEL";
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public string Init(int lngTaxYear, bool boolRegular = true, bool boolSupplemental = false, bool boolAllowMulti = false, string strBillType = "", string strCaption = "")
		{
			return Init(ref lngTaxYear, boolRegular, boolSupplemental, boolAllowMulti, strBillType, strCaption);
		}

		public string Init(ref int lngTaxYear, bool boolRegular = true, bool boolSupplemental = false, bool boolAllowMulti = false, string strBillType = "", string strCaption = "")
		{
			string Init = "";
			// returns null string for all, comma delimited string for anything else and "CANCEL" for cancellation
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				Init = string.Empty;
				strReturn = string.Empty;
				txtCaption.Text = strCaption;
				strSQL = "SELECT distinct raterec.ID as ratekey,taxrate,numberofperiods,ratetype,billingdate from";
				if (boolRegular)
				{
					if (!boolSupplemental)
					{
						strSQL += " raterec inner join billingmaster on (raterec.ID = billingmaster.ratekey) where raterec.year = " + FCConvert.ToString(lngTaxYear) + " and ratetype = 'R'";
					}
					else
					{
						strSQL += " raterec inner join billingmaster on (raterec.ID = billingmaster.ratekey) where raterec.year = " + FCConvert.ToString(lngTaxYear) + " and ratetype <> 'L'";
					}
				}
				else
				{
					strSQL += " raterec inner join billingmaster on (raterec.ID = billingmaster.ratekey) where raterec.year = " + FCConvert.ToString(lngTaxYear) + " and ratetype = 'S'";
				}
				if (Strings.Trim(strBillType) != string.Empty)
				{
					strSQL += " and billINGtype = '" + strBillType + "' ";
				}
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("No bills fit this criteria", "No Bills Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
					strReturn = "CANCEL";
					Init = "CANCEL";
					Close();
					return Init;
				}
				else
				{
					if (clsLoad.RecordCount() == 1)
					{
						strReturn = " ratekey = " + clsLoad.Get_Fields_Int32("ratekey");
						Init = strReturn;
						Close();
						return Init;
					}
				}
				FillGrid(ref boolAllowMulti);
				this.Show(FormShowEnum.Modal, App.MainForm);
				Init = strReturn;
				return Init;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Init;
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.15 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.35 * GridWidth));
			Grid.ColWidth(2, FCConvert.ToInt32(0.15 * GridWidth));
			Grid.ColWidth(3, FCConvert.ToInt32(0.15 * GridWidth));
		}

		private void FillGrid(ref bool boolAllowMulti)
		{
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				Grid.Rows = 1;
				Grid.Cols = 6;
				Grid.ColHidden(5, true);
				Grid.TextMatrix(0, 0, "Tax Rate");
				Grid.TextMatrix(0, 1, "Description");
				Grid.TextMatrix(0, 2, "Periods");
				Grid.TextMatrix(0, 3, "Bill Type");
				Grid.TextMatrix(0, 4, "Bill Date");
				if (boolAllowMulti)
				{
					Grid.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionListBox;
					Grid.AllowSelection = true;
				}
				else
				{
					Grid.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionByRow;
					Grid.AllowSelection = false;
				}
				// If boolAll Then
				// .AddItem "All Rate Records" & vbTab & "All Rate Records" & vbTab & "All Rate Records" & vbTab & "All Rate Records" & vbTab & "All Rate Records" & vbTab & 0
				// .MergeRow(1) = True
				// .MergeCells = flexMergeRestrictRows
				// .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, 1, 4) = flexAlignCenterCenter
				// End If
				while (!clsLoad.EndOfFile())
				{
					Grid.AddItem(Strings.Format(clsLoad.Get_Fields_Double("taxrate") * 1000, "0.00") + "\t" + "\t" + clsLoad.Get_Fields_Int16("numberofperiods") + "\t" + clsLoad.Get_Fields_String("ratetype") + "\t" + Convert.ToDateTime(clsLoad.Get_Fields_DateTime("billingdate")).ToString("d") + "\t" + clsLoad.Get_Fields_Int32("ratekey"));
					clsLoad.MoveNext();
				}
				for (x = 1; x <= Grid.Rows - 1; x++)
				{
					clsLoad.OpenRecordset("select description from raterec where ID = " + Grid.TextMatrix(x, 5), "twcl0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						Grid.TextMatrix(x, 1, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					}
				}
				//FC:FINAL:DDU:#1865 - align all columns to left
				Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// x
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			int x;
			string strTemp;
			strTemp = "";
			if (Grid.AllowSelection == true)
			{
				for (x = 1; x <= Grid.Rows - 1; x++)
				{
					if (Grid.IsSelected(x))
					{
						strTemp += "RateKey = " + Grid.TextMatrix(x, 5) + " or ";
					}
				}
				// x
			}
			else
			{
				if (Grid.Row >= 1)
				{
					strTemp = "RateKey = " + Grid.TextMatrix(Grid.Row, 5) + " or ";
				}
			}
			if (strTemp == string.Empty)
			{
				MessageBox.Show("You must select at least one rate record", "No Record Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 4);
			strReturn = strTemp;
			Close();
		}

		public void mnuSaveContinue_Click()
		{
			mnuSaveContinue_Click(mnuSaveContinue, new System.EventArgs());
		}
	}
}
