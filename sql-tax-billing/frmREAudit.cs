﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmREAudit.
	/// </summary>
	public partial class frmREAudit : BaseForm
	{
		public frmREAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmREAudit InstancePtr
		{
			get
			{
				return (frmREAudit)Sys.GetInstance(typeof(frmREAudit));
			}
		}

		protected frmREAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		double dblTaxRate;
		clsRateRecord clsRateInfo = new clsRateRecord();
		double lngTotLand;
		double lngTotBldg;
		double lngTotExempt;
		double lngTotAssess;
		double dblTotTax;
		public string strBegin = "";
		/// <summary>
		/// used for ranges
		/// </summary>
		public string strEnd = "";
		/// <summary>
		/// used for ranges
		/// </summary>
		public int intWhich;
		/// <summary>
		/// used for ranges
		/// </summary>
		public int intWhereFrom;
		/// <summary>
		/// to tell if this is an audit or billing transfer
		/// </summary>
		public bool boolUseOwnerAsOf;
		public string strAsOfDate = "";
		bool boolDoPPAfter;
		bool boolQueryPrint;
		const int colAcct = 0;
		const int colName = 1;
		const int colMapLot = 2;
		const int colSnum = 3;
		const int colLoc = 4;
		const int colLand = 5;
		const int colBldg = 6;
		const int colExempt = 7;
		const int colTot = 8;
		const int colTax = 9;
		const int colLCodes = 10;
		const int colBCodes = 11;
		const int colECodes = 12;
		int intSortDirection;
		public bool boolDisplayFirst;
		public string strSequence = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cPartyController tPCont = new cPartyController();
		private cPartyController tPCont_AutoInitialized;

		private cPartyController tPCont
		{
			get
			{
				if (tPCont_AutoInitialized == null)
				{
					tPCont_AutoInitialized = new cPartyController();
				}
				return tPCont_AutoInitialized;
			}
			set
			{
				tPCont_AutoInitialized = value;
			}
		}

		private string strGroupToUse = "";

		private void AuditGrid_BeforeSort(object sender, DataGridViewCellMouseEventArgs e)
		{
			// Order = 0
			if ((AuditGrid.Col == colSnum) || (AuditGrid.Col == colLoc))
			{
				switch (intSortDirection)
				{
					case 0:
						{
							AuditGrid.Col = colSnum;
							AuditGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
							AuditGrid.Col = colLoc;
							AuditGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
							intSortDirection = 1;
							break;
						}
					case 1:
						{
							AuditGrid.Col = colSnum;
							AuditGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
							AuditGrid.Col = colLoc;
							AuditGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
							intSortDirection = 0;
							break;
						}
				}
				//end switch
				//e.order = 0;
			}
		}

		private void AuditGrid_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = AuditGrid[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = AuditGrid.GetFlexRowIndex(e.RowIndex);
			lngMC = AuditGrid.GetFlexColIndex(e.ColumnIndex);
			if (lngMR > 0 && lngMR < AuditGrid.Rows)
			{
				switch (lngMC)
				{
					case colLand:
						{
							//ToolTip1.SetToolTip(AuditGrid, AuditGrid.TextMatrix(lngMR, colLCodes));
							cell.ToolTipText = AuditGrid.TextMatrix(lngMR, colLCodes);
							break;
						}
					case colExempt:
						{
							//ToolTip1.SetToolTip(AuditGrid, AuditGrid.TextMatrix(lngMR, colECodes));
							cell.ToolTipText = AuditGrid.TextMatrix(lngMR, colECodes);
							break;
						}
					default:
						{
                            //ToolTip1.SetToolTip(AuditGrid, "");
                            cell.ToolTipText = "";
							break;
						}
				}
				//end switch
			}
			else
			{
                //ToolTip1.SetToolTip(AuditGrid, "");
                cell.ToolTipText = "";
			}
		}

		private void frmREAudit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

        private void frmREAudit_Load(object sender, System.EventArgs e)
		{
			boolQueryPrint = false;
			SetupAuditGrid();
			SetupTotalGrid();
			// Call FillAuditGrid
			if (modGlobalConstants.Statics.boolMaxForms)
			{
				this.WindowState = FormWindowState.Maximized;
			}

			if (intWhereFrom != 1)
			{
				// doing a transfer not an audit
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			// check to see if they want to print the commitment book now
			if (intWhereFrom == 3 && boolQueryPrint)
			{
				intResponse = MessageBox.Show("Print Commitment Book Now?", "Print Commitment Book?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResponse == DialogResult.Yes)
				{
					// print the commitment book for this year
					frmPickCommitmentYear.InstancePtr.Init(clsRateInfo.RateKey, true, FCConvert.ToInt32(FormShowEnum.Modal));
				}
			}
		}

		private void frmREAudit_Resize(object sender, System.EventArgs e)
		{
			ResizeAuditGrid();
			ResizeTotalGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDoPPAfter)
			{
				//Application.DoEvents();
				// add one to wherefrom to get the same code but for PP
				frmPPAudit.InstancePtr.Init(ref clsRateInfo, (intWhereFrom + 1));
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupAuditGrid()
		{
			AuditGrid.FixedCols = 0;
			AuditGrid.FixedRows = 1;
			AuditGrid.Rows = 1;
			AuditGrid.Cols = 13;
			AuditGrid.ColHidden(10, true);
			AuditGrid.ColHidden(11, true);
			AuditGrid.ColHidden(12, true);
			AuditGrid.ColAlignment(colSnum, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ColAlignment(colLand, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ColAlignment(colBldg, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ColAlignment(colExempt, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ColAlignment(colTot, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ColAlignment(colTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
			AuditGrid.ExtendLastCol = true;
			AuditGrid.TextMatrix(0, 0, "Acct");
			AuditGrid.TextMatrix(0, 1, "Name");
			AuditGrid.TextMatrix(0, 2, "Map/Lot");
			AuditGrid.TextMatrix(0, colSnum, "#");
			AuditGrid.TextMatrix(0, colLoc, "Location");
			AuditGrid.TextMatrix(0, colLand, "Land");
			AuditGrid.TextMatrix(0, colBldg, "Building");
			AuditGrid.TextMatrix(0, colExempt, "Exempt");
			AuditGrid.TextMatrix(0, colTot, "Total");
			AuditGrid.TextMatrix(0, colTax, "Tax");
			//FC:FINAL:CHN - issue #1286: Audit. Incorrect columns ordering. 
			AuditGrid.ColDataType(0, FCGrid.DataTypeSettings.flexDTLong);
			AuditGrid.ColDataType(colLand, FCGrid.DataTypeSettings.flexDTDouble);
			AuditGrid.ColDataType(colBldg, FCGrid.DataTypeSettings.flexDTDouble);
			AuditGrid.ColDataType(colExempt, FCGrid.DataTypeSettings.flexDTDouble);
			AuditGrid.ColDataType(colTot, FCGrid.DataTypeSettings.flexDTDouble);
			AuditGrid.ColDataType(colTax, FCGrid.DataTypeSettings.flexDTDouble);
		}

		private void SetupTotalGrid()
		{
			TotalGrid.FixedCols = 0;
			TotalGrid.FixedRows = 1;
			TotalGrid.Rows = 3;
			TotalGrid.Cols = 7;
			TotalGrid.ExtendLastCol = true;
			TotalGrid.FontBold = true;
			TotalGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.TextMatrix(1, 0, "Total");
			TotalGrid.TextMatrix(2, 0, "Totally Exempt w Homesteads");
			TotalGrid.TextMatrix(0, 1, "Count");
			TotalGrid.TextMatrix(0, 2, "Land");
			TotalGrid.TextMatrix(0, 3, "Building");
			TotalGrid.TextMatrix(0, 4, "Exempt");
			TotalGrid.TextMatrix(0, 5, "Total");
			TotalGrid.TextMatrix(0, 6, "Tax");
			GridSummary.FixedCols = 0;
			GridSummary.FixedRows = 1;
			GridSummary.Rows = 3;
			GridSummary.Cols = 7;
			GridSummary.ExtendLastCol = true;
			GridSummary.FontBold = true;
			GridSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridSummary.TextMatrix(1, 0, "Total");
			GridSummary.TextMatrix(2, 0, "Tot. Exempt Hmstd");
			GridSummary.TextMatrix(0, 1, "Count");
			GridSummary.TextMatrix(0, 2, "Land");
			GridSummary.TextMatrix(0, 3, "Building");
			GridSummary.TextMatrix(0, 4, "Exemptions");
			GridSummary.TextMatrix(0, 5, "Assessment");
			GridSummary.TextMatrix(0, 6, "Tax");
		}

		private void ResizeAuditGrid()
		{
			int GridWidth;
			GridWidth = AuditGrid.WidthOriginal;
			AuditGrid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.05));
			AuditGrid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.16));
			AuditGrid.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.11));
			AuditGrid.ColWidth(colSnum, FCConvert.ToInt32(GridWidth * 0.04));
			AuditGrid.ColWidth(colLoc, FCConvert.ToInt32(GridWidth * 0.13));
			AuditGrid.ColWidth(colLand, FCConvert.ToInt32(GridWidth * 0.08));
			AuditGrid.ColWidth(colBldg, FCConvert.ToInt32(GridWidth * 0.09));
			AuditGrid.ColWidth(colExempt, FCConvert.ToInt32(GridWidth * 0.1));
			AuditGrid.ColWidth(colTot, FCConvert.ToInt32(GridWidth * 0.12));
			AuditGrid.ColWidth(colTax, FCConvert.ToInt32(GridWidth * 0.08));
		}

		private void ResizeTotalGrid()
		{
			int GridWidth;
			GridWidth = TotalGrid.WidthOriginal;
			TotalGrid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.26));
			TotalGrid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.12));
			TotalGrid.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.12));
			TotalGrid.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.12));
			TotalGrid.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.12));
			TotalGrid.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.14));
			TotalGrid.ColWidth(6, FCConvert.ToInt32(GridWidth * 0.1));
			//TotalGrid.HeightOriginal = TotalGrid.RowHeight(0) * 3 + 30;
			GridWidth = GridSummary.WidthOriginal;
			GridSummary.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.16));
			GridSummary.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.09));
			GridSummary.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.14));
			GridSummary.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.15));
			GridSummary.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.15));
			GridSummary.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.15));
			GridSummary.ColWidth(6, FCConvert.ToInt32(GridWidth * 0.15));
			//GridSummary.HeightOriginal = GridSummary.RowHeight(0) * 3 + 30;
		}

		private void FillAuditGrid()
		{
			double dblTemp = 0;
			double[] dblWeights = new double[4 + 1];
			double[] dblTempDue = new double[4 + 1];
			bool boolAllEqual = false;
			bool boolPrePaid;
			bool boolHasTaxAcquireds;
			string strTemp = "";
			string strTemp2 = "";
			string strTemp3 = "";
			clsDRWrapper clsAudit = new clsDRWrapper();
			clsDRWrapper clsBill = new clsDRWrapper();
			clsDRWrapper rsAddresses = new clsDRWrapper();
			int lngLand = 0;
			int lngBldg = 0;
			int lngExempt = 0;
			int lngAssess = 0;
			int lngCount;
			int lngHomesteadLand = 0;
			int lngHomesteadBldg = 0;
			int lngHomesteadExempt = 0;
			int lngHomesteadCount;
			double dblTax = 0;
			string strWhere = "";
			string strWhere2 = "";
			int intResponse = 0;
			bool boolOVAll = false;
			bool boolNewAll = false;
			bool boolSkipAll;
			bool boolAlreadyAsked;
			int x;
			int y;
			bool boolWriteBill = false;
			bool boolNewBill = false;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp = 0;
			double dblTempTax = 0;
			double dblTaxperPeriod = 0;
			int intNumPeriods = 0;
			double dblLeftOver = 0;
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper rsTemp2 = new clsDRWrapper();
			clsDRWrapper clsBookPage = new clsDRWrapper();
			double dblTotalTax = 0;
			string strSQL = "";
			string strLCodes = "";
			string strECodes = "";
			string strBCodes = "";
			string strSQLFix = "";
			string strBookPage = "";
			string strBookPageDate = "";
			int lngRateToReplace = 0;
			double dblDiscount = 0;
			bool boolApplyDiscount = false;
			double dblReturnAmount = 0;
			bool boolCancelBill = false;
			bool boolUseCurrentOwner = false;
			DateTime dtPrePaymentEffectiveInterestDate;
			string[] strAry = null;
			// vbPorter upgrade warning: dblTotalTaxAcquired As double	OnReadFCConvert.ToDecimal(
			double dblTotalTaxAcquired = 0;
			modBudgetaryAccounting.FundType[] FundRec = null;
			int intCurFundRec = 0;
			string strNameWhere;
			cParty tParty;
			// vbPorter upgrade warning: tAddr As cPartyAddress	OnWrite(Collection)
			cPartyAddress tAddr;
			clsDRWrapper rsRE = new clsDRWrapper();
			string strREGroupToUse;
			FCUtils.StartTask(this, () =>
			{
				this.ShowWait();
				try
				{
					// On Error GoTo ErrorHandler
					strREGroupToUse = "Live";
					if (strGroupToUse != "")
					{
						strREGroupToUse = strGroupToUse;
					}
					rsRE.GroupName = strREGroupToUse;
					rsAddresses.GroupName = rsRE.GroupName;
					strNameWhere = "";
					boolHasTaxAcquireds = false;
					switch (intWhich)
					{
						case -1:
							{
								// all
								strWhere = "";
								strWhere2 = "";
								break;
							}
						case 0:
							{
								// account
								if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
								{
									strWhere = " and master.rsaccount between " + strBegin + " and " + strEnd;
								}
								else
								{
									strTemp = " and (";
									strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= Information.UBound(strAry, 1); x++)
									{
										strTemp += "(master.rsaccount = " + strAry[x] + ") or";
									}
									// x
									strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
									// get rid of last or
									strTemp += ") ";
									strWhere = strTemp;
								}
								break;
							}
						case 1:
							{
								// name
                                strWhere = " and deedname1 between '" + strBegin + "' and '" + strEnd + "'";
								break;
							}
						case 2:
							{
								// maplot
								if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
								{
									strWhere = " and rsmaplot between '" + strBegin + "' and '" + strEnd + "'";
								}
								else
								{
									strTemp = " and (";
									strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= Information.UBound(strAry, 1); x++)
									{
										strTemp += "(rsmaplot = '" + strAry[x] + "') or";
									}
									// x
									strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
									// get rid of last or
									strTemp += ") ";
									strWhere = strTemp;
								}
								break;
							}
						case 3:
							{
								// location
								if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
								{
									strWhere = " and rslocstreet between '" + strBegin + "' and '" + strEnd + "'";
								}
								else
								{
									strTemp = " and (";
									strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= Information.UBound(strAry, 1); x++)
									{
										strTemp += "(rslocstreet = '" + strAry[x] + "') or";
									}
									// x
									strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
									// get rid of last or
									strTemp += ") ";
									strWhere = strTemp;
								}
								break;
							}
						case 4:
							{
								// tran code
								if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
								{
									strWhere = " and ritrancode between " + strBegin + " and " + strEnd;
								}
								else
								{
									strTemp = " and (";
									strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= Information.UBound(strAry, 1); x++)
									{
										strTemp += "(ritrancode = " + strAry[x] + ") or";
									}
									// x
									strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
									// get rid of last or
									strTemp += ") ";
									strWhere = strTemp;
								}
								break;
							}
						case 5:
							{
								// land code
								if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
								{
									strWhere = " and rilandcode between " + strBegin + " and " + strEnd;
								}
								else
								{
									strTemp = " and (";
									strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= Information.UBound(strAry, 1); x++)
									{
										strTemp += "(rilandcode = " + strAry[x] + ") or";
									}
									// x
									strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
									// get rid of last or
									strTemp += ") ";
									strWhere = strTemp;
								}
								break;
							}
						case 6:
							{
								// bldg code
								if (Strings.InStr(1, strBegin, ",", CompareConstants.vbTextCompare) <= 0)
								{
									strWhere = " and ribldgcode between " + strBegin + " and " + strEnd;
								}
								else
								{
									strTemp = " and (";
									strAry = Strings.Split(strBegin, ",", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= Information.UBound(strAry, 1); x++)
									{
										strTemp += "(ribldgcode = " + strAry[x] + ") or";
									}
									// x
									strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
									// get rid of last or
									strTemp += ") ";
									strWhere = strTemp;
								}
								break;
							}
					}
					//end switch
					if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
					{
						strWhere += " and ritrancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
					}
					if (intWhereFrom == 3)
					{
						strTemp2 = "between " + strBegin;
						strTemp3 = "and " + strEnd;
						switch (intWhich)
						{
							case -1:
								{
									strTemp = "All";
									strTemp2 = "";
									strTemp3 = "";
									break;
								}
							case 0:
								{
									strTemp = " range of Account";
									break;
								}
							case 1:
								{
									strTemp = " range of Name";
									break;
								}
							case 2:
								{
									strTemp = " range of MapLot";
									break;
								}
							case 3:
								{
									strTemp = " range of Location";
									break;
								}
							case 4:
								{
									strTemp = " range of Tran Code";
									break;
								}
							case 5:
								{
									strTemp = " range of Land Code";
									break;
								}
							case 6:
								{
									strTemp = " range of Bldg Code";
									break;
								}
						}
						//end switch
						if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
						{
							modGlobalFunctions.AddCYAEntry("CL", "Created RE Bills", "By " + strTemp, strTemp2, strTemp3, "Rate Key " + FCConvert.ToString(clsRateInfo.RateKey));
						}
						else
						{
							modGlobalFunctions.AddCYAEntry("CL", "Created RE Bills for towncode " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown), "By " + strTemp, strTemp2, strTemp3, "Rate Key " + FCConvert.ToString(clsRateInfo.RateKey));
						}
						lngRateToReplace = 0;
						clsAudit.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
						if (FCConvert.ToBoolean(clsAudit.Get_Fields_Boolean("applydiscounts")))
						{
							boolApplyDiscount = true;
							clsAudit.OpenRecordset("select top 1 * from pendingdiscount", "twcl0000.vb1");
							if (!clsAudit.EndOfFile())
							{
								if (MessageBox.Show("Unfinalized pending discounts found. Add to these?" + "\r\n" + "Answering no will clear the previous pending discounts.", "Pending Discounts Found", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
								{
									clsAudit.Execute("delete from pendingdiscount", "twcl0000.vb1");
									modGlobalFunctions.AddCYAEntry_26("CL", "Deleted pending discounts", "Bill Year " + FCConvert.ToString(clsRateInfo.TaxYear));
								}
							}
						}
						else
						{
							boolApplyDiscount = false;
						}
                        // Check if there are records with the same ratekey or if there are already
                        // regular bills out there.  If so, ask what to do
                        clsAudit.OpenRecordset("select ratekey from (select BILLINGMASTER.* from " + rsRE.CurrentPrefix + "RealEstate.dbo.master inner join (billingmaster inner join RATEREC on (billingmaster.ratekey = raterec.ID) ) on (billingmaster.account = master.rsaccount)  where billingmaster.ratekey > 0 and ratetype = 'R' and billingyear between " + FCConvert.ToString(clsRateInfo.TaxYear) + "0 and " + FCConvert.ToString(clsRateInfo.TaxYear) + "9 AND billingmaster.billingtype = 'RE' and master.rscard = 1 " + strWhere + " " + strNameWhere + " ) as temp group by ratekey", "Collections");
						if (!clsAudit.EndOfFile())
						{
							// there are already some bill records out there
							if (clsAudit.FindFirstRecord("ratekey", clsRateInfo.RateKey))
							{
								// already some of the same rate key
								// must overwrite,skip or cancel								
								intResponse = frmGetOptions.InstancePtr.Init(1, "Bills Already Exist", "There are already bills for this rate key with values for this tax year.", "Skip these bills", strOption3: "Overwrite these bills", strOption5: "Cancel");
								switch (intResponse)
								{
									case -1:
										{
											// cancel
											boolDoPPAfter = false;
                                            // abort everything
                                            this.EndWait();
                                            Close();
											return;
										}
									case 1:
										{
											// skip
											boolSkipAll = true;
											boolOVAll = false;
											boolNewAll = false;
											break;
										}
									case 3:
										{
											// overwrite
											boolOVAll = true;
											boolSkipAll = false;
											boolNewAll = false;
											lngRateToReplace = clsRateInfo.RateKey;
											modGlobalFunctions.AddCYAEntry_242("CL", "OV Bills", "Billing overwrote RE bills for year " + Strings.Left(FCConvert.ToString(clsRateInfo.TaxYear), 4), "Rate " + FCConvert.ToString(lngRateToReplace), "With rate " + FCConvert.ToString(lngRateToReplace));
											break;
										}
									case 5:
										{
											// cancel
											boolDoPPAfter = false;
                                            // abort everything
                                            this.EndWait();
                                            Close();
											return;
										}
								}
								//end switch
							}
							else
							{
								// overwrite,add new, skip or cancel
								// if overwrite and more than one, must pick the ratekey to overwrite
								intResponse = frmGetOptions.InstancePtr.Init(1, "Bills Already Exist", "There are already bills with other rate key(s) with values for this tax year", "Skip these bills", "Overwrite these bills", "Create New Bills", "Cancel");
								switch (intResponse)
								{
									case -1:
										{
											// cancel
											boolDoPPAfter = false;
                                            this.EndWait();
                                            Close();
											return;
										}
									case 1:
										{
											// skip
											boolSkipAll = true;
											boolOVAll = false;
											boolNewAll = false;
											break;
										}
									case 2:
										{
											// overwrite
											boolSkipAll = false;
											boolOVAll = true;
											boolNewAll = false;
											break;
										}
									case 3:
										{
											// add new
											boolSkipAll = false;
											boolOVAll = false;
											boolNewAll = true;
											modGlobalFunctions.AddCYAEntry_80("CL", "New Bills", "Billing made 2nd etc bills for year " + Strings.Left(FCConvert.ToString(clsRateInfo.TaxYear), 4), "Rate Key " + FCConvert.ToString(clsRateInfo.RateKey));
											break;
										}
									case 4:
										{
											// cancel
											boolDoPPAfter = false;
                                            this.EndWait();
                                            Close();
											return;
										}
								}
								//end switch
								if (boolOVAll)
								{
									// check if there is more than one rate rec
									// if so, make them choose the one to overwrite
									if (clsAudit.RecordCount() > 1)
									{
										strTemp = frmPickRateToPrint.InstancePtr.Init(clsRateInfo.TaxYear, true, false, false, "RE", "Overwrite bills that use which rate record?");
										if (strTemp == "CANCEL" || strTemp == string.Empty)
										{
											boolDoPPAfter = false;
                                            this.EndWait();
                                            Close();
											return;
										}
										// returns string of ratekey = x
										strTemp = Strings.Trim(Strings.Mid(strTemp, Strings.InStr(1, strTemp, "=", CompareConstants.vbTextCompare) + 1));
										// everything after the =
										lngRateToReplace = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
									}
									else
									{
										lngRateToReplace = FCConvert.ToInt32(clsAudit.Get_Fields_Int32("ratekey"));
									}
									modGlobalFunctions.AddCYAEntry_242("CL", "OV Bills", "Billing overwrote RE bills for year " + Strings.Left(FCConvert.ToString(clsRateInfo.TaxYear), 4), "Rate " + FCConvert.ToString(clsRateInfo.RateKey), "With rate " + FCConvert.ToString(lngRateToReplace));
								}
							}
						}
					}
					if (intWhereFrom == 3)
					{
						this.UpdateWait("Transferring information");
					}
					else
					{
						this.UpdateWait("Loading Audit");
					}
					//Application.DoEvents();
					clsAudit.GroupName = rsRE.GroupName;
					if (intWhereFrom == 3)
					{
						if (!modGlobalVariables.Statics.CustomizedInfo.boolZeroLiabilityOption)
						{
							strSQL = "select  * from master inner join (select rsaccount,sum(piacres) as acretot,sum(convert(float,lastlandval)) as landtot,sum(convert(float,lastbldgval)) as bldgtot, sum(convert(float,rlexemption)) as exempttot,sum(convert(float,RSsoft)) as softAcres,sum(convert(float,RSmixed)) as mixedacres,sum(convert(float,RShard)) as hardacres,sum(convert(float,RSsoftvalue)) as softtot,";
							strSQL += " sum(convert(float,RSmixedvalue)) as mixedtot,sum(convert(float,RShardvalue)) as hardtot from master where rsdeleted <> 1 and rsaccount > 0 group by rsaccount having (sum(convert(float,lastlandval)) + sum(convert(float,lastbldgval)) - sum(convert(float,rlexemption)) > 0) or (sum(convert(float,homesteadvalue)) > 0)) as audittransfer on (audittransfer.rsaccount = master.rsaccount)  where master.rsdeleted <> 1 and master.rscard = 1 " + strWhere + " order by MASTER.rsaccount";
						}
						else
						{
							strSQL = "select  * from master inner join (select rsaccount,sum(piacres) as acretot,sum(convert(float,lastlandval)) as landtot,sum(convert(float,lastbldgval)) as bldgtot, sum(convert(float,rlexemption)) as exempttot,sum(RSsoft) as softAcres,sum(RSmixed) as mixedacres,sum(RShard) as hardacres,sum(convert(float,RSsoftvalue)) as softtot,sum(convert(float,RSmixedvalue)) as mixedtot,sum(convert(float,RShardvalue)) as hardtot from master where rsdeleted <> 1 and rsaccount > 0 group by rsaccount) as audittransfer on (audittransfer.rsaccount = master.rsaccount) where master.rsdeleted <> 1 and master.rscard = 1 " + strWhere + " order by MASTER.rsaccount";
						}
						clsAudit.OpenRecordset(strSQL, "RealEstate");
						// get rid of any that were put here in the last run but data changed and they should no longer be there
						if (!modGlobalVariables.Statics.CustomizedInfo.boolZeroLiabilityOption)
						{
                            strSQLFix = "(select rsaccount from " + rsRE.CurrentPrefix + "RealEstate.dbo.master where rsdeleted <> 1 and rsaccount > 0 " + strWhere + " " + strNameWhere + " group by rsaccount having (sum(convert(float,lastlandval)) + sum(convert(float,lastbldgval)) - sum(convert(float,rlexemption)) = 0) and (sum(convert(float,homesteadVALUE)) = 0))";
                            strSQLFix = "update billingmaster set TRANSferfrombillingdatefirst = 0,transferfrombillingdatelast = 0,homesteadexemption = 0,otherexempt1 = 0,otherexempt2 = 0,ratekey = 0,Landvalue = 0,buildingvalue = 0,exemptvalue = 0,taxdue1 = 0,taxdue2 = 0,taxdue3 = 0,taxdue4 = 0 where  billingtype = 'RE' and billingyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and account in " + strSQLFix;
							clsTemp.Execute(strSQLFix, "twcl0000.vb1");
						}
						strSQLFix = "(SELECT rsaccount from " + clsTemp.CurrentPrefix + "RealEstate.dbo.master  where rsdeleted = 1 and rsaccount > 0 group by rsaccount)";
						strSQLFix = "update billingmaster set TRANSferfrombillingdatefirst = 0,transferfrombillingdatelast = 0,homesteadexemption = 0,otherexempt1 = 0,otherexempt2 = 0,ratekey = 0,Landvalue = 0,buildingvalue = 0,exemptvalue = 0,taxdue1 = 0,taxdue2 = 0,taxdue3 = 0,taxdue4 = 0 where  billingtype = 'RE' and billingyear between " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and " + FCConvert.ToString(clsRateInfo.TaxYear) + "9 and ratekey = " + FCConvert.ToString(clsRateInfo.RateKey) + " and account in " + strSQLFix;
						clsTemp.Execute(strSQLFix, "twcl0000.vb1");
						if (!modGlobalVariables.Statics.CustomizedInfo.boolZeroLiabilityOption)
						{
							strSQLFix = "delete from billingmaster where billingtype = 'RE' and billingyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and taxdue1 = 0 and taxdue2 = 0 and taxdue3 = 0 and taxdue4 = 0 and principalpaid = 0 and interestpaid = 0 and homesteadexemption = 0";
							clsTemp.Execute(strSQLFix, "twcl0000.vb1");
						}
					}
					else
					{
						if (!modGlobalVariables.Statics.CustomizedInfo.boolZeroLiabilityOption)
						{							
							strSQL = "select  * from master inner join (select rsaccount,sum(piacres ) as acretot,sum(convert(float,lastlandval)) as landtot,sum(convert(float,lastbldgval)) as bldgtot, sum(convert(float,rlexemption)) as exempttot,sum(RSsoft) as softAcres,sum(RSmixed) as mixedacres,sum(RShard) as hardacres,sum(convert(float,RSsoftvalue)) as softtot,sum(convert(float,RSmixedvalue)) as mixedtot,sum(convert(float,RShardvalue)) as hardtot from master where rsdeleted <> 1 and rsaccount > 0 group by rsaccount having (sum(convert(float,lastlandval)) ";
							strSQL += " + sum(convert(float,lastbldgval)) - sum(convert(float,rlexemption)) > 0) or (sum(convert(float,homesteadvalue)) > 0)) as audittransfer on (audittransfer.rsaccount = master.rsaccount) where master.rsdeleted <> 1 and master.rscard = 1 " + strWhere + " order by MASTER.rsaccount";
						}
						else
						{
                            strSQL = "select  * from master inner join (select rsaccount,sum(piacres ) as acretot,sum(convert(float,lastlandval)) as landtot,sum(convert(float,lastbldgval)) as bldgtot, sum(convert(float,rlexemption)) as exempttot,sum(RSsoft) as softAcres,sum(RSmixed) as mixedacres,sum(RShard) as hardacres,sum(convert(float,RSsoftvalue)) as softtot,sum(convert(float,RSmixedvalue)) as mixedtot,sum(convert(float,RShardvalue)) as hardtot from master where rsdeleted <> 1 and rsaccount > 0 group by rsaccount) as audittransfer on (audittransfer.rsaccount = master.rsaccount) where master.rsdeleted <> 1 and master.rscard = 1 " + strWhere + " " + strNameWhere + " order by MASTER.rsaccount";                           
						}
						clsAudit.OpenRecordset(strSQL, "RealEstate");
					}
					if (clsAudit.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("No accounts in this range to process.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.EndWait();
                        Close();
						return;
					}
					lngTotLand = 0;
					lngTotBldg = 0;
					lngTotExempt = 0;
					lngTotAssess = 0;
					dblTotTax = 0;
					lngCount = 0;
					lngHomesteadCount = 0;
					AuditGrid.Redraw = false;
					string sFullName = "";
					int AuditGridRowsCount = clsAudit.RecordCount();
					AuditGrid.Rows = AuditGridRowsCount + AuditGrid.FixedRows;
					for (int i = 0; i < AuditGridRowsCount; i++)
					{
						// TODO Get_Fields: Field [landtot] not found!! (maybe it is an alias?)
						lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAudit.Get_Fields("landtot"))));
						// TODO Get_Fields: Field [bldgtot] not found!! (maybe it is an alias?)
						lngBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAudit.Get_Fields("bldgtot"))));
						// TODO Get_Fields: Field [exempttot] not found!! (maybe it is an alias?)
						lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAudit.Get_Fields("exempttot"))));
						lngAssess = lngLand + lngBldg - lngExempt;
						dblTax = modMain.Round(lngAssess * clsRateInfo.TaxRate, 2);
						if (intWhereFrom != 3)
						{
							if (lngAssess == 0)
							{
								lngHomesteadLand += lngLand;
								lngHomesteadBldg += lngBldg;
								lngHomesteadExempt += lngExempt;
								lngHomesteadCount += 1;
							}
							else
							{
								dblTotTax += dblTax;
								lngTotLand += lngLand;
								lngTotBldg += lngBldg;
								lngTotExempt += lngExempt;
								lngTotAssess += lngAssess;
								lngCount += 1;
							}
						}
						this.UpdateWait("Processing " + clsAudit.Get_Fields_Int32("rsaccount"));
						if (FCConvert.ToBoolean(clsAudit.Get_Fields_Boolean("taxacquired")))
						{
							boolHasTaxAcquireds = true;
						}
						strECodes = "Exempt Codes:  " + FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("riexemptcd1"))) + "   " + FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("riexemptcd2"))) + "   " + FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("riexemptcd3")));
						strLCodes = "";
						if (Conversion.Val(clsAudit.Get_Fields_Int32("piland1type")) > 0)
						{
							strLCodes += FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("piland1type"))) + " ";
						}
						if (Conversion.Val(clsAudit.Get_Fields_Int32("piland2type")) > 0)
						{
							strLCodes += FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("piland2type"))) + " ";
						}
						if (Conversion.Val(clsAudit.Get_Fields_Int32("piland3type")) > 0)
						{
							strLCodes += FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("piland3type"))) + " ";
						}
						if (Conversion.Val(clsAudit.Get_Fields_Int32("piland4type")) > 0)
						{
							strLCodes += FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("piland4type"))) + " ";
						}
						if (Conversion.Val(clsAudit.Get_Fields_Int32("piland5type")) > 0)
						{
							strLCodes += FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("piland5type"))) + " ";
						}
						if (Conversion.Val(clsAudit.Get_Fields_Int32("piland6type")) > 0)
						{
							strLCodes += FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("piland6type"))) + " ";
						}
						if (Conversion.Val(clsAudit.Get_Fields_Int32("piland7type")) > 0)
						{
							strLCodes += FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("piland7type"))) + " ";
						}
						if (strLCodes != string.Empty)
						{
							// TODO Get_Fields: Field [acretot] not found!! (maybe it is an alias?)
							strLCodes = "Land: " + FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("acretot"))) + " Acres  Codes: " + strLCodes;
						}
						else
						{
							// TODO Get_Fields: Field [acretot] not found!! (maybe it is an alias?)
							strLCodes = "Land : " + FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("acretot"))) + " Acres";
						}
						//FC:FINAL:IPI - #1491 - performance improvement: do not create dummy cParty objects
						sFullName = clsAudit.Get_Fields_String("Deedname1");
						AuditGrid[0, i].Value = clsAudit.Get_Fields_Int32("rsaccount");
						AuditGrid[1, i].Value = sFullName;
						AuditGrid[2, i].Value = clsAudit.Get_Fields_String("rsmaplot");
						AuditGrid[3, i].Value = FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_String("rslocnumalph")));
						AuditGrid[4, i].Value = Strings.Trim(FCConvert.ToString(clsAudit.Get_Fields_String("rslocstreet")));
						AuditGrid[5, i].Value = Strings.Format(lngLand, "#,###,###,##0");
						AuditGrid[6, i].Value = Strings.Format(lngBldg, "#,###,###,##0");
						AuditGrid[7, i].Value = Strings.Format(lngExempt, "#,###,###,##0");
						AuditGrid[8, i].Value = Strings.Format(lngAssess, "#,###,###,##0");
						AuditGrid[9, i].Value = Strings.Format(dblTax, "#,###,###,##0.00");
						AuditGrid[10, i].Value = strLCodes;
						AuditGrid[11, i].Value = strBCodes;
						AuditGrid[12, i].Value = strECodes;
					SkipHere:
						;
						clsAudit.MoveNext();
						// frmWait.lblMessage.Refresh
					}
					AuditGrid.Redraw = true;
					if (intWhereFrom == 3)
					{
						//Application.DoEvents();
						if (boolHasTaxAcquireds && modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable))
							{
								// Unload frmWait
								//FC:FINAL:IPI - #1491 - hiding bbusy window will deactivate current form and MsgBox will be displayed in background
								//bbusy.StopBusy();
								//bbusy.Unload();
								/*- bbusy = null; */
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
								MessageBox.Show("You must set up a valid Tax Acquired Receivable account in Budgetary before creating Real Estate bills", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								//FC:FINAL:IPI - #1491 - unload bbusy after showing MsgBox
								//bbusy.StopBusy();
								//bbusy.Unload();
								this.EndWait();
								Close();
								return;
							}
						}
						clsBill.Execute("update collections set lastbilledyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1", "twcl0000.vb1");
						if (lngRateToReplace > 0)
						{
							// get any with this ratekey or a dash one with only pre-payments on it
							clsBill.OpenRecordset("select * from billingmaster where billingtype = 'RE' and ((billingyear between " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and " + FCConvert.ToString(clsRateInfo.TaxYear) + "9 and ratekey = " + FCConvert.ToString(lngRateToReplace) + ") or (billingyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and (taxdue1 + taxdue2 + taxdue3 + taxdue4) <= 0 and ratekey = 0))", "twcl0000.vb1");
						}
						else
						{
							// no dash ones with tax due exist
							clsBill.OpenRecordset("select * from billingmaster where billingyear = " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and billingtype = 'RE' order by account", "twcl0000.vb1");
						}
						// initialize to 0.  This keeps track of tax to send to budgetary
						dblTotalTax = 0;
						dblTotalTaxAcquired = 0;
						clsAudit.MoveFirst();
						for (x = 1; x <= AuditGrid.Rows - 1; x++)
						{
							boolPrePaid = false;
							this.UpdateWait("Processing " + clsAudit.Get_Fields_Int32("Rsaccount"));
							//Application.DoEvents();
							if (intWhereFrom == 3)
							{
								// We aren't doing an audit, we're building billing records
								boolWriteBill = false;
								boolCancelBill = false;
								clsBill.FindFirst("account = " + clsAudit.Get_Fields_Int32("rsaccount"));
								boolNewBill = false;
								if (!clsBill.NoMatch)
								{
									// make sure we aren't overwriting some bills
									if ((Conversion.Val(clsBill.Get_Fields_Decimal("taxdue1")) > 0) || (Conversion.Val(clsBill.Get_Fields_Decimal("taxdue2")) > 0) || (Conversion.Val(clsBill.Get_Fields_Decimal("taxdue3")) > 0) || (Conversion.Val(clsBill.Get_Fields_Decimal("taxdue4")) > 0) || clsBill.Get_Fields_Int32("ratekey") > 0)
									{
										boolNewBill = false;
										if (boolOVAll || boolNewAll)
										{
											boolWriteBill = true;
											if (boolNewAll)
												boolNewBill = true;
										}
										else
										{
											// skip
											boolWriteBill = false;
										}
									}
									else
									{
										boolWriteBill = true;
										if (Conversion.Val(clsBill.Get_Fields_Decimal("principalpaid")) > 0)
										{
											boolPrePaid = true;
										}
									}
									// if taxdue1,2,3,4 > 0
									if (boolWriteBill)
									{
										if (!boolNewBill)
										{
											clsBill.Edit();
										}
										else
										{
											clsBill.AddNew();
											clsBill.Update();
										}
									}
								}
								else
								{
									boolWriteBill = true;
									boolNewBill = true;
									clsBill.AddNew();
									clsBill.Update();
								}
								// if not nomatch
								if (boolWriteBill)
								{
									lngLand = FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colLand)));
									lngBldg = FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBldg)));
									lngExempt = FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colExempt)));
									lngAssess = lngLand + lngBldg - lngExempt;
									dblTax = modMain.Round(lngAssess * clsRateInfo.TaxRate, 2);
									if (lngAssess == 0)
									{
										lngHomesteadLand += lngLand;
										lngHomesteadBldg += lngBldg;
										lngHomesteadExempt += lngExempt;
										lngHomesteadCount += 1;
									}
									else
									{
										dblTotTax += dblTax;
										lngTotLand += lngLand;
										lngTotBldg += lngBldg;
										lngTotExempt += lngExempt;
										lngTotAssess += lngAssess;
										lngCount += 1;
									}
									clsBill.Set_Fields("account", AuditGrid.TextMatrix(x, 0));
									// RE account Number
									clsBill.Set_Fields("billingtype", "RE");
									// real estate bill
									if (!boolNewBill)
									{

									}
									else
									{
										// determine what one this is
										clsTemp.OpenRecordset("select max(billingyear) as maxyear from billingmaster where account = " + AuditGrid.TextMatrix(x, 0) + " and billingtype = 'RE' and billingyear between " + FCConvert.ToString(clsRateInfo.TaxYear) + "1 and " + FCConvert.ToString(clsRateInfo.TaxYear) + "9", modGlobalVariables.strCLDatabase);
										if (!clsTemp.EndOfFile())
										{
											// TODO Get_Fields: Field [maxyear] not found!! (maybe it is an alias?)
											lngTemp = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxyear")) - (clsRateInfo.TaxYear * 10));
											if (lngTemp < 1)
											{
												clsBill.Set_Fields("billingyear", FCConvert.ToString(clsRateInfo.TaxYear) + "1");
											}
											else
											{
												if (lngTemp < 9)
												{
													clsBill.Set_Fields("billingyear", FCConvert.ToString(clsRateInfo.TaxYear) + FCConvert.ToString(lngTemp + 1));
												}
												else
												{
													MessageBox.Show("There is already the max number of bills for account " + AuditGrid.TextMatrix(x, 0) + "." + "\r\n" + "Cannot create a bill for this account");
													modGlobalFunctions.AddCYAEntry_26("CL", "Max Bills for " + AuditGrid.TextMatrix(x, 0), "unable to make new bill");
													boolCancelBill = true;
												}
											}
										}
										else
										{
											clsBill.Set_Fields("billingyear", FCConvert.ToString(clsRateInfo.TaxYear) + "1");
										}
										clsBill.Set_Fields("interestappliedthroughdate", DateAndTime.DateAdd("d", -1, clsRateInfo.Get_InterestStartDate(1)));
									}
									// TODO Get_Fields: Field [acretot] not found!! (maybe it is an alias?)
									clsBill.Set_Fields("acres", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("acretot"))));
									// TODO Get_Fields: Field [softacres] not found!! (maybe it is an alias?)
									clsBill.Set_Fields("tgsoftacres", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("softacres"))));
									// TODO Get_Fields: Field [mixedacres] not found!! (maybe it is an alias?)
									clsBill.Set_Fields("tgmixedacres", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("mixedacres"))));
									// TODO Get_Fields: Field [hardacres] not found!! (maybe it is an alias?)
									clsBill.Set_Fields("tghardacres", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("hardacres"))));
									// TODO Get_Fields: Field [softtot] not found!! (maybe it is an alias?)
									clsBill.Set_Fields("tgsoftvalue", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("softtot"))));
									// TODO Get_Fields: Field [mixedtot] not found!! (maybe it is an alias?)
									clsBill.Set_Fields("tgmixedvalue", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("mixedtot"))));
									// TODO Get_Fields: Field [hardtot] not found!! (maybe it is an alias?)
									clsBill.Set_Fields("tghardvalue", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields("hardtot"))));
									clsBill.Set_Fields("exempt1", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("RIEXEMPTcd1"))));
									clsBill.Set_Fields("Exempt2", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("riexemptcd2"))));
									clsBill.Set_Fields("TRANCODE", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("ritrancode"))));
									clsBill.Set_Fields("BUILDINGcode", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("ribldgcode"))));
									// TODO Get_Fields: Field [EXEMPTTOT] not found!! (maybe it is an alias?)
									if (Conversion.Val(clsAudit.Get_Fields("EXEMPTTOT")) > 0)
									{
										clsBill.Set_Fields("homesteadexemption", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("homesteadvalue"))));
										// TODO Get_Fields: Field [exempttot] not found!! (maybe it is an alias?)
										clsBill.Set_Fields("otherexempt1", Conversion.Val(clsAudit.Get_Fields("exempttot")) - Conversion.Val(clsAudit.Get_Fields_Int32("homesteadvalue")));
										if (Conversion.Val(clsBill.Get_Fields_Double("otherexempt1")) <= 0)
											clsBill.Set_Fields("otherexempt1", 0);
									}
									else
									{
										clsBill.Set_Fields("homesteadexemption", 0);
										clsBill.Set_Fields("otherexempt1", 0);
									}
									if (fecherFoundation.FCUtils.IsEmptyDateTime(clsBill.Get_Fields_DateTime("transferfrombillingdatefirst")))
									{
										clsBill.Set_Fields("transferfrombillingdatefirst", DateTime.Now);
									}
									else
									{
										if (FCConvert.ToDateTime(clsBill.Get_Fields_DateTime("transferfrombillingdatefirst") as object).ToOADate() == 0)
										{
											clsBill.Set_Fields("transferfrombillingdatefirst", DateTime.Now);
										}
										else
										{
											clsBill.Set_Fields("transferfrombillingdatelast", DateTime.Now);
										}
									}
									clsBill.Set_Fields("exempt3", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_Int32("riexemptcd3"))));
									// check if using owner as of certain date and if previous records exist
									boolUseCurrentOwner = true;
									if (boolUseOwnerAsOf)
									{
										rsRE.OpenRecordset("select * from previousowner where ACCOUNT = " + AuditGrid.TextMatrix(x, 0) + " AND saledate > '" + strAsOfDate + "' and year(saledate) < " + FCConvert.ToString(DateTime.Today.Year + 1) + " order by saledate", "twre0000.vb1");
										if (!rsRE.EndOfFile())
										{
											boolUseCurrentOwner = false;
										}
									}
									clsTemp.Execute("delete from owners where associd = " + clsBill.Get_Fields_Int32("ID"), modGlobalVariables.strCLDatabase);
									clsBill.Set_Fields("MailingAddress3", "");
									if (boolUseCurrentOwner)
									{
										clsBill.Set_Fields("name1", clsAudit.Get_Fields_String("DeedName1"));
										clsBill.Set_Fields("name2", clsAudit.Get_Fields_String("DeedName2"));
										clsBill.Set_Fields("address1", "");
										clsBill.Set_Fields("address2", "");
										clsBill.Set_Fields("address3", "");
										clsBill.Set_Fields("MailingAddress3", "");
										clsBill.Set_Fields("zip", "");
										if (Conversion.Val(clsAudit.Get_Fields_Int32("ownerpartyid")) > 0)
										{
											tParty = tPCont.GetParty(clsAudit.Get_Fields_Int32("ownerpartyid"));
											if (!(tParty == null))
											{												
												if (tParty.Addresses.Count > 0)
												{
													tAddr = tParty.Addresses[1];
													if (!(tAddr == null))
													{
														clsBill.Set_Fields("address1", tAddr.Address1);
														clsBill.Set_Fields("address2", tAddr.Address2);
														clsBill.Set_Fields("address3", tAddr.City + " " + tAddr.State + " " + tAddr.Zip);
														clsBill.Set_Fields("MailingAddress3", tAddr.Address3);
														clsBill.Set_Fields("zip", tAddr.Zip);
													}
												}
											}
										}

										rsAddresses.OpenRecordset("select * from owners where account = " + AuditGrid.TextMatrix(x, 0) + " and associd = 0", modGlobalVariables.strREDatabase);
										rsTemp2.OpenRecordset("select * from owners where account = -1", modGlobalVariables.strCLDatabase);
										while (!rsAddresses.EndOfFile())
										{
											rsTemp2.AddNew();
											rsTemp2.Set_Fields("account", AuditGrid.TextMatrix(x, 0));
											rsTemp2.Set_Fields("module", "RE");
											rsTemp2.Set_Fields("associd", clsBill.Get_Fields_Int32("ID"));
											rsTemp2.Set_Fields("billyear", clsRateInfo.TaxYear);
											rsTemp2.Set_Fields("name", rsAddresses.Get_Fields_String("name"));
											rsTemp2.Set_Fields("address1", rsAddresses.Get_Fields_String("address1"));
											rsTemp2.Set_Fields("address2", rsAddresses.Get_Fields_String("address2"));
											rsTemp2.Set_Fields("city", rsAddresses.Get_Fields_String("city"));
											rsTemp2.Set_Fields("state", rsAddresses.Get_Fields_String("state"));
											rsTemp2.Set_Fields("zip", rsAddresses.Get_Fields_String("zip"));
											rsTemp2.Set_Fields("zip4", rsAddresses.Get_Fields_String("zip4"));
											rsTemp2.Update();
											rsAddresses.MoveNext();
										}
									}
									else
									{
										// use previous owner
										clsBill.Set_Fields("name1", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("Name"))));
										clsBill.Set_Fields("name2", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("secowner"))));
										clsBill.Set_Fields("address1", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("address1"))));
										clsBill.Set_Fields("address2", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("address2"))));
										clsBill.Set_Fields("address3", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("city"))) + " " + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("state"))) + " " + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("zip"))));
										clsBill.Set_Fields("zip", rsRE.Get_Fields_String("zip"));
										if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("zip4"))) != string.Empty)
										{
											clsBill.Set_Fields("address3", clsBill.Get_Fields_String("address3") + "-" + rsRE.Get_Fields_String("zip4"));
											clsBill.Set_Fields("zip", clsBill.Get_Fields_String("zip"));
										}
										AuditGrid.TextMatrix(x, 1, FCConvert.ToString(clsBill.Get_Fields_String("name1")));
										rsAddresses.OpenRecordset("select * from owners where account = " + AuditGrid.TextMatrix(x, 0) + " and associd = " + rsRE.Get_Fields_Int32("ID"), modGlobalVariables.strREDatabase);
										rsTemp2.OpenRecordset("select * from owners where account = -1", modGlobalVariables.strCLDatabase);
										while (!rsAddresses.EndOfFile())
										{
											rsTemp2.AddNew();
											rsTemp2.Set_Fields("account", AuditGrid.TextMatrix(x, 0));
											rsTemp2.Set_Fields("module", "RE");
											rsTemp2.Set_Fields("associd", clsBill.Get_Fields_Int32("ID"));
											rsTemp2.Set_Fields("billyear", clsRateInfo.TaxYear);
											rsTemp2.Set_Fields("name", rsAddresses.Get_Fields_String("name"));
											rsTemp2.Set_Fields("address1", rsAddresses.Get_Fields_String("address1"));
											rsTemp2.Set_Fields("address2", rsAddresses.Get_Fields_String("address2"));
											rsTemp2.Set_Fields("city", rsAddresses.Get_Fields_String("city"));
											rsTemp2.Set_Fields("state", rsAddresses.Get_Fields_String("state"));
											rsTemp2.Set_Fields("zip", rsAddresses.Get_Fields_String("zip"));
											rsTemp2.Set_Fields("zip4", rsAddresses.Get_Fields_String("zip4"));
											rsTemp2.Update();
											rsAddresses.MoveNext();
										}
									}
									clsBill.Set_Fields("maplot", Strings.Trim(FCConvert.ToString(clsAudit.Get_Fields_String("rsmaplot"))));
									clsBill.Set_Fields("streetnumber", FCConvert.ToString(Conversion.Val(clsAudit.Get_Fields_String("rslocnumalph"))));
									clsBill.Set_Fields("apt", Strings.Trim(FCConvert.ToString(clsAudit.Get_Fields_String("rslocapt"))));
									clsBill.Set_Fields("streetname", Strings.Trim(FCConvert.ToString(clsAudit.Get_Fields_String("rslocstreet"))));
									clsBill.Set_Fields("ref1", clsAudit.Get_Fields_String("rsref1"));
									clsBill.Set_Fields("ref2", clsAudit.Get_Fields_String("rsref2"));
									clsBill.Set_Fields("landvalue", FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colLand))));
									clsBill.Set_Fields("buildingvalue", FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBldg))));
									clsBill.Set_Fields("exemptvalue", FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colExempt))));
									clsBill.Set_Fields("whetherbilledbefore", "A");
									clsBill.Set_Fields("taxdue1", 0);
									clsBill.Set_Fields("taxdue2", 0);
									clsBill.Set_Fields("taxdue3", 0);
									clsBill.Set_Fields("taxdue4", 0);
									dblTempTax = FCConvert.ToDouble(AuditGrid.TextMatrix(x, colTax));
									// add to total tax here since you don't want to send it to budgetary
									// if you didn't write a bill for it, even though it's in the list
									if (!FCConvert.ToBoolean(clsAudit.Get_Fields_Boolean("taxacquired")))
									{
										dblTotalTax += dblTempTax;
									}
									else
									{
										dblTotalTaxAcquired += dblTempTax;
									}
									intNumPeriods = clsRateInfo.NumberOfPeriods;
									dblTaxperPeriod = dblTempTax / intNumPeriods;
									dblTaxperPeriod = modMain.Round(dblTaxperPeriod, 2);
									dblTempDue[0] = 0;
									dblTempDue[1] = 0;
									dblTempDue[2] = 0;
									dblTempDue[3] = 0;
									if (dblTempTax >= 100 || intNumPeriods < 4 || modGlobalVariables.Statics.CustomizedInfo.SplitSmallAmounts)
									{
										boolAllEqual = true;
										for (y = 1; y <= intNumPeriods; y++)
										{
											if (clsRateInfo.Get_BillWeight(y) != clsRateInfo.Get_BillWeight(1))
											{
												boolAllEqual = false;
											}
										}
										// y
										if (boolAllEqual)
										{
											for (y = 1; y <= intNumPeriods; y++)
											{
												clsBill.Set_Fields("taxdue" + y, modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(dblTaxperPeriod)), 2));
											}
											// y
											dblLeftOver = modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(dblTempTax - (dblTaxperPeriod * intNumPeriods))), 2);
											if (dblLeftOver < 0)
											{
												// make the last bill the smaller
												clsBill.Set_Fields("taxdue" + intNumPeriods, clsBill.Get_Fields_Decimal("taxdue" + FCConvert.ToString(intNumPeriods)) + FCConvert.ToDecimal(dblLeftOver));
											}
											else
											{
												// make the first bill the largest
												clsBill.Set_Fields("taxdue1", clsBill.Get_Fields_Decimal("taxdue1") + FCConvert.ToDecimal(dblLeftOver));
											}
											// if dblleftover
										}
										else
										{
											dblTemp = 0;
											dblLeftOver = dblTempTax;
											for (y = 1; y <= intNumPeriods - 1; y++)
											{
												if (dblLeftOver > 0)
												{
													dblTemp = modMain.Round(FCConvert.ToDouble(FCConvert.ToDecimal(clsRateInfo.Get_BillWeight(y) * dblTempTax)), 2);
													if (dblTemp <= dblLeftOver)
													{
														dblTempDue[y - 1] = dblTemp;
														dblLeftOver -= dblTemp;
													}
													else
													{
														dblTempDue[y - 1] = dblLeftOver;
														dblLeftOver = 0;
													}
												}
											}
											// y
											if (dblLeftOver > 0)
											{
												dblTempDue[intNumPeriods - 1] = dblLeftOver;
											}
											for (y = 1; y <= 4; y++)
											{
												clsBill.Set_Fields("TaxDue" + y, dblTempDue[y - 1]);
											}
											// y
										}
									}
									else
									{
										// if less than 100 then put it all to period 1
										clsBill.Set_Fields("taxdue1", dblTempTax);
									}
									clsBill.Set_Fields("ratekey", clsRateInfo.RateKey);
									strBookPage = modGlobalFunctions.GetCurrentBookPageString(FCConvert.ToInt32(AuditGrid.TextMatrix(x, 0)));
									while (strBookPage.Length >= 255)
									{
										y = Strings.InStrRev(strBookPage, " ", -1, CompareConstants.vbTextCompare);
										if (y < 1)
										{
											strBookPage = Strings.Left(strBookPage, 254);
										}
										else
										{
											strBookPage = Strings.Trim(Strings.Left(strBookPage, y));
										}
									}
									clsBill.Set_Fields("bookpage", Strings.Trim(strBookPage));
									// check for prepayments
									if (boolApplyDiscount)
									{
										if (!boolCancelBill)
										{
											clsBill.Update();
											clsBill.Edit();
											if (clsBill.EndOfFile())
											{
												// fixes a wierd bug when adding new and updating and editing
												clsBill.MovePrevious();
												clsBill.Edit();
											}
										}
										else
										{
											if (boolNewBill)
											{
												clsBill.Delete();
											}
										}
										clsTemp.OpenRecordset("select * from paymentrec where code = 'D' and billkey = " + clsBill.Get_Fields_Int32("ID"), "twcl0000.vb1");
										if (clsTemp.EndOfFile())
										{
											dblDiscount = 0;
											dblReturnAmount = 0;
											if (Conversion.Val(clsBill.Get_Fields_Decimal("principalpaid")) > 0)
											{
												int temp = FCConvert.ToInt32(AuditGrid.TextMatrix(x, 0));
												if (modCLDiscount.PrePaymentsPlusDiscountFinishesAccount(ref temp, FCConvert.ToInt32(Conversion.Val(clsRateInfo.TaxYear) + "1"), "RE", ref dblReturnAmount, ref dblDiscount))
												{
													clsTemp.Execute("delete from pendingdiscount where billkey = " + clsBill.Get_Fields_Int32("ID"), "twcl0000.vb1");
													modCLDiscount.AddDiscountAmountToPendingTable_78(dblDiscount, FCConvert.ToInt32(Conversion.Val(AuditGrid.TextMatrix(x, 0))), FCConvert.ToInt32(Conversion.Val(clsRateInfo.TaxYear) + "1"), "RE");
													modGlobalFunctions.AddCYAEntry_242("CL", "Applied discount", "RE Acct " + FCConvert.ToString(Conversion.Val(AuditGrid.TextMatrix(x, 0))), "Bill Year " + FCConvert.ToString(Conversion.Val(FCConvert.ToString(clsRateInfo.TaxYear) + "1")), "Discount " + FCConvert.ToString(dblReturnAmount));
												}
											}
										}
									}
									if (!boolCancelBill)
									{
										clsBill.Update();
									}
								}
								else
								{
									// mark as unwritten
									AuditGrid.RowHidden(x, true);
								}
								// if boolwritebill
							}
							clsAudit.MoveNext();
						}
						// x
					}
					if (intWhereFrom == 3)
					{
						for (x = AuditGrid.Rows - 1; x >= 1; x--)
						{
							if (AuditGrid.RowHidden(x))
							{
								AuditGrid.RemoveItem(x);
							}
						}
						// x
						clsTemp.OpenRecordset("select BillingMaster.ID as BillingMasterID, TaxClub.ID as TaxClubID, * from billingmaster inner join taxclub on (billingmaster.account = taxclub.account) and (billingmaster.billingtype = taxclub.type) and (billingmaster.billingyear = taxclub.year)  where taxclub.billkey <> billingmaster.ID", "Collections");
						if (clsTemp.EndOfFile() != true && clsTemp.BeginningOfFile() != true)
						{
							clsDRWrapper clsTemp20 = new clsDRWrapper();
							do
							{
								//Application.DoEvents();
								// TODO Get_Fields: Field [BillingMasterID] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TaxClubID] not found!! (maybe it is an alias?)
								clsTemp20.Execute("UPDATE TaxClub Set BillKey = " + clsTemp.Get_Fields("BillingMasterID") + " WHERE ID = " + clsTemp.Get_Fields("TaxClubID"), "Collections");
								clsTemp.MoveNext();
							}
							while (clsTemp.EndOfFile() != true);
						}
					}
					// Call AuditGrid.AddItem("Total")
					TotalGrid.TextMatrix(1, 2, Strings.Format(lngTotLand, "###,###,###,##0"));
					TotalGrid.TextMatrix(1, 3, Strings.Format(lngTotBldg, "###,###,###,##0"));
					TotalGrid.TextMatrix(1, 4, Strings.Format(lngTotExempt, "###,###,###,##0"));
					TotalGrid.TextMatrix(1, 5, Strings.Format(lngTotAssess, "###,###,###,##0"));
					TotalGrid.TextMatrix(1, 6, Strings.Format(dblTotTax, "###,###,##0.00"));
					TotalGrid.TextMatrix(1, 1, FCConvert.ToString(lngCount));
					TotalGrid.TextMatrix(2, 1, FCConvert.ToString(lngHomesteadCount));
					TotalGrid.TextMatrix(2, 2, Strings.Format(lngHomesteadLand, "###,###,###,##0"));
					TotalGrid.TextMatrix(2, 3, Strings.Format(lngHomesteadBldg, "###,###,###,##0"));
					TotalGrid.TextMatrix(2, 4, Strings.Format(lngHomesteadExempt, "###,###,###,##0"));
					TotalGrid.TextMatrix(2, 5, "0");
					TotalGrid.TextMatrix(2, 6, "0.00");
					GridSummary.TextMatrix(1, 2, Strings.Format(lngTotLand, "###,###,###,##0"));
					GridSummary.TextMatrix(1, 3, Strings.Format(lngTotBldg, "###,###,###,##0"));
					GridSummary.TextMatrix(1, 4, Strings.Format(lngTotExempt, "###,###,###,##0"));
					GridSummary.TextMatrix(1, 5, Strings.Format(lngTotAssess, "###,###,###,##0"));
					GridSummary.TextMatrix(1, 6, Strings.Format(dblTotTax, "###,###,##0.00"));
					GridSummary.TextMatrix(1, 1, FCConvert.ToString(lngCount));
					GridSummary.TextMatrix(2, 1, FCConvert.ToString(lngHomesteadCount));
					GridSummary.TextMatrix(2, 2, Strings.Format(lngHomesteadLand, "###,###,###,##0"));
					GridSummary.TextMatrix(2, 3, Strings.Format(lngHomesteadBldg, "###,###,###,##0"));
					GridSummary.TextMatrix(2, 4, Strings.Format(lngHomesteadExempt, "###,###,###,##0"));
					GridSummary.TextMatrix(2, 5, "0");
					GridSummary.TextMatrix(2, 6, "0.00");
					if (intWhereFrom == 3)
					{
						// send the total figure to budgetary
						// check to see that it hasn't expired
						if (modGlobalConstants.Statics.gboolBD)
						{
							int lngYear = 0;
							int lngReturn = 0;
							lngYear = clsRateInfo.TaxYear;
							if (dblTotalTax > 0 || dblTotalTaxAcquired > 0)
							{
								intCurFundRec = -1;
								// The database is found
								// Unload frmWait
								//FC:FINAL:IPI - #1491 - hiding bbusy window will deactivate current form and MsgBox will be displayed in background; unload bbusy later
								//bbusy.StopBusy();
								//bbusy.Unload();
								if (dblTotalTax > 0)
								{
									intCurFundRec += 1;
									Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
									FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.REReceivable;
									FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(dblTotalTax);
									FundRec[intCurFundRec].UseDueToFrom = false;
									FundRec[intCurFundRec].AcctType = "A";
									FundRec[intCurFundRec].RCB = "R";
									FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Tax Commitment";
									if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
									{
										FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
									}
								}
								if (dblTotalTaxAcquired > 0)
								{
									intCurFundRec += 1;
									Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
									FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable;
									FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(dblTotalTaxAcquired);
									FundRec[intCurFundRec].UseDueToFrom = false;
									FundRec[intCurFundRec].AcctType = "A";
									FundRec[intCurFundRec].RCB = "R";
									FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Tax Acquired";
									if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
									{
										FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
									}
								}
								intCurFundRec += 1;
								Array.Resize(ref FundRec, intCurFundRec + 1 + 1);
								FundRec[intCurFundRec].Account = modGlobalVariables.Statics.REPPAccountInfo.RECommitment;
								FundRec[intCurFundRec].Amount = FCConvert.ToDecimal(-(dblTotalTax + dblTotalTaxAcquired));
								FundRec[intCurFundRec].UseDueToFrom = false;
								FundRec[intCurFundRec].AcctType = "A";
								FundRec[intCurFundRec].RCB = "R";
								FundRec[intCurFundRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Tax Commitment";
								if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
								{
									FundRec[intCurFundRec].Description += " Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
									lngReturn = modBudgetaryAccounting.AddToJournal(ref FundRec, "GJ", 0, DateTime.Today, Strings.Right(FCConvert.ToString(lngYear), 2) + " Tax Commitment Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
								}
								else
								{
									lngReturn = modBudgetaryAccounting.AddToJournal(ref FundRec, "GJ", 0, DateTime.Today, FCConvert.ToString(lngYear) + " Tax Commitment");
								}
								if (lngReturn > 0)
								{
									MessageBox.Show("Data added to budgetary database in Journal " + FCConvert.ToString(lngReturn), "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
									if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
									{
										modGlobalFunctions.AddCYAEntry_242("CL", "Tax Commitment Journal" + FCConvert.ToString(lngReturn), "Year " + FCConvert.ToString(lngYear), "Tax " + FCConvert.ToString(dblTotalTax), "TAcq. " + FCConvert.ToString(dblTotalTaxAcquired));
									}
									else
									{
										modGlobalFunctions.AddCYAEntry_728("CL", "Tax Commitment Journal" + FCConvert.ToString(lngReturn), "Year " + FCConvert.ToString(lngYear), "Tax " + FCConvert.ToString(dblTotalTax), "TAcq. " + FCConvert.ToString(dblTotalTaxAcquired), "Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
									}
								}
								else
								{
									MessageBox.Show("The journal entry was not created in budgetary", "No Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
									{
										modGlobalFunctions.AddCYAEntry_242("CL", "Tax Comm Journal Failed", "Year " + FCConvert.ToString(lngYear), "Tax " + FCConvert.ToString(dblTotalTax), "TAcq. " + FCConvert.ToString(dblTotalTaxAcquired));
									}
									else
									{
										modGlobalFunctions.AddCYAEntry_728("CL", "Tax Comm Journal Failed", "Year " + FCConvert.ToString(lngYear), "Tax " + FCConvert.ToString(dblTotalTax), "TAcq. " + FCConvert.ToString(dblTotalTaxAcquired), "Town " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
									}
								}
							}
						}
						// run the report in the background so it can be saved without the user having to see it
						rptREAudit.InstancePtr.Unload();
						//FC:FINAL:CHN - issue #1388: Fix incorrect ordering (Different event calls in reports generating).
						// if (boolDisplayFirst)
						//     rptREAudit.InstancePtr.Run(true);
					}
					// Unload frmWait
					//bbusy.StopBusy();
					//bbusy.Unload();
					this.EndWait();
					//FC:FINAL:IPI - #1491 - hiding bbusy window will deactivate current form and MsgBox will be displayed in background; activate current form after unloading bbusy
					this.Activate();
					/*- bbusy = null; */
					mnuFile.Enabled = true;
					// If Not boolDisplayFirst Then
					if ((Strings.UCase(strSequence) == "NAME") || (Strings.UCase(strSequence) == ""))
					{
						AuditGrid.Col = colName;
						AuditGrid.Sort = FCGrid.SortSettings.flexSortStringAscending;
					}
					else if (Strings.UCase(strSequence) == "ACCOUNT")
					{
						AuditGrid.Col = colAcct;
						AuditGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
					}
					else if (Strings.UCase(strSequence) == "MAPLOT")
					{
						AuditGrid.Col = colMapLot;
						AuditGrid.Sort = FCGrid.SortSettings.flexSortStringAscending;
					}
					else if (Strings.UCase(strSequence) == "TAX")
					{
						AuditGrid.Col = colTax;
						AuditGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
					}
					else if (Strings.UCase(strSequence) == "LOCATION")
					{
						AuditGrid.Col = colSnum;
						AuditGrid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
						AuditGrid.Col = colLoc;
						AuditGrid.Sort = FCGrid.SortSettings.flexSortStringAscending;
					}
					//FC:FINAL:CHN - issue #1388: Fix incorrect ordering (Different event calls in reports generating).
					if (intWhereFrom == 3 && boolDisplayFirst)
						rptREAudit.InstancePtr.Run(true);
					if (!boolDisplayFirst)
					{
						mnuPrint_Click();
					}
					else
					{
						boolQueryPrint = true;
					}
                    this.EndWait();
					return;
				}
				catch (Exception ex)
				{
					// ErrorHandler:
					// Unload frmWait
					//bbusy.Unload();
					this.EndWait();
					//FC:FINAL:IPI - #1491 - hiding bbusy window will deactivate current form and MsgBox will be displayed in background; activate current form after unloading bbusy
					this.Activate();
					/*- bbusy = null; *///Application.DoEvents();
					mnuFile.Enabled = true;
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fill Audit Grid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			});
		}

		public void Init(ref clsRateRecord RInfo, short intCalledFrom, bool boolDoBoth, string strDBGroupToUse = "Live")
		{
			int x;
			boolUseOwnerAsOf = false;
			strAsOfDate = "";
			strGroupToUse = strDBGroupToUse;
			mnuFile.Enabled = false;
			clsRateInfo.BillingDate = RInfo.BillingDate;
			clsRateInfo.NumberOfPeriods = RInfo.NumberOfPeriods;
			intWhereFrom = intCalledFrom;
			if (intWhereFrom == 3)
			{
				lblDoubleClick.Visible = false;
				mnuView.Visible = true;
				cmdView.Visible = true;
				framSummary.Visible = true;
			}
			for (x = 1; x <= 4; x++)
			{
				clsRateInfo.Set_DueDate(x, RInfo.Get_DueDate(x));
				clsRateInfo.Set_InterestStartDate(x, RInfo.Get_InterestStartDate(x));
				clsRateInfo.Set_BillWeight(x, RInfo.Get_BillWeight(x));
			}
			// x
			clsRateInfo.InterestRate = RInfo.InterestRate;
			clsRateInfo.RateType = RInfo.RateType;
			clsRateInfo.TaxRate = RInfo.TaxRate;
			clsRateInfo.TaxYear = RInfo.TaxYear;
			clsRateInfo.RateKey = RInfo.RateKey;
			boolDoPPAfter = boolDoBoth;
			if (intWhereFrom == 3)
			{
				frmRERange.InstancePtr.Show(FormShowEnum.Modal);
				boolDisplayFirst = true;
			}
			else
			{
				frmREAuditRange.InstancePtr.Show(FormShowEnum.Modal);
			}
			if (!modGlobalVariables.Statics.CancelledIt)
			{
				if (boolDisplayFirst)
				{
					this.Show(App.MainForm);
				}
                //FC:FINAL:AM:#3179 - moved code from load
                else
                {
                    SetupAuditGrid();
                    SetupTotalGrid();
                }
				FillAuditGrid();
			}
			else
			{
				Close();
				return;
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// rptREAudit.Show , MDIParent
			frmReportViewer.InstancePtr.Init(rptREAudit.InstancePtr, boolAllowEmail: true, strAttachmentName: "REAudit");
		}

		public void mnuPrint_Click()
		{
			mnuPrint_Click(mnuPrint, new System.EventArgs());
		}

		private void mnuPrintSingleLine_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptREAuditOneLine.InstancePtr, boolAllowEmail: true, strAttachmentName: "REAudit");
		}

		private void mnuView_Click(object sender, System.EventArgs e)
		{
			if (framSummary.Visible == true)
			{
				framSummary.Visible = false;
				mnuView.Text = "View Summary";
				cmdView.Text = "View Summary";
			}
			else
			{
				framSummary.Visible = true;
				mnuView.Text = "View Detail";
				cmdView.Text = "View Detail";
			}
		}

		private void ReTotalValues()
		{
			int x;
			int lngLandTotal;
			int lngBldgTotal;
			int lngExemptTotal;
			int lngAssessTotal;
			double dblTaxTotal;
			int lngTempLand = 0;
			int lngTempBuilding = 0;
			int lngTempExempt = 0;
			int lngHomesteadLandTotal;
			int lngHomesteadBldgTotal;
			int lngHomesteadExemptTotal;
			int lngCount;
			int lngHomesteadCount;
			lngCount = 0;
			lngHomesteadCount = 0;
			lngLandTotal = 0;
			lngBldgTotal = 0;
			lngExemptTotal = 0;
			lngAssessTotal = 0;
			lngHomesteadLandTotal = 0;
			lngHomesteadBldgTotal = 0;
			lngHomesteadExemptTotal = 0;
			dblTaxTotal = 0;
			for (x = 1; x <= AuditGrid.Rows - 1; x++)
			{
				lngTempLand = FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colLand)));
				lngTempBuilding = FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colBldg)));
				lngTempExempt = FCConvert.ToInt32(FCConvert.ToDouble(AuditGrid.TextMatrix(x, colExempt)));
				if (lngTempBuilding - lngTempExempt + lngTempLand > 0)
				{
					lngLandTotal += lngTempLand;
					lngBldgTotal += lngTempBuilding;
					lngExemptTotal += lngTempExempt;
					lngCount += 1;
				}
				else
				{
					lngHomesteadLandTotal += lngTempLand;
					lngHomesteadBldgTotal += lngTempBuilding;
					lngHomesteadExemptTotal += lngTempExempt;
					lngHomesteadCount += 1;
				}
			}
			// x
			lngAssessTotal = lngBldgTotal - lngExemptTotal + lngLandTotal;
			dblTaxTotal = modMain.Round(clsRateInfo.TaxRate * lngAssessTotal, 2);
			// now save these figures in the grid
			TotalGrid.TextMatrix(1, 2, Strings.Format(lngLandTotal, "###,###,###,##0"));
			TotalGrid.TextMatrix(1, 3, Strings.Format(lngBldgTotal, "###,###,###,##0"));
			TotalGrid.TextMatrix(1, 4, Strings.Format(lngExemptTotal, "###,###,###,##0"));
			TotalGrid.TextMatrix(1, 5, Strings.Format(lngAssessTotal, "###,###,###,##0"));
			TotalGrid.TextMatrix(1, 6, Strings.Format(dblTaxTotal, "###,###,##0.00"));
			TotalGrid.TextMatrix(1, 1, FCConvert.ToString(lngCount));
			TotalGrid.TextMatrix(2, 1, FCConvert.ToString(lngHomesteadCount));
			TotalGrid.TextMatrix(2, 2, Strings.Format(lngHomesteadLandTotal, "###,###,###,##0"));
			TotalGrid.TextMatrix(2, 3, Strings.Format(lngHomesteadBldgTotal, "###,###,###,##0"));
			TotalGrid.TextMatrix(2, 4, Strings.Format(lngHomesteadExemptTotal, "###,###,###,##0"));
			TotalGrid.TextMatrix(2, 5, "0");
			TotalGrid.TextMatrix(2, 6, "0.00");
			GridSummary.TextMatrix(1, 2, Strings.Format(lngLandTotal, "###,###,###,##0"));
			GridSummary.TextMatrix(1, 3, Strings.Format(lngBldgTotal, "###,###,###,##0"));
			GridSummary.TextMatrix(1, 4, Strings.Format(lngExemptTotal, "###,###,###,##0"));
			GridSummary.TextMatrix(1, 5, Strings.Format(lngAssessTotal, "###,###,###,##0"));
			GridSummary.TextMatrix(1, 6, Strings.Format(dblTaxTotal, "###,###,##0.00"));
			GridSummary.TextMatrix(2, 2, Strings.Format(lngHomesteadLandTotal, "###,###,###,##0"));
			GridSummary.TextMatrix(2, 3, Strings.Format(lngHomesteadBldgTotal, "###,###,###,##0"));
			GridSummary.TextMatrix(2, 4, Strings.Format(lngHomesteadExemptTotal, "###,###,###,##0"));
			GridSummary.TextMatrix(1, 1, FCConvert.ToString(lngCount));
			GridSummary.TextMatrix(2, 1, FCConvert.ToString(lngHomesteadCount));
		}
	}
}
