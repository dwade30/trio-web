﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using iTextSharp.text;
using SharedApplication.Extensions;
using DataGridViewCellCancelEventArgs = Wisej.Web.DataGridViewCellCancelEventArgs;
using DataGridViewCellEventArgs = Wisej.Web.DataGridViewCellEventArgs;
using DataGridViewCellMouseEventArgs = Wisej.Web.DataGridViewCellMouseEventArgs;
using DataGridViewCellValidatingEventArgs = Wisej.Web.DataGridViewCellValidatingEventArgs;
using DataGridViewColumn = Wisej.Web.DataGridViewColumn;
using DataGridViewColumnEventArgs = Wisej.Web.DataGridViewColumnEventArgs;
using DataGridViewColumnStateChangedEventArgs = Wisej.Web.DataGridViewColumnStateChangedEventArgs;
using DataGridViewElementStates = Wisej.Web.DataGridViewElementStates;
using DataGridViewRowEventArgs = Wisej.Web.DataGridViewRowEventArgs;
using DialogResult = Wisej.Web.DialogResult;
using KeyEventArgs = Wisej.Web.KeyEventArgs;
using Keys = Wisej.Web.Keys;
using MessageBox = Wisej.Web.MessageBox;
using MessageBoxButtons = Wisej.Web.MessageBoxButtons;
using MessageBoxIcon = Wisej.Web.MessageBoxIcon;
using PictureBoxSizeMode = Wisej.Web.PictureBoxSizeMode;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	public partial class frmCustomReport : BaseForm
	{
		public frmCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomReport InstancePtr
		{
			get
			{
				return (frmCustomReport)Sys.GetInstance(typeof(frmCustomReport));
			}
		}

		protected frmCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ****************************************************************************
		/// </summary>
		/// <summary>
		/// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		/// </summary>
		/// <summary>
		/// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		/// </summary>
		/// <summary>
		/// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		/// </summary>
		/// <summary>
		/// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomReport.rpt
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		/// </summary>
		/// <summary>
		/// SetFormFieldCaptions IN modCustomReport.mod
		/// </summary>
		/// <summary>
		/// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// A CALL TO THIS FORM WOULD BE:
		/// </summary>
		/// <summary>
		/// frmCustomReport.Show , MDIParent
		/// </summary>
		/// <summary>
		/// Call SetFormFieldCaptions(frmCustomReport, "Births")
		/// </summary>
		/// <summary>
		/// ****************************************************************************
		/// </summary>
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strReportName = "";
		string strTemp = "";
		bool boolPrintPreview;
		bool boolSaveReport;
		bool boolDelete;
		bool boolEditedReport;
		int lngCurrentID;
		// vbPorter upgrade warning: dblSizeRatio As double	OnWrite(short, float)
		double dblSizeRatio;
		bool boolPresetReport;
		const int CNSTCOLWHEREDESC = 0;
		const int CNSTCOLWHERESTART = 1;
		const int CNSTCOLWHEREEND = 2;
		const int CNSTCOLWHEREDATATYPE = 3;
		const int CNSTCOLWHERESPECIALCASE = 4;
		const int CNSTCOLWHEREFIELDNAME = 5;
		const int CNSTCOLWHERELIST = 6;
		const int CNSTROWWHEREACCOUNT = 0;
		const int CNSTROWWHERENAME = 1;
		const int CNSTROWWHERENAME2 = 2;
		const int CNSTROWWHEREADDRESS1 = 3;
		const int CNSTROWWHEREADDRESS2 = 4;
		const int CNSTROWWHEREADDRESS3 = 5;
		const int CNSTROWWHERELOCATION = 6;
		const int CNSTROWWHEREMAPLOT = 7;
		const int CNSTROWWHEREBOOKPAGE = 8;
		const int CNSTROWWHEREBILLTYPE = 9;
		const int CNSTROWWHEREBILLYEAR = 10;
		const int CNSTROWWHEREORIGINALTAX = 11;
		const int CNSTROWWHEREPAIDTODATE = 12;
		const int CNSTROWWHERECURRENTDUE = 13;
		const int CNSTROWWHEREOUTSTANDING = 32;
		const int CNSTROWWHEREOUTSTANDINGPRIN = 33;
		const int CNSTROWWHERELANDVALUE = 14;
		const int CNSTROWWHEREBLDGVALUE = 15;
		const int CNSTROWWHEREEXEMPTVALUE = 16;
		const int CNSTROWWHEREBILLABLE = 17;
		const int CNSTROWWHERETRANCODE = 18;
		const int CNSTROWWHERELANDCODE = 19;
		const int CNSTROWWHEREBLDGCODE = 20;
		const int CNSTROWWHEREACRES = 21;
		const int CNSTROWWHEREPPBILLABLE = 22;
		const int CNSTROWWHERECAT1 = 23;
		const int CNSTROWWHERECAT2 = 24;
		const int CNSTROWWHERECAT3 = 25;
		const int CNSTROWWHERECAT4 = 26;
		const int CNSTROWWHERECAT5 = 27;
		const int CNSTROWWHERECAT6 = 28;
		const int CNSTROWWHERECAT7 = 29;
		const int CNSTROWWHERECAT8 = 30;
		const int CNSTROWWHERECAT9 = 31;
		const int CNSTGRIDREPORTNAME = 0;
		const int CNSTGRIDREPORTAUTOID = 1;
		string[] strCategory = new string[9 + 1];

		class FieldListItem
		{
			public string Text { get; set; }
			public int Code { get; set; }
			public int Type { get; set; }
			public bool SpecialCase { get; set; }
			public string DBFieldName { get; set; }
			public bool Sortable { get; set; }
			public bool Totalable { get; set; }
		}

		private static List<FieldListItem> listItems = new List<FieldListItem>()
		{
			new FieldListItem {Text="ACCOUNT", Code=CNSTROWWHEREACCOUNT, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Account", Sortable=true, Totalable=false},
			new FieldListItem {Text="OWNER", Code=CNSTCOLWHEREFIELDNAME, Type=modCustomReport.GRIDTEXTRANGE, SpecialCase=false, DBFieldName="Name1", Sortable=true, Totalable=false},
			new FieldListItem {Text="SECOND OWNER", Code=CNSTROWWHERENAME2, Type=modCustomReport.GRIDTEXTRANGE, SpecialCase=false, DBFieldName="Name2", Sortable=true, Totalable=false},
			new FieldListItem {Text="ADDRESS 1", Code=CNSTROWWHEREADDRESS1, Type=modCustomReport.GRIDTEXT, SpecialCase=false, DBFieldName="Address1", Sortable=false, Totalable=false},
			new FieldListItem {Text="ADDRESS 2", Code=CNSTROWWHEREADDRESS2, Type=modCustomReport.GRIDTEXT, SpecialCase=false, DBFieldName="Address2", Sortable=false, Totalable=false},
			new FieldListItem {Text="ADDRESS 3", Code=CNSTROWWHEREADDRESS3, Type=modCustomReport.GRIDTEXT, SpecialCase=false, DBFieldName="Address3", Sortable=false, Totalable=false},
			new FieldListItem {Text="LOCATION", Code=CNSTROWWHERELOCATION, Type=modCustomReport.GRIDTEXT, SpecialCase=false, DBFieldName="StreetName", Sortable=false, Totalable=false},
			new FieldListItem {Text="MAP LOT", Code=CNSTROWWHEREMAPLOT, Type=modCustomReport.GRIDTEXTRANGE, SpecialCase=false, DBFieldName="MapLot", Sortable=true, Totalable=false},
			new FieldListItem {Text="BOOK & PAGE", Code=CNSTROWWHEREBOOKPAGE, Type=modCustomReport.GRIDTEXT, SpecialCase=false, DBFieldName="BookPage", Sortable=false, Totalable=false},
			new FieldListItem {Text="BILLING TYPE", Code=CNSTROWWHEREBILLTYPE, Type=modCustomReport.GRIDCOMBOIDTEXT, SpecialCase=false, DBFieldName="BillingType", Sortable=true, Totalable=false},
			new FieldListItem {Text="BILLING YEAR", Code=CNSTROWWHEREBILLYEAR, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=true, DBFieldName="BillingYear", Sortable=true, Totalable=false},
			new FieldListItem {Text="ORIGINAL TAX DUE", Code=CNSTROWWHEREORIGINALTAX, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=true, DBFieldName="TaxDue1", Sortable=true, Totalable=true},
			new FieldListItem {Text="PAID TO DATE", Code=CNSTROWWHEREPAIDTODATE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=true, DBFieldName="PrincipalPaid", Sortable=true, Totalable=true},
			new FieldListItem {Text="CURRENT DUE", Code=CNSTROWWHERECURRENTDUE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=true, DBFieldName="TaxDue1", Sortable=false, Totalable=false},
			new FieldListItem {Text="OUTSTANDING", Code=CNSTROWWHEREOUTSTANDING, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=true, DBFieldName="TaxDue1", Sortable=false, Totalable=false},
			new FieldListItem {Text="OUTSTANDING PRINCIPAL", Code=CNSTROWWHEREOUTSTANDINGPRIN, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=true, DBFieldName="TaxDue1", Sortable=false, Totalable=false},
			new FieldListItem {Text="LAND VALUE", Code=CNSTROWWHERELANDVALUE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="LandValue", Sortable=true, Totalable=true},
			new FieldListItem {Text="BUILDING VALUE", Code=CNSTROWWHEREBLDGVALUE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="BuildingValue", Sortable=true, Totalable=true},
			new FieldListItem {Text="EXEMPT VALUE", Code=CNSTROWWHEREEXEMPTVALUE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=true, DBFieldName="ExemptValue", Sortable=true, Totalable=true},
			new FieldListItem {Text="RE BILLABLE VALUE", Code=CNSTROWWHEREBILLABLE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=true, DBFieldName="BuildingValue", Sortable=true, Totalable=true},
			new FieldListItem {Text="TRAN CODE", Code=CNSTROWWHERETRANCODE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="TranCode", Sortable=true, Totalable=false},
			new FieldListItem {Text="LAND CODE", Code=CNSTROWWHERELANDCODE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="LandCode", Sortable=true, Totalable=false},
			new FieldListItem {Text="BUILDING CODE", Code=CNSTROWWHEREBLDGCODE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="BuildingCode", Sortable=true, Totalable=false},
			new FieldListItem {Text="ACRES", Code=CNSTROWWHEREACRES, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Acres", Sortable=true, Totalable=true},
			new FieldListItem {Text="PP BILLABLE VALUE", Code=CNSTROWWHEREPPBILLABLE, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="PPAssessment", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 1", Code=CNSTROWWHERECAT1, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category1", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 2", Code=CNSTROWWHERECAT2, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category2", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 3", Code=CNSTROWWHERECAT3, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category3", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 4", Code=CNSTROWWHERECAT4, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category4", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 5", Code=CNSTROWWHERECAT5, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category5", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 6", Code=CNSTROWWHERECAT6, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category6", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 7", Code=CNSTROWWHERECAT7, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category7", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 8", Code=CNSTROWWHERECAT8, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category8", Sortable=true, Totalable=true},
			new FieldListItem {Text="CATEGORY 9", Code=CNSTROWWHERECAT9, Type=modCustomReport.GRIDNUMRANGE, SpecialCase=false, DBFieldName="Category9", Sortable=true, Totalable=true}
	};

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			txtAccountStart.Text = String.Empty;
			txtAccountEnd.Text = String.Empty;
			txtNameStart.Text = String.Empty;
			txtNameEnd.Text = String.Empty;
			txtSecondNameStart.Text = String.Empty;
			txtSecondNameEnd.Text = String.Empty;
			txtMapLotStart.Text = String.Empty;
			txtMapLotEnd.Text = String.Empty;
			cmbBillingType.SelectedIndex = -1;
			cmbBillingYearStart.SelectedIndex = -1;
			cmbBillingYearEnd.SelectedIndex = -1;
			txtOriginalTaxStart.Text = String.Empty;
			txtOriginalTaxEnd.Text = String.Empty;
			txtPaidToDateStart.Text = String.Empty;
			txtPaidToDateEnd.Text = String.Empty;
			txtLandValueStart.Text = String.Empty;
			txtLandValueEnd.Text = String.Empty;
			txtBuildingValueStart.Text = String.Empty;
			txtBuildingValueEnd.Text = String.Empty;
			txtExemptValueStart.Text = String.Empty;
			txtExemptValueEnd.Text = String.Empty;
			txtREBillableValueStart.Text = String.Empty;
			txtREBillableValueEnd.Text = String.Empty;
			txtTranCodeStart.Text = String.Empty;
			txtTranCodeEnd.Text = String.Empty;
			txtLandCodeStart.Text = String.Empty;
			txtLandCodeEnd.Text = String.Empty;
			txtBuildingCodeStart.Text = String.Empty;
			txtBuildingCodeEnd.Text = String.Empty;
			txtAcresStart.Text = String.Empty;
			txtAcresEnd.Text = String.Empty;
			txtPPBillableValueStart.Text = String.Empty;
			txtPPBillableValueEnd.Text = String.Empty;
			txtCategory1Start.Text = String.Empty;
			txtCategory1End.Text = String.Empty;
			txtCategory2Start.Text = String.Empty;
			txtCategory2End.Text = String.Empty;
			txtCategory3Start.Text = String.Empty;
			txtCategory3End.Text = String.Empty;
			txtCategory4Start.Text = String.Empty;
			txtCategory4End.Text = String.Empty;
			txtCategory5Start.Text = String.Empty;
			txtCategory5End.Text = String.Empty;
			txtCategory6Start.Text = String.Empty;
			txtCategory6End.Text = String.Empty;
			txtCategory7Start.Text = String.Empty;
			txtCategory7End.Text = String.Empty;
			txtCategory8Start.Text = String.Empty;
			txtCategory8End.Text = String.Empty;
			txtCategory9Start.Text = String.Empty;
			txtCategory9End.Text = String.Empty;
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void frmCustomReport_Activated(object sender, System.EventArgs e)
		{
			ResizeVSWhere();
		}

		private void frmCustomReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F2:
					{
						lstFields.Focus();
						mnuAddRow_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F3:
					{
						lstFields.Focus();
						AddColumn();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F4:
					{
						lstFields.Focus();
						mnuDeleteRow_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F5:
					{
						lstFields.Focus();
						mnuDeleteColumn_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Escape:
					{
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmCustomReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomReport properties;
			//frmCustomReport.ScaleWidth	= 9045;
			//frmCustomReport.ScaleHeight	= 7320;
			//frmCustomReport.LinkTopic	= "Form1";
			//frmCustomReport.LockControls	= true;
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			boolEditedReport = false;
			dblSizeRatio = 1;
			// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
			// OF THE MDI PARENT FORM
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// SET THE PROPERTIES FOR THE REPORT LAYOUT SECTION
			vsLayout.Rows = 2;
			vsLayout.Cols = 1;
			vsLayout.ColData(0, vsLayout.ColWidth(0));
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, 1, 0, -1);
			// initialize to have nothing
			Image1.SizeMode = PictureBoxSizeMode.Normal;
			//Image1.Width = 1440 * 14;
			//Image1.Width = vsLayout.Width;
			//Image1.Image = ImageList1.Images[0];
			//HScroll1.Minimum = 0;
			//if (Image1.Width > Frame1.Width)
			//{
			//    HScroll1.Maximum = Image1.Width - Frame1.Width;
			//}
			//else
			//{
			//    HScroll1.Enabled = false;
			//}
			//vsLayout.Width = Image1.Width - 720;
			vsLayout.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(1, true);
            vsLayout.Left = Image1.Left;
            // Line1.X1 = Image1.Left + (1440 * 8) - 900
            //Line1.X1 = FCConvert.ToSingle(1440 * 7.5);
            //Line1.X2 = Line1.X1;
            //Line1.Y2 = vsLayout.TopOriginal + vsLayout.HeightOriginal;
            // fraLayout.Enabled = strPreSetReport = vbNullString
			// fraFields.Enabled = strPreSetReport = vbNullString
			if (!boolPresetReport)
			{
				fraSort.Enabled = true;
				fraFields.Enabled = true;
				fraReports.Enabled = true;
				fraMessage.Visible = false;
				vsLayout.Visible = true;
			}
			else
			{
				fraSort.Enabled = false;
				fraFields.Enabled = false;
				fraReports.Visible = false;
				fraMessage.Visible = true;
				vsLayout.Visible = false;
			}
			LoadCategories();
			SetupWhereGrid();
			SetupLstFields();
			SetupLstSort();
			SetupLstTotal();
			SetupGridReport();
		}

		private void frmCustomReport_Resize(object sender, System.EventArgs e)
		{
			//dblSizeRatio = Line2.X2 / 1440;
			//Line1.X1 = FCConvert.ToSingle(1440 * 7.5 * dblSizeRatio);
			ResizeVSWhere();
			ResizeVSLayout();
		}

		private void ResizeVSLayout()
		{
			// have to resize it to stay relative to the ruler
			int x;
			for (x = 0; x <= vsLayout.Cols - 1; x++)
			{
				vsLayout.ColWidth(x, FCConvert.ToInt32(FCConvert.ToDouble(vsLayout.ColData(x)) * dblSizeRatio));
			}
			// x
			// Line1.X1 = Int(Image1.Left + (1440 * 8 * dblSizeRatio) - (900 * dblSizeRatio))
			//Line1.X1 = FCConvert.ToSingle(1440 * 7.5 * dblSizeRatio);
			//Line1.X2 = Line1.X1;
		}

		private void ResizeVSWhere()
		{
//			int GridWidth = 0;
//			GridWidth = vsWhere.WidthOriginal;
//			vsWhere.ColWidth(0, FCConvert.ToInt32(0.40 * GridWidth));
//			vsWhere.ColWidth(1, FCConvert.ToInt32(0.28 * GridWidth));
		}

		private void GridReport_ClickEvent(object sender, System.EventArgs e)
		{
			if (GridReport.Row < 1)
				return;
			if (GridReport.Row == 1)
			{
				// new report so clear everything
				return;
			}
			LoadData(FCConvert.ToInt32(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID)));
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//Image1.Left = FCConvert.ToInt32(-HScroll1.Value + (30 * dblSizeRatio));
			// vsLayout.Left = (720 * dblSizeRatio) + (-HScroll1.Value + (30 * dblSizeRatio))
			vsLayout.Left = Image1.Left;
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			//Image1.Left = FCConvert.ToInt32(-HScroll1.Value + (30 * dblSizeRatio));
			// vsLayout.Left = (720 * dblSizeRatio) + (-HScroll1.Value + (30 * dblSizeRatio))
			vsLayout.Left = Image1.Left;
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			int intCount = 0;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					intCount += 1;
				}
				lstSort.SetSelected(intCounter, true);
			}
			if (intCount == lstSort.Items.Count)
			{
				for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
				{
					lstSort.SetSelected(intCounter, false);
				}
			}
			if (lstSort.Items.Count > 0)
				lstSort.SelectedIndex = 0;
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			if (vsLayout.Row < 1)
				vsLayout.Row = 1;
			vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col, lstFields.Items[lstFields.SelectedIndex].Text);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, lstFields.ItemData(lstFields.SelectedIndex));
			boolEditedReport = true;
		}

        //private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //    MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //    int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //    float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //    float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //    // THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
        //    // CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
        //    // WHERE TO SWAP THE TWO ITEMS.
        //    //
        //    // THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //    // WILL BE DISPLAYED ON THE REPORT ITSELF
        //    intStart = lstSort.SelectedIndex;
        //    boolEditedReport = true;
        //}
        //private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //    MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //    int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //    float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //    float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //    // THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
        //    // ITEMS THAT ARE TO BE SWAPED
        //    //
        //    // THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //    // WILL BE DISPLAYED ON THE REPORT ITSELF
        //    bool blnTempSelected = false;
        //    bool blnTemp2Selected = false;
        //    int intTemp = 0;
        //    // IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
        //    if (intStart != lstSort.SelectedIndex)
        //    {
        //        // SAVE THE CAPTION AND ID FOR THE NEW ITEM
        //        intTemp = lstSort.SelectedIndex;
        //        blnTempSelected = lstSort.Selected(intTemp);
        //        blnTemp2Selected = lstSort.Selected(intStart);
        //        strTemp = lstSort.Items[intTemp].Text;
        //        intID = lstSort.ItemData(intTemp);
        //        // CHANGE THE NEW ITEM
        //        lstSort.Items[intTemp].Text = lstSort.Items[intStart].Text;
        //        lstSort.ItemData(intTemp, lstSort.ItemData(intStart));
        //        // SAVE THE OLD ITEM
        //        lstSort.Items[intStart].Text = strTemp;
        //        lstSort.ItemData(intStart, intID);
        //        // SET BOTH ITEMS TO BE SELECTED
        //        //Application.DoEvents();
        //        lstSort.SetSelected(intTemp, blnTemp2Selected);
        //        lstSort.SetSelected(intStart, blnTempSelected);
        //    }
        //}

        private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			boolEditedReport = true;
			AddColumn();
		}

		private void AddColumn()
		{
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, 1, vsLayout.Cols - 1, vsLayout.Rows - 1, vsLayout.Cols - 1, -1);
			vsLayout.ColData(vsLayout.Cols - 1, Conversion.Int(vsLayout.ColWidth(vsLayout.Cols - 1) / dblSizeRatio));
			//FC:FINAL:CHN - issue #1408: Missing focus after adding column.
			vsLayout.Col = vsLayout.Cols - 1;
			vsLayout.Focus();
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			vsLayout.Rows += 1;
			//FC:FINAL:RPU: #1432 - Comment out mergerow because it will do a wrong expand
			//vsLayout.MergeRow(vsLayout.Rows - 1, true);
			vsLayout.Select(vsLayout.Rows - 1, 0);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, 0, vsLayout.Row, vsLayout.Cols - 1, -1);
			boolEditedReport = true;
		}

		public void mnuAddRow_Click()
		{
			mnuAddRow_Click(cmdAddRow, new System.EventArgs());
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			boolEditedReport = true;
			txtAccountStart.Focus();
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.");
			}
			boolEditedReport = true;
		}

		public void mnuDeleteColumn_Click()
		{
			mnuDeleteColumn_Click(cmdDeleteColumn, new System.EventArgs());
		}

		private void mnuDeleteReport_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngAutoID As int	OnWrite(string)
			int lngAutoID;
			clsDRWrapper clsEx = new clsDRWrapper();
			if (GridReport.Row < 2)
				return;
			lngAutoID = FCConvert.ToInt32(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID));
			GridReport.RemoveItem(GridReport.Row);
			GridReport.Row = 1;
			// new report
			clsEx.Execute("Delete from customreports where ID = " + FCConvert.ToString(lngAutoID), "twbl0000.vb1", false);
			clsEx.Execute("delete from reportparameters where reportnumber = " + FCConvert.ToString(lngAutoID), "twbl0000.vb1", false);
			clsEx.Execute("Delete from customreportfields where reportnumber = " + FCConvert.ToString(lngAutoID), "twbl0000.vb1", false);
			lngCurrentID = -1;
			MessageBox.Show("Report Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Row < 1)
				return;
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.");
			}
			boolEditedReport = true;
		}

		public void mnuDeleteRow_Click()
		{
			mnuDeleteRow_Click(cmdDeleteRow, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			if (GridReport.Row < 1)
			{
				MessageBox.Show("You must save or load a report before printing", "No Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (boolEditedReport && lngCurrentID > 0)
			{
				if (MessageBox.Show("You have made unsaved changes to the report." + "\r\n" + "Do you wish to save now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					SaveData_2(FCConvert.ToInt32(Conversion.Val(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID))));
				}
			}
			if (Conversion.Val(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID)) < 1)
			{
				if (lngCurrentID < 1)
				{
					MessageBox.Show("You must save or load a report before printing", "No Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					rptBLCustomReport.InstancePtr.Unload();
					rptBLCustomReport.InstancePtr.Init(lngCurrentID, false);
					return;
				}
			}
			rptBLCustomReport.InstancePtr.Unload();
			rptBLCustomReport.InstancePtr.Init(FCConvert.ToInt32(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID)), false);
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			if (GridReport.Row < 1)
			{
				MessageBox.Show("You must save or load a report before printing", "No Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (boolEditedReport && lngCurrentID > 0)
			{
				if (MessageBox.Show("You have made unsaved changes to the report." + "\r\n" + "Do you wish to save now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					SaveData_2(FCConvert.ToInt32(Conversion.Val(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID))));
				}
			}
			if (Conversion.Val(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID)) < 1)
			{
				if (lngCurrentID < 1)
				{
					MessageBox.Show("You must save or load a report before printing", "No Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					rptBLCustomReport.InstancePtr.Unload();
					rptBLCustomReport.InstancePtr.Init(lngCurrentID);
					return;
				}
			}
			rptBLCustomReport.InstancePtr.Unload();
			rptBLCustomReport.InstancePtr.Init(FCConvert.ToInt32(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID)));
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (GridReport.Row > 0)
			{
				SaveData_2(FCConvert.ToInt32(Conversion.Val(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID))));
			}
			else
			{
				SaveData_2(-1);
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			bool boolOkToContinue;
			boolOkToContinue = false;
			if (GridReport.Row > 0)
			{
				if (SaveData_2(FCConvert.ToInt32(Conversion.Val(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID)))))
				{
					boolOkToContinue = true;
				}
			}
			else
			{
				if (SaveData_2(-1))
				{
					boolOkToContinue = true;
				}
			}
			if (boolOkToContinue)
			{
				rptBLCustomReport.InstancePtr.Unload();
				rptBLCustomReport.InstancePtr.Init(FCConvert.ToInt32(GridReport.TextMatrix(GridReport.Row, CNSTGRIDREPORTAUTOID)));
			}
		}

		private void mnuSaveNew_Click(object sender, System.EventArgs e)
		{
			SaveData_2(-1);
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}
		//FC:FINAL:BBE - replace FlexGrid event AfterUserResize with ColumnWidthChanged, RowHeightChanged
		private void vsLayout_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsLayout_AfterUserResize(grid.GetFlexRowIndex(e.RowIndex), -1);
		}

		private void vsLayout_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsLayout_AfterUserResize(-1, grid.GetFlexColIndex(e.Column.Index));
            ResizeGrid();
        }

		private void vsLayout_AfterUserResize(int row, int col)
		{
			//FC:FINAL:MSh - i.issue #1595: size of column will be saved in wrong column
			//vsLayout.ColData(vsLayout.Col, FCConvert.ToInt32(vsLayout.ColWidth(vsLayout.Col) / dblSizeRatio));
			vsLayout.ColData(col, FCConvert.ToInt32(vsLayout.ColWidth(col) / dblSizeRatio));
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void SetupWhereGrid()
		{
			clsDRWrapper rsYears = new clsDRWrapper();
			int lastYear = 0;
			
			lblCategory1.Text = strCategory[1];
			lblCategory2.Text = strCategory[2];
			lblCategory3.Text = strCategory[3];
			lblCategory4.Text = strCategory[4];
			lblCategory5.Text = strCategory[5];
			lblCategory6.Text = strCategory[6];
			lblCategory7.Text = strCategory[7];
			lblCategory8.Text = strCategory[8];
			lblCategory9.Text = strCategory[9];

			cmbBillingType.Items.Add("Real Estate");
			cmbBillingType.Items.Add("Personal Property");
			cmbBillingType.Capture = true;

			rsYears.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster ORDER BY BillingYear DESC", modGlobalVariables.strCLDatabase);
			while (!rsYears.EndOfFile())
			{
				if (lastYear != Convert.ToInt32(rsYears.Get_Fields_String("BillingYear").Left(4)))
				{
					lastYear = Convert.ToInt32(rsYears.Get_Fields_String("BillingYear").Left(4));
					cmbBillingYearStart.Items.Add(lastYear);
					cmbBillingYearEnd.Items.Add(lastYear);
				}
				rsYears.MoveNext();
			}
			/* XXXXXXXXXXXXX
			int CRow = 0;
			vsWhere.Cols = 7;
			vsWhere.Rows = 0;
			vsWhere.ColHidden(CNSTCOLWHEREDATATYPE, true);
			vsWhere.ColHidden(CNSTCOLWHEREFIELDNAME, true);
			vsWhere.ColHidden(CNSTCOLWHERESPECIALCASE, true);
			vsWhere.ColHidden(CNSTCOLWHERELIST, true);
			vsWhere.ColDataType(CNSTCOLWHERESPECIALCASE, FCGrid.DataTypeSettings.flexDTBoolean);
			vsWhere.Rows += 1;
			*/ // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		}

		private void LoadCategories()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from ratioTRENDS order by type", "twpp0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				if (clsLoad.Get_Fields("type") <= 9)
				{
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					strCategory[clsLoad.Get_Fields("type")] = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					if (strCategory[clsLoad.Get_Fields("type")] == string.Empty)
					{
						// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
						strCategory[clsLoad.Get_Fields("type")] = "Category " + clsLoad.Get_Fields("type");
					}
				}
				clsLoad.MoveNext();
			}
		}

		private void SetupLstFields()
		{
			// Copy all of the report fields
			foreach (FieldListItem item in listItems)
			{
				lstFields.AddItem(GetListItemText(item));
				lstFields.ItemData(lstFields.NewIndex, item.Code);
			}
		}

		private void SetupLstSort()
		{
			// Copy all of the Sortable report fields
			lstSort.Items.Clear();
			foreach (FieldListItem item in listItems.Where(x => x.Sortable))
			{
				lstSort.AddItem(GetListItemText(item));
				lstSort.ItemData(lstSort.NewIndex, item.Code);
			}
		}

		private void SetupLstTotal()
		{
			// Copy all of the Totalable report fields
			lstTotal.Items.Clear();
			foreach (FieldListItem item in listItems.Where(x => x.Totalable))
			{
				lstTotal.AddItem(GetListItemText(item));
				lstTotal.ItemData(lstTotal.NewIndex, item.Code);
			}
		}

		private string GetListItemText(FieldListItem item)
		{
			string itemText;
			switch (item.Code)
			{
				case CNSTROWWHERECAT1:
					itemText = strCategory[1];
					break;
				case CNSTROWWHERECAT2:
					itemText = strCategory[2];
					break;
				case CNSTROWWHERECAT3:
					itemText = strCategory[3];
					break;
				case CNSTROWWHERECAT4:
					itemText = strCategory[4];
					break;
				case CNSTROWWHERECAT5:
					itemText = strCategory[5];
					break;
				case CNSTROWWHERECAT6:
					itemText = strCategory[6];
					break;
				case CNSTROWWHERECAT7:
					itemText = strCategory[7];
					break;
				case CNSTROWWHERECAT8:
					itemText = strCategory[8];
					break;
				case CNSTROWWHERECAT9:
					itemText = strCategory[9];
					break;
				default:
					itemText = item.Text;
					break;
			}
			return itemText;
		}

			private void SetupGridReport()
		{
			// set it up then load it
			clsDRWrapper clsLoad = new clsDRWrapper();
			GridReport.Rows = 1;
			GridReport.ColHidden(CNSTGRIDREPORTAUTOID, true);
			GridReport.TextMatrix(0, CNSTGRIDREPORTNAME, "Report Name");
			GridReport.AddItem("New Report" + "\t" + FCConvert.ToString(-1));
			// now load the reports
			clsLoad.OpenRecordset("select * from customreports order by reportname", "twbl0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				GridReport.AddItem(clsLoad.Get_Fields_String("reportname") + "\t" + clsLoad.Get_Fields_Int32("ID"));
				clsLoad.MoveNext();
			}
		}
		// vbPorter upgrade warning: lngAutoID As int	OnWrite(double, short)
		private bool SaveData_2(int lngAutoID)
		{
			return SaveData(ref lngAutoID);
		}

		private bool SaveData(ref int lngAutoID)
		{
			bool SaveData = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngAutoIDToUse;
			int lngRow = 0;
			string strTemp = "";
			string strMin = "";
			string strMax = "";
			string strSQL = "";
			int x;
			int lngCode = 0;
			bool boolCreate = false;
			int lngCol;
			int lngTemp = 0;
			int lngRowToSet = 0;
			FieldListItem gItem;
			try
			{
				// On Error GoTo ErrorHandler
				SaveData = false;
				lngAutoIDToUse = lngAutoID;
				clsSave.OpenRecordset("select * from customreports where ID = " + FCConvert.ToString(lngAutoID), "twbl0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
					clsSave.Set_Fields("Title", Strings.Trim(txtTitle.Text));
					lngRowToSet = GridReport.Row;
				}
				else
				{
					// new
					// get reportname
					//FC:FINAL:BBE - used object and reassign it back
					object strInput = strTemp;
					if (!frmInput.InstancePtr.Init(ref strInput, "Report Name", "Enter the report name", 2880, false, modGlobalConstants.InputDTypes.idtString))
					{
						return SaveData;
					}
					strTemp = FCConvert.ToString(strInput);
					if (Strings.Trim(strTemp) == string.Empty)
					{
						MessageBox.Show("Cannot save without a report name", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
					clsSave.AddNew();
					GridReport.Rows += 1;
					lngRow = GridReport.Rows - 1;
					GridReport.TextMatrix(lngRow, CNSTGRIDREPORTNAME, Strings.Trim(strTemp));
					clsSave.Set_Fields("ReportName", Strings.Trim(strTemp));
					clsSave.Set_Fields("Title", Strings.Trim(txtTitle.Text));
					lngRowToSet = GridReport.Rows - 1;
				}
				// get the colwidths.  This has nothing to do with the visible widths on the report, only what the grid looks like on this screen
				strTemp = "";
				for (x = 0; x <= vsLayout.Cols - 1; x++)
				{
					strTemp += vsLayout.ColData(x) + ",";
				}
				// x
				if (strTemp != string.Empty)
				{
					// get rid of last comma
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				clsSave.Set_Fields("colwidths", strTemp);
				clsSave.Update();
				lngAutoIDToUse = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				GridReport.TextMatrix(lngRow, CNSTGRIDREPORTAUTOID, FCConvert.ToString(lngAutoIDToUse));
				// now save the parameters
				clsSave.Execute("delete from reportparameters where reportnumber = " + FCConvert.ToString(lngAutoIDToUse), "twbl0000.vb1", false);

				if (!String.IsNullOrWhiteSpace(txtAccountStart.Text) || !String.IsNullOrWhiteSpace(txtAccountEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREACCOUNT);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Account");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtAccountStart.Text.Trim());
					clsSave.Set_Fields("Max", txtAccountEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtNameStart.Text) || !String.IsNullOrWhiteSpace(txtNameEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERENAME);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Name1");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtNameStart.Text.Trim());
					clsSave.Set_Fields("Max", txtNameEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDTEXTRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtSecondNameStart.Text) || !String.IsNullOrWhiteSpace(txtSecondNameEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERENAME2 );
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Name2");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtSecondNameStart.Text.Trim());
					clsSave.Set_Fields("Max", txtSecondNameEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDTEXTRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtMapLotStart.Text) || !String.IsNullOrWhiteSpace(txtMapLotEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREMAPLOT);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "MapLot");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtMapLotStart.Text.Trim());
					clsSave.Set_Fields("Max", txtMapLotEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDTEXTRANGE);
					clsSave.Update();
				}

				if (cmbBillingType.SelectedIndex >= 0)
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREBILLTYPE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "BillingType");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", cmbBillingType.SelectedIndex == 0 ? "RE" : "PP");
					clsSave.Set_Fields("Max", "");
					clsSave.Set_Fields("type", modCustomReport.GRIDCOMBOIDTEXT);
					clsSave.Update();
				}

				if (cmbBillingYearStart.SelectedIndex >= 0 || cmbBillingYearEnd.SelectedIndex >= 0)
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREBILLYEAR);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "BillingYear");
					clsSave.Set_Fields("SpecialCase", true);
					clsSave.Set_Fields("Min", cmbBillingYearStart.SelectedItem.ToString());
					clsSave.Set_Fields("Max", cmbBillingYearEnd.SelectedItem.ToString());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtOriginalTaxStart.Text) || !String.IsNullOrWhiteSpace(txtOriginalTaxEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREORIGINALTAX);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "TaxDue1");
					clsSave.Set_Fields("SpecialCase", true);
					clsSave.Set_Fields("Min", txtOriginalTaxStart.Text.Trim());
					clsSave.Set_Fields("Max", txtOriginalTaxEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtPaidToDateStart.Text) || !String.IsNullOrWhiteSpace(txtPaidToDateEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREPAIDTODATE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "PrincipalPaid");
					clsSave.Set_Fields("SpecialCase", true);
					clsSave.Set_Fields("Min", txtPaidToDateStart.Text.Trim());
					clsSave.Set_Fields("Max", txtPaidToDateEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtLandValueStart.Text) || !String.IsNullOrWhiteSpace(txtLandValueEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERELANDVALUE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "LandValue");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtLandValueStart.Text.Trim());
					clsSave.Set_Fields("Max", txtLandValueEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}
				if (!String.IsNullOrWhiteSpace(txtBuildingValueStart.Text) || !String.IsNullOrWhiteSpace(txtBuildingValueEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREBLDGVALUE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "BuildingValue");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtBuildingValueStart.Text.Trim());
					clsSave.Set_Fields("Max", txtBuildingValueEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtExemptValueStart.Text) || !String.IsNullOrWhiteSpace(txtExemptValueEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREEXEMPTVALUE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "ExemptValue");
					clsSave.Set_Fields("SpecialCase", true);
					clsSave.Set_Fields("Min", txtExemptValueStart.Text.Trim());
					clsSave.Set_Fields("Max", txtExemptValueEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}


				if (!String.IsNullOrWhiteSpace(txtREBillableValueStart.Text) || !String.IsNullOrWhiteSpace(txtREBillableValueEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREBILLABLE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "BuildingValue");
					clsSave.Set_Fields("SpecialCase", true);
					clsSave.Set_Fields("Min", txtREBillableValueStart.Text.Trim());
					clsSave.Set_Fields("Max", txtREBillableValueEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtTranCodeStart.Text) || !String.IsNullOrWhiteSpace(txtTranCodeEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERETRANCODE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "TranCode");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtTranCodeStart.Text.Trim());
					clsSave.Set_Fields("Max", txtTranCodeEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtLandCodeStart.Text) || !String.IsNullOrWhiteSpace(txtLandCodeEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERELANDCODE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "LandCode");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtLandCodeStart.Text.Trim());
					clsSave.Set_Fields("Max", txtLandCodeEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtBuildingCodeStart.Text) || !String.IsNullOrWhiteSpace(txtBuildingCodeEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREBLDGCODE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "BuildingCode");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtBuildingCodeStart.Text.Trim());
					clsSave.Set_Fields("Max", txtBuildingCodeEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtAcresStart.Text) || !String.IsNullOrWhiteSpace(txtAcresEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREACRES);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Acres");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtAcresStart.Text.Trim());
					clsSave.Set_Fields("Max", txtAcresEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtPPBillableValueStart.Text) || !String.IsNullOrWhiteSpace(txtPPBillableValueEnd.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHEREPPBILLABLE);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "PPAssessment");
					clsSave.Set_Fields("SpecialCase", true);
					clsSave.Set_Fields("Min", txtPPBillableValueStart.Text.Trim());
					clsSave.Set_Fields("Max", txtPPBillableValueEnd.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory1Start.Text) || !String.IsNullOrWhiteSpace(txtCategory1End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT1);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category1");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory1Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory1End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory2Start.Text) || !String.IsNullOrWhiteSpace(txtCategory2End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT2);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category2");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory2Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory2End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory3Start.Text) || !String.IsNullOrWhiteSpace(txtCategory3End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT3);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category3");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory3Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory3End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory4Start.Text) || !String.IsNullOrWhiteSpace(txtCategory4End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT4);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category4");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory4Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory4End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory5Start.Text) || !String.IsNullOrWhiteSpace(txtCategory5End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT5);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category5");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory5Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory5End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory6Start.Text) || !String.IsNullOrWhiteSpace(txtCategory6End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT6);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category6");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory6Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory6End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory7Start.Text) || !String.IsNullOrWhiteSpace(txtCategory7End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT7);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category7");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory7Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory7End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory8Start.Text) || !String.IsNullOrWhiteSpace(txtCategory8End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT8);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category8");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory8Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory8End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}

				if (!String.IsNullOrWhiteSpace(txtCategory9Start.Text) || !String.IsNullOrWhiteSpace(txtCategory9End.Text))
				{
					clsSave.OpenRecordset("select * from reportparameters where reportnumber = -10", "twbl0000.vb1");
					clsSave.AddNew();
					clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
					clsSave.Set_Fields("Code", CNSTROWWHERECAT9);
					clsSave.Set_Fields("sortby", false);
					clsSave.Set_Fields("line", 0);
					clsSave.Set_Fields("fieldname", "Category9");
					clsSave.Set_Fields("SpecialCase", false);
					clsSave.Set_Fields("Min", txtCategory9Start.Text.Trim());
					clsSave.Set_Fields("Max", txtCategory9End.Text.Trim());
					clsSave.Set_Fields("type", modCustomReport.GRIDNUMRANGE);
					clsSave.Update();
				}
				// lngRow
				// now do the sort by stuff
				// go through list and use only those checked off
				// check to see if there is already a parameter record
				int gRow = 0;
				int y;
				clsSave.OpenRecordset("select * from reportparameters where reportnumber = " + FCConvert.ToString(lngAutoIDToUse) + " order by code", "twbl0000.vb1");
				x = 0;
				// x will track the current sort by line (that is used)
				if (lstSort.SelectedItems.Any())
				{
					for (lngRow = 0; lngRow <= lstSort.Items.Count - 1; lngRow++)
					{
						if (lstSort.Selected(lngRow))
						{
							x += 1;
							boolCreate = true;
							lngCode = lstSort.ItemData(lngRow);
							if (!(clsSave.EndOfFile() && clsSave.BeginningOfFile()))
							{
								if (clsSave.FindFirstRecord("Code", lngCode))
								{
									boolCreate = false;
								}
							}
							if (boolCreate)
							{
								gItem = listItems.FirstOrDefault(g => g.Code == lngCode);
								if (gItem != null)
								{
									clsSave.AddNew();
									clsSave.Set_Fields("sortby", true);
									clsSave.Set_Fields("Line", x);
									clsSave.Set_Fields("Code", lngCode);
									clsSave.Set_Fields("Min", "");
									clsSave.Set_Fields("Max", "");
									clsSave.Set_Fields("reportnumber", lngAutoIDToUse);
									clsSave.Set_Fields("fieldname", gItem.DBFieldName);
									clsSave.Set_Fields("Type", gItem.Type);
									clsSave.Set_Fields("SpecialCase", gItem.SpecialCase);
									clsSave.Update();
								}
							}
							else
							{
								clsSave.Edit();
								clsSave.Set_Fields("sortby", true);
								clsSave.Set_Fields("Line", x);
								clsSave.Update();
							}
						}
					}
					// lngRow
				}
				clsSave.Execute("Delete from CustomReportFields where reportnumber = " + FCConvert.ToString(lngAutoIDToUse), "TWBl0000.vb1", false);
				clsSave.OpenRecordset("select * from customreportfields where reportnumber = -10", "Twbl0000.vb1");
				int lngLastCode;
				lngLastCode = -1;
				for (lngRow = 1; lngRow <= vsLayout.Rows - 1; lngRow++)
				{
					for (lngCol = 0; lngCol <= vsLayout.Cols - 1; lngCol++)
					{
						if (lngCol == 0 || lngLastCode != FCConvert.ToInt32(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol)))
						{
							// must be careful of merged cells
							lngLastCode = FCConvert.ToInt32(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol));
							clsSave.AddNew();
							clsSave.Set_Fields("ReportNumber", lngAutoIDToUse);
							clsSave.Set_Fields("Code", lngLastCode);
							clsSave.Set_Fields("Row", lngRow);
							clsSave.Set_Fields("Col", lngCol + 1);
							// don't want it to be zero based
							clsSave.Set_Fields("Description", Strings.Trim(vsLayout.TextMatrix(lngRow, lngCol)));
							lngTemp = 0;
							// get width even if it is a merged cell
							for (x = lngCol; x <= vsLayout.Cols - 1; x++)
							{
								if (FCConvert.ToInt32(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, x)) == lngLastCode)
								{
									lngTemp += FCConvert.ToInt32(vsLayout.ColData(x));
								}
								else
								{
									break;
								}
							}
							// x
							clsSave.Set_Fields("Width", lngTemp);
							if (lngLastCode >= 0)
							{
								// make sure we only try to get this info for valid codes
								gItem = listItems.FirstOrDefault(g => g.Code == lngLastCode);
								if (gRow >= 0)
								{
									clsSave.Set_Fields("FieldName", gItem.DBFieldName);
									clsSave.Set_Fields("SpecialCase", gItem.SpecialCase);
									clsSave.Set_Fields("Type", gItem.Type);
								}
							}
							clsSave.Update();
						}
					}
					// lngCol
				}
				// lngRow
				for (lngRow = 0; lngRow <= lstTotal.Items.Count - 1; lngRow++)
				{
					if (lstTotal.Selected(lngRow))
					{
						clsSave.Execute("update customreportfields set totalfield = 1 where reportnumber = " + FCConvert.ToString(lngAutoIDToUse) + " and code = " + FCConvert.ToString(lstTotal.ItemData(lngRow)), "twbl0000.vb1");
					}
				}
				// lngRow
				GridReport.Row = lngRowToSet;
				SaveData = true;
				boolEditedReport = false;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
		// vbPorter upgrade warning: lngAutoID As int	OnWrite(string)
		private void LoadData(int lngAutoID)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x;
			string strTemp;
			string[] strAry = null;
			int lngRow = 0;
			int lngCol = 0;
			clsLoad.OpenRecordset("SELECT * from customreports where ID = " + FCConvert.ToString(lngAutoID), "twbl0000.vb1");
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("Could not find report", "Report Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			lngCurrentID = lngAutoID;
			// set up report info
			txtTitle.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Title"));
			strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("colwidths"));
			strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);

			// clear the where criteria
			cmdClear_Click();

			clsLoad.OpenRecordset("select * from reportparameters where reportnumber = " + FCConvert.ToString(lngAutoID), "twbl0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				switch (clsLoad.Get_Fields_Int32("code"))
				{
					case CNSTROWWHEREACCOUNT:
					{
						txtAccountStart.Text = clsLoad.Get_Fields_String("Min");
						txtAccountEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERENAME:
					{
						txtNameStart.Text = clsLoad.Get_Fields_String("Min");
						txtNameEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERENAME2:
					{
						txtSecondNameStart.Text = clsLoad.Get_Fields_String("Min");
						txtSecondNameEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREMAPLOT:
					{
						txtMapLotStart.Text = clsLoad.Get_Fields_String("Min");
						txtMapLotEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREBILLTYPE:
					{
						cmbBillingType.SelectedIndex = clsLoad.Get_Fields_String("Min") == "RE" ? 0 : 1;
						break;
					}
					case CNSTROWWHEREBILLYEAR:
					{
						int intYear = 0;
						intYear = clsLoad.Get_Fields_Int32("Min");
						cmbBillingYearStart.SelectedIndex = cmbBillingYearStart.Items.IndexOf(intYear);
						intYear = clsLoad.Get_Fields_Int32("Max");
						cmbBillingYearEnd.SelectedIndex = cmbBillingYearEnd.Items.IndexOf(intYear);
						break;
					}
					case CNSTROWWHEREORIGINALTAX:
					{
						txtOriginalTaxStart.Text = clsLoad.Get_Fields_String("Min");
						txtOriginalTaxEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREPAIDTODATE:
					{
						txtPaidToDateStart.Text = clsLoad.Get_Fields_String("Min");
						txtPaidToDateEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERELANDVALUE:
					{
						txtLandValueStart.Text = clsLoad.Get_Fields_String("Min");
						txtLandValueEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREBLDGVALUE:
					{
						txtBuildingValueStart.Text = clsLoad.Get_Fields_String("Min");
						txtBuildingValueEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREEXEMPTVALUE:
					{
						txtExemptValueStart.Text = clsLoad.Get_Fields_String("Min");
						txtExemptValueEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREBILLABLE:
					{
						txtREBillableValueStart.Text = clsLoad.Get_Fields_String("Min");
						txtREBillableValueEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERETRANCODE:
					{
						txtTranCodeStart.Text = clsLoad.Get_Fields_String("Min");
						txtTranCodeEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERELANDCODE:
					{
						txtLandCodeStart.Text = clsLoad.Get_Fields_String("Min");
						txtLandCodeEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREBLDGCODE:
					{
						txtBuildingCodeStart.Text = clsLoad.Get_Fields_String("Min");
						txtBuildingCodeEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREACRES:
					{
						txtAcresStart.Text = clsLoad.Get_Fields_String("Min");
						txtAcresEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHEREPPBILLABLE:
					{
						txtPPBillableValueStart.Text = clsLoad.Get_Fields_String("Min");
						txtPPBillableValueEnd.Text = clsLoad.Get_Fields_String("Max");
						break;
					}

					case CNSTROWWHERECAT1:
					{
						txtCategory1Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory1End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERECAT2:
					{
						txtCategory2Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory2End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERECAT3:
					{
						txtCategory3Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory3End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERECAT4:
					{
						txtCategory4Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory4End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERECAT5:
					{
						txtCategory5Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory5End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERECAT6:
					{
						txtCategory6Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory6End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERECAT7:
					{
						txtCategory7Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory7End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERECAT8:
					{
						txtCategory8Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory8End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
					case CNSTROWWHERECAT9:
					{
						txtCategory9Start.Text = clsLoad.Get_Fields_String("Min");
						txtCategory9End.Text = clsLoad.Get_Fields_String("Max");
						break;
					}
				}
				clsLoad.MoveNext();
			}
			clsLoad.OpenRecordset("select * from reportparameters where sortby = 1 AND REPORTNUMBER = " + FCConvert.ToString(lngAutoID) + " order by line desc", "twbl0000.vb1");
			// order descending so we can just add to the front of the list each time
			// reset the list and then rearrange as needed
			SetupLstSort();
			while (!clsLoad.EndOfFile())
			{
				strTemp = "";
				for (x = 0; x <= lstSort.Items.Count - 1; x++)
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					if (lstSort.ItemData(x) == FCConvert.ToInt32(clsLoad.Get_Fields("code")))
					{
						strTemp = lstSort.Items[x].Text;
						lstSort.Items.RemoveAt(x);
						lstSort.Items.Insert(0, strTemp);
						lstSort.SetSelected(0, true);
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						lstSort.ItemData(0, FCConvert.ToInt32(clsLoad.Get_Fields("code")));
						break;
					}
				}
				// x
				clsLoad.MoveNext();
			}
			clsLoad.OpenRecordset("select * from customreportfields where totalfield = 1 and reportnumber = " + FCConvert.ToString(lngAutoID), "twbl0000.vb1");
			SetupLstTotal();
			while (!clsLoad.EndOfFile())
			{
				for (x = 0; x <= lstTotal.Items.Count - 1; x++)
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					if (lstTotal.ItemData(x) == FCConvert.ToInt32(clsLoad.Get_Fields("code")))
					{
						lstTotal.SetSelected(x, true);
					}
				}
				// x
				clsLoad.MoveNext();
			}
			// set up vslayout
			int lngMaxRows;
			int lngMaxCols;
			lngMaxRows = 0;
			lngMaxCols = 0;
			clsLoad.OpenRecordset("select max(row) as maxrow,max(col) as maxcol from customreportfields where reportnumber = " + FCConvert.ToString(lngAutoID), "twbl0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [maxrow] not found!! (maybe it is an alias?)
				lngMaxRows = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("maxrow"))));
				// TODO Get_Fields: Field [maxcol] not found!! (maybe it is an alias?)
				lngMaxCols = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("maxcol"))));
			}
			vsLayout.Rows = 2;
			vsLayout.Cols = 1;
			// grid is mostly cleared
			vsLayout.Rows = lngMaxRows + 1;
			// take into account the fixed row
			vsLayout.Cols = lngMaxCols;
			if (Information.UBound(strAry, 1) + 1 > vsLayout.Cols)
			{
				vsLayout.Cols = Information.UBound(strAry, 1) + 1;
			}
			// size the grid cols
			for (x = 0; x <= Information.UBound(strAry, 1); x++)
			{
				vsLayout.ColWidth(x, FCConvert.ToInt32(FCConvert.ToDouble(strAry[x]) * dblSizeRatio));
				vsLayout.ColData(x, strAry[x]);
			}
			// x
			//FC:FINAL:RPU: #i1594 - Comment out merge row because it will do a wrong expend
			// make sure merging will work
			//for (x = 1; x <= vsLayout.Rows - 1; x++)
			//{
			//    vsLayout.MergeRow(x, true);
			//} // x
			lngMaxRows = vsLayout.Rows - 1;
			lngMaxCols = vsLayout.Cols - 1;
			// now fill in the grid
			clsLoad.OpenRecordset("select * from customreportfields where reportnumber = " + FCConvert.ToString(lngAutoID) + " order by row,col", "twbl0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				lngRow = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("row"));
				lngCol = clsLoad.Get_Fields_Int32("col") - 1;
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol, clsLoad.Get_Fields("code"));
				vsLayout.TextMatrix(lngRow, lngCol, FCConvert.ToString(clsLoad.Get_Fields_String("Description")));
				if (lngCol < lngMaxCols)
				{
					// could be merged
					clsLoad.MoveNext();
					if (!clsLoad.EndOfFile())
					{
						if (lngRow == FCConvert.ToInt32(clsLoad.Get_Fields_Int32("row")))
						{
							if (lngCol + 1 < clsLoad.Get_Fields_Int32("col") - 1)
							{
								// this is a merged cell
								for (x = lngCol + 1; x <= clsLoad.Get_Fields_Int32("col") - 2; x++)
								{
									vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, x, vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol));
									vsLayout.TextMatrix(lngRow, x, vsLayout.TextMatrix(lngRow, lngCol));
								}
								// x
							}
						}
						else
						{
							// this is merged till end of row
							for (x = lngCol + 1; x <= lngMaxCols; x++)
							{
								vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, x, vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol));
								vsLayout.TextMatrix(lngRow, x, vsLayout.TextMatrix(lngRow, lngCol));
							}
							// x
						}
					}
					else
					{
						if (lngCol < lngMaxCols)
						{
							// this is merged till end of row
							for (x = lngCol + 1; x <= lngMaxCols; x++)
							{
								vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, x, vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol));
								vsLayout.TextMatrix(lngRow, x, vsLayout.TextMatrix(lngRow, lngCol));
							}
							// x
						}
					}
				}
				else
				{
					clsLoad.MoveNext();
				}
			}
		}

        private void VsLayout_ColumnRemoved(object sender, DataGridViewColumnEventArgs e)
        {
            ResizeGrid();
        }

        private void VsLayout_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            ResizeGrid();
        }


        private void VsLayout_ColumnStateChanged(object sender, DataGridViewColumnStateChangedEventArgs e)
        {
            if (e.StateChanged == DataGridViewElementStates.Visible)
            {
                ResizeGrid();
            }
        }

        private void ResizeGrid()
        {
            int gridWidth = 0;
            //add grid columns width
            foreach (DataGridViewColumn column in vsLayout.Columns)
            {
                if (column.Visible)
                {
                    gridWidth += column.Width;
                }
            }
            //add grid border
            gridWidth += 2;
            gridWidth = gridWidth < 984 ? 984 : gridWidth;
            vsLayout.Width = gridWidth;
        }

		private void cmbBillingType_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Backspace || e.KeyCode == Keys.Delete)
			{
				cmbBillingType.SelectedIndex = -1;
			}
			else
			{
				e.Handled = false;
			}
		}

		private void cmbBillingYearStart_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Backspace || e.KeyCode == Keys.Delete)
			{
				cmbBillingYearStart.SelectedIndex = -1;
			}
			else
			{
				e.Handled = false;
			}
		}

		private void cmbBillingYearEnd_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Backspace || e.KeyCode == Keys.Delete)
			{
				cmbBillingYearEnd.SelectedIndex = -1;
			}
			else
			{
				e.Handled = false;
			}
		}

		private void ClientArea_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			cmbBillingType.DroppedDown = false;
			cmbBillingYearStart.DroppedDown = false;
			cmbBillingYearEnd.DroppedDown = false;
		}

		private void WhereCriteriaPanel_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			cmbBillingType.DroppedDown = false;
			cmbBillingYearStart.DroppedDown = false;
			cmbBillingYearEnd.DroppedDown = false;
		}
	}
}
