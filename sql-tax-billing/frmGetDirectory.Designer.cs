﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmGetDirectory.
	/// </summary>
	partial class frmGetDirectory : BaseForm
	{
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCDriveListBox Drive1;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCDirListBox Dir1;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetDirectory));
			this.cmdCancel = new fecherFoundation.FCButton();
			this.Drive1 = new fecherFoundation.FCDriveListBox();
			this.cmdDone = new fecherFoundation.FCButton();
			this.Dir1 = new fecherFoundation.FCDirListBox();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 407);
			this.BottomPanel.Size = new System.Drawing.Size(472, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.Drive1);
			this.ClientArea.Controls.Add(this.cmdDone);
			this.ClientArea.Controls.Add(this.Dir1);
			this.ClientArea.Controls.Add(this.Image1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(472, 347);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(472, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(227, 30);
			this.HeaderText.Text = "Trio Setup Program";
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(142, 293);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(84, 40);
			this.cmdCancel.TabIndex = 5;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// Drive1
			// 
			this.Drive1.Items.AddRange(new object[] {
				((object)(resources.GetObject("Drive1.Items"))),
				((object)(resources.GetObject("Drive1.Items1"))),
				((object)(resources.GetObject("Drive1.Items2"))),
				((object)(resources.GetObject("Drive1.Items3"))),
				((object)(resources.GetObject("Drive1.Items4"))),
				((object)(resources.GetObject("Drive1.Items5"))),
				((object)(resources.GetObject("Drive1.Items6"))),
				((object)(resources.GetObject("Drive1.Items7"))),
				((object)(resources.GetObject("Drive1.Items8"))),
				((object)(resources.GetObject("Drive1.Items9"))),
				((object)(resources.GetObject("Drive1.Items10")))
			});
			this.Drive1.Location = new System.Drawing.Point(30, 70);
			this.Drive1.Name = "Drive1";
			this.Drive1.Size = new System.Drawing.Size(275, 40);
			this.Drive1.TabIndex = 1;
			this.Drive1.SelectedIndexChanged += new System.EventHandler(this.Drive1_SelectedIndexChanged);
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "actionButton";
			this.cmdDone.ForeColor = System.Drawing.Color.White;
			this.cmdDone.Location = new System.Drawing.Point(30, 293);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(94, 40);
			this.cmdDone.TabIndex = 4;
			this.cmdDone.Text = "Continue";
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			// 
			// Dir1
			// 
			this.Dir1.Location = new System.Drawing.Point(30, 130);
			this.Dir1.Name = "Dir1";
			this.Dir1.Size = new System.Drawing.Size(275, 103);
			this.Dir1.TabIndex = 2;
			this.Dir1.Click += new System.EventHandler(this.Dir1_Click);
			// 
			// Image1
			// 
			this.Image1.AllowDrop = true;
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.DrawStyle = ((short)(0));
			this.Image1.DrawWidth = ((short)(1));
			this.Image1.FillStyle = ((short)(1));
			this.Image1.FontTransparent = true;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(327, 130);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
			this.Image1.Size = new System.Drawing.Size(59, 55);
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image1.TabIndex = 6;
			this.Image1.Visible = false;
			this.Image1.MouseDown += new Wisej.Web.MouseEventHandler(this.Image1_MouseDown);
			this.Image1.MouseUp += new Wisej.Web.MouseEventHandler(this.Image1_MouseUp);
			this.Image1.Click += new System.EventHandler(this.Image1_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 252);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(389, 19);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "THEN CLICK CONTINUE TO PROCEED WITH THE SETUP PROCESS";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(328, 17);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "PLEASE SELECT YOUR EXECUTABLE (TRIOVB) FOLDER";
            // 
            // frmGetDirectory
            // 
            this.AcceptButton = this.cmdDone;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(472, 515);
			this.FillColor = 0;
			this.Name = "frmGetDirectory";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Trio Setup Program";
			this.Load += new System.EventHandler(this.frmGetDirectory_Load);
			this.Activated += new System.EventHandler(this.frmGetDirectory_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetDirectory_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
