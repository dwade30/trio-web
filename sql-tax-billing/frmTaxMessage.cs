﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmTaxMessage.
	/// </summary>
	public partial class frmTaxMessage : BaseForm
	{
		public frmTaxMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblDistributionPercent = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblDistribution = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblDistAmount = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.txtMessage = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtReturnAddress = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.lblDistributionPercent.AddControlArrayElement(lblDistributionPercent_4, 4);
			this.lblDistributionPercent.AddControlArrayElement(lblDistributionPercent_3, 3);
			this.lblDistributionPercent.AddControlArrayElement(lblDistributionPercent_2, 2);
			this.lblDistributionPercent.AddControlArrayElement(lblDistributionPercent_1, 1);
			this.lblDistributionPercent.AddControlArrayElement(lblDistributionPercent_0, 0);
			this.lblDistribution.AddControlArrayElement(lblDistribution_4, 4);
			this.lblDistribution.AddControlArrayElement(lblDistribution_3, 3);
			this.lblDistribution.AddControlArrayElement(lblDistribution_2, 2);
			this.lblDistribution.AddControlArrayElement(lblDistribution_1, 1);
			this.lblDistribution.AddControlArrayElement(lblDistribution_0, 0);
			this.lblDistAmount.AddControlArrayElement(lblDistAmount_4, 4);
			this.lblDistAmount.AddControlArrayElement(lblDistAmount_3, 3);
			this.lblDistAmount.AddControlArrayElement(lblDistAmount_2, 2);
			this.lblDistAmount.AddControlArrayElement(lblDistAmount_1, 1);
			this.lblDistAmount.AddControlArrayElement(lblDistAmount_0, 0);
			this.txtMessage.AddControlArrayElement(txtMessage_12, 12);
			this.txtMessage.AddControlArrayElement(txtMessage_11, 11);
			this.txtMessage.AddControlArrayElement(txtMessage_10, 10);
			this.txtMessage.AddControlArrayElement(txtMessage_9, 9);
			this.txtMessage.AddControlArrayElement(txtMessage_8, 8);
			this.txtMessage.AddControlArrayElement(txtMessage_7, 7);
			this.txtMessage.AddControlArrayElement(txtMessage_6, 6);
			this.txtMessage.AddControlArrayElement(txtMessage_5, 5);
			this.txtMessage.AddControlArrayElement(txtMessage_4, 4);
			this.txtMessage.AddControlArrayElement(txtMessage_3, 3);
			this.txtMessage.AddControlArrayElement(txtMessage_2, 2);
			this.txtMessage.AddControlArrayElement(txtMessage_1, 1);
			this.txtMessage.AddControlArrayElement(txtMessage_0, 0);
			this.txtReturnAddress.AddControlArrayElement(txtReturnAddress_3, 3);
			this.txtReturnAddress.AddControlArrayElement(txtReturnAddress_2, 2);
			this.txtReturnAddress.AddControlArrayElement(txtReturnAddress_1, 1);
			this.txtReturnAddress.AddControlArrayElement(txtReturnAddress_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxMessage InstancePtr
		{
			get
			{
				return (frmTaxMessage)Sys.GetInstance(typeof(frmTaxMessage));
			}
		}

		protected frmTaxMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsBillFormat clsTaxBillFormat = new clsBillFormat();

		private void frmTaxMessage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuContinue_Click();
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmTaxMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaxMessage properties;
			//frmTaxMessage.ScaleWidth	= 12210;
			//frmTaxMessage.ScaleHeight	= 8970;
			//frmTaxMessage.LinkTopic	= "Form1";
			//frmTaxMessage.LockControls	= true;
			//End Unmaped Properties
			modGlobalVariables.Statics.CancelledIt = false;
			modGlobalFunctions.SetFixedSize(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				modGlobalVariables.Statics.CancelledIt = true;
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			SaveTaxMessage();
			Close();
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillReturnAddress()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			clsLoad.OpenRecordset("select * from returnaddress", "twbl0000.vb1");
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("The return address must be filled in before printing bills.", "Enter Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				frmReturnAddress.InstancePtr.Init();
				clsLoad.OpenRecordset("select * from returnaddress", "twbl0000.vb1");
			}
			txtReturnAddress[0].Text = Strings.Mid(clsLoad.Get_Fields_String("address1") + " ", 1, 31);
			txtReturnAddress[1].Text = Strings.Mid(clsLoad.Get_Fields_String("address2") + " ", 1, 31);
			txtReturnAddress[2].Text = Strings.Mid(clsLoad.Get_Fields_String("address3") + " ", 1, 31);
			// strTemp = Trim(Trim(clsLoad.Fields("city") & "  " & clsLoad.Fields("state")) & " " & clsLoad.Fields("zip"))
			// If clsLoad.Fields("zip4") <> vbNullString Then strTemp = strTemp & "-" & clsLoad.Fields("zip4")
			// txtReturnAddress(3).Text = Trim(Mid(strTemp & " ", 1, 31))
			txtReturnAddress[3].Text = Strings.Mid(clsLoad.Get_Fields_String("address4") + " ", 1, 31);
			if (Strings.Trim(txtReturnAddress[2].Text) == "")
			{
				txtReturnAddress[2].Text = txtReturnAddress[3].Text;
				txtReturnAddress[3].Text = "";
			}
			if (Strings.Trim(txtReturnAddress[1].Text) == "")
			{
				txtReturnAddress[1].Text = txtReturnAddress[2].Text;
				txtReturnAddress[2].Text = txtReturnAddress[3].Text;
				txtReturnAddress[3].Text = "";
			}
		}

		private void FillDistribution()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intWhichLine = 0;
			int x;
			clsLoad.OpenRecordset("select * from distributiontable", "twbl0000.vb1");
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("The distribution information must be filled out before printing bills.", "Enter Distribution", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				frmDistribution.InstancePtr.Init();
				clsLoad.OpenRecordset("select * from distributiontable", "twbl0000.vb1");
			}
			for (x = 0; x <= 4; x++)
			{
				lblDistribution[FCConvert.ToInt16(x)].Text = "";
				lblDistributionPercent[FCConvert.ToInt16(x)].Text = "";
				lblDistAmount[FCConvert.ToInt16(x)].Visible = false;
			}
			// x
			while (!clsLoad.EndOfFile())
			{
				intWhichLine = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("distributionnumber"))));
				if (intWhichLine > 5)
				{
					clsLoad.MoveLast();
				}
				else
				{
					lblDistribution[FCConvert.ToInt16(intWhichLine - 1)].Text = FCConvert.ToString(clsLoad.Get_Fields_String("distributionname"));
					lblDistributionPercent[FCConvert.ToInt16(intWhichLine - 1)].Text = Strings.Format(clsLoad.Get_Fields_Double("distributionpercent"), "00.00") + "%";
					lblDistAmount[FCConvert.ToInt16(intWhichLine - 1)].Visible = true;
				}
				clsLoad.MoveNext();
			}
		}

		public void Init(ref clsBillFormat clsFormat)
		{
			clsTaxBillFormat.CopyFormat(ref clsFormat);
			if (clsTaxBillFormat.ReturnAddress)
			{
				FillReturnAddress();
			}
			else
			{
				framReturnAddress.Visible = false;
			}
			if (clsTaxBillFormat.Breakdown)
			{
				if (!clsTaxBillFormat.BreakdownDollars)
				{
					framDistTotals.Visible = false;
				}
				FillDistribution();
			}
			else
			{
				framDistribution.Visible = false;
				framDistTotals.Visible = false;
			}
			SetupForm();
			LoadTaxMessage();
			this.Show(FormShowEnum.Modal);
		}

		private void LoadTaxMessage()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x;
			string strTemp = "";
			bool boolMessageNotEmpty;
			for (x = 0; x <= 12; x++)
			{
				txtMessage[FCConvert.ToInt16(x)].Text = "";
			}
			// x
			clsLoad.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
			boolMessageNotEmpty = false;
			while (!clsLoad.EndOfFile())
			{
				strTemp = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("message")), 1, clsTaxBillFormat.MessageCharacters);
				if (Strings.Trim(strTemp) != string.Empty)
					boolMessageNotEmpty = true;
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				txtMessage[FCConvert.ToInt16(clsLoad.Get_Fields("line"))].Text = strTemp;
				clsLoad.MoveNext();
			}
			if (!boolMessageNotEmpty)
			{
				// blank message so fill in
				modGlobalRoutines.SaveDefaultTaxMessage();
				clsLoad.OpenRecordset("select * from taxmessage order by line", "twbl0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					strTemp = Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_String("message")), 1, clsTaxBillFormat.MessageCharacters);
					// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
					txtMessage[FCConvert.ToInt16(clsLoad.Get_Fields("line"))].Text = strTemp;
					clsLoad.MoveNext();
				}
			}
		}

		private void SaveTaxMessage()
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			int x;
			clsSave.Execute("delete from taxmessage", "twbl0000.vb1");
			for (x = 0; x <= clsTaxBillFormat.NumMessageLines - 1; x++)
			{
				clsSave.Execute("insert into taxmessage (message,line) values ('" + txtMessage[FCConvert.ToInt16(x)].Text + "'," + FCConvert.ToString(x) + ")", "twbl0000.vb1");
			}
			// x
		}

		private void txtMessage_KeyDown(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode != Keys.F10 && KeyCode != Keys.Escape && KeyCode != Keys.Back && KeyCode != Keys.Delete)
			{
				if (KeyCode == Keys.Return)
				{
					KeyCode = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
				else if (txtMessage[Index].MaxLength <= txtMessage[Index].Text.Length)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void txtMessage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txtMessage.GetIndex((FCTextBox)sender);
			txtMessage_KeyDown(index, sender, e);
		}

		private void SetupForm()
		{
			// setup the form based on the bill format
			int intBillFormat;
			int x;
			string strTest = "";
			intBillFormat = clsTaxBillFormat.Billformat;
			switch (intBillFormat)
			{
				case 1:
				case 4:
					{
						clsTaxBillFormat.NumMessageLines = 8;
						if (clsTaxBillFormat.BreakdownDollars)
						{
							clsTaxBillFormat.MessageLeft = 3840;
							strTest = Strings.StrDup(48, "W");
							lblTest.Text = strTest;
							for (x = 0; x <= 12; x++)
							{
								txtMessage[FCConvert.ToInt16(x)].MaxLength = 48;
								//FC:FINAL:CHN - issue #1490: Incorrect margin parameters (missing converting).
								// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
								txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
							}
							// x
						}
						else if (clsTaxBillFormat.Breakdown == true)
						{
							//FC:FINAL:CHN - issue #1490: Redesign.
							clsTaxBillFormat.MessageLeft = 2400;
							strTest = Strings.StrDup(48, "W");
							lblTest.Text = strTest;
							for (x = 0; x <= 12; x++)
							{
								txtMessage[FCConvert.ToInt16(x)].MaxLength = 48;
								//FC:FINAL:CHN - issue #1490: Incorrect margin parameters (missing converting).
								// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
								txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
							}
							// x
						}
						else
						{
							clsTaxBillFormat.MessageLeft = 2400;
							clsTaxBillFormat.MessageCharacters = 52;
							strTest = Strings.StrDup(48, "W");
							lblTest.Text = strTest;
							for (x = 0; x <= 12; x++)
							{
								txtMessage[FCConvert.ToInt16(x)].MaxLength = 48;
								//FC:FINAL:CHN - issue #1490: Incorrect margin parameters (missing converting).
								// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
								txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
							}
							// x
						}
						//FC:FINAL:CHN - issue #1490: Redesign.
						// clsTaxBillFormat.MessageTop = 3030;
						clsTaxBillFormat.MessageTop = 1200;
						clsTaxBillFormat.DistributionLeft = 30;
						// clsTaxBillFormat.DistributionTop = 3930;
						clsTaxBillFormat.DistributionTop = 3500;
						break;
					}
				case 2:
				case 3:
					{
						clsTaxBillFormat.NumMessageLines = 9;
						//FC:FINAL:CHN - issue #1490: Redesign.
						// clsTaxBillFormat.MessageLeft = 50;
						// clsTaxBillFormat.DistributionLeft = 3840;
						clsTaxBillFormat.MessageLeft = 3840;
						clsTaxBillFormat.DistributionLeft = 50;
						// clsTaxBillFormat.DistributionTop = 3930;
						clsTaxBillFormat.DistributionTop = 3500;
						// .MessageTop = 1350
						clsTaxBillFormat.MessageTop = 1200;
						strTest = Strings.StrDup(44, "W");
						lblTest.Text = strTest;
						for (x = 0; x <= 12; x++)
						{
							txtMessage[FCConvert.ToInt16(x)].MaxLength = 36;
							//FC:FINAL:CHN - issue #1490: Incorrect margin parameters (missing converting).
							// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
							txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
						}
						// x
						//FC:FINAL:CHN - issue #1490: Incorrect margin parameters (missing converting).
						// framMailingAddress.Left = 50;
						// framMailingAddress.Top = 3930 + framDistribution.Height + 50;
						framMailingAddress.TopOriginal = 3930 + framDistribution.HeightOriginal + 50;
						break;
					}
				case 5:
				case 6:
					{
						clsTaxBillFormat.NumMessageLines = 13;
						clsTaxBillFormat.MessageTop = 3030;
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						// clsTaxBillFormat.DistributionTop = clsTaxBillFormat.MessageTop + framMessage.Height + 10;
						clsTaxBillFormat.DistributionTop = clsTaxBillFormat.MessageTop + framMessage.HeightOriginal + 10;
						strTest = Strings.StrDup(32, "W");
						lblTest.Text = strTest;
						for (x = 0; x <= 12; x++)
						{
							txtMessage[FCConvert.ToInt16(x)].MaxLength = 30;
							//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
							// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
							txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
						}
						// x
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						// framMailingAddress.Left = 50;
						// framMailingAddress.LeftOriginal = 50;
						// clsTaxBillFormat.MessageLeft = framMailingAddress.Left + framMailingAddress.Width + 10;
						clsTaxBillFormat.MessageLeft = 50 + framMailingAddress.WidthOriginal + 10;
						clsTaxBillFormat.DistributionLeft = clsTaxBillFormat.MessageLeft;
						// framMailingAddress.Top = clsTaxBillFormat.MessageTop + 1440;
						// framMailingAddress.TopOriginal = clsTaxBillFormat.MessageTop + 1440;
						break;
					}
				case 7:
					{
						break;
					}
				case 8:
					{
						clsTaxBillFormat.NumMessageLines = 8;
						if (clsTaxBillFormat.BreakdownDollars)
						{
							clsTaxBillFormat.MessageLeft = 3840;
							strTest = Strings.StrDup(49, "W");
							lblTest.Text = strTest;
							for (x = 0; x <= 12; x++)
							{
								txtMessage[FCConvert.ToInt16(x)].MaxLength = 49;
								//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
								// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
								txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
							}
							// x
						}
						else if (clsTaxBillFormat.Breakdown == true)
						{
							clsTaxBillFormat.MessageLeft = 2400;
							strTest = Strings.StrDup(49, "W");
							lblTest.Text = strTest;
							for (x = 0; x <= 12; x++)
							{
								txtMessage[FCConvert.ToInt16(x)].MaxLength = 49;
								//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
								// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
								txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
							}
							// x
						}
						else
						{
							clsTaxBillFormat.MessageLeft = 2400;
							clsTaxBillFormat.MessageCharacters = 49;
							strTest = Strings.StrDup(49, "W");
							lblTest.Text = strTest;
							for (x = 0; x <= 12; x++)
							{
								txtMessage[FCConvert.ToInt16(x)].MaxLength = 49;
								//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
								// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
								txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
							}
							// x
						}
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						clsTaxBillFormat.MessageTop = 3030;
						clsTaxBillFormat.DistributionLeft = 30;
						clsTaxBillFormat.DistributionTop = 3930;
						// framMailingAddress.Left = 50;
						// framMailingAddress.LeftOriginal = 50;
						break;
					}
				case 9:
					{
						// A,B
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						clsTaxBillFormat.NumMessageLines = 6;
						// txtMessage[1].Top = txtMessage[1].Top + txtMessage[1].Height;
						// txtMessage[2].Top = txtMessage[1].Top + txtMessage[1].Height;
						// txtMessage[3].Top = txtMessage[2].Top + txtMessage[2].Height;
						// txtMessage[4].Top = txtMessage[3].Top + txtMessage[3].Height;
						// txtMessage[5].Top = txtMessage[4].Top + txtMessage[4].Height;
						txtMessage[1].TopOriginal = txtMessage[0].TopOriginal + txtMessage[0].HeightOriginal;
						txtMessage[2].TopOriginal = txtMessage[1].TopOriginal + txtMessage[1].HeightOriginal;
						txtMessage[3].TopOriginal = txtMessage[2].TopOriginal + txtMessage[2].HeightOriginal;
						txtMessage[4].TopOriginal = txtMessage[3].TopOriginal + txtMessage[3].HeightOriginal;
						txtMessage[5].TopOriginal = txtMessage[4].TopOriginal + txtMessage[4].HeightOriginal;
						framReturnAddress.Visible = false;
						framDistribution.Visible = false;
						framDistTotals.Visible = false;
						//FC:FINAL:CHN - issue #1510: Redesign.
						// clsTaxBillFormat.MessageLeft = 50;
						clsTaxBillFormat.MessageLeft = 3840;
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						// clsTaxBillFormat.MessageTop = framMessage.Top;
						clsTaxBillFormat.MessageTop = framMessage.TopOriginal;
						clsTaxBillFormat.MessageCharacters = 42;
						strTest = Strings.StrDup(42, "W");
						lblTest.Text = strTest;
						for (x = 0; x <= 12; x++)
						{
							txtMessage[FCConvert.ToInt16(x)].MaxLength = 42;
							//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
							// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
							txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
						}
						// x
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						// framMailingAddress.Left = clsTaxBillFormat.MessageLeft + framMessage.Width + 50;
						// framMailingAddress.Top = clsTaxBillFormat.MessageTop;
						lblDescription.Visible = true;
						lblDescription.Text = "Message Line 1 will appear separately. (Above 'Interest At')";
						// lblDescription.Top = framMessage.Top - 40 - lblDescription.Height;
						// lblDescription.Left = clsTaxBillFormat.MessageLeft;
						lblDescription.TopOriginal = framMessage.TopOriginal - 40 - lblDescription.HeightOriginal;
						lblDescription.LeftOriginal = clsTaxBillFormat.MessageLeft;
						break;
					}
				case 10:
					{
						// A,B
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						clsTaxBillFormat.NumMessageLines = 4;
						// txtMessage[1].Top = txtMessage[1].Top + txtMessage[1].Height;
						// txtMessage[2].Top = txtMessage[1].Top + txtMessage[1].Height;
						// txtMessage[3].Top = txtMessage[2].Top + txtMessage[2].Height;
						txtMessage[1].TopOriginal = txtMessage[0].TopOriginal + txtMessage[0].HeightOriginal;
						txtMessage[2].TopOriginal = txtMessage[1].TopOriginal + txtMessage[1].HeightOriginal;
						txtMessage[3].TopOriginal = txtMessage[2].TopOriginal + txtMessage[2].HeightOriginal;
						framReturnAddress.Visible = false;
						framDistribution.Visible = false;
						framDistTotals.Visible = false;
						//FC:FINAL:CHN - issue #1510: Redesign.
						// clsTaxBillFormat.MessageLeft = 50;
						clsTaxBillFormat.MessageLeft = 3840;
						// clsTaxBillFormat.MessageTop = framMessage.Top;
						clsTaxBillFormat.MessageTop = framMessage.TopOriginal;
						clsTaxBillFormat.MessageCharacters = 42;
						strTest = Strings.StrDup(42, "W");
						lblTest.Text = strTest;
						for (x = 0; x <= 12; x++)
						{
							txtMessage[FCConvert.ToInt16(x)].MaxLength = 42;
							//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
							// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
							txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
						}
						// x
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						// framMailingAddress.Left = clsTaxBillFormat.MessageLeft + framMessage.Width + 50;
						// framMailingAddress.Top = clsTaxBillFormat.MessageTop;
						lblDescription.Visible = true;
						lblDescription.Text = "Message Line 1 will appear separately. (Above 'Interest At')";
						// lblDescription.Top = framMessage.Top - 40 - lblDescription.Height;
						// lblDescription.Left = clsTaxBillFormat.MessageLeft;
						lblDescription.TopOriginal = framMessage.TopOriginal - 40 - lblDescription.HeightOriginal;
						lblDescription.LeftOriginal = clsTaxBillFormat.MessageLeft;
						break;
					}
				case 11:
					{
						// C
						//FC:FINAL:CHN - issue #1510: Redesign.
						clsTaxBillFormat.MessageLeft = 3840;
						clsTaxBillFormat.NumMessageLines = 5;
						clsTaxBillFormat.MessageCharacters = 75;
						strTest = Strings.StrDup(75, "W");
						lblTest.Text = strTest;
						for (x = 0; x <= 12; x++)
						{
							txtMessage[FCConvert.ToInt16(x)].MaxLength = 75;
							//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
							// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
							txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
						}
						// x
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting). Some redesign - different behavior with original.
						framReturnAddress.Visible = false;
						framDistribution.Visible = false;
						framDistTotals.Visible = false;
						// framMailingAddress.Left = 50;
						// framMailingAddress.Top = framMessage.Top;
						// clsTaxBillFormat.MessageTop = framMailingAddress.Top + framMailingAddress.Height + 50;
						clsTaxBillFormat.MessageTop = framMessage.TopOriginal;
						break;
					}
				case 12:
					{
						// D
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						// clsTaxBillFormat.MessageLeft = 50;
						clsTaxBillFormat.MessageLeft = 3840;
						// clsTaxBillFormat.MessageTop = framMessage.Top;
						clsTaxBillFormat.MessageTop = framMessage.TopOriginal;
						framReturnAddress.Visible = false;
						framDistribution.Visible = false;
						framDistTotals.Visible = false;
						clsTaxBillFormat.NumMessageLines = 7;
						clsTaxBillFormat.MessageCharacters = 20;
						strTest = Strings.StrDup(20, "W");
						lblTest.Text = strTest;
						for (x = 0; x <= 1; x++)
						{
							txtMessage[FCConvert.ToInt16(x)].MaxLength = 20;
							//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
							// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
							txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
						}
						// x
						clsTaxBillFormat.MessageCharacters = 42;
						strTest = Strings.StrDup(42, "W");
						lblTest.Text = strTest;
						// for (x = 2; x <= 12; x++)
						for (x = 0; x <= 12; x++)
						{
							txtMessage[FCConvert.ToInt16(x)].MaxLength = 42;
							//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
							// txtMessage[FCConvert.ToInt16(x].Width = lblTest.Width + 200;
							txtMessage[FCConvert.ToInt16(x)].WidthOriginal = lblTest.WidthOriginal + 200;
						}
						// x
						//FC:FINAL:CHN - issue #1510: Incorrect margin parameters (missing converting).
						// framMailingAddress.Left = clsTaxBillFormat.MessageLeft + framMessage.Width + 50;
						// framMailingAddress.Top = clsTaxBillFormat.MessageTop;
						lblDescription.Visible = true;
						lblDescription.Text = "Message Line 1 and 2 will appear separately. (Below Tax Year)";
						// lblDescription.Top = framMessage.Top - 40 - lblDescription.Height;
						// lblDescription.Left = clsTaxBillFormat.MessageLeft;
						lblDescription.TopOriginal = framMessage.TopOriginal - 40 - lblDescription.HeightOriginal;
						lblDescription.LeftOriginal = clsTaxBillFormat.MessageLeft;
						break;
					}
				case 13:
					{
						break;
					}
			}
			//end switch
			//FC:FINAL:CHN - issue #1490: Incorrect margin parameters (missing converting).
			// framMessage.Top = clsTaxBillFormat.MessageTop;
			framMessage.TopOriginal = clsTaxBillFormat.MessageTop;
			// framMessage.Left = clsTaxBillFormat.MessageLeft;
			framMessage.LeftOriginal = clsTaxBillFormat.MessageLeft;
			if (clsTaxBillFormat.Billformat == 12)
			{
				// framMessage.Width = txtMessage[2].Width + 200;
				framMessage.WidthOriginal = txtMessage[2].WidthOriginal + 200;
			}
			else
			{
				// framMessage.Width = .MessageLeft * 2 + txtMessage(0).Width
				// framMessage.Width = 200 + txtMessage[0].Width;
				framMessage.WidthOriginal = 200 + txtMessage[0].WidthOriginal;
			}
			// framDistribution.Top = clsTaxBillFormat.DistributionTop;
			framDistribution.TopOriginal = clsTaxBillFormat.DistributionTop;
			// framDistribution.Left = clsTaxBillFormat.DistributionLeft;
			framDistribution.LeftOriginal = clsTaxBillFormat.DistributionLeft;
			// framDistTotals.Left = framDistribution.Left + 2340;
			framDistTotals.LeftOriginal = framDistribution.LeftOriginal + 2340;
			// framDistTotals.Top = framDistribution.Top;
			framDistTotals.TopOriginal = framDistribution.TopOriginal;
			clsTaxBillFormat.MessageCharacters = FCConvert.ToInt16(txtMessage[0].MaxLength);
			for (x = clsTaxBillFormat.NumMessageLines; x <= 12; x++)
			{
				txtMessage[FCConvert.ToInt16(x)].Visible = false;
			}
			// x
			// size the frame so it just covers the visible textboxes
			// framMessage.Height = framMessage.Top + (txtMessage[0].Height * clsTaxBillFormat.NumMessageLines) + txtMessage[0].Top + 20;
			framMessage.HeightOriginal = framMessage.TopOriginal + (txtMessage[0].HeightOriginal * clsTaxBillFormat.NumMessageLines) + txtMessage[0].TopOriginal + 20;
		}

		private void txtMessage_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Right)
			{
				// single quote
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMessage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			short index = txtMessage.GetIndex((FCTextBox)sender);
			txtMessage_KeyPress(index, sender, e);
		}
	}
}
