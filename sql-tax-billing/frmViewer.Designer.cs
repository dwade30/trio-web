﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmViewer.
	/// </summary>
	partial class frmViewer : FCForm
	{
		public FCGrid GridList;
		public fecherFoundation.FCFrame framReports;
		public fecherFoundation.FCComboBox cmbDate;
		public ARViewer ARViewer21;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuLoad;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuEmail;
		public fecherFoundation.FCToolStripMenuItem mnuEmailRTF;
		public fecherFoundation.FCToolStripMenuItem mnuEmailPDF;
		public fecherFoundation.FCToolStripMenuItem mnuExport;
		public fecherFoundation.FCToolStripMenuItem mnuRTF;
		public fecherFoundation.FCToolStripMenuItem mnuExportPDF;
		public fecherFoundation.FCToolStripMenuItem mnuHTML;
		public fecherFoundation.FCToolStripMenuItem mnuExcel;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmViewer));
			this.GridList = new fecherFoundation.FCGrid();
			this.framReports = new fecherFoundation.FCFrame();
			this.cmbDate = new fecherFoundation.FCComboBox();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuLoad = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEmail = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEmailRTF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEmailPDF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRTF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExportPDF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHTML = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExcel = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.toolBar1 = new Wisej.Web.ToolBar();
			this.toolBarButtonEmailPDF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonEmailRTF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportPDF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportRTF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportHTML = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportExcel = new Wisej.Web.ToolBarButton();
			this.toolBarButtonPrint = new Wisej.Web.ToolBarButton();
			this.ARViewer21 = new Global.ARViewer();
			this.cmdPreview = new fecherFoundation.FCButton();
			((System.ComponentModel.ISupportInitialize)(this.GridList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framReports)).BeginInit();
			this.framReports.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// GridList
			// 
			this.GridList.AllowSelection = false;
			this.GridList.AllowUserToResizeColumns = false;
			this.GridList.AllowUserToResizeRows = false;
			this.GridList.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridList.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridList.BackColorBkg = System.Drawing.Color.Empty;
			this.GridList.BackColorFixed = System.Drawing.Color.Empty;
			this.GridList.BackColorSel = System.Drawing.Color.Empty;
			this.GridList.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridList.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridList.ColumnHeadersHeight = 30;
			this.GridList.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridList.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridList.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridList.DragIcon = null;
			this.GridList.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridList.FixedCols = 0;
			this.GridList.FixedRows = 0;
			this.GridList.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridList.FrozenCols = 0;
			this.GridList.GridColor = System.Drawing.Color.Empty;
			this.GridList.GridColorFixed = System.Drawing.Color.Empty;
			this.GridList.Location = new System.Drawing.Point(497, 123);
			this.GridList.Name = "GridList";
			this.GridList.ReadOnly = true;
			this.GridList.RowHeadersVisible = false;
			this.GridList.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridList.RowHeightMin = 0;
			this.GridList.Rows = 0;
			this.GridList.ScrollTipText = null;
			this.GridList.ShowColumnVisibilityMenu = false;
			this.GridList.ShowFocusCell = false;
			this.GridList.Size = new System.Drawing.Size(81, 51);
			this.GridList.StandardTab = true;
			this.GridList.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridList.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridList.TabIndex = 3;
			this.GridList.Visible = false;
			// 
			// framReports
			// 
			this.framReports.Controls.Add(this.cmdPreview);
			this.framReports.Controls.Add(this.cmbDate);
			this.framReports.Location = new System.Drawing.Point(464, 196);
			this.framReports.Name = "framReports";
			this.framReports.Size = new System.Drawing.Size(273, 160);
			this.framReports.TabIndex = 2;
			this.framReports.Text = "Date Report Created";
			// 
			// cmbDate
			// 
			this.cmbDate.AutoSize = false;
			this.cmbDate.BackColor = System.Drawing.SystemColors.Window;
			this.cmbDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDate.FormattingEnabled = true;
			this.cmbDate.Location = new System.Drawing.Point(20, 30);
			this.cmbDate.Name = "cmbDate";
			this.cmbDate.Size = new System.Drawing.Size(236, 40);
			this.cmbDate.TabIndex = 0;
			this.cmbDate.Text = "Combo1";
			// 
			// MainMenu1
			// 
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuLoad
			// 
			this.mnuLoad.Index = 0;
			this.mnuLoad.Name = "mnuLoad";
			this.mnuLoad.Text = "Load New Report";
			this.mnuLoad.Click += new System.EventHandler(this.mnuLoad_Click);
			// 
			// mnuPreview
			// 
			this.mnuPreview.Index = 5;
			this.mnuPreview.Name = "mnuPreview";
			this.mnuPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPreview.Text = "Preview Report";
			this.mnuPreview.Click += new System.EventHandler(this.mnuPreview_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuLoad,
				this.mnuSepar2,
				this.mnuEmail,
				this.mnuExport,
				this.mnuSepar3,
				this.mnuPreview,
				this.mnuSepar1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuEmail
			// 
			this.mnuEmail.Index = 2;
			this.mnuEmail.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuEmailRTF,
				this.mnuEmailPDF
			});
			this.mnuEmail.Name = "mnuEmail";
			this.mnuEmail.Text = "E-mail";
			// 
			// mnuEmailRTF
			// 
			this.mnuEmailRTF.Index = 0;
			this.mnuEmailRTF.Name = "mnuEmailRTF";
			this.mnuEmailRTF.Text = "E-mail as Rich Text";
			this.mnuEmailRTF.Click += new System.EventHandler(this.mnuEmailRTF_Click);
			// 
			// mnuEmailPDF
			// 
			this.mnuEmailPDF.Index = 1;
			this.mnuEmailPDF.Name = "mnuEmailPDF";
			this.mnuEmailPDF.Text = "E-mail as PDF";
			this.mnuEmailPDF.Click += new System.EventHandler(this.mnuEmailPDF_Click);
			// 
			// mnuExport
			// 
			this.mnuExport.Index = 3;
			this.mnuExport.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRTF,
				this.mnuExportPDF,
				this.mnuHTML,
				this.mnuExcel
			});
			this.mnuExport.Name = "mnuExport";
			this.mnuExport.Text = "Export";
			// 
			// mnuRTF
			// 
			this.mnuRTF.Index = 0;
			this.mnuRTF.Name = "mnuRTF";
			this.mnuRTF.Text = "Export as Rich Text";
			this.mnuRTF.Click += new System.EventHandler(this.mnuRTF_Click);
			// 
			// mnuExportPDF
			// 
			this.mnuExportPDF.Index = 1;
			this.mnuExportPDF.Name = "mnuExportPDF";
			this.mnuExportPDF.Text = "Export as PDF";
			this.mnuExportPDF.Click += new System.EventHandler(this.mnuExportPDF_Click);
			// 
			// mnuHTML
			// 
			this.mnuHTML.Index = 2;
			this.mnuHTML.Name = "mnuHTML";
			this.mnuHTML.Text = "Export as HTML";
			this.mnuHTML.Click += new System.EventHandler(this.mnuHTML_Click);
			// 
			// mnuExcel
			// 
			this.mnuExcel.Index = 3;
			this.mnuExcel.Name = "mnuExcel";
			this.mnuExcel.Text = "Export as Excel";
			this.mnuExcel.Click += new System.EventHandler(this.mnuExcel_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 4;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 6;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 7;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// toolBar1
			// 
			this.toolBar1.AutoSize = false;
			this.toolBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
			this.toolBar1.Buttons.AddRange(new Wisej.Web.ToolBarButton[] {
				this.toolBarButtonEmailPDF,
				this.toolBarButtonEmailRTF,
				this.toolBarButtonExportPDF,
				this.toolBarButtonExportRTF,
				this.toolBarButtonExportHTML,
				this.toolBarButtonExportExcel,
				this.toolBarButtonPrint
			});
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.Size = new System.Drawing.Size(977, 40);
			this.toolBar1.TabIndex = 0;
			this.toolBar1.TabStop = false;
            this.toolBar1.ButtonClick += new Wisej.Web.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // toolBarButtonEmailPDF
            // 
            this.toolBarButtonEmailPDF.ImageSource = "icon-report-email-pdf";
			this.toolBarButtonEmailPDF.Name = "toolBarButtonEmailPDF";
			this.toolBarButtonEmailPDF.ToolTipText = "Email as PDF";
			// 
			// toolBarButtonEmailRTF
			// 
			this.toolBarButtonEmailRTF.ImageSource = "icon-report-email-rtf";
			this.toolBarButtonEmailRTF.Name = "toolBarButtonEmailRTF";
			this.toolBarButtonEmailRTF.ToolTipText = "Email as RTF";
			// 
			// toolBarButtonExportPDF
			// 
			this.toolBarButtonExportPDF.ImageSource = "icon-report-export-pdf";
			this.toolBarButtonExportPDF.Name = "toolBarButtonExportPDF";
			this.toolBarButtonExportPDF.ToolTipText = "Export as PDF";
			// 
			// toolBarButtonExportRTF
			// 
			this.toolBarButtonExportRTF.ImageSource = "icon-report-export-rtf";
			this.toolBarButtonExportRTF.Name = "toolBarButtonExportRTF";
			this.toolBarButtonExportRTF.ToolTipText = "Export as RTF";
			// 
			// toolBarButtonExportHTML
			// 
			this.toolBarButtonExportHTML.ImageSource = "icon-report-export-html";
			this.toolBarButtonExportHTML.Name = "toolBarButtonExportHTML";
			this.toolBarButtonExportHTML.ToolTipText = "Export as HTML";
			// 
			// toolBarButtonExportExcel
			// 
			this.toolBarButtonExportExcel.ImageSource = "icon-report-export-excel";
			this.toolBarButtonExportExcel.Name = "toolBarButtonExportExcel";
			this.toolBarButtonExportExcel.ToolTipText = "Export as Excel";
			// 
			// toolBarButtonPrint
			// 
			this.toolBarButtonPrint.ImageSource = "icon-report-print";
			this.toolBarButtonPrint.Name = "toolBarButtonExportExcel";
			this.toolBarButtonPrint.ToolTipText = "Print";
			// 
			// ARViewer21
			// 
			this.ARViewer21.Dock = Wisej.Web.DockStyle.Fill;
			this.ARViewer21.Location = new System.Drawing.Point(0, 40);
			this.ARViewer21.Name = "ARViewer21";
			this.ARViewer21.ReportName = null;
			this.ARViewer21.ReportSource = null;
			this.ARViewer21.Size = new System.Drawing.Size(977, 520);
			this.ARViewer21.TabIndex = 1;
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.Location = new System.Drawing.Point(20, 92);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.Size = new System.Drawing.Size(236, 48);
			this.cmdPreview.TabIndex = 1;
			this.cmdPreview.Text = "Preview Report";
			this.cmdPreview.Click += new System.EventHandler(this.mnuPreview_Click);
			// 
			// frmViewer
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(977, 560);
			this.Controls.Add(this.framReports);
			this.Controls.Add(this.ARViewer21);
			this.Controls.Add(this.toolBar1);
			this.Controls.Add(this.GridList);
			this.KeyPreview = true;
			this.Name = "frmViewer";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.frmViewer_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmViewer_KeyDown);
			((System.ComponentModel.ISupportInitialize)(this.GridList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framReports)).EndInit();
			this.framReports.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private ToolBar toolBar1;
		private ToolBarButton toolBarButtonEmailPDF;
		private ToolBarButton toolBarButtonEmailRTF;
		private ToolBarButton toolBarButtonExportPDF;
		private ToolBarButton toolBarButtonExportRTF;
		private ToolBarButton toolBarButtonExportHTML;
		private ToolBarButton toolBarButtonExportExcel;
		private ToolBarButton toolBarButtonPrint;
		private FCButton cmdPreview;
	}
}
