﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmRERange.
	/// </summary>
	partial class frmRERange : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbAccounts;
		public fecherFoundation.FCLabel lblAccounts;
		public Global.T2KDateBox t2kUseOwnerAsOf;
		public fecherFoundation.FCCheckBox chkUseOwnerAsOf;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRERange));
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbAccounts = new fecherFoundation.FCComboBox();
			this.lblAccounts = new fecherFoundation.FCLabel();
			this.t2kUseOwnerAsOf = new Global.T2KDateBox();
			this.chkUseOwnerAsOf = new fecherFoundation.FCCheckBox();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kUseOwnerAsOf)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseOwnerAsOf)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 258);
			this.BottomPanel.Size = new System.Drawing.Size(949, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.t2kUseOwnerAsOf);
			this.ClientArea.Controls.Add(this.chkUseOwnerAsOf);
			this.ClientArea.Controls.Add(this.txtEnd);
			this.ClientArea.Controls.Add(this.txtStart);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.cmbAccounts);
			this.ClientArea.Controls.Add(this.lblAccounts);
			this.ClientArea.Controls.Add(this.lblTo);
			this.ClientArea.Size = new System.Drawing.Size(949, 198);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(949, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(247, 30);
			this.HeaderText.Text = "Real Estate Accounts";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"Accounts",
				"Names",
				"Map / Lots",
				"Locations",
				"Tran Codes",
				"Land Codes",
				"Bldg Codes"
			});
			this.cmbRange.Location = new System.Drawing.Point(581, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(242, 40);
			this.cmbRange.TabIndex = 5;
			this.cmbRange.Text = "Accounts";
			this.cmbRange.Visible = false;
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(472, 44);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 4;
			this.lblRange.Text = "RANGE";
			this.lblRange.Visible = false;
			// 
			// cmbAccounts
			// 
			this.cmbAccounts.AutoSize = false;
			this.cmbAccounts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAccounts.FormattingEnabled = true;
			this.cmbAccounts.Items.AddRange(new object[] {
				"All Accounts",
				"Range",
				"Specific"
			});
			this.cmbAccounts.Location = new System.Drawing.Point(164, 30);
			this.cmbAccounts.Name = "cmbAccounts";
			this.cmbAccounts.Size = new System.Drawing.Size(212, 40);
			this.cmbAccounts.TabIndex = 1;
			this.cmbAccounts.Text = "All Accounts";
			this.cmbAccounts.SelectedIndexChanged += new System.EventHandler(this.cmbAccounts_SelectedIndexChanged);
			// 
			// lblAccounts
			// 
			this.lblAccounts.AutoSize = true;
			this.lblAccounts.Location = new System.Drawing.Point(30, 44);
			this.lblAccounts.Name = "lblAccounts";
			this.lblAccounts.Size = new System.Drawing.Size(77, 15);
			this.lblAccounts.TabIndex = 0;
			this.lblAccounts.Text = "ACCOUNTS";
			// 
			// t2kUseOwnerAsOf
			// 
			this.t2kUseOwnerAsOf.Location = new System.Drawing.Point(254, 90);
			this.t2kUseOwnerAsOf.Mask = "##/##/####";
			this.t2kUseOwnerAsOf.Name = "t2kUseOwnerAsOf";
			this.t2kUseOwnerAsOf.Size = new System.Drawing.Size(122, 40);
			this.t2kUseOwnerAsOf.TabIndex = 3;
			this.t2kUseOwnerAsOf.Text = "  /  /";
			this.t2kUseOwnerAsOf.Visible = false;
			// 
			// chkUseOwnerAsOf
			// 
			this.chkUseOwnerAsOf.Location = new System.Drawing.Point(30, 98);
			this.chkUseOwnerAsOf.Name = "chkUseOwnerAsOf";
			this.chkUseOwnerAsOf.Size = new System.Drawing.Size(154, 27);
			this.chkUseOwnerAsOf.TabIndex = 2;
			this.chkUseOwnerAsOf.Text = "Use Owner As Of";
			this.chkUseOwnerAsOf.Visible = false;
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(778, 90);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(158, 40);
			this.txtEnd.TabIndex = 8;
			this.txtEnd.Visible = false;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(470, 90);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(160, 40);
			this.txtStart.TabIndex = 6;
			this.txtStart.Visible = false;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(690, 104);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(28, 19);
			this.lblTo.TabIndex = 7;
			this.lblTo.Text = "TO";
			this.lblTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTo.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuCancel
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuCancel
			// 
			this.mnuCancel.Index = 2;
			this.mnuCancel.Name = "mnuCancel";
			this.mnuCancel.Text = "Cancel";
			this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(423, 30);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(100, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmRERange
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(949, 366);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmRERange";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Real Estate Accounts";
			this.Load += new System.EventHandler(this.frmRERange_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRERange_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kUseOwnerAsOf)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseOwnerAsOf)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
