﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREAuditOneLine.
	/// </summary>
	public partial class rptREAuditOneLine : BaseSectionReport
	{
		public rptREAuditOneLine()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate Audit Report";
		}

		public static rptREAuditOneLine InstancePtr
		{
			get
			{
				return (rptREAuditOneLine)Sys.GetInstance(typeof(rptREAuditOneLine));
			}
		}

		protected rptREAuditOneLine _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptREAuditOneLine	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCurrRow;
		int lngLastRow;
		bool boolSaveToFile;
		const int colAcct = 0;
		const int colName = 1;
		const int colMap = 2;
		const int colSnum = 3;
		const int colLoc = 4;
		const int colLand = 5;
		const int colBldg = 6;
		const int colExempt = 7;
		const int colTot = 8;
		const int colTax = 9;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = (lngCurrRow > lngLastRow);
		}

		public void Init()
		{
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (frmREAudit.InstancePtr.intWhereFrom == 3)
			{
				lblTitle.Text = "Real Estate Billing Transfer Report";
				this.Name = "Real Estate Billing Transfer Report";
			}
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
			{
				txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				txtMuniName.Text = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
			}
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lngCurrRow = 1;
			lngLastRow = frmREAudit.InstancePtr.AuditGrid.Rows - 1;
			txtTotLand.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 2);
			txtTotBldg.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 3);
			txtTotAssess.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 5);
			txtTotTax.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 6);
			txtCount.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(1, 1);
			txtHsteadCount.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(2, 1);
			txthsteadLand.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(2, 2);
			txtHsteadBldg.Text = frmREAudit.InstancePtr.TotalGrid.TextMatrix(2, 3);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// If Not frmREAudit.boolDisplayFirst Then
			// Unload frmREAudit
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCurrRow <= lngLastRow)
			{
				txtName.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colName);
				txtLand.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colLand);
				txtBldg.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colBldg);
				txtAssessment.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colTot);
				txtTax.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colTax);
				txtMapLot.Text = frmREAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colMap);
			}
			lngCurrRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

	
	}
}
