﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPCollectionWide.
	/// </summary>
	public partial class rptPPCollectionWide : BaseSectionReport
	{
		public rptPPCollectionWide()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Personal Property Collection List";
		}

		public static rptPPCollectionWide InstancePtr
		{
			get
			{
				return (rptPPCollectionWide)Sys.GetInstance(typeof(rptPPCollectionWide));
			}
		}

		protected rptPPCollectionWide _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCommit.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPCollectionWide	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		/// <summary>
		/// This is the Wide and Landscape version of the Personal Property Collections List
		/// </summary>
		int TaxYear;
		clsDRWrapper clsCommit = new clsDRWrapper();
		string Addr1 = "";
		string Addr2 = "";
		string addr3 = "";
		private string strMailingAddress3 = "";
		int intStartPage;
		int intPage;
		string[] Categoryarray = new string[9 + 1];
		// vbPorter upgrade warning: lngValSub As int	OnWrite(short, double)
		int lngValSub;
		// vbPorter upgrade warning: lngExemptsub As int	OnWrite(short, double)
		int lngExemptsub;
		// vbPorter upgrade warning: lngAssessSub As int	OnWrite(short, double)
		int lngAssessSub;
		double lngTaxSub;
		double dblDisc;
		double dblTax;
		int lngPage;
		bool boolIncludeAddress;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			lngValSub = 0;
			lngExemptsub = 0;
			lngAssessSub = 0;
			lngTaxSub = 0;
			lngPage = 1;
			// These fields will be filled with values that will then automatically
			// go to the textboxes and the summary textboxes in the page footer
			this.Fields.Add("Cat1Val");
			this.Fields.Add("Cat2Val");
			this.Fields.Add("Cat3Val");
			this.Fields.Add("CatOtherVal");
			this.Fields.Add("TheBinder");
			// fill the array with the 9 category descriptions
			clsTemp.OpenRecordset("select * from ratiotrends order by type", "twpp0000.vb1");
			while (!clsTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				Categoryarray[clsTemp.Get_Fields("type")] = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
				clsTemp.MoveNext();
			}
			clsTemp.Dispose();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsCommit.EndOfFile();
			if (!eArgs.EOF)
			{
				FillValueFields();
			}
		}

		private void ActiveReport_QueryClose(ref short Cancel, ref short CloseMode)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			Font fnt;
			int const_printtoolid;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so we handle the event
			const_printtoolid = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_printtoolid;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//}
			// cnt
			intPage = intStartPage;
			txtDate.Text = DateTime.Today.ToShortDateString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	string vbPorterVar = Tool.Caption;
		//	// if they click the print button
		//	if (vbPorterVar == "Print...")
		//	{
		//		// get the page range they want to print
		//		modGlobalVariables.Statics.gintStartPage = intStartPage;
		//		modGlobalVariables.Statics.gintEndPage = this.Pages.Count;
		//		frmNumPages.InstancePtr.Init(ref modGlobalVariables.Statics.gintEndPage, ref modGlobalVariables.Statics.gintStartPage);
		//		// frmNumPages.Show vbModal, MDIParent
		//		// set the printers printrange
		//		this.Printer.FromPage = modGlobalVariables.Statics.gintStartPage;
		//		this.Printer.ToPage = modGlobalVariables.Statics.gintEndPage;
		//		this.PrintReport(false);
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsCommit.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = clsCommit.Get_Fields_String("account");
				txtName.Text = clsCommit.Get_Fields_String("name1");
				txtAssessment.Value = Conversion.Val(clsCommit.Get_Fields_Int32("ppassessment"));
				lngValSub += FCConvert.ToInt32(Conversion.Val(clsCommit.Get_Fields_Int32("ppassessment")));
				txtExempt.Value = Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
				lngExemptsub += FCConvert.ToInt32(Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue")));
				lngAssessSub += FCConvert.ToInt32(Conversion.Val(clsCommit.Get_Fields_Int32("ppassessment")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue")));
				txtTotal.Value = Conversion.Val(clsCommit.Get_Fields_Int32("ppassessment")) - Conversion.Val(clsCommit.Get_Fields_Int32("exemptvalue"));
				dblTax = Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue1")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue2")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue3")) + Conversion.Val(clsCommit.Get_Fields_Decimal("taxdue4"));
				txtTax.Value = dblTax;
				lngTaxSub += dblTax;
				txtDiscount.Text = Strings.Format(modMain.Round(dblTax * dblDisc, 2), "#,###,###,##0.00");
				// get the address fields and then condense them to get rid of gaps
				if (boolIncludeAddress)
				{
					Addr1 = FCConvert.ToString(clsCommit.Get_Fields_String("address1"));
					Addr2 = FCConvert.ToString(clsCommit.Get_Fields_String("address2"));
					addr3 = FCConvert.ToString(clsCommit.Get_Fields_String("address3"));
					strMailingAddress3 = FCConvert.ToString(clsCommit.Get_Fields_String("MailingAddress3"));
					if (Strings.Trim(Addr1) == string.Empty)
					{
						Addr1 = Addr2;
						Addr2 = strMailingAddress3;
						strMailingAddress3 = addr3;
						addr3 = "";
						if (Strings.Trim(Addr1) == string.Empty)
						{
							Addr1 = Addr2;
							Addr2 = strMailingAddress3;
							strMailingAddress3 = addr3;
							addr3 = "";
						}
					}
					if (Strings.Trim(Addr2) == string.Empty)
					{
						Addr2 = strMailingAddress3;
						strMailingAddress3 = addr3;
						addr3 = "";
					}
					txtAddress1.Text = Addr1;
					txtAddress2.Text = Addr2;
					txtAddress3.Text = addr3;
					txtMailingAddress3.Text = strMailingAddress3;
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtLocation.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtAddress1.Text = Strings.Trim(Strings.Trim(Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields("streetnumber"))) + " " + clsCommit.Get_Fields_String("apt")) + " " + Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("streetname"))));
				}
				clsCommit.MoveNext();
			}
		}
		// vbPorter upgrade warning: lngTaxYear As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intWhichReport As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		public void Init(int lngTaxYear, bool boolSetFont, string strFontName, string strPrinterName, int intWhichReport, double dblDiscount, int intOrder, bool boolPrintAddress = true)
		{
			string strOrder = "";
			boolIncludeAddress = boolPrintAddress;
			TaxYear = lngTaxYear;
			// set the initial page number
			intStartPage = 1;
			if (intOrder == 2)
			{
				strOrder = "name1";
			}
			else
			{
				strOrder = "account";
			}
			dblDisc = dblDiscount;
			if (dblDisc > 0)
			{
				dblDisc = modMain.Round(dblDisc / 100, 4);
				txtDiscount.Visible = true;
				lblDiscount.Visible = true;
			}
			else
			{
				txtDiscount.Visible = false;
				lblDiscount.Visible = false;
			}
			clsCommit.OpenRecordset("select * from billingmaster where billingyear = " + FCConvert.ToString(TaxYear) + "1 and billingtype = 'PP' order by " + strOrder, "twcl0000.vb1");
			if (!clsCommit.EndOfFile())
			{
				lblTitle.Text = lblTitle.Text + FCConvert.ToString(TaxYear);
				lblMuniname.Text = modGlobalConstants.Statics.MuniName;
				// if using a printer font, then set all textboxes to it, and make captions bold
				if (boolSetFont)
				{
					foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl fld in this.GetAllControls())
					{
						bool setFont = false;
						bool bold = false;
						if (FCConvert.ToString(fld.Tag) == "textbox")
						{
							setFont = true;
						}
						else if (FCConvert.ToString(fld.Tag) == "bold")
						{
							setFont = true;
							bold = true;
						}
						if (setFont)
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = fld as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textBox != null)
							{
								textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = fld as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
								}
							}
						}
					}
					// fld
					//this.Printer.RenderMode = 1;
				}
				// set the printer to the one they picked
				this.Document.Printer.PrinterName = strPrinterName;
				// set the orientation depending on which report they picked
				switch (intWhichReport)
				{
					case 2:
						{
							// landscape regular
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							break;
						}
					case 3:
						{
							// wide printer
							//this.Document.Printer.PaperSize = 39;
							this.Document.Printer.PaperKind = System.Drawing.Printing.PaperKind.USStandardFanfold;
							break;
						}
				}
				//end switch
				// Me.Show , MDIParent
				//FC:FINAL:MSH - restore missing report call
				frmReportViewer.InstancePtr.Init(this, strPrinterName, boolLandscape: intWhichReport == 2, boolAllowEmail: true, strAttachmentName: "PPCollectionList");
			}
			else
			{
				// nothing to print so exit
				MessageBox.Show("No records found.", "Nothing to print", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//MDIParent.InstancePtr.Show();
				this.Close();
			}
			return;
		}

		private void FillValueFields()
		{
			// must do this or the values will not fill in correctly
			int x;
			// loop counter
			int CurrCategory;
			int lngOther;
			// accumulate category values
			int lngCategory;
			// current category's value
			if (Strings.Trim(FCConvert.ToString(clsCommit.Get_Fields_String("name1"))) != string.Empty)
			{
				this.Fields["TheBinder"].Value = Strings.Left(FCConvert.ToString(clsCommit.Get_Fields_String("name1")), 1);
			}
			else
			{
				this.Fields["TheBinder"].Value = "";
			}
			// fill the monetary fields
			// fill category information, but initialize some things first
			CurrCategory = 0;
			lngOther = 0;
			lngCategory = 0;
			txtCategory1.Text = "";
			txtCategory2.Text = "";
			txtCategory3.Text = "";
			txtCategoryOther.Text = "";
			this.Fields["Cat1Val"].Value = 0;
			this.Fields["Cat2Val"].Value = 0;
			this.Fields["Cat3Val"].Value = 0;
			this.Fields["CatOtherVal"].Value = 0;
			txtCatOtherVal.Text = "";
			// just use first four categories that have values
			// if more than four then put 4+ together as other
			for (x = 1; x <= 9; x++)
			{
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				lngCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCommit.Get_Fields("category" + FCConvert.ToString(x)))));
				if (lngCategory > 0)
				{
					CurrCategory += 1;
					if (CurrCategory < 5)
					{
						lngOther = lngCategory;
						switch (CurrCategory)
						{
							case 1:
								{
									txtCategory1.Text = Categoryarray[x];
									this.Fields["Cat1Val"].Value = lngCategory;
									break;
								}
							case 2:
								{
									txtCategory2.Text = Categoryarray[x];
									this.Fields["Cat2Val"].Value = lngCategory;
									break;
								}
							case 3:
								{
									txtCategory3.Text = Categoryarray[x];
									this.Fields["Cat3Val"].Value = lngCategory;
									break;
								}
							case 4:
								{
									txtCategoryOther.Text = Categoryarray[x];
									this.Fields["CatOtherVal"].Value = lngCategory;
									break;
								}
						}
						//end switch
					}
					else
					{
						lngOther += lngCategory;
						txtCategoryOther.Text = "OTHER";
						this.Fields["CatOtherVal"].Value = lngOther;
					}
				}
			}
			// x
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotAssess.Text = Strings.Format(lngValSub, "#,###,###,##0");
			txtTotExempt.Text = Strings.Format(lngExemptsub, "#,###,###,##0");
			txtTotTotal.Text = Strings.Format(lngAssessSub, "#,###,###,##0");
			txtTotTax.Text = Strings.Format(lngTaxSub, "#,###,###,##0.00");
		}

		
	}
}
