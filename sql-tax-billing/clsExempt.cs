﻿//Fecher vbPorter - Version 1.0.0.35
namespace TWBL0000
{
	public class clsExempt
	{
		//=========================================================
		string strDescription;
		string strShortDescription;
		int lngUnit;

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public int Unit
		{
			get
			{
				int Unit = 0;
				Unit = lngUnit;
				return Unit;
			}
			set
			{
				lngUnit = value;
			}
		}
	}
}
