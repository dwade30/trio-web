﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPAudit.
	/// </summary>
	public partial class rptPPAudit : BaseSectionReport
	{
		public rptPPAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Personal Property Audit Report";
		}

		public static rptPPAudit InstancePtr
		{
			get
			{
				return (rptPPAudit)Sys.GetInstance(typeof(rptPPAudit));
			}
		}

		protected rptPPAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCurrRow;
		int lngLastRow;
		int intPage;
		bool boolSaveToFile;
		bool boolSaveFile;
		bool boolUnload;
		const int colAcct = 0;
		const int colName = 1;
		const int colSnum = 2;
		const int colLoc = 3;
		const int colValue = 4;
		const int colExempt = 5;
		const int colAssessment = 6;
		const int colTax = 7;
		private bool boolFromAudit;

		public void Init(bool boolPrintSingleLineVersion = false, bool boolSave = true, bool boolAudit = false, bool boolExit = false)
		{
			boolUnload = boolExit;
			if (boolPrintSingleLineVersion)
			{
				txtLocation.Visible = false;
				Detail.Height = txtName.Top + txtName.Height + 50 / 1440f;
			}
			boolSaveFile = boolSave;
			boolFromAudit = boolAudit;
			//Application.DoEvents();
			//FC:FINAL:MSH - issue #1340: restore missing report call
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "PPAudit");
		}
		//private void ActiveReport_Error(int Number, DDActiveReports2.IReturnString Description, int Scode, string Source, string HelpFile, int HelpContext, DDActiveReports2.IReturnBool CancelDisplay)
		//{
		//	MessageBox.Show("Report Error " + FCConvert.ToString(Number) + " " + Description + "\r\n" + "Subsystem " + FCConvert.ToString(Scode) + "\r\n" + "Source " + Source, "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
		//}
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = (lngCurrRow > lngLastRow);
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			string strFileNameToUse = "";
			modGlobalVariables.Statics.gboolDonePPXferReport = true;
			if (boolSaveToFile)
			{
				// save to a file
				// check to see what files are already saved so we know what to name this one
				strFileNameToUse = modGlobalRoutines.DetermineFileNameFromMask_8("PPBLTR*.rdf", TWSharedLibrary.Variables.Statics.ReportPath);
				// now save it in the report directory
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, strFileNameToUse));
				modGlobalVariables.Statics.gboolDonePPXferReport = true;
				if (boolUnload)
				{
					this.Close();
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			modGlobalVariables.Statics.gboolDonePPXferReport = false;
			if (this.Visible == false && !boolFromAudit)
			{
				boolSaveToFile = true;
			}
			else
			{
				boolSaveToFile = boolSaveFile;
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			}
			if (frmPPAudit.InstancePtr.intWhereFrom == 4)
			{
				Label9.Text = "Personal Property Billing Transfer Report";
			}
			lngCurrRow = 1;
			lngLastRow = frmPPAudit.InstancePtr.AuditGrid.Rows - 1;
			txtDate.Text = DateTime.Today.ToShortDateString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
			{
				txtMuniname.Text = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				txtMuniname.Text = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
			}
			txtTotValue.Text = frmPPAudit.InstancePtr.TotalGrid.TextMatrix(frmPPAudit.InstancePtr.TotalGrid.Rows - 1, 2);
			txttotExempt.Text = frmPPAudit.InstancePtr.TotalGrid.TextMatrix(frmPPAudit.InstancePtr.TotalGrid.Rows - 1, 3);
			txtTotAssess.Text = frmPPAudit.InstancePtr.TotalGrid.TextMatrix(frmPPAudit.InstancePtr.TotalGrid.Rows - 1, 4);
			txtTotTax.Text = frmPPAudit.InstancePtr.TotalGrid.TextMatrix(frmPPAudit.InstancePtr.TotalGrid.Rows - 1, 5);
			txtCount.Text = frmPPAudit.InstancePtr.TotalGrid.TextMatrix(frmPPAudit.InstancePtr.TotalGrid.Rows - 1, 1);
			txtCat1.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(0, 0);
			txtCat2.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(1, 0);
			txtCat3.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(2, 0);
			txtCatTot1.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(0, 1);
			txtCatTot2.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(1, 1);
			txtCatTot3.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(2, 1);
			txtCat4.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(0, 2);
			txtCat5.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(1, 2);
			txtCat6.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(2, 2);
			txtCatTot4.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(0, 3);
			txtCatTot5.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(1, 3);
			txtCatTot6.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(2, 3);
			txtCat7.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(0, 4);
			txtCat8.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(1, 4);
			txtCat9.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(2, 4);
			txtCatTot7.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(0, 5);
			txtCatTot8.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(1, 5);
			txtCatTot9.Text = frmPPAudit.InstancePtr.CategoryGrid.TextMatrix(2, 5);
			txtCatBETE1.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(0, 0);
			txtCatBETE2.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(1, 0);
			txtCatBETE3.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(2, 0);
			txtCatTot1BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(0, 1);
			txtCatTot2BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(1, 1);
			txtCatTot3BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(2, 1);
			txtCatBETE4.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(0, 2);
			txtCatBETE5.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(1, 2);
			txtCatBETE6.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(2, 2);
			txtCatTot4BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(0, 3);
			txtCatTot5BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(1, 3);
			txtCatTot6BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(2, 3);
			txtCatBETE7.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(0, 4);
			txtCatBETE8.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(1, 4);
			txtCatBETE9.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(2, 4);
			txtCatTot7BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(0, 5);
			txtCatTot8BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(1, 5);
			txtCatTot9BETE.Text = frmPPAudit.InstancePtr.GridBETEExempt.TextMatrix(2, 5);
			intPage = 1;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// Unload frmPPAudit
			modGlobalVariables.Statics.gboolDonePPXferReport = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCurrRow <= lngLastRow)
			{
				txtAcct.Text = frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colAcct);
				txtName.Text = frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colName);
				if (Conversion.Val(frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colSnum)) > 0)
				{
					txtLocation.Text = frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colSnum) + " " + frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colLoc);
				}
				else
				{
					txtLocation.Text = frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colLoc);
				}
				txtValue.Text = frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colValue);
				txtExemption.Text = frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colExempt);
				txtAssess.Text = frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colAssessment);
				txtTax.Text = frmPPAudit.InstancePtr.AuditGrid.TextMatrix(lngCurrRow, colTax);
			}
			lngCurrRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		private void rptPPAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPPAudit properties;
			//rptPPAudit.Caption	= "Personal Property Audit Report";
			//rptPPAudit.Icon	= "rptPPAudit.dsx":0000";
			//rptPPAudit.Left	= 0;
			//rptPPAudit.Top	= 0;
			//rptPPAudit.Width	= 12840;
			//rptPPAudit.Height	= 12555;
			//rptPPAudit.StartUpPosition	= 3;
			//rptPPAudit.SectionData	= "rptPPAudit.dsx":058A;
			//End Unmaped Properties
		}
	}
}
