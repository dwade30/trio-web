﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptBillFormat8.
	/// </summary>
	partial class rptBillFormat8
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillFormat8));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.ReturnAddressShape = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.MainTextShape = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.MailingAddressShape = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtMailing1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmessage7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccountLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessmentLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLotLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayment2Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDiscountAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDiscountDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayment4Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDueDate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMessage8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrepaidLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrepaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPageLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDist5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistPerc5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDistAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailing5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmessage7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessmentLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLotLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment2Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscountAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscountDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment4Label)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrepaidLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrepaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.ReturnAddressShape,
				this.txtAddress1,
				this.txtAddress2,
				this.txtAddress3,
				this.txtAddress4,
				this.MainTextShape,
				this.MailingAddressShape,
				this.txtMailing1,
				this.txtMailing2,
				this.txtMailing3,
				this.txtMailing4,
				this.txtMessage1,
				this.txtMessage4,
				this.txtMessage5,
				this.txtMessage6,
				this.txtmessage7,
				this.txtMessage2,
				this.txtMessage3,
				this.lblTitle,
				this.txtAccountLabel,
				this.lblLand,
				this.txtBldgLabel,
				this.txtExemptLabel,
				this.txtAssessmentLabel,
				this.txtLand,
				this.txtBldg,
				this.txtExemption,
				this.txtAssessment,
				this.Field8,
				this.txtRate,
				this.Field9,
				this.txtTaxDue,
				this.txtAccount,
				this.txtLocLabel,
				this.txtLocation,
				this.txtMapLotLabel,
				this.txtMapLot,
				this.Field11,
				this.Field12,
				this.Field13,
				this.txtDueDate1,
				this.txtAmount1,
				this.txtPayment2Label,
				this.txtDueDate2,
				this.txtAmount2,
				this.txtDiscount,
				this.Field16,
				this.Field17,
				this.txtDiscountAmount,
				this.Field18,
				this.txtDiscountDate,
				this.Field19,
				this.txtDueDate3,
				this.txtAmount3,
				this.txtPayment4Label,
				this.txtDueDate4,
				this.txtAmount4,
				this.txtMessage8,
				this.txtPrepaidLabel,
				this.txtPrepaid,
				this.txtBookPageLabel,
				this.txtBookPage,
				this.Field20,
				this.txtDist1,
				this.txtDistPerc1,
				this.txtDist2,
				this.txtDist3,
				this.txtDist4,
				this.txtDist5,
				this.txtDistPerc2,
				this.txtDistPerc3,
				this.txtDistPerc4,
				this.txtDistPerc5,
				this.txtDistAmount1,
				this.txtDistAmount2,
				this.txtDistAmount3,
				this.txtDistAmount4,
				this.txtDistAmount5,
				this.txtMailing5
			});
			this.Detail.Height = 4.46875F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// ReturnAddressShape
			// 
			this.ReturnAddressShape.Height = 1.09375F;
			this.ReturnAddressShape.Left = 0.5F;
			this.ReturnAddressShape.Name = "ReturnAddressShape";
			this.ReturnAddressShape.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.ReturnAddressShape.Top = 0F;
			this.ReturnAddressShape.Visible = false;
			this.ReturnAddressShape.Width = 3.5F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.CanGrow = false;
			this.txtAddress1.Height = 0.19F;
			this.txtAddress1.Left = 0.5F;
			this.txtAddress1.MultiLine = false;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Tag = "textbox";
			this.txtAddress1.Text = "Field1";
			this.txtAddress1.Top = 0F;
			this.txtAddress1.Width = 3.4375F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.CanGrow = false;
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 0.5F;
			this.txtAddress2.MultiLine = false;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Tag = "textbox";
			this.txtAddress2.Text = "Field1";
			this.txtAddress2.Top = 0.15625F;
			this.txtAddress2.Width = 3.4375F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.CanGrow = false;
			this.txtAddress3.Height = 0.19F;
			this.txtAddress3.Left = 0.5F;
			this.txtAddress3.MultiLine = false;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Tag = "textbox";
			this.txtAddress3.Text = "Field1";
			this.txtAddress3.Top = 0.34375F;
			this.txtAddress3.Width = 3.4375F;
			// 
			// txtAddress4
			// 
			this.txtAddress4.CanGrow = false;
			this.txtAddress4.Height = 0.19F;
			this.txtAddress4.Left = 0.5F;
			this.txtAddress4.MultiLine = false;
			this.txtAddress4.Name = "txtAddress4";
			this.txtAddress4.Tag = "textbox";
			this.txtAddress4.Text = "Field1";
			this.txtAddress4.Top = 0.5F;
			this.txtAddress4.Width = 3.4375F;
			// 
			// MainTextShape
			// 
			this.MainTextShape.Height = 2.40625F;
			this.MainTextShape.Left = 0F;
			this.MainTextShape.Name = "MainTextShape";
			this.MainTextShape.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.MainTextShape.Top = 1.09375F;
			this.MainTextShape.Visible = false;
			this.MainTextShape.Width = 7.5F;
			// 
			// MailingAddressShape
			// 
			this.MailingAddressShape.Height = 0.90625F;
			this.MailingAddressShape.Left = 0.5F;
			this.MailingAddressShape.Name = "MailingAddressShape";
			this.MailingAddressShape.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.MailingAddressShape.Top = 3.5F;
			this.MailingAddressShape.Visible = false;
			this.MailingAddressShape.Width = 3.75F;
			// 
			// txtMailing1
			// 
			this.txtMailing1.CanGrow = false;
			this.txtMailing1.Height = 0.19F;
			this.txtMailing1.Left = 0.5F;
			this.txtMailing1.MultiLine = false;
			this.txtMailing1.Name = "txtMailing1";
			this.txtMailing1.Tag = "textbox";
			this.txtMailing1.Text = "Field1";
			this.txtMailing1.Top = 3.53125F;
			this.txtMailing1.Width = 3.625F;
			// 
			// txtMailing2
			// 
			this.txtMailing2.CanGrow = false;
			this.txtMailing2.Height = 0.19F;
			this.txtMailing2.Left = 0.5F;
			this.txtMailing2.MultiLine = false;
			this.txtMailing2.Name = "txtMailing2";
			this.txtMailing2.Tag = "textbox";
			this.txtMailing2.Text = "Field1";
			this.txtMailing2.Top = 3.6875F;
			this.txtMailing2.Width = 3.625F;
			// 
			// txtMailing3
			// 
			this.txtMailing3.CanGrow = false;
			this.txtMailing3.Height = 0.19F;
			this.txtMailing3.Left = 0.5F;
			this.txtMailing3.MultiLine = false;
			this.txtMailing3.Name = "txtMailing3";
			this.txtMailing3.Tag = "textbox";
			this.txtMailing3.Text = "Field1";
			this.txtMailing3.Top = 3.84375F;
			this.txtMailing3.Width = 3.625F;
			// 
			// txtMailing4
			// 
			this.txtMailing4.CanGrow = false;
			this.txtMailing4.Height = 0.19F;
			this.txtMailing4.Left = 0.5F;
			this.txtMailing4.MultiLine = false;
			this.txtMailing4.Name = "txtMailing4";
			this.txtMailing4.Tag = "textbox";
			this.txtMailing4.Text = "Field1";
			this.txtMailing4.Top = 4F;
			this.txtMailing4.Width = 3.625F;
			// 
			// txtMessage1
			// 
			this.txtMessage1.CanGrow = false;
			this.txtMessage1.Height = 0.19F;
			this.txtMessage1.Left = 2.625F;
			this.txtMessage1.MultiLine = false;
			this.txtMessage1.Name = "txtMessage1";
			this.txtMessage1.Tag = "textbox";
			this.txtMessage1.Text = " WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
			this.txtMessage1.Top = 2.03125F;
			this.txtMessage1.Width = 4.8125F;
			// 
			// txtMessage4
			// 
			this.txtMessage4.CanGrow = false;
			this.txtMessage4.Height = 0.19F;
			this.txtMessage4.Left = 2.625F;
			this.txtMessage4.MultiLine = false;
			this.txtMessage4.Name = "txtMessage4";
			this.txtMessage4.Tag = "textbox";
			this.txtMessage4.Text = " ";
			this.txtMessage4.Top = 2.5F;
			this.txtMessage4.Width = 4.8125F;
			// 
			// txtMessage5
			// 
			this.txtMessage5.CanGrow = false;
			this.txtMessage5.Height = 0.19F;
			this.txtMessage5.Left = 2.625F;
			this.txtMessage5.MultiLine = false;
			this.txtMessage5.Name = "txtMessage5";
			this.txtMessage5.Tag = "textbox";
			this.txtMessage5.Text = " ";
			this.txtMessage5.Top = 2.65625F;
			this.txtMessage5.Width = 4.8125F;
			// 
			// txtMessage6
			// 
			this.txtMessage6.CanGrow = false;
			this.txtMessage6.Height = 0.19F;
			this.txtMessage6.Left = 2.625F;
			this.txtMessage6.MultiLine = false;
			this.txtMessage6.Name = "txtMessage6";
			this.txtMessage6.Tag = "textbox";
			this.txtMessage6.Text = " ";
			this.txtMessage6.Top = 2.8125F;
			this.txtMessage6.Width = 4.8125F;
			// 
			// txtmessage7
			// 
			this.txtmessage7.CanGrow = false;
			this.txtmessage7.Height = 0.19F;
			this.txtmessage7.Left = 2.625F;
			this.txtmessage7.MultiLine = false;
			this.txtmessage7.Name = "txtmessage7";
			this.txtmessage7.Tag = "textbox";
			this.txtmessage7.Text = " ";
			this.txtmessage7.Top = 2.96875F;
			this.txtmessage7.Width = 4.8125F;
			// 
			// txtMessage2
			// 
			this.txtMessage2.CanGrow = false;
			this.txtMessage2.Height = 0.19F;
			this.txtMessage2.Left = 2.625F;
			this.txtMessage2.MultiLine = false;
			this.txtMessage2.Name = "txtMessage2";
			this.txtMessage2.Tag = "textbox";
			this.txtMessage2.Text = " ";
			this.txtMessage2.Top = 2.1875F;
			this.txtMessage2.Width = 4.8125F;
			// 
			// txtMessage3
			// 
			this.txtMessage3.CanGrow = false;
			this.txtMessage3.Height = 0.19F;
			this.txtMessage3.Left = 2.625F;
			this.txtMessage3.MultiLine = false;
			this.txtMessage3.Name = "txtMessage3";
			this.txtMessage3.Tag = "textbox";
			this.txtMessage3.Text = " ";
			this.txtMessage3.Top = 2.34375F;
			this.txtMessage3.Width = 4.8125F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1666667F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.75F;
			this.lblTitle.MultiLine = false;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "text-align: center";
			this.lblTitle.Tag = "textbox";
			this.lblTitle.Text = "R E A L  E S T A T E  T A X  B I L L  - ";
			this.lblTitle.Top = 1.09375F;
			this.lblTitle.Width = 4.875F;
			// 
			// txtAccountLabel
			// 
			this.txtAccountLabel.CanGrow = false;
			this.txtAccountLabel.Height = 0.19F;
			this.txtAccountLabel.Left = 0.0625F;
			this.txtAccountLabel.MultiLine = false;
			this.txtAccountLabel.Name = "txtAccountLabel";
			this.txtAccountLabel.Tag = "textbox";
			this.txtAccountLabel.Text = "Account";
			this.txtAccountLabel.Top = 1.25F;
			this.txtAccountLabel.Width = 0.9375F;
			// 
			// lblLand
			// 
			this.lblLand.CanGrow = false;
			this.lblLand.Height = 0.19F;
			this.lblLand.Left = 0.0625F;
			this.lblLand.MultiLine = false;
			this.lblLand.Name = "lblLand";
			this.lblLand.Tag = "textbox";
			this.lblLand.Text = "Land";
			this.lblLand.Top = 1.40625F;
			this.lblLand.Width = 0.9375F;
			// 
			// txtBldgLabel
			// 
			this.txtBldgLabel.CanGrow = false;
			this.txtBldgLabel.Height = 0.19F;
			this.txtBldgLabel.Left = 0.0625F;
			this.txtBldgLabel.MultiLine = false;
			this.txtBldgLabel.Name = "txtBldgLabel";
			this.txtBldgLabel.Tag = "textbox";
			this.txtBldgLabel.Text = "Buildings";
			this.txtBldgLabel.Top = 1.5625F;
			this.txtBldgLabel.Width = 0.9375F;
			// 
			// txtExemptLabel
			// 
			this.txtExemptLabel.CanGrow = false;
			this.txtExemptLabel.Height = 0.19F;
			this.txtExemptLabel.Left = 0.0625F;
			this.txtExemptLabel.MultiLine = false;
			this.txtExemptLabel.Name = "txtExemptLabel";
			this.txtExemptLabel.Tag = "textbox";
			this.txtExemptLabel.Text = "Exemption";
			this.txtExemptLabel.Top = 1.71875F;
			this.txtExemptLabel.Width = 0.9375F;
			// 
			// txtAssessmentLabel
			// 
			this.txtAssessmentLabel.CanGrow = false;
			this.txtAssessmentLabel.Height = 0.19F;
			this.txtAssessmentLabel.Left = 0.0625F;
			this.txtAssessmentLabel.MultiLine = false;
			this.txtAssessmentLabel.Name = "txtAssessmentLabel";
			this.txtAssessmentLabel.Tag = "textbox";
			this.txtAssessmentLabel.Text = "Assessment";
			this.txtAssessmentLabel.Top = 1.875F;
			this.txtAssessmentLabel.Width = 1F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 1F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Tag = "textbox";
			this.txtLand.Text = null;
			this.txtLand.Top = 1.40625F;
			this.txtLand.Width = 1.1875F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 1F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Tag = "textbox";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 1.5625F;
			this.txtBldg.Width = 1.1875F;
			// 
			// txtExemption
			// 
			this.txtExemption.CanGrow = false;
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 1F;
			this.txtExemption.MultiLine = false;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Tag = "textbox";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 1.71875F;
			this.txtExemption.Width = 1.1875F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.CanGrow = false;
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 1F;
			this.txtAssessment.MultiLine = false;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Tag = "textbox";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 1.875F;
			this.txtAssessment.Width = 1.1875F;
			// 
			// Field8
			// 
			this.Field8.CanGrow = false;
			this.Field8.Height = 0.19F;
			this.Field8.Left = 0.0625F;
			this.Field8.MultiLine = false;
			this.Field8.Name = "Field8";
			this.Field8.Tag = "textbox";
			this.Field8.Text = "Tax Rate";
			this.Field8.Top = 2.03125F;
			this.Field8.Width = 0.9375F;
			// 
			// txtRate
			// 
			this.txtRate.CanGrow = false;
			this.txtRate.Height = 0.19F;
			this.txtRate.Left = 1F;
			this.txtRate.MultiLine = false;
			this.txtRate.Name = "txtRate";
			this.txtRate.Style = "text-align: right";
			this.txtRate.Tag = "textbox";
			this.txtRate.Text = null;
			this.txtRate.Top = 2.03125F;
			this.txtRate.Width = 1.1875F;
			// 
			// Field9
			// 
			this.Field9.CanGrow = false;
			this.Field9.Height = 0.19F;
			this.Field9.Left = 0.0625F;
			this.Field9.MultiLine = false;
			this.Field9.Name = "Field9";
			this.Field9.Tag = "textbox";
			this.Field9.Text = "Tax Due";
			this.Field9.Top = 2.1875F;
			this.Field9.Width = 0.9375F;
			// 
			// txtTaxDue
			// 
			this.txtTaxDue.CanGrow = false;
			this.txtTaxDue.Height = 0.19F;
			this.txtTaxDue.Left = 1F;
			this.txtTaxDue.MultiLine = false;
			this.txtTaxDue.Name = "txtTaxDue";
			this.txtTaxDue.Style = "text-align: right";
			this.txtTaxDue.Tag = "textbox";
			this.txtTaxDue.Text = null;
			this.txtTaxDue.Top = 2.1875F;
			this.txtTaxDue.Width = 1.1875F;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 1F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Tag = "textbox";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 1.25F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtLocLabel
			// 
			this.txtLocLabel.CanGrow = false;
			this.txtLocLabel.Height = 0.19F;
			this.txtLocLabel.Left = 1.8125F;
			this.txtLocLabel.MultiLine = false;
			this.txtLocLabel.Name = "txtLocLabel";
			this.txtLocLabel.Tag = "textbox";
			this.txtLocLabel.Text = "Location";
			this.txtLocLabel.Top = 1.25F;
			this.txtLocLabel.Width = 0.875F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 2.75F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Tag = "textbox";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1.25F;
			this.txtLocation.Width = 2.0625F;
			// 
			// txtMapLotLabel
			// 
			this.txtMapLotLabel.CanGrow = false;
			this.txtMapLotLabel.Height = 0.19F;
			this.txtMapLotLabel.Left = 4.875F;
			this.txtMapLotLabel.MultiLine = false;
			this.txtMapLotLabel.Name = "txtMapLotLabel";
			this.txtMapLotLabel.Tag = "textbox";
			this.txtMapLotLabel.Text = "Map/Lot";
			this.txtMapLotLabel.Top = 1.25F;
			this.txtMapLotLabel.Width = 0.75F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 5.6875F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Tag = "textbox";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.25F;
			this.txtMapLot.Width = 1.8125F;
			// 
			// Field11
			// 
			this.Field11.CanGrow = false;
			this.Field11.Height = 0.19F;
			this.Field11.Left = 2.25F;
			this.Field11.MultiLine = false;
			this.Field11.Name = "Field11";
			this.Field11.Tag = "textbox";
			this.Field11.Text = "Due Date";
			this.Field11.Top = 1.71875F;
			this.Field11.Width = 0.8125F;
			// 
			// Field12
			// 
			this.Field12.CanGrow = false;
			this.Field12.Height = 0.19F;
			this.Field12.Left = 2.25F;
			this.Field12.MultiLine = false;
			this.Field12.Name = "Field12";
			this.Field12.Tag = "textbox";
			this.Field12.Text = "Amount";
			this.Field12.Top = 1.875F;
			this.Field12.Width = 0.75F;
			// 
			// Field13
			// 
			this.Field13.CanGrow = false;
			this.Field13.Height = 0.19F;
			this.Field13.Left = 3.0625F;
			this.Field13.MultiLine = false;
			this.Field13.Name = "Field13";
			this.Field13.Style = "text-align: right";
			this.Field13.Tag = "textbox";
			this.Field13.Text = "Payment 1";
			this.Field13.Top = 1.5625F;
			this.Field13.Width = 0.9375F;
			// 
			// txtDueDate1
			// 
			this.txtDueDate1.CanGrow = false;
			this.txtDueDate1.Height = 0.19F;
			this.txtDueDate1.Left = 3.0625F;
			this.txtDueDate1.MultiLine = false;
			this.txtDueDate1.Name = "txtDueDate1";
			this.txtDueDate1.Style = "text-align: right";
			this.txtDueDate1.Tag = "textbox";
			this.txtDueDate1.Text = null;
			this.txtDueDate1.Top = 1.71875F;
			this.txtDueDate1.Width = 0.9375F;
			// 
			// txtAmount1
			// 
			this.txtAmount1.CanGrow = false;
			this.txtAmount1.Height = 0.19F;
			this.txtAmount1.Left = 3F;
			this.txtAmount1.MultiLine = false;
			this.txtAmount1.Name = "txtAmount1";
			this.txtAmount1.Style = "text-align: right";
			this.txtAmount1.Tag = "textbox";
			this.txtAmount1.Text = null;
			this.txtAmount1.Top = 1.875F;
			this.txtAmount1.Width = 1F;
			// 
			// txtPayment2Label
			// 
			this.txtPayment2Label.CanGrow = false;
			this.txtPayment2Label.Height = 0.19F;
			this.txtPayment2Label.Left = 4.0625F;
			this.txtPayment2Label.MultiLine = false;
			this.txtPayment2Label.Name = "txtPayment2Label";
			this.txtPayment2Label.Style = "text-align: right";
			this.txtPayment2Label.Tag = "textbox";
			this.txtPayment2Label.Text = "Payment2";
			this.txtPayment2Label.Top = 1.5625F;
			this.txtPayment2Label.Visible = false;
			this.txtPayment2Label.Width = 0.875F;
			// 
			// txtDueDate2
			// 
			this.txtDueDate2.CanGrow = false;
			this.txtDueDate2.Height = 0.19F;
			this.txtDueDate2.Left = 4F;
			this.txtDueDate2.MultiLine = false;
			this.txtDueDate2.Name = "txtDueDate2";
			this.txtDueDate2.Style = "text-align: right";
			this.txtDueDate2.Tag = "textbox";
			this.txtDueDate2.Text = null;
			this.txtDueDate2.Top = 1.71875F;
			this.txtDueDate2.Visible = false;
			this.txtDueDate2.Width = 0.9375F;
			// 
			// txtAmount2
			// 
			this.txtAmount2.CanGrow = false;
			this.txtAmount2.Height = 0.19F;
			this.txtAmount2.Left = 4F;
			this.txtAmount2.MultiLine = false;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Style = "text-align: right";
			this.txtAmount2.Tag = "textbox";
			this.txtAmount2.Text = null;
			this.txtAmount2.Top = 1.875F;
			this.txtAmount2.Visible = false;
			this.txtAmount2.Width = 0.9375F;
			// 
			// txtDiscount
			// 
			this.txtDiscount.CanGrow = false;
			this.txtDiscount.Height = 0.19F;
			this.txtDiscount.Left = 4.9375F;
			this.txtDiscount.Name = "txtDiscount";
			this.txtDiscount.Tag = "textbox";
			this.txtDiscount.Text = "00.00%";
			this.txtDiscount.Top = 1.5625F;
			this.txtDiscount.Width = 0.6875F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.19F;
			this.Field16.Left = 5.625F;
			this.Field16.Name = "Field16";
			this.Field16.Tag = "textbox";
			this.Field16.Text = "% discount available.";
			this.Field16.Top = 1.5625F;
			this.Field16.Width = 1.9375F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.19F;
			this.Field17.Left = 4.9375F;
			this.Field17.Name = "Field17";
			this.Field17.Tag = "textbox";
			this.Field17.Text = "To obtain, pay";
			this.Field17.Top = 1.71875F;
			this.Field17.Width = 1.4375F;
			// 
			// txtDiscountAmount
			// 
			this.txtDiscountAmount.Height = 0.19F;
			this.txtDiscountAmount.Left = 6.375F;
			this.txtDiscountAmount.MultiLine = false;
			this.txtDiscountAmount.Name = "txtDiscountAmount";
			this.txtDiscountAmount.Tag = "textbox";
			this.txtDiscountAmount.Text = "000,000.00";
			this.txtDiscountAmount.Top = 1.71875F;
			this.txtDiscountAmount.Width = 1.1875F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.19F;
			this.Field18.Left = 4.9375F;
			this.Field18.Name = "Field18";
			this.Field18.Tag = "textbox";
			this.Field18.Text = "in full by";
			this.Field18.Top = 1.875F;
			this.Field18.Width = 1.125F;
			// 
			// txtDiscountDate
			// 
			this.txtDiscountDate.Height = 0.19F;
			this.txtDiscountDate.Left = 6.0625F;
			this.txtDiscountDate.Name = "txtDiscountDate";
			this.txtDiscountDate.Tag = "textbox";
			this.txtDiscountDate.Text = "00/00/00.";
			this.txtDiscountDate.Top = 1.875F;
			this.txtDiscountDate.Width = 1.3125F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.19F;
			this.Field19.Left = 5F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "text-align: right";
			this.Field19.Tag = "textbox";
			this.Field19.Text = "Payment 3";
			this.Field19.Top = 1.5625F;
			this.Field19.Width = 0.875F;
			// 
			// txtDueDate3
			// 
			this.txtDueDate3.Height = 0.19F;
			this.txtDueDate3.Left = 5F;
			this.txtDueDate3.Name = "txtDueDate3";
			this.txtDueDate3.Style = "text-align: right";
			this.txtDueDate3.Tag = "textbox";
			this.txtDueDate3.Text = null;
			this.txtDueDate3.Top = 1.71875F;
			this.txtDueDate3.Width = 0.875F;
			// 
			// txtAmount3
			// 
			this.txtAmount3.Height = 0.19F;
			this.txtAmount3.Left = 5F;
			this.txtAmount3.Name = "txtAmount3";
			this.txtAmount3.Style = "text-align: right";
			this.txtAmount3.Tag = "textbox";
			this.txtAmount3.Text = null;
			this.txtAmount3.Top = 1.875F;
			this.txtAmount3.Width = 0.875F;
			// 
			// txtPayment4Label
			// 
			this.txtPayment4Label.Height = 0.19F;
			this.txtPayment4Label.Left = 5.9375F;
			this.txtPayment4Label.Name = "txtPayment4Label";
			this.txtPayment4Label.Style = "text-align: right";
			this.txtPayment4Label.Tag = "textbox";
			this.txtPayment4Label.Text = "Payment 4";
			this.txtPayment4Label.Top = 1.5625F;
			this.txtPayment4Label.Width = 0.875F;
			// 
			// txtDueDate4
			// 
			this.txtDueDate4.Height = 0.19F;
			this.txtDueDate4.Left = 5.9375F;
			this.txtDueDate4.MultiLine = false;
			this.txtDueDate4.Name = "txtDueDate4";
			this.txtDueDate4.Style = "text-align: right";
			this.txtDueDate4.Tag = "textbox";
			this.txtDueDate4.Text = null;
			this.txtDueDate4.Top = 1.71875F;
			this.txtDueDate4.Visible = false;
			this.txtDueDate4.Width = 0.875F;
			// 
			// txtAmount4
			// 
			this.txtAmount4.Height = 0.19F;
			this.txtAmount4.Left = 5.9375F;
			this.txtAmount4.Name = "txtAmount4";
			this.txtAmount4.Style = "text-align: right";
			this.txtAmount4.Tag = "textbox";
			this.txtAmount4.Text = null;
			this.txtAmount4.Top = 1.875F;
			this.txtAmount4.Visible = false;
			this.txtAmount4.Width = 0.875F;
			// 
			// txtMessage8
			// 
			this.txtMessage8.Height = 0.19F;
			this.txtMessage8.Left = 2.625F;
			this.txtMessage8.Name = "txtMessage8";
			this.txtMessage8.Tag = "textbox";
			this.txtMessage8.Text = " ";
			this.txtMessage8.Top = 3.125F;
			this.txtMessage8.Width = 4.8125F;
			// 
			// txtPrepaidLabel
			// 
			this.txtPrepaidLabel.CanGrow = false;
			this.txtPrepaidLabel.Height = 0.19F;
			this.txtPrepaidLabel.Left = 4.875F;
			this.txtPrepaidLabel.Name = "txtPrepaidLabel";
			this.txtPrepaidLabel.Tag = "textbox";
			this.txtPrepaidLabel.Text = "Paid to date:";
			this.txtPrepaidLabel.Top = 1.40625F;
			this.txtPrepaidLabel.Width = 1.3125F;
			// 
			// txtPrepaid
			// 
			this.txtPrepaid.CanGrow = false;
			this.txtPrepaid.Height = 0.19F;
			this.txtPrepaid.Left = 6.25F;
			this.txtPrepaid.Name = "txtPrepaid";
			this.txtPrepaid.Tag = "textbox";
			this.txtPrepaid.Text = null;
			this.txtPrepaid.Top = 1.40625F;
			this.txtPrepaid.Width = 1.25F;
			// 
			// txtBookPageLabel
			// 
			this.txtBookPageLabel.CanGrow = false;
			this.txtBookPageLabel.Height = 0.19F;
			this.txtBookPageLabel.Left = 2.25F;
			this.txtBookPageLabel.Name = "txtBookPageLabel";
			this.txtBookPageLabel.Tag = "textbox";
			this.txtBookPageLabel.Text = "Book&Page";
			this.txtBookPageLabel.Top = 1.40625F;
			this.txtBookPageLabel.Width = 0.875F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.CanGrow = false;
			this.txtBookPage.Height = 0.19F;
			this.txtBookPage.Left = 3.25F;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Tag = "textbox";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.40625F;
			this.txtBookPage.Width = 1.5625F;
			// 
			// Field20
			// 
			this.Field20.CanGrow = false;
			this.Field20.Height = 0.19F;
			this.Field20.Left = 0.1875F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "text-align: center";
			this.Field20.Tag = "textbox";
			this.Field20.Text = "Tax Breakdown";
			this.Field20.Top = 2.34375F;
			this.Field20.Width = 2.125F;
			// 
			// txtDist1
			// 
			this.txtDist1.CanGrow = false;
			this.txtDist1.Height = 0.19F;
			this.txtDist1.Left = 0.0625F;
			this.txtDist1.MultiLine = false;
			this.txtDist1.Name = "txtDist1";
			this.txtDist1.Tag = "textbox";
			this.txtDist1.Text = null;
			this.txtDist1.Top = 2.5F;
			this.txtDist1.Width = 1F;
			// 
			// txtDistPerc1
			// 
			this.txtDistPerc1.CanGrow = false;
			this.txtDistPerc1.Height = 0.19F;
			this.txtDistPerc1.Left = 1.0625F;
			this.txtDistPerc1.MultiLine = false;
			this.txtDistPerc1.Name = "txtDistPerc1";
			this.txtDistPerc1.Style = "text-align: right";
			this.txtDistPerc1.Tag = "textbox";
			this.txtDistPerc1.Text = null;
			this.txtDistPerc1.Top = 2.5F;
			this.txtDistPerc1.Width = 0.6875F;
			// 
			// txtDist2
			// 
			this.txtDist2.CanGrow = false;
			this.txtDist2.Height = 0.19F;
			this.txtDist2.Left = 0.0625F;
			this.txtDist2.MultiLine = false;
			this.txtDist2.Name = "txtDist2";
			this.txtDist2.Tag = "textbox";
			this.txtDist2.Text = " ";
			this.txtDist2.Top = 2.65625F;
			this.txtDist2.Visible = false;
			this.txtDist2.Width = 1F;
			// 
			// txtDist3
			// 
			this.txtDist3.CanGrow = false;
			this.txtDist3.Height = 0.19F;
			this.txtDist3.Left = 0.0625F;
			this.txtDist3.MultiLine = false;
			this.txtDist3.Name = "txtDist3";
			this.txtDist3.Tag = "textbox";
			this.txtDist3.Text = " ";
			this.txtDist3.Top = 2.8125F;
			this.txtDist3.Visible = false;
			this.txtDist3.Width = 1F;
			// 
			// txtDist4
			// 
			this.txtDist4.CanGrow = false;
			this.txtDist4.Height = 0.19F;
			this.txtDist4.Left = 0.0625F;
			this.txtDist4.MultiLine = false;
			this.txtDist4.Name = "txtDist4";
			this.txtDist4.Tag = "textbox";
			this.txtDist4.Text = " ";
			this.txtDist4.Top = 2.96875F;
			this.txtDist4.Visible = false;
			this.txtDist4.Width = 1F;
			// 
			// txtDist5
			// 
			this.txtDist5.CanGrow = false;
			this.txtDist5.Height = 0.19F;
			this.txtDist5.Left = 0.0625F;
			this.txtDist5.MultiLine = false;
			this.txtDist5.Name = "txtDist5";
			this.txtDist5.Tag = "textbox";
			this.txtDist5.Text = " ";
			this.txtDist5.Top = 3.125F;
			this.txtDist5.Visible = false;
			this.txtDist5.Width = 1F;
			// 
			// txtDistPerc2
			// 
			this.txtDistPerc2.CanGrow = false;
			this.txtDistPerc2.Height = 0.19F;
			this.txtDistPerc2.Left = 1.0625F;
			this.txtDistPerc2.MultiLine = false;
			this.txtDistPerc2.Name = "txtDistPerc2";
			this.txtDistPerc2.Style = "text-align: right";
			this.txtDistPerc2.Tag = "textbox";
			this.txtDistPerc2.Text = " ";
			this.txtDistPerc2.Top = 2.65625F;
			this.txtDistPerc2.Visible = false;
			this.txtDistPerc2.Width = 0.6875F;
			// 
			// txtDistPerc3
			// 
			this.txtDistPerc3.CanGrow = false;
			this.txtDistPerc3.Height = 0.19F;
			this.txtDistPerc3.Left = 1.0625F;
			this.txtDistPerc3.MultiLine = false;
			this.txtDistPerc3.Name = "txtDistPerc3";
			this.txtDistPerc3.Style = "text-align: right";
			this.txtDistPerc3.Tag = "textbox";
			this.txtDistPerc3.Text = " ";
			this.txtDistPerc3.Top = 2.8125F;
			this.txtDistPerc3.Visible = false;
			this.txtDistPerc3.Width = 0.6875F;
			// 
			// txtDistPerc4
			// 
			this.txtDistPerc4.CanGrow = false;
			this.txtDistPerc4.Height = 0.19F;
			this.txtDistPerc4.Left = 1.0625F;
			this.txtDistPerc4.MultiLine = false;
			this.txtDistPerc4.Name = "txtDistPerc4";
			this.txtDistPerc4.Style = "text-align: right";
			this.txtDistPerc4.Tag = "textbox";
			this.txtDistPerc4.Text = " ";
			this.txtDistPerc4.Top = 2.96875F;
			this.txtDistPerc4.Visible = false;
			this.txtDistPerc4.Width = 0.6875F;
			// 
			// txtDistPerc5
			// 
			this.txtDistPerc5.CanGrow = false;
			this.txtDistPerc5.Height = 0.19F;
			this.txtDistPerc5.Left = 1.0625F;
			this.txtDistPerc5.MultiLine = false;
			this.txtDistPerc5.Name = "txtDistPerc5";
			this.txtDistPerc5.Style = "text-align: right";
			this.txtDistPerc5.Tag = "textbox";
			this.txtDistPerc5.Text = " ";
			this.txtDistPerc5.Top = 3.125F;
			this.txtDistPerc5.Visible = false;
			this.txtDistPerc5.Width = 0.6875F;
			// 
			// txtDistAmount1
			// 
			this.txtDistAmount1.CanGrow = false;
			this.txtDistAmount1.Height = 0.19F;
			this.txtDistAmount1.Left = 1.75F;
			this.txtDistAmount1.MultiLine = false;
			this.txtDistAmount1.Name = "txtDistAmount1";
			this.txtDistAmount1.Style = "text-align: right";
			this.txtDistAmount1.Tag = "textbox";
			this.txtDistAmount1.Text = null;
			this.txtDistAmount1.Top = 2.5F;
			this.txtDistAmount1.Visible = false;
			this.txtDistAmount1.Width = 0.875F;
			// 
			// txtDistAmount2
			// 
			this.txtDistAmount2.CanGrow = false;
			this.txtDistAmount2.Height = 0.19F;
			this.txtDistAmount2.Left = 1.75F;
			this.txtDistAmount2.MultiLine = false;
			this.txtDistAmount2.Name = "txtDistAmount2";
			this.txtDistAmount2.Style = "text-align: right";
			this.txtDistAmount2.Tag = "textbox";
			this.txtDistAmount2.Text = " ";
			this.txtDistAmount2.Top = 2.65625F;
			this.txtDistAmount2.Visible = false;
			this.txtDistAmount2.Width = 0.875F;
			// 
			// txtDistAmount3
			// 
			this.txtDistAmount3.CanGrow = false;
			this.txtDistAmount3.Height = 0.19F;
			this.txtDistAmount3.Left = 1.75F;
			this.txtDistAmount3.MultiLine = false;
			this.txtDistAmount3.Name = "txtDistAmount3";
			this.txtDistAmount3.Style = "text-align: right";
			this.txtDistAmount3.Tag = "textbox";
			this.txtDistAmount3.Text = " ";
			this.txtDistAmount3.Top = 2.8125F;
			this.txtDistAmount3.Visible = false;
			this.txtDistAmount3.Width = 0.875F;
			// 
			// txtDistAmount4
			// 
			this.txtDistAmount4.CanGrow = false;
			this.txtDistAmount4.Height = 0.19F;
			this.txtDistAmount4.Left = 1.75F;
			this.txtDistAmount4.MultiLine = false;
			this.txtDistAmount4.Name = "txtDistAmount4";
			this.txtDistAmount4.Style = "text-align: right";
			this.txtDistAmount4.Tag = "textbox";
			this.txtDistAmount4.Text = " ";
			this.txtDistAmount4.Top = 2.96875F;
			this.txtDistAmount4.Visible = false;
			this.txtDistAmount4.Width = 0.875F;
			// 
			// txtDistAmount5
			// 
			this.txtDistAmount5.CanGrow = false;
			this.txtDistAmount5.Height = 0.19F;
			this.txtDistAmount5.Left = 1.75F;
			this.txtDistAmount5.MultiLine = false;
			this.txtDistAmount5.Name = "txtDistAmount5";
			this.txtDistAmount5.Style = "text-align: right";
			this.txtDistAmount5.Tag = "textbox";
			this.txtDistAmount5.Text = " ";
			this.txtDistAmount5.Top = 3.125F;
			this.txtDistAmount5.Visible = false;
			this.txtDistAmount5.Width = 0.875F;
			// 
			// txtMailing5
			// 
			this.txtMailing5.CanGrow = false;
			this.txtMailing5.Height = 0.19F;
			this.txtMailing5.Left = 0.5F;
			this.txtMailing5.MultiLine = false;
			this.txtMailing5.Name = "txtMailing5";
			this.txtMailing5.Tag = "textbox";
			this.txtMailing5.Text = "Field1";
			this.txtMailing5.Top = 4.15625F;
			this.txtMailing5.Width = 3.625F;
			// 
			// rptBillFormat8
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmessage7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessmentLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLotLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment2Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscountAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDiscountDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayment4Label)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDueDate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrepaidLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrepaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPageLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDist5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistPerc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDistAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailing5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape ReturnAddressShape;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape MainTextShape;
		private GrapeCity.ActiveReports.SectionReportModel.Shape MailingAddressShape;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmessage7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccountLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessmentLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLotLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayment2Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscountAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDiscountDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayment4Label;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDueDate4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrepaidLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrepaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPageLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDist5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistPerc5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDistAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailing5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
