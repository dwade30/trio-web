﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmBillViewer.
	/// </summary>
	public partial class frmBillViewer : FCForm
	{
		public frmBillViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillViewer InstancePtr
		{
			get
			{
				return (frmBillViewer)Sys.GetInstance(typeof(frmBillViewer));
			}
		}

		protected frmBillViewer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsBillFormat clsTaxFormat = new clsBillFormat();
		int lngYear;
		string strFont;
		string strBillSQL;
		int intLimit;
		clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		string strPrinterToUse;
		bool boolShownModally;
		string strEmailAttachmentName;

		private void frmBillViewer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBillViewer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBillViewer properties;
			//frmBillViewer.ScaleWidth	= 9225;
			//frmBillViewer.ScaleHeight	= 7860;
			//frmBillViewer.LinkTopic	= "Form1";
			//frmBillViewer.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (!(ARViewer21.ReportSource == null))
			{
				ARViewer21.ReportSource.Dispose();
			}
			ARViewer21.ReportSource = null;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
		// vbPorter upgrade warning: intYear As short	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intPreviewLimit As short	OnWriteFCConvert.ToInt32(
		public void Init(ref clsBillFormat clsTaxBillFormat, short intYear, ref string strFontName, ref int intPreviewLimit, string strPrinterN = "", bool boolShowModal = false, bool boolDefaultMargin = false)
		{
			boolShownModally = boolShowModal;
			strPrinterToUse = strPrinterN;
			clsTaxFormat.CopyFormat(ref clsTaxBillFormat);
			strBillSQL = clsTaxFormat.SQLStatement;
			lngYear = intYear;
			strFont = strFontName;
			intLimit = intPreviewLimit;
			GetReport_2(FCConvert.ToInt16(intLimit));
			strEmailAttachmentName = FCConvert.ToString(intYear) + "TaxBills";
			if (boolShowModal)
			{
				this.Show(FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}
		// vbPorter upgrade warning: intPreviewLimit As short	OnWriteFCConvert.ToInt32(
		private void GetReport_2(short intPreviewLimit)
		{
			GetReport(ref intPreviewLimit);
		}

		private void GetReport(ref short intPreviewLimit)
		{
			bool boolShow = false;
			if (intPreviewLimit > 0)
			{
				boolShow = true;
				// clsTaxFormat.SQLStatement = "select top " & intPreviewLimit & " * from (" & strBillSQL & ")"
				clsTaxFormat.SQLStatement = "select top " + FCConvert.ToString(intPreviewLimit) + Strings.Right(strBillSQL, strBillSQL.Length - 6);
			}
			else
			{
				boolShow = false;
				clsTaxFormat.SQLStatement = strBillSQL;
			}
			if (boolShow)
			{
				switch (clsTaxFormat.Billformat)
				{
					//case 1:
					//case 4:
					//	{
					//		ARViewer21.ReportSource = rptBillFormat1_4.InstancePtr;
					//		rptBillFormat1_4.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 2:
					//case 3:
					//	{
					//		ARViewer21.ReportSource = rptBillFormat2_3.InstancePtr;
					//		rptBillFormat2_3.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 5:
					//case 6:
					//	{
					//		ARViewer21.ReportSource = rptBillFormat5_6.InstancePtr;
					//		rptBillFormat5_6.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 7:
					//	{
					//		ARViewer21.ReportSource = rptBillFormat7.InstancePtr;
					//		rptBillFormat7.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 8:
					//	{
					//		ARViewer21.ReportSource = rptBillFormat8.InstancePtr;
					//		rptBillFormat8.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 9:
					//case 10:
					//	{
					//		ARViewer21.ReportSource = rptBillFormatA.InstancePtr;
					//		rptBillFormatA.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 11:
					//	{
					//		// C
					//		ARViewer21.ReportSource = rptBillFormatC.InstancePtr;
					//		rptBillFormatC.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 12:
					//	{
					//		// D
					//		ARViewer21.ReportSource = rptBillFormatD.InstancePtr;
					//		rptBillFormatD.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 13:
					//	{
					//		// F
					//		ARViewer21.ReportSource = rptBillFormatF.InstancePtr;
					//		rptBillFormatF.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					case 14:
						{
							// G
							ARViewer21.ReportSource = rptBillFormatG.InstancePtr;
							rptBillFormatG.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
							break;
						}
					case 15:
						{
							ARViewer21.ReportSource = rptBillFormatG.InstancePtr;
							rptBillFormatG.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
							break;
						}
					case 16:
						{
							ARViewer21.ReportSource = rptBillFormatI.InstancePtr;
							rptBillFormatI.InstancePtr.Init(clsTaxFormat, lngYear, strFont, true, strPrinterToUse, boolShownModally);
							break;
						}
					case 17:
						{
							ARViewer21.ReportSource = rptCustomBill.InstancePtr;
							clsBLCustomBill clsBill = new clsBLCustomBill();
							clsBill.SQL = clsTaxFormat.SQLStatement;
							clsBill.AssignBillFormat(ref clsTaxFormat);
							clsBill.UsePrinterFonts = true;
							clsBill.FormatID = clsTaxFormat.CustomBillID;
							clsBill.DataDBFile = "Twcl0000.vb1";
							clsBill.DBFile = "twbl0000.vb1";
							clsBill.PrinterName = strPrinterToUse;
							rptCustomBill.InstancePtr.Init(clsBill, true, true);
							break;
						}
				}
				//end switch
				if (clsTaxFormat.Billformat != 17)
				{
					//FC:FINAL:AM: move code to switch
					//ARViewer21.ReportSource.Init(clsTaxFormat, lngYear, strFont, , strPrinterToUse, boolShownModally);
				}
			}
			else
			{
				ARViewer21.ReportSource.Close();
				ARViewer21.ReportSource = null;
				if (clsTaxFormat.Billformat < 14)
				{
					if (modPrinterFunctions.PrintXsForAlignment("The top of the X should align with the top of the bill", 40, 1, strPrinterToUse) == DialogResult.No)
					{
						return;
					}
				}
				switch (clsTaxFormat.Billformat)
				{
					//case 1:
					//case 4:
					//	{
					//		rptBillFormat1_4.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 2:
					//case 3:
					//	{
					//		rptBillFormat2_3.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 5:
					//case 6:
					//	{
					//		rptBillFormat5_6.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 7:
					//	{
					//		rptBillFormat7.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 8:
					//	{
					//		rptBillFormat8.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 9:
					//case 10:
					//	{
					//		rptBillFormatA.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 11:
					//	{
					//		// C
					//		rptBillFormatC.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 12:
					//	{
					//		// D
					//		rptBillFormatD.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					//case 13:
					//	{
					//		// F
					//		rptBillFormatF.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
					//		break;
					//	}
					case 14:
						{
							// G
							rptBillFormatG.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
							break;
						}
					case 15:
						{
							rptBillFormatG.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
							break;
						}
					case 16:
						{
							rptBillFormatI.InstancePtr.Init(clsTaxFormat, lngYear, strFont, false, strPrinterToUse, boolShownModally);
							break;
						}
					case 17:
						{
							clsBLCustomBill clsBill2 = new clsBLCustomBill();
							clsDRWrapper clsTemp = new clsDRWrapper();
							clsBill2.SQL = clsTaxFormat.SQLStatement;
							clsBill2.AssignBillFormat(ref clsTaxFormat);
							clsBill2.UsePrinterFonts = true;
							clsBill2.FormatID = clsTaxFormat.CustomBillID;
							clsBill2.DataDBFile = "Twcl0000.vb1";
							clsBill2.PrinterName = strPrinterToUse;
							clsTemp.OpenRecordset("select * from custombills where ID = " + FCConvert.ToString(clsBill2.FormatID), "Twbl0000.vb1");
							if (!clsTemp.EndOfFile())
							{
								if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("islaser")))
								{
									clsBill2.DotMatrixFormat = false;
								}
								else
								{
									clsBill2.DotMatrixFormat = true;
								}
								if (clsBill2.DotMatrixFormat)
								{
									if (modPrinterFunctions.PrintXsForAlignment("The top of the X should align with the top of the bill", 40, 1, strPrinterToUse) == DialogResult.No)
									{
										return;
									}
								}
							}
							//FC:TODO:AM: report source not set
							//ARViewer21.ReportSource.Init(clsBill2, false);
							break;
						}
				}
				//end switch
				Close();
			}
			// ARViewer21.Zoom = -1
		}

		private void mnuExportPDF_Click(object sender, System.EventArgs e)
		{
			try
            {
                string fileName = "";
                FCSectionReport report = null;
                GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                if (!(ARViewer21.ReportSource == null))
                {
                    report = ARViewer21.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".pdf";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(ARViewer21.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".pdf";
                }              
				using (MemoryStream stream = new MemoryStream())
				{
					a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In PDF export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuPrintAll_Click(object sender, System.EventArgs e)
		{
			GetReport_2(0);
		}

		private void mnuPrintPreviewed_Click(object sender, System.EventArgs e)
		{
			if (clsTaxFormat.Billformat < 14)
			{
				if (modPrinterFunctions.PrintXsForAlignment("The top of the X should align with the top of the bill", 40, 1, strPrinterToUse) == DialogResult.No)
				{
					return;
				}
			}
			else if (clsTaxFormat.Billformat == 17)
			{
				// custom
				clsDRWrapper clsTemp = new clsDRWrapper();
				clsTemp.OpenRecordset("select * from custombills where ID = " + FCConvert.ToString(clsTaxFormat.CustomBillID), "TWBL0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					if (!FCConvert.ToBoolean((clsTemp.Get_Fields_Boolean("islaser"))))
					{
						if (modPrinterFunctions.PrintXsForAlignment("The top of the X should align with the top of the bill", 40, 1, strPrinterToUse) == DialogResult.No)
						{
							return;
						}
					}
				}
			}
			clsPrint.ReportPrint(ARViewer21.ReportSource);
		}

		private void mnuRTF_Click(object sender, System.EventArgs e)
		{
			try
			{
				//string strOldDir;
				////FileSystemObject fso = new FileSystemObject();
				//Scripting.File fl;
				//if (ARViewer21.ReportSource.State==GrapeCity.ActiveReports.SectionReport.ReportState.InProgress) {
				//	MessageBox.Show("Report not done", "Report Loading", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//	return;
				//}
				//strOldDir = FCFileSystem.CurDir();
				//Information.Err().Clear();
				//// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				// /*? On Error Resume Next  */
				//MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.rtf";
				//MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "rtf";
				//MDIParent.InstancePtr.CommonDialog1_Save.InitDir = Application.StartupPath;
				//MDIParent.InstancePtr.CommonDialog1_Save.ShowSave();
				//if (Information.Err().Number==0) {
				//	ActiveReportsRTFExport.ARExportRTF a = new ActiveReportsRTFExport.ARExportRTF();
				//	a = new ActiveReportsRTFExport.ARExportRTF();
				//	a.FileName = MDIParent.InstancePtr.CommonDialog1_Save.FileName;
				//	a.Export(ARViewer21.Pages);
				//	if (!File.Exists(a.FileName)) {
				//		// If Left(UCase(fso.GetDriveName(a.FileName)), 1) = "A" Then
				//		MessageBox.Show("No file created"+"\r\n"+"Export not successful"+"\r\n"+"Check to make sure that the directory is not write protected and that there is enough free space to store the file", "Not Exported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//		// Else
				//		// MsgBox "No file created" & vbNewLine & "Export not successful", vbExclamation, "Not Exported"
				//		// End If
				//	} else {
				//		fl = fso.GetFile(a.FileName);
				//		if (FCConvert.ToInt32(fl.Size>0) {
				//			MessageBox.Show("Export to file  "+MDIParent.InstancePtr.CommonDialog1_Save.FileName+"  was completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//		} else {
				//			MessageBox.Show("Empty file created"+"\r\n"+"Export not successful"+"\r\n"+"Check to make sure that there is enough free space to create the file", "Not Exported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//		}
				//	}
				//} else {
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				//ChDrive(strOldDir);
				//FCFileSystem.CurDir() = strOldDir;
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				string fileName = ARViewer21.ReportSource.Name + ".rtf";
				using (MemoryStream stream = new MemoryStream())
				{
					a.Export(ARViewer21.ReportSource.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In RTF export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuEmailPDF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				//ActiveReportsPDFExport.ARExportPDF a = new ActiveReportsPDFExport.ARExportPDF();
				//a = new ActiveReportsPDFExport.ARExportPDF();
				////FileSystemObject fso = new FileSystemObject();
				//string strList = "";
				//a.FileName = FCFileSystem.CurDir()+"\\rpt\\"+fso.GetTempName();
				//a.Export(ARViewer21.Pages);
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				string strList = "";
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
				string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Document.Name + ".pdf";
				a.Export(ARViewer21.ReportSource.Document, fileName);
				if (!File.Exists(fileName))
				{
					MessageBox.Show("Could not create report file to send as attachment", "Cannot E-mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (Strings.Trim(strEmailAttachmentName) != string.Empty)
					{
						strList = fileName + ";" + strEmailAttachmentName + ".pdf";
					}
					else
					{
						strList = fileName;
					}
					frmEMail.InstancePtr.Init(strList, "", "", "", false, false, true, showAsModalForm: true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailPDF_Click");
			}
		}

		private void mnuEmailRTF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				string strList = "";
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
				string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Document.Name + ".rtf";
				a.Export(ARViewer21.ReportSource.Document, fileName);
				if (!File.Exists(fileName))
				{
					MessageBox.Show("Could not create report file to send as attachment", "Cannot E-mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (Strings.Trim(strEmailAttachmentName) != string.Empty)
					{
						strList = fileName + ";" + strEmailAttachmentName + ".rtf";
					}
					else
					{
						strList = fileName;
					}
					frmEMail.InstancePtr.Init(strList, "", "", "", false, false, true, showAsModalForm: true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailRTF_Click");
			}
		}

		private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
		{
			if (e.Button == toolBarButtonEmailPDF)
			{
				this.mnuEmailPDF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonEmailRTF)
			{
				this.mnuEmailRTF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportPDF)
			{
				this.mnuExportPDF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportRTF)
			{
				this.mnuRTF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonPrintAll)
			{
				this.mnuPrintAll_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonPrintPreview)
			{
				this.mnuPrintPreviewed_Click(e.Button, EventArgs.Empty);
			}
		}
	}
}
