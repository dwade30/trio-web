﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmChooseImage.
	/// </summary>
	partial class frmChooseImage : BaseForm
	{
		public fecherFoundation.FCComboBox cmbShow;
		public fecherFoundation.FCLabel lblShow;
		public fecherFoundation.FCCheckBox chkUseSeal;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChooseImage));
			this.cmbShow = new fecherFoundation.FCComboBox();
			this.lblShow = new fecherFoundation.FCLabel();
			this.chkUseSeal = new fecherFoundation.FCCheckBox();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveAndExit = new fecherFoundation.FCButton();
			this.upload1 = new Wisej.Web.Upload();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkUseSeal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAndExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 557);
			this.BottomPanel.Size = new System.Drawing.Size(1049, 109);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkUseSeal);
			this.ClientArea.Controls.Add(this.cmbShow);
			this.ClientArea.Controls.Add(this.lblShow);
			this.ClientArea.Controls.Add(this.Image1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.upload1);
			this.ClientArea.Size = new System.Drawing.Size(1049, 497);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1049, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(260, 30);
			this.HeaderText.Text = "Choose Image for Bills";
			// 
			// cmbShow
			// 
			this.cmbShow.AutoSize = false;
			this.cmbShow.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbShow.FormattingEnabled = true;
			this.cmbShow.Items.AddRange(new object[] {
				"Use Image",
				"Don\'t use Image"
			});
			this.cmbShow.Location = new System.Drawing.Point(147, 70);
			this.cmbShow.Name = "cmbShow";
			this.cmbShow.Size = new System.Drawing.Size(205, 40);
			this.cmbShow.TabIndex = 2;
			this.cmbShow.Text = "Use Image";
			// 
			// lblShow
			// 
			this.lblShow.AutoSize = true;
			this.lblShow.Location = new System.Drawing.Point(30, 84);
			this.lblShow.Name = "lblShow";
			this.lblShow.Size = new System.Drawing.Size(46, 15);
			this.lblShow.TabIndex = 1;
			this.lblShow.Text = "SHOW";
			// 
			// chkUseSeal
			// 
			this.chkUseSeal.Location = new System.Drawing.Point(416, 78);
			this.chkUseSeal.Name = "chkUseSeal";
			this.chkUseSeal.Size = new System.Drawing.Size(138, 27);
			this.chkUseSeal.TabIndex = 3;
			this.chkUseSeal.Text = "Use Town Seal";
			this.chkUseSeal.CheckedChanged += new System.EventHandler(this.chkUseSeal_CheckedChanged);
			// 
			// Image1
			// 
			this.Image1.AllowDrop = true;
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.DrawStyle = ((short)(0));
			this.Image1.DrawWidth = ((short)(1));
			this.Image1.Enabled = false;
			this.Image1.FillStyle = ((short)(1));
			this.Image1.FontTransparent = true;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(30, 210);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
			this.Image1.Size = new System.Drawing.Size(524, 367);
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image1.TabIndex = 8;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(524, 24);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "THIS OPTION ONLY APPLIES TO LASER FORMAT BILLS";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveExit,
				this.mnuSP,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 0;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP
			// 
			this.mnuSP.Index = 1;
			this.mnuSP.Name = "mnuSP";
			this.mnuSP.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveAndExit
			// 
			this.cmdSaveAndExit.AppearanceKey = "acceptButton";
			this.cmdSaveAndExit.Location = new System.Drawing.Point(317, 45);
			this.cmdSaveAndExit.Name = "cmdSaveAndExit";
			this.cmdSaveAndExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAndExit.Size = new System.Drawing.Size(140, 48);
			this.cmdSaveAndExit.TabIndex = 0;
			this.cmdSaveAndExit.Text = "Save & Exit";
			this.cmdSaveAndExit.Click += new System.EventHandler(this.cmdSaveAndExit_Click);
			// 
			// upload1
			// 
			this.upload1.Location = new System.Drawing.Point(30, 141);
			this.upload1.Name = "upload1";
			this.upload1.Size = new System.Drawing.Size(320, 40);
			this.upload1.TabIndex = 14;
			this.upload1.Text = "Pick Image";
			this.upload1.Uploaded += new Wisej.Web.UploadedEventHandler(this.upload1_Uploaded);
			// 
			// frmChooseImage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1049, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmChooseImage";
			this.Text = "Choose Image for Bills";
			this.Load += new System.EventHandler(this.frmChooseImage_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChooseImage_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkUseSeal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndExit)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAndExit;
		private Upload upload1;
	}
}
