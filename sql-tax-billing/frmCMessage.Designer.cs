﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmCMessage.
	/// </summary>
	partial class frmCMessage : BaseForm
	{
		public fecherFoundation.FCTextBox txtFurn;
		public fecherFoundation.FCTextBox txtMach;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtBegin;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCMessage));
			this.txtFurn = new fecherFoundation.FCTextBox();
			this.txtMach = new fecherFoundation.FCTextBox();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtBegin = new fecherFoundation.FCTextBox();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 402);
			this.BottomPanel.Size = new System.Drawing.Size(549, 73);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdContinue);
			this.ClientArea.Controls.Add(this.txtFurn);
			this.ClientArea.Controls.Add(this.txtMach);
			this.ClientArea.Controls.Add(this.txtEnd);
			this.ClientArea.Controls.Add(this.txtBegin);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(549, 342);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(549, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(221, 30);
			this.HeaderText.Text = "Format C Message";
			// 
			// txtFurn
			// 
			this.txtFurn.MaxLength = 1;
			this.txtFurn.AutoSize = false;
			this.txtFurn.BackColor = System.Drawing.SystemColors.Window;
			this.txtFurn.LinkItem = null;
			this.txtFurn.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFurn.LinkTopic = null;
			this.txtFurn.Location = new System.Drawing.Point(246, 210);
			this.txtFurn.Name = "txtFurn";
			this.txtFurn.Size = new System.Drawing.Size(93, 40);
			this.txtFurn.TabIndex = 7;
			this.txtFurn.Text = "0";
			// 
			// txtMach
			// 
			this.txtMach.MaxLength = 1;
			this.txtMach.AutoSize = false;
			this.txtMach.BackColor = System.Drawing.SystemColors.Window;
			this.txtMach.LinkItem = null;
			this.txtMach.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMach.LinkTopic = null;
			this.txtMach.Location = new System.Drawing.Point(246, 150);
			this.txtMach.Name = "txtMach";
			this.txtMach.Size = new System.Drawing.Size(93, 40);
			this.txtMach.TabIndex = 5;
			this.txtMach.Text = "0";
			// 
			// txtEnd
			// 
			this.txtEnd.MaxLength = 2;
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(246, 90);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(93, 40);
			this.txtEnd.TabIndex = 3;
			this.txtEnd.Text = "03";
			// 
			// txtBegin
			// 
			this.txtBegin.MaxLength = 2;
			this.txtBegin.AutoSize = false;
			this.txtBegin.BackColor = System.Drawing.SystemColors.Window;
			this.txtBegin.LinkItem = null;
			this.txtBegin.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtBegin.LinkTopic = null;
			this.txtBegin.Location = new System.Drawing.Point(246, 30);
			this.txtBegin.Name = "txtBegin";
			this.txtBegin.Size = new System.Drawing.Size(93, 40);
			this.txtBegin.TabIndex = 1;
			this.txtBegin.Text = "02";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 224);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(181, 27);
			this.Label4.TabIndex = 6;
			this.Label4.Text = "FURN + FIXTURES CODE";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 160);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(183, 27);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "MACH + EQUIPMENT CODE";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(141, 25);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "ENDING JUNE 20, 20";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(188, 25);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "BEGINNING JULY 1, 20";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// cmdContinue
			// 
			this.cmdContinue.AppearanceKey = "acceptButton";
			this.cmdContinue.Location = new System.Drawing.Point(30, 280);
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdContinue.Size = new System.Drawing.Size(123, 48);
			this.cmdContinue.TabIndex = 0;
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.Click += new System.EventHandler(this.cmdContinue_Click);
			// 
			// frmCMessage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(549, 475);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCMessage";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Format C Message";
			this.Load += new System.EventHandler(this.frmCMessage_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdContinue;
	}
}
