﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	public class modREPPBudStuff
	{
		//=========================================================
		const string MODULE_NAME = "modREPPBudStuff";
		/// <summary>
		/// Public strZeroDiv As String
		/// </summary>
		// vbPorter upgrade warning: lngAccountNumber As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: BKey As int	OnWriteFCConvert.ToDouble(
		public static double CalculateAccountTotalDue_6(int lngAccountNumber, bool boolREAccount, int BKey = 0)
		{
			return CalculateAccountTotalDue(ref lngAccountNumber, ref boolREAccount, BKey);
		}

		public static double CalculateAccountTotalDue_8(int lngAccountNumber, bool boolREAccount, int BKey = 0)
		{
			return CalculateAccountTotalDue(ref lngAccountNumber, ref boolREAccount, BKey);
		}

		public static double CalculateAccountTotalDue_20(int lngAccountNumber, bool boolREAccount, int BKey = 0)
		{
			return CalculateAccountTotalDue(ref lngAccountNumber, ref boolREAccount, BKey);
		}

		public static double CalculateAccountTotalDue_26(int lngAccountNumber, bool boolREAccount, int BKey = 0)
		{
			return CalculateAccountTotalDue(ref lngAccountNumber, ref boolREAccount, BKey);
		}

		public static double CalculateAccountTotalDue(ref int lngAccountNumber, ref bool boolREAccount, int BKey = 0)
		{
			double CalculateAccountTotalDue = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the total remaining balance and
				// return it as a double for the account and type passed in
				// if the billing key is passed in, it will subtract that bills amount from the total
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				string strSQL = "";
				double dblTotal = 0;
				double dblXtraInt = 0;
				if (BKey == 0)
				{
					// account total
					if (boolREAccount)
					{
						strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'RE'";
					}
					else
					{
						strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'PP'";
					}
				}
				else
				{
					// past due
					if (boolREAccount)
					{
						strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'RE' and ID <> " + FCConvert.ToString(BKey);
					}
					else
					{
						strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'PP' and ID <> " + FCConvert.ToString(BKey);
					}
				}
				rsLien.OpenRecordset("SELECT * FROM LienRec", modGlobalVariables.strCLDatabase);
				rsCL.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
				if (!rsCL.EndOfFile())
				{
					while (!rsCL.EndOfFile())
					{
						// calculate each year
						if (FCConvert.ToInt32(rsCL.Get_Fields_Int32("LienRecordNumber")) == 0)
						{
							// non-lien
							dblTotal += modCLCalculations.CalculateAccountCL(ref rsCL, lngAccountNumber, DateTime.Today, ref dblXtraInt);
						}
						else
						{
							// lien
							rsLien.FindFirstRecord("ID", rsCL.Get_Fields_Int32("LienrecordNumber"));
							if (rsLien.NoMatch)
							{
								// no match has been found
								// do nothing
							}
							else
							{
								// calculate the lien record
								dblTotal += modCLCalculations.CalculateAccountCLLien(rsLien, DateTime.Today, ref dblXtraInt);
							}
						}
						rsCL.MoveNext();
					}
					if (BKey != 0)
					{
						// If dblTotal > 0 Then
						// strSQL = "select * from billingmaster where billkey = " & BKey
						// rsCL.OpenRecordset strSQL, strCLDatabase
						// Dim dblTemp As Double
						// dblTemp = CalculateAccountCL(rsCL, lngAccountNumber, Date, dblXtraInt)
						// If dblTemp > 0 Then
						// If dblTemp <= dblTotal Then
						// dblTotal = dblTotal - CalculateAccountCL(rsCL, lngAccountNumber, Date, dblXtraInt)
						// Else
						// dblTotal = 0
						// End If
						// End If
						// Else
						// dblTotal = 0
						// End If
						if (dblTotal < 0)
							dblTotal = 0;
						// no negative past due
					}
				}
				else
				{
					// nothing found
					dblTotal = 0;
				}
				CalculateAccountTotalDue = dblTotal;
				return CalculateAccountTotalDue;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CalculateAccountTotalDue = 0;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Calculate Account Total Due Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateAccountTotalDue;
		}

		public static void ApplyPrepaymentInterestToBills()
		{
			// applies prepayment interest to bills as a discount
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			double dblPrePaymentInterest = 0;
			DateTime dtPrePaymentEffectiveInterestDate = DateTime.Now;
			int lngYear = 0;
			string strSQL = "";
			modCollectionsRelated.IntPaymentRecord IRec = new modCollectionsRelated.IntPaymentRecord(0);
			clsDRWrapper clsPay = new clsDRWrapper();
			string strAccounts = "";
			decimal dblPrePayment = 0;
			double dblPPPayments;
			double dblREPayments;
			int lngREAccountID;
			int lngPPAccountID;
			string strREAccount;
			string strPPAccount;
			string strRECommit = "";
			string strPPCommit = "";
			int intTownCode = 0;
			try
			{
				// On Error GoTo ErrorHandler
				dblPPPayments = 0;
				dblREPayments = 0;
				clsLoad.OpenRecordset("select * from collections", modGlobalVariables.strCLDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (clsLoad.Get_Fields_Boolean("Payintonprepayment") && Conversion.Val(clsLoad.Get_Fields_Double("OVERPAYINTERESTRATE")) > 0)
					{
						dblPrePaymentInterest = Conversion.Val(clsLoad.Get_Fields_Double("overpayinterestrate")) ;
						// call 113332
						dtPrePaymentEffectiveInterestDate = DateTime.Today;
						//FC:FINAL:BBE - used object and reassign it back
						object strInput = dtPrePaymentEffectiveInterestDate;
						if (!frmInput.InstancePtr.Init(ref strInput, "Prepayment Effective Interest Date", "Interest Date", 2880, false, modGlobalConstants.InputDTypes.idtDate, FCConvert.ToString(dtPrePaymentEffectiveInterestDate)))
						{
							return;
						}
						dtPrePaymentEffectiveInterestDate = Convert.ToDateTime(strInput);
					}
				}
				else
				{
					return;
				}
				clsLoad.OpenRecordset("select top 1 billingyear from billingmaster where billingyear > 0 order by billingyear desc", modGlobalVariables.strCLDatabase);
				if (!clsLoad.EndOfFile())
				{
					lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(FCConvert.ToString(clsLoad.Get_Fields_Int32("billingyear")), 1, 4))));
					//FC:FINAL:BBE - used object and reassign it back
					object strInput = lngYear;
					//FC:FINAL:MSH - i.issue #1646: increase size of textbox(in original ca be displayed 7 symbols)
					//if (!frmInput.InstancePtr.Init(ref strInput, "Billing Year", "Billing Year", 825, false, modGlobalConstants.InputDTypes.idtWholeNumber, FCConvert.ToString(lngYear)))
					if (!frmInput.InstancePtr.Init(ref strInput, "Billing Year", "Billing Year", 1425, false, modGlobalConstants.InputDTypes.idtWholeNumber, FCConvert.ToString(lngYear)))
					{
						return;
					}
					lngYear = FCConvert.ToInt32(strInput);
				}
				else
				{
					MessageBox.Show("There are no bill records", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional || !modGlobalConstants.Statics.gboolBD)
				{
					strSQL = "Select * from billingmaster where BILLINGYEAR between " + FCConvert.ToString(lngYear) + "1 and " + FCConvert.ToString(lngYear) + "9 and (taxdue1 + taxdue2 + taxdue3 + taxdue4) = 0 and principalpaid > 0 and interestappliedthroughdate < '" + FCConvert.ToString(dtPrePaymentEffectiveInterestDate) + "'";
				}
				else
				{
					strSQL = "Select * from billingmaster where BILLINGYEAR between " + FCConvert.ToString(lngYear) + "1 and " + FCConvert.ToString(lngYear) + "9 and (taxdue1 + taxdue2 + taxdue3 + taxdue4) = 0 and principalpaid > 0 and interestappliedthroughdate < '" + FCConvert.ToString(dtPrePaymentEffectiveInterestDate) + "' and trancode = " + FCConvert.ToString(intTownCode);
					intTownCode = frmPickRegionalTown.InstancePtr.Init(-1, false, "Choose town to apply prepayment interest for");
					if (intTownCode < 0)
					{
						MessageBox.Show("Cannot proceed without choosing a town", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("There are no records for billing year " + FCConvert.ToString(lngYear) + " that are eligible to apply pre-payment interest", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				strREAccount = "";
				strPPAccount = "";
				if (modGlobalConstants.Statics.gboolBD)
				{

					modAccountTitle.SetAccountFormats();
					if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.CustomizedInfo.AlternateAccount))
					{
						MessageBox.Show("You must specify a valid alternate account in customize to apply pre-payment interest", "Bad Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						modBudgetaryAccounting.Statics.strZeroDiv = "0";
					}
					else
					{
						modBudgetaryAccounting.Statics.strZeroDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
					}
					if (clsLoad.FindFirstRecord("BillingType", "RE"))
					{
						strREAccount = GetREAccount(ref lngYear, ref strRECommit, intTownCode);
						if (strREAccount == string.Empty)
						{
							// MSGBOX "You must create a Real Estate Receivable Account for " &
							return;
						}
					}
					if (clsLoad.FindFirstRecord("BillingType", "PP"))
					{
						strPPAccount = GetPPAccount(ref lngYear, ref strPPCommit, intTownCode);
						if (strPPAccount == string.Empty)
						{
							return;
						}
					}
				}
				clsLoad.MoveFirst();
				modGlobalFunctions.AddCYAEntry_242("CL", "PrePayment Interest", "Bill Year " + FCConvert.ToString(lngYear), "Eff Date " + FCConvert.ToString(dtPrePaymentEffectiveInterestDate), "Int Rate " + FCConvert.ToString(dblPrePaymentInterest));
				double dblTotInt;
				// vbPorter upgrade warning: dblTotPrepay As double	OnWrite(short, Decimal)
				double dblTotPrepay = 0;
				dblTotInt = 0;
				while (!clsLoad.EndOfFile())
				{
					// need to make an interest payment
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					strSQL = "select * from paymentrec where account = " + clsLoad.Get_Fields("account") + " and [year] = " + clsLoad.Get_Fields_Int32("billingyear");
					if (FCConvert.ToString(clsLoad.Get_Fields_String("billingtype")) == "RE")
					{
						IRec.BillCode = "R";
						strSQL += " and billcode = 'R'";
					}
					else
					{
						IRec.BillCode = "P";
						strSQL += " and billcode = 'P'";
					}
					strSQL += " and billkey = " + clsLoad.Get_Fields_Int32("ID");
					// strSQL = strSQL & " and code <> 'I' and code <> 'D'"
					strSQL += " and code <> 'D' order by effectiveinterestdate";
					dblPrePayment = 0;
					dblTotPrepay = 0;
					dblTotInt = 0;
					clsPay.OpenRecordset(strSQL, modGlobalVariables.strCLDatabase);
					while (!clsPay.EndOfFile())
					{
                        if (clsPay.Get_Fields_DateTime("EffectiveInterestDate") != DateTime.FromOADate(0))
                        {

                            if (FCConvert.ToString(clsPay.Get_Fields("code")) != "I")
                            {
                                // TODO Get_Fields: Check the table for the column [principal] and replace with corresponding Get_Field method
                                dblPrePayment = FCConvert.ToDecimal(Conversion.Val(clsPay.Get_Fields("principal")));
                                if (DateAndTime.DateDiff("d",
                                        (DateTime) clsLoad.Get_Fields_DateTime("interestappliedthroughdate"),
                                        (DateTime) clsPay.Get_Fields_DateTime("EffectiveInterestDate")) >= 0)
                                {
                                    dblTotPrepay += FCConvert.ToDouble(modCLCalculations.CalculateInterest(
                                        dblPrePayment, clsPay.Get_Fields_DateTime("EffectiveInterestDate"),
                                        dtPrePaymentEffectiveInterestDate, dblPrePaymentInterest,
                                        modGlobalVariables.Statics.gintBasis));
                                }
                                else
                                {
                                    dblTotPrepay += FCConvert.ToDouble(modCLCalculations.CalculateInterest(
                                        dblPrePayment, clsLoad.Get_Fields_DateTime("interestappliedthroughdate"),
                                        dtPrePaymentEffectiveInterestDate, dblPrePaymentInterest,
                                        modGlobalVariables.Statics.gintBasis));
                                }
                            }
                            else
                            {
                                dblTotInt += Conversion.Val(clsPay.Get_Fields_Decimal("currentinterest"));
                            }
                        }
                        else
                        {
                            dblTotPrepay = 0;
                            dblTotInt = 0;
                            break;
                        }
						clsPay.MoveNext();
					}
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					IRec.Account = FCConvert.ToInt32(clsLoad.Get_Fields("account"));
					IRec.ActualSystemDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					IRec.Period = "A";
					IRec.BillKey = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID"));
					IRec.RecordedTransactionDate = DateTime.Today;
					IRec.Year = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("billingyear"));
					IRec.EffectiveInterestDate = dtPrePaymentEffectiveInterestDate;
					IRec.PreLienInterest = 0;
					IRec.CurrentInterest = 0;
					if (dblTotInt > 0)
					{
						IRec.Principal = dblTotInt;
						IRec.CurrentInterest = FCConvert.ToDecimal(-dblTotInt);
						if (modCollectionsRelated.CreatePrePaymentInterestRecord_6(IRec, false))
						{
							if (IRec.BillCode == "R")
							{
								dblREPayments += Conversion.Val(IRec.Principal);
							}
							else
							{
								dblPPPayments += Conversion.Val(IRec.Principal);
							}
						}
					}
					if (dblTotPrepay > 0)
					{

						IRec.CurrentInterest = 0;
						IRec.Principal = dblTotPrepay;
						// IRec.Principal = CalculateInterest(dblPrePayment, clsLoad.Fields("interestappliedthroughdate"), dtPrePaymentEffectiveInterestDate, dblPrePaymentInterest, gintBasis)
						if (modCollectionsRelated.CreatePrePaymentInterestRecord_6(IRec, false))
						{
							clsLoad.Edit();
							clsLoad.Set_Fields("principalpaid", IRec.Principal + Conversion.Val(clsLoad.Get_Fields_Decimal("principalpaid")));
							if (dblTotInt > 0)
							{
								clsLoad.Set_Fields("principalpaid", clsLoad.Get_Fields_Decimal("principalpaid") + FCConvert.ToDecimal(dblTotInt));
							}
							clsLoad.Set_Fields("interestcharged", 0);
							clsLoad.Set_Fields("interestappliedthroughdate", dtPrePaymentEffectiveInterestDate);
							clsLoad.Update();
							if (IRec.BillCode == "R")
							{
								dblREPayments += Conversion.Val(IRec.Principal);
							}
							else
							{
								dblPPPayments += Conversion.Val(IRec.Principal);
							}
						}
					}
					clsLoad.MoveNext();
				}
				// have total, may have to enter into budgetary
				if (modGlobalConstants.Statics.gboolBD)
				{
                    if (dblREPayments > 0 || dblPPPayments > 0)
                    {
                        MakeJournalEntries(ref dblREPayments, ref dblPPPayments, ref strREAccount,
                            ref modGlobalVariables.Statics.CustomizedInfo.AlternateAccount, ref strPPAccount,
                            ref modGlobalVariables.Statics.CustomizedInfo.AlternateAccount, ref lngYear);
                    }
                    else
                    {
                        MessageBox.Show("There was no pre-payment interest to be applied", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
				else
				{
					MessageBox.Show("Pre-payment interest has been applied", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ApplyPrepaymentInterestToBills", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: dblRE As double	OnReadFCConvert.ToDecimal(
		// vbPorter upgrade warning: dblPP As double	OnReadFCConvert.ToDecimal(
		private static void MakeJournalEntries(ref double dblRE, ref double dblPP, ref string strREAccount, ref string strRECommit, ref string strPPAccount, ref string strPPCommit, ref int lngYear)
		{
			// enters the PP and RE values into a journal
			try
			{
				// On Error GoTo ErrorHandler
				modBudgetaryAccounting.FundType[] FundRec = null;
				int intCurRec;
				int lngReturn;
				intCurRec = -1;
				if (dblRE > 0)
				{
					intCurRec += 1;
					Array.Resize(ref FundRec, intCurRec + 1 + 1);
					FundRec[intCurRec].Account = strREAccount;
					FundRec[intCurRec].Amount = FCConvert.ToDecimal(-dblRE);
					FundRec[intCurRec].UseDueToFrom = false;
					FundRec[intCurRec].AcctType = "A";
					FundRec[intCurRec].RCB = "R";
					FundRec[intCurRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Pre-Payment Interest";
					intCurRec += 1;
					Array.Resize(ref FundRec, intCurRec + 1 + 1);
					FundRec[intCurRec].Account = strRECommit;
					FundRec[intCurRec].Amount = FCConvert.ToDecimal(dblRE);
					FundRec[intCurRec].UseDueToFrom = false;
					FundRec[intCurRec].AcctType = "A";
					FundRec[intCurRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Pre-Payment Interest";
				}
				if (dblPP > 0)
				{
					intCurRec += 1;
					if (intCurRec > Information.UBound(FundRec, 1))
					{
						Array.Resize(ref FundRec, intCurRec + 1 + 1);
					}
					FundRec[intCurRec].Account = strPPAccount;
					FundRec[intCurRec].Amount = FCConvert.ToDecimal(-dblPP);
					FundRec[intCurRec].UseDueToFrom = false;
					FundRec[intCurRec].AcctType = "A";
					FundRec[intCurRec].RCB = "R";
					FundRec[intCurRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Pre-Payment Interest";
					intCurRec += 1;
					Array.Resize(ref FundRec, intCurRec + 1 + 1);
					FundRec[intCurRec].Account = strPPCommit;
					FundRec[intCurRec].Amount = FCConvert.ToDecimal(dblPP);
					FundRec[intCurRec].AcctType = "A";
					FundRec[intCurRec].RCB = "R";
					FundRec[intCurRec].Description = Strings.Right(FCConvert.ToString(lngYear), 2) + " Pre-Payment Interest";
					FundRec[intCurRec].UseDueToFrom = false;
				}

                if (dblRE > 0 || dblPP > 0)
                {
                    lngReturn = modBudgetaryAccounting.AddToJournal(ref FundRec, "GJ", 0, DateTime.Today,
                        FCConvert.ToString(lngYear) + " Pre-Payment Interest");
                    if (lngReturn > 0)
                    {
                        MessageBox.Show("Data added to budgetary database in Journal " + FCConvert.ToString(lngReturn),
                            "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("The journal entry was not created in budgetary", "No Journal Entry",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MakeJournalEntries", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intTownCode As short	OnWriteFCConvert.ToInt32(
		private static string GetREAccount(ref int lngYear, ref string strRECommitmentAccount, int intTownCode = 1)
		{
			string GetREAccount = "";
			// returns the RE receivable account
			string strReturn;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			string strREFund = "";
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetREAccount = strReturn;
				if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					strSQL = "select * from standardaccounts where code = 'RM'";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty)
						{
							MessageBox.Show("You must set up a Real Estate Commitment Account before proceeding", "No Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return GetREAccount;
						}
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						strRECommitmentAccount = FCConvert.ToString(clsLoad.Get_Fields("account"));
					}
					else
					{
						MessageBox.Show("You must set up a Real Estate Commitment Account before proceeding", "No Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return GetREAccount;
					}
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						strSQL = "select * from deptdivtitles where department = '" + modBudgetaryAccounting.GetDepartment(strRECommitmentAccount) + "'";
					}
					else
					{
						strSQL = "select * from deptdivtitles where department = '" + modBudgetaryAccounting.GetDepartment(strRECommitmentAccount) + "' and division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "'";
					}
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						strREFund = FCConvert.ToString(clsLoad.Get_Fields("Fund"));
					}
					else
					{
						MessageBox.Show("Could not find the Real Estate Fund", "No Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return GetREAccount;
					}
				}
				else
				{
					strSQL = "Select * from tblRegions where townnumber = " + FCConvert.ToString(intTownCode);
					clsLoad.OpenRecordset(strSQL, "CentralData");
					if (!clsLoad.EndOfFile())
					{
						strRECommitmentAccount = FCConvert.ToString(clsLoad.Get_Fields_String("RECommitmentAccount"));
						if (strRECommitmentAccount != string.Empty)
						{
							strREFund = Strings.Format(modBudgetaryAccounting.GetFundFromAccount(strRECommitmentAccount), Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), "0"));
						}
						else
						{
							return GetREAccount;
						}
					}
					else
					{
						return GetREAccount;
					}
				}
				modGlobalVariables.Statics.REPPAccountInfo.Fund = strREFund;
				modGlobalVariables.Statics.REPPAccountInfo.RECommitment = strRECommitmentAccount;
				strSQL = "Select * from standardaccounts where code = 'RR'";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty)
					{
						MessageBox.Show("You must set up a Real Estate Receivable Account before proceeding", "No account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return GetREAccount;
					}
					if (!modAccountTitle.Statics.YearFlag)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strReturn = "G " + strREFund + "-" + clsLoad.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(lngYear), 2);
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strReturn = "G " + strREFund + "-" + clsLoad.Get_Fields("Account");
					}
				}
				else
				{
					MessageBox.Show("You must set up a Real Estate Receivable Account before proceeding", "No Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return GetREAccount;
				}
				modGlobalVariables.Statics.REPPAccountInfo.REReceivable = strReturn;
				strSQL = "select * from standardaccounts where code = 'TR'";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (!(FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty))
					{
						if (!modAccountTitle.Statics.YearFlag)
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable = "G " + strREFund + "-" + clsLoad.Get_Fields("account") + "-" + Strings.Right(FCConvert.ToString(lngYear), 2);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable = "G " + strREFund + "-" + clsLoad.Get_Fields("Account");
						}
					}
				}
				GetREAccount = strReturn;
				return GetREAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetREAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetREAccount;
		}
		// vbPorter upgrade warning: intTownCode As short	OnWriteFCConvert.ToInt32(
		private static string GetPPAccount(ref int lngYear, ref string strPPCommitmentAccount, int intTownCode = 1)
		{
			string GetPPAccount = "";
			// returns the RE receivable account
			string strReturn;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			string strPPFund = "";
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetPPAccount = strReturn;
				if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					strSQL = "select * from standardaccounts where code = 'PM'";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty)
						{
							return GetPPAccount;
						}
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						strPPCommitmentAccount = FCConvert.ToString(clsLoad.Get_Fields("account"));
					}
					else
					{
						MessageBox.Show("You must set up a Personal Property Commitment Account before proceeding", "No Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return GetPPAccount;
					}
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						strSQL = "select * from deptdivtitles where department = '" + modBudgetaryAccounting.GetDepartment(strPPCommitmentAccount) + "'";
					}
					else
					{
						strSQL = "select * from deptdivtitles where department = '" + modBudgetaryAccounting.GetDepartment(strPPCommitmentAccount) + "' and division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "'";
					}
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						strPPFund = FCConvert.ToString(clsLoad.Get_Fields("Fund"));
					}
					else
					{
						MessageBox.Show("Could not find the Personal Property Fund", "No Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return GetPPAccount;
					}
				}
				else
				{
					strSQL = "Select * from tblRegions where townnumber = " + FCConvert.ToString(intTownCode);
					clsLoad.OpenRecordset(strSQL, "CentralData");
					if (!clsLoad.EndOfFile())
					{
						strPPCommitmentAccount = FCConvert.ToString(clsLoad.Get_Fields_String("PPCommitmentAccount"));
						if (strPPCommitmentAccount != string.Empty)
						{
							strPPFund = Strings.Format(modBudgetaryAccounting.GetFundFromAccount(strPPCommitmentAccount), Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), "0"));
						}
						else
						{
							return GetPPAccount;
						}
					}
					else
					{
						return GetPPAccount;
					}
				}
				modGlobalVariables.Statics.REPPAccountInfo.Fund = strPPFund;
				modGlobalVariables.Statics.REPPAccountInfo.PPCommitment = strPPCommitmentAccount;
				strSQL = "Select * from standardaccounts where code = 'PR'";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty)
					{
						MessageBox.Show("You must set up a Personal Property Receivable Account before proceeding", "No account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return GetPPAccount;
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strReturn = "G " + strPPFund + "-" + clsLoad.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(lngYear), 2);
				}
				else
				{
					MessageBox.Show("You must set up a Personal Property Receivable Account before proceeding", "No Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return GetPPAccount;
				}
				modGlobalVariables.Statics.REPPAccountInfo.PPReceivable = strReturn;
				GetPPAccount = strReturn;
				return GetPPAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetPPAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetPPAccount;
		}
		/// <summary>
		/// Public Function GetDepartment(x As String) As String
		/// </summary>
		/// <summary>
		/// GetDepartment = Mid(x, 3, Val(Left(Exp, 2)))
		/// </summary>
		/// <summary>
		/// End Function
		/// </summary>
		public static string GetExpDivision(ref string x)
		{
			string GetExpDivision = "";
			GetExpDivision = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			return GetExpDivision;
		}
		// vbPorter upgrade warning: intTownCode As short	OnWriteFCConvert.ToInt32(
		public static void LoadREPPAccountInfo_2(int lngYear, short intTownCode = 1)
		{
			LoadREPPAccountInfo(ref lngYear, intTownCode);
		}

		public static void LoadREPPAccountInfo(ref int lngYear, short intTownCode = 1)
		{
			string strREAccount;
			string strPPAccount;
			string strREReceivable = "";
			string strPPReceivable = "";
			string strTAReceivable = "";
			strREAccount = "";
			strPPAccount = "";
			if (modGlobalConstants.Statics.gboolBD)
			{
				modAccountTitle.SetAccountFormats();
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					modBudgetaryAccounting.Statics.strZeroDiv = "0";
				}
				else
				{
					modBudgetaryAccounting.Statics.strZeroDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
				}
				GetRECommitmentAccount(intTownCode);
				GetRESupplementalAccount(intTownCode);
				if (modGlobalVariables.Statics.REPPAccountInfo.RECommitment != string.Empty)
				{
					modGlobalVariables.Statics.REPPAccountInfo.Fund = Strings.Format(modBudgetaryAccounting.GetFundFromAccount(modGlobalVariables.Statics.REPPAccountInfo.RECommitment), Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), "0"));
				}
				if (modGlobalVariables.Statics.REPPAccountInfo.Fund != string.Empty)
				{
					GetREReceivableAccount(ref modGlobalVariables.Statics.REPPAccountInfo.Fund, ref lngYear, intTownCode);
					GetRETaxAcquiredReceivableAccount(ref modGlobalVariables.Statics.REPPAccountInfo.Fund, ref lngYear, intTownCode);
				}
				else
				{
					modGlobalVariables.Statics.REPPAccountInfo.REReceivable = "";
					modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable = "";
				}
				GetPPCommitmentAccount(intTownCode);
				GetPPSupplementalAccount(intTownCode);
				if (modGlobalVariables.Statics.REPPAccountInfo.Fund == string.Empty && modGlobalVariables.Statics.REPPAccountInfo.PPCommitment != string.Empty)
				{
					modGlobalVariables.Statics.REPPAccountInfo.Fund = Strings.Format(modBudgetaryAccounting.GetFundFromAccount(modGlobalVariables.Statics.REPPAccountInfo.PPCommitment), Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), "0"));
				}
				if (modGlobalVariables.Statics.REPPAccountInfo.Fund != string.Empty)
				{
					GetPPReceivableAccount(ref modGlobalVariables.Statics.REPPAccountInfo.Fund, ref lngYear, intTownCode);
				}
				else
				{
					modGlobalVariables.Statics.REPPAccountInfo.PPReceivable = "";
				}
				// If UCase(MuniName) = "LIMERICK" Then
				// Dim fso As New FileSystemObject
				// Dim ts As TextStream
				// If File.Exists("BLTest.txt") Then
				// Call fso.DeleteFile("BLTest.txt")
				// End If
				// Set ts = fso.CreateTextFile("BLTest.txt")
				// ts.WriteLine "Data Directory: " & CurDir
				// ts.WriteLine "On computer: " & clsSecurityClass.GetNameOfComputer
				// ts.WriteLine "Fund: " & REPPAccountInfo.Fund
				// ts.WriteLine "RE Commitment Account: " & REPPAccountInfo.RECommitment
				// ts.WriteLine "RE Supplemental: " & REPPAccountInfo.RESupplemental
				// ts.WriteLine "RE Receivable: " & REPPAccountInfo.REReceivable
				// ts.WriteLine "RE TaxAcquired: " & REPPAccountInfo.TaxAcquiredReceivable
				// ts.WriteLine "PP Commitment: " & REPPAccountInfo.PPCommitment
				// ts.WriteLine "PP Supplemental: " & REPPAccountInfo.PPSupplemental
				// ts.WriteLine "PP Receivable: " & REPPAccountInfo.PPReceivable
				// ts.Close
				// 
				// End If
			}
		}

		private static void GetRECommitmentAccount(short intTownCode = 1)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper clsLoad = new clsDRWrapper();
				modGlobalVariables.Statics.REPPAccountInfo.RECommitment = "";
				if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					strSQL = "select * from standardaccounts where code = 'RM'";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (!(FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty))
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.RECommitment = FCConvert.ToString(clsLoad.Get_Fields("account"));
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.RECommitment))
							{
								modGlobalVariables.Statics.REPPAccountInfo.RECommitment = "";
							}
						}
						else
						{
							return;
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					strSQL = "Select * from tblregions where townNUMBER = " + FCConvert.ToString(intTownCode);
					clsLoad.OpenRecordset(strSQL, "CentralData");
					if (!clsLoad.EndOfFile())
					{
						modGlobalVariables.Statics.REPPAccountInfo.RECommitment = FCConvert.ToString(clsLoad.Get_Fields_String("RECommitmentAccount"));
						if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.RECommitment, true))
						{
							modGlobalVariables.Statics.REPPAccountInfo.RECommitment = "";
						}
					}
					else
					{
						modGlobalVariables.Statics.REPPAccountInfo.RECommitment = "";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetRECommitmentAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void GetRESupplementalAccount(short intTownCode = 1)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper clsLoad = new clsDRWrapper();
				modGlobalVariables.Statics.REPPAccountInfo.RESupplemental = "";
				if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					strSQL = "select * from standardaccounts where code = 'RS'";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (!(FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty))
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.RESupplemental = FCConvert.ToString(clsLoad.Get_Fields("account"));
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.RESupplemental))
							{
								modGlobalVariables.Statics.REPPAccountInfo.RESupplemental = "";
							}
						}
						else
						{
							return;
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					strSQL = "Select * from tblregions where townnumber = " + FCConvert.ToString(intTownCode);
					clsLoad.OpenRecordset(strSQL, "CentralData");
					if (!clsLoad.EndOfFile())
					{
						modGlobalVariables.Statics.REPPAccountInfo.RESupplemental = FCConvert.ToString(clsLoad.Get_Fields_String("RESupplementalAccount"));
						if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.RESupplemental))
						{
							modGlobalVariables.Statics.REPPAccountInfo.RESupplemental = "";
						}
					}
					else
					{
						modGlobalVariables.Statics.REPPAccountInfo.RESupplemental = "";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetRECommitmentAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void GetPPCommitmentAccount(short intTownCode = 1)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper clsLoad = new clsDRWrapper();
				modGlobalVariables.Statics.REPPAccountInfo.PPCommitment = "";
				if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					strSQL = "select * from standardaccounts where code = 'PM'";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (!(FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty))
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.PPCommitment = FCConvert.ToString(clsLoad.Get_Fields("account"));
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.PPCommitment))
							{
								modGlobalVariables.Statics.REPPAccountInfo.PPCommitment = "";
							}
						}
						else
						{
							return;
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					strSQL = "Select * from tblregions where townnumber = " + FCConvert.ToString(intTownCode);
					clsLoad.OpenRecordset(strSQL, "CentralData");
					if (!clsLoad.EndOfFile())
					{
						modGlobalVariables.Statics.REPPAccountInfo.PPCommitment = FCConvert.ToString(clsLoad.Get_Fields_String("ppCommitmentAccount"));
						if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.PPCommitment))
						{
							modGlobalVariables.Statics.REPPAccountInfo.PPCommitment = "";
						}
					}
					else
					{
						modGlobalVariables.Statics.REPPAccountInfo.PPCommitment = "";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetppCommitmentAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void GetPPSupplementalAccount(short intTownCode = 1)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper clsLoad = new clsDRWrapper();
				modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental = "";
				if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					strSQL = "select * from standardaccounts where code = 'PS'";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (!(FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty))
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental = FCConvert.ToString(clsLoad.Get_Fields("account"));
							if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental))
							{
								modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental = "";
							}
						}
						else
						{
							return;
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					strSQL = "Select * from tblregions where townnumber = " + FCConvert.ToString(intTownCode);
					clsLoad.OpenRecordset(strSQL, "CentralData");
					if (!clsLoad.EndOfFile())
					{
						modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental = FCConvert.ToString(clsLoad.Get_Fields_String("ppsupplementalAccount"));
						if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental))
						{
							modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental = "";
						}
					}
					else
					{
						modGlobalVariables.Statics.REPPAccountInfo.PPSupplemental = "";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetppSupplementalAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void GetREReceivableAccount(ref string strREFund, ref int lngYear, short intTownCode = 1)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				clsDRWrapper clsLoad = new clsDRWrapper();
				modGlobalVariables.Statics.REPPAccountInfo.REReceivable = "";
				strSQL = "Select * from standardaccounts where code = 'RR'";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (!(FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty))
					{
						if (!modAccountTitle.Statics.YearFlag)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.REReceivable = "G " + strREFund + "-" + clsLoad.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(lngYear), 2);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.REReceivable = "G " + strREFund + "-" + clsLoad.Get_Fields("Account");
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					return;
				}
				if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.REReceivable))
				{
					modGlobalVariables.Statics.REPPAccountInfo.REReceivable = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetREReceivableAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void GetRETaxAcquiredReceivableAccount(ref string strREFund, ref int lngYear, short intTownCode = 1)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				clsDRWrapper clsLoad = new clsDRWrapper();
				string strAccount = "";
				string strAcct = "";
				string strFund = "";
				string strSuffix = "";
				modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable = "";
				// If Not gboolCR Then
				strSQL = "Select * from standardaccounts where code = 'TR'";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (!(FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty))
					{
						if (!modAccountTitle.Statics.YearFlag)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable = "G " + strREFund + "-" + clsLoad.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(lngYear), 2);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable = "G " + strREFund + "-" + clsLoad.Get_Fields("Account");
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					return;
				}
				if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable))
				{
					modGlobalVariables.Statics.REPPAccountInfo.TaxAcquiredReceivable = "";
				}
				// Else
				// strSQL = "select * from type where typecode = 890"
				// Call clsLoad.OpenRecordset(strSQL, strCRDatabase)
				// If Not clsLoad.EndOfFile Then
				// Dim strTemp As String
				// strAccount = clsLoad.Fields("account1")
				// If UCase(Left(Trim(strAccount) & " ", 1)) <> "G" Then
				// REPPAccountInfo.TaxAcquiredReceivable = strAccount
				// Else
				// strFund = Format(GetFundFromAccount(strAccount), String(Val(Left(Ledger, 2)), "0"))
				// strAcct = GetAccount(strAccount)
				// strTemp = "G " & strFund & "-" & strAcct
				// If Not YearFlag Then
				// add the suffix
				// 
				// If clsLoad.Fields("year1") Then
				// strSuffix = Format(Right(lngYear, 2), String(Val(Mid(Ledger, 5, 2)), "0"))
				// Else
				// strSuffix = Format(GetSuffix(strAccount), String(Val(Mid(Ledger, 5, 2)), "0"))
				// End If
				// strTemp = strTemp & "-" & strSuffix
				// End If
				// REPPAccountInfo.TaxAcquiredReceivable = strTemp
				// End If
				// If Not AccountValidate(REPPAccountInfo.TaxAcquiredReceivable) Then
				// REPPAccountInfo.TaxAcquiredReceivable = ""
				// End If
				// Else
				// Exit Sub
				// End If
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetRETaxAcquiredReceivableAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void GetPPReceivableAccount(ref string strPPFund, ref int lngYear, short intTownCode = 1)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				clsDRWrapper clsLoad = new clsDRWrapper();
				modGlobalVariables.Statics.REPPAccountInfo.PPReceivable = "";
				strSQL = "Select * from standardaccounts where code = 'PR'";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strBDDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (!(FCConvert.ToString(clsLoad.Get_Fields("Account")) == string.Empty))
					{
						if (!modAccountTitle.Statics.YearFlag)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.PPReceivable = "G " + strPPFund + "-" + clsLoad.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(lngYear), 2);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.REPPAccountInfo.PPReceivable = "G " + strPPFund + "-" + clsLoad.Get_Fields("Account");
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					return;
				}
				if (!modValidateAccount.AccountValidate(modGlobalVariables.Statics.REPPAccountInfo.PPReceivable))
				{
					modGlobalVariables.Statics.REPPAccountInfo.PPReceivable = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetPPReceivableAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static string GetAccount(ref string x)
		{
			string GetAccount = "";
			GetAccount = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			return GetAccount;
		}

		public static string GetSuffix(ref string x)
		{
			string GetSuffix = "";
			GetSuffix = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)))));
			return GetSuffix;
		}
	}
}
