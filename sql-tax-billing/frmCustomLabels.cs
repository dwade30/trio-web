﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.Drawing;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	public partial class frmCustomLabels : BaseForm
	{
		public frmCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomLabels InstancePtr
		{
			get
			{
				return (frmCustomLabels)Sys.GetInstance(typeof(frmCustomLabels));
			}
		}

		protected frmCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		/// </summary>
		/// <summary>
		/// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		/// </summary>
		/// <summary>
		/// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		/// </summary>
		/// <summary>
		/// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomLabels.rpt
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		/// </summary>
		/// <summary>
		/// SetFormFieldCaptions IN modCustomReport.mod
		/// </summary>
		/// <summary>
		/// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// A CALL TO THIS FORM WOULD BE:
		/// </summary>
		/// <summary>
		/// frmCustomLabels.Show , MDIParent
		/// </summary>
		/// <summary>
		/// Call SetFormFieldCaptions(frmCustomLabels, "Births")
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolSaveReport;
		bool boolNotLoading;
		bool boolFromBills;
		clsBillFormat clsBFormat = new clsBillFormat();
		const int CNSTGRIDCOLRATEKEY = 0;
		const int CNSTGRIDCOLRATE = 1;
		const int CNSTGRIDCOLDESC = 2;
		const int CNSTGRIDCOLPERIODS = 3;
		const int CNSTGRIDCOLTYPE = 4;
		const int CNSTGRIDCOLBILLDATE = 5;
		bool boolFromMortgageHolder;
		// vbPorter upgrade warning: lngID As int	OnWrite(short, string)
		public void Init(int lngID, int intDummy, bool boolFromPrintBills = false, clsBillFormat clsTax = null)
		{
			// currently lngID is only used as a mortgage holder id
			intID = lngID;
			boolFromBills = boolFromPrintBills;
			if (boolFromBills)
			{
				clsBFormat.CopyFormat(ref clsTax);
				fraFields.Enabled = false;
				fraSort.Enabled = false;
				fraWhere.Enabled = false;
				mnuClear.Visible = false;
				mnuSepar.Visible = false;
				//FC:FINAL:CHN - i.issue #1540: "Clear Search Criteria" Button not needed at "Bills->Print bills" but need at "Printing->labels".
				this.cmdClear.Visible = false;
			}
			boolFromMortgageHolder = true;
			if (boolFromMortgageHolder)
			{
				modCustomReport.Statics.strReportType = "MORTGAGEHOLDER";
			}
			this.Show(App.MainForm);
		}

		private void FillLabelTypeCombo()
		{
			cmbLabelType.AddItem("Avery 4013");
			cmbLabelType.AddItem("Avery 4014");
			cmbLabelType.AddItem("Avery 5160,5260,5970");
			cmbLabelType.AddItem("Avery 5161,5261,5661");
			cmbLabelType.AddItem("Avery 5162,5262,5662");
			cmbLabelType.AddItem("Avery 5163,5263,5663");
		}

		private void cmbLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strDesc = "";
			if (cmbLabelType.SelectedIndex < 0)
				return;
			switch (cmbLabelType.SelectedIndex)
			{
				case 0:
					{
						strDesc = "Style 4013.  1 in. x 3.5 in. This type is a continuous sheet of labels used with a dot matrix.";
						vsLayout.ColWidth(0, FCConvert.ToInt32(3.5 * 1440));
						txtAlignment.Visible = false;
						lblLaserAdjustment.Visible = false;
						chkChooseLabelStart.Visible = false;
						break;
					}
				case 1:
					{
						strDesc = "Style 4014.  1 7/16 in. x 4 in. This type is a continuous sheet of labels used with a dot matrix.";
						vsLayout.ColWidth(0, 4 * 1440);
						lblLaserAdjustment.Visible = false;
						txtAlignment.Visible = false;
						chkChooseLabelStart.Visible = false;
						break;
					}
				case 2:
					{
						strDesc = "Style 5160, 5260, 5560, 5660, 5960, 5970, 5971, 5972, 5979, 5980, 6241, 6460. Sheet of 3 x 10. 1 in. X 2 5/8 in.";
						vsLayout.ColWidth(0, FCConvert.ToInt32(2.625 * 1440));
						lblLaserAdjustment.Visible = true;
						txtAlignment.Visible = true;
						chkChooseLabelStart.Visible = true;
						break;
					}
				case 3:
					{
						strDesc = "Style 5161, 5261, 5661, 5961. Sheet of 2 X 10. 1 in. X 4 in.";
						vsLayout.ColWidth(0, 4 * 1440);
						lblLaserAdjustment.Visible = true;
						txtAlignment.Visible = true;
						chkChooseLabelStart.Visible = true;
						break;
					}
				case 4:
					{
						strDesc = "Style 5162, 5262, 5662, 5962. Sheet of 2 X 7. 1 1/3 in. X 4 in.";
						vsLayout.ColWidth(0, 4 * 1440);
						lblLaserAdjustment.Visible = true;
						txtAlignment.Visible = true;
						chkChooseLabelStart.Visible = true;
						break;
					}
				case 5:
					{
						strDesc = "Style 5163, 5263, 5663, 5963. Sheet of 2 X 5. 2 in. X 4 in.";
						vsLayout.ColWidth(0, 4 * 1440);
						lblLaserAdjustment.Visible = true;
						txtAlignment.Visible = true;
						chkChooseLabelStart.Visible = true;
						break;
					}
			}
			//end switch
			lblDescription.Text = strDesc;
		}

		private void cmdClear_Click()
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		private void cmdExit_Click()
		{
			Close();
		}

		private void cmdPrint_Click()
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			string strSQL = "";
			string strPrinterName;
			int NumFonts = 0;
			bool boolUseFont;
			string strFont = "";
			int intCPI = 0;
			int x;
			modRegistry.SaveRegistryKey("BLLabelLaserAdjustment", FCConvert.ToString(Conversion.Val(txtAlignment.Text)));
			if (!boolFromBills)
			{
				if (Grid.Row < 1 && lstFields.SelectedIndex != 0 && !boolFromMortgageHolder)
				{
					if (lstFields.SelectedIndex > 0)
					{
						MessageBox.Show("You must choose a rate record", "No Rate Chosen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				// 
				// CLEAR THE FIELDS WIDTH ARRAY
				for (intCounter = 0; intCounter <= 49; intCounter++)
				{
					modCustomReport.Statics.strFieldWidth[intCounter] = 0;
				}
				// 
				// PREPARE THE SQL TO SHOW THE REPORT
				// BUILD THE SQL FOR THE NEW CUSTOM REPORT
				strSQL = BuildSQL();
			}
			// GET THE NUMBER OF FIELDS TO DISPLAY AND FILL THE FIELDS CAPTION
			// ARRAY WITH THE DATABASE FIELD NAMES FOR THOSE THAT WERE CHOSEN
			// Call GetNumberOfFields(strCustomSQL)
			// 
			// 
			// GET THE CAPTIONS TO DISPLAY ON THE REPORT. THE DEVELOPER MAY
			// WANT THE DISPLAYED 'HEADINGS' TO BE DIFFERENT THEN THE DATABASE
			// FIELD NAMES OR THE CUSTOM REPORT FORM'S DISPLAY NAME
			// Call SetColumnCaptions(Me)
			// 
			// make them choose the printer so we can set the correct printer font
			//FC:FINAL:DDU:#i1470 - prevent object reference not set
			//MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
			//MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
			boolUseFont = false;
			strPrinterName = FCGlobal.Printer.DeviceName;
			int intLabelStart;
			intLabelStart = 1;
			if (cmbLabelType.SelectedIndex < 2)
			{
				NumFonts = FCGlobal.Printer.Fonts.Count;
				intCPI = 10;
				for (x = 0; x <= NumFonts - 1; x++)
				{
					strFont = FCGlobal.Printer.Fonts[x];
					if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
					{
						strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
						if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
						{
							boolUseFont = true;
							strFont = FCGlobal.Printer.Fonts[x];
							break;
						}
					}
				}
				// x
			}
			else
			{
				strFont = "";
				if (chkChooseLabelStart.CheckState == Wisej.Web.CheckState.Checked)
				{
					int NRows = 0;
					int NCols = 0;
					switch (cmbLabelType.SelectedIndex)
					{
						case 2:
							{
								// 5260
								NRows = 10;
								NCols = 3;
								break;
							}
						case 3:
							{
								// 5261
								NRows = 10;
								NCols = 2;
								break;
							}
						case 4:
							{
								// 5262
								NRows = 7;
								NCols = 2;
								break;
							}
						case 5:
							{
								NRows = 5;
								NCols = 2;
								break;
							}
					}
					//end switch
					intLabelStart = frmChooseLabelPosition.InstancePtr.Init(NRows, NCols);
					if (intLabelStart < 1)
						return;
				}
			}
			// SHOW THE REPORT
			if (!boolUseFont)
				strFont = "";
			if (!boolFromBills)
			{
				rptCustomLabels.InstancePtr.Init(strSQL, modCustomReport.Statics.strReportType, cmbLabelType.SelectedIndex, strPrinterName, strFont, intLabelStart);
			}
			else
			{
				rptBillLabels.InstancePtr.Init(clsBFormat, cmbLabelType.SelectedIndex, strPrinterName, strFont, this.Modal, intLabelStart);
			}
			Close();
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public string BuildSQL()
		{
			string BuildSQL = "";
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			string strSQL = "";
			string strWhere;
			string strOrderBy;
			BuildSQL = "";
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "MORTGAGEHOLDER")
			{
				strSQL = "Select *,'' as secowner from mortgageholders ";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "REALESTATE")
			{
				strSQL = "select account,name1 as name,name2 as secowner,address1,address2,address3 from billingmaster ";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "PERSONALPROPERTY")
			{
				strSQL = "select account,name1 as name,name2 as secowner,address1,address2,address3 from billingmaster ";
			}
			strWhere = BuildWhereParameter();
			strSQL += strWhere;
			// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
			strOrderBy = BuildSortParameter();
			if (Strings.Trim(strOrderBy) != string.Empty)
			{
				strSQL += " order by " + strOrderBy;
			}
			BuildSQL = strSQL;
			return BuildSQL;
		}

		public string BuildSortParameter()
		{
			string BuildSortParameter = "";
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					strSort += vsWhere.TextMatrix(lstSort.ItemData(intCounter), 3);
				}
			}
			BuildSortParameter = strSort;
			return BuildSortParameter;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string BuildWhereParameter()
		{
			string BuildWhereParameter = "";
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			int intCounter;
			string strTemp = "";
			string strWord;
			// CLEAR THE VARIABLES
			strWhere = " ";
			strWord = " where ";
			if (modCustomReport.Statics.strReportType != "MORTGAGEHOLDER")
			{
				strWord = " and ";
				strWhere = " where ratekey = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLRATEKEY)));
				if (modCustomReport.Statics.strReportType == "PERSONALPROPERTY")
				{
					strWhere += " and billingtype = 'PP' ";
				}
				else
				{
					strWhere += " and billingtype = 'RE' ";
				}
				if (chkExclude.CheckState == Wisej.Web.CheckState.Checked)
				{
					strWhere += " and (taxdue1 + taxdue2 + taxdue3 + taxdue4 - principalpaid) > 0 ";
				}
			}
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				switch (intCounter)
				{
					case 0:
						{
							// name
							if (modCustomReport.Statics.strReportType == "MORTGAGEHOLDER")
							{
								strTemp = "name";
							}
							else
							{
								strTemp = "name1";
							}
							if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != string.Empty)
							{
								if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != string.Empty)
								{
									strWhere += strWord + strTemp + " between '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "' and '" + Strings.Trim(vsWhere.TextMatrix(0, 2)) + "' ";
								}
								else
								{
									strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "' ";
								}
								strWord = " and ";
							}
							break;
						}
					case 1:
						{
							// account
							if (modCustomReport.Statics.strReportType == "MORTGAGEHOLDER")
							{
								// mh
								strTemp = "mortgageholderid";
							}
							else if (modCustomReport.Statics.strReportType == "REALESTATE")
							{
								// re
								strTemp = "account";
							}
							else
							{
								strTemp = "account";
								// pp
							}
							if (Conversion.Val(vsWhere.TextMatrix(1, 1)) > 0)
							{
								if (Conversion.Val(vsWhere.TextMatrix(1, 2)) > 0)
								{
									strWhere += strWord + strTemp + " between " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " and " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2))) + " ";
								}
								else
								{
									strWhere += strWord + strTemp + " = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " ";
								}
								strWord = " and ";
							}
							// Case 2
							// location
							// If strReportType = "MORTGAGEHOLDER" Then
							// mh
							// strTemp = "zip"
							// If Trim(vsWhere.TextMatrix(2, 1)) <> vbNullString Then
							// If Trim(vsWhere.TextMatrix(2, 2)) <> vbNullString Then
							// strWhere = strWhere & strWord & strTemp & " between '" & Trim(vsWhere.TextMatrix(3, 1)) & "' and '" & Trim(vsWhere.TextMatrix(3, 2)) & "' "
							// Else
							// strWhere = strWhere & strWord & strTemp & " = '" & Trim(vsWhere.TextMatrix(3, 1)) & "' "
							// End If
							// strWord = " and "
							// End If
							// ElseIf strReportType = "REALESTATE" Then
							// re
							// strTemp = "streetname"
							// Else
							// pp
							// strTemp = "streetname"
							// End If
							// 
							// If Trim(vsWhere.TextMatrix(2, 1)) <> vbNullString Then
							// If Trim(vsWhere.TextMatrix(2, 2)) <> vbNullString Then
							// strWhere = strWhere & strWord & strTemp & " between '" & Trim(vsWhere.TextMatrix(2, 1)) & "' and '" & Trim(vsWhere.TextMatrix(2, 2)) & "' "
							// Else
							// strWhere = strWhere & strWord & strTemp & " = '" & Trim(vsWhere.TextMatrix(2, 1)) & "' "
							// End If
							// strWord = " and "
							// End If
							break;
						}
					case 2:
						{
							// zip
							strTemp = "zip";
							// If strReportType = "PERSONALPROPERTY" Then
							// If Trim(vsWhere.TextMatrix(3, 1)) <> vbNullString Then
							// If Trim(vsWhere.TextMatrix(3, 2)) <> vbNullString Then
							// strWhere = strWhere & strWord & strtemp & " between " & Val(vsWhere.TextMatrix(3, 1)) & " and " & Val(vsWhere.TextMatrix(3, 2)) & " "
							// Else
							// strWhere = strWhere & strWord & strtemp & " = " & Val(vsWhere.TextMatrix(3, 1)) & " "
							// End If
							// strWord = " and "
							// End If
							// Else
							// If strReportType = "REALESTATE" Then strtemp = "rszip"
							if (Strings.Trim(vsWhere.TextMatrix(2, 1)) != string.Empty)
							{
								if (Strings.Trim(vsWhere.TextMatrix(2, 2)) != string.Empty)
								{
									strWhere += strWord + strTemp + " between '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' and '" + Strings.Trim(vsWhere.TextMatrix(2, 2)) + "' ";
								}
								else
								{
									strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' ";
								}
								strWord = " and ";
							}
							// 
							// End If
							// Case 4
							// maplot or businesscode
							// If strReportType = "PERSONALPROPERTY" Then
							// If Val(vsWhere.TextMatrix(4, 1)) > 0 Then
							// If Val(vsWhere.TextMatrix(4, 2)) > 0 Then
							// strWhere = strWhere & strWord & " businesscode between " & Val(vsWhere.TextMatrix(4, 1)) & " and " & Val(vsWhere.TextMatrix(4, 2)) & " "
							// Else
							// strWhere = strWhere & strWord & " businesscode = " & Val(vsWhere.TextMatrix(4, 1)) & " "
							// End If
							// strWord = " and "
							// End If
							// Else
							// If Trim(vsWhere.TextMatrix(4, 1)) <> vbNullString Then
							// If Trim(vsWhere.TextMatrix(4, 2)) <> vbNullString Then
							// strWhere = strWhere & strWord & " maplot between '" & Trim(vsWhere.TextMatrix(4, 1)) & "' and '" & Trim(vsWhere.TextMatrix(4, 2)) & "' "
							// Else
							// strWhere = strWhere & strWord & " maplot = '" & Trim(vsWhere.TextMatrix(4, 1)) & "' "
							// End If
							// strWord = " and "
							// End If
							// End If
							break;
						}
				}
				//end switch
			}
			// intCounter
			BuildWhereParameter = strWhere;
			return BuildWhereParameter;
		}

		private void frmCustomLabels_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
			// Case vbKeyF2
			// KeyCode = 0
			// Call mnuAddRow_Click
			// 
			// Case vbKeyF3
			// KeyCode = 0
			// Call mnuAddColumn_Click
			// 
			// Case vbKeyF4
			// KeyCode = 0
			// Call mnuDeleteRow_Click
			// 
			// Case vbKeyF5
			// KeyCode = 0
			// Call mnuDeleteColumn_Click
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomLabels properties;
			//frmCustomLabels.ScaleWidth	= 8955;
			//frmCustomLabels.ScaleHeight	= 7455;
			//frmCustomLabels.LinkTopic	= "Form1";
			//frmCustomLabels.LockControls	= true;
			//End Unmaped Properties
			boolNotLoading = false;
			txtAlignment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("BLLabelLaserAdjustment")));
			SetupGrid();
			FillYearCombo();
			SetParameters();
			FillLayoutGrid();
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			FillLabelTypeCombo();
			cmbLabelType.SelectedIndex = 0;
			if (boolFromMortgageHolder && !boolFromBills)
			{
				lstFields.Enabled = false;
				lstSort.Enabled = false;
				vsWhere.Enabled = false;
				if (intID > 0)
				{
					vsWhere.TextMatrix(1, 1, FCConvert.ToString(intID));
				}
				fraFields.Enabled = false;
				fraSort.Enabled = false;
				fraWhere.Enabled = false;
			}
			else if (!boolFromBills)
			{
				fraFields.Enabled = true;
				fraSort.Enabled = true;
				fraWhere.Enabled = true;
				mnuClear.Visible = true;
				mnuSepar.Visible = true;
				//FC:FINAL:CHN - i.issue #1540: "Clear Search Criteria" Button not needed at "Bills->Print bills" but need at "Printing->labels".
				this.cmdClear.Visible = true;
			}
			boolNotLoading = true;
		}

		private void frmCustomLabels_Resize(object sender, System.EventArgs e)
		{
			int GridWidth;
			GridWidth = vsWhere.WidthOriginal;
			vsWhere.ColWidth(0, FCConvert.ToInt32(0.36 * GridWidth));
			vsWhere.ColWidth(1, FCConvert.ToInt32(0.31 * GridWidth));
			vsWhere.ColWidth(2, FCConvert.ToInt32(0.31 * GridWidth));
			vsWhere.ColWidth(3, 0);
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modCustomReport.Statics.strPreSetReport = string.Empty;
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			switch (lstFields.SelectedIndex)
			{
				case 0:
					{
						// mortgage holders
						modCustomReport.Statics.strReportType = "MORTGAGEHOLDER";
						LoadWhereGrid_2("MORTGAGEHOLDER");
						LoadSortList();
						FillLayoutGrid_2(0);
						framYear.Visible = false;
						Grid.Visible = false;
						chkExclude.Visible = false;
						break;
					}
				case 1:
					{
						// Real Estate
						framYear.Visible = true;
						Grid.Visible = true;
						chkExclude.Visible = true;
						modCustomReport.Statics.strReportType = "REALESTATE";
						LoadWhereGrid_2("REALESTATE");
						LoadSortList();
						FillLayoutGrid_2(1);
						break;
					}
				case 2:
					{
						// Personal Property
						framYear.Visible = true;
						Grid.Visible = true;
						chkExclude.Visible = true;
						modCustomReport.Statics.strReportType = "PERSONALPROPERTY";
						LoadWhereGrid_2("PERSONALPROPERTY");
						LoadSortList();
						FillLayoutGrid_2(2);
						break;
					}
			}
			//end switch
		}

		private void FillLayoutGrid_2(short intLType)
		{
			FillLayoutGrid(intLType);
		}

		private void FillLayoutGrid(short intLType = 0)
		{
			int GridWidth;
			vsLayout.Cols = 1;
			if (intLType != 1)
			{
				vsLayout.Rows = 4;
				vsLayout.ColWidth(0, 4 * 1440);
				// four inches
				vsLayout.TextMatrix(0, 0, "Name");
				vsLayout.TextMatrix(1, 0, "Address 1");
				vsLayout.TextMatrix(2, 0, "Address 2");
				vsLayout.TextMatrix(3, 0, "City, State 00000");
			}
			else
			{
				vsLayout.Rows = 5;
				vsLayout.ColWidth(0, 4 * 1440);
				// four inches
				vsLayout.TextMatrix(0, 0, "Name");
				vsLayout.TextMatrix(1, 0, "Second Owner");
				vsLayout.TextMatrix(2, 0, "Address 1");
				vsLayout.TextMatrix(3, 0, "Address 2");
				vsLayout.TextMatrix(4, 0, "City, State 00000");
			}
            //FC:FINAL:AM:#3196 - set the row height
            vsLayout.RowHeight(-1, 225);
		}

		//private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// intStart = lstFields.ListIndex
		//}

		//private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
		//	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	// strtemp = lstFields.List(lstFields.ListIndex)
		//	// intID = lstFields.ItemData(lstFields.ListIndex)
		//	// 
		//	// CHANGE THE NEW ITEM
		//	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
		//	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
		//	// 
		//	// SAVE THE OLD ITEM
		//	// lstFields.List(intStart) = strtemp
		//	// lstFields.ItemData(intStart) = intID
		//	// 
		//	// SET BOTH ITEMS TO BE SELECTED
		//	// lstFields.Selected(lstFields.ListIndex) = True
		//	// lstFields.Selected(intStart) = True
		//	// End If
		//}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	//if (intStart != lstSort.SelectedIndex)
		//	//{
		//	//    // SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	//    strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
		//	//    intID = lstSort.ItemData(lstSort.SelectedIndex);
		//	//    // CHANGE THE NEW ITEM
		//	//    lstSort.Items[lstSort.ListIndex].Text = lstSort.Items[intStart].Text;
		//	//    lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//	//    // SAVE THE OLD ITEM
		//	//    lstSort.Items[intStart].Text = strTemp;
		//	//    lstSort.ItemData(intStart, intID);
		//	//    // SET BOTH ITEMS TO BE SELECTED
		//	//    lstSort.SetSelected(lstSort.ListIndex, true);
		//	//    lstSort.SetSelected(intStart, true);
		//	//}
		//}

		private void mnuAddColumn_Click()
		{
			if (vsLayout.Row < 0)
				vsLayout.Row = 0;
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		private void mnuAddRow_Click()
		{
			vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
			vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click()
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.", "Add Column", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuDeleteRow_Click()
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.", "Add Row", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void optType_Click(ref short Index)
		{
			// NUMBER OF ROWS IS HIGHT / 240 (HEIGHT OF ONE ROW) PLUS ONE FOR
			// THE HEADER ROW   1440 = 1 INCH
			int intCounter;
			// THESE DIMENTIONS ARE A BIT DIFFERENT THEN THE NUMBERS OUTLINED
			// IN MS WORD BECAUSE WE ARE USING A DIFFERENT FONT HERE THEN
			// THE MS WORD DEFAULT FONT.
			// 
			vsLayout.Clear();
			switch (Index)
			{
				case 0:
					{
						// Avery 5160 (3 X 10)  Height = 1" Width = 2.63"
						// 1440/240(row height)
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
						Line1.X1 = 2.8F;
						Line1.X2 = 2.8F;
						Line1.Y1 = 0;
						Line1.Y2 = vsLayout.Height;
						break;
					}
				case 1:
					{
						// Avery 5161 (2 X 10)  Height = 1" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
						Line1.X1 = 4F;
						Line1.X2 = 4F;
						Line1.Y1 = 0;
						Line1.Y2 = vsLayout.Height;
						break;
					}
				case 2:
					{
						// Avery 5163 (2 X 5)  Height = 2" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 13;
						vsLayout.Cols = 1;
						Line1.X1 = 4F;
						Line1.X2 = 4F;
						Line1.Y1 = 0;
						Line1.Y2 = vsLayout.Height;
						break;
					}
				case 3:
					{
						// Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 9;
						vsLayout.Cols = 1;
						Line1.X1 = 4F;
						Line1.X2 = 4F;
						Line1.Y1 = 0;
						Line1.Y2 = vsLayout.Height;
						break;
					}
			}
			//end switch
			for (intCounter = 0; intCounter <= vsLayout.Rows - 1; intCounter++)
			{
				vsLayout.MergeRow(intCounter, true);
			}
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, e.RowIndex, e.ColumnIndex)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, e.RowIndex, e.ColumnIndex, -1);
			}
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(e.RowIndex, 3, vsWhere.ComboData());
			if (Strings.Trim(vsWhere.TextMatrix(e.RowIndex, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(e.RowIndex, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(e.RowIndex, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(e.RowIndex, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[e.RowIndex]))
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						vsWhere.EditMask = "##/##/####";
						// Case GRIDCOMBOIDTEXT, GRIDCOMBOIDNUM, GRIDCOMBOTEXT
						// vsWhere.ComboList = strComboList(Row, 0)
						break;
					}
			}
			//end switch
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
			{
				vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
			}
			if (vsWhere.Col == 2)
			{
				//FC:FINAL:MSH - i.issue #1442: can't apply '==' operator to int and Color
				//FC:FINAL:DDU:#i1474 - changing right part to be the number as in original
				if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					vsWhere.Col = 1;
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsWhere.GetFlexRowIndex(e.RowIndex);
			int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
							vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(row, col, string.Empty);
							vsWhere.Refresh();
							return;
						}
						if (Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
						if (!modCustomReport.IsValidDate(vsWhere.EditText))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modCustomReport.Statics.strComboList[row, 0];
						break;
					}
			}
			//end switch
		}

		public void LoadWhereGrid_2(string strType)
		{
			LoadWhereGrid(ref strType);
		}

		public void LoadWhereGrid(ref string strType)
		{
			vsWhere.Rows = 0;
			if (Strings.UCase(strType) == "MORTGAGEHOLDER")
			{
				vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "name");
				vsWhere.AddItem("Holder Number" + "\t" + "\t" + "\t" + "mortgageholderid");
				// FormName.vsWhere.AddItem ("Location" & vbTab & vbTab & vbTab & "streetname")
				vsWhere.AddItem("Zip" + "\t" + "\t" + "\t" + "zip");
			}
			else if (Strings.UCase(strType) == "REALESTATE")
			{
				vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "name1");
				vsWhere.AddItem("Account" + "\t" + "\t" + "\t" + "account");
				// vsWhere.AddItem ("Location" & vbTab & vbTab & vbTab & "rslocstreet")
				vsWhere.AddItem("Zip" + "\t" + "\t" + "\t" + "zip");
				// vsWhere.AddItem ("Map / Lot" & vbTab & vbTab & vbTab & "rsmaplot")
			}
			else if (Strings.UCase(strType) == "PERSONALPROPERTY")
			{
				vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "Name1");
				vsWhere.AddItem("Account" + "\t" + "\t" + "\t" + "account");
				// vsWhere.AddItem ("Location" & vbTab & vbTab & vbTab & "street")
				vsWhere.AddItem("Zip" + "\t" + "\t" + "\t" + "zip");
			}
		}

		private void SetParameters()
		{
			modCustomReport.Statics.strCustomTitle = "Mortgage Holder Labels";
			lstFields.AddItem("Mortgage Holder");
			lstFields.ItemData(lstFields.NewIndex, 0);
			lstFields.AddItem("Real Estate");
			lstFields.ItemData(lstFields.NewIndex, 1);
			lstFields.AddItem("Personal Property");
			lstFields.ItemData(lstFields.NewIndex, 2);
			LoadWhereGrid_2("MORTGAGEHOLDER");
			LoadSortList();
		}

		private void LoadSortList()
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
			lstSort.Items.Clear();
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				lstSort.AddItem(vsWhere.TextMatrix(intCounter, 0));
				lstSort.ItemData(lstSort.NewIndex, intCounter);
			}
		}

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (boolNotLoading)
			{
				if (Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString()) > 0)
				{
					FillGrid_2(FCConvert.ToInt32(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
				}
			}
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.Cols = 6;
			Grid.ColHidden(CNSTGRIDCOLRATEKEY, true);
			Grid.ExtendLastCol = true;
			Grid.TextMatrix(0, CNSTGRIDCOLRATE, "Tax Rate");
			Grid.TextMatrix(0, CNSTGRIDCOLDESC, "Description");
			Grid.TextMatrix(0, CNSTGRIDCOLPERIODS, "Periods");
			Grid.TextMatrix(0, CNSTGRIDCOLTYPE, "Type");
			Grid.TextMatrix(0, CNSTGRIDCOLBILLDATE, "Bill Date");
			// regular or supplemental
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLRATE, FCConvert.ToInt32(GridWidth * 0.12));
			Grid.ColWidth(CNSTGRIDCOLDESC, FCConvert.ToInt32(GridWidth * 0.5));
			Grid.ColWidth(CNSTGRIDCOLPERIODS, FCConvert.ToInt32(GridWidth * 0.12));
			Grid.ColWidth(CNSTGRIDCOLTYPE, FCConvert.ToInt32(GridWidth * 0.1));
		}
		// vbPorter upgrade warning: intYear As short	OnWrite(double, int)
		private void FillGrid_2(int intYear)
		{
			FillGrid(intYear);
		}

		private void FillGrid(int intYear)
		{
			clsDRWrapper clsrate = new clsDRWrapper();
			string strCommit = "";
			Grid.Rows = 1;
			clsrate.OpenRecordset("select * from raterec  where  year = " + FCConvert.ToString(intYear) + " order by year desc,billingdate desc ,ID desc", "twcl0000.vb1");
			while (!clsrate.EndOfFile())
			{
				strCommit = Strings.Format(clsrate.Get_Fields_DateTime("commitmentdate"), "MM/dd/yyyy");
				if (strCommit != string.Empty)
				{
					if (FCConvert.ToDateTime(strCommit).Year == 1899)
					{
						strCommit = "";
					}
					else
					{
						strCommit = Strings.Format(clsrate.Get_Fields_DateTime("commitmentdate"), "MM/dd/yy");
					}
				}
				Grid.AddItem(clsrate.Get_Fields_Int32("ID") + "\t" + Strings.Format(Conversion.Val(clsrate.Get_Fields_Double("taxrate")) * 1000, "##0.000") + "\t" + clsrate.Get_Fields_String("description") + "\t" + clsrate.Get_Fields_Int16("numberofperiods") + "\t" + clsrate.Get_Fields_String("ratetype") + "\t" + Strings.Format(clsrate.Get_Fields_DateTime("billingdate"), "MM/dd/yyyy"));
				clsrate.MoveNext();
			}
			// Grid.AddItem (vbTab & vbTab & "Add New")
		}

		private void FillYearCombo()
		{
			clsDRWrapper clsrate = new clsDRWrapper();
			int intYear;
			clsrate.OpenRecordset("select * from defaultstable", "twbl0000.vb1");
			if (clsrate.EndOfFile())
			{
				clsrate.AddNew();
			}
			intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsrate.Get_Fields_Int16("defaultyear"))));
			FillGrid(intYear);
			clsrate.OpenRecordset("select year from raterec  group by year order by year desc ", "twcl0000.vb1");
			boolNotLoading = true;
			if (!clsrate.EndOfFile())
			{
				while (!clsrate.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
					cmbYear.AddItem(FCConvert.ToString(clsrate.Get_Fields("year")));
					// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(clsrate.Get_Fields("year")) == intYear)
						cmbYear.SelectedIndex = cmbYear.NewIndex;
					clsrate.MoveNext();
				}
			}
			else
			{
				cmbYear.AddItem(DateTime.Today.Year.ToString());
				cmbYear.SelectedIndex = 0;
			}
		}

		private void cmdPrintLabels_Click(object sender, EventArgs e)
		{
			mnuPrint_Click(mnuPrint, EventArgs.Empty);
		}
	}
}
