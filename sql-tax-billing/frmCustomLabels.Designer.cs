//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	partial class frmCustomLabels : BaseForm
	{
		public fecherFoundation.FCCheckBox chkChooseLabelStart;
		public fecherFoundation.FCCheckBox chkExclude;
		public FCGrid Grid;
		public fecherFoundation.FCFrame framYear;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtAlignment;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCFrame fraType;
		public fecherFoundation.FCComboBox cmbLabelType;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCLabel lblLaserAdjustment;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomLabels));
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.chkChooseLabelStart = new fecherFoundation.FCCheckBox();
            this.chkExclude = new fecherFoundation.FCCheckBox();
            this.Grid = new fecherFoundation.FCGrid();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.framYear = new fecherFoundation.FCFrame();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.txtAlignment = new fecherFoundation.FCTextBox();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.Line1 = new fecherFoundation.FCLine();
            this.fraType = new fecherFoundation.FCFrame();
            this.cmbLabelType = new fecherFoundation.FCComboBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.lblLaserAdjustment = new fecherFoundation.FCLabel();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdPrintLabels = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkChooseLabelStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExclude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framYear)).BeginInit();
            this.framYear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            this.vsLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
            this.fraType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintLabels);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(776, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.chkChooseLabelStart);
            this.ClientArea.Controls.Add(this.chkExclude);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.framYear);
            this.ClientArea.Controls.Add(this.txtAlignment);
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.lblLaserAdjustment);
            this.ClientArea.Size = new System.Drawing.Size(776, 520);
            this.ClientArea.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(776, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(84, 30);
            this.HeaderText.Text = "Labels";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // chkChooseLabelStart
            // 
            this.chkChooseLabelStart.Location = new System.Drawing.Point(476, 752);
            this.chkChooseLabelStart.Name = "chkChooseLabelStart";
            this.chkChooseLabelStart.Size = new System.Drawing.Size(229, 27);
            this.chkChooseLabelStart.TabIndex = 4;
            this.chkChooseLabelStart.Text = "Choose Label to Start From";
            this.ToolTip1.SetToolTip(this.chkChooseLabelStart, "Choose this option if this sheet is already partially printed on");
            this.chkChooseLabelStart.Visible = false;
            // 
            // chkExclude
            // 
            this.chkExclude.Location = new System.Drawing.Point(476, 645);
            this.chkExclude.Name = "chkExclude";
            this.chkExclude.Size = new System.Drawing.Size(255, 27);
            this.chkExclude.TabIndex = 7;
            this.chkExclude.Text = "Exclude 0 or negative balances";
            this.ToolTip1.SetToolTip(this.chkExclude, null);
            this.chkExclude.Visible = false;
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 6;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(321, 362);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(367, 266);
            this.Grid.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.Grid, null);
            this.Grid.Visible = false;
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Location = new System.Drawing.Point(33, 450);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(258, 178);
            this.fraSort.TabIndex = 3;
            this.fraSort.Text = "Fields To Sort By";
            this.ToolTip1.SetToolTip(this.fraSort, null);
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(219, 127);
            this.lstSort.Style = 1;
            this.ToolTip1.SetToolTip(this.lstSort, null);
            // 
            // framYear
            // 
            this.framYear.Controls.Add(this.cmbYear);
            this.framYear.Location = new System.Drawing.Point(321, 252);
            this.framYear.Name = "framYear";
            this.framYear.Size = new System.Drawing.Size(123, 90);
            this.framYear.TabIndex = 5;
            this.framYear.Text = "Year";
            this.ToolTip1.SetToolTip(this.framYear, null);
            this.framYear.Visible = false;
            // 
            // cmbYear
            // 
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.Location = new System.Drawing.Point(20, 30);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(83, 40);
            this.ToolTip1.SetToolTip(this.cmbYear, null);
            this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
            // 
            // txtAlignment
            // 
            this.txtAlignment.BackColor = System.Drawing.SystemColors.Window;
            this.txtAlignment.Location = new System.Drawing.Point(651, 692);
            this.txtAlignment.Name = "txtAlignment";
            this.txtAlignment.Size = new System.Drawing.Size(64, 40);
            this.txtAlignment.TabIndex = 9;
            this.txtAlignment.Text = "0";
            this.ToolTip1.SetToolTip(this.txtAlignment, "Laser line adjustment expressed in lines");
            this.txtAlignment.Visible = false;
            // 
            // fraWhere
            // 
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 645);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(416, 168);
            this.fraWhere.TabIndex = 4;
            this.fraWhere.Text = "Select Search Criteria";
            this.ToolTip1.SetToolTip(this.fraWhere, null);
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Cols = 4;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.ExtendLastCol = true;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(20, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.ShowFocusCell = false;
            this.vsWhere.Size = new System.Drawing.Size(375, 122);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.ToolTip1.SetToolTip(this.vsWhere, null);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Location = new System.Drawing.Point(33, 252);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(258, 178);
            this.fraFields.TabIndex = 2;
            this.fraFields.Text = "Label Type";
            this.ToolTip1.SetToolTip(this.fraFields, null);
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(219, 127);
            this.ToolTip1.SetToolTip(this.lstFields, null);
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.vsLayout);
            this.Frame1.Controls.Add(this.fraType);
            this.Frame1.Controls.Add(this.fraMessage);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(700, 210);
            this.Frame1.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // vsLayout
            // 
            this.vsLayout.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Both;
            this.vsLayout.Cols = 1;
            this.vsLayout.ColumnHeadersVisible = false;
            this.vsLayout.Controls.Add(this.Line1);
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.FixedRows = 0;
            this.vsLayout.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.Rows = 4;
            this.vsLayout.ShowFocusCell = false;
            this.vsLayout.Size = new System.Drawing.Size(414, 202);
            this.ToolTip1.SetToolTip(this.vsLayout, null);
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Line1.LineWidth = 2;
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(2, 376);
            this.ToolTip1.SetToolTip(this.Line1, null);
            this.Line1.Y2 = 376F;
            // 
            // fraType
            // 
            this.fraType.Controls.Add(this.cmbLabelType);
            this.fraType.Controls.Add(this.lblDescription);
            this.fraType.Location = new System.Drawing.Point(443, 0);
            this.fraType.Name = "fraType";
            this.fraType.Size = new System.Drawing.Size(257, 162);
            this.fraType.TabIndex = 1;
            this.fraType.Text = "Label Type";
            this.ToolTip1.SetToolTip(this.fraType, null);
            // 
            // cmbLabelType
            // 
            this.cmbLabelType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLabelType.Location = new System.Drawing.Point(20, 30);
            this.cmbLabelType.Name = "cmbLabelType";
            this.cmbLabelType.Size = new System.Drawing.Size(217, 40);
            this.ToolTip1.SetToolTip(this.cmbLabelType, null);
            this.cmbLabelType.SelectedIndexChanged += new System.EventHandler(this.cmbLabelType_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(20, 90);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(217, 55);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "LABEL1";
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // fraMessage
            // 
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.Location = new System.Drawing.Point(10, 40);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(371, 88);
            this.fraMessage.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.fraMessage, null);
            this.fraMessage.Visible = false;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(24, 23);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(321, 48);
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry2});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // lblLaserAdjustment
            // 
            this.lblLaserAdjustment.Location = new System.Drawing.Point(476, 706);
            this.lblLaserAdjustment.Name = "lblLaserAdjustment";
            this.lblLaserAdjustment.Size = new System.Drawing.Size(148, 18);
            this.lblLaserAdjustment.TabIndex = 8;
            this.lblLaserAdjustment.Text = "LASER LINE ADJUSTMENT";
            this.lblLaserAdjustment.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblLaserAdjustment, null);
            this.lblLaserAdjustment.Visible = false;
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuSepar,
            this.mnuPrint,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 2;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print Labels";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 3;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrintLabels
            // 
            this.cmdPrintLabels.AppearanceKey = "acceptButton";
            this.cmdPrintLabels.Location = new System.Drawing.Point(128, 27);
            this.cmdPrintLabels.Name = "cmdPrintLabels";
            this.cmdPrintLabels.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintLabels.Size = new System.Drawing.Size(147, 48);
            this.cmdPrintLabels.Text = "Print Labels";
            this.cmdPrintLabels.Click += new System.EventHandler(this.cmdPrintLabels_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(894, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(147, 24);
            this.cmdClear.TabIndex = 1;
            this.cmdClear.Text = "Clear Search Criteria";
            this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // frmCustomLabels
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(776, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomLabels";
            this.Text = "Labels";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCustomLabels_Load);
            this.Resize += new System.EventHandler(this.frmCustomLabels_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomLabels_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkChooseLabelStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExclude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.framYear)).EndInit();
            this.framYear.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            this.vsLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
            this.fraType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrintLabels;
		private FCButton cmdClear;
	}
}