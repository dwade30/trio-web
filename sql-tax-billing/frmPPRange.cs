﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmPPRange.
	/// </summary>
	public partial class frmPPRange : BaseForm
	{
		public frmPPRange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPRange InstancePtr
		{
			get
			{
				return (frmPPRange)Sys.GetInstance(typeof(frmPPRange));
			}
		}

		protected frmPPRange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public bool boolFromSupplemental;

		private void frmPPRange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuCancel_Click();
			}
		}

		private void frmPPRange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPRange properties;
			//frmPPRange.ScaleWidth	= 5880;
			//frmPPRange.ScaleHeight	= 3810;
			//frmPPRange.LinkTopic	= "Form1";
			//frmPPRange.LockControls	= true;
			//End Unmaped Properties
			clsDRWrapper clsTemp = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			clsTemp.OpenRecordset("select * from PPRATIOOPENS", "twpp0000.vb1");
			if (cmbAccounts.Items.Contains("Open1"))
			{
				cmbAccounts.Items.Insert(3, Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield1"))));
				cmbAccounts.Items.Remove("Open1");
			}
			if (cmbAccounts.Items.Contains("Open2"))
			{
				cmbAccounts.Items.Insert(4, Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield2"))));
				cmbAccounts.Items.Remove("Open2");
			}
			modGlobalVariables.Statics.CancelledIt = false;
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			Close();
		}

		public void mnuCancel_Click()
		{
			mnuCancel_Click(mnuCancel, new System.EventArgs());
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			string strBegin = "";
			string strEnd = "";
			int intWhich = 0;
			if (!boolFromSupplemental)
			{
				if (cmbAccounts.SelectedIndex == 0)
				{
					frmPPAudit.InstancePtr.intWhich = -1;
				}
				else
				{
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						MessageBox.Show("You must specify a start range", "Specify Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						frmPPAudit.InstancePtr.intWhich = 0;
						if (Conversion.Val(txtStart.Text) > 0)
						{
							frmPPAudit.InstancePtr.strBegin = txtStart.Text;
							if (Conversion.Val(txtEnd.Text) > 0)
							{
								frmPPAudit.InstancePtr.strEnd = txtEnd.Text;
							}
							else
							{
								frmPPAudit.InstancePtr.strEnd = txtStart.Text;
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						if (cmbRange.SelectedIndex == 1)
						{
							frmPPAudit.InstancePtr.intWhich = 1;
						}
						else if (cmbRange.SelectedIndex == 2)
						{
							frmPPAudit.InstancePtr.intWhich = 2;
						}
						else if (cmbRange.SelectedIndex == 3)
						{
							frmPPAudit.InstancePtr.intWhich = 3;
						}
						else if (cmbRange.SelectedIndex == 4)
						{
							frmPPAudit.InstancePtr.intWhich = 4;
						}
						frmPPAudit.InstancePtr.strBegin = txtStart.Text;
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							if (cmbRange.SelectedIndex == 1)
							{
								frmPPAudit.InstancePtr.strEnd = txtStart.Text + "zzz";
							}
							else
							{
								frmPPAudit.InstancePtr.strEnd = txtStart.Text;
							}
						}
						else
						{
							if (cmbRange.SelectedIndex == 1)
							{
								frmPPAudit.InstancePtr.strEnd = txtEnd.Text + "zzz";
							}
							else
							{
								frmPPAudit.InstancePtr.strEnd = txtEnd.Text;
							}
						}
					}
				}
			}
			else
			{
				// from supplemental
				if (cmbAccounts.SelectedIndex == 0)
				{
					// all
					frmSupplementalBill.InstancePtr.TransferPP(-1);
				}
				else
				{
					// range
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						MessageBox.Show("You must specify a start range", "Specify Start", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						if (Conversion.Val(txtStart.Text) > 0)
						{
							strBegin = txtStart.Text;
							if (Conversion.Val(txtEnd.Text) > 0)
							{
								strEnd = txtEnd.Text;
							}
							else
							{
								strEnd = txtStart.Text;
							}
						}
						else
						{
							MessageBox.Show("Invalid Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							MessageBox.Show("You must specify an end range", "Specify End", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						strBegin = txtStart.Text;
						strEnd = txtEnd.Text;
					}
					if (cmbRange.SelectedIndex == 0)
					{
						// account
						intWhich = 0;
					}
					else if (cmbRange.SelectedIndex == 1)
					{
						// name
						intWhich = 1;
					}
					else if (cmbRange.SelectedIndex == 2)
					{
						// location
						intWhich = 2;
					}
					else if (cmbRange.SelectedIndex == 3)
					{
						// open1
						intWhich = 3;
					}
					else if (cmbRange.SelectedIndex == 4)
					{
						// open2
						intWhich = 4;
					}
					frmSupplementalBill.InstancePtr.TransferPP(intWhich, strBegin, strEnd);
				}
			}
			Close();
		}

		private void optAccounts_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 1)
			{
				txtStart.Visible = true;
				txtEnd.Visible = true;
				Label1.Visible = true;
				cmbRange.Visible = true;
				lblRange.Visible = true;
			}
			else
			{
				txtStart.Visible = false;
				txtEnd.Visible = false;
				Label1.Visible = false;
				cmbRange.Visible = false;
				lblRange.Visible = false;
			}
		}

		private void optAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbAccounts.SelectedIndex);
			optAccounts_CheckedChanged(index, sender, e);
		}

		private void cmbAccounts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAccounts.SelectedIndex == 0)
			{
				optAccounts_CheckedChanged(sender, e);
			}
			else if (cmbAccounts.SelectedIndex == 1)
			{
				optAccounts_CheckedChanged(sender, e);
			}
		}
	}
}
