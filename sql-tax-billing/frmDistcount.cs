﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmDiscount.
	/// </summary>
	public partial class frmDiscount : BaseForm
	{
		//FC:FINAL:RPU: #i1546 - Add a flag to see if the closing was called from Save button
		bool saveBtn = false;

		public frmDiscount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDiscount InstancePtr
		{
			get
			{
				return (frmDiscount)Sys.GetInstance(typeof(frmDiscount));
			}
		}

		protected frmDiscount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolModal;

		private void frmDiscount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuSave_Click();
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
			}
		}

		private void frmDiscount_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDiscount properties;
			//frmDiscount.ScaleWidth	= 4500;
			//frmDiscount.ScaleHeight	= 2415;
			//frmDiscount.LinkTopic	= "Form1";
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from discount", "twbl0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
				txtPercent.Text = Strings.Format(clsLoad.Get_Fields("discount"), "#0.00");
				// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
				txtDate.Text = FCConvert.ToString(clsLoad.Get_Fields("duedate"));
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu && boolModal)
				e.Cancel = true;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (!boolModal)
			{
				//MDIParent.InstancePtr.Show();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gboolCancelForm = true;
			Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			txtPercent.Text = Strings.Format(txtPercent.Text, "#0.00");
			clsSave.OpenRecordset("select * from discount", "twbl0000.vb1");
			if (clsSave.EndOfFile())
			{
				clsSave.AddNew();
			}
			else
			{
				clsSave.Edit();
			}
			clsSave.Set_Fields("discount", FCConvert.ToString(Conversion.Val(txtPercent.Text)));
			clsSave.Set_Fields("duedate", Strings.Trim(txtDate.Text));
			clsSave.Update();
			modGlobalVariables.Statics.gboolCancelForm = false;
			//FC:FINAL:RPU:#i1546 - Set flag saveBtn
			saveBtn = true;
			Close();
			saveBtn = false;
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

        //FC:FINAL:AM:#4076 - change type to T2KDateBox
        //private void txtDate_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtDate.Text.Length == 2 || txtDate.Text.Length == 5) && Strings.Right(txtDate.Text, 1) != "/")
        //	{
        //		txtDate.Text = txtDate.Text + "/";
        //		txtDate.SelectionStart = txtDate.Text.Length;
        //		txtDate.SelectionLength = txtDate.Text.Length;
        //		//FC:FINAL:CHN - issue #1486: Not updated TextBox.
        //		FCUtils.ApplicationUpdate(txtDate);
        //	}
        //}

        //private void txtDate_Enter(object sender, System.EventArgs e)
        //{
        //	txtDate.SelectionStart = 0;
        //	txtDate.SelectionLength = Strings.Trim(txtDate.Text).Length;
        //}

        private void txtPercent_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtPercent.Text = Strings.Format(txtPercent.Text, "#0.00");
		}

		public void Init()
		{
			boolModal = true;
			this.Show(FormShowEnum.Modal);
		}

		private void cmdSaveAndExit_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}
	}
}
