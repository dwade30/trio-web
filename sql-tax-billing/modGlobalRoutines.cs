﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBL0000
{
	public class modGlobalRoutines
	{
		//=========================================================
		public static int GetNextReceiptNumber()
		{
			int GetNextReceiptNumber = 0;
			GetNextReceiptNumber = 0;
			return GetNextReceiptNumber;
		}
		// vbPorter upgrade warning: intValue As object	OnWrite(int, string, double)
		// vbPorter upgrade warning: intLength As short	OnWrite(short, string)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		//FC:FINAL:BBE - unused
		//public static string PadToString_6(ref object intValue, short intLength)
		//{
		//	return PadToString(intValue, intLength);
		//}
		//public static string PadToString_8(object intValue, short intLength)
		//{
		//	return PadToString(intValue, intLength);
		//}
		public static string PadToString(object intValue, int intLength)
		{
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			// vbPorter upgrade warning: strTemp As string	OnWrite(string, short)
			string strTemp = "";
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			if (Information.IsNumeric(intValue))
			{
				strTemp = Conversion.Str(FCConvert.ToString(Conversion.Val(intValue)));
			}
			else
			{
				strTemp = FCConvert.ToString(0);
			}
			strTemp = Strings.Trim(strTemp);
			if (strTemp.Length > intLength)
			{
				PadToString = Strings.Right(strTemp, intLength);
			}
			else
			{
				PadToString = Strings.StrDup(intLength - strTemp.Length, "0") + strTemp;
			}
			return PadToString;
		}

		public static string DetermineFileNameFromMask_8(string strMask, string strDir)
		{
			return DetermineFileNameFromMask(ref strMask, ref strDir);
		}

		public static string DetermineFileNameFromMask(ref string strMask, ref string strDir)
		{
			string DetermineFileNameFromMask = "";
			string strFileName;
			int intOldest;
			int intHighest;
			int intNumberToUse;
			//FileSystemObject fso = new FileSystemObject();
			FileInfo flFile;
			DateTime dtCurrent;
			DateTime dtOldest;
			// this will return a nullstring if something went wrong
			DetermineFileNameFromMask = "";
			strFileName = FCFileSystem.Dir(strDir + strMask, 0);
			intNumberToUse = 1;
			intOldest = 0;
			intHighest = 0;
			dtOldest = DateTime.Now;
			// go through all files with this name format
			// check to see if all 9 have been used
			// check to see which one is the oldest
			while (strFileName != string.Empty)
			{
				if (Conversion.Val(Strings.Mid(Strings.Right(strFileName, 6), 1, 2)) > intHighest)
					intHighest = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Strings.Right(strFileName, 6), 1, 2))));
				// get the file information
				flFile = new FileInfo(strDir + strFileName);
				dtCurrent = flFile.LastWriteTime;
				if (DateAndTime.DateDiff("n", dtOldest, dtCurrent) < 0)
				{
					// this one is older
					dtOldest = dtCurrent;
					intOldest = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Strings.Right(strFileName, 6), 1, 2))));
				}
				strFileName = FCFileSystem.Dir();
			}
			if (intHighest >= 9)
			{
				// use the oldest file
				intNumberToUse = intOldest;
			}
			else if (intHighest > 0)
			{
				intNumberToUse = intHighest + 1;
			}
			strFileName = strMask;
			strFileName = Strings.Mid(strFileName, 1, 6) + Strings.Format(intNumberToUse, "00") + Strings.Right(strFileName, 4);
			// if the file exists, then you must delete it
			if (File.Exists(strDir + strFileName))
			{
				File.Delete(strDir + strFileName);
			}
			DetermineFileNameFromMask = strFileName;
			return DetermineFileNameFromMask;
		}

		public static void ParseMapLot(string strMapLot, ref string strOut1, ref string strOut2, ref string strOut3, ref string strOut4)
		{
			int intPosition;
			int x;
			intPosition = Strings.InStr(1, strMapLot, "-", CompareConstants.vbTextCompare);
			x = 1;
			if (intPosition > 0 && intPosition < 5)
			{
				while ((strMapLot.Length > 0))
				{
					if (intPosition == 0)
					{
						intPosition = strMapLot.Length + 1;
					}
					switch (x)
					{
						case 1:
							{
								strOut1 = Strings.Mid(strMapLot, 1, intPosition - 1);
								break;
							}
						case 2:
							{
								strOut2 = Strings.Mid(strMapLot, 1, intPosition - 1);
								break;
							}
						case 3:
							{
								strOut3 = Strings.Mid(strMapLot, 1, intPosition - 1);
								break;
							}
						case 4:
							{
								strOut4 = Strings.Mid(strMapLot, 1, intPosition - 1);
								break;
							}
					}
					//end switch
					if (intPosition > strMapLot.Length)
						break;
					strMapLot = Strings.Mid(strMapLot, intPosition + 1);
					intPosition = Strings.InStr(1, strMapLot, "-", CompareConstants.vbBinaryCompare);
					x += 1;
				}
			}
			else
			{
				strOut1 = Strings.Mid(strMapLot, 1, 4);
				strOut2 = Strings.Mid(strMapLot, 5, 4);
				strOut3 = Strings.Mid(strMapLot, 9, 4);
				strOut4 = Strings.Mid(strMapLot, 13, 4);
			}
		}

		public static void SaveDefaultTaxMessage()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strMessage;
			int intFormat;
			int x;
			int intMaxChars = 0;
			int intCurrChars;
			int intCurrLine;
			string[] strLine = new string[11 + 1];
			string strTemp = "";
			// try to determine which bill format to save this for.
			strMessage = "Without state aid to education, state revenue sharing and state reimbursement for the Maine resident homestead ";
			strMessage += "property tax exemption, your tax bill would have been xx.x% higher. Total Bonded Indebtedness as of XX/XX/XXXX: $X,XXX,XXX";
			clsLoad.OpenRecordset("select * FROM billsetup", "twbl0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [billformat] and replace with corresponding Get_Field method
				intFormat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("billformat"))));
			}
			else
			{
				intFormat = 0;
			}
			switch (intFormat)
			{
				case 1:
				case 4:
					{
						intMaxChars = 48;
						break;
					}
				case 2:
				case 3:
					{
						intMaxChars = 36;
						break;
					}
				case 5:
				case 6:
					{
						intMaxChars = 30;
						break;
					}
				case 8:
					{
						intMaxChars = 49;
						break;
					}
				case 9:
				case 10:
				case 12:
					{
						intMaxChars = 42;
						break;
					}
				case 11:
					{
						intMaxChars = 75;
						break;
					}
				case 14:
				case 15:
				case 16:
					{
						intMaxChars = 88;
						break;
					}
				default:
					{
						intMaxChars = 30;
						break;
					}
			}
			//end switch
			for (x = 0; x <= 11; x++)
			{
				strLine[x] = string.Empty;
			}
			// x
			intCurrLine = 0;
			while (Strings.Trim(strMessage) != string.Empty)
			{
				if (strMessage.Length <= intMaxChars)
				{
					strLine[intCurrLine] = strMessage;
					strMessage = string.Empty;
				}
				else
				{
					strTemp = Strings.Mid(strMessage, 1, intMaxChars);
					if (Strings.Right(strTemp, 1) == " ")
					{
						strLine[intCurrLine] = Strings.Trim(strTemp);
						strMessage = Strings.Mid(strMessage, strTemp.Length + 1);
					}
					else
					{
						x = Strings.InStrRev(strTemp, " ", -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Mid(strTemp, 1, x - 1);
						strLine[intCurrLine] = Strings.Trim(strTemp);
						strMessage = Strings.Mid(strMessage, strTemp.Length + 1);
					}
				}
				intCurrLine += 1;
				strMessage = Strings.Trim(strMessage);
			}
			clsLoad.Execute("delete from taxmessage", "twbl0000.vb1");
			clsLoad.OpenRecordset("select * from taxmessage", "twbl0000.vb1");
			for (x = 0; x <= 11; x++)
			{
				clsLoad.AddNew();
				clsLoad.Set_Fields("line", x);
				clsLoad.Set_Fields("Message", strLine[x]);
				clsLoad.Update();
			}
			// x
		}

		public static string escapequote(string a)
		{
			string escapequote = "";
			// searches for a singlequote and puts another next to it
			string tstring = "";
			string t2string = "";
			int stringindex;
			int intpos = 0;
			escapequote = a;
			if (a == string.Empty)
				return escapequote;
			stringindex = 1;
			while (stringindex <= a.Length)
			{
				intpos = Strings.InStr(stringindex, a, "'", CompareConstants.vbBinaryCompare);
				if (intpos == 0)
					break;
				tstring = Strings.Mid(a, 1, intpos);
				t2string = Strings.Mid(a, intpos + 1);
				if (intpos == a.Length)
				{
					// a = tstring & "' "
					a = Strings.Mid(a, 1, intpos - 1);
					break;
				}
				else
				{
					a = tstring + "'" + t2string;
				}
				stringindex = intpos + 2;
			}
			escapequote = a;
			return escapequote;
		}
	}
}
