﻿namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptPPAudit.
	/// </summary>
	partial class rptPPAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPPAudit));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtTotValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txttotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCatBETE1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot1BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatBETE4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot4BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatBETE7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot7BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatBETE2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot2BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatBETE5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot5BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatBETE8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot8BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatBETE3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot3BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatBETE6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot6BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatBETE9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCatTot9BETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot1BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot4BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot7BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot2BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot5BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot8BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot3BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot6BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot9BETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAcct,
            this.txtName,
            this.txtValue,
            this.txtExemption,
            this.txtAssess,
            this.txtTax,
            this.txtLocation});
            this.Detail.Height = 0.3229167F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtAcct
            // 
            this.txtAcct.Height = 0.19F;
            this.txtAcct.Left = 0F;
            this.txtAcct.Name = "txtAcct";
            this.txtAcct.Text = "Field1";
            this.txtAcct.Top = 0F;
            this.txtAcct.Width = 0.5625F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.19F;
            this.txtName.Left = 0.5625F;
            this.txtName.Name = "txtName";
            this.txtName.Text = "Field2";
            this.txtName.Top = 0F;
            this.txtName.Width = 2.0625F;
            // 
            // txtValue
            // 
            this.txtValue.Height = 0.19F;
            this.txtValue.Left = 2.625F;
            this.txtValue.Name = "txtValue";
            this.txtValue.Style = "text-align: right";
            this.txtValue.Text = null;
            this.txtValue.Top = 0F;
            this.txtValue.Width = 1.0625F;
            // 
            // txtExemption
            // 
            this.txtExemption.Height = 0.19F;
            this.txtExemption.Left = 3.75F;
            this.txtExemption.Name = "txtExemption";
            this.txtExemption.Style = "text-align: right";
            this.txtExemption.Text = "Field1";
            this.txtExemption.Top = 0F;
            this.txtExemption.Width = 1.25F;
            // 
            // txtAssess
            // 
            this.txtAssess.Height = 0.19F;
            this.txtAssess.Left = 5.0625F;
            this.txtAssess.Name = "txtAssess";
            this.txtAssess.Style = "text-align: right";
            this.txtAssess.Text = "Field1";
            this.txtAssess.Top = 0F;
            this.txtAssess.Width = 1.1875F;
            // 
            // txtTax
            // 
            this.txtTax.Height = 0.19F;
            this.txtTax.Left = 6.3125F;
            this.txtTax.Name = "txtTax";
            this.txtTax.Style = "text-align: right";
            this.txtTax.Text = "Field1";
            this.txtTax.Top = 0F;
            this.txtTax.Width = 1.125F;
            // 
            // txtLocation
            // 
            this.txtLocation.Height = 0.19F;
            this.txtLocation.Left = 0.5625F;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Text = null;
            this.txtLocation.Top = 0.15625F;
            this.txtLocation.Width = 2.0625F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.CanGrow = false;
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Visible = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotValue,
            this.txttotExempt,
            this.txtTotAssess,
            this.txtTotTax,
            this.Label8,
            this.txtCat1,
            this.txtCatTot1,
            this.txtCat4,
            this.txtCatTot4,
            this.txtCat7,
            this.txtCatTot7,
            this.txtCat2,
            this.txtCatTot2,
            this.txtCat5,
            this.txtCatTot5,
            this.txtCat8,
            this.txtCatTot8,
            this.txtCat3,
            this.txtCatTot3,
            this.txtCat6,
            this.txtCatTot6,
            this.txtCat9,
            this.txtCatTot9,
            this.txtCount,
            this.Label10,
            this.txtCatBETE1,
            this.txtCatTot1BETE,
            this.txtCatBETE4,
            this.txtCatTot4BETE,
            this.txtCatBETE7,
            this.txtCatTot7BETE,
            this.txtCatBETE2,
            this.txtCatTot2BETE,
            this.txtCatBETE5,
            this.txtCatTot5BETE,
            this.txtCatBETE8,
            this.txtCatTot8BETE,
            this.txtCatBETE3,
            this.txtCatTot3BETE,
            this.txtCatBETE6,
            this.txtCatTot6BETE,
            this.txtCatBETE9,
            this.txtCatTot9BETE});
            this.ReportFooter.Height = 1.739583F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // txtTotValue
            // 
            this.txtTotValue.Height = 0.19F;
            this.txtTotValue.Left = 2.375F;
            this.txtTotValue.Name = "txtTotValue";
            this.txtTotValue.Style = "font-weight: bold; text-align: right";
            this.txtTotValue.Text = "Field1";
            this.txtTotValue.Top = 0.25F;
            this.txtTotValue.Width = 1.3125F;
            // 
            // txttotExempt
            // 
            this.txttotExempt.Height = 0.19F;
            this.txttotExempt.Left = 3.75F;
            this.txttotExempt.Name = "txttotExempt";
            this.txttotExempt.Style = "font-weight: bold; text-align: right";
            this.txttotExempt.Text = "Field2";
            this.txttotExempt.Top = 0.25F;
            this.txttotExempt.Width = 1.25F;
            // 
            // txtTotAssess
            // 
            this.txtTotAssess.Height = 0.19F;
            this.txtTotAssess.Left = 5.0625F;
            this.txtTotAssess.Name = "txtTotAssess";
            this.txtTotAssess.Style = "font-weight: bold; text-align: right";
            this.txtTotAssess.Text = "Field3";
            this.txtTotAssess.Top = 0.25F;
            this.txtTotAssess.Width = 1.1875F;
            // 
            // txtTotTax
            // 
            this.txtTotTax.Height = 0.19F;
            this.txtTotTax.Left = 6.3125F;
            this.txtTotTax.Name = "txtTotTax";
            this.txtTotTax.Style = "font-weight: bold; text-align: right";
            this.txtTotTax.Text = "Field4";
            this.txtTotTax.Top = 0.25F;
            this.txtTotTax.Width = 1.125F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.19F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.0625F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-weight: bold";
            this.Label8.Text = "Total:";
            this.Label8.Top = 0.25F;
            this.Label8.Width = 1F;
            // 
            // txtCat1
            // 
            this.txtCat1.Height = 0.19F;
            this.txtCat1.Left = 0.0625F;
            this.txtCat1.MultiLine = false;
            this.txtCat1.Name = "txtCat1";
            this.txtCat1.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat1.Text = "Field1";
            this.txtCat1.Top = 0.40625F;
            this.txtCat1.Width = 1.4875F;
            // 
            // txtCatTot1
            // 
            this.txtCatTot1.Height = 0.19F;
            this.txtCatTot1.Left = 1.5F;
            this.txtCatTot1.MultiLine = false;
            this.txtCatTot1.Name = "txtCatTot1";
            this.txtCatTot1.Style = "text-align: right";
            this.txtCatTot1.Text = "Field2";
            this.txtCatTot1.Top = 0.40625F;
            this.txtCatTot1.Width = 1F;
            // 
            // txtCat4
            // 
            this.txtCat4.Height = 0.19F;
            this.txtCat4.Left = 2.5625F;
            this.txtCat4.MultiLine = false;
            this.txtCat4.Name = "txtCat4";
            this.txtCat4.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat4.Text = "Field3";
            this.txtCat4.Top = 0.40625F;
            this.txtCat4.Width = 1.4955F;
            // 
            // txtCatTot4
            // 
            this.txtCatTot4.Height = 0.19F;
            this.txtCatTot4.Left = 4F;
            this.txtCatTot4.MultiLine = false;
            this.txtCatTot4.Name = "txtCatTot4";
            this.txtCatTot4.Style = "text-align: right";
            this.txtCatTot4.Text = "Field4";
            this.txtCatTot4.Top = 0.40625F;
            this.txtCatTot4.Width = 1F;
            // 
            // txtCat7
            // 
            this.txtCat7.Height = 0.19F;
            this.txtCat7.Left = 5.0625F;
            this.txtCat7.MultiLine = false;
            this.txtCat7.Name = "txtCat7";
            this.txtCat7.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat7.Text = "Field5";
            this.txtCat7.Top = 0.40625F;
            this.txtCat7.Width = 1.4955F;
            // 
            // txtCatTot7
            // 
            this.txtCatTot7.Height = 0.19F;
            this.txtCatTot7.Left = 6.5F;
            this.txtCatTot7.MultiLine = false;
            this.txtCatTot7.Name = "txtCatTot7";
            this.txtCatTot7.Style = "text-align: right";
            this.txtCatTot7.Text = "Field6";
            this.txtCatTot7.Top = 0.40625F;
            this.txtCatTot7.Width = 1F;
            // 
            // txtCat2
            // 
            this.txtCat2.Height = 0.19F;
            this.txtCat2.Left = 0.0625F;
            this.txtCat2.MultiLine = false;
            this.txtCat2.Name = "txtCat2";
            this.txtCat2.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat2.Text = "Field7";
            this.txtCat2.Top = 0.5625F;
            this.txtCat2.Width = 1.4875F;
            // 
            // txtCatTot2
            // 
            this.txtCatTot2.Height = 0.19F;
            this.txtCatTot2.Left = 1.5F;
            this.txtCatTot2.MultiLine = false;
            this.txtCatTot2.Name = "txtCatTot2";
            this.txtCatTot2.Style = "text-align: right";
            this.txtCatTot2.Text = "Field8";
            this.txtCatTot2.Top = 0.5625F;
            this.txtCatTot2.Width = 1F;
            // 
            // txtCat5
            // 
            this.txtCat5.Height = 0.19F;
            this.txtCat5.Left = 2.5625F;
            this.txtCat5.MultiLine = false;
            this.txtCat5.Name = "txtCat5";
            this.txtCat5.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat5.Text = "Field9";
            this.txtCat5.Top = 0.5625F;
            this.txtCat5.Width = 1.4955F;
            // 
            // txtCatTot5
            // 
            this.txtCatTot5.Height = 0.19F;
            this.txtCatTot5.Left = 4F;
            this.txtCatTot5.MultiLine = false;
            this.txtCatTot5.Name = "txtCatTot5";
            this.txtCatTot5.Style = "text-align: right";
            this.txtCatTot5.Text = "Field10";
            this.txtCatTot5.Top = 0.5625F;
            this.txtCatTot5.Width = 1F;
            // 
            // txtCat8
            // 
            this.txtCat8.Height = 0.19F;
            this.txtCat8.Left = 5.0625F;
            this.txtCat8.MultiLine = false;
            this.txtCat8.Name = "txtCat8";
            this.txtCat8.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat8.Text = "Field11";
            this.txtCat8.Top = 0.5625F;
            this.txtCat8.Width = 1.4955F;
            // 
            // txtCatTot8
            // 
            this.txtCatTot8.Height = 0.19F;
            this.txtCatTot8.Left = 6.5F;
            this.txtCatTot8.MultiLine = false;
            this.txtCatTot8.Name = "txtCatTot8";
            this.txtCatTot8.Style = "text-align: right";
            this.txtCatTot8.Text = "Field12";
            this.txtCatTot8.Top = 0.5625F;
            this.txtCatTot8.Width = 1F;
            // 
            // txtCat3
            // 
            this.txtCat3.Height = 0.19F;
            this.txtCat3.Left = 0.0625F;
            this.txtCat3.MultiLine = false;
            this.txtCat3.Name = "txtCat3";
            this.txtCat3.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat3.Text = "Field13";
            this.txtCat3.Top = 0.71875F;
            this.txtCat3.Width = 1.4875F;
            // 
            // txtCatTot3
            // 
            this.txtCatTot3.Height = 0.19F;
            this.txtCatTot3.Left = 1.5F;
            this.txtCatTot3.MultiLine = false;
            this.txtCatTot3.Name = "txtCatTot3";
            this.txtCatTot3.Style = "text-align: right";
            this.txtCatTot3.Text = "Field14";
            this.txtCatTot3.Top = 0.71875F;
            this.txtCatTot3.Width = 1F;
            // 
            // txtCat6
            // 
            this.txtCat6.Height = 0.19F;
            this.txtCat6.Left = 2.5625F;
            this.txtCat6.MultiLine = false;
            this.txtCat6.Name = "txtCat6";
            this.txtCat6.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat6.Text = "Field15";
            this.txtCat6.Top = 0.71875F;
            this.txtCat6.Width = 1.4955F;
            // 
            // txtCatTot6
            // 
            this.txtCatTot6.Height = 0.19F;
            this.txtCatTot6.Left = 4F;
            this.txtCatTot6.MultiLine = false;
            this.txtCatTot6.Name = "txtCatTot6";
            this.txtCatTot6.Style = "text-align: right";
            this.txtCatTot6.Text = "Field16";
            this.txtCatTot6.Top = 0.71875F;
            this.txtCatTot6.Width = 1F;
            // 
            // txtCat9
            // 
            this.txtCat9.Height = 0.19F;
            this.txtCat9.Left = 5.0625F;
            this.txtCat9.MultiLine = false;
            this.txtCat9.Name = "txtCat9";
            this.txtCat9.Style = "font-weight: bold; white-space: nowrap";
            this.txtCat9.Text = "Field17";
            this.txtCat9.Top = 0.71875F;
            this.txtCat9.Width = 1.4955F;
            // 
            // txtCatTot9
            // 
            this.txtCatTot9.Height = 0.19F;
            this.txtCatTot9.Left = 6.5F;
            this.txtCatTot9.MultiLine = false;
            this.txtCatTot9.Name = "txtCatTot9";
            this.txtCatTot9.Style = "text-align: right";
            this.txtCatTot9.Text = "Field18";
            this.txtCatTot9.Top = 0.71875F;
            this.txtCatTot9.Width = 1F;
            // 
            // txtCount
            // 
            this.txtCount.Height = 0.19F;
            this.txtCount.Left = 1.0625F;
            this.txtCount.MultiLine = false;
            this.txtCount.Name = "txtCount";
            this.txtCount.Style = "font-weight: bold; text-align: left";
            this.txtCount.Text = "Count";
            this.txtCount.Top = 0.25F;
            this.txtCount.Width = 1.25F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.0625F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-weight: bold";
            this.Label10.Text = "BETE Exempt:";
            this.Label10.Top = 0.9375F;
            this.Label10.Width = 1.166667F;
            // 
            // txtCatBETE1
            // 
            this.txtCatBETE1.Height = 0.19F;
            this.txtCatBETE1.Left = 0.0625F;
            this.txtCatBETE1.MultiLine = false;
            this.txtCatBETE1.Name = "txtCatBETE1";
            this.txtCatBETE1.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE1.Text = "Field1";
            this.txtCatBETE1.Top = 1.15625F;
            this.txtCatBETE1.Width = 1.4875F;
            // 
            // txtCatTot1BETE
            // 
            this.txtCatTot1BETE.Height = 0.19F;
            this.txtCatTot1BETE.Left = 1.5F;
            this.txtCatTot1BETE.MultiLine = false;
            this.txtCatTot1BETE.Name = "txtCatTot1BETE";
            this.txtCatTot1BETE.Style = "text-align: right";
            this.txtCatTot1BETE.Text = "Field2";
            this.txtCatTot1BETE.Top = 1.15625F;
            this.txtCatTot1BETE.Width = 1F;
            // 
            // txtCatBETE4
            // 
            this.txtCatBETE4.Height = 0.19F;
            this.txtCatBETE4.Left = 2.5625F;
            this.txtCatBETE4.MultiLine = false;
            this.txtCatBETE4.Name = "txtCatBETE4";
            this.txtCatBETE4.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE4.Text = "Field3";
            this.txtCatBETE4.Top = 1.15625F;
            this.txtCatBETE4.Width = 1.4955F;
            // 
            // txtCatTot4BETE
            // 
            this.txtCatTot4BETE.Height = 0.19F;
            this.txtCatTot4BETE.Left = 4F;
            this.txtCatTot4BETE.MultiLine = false;
            this.txtCatTot4BETE.Name = "txtCatTot4BETE";
            this.txtCatTot4BETE.Style = "text-align: right";
            this.txtCatTot4BETE.Text = "Field4";
            this.txtCatTot4BETE.Top = 1.15625F;
            this.txtCatTot4BETE.Width = 1F;
            // 
            // txtCatBETE7
            // 
            this.txtCatBETE7.Height = 0.19F;
            this.txtCatBETE7.Left = 5.0625F;
            this.txtCatBETE7.MultiLine = false;
            this.txtCatBETE7.Name = "txtCatBETE7";
            this.txtCatBETE7.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE7.Text = "Field5";
            this.txtCatBETE7.Top = 1.15625F;
            this.txtCatBETE7.Width = 1.4955F;
            // 
            // txtCatTot7BETE
            // 
            this.txtCatTot7BETE.Height = 0.19F;
            this.txtCatTot7BETE.Left = 6.5F;
            this.txtCatTot7BETE.MultiLine = false;
            this.txtCatTot7BETE.Name = "txtCatTot7BETE";
            this.txtCatTot7BETE.Style = "text-align: right";
            this.txtCatTot7BETE.Text = "Field6";
            this.txtCatTot7BETE.Top = 1.15625F;
            this.txtCatTot7BETE.Width = 1F;
            // 
            // txtCatBETE2
            // 
            this.txtCatBETE2.Height = 0.19F;
            this.txtCatBETE2.Left = 0.0625F;
            this.txtCatBETE2.MultiLine = false;
            this.txtCatBETE2.Name = "txtCatBETE2";
            this.txtCatBETE2.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE2.Text = "Field7";
            this.txtCatBETE2.Top = 1.3125F;
            this.txtCatBETE2.Width = 1.4875F;
            // 
            // txtCatTot2BETE
            // 
            this.txtCatTot2BETE.Height = 0.19F;
            this.txtCatTot2BETE.Left = 1.5F;
            this.txtCatTot2BETE.MultiLine = false;
            this.txtCatTot2BETE.Name = "txtCatTot2BETE";
            this.txtCatTot2BETE.Style = "text-align: right";
            this.txtCatTot2BETE.Text = "Field8";
            this.txtCatTot2BETE.Top = 1.3125F;
            this.txtCatTot2BETE.Width = 1F;
            // 
            // txtCatBETE5
            // 
            this.txtCatBETE5.Height = 0.19F;
            this.txtCatBETE5.Left = 2.5625F;
            this.txtCatBETE5.MultiLine = false;
            this.txtCatBETE5.Name = "txtCatBETE5";
            this.txtCatBETE5.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE5.Text = "Field9";
            this.txtCatBETE5.Top = 1.3125F;
            this.txtCatBETE5.Width = 1.4955F;
            // 
            // txtCatTot5BETE
            // 
            this.txtCatTot5BETE.Height = 0.19F;
            this.txtCatTot5BETE.Left = 4F;
            this.txtCatTot5BETE.MultiLine = false;
            this.txtCatTot5BETE.Name = "txtCatTot5BETE";
            this.txtCatTot5BETE.Style = "text-align: right";
            this.txtCatTot5BETE.Text = "Field10";
            this.txtCatTot5BETE.Top = 1.3125F;
            this.txtCatTot5BETE.Width = 1F;
            // 
            // txtCatBETE8
            // 
            this.txtCatBETE8.Height = 0.19F;
            this.txtCatBETE8.Left = 5.0625F;
            this.txtCatBETE8.MultiLine = false;
            this.txtCatBETE8.Name = "txtCatBETE8";
            this.txtCatBETE8.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE8.Text = "Field11";
            this.txtCatBETE8.Top = 1.3125F;
            this.txtCatBETE8.Width = 1.4955F;
            // 
            // txtCatTot8BETE
            // 
            this.txtCatTot8BETE.Height = 0.19F;
            this.txtCatTot8BETE.Left = 6.5F;
            this.txtCatTot8BETE.MultiLine = false;
            this.txtCatTot8BETE.Name = "txtCatTot8BETE";
            this.txtCatTot8BETE.Style = "text-align: right";
            this.txtCatTot8BETE.Text = "Field12";
            this.txtCatTot8BETE.Top = 1.3125F;
            this.txtCatTot8BETE.Width = 1F;
            // 
            // txtCatBETE3
            // 
            this.txtCatBETE3.Height = 0.19F;
            this.txtCatBETE3.Left = 0.0625F;
            this.txtCatBETE3.MultiLine = false;
            this.txtCatBETE3.Name = "txtCatBETE3";
            this.txtCatBETE3.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE3.Text = "Field13";
            this.txtCatBETE3.Top = 1.46875F;
            this.txtCatBETE3.Width = 1.4875F;
            // 
            // txtCatTot3BETE
            // 
            this.txtCatTot3BETE.Height = 0.19F;
            this.txtCatTot3BETE.Left = 1.5F;
            this.txtCatTot3BETE.MultiLine = false;
            this.txtCatTot3BETE.Name = "txtCatTot3BETE";
            this.txtCatTot3BETE.Style = "text-align: right";
            this.txtCatTot3BETE.Text = "Field14";
            this.txtCatTot3BETE.Top = 1.46875F;
            this.txtCatTot3BETE.Width = 1F;
            // 
            // txtCatBETE6
            // 
            this.txtCatBETE6.Height = 0.19F;
            this.txtCatBETE6.Left = 2.5625F;
            this.txtCatBETE6.MultiLine = false;
            this.txtCatBETE6.Name = "txtCatBETE6";
            this.txtCatBETE6.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE6.Text = "Field15";
            this.txtCatBETE6.Top = 1.46875F;
            this.txtCatBETE6.Width = 1.4955F;
            // 
            // txtCatTot6BETE
            // 
            this.txtCatTot6BETE.Height = 0.19F;
            this.txtCatTot6BETE.Left = 4F;
            this.txtCatTot6BETE.MultiLine = false;
            this.txtCatTot6BETE.Name = "txtCatTot6BETE";
            this.txtCatTot6BETE.Style = "text-align: right";
            this.txtCatTot6BETE.Text = "Field16";
            this.txtCatTot6BETE.Top = 1.46875F;
            this.txtCatTot6BETE.Width = 1F;
            // 
            // txtCatBETE9
            // 
            this.txtCatBETE9.Height = 0.19F;
            this.txtCatBETE9.Left = 5.0625F;
            this.txtCatBETE9.MultiLine = false;
            this.txtCatBETE9.Name = "txtCatBETE9";
            this.txtCatBETE9.Style = "font-weight: bold; white-space: nowrap";
            this.txtCatBETE9.Text = "Field17";
            this.txtCatBETE9.Top = 1.46875F;
            this.txtCatBETE9.Width = 1.4955F;
            // 
            // txtCatTot9BETE
            // 
            this.txtCatTot9BETE.Height = 0.19F;
            this.txtCatTot9BETE.Left = 6.5F;
            this.txtCatTot9BETE.MultiLine = false;
            this.txtCatTot9BETE.Name = "txtCatTot9BETE";
            this.txtCatTot9BETE.Style = "text-align: right";
            this.txtCatTot9BETE.Text = "Field18";
            this.txtCatTot9BETE.Top = 1.46875F;
            this.txtCatTot9BETE.Width = 1F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtPage,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.txtMuniname,
            this.Label9,
            this.txtTime,
            this.txtDate});
            this.PageHeader.Height = 0.625F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.19F;
            this.txtPage.Left = 6.4375F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "text-align: right";
            this.txtPage.Text = "Field1";
            this.txtPage.Top = 0.15625F;
            this.txtPage.Width = 0.9375F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.19F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "";
            this.Label2.Text = "Acct";
            this.Label2.Top = 0.46875F;
            this.Label2.Width = 0.5F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.19F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.5625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "";
            this.Label3.Text = "Name";
            this.Label3.Top = 0.46875F;
            this.Label3.Width = 1.625F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.19F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 2.625F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "text-align: right";
            this.Label4.Text = "Value";
            this.Label4.Top = 0.46875F;
            this.Label4.Width = 1.0625F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.19F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 3.75F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "text-align: right";
            this.Label5.Text = "Exemption";
            this.Label5.Top = 0.46875F;
            this.Label5.Width = 1.25F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.19F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 5.0625F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "text-align: right";
            this.Label6.Text = "Assessment";
            this.Label6.Top = 0.46875F;
            this.Label6.Width = 1.1875F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.19F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 6.3125F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "text-align: right";
            this.Label7.Text = "Tax";
            this.Label7.Top = 0.46875F;
            this.Label7.Width = 1.125F;
            // 
            // txtMuniname
            // 
            this.txtMuniname.Height = 0.19F;
            this.txtMuniname.Left = 0F;
            this.txtMuniname.MultiLine = false;
            this.txtMuniname.Name = "txtMuniname";
            this.txtMuniname.Style = "text-align: left";
            this.txtMuniname.Text = "Field1";
            this.txtMuniname.Top = 0F;
            this.txtMuniname.Width = 1.6875F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.21875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 1.6875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.Label9.Text = "Personal Property Audit Report";
            this.Label9.Top = 0F;
            this.Label9.Width = 4F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.19F;
            this.txtTime.Left = 0F;
            this.txtTime.MultiLine = false;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "text-align: left";
            this.txtTime.Text = "Field1";
            this.txtTime.Top = 0.15625F;
            this.txtTime.Width = 1.3125F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.19F;
            this.txtDate.Left = 6.0625F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "text-align: right";
            this.txtDate.Text = "Field1";
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1.3125F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.01041667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptPPAudit
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotAssess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCat9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot1BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot4BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot7BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot2BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot5BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot8BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot3BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot6BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatBETE9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatTot9BETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot1BETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot4BETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot7BETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot2BETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot5BETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot8BETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot3BETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot6BETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatBETE9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTot9BETE;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
