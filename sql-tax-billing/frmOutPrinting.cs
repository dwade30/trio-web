﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmOutPrinting.
	/// </summary>
	public partial class frmOutPrinting : BaseForm
	{
		public frmOutPrinting()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmOutPrinting InstancePtr
		{
			get
			{
				return (frmOutPrinting)Sys.GetInstance(typeof(frmOutPrinting));
			}
		}

		protected frmOutPrinting _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strREFormat = "";
		/// <summary>
		/// these strings hold the formate description
		/// </summary>
		string strCommaFormat = "";
		string strREEnhanced = "";
		string strHeaderFormat;
		int lngRateRecToPrint;
		double dblTotalOriginalRETax;
		double dblTotalOriginalPPTax;
		double dblTotalREPrePayments;
		double dblTotalPPPrePayments;
		double dblTotalRENetTaxOwed;
		double dblTotalPPNetTaxOwed;
		double dblTotalREAssess;
		double dblTotalPPAssess;
		int lngNumNewNames;
		int lngMHCopies;
		int lngNumIPs;
		double dblTotalCopiesRENetOwed;
		string[] strCatDesc = new string[10 + 1];
		cSettingsController setCont = new cSettingsController();

		private void chkDiscount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDiscount.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtPercent.Enabled = true;
				txtDate.Enabled = true;
			}
			else
			{
				txtPercent.Enabled = false;
				txtDate.Enabled = false;
			}
		}

		private void chkEnhanced_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkEnhanced.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (cmbFormat.SelectedIndex == 0)
				{
					RichTextBox1.Text = strREEnhanced;
				}
			}
			else
			{
				if (cmbFormat.SelectedIndex == 0)
				{
					RichTextBox1.Text = strREFormat;
				}
			}
		}

		private void chkIncludeInformationSection_Click()
		{
			FillStrings();
		}

		private void chkInlcude3rdAddress_Click()
		{
			FillStrings();
		}

		private void chkIncludePastDue_Click()
		{
			FillStrings();
		}

		private void cmdPrint_Click()
		{
			// MDIParent.InstancePtr.CommonDialog1_Save.Flags = (vbPorterConverter.cdlPDReturnDC+vbPorterConverter.cdlPDNoPageNums)	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			if (RichTextBox1.SelLength == 0)
			{
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0 /*? MDIParent.InstancePtr.CommonDialog1_Save.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDAllPages	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0 /*? MDIParent.InstancePtr.CommonDialog1_Save.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDSelection	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//Information.Err().Clear();
				////FC:FINAL:DDU:#i1563 - initialize CommonDialog1
				//MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
				//MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
				//if (Information.Err().Number == 0)
				//{
				RichTextBox1.SelPrint(0);
				//	FCGlobal.Printer.EndDoc();
				//}
			}
		}

		private void frmOutPrinting_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmOutPrinting_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmOutPrinting properties;
			//frmOutPrinting.ScaleWidth	= 9225;
			//frmOutPrinting.ScaleHeight	= 8310;
			//frmOutPrinting.LinkTopic	= "Form1";
			//RichTextBox1 properties;
			//RichTextBox1.ScrollBars	= 2;
			//RichTextBox1.ReadOnly	= true;
			//RichTextBox1.TextRTF	= $"frmOutPrinting.frx":0617;
			//End Unmaped Properties
			frmDistribution.InstancePtr.Init(5, true);
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:CHN - issue #1519: Incorrect text color.
			// Label1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			// Label2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			FillStrings();
			string strTemp;
			// strTemp = setCont.GetSettingValue("OutprintUseThirdAddressLine", "TaxBilling", "", "", "")
			// If Trim(LCase(strTemp)) = "true" Then
			// chkInlcude3rdAddress.Value = vbChecked
			// Else
			// chkInlcude3rdAddress.Value = vbUnchecked
			// End If
			strTemp = setCont.GetSettingValue("OutprintIndebtednessDate", "TaxBilling", "", "", "");
			if (Information.IsDate(strTemp))
			{
				t2kIndebtednessDate.Text = strTemp;
			}
			strTemp = setCont.GetSettingValue("OutprintIndebtednessAmount", "TaxBilling", "", "", "");
			txtIndebtednessAmount.Text = strTemp;
			strTemp = setCont.GetSettingValue("OutprintStateReductionPercent", "TaxBilling", "", "", "");
			txtStateAidReduction.Text = FCConvert.ToString(Conversion.Val(strTemp));
			LoadDiscount();
			LoadPPCats();
		}

		private void LoadPPCats()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					strCatDesc[clsLoad.Get_Fields("type")] = Strings.Replace(FCConvert.ToString(clsLoad.Get_Fields_String("description")), "\r\n", "", 1, -1, CompareConstants.vbBinaryCompare);
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoaddPPCats", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadDiscount()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				string strReturn;
				clsLoad.OpenRecordset("select * from discount", modGlobalVariables.strBLDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [discount] and replace with corresponding Get_Field method
					txtPercent.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("discount")), "0.00");
					// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
					if (Information.IsDate(clsLoad.Get_Fields("duedate")))
					{
						// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
						if (Convert.ToDateTime(clsLoad.Get_Fields("duedate")).ToOADate() != 0)
						{
							// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
							txtDate.Text = Strings.Format(clsLoad.Get_Fields("duedate"), "MM/dd/yyyy");
						}
						else
						{
							txtDate.Text = "";
						}
					}
					else
					{
						txtDate.Text = "";
					}
				}
				else
				{
					txtPercent.Text = "0.00";
					txtDate.Text = "";
				}
				strReturn = modRegistry.GetRegistryKey("BLOutprintUseDiscount", "BL");
				if (Strings.Trim(strReturn) != string.Empty)
				{
					if (FCConvert.CBool(strReturn))
					{
						chkDiscount.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDiscount.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					chkDiscount.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadDiscount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void mnuCopy_Click(object sender, System.EventArgs e)
		{
			string OldPath = "";
			// OldPath = CurDir
			//! Load frmFileList;
			//FC:FINAL:RPU:#i1445 - Don't show it because the file will be saved to client
			//frmFileList.InstancePtr.Init(null, null, "Select the Drive and Directory to save Out-Print File.");
			//frmFileList.InstancePtr.ShowDialog();
			if (Strings.Right(modGlobalVariables.Statics.gstrFolderName, 1) != "\\")
				modGlobalVariables.Statics.gstrFolderName += "\\";
			CopyZipFile(ref modGlobalVariables.Statics.gstrFolderName);
			frmWait.InstancePtr.Unload();
			// ChDir OldPath
		}

		private void CopyZipFile(ref string strDestDir)
		{
			// takes the directory to place the file as input
			//FC:FINAL:AM: not used
			//cZip zZip;
			string strDestName;
			// holds the file name to save as
			string strSourceName;
			string zipFileName = "";
			bool boolFirsttime;
			try
			{
				// On Error GoTo ErrorHandler
				boolFirsttime = true;
				ResumeCode:
				;
				strDestName = "TSPPBILL.zip";
				strSourceName = "TSPPBILL.FIL";
				if (boolFirsttime)
				{
					//FC:FINAL:MSH - i.issue #1573: in original one item is disabled by default and won't be changed,
					// so to avoid incorrect logic in work replace SelectedIndex by Text
					//if (cmbModule.SelectedIndex == 0)
					if (cmbModule.Text == "Real Estate")
					{
						strDestName = "TSREBILL.zip";
						strSourceName = "TSREBILL.FIL";
					}
					//FC:FINAL:MSH - i.issue #1573: in original one item is disabled by default and won't be changed,
					// so to avoid incorrect logic in work replace SelectedIndex by Text
					//else if (cmbModule.SelectedIndex == 1)
					else if (cmbModule.Text == "Personal Property")
					{
						strDestName = "TSPPBILL.zip";
						strSourceName = "TSPPBILL.FIL";
					}
						//FC:FINAL:MSH - i.issue #1573: in original one item is disabled by default and won't be changed,
						// so to avoid incorrect logic in work replace SelectedIndex by Text
						//else if (cmbModule.SelectedIndex == 2)
						else if (cmbModule.Text == "Both (Combined)")
					{
						strDestName = "TSREBILL.zip";
						strSourceName = "TSREBILL.FIL";
					}
							//FC:FINAL:MSH - i.issue #1573: in original one item is disabled by default and won't be changed,
							// so to avoid incorrect logic in work replace SelectedIndex by Text
							//else if (cmbModule.SelectedIndex == 3)
							else if (cmbModule.Text == "Both (Not Combined)")
					{
						strDestName = "TSREBILL.zip";
						strSourceName = "TSREBILL.FIL";
					}
				}
				//FC:FINAL:RPU:#i1445 - Check for file existance in UserData directory
				if (!FCFileSystem.FileExists(strSourceName))
				{
					MessageBox.Show("The file " + strSourceName + " could not be found.  Please build the Out-Print file first.", "Missing File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// the file is there so now zip it up
				//! Load frmWait;
				frmWait.InstancePtr.Init("Please wait while zip file is created.");
				// frmWait.lblMessage.Caption = "Please wait while zip file is created."
				//Application.DoEvents();
				//FC:FINAL:BBE: replace AbaleZip1 with .NET classes
				//AbaleZip1.AddFilesToProcess(strSourceName);
				//AbaleZip1.ZipFilename = strDestDir + strDestName;
				//AbaleZip1.Zip();
				//FC:FINAL:RPU:#i1445 - Set strDestDir to be the directory in which the file is found
				strDestDir = FCFileSystem.CurDir();
				zipFileName = Path.Combine(strDestDir, strDestName);
				//FC:FINAL:BBE - Only zip files that are set by in the FilesToProcess property
				using (FileStream zipToOpen = new FileStream(zipFileName, FileMode.Create))
				{
					using (System.IO.Compression.ZipArchive archive = new System.IO.Compression.ZipArchive(zipToOpen, System.IO.Compression.ZipArchiveMode.Create))
					{
						//FC:FINAL:RPU:#i1445 - Use the full path of source
						archive.CreateEntryFromFile(Path.Combine(strDestDir, strSourceName), strSourceName);
					}
				}
				FCUtils.Download(zipFileName);
				// Set zZip = New cZip
				// With zZip
				// .BasePath = strDestDir
				// .ClearFileSpecs
				// .ZipFile = strDestDir & strDestName
				// .StoreFolderNames = False
				// .RecurseSubDirs = False
				// .AddFileSpec strSourceName
				// .Zip
				// End With
				frmWait.InstancePtr.Unload();
				//FC:FINAL:MSH - i.issue #1573: in original one item is disabled by default and won't be changed,
				// so to avoid incorrect logic in work replace SelectedIndex by Text
				//if ((cmbModule.SelectedIndex == 2 || cmbModule.SelectedIndex == 3) && boolFirsttime)
				if ((cmbModule.Text == "Both (Combined)" || cmbModule.Text == "Both (Not Combined)") && boolFirsttime)
				{
					boolFirsttime = false;
					goto ResumeCode;
				}
				MessageBox.Show("File copied", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CopyZipFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        private void mnuCreate_Click(object sender, System.EventArgs e)
        {
            this.LockCloseDuringLongProcess();
            FCUtils.StartTask(this, () =>
            {
                int lngYear;
                clsDRWrapper clsTest = new clsDRWrapper();
                bool boolCombine = false;
                string strTemp = "";
                bool boolSuccessful;
                string strMessage = "";
                try
                {
                    // On Error GoTo ErrorHandler
                    // On Error GoTo CallErrorRoutine
                    // gstrCurrentRoutine = "Menu Create"
                    // Dim Mouse As clsMousePointer
                    // Set Mouse = New clsMousePointer
                    // GoTo ResumeCode
                    // CallErrorRoutine:
                    // Call SetErrorHandler
                    // Exit Sub
                    // ResumeCode:
                    // If chkInlcude3rdAddress.Value = vbChecked Then
                    // Call setCont.SaveSetting("True", "OutprintUseThirdAddressLine", "TaxBilling", "", "", "")
                    // Else
                    // Call setCont.SaveSetting("False", "OutprintUseThirdAddressLine", "TaxBilling", "", "", "")
                    // End If
                    setCont.SaveSetting(t2kIndebtednessDate.Text, "OutprintIndebtednessDate", "TaxBilling", "", "", "");
                    setCont.SaveSetting(txtIndebtednessAmount.Text, "OutprintIndebtednessAmount", "TaxBilling", "", "", "");
                    setCont.SaveSetting(FCConvert.ToString(Conversion.Val(txtStateAidReduction.Text)), "OutprintStateReductionPercent", "TaxBilling", "", "", "");
                    lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
                    boolSuccessful = true;
                    lngNumNewNames = 0;
                    lngNumIPs = 0;
                    lngMHCopies = 0;
                    dblTotalCopiesRENetOwed = 0;
                    dblTotalOriginalRETax = 0;
                    dblTotalOriginalPPTax = 0;
                    dblTotalREPrePayments = 0;
                    dblTotalPPPrePayments = 0;
                    dblTotalRENetTaxOwed = 0;
                    dblTotalPPNetTaxOwed = 0;
                    dblTotalREAssess = 0;
                    dblTotalPPAssess = 0;
                    if (chkDiscount.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        if (Conversion.Val(txtPercent.Text) == 0)
                        {
                            MessageBox.Show("You chose to use discounts but didn't enter a discount rate", "Bad Discount Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        if (!Information.IsDate(txtDate.Text))
                        {
                            MessageBox.Show("You must specify a valid discount due date", "Invalid Due Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        modRegistry.SaveRegistryKey("BLOutprintUseDiscount", "TRUE", "BL");
                        clsTest.Execute("update discount set discount = " + FCConvert.ToString(Conversion.Val(txtPercent.Text)) + ",duedate = '" + txtDate.Text + "'", modGlobalVariables.strBLDatabase);
                    }
                    else
                    {
                        modRegistry.SaveRegistryKey("BLOutprintUseDiscount", "FALSE", "BL");
                    }
                    if (Strings.Trim(txtYear.Text) == "")
                    {
                        MessageBox.Show("You must select a tax year to continue", "Invalid Tax Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (Conversion.Val(Strings.Trim(txtYear.Text)) == 0)
                    {
                        MessageBox.Show("You must select a valid tax year to continue", "Invalid Tax Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    //FC:FINAL:MSH - i.issue #1573: in original one item is disabled by default and won't be changed,
                    // so to avoid incorrect logic in work replace SelectedIndex by Text
                    //if (cmbModule.SelectedIndex == 0)
                    if (cmbModule.Text == "Real Estate")
                    {
                        // real estate
                        strMessage = "Only Real Estate file created";
                        clsTest.OpenRecordset("select top 1 * from billingmaster where billingtype = 'RE' and billingyear between " + FCConvert.ToString(lngYear) + "1 and " + FCConvert.ToString(lngYear) + "9 order by billingyear", "twcl0000.vb1");
                        if (clsTest.EndOfFile())
                        {
                            MessageBox.Show("No Real Estate bills found for year " + FCConvert.ToString(lngYear), "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        else
                        {
                            strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, false, "RE");
                            if (Strings.Trim(strTemp) == string.Empty || Strings.UCase(strTemp) == "CANCEL")
                                return;
                            strTemp = Strings.Trim(Strings.Mid(strTemp, 11));
                            lngRateRecToPrint = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                            // Load frmWait
                            // frmWait.Init ("Please wait while the Out-Print file is created.")
                            // frmWait.lblMessage.Caption = "Please wait while the Out-Print file is created."
                            // DoEvents
                            // If optFormat(0).Value Then
                            // original fixed width format
                            // If chkEnhanced.Value = vbChecked Then
                            // boolSuccessful = boolSuccessful And BuildFixedEnhanced(lngYear, 0, False)
                            // Else
                            // boolSuccessful = boolSuccessful And BuildFixed(lngYear, 0, False)
                            // End If
                            // Else
                            // comma delimited
                            boolSuccessful = boolSuccessful && BuildComma_24(lngYear, 0, false);
                            // End If
                            frmWait.InstancePtr.Unload();
                        }
                    }
                    //FC:FINAL:MSH - i.issue #1573: in original one item is disabled by default and won't be changed,
                    // so to avoid incorrect logic in work replace SelectedIndex by Text
                    //else if (cmbModule.SelectedIndex == 1)
                    else if (cmbModule.Text == "Personal Property")
                    {
                        // personal property
                        strMessage = "Only Personal Property file created";
                        strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, false, "PP");
                        if (Strings.Trim(strTemp) == string.Empty)
                            return;
                        strTemp = Strings.Trim(Strings.Mid(strTemp, 11));
                        lngRateRecToPrint = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                        // Load frmWait
                        // frmWait.Init ("Please wait while the Out-Print file is created.")
                        // frmWait.lblMessage.Caption = "Please wait while the Out-Print file is created."
                        // DoEvents
                        // If optFormat(0).Value Then
                        // If chkEnhanced.Value = vbChecked Then
                        // boolSuccessful = boolSuccessful And BuildFixedEnhanced(lngYear, 1, False)
                        // Else
                        // boolSuccessful = boolSuccessful And BuildFixed(lngYear, 1, False)
                        // End If
                        // Else
                        boolSuccessful = boolSuccessful && BuildComma_24(lngYear, 1, false);
                        // End If
                        frmWait.InstancePtr.Unload();
                    }
                    else
                    {
                        // both
                        strMessage = "Real Estate and Personal Property files created";
                        strTemp = frmPickRateToPrint.InstancePtr.Init(ref lngYear, true, true, false);
                        if (Strings.Trim(strTemp) == string.Empty)
                            return;
                        strTemp = Strings.Trim(Strings.Mid(strTemp, 11));
                        lngRateRecToPrint = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                        // Load frmWait
                        // frmWait.Init ("Please wait while Out-Print files are created.")
                        // frmWait.lblMessage.Caption = "Please wait while Out-Print files are created."
                        // DoEvents
                        // If optFormat(0).Value Then
                        // If optModule(2).Value Then
                        // boolCombine = True
                        // Else
                        // boolCombine = False
                        // End If
                        // If chkEnhanced.Value = vbChecked Then
                        // boolSuccessful = boolSuccessful And BuildFixedEnhanced(lngYear, 0, boolCombine)
                        // boolSuccessful = boolSuccessful And BuildFixedEnhanced(lngYear, 1, boolCombine)
                        // Else
                        // boolSuccessful = boolSuccessful And BuildFixed(lngYear, 0, boolCombine)
                        // boolSuccessful = boolSuccessful And BuildFixed(lngYear, 1, boolCombine)
                        // End If
                        // 
                        // Unload frmWait
                        // Else
                        //FC:FINAL:MSH - i.issue #1573: in original one item is disabled by default and won't be changed,
                        // so to avoid incorrect logic in work replace SelectedIndex by Text
                        //if (cmbModule.SelectedIndex == 2)
                        if (cmbModule.Text == "Both (Combined)")
                        {
                            boolCombine = true;
                        }
                        else
                        {
                            boolCombine = false;
                        }
                        boolSuccessful = boolSuccessful && BuildComma_6(lngYear, 0, boolCombine);
                        boolSuccessful = boolSuccessful && BuildComma_6(lngYear, 1, boolCombine);
                        frmWait.InstancePtr.Unload();
                        // End If
                    }
                    if (boolSuccessful)
                    {
                        MessageBox.Show("Outprint file creation complete" + "\r\n" + "You may review the details and totals of the files created in the outprint reports", "File Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    rptOutprintSummary.InstancePtr.Init(ref dblTotalOriginalRETax, ref dblTotalOriginalPPTax, ref dblTotalREPrePayments, ref dblTotalPPPrePayments, ref dblTotalRENetTaxOwed, ref dblTotalPPNetTaxOwed, ref dblTotalREAssess, ref dblTotalPPAssess, ref strMessage, ref lngNumNewNames, ref lngMHCopies, ref dblTotalCopiesRENetOwed, ref lngNumIPs);
                    return;
                }
                catch (Exception ex)
                {
                    // ErrorHandler:
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuCreate_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                finally
                {
                    this.UnlockCloseDuringLongProcess();
                }
            });
        }

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintFormat_Click(object sender, System.EventArgs e)
		{
			frmShowResults.InstancePtr.Init(RichTextBox1.Text);
		}

		private void optFormat_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				RichTextBox1.Text = strREFormat;
				chkEnhanced.Enabled = true;
				if (chkEnhanced.CheckState == Wisej.Web.CheckState.Checked)
				{
					RichTextBox1.Text = strREEnhanced;
				}
			}
			else
			{
				RichTextBox1.Text = strCommaFormat;
				chkEnhanced.Enabled = false;
			}
		}

		private void optFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbFormat.SelectedIndex);
			optFormat_CheckedChanged(index, sender, e);
		}

		private void FillStrings()
		{
			strREFormat = "The file(s) TSREBill.fil and TSPPBill.fil are in fixed length format" + "\r\n" + "and are in " + FCFileSystem.CurDir() + "\r\n";
			strREFormat += "Example:" + "\r\n";
			strREFormat += "RE00017922B Baker Street               104-100          Holmes, Sherlock etc." + "\r\n" + "\r\n";
			strREFormat += "Type		Length		Description" + "\r\n";
			strREFormat += "Bill Type			2		RE,PP or CM" + "\r\n";
			strREFormat += "Account #		6		Account Number" + "\r\n";
			strREFormat += "Location			31		Property Location" + "\r\n";
			strREFormat += "Map/Lot			17		Map and Lot Number" + "\r\n";
			strREFormat += "Name			34		Name" + "\r\n";
			strREFormat += "Address 1		34		Address Line One" + "\r\n";
			strREFormat += "Address 2		34		Address Line Two" + "\r\n";
			strREFormat += "City				24		Town or City" + "\r\n";
			strREFormat += "State			2		State Abbreviated" + "\r\n";
			strREFormat += "Zip Code			5		Zip Code" + "\r\n";
			strREFormat += "Land Value		10		Real Estate Land Value" + "\r\n";
			strREFormat += "Building Value	10		Real Estate Building Value" + "\r\n";
			strREFormat += "Total			10		Total Land and Building Value" + "\r\n";
			strREFormat += "Code1			10		Value of Code 1 Personal Property Data" + "\r\n";
			strREFormat += "Code2			10		Value of Code 2 Personal Property Data" + "\r\n";
			strREFormat += "Code3			10		Value of Code 3 Personal Property Data" + "\r\n";
			strREFormat += "Code3-9			10		Total for Codes 3 through 9" + "\r\n";
			strREFormat += "Code4-9			10		Total for Codes 4 through 9" + "\r\n";
			strREFormat += "Total			10		Total Personal Property Value" + "\r\n";
			strREFormat += "Exemption		10		Total Exemption Amount" + "\r\n";
			strREFormat += "Assessment		10		Net Assessment" + "\r\n";
			strREFormat += "Tax				10		Total Tax Less amount paid to date" + "\r\n";
			strREFormat += "Tax Due 1		10		Period one amount owed" + "\r\n";
			strREFormat += "Tax Due 2		10		Period two amount owed" + "\r\n";
			strREFormat += "Tax Due 3		10		Period three amount owed" + "\r\n";
			strREFormat += "Tax Due 4		10		Period four amount owed" + "\r\n";
			strREFormat += "Book & Page		12		Book and Page" + "\r\n";
			strREFormat += "Homestead		10		Real Estate Homestead Exemption Amount" + "\r\n";
			strREFormat += "Other			10		Remaining Exemption amount" + "\r\n";
			strREFormat += "Distribution1		10		Distribution one amount" + "\r\n";
			strREFormat += "Distribution2		10		Distribution two amount" + "\r\n";
			strREFormat += "Distribution3		10		Distribution three amount" + "\r\n";
			strREFormat += "Distribution4		10		Distribution four amount" + "\r\n";
			strREFormat += "Distribution5		10		Distribution five amount" + "\r\n";
			strREFormat += "Distribution1		6		Distribution one percent" + "\r\n";
			strREFormat += "Distribution2		6		Distribution two percent" + "\r\n";
			strREFormat += "Distribution3		6		Distribution three percent" + "\r\n";
			strREFormat += "Distribution4		6		Distribution four percent" + "\r\n";
			strREFormat += "Distribution5		6		Distribution five percent" + "\r\n";
			strREFormat += "Paid To Date		10		Paid to Date";
			strREEnhanced = "The file(s) TSREBill.fil and TSPPBill.fil are in fixed length format" + "\r\n" + "and are in " + FCFileSystem.CurDir() + "\r\n";
			strREEnhanced += "Example:" + "\r\n";
			strREEnhanced += "RE00017922B Baker Street               104-100          Holmes, Sherlock etc." + "\r\n" + "\r\n";
			strREEnhanced += "Type			Length	Description" + "\r\n";
			strREEnhanced += "Bill Type			2		RE,PP or CM" + "\r\n";
			strREEnhanced += "Account #		6		Account Number" + "\r\n";
			strREEnhanced += "Location			31		Property Location" + "\r\n";
			strREEnhanced += "Map/Lot			17		Map and Lot Number" + "\r\n";
			strREEnhanced += "Name			34		Name" + "\r\n";
			strREEnhanced += "Address 1		34		Address Line One" + "\r\n";
			strREEnhanced += "Address 2		34		Address Line Two" + "\r\n";
			strREEnhanced += "City				24		Town or City" + "\r\n";
			strREEnhanced += "State			2		State Abbreviated" + "\r\n";
			strREEnhanced += "Zip Code			5		Zip Code" + "\r\n";
			strREEnhanced += "Land Value		10		Real Estate Land Value" + "\r\n";
			strREEnhanced += "Building Value	10		Real Estate Building Value" + "\r\n";
			strREEnhanced += "Total			10		Total Land and Building Value" + "\r\n";
			strREEnhanced += "Code1			10		Value of Code 1 Personal Property Data" + "\r\n";
			strREEnhanced += "Code2			10		Value of Code 2 Personal Property Data" + "\r\n";
			strREEnhanced += "Code3			10		Value of Code 3 Personal Property Data" + "\r\n";
			strREEnhanced += "Code3-9			10		Total for Codes 3 through 9" + "\r\n";
			strREEnhanced += "Code4-9			10		Total for Codes 4 through 9" + "\r\n";
			strREEnhanced += "Total			10		Total Personal Property Value" + "\r\n";
			strREEnhanced += "Exemption		10		Total Exemption Amount" + "\r\n";
			strREEnhanced += "Assessment		10		Net Personal Property Assessment" + "\r\n";
			strREEnhanced += "Tax				14		Total Tax Less amount paid to date" + "\r\n";
			strREEnhanced += "Tax Due 1		14		Period one amount owed" + "\r\n";
			strREEnhanced += "Tax Due 2		14		Period two amount owed" + "\r\n";
			strREEnhanced += "Tax Due 3		14		Period three amount owed" + "\r\n";
			strREEnhanced += "Tax Due 4		14		Period four amount owed" + "\r\n";
			strREEnhanced += "Book & Page		12		Book and Page" + "\r\n";
			strREEnhanced += "Homestead		10		Real Estate Homestead Exemption Amount" + "\r\n";
			strREEnhanced += "Other			10		Remaining Exemption amount" + "\r\n";
			strREEnhanced += "Distribution1		14		Distribution one amount" + "\r\n";
			strREEnhanced += "Distribution2		14		Distribution two amount" + "\r\n";
			strREEnhanced += "Distribution3		14		Distribution three amount" + "\r\n";
			strREEnhanced += "Distribution4		14		Distribution four amount" + "\r\n";
			strREEnhanced += "Distribution5		14		Distribution five amount" + "\r\n";
			strREEnhanced += "Distribution1		6		Distribution one percent" + "\r\n";
			strREEnhanced += "Distribution2		6		Distribution two percent" + "\r\n";
			strREEnhanced += "Distribution3		6		Distribution three percent" + "\r\n";
			strREEnhanced += "Distribution4		6		Distribution four percent" + "\r\n";
			strREEnhanced += "Distribution5		6		Distribution five percent" + "\r\n";
			strREEnhanced += "Paid To Date		14		Paid to Date" + "\r\n";
			strREEnhanced += "RE Assessment	11		Net Real Estate Assessment" + "\r\n";
			strREEnhanced += "Reference 1		36		Real Estate Reference 1" + "\r\n";
			strREEnhanced += "Reference 2		36		Real Estate Reference 2" + "\r\n";
			strREEnhanced += "Second Owner	34		Real Estate Second Owner" + "\r\n";
			strREEnhanced += "Acreage			10		Real Estate Acreage" + "\r\n";
			strREEnhanced += "Mort. Holder ID	6		Mortgage Holder Number";
			strHeaderFormat = "Header Record" + "\r\n";
			strHeaderFormat += "Type			Header Record" + "\r\n";
			strHeaderFormat += "Town			Town/City Name" + "\r\n";
			strHeaderFormat += "File Type		RE,PP or CM" + "\r\n";
			strHeaderFormat += "Version			Program Version" + "\r\n";
			strHeaderFormat += "Dist Perc 1		Distribution Percent 1" + "\r\n";
			strHeaderFormat += "Dist Perc 2		Distribution Percent 2" + "\r\n";
			strHeaderFormat += "Dist Perc 3		Distribution Percent 3" + "\r\n";
			strHeaderFormat += "Dist Perc 4		Distribution Percent 4" + "\r\n";
			strHeaderFormat += "Dist Perc 5		Distribution Percent 5" + "\r\n";
			strHeaderFormat += "Billing Date		Billing Date" + "\r\n";
			strHeaderFormat += "Tax Year		Tax Year" + "\r\n";
			strHeaderFormat += "Tax Rate		Tax Rate Per 1000" + "\r\n";
			strHeaderFormat += "Interest Rate		Interest Rate as a Percent" + "\r\n";
			strHeaderFormat += "# Periods		Number of Pay Periods" + "\r\n";
			strHeaderFormat += "Due Date 1		First Period Date Due" + "\r\n";
			strHeaderFormat += "Interest Date 1		First Period Interest Date" + "\r\n";
			strHeaderFormat += "Due Date 2		Second Period Date Due" + "\r\n";
			strHeaderFormat += "Interest Date 2		Second Period Interest Date" + "\r\n";
			strHeaderFormat += "Due Date 3		Third Period Date Due" + "\r\n";
			strHeaderFormat += "Interest Date 3		Third Period Interest Date" + "\r\n";
			strHeaderFormat += "Due Date 4		Fourth Period Date Due" + "\r\n";
			strHeaderFormat += "Interest Date 4		Fourth Period Interest Date" + "\r\n";
			strHeaderFormat += "Discount Perc		Discount Percentage" + "\r\n";
			strHeaderFormat += "Discount Date		Discount Due Date" + "\r\n";
			strHeaderFormat += "PP Category 1		Personal Property Category 1 Description" + "\r\n";
			strHeaderFormat += "PP Category 2		Personal Property Category 2 Description" + "\r\n";
			strHeaderFormat += "PP Category 3		Personal Property Category 3 Description" + "\r\n";
			strHeaderFormat += "PP Category 4		Personal Property Category 4 Description" + "\r\n";
			strHeaderFormat += "PP Category 5		Personal Property Category 5 Description" + "\r\n";
			strHeaderFormat += "PP Category 6		Personal Property Category 6 Description" + "\r\n";
			strHeaderFormat += "PP Category 7		Personal Property Category 7 Description" + "\r\n";
			strHeaderFormat += "PP Category 8		Personal Property Category 8 Description" + "\r\n";
			strHeaderFormat += "PP Category 9		Personal Property Category 9 Description" + "\r\n";
			strHeaderFormat += "Indebtedness Date" + "\r\n";
			strHeaderFormat += "Indebtedness Amount" + "\r\n";
			strHeaderFormat += "State Aid Reduction %" + "\r\n";
			strHeaderFormat += "\r\n" + "\r\n";
			strCommaFormat = "The file(s) TSREBill.fil and TSPPBill.fil are in a comma delimited format" + "\r\n" + "and are in " + FCFileSystem.CurDir() + "\r\n";
			strCommaFormat += "Example:" + "\r\n";
			strCommaFormat += "\"RE\",\"179\",\"104-100\",\"22B Baker Street\",\"Sherlock, Holmes\",\"22B Baker Street\" etc." + "\r\n" + "\r\n";
			// strCommaFormat = strCommaFormat & "Type             Description" & vbNewLine
			strCommaFormat += strHeaderFormat + "Bill Records" + "\r\n";
			strCommaFormat += "Bill Type		RE,PP or CM" + "\r\n";
			strCommaFormat += "Account #		Account Number" + "\r\n";
			strCommaFormat += "Location		Property Location" + "\r\n";
			strCommaFormat += "Map/Lot			Map and Lot Number" + "\r\n";
			strCommaFormat += "Name			Name" + "\r\n";
			strCommaFormat += "Address 1		Address Line One" + "\r\n";
			strCommaFormat += "Address 2		Address Line Two" + "\r\n";
			strCommaFormat += "City			Town or City" + "\r\n";
			strCommaFormat += "State			State Abbreviated" + "\r\n";
			strCommaFormat += "Zip Code		Zip Code" + "\r\n";
			strCommaFormat += "Land Value		Real Estate Land Value" + "\r\n";
			strCommaFormat += "Building Value		Real Estate Building Value" + "\r\n";
			strCommaFormat += "Total			Total Land and Building Value" + "\r\n";
			strCommaFormat += "RE Exemption		Total Real Estate Exemption Amount" + "\r\n";
			strCommaFormat += "Code1			Value of Code 1 Personal Property Data" + "\r\n";
			strCommaFormat += "Code2			Value of Code 2 Personal Property Data" + "\r\n";
			strCommaFormat += "Code3			Value of Code 3 Personal Property Data" + "\r\n";
			strCommaFormat += "Code4			Value of Code 4 Personal Property Data" + "\r\n";
			strCommaFormat += "Code5			Value of Code 5 Personal Property Data" + "\r\n";
			strCommaFormat += "Code6			Value of Code 6 Personal Property Data" + "\r\n";
			strCommaFormat += "Code7			Value of Code 7 Personal Property Data" + "\r\n";
			strCommaFormat += "Code8			Value of Code 8 Personal Property Data" + "\r\n";
			strCommaFormat += "Code9			Value of Code 9 Personal Property Data" + "\r\n";
			strCommaFormat += "Total			Total Personal Property Value" + "\r\n";
			strCommaFormat += "PP Exemption		Total PP Exemption Amount" + "\r\n";
			strCommaFormat += "PP Assessment		Net Personal Property Assessment" + "\r\n";
			strCommaFormat += "Tax			Total Tax Less amount paid to date" + "\r\n";
			strCommaFormat += "Tax Due 1		Period one amount owed" + "\r\n";
			strCommaFormat += "Tax Due 2		Period two amount owed" + "\r\n";
			strCommaFormat += "Tax Due 3		Period three amount owed" + "\r\n";
			strCommaFormat += "Tax Due 4		Period four amount owed" + "\r\n";
			strCommaFormat += "Book & Page		Book and Page" + "\r\n";
			strCommaFormat += "Homestead		Real Estate Homestead Exemption Amount" + "\r\n";
			strCommaFormat += "Other			Remaining Exemption amount" + "\r\n";
			strCommaFormat += "Distribution1		Distribution one amount" + "\r\n";
			strCommaFormat += "Distribution2		Distribution two amount" + "\r\n";
			strCommaFormat += "Distribution3		Distribution three amount" + "\r\n";
			strCommaFormat += "Distribution4		Distribution four amount" + "\r\n";
			strCommaFormat += "Distribution5		Distribution five amount" + "\r\n";
			strCommaFormat += "Distribution1		Distribution one percent" + "\r\n";
			strCommaFormat += "Distribution2		Distribution two percent" + "\r\n";
			strCommaFormat += "Distribution3		Distribution three percent" + "\r\n";
			strCommaFormat += "Distribution4		Distribution four percent" + "\r\n";
			strCommaFormat += "Distribution5		Distribution five percent" + "\r\n";
			strCommaFormat += "Paid To Date		Paid to Date" + "\r\n";
			strCommaFormat += "RE Assessment		Net Real Estate Assessment" + "\r\n";
			strCommaFormat += "Reference 1		Real Estate Reference 1" + "\r\n";
			strCommaFormat += "Reference 2		Real Estate Reference 2" + "\r\n";
			strCommaFormat += "Second Owner		Second Owner" + "\r\n";
			strCommaFormat += "Acreage			Real Estate Total Acreage" + "\r\n";
			strCommaFormat += "Mort. Holder ID		Mortgage Holder Number" + "\r\n";
			strCommaFormat += "Soft Acres		Tree Growth Soft Acreage" + "\r\n";
			strCommaFormat += "Mixed Acres		Tree Growth Mixed Acreage" + "\r\n";
			strCommaFormat += "Hard Acres		Tree Growth Hard Acreage" + "\r\n";
			strCommaFormat += "Soft Value		Tree Growth Soft Amount" + "\r\n";
			strCommaFormat += "Mixed Value		Tree Growth Mixed Amount" + "\r\n";
			strCommaFormat += "Hard Value		Tree Growth Hard Amount" + "\r\n";
			strCommaFormat += "Discount Amount		Discount Amount" + "\r\n";
			strCommaFormat += "RE Exempt		Amount 1" + "\r\n";
			strCommaFormat += "RE Exempt		Description 1" + "\r\n";
			strCommaFormat += "RE Exempt		Amount 2" + "\r\n";
			strCommaFormat += "RE Exempt		Description 2" + "\r\n";
			strCommaFormat += "RE Exempt		Amount 3" + "\r\n";
			strCommaFormat += "RE Exempt		Description 3" + "\r\n";
			strCommaFormat += "Additional Tax";
			// If chkInlcude3rdAddress.Value = vbChecked Then
			strCommaFormat += "\r\n" + "Address 3		Address Line Three";
			// End If
			// If chkIncludePastDue.Value = vbChecked Then
			strCommaFormat += "\r\n" + "Past Due";
			// End If
			RichTextBox1.Text = strCommaFormat;
		}

		private bool BuildFixed(ref int lngYear, ref short intModule, ref bool boolCombined)
		{
			bool BuildFixed = false;
			// saves the original fixed length format
			modTypes.OutPrintingFormat OutP = new modTypes.OutPrintingFormat(0);
			// user type that contains fixed length format
			clsDRWrapper clsBill = new clsDRWrapper();
			// vbPorter upgrade warning: strBillType As string	OnRead(FixedString)
			string strBillType = "";
			// vbPorter upgrade warning: strTemp As string	OnRead(FixedString)
			string strTemp = "";
			clsDRWrapper clsAggregate = new clsDRWrapper();
			string strTbl1 = "";
			string strTbl2 = "";
			string strSQL = "";
			string strQry1 = "";
			string strQry2 = "";
			string strRE = "";
			bool boolREFile = false;
			clsDRWrapper clsDist = new clsDRWrapper();
			// holds the distribution information
			double[] DistPerc = new double[5 + 1];
			int x;
			double dblTax = 0;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			long lngTemp = 0;
			// temp variable for adding sor a total
			int lngRecNum;
			// vbPorter upgrade warning: lngTestExempt As int	OnWrite(short, double)
			long lngTestExempt;
			double dblLeftOver = 0;
			string[] strAry = null;
			// vbPorter upgrade warning: lngNetAssess As int	OnWrite(int, double)
			long lngNetAssess = 0;
			string strOldName = "";
			string strSecOwner = "";
			string strCity = "";
			string strState = "";
			string strZip = "";
			string strAddr1 = "";
			string strAddr2 = "";
			clsDRWrapper clsMortgageHolders = new clsDRWrapper();
			clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				ResumeCode:
				;
				BuildFixed = false;
				lngTestExempt = 0;
				iLabel1:
				lngRecNum = 1;
				// get the distribution information first
				iLabel2:
				clsDist.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				for (x = 1; x <= 5; x++)
				{
					iLabel3:
					DistPerc[x] = 0;
				}
				// x
				if (clsDist.EndOfFile())
				{
					// 4        MsgBox "The distribution information must be filled out before creating outprint files.", vbExclamation, "Distribution Missing"
					frmWait.InstancePtr.Unload();
					iLabel5:
					frmDistribution.InstancePtr.Init();
					frmWait.InstancePtr.Init("Please wait while the Out-Print file is created.");
					iLabel6:
					clsDist.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				}
				while (!clsDist.EndOfFile())
				{
					iLabel7:
					DistPerc[clsDist.Get_Fields_Int16("distributionnumber")] = clsDist.Get_Fields_Double("distributionpercent");
					iLabel8:
					clsDist.MoveNext();
				}
				FCFileSystem.FileClose();
				if (intModule == 0)
				{
					// real estate
					iLabel9:
					boolREFile = true;
					if (FCFileSystem.FileExists("tsrebill.fil"))
                    {
                        //FC:FINAL:MSH - use FCFileSystem instead of File to avoid problems with wrong paths (same with issue #1604)
                        //File.Delete("tsrebill.fil");
                        FCFileSystem.DeleteFile("tsrebill.fil");
                    }
                    iLabel10:
					FCFileSystem.FileOpen(42, "TSREBill.fil", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(OutP));
					// use the twre link table so that we can join with the association table
					strRE = "select * from master innerjoin [";
					if (boolCombined)
					{
						iLabel11:
						strTbl1 = "(select account as ac,acres,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,PPASSESSMENT,principalpaid,0 as exemptvalue  from billingmaster where billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						strQry2 = "select ac,acres,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid,exemptvalue from ";
						iLabel12:
						strQry1 = strQry2 + strTbl1;
						strTbl2 = "(select moduleassociation.remasteracct as ac,acres, taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid,exemptvalue  from billingmaster inner join " + clsBill.CurrentPrefix + "CentralData.dbo.moduleassociation on (billingmaster.account = moduleassociation.account) where moduleassociation.module = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and moduleassociation.primaryassociate = -1 and billingmaster.billingtype = 'PP' and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						iLabel13:
						strQry2 += strTbl2;
						iLabel14:
						strSQL = "SELECT ac,sum(acres) as acreage, sum(taxdue1) AS tax1, sum( taxdue2) AS tax2,sum(taxdue3) as tax3,sum(taxdue4) as tax4 ,sum(landvalue) as landsum,sum(buildingvalue) as buildingsum,sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9, sum(homesteadexemption) as homestead,sum(otherexempt1) as otherexemptsum,sum(ppassessment) as PPValue,sum(principalpaid) as prepaid,sum(exemptvalue) as ppexempt from [" + strQry1 + " union " + strQry2 + "]. AS TblCombined group by ac";
						// this procedure will get the merged taxbills
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from BILLINGmaster inner join (" + strSQL + ") as billfixedformat on (billfixedformat.ac = billingmaster.account) where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " order by billingmaster.name1";
						}
						else
						{
							strSQL = "select * from BILLINGmaster inner join (" + strSQL + ") as billfixedformat on (billfixedformat.ac = billingmaster.account) where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and taxdue1 + taxdue2 + taxdue3 + taxdue4 > 0 order by billingmaster.name1";
						}
					}
					else
					{
						// not combined
						strTbl1 = "(select account as ac,acres ,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,PPASSESSMENT,principalpaid,exemptvalue  from billingmaster where billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						strQry2 = "select ac,acres,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid,exemptvalue from ";
						strQry1 = strQry2 + strTbl1;
						strSQL = "SELECT ac,SUM(ACRES) as acreage, sum(taxdue1) AS tax1, sum( taxdue2) AS tax2,sum(taxdue3) as tax3,sum(taxdue4) as tax4 ,sum(landvalue) as landsum,sum(buildingvalue) as buildingsum,sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9, sum(homesteadexemption) as homestead,sum(otherexempt1) as otherexemptsum,sum(ppassessment) as PPValue,sum(principalpaid) as prepaid,sum(exemptvalue) as ppexempt from [" + strQry1 + "]. AS TblCombined group by ac";
						// this procedure will get the merged taxbills
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from billingmaster inner join (" + strSQL + ") as billfixedformat  on (billingmaster.account = billfixedformat.ac)   where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " ORDER by billingmaster.name1";
						}
						else
						{
							strSQL = "select * from billingmaster inner join (" + strSQL + ") as billfixedformat  on (billingmaster.account = billfixedformat.ac)   where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and taxdue1 + taxdue2 + taxdue3 + taxdue4 > 0  ORDER by billingmaster.name1";
						}
					}
					clsBill.OpenRecordset(strSQL, "twcl0000.vb1");
					if (clsBill.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						//Application.DoEvents();
						MessageBox.Show("No Real Estate Bills found", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return BuildFixed;
					}
					strBillType = "RE";
				}
				else
				{
					// personal property
					boolREFile = false;
                    //FC:FINAL:MSH - use FCFileSystem instead of File to avoid problems with wrong paths (same with issue #1604)
                    //if (File.Exists("tsppbill.fil"))
                    //	File.Delete("tsppbill.fil");
                    if (FCFileSystem.FileExists("tsppbill.fil"))
                        FCFileSystem.DeleteFile("tsppbill.fil");
                    FCFileSystem.FileOpen(42, "TSPPBill.fil", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(OutP));
					strBillType = "PP";
					if (boolCombined)
					{
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from ppmaster inner join (select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as ppexempt from " + clsBill.CurrentPrefix + "Collections.dbo.billingmaster left join [(select * from " + clsBill.CurrentPrefix + "CentralData.dbo.moduleassociation where module = 'PP' and primaryassociate = -1)]. as Tbl1 on (billingmaster.account = tbl1.account) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ") as billfixedformat on (ppmaster.account = billfixedformat.ac)";
						}
						else
						{
							strSQL = "select * from ppmaster inner join (select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as ppexempt from " + clsBill.CurrentPrefix + "Collections.dbo.billingmaster left join [(select * from " + clsBill.CurrentPrefix + "CentralData.dbo.moduleassociation where module = 'PP' and primaryassociate = -1)]. as Tbl1 on (billingmaster.account = tbl1.account) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and taxdue1 + taxdue2 + taxdue3 + taxdue4 > 0) as billfixedformat on (ppmaster.account = billfixedformat.ac)";
						}
					}
					else
					{
						// not combined
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from ppmaster inner join (select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as ppexempt from " + clsBill.CurrentPrefix + "Collections.dbobillingmaster  where billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and  billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ") as billfixedformat on (ppmaster.account = billfixedformat.ac)";
						}
						else
						{
							strSQL = "select * from ppmaster inner join (select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as ppexempt from " + clsBill.CurrentPrefix + "Collections.dbobillingmaster  where billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and  billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and taxdue1 + taxdue2 + taxdue3 + taxdue4 > 0) as billfixedformat on (ppmaster.account = billfixedformat.ac)";
						}
					}
					clsBill.OpenRecordset(strSQL, "twpp0000.vb1");
					if (clsBill.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						//Application.DoEvents();
						MessageBox.Show("No Personal Property Bills Found", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return BuildFixed;
					}
				}
				while (!clsBill.EndOfFile())
				{
					OutP.LineEnd = "\r\n";
					lngNetAssess = 0;
					if (boolREFile)
					{
						iLabel40:
						OutP.BillType = strBillType;
						// This may change later if it is a combined
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						OutP.Account = Strings.Format(clsBill.Get_Fields("account"), "000000");
						iLabel42:
						strTemp = Strings.StrDup(34, " ");
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("name1"))));
						iLabel44:
						OutP.Name = strTemp;
						iLabel45:
						strTemp = Strings.StrDup(31, " ");
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						iLabel46:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(clsBill.Get_Fields("streetnumber") + " " + clsBill.Get_Fields_String("apt") + " " + clsBill.Get_Fields_String("streetname")));
						iLabel47:
						OutP.Location = strTemp;
						iLabel48:
						strTemp = Strings.StrDup(17, " ");
						iLabel49:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(clsBill.Get_Fields_String("maplot")));
						iLabel50:
						OutP.MapLot = strTemp;
						iLabel51:
						strTemp = Strings.StrDup(34, " ");
						iLabel52:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(clsBill.Get_Fields_String("address1")));
						iLabel53:
						OutP.Address1 = strTemp;
						iLabel54:
						strTemp = Strings.StrDup(34, " ");
						iLabel55:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(clsBill.Get_Fields_String("address2")));
						iLabel56:
						OutP.Address2 = strTemp;
						iLabel57:
						if (Strings.Trim(clsBill.Get_Fields_String("name2")) != string.Empty)
						{
							if (Strings.Trim(OutP.Address1) == string.Empty || Strings.Trim(OutP.Address2) == string.Empty)
							{
								if (Strings.Trim(OutP.Address1) != string.Empty)
								{
									OutP.Address2 = OutP.Address1;
								}
								strTemp = Strings.StrDup(34, " ");
								fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(clsBill.Get_Fields_String("name2")));
								OutP.Address1 = strTemp;
							}
						}
						iLabel58:
						strTemp = Strings.StrDup(24, " ");
						if (Strings.Trim(clsBill.Get_Fields_String("address3")) != string.Empty)
						{
							// 59                Mid(strtemp, 1) = Trim(clsBill.Fields("rsaddr3"))
							iLabel59:
							strAry = Strings.Split(Strings.Trim(clsBill.Get_Fields_String("address3")), " ", -1, CompareConstants.vbTextCompare);
							if (Information.UBound(strAry, 1) > 1)
							{
								OutP.State = Strings.Left(strAry[Information.UBound(strAry, 1) - 1] + "  ", 2);
								for (x = 0; x <= Information.UBound(strAry, 1) - 2; x++)
								{
									strTemp += strAry[x] + " ";
								}
								// x
								strTemp = Strings.Trim(strTemp);
								strTemp = Strings.Left(strTemp + Strings.StrDup(24, " "), 24);
								OutP.City = strTemp;
							}
							else if (Information.UBound(strAry, 1) > 0)
							{
								iLabel60:
								OutP.State = Strings.Left(strAry[Information.UBound(strAry, 1)] + "  ", 2);
								for (x = 0; x <= Information.UBound(strAry, 1) - 1; x++)
								{
									strTemp += strAry[x] + " ";
								}
								// x
								strTemp = Strings.Trim(strTemp);
								strTemp = Strings.Left(strTemp + Strings.StrDup(24, " "), 24);
								OutP.City = strTemp;
							}
							else
							{
								OutP.State = " ";
								fecherFoundation.Strings.MidSet(ref strTemp, 1, strAry[0]);
								OutP.City = strTemp;
							}
						}
						else
						{
							OutP.City = Strings.StrDup(24, " ");
							OutP.State = "  ";
						}
						iLabel64:
						strTemp = Strings.StrDup(5, " ");
						iLabel65:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("zip"))));
						iLabel66:
						OutP.Zip = strTemp;
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						iLabel67:
						OutP.Land = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("landsum"))), 10);
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						iLabel68:
						OutP.Building = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("buildingsum"))), 10);
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						iLabel69:
						OutP.Total = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("buildingsum")) + Conversion.Val(clsBill.Get_Fields("landsum"))), 10);
						iLabel70:
						strTemp = Strings.StrDup(12, " ");
						iLabel71:
						if (Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("bookpage"))) != string.Empty)
						{
							iLabel72:
							strAry = Strings.Split(FCConvert.ToString(clsBill.Get_Fields_String("bookpage")), " ", -1, CompareConstants.vbTextCompare);
							strTemp = Strings.Mid(strAry[0] + Strings.StrDup(12, " "), 1, 12);
						}
						iLabel73:
						OutP.BookPage = strTemp;
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						iLabel74:
						OutP.HomesteadExempt = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("homestead"))), 10);
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						iLabel75:
						OutP.OtherExempt = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("otherexemptsum"))), 10);
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						lngTestExempt += Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("homestead")));
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						lngTestExempt += Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("otherexemptsum")));
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						lngTemp = Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("landsum")) + Conversion.Val(clsBill.Get_Fields("buildingsum")) - Conversion.Val(clsBill.Get_Fields("homestead")) - Conversion.Val(clsBill.Get_Fields("otherexemptsum")));
						lngNetAssess += lngTemp;
						dblTotalREAssess += lngTemp;
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						dblTotalOriginalRETax += (Conversion.Val(clsBill.Get_Fields("tax1"))) + (Conversion.Val(clsBill.Get_Fields("tax2"))) + (Conversion.Val(clsBill.Get_Fields("tax3"))) + (Conversion.Val(clsBill.Get_Fields("tax4")));
						if (boolCombined)
						{
							// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
							if (Conversion.Val(clsBill.Get_Fields("ppexempt")) < Conversion.Val(clsBill.Get_Fields("ppvalue")))
							{
								// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
								dblTotalREAssess += Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt"));
							}
						}
					}
					else
					{
						// personal property
						iLabel76:
						OutP.BillType = strBillType;
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						iLabel77:
						OutP.Account = Strings.Format(clsBill.Get_Fields("account"), "000000");
						iLabel78:
						strTemp = Strings.StrDup(34, " ");
						iLabel79:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("name"))));
						iLabel80:
						OutP.Name = strTemp;
						iLabel81:
						strTemp = Strings.StrDup(31, " ");
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						iLabel82:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(clsBill.Get_Fields("streetnumber") + " " + clsBill.Get_Fields_String("street")));
						iLabel83:
						OutP.Location = strTemp;
						iLabel85:
						strTemp = Strings.StrDup(17, " ");
						iLabel86:
						OutP.MapLot = strTemp;
						iLabel88:
						strTemp = Strings.StrDup(34, " ");
						iLabel89:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("ADDRESS1"))));
						iLabel90:
						OutP.Address1 = strTemp;
						iLabel92:
						strTemp = Strings.StrDup(34, " ");
						iLabel93:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address2"))));
						iLabel94:
						OutP.Address2 = strTemp;
						iLabel95:
						strTemp = Strings.StrDup(24, " ");
						iLabel96:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("city"))));
						iLabel97:
						OutP.City = strTemp;
						iLabel98:
						strTemp = "  ";
						iLabel99:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("state"))));
						iLabel100:
						OutP.State = strTemp;
						iLabel101:
						strTemp = Strings.StrDup(5, " ");
						strTemp = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("zip"))), 5);
						// 102            Mid(strTemp, 1) = Trim(clsBill.Fields("zip"))
						iLabel103:
						OutP.Zip = strTemp;
						iLabel104:
						OutP.Land = Strings.StrDup(10, " ");
						iLabel105:
						OutP.Building = Strings.StrDup(10, " ");
						iLabel106:
						OutP.Total = Strings.StrDup(10, " ");
						iLabel107:
						OutP.BookPage = Strings.StrDup(12, " ");
						iLabel108:
						OutP.HomesteadExempt = Strings.StrDup(10, " ");
						iLabel109:
						OutP.OtherExempt = Strings.StrDup(10, " ");
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						dblTotalOriginalPPTax += (Conversion.Val(clsBill.Get_Fields("tax1"))) + (Conversion.Val(clsBill.Get_Fields("tax2"))) + (Conversion.Val(clsBill.Get_Fields("tax3"))) + (Conversion.Val(clsBill.Get_Fields("tax4")));
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBill.Get_Fields("ppexempt")) < Conversion.Val(clsBill.Get_Fields("ppvalue")))
						{
							// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
							dblTotalPPAssess += Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt"));
						}
					}
					// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
					iLabel110:
					OutP.TaxDue1 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax1")), 10);
					// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
					iLabel111:
					OutP.TaxDue2 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax2")), 10);
					// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
					iLabel112:
					OutP.TaxDue3 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax3")), 10);
					// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
					iLabel113:
					OutP.TaxDue4 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax4")), 10);
					iLabel114:
					dblTax = FCConvert.ToDouble(OutP.TaxDue1) + FCConvert.ToDouble(OutP.TaxDue2) + FCConvert.ToDouble(OutP.TaxDue3) + FCConvert.ToDouble(OutP.TaxDue4);
					iLabel115:
					OutP.DistPercent1 = modMain.DblToString_6(DistPerc[1], 6, 3);
					iLabel116:
					OutP.DistPercent2 = modMain.DblToString_6(DistPerc[2], 6, 3);
					iLabel117:
					OutP.DistPercent3 = modMain.DblToString_6(DistPerc[3], 6, 3);
					iLabel118:
					OutP.DistPercent4 = modMain.DblToString_6(DistPerc[4], 6, 3);
					iLabel119:
					OutP.DistPercent5 = modMain.DblToString_6(DistPerc[5], 6, 3);
					iLabel120:
					OutP.Distribution1 = modMain.DblToString_8(DistPerc[1] / 100 * dblTax, 10);
					iLabel121:
					OutP.Distribution2 = modMain.DblToString_8(DistPerc[2] / 100 * dblTax, 10);
					iLabel122:
					OutP.Distribution3 = modMain.DblToString_8(DistPerc[3] / 100 * dblTax, 10);
					iLabel123:
					OutP.Distribution4 = modMain.DblToString_8(DistPerc[4] / 100 * dblTax, 10);
					iLabel124:
					OutP.Distribution5 = modMain.DblToString_8(DistPerc[5] / 100 * dblTax, 10);
					// 120        OutP.Distribution1 = DblToString(DistPerc(1) * dblTax, 10)
					// 121        OutP.Distribution2 = DblToString(DistPerc(2) * dblTax, 10)
					// 122        OutP.Distribution3 = DblToString(DistPerc(3) * dblTax, 10)
					// 123        OutP.Distribution4 = DblToString(DistPerc(4) * dblTax, 10)
					// 124        OutP.Distribution5 = DblToString(DistPerc(5) * dblTax, 10)
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					iLabel125:
					OutP.PaidToDate = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("prepaid")), 10);
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					dblLeftOver = Conversion.Val(clsBill.Get_Fields("prepaid"));
					if (boolREFile)
					{
						dblTotalREPrePayments += dblLeftOver;
					}
					else
					{
						dblTotalPPPrePayments += dblLeftOver;
					}
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					iLabel126:
					dblTax -= Conversion.Val(clsBill.Get_Fields("prepaid"));
					if (boolREFile)
					{
						dblTotalRENetTaxOwed += dblTax;
					}
					else
					{
						dblTotalPPNetTaxOwed += dblTax;
					}
					iLabel127:
					OutP.TotalTax = modMain.DblToString_6(dblTax, 10);
					if (dblLeftOver > 0)
					{
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax1")))
						{
							OutP.TaxDue1 = modMain.DblToString_8(0, 10);
							// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax1"));
						}
						else
						{
							// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
							OutP.TaxDue1 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax1")) - dblLeftOver, 10);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax2")))
						{
							OutP.TaxDue2 = modMain.DblToString_8(0, 10);
							// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax2"));
						}
						else
						{
							// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
							OutP.TaxDue2 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax2")) - dblLeftOver, 10);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax3")))
						{
							OutP.TaxDue3 = modMain.DblToString_8(0, 10);
							// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax3"));
						}
						else
						{
							// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
							OutP.TaxDue3 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax3")) - dblLeftOver, 10);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax4")))
						{
							OutP.TaxDue4 = modMain.DblToString_8(0, 10);
							// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax4"));
						}
						else
						{
							// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
							OutP.TaxDue4 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax4")) - dblLeftOver, 10);
							dblLeftOver = 0;
						}
					}
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					iLabel128:
					if (boolREFile && (Conversion.Val(clsBill.Get_Fields("ppvalue")) > 0) && boolCombined)
					{
						// combined bill
						// shouldn't need to check boolcombined since ppvalue shouldn't have a value in an re bill file
						iLabel129:
						OutP.BillType = "CM";
					}
					// Personal property stuff
					// TODO Get_Fields: Field [cat1] not found!! (maybe it is an alias?)
					iLabel130:
					OutP.PPCode1 = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat1"))), 10);
					// TODO Get_Fields: Field [cat2] not found!! (maybe it is an alias?)
					iLabel131:
					OutP.PPCode2 = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat2"))), 10);
					// TODO Get_Fields: Field [cat3] not found!! (maybe it is an alias?)
					iLabel132:
					OutP.PPCode3 = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat3"))), 10);
					// TODO Get_Fields: Field [cat4] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat5] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat6] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat7] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat8] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat9] not found!! (maybe it is an alias?)
					iLabel133:
					lngTemp = Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat4")) + Conversion.Val(clsBill.Get_Fields("cat5")) + Conversion.Val(clsBill.Get_Fields("cat6")) + Conversion.Val(clsBill.Get_Fields("cat7")) + Conversion.Val(clsBill.Get_Fields("cat8")) + Conversion.Val(clsBill.Get_Fields("cat9")));
					// TODO Get_Fields: Field [cat3] not found!! (maybe it is an alias?)
					iLabel134:
					OutP.PPCode4 = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat3")) + lngTemp), 10);
					iLabel135:
					OutP.PPCode5 = modMain.NumtoString_6(lngTemp, 10);
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					iLabel136:
					OutP.TotalPP = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("ppvalue"))), 10);
					// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					iLabel137:
					if (Conversion.Val(clsBill.Get_Fields("ppexempt")) > Conversion.Val(clsBill.Get_Fields("ppvalue")))
					{
						iLabel138:
						OutP.TotalExempt = OutP.TotalPP;
						iLabel139:
						OutP.TotalExempt = OutP.TotalPP;
					}
					else
					{
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						iLabel140:
						OutP.TotalExempt = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("ppexempt"))), 10);
						// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						iLabel141:
						OutP.NetAssessment = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt"))), 10);
						// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						lngNetAssess += Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt")));
					}
					// temp fix for holden
					OutP.NetAssessment = modMain.NumtoString_6(lngNetAssess, 10);
					// now write the file
					WriteAnother:
					;
					iLabel142:
					FCFileSystem.FilePut(42, OutP, -1/*? lngRecNum */);
					iLabel143:
					lngRecNum += 1;
					if (chkNewOwnerCopy.CheckState == Wisej.Web.CheckState.Checked && OutP.BillType != "PP")
					{
						string temp = "";
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (modMain.HasNewOwner_8(clsBill.Get_Fields("Account"), clsBill.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strTemp, ref strCity, ref strState, ref strZip, ref temp, ref temp))
						{
							OutP.Name = Strings.Left(strOldName + Strings.StrDup(34, " "), 34);
							OutP.Address1 = Strings.Left(strSecOwner + Strings.StrDup(34, " "), 34);
							OutP.Address2 = Strings.Left(strAddr1 + Strings.StrDup(34, " "), 34);
							OutP.City = Strings.Left(strCity + Strings.StrDup(24, " "), 24);
							OutP.State = Strings.Left(strState + "  ", 2);
							OutP.Zip = Strings.Left(strZip + "     ", 5);
							FCFileSystem.FilePut(42, OutP, -1/*? lngRecNum */);
							lngRecNum += 1;
							lngNumNewNames += 1;
							dblTotalCopiesRENetOwed += FCConvert.ToDouble(OutP.TotalTax);
						}
					}
					if (chkMHCopy.CheckState == Wisej.Web.CheckState.Checked && OutP.BillType != "PP")
					{
						clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' and account = " + OutP.Account, "CentralData");
						while (!clsMortgageAssociations.EndOfFile())
						{
							clsMortgageHolders.OpenRecordset("select * from mortgageholders where ID = " + clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"), "CentralData");
							if (!clsMortgageHolders.EndOfFile())
							{
								OutP.Name = Strings.Left(clsBill.Get_Fields_String("name1") + Strings.StrDup(34, " "), 34);
								OutP.Address1 = Strings.Left("C/O " + clsMortgageHolders.Get_Fields_String("name") + Strings.StrDup(34, " "), 34);
								OutP.Address2 = Strings.Left(Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1"))) + Strings.StrDup(34, " "), 34);
								OutP.City = Strings.Left(clsMortgageHolders.Get_Fields_String("City") + Strings.StrDup(24, " "), 24);
								OutP.State = Strings.Left(clsMortgageHolders.Get_Fields_String("state") + "  ", 2);
								OutP.Zip = Strings.Left(clsMortgageHolders.Get_Fields_String("Zip") + "     ", 5);
								FCFileSystem.FilePut(42, OutP, -1/*? lngRecNum */);
								lngRecNum += 1;
								lngMHCopies += 1;
								dblTotalCopiesRENetOwed += FCConvert.ToDouble(OutP.TotalTax);
							}
							clsMortgageAssociations.MoveNext();
						}
					}
					// Print #42, OutP.BillType;
					// Print #42, OutP.Account;
					// Print #42, OutP.Location;
					// Print #42, OutP.MapLot;
					// Print #42, OutP.Name;
					// Print #42, OutP.Address1;
					// Print #42, OutP.Address2;
					// Print #42, OutP.City;
					// Print #42, OutP.State;
					// Print #42, OutP.Zip;
					// Print #42, OutP.Land;
					// Print #42, OutP.Building;
					// Print #42, OutP.Total;
					// Print #42, OutP.PPCode1;
					// Print #42, OutP.PPCode2;
					// Print #42, OutP.PPCode3;
					// Print #42, OutP.PPCode4;
					// Print #42, OutP.PPCode5;
					// Print #42, OutP.TotalPP;
					// Print #42, OutP.TotalExempt;
					// Print #42, OutP.NetAssessment;
					// Print #42, OutP.TotalTax;
					// Print #42, OutP.TaxDue1;
					// Print #42, OutP.TaxDue2;
					// Print #42, OutP.TaxDue3;
					// Print #42, OutP.TaxDue4;
					// Print #42, OutP.BookPage;
					// Print #42, OutP.HomesteadExempt;
					// Print #42, OutP.OtherExempt;
					// Print #42, OutP.Distribution1;
					// Print #42, OutP.Distribution2;
					// Print #42, OutP.Distribution3;
					// Print #42, OutP.Distribution4;
					// Print #42, OutP.Distribution5;
					// Print #42, OutP.DistPercent1;
					// Print #42, OutP.DistPercent2;
					// Print #42, OutP.DistPercent3;
					// Print #42, OutP.DistPercent4;
					// Print #42, OutP.DistPercent5;
					// Print #42, OutP.PaidToDate
					// Print #42, vbNewLine
					iLabel144:
					clsBill.MoveNext();
				}
				FCFileSystem.FileClose();
				BuildFixed = true;
				// MsgBox "Exempt total = " & Format(lngTestExempt, "###,###,###,##0")
				return BuildFixed;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In BuildFixed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildFixed;
		}

		private bool BuildComma_6(int lngYear, short intModule, bool boolCombined)
		{
			return BuildComma(lngYear, intModule, boolCombined);
		}

		private bool BuildComma_24(int lngYear, short intModule, bool boolCombined)
		{
			return BuildComma(lngYear, intModule, boolCombined);
		}

		private bool BuildComma(int lngYear, short intModule, bool boolCombined)
		{
			bool BuildComma = false;
			modTypes.OutPrintCommaFormat OutP = new modTypes.OutPrintCommaFormat(0);
			bool boolCalculate = false;
			clsDRWrapper clsBill = new clsDRWrapper();
			// vbPorter upgrade warning: strBillType As string	OnRead(FixedString)
			string strBillType = "";
			string strTemp;
			// Dim clsAggregate As New clsDRWrapper
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTbl1 = "";
			string strTbl2 = "";
			string strSQL = "";
			string strQry1 = "";
			string strQry2 = "";
			string strRE = "";
			bool boolREFile = false;
			clsDRWrapper clsDist = new clsDRWrapper();
			// holds the distribution information
			double[] DistPerc = new double[5 + 1];
			int x;
			double dblTax = 0;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			long lngTemp = 0;
			// temp variable for adding sor a total
			double dblLeftOver = 0;
			string[] strAry = null;
			string strNewName = "";
			string strSecOwner = "";
			string strCity = "";
			string strState = "";
			string strZip = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strMailingAddress3 = "";
			clsDRWrapper clsMortgageHolders = new clsDRWrapper();
			clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
			clsRateRecord rrRateInfo = new clsRateRecord();
			double dblDiscountPerc;
			StreamWriter ts = null;
			bool boolFileOpen = false;
			string strRecord;
			int intExempt = 0;
			clsDRWrapper clsexempt = new clsDRWrapper();
			clsDRWrapper rsIPs = new clsDRWrapper();
			int y;
			double[] dblAbate = new double[4 + 1];
			double[] dblTaxDue = new double[4 + 1];
			bool boolOutput3rdAddressLine;
			bool boolOutputPastDue;
			string strInformationMessage = "";
			string strRemittanceMessage = "";
			FCCollection colInformationMessage = new FCCollection();
			FCCollection colRemittanceMessage = new FCCollection();
			string strDelimChar = "";
			object varTemp;
			string resultFile = "";
			try
			{
				// On Error GoTo ErrorHandler
				// Load frmWait
				// frmWait.Init ("Please wait while the Out-Print file is created.")
				// DoEvents
				modErrorHandler.Statics.gstrCurrentRoutine = "Build Delimited Format";
				// If chkInlcude3rdAddress.Value = vbChecked Then
				boolOutput3rdAddressLine = true;
				// Else
				// boolOutput3rdAddressLine = False
				// End If
				// If chkIncludePastDue.Value = vbChecked Then
				boolOutputPastDue = true;
				// Else
				// boolOutputPastDue = False
				// End If
				// If chkIncludeInformationSection.Value = vbChecked Then
				// boolIncludeInformationMessage = True
				// Else
				// boolIncludeInformationMessage = False
				// End If
				boolFileOpen = false;
				BuildComma = false;
				// If boolIncludeInformationMessage Then
				// Call frmLaserMessage.Show(vbModal)
				// Call clsTemp.OpenRecordset("select * from taxmessage order by [line]", "twbl0000.vb1")
				// Do While Not clsTemp.EndOfFile
				// colInformationMessage.Add (Replace(clsTemp.Fields("message"), Chr(34), ""))
				// clsTemp.MoveNext
				// Loop
				// Call clsTemp.OpenRecordset("select * from remittancemessage order by [line]", "twbl0000.vb1")
				// Do While Not clsTemp.EndOfFile
				// colRemittanceMessage.Add (Replace(clsTemp.Fields("message"), Chr(34), ""))
				// clsTemp.MoveNext
				// Loop
				// x = colInformationMessage.Count
				// Do While x > 0
				// If Trim(colInformationMessage[x]) = "" Then
				// Call colInformationMessage.Remove(x)
				// x = x - 1
				// Else
				// Exit Do
				// End If
				// Loop
				// x = colRemittanceMessage.Count
				// Do While x > 0
				// If Trim(colRemittanceMessage[x]) = "" Then
				// Call colRemittanceMessage.Remove(x)
				// x = x - 1
				// Else
				// Exit Do
				// End If
				// Loop
				// End If
				//! Load frmWait;
				frmWait.InstancePtr.Init("Please wait while the Out-Print file is created.");
				//Application.DoEvents();
				clsexempt.OpenRecordset("select * from exemptCODE order by code", modGlobalVariables.strREDatabase);
				// get the distribution information first
				clsDist.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				for (x = 1; x <= 5; x++)
				{
					DistPerc[x] = 0;
				}
				// x
				if (clsDist.EndOfFile())
				{
					// MsgBox "The distribution information must be filled out before creating outprint files.", vbExclamation, "Missing Distribution"
					frmWait.InstancePtr.Unload();
					frmDistribution.InstancePtr.Init();
					frmWait.InstancePtr.Init("Please wait while the Out-Print file is created.");
					clsDist.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				}
				while (!clsDist.EndOfFile())
				{
					if (clsDist.Get_Fields_Int16("distributionnumber") > 5)
						break;
					DistPerc[clsDist.Get_Fields_Int16("distributionnumber")] = clsDist.Get_Fields_Double("distributionpercent");
					clsDist.MoveNext();
				}
				FCFileSystem.FileClose();
				//Application.DoEvents();
				if (intModule == 0)
				{
					// real estate
					boolREFile = true;
					if (FCFileSystem.FileExists("TSREBill.fil"))
					{
						FCFileSystem.DeleteFile("tsrebill.fil");
					}
					ts = FCFileSystem.CreateTextFile("TSREBill.fil");
					//FC:FINAL:MSH - issue #1866: save name of the file, which will be created for next downloading
					resultFile = "TSREBill.fil";
					// Open "TSREBill.fil" For Output As #42 Len = Len(OutP)
					boolFileOpen = true;
					if (boolCombined)
					{
						// use the twre link table so that we can join with the association table
						// strRE = "select * from master innerjoin ["
						strTbl1 = "(select account as ac,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,PPASSESSMENT,principalpaid,0 as exemptvalue,tgsoftacres,tgmixedacres,tghardacres,tgsoftvalue,tgmixedvalue,tghardvalue  from billingmaster where billingtype = 'RE' AND ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						strQry2 = "select ac,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid,exemptvalue from ";
						strQry1 = strQry2 + strTbl1;
						strTbl2 = "(select moduleassociation.remasteracct as ac, taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid,exemptvalue,tgsoftacres,tgmixedacres,tghardacres,tgsoftvalue,tgmixedvalue,tghardvalue  from billingmaster inner join " + clsBill.CurrentPrefix + "CentralData.dbo.moduleassociation on (billingmaster.account = moduleassociation.account) where moduleassociation.module = 'PP' and moduleassociation.primaryassociate = -1 and billingmaster.billingtype = 'PP' and billingmaster.ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						strQry2 += strTbl2;
						strSQL = "SELECT ac, sum(taxdue1) AS tax1, sum( taxdue2) AS tax2,sum(taxdue3) as tax3,sum(taxdue4) as tax4 ,sum(landvalue) as landsum,sum(buildingvalue) as buildingsum,sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9, sum(homesteadexemption) as homestead,sum(otherexempt1) as otherexemptsum,sum(ppassessment) as PPValue,sum(principalpaid) as prepaid,sum(exemptvalue) as ppexempt,sum(tgsoftacres) as softacres,sum(tgmixedacres) as mixedacres,sum(tghardacres) as hardacres,sum(tgsoftvalue) as softvalue,sum(tgmixedvalue) as mixedvalue,sum(tghardvalue) as hardvalue from (" + strQry1 + " union " + strQry2 + " as temp) AS TblCombined group by ac";
						// this procedure will get the merged taxbills
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from BILLINGmaster inner join (" + strSQL + ") as billfixedformat on (billfixedformat.ac = billingmaster.account) where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " order by billingmaster.name1";
						}
						else
						{
							strSQL = "select * from BILLINGmaster inner join (" + strSQL + ") as billfixedformat on (billfixedformat.ac = billingmaster.account) where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and (tax1 > 0 or tax2 > 0 or tax3 > 0 or tax4 > 0) and (tax1 + tax2 + tax3 + tax4 - prepaid > 0) order by billingmaster.name1";
						}
					}
					else
					{
						// use the twre link table so that we can join with the association table
						strTbl1 = "(select account as ac,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,PPASSESSMENT,principalpaid,exemptvalue,TGSOFtacres,tgmixedacres,tghardacres,tgsoftvalue,tgmixedvalue,tghardvalue  from billingmaster where billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						strQry2 = "select ac,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid,exemptvalue,tgsoftacres,tgmixedacres,tghardacres,tgsoftvalue,tgmixedvalue,tghardvalue from ";
						strQry1 = strQry2 + strTbl1;
						strSQL = "SELECT ac, sum(taxdue1) AS tax1, sum( taxdue2) AS tax2,sum(taxdue3) as tax3,sum(taxdue4) as tax4 ,sum(landvalue) as landsum,sum(buildingvalue) as buildingsum,sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9, sum(homesteadexemption) as homestead,sum(otherexempt1) as otherexemptsum,sum(ppassessment) as PPValue,sum(principalpaid) as prepaid,sum(exemptvalue) as ppexempt,sum(tgsoftacres) as softacres,sum(tgmixedacres) as mixedacres,sum(tghardacres) as hardacres,sum(tgsoftvalue) as softvalue,sum(tgmixedvalue) as mixedvalue,sum(tghardvalue) as hardvalue from (" + strQry1 + " as temp) AS TblCombined group by ac";
						// this procedure will get the merged taxbills
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from billingmaster inner join (" + strSQL + ") as billfixedformat  on (billingmaster.account = billfixedformat.ac)   where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + "  ORDER by billingmaster.name1";
						}
						else
						{
							strSQL = "select * from billingmaster inner join (" + strSQL + ") as billfixedformat  on (billingmaster.account = billfixedformat.ac)   where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and (isnull(tax1,0) > 0 or isnull(tax2,0) > 0 or isnull(tax3,0) > 0 or isnull(tax4,0) > 0) and (isnull(tax1,0) + isnull(tax2,0) + isnull(tax3,0) + isnull(tax4,0) - isnull(prepaid,0) > 0) ORDER by billingmaster.name1";
						}
					}
					clsBill.OpenRecordset(strSQL, "twcl0000.vb1");
					if (clsBill.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						//Application.DoEvents();
						MessageBox.Show("No Real Estate Bills found", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return BuildComma;
					}
					strBillType = "RE";
				}
				else if (intModule == 1)
				{
					// personal property
					boolREFile = false;
					//FC:FINAL:RPU:#i1445 - Use FCFileSystem instead of File
					if (FCFileSystem.FileExists("TSppBill.fil"))
					{
						FCFileSystem.DeleteFile("tsppbill.fil");
					}
					ts = FCFileSystem.CreateTextFile("TSppBill.fil");
					//FC:FINAL:MSH - issue #1866: save name of the file, which will be created for next downloading
					resultFile = "TSppBill.fil";
					strBillType = "PP";
					if (boolCombined)
					{
						strTbl1 = "select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as ppexempt from billingmaster left join [(select * from " + clsBill.CurrentPrefix + "CentralData.dbo.moduleassociation where module = 'PP' and primaryassociate = -1)]. as Tbl1 on (billingmaster.account = tbl1.account) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + "";
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from billingmaster inner join (" + strTbl1 + ") as billfixedformat on (billingmaster.account = billfixedformat.ac) where billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + "  order by billingmaster.name1";
						}
						else
						{
							strSQL = "select * from billingmaster inner join (" + strTbl1 + ") as billfixedformat on (billingmaster.account = billfixedformat.ac) where billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and (tax1 > 0 or tax2 > 0 or tax3 > 0 or tax4 > 0) and (tax1 + tax2 + tax3 + tax4 - prepaid > 0) order by billingmaster.name1";
						}
					}
					else
					{
						strTbl1 = "select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as ppexempt from billingmaster where  billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + "";
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from billingmaster inner join (" + strTbl1 + ") as billfixedformat on (billingmaster.account = billfixedformat.ac) where billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " order by billingmaster.name1";
						}
						else
						{
							strSQL = "select * from billingmaster inner join (" + strTbl1 + ") as billfixedformat on (billingmaster.account = billfixedformat.ac) where billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and (isnull(tax1,0) > 0 or isnull(tax2,0) > 0 or isnull(tax3,0) > 0 or isnull(tax4,0) > 0) and (isnull(tax1,0) + isnull(tax2,0) + isnull(tax3,0) + isnull(tax4,0) - isnull(prepaid,0) > 0) order by billingmaster.name1";
						}
					}
					clsBill.OpenRecordset(strSQL, "twcl0000.vb1");
					if (clsBill.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						//Application.DoEvents();
						MessageBox.Show("No Personal Property Bills Found", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return BuildComma;
					}
				}
				dblDiscountPerc = 0;
				// GoTo DontMakeHeader
				// write header record
				if (!rrRateInfo.LoadRate(lngRateRecToPrint))
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Unable to load rate record information for rate key " + FCConvert.ToString(lngRateRecToPrint), "Missing Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return BuildComma;
				}
				strRecord = FCConvert.ToString(Convert.ToChar(34)) + "HR" + FCConvert.ToString(Convert.ToChar(34)) + ",";
				// header record
				if (!modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
				{
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + modGlobalConstants.Statics.MuniName + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				else
				{
					clsTemp = new clsDRWrapper();
					clsTemp.OpenRecordset("select top 1 trancode from billingmaster where ratekey = " + FCConvert.ToString(lngRateRecToPrint), "twcl0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						if (Conversion.Val(clsTemp.Get_Fields("trancode")) > 0)
						{
							// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + modRegionalTown.GetTownKeyName_2(FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("trancode")))) + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
						else
						{
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + modGlobalConstants.Statics.MuniName + FCConvert.ToString(Convert.ToChar(34)) + ",";
						}
					}
					else
					{
						strRecord += FCConvert.ToString(Convert.ToChar(34)) + modGlobalConstants.Statics.MuniName + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
				}
				//Application.DoEvents();
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + strBillType + FCConvert.ToString(Convert.ToChar(34)) + ",";
				// indicate what is in file
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(modMain.DblToString_6(DistPerc[1], 6, 3)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				// distribution percentages
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(modMain.DblToString_6(DistPerc[2], 6, 3)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(modMain.DblToString_6(DistPerc[3], 6, 3)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(modMain.DblToString_6(DistPerc[4], 6, 3)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(modMain.DblToString_6(DistPerc[5], 6, 3)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.BillingDate, "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(rrRateInfo.TaxYear) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(rrRateInfo.TaxRate * 1000) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(rrRateInfo.InterestRate * 100) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(rrRateInfo.NumberOfPeriods) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.Get_DueDate(1), "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.Get_InterestStartDate(1), "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				if (rrRateInfo.NumberOfPeriods > 1)
				{
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.Get_DueDate(2), "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.Get_InterestStartDate(2), "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				else
				{
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + "" + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + "" + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (rrRateInfo.NumberOfPeriods > 2)
				{
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.Get_DueDate(3), "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.Get_InterestStartDate(3), "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				else
				{
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + "" + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + "" + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (rrRateInfo.NumberOfPeriods > 3)
				{
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.Get_DueDate(4), "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(rrRateInfo.Get_InterestStartDate(4), "MM/dd/yyyy") + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				else
				{
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + "" + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + "" + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				if (chkDiscount.CheckState == Wisej.Web.CheckState.Checked)
				{
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(txtPercent.Text, "0.00") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					dblDiscountPerc = Conversion.Val(txtPercent.Text) / 100;
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + txtDate.Text + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				else
				{
					dblDiscountPerc = 0;
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				}
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[1]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[2]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[3]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[4]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[5]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[6]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[7]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[8]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + Strings.Trim(strCatDesc[9]) + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + t2kIndebtednessDate.Text + FCConvert.ToString(Convert.ToChar(34)) + ",";
				strTemp = txtIndebtednessAmount.Text;
				strTemp = strTemp.Replace(",", "");
				strTemp = strTemp.Replace(" ", "");
				strTemp = Strings.Format(FCConvert.ToString(Conversion.Val(strTemp)), "#,###,###,###,##0.00");
				strRecord += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34));
				strRecord += "," + FCConvert.ToString(Convert.ToChar(34)) + FCConvert.ToString(Conversion.Val(txtStateAidReduction.Text)) + FCConvert.ToString(Convert.ToChar(34));
				// 235        If boolIncludeInformationMessage Then
				// strRecord = strRecord & "," & Chr(34)
				// strDelimChar = ""
				// 240        For Each varTemp In colInformationMessage
				// strRecord = strRecord & strDelimChar & varTemp
				// strDelimChar = vbNewLine
				// Next
				// 250        strRecord = strRecord & Chr(34) & "," & Chr(34)
				// strDelimChar = ""
				// 255        For Each varTemp In colRemittanceMessage
				// strRecord = strRecord & strDelimChar & varTemp
				// strDelimChar = vbNewLine
				// Next
				// 260        strRecord = strRecord & Chr(34)
				// End If
				ts.WriteLine(strRecord);
				DontMakeHeader:
				;
				int lngRecCount;
				lngRecCount = 0;
				// frmWait.lblCounter.Visible = True
				while (!clsBill.EndOfFile())
				{
					//Application.DoEvents();
					OutP.MortgageHolderID = "0";
					lngRecCount += 1;
					frmWait.InstancePtr.lblMessage.Text = "Creating Outprint Record " + "\r\n" + FCConvert.ToString(lngRecCount);
					frmWait.InstancePtr.lblMessage.Refresh();
					//Application.DoEvents();
					if (boolREFile)
					{
						OutP.BillType = strBillType;
						// This may change later if it is a combined
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						OutP.Account = FCConvert.ToString(clsBill.Get_Fields("account"));
						// If chkPrevious.Value = vbChecked And Val(clsBill.Fields("oldaccount")) > 0 Then
						// OutP.Name = Trim(clsBill.Fields("oldname"))
						// OutP.Address1 = Trim(clsBill.Fields("oldaddr1"))
						// OutP.Address2 = Trim(clsBill.Fields("oldaddr2"))
						// OutP.City = Trim(clsBill.Fields("oldcity"))
						// OutP.State = Trim(clsBill.Fields("oldstate"))
						// OutP.Zip = Trim(clsBill.Fields("oldzip"))
						// 
						// Else
						OutP.Name = Strings.Trim(Strings.Replace(FCConvert.ToString(clsBill.Get_Fields_String("name1")), "\r\n", "", 1, -1, CompareConstants.vbBinaryCompare));
						OutP.SecondOwner = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("name2")).Replace("\r\n", ""));
						OutP.Address1 = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address1")).Replace("\r\n", ""));
						OutP.Address2 = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address2")).Replace("\r\n", ""));
						OutP.Address3 = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("Mailingaddress3")).Replace("\r\n", ""));
						if (Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address3"))) != string.Empty)
						{
							strAry = Strings.Split(Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address3"))), " ", -1, CompareConstants.vbTextCompare);
							if (Information.UBound(strAry, 1) > 1)
							{
								if (strAry[Information.UBound(strAry, 1) - 1].Length == 2)
								{
									OutP.State = Strings.Left(strAry[Information.UBound(strAry, 1) - 1] + "  ", 2);
									strTemp = "";
									for (x = 0; x <= Information.UBound(strAry, 1) - 2; x++)
									{
										strTemp += strAry[x] + " ";
									}
									// x
								}
								else
								{
									if (Information.IsNumeric(strAry[Information.UBound(strAry, 1) - 1]))
									{
										OutP.State = strAry[Information.UBound(strAry, 1) - 2];
										strTemp = "";
										for (x = 0; x <= Information.UBound(strAry, 1) - 3; x++)
										{
											strTemp += strAry[x] + " ";
										}
										// x
									}
									else if (strAry[Information.UBound(strAry, 1) - 1].Length != 2 && strAry[Information.UBound(strAry, 1) - 2].Length == 2)
									{
										OutP.State = strAry[Information.UBound(strAry, 1) - 2];
										strTemp = "";
										for (x = 0; x <= Information.UBound(strAry, 1) - 3; x++)
										{
											strTemp += strAry[x] + " ";
										}
										// x
									}
									else if (strAry[Information.UBound(strAry, 1) - 1].Length == 2)
									{
										OutP.State = strAry[Information.UBound(strAry, 1) - 1];
										strTemp = "";
										for (x = 0; x <= Information.UBound(strAry, 1) - 2; x++)
										{
											strTemp += strAry[x] + " ";
										}
										// x
									}
									else
									{
										OutP.State = " ";
										strTemp = "";
										for (x = 0; x <= Information.UBound(strAry, 1) - 1; x++)
										{
											strTemp += strAry[x] + " ";
										}
										// x
									}
								}
								strTemp = Strings.Trim(strTemp);
								OutP.City = strTemp;
							}
							else if (Information.UBound(strAry, 1) > 0)
							{
								if (strAry[1].Length == 2)
								{
									OutP.State = Strings.Left(strAry[Information.UBound(strAry, 1)] + "  ", 2);
									OutP.City = strAry[0];
								}
								else
								{
									OutP.State = "  ";
									OutP.City = strAry[0] + " " + strAry[1];
								}
								// strTemp = ""
								// For x = 0 To UBound(strAry) - 1
								// strTemp = strTemp & strAry(x) & " "
								// Next x
								// strTemp = Trim(strTemp)
								// OutP.City = strTemp
							}
							else
							{
								OutP.State = " ";
								strTemp = strAry[0];
								OutP.City = strTemp;
							}
						}
						else
						{
							OutP.City = "";
							OutP.State = "";
						}
						OutP.Zip = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("zip")));
						// End If
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						if (Conversion.Val(clsBill.Get_Fields("streetnumber")) > 0)
						{
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							OutP.Location = Strings.Trim(clsBill.Get_Fields("streetnumber") + " " + clsBill.Get_Fields_String("apt") + " " + clsBill.Get_Fields_String("streetname"));
						}
						else
						{
							OutP.Location = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("streetname")));
						}
						OutP.MapLot = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("maplot")));
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						OutP.Land = Strings.Trim(modMain.NumtoString_8(clsBill.Get_Fields("landsum"), 20));
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						OutP.Building = Strings.Trim(modMain.NumtoString_8(clsBill.Get_Fields("buildingsum"), 20));
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						OutP.Total = Strings.Trim(modMain.NumtoString_8(clsBill.Get_Fields("buildingsum") + clsBill.Get_Fields("landsum"), 20));
						OutP.BookPage = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("bookpage")));
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						OutP.HomesteadExempt = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("homestead")))), 20));
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						OutP.OtherExempt = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("otherexemptsum")))), 20));
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						//FC:FINAL:CHN - issue #1520: Incorrect converting.
						// OutP.REExempt = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("homestead")))) + FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("otherexemptsum"))))), 20));
						OutP.REExempt = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64((Conversion.Val(clsBill.Get_Fields("homestead"))) + (Conversion.Val(clsBill.Get_Fields("otherexemptsum")))), 20));
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						lngTemp = Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("landsum")) + Conversion.Val(clsBill.Get_Fields("buildingsum")) - Conversion.Val(clsBill.Get_Fields("homestead")) - Conversion.Val(clsBill.Get_Fields("otherexemptsum")));
						dblTotalREAssess += lngTemp;
						OutP.NetREAssessment = Strings.Trim(modMain.NumtoString_6(lngTemp, 20));
						OutP.Ref1 = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("ref1")));
						OutP.Ref2 = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("ref2")));
						OutP.Acreage = Strings.Trim(modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields_Double("acres")))), 12));
						// TODO Get_Fields: Field [softacres] not found!! (maybe it is an alias?)
						OutP.SoftAcres = Strings.Trim(modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("softacres")))), 12));
						// TODO Get_Fields: Field [mixedacres] not found!! (maybe it is an alias?)
						OutP.MixedAcres = Strings.Trim(modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("mixedacres")))), 12));
						// TODO Get_Fields: Field [hardacres] not found!! (maybe it is an alias?)
						OutP.HardAcres = Strings.Trim(modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("hardacres")))), 12));
						// TODO Get_Fields: Field [softvalue] not found!! (maybe it is an alias?)
						OutP.SoftValue = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("softvalue")))), 20));
						// TODO Get_Fields: Field [mixedvalue] not found!! (maybe it is an alias?)
						OutP.MixedValue = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("mixedvalue")))), 20));
						// TODO Get_Fields: Field [hardvalue] not found!! (maybe it is an alias?)
						OutP.HardValue = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("hardvalue")))), 20));
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						dblTotalOriginalRETax += (Conversion.Val(clsBill.Get_Fields("tax1"))) + (Conversion.Val(clsBill.Get_Fields("tax2"))) + (Conversion.Val(clsBill.Get_Fields("tax3"))) + (Conversion.Val(clsBill.Get_Fields("tax4")));
						if (boolCombined)
						{
							// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
							if (Conversion.Val(clsBill.Get_Fields("ppexempt")) < Conversion.Val(clsBill.Get_Fields("ppvalue")))
							{
								// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
								dblTotalREAssess += Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt"));
							}
						}
					}
					else
					{
						// personal property
						OutP.BillType = strBillType;
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						OutP.Account = FCConvert.ToString(clsBill.Get_Fields("account"));
						// OutP.Name = Trim(clsBill.Fields("name"))
						OutP.Name = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("name1")));
						// OutP.Name = strTemp
						OutP.SecondOwner = "";
						// OutP.Location = Trim(clsBill.Fields("streetnumber") & " " & clsBill.Fields("street"))
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						OutP.Location = Strings.Trim(clsBill.Get_Fields("streetnumber") + " " + clsBill.Get_Fields_String("streetname"));
						OutP.MapLot = " ";
						OutP.Address1 = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("ADDRESS1")));
						OutP.Address2 = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address2")));
						OutP.Address3 = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("mailingaddress3")));
						// OutP.City = Trim(clsBill.Fields("city"))
						// 
						// OutP.State = Trim(clsBill.Fields("state"))
						// If Trim(clsBill.Fields("address3")) <> vbNullString Then
						// strAry = Split(Trim(clsBill.Fields("address3")), " ", , vbTextCompare)
						// If UBound(strAry) > 1 Then
						// OutP.State = Left(strAry(UBound(strAry) - 1) & "  ", 2)
						// strTemp = ""
						// For x = 0 To UBound(strAry) - 2
						// strTemp = strTemp & strAry(x) & " "
						// Next x
						// strTemp = Trim(strTemp)
						// OutP.City = strTemp
						// ElseIf UBound(strAry) > 0 Then
						// OutP.State = Left(strAry(UBound(strAry)) & "  ", 2)
						// strTemp = ""
						// For x = 0 To UBound(strAry) - 1
						// strTemp = strTemp & strAry(x) & " "
						// Next x
						// strTemp = Trim(strTemp)
						// OutP.City = strTemp
						// Else
						// OutP.State = " "
						// Mid(strTemp, 1) = strAry(0)
						// OutP.City = strTemp
						// End If
						// Else
						// OutP.City = ""
						// OutP.State = ""
						// End If
						if (Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address3"))) != string.Empty)
						{
							strAry = Strings.Split(Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address3"))), " ", -1, CompareConstants.vbTextCompare);
							if (Information.UBound(strAry, 1) > 1)
							{
								if (strAry[Information.UBound(strAry, 1) - 1].Length == 2)
								{
									OutP.State = Strings.Left(strAry[Information.UBound(strAry, 1) - 1] + "  ", 2);
									strTemp = "";
									for (x = 0; x <= Information.UBound(strAry, 1) - 2; x++)
									{
										strTemp += strAry[x] + " ";
									}
									// x
								}
								else
								{
									if (Information.IsNumeric(strAry[Information.UBound(strAry, 1) - 1]))
									{
										OutP.State = strAry[Information.UBound(strAry, 1) - 2];
										strTemp = "";
										for (x = 0; x <= Information.UBound(strAry, 1) - 3; x++)
										{
											strTemp += strAry[x] + " ";
										}
										// x
									}
									else if (strAry[Information.UBound(strAry, 1) - 1].Length != 2 && strAry[Information.UBound(strAry, 1) - 2].Length == 2)
									{
										OutP.State = strAry[Information.UBound(strAry, 1) - 2];
										strTemp = "";
										for (x = 0; x <= Information.UBound(strAry, 1) - 3; x++)
										{
											strTemp += strAry[x] + " ";
										}
										// x
									}
									else if (strAry[Information.UBound(strAry, 1) - 1].Length == 2)
									{
										OutP.State = strAry[Information.UBound(strAry, 1) - 1];
										strTemp = "";
										for (x = 0; x <= Information.UBound(strAry, 1) - 2; x++)
										{
											strTemp += strAry[x] + " ";
										}
										// x
									}
									else
									{
										OutP.State = " ";
										strTemp = "";
										for (x = 0; x <= Information.UBound(strAry, 1) - 1; x++)
										{
											strTemp += strAry[x] + " ";
										}
										// x
									}
								}
								strTemp = Strings.Trim(strTemp);
								OutP.City = strTemp;
							}
							else if (Information.UBound(strAry, 1) > 0)
							{
								if (strAry[1].Length == 2)
								{
									OutP.State = Strings.Left(strAry[Information.UBound(strAry, 1)] + "  ", 2);
									OutP.City = strAry[0];
								}
								else
								{
									OutP.State = "  ";
									OutP.City = strAry[0] + " " + strAry[1];
								}
								// strTemp = ""
								// For x = 0 To UBound(strAry) - 1
								// strTemp = strTemp & strAry(x) & " "
								// Next x
								// strTemp = Trim(strTemp)
								// OutP.City = strTemp
							}
							else
							{
								OutP.State = " ";
								strTemp = strAry[0];
								OutP.City = strTemp;
							}
						}
						else
						{
							OutP.City = "";
							OutP.State = "";
						}
						OutP.Zip = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("zip")));
						OutP.Land = "";
						OutP.Building = "";
						OutP.REExempt = "";
						OutP.Total = "";
						OutP.NetREAssessment = "";
						OutP.Ref1 = "";
						OutP.Ref2 = "";
						OutP.BookPage = "";
						OutP.HomesteadExempt = "";
						OutP.OtherExempt = "";
						OutP.Acreage = "";
						OutP.SoftAcres = "";
						OutP.SoftValue = "";
						OutP.MixedAcres = "";
						OutP.MixedValue = "";
						OutP.HardAcres = "";
						OutP.HardValue = "";
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						dblTotalOriginalPPTax += Conversion.Val(clsBill.Get_Fields("tax1")) + Conversion.Val(clsBill.Get_Fields("tax2")) + Conversion.Val(clsBill.Get_Fields("tax3")) + Conversion.Val(clsBill.Get_Fields("tax4"));
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBill.Get_Fields("ppexempt")) < Conversion.Val(clsBill.Get_Fields("ppvalue")))
						{
							// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
							dblTotalPPAssess += Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt"));
						}
					}
					for (x = 1; x <= 4; x++)
					{
						// TODO Get_Fields: Check the table for the column [tax] and replace with corresponding Get_Field method
						dblTaxDue[x] = Conversion.Val(clsBill.Get_Fields("tax" + FCConvert.ToString(x)));
					}
					// x
					OutP.TaxDue1 = Strings.Trim(modMain.DblToString_6(dblTaxDue[1], 20));
					OutP.TaxDue2 = Strings.Trim(modMain.DblToString_6(dblTaxDue[2], 20));
					OutP.TaxDue3 = Strings.Trim(modMain.DblToString_6(dblTaxDue[3], 20));
					OutP.TaxDue4 = Strings.Trim(modMain.DblToString_6(dblTaxDue[4], 20));
					if (boolOutputPastDue)
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						OutP.PastDue = Strings.Trim(modMain.DblToString_8(modREPPBudStuff.CalculateAccountTotalDue_20(clsBill.Get_Fields("account"), boolREFile, clsBill.Get_Fields_Int32("id")), 20));
					}
					boolCalculate = false;
					clsTemp.OpenRecordset("select count(account) as thecount from paymentrec where [year] between " + FCConvert.ToString(lngYear) + "1 and " + FCConvert.ToString(lngYear) + "9 and code = 'A' and period <> '1' and billcode = '" + Strings.UCase(Strings.Left(OutP.BillType, 1)) + "' and account = " + OutP.Account, modGlobalVariables.strCLDatabase);
					if (!clsTemp.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsTemp.Get_Fields("thecount")) > 0)
						{
							boolCalculate = true;
						}
					}
					for (x = 1; x <= 4; x++)
					{
						dblAbate[x] = 0;
					}
					// x
					if (boolCalculate)
					{
						clsTemp.OpenRecordset("select * from billingmaster where billingyear between " + FCConvert.ToString(lngYear) + "1 and " + FCConvert.ToString(lngYear) + "9 and billingtype = '" + OutP.BillType + "' and account = " + OutP.Account, modGlobalVariables.strCLDatabase);
						while (!clsTemp.EndOfFile())
						{
							//Application.DoEvents();
							modCLCalculations.CheckForAbatementPayments(clsTemp.Get_Fields_Int32("id"), clsTemp.Get_Fields_Int32("ratekey"), boolREFile, ref dblAbate[1], ref dblAbate[2], ref dblAbate[3], ref dblAbate[4], Conversion.Val(clsTemp.Get_Fields_Decimal("taxdue1")), Conversion.Val(clsTemp.Get_Fields_Decimal("taxdue2")), Conversion.Val(clsTemp.Get_Fields_Decimal("taxdue3")), Conversion.Val(clsTemp.Get_Fields_Decimal("taxdue4")));
							for (x = 1; x <= 4; x++)
							{
								dblTaxDue[x] -= dblAbate[x];
							}
							// x
							clsTemp.MoveNext();
						}
					}
					dblTax = FCConvert.ToDouble(OutP.TaxDue1) + FCConvert.ToDouble(OutP.TaxDue2) + FCConvert.ToDouble(OutP.TaxDue3) + FCConvert.ToDouble(OutP.TaxDue4);
					// OutP.DiscountAmount = Trim(DblToString(dblTax - Format(dblDiscountPerc * dblTax, "0.00"), 20))
					OutP.DiscountAmount = Strings.Trim(modMain.DblToString_8(dblTax - FCConvert.ToDouble(Strings.Format(modMain.Round(dblDiscountPerc * dblTax, 2), "0.00")), 20));
					OutP.DistPercent1 = Strings.Trim(modMain.DblToString_6(DistPerc[1], 6, 3));
					OutP.DistPercent2 = Strings.Trim(modMain.DblToString_6(DistPerc[2], 6, 3));
					OutP.DistPercent3 = Strings.Trim(modMain.DblToString_6(DistPerc[3], 6, 3));
					OutP.DistPercent4 = Strings.Trim(modMain.DblToString_6(DistPerc[4], 6, 3));
					OutP.DistPercent5 = Strings.Trim(modMain.DblToString_6(DistPerc[5], 6, 3));
					OutP.Distribution1 = Strings.Trim(modMain.DblToString_8(DistPerc[1] / 100 * dblTax, 20));
					OutP.Distribution2 = Strings.Trim(modMain.DblToString_8(DistPerc[2] / 100 * dblTax, 20));
					OutP.Distribution3 = Strings.Trim(modMain.DblToString_8(DistPerc[3] / 100 * dblTax, 20));
					OutP.Distribution4 = Strings.Trim(modMain.DblToString_8(DistPerc[4] / 100 * dblTax, 20));
					OutP.Distribution5 = Strings.Trim(modMain.DblToString_8(DistPerc[5] / 100 * dblTax, 20));
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					OutP.PaidToDate = Strings.Trim(modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("prepaid")))), 20));
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					dblLeftOver = Conversion.Val(clsBill.Get_Fields("prepaid"));
					if (boolREFile)
					{
						dblTotalREPrePayments += dblLeftOver;
					}
					else
					{
						dblTotalPPPrePayments += dblLeftOver;
					}
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					dblTax -= Conversion.Val(clsBill.Get_Fields("prepaid"));
					if (boolREFile)
					{
						dblTotalRENetTaxOwed += dblTax;
					}
					else
					{
						dblTotalPPNetTaxOwed += dblTax;
					}
					OutP.TotalTax = Strings.Trim(modMain.DblToString_6(dblTax, 20));
					if (dblLeftOver > 0)
					{
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax1")))
						{
							OutP.TaxDue1 = modMain.DblToString_8(0, 10);
							// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax1"));
						}
						else
						{
							// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
							OutP.TaxDue1 = modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("tax1")))) - dblLeftOver, 10);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax2")))
						{
							OutP.TaxDue2 = modMain.DblToString_8(0, 10);
							// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax2"));
						}
						else
						{
							// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
							OutP.TaxDue2 = modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("tax2")))) - dblLeftOver, 10);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax3")))
						{
							OutP.TaxDue3 = modMain.DblToString_8(0, 10);
							// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax3"));
						}
						else
						{
							// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
							OutP.TaxDue3 = modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("tax3")))) - dblLeftOver, 10);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax4")))
						{
							OutP.TaxDue4 = modMain.DblToString_8(0, 10);
							// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax4"));
						}
						else
						{
							// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
							OutP.TaxDue4 = modMain.DblToString_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("tax4")))) - dblLeftOver, 10);
							dblLeftOver = 0;
						}
					}
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					if (boolREFile && (Conversion.Val(clsBill.Get_Fields("ppvalue")) > 0) && boolCombined)
					{
						// combined bill
						OutP.BillType = "CM";
					}
					// Personal property stuff
					// TODO Get_Fields: Field [cat1] not found!! (maybe it is an alias?)
					OutP.PPCode1 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat1")))), 20));
					// TODO Get_Fields: Field [cat2] not found!! (maybe it is an alias?)
					OutP.PPCode2 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat2")))), 20));
					// TODO Get_Fields: Field [cat3] not found!! (maybe it is an alias?)
					OutP.PPCode3 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat3")))), 20));
					// TODO Get_Fields: Field [cat4] not found!! (maybe it is an alias?)
					OutP.PPCode4 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat4")))), 20));
					// TODO Get_Fields: Field [cat5] not found!! (maybe it is an alias?)
					OutP.PPCode5 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat5")))), 20));
					// TODO Get_Fields: Field [cat6] not found!! (maybe it is an alias?)
					OutP.PPCode6 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat6")))), 20));
					// TODO Get_Fields: Field [cat7] not found!! (maybe it is an alias?)
					OutP.PPCode7 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat7")))), 20));
					// TODO Get_Fields: Field [cat8] not found!! (maybe it is an alias?)
					OutP.PPCode8 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat8")))), 20));
					// TODO Get_Fields: Field [cat9] not found!! (maybe it is an alias?)
					OutP.PPcode9 = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("cat9")))), 20));
					// lngtemp = Val(clsBill.Fields("cat4")) + Val(clsBill.Fields("cat5")) + Val(clsBill.Fields("cat6")) + Val(clsBill.Fields("cat7")) + Val(clsBill.Fields("cat8")) + Val(clsBill.Fields("cat9"))
					// OutP.PPCode4 = Trim(NumtoString(Val(clsBill.Fields("cat3")) + lngtemp, 20))
					// OutP.PPCode5 = Trim(NumtoString(lngtemp, 20))
					//Application.DoEvents();
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					OutP.TotalPP = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("ppvalue")))), 20));
					// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					if (Conversion.Val(clsBill.Get_Fields("ppexempt")) > Conversion.Val(clsBill.Get_Fields("ppvalue")))
					{
						OutP.TotalExempt = OutP.TotalPP;
						OutP.NetAssessment = OutP.TotalPP;
					}
					else
					{
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						OutP.TotalExempt = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("ppexempt")))), 20));
						// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						OutP.NetAssessment = Strings.Trim(modMain.NumtoString_8(clsBill.Get_Fields("ppvalue") - clsBill.Get_Fields("ppexempt"), 20));
					}
					OutP.REExemptDesc[0] = "";
					OutP.REExemptDesc[1] = "";
					OutP.REExemptDesc[2] = "";
					OutP.REExemptAmounts[0] = "";
					OutP.REExemptAmounts[1] = "";
					OutP.REExemptAmounts[2] = "";
					if (boolREFile)
					{
						intExempt = 0;
						for (x = 1; x <= 3; x++)
						{
							//Application.DoEvents();
							if (Conversion.Val(clsBill.Get_Fields_Int32("exempt" + FCConvert.ToString(x))) > 0)
							{
								if (clsexempt.FindFirstRecord("Code", Conversion.Val(clsBill.Get_Fields_Int32("exempt" + FCConvert.ToString(x)))))
								{
									// TODO Get_Fields: Check the table for the column [Category] and replace with corresponding Get_Field method
									if (!(clsexempt.Get_Fields_Int32("Category") == 1))
									{
										intExempt += 1;
										if (intExempt < 3)
										{
											// TODO Get_Fields: Field [OtherExempt] not found!! (maybe it is an alias?)
											OutP.REExemptAmounts[x - 1] = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields("OtherExempt" + FCConvert.ToString(intExempt))))), 20));
										}
									}
									else
									{
										OutP.REExemptAmounts[x - 1] = Strings.Trim(modMain.NumtoString_8(Convert.ToInt64(FCConvert.ToString(Conversion.Val(clsBill.Get_Fields_Double("HomesteadExemptION")))), 20));
									}
									OutP.REExemptDesc[x - 1] = Strings.Trim(FCConvert.ToString(clsexempt.Get_Fields_String("Description")));
									if (OutP.REExemptDesc[x - 1] != "")
									{
										OutP.REExemptDesc[x - 1] = Strings.Replace(OutP.REExemptDesc[x - 1], ".", "", 1, -1, CompareConstants.vbTextCompare);
									}
								}
							}
						}
						// x
					}
					// now  the file
					WriteAnother:
					;
					strRecord = FCConvert.ToString(Convert.ToChar(34)) + OutP.BillType + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Account + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Location + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MapLot + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Name.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address1.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address2.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.City.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.State + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Zip + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Land + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Building + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Total + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode6 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode7 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode8 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPcode9 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalPP + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.NetAssessment + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalTax + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.BookPage + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HomesteadExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.OtherExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PaidToDate + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.NetREAssessment + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Ref1.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Ref2.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SecondOwner.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Acreage + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MortgageHolderID + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SoftAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MixedAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HardAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SoftValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MixedValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HardValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DiscountAmount + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[0] + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[0] + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[1] + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[1] + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[2] + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[2] + FCConvert.ToString(Convert.ToChar(34)) + ",";
					strRecord += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34));
					if (boolOutput3rdAddressLine)
					{
						strRecord += ",";
						strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address3 + FCConvert.ToString(Convert.ToChar(34));
					}
					if (boolOutputPastDue)
					{
						strRecord += ",";
						strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PastDue + FCConvert.ToString(Convert.ToChar(34));
					}
					ts.WriteLine(strRecord);
					// Print #42, vbNewLine
					//Application.DoEvents();
					if (chkNewOwnerCopy.CheckState == Wisej.Web.CheckState.Checked && OutP.BillType != "PP")
					{
						strSecOwner = "";
						string temp = "";
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (modMain.HasNewOwner_8(clsBill.Get_Fields("Account"), clsBill.Get_Fields_String("name1"), ref strNewName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strTemp, ref strCity, ref strState, ref strZip, ref strMailingAddress3, ref temp))
						{
							OutP.Name = Strings.Trim(strNewName);
							OutP.Address1 = strAddr1;
							OutP.Address2 = strAddr2;
							OutP.Address3 = strMailingAddress3;
							OutP.City = strCity;
							OutP.State = strState;
							OutP.Zip = strZip;
							OutP.SecondOwner = strSecOwner;
							strRecord = FCConvert.ToString(Convert.ToChar(34)) + OutP.BillType + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Account + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Location.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MapLot + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Name.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address1.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address2.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.City.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.State + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Zip + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Land + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Building + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Total + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode6 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode7 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode8 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPcode9 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalPP + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.NetAssessment + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalTax + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.BookPage + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HomesteadExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.OtherExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PaidToDate + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.NetREAssessment + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Ref1.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Ref2.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SecondOwner.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Acreage + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MortgageHolderID + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SoftAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MixedAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HardAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SoftValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MixedValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HardValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DiscountAmount + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[0] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[0] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[1] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[1] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[2] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[2] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34));
							if (boolOutput3rdAddressLine)
							{
								strRecord += ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address3 + FCConvert.ToString(Convert.ToChar(34));
							}
							if (boolOutputPastDue)
							{
								strRecord += ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PastDue + FCConvert.ToString(Convert.ToChar(34));
							}
							ts.WriteLine(strRecord);
							lngNumNewNames += 1;
							dblTotalCopiesRENetOwed += FCConvert.ToDouble(OutP.TotalTax);
						}
					}
					if (chkInterestedParties.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (!(OutP.BillType == "CM"))
						{
							rsIPs.OpenRecordset("select * from owners where  module = '" + OutP.BillType + "' and billyear = " + FCConvert.ToString(lngYear) + " and account = " + OutP.Account + " order by name", modGlobalVariables.strCLDatabase);
						}
						else
						{
							rsIPs.OpenRecordset("select * from owners where  module = 'RE' and billyear = " + FCConvert.ToString(lngYear) + " and account = " + OutP.Account + " order by name", modGlobalVariables.strCLDatabase);
						}
						while (!rsIPs.EndOfFile())
						{
							//Application.DoEvents();
							OutP.Name = Strings.Trim(FCConvert.ToString(rsIPs.Get_Fields_String("Name")));
							OutP.Address1 = FCConvert.ToString(rsIPs.Get_Fields_String("Address1"));
							OutP.Address2 = FCConvert.ToString(rsIPs.Get_Fields_String("Address2"));
							OutP.Address3 = "";
							OutP.City = FCConvert.ToString(rsIPs.Get_Fields_String("city"));
							OutP.State = FCConvert.ToString(rsIPs.Get_Fields_String("state"));
							OutP.Zip = FCConvert.ToString(rsIPs.Get_Fields_String("Zip"));
							OutP.SecondOwner = "";
							strRecord = FCConvert.ToString(Convert.ToChar(34)) + OutP.BillType + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Account + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Location.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MapLot + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Name.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address1.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address2.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.City.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.State + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Zip + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Land + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Building + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Total + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode6 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode7 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode8 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPcode9 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalPP + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.NetAssessment + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalTax + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.BookPage + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HomesteadExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.OtherExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PaidToDate + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.NetREAssessment + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Ref1.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Ref2.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SecondOwner.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Acreage + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MortgageHolderID + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SoftAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MixedAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HardAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SoftValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MixedValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HardValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DiscountAmount + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[0] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[0] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[1] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[1] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[2] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[2] + FCConvert.ToString(Convert.ToChar(34)) + ",";
							strRecord += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34));
							if (boolOutput3rdAddressLine)
							{
								strRecord += ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address3 + FCConvert.ToString(Convert.ToChar(34));
							}
							if (boolOutputPastDue)
							{
								strRecord += ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PastDue + FCConvert.ToString(Convert.ToChar(34));
							}
							ts.WriteLine(strRecord);
							lngNumIPs += 1;
							dblTotalCopiesRENetOwed += FCConvert.ToDouble(OutP.TotalTax);
							rsIPs.MoveNext();
						}
					}
					if (chkMHCopy.CheckState == Wisej.Web.CheckState.Checked && OutP.BillType != "PP")
					{
						clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' and account = " + OutP.Account, "CentralData");
						while (!clsMortgageAssociations.EndOfFile())
						{
							//Application.DoEvents();
							clsMortgageHolders.OpenRecordset("select * from mortgageholders where ID = " + clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"), "CentralData");
							if (!clsMortgageHolders.EndOfFile())
							{
								OutP.Name = Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("name1")).Replace("\r\n", ""));
								OutP.Address1 = Strings.Trim("C/O " + FCConvert.ToString(clsMortgageHolders.Get_Fields_String("name")).Replace("\r\n", ""));
								OutP.Address2 = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1")).Replace("\r\n", ""));
								OutP.Address3 = "";
								OutP.City = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("City")).Replace("\r\n", ""));
								OutP.State = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("state")));
								OutP.Zip = Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("Zip")));
								OutP.MortgageHolderID = FCConvert.ToString(Conversion.Val(clsMortgageHolders.Get_Fields_Int32("mortgageholderid")));
								strRecord = FCConvert.ToString(Convert.ToChar(34)) + OutP.BillType + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Account + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Location.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MapLot + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Name.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address1.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address2.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.City.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.State + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Zip + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Land + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Building + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Total + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode6 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode7 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPCode8 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PPcode9 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalPP + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.NetAssessment + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TotalTax + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.TaxDue4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.BookPage + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HomesteadExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.OtherExempt + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Distribution5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent1 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent2 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent3 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent4 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DistPercent5 + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PaidToDate + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.NetREAssessment + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Ref1.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Ref2.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SecondOwner.Replace("\r\n", "") + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Acreage + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MortgageHolderID + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SoftAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MixedAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HardAcres + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.SoftValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.MixedValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.HardValue + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.DiscountAmount + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[0] + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[0] + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[1] + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[1] + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptAmounts[2] + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.REExemptDesc[2] + FCConvert.ToString(Convert.ToChar(34)) + ",";
								strRecord += FCConvert.ToString(Convert.ToChar(34)) + "0" + FCConvert.ToString(Convert.ToChar(34));
								if (boolOutput3rdAddressLine)
								{
									strRecord += ",";
									strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.Address3 + FCConvert.ToString(Convert.ToChar(34));
								}
								if (boolOutputPastDue)
								{
									strRecord += ",";
									strRecord += FCConvert.ToString(Convert.ToChar(34)) + OutP.PastDue + FCConvert.ToString(Convert.ToChar(34));
								}
								ts.WriteLine(strRecord);
								lngMHCopies += 1;
								dblTotalCopiesRENetOwed += FCConvert.ToDouble(OutP.TotalTax);
							}
							clsMortgageAssociations.MoveNext();
						}
					}
					clsBill.MoveNext();
				}
				ts.Close();
				//FC:FINAL:MSH - issue #1866: send the file from the server to the client side
				using(var stream = new FileStream(Path.Combine(FCFileSystem.Statics.UserDataFolder, resultFile), FileMode.Open))
				{
					FCUtils.Download(stream, resultFile);
				}
				boolFileOpen = false;
				BuildComma = true;
				frmWait.InstancePtr.Unload();
				// frmShowResults.Init (RichTextBox1.Text)
				return BuildComma;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				if (boolFileOpen)
					ts.Close();
				boolFileOpen = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In BuildComma", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildComma;
		}

		private void optModule_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 1)
			{
				chkMHCopy.Enabled = false;
				chkNewOwnerCopy.Enabled = false;
			}
			else
			{
				chkMHCopy.Enabled = true;
				chkNewOwnerCopy.Enabled = true;
			}
		}

		private void optModule_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbModule.SelectedIndex);
			optModule_CheckedChanged(index, sender, e);
		}

		private bool BuildFixedEnhanced(ref int lngYear, ref short intModule, ref bool boolCombined)
		{
			bool BuildFixedEnhanced = false;
			// saves the original fixed length format
			modTypes.OutPrintingEnhancedFormat OutP = new modTypes.OutPrintingEnhancedFormat(0);
			// user type that contains fixed length format
			clsDRWrapper clsBill = new clsDRWrapper();
			// vbPorter upgrade warning: strBillType As string	OnRead(FixedString)
			string strBillType = "";
			// vbPorter upgrade warning: strTemp As string	OnRead(FixedString)
			string strTemp = "";
			clsDRWrapper clsAggregate = new clsDRWrapper();
			string strTbl1 = "";
			string strTbl2 = "";
			string strSQL = "";
			string strQry1 = "";
			string strQry2 = "";
			string strRE = "";
			bool boolREFile = false;
			clsDRWrapper clsDist = new clsDRWrapper();
			// holds the distribution information
			double[] DistPerc = new double[5 + 1];
			int x;
			double dblTax = 0;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			long lngTemp = 0;
			// temp variable for adding sor a total
			int lngRecNum;
			// vbPorter upgrade warning: lngTestExempt As int	OnWrite(short, double)
			long lngTestExempt;
			double dblLeftOver = 0;
			double dblTemp = 0;
			string[] strAry = null;
			// vbPorter upgrade warning: lngNetAssess As int	OnWrite(int, double)
			long lngNetAssess = 0;
			string strOldName = "";
			string strSecOwner = "";
			string strCity = "";
			string strState = "";
			string strZip = "";
			string strAddr1 = "";
			string strAddr2 = "";
			clsDRWrapper clsMortgageAssociations = new clsDRWrapper();
			clsDRWrapper clsMortgageHolders = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				ResumeCode:
				;
				BuildFixedEnhanced = false;
				lngTestExempt = 0;
				iLabel1:
				lngRecNum = 1;
				// get the distribution information first
				iLabel2:
				clsDist.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				for (x = 1; x <= 5; x++)
				{
					iLabel3:
					DistPerc[x] = 0;
				}
				// x
				if (clsDist.EndOfFile())
				{
					// 4        MsgBox "The distribution information must be filled out before creating outprint files.", vbExclamation, "Distribution Missing"
					frmWait.InstancePtr.Unload();
					iLabel5:
					frmDistribution.InstancePtr.Init();
					frmWait.InstancePtr.Init("Please wait while the Out-Print file is created.");
					iLabel6:
					clsDist.OpenRecordset("select * from distributiontable order by distributionnumber", "twbl0000.vb1");
				}
				while (!clsDist.EndOfFile())
				{
					iLabel7:
					DistPerc[clsDist.Get_Fields_Int16("distributionnumber")] = clsDist.Get_Fields_Double("distributionpercent");
					iLabel8:
					clsDist.MoveNext();
				}
				FCFileSystem.FileClose();
				if (intModule == 0)
				{
					// real estate
					iLabel9:
					boolREFile = true;
					if (FCFileSystem.FileExists("tsrebill.fil"))
                    {
                        //FC:FINAL:MSH - use FCFileSystem instead of File to avoid problems with wrong paths (same with issue #1604)
                        //File.Delete("tsrebill.fil");
                        FCFileSystem.DeleteFile("tsrebill.fil");
                    }
                    iLabel10:
					FCFileSystem.FileOpen(42, "TSREBill.fil", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(OutP));
					// use the twre link table so that we can join with the association table
					strRE = "select * from master innerjoin [";
					if (boolCombined)
					{
						iLabel11:
						strTbl1 = "(select account as ac,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,PPASSESSMENT,principalpaid,0 as exemptvalue ,bookpage  from billingmaster where billingtype = 'RE' AND ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						strQry2 = "select ac,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid,exemptvalue from ";
						iLabel12:
						strQry1 = strQry2 + strTbl1;
						strTbl2 = "(select moduleassociation.remasteracct as ac, taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid, exemptvalue ,bookpage  from billingmaster inner join " + clsBill.CurrentPrefix + "CentralData.dbo.moduleassociation on (billingmaster.account = moduleassociation.account) where moduleassociation.module = 'PP' and moduleassociation.primaryassociate = -1 and billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						iLabel13:
						strQry2 += strTbl2;
						iLabel14:
						strSQL = "SELECT ac, sum(taxdue1) AS tax1, sum( taxdue2) AS tax2,sum(taxdue3) as tax3,sum(taxdue4) as tax4 ,sum(landvalue) as landsum,sum(buildingvalue) as buildingsum,sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9, sum(homesteadexemption) as homestead,sum(otherexempt1) as otherexemptsum,sum(ppassessment) as PPValue,sum(principalpaid) as prepaid,sum(exemptvalue) as ppexempt from [" + strQry1 + " union " + strQry2 + "]. AS TblCombined group by ac";
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from BILLINGmaster inner join (" + strSQL + ") as billfixedformat on (billfixedformat.ac = billingmaster.account) where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + "  order by billingmaster.name1";
						}
						else
						{
							strSQL = "select * from BILLINGmaster inner join (" + strSQL + ") as billfixedformat on (billfixedformat.ac = billingmaster.account) where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and (tax1 > 0 or tax2 > 0 or tax3 > 0 or tax4 > 0) order by billingmaster.name1";
						}
					}
					else
					{
						// not combined
						strTbl1 = "(select account as ac,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,PPASSESSMENT,principalpaid,exemptvalue,BOOKPAGE  from billingmaster where billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + ")";
						strQry2 = "select ac,taxdue1,taxdue2,taxdue3,taxdue4,landvalue,buildingvalue,category1,category2,category3,category4,category5,category6,category7,category8,category9,homesteadexemption,otherexempt1,ppassessment,principalpaid,exemptvalue,bookpage from ";
						strQry1 = strQry2 + strTbl1;
						strSQL = "SELECT ac, sum(taxdue1) AS tax1, sum( taxdue2) AS tax2,sum(taxdue3) as tax3,sum(taxdue4) as tax4 ,sum(landvalue) as landsum,sum(buildingvalue) as buildingsum,sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9, sum(homesteadexemption) as homestead,sum(otherexempt1) as otherexemptsum,sum(ppassessment) as PPValue,sum(principalpaid) as prepaid,sum(exemptvalue) as ppexempt from [" + strQry1 + "]. AS TblCombined group by ac";
						// This statement will join the re master table with the bill records
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from billingmaster inner join (" + strSQL + ") as billfixedformat  on (billingmaster.account = billfixedformat.ac)   where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + "  ORDER by billingmaster.name1";
						}
						else
						{
							strSQL = "select * from billingmaster inner join (" + strSQL + ") as billfixedformat  on (billingmaster.account = billfixedformat.ac)   where billingmaster.billingtype = 'RE' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + " and (tax1 > 0 or tax2 > 0 or tax3 > 0 or tax4 > 0) ORDER by billingmaster.name1";
						}
					}
					clsBill.OpenRecordset(strSQL, "twcl0000.vb1");
					if (clsBill.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						//Application.DoEvents();
						MessageBox.Show("No Real Estate Bills found", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return BuildFixedEnhanced;
					}
					strBillType = "RE";
				}
				else
				{
					// personal property
					boolREFile = false;
                    //FC:FINAL:MSH - use FCFileSystem instead of File to avoid problems with wrong paths (same with issue #1604)
                    //if (File.Exists("tsppbill.fil"))
                    //	File.Delete("tsppbill.fil");
                    if (FCFileSystem.FileExists("tsppbill.fil"))
                        FCFileSystem.DeleteFile("tsppbill.fil");
                    FCFileSystem.FileOpen(42, "TSPPBill.fil", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(OutP));
					strBillType = "PP";
					if (boolCombined)
					{
						strTbl1 = "select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as ppexempt from billingmaster left join [(select * from " + clsBill.CurrentPrefix + "CentralData.dbo.moduleassociation where module = 'PP' and primaryassociate = -1)]. as Tbl1 on (billingmaster.account = tbl1.account) where  isnull(moduleassociation.primaryassociate) and  billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + "";
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from ppmaster inner join (" + strSQL + ") as billfixedformat on (ppmaster.account = billfixedformat.ac) ";
						}
						else
						{
							strSQL = "select * from ppmaster inner join (" + strSQL + ") as billfixedformat on (ppmaster.account = billfixedformat.ac) where (tax1 > 0 or tax2 > 0 or tax3 > 0 or tax4 > 0)";
						}
					}
					else
					{
						// not combined
						strTbl1 = "select billingmaster.account as ac, taxdue1 as tax1,taxdue2 as tax2,taxdue3 as tax3,taxdue4 as tax4,category1 as cat1,category2 as cat2,category3 as cat3,category4 as cat4,category5 as cat5,category6 as cat6,category7 as cat7,category8 as cat8,category9 as cat9,ppassessment as ppvalue,principalpaid as prepaid,exemptvalue  as ppexempt from billingmaster  where billingmaster.billingtype = 'PP' and ratekey = " + FCConvert.ToString(lngRateRecToPrint) + " and billingmaster.billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9) + "";
						if (!(chkPrint.CheckState == Wisej.Web.CheckState.Checked))
						{
							strSQL = "select * from ppmaster inner join (" + strSQL + ") as billfixedformat on (ppmaster.account = billfixedformat.ac) ";
						}
						else
						{
							strSQL = "select * from ppmaster inner join (" + strSQL + ") as billfixedformat on (ppmaster.account = billfixedformat.ac) where (tax1 > 0 or tax2 > 0 or tax3 > 0 or tax4 > 0)";
						}
					}
					clsBill.OpenRecordset(strSQL, "twpp0000.vb1");
					if (clsBill.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						//Application.DoEvents();
						MessageBox.Show("No Personal Property Bills Found", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return BuildFixedEnhanced;
					}
				}
				while (!clsBill.EndOfFile())
				{
					OutP.LineEnd = "\r\n";
					OutP.MortgageHolderID = "000000";
					lngNetAssess = 0;
					if (boolREFile)
					{
						iLabel40:
						OutP.BillType = strBillType;
						// This may change later if it is a combined
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						OutP.Account = Strings.Format(clsBill.Get_Fields("account"), "000000");
						iLabel42:
						strTemp = Strings.StrDup(34, " ");
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("name1"))));
						iLabel44:
						OutP.Name = strTemp;
						strTemp = Strings.Left(Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("name2"))) + Strings.StrDup(34, " "), 34);
						OutP.SecondOwner = strTemp;
						iLabel45:
						strTemp = Strings.StrDup(31, " ");
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						if (Conversion.Val(clsBill.Get_Fields("streetnumber")) > 0)
						{
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							iLabel46:
							fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(clsBill.Get_Fields("streetnumber") + " " + clsBill.Get_Fields_String("apt") + " " + clsBill.Get_Fields_String("streetname")));
						}
						else
						{
							fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("streetname"))));
						}
						iLabel47:
						OutP.Location = strTemp;
						iLabel48:
						strTemp = Strings.StrDup(17, " ");
						iLabel49:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("maplot"))));
						iLabel50:
						OutP.MapLot = strTemp;
						iLabel51:
						strTemp = Strings.StrDup(34, " ");
						iLabel52:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address1"))));
						iLabel53:
						OutP.Address1 = strTemp;
						iLabel54:
						strTemp = Strings.StrDup(34, " ");
						iLabel55:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address2"))));
						iLabel56:
						OutP.Address2 = strTemp;
						// 10/24/2005
						// 57            If Trim(clsBill.Fields("name2")) <> vbNullString Then
						// If Trim(OutP.Address1) = vbNullString Or Trim(OutP.Address2) = vbNullString Then
						// If Trim(OutP.Address1) <> vbNullString Then
						// OutP.Address2 = OutP.Address1
						// End If
						// strtemp = String(34, " ")
						// Mid(strtemp, 1) = Trim(clsBill.Fields("name2"))
						// OutP.Address1 = strtemp
						// OutP.SecondOwner = ""
						// End If
						// End If
						iLabel58:
						strTemp = Strings.StrDup(24, " ");
						if (Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address3"))) != string.Empty)
						{
							// 59                Mid(strtemp, 1) = Trim(clsBill.Fields("rsaddr3"))
							iLabel59:
							strAry = Strings.Split(Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address3"))), " ", -1, CompareConstants.vbTextCompare);
							if (Information.UBound(strAry, 1) > 1)
							{
								OutP.State = Strings.Left(strAry[Information.UBound(strAry, 1) - 1] + "  ", 2);
								for (x = 0; x <= Information.UBound(strAry, 1) - 2; x++)
								{
									strTemp += strAry[x] + " ";
								}
								// x
								strTemp = Strings.Trim(strTemp);
								strTemp = Strings.Left(strTemp + Strings.StrDup(24, " "), 24);
								OutP.City = strTemp;
							}
							else if (Information.UBound(strAry, 1) > 0)
							{
								iLabel60:
								OutP.State = Strings.Left(strAry[Information.UBound(strAry, 1)] + "  ", 2);
								for (x = 0; x <= Information.UBound(strAry, 1) - 1; x++)
								{
									strTemp += strAry[x] + " ";
								}
								// x
								strTemp = Strings.Trim(strTemp);
								strTemp = Strings.Left(strTemp + Strings.StrDup(24, " "), 24);
								OutP.City = strTemp;
							}
							else
							{
								OutP.State = " ";
								fecherFoundation.Strings.MidSet(ref strTemp, 1, strAry[0]);
								OutP.City = strTemp;
							}
						}
						else
						{
							OutP.City = Strings.StrDup(24, " ");
							OutP.State = "  ";
						}
						iLabel64:
						strTemp = Strings.StrDup(5, " ");
						iLabel65:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("zip"))));
						iLabel66:
						OutP.Zip = strTemp;
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						iLabel67:
						OutP.Land = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("landsum"))), 10);
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						iLabel68:
						OutP.Building = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("buildingsum"))), 10);
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						iLabel69:
						OutP.Total = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("buildingsum")) + Conversion.Val(clsBill.Get_Fields("landsum"))), 10);
						iLabel70:
						strTemp = Strings.StrDup(12, " ");
						iLabel71:
						if (Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("bookpage"))) != string.Empty)
						{
							iLabel72:
							strAry = Strings.Split(FCConvert.ToString(clsBill.Get_Fields_String("bookpage")), " ", -1, CompareConstants.vbTextCompare);
							strTemp = Strings.Mid(strAry[0] + Strings.StrDup(12, " "), 1, 12);
						}
						iLabel73:
						OutP.BookPage = strTemp;
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						iLabel74:
						OutP.HomesteadExempt = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("homestead"))), 10);
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						iLabel75:
						OutP.OtherExempt = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("otherexemptsum"))), 10);
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						lngTestExempt += Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("homestead")));
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						lngTestExempt += Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("otherexemptsum")));
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [buildingsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [homestead] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [otherexemptsum] not found!! (maybe it is an alias?)
						lngTemp = Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("landsum")) + Conversion.Val(clsBill.Get_Fields("buildingsum")) - Conversion.Val(clsBill.Get_Fields("homestead")) - Conversion.Val(clsBill.Get_Fields("otherexemptsum")));
						lngNetAssess += lngTemp;
						dblTotalREAssess += lngTemp;
						OutP.RENet = modMain.NumtoString_6(lngTemp, 11);
						OutP.Ref1 = Strings.Left(Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("ref1"))) + Strings.StrDup(36, " "), 36);
						OutP.Ref2 = Strings.Left(Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("ref2"))) + Strings.StrDup(36, " "), 36);
						dblTemp = Conversion.Val(clsBill.Get_Fields_Double("acres"));
						OutP.Acreage = modMain.DblToString_6(dblTemp, 10, 2);
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						dblTotalOriginalRETax += Conversion.Val(clsBill.Get_Fields("tax1")) + Conversion.Val(clsBill.Get_Fields("tax2")) + Conversion.Val(clsBill.Get_Fields("tax3")) + Conversion.Val(clsBill.Get_Fields("tax4"));
						if (boolCombined)
						{
							// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
							if (Conversion.Val(clsBill.Get_Fields("ppexempt")) < Conversion.Val(clsBill.Get_Fields("ppvalue")))
							{
								// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
								dblTotalREAssess += Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt"));
							}
						}
					}
					else
					{
						// personal property
						iLabel76:
						OutP.BillType = strBillType;
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						iLabel77:
						OutP.Account = Strings.Format(clsBill.Get_Fields("account"), "000000");
						iLabel78:
						strTemp = Strings.StrDup(34, " ");
						iLabel79:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("name"))));
						iLabel80:
						OutP.Name = strTemp;
						iLabel81:
						strTemp = Strings.StrDup(31, " ");
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						iLabel82:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(clsBill.Get_Fields("streetnumber") + " " + clsBill.Get_Fields_String("street")));
						iLabel83:
						OutP.Location = strTemp;
						iLabel85:
						strTemp = Strings.StrDup(17, " ");
						iLabel86:
						OutP.MapLot = strTemp;
						iLabel88:
						strTemp = Strings.StrDup(34, " ");
						iLabel89:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("ADDRESS1"))));
						iLabel90:
						OutP.Address1 = strTemp;
						iLabel92:
						strTemp = Strings.StrDup(34, " ");
						iLabel93:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("address2"))));
						iLabel94:
						OutP.Address2 = strTemp;
						iLabel95:
						strTemp = Strings.StrDup(24, " ");
						iLabel96:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("city"))));
						iLabel97:
						OutP.City = strTemp;
						iLabel98:
						strTemp = "  ";
						iLabel99:
						fecherFoundation.Strings.MidSet(ref strTemp, 1, Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("state"))));
						iLabel100:
						OutP.State = strTemp;
						iLabel101:
						strTemp = Strings.StrDup(5, " ");
						strTemp = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(clsBill.Get_Fields_String("zip"))), 5);
						// 102            Mid(strTemp, 1) = Trim(clsBill.Fields("zip"))
						iLabel103:
						OutP.Zip = strTemp;
						iLabel104:
						OutP.Land = Strings.StrDup(10, " ");
						iLabel105:
						OutP.Building = Strings.StrDup(10, " ");
						iLabel106:
						OutP.Total = Strings.StrDup(10, " ");
						iLabel107:
						OutP.BookPage = Strings.StrDup(12, " ");
						iLabel108:
						OutP.HomesteadExempt = Strings.StrDup(10, " ");
						iLabel109:
						OutP.OtherExempt = Strings.StrDup(10, " ");
						OutP.RENet = Strings.StrDup(11, " ");
						OutP.Ref1 = Strings.StrDup(36, " ");
						OutP.Ref2 = Strings.StrDup(36, " ");
						OutP.SecondOwner = Strings.StrDup(34, " ");
						OutP.Acreage = Strings.StrDup(10, " ");
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						dblTotalOriginalPPTax += Conversion.Val(clsBill.Get_Fields("tax1")) + Conversion.Val(clsBill.Get_Fields("tax2")) + Conversion.Val(clsBill.Get_Fields("tax3")) + Conversion.Val(clsBill.Get_Fields("tax4"));
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsBill.Get_Fields("ppexempt")) < Conversion.Val(clsBill.Get_Fields("ppvalue")))
						{
							// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
							dblTotalPPAssess += Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt"));
						}
					}
					// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
					iLabel110:
					OutP.TaxDue1 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax1")), 14);
					// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
					iLabel111:
					OutP.TaxDue2 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax2")), 14);
					// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
					iLabel112:
					OutP.TaxDue3 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax3")), 14);
					// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
					iLabel113:
					OutP.TaxDue4 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax4")), 14);
					iLabel114:
					dblTax = FCConvert.ToDouble(OutP.TaxDue1) + FCConvert.ToDouble(OutP.TaxDue2) + FCConvert.ToDouble(OutP.TaxDue3) + FCConvert.ToDouble(OutP.TaxDue4);
					iLabel115:
					OutP.DistPercent1 = modMain.DblToString_6(DistPerc[1], 6, 3);
					iLabel116:
					OutP.DistPercent2 = modMain.DblToString_6(DistPerc[2], 6, 3);
					iLabel117:
					OutP.DistPercent3 = modMain.DblToString_6(DistPerc[3], 6, 3);
					iLabel118:
					OutP.DistPercent4 = modMain.DblToString_6(DistPerc[4], 6, 3);
					iLabel119:
					OutP.DistPercent5 = modMain.DblToString_6(DistPerc[5], 6, 3);
					iLabel120:
					OutP.Distribution1 = modMain.DblToString_8(DistPerc[1] / 100 * dblTax, 14);
					iLabel121:
					OutP.Distribution2 = modMain.DblToString_8(DistPerc[2] / 100 * dblTax, 14);
					iLabel122:
					OutP.Distribution3 = modMain.DblToString_8(DistPerc[3] / 100 * dblTax, 14);
					iLabel123:
					OutP.Distribution4 = modMain.DblToString_8(DistPerc[4] / 100 * dblTax, 14);
					iLabel124:
					OutP.Distribution5 = modMain.DblToString_8(DistPerc[5] / 100 * dblTax, 14);
					// 120        OutP.Distribution1 = DblToString(DistPerc(1) * dblTax, 10)
					// 121        OutP.Distribution2 = DblToString(DistPerc(2) * dblTax, 10)
					// 122        OutP.Distribution3 = DblToString(DistPerc(3) * dblTax, 10)
					// 123        OutP.Distribution4 = DblToString(DistPerc(4) * dblTax, 10)
					// 124        OutP.Distribution5 = DblToString(DistPerc(5) * dblTax, 10)
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					iLabel125:
					OutP.PaidToDate = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("prepaid")), 14);
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					dblLeftOver = Conversion.Val(clsBill.Get_Fields("prepaid"));
					if (boolREFile)
					{
						dblTotalREPrePayments += dblLeftOver;
					}
					else
					{
						dblTotalPPPrePayments += dblLeftOver;
					}
					// TODO Get_Fields: Field [prepaid] not found!! (maybe it is an alias?)
					iLabel126:
					dblTax -= Conversion.Val(clsBill.Get_Fields("prepaid"));
					if (boolREFile)
					{
						dblTotalRENetTaxOwed += dblTax;
					}
					else
					{
						dblTotalPPNetTaxOwed += dblTax;
					}
					iLabel127:
					OutP.TotalTax = modMain.DblToString_6(dblTax, 14);
					if (dblLeftOver > 0)
					{
						// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax1")))
						{
							OutP.TaxDue1 = modMain.DblToString_8(0, 14);
							// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax1"));
						}
						else
						{
							// TODO Get_Fields: Field [tax1] not found!! (maybe it is an alias?)
							OutP.TaxDue1 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax1")) - dblLeftOver, 14);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax2")))
						{
							OutP.TaxDue2 = modMain.DblToString_8(0, 14);
							// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax2"));
						}
						else
						{
							// TODO Get_Fields: Field [tax2] not found!! (maybe it is an alias?)
							OutP.TaxDue2 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax2")) - dblLeftOver, 14);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax3")))
						{
							OutP.TaxDue3 = modMain.DblToString_8(0, 14);
							// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax3"));
						}
						else
						{
							// TODO Get_Fields: Field [tax3] not found!! (maybe it is an alias?)
							OutP.TaxDue3 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax3")) - dblLeftOver, 14);
							dblLeftOver = 0;
						}
						// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
						if (dblLeftOver > Conversion.Val(clsBill.Get_Fields("tax4")))
						{
							OutP.TaxDue4 = modMain.DblToString_8(0, 14);
							// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
							dblLeftOver -= Conversion.Val(clsBill.Get_Fields("tax4"));
						}
						else
						{
							// TODO Get_Fields: Field [tax4] not found!! (maybe it is an alias?)
							OutP.TaxDue4 = modMain.DblToString_8(Conversion.Val(clsBill.Get_Fields("tax4")) - dblLeftOver, 14);
							dblLeftOver = 0;
						}
					}
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					iLabel128:
					if (boolREFile && (Conversion.Val(clsBill.Get_Fields("ppvalue")) > 0) && boolCombined)
					{
						// combined bill
						// shouldn't need to check boolcombined since ppvalue shouldn't have a value in an re bill file
						iLabel129:
						OutP.BillType = "CM";
					}
					// Personal property stuff
					// TODO Get_Fields: Field [cat1] not found!! (maybe it is an alias?)
					iLabel130:
					OutP.PPCode1 = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat1"))), 10);
					// TODO Get_Fields: Field [cat2] not found!! (maybe it is an alias?)
					iLabel131:
					OutP.PPCode2 = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat2"))), 10);
					// TODO Get_Fields: Field [cat3] not found!! (maybe it is an alias?)
					iLabel132:
					OutP.PPCode3 = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat3"))), 10);
					// TODO Get_Fields: Field [cat4] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat5] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat6] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat7] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat8] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [cat9] not found!! (maybe it is an alias?)
					iLabel133:
					lngTemp = Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat4")) + Conversion.Val(clsBill.Get_Fields("cat5")) + Conversion.Val(clsBill.Get_Fields("cat6")) + Conversion.Val(clsBill.Get_Fields("cat7")) + Conversion.Val(clsBill.Get_Fields("cat8")) + Conversion.Val(clsBill.Get_Fields("cat9")));
					// TODO Get_Fields: Field [cat3] not found!! (maybe it is an alias?)
					iLabel134:
					OutP.PPCode4 = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("cat3")) + lngTemp), 10);
					iLabel135:
					OutP.PPCode5 = modMain.NumtoString_6(lngTemp, 10);
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					iLabel136:
					OutP.TotalPP = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("ppvalue"))), 10);
					// TODO Get_Fields: Check the table for the column [ACCOUNT] and replace with corresponding Get_Field method
					if (clsBill.Get_Fields_Int32("ACCOUNT") == 18)
					{
						OutP.TotalPP = OutP.TotalPP;
					}
					// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
					iLabel137:
					if (Conversion.Val(clsBill.Get_Fields("ppexempt")) > Conversion.Val(clsBill.Get_Fields("ppvalue")))
					{
						iLabel138:
						OutP.TotalExempt = OutP.TotalPP;
						iLabel139:
						OutP.NetAssessment = "0";
					}
					else
					{
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						iLabel140:
						OutP.TotalExempt = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("ppexempt"))), 10);
						// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						iLabel141:
						OutP.NetAssessment = modMain.NumtoString_8(Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt"))), 10);
						// TODO Get_Fields: Field [ppvalue] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [ppexempt] not found!! (maybe it is an alias?)
						lngNetAssess += Convert.ToInt64(Conversion.Val(clsBill.Get_Fields("ppvalue")) - Conversion.Val(clsBill.Get_Fields("ppexempt")));
					}
					// temp fix for holden
					// OutP.NetAssessment = NumtoString(lngNetAssess, 10)
					// now write the file
					WriteAnother:
					;
					iLabel142:
					FCFileSystem.FilePut(42, OutP, -1/*? lngRecNum */);
					iLabel143:
					lngRecNum += 1;
					if (chkNewOwnerCopy.CheckState == Wisej.Web.CheckState.Checked && OutP.BillType != "PP")
					{
						string temp = "";
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (modMain.HasNewOwner_8(clsBill.Get_Fields("Account"), clsBill.Get_Fields_String("name1"), ref strOldName, ref strSecOwner, ref strAddr1, ref strAddr2, ref strTemp, ref strCity, ref strState, ref strZip, ref temp, ref temp))
						{
							OutP.Name = Strings.Left(strOldName + Strings.StrDup(34, " "), 34);
							OutP.SecondOwner = Strings.Left(strSecOwner + Strings.StrDup(34, " "), 34);
							OutP.Address1 = Strings.Left(strAddr1 + Strings.StrDup(34, " "), 34);
							OutP.Address2 = Strings.Left(strAddr2 + Strings.StrDup(34, " "), 34);
							OutP.City = Strings.Left(strCity + Strings.StrDup(24, " "), 24);
							OutP.State = Strings.Left(strState + "  ", 2);
							OutP.Zip = Strings.Left(strZip + "     ", 5);
							FCFileSystem.FilePut(42, OutP, -1/*? lngRecNum */);
							lngRecNum += 1;
							lngNumNewNames += 1;
							dblTotalCopiesRENetOwed += FCConvert.ToDouble(OutP.TotalTax);
						}
					}
					if (chkMHCopy.CheckState == Wisej.Web.CheckState.Checked && OutP.BillType != "PP")
					{
						clsMortgageAssociations.OpenRecordset("select * from mortgageassociation where REceivebill = 1 and module = 'RE' and account = " + OutP.Account, "CentralData");
						while (!clsMortgageAssociations.EndOfFile())
						{
							clsMortgageHolders.OpenRecordset("select * from mortgageholders where ID = " + clsMortgageAssociations.Get_Fields_Int32("mortgageholderid"), "CentralData");
							if (!clsMortgageHolders.EndOfFile())
							{
								OutP.Name = Strings.Left(clsBill.Get_Fields_String("name1") + Strings.StrDup(34, " "), 34);
								OutP.SecondOwner = Strings.Left(clsBill.Get_Fields_String("name2") + Strings.StrDup(34, " "), 34);
								OutP.Address1 = Strings.Left("C/O " + clsMortgageHolders.Get_Fields_String("name") + Strings.StrDup(34, " "), 34);
								OutP.Address2 = Strings.Left(Strings.Trim(FCConvert.ToString(clsMortgageHolders.Get_Fields_String("address1"))) + Strings.StrDup(34, " "), 34);
								OutP.City = Strings.Left(clsMortgageHolders.Get_Fields_String("City") + Strings.StrDup(24, " "), 24);
								OutP.State = Strings.Left(clsMortgageHolders.Get_Fields_String("state") + "  ", 2);
								OutP.Zip = Strings.Left(clsMortgageHolders.Get_Fields_String("Zip") + "     ", 5);
								OutP.MortgageHolderID = Strings.Format(Conversion.Val(clsMortgageHolders.Get_Fields_Int32("mortgageholderid")), "000000");
								FCFileSystem.FilePut(42, OutP, -1/*? lngRecNum */);
								lngRecNum += 1;
								lngMHCopies += 1;
								dblTotalCopiesRENetOwed += FCConvert.ToDouble(OutP.TotalTax);
							}
							clsMortgageAssociations.MoveNext();
						}
					}
					iLabel144:
					clsBill.MoveNext();
				}
				FCFileSystem.FileClose();
				// MsgBox "Exempt total = " & Format(lngTestExempt, "###,###,###,##0")
				BuildFixedEnhanced = true;
				return BuildFixedEnhanced;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCFileSystem.FileClose();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In BuildFixedEnhanced", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildFixedEnhanced;
		}

		private void T2KDateBox1_Click()
		{
		}

		private void Text1_Change()
		{
		}

		private void cmbModule_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbModule.SelectedIndex == 0)
			{
				optModule_CheckedChanged(sender, e);
			}
			else if (cmbModule.SelectedIndex == 1)
			{
				optModule_CheckedChanged(sender, e);
			}
			else if (cmbModule.SelectedIndex == 2)
			{
				optModule_CheckedChanged(sender, e);
			}
			else if (cmbModule.SelectedIndex == 3)
			{
				optModule_CheckedChanged(sender, e);
			}
		}

		private void cmbFormat_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFormat.SelectedIndex == 0)
			{
				optFormat_CheckedChanged(sender, e);
			}
			else if (cmbFormat.SelectedIndex == 1)
			{
				optFormat_CheckedChanged(sender, e);
			}
		}
	}
}
