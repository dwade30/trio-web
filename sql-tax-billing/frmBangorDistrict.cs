﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for frmBangorDistrict.
	/// </summary>
	public partial class frmBangorDistrict : BaseForm
	{
		public frmBangorDistrict()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBangorDistrict InstancePtr
		{
			get
			{
				return (frmBangorDistrict)Sys.GetInstance(typeof(frmBangorDistrict));
			}
		}

		protected frmBangorDistrict _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		cBangorValuations bv;

		private void FillcmbDistrict()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from tbltrancode ORDER by code", "RealEstate");
			cmbDistrict.Clear();
			while (!rsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				cmbDistrict.AddItem(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("code"))) + " - " + rsLoad.Get_Fields_String("description"));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				cmbDistrict.ItemData(cmbDistrict.NewIndex, FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("code"))));
				rsLoad.MoveNext();
			}
		}

		private void CopyValues()
		{
			if (cmbDistrict.SelectedIndex >= 0)
			{
				bv.CopyDistrict(cmbDistrict.ItemData(cmbDistrict.SelectedIndex));
                MessageBox.Show("Values copied", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("You must select a district to copy values for", "No District", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void ClearValues()
		{
			if (cmbDistrict.SelectedIndex >= 0)
			{
				bv.ClearDistrict(cmbDistrict.ItemData(cmbDistrict.SelectedIndex));
                MessageBox.Show("Values cleared", "",MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("You must select a district to clear values for", "No District", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void bv_ActionComplete(string strMessage)
		{
			MessageBox.Show(strMessage, "Billing", MessageBoxButtons.OK, MessageBoxIcon.Information);
			lblMessage.Text = "";
			lblMessage.Refresh();
			//Application.DoEvents();
		}

		private void bv_LabelUpdate(string strMessage)
		{
			lblMessage.Text = strMessage;
			lblMessage.Refresh();
			//Application.DoEvents();
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			ClearValues();
		}

		private void cmdCopy_Click(object sender, System.EventArgs e)
		{
			CopyValues();
		}

		private void frmBangorDistrict_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBangorDistrict_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBangorDistrict properties;
			//frmBangorDistrict.FillStyle	= 0;
			//frmBangorDistrict.ScaleWidth	= 5880;
			//frmBangorDistrict.ScaleHeight	= 4065;
			//frmBangorDistrict.LinkTopic	= "Form2";
			//frmBangorDistrict.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			FillcmbDistrict();
			bv = new cBangorValuations();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void cmdSaveAndContinue_Click(object sender, EventArgs e)
		{
		}
	}
}
