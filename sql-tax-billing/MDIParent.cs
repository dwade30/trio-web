﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWBL0000
{
	public class MDIParent
	//: BaseForm
	{
        public FCCommonDialog CommonDialog1;
        public MDIParent()
		{
			//
			// Required for Windows Form Designer support
			//
			//FC:FINAL:CHN - issue #1287: Not initialized MIDParent fields.
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.CommonDialog1 = new FCCommonDialog();
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// Private clsCmdMenu16 As New clsAlphaTextBox
		/// </summary>
		/// <summary>
		/// Private clsCmdMenu17 As New clsAlphaTextBox
		/// </summary>
		/// <summary>
		/// Private clsCmdMenu18 As New clsAlphaTextBox
		/// </summary>
		/// <summary>
		/// Private clsCmdMenu19 As New clsAlphaTextBox
		/// </summary>
		private int intExit;
		//private void SetMenuOptions(string strMenu)
		//{
		//    //GRID.ColData(1, string.Empty);
		//    // clears the whole col
		//    //CloseChildForms();
		//    //GRID.Tag = (System.Object)(strMenu);
		//    if (Strings.UCase(strMenu) == "MAIN")
		//    {
		//        MainCaptions();
		//    }
		//    else if (Strings.UCase(strMenu) == "FILE")
		//    {
		//        FileMaintCaptions();
		//    }
		//    else if (Strings.UCase(strMenu) == "BILL")
		//    {
		//        BillCaptions();
		//    }
		//    else if (Strings.UCase(strMenu) == "PRINTING")
		//    {
		//        PrintingCaptions();
		//        // Case "REALESTATE"
		//        // Call RealEstateCaptions
		//        // Case "PERSONALPROPERTY"
		//        // Call PersonalPropertyCaptions
		//    }
		//    else if (Strings.UCase(strMenu) == "MORTGAGE")
		//    {
		//        MortgageCaptions();
		//    }
		//    else if (Strings.UCase(strMenu) == "OUTPRINTING")
		//    {
		//        OutPrintingCaptions();
		//    }
		//    else
		//    {
		//    }
		//}
		public void CloseChildForms()
		{
			//FC:FINAL:DSE #925 Cannot change a collection inside a foreach statement
			//foreach (Form frm in Application.OpenForms)
			//{
			//    if (frm.Name != "MDIParent")
			//    {
			//        frm.Close();
			//    }
			//}
			FCUtils.CloseFormsOnProject();
		}
		// vbPorter upgrade warning: intMenuChoice As short	OnWriteFCConvert.ToInt32(
		private void MenuClick(int intMenuChoice)
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
				MainActions(intMenuChoice);
			}
			else if (vbPorterVar == "BILL")
			{
				BillActions(intMenuChoice);
			}
			else if (vbPorterVar == "FILE")
			{
				FileMaintActions(intMenuChoice);
			}
			else if (vbPorterVar == "MORTGAGE")
			{
				MortgageActions(intMenuChoice);
				// Case "REALESTATE"
				// Call RealEstateActions(intMenuChoice)
				// Case "PERSONALPROPERTY"
				// Call PersonalPropertyActions(intMenuChoice)
			}
			else if (vbPorterVar == "PRINTING")
			{
				PrintingActions(intMenuChoice);
			}
			else if (vbPorterVar == "OUTPRINTING")
			{
				OutprintingActions(intMenuChoice);
			}
			else if (vbPorterVar == "CUSTOM")
			{
				mnuEditDistrictValues_Click(null, null);
			}
			else
			{
			}
		}

		//private void MDIParent_Activated(object sender, System.EventArgs e)
		//{
		//	this.StatusBar1.Panels[0].Width = GRID.Width;
		//	//modGlobalFunctions.SetHelpMenuOptions();
		//	AutoSizeMenu_2(13);
		//	if (!(this.WindowState == FormWindowState.Minimized))
		//	{
		//		//Picture1.Width = this.Width - GRID.Width - 135;
		//		//modArchiveImage.ShowArchiveImage(ref modGlobalConstants.Statics.gstrArchiveYear);
		//		picArchive.Width = Picture1.Width;
		//		picArchive.Height = Picture1.Height;
		//		// Image1.Picture = ImageList1.ListImages(4).Picture
		//		// If Picture1.Width > Picture1.Height Then
		//		// Image1.Width = Picture1.Height
		//		// Image1.Height = Picture1.Height
		//		// Else
		//		// Image1.Width = Picture1.Width
		//		// Image1.Height = Picture1.Height
		//		// End If
		//		// If Image1.Width < Picture1.Width Then
		//		// Image1.Left = ((Picture1.Width - Image1.Width) / 2)
		//		// End If
		//	}
		//}
		//private void MDIParent_Load(object sender, System.EventArgs e)
		public void Init()
		{
			//Begin Unmaped Properties
			//MDIParent properties;
			//MDIParent.LinkMode	= 1  'Source;
			//MDIParent.LinkTopic	= "mdiparent";
			//MDIParent.LockControls	= true;
			//End Unmaped Properties
			string strReturn = "";
			int intUserID;
			App.MainForm.NavigationMenu.Owner = this;
			App.MainForm.menuTree.ImageList = CreateImageList();
			modGlobalFunctions.LoadTRIOColors();
			//modGlobalFunctions.SetMenuBackColor();
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, GRID.Rows - 1, GRID.Cols - 1, GRID.BackColor);
			modGlobalConstants.Statics.TRIOCOLORBLUE = Information.RGB(0, 0, 200);
			//this.GRID.ColWidth(0, 200);
			//this.GRID.ColWidth(1, 2400);
			//GRID.Width = 2700;
			//GRID.Select(0, 1);
			//this.StatusBar1.Panels[0].Text = modGlobalConstants.MuniName;
			App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
			if (strReturn == string.Empty)
				strReturn = "False";
			modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
			//mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
			//if (Strings.LCase(modGlobalConstants.Statics.MuniName) == "bangor")
			//{
			//	mnuBangor.Visible = true;
			//}
			//else
			//{
			//	mnuBangor.Visible = false;
			//}
			MainCaptions();
			
				strReturn = "False";
			modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
			//mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
			//SetMenuOptions("MAIN");
		}

		public void PrintingCaptions(FCMenuItem parent)
		{
			int CurRow;
			string strTemp = "";
			int lngFCode = 0;
			bool boolDisable = false;
			string imageKey = "";
			//MDIParent.InstancePtr.Text = "TRIO Software - Tax Billing       [Printing Menu]";
			ClearLabels();
			for (CurRow = 1; CurRow <= 2; CurRow++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (CurRow)
				{
				// Select Case intCounter
					case 1:
						{
							strTemp = "Reports";
							lngFCode = modGlobalVariables.CNSTLISTINGS;
							break;
						}
					case 2:
						{
							strTemp = "Labels";
							lngFCode = modGlobalVariables.CNSTLABELS;
							break;
						}

				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0)
				{
					boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				FCMenuItem newItem = parent.SubItems.Add(strTemp, "MenuClick:" + CurRow, "PRINTING", !boolDisable, 2, imageKey);
			}
			//AutoSizeMenu_2(20);
		}

		private void PrintingActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmListings.InstancePtr.Show(App.MainForm);
						break;
					}
				case 2:
					{
						frmCustomLabels.InstancePtr.Unload();
						//Application.DoEvents();
						frmCustomLabels.InstancePtr.Show(App.MainForm);
						break;
					}

			}
			//end switch
		}

		public void MainActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						// Call SetMenuOptions("REALESTATE")
						if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
						{
							modGlobalVariables.Statics.CustomizedInfo.intCurrentTown = frmPickRegionalTown.InstancePtr.Init(-1, false, "Choose the town to perform the audit for");
							if (modGlobalVariables.Statics.CustomizedInfo.intCurrentTown < 0)
							{
								MessageBox.Show("Cannot proceed without choosing a town", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						frmAuditInfo.InstancePtr.Init(1);
						break;
					}
				case 7:
					{
						if (modRegionalTown.IsRegionalTown())
						{
							modGlobalVariables.Statics.CustomizedInfo.intCurrentTown = 1;
						}
						frmTaxRateCalc.InstancePtr.Show(App.MainForm);
						break;
					}
				case 2:
					{
						// prepayment interest
						modREPPBudStuff.ApplyPrepaymentInterestToBills();
						break;
					}
				case 3:
					{
						// Call SetMenuOptions("PERSONALPROPERTY")
						if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
						{
							modGlobalVariables.Statics.CustomizedInfo.intCurrentTown = frmPickRegionalTown.InstancePtr.Init(-1, false, "Choose the town to transfer billing information for");
							if (modGlobalVariables.Statics.CustomizedInfo.intCurrentTown < 0)
							{
								MessageBox.Show("Cannot proceed without choosing a town", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						frmAuditInfo.InstancePtr.Init(3);
						break;
					}
				case 4:
					{
						clsDRWrapper clsDefault = new clsDRWrapper();
						clsDefault.OpenRecordset("SELECT * FROM defaultstable", "twbl0000.vb1");
						//! Load frmListings;
						if (!clsDefault.EndOfFile())
						{
							frmListings.InstancePtr.txtYear.Text = FCConvert.ToString(clsDefault.Get_Fields_Int16("defaultyear"));
						}
						else
						{
							frmListings.InstancePtr.txtYear.Text = FCConvert.ToString(DateTime.Today.Year);
						}
						frmListings.InstancePtr.cmbReport.SelectedIndex = 0;
						frmListings.InstancePtr.Show(App.MainForm);
						break;
					}
				case 6:
					{
						if (modGlobalVariables.Statics.CustomizedInfo.boolIsRegional)
						{
							modGlobalVariables.Statics.CustomizedInfo.intCurrentTown = frmPickRegionalTown.InstancePtr.Init(-1, false, "Choose the town to create supplemental bills for");
							if (modGlobalVariables.Statics.CustomizedInfo.intCurrentTown < 0)
							{
								MessageBox.Show("Cannot proceed without choosing a town", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						frmSupplementalBill.InstancePtr.Show(App.MainForm);
						break;
					}
			//case 8:
			//    {
			//        SetMenuOptions("PRINTING");
			//        break;
			//    }
			//case 9:
			//    {
			//        SetMenuOptions("MORTGAGE");
			//        break;
			//    }
			//case 10:
			//    {
			//        SetMenuOptions("FILE");
			//        break;
			//    }
			//case 11:
			//    {
			//        Close();
			//        break;
			//    }
			}
			//end switch
		}

		public object MainCaptions()
		{
			object MainCaptions = null;
			int CurRow;
			string strTemp = "";
			int lngFCode = 0;
			bool boolDisable = false;
			string imageKey = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			bool boolTemp = false;
			App.MainForm.Text = "TRIO Software - Tax Billing       [Main]";
			App.MainForm.Caption = "TAX BILLING";
			App.MainForm.ApplicationIcon = "icon-tax-billing";
            App.MainForm.NavigationMenu.OriginName = "Property Tax Billing";
			string assemblyName = this.GetType().Assembly.GetName().Name;
			if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
			{
				App.MainForm.ApplicationIcons.Add(assemblyName, "icon-tax-billing");
			}
			ClearLabels();
			int rowsNumber = Strings.LCase(modGlobalConstants.Statics.MuniName) == "bangor" ? 11 : 10;
			for (CurRow = 1; CurRow <= rowsNumber; CurRow++)
			{
				boolTemp = true;
				lngFCode = 0;
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							imageKey = "audit";
							strTemp = "Audit";
							lngFCode = modGlobalVariables.CNSTAUDIT;
							break;
						}
					case 7:
						{
							imageKey = "tax-rate-calculator";
							strTemp = "Tax Rate Calculator";
							lngFCode = modGlobalVariables.CNSTEDITTAXRATES;
							break;
						}
					case 2:
						{
							imageKey = "apply-pre-payment-interest";
							boolTemp = false;
							boolDisable = true;
							strTemp = "Apply Pre-Payment Interest";
							lngFCode = modGlobalVariables.CNSTXFER;
							clsLoad.OpenRecordset("select * from collections", modGlobalVariables.strCLDatabase);
							if (!clsLoad.EndOfFile())
							{
								if (clsLoad.Get_Fields_Boolean("Payintonprepayment") && Conversion.Val(clsLoad.Get_Fields_Double("OVERPAYINTERESTRATE")) > 0)
								{
									boolTemp = true;
									boolDisable = false;
								}
							}
							break;
						}
					case 3:
						{
							imageKey = "transfer-new-billing-data";
							strTemp = "Transfer New Billing Data";
							lngFCode = modGlobalVariables.CNSTXFER;
							break;
						}
					case 4:
						{
							imageKey = "commitment-book";
							strTemp = "Commitment Book";
							break;
						}
					case 5:
						{
							imageKey = "bills";
							strTemp = "Bills";
							lngFCode = modGlobalVariables.CNSTBILLINGMENU;
							break;
						}
					case 6:
						{
							imageKey = "supplimental-bills";
							strTemp = "Supplemental Bills";
							lngFCode = modGlobalVariables.CNSTSUPPLEMENTAL;
							break;
						}
					case 8:
						{
							imageKey = "printing";
							strTemp = "Printing";
							lngFCode = modGlobalVariables.CNSTPRINTING;
							break;
						}
					case 9:
						{
							imageKey = "account-associations";
							strTemp = "Account Associations";
							lngFCode = modGlobalVariables.CNSTASSOCIATIONS;
							break;
						}
					case 10:
						{
							imageKey = "file-maintenance";
							strTemp = "File Maintenance";
							lngFCode = modGlobalVariables.CNSTFILEMAINT;
							break;
						}
					case 11:
						{
							imageKey = "street-maintenance";
							strTemp = "Custom Programs";
							lngFCode = 0;
							break;
						}
				//case 11:
				//    {
				//        strTemp = "X. Exit Billing";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        lngFCode = 0;
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0)
				{
					boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "MenuClick:" + CurRow, "MAIN", !boolDisable, 1, imageKey);
				switch (CurRow)
				{
					case 5:
						{
							BillCaptions(newItem);
							break;
						}
					case 8:
						{
							PrintingCaptions(newItem);
							break;
						}
					case 9:
						{
							MortgageCaptions(newItem);
							break;
						}
					case 10:
						{
							FileMaintCaptions(newItem);
							break;
						}
					case 11:
						{
							CustomCaptions(newItem);
							break;
						}
				}
			}
			return MainCaptions;
		}

		public void CustomCaptions(FCMenuItem parent)
		{
			int CurRow;
			string strTemp = "";
			//MDIParent.InstancePtr.Text = "TRIO Software - Tax Billing [Custom]";
			ClearLabels();
			for (CurRow = 1; CurRow <= 1; CurRow++)
			{
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "District Valuations";
							break;
						}
				}
				FCMenuItem newItem = parent.SubItems.Add(strTemp, "MenuClick:" + CurRow, "CUSTOM", true, 2);
			}
		}

		public ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-audit", "audit"),
				new ImageListEntry("menutree-tax-rate-calculator", "tax-rate-calculator"),
				new ImageListEntry("menutree-apply-pre-payment-interest", "apply-pre-payment-interest"),
				new ImageListEntry("menutree-transfer-new-billing-data", "transfer-new-billing-data"),
				new ImageListEntry("menutree-commitment-book", "commitment-book"),
				new ImageListEntry("menutree-bills", "bills"),
				new ImageListEntry("menutree-supplimental-bills", "supplimental-bills"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-account-associations", "account-associations"),
				new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
				new ImageListEntry("menutree-street-maintenance", "street-maintenance")
			});
			return imageList;
		}

		public object MortgageCaptions(FCMenuItem parent)
		{
			object MortgageCaptions = null;
			int CurRow;
			string strTemp = "";
			int lngFCode = 0;
			bool boolDisable = false;
			string imageKey = "";
			//MDIParent.InstancePtr.Text = "TRIO Software - Tax Billing       [Account Associations]";
			ClearLabels();
			// Select Case intCounter
			for (CurRow = 1; CurRow <= 2; CurRow++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Mortgage Maintenance";
							lngFCode = modGlobalVariables.CNSTMORTGAGEMAINT;
							break;
						}
					case 2:
						{
							strTemp = "Group Maintenance";
							lngFCode = modGlobalVariables.CNSTGROUPMAINT;
							break;
						}
				//case 3:
				//    {
				//        strTemp = "X. Return to Main";
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0)
				{
					boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				FCMenuItem newItem = parent.SubItems.Add(strTemp, "MenuClick:" + CurRow, "MORTGAGE", !boolDisable, 2, imageKey);
			}
			//AutoSizeMenu_2(20);
			return MortgageCaptions;
		}

		private void MortgageActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmGetMortgage.InstancePtr.Show(App.MainForm);
						break;
					}
				case 2:
					{
						frmGetGroup.InstancePtr.Show(App.MainForm);
						break;
					}
			//case 3:
			//    {
			//        SetMenuOptions("MAIN");
			//        break;
			//    }
			}
			//end switch
		}

		public object FileMaintCaptions(FCMenuItem parent)
		{
			object FileMaintCaptions = null;
			int CurRow;
			string strTemp = "";
			int lngFCode = 0;
			bool boolDisable = false;
			string imageKey = "";
			//MDIParent.InstancePtr.Text = "TRIO Software - Tax Billing        [File Maintenance]";
			ClearLabels();
			for (CurRow = 1; CurRow <= 8; CurRow++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (CurRow)
				{
				// Select Case intCounter
					case 1:
						{
							strTemp = "Customize";
							lngFCode = modGlobalVariables.CNSTCUSTOMIZE;
							break;
						}
					case 2:
						{
							strTemp = "Check Database Structure";
							break;
						}
					case 3:
						{
							strTemp = "Distribution Percentages";
							lngFCode = modGlobalVariables.CNSTDISTPERCENTS;
							break;
						}
					case 4:
						{
							strTemp = "Return Address";
							break;
						}
					case 5:
						{
							strTemp = "Discount";
							lngFCode = modGlobalVariables.CNSTDISCOUNT;
							break;
						}
					case 6:
						{
							strTemp = "Choose Bill Image";
							lngFCode = modGlobalVariables.CNSTIMAGE;
							break;
						}
					case 7:
						{
							strTemp = "Edit Custom Bill";
							break;
						}
					case 8:
						{
							strTemp = "Database Extract";
							break;
						}
				//case 9:
				//    {
				//        strTemp = "X. Return to Main";
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0)
				{
					boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				FCMenuItem newItem = parent.SubItems.Add(strTemp, "MenuClick:" + CurRow, "FILE", !boolDisable, 2, imageKey);
			}
			// Call AutoSizeMenu(10)
			//AutoSizeMenu_2(20);
			return FileMaintCaptions;
		}

		//private void MDIParent_Resize(object sender, System.EventArgs e)
		//{
		//	picArchive.Width = Picture1.Width;
		//	picArchive.Height = Picture1.Height;
		//}

		private void MDIForm_Unload(object sender, FCFormClosingEventArgs e)
		{
			modReplaceWorkFiles.EntryFlagFile(true, "BL");
            //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
            //Interaction.Shell(FCFileSystem.Statics.UserDataFolder + "\\TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
            App.MainForm.OpenModule("TWGNENTY");
            //- MDIParent.Close();
            Application.Exit();
		}

		private void mnuBudgetaryHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twbd0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuCashReceiptsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twcr0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuClerkHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twck0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuCodeEnforcementHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twce0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuEditDistrictValues_Click(object sender, System.EventArgs e)
		{
			frmBangorDistrict.InstancePtr.Show(App.MainForm);
		}

		private void mnuEnhanced911Help_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twe90000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		//private void mnuFExit_Click(object sender, System.EventArgs e)
		//{
		//	Close();
		//}

		private void mnuGeneralEntryHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twgn0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuHAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout.InstancePtr.Show();
		}

		private void mnuMotorVehicleRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twmv0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		//private void mnuOMaxForms_Click(object sender, System.EventArgs e)
		//{
		//	uint nr = 0;
		//	modAPIsConst.RegCreateKey(modGlobalVariables.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, ref nr);
		//	if (mnuOMaxForms.Checked)
		//	{
		//		modRegistry.SaveKeyValue(modGlobalVariables.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "False");
		//	}
		//	else
		//	{
		//		modRegistry.SaveKeyValue(modGlobalVariables.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "True");
		//	}
		//	modGlobalConstants.Statics.boolMaxForms = !modGlobalConstants.Statics.boolMaxForms;
		//	mnuOMaxForms.Checked = !mnuOMaxForms.Checked;
		//}

		//private void AutoSizeMenu_2(short intLabels)
		//{
		//	AutoSizeMenu(ref intLabels);
		//}

		//private void AutoSizeMenu(ref short intLabels)
		//{
		//	int intCounter;
		//	// vbPorter upgrade warning: FullRowHeight As int	OnWriteFCConvert.ToDouble(
		//	int FullRowHeight;
		//	// this will store the height of the labels with text
		//	intLabels = 20;
		//	// Grid.RowHeight(0) = 0       'the first row in the grid is height = 0
		//	/*FullRowHeight*/ = FCConvert.ToInt32(GRID.HeightOriginal / (intLabels + (((21 - intLabels) / 3.0) / 2)));
		//	for (intCounter = 1; intCounter <= 20; intCounter++)
		//	{
		//		// this just assigns the row height
		//		// If Grid.TextMatrix(intCounter, 1) = vbNullString Then
		//		// Grid.RowHeight(intCounter) = (FullRowHeight / 3) - 55
		//		// -50 is just a small adjustment
		//		// Else
		//		//GRID.RowHeight(intCounter, FullRowHeight + 15);
		//		// +20 minor adjustment to help with two row labels
		//		// End If
		//	}
		//}

		private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twpy0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twpp0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuPrintForms_Click(object sender, System.EventArgs e)
		{
			frmScreenPrint.InstancePtr.ShowDialog();
			//this.ZOrder(ZOrderConstants.SendToBack);
		}

		private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twre0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuRedbookHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twrb0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twbl0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twcl0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twut0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twvr0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		//private void GRID_KeyDownEvent(object sender, KeyEventArgs e)
		//{
		//	int intCounter;
		//	int intPlaceHolder = 0;
		//	Keys KeyCode = e.KeyCode;
		//	if (KeyCode >= Keys.NumPad0 && KeyCode <= Keys.NumPad9)
		//	{
		//		KeyCode -= 48;
		//	}
		//	if ((KeyCode == Keys.Down) || (KeyCode == Keys.Up))
		//	{
		//		intPlaceHolder = GRID.Row;
		//		if (KeyCode == Keys.Down)
		//		{
		//			KeepGoing:
		//			if ((GRID.Row + 1) <= (GRID.Rows - 1))
		//			{
		//				GRID.Row += 1;
		//				if (GRID.TextMatrix(GRID.Row, 1) == string.Empty)
		//				{
		//					GRID.Row -= 1;
		//				}
		//				else if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				{
		//					goto KeepGoing;
		//				}
		//			}
		//		}
		//		else
		//		{
		//			KeepGoingUP:
		//			if ((GRID.Row - 1) >= 1)
		//			{
		//				GRID.Row -= 1;
		//				if (GRID.Row < 1)
		//					GRID.Row = 1;
		//				if (GRID.TextMatrix(GRID.Row, 1) == string.Empty)
		//				{
		//				}
		//				else if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				{
		//					goto KeepGoingUP;
		//				}
		//			}
		//			else
		//			{
		//				GRID.Row = intPlaceHolder;
		//			}
		//		}
		//		// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1) = ImageList1.ListImages("Clear").Picture
		//		GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, GRID.Rows - 1, null);
		//		GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, GRID.Row, 0, ImageList1.Images["Left"]);
		//		KeyCode = 0;
		//		return;
		//	}
		//	for (intCounter = 1; intCounter <= GRID.Rows - 1; intCounter++)
		//	{
		//		if (Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1) == FCConvert.ToString(Convert.ToChar(KeyCode)))
		//		{
		//			if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intCounter, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				return;
		//			// CallByName MDIParent, "Menu" & intCounter & "_click", VbMethod
		//			MenuClick(intCounter);
		//			KeyCode = 0;
		//			break;
		//		}
		//	}
		//}

		//private void GRID_KeyPressEvent(object sender, KeyPressEventArgs e)
		//{
		//	int KeyAscii = Strings.Asc(e.KeyChar);
		//	if (KeyAscii != 13)
		//		return;
		//	if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION || GRID.TextMatrix(GRID.Row, 1) == string.Empty)
		//		return;
		//	// CallByName MDIParent, "Menu" & LabelNumber(Asc(UCase(Left(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, (GRID.Row), 1), 1)))) & "_click", VbMethod
		//	MenuClick(GRID.Row);
		//}

		//private void GRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	int lngRow;
		//	lngRow = GRID.MouseRow;
		//	if (lngRow < 0)
		//		return;
		//	if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION || GRID.TextMatrix(lngRow, 1) == string.Empty)
		//		return;
		//	// CallByName MDIParent, "Menu" & LabelNumber(Asc(UCase(Left(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, (GRID.Row), 1), 1)))) & "_click", VbMethod
		//	MenuClick(GRID.Row);
		//}

		//private void GRID_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	double i;
		//	// avoid extra work
		//	if (GRID.MouseRow == Statics.R)
		//		return;
		//	Statics.R = GRID.MouseRow;
		//	if (Statics.R < 1)
		//		return;
		//	// move selection (even with no click)
		//	GRID.Select(Statics.R, 1);
		//	// Grid.Select R, 0
		//	// remove cursor image
		//	// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1, 0) = ImageList1.ListImages("Clear").Picture
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, GRID.Rows - 1, 0, null);
		//	// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, Grid.Rows - 1, 1) = TRIOCOLORGRAYBACKGROUND
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, GRID.Rows - 1, 1, GRID.BackColor);
		//	// show cursor image if there is some text in column 5
		//	if (Statics.R == 0)
		//		return;
		//	if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Statics.R, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION || GRID.TextMatrix(Statics.R, 1) == string.Empty)
		//	{
		//		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
		//		return;
		//	}
		//	else
		//	{
		//		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
		//	}
		//	// Call Grid.Select(R, 0, R, 1)
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, Statics.R, 0, ImageList1.Images["Left"]);
		//	GRID.Select(Statics.R, 1);
		//}

		private void ClearLabels()
		{
			// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, Grid.Rows - 1, 1) = TRIOCOLORGRAYBACKGROUND
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, GRID.Rows - 1, 1, GRID.BackColor);
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 20, 1, modGlobalConstants.Statics.TRIOCOLORBLACK);
			//GRID.ColData(1, string.Empty);
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			// mdiparent shouldnt need this function.  mdi functions should always send false
			// to validpermissions
			// Dim clsChildList As clsDRWrapper
			// Dim dcTemp As New clsDRWrapper
			// Dim strPerm As String
			// Dim lngChild As Long
			// 
			// 
			// Set clsChildList = New clsDRWrapper
			// 
			// Call tscRESecurity.Get_Children(clsChildList, lngFuncID)
			// 
			// Do While Not clsChildList.EndOfFile
			// lngChild = clsChildList.GetData("childid")
			// strPerm = tscRESecurity.Check_Permissions(lngChild)
			// 
			// Select Case lngFuncID
			// Case COSTUPDATE
			// Select Case lngChild
			// 
			// End Select
			// Case UPDATEMENU
			// End Select
			// 
			// Call clsChildList.MoveNext
			// Loop
		}

		private void FileMaintActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmCustomize.InstancePtr.Show(App.MainForm);
						break;
					}
				case 2:
					{
						if (modMain.CheckDatabaseStructure())
						{
							MessageBox.Show("Database structure checked", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("Database structure check failed", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						break;
					}
				case 3:
					{
						frmDistribution.InstancePtr.Init(7, false);
						// frmDistribution.Show , MDIParent
						break;
					}
				case 4:
					{
						frmReturnAddress.InstancePtr.Show(App.MainForm);
						break;
					}
				case 5:
					{
						frmDiscount.InstancePtr.Show(App.MainForm);
						break;
					}
				case 6:
					{
						frmChooseImage.InstancePtr.Show(App.MainForm);
						break;
					}
				case 7:
					{
						frmCustomBill.InstancePtr.Init("BL");
						break;
					}
				case 8:
					{
						frmExport.InstancePtr.Init();
						break;
					}
			//case 9:
			//    {
			//        SetMenuOptions("MAIN");
			//        break;
			//    }
			//case 10:
			//    {
			//        break;
			//    }
			}
			//end switch
		}
		/// <summary>
		/// Private Sub RealEstateActions(ByVal intMenu As Integer)
		/// </summary>
		/// <summary>
		/// Select Case intMenu
		/// </summary>
		/// <summary>
		/// Case 1
		/// </summary>
		/// <summary>
		/// Load frmAuditInfo
		/// </summary>
		/// <summary>
		/// Call frmAuditInfo.Init(1)
		/// </summary>
		/// <summary>
		/// frmAuditInfo.Show , MDIParent
		/// </summary>
		/// <summary>
		/// Screen.MousePointer = vbHourglass
		/// </summary>
		/// <summary>
		/// frmREAudit.Show , MDIParent
		/// </summary>
		/// <summary>
		/// Case 2
		/// </summary>
		/// <summary>
		/// Load frmAuditInfo
		/// </summary>
		/// <summary>
		/// Call frmAuditInfo.Init(3)
		/// </summary>
		/// <summary>
		/// frmAuditInfo.Show , MDIParent
		/// </summary>
		/// <summary>
		/// Case 3
		/// </summary>
		/// <summary>
		/// SetMenuOptions ("MAIN")
		/// </summary>
		/// <summary>
		/// End Select
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Private Sub PersonalPropertyActions(ByVal intMenu As Integer)
		/// </summary>
		/// <summary>
		/// Select Case intMenu
		/// </summary>
		/// <summary>
		/// Case 1
		/// </summary>
		/// <summary>
		/// Load frmAuditInfo
		/// </summary>
		/// <summary>
		/// Call frmAuditInfo.Init(2)
		/// </summary>
		/// <summary>
		/// frmAuditInfo.Show , MDIParent
		/// </summary>
		/// <summary>
		/// Case 2
		/// </summary>
		/// <summary>
		/// Load frmAuditInfo
		/// </summary>
		/// <summary>
		/// Call frmAuditInfo.Init(4)
		/// </summary>
		/// <summary>
		/// frmAuditInfo.Show , MDIParent
		/// </summary>
		/// <summary>
		/// Case 3
		/// </summary>
		/// <summary>
		/// SetMenuOptions ("MAIN")
		/// </summary>
		/// <summary>
		/// End Select
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		/// Private Sub RealEstateCaptions()
		/// </summary>
		/// <summary>
		/// Dim CurRow As Integer
		/// </summary>
		/// <summary>
		/// Dim strtemp As String
		/// </summary>
		/// <summary>
		/// Dim lngFCode As Long
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// MDIParent.Caption = "TRIO Software - Tax Billing       [Real Estate]"
		/// </summary>
		/// <summary>
		/// ClearLabels
		/// </summary>
		/// <summary>
		/// Select Case intCounter
		/// </summary>
		/// <summary>
		/// With Grid
		/// </summary>
		/// <summary>
		/// For CurRow = 0 To 20
		/// </summary>
		/// <summary>
		/// lngFCode = 0
		/// </summary>
		/// <summary>
		/// Select Case CurRow
		/// </summary>
		/// <summary>
		/// Case 1
		/// </summary>
		/// <summary>
		/// strtemp = "1. Audit"
		/// </summary>
		/// <summary>
		/// Case 2
		/// </summary>
		/// <summary>
		/// strtemp = "2. Transfer New Billing Data"
		/// </summary>
		/// <summary>
		/// Case 3
		/// </summary>
		/// <summary>
		/// strtemp = "3. Return to Main"
		/// </summary>
		/// <summary>
		/// Case Else
		/// </summary>
		/// <summary>
		/// strtemp = ""
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// End Select
		/// </summary>
		/// <summary>
		/// .TextMatrix((CurRow), 1) = strtemp
		/// </summary>
		/// <summary>
		/// If lngFCode <> 0 Then
		/// </summary>
		/// <summary>
		/// Call EnableDisableMenuOption(ValidPermissions(Me, lngFCode, False), CurRow)
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// Next
		/// </summary>
		/// <summary>
		/// End With
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Call AutoSizeMenu(20)
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Private Sub PersonalPropertyCaptions()
		/// </summary>
		/// <summary>
		/// Dim CurRow As Integer
		/// </summary>
		/// <summary>
		/// Dim strtemp As String
		/// </summary>
		/// <summary>
		/// Dim lngFCode As Long
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// MDIParent.Caption = "TRIO Software - Tax Billing       [Personal Property]"
		/// </summary>
		/// <summary>
		/// ClearLabels
		/// </summary>
		/// <summary>
		/// Select Case intCounter
		/// </summary>
		/// <summary>
		/// With Grid
		/// </summary>
		/// <summary>
		/// For CurRow = 0 To 20
		/// </summary>
		/// <summary>
		/// lngFCode = 0
		/// </summary>
		/// <summary>
		/// Select Case CurRow
		/// </summary>
		/// <summary>
		/// Case 1
		/// </summary>
		/// <summary>
		/// strtemp = "1. Audit"
		/// </summary>
		/// <summary>
		/// LabelNumber(Asc("1")) = 1
		/// </summary>
		/// <summary>
		/// Case 2
		/// </summary>
		/// <summary>
		/// strtemp = "2. Transfer New Billing Data"
		/// </summary>
		/// <summary>
		/// LabelNumber(Asc("2")) = 2
		/// </summary>
		/// <summary>
		/// Case 3
		/// </summary>
		/// <summary>
		/// strtemp = "3. Return to Main"
		/// </summary>
		/// <summary>
		/// LabelNumber(Asc("X")) = 3
		/// </summary>
		/// <summary>
		/// Case Else
		/// </summary>
		/// <summary>
		/// strtemp = ""
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// End Select
		/// </summary>
		/// <summary>
		/// .TextMatrix((CurRow), 1) = strtemp
		/// </summary>
		/// <summary>
		/// If lngFCode <> 0 Then
		/// </summary>
		/// <summary>
		/// Call EnableDisableMenuOption(ValidPermissions(Me, lngFCode, False), CurRow)
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// Next
		/// </summary>
		/// <summary>
		/// End With
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Call AutoSizeMenu(20)
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void BillCaptions(FCMenuItem parent)
		{
			int CurRow;
			string strTemp = "";
			int lngFCode = 0;
			bool boolDisable = false;
			string imageKey = "";
			//MDIParent.InstancePtr.Text = "TRIO Software - Tax Billing       [Bills]";
			ClearLabels();
			for (CurRow = 1; CurRow <= 2; CurRow++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Print Bills";
							lngFCode = modGlobalVariables.CNSTPRINTBILLS;
							break;
						}
					case 2:
						{
							strTemp = "Out-Printing";
							lngFCode = modGlobalVariables.CNSTOUTPRINTING;
							break;
						}
				//case 3:
				//    {
				//        strTemp = "3. Return to Main";
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0)
				{
					boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				FCMenuItem newItem = parent.SubItems.Add(strTemp, "MenuClick:" + CurRow, "BILL", !boolDisable, 2, imageKey);
			}
			// CurRow
			//AutoSizeMenu_2(20);
		}

		private void OutPrintingCaptions()
		{
			int CurRow;
			string strTemp = "";
			int lngFCode = 0;
			bool boolDisable = false;
			string imageKey = "";
			//MDIParent.InstancePtr.Text = "TRIO Software - Tax Billing       [Out-Printing]";
			ClearLabels();
			// Select Case intCounter
			for (CurRow = 1; CurRow <= 4; CurRow++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Create RE Out-Print File";
							break;
						}
					case 2:
						{
							strTemp = "Create PP Out-Print File";
							break;
						}
					case 3:
						{
							strTemp = "Copy RE Out-Print File to Disk";
							break;
						}
					case 4:
						{
							strTemp = "Copy PP Out-Print File to Disk";
							break;
						}
				//case 5:
				//    {
				//        strTemp = "X. Return to Main";
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0)
				{
					boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "MenuClick:" + CurRow, "OUTPRINTING", !boolDisable, 1, imageKey);
			}
			//AutoSizeMenu_2(20);
		}

		private void BillActions(int intMenu)
		{
			int lngYear;
			string strResponse = "";
			switch (intMenu)
			{
				case 1:
					{
						// frmChooseBillFormat.Show , MDIParent
						frmChooseBillFormat.InstancePtr.Init(false);
						break;
					}
				case 2:
					{
						frmOutPrinting.InstancePtr.Show(App.MainForm);
						break;
					}
			//case 3:
			//    {
			//        SetMenuOptions("MAIN");
			//        break;
			//    }
			}
			//end switch
		}

		private void OutprintingActions(int intMenu)
		{
			int lngYear;
			string strResponse = "";
			switch (intMenu)
			{
				case 1:
					{
						REYear:
						;
						strResponse = Interaction.InputBox("Please enter the tax year", "");
						if (Conversion.Val(strResponse) == 0)
						{
							// cancelled
							return;
						}
						if (Conversion.Val(strResponse) < 1990 || Conversion.Val(strResponse) > 2030)
						{
							MessageBox.Show("Invalid year", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							goto REYear;
						}
						lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strResponse)));
						break;
					}
			//case 2:
			//    {
			//        break;
			//    }
			//case 3:
			//    {
			//        break;
			//    }
			//case 4:
			//    {
			//        break;
			//    }
			//case 5:
			//    {
			//        SetMenuOptions("MAIN");
			//        break;
			//    }
			}
			//end switch
		}

		public class StaticVariables
		{
			/// </summary>
			/// Summary description for MDIParent.
			/// <summary>
			public int R = 0, C;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
