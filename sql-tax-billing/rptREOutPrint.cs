﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using System.Drawing;
using System.IO;
using TWSharedLibrary;

namespace TWBL0000
{
	/// <summary>
	/// Summary description for rptREOutPrint.
	/// </summary>
	public partial class rptREOutPrint : BaseSectionReport
	{
		public rptREOutPrint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate Out-Print Report";
		}

		public static rptREOutPrint InstancePtr
		{
			get
			{
				return (rptREOutPrint)Sys.GetInstance(typeof(rptREOutPrint));
			}
		}

		protected rptREOutPrint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptREOutPrint	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		//clsDRWrapper clsOutPrint = new clsDRWrapper();
		//double[] DistArray = new double[5 + 1];
		//clsReportPrinterFunctions clsPrint = new clsReportPrinterFunctions();
		//FileSystemObject fso = new FileSystemObject();
		StreamReader txtStream;
		modTypes.OutPrintingFormat OPF = new modTypes.OutPrintingFormat(0);
		modTypes.OutPrintingEnhancedFormat OPFEnh = new modTypes.OutPrintingEnhancedFormat(0);
		bool boolCommaDelimited;
		int lngRecordCount;
		/// <summary>
		/// keeps track of how many records have been printed
		/// </summary>
		bool boolUsingEnhanced;
		double lngTotalAssessment;
		double lngTotalLand;
		double lngTotalBuilding;
		double dblTotalTax;
		double lngTotalHomestead;
		double lngTotalOther;
		double lngTotalPP;
		bool boolAddTotals;
		int lngLastAccount;
		string strLastType;
		int lngCurrAccount;
		// vbPorter upgrade warning: strCurrType As string	OnWrite(string, FixedString)
		string strCurrType = "";

		private bool IsFileEnded_2(short intFile)
		{
			return IsFileEnded(ref intFile);
		}

		private bool IsFileEnded(ref short intFile)
		{
			bool IsFileEnded = false;
			IsFileEnded = FCFileSystem.EOF(intFile);
			return IsFileEnded;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// EOF = clsOutPrint.EndOfFile
			if (boolCommaDelimited)
			{
				eArgs.EOF = txtStream.EndOfStream;
			}
			else
			{
				eArgs.EOF = IsFileEnded_2(42);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCFileSystem.FileClose();
            //FC:FINAL:MSH - issue #1628: close StreamReader after finishing work with file
            if (txtStream != null)
            {
                txtStream.Close();
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int cnt;
			int const_printtoolid;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngLastAccount = -1;
			strLastType = "";
			lngTotalAssessment = 0;
			lngTotalLand = 0;
			lngTotalBuilding = 0;
			dblTotalTax = 0;
			lngTotalHomestead = 0;
			lngTotalOther = 0;
			lngTotalPP = 0;
			lblMuniname.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			txtDate.Text = DateTime.Today.ToShortDateString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCFileSystem.FileClose();
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			// If Not clsOutPrint.EndOfFile Then
			string strData = "";
			string[] strDataArray = null;
			int lngTemp = 0;
			int x;
			boolAddTotals = false;
			if (!boolCommaDelimited)
			{
				if (!FCFileSystem.EOF(42))
				{
					lngRecordCount += 1;
					if (boolUsingEnhanced)
					{
						GetEnhancedFixed();
					}
					else
					{
						GetRegularFixed();
					}
				}
			}
			else
			{
				if (!txtStream.EndOfStream)
				{
					boolAddTotals = true;
					strData = txtStream.ReadLine();
					// one bill record
					lngRecordCount += 1;
					// the next two lines will allow us to split the comma delimted record without splitting on commas in text
					strData = Strings.Replace(strData, FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)), "^", 1, -1, CompareConstants.vbTextCompare);
					strData = Strings.Replace(strData, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
					strDataArray = Strings.Split(strData, "^", -1, CompareConstants.vbTextCompare);
					strCurrType = strDataArray[0];
					txtAccount.Text = strDataArray[1];
					lngCurrAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(strDataArray[1])));
					txtLocation.Text = Strings.Trim(strDataArray[2]);
					txtMapLot.Text = Strings.Trim(strDataArray[3]);
					txtName.Text = Strings.Trim(strDataArray[4]);
					txtAddress1.Text = Strings.Trim(strDataArray[5]);
					txtAddress2.Text = Strings.Trim(strDataArray[6]);
					txtAddress3.Text = Strings.Trim(strDataArray[7]) + " " + Strings.Trim(strDataArray[8]) + " " + Strings.Trim(strDataArray[9]);
					txtLand.Text = Strings.Trim(strDataArray[10]);
					txtBldg.Text = Strings.Trim(strDataArray[11]);
					txtPaidToDate.Text = Strings.Trim(strDataArray[44]);
					txtHomestead.Text = Strings.Trim(strDataArray[32]);
					txtOther.Text = Strings.Trim(strDataArray[33]);
					txtTax.Text = Strings.Trim(strDataArray[26]);
					txtDist1.Text = Strings.Trim(strDataArray[34]);
					txtDist2.Text = Strings.Trim(strDataArray[35]);
					txtDist3.Text = Strings.Trim(strDataArray[36]);
					txtDist4.Text = Strings.Trim(strDataArray[37]);
					txtDist5.Text = Strings.Trim(strDataArray[38]);
					txtPeriod1.Text = Strings.Trim(strDataArray[27]);
					txtPeriod2.Text = Strings.Trim(strDataArray[28]);
					txtPeriod3.Text = Strings.Trim(strDataArray[29]);
					txtPeriod4.Text = Strings.Trim(strDataArray[30]);
					if (strDataArray[0] == "RE")
					{
						lblAssessPP.Visible = false;
						lblExemptPP.Visible = false;
						lblTotalPP.Visible = false;
						txtAssessPP.Visible = false;
						txtExemptPP.Visible = false;
						txtTotalPP.Visible = false;
						lblCat1.Visible = false;
						lblCat2.Visible = false;
						lblCat3.Visible = false;
						lblCat49.Visible = false;
						txtCat1.Visible = false;
						txtCat2.Visible = false;
						txtCat3.Visible = false;
						txtCat49.Visible = false;
						txtTotalPP.Text = "";
						txtAssessPP.Text = "";
						txtExemptPP.Text = "";
					}
					else
					{
						lblAssessPP.Visible = true;
						lblExemptPP.Visible = true;
						lblTotalPP.Visible = true;
						txtAssessPP.Visible = true;
						txtExemptPP.Visible = true;
						txtTotalPP.Visible = true;
						lblCat1.Visible = true;
						lblCat2.Visible = true;
						lblCat3.Visible = true;
						lblCat49.Visible = true;
						txtCat1.Visible = true;
						txtCat2.Visible = true;
						txtCat3.Visible = true;
						txtCat49.Visible = true;
						txtCat1.Text = Strings.Trim(strDataArray[14]);
						txtCat2.Text = Strings.Trim(strDataArray[15]);
						txtCat3.Text = Strings.Trim(strDataArray[16]);
						// txtCat49.Text = Trim(strDataArray(17))
						lngTemp = 0;
						for (x = 4; x <= 9; x++)
						{
							if (Strings.Trim(strDataArray[13 + x]) != string.Empty)
							{
								lngTemp += FCConvert.ToInt32(FCConvert.ToDouble(strDataArray[13 + x]));
							}
						}
						// x
						txtCat49.Text = Strings.Format(lngTemp, "#,###,##0");
						txtTotalPP.Text = Strings.Trim(strDataArray[23]);
						txtAssessPP.Text = Strings.Trim(strDataArray[25]);
						txtExemptPP.Text = Strings.Trim(strDataArray[24]);
					}
				}
			}
			if (boolAddTotals)
			{
				if (strCurrType != strLastType || lngCurrAccount != lngLastAccount)
				{
					strLastType = strCurrType;
					lngLastAccount = lngCurrAccount;
					//FC:FINAL:CHN - issue #1382: Change C# converting on methods as at VB6 version.
					if (txtLand.Text != string.Empty)
					{
						// lngTotalLand += FCConvert.ToInt32(txtLand.Text);
						lngTotalLand += Conversion.CLng(txtLand.Text);
					}
					if (txtBldg.Text != string.Empty)
					{
						// lngTotalBuilding += FCConvert.ToInt32(txtBldg.Text);
						lngTotalBuilding += Conversion.CLng(txtBldg.Text);
					}
					if (txtHomestead.Text != string.Empty)
					{
						// lngTotalHomestead += FCConvert.ToInt32(txtHomestead.Text);
						lngTotalHomestead += Conversion.CLng(txtHomestead.Text);
					}
					if (txtOther.Text != string.Empty)
					{
						// lngTotalOther += FCConvert.ToInt32(txtOther.Text);
						lngTotalOther += Conversion.CLng(txtOther.Text);
					}
					if (txtTax.Text != string.Empty)
					{
						// dblTotalTax += FCConvert.ToDouble(txtTax.Text);
						dblTotalTax += Conversion.CDbl(txtTax.Text);
					}
					if (txtAssessPP.Text != string.Empty)
					{
						// lngTotalPP += FCConvert.ToInt32(txtAssessPP.Text);
						lngTotalPP += Conversion.CLng(txtAssessPP.Text);
					}
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = lngRecordCount.ToString();
			txtTotalLand.Text = Strings.Format(lngTotalLand, "#,###,###,##0");
			txtTotalBuilding.Text = Strings.Format(lngTotalBuilding, "#,###,###,##0");
			txtTotalAssessment.Text = Strings.Format(lngTotalBuilding + lngTotalLand - lngTotalHomestead - lngTotalOther, "#,###,###,##0");
			txtTotalTax.Text = Strings.Format(dblTotalTax, "#,###,###,##0.00");
			txtTotalExempt.Text = Strings.Format(lngTotalHomestead + lngTotalOther, "#,###,###,##0");
			txtTotalHomestead.Text = Strings.Format(lngTotalHomestead, "#,###,###,##0");
			txtTotalOther.Text = Strings.Format(lngTotalOther, "#,###,###,##0");
			txtTotalPPTotal.Text = Strings.Format(lngTotalPP, "#,###,###,##0");
			if (lngTotalPP > 0)
			{
				txtTotalPPTotal.Visible = true;
				lblPPTotal.Visible = true;
			}
			else
			{
				txtTotalPPTotal.Visible = false;
				lblPPTotal.Visible = false;
			}
		}
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intYear As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intWhichReport As short	OnWriteFCConvert.ToInt32(
		public void Init(int intOrder, int intYear, bool boolSetFont, string strFontName, string strPrinterName, int intWhichReport)
		{
			string strTemp = "";
			int x;
			string strSQL = "";
			string strOrder = "";
			// Select Case intOrder
			// Case 1
			// strOrder = "master.rsaccount"
			// Case 2
			// strOrder = "master.rsname"
			// Case 3
			// strOrder = "master.rsmaplot"
			// End Select
			if (boolSetFont)
			{
				// now set each box and label to the correct printer font
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl fld in this.GetAllControls())
				{
					bool setFont = false;
					bool bold = false;
					if (FCConvert.ToString(fld.Tag) == "textbox")
					{
						setFont = true;
					}
					else if (FCConvert.ToString(fld.Tag) == "bold")
					{
						setFont = true;
						bold = true;
					}
					if (setFont)
					{
						GrapeCity.ActiveReports.SectionReportModel.TextBox textBox = fld as GrapeCity.ActiveReports.SectionReportModel.TextBox;
						if (textBox != null)
						{
							textBox.Font = new Font(strFontName, textBox.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
						}
						else
						{
							GrapeCity.ActiveReports.SectionReportModel.Label label = fld as GrapeCity.ActiveReports.SectionReportModel.Label;
							if (label != null)
							{
								label.Font = new Font(strFontName, label.Font.Size, bold ? FontStyle.Bold : FontStyle.Regular);
							}
						}
					}
				}
				//this.Printer.RenderMode = 1;
			}
			// change to the selected printer
			this.Document.Printer.PrinterName = strPrinterName;
			switch (intWhichReport)
			{
				case 2:
					{
						// landscape regular
						this.Document.Printer.DefaultPageSettings.Landscape = true;
						break;
					}
				case 3:
					{
						// wide printer
						//this.Document.Printer.PaperSize = 39;
						this.Document.Printer.PaperKind = System.Drawing.Printing.PaperKind.USStandardFanfold;
						this.PageSettings.PaperWidth = this.Document.Printer.PaperWidth;
						this.PageSettings.PaperHeight = this.Document.Printer.PaperHeight;
						break;
					}
			}
			//end switch
			if (FCFileSystem.FileExists("TSREBILL.FIL"))
			{
				//FC:FINAL:CHN - issue #1382: Change File class on FCFileSystem to use same paths.
				// txtStream = File.OpenText("TSREBILL.FIL");
				txtStream = FCFileSystem.OpenText("TSREBILL.FIL");
				if (txtStream == null)
				{
					MessageBox.Show("Cannot open Real Estate outprinting file", "No file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
				}
				if (txtStream.EndOfStream)
				{
					MessageBox.Show("Cannot open Real Estate outprinting file", "No file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
				}
			}
			else
			{
				MessageBox.Show("Cannot find Real Estate outprinting file", "No file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
				return;
			}
			lngRecordCount = 0;
			for (int i = 0; i < 8; i++)
			{
				strTemp += (char)txtStream.Read();
			}
			if (FCConvert.ToBoolean(Strings.InStr(1, strTemp, ",", CompareConstants.vbTextCompare)))
			{
				boolCommaDelimited = true;
				txtStream.Close();
				//FC:FINAL:CHN - issue #1382: Change File class on FCFileSystem to use same paths.
				//  txtStream = File.OpenText("TSREBILL.FIL");
				txtStream = FCFileSystem.OpenText("TSREBILL.FIL");
				txtStream.ReadLine();
				// skip the header record
			}
			else
			{
				boolCommaDelimited = false;
				txtStream.Close();
				FCFileSystem.FileClose();
				FCFileSystem.FileOpen(42, "TSREBILL.FIL", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(OPF));
				FCFileSystem.FileGet(42, ref OPF, -1/*? 1 */);
				if (OPF.LineEnd == "\r\n")
				{
					boolUsingEnhanced = false;
				}
				else
				{
					boolUsingEnhanced = true;
					FCFileSystem.FileClose();
					FCFileSystem.FileOpen(42, "TSREBILL.FIL", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(OPFEnh));
					FCFileSystem.FileGet(42, ref OPFEnh, -1/*? 1 */);
				}
			}
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this, strPrinterName, 0, false, false, "Pages", intWhichReport == 2, "", "TRIO Software", false, true, "REOutprint");
			// the distributions are set up
		}

		private void GetRegularFixed()
		{
			FCFileSystem.FileGet(42, ref OPF, lngRecordCount);
			txtLand.Text = "0";
			txtBldg.Text = "0";
			txtTax.Text = "0";
			if (FCFileSystem.EOF(42))
			{
				this.Detail.Visible = false;
				lngRecordCount -= 1;
				return;
			}
			txtAccount.Text = FCConvert.ToString(Conversion.Val(OPF.Account));
			lngCurrAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(OPF.Account)));
			boolAddTotals = true;
			// txtLocation.Text = Trim(Mid(strData, 1, 31))
			txtLocation.Text = Strings.Trim(OPF.Location);
			txtMapLot.Text = Strings.Trim(OPF.MapLot);
			txtName.Text = Strings.Trim(OPF.Name);
			txtAddress1.Text = Strings.Trim(OPF.Address1);
			txtAddress2.Text = Strings.Trim(OPF.Address2);
			txtAddress3.Text = Strings.Trim(OPF.City) + " " + Strings.Trim(OPF.State) + " " + Strings.Trim(OPF.Zip);
			txtLand.Text = Strings.Trim(OPF.Land);
			txtBldg.Text = Strings.Trim(OPF.Building);
			txtPaidToDate.Text = Strings.Trim(OPF.PaidToDate);
			txtHomestead.Text = Strings.Trim(OPF.HomesteadExempt);
			txtOther.Text = Strings.Trim(OPF.OtherExempt);
			txtTax.Text = Strings.Trim(OPF.TotalTax);
			txtDist1.Text = Strings.Trim(OPF.Distribution1);
			txtDist2.Text = Strings.Trim(OPF.Distribution2);
			txtDist3.Text = Strings.Trim(OPF.Distribution3);
			txtDist4.Text = Strings.Trim(OPF.Distribution4);
			txtDist5.Text = Strings.Trim(OPF.Distribution5);
			txtPeriod1.Text = Strings.Trim(OPF.TaxDue1);
			txtPeriod2.Text = Strings.Trim(OPF.TaxDue2);
			txtPeriod3.Text = Strings.Trim(OPF.TaxDue3);
			txtPeriod4.Text = Strings.Trim(OPF.TaxDue4);
			strCurrType = OPF.BillType;
			if (OPF.BillType == "RE")
			{
				lblAssessPP.Visible = false;
				lblExemptPP.Visible = false;
				lblTotalPP.Visible = false;
				txtAssessPP.Visible = false;
				txtExemptPP.Visible = false;
				txtTotalPP.Visible = false;
				lblCat1.Visible = false;
				lblCat2.Visible = false;
				lblCat3.Visible = false;
				lblCat49.Visible = false;
				txtCat1.Visible = false;
				txtCat2.Visible = false;
				txtCat3.Visible = false;
				txtCat49.Visible = false;
				txtTotalPP.Text = "";
				txtAssessPP.Text = "";
				txtExemptPP.Text = "";
			}
			else
			{
				lblAssessPP.Visible = true;
				lblExemptPP.Visible = true;
				lblTotalPP.Visible = true;
				txtAssessPP.Visible = true;
				txtExemptPP.Visible = true;
				txtTotalPP.Visible = true;
				lblCat1.Visible = true;
				lblCat2.Visible = true;
				lblCat3.Visible = true;
				lblCat49.Visible = true;
				txtCat1.Visible = true;
				txtCat2.Visible = true;
				txtCat3.Visible = true;
				txtCat49.Visible = true;
				txtCat1.Text = Strings.Trim(OPF.PPCode1);
				txtCat2.Text = Strings.Trim(OPF.PPCode2);
				txtCat3.Text = Strings.Trim(OPF.PPCode3);
				txtCat49.Text = Strings.Trim(OPF.PPCode5);
				txtTotalPP.Text = Strings.Trim(OPF.TotalPP);
				txtAssessPP.Text = Strings.Trim(OPF.NetAssessment);
				txtExemptPP.Text = Strings.Trim(OPF.TotalExempt);
			}
		}

		private void GetEnhancedFixed()
		{
			FCFileSystem.FileGet(42, ref OPFEnh, lngRecordCount);
			txtLand.Text = "0";
			txtBldg.Text = "0";
			txtTax.Text = "0";
			if (FCFileSystem.EOF(42))
			{
				this.Detail.Visible = false;
				lngRecordCount -= 1;
				return;
			}
			strCurrType = OPFEnh.BillType;
			lngCurrAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(OPFEnh.Account)));
			txtAccount.Text = FCConvert.ToString(Conversion.Val(OPFEnh.Account));
			boolAddTotals = true;
			// txtLocation.Text = Trim(Mid(strData, 1, 31))
			txtLocation.Text = OPFEnh.Location;
			txtMapLot.Text = Strings.Trim(OPFEnh.MapLot);
			txtName.Text = Strings.Trim(OPFEnh.Name);
			txtAddress1.Text = Strings.Trim(OPFEnh.Address1);
			txtAddress2.Text = Strings.Trim(OPFEnh.Address2);
			txtAddress3.Text = Strings.Trim(OPFEnh.City) + " " + Strings.Trim(OPFEnh.State) + " " + Strings.Trim(OPFEnh.Zip);
			txtLand.Text = Strings.Trim(OPFEnh.Land);
			txtBldg.Text = Strings.Trim(OPFEnh.Building);
			txtPaidToDate.Text = Strings.Trim(OPFEnh.PaidToDate);
			txtHomestead.Text = Strings.Trim(OPFEnh.HomesteadExempt);
			txtOther.Text = Strings.Trim(OPFEnh.OtherExempt);
			txtTax.Text = Strings.Trim(OPFEnh.TotalTax);
			txtDist1.Text = Strings.Trim(OPFEnh.Distribution1);
			txtDist2.Text = Strings.Trim(OPFEnh.Distribution2);
			txtDist3.Text = Strings.Trim(OPFEnh.Distribution3);
			txtDist4.Text = Strings.Trim(OPFEnh.Distribution4);
			txtDist5.Text = Strings.Trim(OPFEnh.Distribution5);
			txtPeriod1.Text = Strings.Trim(OPFEnh.TaxDue1);
			txtPeriod2.Text = Strings.Trim(OPFEnh.TaxDue2);
			txtPeriod3.Text = Strings.Trim(OPFEnh.TaxDue3);
			txtPeriod4.Text = Strings.Trim(OPFEnh.TaxDue4);
			if (OPFEnh.BillType == "RE")
			{
				lblAssessPP.Visible = false;
				lblExemptPP.Visible = false;
				lblTotalPP.Visible = false;
				txtAssessPP.Visible = false;
				txtExemptPP.Visible = false;
				txtTotalPP.Visible = false;
				lblCat1.Visible = false;
				lblCat2.Visible = false;
				lblCat3.Visible = false;
				lblCat49.Visible = false;
				txtCat1.Visible = false;
				txtCat2.Visible = false;
				txtCat3.Visible = false;
				txtCat49.Visible = false;
				txtTotalPP.Text = "";
				txtAssessPP.Text = "";
				txtExemptPP.Text = "";
			}
			else
			{
				lblAssessPP.Visible = true;
				lblExemptPP.Visible = true;
				lblTotalPP.Visible = true;
				txtAssessPP.Visible = true;
				txtExemptPP.Visible = true;
				txtTotalPP.Visible = true;
				lblCat1.Visible = true;
				lblCat2.Visible = true;
				lblCat3.Visible = true;
				lblCat49.Visible = true;
				txtCat1.Visible = true;
				txtCat2.Visible = true;
				txtCat3.Visible = true;
				txtCat49.Visible = true;
				txtCat1.Text = Strings.Trim(OPFEnh.PPCode1);
				txtCat2.Text = Strings.Trim(OPFEnh.PPCode2);
				txtCat3.Text = Strings.Trim(OPFEnh.PPCode3);
				txtCat49.Text = Strings.Trim(OPFEnh.PPCode5);
				txtTotalPP.Text = Strings.Trim(OPFEnh.TotalPP);
				txtAssessPP.Text = Strings.Trim(OPFEnh.NetAssessment);
				txtExemptPP.Text = Strings.Trim(OPFEnh.TotalExempt);
			}
		}

		
	}
}
