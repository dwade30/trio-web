﻿namespace AutoImportREPictures
{ 
    public enum RealPropertyRangeType
    {
        None = 0,
        Range = 1,
        Individual = 2
    }

    public enum RealPropertyOrderByType
    {
        Account = 0,
        MapLot = 1,
        DeedName = 2,
        //Location = 3
    }

    public enum RealPropertyRangeByType
    {
        Account = 0,
        Maplot = 1,
        DeedName = 2
    }

    public static class RealPropertyEnumUtility
    {
        public static RealPropertyRangeByType ToRangeByType(int enumValue)
        {
            switch(enumValue)
            {
                case 1:
                    return RealPropertyRangeByType.Maplot;
                case 2:
                    return RealPropertyRangeByType.DeedName;
            }
            return RealPropertyRangeByType.Account;
        }
        public static RealPropertyRangeType ToRangeType(int enumValue)
        {
            switch(enumValue)
            {
                case 1:
                    return RealPropertyRangeType.Range;
                case 2:
                    return RealPropertyRangeType.Individual;
            }

            return RealPropertyRangeType.None;
        }

        public static RealPropertyOrderByType ToOrderByType(int enumValue)
        {
            switch(enumValue)
            {
                case 1:
                    return RealPropertyOrderByType.MapLot;
                case 2:
                    return RealPropertyOrderByType.DeedName;
            }
            return RealPropertyOrderByType.Account;
        }
        public static int ToInteger(this RealPropertyRangeType rangeType)
        {
            switch(rangeType)
            {
                case RealPropertyRangeType.Individual:
                    return 2;
                case RealPropertyRangeType.Range:
                    return 1;
            }

            return 0;
        }
        public static int ToInteger(this RealPropertyRangeByType rangeType)
        {
            switch (rangeType)
            {
                case RealPropertyRangeByType.Maplot:
                    return 1;
                case RealPropertyRangeByType.DeedName:
                    return 2;
            }

            return 0;
        }
    public static int ToInteger(this RealPropertyOrderByType orderType)
        {
            switch(orderType)
            {
                case RealPropertyOrderByType.MapLot:
                    return 1;
                case RealPropertyOrderByType.DeedName:
                    return 2;
            }
            return 0;
        }
    }
}