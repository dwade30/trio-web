﻿namespace AutoImportREPictures
{
    public class ImportProfile
    {
        public string Name { get; set; } = "";
        public string Url { get; set; } = "";
        public string DataEnvironment { get; set; } = "";
        public string SeparatorCharacter { get; set; } = "-";
        public bool IsMapLot { get; set; } = false;
        public bool IncludesCard { get; set; } = false;
        public bool IncludesSequenceNumber { get; set; } = false;

        public bool IncludesMiscData { get; set; } = false;
        public bool UseSeparators { get; set; } = true;
        public int MaplotAccountStartPosition { get; set; } = 0;
        public int CardStartPosition { get; set; } = 0;
        public int MiscStartPosition { get; set; } = 0;
        public int SequenceStartPosition { get; set; } = 0;
        public string SourceDirectory { get; set; } = "";

        public ImportProfile Copy()
        {
            var p = new ImportProfile();
            p.IncludesCard = IncludesCard;
            p.IncludesMiscData = IncludesMiscData;
            p.IncludesSequenceNumber = IncludesSequenceNumber;
            p.IsMapLot = IsMapLot;
            p.MaplotAccountStartPosition = MaplotAccountStartPosition;
            p.MiscStartPosition = MiscStartPosition;
            p.SeparatorCharacter = SeparatorCharacter;
            p.SequenceStartPosition = SequenceStartPosition;
            p.UseSeparators = UseSeparators;
            p.CardStartPosition = CardStartPosition;
            p.SourceDirectory = SourceDirectory;
            p.Name = Name;
            p.Url = Url;
            return p;
        }

        public bool Equals(ImportProfile p)
        {
            bool r;
            r = IncludesCard = p.IncludesCard;
            r = r && (p.UseSeparators == UseSeparators);
            r = r && (p.DataEnvironment == DataEnvironment);
            r = r && (p.IncludesMiscData == IncludesMiscData);
            r = r && (p.IncludesSequenceNumber == IncludesSequenceNumber);
            r = r && (p.IsMapLot == IsMapLot);
            r = r && (p.IncludesCard == IncludesCard);
            r = r && (p.CardStartPosition == CardStartPosition);
            r = r && (p.Name == Name);
            r = r && (p.Url == Url);
            r = r && (p.MaplotAccountStartPosition == MaplotAccountStartPosition);
            r = r && (p.MiscStartPosition == MiscStartPosition);
            r = r && (p.SeparatorCharacter == SeparatorCharacter);
            r = r && (p.SequenceStartPosition == SequenceStartPosition);
            r = r && (p.SourceDirectory == SourceDirectory);
            return r;
        }

        public void Clear()
        {
            Name = "";
            this.CardStartPosition = 0;
            this.IncludesCard = false;
            this.IsMapLot = false;
            this.MaplotAccountStartPosition = 0;
            this.MiscStartPosition = 0;
            this.SeparatorCharacter = "";
            this.SequenceStartPosition = 0;
            this.Url = "";
            this.IncludesMiscData = false;
            this.IncludesSequenceNumber = false;
            this.UseSeparators = true;
        }
    }    
}