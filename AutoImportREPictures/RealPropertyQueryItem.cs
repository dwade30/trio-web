﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoImportREPictures
{
    public class RealPropertyQueryItem
    {
        public int RangeType { get; set; } = 0;
        public int RangeBy { get; set; } = 0;
        public string RangeStart { get; set; } = "";
        public string RangeEnd { get; set; } = "";
        public bool IncludedDeleted { get; set; } = false;
        public int OrderBy { get; set; } = 0;
        public bool OrderAscending { get; set; } = true;
    }
}
