﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.Json;
using System.Text.Json.Serialization;
using Encoder = System.Text.Encoder;
using System.Configuration;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace AutoImportREPictures
{
    public partial class frmAutoImport : Form
    {
        private bool loading = true;
        private string currentAuthToken = "";
        private string currentRefreshToken = "";
        private string currentUser = "";
        private string currentPassword = "";
        private string currentBaseUrl = "";
        private int maxImageWidth = 1920;
        private int maxImageHeight = 1080;
        private const string ConfigurationFilename = "AutoImportPictures.config";
        private Settings currentSettings = new Settings();
        private List<RealProperty> propertyList = new List<RealProperty>();
        private bool uploadCancelled = false;
        public frmAutoImport()
        {
            InitializeComponent();
            this.cmbIdentifierType.SelectedIndexChanged += CmbIdentifierType_SelectedIndexChanged;
            this.cmbFormat.SelectedIndexChanged += CmbFormat_SelectedIndexChanged;
            this.chkCardNumber.Click += ChkCardNumber_Click;
            this.chkIncludesPictureNumber.Click += ChkIncludesPictureNumber_Click;
            this.chkMiscData.Click += ChkMiscData_Click;
            this.cmbProfile.SelectedIndexChanged += CmbProfile_SelectedIndexChanged;
            this.btnUpload.Click += BtnUpload_Click;
            this.cmbSeparator.SelectedIndexChanged += CmbSeparator_SelectedIndexChanged;
            this.txtServer.TextChanged += TxtServer_TextChanged;
            this.txtDataEnvironment.TextChanged += TxtDataEnvironment_TextChanged;
            this.btnSaveProfile.Click += BtnSaveProfile_Click;
            this.btnAddProfile.Click += BtnAddProfile_Click;
            this.btnLoadFiles.Click += BtnLoadFiles_Click;
            this.btnLogin.Click += BtnLogin_Click;
            this.btnLogout.Click += BtnLogout_Click;
            //this.fileItemBindingSource.DataSource = typeof(AutoImportREPictures.FileItem);
            //this.GridPreview.DataSource = this.fileItemBindingSource;
            this.btnCancel.Click += BtnCancel_Click; 
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            uploadCancelled = true;
        }

        private void BtnLogout_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            ConnectToServer();
        }

        private void BtnLoadFiles_Click(object sender, EventArgs e)
        {
            LoadFiles();
        }

        private void BtnAddProfile_Click(object sender, EventArgs e)
        {
            AddProfile();
        }

        private void AddProfile()
        {
            var profileName = PromptDialog.ShowDialog("Profile", "Profile Name");
            if (!string.IsNullOrWhiteSpace(profileName))
            {
                if (currentSettings.Profiles.Any(p => p.Name.ToLower() == profileName))
                {
                    MessageBox.Show("A profile with that name already exists", "Invalid name", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
                cmbProfile.Items.Add(profileName);
                
                loading = true;
                SetProfileComboByName(profileName);
                loading = false;
            }
        }

        private void SetProfileComboByName(string profileName)
        {
            for (int x = 0; x < cmbProfile.Items.Count; x++)
            {
                if (cmbProfile.Items[x].ToString() == profileName)
                {
                    cmbProfile.SelectedIndex = x;
                    break;
                }
            }
        }
        private void BtnSaveProfile_Click(object sender, EventArgs e)
        {
            if (SaveProfile())
            {
                MessageBox.Show("Configuration saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }

        private bool SaveProfile()
        {
            if (string.IsNullOrWhiteSpace(cmbProfile.Text))
            {
                AddProfile();
            }
            if (!string.IsNullOrWhiteSpace(cmbProfile.Text))
            {
                var profile = currentSettings.Profiles.FirstOrDefault(p => p.Name == cmbProfile.Text);
                if (profile == null)
                {                    
                    profile = currentSettings.Profiles.AddNew();
                    profile.Name = cmbProfile.Text;
                }

                profile.IsMapLot = cmbIdentifierType.Text.ToLower() != "account";
                if (UseSeparators())
                {
                    profile.UseSeparators = true;
                    profile.CardStartPosition = nudCardPosition.Value.ToInteger();
                    profile.MaplotAccountStartPosition = nudMaplotAccountPosition.Value.ToInteger();
                    profile.MiscStartPosition = nudMiscPosition.Value.ToInteger();
                    profile.SequenceStartPosition = nudPicNumberPosition.Value.ToInteger();
                }
                else
                {
                    profile.UseSeparators = false;
                    profile.CardStartPosition = nudCardStart.Value.ToInteger();
                    profile.MaplotAccountStartPosition = nudMaplotAccountStart.Value.ToInteger();
                    profile.MiscStartPosition = nudMiscStart.Value.ToInteger();
                    profile.SequenceStartPosition = nudPicNumberStart.Value.ToInteger();
                }

                if (!string.IsNullOrWhiteSpace(cmbSeparator.Text))
                {
                    profile.SeparatorCharacter = cmbSeparator.Text;
                }
                else
                {
                    profile.SeparatorCharacter = "-";
                }

                profile.DataEnvironment = txtDataEnvironment.Text;
                profile.Url = txtServer.Text;
                profile.IncludesCard = chkCardNumber.Checked;
                profile.IncludesMiscData = chkMiscData.Checked;
                profile.IncludesSequenceNumber = chkIncludesPictureNumber.Checked;
                profile.SourceDirectory = txtSourceDirectory.Text.Trim();
            }
           return SaveConfiguration();
        }
        private void LoadConfiguration()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, "TRIO", "RealEstateAutoImport");
            var configFilename = Path.Combine(path,
                ConfigurationFilename);
            currentSettings.Clear();
            if (!System.IO.File.Exists(configFilename))
            {
                return ;
            }

            try
            {
                var configDoc = XDocument.Load(configFilename);
                if (configDoc != null)
                {
                    var tRoot = configDoc.Element("Settings");
                    foreach (XElement profile in tRoot.Element("Profiles").Elements("ImportProfile"))
                    {                       
                       var importProfile = currentSettings.Profiles.AddNew();
                       importProfile.Name = profile.Element("Name").Value;
                       importProfile.Url = profile.Element("Url").Value;
                       importProfile.DataEnvironment = profile.Element("DataEnvironment").Value;
                       importProfile.CardStartPosition = profile.Element("CardStartPosition").Value.ToIntegerValue();
                       importProfile.IncludesCard = profile.Element("IncludesCard").Value.ToBooleanValue();
                       importProfile.IncludesMiscData = profile.Element("IncludesMiscData").Value.ToBooleanValue();
                       importProfile.IncludesSequenceNumber =
                           profile.Element("IncludesSequenceNumber").Value.ToBooleanValue();
                       importProfile.IsMapLot = profile.Element("IsMapLot").Value.ToBooleanValue();
                       importProfile.MaplotAccountStartPosition =
                           profile.Element("MaplotAccountStartPosition").Value.ToIntegerValue();
                       importProfile.MiscStartPosition = profile.Element("MiscStartPosition").Value.ToIntegerValue();
                       importProfile.SeparatorCharacter = profile.Element("SeparatorCharacter").Value;
                       importProfile.SequenceStartPosition =
                           profile.Element("SequenceStartPosition").Value.ToIntegerValue();
                       importProfile.UseSeparators = profile.Element("UseSeparators").Value.ToBooleanValue();
                        importProfile.SourceDirectory = profile.Element("SourceDirectory").Value;
                    }
                    
                }
            }
            catch (Exception ex)
            {

            }

            return ;
        }

        private bool SaveConfiguration()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, "TRIO", "RealEstateAutoImport");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var configFilename = Path.Combine(path,
                ConfigurationFilename);

            try
            {
                using (FileStream fs = new FileStream(configFilename, FileMode.OpenOrCreate))
                {
                    XmlSerializer tXs = new XmlSerializer(typeof(Settings));                    
                    tXs.Serialize(fs,currentSettings);
                    fs.Close();
                }

                return true;
            }
            catch
            {

            }
            return false;
        }
        private void TxtDataEnvironment_TextChanged(object sender, EventArgs e)
        {
            ResetCurrentInformation();
        }

        private void TxtServer_TextChanged(object sender, EventArgs e)
        {
            ResetCurrentInformation();
        }

        private void ResetCurrentInformation()
        {
            currentAuthToken = "";
            currentPassword = "";
            currentRefreshToken = "";
            currentBaseUrl = "";
            currentUser = "";
            propertyList.Clear();
            panelUpload.Visible = false;
            panelUpload.Enabled = false;
        }

        private void LogOut()
        {
            currentAuthToken = "";
            currentRefreshToken = "";
            currentUser = "";
            panelUpload.Visible = false;
            panelUpload.Enabled = false;
            currentPassword = "";
            btnLogin.Enabled = true;
            btnLogout.Enabled = false;
        }

        private void CmbSeparator_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReshowOptions();
        }

        private void BtnUpload_Click(object sender, EventArgs e)
        {
            UploadPictures();
        }

        private void CmbProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentAuthToken = "";
            currentRefreshToken = "";
            currentUser = "";
            currentPassword = "";
            if (!string.IsNullOrWhiteSpace(cmbProfile.Text))
            {
                var profile = currentSettings.Profiles.FirstOrDefault(p => p.Name == cmbProfile.Text);
                if (profile != null)
                {
                    SetControlsFromProfile(profile);
                }
            }
        }

        private void ChkMiscData_Click(object sender, EventArgs e)
        {
            ReshowOptions();
        }

        private void ChkCardNumber_Click(object sender, EventArgs e)
        {
            ReshowOptions();
        }

        private void ChkIncludesPictureNumber_Click(object sender, EventArgs e)
        {
            ReshowOptions();
        }


        private void CmbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {            
            ReshowOptions();
        }

        private bool UseSeparators()
        {
            return cmbFormat.Text == "Use Separators";
        }
        private void CmbIdentifierType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReshowOptions();
        }

        private void frmAutoImport_Load(object sender, EventArgs e)
        {
            loading = true;
            SetupIdentifierCombo();
            SetupFormatCombo();
            SetupSeparatorCombo();
            LoadConfiguration();
            FillProfiles();
            loading = false;

            if (cmbProfile.Items.Count > 0)
            {
                cmbProfile.SelectedIndex = 0;
            }
            else
            {
                ReshowOptions();
            }
        }

        private void FillProfiles()
        {
            cmbProfile.Items.Clear();
            foreach (var profile in currentSettings.Profiles)
            {
                cmbProfile.Items.Add(profile.Name);
            }

        }

        private void SetControlsFromProfile(ImportProfile profile)
        {
            if (profile != null)
            {
                loading = true;
                txtServer.Text = profile.Url;
                txtDataEnvironment.Text = profile.DataEnvironment;
                if (profile.IsMapLot)
                {
                    cmbIdentifierType.SelectedIndex = 0;
                }
                else
                {
                    cmbIdentifierType.SelectedIndex = 1;
                }

                if (profile.UseSeparators)
                {
                    cmbFormat.SelectedIndex = 0;
                }
                else
                {
                    cmbFormat.SelectedIndex = 1;
                }

                chkCardNumber.Checked = profile.IncludesCard;
                chkIncludesPictureNumber.Checked = profile.IncludesSequenceNumber;
                chkMiscData.Checked = profile.IncludesMiscData;
                for (int x = 0; x < cmbSeparator.Items.Count; x++)
                {
                    if (cmbSeparator.Items[x].ToString() == profile.SeparatorCharacter)
                    {
                        cmbSeparator.SelectedIndex = x;
                    }
                }

                if (profile.UseSeparators)
                {
                    nudMaplotAccountPosition.Value = profile.MaplotAccountStartPosition;
                    nudMaplotAccountStart.Value = 0;
                    nudCardPosition.Value = profile.CardStartPosition;
                    nudCardStart.Value = 0;
                    nudMiscPosition.Value = profile.MiscStartPosition;
                    nudMiscStart.Value = 0;
                    nudPicNumberPosition.Value = profile.SequenceStartPosition;
                    nudPicNumberStart.Value = 0;
                }
                else
                {
                    nudMaplotAccountPosition.Value = 0;
                    nudMaplotAccountStart.Value = profile.MaplotAccountStartPosition;
                    nudCardPosition.Value = 0;
                    nudCardStart.Value = profile.CardStartPosition;
                    nudMiscPosition.Value = 0;
                    nudMiscStart.Value = profile.MiscStartPosition;
                    nudPicNumberPosition.Value = 0;
                    nudPicNumberStart.Value = profile.SequenceStartPosition;
                }
                
                if (Directory.Exists(profile.SourceDirectory))
                {
                    txtSourceDirectory.Text = profile.SourceDirectory;
                }
                
                loading = false;
            }
            ReshowOptions();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            BrowseForDirectory();
        }

        private void BrowseForDirectory()
        {
            var fsd = new FolderSelectDialog();
            fsd.Title = "Select the folder the pictures are in";
            if (!string.IsNullOrWhiteSpace(txtSourceDirectory.Text))
            {
                if (Directory.Exists(txtSourceDirectory.Text))
                {
                    fsd.InitialDirectory = txtSourceDirectory.Text;
                }
            }

            if (fsd.ShowDialog(IntPtr.Zero))
            {
                txtSourceDirectory.Text = fsd.FileName;
            }
        }

        private void SetupIdentifierCombo()
        {
            cmbIdentifierType.Items.Clear();
            cmbIdentifierType.Items.Add("Map Lot");
            cmbIdentifierType.Items.Add("Account");
            cmbIdentifierType.SelectedIndex = 0;
        }

        private void SetupSeparatorCombo()
        {
            cmbSeparator.Items.Clear();
            cmbSeparator.Items.Add("-");
            cmbSeparator.Items.Add("_");
            cmbSeparator.Items.Add(".");
            cmbSeparator.SelectedIndex = 0;
        }
        private void SetupFormatCombo()
        {
            cmbFormat.Items.Clear();
            cmbFormat.Items.Add("Use Separators");
            cmbFormat.Items.Add("Fixed Length");
            cmbFormat.SelectedIndex = 0;
        }

        private void ReshowOptions()
        {
            if (loading)
            {
                return;
            }

            switch (cmbSeparator.Text)
            {
                case "-":
                    lblSeparatorType.Text = "Dash";
                    break;
                case "_":
                    lblSeparatorType.Text = "Underscore";
                    break;
                case ".":
                    lblSeparatorType.Text = "Dot";
                    break;
                default:
                    lblSeparatorType.Text = "";
                    break;
            }
            var useSeparators = UseSeparators();
            if (useSeparators)
            {
                tblLayoutMapLot.ColumnStyles[2].Width = 0;
                nudMaplotAccountStart.TabStop = false;
                tblLayoutCard.ColumnStyles[2].Width = 0;
                nudCardStart.TabStop = false;
                tblLayoutMiscData.ColumnStyles[2].Width = 0;
                nudMiscStart.TabStop = false;
                nudPicNumberStart.TabStop = false;
                tblLayoutPictureNumber.ColumnStyles[2].Width = 0;
                tableLayoutHeader.ColumnStyles[2].Width = 0;
                nudMaplotAccountPosition.TabStop = true;
                nudMiscPosition.TabStop = true;
                nudPicNumberPosition.TabStop = true;
                nudCardPosition.TabStop = true;
                tblLayoutMapLot.ColumnStyles[1].Width = 50;
                tblLayoutCard.ColumnStyles[1].Width = 50;
                tblLayoutMiscData.ColumnStyles[1].Width = 50;
                tblLayoutPictureNumber.ColumnStyles[1].Width = 50;
                tableLayoutHeader.ColumnStyles[1].Width = 50;
            }
            else
            {
                nudMaplotAccountPosition.TabStop = false;
                nudMiscPosition.TabStop = false;
                nudPicNumberPosition.TabStop = false;
                nudCardPosition.TabStop = false;

                nudMaplotAccountStart.TabStop = true;
                nudCardStart.TabStop = true;
                nudMiscStart.TabStop = true;
                nudPicNumberStart.TabStop = true;
                tblLayoutMapLot.ColumnStyles[1].Width = 0;
                tblLayoutCard.ColumnStyles[1].Width = 0;
                tblLayoutMiscData.ColumnStyles[1].Width = 0;
                tblLayoutPictureNumber.ColumnStyles[1].Width = 0;
                tableLayoutHeader.ColumnStyles[1].Width = 0;
                tblLayoutMapLot.ColumnStyles[2].Width = 50;
                tblLayoutCard.ColumnStyles[2].Width = 50;
                tblLayoutMiscData.ColumnStyles[2].Width = 50;
                tblLayoutPictureNumber.ColumnStyles[2].Width = 50;
                tableLayoutHeader.ColumnStyles[2].Width = 50;
            }

            if (MoreThanOneItemInFilename())
            {
                panelPosition.Visible = true;
            }
            else
            {
                panelPosition.Visible = false;
            }

            if (cmbIdentifierType.Text == "Account")
            {
                lblMaplotAccount.Text = "Account";
            }
            else
            {
                lblMaplotAccount.Text = "Map Lot";
            }
            if (chkMiscData.Checked)
            {
                tblLayoutMiscData.Visible = true;
            }
            else
            {
                tblLayoutMiscData.Visible = false;
            }

            if (chkCardNumber.Checked)
            {
                tblLayoutCard.Visible = true;
            }
            else
            {
                tblLayoutCard.Visible = false;
            }

            if (chkIncludesPictureNumber.Checked)
            {
                tblLayoutPictureNumber.Visible = true;
            }
            else
            {
                tblLayoutPictureNumber.Visible = false;
            }
        }

        private bool MoreThanOneItemInFilename()
        {
            if (chkIncludesPictureNumber.Checked || chkCardNumber.Checked || chkMiscData.Checked)
            {
                return true;
            }

            return false;
        }

        private string GetTokenUrl()
        {
            return currentBaseUrl + @"/token";
        }

        private bool RenewToken()
        { //TODO: Make this actually renew the token rather than reauthenticate
            return Authenticate();
        }
        private bool Authenticate()
        {
            try
            {
                var response = GetAuthorizationToken(GetTokenUrl(), currentUser, currentPassword);
                if (response != null)
                {
                    currentAuthToken = response.access_token;
                    currentRefreshToken = response.refresh_token;
                    return !string.IsNullOrWhiteSpace(currentAuthToken);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }

            return false;
        }
        private TokenResponseObject GetAuthorizationToken(string tokenUrl,string userName, string password)
        {
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("grant_type", "password")
            });
            content.Headers.ContentType =
                new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            var authToken = "";

            var tokenResponse = httpClient.PostAsync(tokenUrl, content).Result;           
            var bearerData = tokenResponse.Content.ReadAsStringAsync().Result;
            return  JsonSerializer.Deserialize<TokenResponseObject>(bearerData);
        }

        private void SendImage(string authToken)
        {
            var httpClient = new HttpClient();
            
        }

        private void LoadFiles()
        {

            var parsedFiles = ParseFileNames(txtSourceDirectory.Text,chkSubdirectories.Checked,0);
            if (!parsedFiles.Any())
            {
                MessageBox.Show("No files matching were found", "No Files");
                return;
            }

            GetAccounts();
            if (!propertyList.Any())
            {
                MessageBox.Show("No properties were found", "No Properties");
                return;
            }

            var badFiles = new List<FileItem>();

            fileItemBindingSource.Clear();
            //// var fileList = ParseFileNames(txtSourceDirectory.Text, chkSubdirectories.Checked, 0);
            fileItemBindingSource.RaiseListChangedEvents = true;
            var itemCount = 0;
            foreach (var matchResult in MatchFiles(parsedFiles))
            {
                if (matchResult.matched)
                {
                    fileItemBindingSource.Add(matchResult.fileItem);
                    itemCount++;
                }
                else
                {
                    badFiles.Add(matchResult.fileItem);
                }
                
                if (itemCount >= 10)
                {
                    GridPreview.Refresh();
                    itemCount = 0;
                }
            }
        }



        private void GetAccounts()
        {
            if (!propertyList.Any())
            {
                var dataEnvironment = txtDataEnvironment.Text;
                var imageUploadPath = @"/api/" + dataEnvironment + @"/RealProperty";
                var uriBuilder = new UriBuilder(currentBaseUrl);
                uriBuilder.Path = imageUploadPath;
                var imageUploadUrl = uriBuilder.Uri.AbsoluteUri;
                MessageBox.Show("Connecting to " + imageUploadUrl);
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + currentAuthToken);


                    var queryItem = new RealPropertyQueryItem()
                    {
                        IncludedDeleted = false,
                        OrderAscending = true,
                        OrderBy = RealPropertyOrderByType.Account.ToInteger(),
                        RangeType = RealPropertyRangeType.None.ToInteger(),
                        RangeBy = RealPropertyRangeByType.Account.ToInteger(),
                        RangeStart = "",
                        RangeEnd = ""
                    };
                    string jsonString;
                    jsonString = JsonSerializer.Serialize(queryItem);
                    var postContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
                    try
                    {
                        var result =
                            httpClient.PostAsync(imageUploadUrl, postContent).Result; //.EnsureSuccessStatusCode();

                        if (!result.IsSuccessStatusCode)
                        {
                            if (result.StatusCode == HttpStatusCode.Unauthorized)
                            {
                                MessageBox.Show("You do not have permission to view accounts", "Unauthorized");
                                return;
                            }
                            else
                            {
                                MessageBox.Show(result.ReasonPhrase, result.StatusCode.ToString());
                            }
                        }
                        else
                        {
                            
                            var jsonResult = result.Content.ReadAsStringAsync().Result;
                            var returnedList = JsonSerializer.Deserialize<IEnumerable<RealProperty>>(jsonResult);
                            propertyList.AddRange(returnedList);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }

                }
            }
        }

        private IEnumerable<(bool matched, FileItem fileItem)> MatchFiles(IEnumerable<FileItem> fileItems)
        {
            foreach(var fileItem in fileItems)
            {
                RealProperty reProperty = null;
                if (fileItem.Account > 0)
                {
                    reProperty = propertyList.Where(p => p.Account == fileItem.Account).FirstOrDefault();
                }
                else
                {
                    reProperty = propertyList.Where(p => p.ParcelIdentifier == fileItem.MapLot).FirstOrDefault();
                }
                if (reProperty != null)
                {
                    yield return (matched:  true, fileItem:  new FileItem()
                    { Account = reProperty.Account,
                      Card = fileItem.Card,
                      Filename = fileItem.Filename,
                      Name = reProperty.Name,
                      MapLot = reProperty.ParcelIdentifier
                    });
                }
                else
                {
                    yield return (matched: false, fileItem);
                }
            }

        }

        private IEnumerable<FileItem> FileTest()
        {
            for (int x = 0; x < 100; x ++)
            {
                System.Threading.Thread.Sleep(100);
                yield return new FileItem() {
                    Account = x,
                    MapLot = "100-200-100",
                    Card = 1,
                    Filename = x.ToString() + ".jpg",
                     Name = "Name " + x.ToString()
                };
            }
        }

        private bool ConnectToServer()
        {
            if (string.IsNullOrWhiteSpace(txtServer.Text))
            {
                MessageBox.Show("You must specify a TRIO Web site address", "Missing address", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtDataEnvironment.Text))
            {
                MessageBox.Show("You must specify a data environment", "Missing data environment", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return false;
            }
            SaveProfile();
            var serverUrl = txtServer.Text;
            try
            {
                currentBaseUrl = new Uri(serverUrl, UriKind.Absolute).AbsoluteUri;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Bad address", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return false;
            }

            if (string.IsNullOrWhiteSpace(currentAuthToken) || string.IsNullOrWhiteSpace(currentUser) ||
                string.IsNullOrWhiteSpace(currentPassword))
            {
                if (!Login())
                {
                    MessageBox.Show("Unable to authenticate", "Not authenticated", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return false;
                }
            }
            return true;
        }

        private void UploadPictures()
        {
            //if (string.IsNullOrWhiteSpace(txtServer.Text))
            //{
            //    MessageBox.Show("You must specify a TRIO Web site address", "Missing address", MessageBoxButtons.OK,
            //        MessageBoxIcon.Asterisk);
            //    return;
            //}

            //if (string.IsNullOrWhiteSpace(txtDataEnvironment.Text))
            //{
            //    MessageBox.Show("You must specify a data environment", "Missing data environment", MessageBoxButtons.OK,
            //        MessageBoxIcon.Asterisk);
            //    return;
            //}
            //SaveProfile();
            //var serverUrl = txtServer.Text;
            
            //try
            //{
            //    currentBaseUrl = new Uri(serverUrl, UriKind.Absolute).AbsoluteUri;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Bad address", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            //    return;
            //}

            //if (string.IsNullOrWhiteSpace(currentAuthToken) || string.IsNullOrWhiteSpace(currentUser) ||
            //    string.IsNullOrWhiteSpace(currentPassword))
            //{
            //    if (!Login())
            //    {
            //        MessageBox.Show("Unable to authenticate", "Not authenticated", MessageBoxButtons.OK,
            //            MessageBoxIcon.Error);
            //        return;
            //    }
            //}


            var dataEnvironment = txtDataEnvironment.Text;
            var imageUploadPath = GetImageUploadPath(dataEnvironment);
            var uriBuilder = new UriBuilder(currentBaseUrl);
            uriBuilder.Path = imageUploadPath;
            var imageUploadUrl = uriBuilder.Uri.AbsoluteUri;
            var upperLimit = 0;
            var errorCount = 0;
            var errorLimit = 3;
            var authorizationLimit = 3;
            var authorizationCount = 0;
            var fileList = (IEnumerable<FileItem>)fileItemBindingSource.List;
            uploadCancelled = false;
            if (fileList.Any())
            {
                using (var httpClient = new HttpClient())
                {
                    try
                    {

                        panelProgress.Visible = true;
                        panelProgress.Refresh();
                        Application.DoEvents();
                        var fileCount = fileList.Count();
                        int fileNumber = 0;
                        httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + currentAuthToken);
                        int percentageCounter = 0;
                        foreach (var fileItem in fileList)
                        {
                            if (uploadCancelled)
                            {
                                return;
                            }
                            fileNumber++;
                            percentageCounter++;
                            var counterPercentage = (Convert.ToDecimal(percentageCounter) / fileCount) * 100;
                            if (counterPercentage > 1) // only update about every 1 percent.  For 1000 files this would be every 10 files sent
                            {
                                percentageCounter = 0;
                                var filePercentage = Convert.ToInt32( Convert.ToDecimal(fileNumber) / fileCount * 100);
                                if (filePercentage > 100)
                                {
                                    filePercentage = 100;
                                }
                                progressBar1.Value = filePercentage;
                                Application.DoEvents();
                            }

                            var imageItem = new RealEstateImageFileImport();
                            imageItem.Account = fileItem.Account;
                            imageItem.Card = fileItem.Card;
                            imageItem.MediaType = "image/jpeg";
                            imageItem.DataEnvironment = dataEnvironment;
                            imageItem.ImageData = GetImageData(fileItem.Filename);
                            imageItem.Filename = Path.GetFileName(fileItem.Filename);
                            string jsonString;
                            jsonString = JsonSerializer.Serialize(imageItem);


                            var postContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
                            try
                            {
                                var result =
                                    httpClient.PostAsync(imageUploadUrl, postContent)
                                        .Result; //.EnsureSuccessStatusCode();
                                if (!result.IsSuccessStatusCode)
                                {
                                    if (result.StatusCode == HttpStatusCode.Unauthorized)
                                    {
                                        if (authorizationCount >= authorizationLimit)
                                        {
                                            MessageBox.Show("Too many reauthorization attempts. Cannot continue",
                                                "Upload stopped", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return;
                                        }

                                        if (RenewToken())
                                        {
                                            authorizationCount++;
                                            httpClient.DefaultRequestHeaders.Remove("Authorization");
                                            httpClient.DefaultRequestHeaders.Add("Authorization",
                                                "Bearer " + currentAuthToken);
                                            result =
                                                httpClient.PostAsync(imageUploadUrl, postContent)
                                                    .Result; //.EnsureSuccessStatusCode();
                                            if (!result.IsSuccessStatusCode)
                                            {
                                                errorCount++;
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Unable to get authorization to continue", "Unauthorized",
                                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        errorCount++;
                                    }

                                    if (errorCount > errorLimit)
                                    {
                                        MessageBox.Show("Too many errors occurred. Cannot continue", "Errors",
                                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                errorCount++;
                            }

                        }
                    }
                    catch
                    {

                    }
                    finally
                    {
                        panelProgress.Visible = false;
                    }
                }

                MessageBox.Show("Images uploaded", "Uploaded", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("No valid files found", "No files", MessageBoxButtons.OK);
            }
        }

        private bool Login()
        {
            var loginForm = new frmLogin();
            if (loginForm.ShowDialog() == DialogResult.OK)
            {
                currentUser = loginForm.UserName;
                currentPassword = loginForm.Password;
                
                var success = Authenticate();
                if (success)
                {
                    panelUpload.Enabled = true;
                    panelUpload.Visible = true;
                    btnLogout.Enabled = true;
                    btnLogin.Enabled = false;
                }
                else
                {
                    panelUpload.Enabled = false;
                    panelUpload.Visible = false;
                    btnLogout.Enabled = false;
                    btnLogin.Enabled = true;
                }
                return success;
            }

            return false;
        }

        private IEnumerable<FileItem> ParseFileNames(string directory, bool recursive, int upperLimit)
        {
            int parseCount = 0;
            var imageList = new List<FileItem>();
            if (!Directory.Exists(directory))
            {
                MessageBox.Show("Could not find directory " + directory, "Path not found", MessageBoxButtons.OK);
                return imageList;
            }

            var hasCardNumber = chkCardNumber.Checked;
            var hasPictureNumber = chkIncludesPictureNumber.Checked;
            var hasMisc = chkMiscData.Checked;
            var useSeparators = cmbFormat.Text == "Use Separators";
            var hasMapLot = cmbIdentifierType.SelectedIndex == 0;
            var separatorCharacter = cmbSeparator.Text;
            var numPieces = 1;
            int cardPosition = 0;
            int mapLotAccountPosition = 0;
            int sequencePosition = 0;
            int miscPosition = 0;
            int mapLotAccountEnd = 0;
            int cardEnd = 0;
            int sequenceEnd = 0;
            int miscEnd = 0;

            if (hasCardNumber)
            {
                numPieces++;
            }

            if (hasPictureNumber)
            {
                numPieces++;
            }

            if (hasMisc)
            {
                numPieces++;
            }

            if (numPieces > 1)
            {
                if (useSeparators)
                {
                    mapLotAccountPosition = nudMaplotAccountPosition.Value.ToInteger();
                }
                else
                {
                    mapLotAccountPosition = nudMaplotAccountStart.Value.ToInteger();
                }

                if (mapLotAccountPosition == 0)
                {
                    if (hasMapLot)
                    {
                        MessageBox.Show("Invalid map lot position", "Invalid setup", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);                        
                    }
                    else
                    {
                        MessageBox.Show("Invalid account position", "Invalid setup", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                    }

                    return imageList;
                }

                if (hasCardNumber)
                {
                    if (useSeparators)
                    {
                        cardPosition = nudCardPosition.Value.ToInteger();
                    }
                    else
                    {
                        cardPosition = nudCardStart.Value.ToInteger();
                    }

                    if (cardPosition == 0)
                    {
                        MessageBox.Show("Invalid card position", "Invalid setup", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        return imageList;
                    }

                    if (cardPosition == mapLotAccountPosition)
                    {
                        MessageBox.Show("Multiple items have the same positions", "Invalid setup", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        return imageList;
                    }
                }

                if (hasPictureNumber)
                {
                    if (useSeparators)
                    {
                        sequencePosition = nudPicNumberPosition.Value.ToInteger();
                    }
                    else
                    {
                        sequencePosition = nudPicNumberStart.Value.ToInteger();
                    }

                    if (sequencePosition == 0)
                    {
                        MessageBox.Show("Invalid picture number position", "Invalid setup", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        return imageList;
                    }

                    if (sequencePosition == mapLotAccountPosition || (sequencePosition == cardPosition && hasCardNumber))
                    {
                        MessageBox.Show("Multiple items have the same positions", "Invalid setup", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        return imageList;
                    }
                }

                if (hasMisc)
                {
                    if (useSeparators)
                    {
                        miscPosition = nudMiscPosition.Value.ToInteger();
                    }
                    else
                    {
                        miscPosition = nudMiscStart.Value.ToInteger();
                    }

                    if (miscPosition == 0)
                    {
                        MessageBox.Show("Invalid misc. position", "Invalid setup", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        return imageList;
                    }

                    if (miscPosition == mapLotAccountPosition || (hasCardNumber && miscPosition == cardPosition) ||
                        (hasPictureNumber && miscPosition == sequencePosition))
                    {
                        MessageBox.Show("Invalid misc. position", "Invalid setup", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        return imageList;
                    }
                }

                if (!useSeparators)
                {
                    if (cardPosition > mapLotAccountPosition)
                    {
                        mapLotAccountEnd = cardPosition - 1;
                    }

                    if (sequencePosition > mapLotAccountPosition && (sequencePosition < mapLotAccountEnd || mapLotAccountEnd == 0))
                    {
                        mapLotAccountEnd = sequencePosition - 1;
                    }

                    if (miscPosition > mapLotAccountPosition && (miscPosition < mapLotAccountEnd || mapLotAccountEnd == 0))
                    {
                        mapLotAccountEnd = miscPosition - 1;
                    }

                    if (hasCardNumber)
                    {
                        if (mapLotAccountPosition > cardPosition)
                        {
                            cardEnd = mapLotAccountPosition - 1;
                        }

                        if (sequencePosition > cardPosition && (sequencePosition < cardEnd || cardEnd == 0))
                        {
                            cardEnd = sequencePosition - 1;
                        }

                        if (miscPosition > cardPosition && (miscPosition < cardEnd || cardEnd == 0))
                        {
                            cardEnd = miscPosition - 1;
                        }
                    }

                    if (hasPictureNumber)
                    {
                        if (mapLotAccountPosition > sequencePosition)
                        {
                            sequenceEnd = mapLotAccountPosition - 1;
                        }

                        if (cardPosition > sequencePosition && (cardPosition < sequenceEnd || sequenceEnd == 0))
                        {
                            sequenceEnd = cardPosition - 1;
                        }

                        if (miscPosition > sequencePosition && (miscPosition < sequenceEnd || sequenceEnd == 0))
                        {
                            sequenceEnd = miscPosition - 1;
                        }
                    }

                    if (hasMisc)
                    {
                        if (mapLotAccountPosition > miscPosition)
                        {
                            miscEnd = mapLotAccountPosition - 1;
                        }

                        if (cardPosition > miscPosition && (cardPosition < miscEnd || miscEnd == 0))
                        {
                            miscEnd = cardPosition - 1;
                        }

                        if (sequencePosition > miscPosition && (sequencePosition < miscEnd || miscEnd == 0))
                        {
                            miscEnd = sequencePosition - 1;
                        }
                    }
                }
            }

           
            var fileList = GetFileList(directory);
            foreach (var filename in fileList)
            {
                
                var baseName = Path.GetFileName(filename);
                List<string> splitName;
                if (IsImageType(baseName))
                {
                    var importItem = new FileItem();
                    //importItem.MediaType = "image/jpeg";
                    importItem.Filename = filename;
                    
                    var nameOnly = Path.GetFileNameWithoutExtension(filename);
                    if (numPieces == 1)
                    {                        
                        splitName = new List<string>() {nameOnly};
                    }
                    else if (useSeparators)
                    {
                        splitName = nameOnly.Split(separatorCharacter.ToCharArray()).ToList();                        
                    }
                    else
                    {
                        var tempName = "";
                        splitName = new List<string>();
                        if (nameOnly.Length > mapLotAccountEnd)
                        {
                            if (mapLotAccountEnd > 0)
                            {
                                tempName = nameOnly.Substring(mapLotAccountPosition - 1,
                                    mapLotAccountEnd - mapLotAccountPosition + 1);
                            }
                            else
                            {
                                tempName = nameOnly.Substring(mapLotAccountPosition - 1);
                            }
                            if (hasMapLot)
                            {
                                importItem.MapLot = tempName;
                            }
                            else
                            {
                                importItem.Account = tempName.ToIntegerValue();
                            }
                        }

                        if (hasCardNumber)
                        {
                            if (nameOnly.Length > cardEnd)
                            {
                                if (cardEnd > 0)
                                {
                                    importItem.Card = nameOnly.Substring(cardPosition - 1, cardEnd - cardPosition + 1)
                                        .ToIntegerValue();
                                }
                                else
                                {
                                    importItem.Card = nameOnly.Substring(cardPosition - 1).ToIntegerValue();
                                }
                            }
                        }
                    }

                    if (numPieces == 1)
                    {
                        if (hasMapLot)
                        {
                            importItem.MapLot = splitName.FirstOrDefault();
                        }
                        else
                        {
                            importItem.Account = splitName.FirstOrDefault().ToIntegerValue();
                        }
                    }
                    else if (useSeparators)
                    {
                        if (splitName.Count >= mapLotAccountPosition)
                        {
                            if (hasMapLot)
                            {
                                importItem.MapLot = splitName[mapLotAccountPosition-1];
                            }
                            else
                            {
                                importItem.Account = splitName[mapLotAccountPosition-1].ToIntegerValue();
                            }
                        }

                        if (hasCardNumber &&  splitName.Count >= cardPosition)
                        {
                            importItem.Card = splitName[cardPosition - 1].ToIntegerValue();
                        }
                        
                    }
                    

                    if (!string.IsNullOrWhiteSpace(importItem.MapLot) || importItem.Account > 0)
                    {
                        if (!hasCardNumber)
                        {
                            importItem.Card = 1;
                        }
                        if (importItem.Card > 0)
                        {
                            imageList.Add(importItem);
                            parseCount++;
                        }
                    }

                    if (parseCount >= upperLimit && upperLimit > 0)
                    {
                        break;
                    }
                }
            }

            return imageList;
        }


      
        private bool IsImageType(string filename)
        {
            var extension = Path.GetExtension(filename);
            switch (extension.ToLower())
            {
                case ".jpeg":
                case ".jpg":
                case ".gif":
                case ".png":
                case ".bmp":
                case ".tif":
                    return true;               
            }

            return false;
        }
        private IEnumerable<string> GetFileList(string directory)
        {
            var fileList = new List<string>();
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(directory);
            while (queue.Count > 0)
            {
                var path = queue.Dequeue();
                try
                {
                    foreach (var subDirectory in Directory.GetDirectories(path))
                    {
                        queue.Enqueue(subDirectory);
                    }
                }
                catch (Exception ex)
                {

                }

                try
                {
                    fileList.AddRange(Directory.GetFiles(path));
                }
                catch (Exception ex)
                {

                }
            }

            return fileList;
        }

        //private void button2_Click(object sender, EventArgs e)
        //{
           

        //    var tokenUrl = "http://localhost:55236/token";
        //    var userName = "Admin";
        //    var password = "trio2001";
            
        //    var content = new FormUrlEncodedContent(new[]
        //    {
        //        new KeyValuePair<string, string>("username", userName),
        //        new KeyValuePair<string, string>("password", password),
        //        new KeyValuePair<string, string>("grant_type", "password")
        //    });
            
        //    content.Headers.ContentType =
        //        new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");
        //    var woogy = new HttpRequestMessage(HttpMethod.Get, tokenUrl);
        //    woogy.Content = content;


        //    var url = "http://localhost:55236/api/Bucksport/AddImageFileToProperty";
        //    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
        //    httpWebRequest.ContentType = "application/json; charset=utf-8";
        //    httpWebRequest.Method = "POST";
           
        //    var httpClient = new HttpClient();
        //    var authToken = "";

        //    var tokenResponse = httpClient.PostAsync(tokenUrl, content).Result;
            
        //    var bearerData = tokenResponse.Content.ReadAsStringAsync().Result;
        //    var accessToken = JsonSerializer.Deserialize<TokenResponseObject>(bearerData);
        //    authToken = accessToken.access_token;

        //    var imageData = GetImageData(@"d:\download.jpg");
        //    var testObj = new RealEstateImageFileImport();
        //    testObj.Account = 5;
        //    testObj.DataEnvironment = "Bucksport";
        //    testObj.Filename = @"d:\download.jpg";
        //    testObj.ImageData = imageData;
        //    testObj.MediaType = @"image/jpeg";
        //    string jsonString;
        //    jsonString = JsonSerializer.Serialize(testObj);

        //    httpClient.DefaultRequestHeaders.Add("Authorization","Bearer " + authToken);
        //    var postContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
        //    try
        //    {
        //        var result = httpClient.PostAsync(url, postContent).Result; //.EnsureSuccessStatusCode();
        //        if (!result.IsSuccessStatusCode)
        //        {

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

           
            
        //    //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //    //{
        //    //    streamWriter.Write(jsonString);
        //    //    streamWriter.Flush();
        //    //    streamWriter.Close();
        //    //}

        //    //try
        //    //{
        //    //    using (var response = httpWebRequest.GetResponse() as HttpWebResponse)
        //    //    {
        //    //        if (httpWebRequest.HaveResponse && response != null)
        //    //        {
        //    //            var result = response.StatusCode;
                        
        //    //        }
        //    //    }
        //    //}
        //    //catch (WebException ex)
        //    //{
        //    //    if (ex.Response != null)
        //    //    {
        //    //        using (var errorResponse = (HttpWebResponse)ex.Response)
        //    //        {
        //    //            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
        //    //            {
        //    //                string error = reader.ReadToEnd();
                            
        //    //            }
        //    //        }

        //    //    }
        //    //}
        //}

        private string GetImageUploadPath(string dataEnvironment)
        {
            return @"/api/" + dataEnvironment + @"/AddImageFileToProperty";
        }

        private string GetAccountDownloadPath(string dataEnvironment)
        {
            return @"/api/" + dataEnvironment + @"/RealProperty";
        }

        private byte[] GetImageData(string fileName)
        {
            if (!File.Exists(fileName))
            {
                return new byte[] { };
            }

            return ResizeImageAsJpeg(File.ReadAllBytes(fileName), maxImageWidth, maxImageHeight);
        }

        public static byte[] ResizeImageAsJpeg(byte[] imageData, int maxWidthInPixels, int maxHeightInPixels)
        {
            using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
            {
                using (Image img = Image.FromStream(ms))
                {
                    int newWidth = img.Width;
                    int newHeight = img.Height;
                    if (img.Width > maxWidthInPixels || img.Height > maxHeightInPixels)
                    {
                        var ratio = Convert.ToDouble(img.Width) / Convert.ToDouble(img.Height);
                        newWidth = maxWidthInPixels;
                        newHeight = maxHeightInPixels;

                        if (maxWidthInPixels / ratio > maxHeightInPixels)
                        {
                            newWidth = (int)(maxHeightInPixels * ratio);
                        }
                        else
                        {
                            newHeight = (int)(maxWidthInPixels / ratio);
                        }
                    }

                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    ImageCodecInfo imageCodecInfo = ImageCodecInfo.GetImageEncoders().FirstOrDefault(e => e.MimeType == "image/jpeg");
                    System.Drawing.Imaging.Encoder imageEncoder = System.Drawing.Imaging.Encoder.Quality;
                    EncoderParameter encoderParameter = new EncoderParameter(imageEncoder, 90L);
                    encoderParameters.Param[0] = encoderParameter;
                    using (Bitmap b = new Bitmap(img, new Size(newWidth, newHeight)))
                    {
                        using (MemoryStream destinationStream = new MemoryStream())
                        {
                            b.Save(destinationStream, imageCodecInfo, encoderParameters);
                            return destinationStream.ToArray();
                        }
                    }
                }
            }
        }


    }

    public class TokenResponseObject
    {
        public string access_token { get; set; } = "";
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string username { get; set; }
        public string roles { get; set; }

        public DateTime issued { get; set; }
        public DateTime expires { get; set; }
    }

    public static class UtilityExtensions
    {
        public static Int32 ToIntegerValue(this string sourceString)
        {
            if (Int32.TryParse(sourceString.Replace(",", ""), out var result))
            {
                return result;
            }

            return 0;
        }

        public static int ToInteger(this decimal amount)
        {
            return Convert.ToInt32(amount);
        }

        public static bool ToBooleanValue(this string sourceString)
        {
            if (bool.TryParse(sourceString, out var result))
            {
                return result;
            }

            return false;
        }
    }
}
