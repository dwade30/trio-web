﻿namespace AutoImportREPictures
{
    partial class frmAutoImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutoImport));
            this.cmbIdentifierType = new System.Windows.Forms.ComboBox();
            this.cmbFormat = new System.Windows.Forms.ComboBox();
            this.chkIncludesPictureNumber = new System.Windows.Forms.CheckBox();
            this.chkMiscData = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelPosition = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutHeader = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tblLayoutMapLot = new System.Windows.Forms.TableLayoutPanel();
            this.nudMaplotAccountStart = new System.Windows.Forms.NumericUpDown();
            this.lblMaplotAccount = new System.Windows.Forms.Label();
            this.nudMaplotAccountPosition = new System.Windows.Forms.NumericUpDown();
            this.tblLayoutCard = new System.Windows.Forms.TableLayoutPanel();
            this.nudCardStart = new System.Windows.Forms.NumericUpDown();
            this.nudCardPosition = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.tblLayoutPictureNumber = new System.Windows.Forms.TableLayoutPanel();
            this.nudPicNumberStart = new System.Windows.Forms.NumericUpDown();
            this.nudPicNumberPosition = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.tblLayoutMiscData = new System.Windows.Forms.TableLayoutPanel();
            this.nudMiscStart = new System.Windows.Forms.NumericUpDown();
            this.nudMiscPosition = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbSeparator = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSourceDirectory = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chkCardNumber = new System.Windows.Forms.CheckBox();
            this.GridPreview = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnUpload = new System.Windows.Forms.Button();
            this.cmbProfile = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.chkSubdirectories = new System.Windows.Forms.CheckBox();
            this.lblSeparatorType = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDataEnvironment = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSaveProfile = new System.Windows.Forms.Button();
            this.btnAddProfile = new System.Windows.Forms.Button();
            this.btnLoadFiles = new System.Windows.Forms.Button();
            this.panelUpload = new System.Windows.Forms.Panel();
            this.panelProgress = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panelPosition.SuspendLayout();
            this.tableLayoutHeader.SuspendLayout();
            this.tblLayoutMapLot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaplotAccountStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaplotAccountPosition)).BeginInit();
            this.tblLayoutCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCardStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCardPosition)).BeginInit();
            this.tblLayoutPictureNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPicNumberStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPicNumberPosition)).BeginInit();
            this.tblLayoutMiscData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMiscStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMiscPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileItemBindingSource)).BeginInit();
            this.panelUpload.SuspendLayout();
            this.panelProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbIdentifierType
            // 
            this.cmbIdentifierType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIdentifierType.FormattingEnabled = true;
            this.cmbIdentifierType.Location = new System.Drawing.Point(23, 99);
            this.cmbIdentifierType.Name = "cmbIdentifierType";
            this.cmbIdentifierType.Size = new System.Drawing.Size(219, 26);
            this.cmbIdentifierType.TabIndex = 6;
            // 
            // cmbFormat
            // 
            this.cmbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFormat.FormattingEnabled = true;
            this.cmbFormat.Location = new System.Drawing.Point(23, 263);
            this.cmbFormat.Name = "cmbFormat";
            this.cmbFormat.Size = new System.Drawing.Size(219, 26);
            this.cmbFormat.TabIndex = 10;
            // 
            // chkIncludesPictureNumber
            // 
            this.chkIncludesPictureNumber.AutoSize = true;
            this.chkIncludesPictureNumber.Location = new System.Drawing.Point(23, 167);
            this.chkIncludesPictureNumber.Name = "chkIncludesPictureNumber";
            this.chkIncludesPictureNumber.Size = new System.Drawing.Size(195, 22);
            this.chkIncludesPictureNumber.TabIndex = 8;
            this.chkIncludesPictureNumber.Text = "Includes Picture Number";
            this.chkIncludesPictureNumber.UseVisualStyleBackColor = true;
            // 
            // chkMiscData
            // 
            this.chkMiscData.AutoSize = true;
            this.chkMiscData.Location = new System.Drawing.Point(23, 195);
            this.chkMiscData.Name = "chkMiscData";
            this.chkMiscData.Size = new System.Drawing.Size(162, 22);
            this.chkMiscData.TabIndex = 9;
            this.chkMiscData.Text = "Includes Misc. Data";
            this.chkMiscData.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Format";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Match By";
            // 
            // panelPosition
            // 
            this.panelPosition.Controls.Add(this.tableLayoutHeader);
            this.panelPosition.Controls.Add(this.tblLayoutMapLot);
            this.panelPosition.Controls.Add(this.tblLayoutCard);
            this.panelPosition.Controls.Add(this.tblLayoutPictureNumber);
            this.panelPosition.Controls.Add(this.tblLayoutMiscData);
            this.panelPosition.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.panelPosition.Location = new System.Drawing.Point(258, 99);
            this.panelPosition.Name = "panelPosition";
            this.panelPosition.Size = new System.Drawing.Size(302, 194);
            this.panelPosition.TabIndex = 12;
            // 
            // tableLayoutHeader
            // 
            this.tableLayoutHeader.ColumnCount = 3;
            this.tableLayoutHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutHeader.Controls.Add(this.label3, 1, 0);
            this.tableLayoutHeader.Controls.Add(this.label4, 2, 0);
            this.tableLayoutHeader.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutHeader.Name = "tableLayoutHeader";
            this.tableLayoutHeader.RowCount = 1;
            this.tableLayoutHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutHeader.Size = new System.Drawing.Size(292, 31);
            this.tableLayoutHeader.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(136, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 31);
            this.label3.TabIndex = 0;
            this.label3.Text = "Position";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(215, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 31);
            this.label4.TabIndex = 1;
            this.label4.Text = "Start";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tblLayoutMapLot
            // 
            this.tblLayoutMapLot.ColumnCount = 3;
            this.tblLayoutMapLot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tblLayoutMapLot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutMapLot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutMapLot.Controls.Add(this.nudMaplotAccountStart, 2, 0);
            this.tblLayoutMapLot.Controls.Add(this.lblMaplotAccount, 0, 0);
            this.tblLayoutMapLot.Controls.Add(this.nudMaplotAccountPosition, 1, 0);
            this.tblLayoutMapLot.Location = new System.Drawing.Point(3, 40);
            this.tblLayoutMapLot.Name = "tblLayoutMapLot";
            this.tblLayoutMapLot.RowCount = 1;
            this.tblLayoutMapLot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutMapLot.Size = new System.Drawing.Size(292, 31);
            this.tblLayoutMapLot.TabIndex = 12;
            // 
            // nudMaplotAccountStart
            // 
            this.nudMaplotAccountStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMaplotAccountStart.Location = new System.Drawing.Point(215, 3);
            this.nudMaplotAccountStart.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudMaplotAccountStart.Name = "nudMaplotAccountStart";
            this.nudMaplotAccountStart.Size = new System.Drawing.Size(74, 26);
            this.nudMaplotAccountStart.TabIndex = 14;
            // 
            // lblMaplotAccount
            // 
            this.lblMaplotAccount.AutoSize = true;
            this.lblMaplotAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMaplotAccount.Location = new System.Drawing.Point(3, 0);
            this.lblMaplotAccount.Name = "lblMaplotAccount";
            this.lblMaplotAccount.Size = new System.Drawing.Size(126, 31);
            this.lblMaplotAccount.TabIndex = 1;
            this.lblMaplotAccount.Text = "Map Lot";
            this.lblMaplotAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nudMaplotAccountPosition
            // 
            this.nudMaplotAccountPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMaplotAccountPosition.Location = new System.Drawing.Point(135, 3);
            this.nudMaplotAccountPosition.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudMaplotAccountPosition.Name = "nudMaplotAccountPosition";
            this.nudMaplotAccountPosition.Size = new System.Drawing.Size(74, 26);
            this.nudMaplotAccountPosition.TabIndex = 13;
            // 
            // tblLayoutCard
            // 
            this.tblLayoutCard.ColumnCount = 3;
            this.tblLayoutCard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tblLayoutCard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutCard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutCard.Controls.Add(this.nudCardStart, 0, 0);
            this.tblLayoutCard.Controls.Add(this.nudCardPosition, 0, 0);
            this.tblLayoutCard.Controls.Add(this.label6, 0, 0);
            this.tblLayoutCard.Location = new System.Drawing.Point(3, 77);
            this.tblLayoutCard.Name = "tblLayoutCard";
            this.tblLayoutCard.RowCount = 1;
            this.tblLayoutCard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutCard.Size = new System.Drawing.Size(292, 31);
            this.tblLayoutCard.TabIndex = 15;
            // 
            // nudCardStart
            // 
            this.nudCardStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudCardStart.Location = new System.Drawing.Point(215, 3);
            this.nudCardStart.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudCardStart.Name = "nudCardStart";
            this.nudCardStart.Size = new System.Drawing.Size(74, 26);
            this.nudCardStart.TabIndex = 17;
            // 
            // nudCardPosition
            // 
            this.nudCardPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudCardPosition.Location = new System.Drawing.Point(135, 3);
            this.nudCardPosition.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudCardPosition.Name = "nudCardPosition";
            this.nudCardPosition.Size = new System.Drawing.Size(74, 26);
            this.nudCardPosition.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 31);
            this.label6.TabIndex = 2;
            this.label6.Text = "Card";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tblLayoutPictureNumber
            // 
            this.tblLayoutPictureNumber.ColumnCount = 3;
            this.tblLayoutPictureNumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tblLayoutPictureNumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutPictureNumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutPictureNumber.Controls.Add(this.nudPicNumberStart, 0, 0);
            this.tblLayoutPictureNumber.Controls.Add(this.nudPicNumberPosition, 0, 0);
            this.tblLayoutPictureNumber.Controls.Add(this.label7, 0, 0);
            this.tblLayoutPictureNumber.Location = new System.Drawing.Point(3, 114);
            this.tblLayoutPictureNumber.Name = "tblLayoutPictureNumber";
            this.tblLayoutPictureNumber.RowCount = 1;
            this.tblLayoutPictureNumber.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutPictureNumber.Size = new System.Drawing.Size(292, 31);
            this.tblLayoutPictureNumber.TabIndex = 18;
            // 
            // nudPicNumberStart
            // 
            this.nudPicNumberStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudPicNumberStart.Location = new System.Drawing.Point(215, 3);
            this.nudPicNumberStart.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudPicNumberStart.Name = "nudPicNumberStart";
            this.nudPicNumberStart.Size = new System.Drawing.Size(74, 26);
            this.nudPicNumberStart.TabIndex = 20;
            // 
            // nudPicNumberPosition
            // 
            this.nudPicNumberPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudPicNumberPosition.Location = new System.Drawing.Point(135, 3);
            this.nudPicNumberPosition.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudPicNumberPosition.Name = "nudPicNumberPosition";
            this.nudPicNumberPosition.Size = new System.Drawing.Size(74, 26);
            this.nudPicNumberPosition.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 31);
            this.label7.TabIndex = 2;
            this.label7.Text = "Picture Number";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tblLayoutMiscData
            // 
            this.tblLayoutMiscData.ColumnCount = 3;
            this.tblLayoutMiscData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tblLayoutMiscData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutMiscData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayoutMiscData.Controls.Add(this.nudMiscStart, 0, 0);
            this.tblLayoutMiscData.Controls.Add(this.nudMiscPosition, 0, 0);
            this.tblLayoutMiscData.Controls.Add(this.label8, 0, 0);
            this.tblLayoutMiscData.Location = new System.Drawing.Point(3, 151);
            this.tblLayoutMiscData.Name = "tblLayoutMiscData";
            this.tblLayoutMiscData.RowCount = 1;
            this.tblLayoutMiscData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutMiscData.Size = new System.Drawing.Size(292, 31);
            this.tblLayoutMiscData.TabIndex = 21;
            // 
            // nudMiscStart
            // 
            this.nudMiscStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMiscStart.Location = new System.Drawing.Point(215, 3);
            this.nudMiscStart.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudMiscStart.Name = "nudMiscStart";
            this.nudMiscStart.Size = new System.Drawing.Size(74, 26);
            this.nudMiscStart.TabIndex = 23;
            // 
            // nudMiscPosition
            // 
            this.nudMiscPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMiscPosition.Location = new System.Drawing.Point(136, 3);
            this.nudMiscPosition.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudMiscPosition.Name = "nudMiscPosition";
            this.nudMiscPosition.Size = new System.Drawing.Size(73, 26);
            this.nudMiscPosition.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 31);
            this.label8.TabIndex = 2;
            this.label8.Text = "Misc. Data";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 304);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 18);
            this.label9.TabIndex = 9;
            this.label9.Text = "Separator Character";
            // 
            // cmbSeparator
            // 
            this.cmbSeparator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeparator.FormattingEnabled = true;
            this.cmbSeparator.Location = new System.Drawing.Point(23, 329);
            this.cmbSeparator.Name = "cmbSeparator";
            this.cmbSeparator.Size = new System.Drawing.Size(65, 26);
            this.cmbSeparator.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 380);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(125, 18);
            this.label10.TabIndex = 10;
            this.label10.Text = "Source Directory";
            // 
            // txtSourceDirectory
            // 
            this.txtSourceDirectory.Location = new System.Drawing.Point(23, 410);
            this.txtSourceDirectory.Name = "txtSourceDirectory";
            this.txtSourceDirectory.Size = new System.Drawing.Size(361, 26);
            this.txtSourceDirectory.TabIndex = 24;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(400, 407);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 30);
            this.button1.TabIndex = 25;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkCardNumber
            // 
            this.chkCardNumber.AutoSize = true;
            this.chkCardNumber.Location = new System.Drawing.Point(23, 140);
            this.chkCardNumber.Name = "chkCardNumber";
            this.chkCardNumber.Size = new System.Drawing.Size(181, 22);
            this.chkCardNumber.TabIndex = 7;
            this.chkCardNumber.Text = "Includes Card Number";
            this.chkCardNumber.UseVisualStyleBackColor = true;
            // 
            // GridPreview
            // 
            this.GridPreview.AllowUserToAddRows = false;
            this.GridPreview.AllowUserToDeleteRows = false;
            this.GridPreview.AllowUserToResizeColumns = false;
            this.GridPreview.AllowUserToResizeRows = false;
            this.GridPreview.AutoGenerateColumns = false;
            this.GridPreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridPreview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.GridPreview.DataSource = this.fileItemBindingSource;
            this.GridPreview.Location = new System.Drawing.Point(11, 15);
            this.GridPreview.MultiSelect = false;
            this.GridPreview.Name = "GridPreview";
            this.GridPreview.ReadOnly = true;
            this.GridPreview.RowHeadersVisible = false;
            this.GridPreview.RowHeadersWidth = 51;
            this.GridPreview.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GridPreview.Size = new System.Drawing.Size(834, 99);
            this.GridPreview.TabIndex = 14;
            this.GridPreview.TabStop = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Filename";
            this.dataGridViewTextBoxColumn1.HeaderText = "Filename";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "MapLot";
            this.dataGridViewTextBoxColumn2.HeaderText = "MapLot";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Account";
            this.dataGridViewTextBoxColumn3.HeaderText = "Account";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 85;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Card";
            this.dataGridViewTextBoxColumn4.HeaderText = "Card";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 50;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn5.HeaderText = "Name";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 280;
            // 
            // fileItemBindingSource
            // 
            this.fileItemBindingSource.DataSource = typeof(AutoImportREPictures.FileItem);
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(385, 131);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(91, 37);
            this.btnUpload.TabIndex = 30;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            // 
            // cmbProfile
            // 
            this.cmbProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProfile.FormattingEnabled = true;
            this.cmbProfile.Location = new System.Drawing.Point(23, 36);
            this.cmbProfile.Name = "cmbProfile";
            this.cmbProfile.Size = new System.Drawing.Size(219, 26);
            this.cmbProfile.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "Profile";
            // 
            // btnLogout
            // 
            this.btnLogout.Enabled = false;
            this.btnLogout.Location = new System.Drawing.Point(882, 69);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(91, 29);
            this.btnLogout.TabIndex = 32;
            this.btnLogout.Text = "Log out";
            this.toolTip1.SetToolTip(this.btnLogout, "Parse");
            this.btnLogout.UseVisualStyleBackColor = true;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(882, 34);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(91, 29);
            this.btnLogin.TabIndex = 31;
            this.btnLogin.Text = "Log in";
            this.toolTip1.SetToolTip(this.btnLogin, "Parse");
            this.btnLogin.UseVisualStyleBackColor = true;
            // 
            // chkSubdirectories
            // 
            this.chkSubdirectories.AutoSize = true;
            this.chkSubdirectories.Location = new System.Drawing.Point(23, 446);
            this.chkSubdirectories.Name = "chkSubdirectories";
            this.chkSubdirectories.Size = new System.Drawing.Size(197, 22);
            this.chkSubdirectories.TabIndex = 26;
            this.chkSubdirectories.Text = "Include all subdirectories";
            this.chkSubdirectories.UseVisualStyleBackColor = true;
            // 
            // lblSeparatorType
            // 
            this.lblSeparatorType.AutoSize = true;
            this.lblSeparatorType.Location = new System.Drawing.Point(114, 332);
            this.lblSeparatorType.Name = "lblSeparatorType";
            this.lblSeparatorType.Size = new System.Drawing.Size(0, 18);
            this.lblSeparatorType.TabIndex = 21;
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(258, 36);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(212, 26);
            this.txtServer.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(255, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 18);
            this.label5.TabIndex = 22;
            this.label5.Text = "TRIO Site Address";
            // 
            // txtDataEnvironment
            // 
            this.txtDataEnvironment.Location = new System.Drawing.Point(488, 36);
            this.txtDataEnvironment.Name = "txtDataEnvironment";
            this.txtDataEnvironment.Size = new System.Drawing.Size(159, 26);
            this.txtDataEnvironment.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(485, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 18);
            this.label12.TabIndex = 24;
            this.label12.Text = "Data Environment";
            // 
            // btnSaveProfile
            // 
            this.btnSaveProfile.Location = new System.Drawing.Point(666, 69);
            this.btnSaveProfile.Name = "btnSaveProfile";
            this.btnSaveProfile.Size = new System.Drawing.Size(127, 29);
            this.btnSaveProfile.TabIndex = 5;
            this.btnSaveProfile.Text = "Save Profile";
            this.btnSaveProfile.UseVisualStyleBackColor = true;
            // 
            // btnAddProfile
            // 
            this.btnAddProfile.Location = new System.Drawing.Point(666, 34);
            this.btnAddProfile.Name = "btnAddProfile";
            this.btnAddProfile.Size = new System.Drawing.Size(127, 29);
            this.btnAddProfile.TabIndex = 4;
            this.btnAddProfile.Text = "Add Profile";
            this.btnAddProfile.UseVisualStyleBackColor = true;
            // 
            // btnLoadFiles
            // 
            this.btnLoadFiles.Location = new System.Drawing.Point(851, 46);
            this.btnLoadFiles.Name = "btnLoadFiles";
            this.btnLoadFiles.Size = new System.Drawing.Size(125, 37);
            this.btnLoadFiles.TabIndex = 29;
            this.btnLoadFiles.Text = "Load Files";
            this.btnLoadFiles.UseVisualStyleBackColor = true;
            // 
            // panelUpload
            // 
            this.panelUpload.Controls.Add(this.GridPreview);
            this.panelUpload.Controls.Add(this.btnLoadFiles);
            this.panelUpload.Controls.Add(this.btnUpload);
            this.panelUpload.Enabled = false;
            this.panelUpload.Location = new System.Drawing.Point(12, 474);
            this.panelUpload.Name = "panelUpload";
            this.panelUpload.Size = new System.Drawing.Size(994, 189);
            this.panelUpload.TabIndex = 32;
            // 
            // panelProgress
            // 
            this.panelProgress.BackColor = System.Drawing.Color.White;
            this.panelProgress.Controls.Add(this.btnCancel);
            this.panelProgress.Controls.Add(this.label13);
            this.panelProgress.Controls.Add(this.progressBar1);
            this.panelProgress.Location = new System.Drawing.Point(319, 329);
            this.panelProgress.Name = "panelProgress";
            this.panelProgress.Size = new System.Drawing.Size(328, 131);
            this.panelProgress.TabIndex = 33;
            this.panelProgress.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(120, 92);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(91, 27);
            this.btnCancel.TabIndex = 31;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(106, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(117, 18);
            this.label13.TabIndex = 1;
            this.label13.Text = "Uploading Files";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(21, 44);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(288, 25);
            this.progressBar1.TabIndex = 0;
            // 
            // frmAutoImport
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1011, 665);
            this.Controls.Add(this.panelProgress);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.panelUpload);
            this.Controls.Add(this.btnAddProfile);
            this.Controls.Add(this.btnSaveProfile);
            this.Controls.Add(this.txtDataEnvironment);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtServer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblSeparatorType);
            this.Controls.Add(this.chkSubdirectories);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cmbProfile);
            this.Controls.Add(this.chkCardNumber);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtSourceDirectory);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cmbSeparator);
            this.Controls.Add(this.panelPosition);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkMiscData);
            this.Controls.Add(this.chkIncludesPictureNumber);
            this.Controls.Add(this.cmbFormat);
            this.Controls.Add(this.cmbIdentifierType);
            this.Font = new System.Drawing.Font("Arial", 12F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAutoImport";
            this.Text = "Import Pictures";
            this.Load += new System.EventHandler(this.frmAutoImport_Load);
            this.panelPosition.ResumeLayout(false);
            this.tableLayoutHeader.ResumeLayout(false);
            this.tableLayoutHeader.PerformLayout();
            this.tblLayoutMapLot.ResumeLayout(false);
            this.tblLayoutMapLot.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaplotAccountStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaplotAccountPosition)).EndInit();
            this.tblLayoutCard.ResumeLayout(false);
            this.tblLayoutCard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCardStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCardPosition)).EndInit();
            this.tblLayoutPictureNumber.ResumeLayout(false);
            this.tblLayoutPictureNumber.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPicNumberStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPicNumberPosition)).EndInit();
            this.tblLayoutMiscData.ResumeLayout(false);
            this.tblLayoutMiscData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMiscStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMiscPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileItemBindingSource)).EndInit();
            this.panelUpload.ResumeLayout(false);
            this.panelProgress.ResumeLayout(false);
            this.panelProgress.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbIdentifierType;
        private System.Windows.Forms.ComboBox cmbFormat;
        private System.Windows.Forms.CheckBox chkIncludesPictureNumber;
        private System.Windows.Forms.CheckBox chkMiscData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel panelPosition;
        private System.Windows.Forms.TableLayoutPanel tableLayoutHeader;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tblLayoutMapLot;
        private System.Windows.Forms.Label lblMaplotAccount;
        private System.Windows.Forms.TableLayoutPanel tblLayoutCard;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tblLayoutPictureNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tblLayoutMiscData;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbSeparator;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSourceDirectory;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkCardNumber;
        private System.Windows.Forms.DataGridView GridPreview;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cmbProfile;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkSubdirectories;
        private System.Windows.Forms.NumericUpDown nudMaplotAccountStart;
        private System.Windows.Forms.NumericUpDown nudMaplotAccountPosition;
        private System.Windows.Forms.NumericUpDown nudCardStart;
        private System.Windows.Forms.NumericUpDown nudCardPosition;
        private System.Windows.Forms.NumericUpDown nudPicNumberStart;
        private System.Windows.Forms.NumericUpDown nudPicNumberPosition;
        private System.Windows.Forms.NumericUpDown nudMiscStart;
        private System.Windows.Forms.NumericUpDown nudMiscPosition;
        private System.Windows.Forms.Label lblSeparatorType;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDataEnvironment;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnSaveProfile;
        private System.Windows.Forms.Button btnAddProfile;
        private System.Windows.Forms.Button btnLoadFiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn filenameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mapLotDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cardDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource fileItemBindingSource;
        private System.Windows.Forms.Panel panelUpload;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Panel panelProgress;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    }
}

