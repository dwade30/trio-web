﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoImportREPictures
{
    public class FileItem
    {
        public string Filename { get; set; } = "";
        public string MapLot { get; set; } = "";
        public int Account { get; set; } = 0;
        public int Card { get; set; } = 1;
        public string Name { get; set; } = "";
    }
}
