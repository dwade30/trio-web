﻿using System.Drawing;
using System.Runtime.Remoting.Channels;
using System.Windows.Forms;

namespace AutoImportREPictures
{
    public static class PromptDialog
    {
        public static string ShowDialog(string labelText, string caption)
        {
            Form prompt = new Form()
            {
                Width = 300,
                Height = 180,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen,
                Font = new Font("Arial",12),
                AutoScaleMode = AutoScaleMode.None
            };
            Label promptLabel = new Label(){Left = 40, Top = 20, Text = labelText};
            TextBox inputBox = new TextBox(){Left = 40, Top = 50, Width = 200};
            Button okButton = new Button(){Text = "OK", Left = 170, Width = 100, Height = 35,Top = 90,DialogResult = DialogResult.OK};
            okButton.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(inputBox);
            prompt.Controls.Add(okButton);
            prompt.Controls.Add(promptLabel);
            prompt.AcceptButton = okButton;
            return prompt.ShowDialog() == DialogResult.OK ? inputBox.Text : "";
        }
    }
}