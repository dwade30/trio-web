﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoImportREPictures
{
    public class RealProperty 
    {
        public int Account { get; set; } = 0;
        public string ParcelIdentifier { get; set; } = "";
        public int StreetNumber { get; set; } = 0;
        public string StreetName { get; set; } = "";
        public string Name { get; set; } = "";
        public Guid AccountIdentifier { get; set; } = Guid.Empty;
    }
}