﻿using System.ComponentModel;
using System.Linq;

namespace AutoImportREPictures
{
    public class Settings
    {
        public Settings()
        {
            Profiles = new BindingList<ImportProfile>();
        }

        public BindingList<ImportProfile> Profiles { get; set; } 
        public Settings Copy()
        {
            Settings s = new Settings();

            foreach (ImportProfile profile in Profiles)
            {
                s.Profiles.Add(profile.Copy());
            }
            return s;
        }

        public bool Equals(Settings s)
        {
            bool r;
            r = s.Profiles.Count == Profiles.Count;
            foreach (ImportProfile profile in s.Profiles)
            {
                r = r && (Profiles.Any(x => x.Equals(profile)));
            }

            foreach (ImportProfile profile in Profiles)
            {
                r = r && (s.Profiles.Any(x => x.Equals(profile)));
            }

            return r;
            
        }

        public void Clear()
        {
            Profiles.Clear();
        }
    }
}