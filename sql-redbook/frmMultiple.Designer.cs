﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRB0000
{
	/// <summary>
	/// Summary description for frmMultiple.
	/// </summary>
	partial class frmMultiple
	{
		public FCGrid vs1;
		public fecherFoundation.FCButton cmdBack;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblMfg;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileBack;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vs1 = new fecherFoundation.FCGrid();
            this.cmdBack = new fecherFoundation.FCButton();
            this.lblYear = new fecherFoundation.FCLabel();
            this.lblMfg = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBack = new fecherFoundation.FCToolStripMenuItem();
            this.lblLoading = new Wisej.Web.Label();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBack)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdBack);
            this.BottomPanel.Location = new System.Drawing.Point(0, 516);
            this.BottomPanel.Size = new System.Drawing.Size(1040, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.ClientArea.Controls.Add(this.lblLoading);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.lblYear);
            this.ClientArea.Controls.Add(this.lblMfg);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(1060, 624);
            this.ClientArea.Controls.SetChildIndex(this.lblMfg, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblLoading, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1060, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(277, 28);
            this.HeaderText.Text = "Multiple Vehicle Selection";
            // 
            // vs1
            // 
            this.vs1.Cols = 7;
            this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vs1.ExtendLastCol = true;
            this.vs1.FixedCols = 0;
            this.vs1.Location = new System.Drawing.Point(30, 86);
            this.vs1.Name = "vs1";
            this.vs1.RowHeadersVisible = false;
            this.vs1.Rows = 1;
            this.vs1.Size = new System.Drawing.Size(982, 430);
            this.vs1.TabIndex = 3;
            // 
            // cmdBack
            // 
            this.cmdBack.AppearanceKey = "acceptButton";
            this.cmdBack.Location = new System.Drawing.Point(308, 36);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(208, 48);
            this.cmdBack.TabIndex = 0;
            this.cmdBack.Text = "Back to Input Screen";
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(30, 30);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(35, 21);
            this.lblYear.TabIndex = 2;
            // 
            // lblMfg
            // 
            this.lblMfg.Location = new System.Drawing.Point(85, 30);
            this.lblMfg.Name = "lblMfg";
            this.lblMfg.Size = new System.Drawing.Size(153, 36);
            this.lblMfg.TabIndex = 1;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileBack});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileBack
            // 
            this.mnuFileBack.Index = 0;
            this.mnuFileBack.Name = "mnuFileBack";
            this.mnuFileBack.Text = "Back to Input Screen";
            this.mnuFileBack.Click += new System.EventHandler(this.mnuFileBack_Click);
            // 
            // lblLoading
            // 
            this.lblLoading.Location = new System.Drawing.Point(329, 56);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(613, 24);
            this.lblLoading.TabIndex = 1;
            this.lblLoading.Text = "Loading";
            this.lblLoading.Visible = false;
            // 
            // frmMultiple
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1060, 684);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmMultiple";
            this.Text = "Multiple Vehicle Selection";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmMultiple_Load);
            this.Activated += new System.EventHandler(this.frmMultiple_Activated);
            this.Resize += new System.EventHandler(this.frmMultiple_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMultiple_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMultiple_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBack)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private Label lblLoading;
    }
}
