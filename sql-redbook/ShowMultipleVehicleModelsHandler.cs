﻿using SharedApplication.Messaging;
using TWRB0000.commands;

namespace TWRB0000
{
    public class ShowMultipleVehicleModelsHandler : CommandHandler<ShowMultipleVehicleModels>
    {
        protected override void Handle(ShowMultipleVehicleModels command)
        {
            frmMultiple.InstancePtr.Show();
            frmMultiple.InstancePtr.vs1.Focus();
        }
    }
}