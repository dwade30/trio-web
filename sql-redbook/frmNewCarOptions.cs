//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using System.Runtime.ExceptionServices;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.OptionPrices;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWRB0000
{
	public partial class frmNewCarOptions : BaseForm
    {
        public VehicleConfigurationSpecs CurrentConfiguration { get; set; } = new VehicleConfigurationSpecs();
        public VehicleConfigurationOptions CurrentConfigurationOptions { get; set; } = new VehicleConfigurationOptions();
        public bool KeepData { get; set; } = false;
        public frmNewCarOptions()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
        }


        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmNewCarOptions InstancePtr
		{
			get
			{
				return (frmNewCarOptions)Sys.GetInstance(typeof(frmNewCarOptions));
			}
		}

		protected frmNewCarOptions _InstancePtr = null;
       // int[,] OptionPrices = new int[100 + 1, 2 + 1];
		// vbPorter upgrade warning: OldOptionYear As int	OnWriteFCConvert.ToInt32(
		int OldOptionYear;
		public bool OptionFormLoaded;
		string strSQL = "";
		int intBrakesOptionIndex;
		int intWheelsOptionIndex;
		// vbPorter upgrade warning: curEquipmentPackageCost As Decimal	OnWrite	OnReadFCConvert.ToInt32(
		Decimal curEquipmentPackageCost;
        private IBlueBookRepository blueBookRepository;


		private void CalculateTotal()
		{
            modRedBook.Statics.TotalCost = FCConvert.ToInt32(curEquipmentPackageCost);
            foreach (TableLayoutPanel panel  in  OptionsPanel.Controls )
            {
                var metaData = panel.Tag as OptionMetaData;
                var equipped = panel.Controls.FirstOrDefault(c => c.Tag as string == "Equipped") as FCCheckBox;
                var standard = panel.Controls.FirstOrDefault(c => c.Tag as string == "Standard") as FCCheckBox;
                if (metaData != null)
                {
                    if (equipped != null && equipped.Checked)
                    {
                        if (standard != null && !standard.Checked)
                        {
                            if (metaData.IsBrakeOption)
                            {
                                var brakeBox =
                                    panel.Controls.FirstOrDefault(c => c.Tag as string == "Brake Value") as FCTextBox;
                                if (brakeBox != null)
                                {
                                    modRedBook.Statics.TotalCost += (metaData.Price * brakeBox.Text.ToDecimalValue()).ToDouble();
                                }
                            }
                            else if (metaData.IsWheelOption)
                            {
                                var wheelBox = panel.Controls.FirstOrDefault(c => c.Tag == "Wheel Value") as FCTextBox;
                                if (wheelBox != null)
                                {
                                    modRedBook.Statics.TotalCost +=
                                        (metaData.Price * wheelBox.Text.ToDecimalValue()).ToDouble();
                                }
                            }
                            else
                            {
                                modRedBook.Statics.TotalCost += metaData.Price.ToDouble();
                            }
                        }
                    }
                }
            }

			lblTotalCost.Text = Strings.Format(modRedBook.Statics.TotalCost, "currency");
		}



		public void cmdClear_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: fnx As int	OnWriteFCConvert.ToInt32(
			int fnx;
			cboOptions.SelectedIndex = -1;
			fnx = 1;

			modRedBook.Statics.TotalCost = 0;
			modRedBook.Statics.CostOfEachWheel = 0;
			modRedBook.Statics.TotalCostOfWheels = 0;
			modRedBook.Statics.CostOfEachBrake = 0;
			modRedBook.Statics.TotalCostOfBrakes = 0;
			curEquipmentPackageCost = 0;
			lblTotalCost.Text = Strings.Format(modRedBook.Statics.TotalCost, "CURRENCY");
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Mid(lblTotalCost.Text, 2, lblTotalCost.Text.Length)) != 0)
			{
				modRedBook.Statics.TotalCost = FCConvert.ToInt32(double.Parse(FCConvert.ToString(FCConvert.ToDouble(lblTotalCost.Text)), System.Globalization.NumberStyles.Currency));
			}
			OptionFormLoaded = false;
            frmFinal.InstancePtr.CameFrom = "Options";
            frmFinal.InstancePtr.Show();
			InstancePtr.Hide();
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
			OptionFormLoaded = false;
			
			if (modRedBook.Statics.FromMultiple == true)
			{
				InstancePtr.Hide();
				frmMultiple.InstancePtr.Show();
			}
			else
			{
				InstancePtr.Hide();
				frmRedbook.InstancePtr.Show();
			}
		}

		public void Command1_Click()
		{
			Command1_Click(Command1, new System.EventArgs());
		}

		private void frmNewCarOptions_Activated(object sender, System.EventArgs e)
		{
			modGlobalRoutines.ForceFormToResize(this);
			int counter;
            if (!KeepData)
            {
                intBrakesOptionIndex = -1;
                intWheelsOptionIndex = -1;
                FCGlobal.Screen.MousePointer = 0;
                lblVehicleModel.Text = "";
                lblVehicleMake.Text = "";
                FillVehicleInfo();
                FillTheOptions();
                modRedBook.Statics.TotalOptionCost = 0;
                KeepData = false;
            }
        }

        public void FillTheOptions()
        {
            int fx;
            OptionsPanel.Controls.Clear();
            //var configurationOptions = blueBookRepository.Options.GetOptions(
            //    new OptionFilterCriteria(CurrentConfiguration.SizeClassId, CurrentConfiguration.ModelYear));
            var configurationOptions = CurrentConfigurationOptions;
            if (configurationOptions != null)
            {

                var optionPrices = configurationOptions.Options;

                if (optionPrices.Any())
                {
                    foreach (var optionPrice in optionPrices)
                    {
                        //switch (optionPrice.FamilyName.ToLower())
                        //{
                        //        case "transmission":
                        //        case "engine":
                        //        case "rear axle":
                        //        case "front axle":
                        //            continue;
                        //    break;
                        //}
                        var changeToIndeterminateStates = false;
                        var changeToUncheckedStates = false;
                        var metaData = new OptionMetaData();
                        metaData.Description = optionPrice.Name;
                        metaData.Price = optionPrice.MSRP;
                        if (modRedBook.Statics.VehicleType == "H")
                        {
                            switch (optionPrice.Name)
                            {
                                case "Power Steering":
                                    changeToIndeterminateStates = true;
                                    break;
                                case "Aluminum disc wheels (each)":
                                    metaData.IsBrakeOption = true;
                                    break;
                                case "New matched radials (each)":
                                    metaData.IsWheelOption = true;
                                    break;
                            }
                        }
                        else if (modRedBook.Statics.VehicleType == "C" || modRedBook.Statics.VehicleType == "T")
                        {
                            if ((Strings.Mid(modRedBook.Statics.VBrakeType, 1, 1) == "A") ||
                                (Strings.Mid(modRedBook.Statics.VBrakeType, 1, 1) == "T"))
                            {
                                switch (optionPrice.Name)
                                {
                                    case "4-WHEEL ANTI-LOCK BRAKE SYSTEM":
                                        changeToIndeterminateStates = true;
                                        break;
                                }

                                if (Strings.Mid(modRedBook.Statics.VBrakeType, 2, 1) ==
                                    FCConvert.ToString(Convert.ToChar(34)))
                                {
                                    if (optionPrice.Name == "4-WHEEL DISK BRAKES")
                                    {
                                        changeToIndeterminateStates = true;
                                    }
                                }
                            }
                            else if ((Strings.Mid(modRedBook.Statics.VBrakeType, 1, 1) == "A") ||
                                     (Strings.Mid(modRedBook.Statics.VBrakeType, 1, 1) == "T") ||
                                     (Strings.Mid(modRedBook.Statics.VBrakeType, 1, 1) == "P"))
                            {
                                if (optionPrice.Name == "Power Brakes (if not standard)")
                                {
                                    changeToIndeterminateStates = true;
                                }
                            }

                            if ((modRedBook.Statics.VAirType == "N") || (modRedBook.Statics.VAirType == "O"))
                            {
                                if (optionPrice.Name == "Air Conditioning")
                                {
                                    changeToUncheckedStates = true;
                                }
                            }
                        }

                        var panel = GetOptionControl(metaData);
                        var standardCheck = panel.Controls.FirstOrDefault(c => (string) c.Tag == "Standard") as FCCheckBox;
                        var equippedCheck = panel.Controls.FirstOrDefault(c => (string) c.Tag == "Equipped") as FCCheckBox;
                        OptionsPanel.Controls.Add(panel);
                        if (optionPrice.IsStandard)
                        {
                            changeToIndeterminateStates = true;
                        }

                        if (changeToIndeterminateStates)
                        {
                            if (standardCheck != null)
                            {
                                standardCheck.CheckState = CheckState.Checked;
                                standardCheck.ReadOnly = true;
                            }

                            if (equippedCheck != null)
                            {
                                equippedCheck.CheckState = CheckState.Checked;
                                equippedCheck.ReadOnly = true;
                                equippedCheck.ForeColor = Color.Blue;
                            }
                        }

                        if (changeToUncheckedStates)
                        {
                            if (standardCheck != null)
                            {
                                standardCheck.CheckState = CheckState.Unchecked;
                            }

                            if (equippedCheck != null)
                            {
                                equippedCheck.CheckState = CheckState.Unchecked;
                                equippedCheck.ForeColor = Color.Blue;
                            }
                        }
                    }
                }
            }

            OptionFormLoaded = true;
		}



        private TableLayoutPanel GetOptionControl(OptionMetaData metaData)
        {
            var panel = new TableLayoutPanel();
            panel.Margin = new Padding(1);
            panel.BorderStyle = BorderStyle.None;
            panel.ColumnCount = 4;
            panel.RowCount = 1;
            panel.AutoSize = true;
            panel.Dock = DockStyle.Left & DockStyle.Right;
            panel.Padding = new Padding(1);
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 40));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 40));
			panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20));
            var checkStandard = new FCCheckBox()
            {
                AutoSize = false, Dock = DockStyle.Fill, Margin = Padding.Empty,
                CheckAlign = ContentAlignment.MiddleCenter, Tag = "Standard"
            };
            checkStandard.CheckedChanged += CheckStandard_CheckedChanged;
			panel.Controls.Add(checkStandard);
            var checkEquipped = new FCCheckBox()
            {
                AutoSize = false, Dock = DockStyle.Fill, Margin = Padding.Empty,
                CheckAlign = ContentAlignment.MiddleCenter, Tag = "Equipped",
				ForeColor = Color.Black
            };
            checkEquipped.CheckedChanged += CheckEquipped_CheckedChanged;
			panel.Controls.Add(checkEquipped);
			panel.Controls.Add(new Label() { Text = metaData.Description, AutoSize = false, Dock = DockStyle.Fill, Margin = Padding.Empty, TextAlign = ContentAlignment.MiddleLeft,BorderStyle = BorderStyle.None});
            if (metaData.IsBrakeOption)
            {
                var brakeBox = new FCTextBox()
                {
                    Tag = "Brake Value", MaxLength = 1, TextAlign = HorizontalAlignment.Center, Dock = DockStyle.Fill,
                    AutoSize = false
                };

                panel.Controls.Add(brakeBox);
                brakeBox.TextChanged += BrakeBox_TextChanged;
            }
			else if (metaData.IsWheelOption)
            {
                var wheelBox = new FCTextBox()
                {
                    Tag = "Wheel Value", MaxLength = 1, TextAlign = HorizontalAlignment.Center, Dock = DockStyle.Fill,
                    AutoSize = false
                };

                panel.Controls.Add(wheelBox);
                wheelBox.TextChanged += WheelBox_TextChanged;
            }
            panel.Tag = metaData;
            return panel;
        }

        private void WheelBox_TextChanged(object sender, EventArgs e)
        {
           CalculateTotal();
        }

        private void BrakeBox_TextChanged(object sender, EventArgs e)
        {
            CalculateTotal();
        }

        private void CheckStandard_CheckedChanged(object sender, EventArgs e)
        {
            if (OptionFormLoaded)
            {
                var checkBox = sender as FCCheckBox;
                var panel = checkBox.Parent;
                var equippedBox = panel.Controls.FirstOrDefault(c => c.Tag as string == "Equipped") as FCCheckBox;
                if (checkBox.Checked)
                {
                    equippedBox.Checked = true;
					equippedBox.ForeColor = Color.Blue;
                    
                }
                else
                {
                    equippedBox.ForeColor = Color.Black;
                }
            }
            CalculateTotal();

		}

        private void CheckEquipped_CheckedChanged(object sender, EventArgs e)
        {
			if (OptionFormLoaded == true)
            {
                var checkBox = (FCCheckBox) sender;
				
                var panel = checkBox.Parent;
                var standardCheck = panel.Controls.FirstOrDefault(c => c.Tag as string == "Standard") as FCCheckBox;
                if (panel.Tag is OptionMetaData metaData)
                {
                    if (metaData.IsBrakeOption)
                    {
                        modRedBook.Statics.CostOfEachBrake = metaData.Price.ToInteger();
                        var brakeBox = panel.Controls.FirstOrDefault(c => c.Tag as string == "Brake Value");
                        if (brakeBox != null)
                        {
                            brakeBox.Visible = checkBox.Checked;
                            brakeBox.Text = "0";
                        }
                    }
					else if (metaData.IsWheelOption)
                    {
                        modRedBook.Statics.CostOfEachWheel = metaData.Price.ToInteger();
                        var wheelBox = panel.Controls.FirstOrDefault(c => c.Tag as string == "Wheel Value");
                        if (wheelBox != null)
                        {
                            wheelBox.Visible = checkBox.Checked;
                            wheelBox.Text = "0";
                        }
                    }
                }

                if (standardCheck != null)
                {
                    if (standardCheck.CheckState != CheckState.Unchecked)
                    {
                        if (checkBox.CheckState == CheckState.Unchecked)
                        {
                            checkBox.ForeColor = Color.Red;
                        }
                        else
                        {
                            checkBox.ForeColor = Color.Blue;
                        }
                    }
                }
                CalculateTotal();
            }
		}

        public void FillVehicleInfo()
		{
			lblYear.Text = FCConvert.ToString(modRedBook.Statics.VehicleYear);
            lblVehicleModel.Text = CurrentConfiguration.ModelName;

            if (modRedBook.Statics.VehicleType == "B")
			{
				lblVehicleMake.Text = CurrentConfiguration.ManufacturerName + " " + CurrentConfiguration.Description;
			}
			else
			{
                lblVehicleMake.Text = CurrentConfiguration.ManufacturerName;
            }
        }


		private void frmNewCarOptions_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				modRedBook.RedbookShow();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmNewCarOptions_Load(object sender, System.EventArgs e)
		{
            if (blueBookRepository == null)
            {
                blueBookRepository = StaticSettings.GlobalCommandDispatcher.Send(new GetBlueBookRepository()).Result;
            }
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetCustomFormColors();
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
				modRedBook.RedbookShow();
		}

		private void cboOptions_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            string hold = "";
			if (OptionFormLoaded)
			{
                hold = fecherFoundation.Strings.Trim(cboOptions.Text);
				modRedBook.Statics.VehicleMake = fecherFoundation.Strings.Trim(modRedBook.Statics.VehicleMake);
                var eddies = blueBookRepository.Eddies.GetEddies(new EddieFilterCriteria(modRedBook.Statics.VehicleYear,
                    modRedBook.Statics.VehicleMake, hold));
                if (eddies.Any())
                {
                    var eddie = eddies.FirstOrDefault();
                    curEquipmentPackageCost = eddie.Srp.GetValueOrDefault();
					CalculateTotal();
				}
				else
				{
					curEquipmentPackageCost = 0;
					CalculateTotal();
				}
			}
		}

		private void mnuBack_Click(object sender, System.EventArgs e)
		{
			if (Command1.Enabled)
			{
				Command1_Click();
			}
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			if (cmdClear.Enabled)
			{
				cmdClear_Click();
			}
		}

		private void mnuFileExcise_Click(object sender, System.EventArgs e)
		{
			if (cmdProcess.Enabled)
			{
				cmdProcess_Click();
			}
		}

		private void SetCustomFormColors()
		{
			lblTotalCost.ForeColor = ColorTranslator.FromOle(0x800000);
		}

        private void ClientArea_PanelCollapsed(object sender, EventArgs e)
        {

        }
    }
}
