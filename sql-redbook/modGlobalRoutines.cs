﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRB0000
{
	public class modGlobalRoutines
	{
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmRedbook)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static object FormExist(FCForm FormName)
		{
			object FormExist = null;
			foreach (FCForm frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmRedbook, frmCarOptions, frmMultiple)
		public static object ForceFormToResize(FCForm FormName)
		{
			object ForceFormToResize = null;
			if (FormName.WindowState == FormWindowState.Maximized || FormName.WindowState == FormWindowState.Minimized)
			{
			}
			else
			{
				if (FormName.HeightOriginal > 15)
				{
					FormName.HeightOriginal = FormName.HeightOriginal - 15;
				}
			}
			return ForceFormToResize;
		}

		public class StaticVariables
		{
			//=========================================================
			public bool gboolTruckBodyCombination;
			public bool gboolSaveTruckBody;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
