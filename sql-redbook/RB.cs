//Fecher vbPorter - Version 1.0.0.59
using System;
//using System.ComponentModel;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Autofac;
using BlueBook;
using fecherFoundation.VisualBasicLayer;
using Microsoft.Ajax.Utilities;
using SharedApplication;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Models;
using TWSharedLibrary;

namespace TWRB0000
{
	public class modRedBook
	{
		//=========================================================
		//
		public const string DEFAULTDATABASE = "TWRB0000.vb1";
		// HELP STUFF
		// In a Modules Declarations section
		#if Win32
		[DllImport("user32")]
		public static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

		// For holding the return value, Must be long in 32-Bit
		
#else
		[DllImport("User")]
		public static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

		public int success;
		// For holding the return value, Must be Integer in 16-Bit

		#endif
		const int HELP_TAB = 0xF;
		const int TB_INDEX = -3;

		//[DllImport("user32", EntryPoint = "WinHelpA")]
		//private static extern int WinHelp(int hwnd, string lpHelpFile, int wCommand, int dwData);

		//[DllImport("user32")]
		//public static extern object SetCursorPos(int x, int y);

        public struct rvmanf
		{
			public FCFixedString RMKEY;
			public FCFixedString RMMODEL;
			public FCFixedString rmmanf;
			public FCFixedString RMTYPE;
			public FCFixedString rmstuff;
			//FC:FINAL:DDU:Use custom constructor to initialize structure fields
			public rvmanf(int unusedParam)
			{
				this.RMKEY = new FCFixedString(4);
				this.RMMODEL = new FCFixedString(26);
				this.rmmanf = new FCFixedString(41);
				this.RMTYPE = new FCFixedString(11);
				this.rmstuff = new FCFixedString(46);
			}
		};

		public static void Main(bool addExitMenu ,CommandDispatcher commandDispatcher)
		{
			FCFileSystem ff = new FCFileSystem();
			clsDRWrapper rsVersion = new clsDRWrapper();
            // kk04142015 troges-39  Change to single config file
            if (!FCConvert.ToBoolean(modGlobalFunctions.LoadSQLConfig()))
            {
                MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }

           
			modGlobalFunctions.UpdateUsedModules();
			if (!modGlobalConstants.Statics.gboolRB)
			{
				MessageBox.Show("Blue Book has expired.  Please call TRIO!", "Module Expired", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Application.Exit();
			}
			modGlobalFunctions.GetGlobalRegistrySetting();
			// MsgBox gstrTrioSDrive
			modReplaceWorkFiles.EntryFlagFile();
			
			if (fecherFoundation.Strings.Trim(modGlobalConstants.Statics.gstrEntryFlag) == "XD")
			{
				Statics.blnFromDOS = true;
				Statics.blnFromWindowsCR = false;
				Statics.PDSFlag = true;
				Statics.MVFlag = false;
			}
			else if (fecherFoundation.Strings.Trim(modGlobalConstants.Statics.gstrEntryFlag) == "CR")
			{
				Statics.blnFromWindowsCR = true;
				Statics.blnFromDOS = false;
				Statics.MVFlag = false;
				Statics.PDSFlag = false;
			}
			else if (fecherFoundation.Strings.Trim(modGlobalConstants.Statics.gstrEntryFlag) == "MV")
			{
				Statics.blnFromWindowsCR = false;
				Statics.blnFromDOS = false;
				Statics.MVFlag = true;
				Statics.PDSFlag = false;
                cSettingsController set = new cSettingsController();
				if (set.GetSettingValue("PROCESSREG", "TWGNENTY", "REDBOOK", "Y") != "Y")
				{
					Statics.blnProcessReg = false;
				}
				else
				{
					Statics.blnProcessReg = true;
				}
			}
			else
			{
				Statics.blnFromWindowsCR = false;
				Statics.blnFromDOS = false;
				Statics.MVFlag = false;
				Statics.PDSFlag = false;
			}
            modReplaceWorkFiles.EntryFlagFile(true, "  ");
            modReplaceWorkFiles.GetGlobalVariables();
            GetMuniName();
            MDIParent.InstancePtr.blnFirstTimeIn = true;
            MDIParent.InstancePtr.Init(addExitMenu);
        }



		public static void GetMuniName()
		{
			clsDRWrapper Generalrs = new clsDRWrapper();
			modReplaceWorkFiles.GetGlobalVariables();
			Generalrs.OpenRecordset("SELECT * FROM Modules", "SystemSettings");
			Statics.Auto = false;
			Statics.RV = false;
			Statics.Cycle = false;
			Statics.HeavyTruck = false;
			Statics.TruckBody = false;
			Statics.Trailer = false;
			if (Generalrs.Get_Fields("RB") == true && Generalrs.Get_Fields_DateTime("RBDate").ToOADate() >= DateTime.Today.ToOADate())
			{
                if (FCConvert.ToString(Generalrs.Get_Fields_String("MV_RBLVL_AUTO")) == "Y")
					Statics.Auto = true;
                if (FCConvert.ToString(Generalrs.Get_Fields_String("MV_RBLVL_RCVH")) == "Y")
					Statics.RV = true;
                if (FCConvert.ToString(Generalrs.Get_Fields_String("MV_RBLVL_MTCY")) == "Y")
					Statics.Cycle = true;
                if (FCConvert.ToString(Generalrs.Get_Fields_String("MV_RBLVL_HVTK")) == "Y")
					Statics.HeavyTruck = true;
                if (FCConvert.ToString(Generalrs.Get_Fields_String("MV_RBLVL_TKBD")) == "Y")
					Statics.TruckBody = true;
                if (FCConvert.ToString(Generalrs.Get_Fields_String("MV_RBLVL_TRAL")) == "Y")
					Statics.Trailer = true;
			}
        }

		

        public static Rbcar GetAllRbCarInfo(Rbcar rbCar, string vehicleType)
        {
            modRedBook.Statics.CurrentVehicleObject = rbCar;
            if (rbCar != null)
            {
                Statics.VModel = rbCar.Model;
                Statics.VYear = rbCar.Year;
                Statics.VDescription = rbCar.Description;
                Statics.VEngine = rbCar.Engine;
                Statics.VTransmission = rbCar.Transmission;
                Statics.VAirType = rbCar.AirCondition;
                Statics.VBrakeType = rbCar.BrakeType;
                Statics.VNumberOfPassengers = rbCar.NumberOfPassengers;
                Statics.VSRP = rbCar.SuggestedRetailValue;
                if (vehicleType.ToLower() == "c")
                {
                    Statics.VWheelbase = rbCar.WheelBase;
                }

                Statics.VManufacturer = rbCar.Manufacturer;
			}

            return rbCar;
        }

        public static void GetAllRbCarInfo(VehicleConfigurationSpecs specs)
        {
            modRedBook.Statics.CurrentVehicleObject = specs;
            if (specs != null)
            {
                Statics.VModel = specs.ModelName;
                Statics.VYear = specs.ModelYear.ToString();
                Statics.VDescription = specs.Description;
                Statics.VEngine = specs.GetSpecByName("engineSize");
                Statics.VTransmission = specs.GetSpecByName("transmissionType");
                Statics.VAirType = specs.GetSpecByName("air");
                Statics.VBrakeType = specs.GetSpecByName("brakeType");
                Statics.VNumberOfPassengers = specs.GetSpecByName("passengers");
                Statics.VSRP = specs.GetSpecByName("msrp");
            }
        }

        public static void GetAllTrailerDataInfo(TrailerData trailer)
        {
            Statics.VSRP = trailer.List;
        }

        public static void GetAllTrailerDataInfo(int id)
        {

        }
        public static void GetAllMotorCycleInfo(IBlueBookRepository blueBookRepository,int id)
        {
            var cycle = blueBookRepository.Cycles.GetCycle(id);
            modRedBook.Statics.CurrentVehicleObject = cycle;
            if (cycle != null)
            {
                Statics.VModel = cycle.ModelNumber;
                Statics.VYear = cycle.Year;
                Statics.VDescription = cycle.Description;
                Statics.VEngine = cycle.Cylinders;
                Statics.VTransmission = cycle.Displacement;
                Statics.VNumberOfPassengers = cycle.Weight;
                Statics.VSRP = cycle.Msrp;
                Statics.VManufacturer = cycle.Manufacturer;
            }
        }

        public static void GetAllMotorCycleInfo(VehicleConfigurationSpecs specs)
        {
            modRedBook.Statics.CurrentVehicleObject = specs;
            if (specs != null)
            {
                Statics.VModel = specs.ModelName;
                Statics.VYear = specs.ModelYear.ToString();
                Statics.VDescription = specs.Description;
                Statics.VEngine = specs.GetSpecByName("cylinders");
                Statics.VTransmission = specs.GetSpecByName("displacement");
                Statics.VNumberOfPassengers = specs.GetSpecByName("weight");
                Statics.VSRP = specs.GetSpecByName("msrp");
                Statics.VManufacturer = specs.ManufacturerName;
            }
        }

        public static void GetAllHeavyTruckInfo(IBlueBookRepository blueBookRepository,int id)
        {
            var heavyTruck = blueBookRepository.HeavyTrucks.GetHeavyTruck(id);
            modRedBook.Statics.CurrentVehicleObject = heavyTruck;
            if (heavyTruck != null)
            {
                Statics.VModel = heavyTruck.ModelNumber;
				Statics.VManufacturer = heavyTruck.Make;
				Statics.VYear = heavyTruck.Year;
				Statics.VehicleYear = heavyTruck.Year.ToIntegerValue();
				Statics.VDescription = heavyTruck.Description;
				Statics.VEngineMake = heavyTruck.EngineMake;
				Statics.VEngineDescription = heavyTruck.EngineDescription;
				Statics.VAxle1Make = heavyTruck.Axle1Make;
				Statics.VAxle1Description = heavyTruck.Axle1Description;
				Statics.VTransmissionDescription = heavyTruck.TransmissionDescription;
				Statics.VTransmissionMake = heavyTruck.TransmissionMake;
				Statics.VGVW = heavyTruck.GrossVehicleWeight;
				Statics.VWheelbase = heavyTruck.Wheelbase;
				Statics.VAxle2Make = heavyTruck.Axle2Make;
				Statics.VAxle2Description = heavyTruck.Axle2Description;
				Statics.VNetWeight = heavyTruck.NetWeight;
				Statics.VRetailPrice = heavyTruck.FactoryPrice;
				Statics.VSRP = heavyTruck.FactoryPrice;
			}
		}


		public static void GetFullName()
		{
			string hold = "";
			if ((Statics.VehicleType == "C") || (Statics.VehicleType == "T"))
			{
				hold = Strings.Mid(Statics.VehicleMake, 1, 3);
				if (hold == "ACU")
				{
					Statics.VehicleMake = "ACURA";
				}
				else if (hold == "ALFA")
				{
					Statics.VehicleMake = "ALFA-ROMEO";
				}
				else if (hold == "AMG")
				{
					Statics.VehicleMake = "AMERICAN GENERAL";
				}
				else if (hold == "AME")
				{
					Statics.VehicleMake = "AMERICAN MOTORS";
				}
				else if (hold == "BUI")
				{
					Statics.VehicleMake = "BUICK";
				}
				else if (hold == "CAD")
				{
					Statics.VehicleMake = "CADILLAC";
				}
				else if (hold == "CHE")
				{
					if (Strings.Mid(Statics.VehicleMake, 41) == "C")
					{
						Statics.VehicleMake = "CHECKER";
					}
					else
					{
						Statics.VehicleMake = "CHEVROLET";
					}
				}
				else if (hold == "CHR")
				{
					Statics.VehicleMake = "CHRYSLER";
				}
				else if (hold == "DAE")
				{
					Statics.VehicleMake = "DAEWOO";
				}
				else if (hold == "DAI")
				{
					Statics.VehicleMake = "DAIHATSU";
				}
				else if (hold == "DOD")
				{
					Statics.VehicleMake = "DODGE";
				}
				else if (hold == "EGI")
				{
					Statics.VehicleMake = "EAGLE";
				}
				else if (hold == "HON")
				{
					Statics.VehicleMake = "HONDA";
				}
				else if (hold == "HYU")
				{
					Statics.VehicleMake = "HYUNDAI";
				}
				else if (hold == "INF")
				{
					Statics.VehicleMake = "INFINIT";
				}
				else if (hold == "ISU")
				{
					Statics.VehicleMake = "ISUZU";
				}
				else if (hold == "JAG")
				{
					Statics.VehicleMake = "JAGUAR";
				}
				else if (hold == "LEX")
				{
					Statics.VehicleMake = "LEXUS";
				}
				else if (hold == "LIN")
				{
					Statics.VehicleMake = "LINCOLN";
				}
				else if (hold == "MAZ")
				{
					Statics.VehicleMake = "MAZDA";
				}
				else if (hold == "MER")
				{
					if (Strings.Mid(Statics.VehicleMake, 4, 1) == "Z")
					{
						Statics.VehicleMake = "MERCEDES-BENZ";
					}
					else if (Strings.Mid(Statics.VehicleMake, 4, 1) == "C")
					{
						Statics.VehicleMake = "MERCURY";
					}
					else if (Strings.Mid(Statics.VehicleMake, 4, 1) == "K")
					{
						Statics.VehicleMake = "MERKUR";
					}
				}
				else if (hold == "MIT")
				{
					Statics.VehicleMake = "MITSUBISHI";
				}
				else if (hold == "NIS")
				{
					Statics.VehicleMake = "NISSAN";
				}
				else if (hold == "OLD")
				{
					Statics.VehicleMake = "OLDSMOBILE";
				}
				else if (hold == "PEU")
				{
					Statics.VehicleMake = "PEUGEOT";
				}
				else if (hold == "PLY")
				{
					Statics.VehicleMake = "PLYMOUTH";
				}
				else if (hold == "PON")
				{
					Statics.VehicleMake = "PONTIAC";
				}
				else if (hold == "POR")
				{
					Statics.VehicleMake = "PORCHE";
				}
				else if (hold == "REN")
				{
					Statics.VehicleMake = "RENAULT";
				}
				else if (hold == "ROV")
				{
					Statics.VehicleMake = "ROVER";
				}
				else if (hold == "SAT")
				{
					Statics.VehicleMake = "SATURN";
				}
				else if (hold == "STE")
				{
					Statics.VehicleMake = "STERLING";
				}
				else if (hold == "SUB")
				{
					Statics.VehicleMake = "SUBARU";
				}
				else if (hold == "SUZ")
				{
					Statics.VehicleMake = "SUZUKI";
				}
				else if (hold == "TOY")
				{
					Statics.VehicleMake = "TOYOTA";
				}
				else if (hold == "TRI")
				{
					Statics.VehicleMake = "TRUIMPH";
				}
				else if (hold == "VOL")
				{
					if (Strings.Mid(Statics.VehicleMake, 4, 1) == "K")
					{
						Statics.VehicleMake = "VOLKSWAGEN";
					}
					else
					{
						Statics.VehicleMake = "VOLVO";
					}
				}
			}
			
		}

        public static string GetFullNameForRBCars(string vehicleMake)
        {
			string hold = "";
			if ((Statics.VehicleType == "C") || (Statics.VehicleType == "T"))
			{
				hold = Strings.Mid(vehicleMake, 1, 3);
				if (hold == "ACU")
				{
					Statics.VehicleMake = "ACURA";
				}
				else if (hold == "ALFA")
				{
					Statics.VehicleMake = "ALFA-ROMEO";
				}
				else if (hold == "AMG")
				{
					Statics.VehicleMake = "AMERICAN GENERAL";
				}
				else if (hold == "AME")
				{
					Statics.VehicleMake = "AMERICAN MOTORS";
				}
				else if (hold == "BUI")
				{
					Statics.VehicleMake = "BUICK";
				}
				else if (hold == "CAD")
				{
					Statics.VehicleMake = "CADILLAC";
				}
				else if (hold == "CHE")
				{
					if (Strings.Mid(vehicleMake, 41) == "C")
					{
						Statics.VehicleMake = "CHECKER";
					}
					else
					{
						Statics.VehicleMake = "CHEVROLET";
					}
				}
				else if (hold == "CHR")
				{
					Statics.VehicleMake = "CHRYSLER";
				}
				else if (hold == "DAE")
				{
					Statics.VehicleMake = "DAEWOO";
				}
				else if (hold == "DAI")
				{
					Statics.VehicleMake = "DAIHATSU";
				}
				else if (hold == "DOD")
				{
					Statics.VehicleMake = "DODGE";
				}
				else if (hold == "EGI")
				{
					Statics.VehicleMake = "EAGLE";
				}
				else if (hold == "HON")
				{
					Statics.VehicleMake = "HONDA";
				}
				else if (hold == "HYU")
				{
					Statics.VehicleMake = "HYUNDAI";
				}
				else if (hold == "INF")
				{
					Statics.VehicleMake = "INFINIT";
				}
				else if (hold == "ISU")
				{
					Statics.VehicleMake = "ISUZU";
				}
				else if (hold == "JAG")
				{
					Statics.VehicleMake = "JAGUAR";
				}
				else if (hold == "LEX")
				{
					Statics.VehicleMake = "LEXUS";
				}
				else if (hold == "LIN")
				{
					Statics.VehicleMake = "LINCOLN";
				}
				else if (hold == "MAZ")
				{
					Statics.VehicleMake = "MAZDA";
				}
				else if (hold == "MER")
				{
					if (Strings.Mid(vehicleMake, 4, 1) == "Z")
					{
						Statics.VehicleMake = "MERCEDES-BENZ";
					}
					else if (Strings.Mid(vehicleMake, 4, 1) == "C")
					{
						Statics.VehicleMake = "MERCURY";
					}
					else if (Strings.Mid(vehicleMake, 4, 1) == "K")
					{
						Statics.VehicleMake = "MERKUR";
					}
				}
				else if (hold == "MIT")
				{
					Statics.VehicleMake = "MITSUBISHI";
				}
				else if (hold == "NIS")
				{
					Statics.VehicleMake = "NISSAN";
				}
				else if (hold == "OLD")
				{
					Statics.VehicleMake = "OLDSMOBILE";
				}
				else if (hold == "PEU")
				{
					Statics.VehicleMake = "PEUGEOT";
				}
				else if (hold == "PLY")
				{
					Statics.VehicleMake = "PLYMOUTH";
				}
				else if (hold == "PON")
				{
					Statics.VehicleMake = "PONTIAC";
				}
				else if (hold == "POR")
				{
					Statics.VehicleMake = "PORCHE";
				}
				else if (hold == "REN")
				{
					Statics.VehicleMake = "RENAULT";
				}
				else if (hold == "ROV")
				{
					Statics.VehicleMake = "ROVER";
				}
				else if (hold == "SAT")
				{
					Statics.VehicleMake = "SATURN";
				}
				else if (hold == "STE")
				{
					Statics.VehicleMake = "STERLING";
				}
				else if (hold == "SUB")
				{
					Statics.VehicleMake = "SUBARU";
				}
				else if (hold == "SUZ")
				{
					Statics.VehicleMake = "SUZUKI";
				}
				else if (hold == "TOY")
				{
					Statics.VehicleMake = "TOYOTA";
				}
				else if (hold == "TRI")
				{
					Statics.VehicleMake = "TRUIMPH";
				}
				else if (hold == "VOL")
				{
					if (Strings.Mid(vehicleMake, 4, 1) == "K")
					{
						Statics.VehicleMake = "VOLKSWAGEN";
					}
					else
					{
						Statics.VehicleMake = "VOLVO";
					}
				}
			}

            return Statics.VehicleMake;
        }

        public static string GetFullNameForHeavyTruck(string vehicleMake)
        {

				Statics.VehicleMake = Strings.Mid(Statics.VehicleMake, 1, 2);
				if (Statics.VehicleMake == "FR")
				{
					Statics.VehicleMake = "FREIGHTLINER";
				}
				else if (Statics.VehicleMake == "AU")
				{
					Statics.VehicleMake = "AUTOCAR";
				}
				else if (Statics.VehicleMake == "DO")
				{
					Statics.VehicleMake = "DODGE";
				}
				else if (Statics.VehicleMake == "FO")
				{
					Statics.VehicleMake = "FORD";
				}
				else if (Statics.VehicleMake == "GM")
				{
					Statics.VehicleMake = "GMC/CHEVY";
				}
				else if (Statics.VehicleMake == "HI")
				{
					Statics.VehicleMake = "HINO";
				}
				else if (Statics.VehicleMake == "IH")
				{
					Statics.VehicleMake = "INTERNATIONAL";
				}
				else if ((Statics.VehicleMake == "IS") || (Statics.VehicleMake == "IN"))
				{
					Statics.VehicleMake = "ISUZU";
				}
				else if (Statics.VehicleMake == "IV")
				{
					Statics.VehicleMake = "IVECO";
				}
				else if (Statics.VehicleMake == "KE")
				{
					Statics.VehicleMake = "KENWORTH";
				}
				else if (Statics.VehicleMake == "MA")
				{
					Statics.VehicleMake = "MACK-MIDLINER";
				}
				else if (Statics.VehicleMake == "MC")
				{
					Statics.VehicleMake = "MACK";
				}
				else if (Statics.VehicleMake == "MR")
				{
					Statics.VehicleMake = "MARMON";
				}
				else if (Statics.VehicleMake == "MS")
				{
					Statics.VehicleMake = "MERCEDES";
				}
				else if (Statics.VehicleMake == "MX")
				{
					Statics.VehicleMake = "MITSUBISHI FUSO";
				}
				else if (Statics.VehicleMake == "PE")
				{
					Statics.VehicleMake = "PETERBILT";
				}
				else if (Statics.VehicleMake == "SC")
				{
					Statics.VehicleMake = "SCANIA";
				}
				else if (Statics.VehicleMake == "ST")
				{
					Statics.VehicleMake = "STERLING";
				}
				else if (Statics.VehicleMake == "UD")
				{
					Statics.VehicleMake = "U D TRUCKS";
				}
				else if (Statics.VehicleMake == "UT")
				{
					Statics.VehicleMake = "UTILIMASTER";
				}
				else if (Statics.VehicleMake == "VO")
				{
					Statics.VehicleMake = "VOLVO";
				}
				else if (Statics.VehicleMake == "WE")
				{
					Statics.VehicleMake = "WESTERN STAR";
				}
				else if (Statics.VehicleMake == "WH")
				{
					Statics.VehicleMake = "WHITE GMC";
				}
				else if (Statics.VehicleMake == "FW")
				{
					Statics.VehicleMake = "FWD";
				}
				else if (Statics.VehicleMake == "MG")
				{
					Statics.VehicleMake = "MAGIRUS";
				}
				else if (Statics.VehicleMake == "OS")
				{
					Statics.VehicleMake = "OSHKOSH";
				}
				else if (Statics.VehicleMake == "WI")
				{
					Statics.VehicleMake = "WHITE";
				}

                return Statics.VehicleMake;
        }
		public static void ClearVariables()
		{
			Statics.VManufacturer = "";
			Statics.VModel = "";
			// rsFinalVehicle.fields("model
			Statics.VYear = "";
			// rsFinalVehicle.fields("Year
			Statics.VDescription = "";
			// rsFinalVehicle.fields("Description
			Statics.VFeet = 0;
			// rsFinalVehicle.fields("feet
			Statics.VInches = "";
			// rsFinalVehicle.fields("Inches
			Statics.VGrossVehicleWeight = "";
			// rsFinalVehicle.fields("Weight
			Statics.VSRP = "";
			// rsFinalVehicle.fields("srp"
			Statics.VModel = "";
			// rsFinalVehicle.fields("model
			Statics.VEngine = "";
			// rsFinalVehicle.fields("engine
			Statics.VTransmission = "";
			// rsFinalVehicle.fields("transmission
			Statics.VAirType = "";
			// rsFinalVehicle.fields("AirCondition
			Statics.VBrakeType = "";
			// rsFinalVehicle.fields("braketype
			Statics.VNumberOfPassengers = "";
			// rsFinalVehicle.fields("numberofpassengers
			Statics.VSRP = "";
			// rsFinalVehicle.fields("SuggestedRetailValue
			Statics.TotalCostOfWheels = 0;
			Statics.CostOfEachWheel = 0;
			Statics.TotalCost = 0;
		}
		// vbPorter upgrade warning: frm1 As Form	OnWrite(frmRedbook)


		public static void RedbookShow()
		{
			Statics.bolClearing = true;
			frmRedbook.InstancePtr.Unload();

			FCUtils.CloseFormsOnProject(false, frmRedbook.InstancePtr.Name);
			frmRedbook.InstancePtr.Show();
			Statics.bolClearing = false;
		}

		public class StaticVariables
		{
            public bool moduleInitialized = false;
            public int success;
			public bool bolClearing;
			public string strFile = string.Empty;
			// HELP STUFF
			public string gstrTruckVIN = "";
			// vbPorter upgrade warning: gstrTruckYEAR As string	OnWrite
			public string gstrTruckYEAR = "";
			public string gstrTruckMAKE = "";
			public string gstrTruckMODEL = "";
			public double gdblTruckBodyCombination;
			public bool PDSFlag;
			public bool MVFlag;
			public string SearchType = "";
			public string VehicleType = "";
			public bool VehicleMediumTruck;
			public int VehicleIDNumber;
			public string StandardOptions = "";
			// vbPorter upgrade warning: VehicleYear As int	OnWrite(string)	OnRead(int, string, FixedString)
			public int VehicleYear;
			// vbPorter upgrade warning: VehicleModel As string	OnRead(FixedString)
			public string VehicleModel = "";
			public string VehicleDescription = "";
			public string VehicleEngine = "";
			public string LongVehicleModel = "";
			// vbPorter upgrade warning: VehicleMake As string	OnRead(FixedString)
			public string VehicleMake = "";
			public bool FromMultiple;
			public int[] VehiclesInGrid = new int[1500 + 1];
			public int VehicleSelectedFromMultiple;
			public string strSQL = "";
			
			public string SearchModel = string.Empty;
			public string SearchYear = string.Empty;
			public string SearchMake = "";
			public int TotalCostOfWheels;
			public int CostOfEachWheel;
			public int TotalCostOfBrakes;
			public int CostOfEachBrake;
			public bool StickerButton;
			public string Xy = "";
			// sticker value from sticker form
			public bool StickerFormLoaded;
			// vbPorter upgrade warning: VYear As string	OnRead
			// Final Vehicle Stuff
			public string VYear = "";
			public string VModel = string.Empty;
			public string VManufacturer = string.Empty;
			public string VDescription = string.Empty;
			public string VEngine = string.Empty;
			public string VBrakeType = string.Empty;
			public string VAirType = string.Empty;
			public string VTransmission = string.Empty;
			public string VAxle1Make = "";
			public string VEngineMake = "";
			public string VEngineDescription = "";
			public string VAxle1Description = "";
			public string VTransmissionMake = "";
			public string VTransmissionDescription = "";
			public string VGVW = "";
			public string VWheelbase = "";
			public string VAxle2Make = "";
			public string VAxle2Description = "";
			public string VRetailPrice = "";
			public string VNetWeight = "";
			public string VNumberOfPassengers = string.Empty;
			public string VGrossVehicleWeight = string.Empty;
			public int VFeet;
			public string VInches = string.Empty;
			public string TruckManf = "";
			public string VIN = string.Empty;
			// vbPorter upgrade warning: TotalOptionCost As Decimal	OnWrite
			public Decimal TotalOptionCost;
			public bool blnFromDOS;
			public bool blnFromWindowsCR;
			public bool blnProcessReg;
			// what is teh user licensed for
			public bool Trailer;
			public bool TruckBody;
			public bool RV;
			public bool Cycle;
			public bool HeavyTruck;
			public bool Auto;
            public object CurrentVehicleObject;
			public string VSRP = string.Empty;
			// vbPorter upgrade warning: TotalCost As int	OnWrite(int, Decimal, double)
			public double TotalCost;
			public rvmanf mr = new rvmanf();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
