﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRB0000
{
	public partial class frmSticker : BaseForm
	{
		public frmSticker()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSticker InstancePtr
		{
			get
			{
				return (frmSticker)Sys.GetInstance(typeof(frmSticker));
			}
		}

		protected frmSticker _InstancePtr = null;
		//=========================================================
		float Rate;

		private void frmSticker_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// If KeyCode = vbKeyF1 Then Call RoboHelp(frmSticker)
		}

		private void cmdCalculate_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtPrice.Text) != 0)
			{
				if (Conversion.Val(txtPrice.Text) < 9999999)
				{
					modRedBook.Statics.Xy = txtPrice.Text;
					frmSticker.InstancePtr.Close();
				}
				else
				{
					MessageBox.Show("Please enter a valid Sticker Price.", "Sticker Price Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			frmSticker.InstancePtr.Close();
		}

		private void frmSticker_Activated(object sender, System.EventArgs e)
		{
			frmSticker.InstancePtr.LeftOriginal = FCConvert.ToInt32((FCGlobal.Screen.WidthOriginal - frmSticker.InstancePtr.WidthOriginal) / 2.0);
			frmSticker.InstancePtr.TopOriginal = FCConvert.ToInt32((FCGlobal.Screen.HeightOriginal - frmSticker.InstancePtr.HeightOriginal) / 2.0);
			txtPrice.Focus();
		}

		private void frmSticker_Load(object sender, System.EventArgs e)
		{
            modRedBook.Statics.StickerFormLoaded = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void txtPrice_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			//FC:FINAL:DDU:#2626 - on enter proceed as in original
			if (KeyAscii == Keys.Enter)
			{
				cmdCalculate_Click(null, null);
			}
			else if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
