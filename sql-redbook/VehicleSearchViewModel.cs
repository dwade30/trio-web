using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using Global;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.Extensions;
using SharedApplication.SystemSettings;
using TWRB0000.commands;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRB0000
{
    public interface IVehicleSearchViewModel
    {
        string Vin { get; set; }
        bool SearchByVin();
    }

    public class VehicleSearchViewModel : IVehicleSearchViewModel
    {
        public string Vin { get; set; } = "";
        private IBlueBookRepository blueBookRepository;

        public VehicleSearchViewModel(IBlueBookRepository blueBookRepository)
        {
            this.blueBookRepository = blueBookRepository;
        }
        public bool SearchByVin()
        {
            if (Vin.Length < 17)
            {
                switch (Vin)
                {
                    case "L":
                        cSettingsController set = new cSettingsController();
                        var lastvin = set.GetSettingValue("LastVIN", "RedBook", "User", TWSharedLibrary.Variables.Statics.UserID, "");
                        frmWait.InstancePtr.Unload();
                        if (lastvin.IsNullOrWhiteSpace())
                        {
                            MessageBox.Show("There is no stored last VIN used.", "No Stored VIN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            Vin = lastvin;
                        }
                        return false;
                        break;
                    default:
                        FCMessageBox.Show("The VIN you have entered is less than 17 characters long. Please check your number.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "VIN Error");
                       // txtVIN.Focus();
                        return false;
                }
            }

            var vehicles = blueBookRepository.VehicleInformation.GetVehicleInformation(Vin).ToList();
            if (!vehicles.Any())
            {
                FCMessageBox.Show("No vehicle found for that VIN", MsgBoxStyle.OkOnly, "No Vehicle Found");
                //MessageBox.Show("No vehicle found for that VIN", "No Vehicle Found", MessageBoxButtons.OK);
                return false;
            }

            modRedBook.Statics.VIN = Vin;
            return ShowVehicles(vehicles);
        }

        private bool ShowVehicles(IEnumerable<VehicleInformation> vehicles)
        {
            if (!vehicles.Any())
            {
                return false;
            }
            var vehicle = vehicles.FirstOrDefault();
            var vehicleAllowed = false;
            var vehicleTypeDescription = "";
            switch ((VehicleClassification)vehicle.ClassificationId)
            {
                case VehicleClassification.CommercialTrucks:
                    if (modRedBook.Statics.HeavyTruck)
                    {
                        vehicleAllowed = true;
                    }

                    modRedBook.Statics.VehicleType = "H";
                    vehicleTypeDescription = "heavy trucks";
                    break;
                case VehicleClassification.CommercialTrailers:
                    if (modRedBook.Statics.Trailer)
                    {
                        vehicleAllowed = true;
                    }

                    modRedBook.Statics.VehicleType = "X";
                    vehicleTypeDescription = "commercial trailers";
                    break;
                case VehicleClassification.PassengerVehicles:
                    if (modRedBook.Statics.Auto)
                    {
                        vehicleAllowed = true;
                    }

                    modRedBook.Statics.VehicleType = "C";
                    vehicleTypeDescription = "automobiles and light trucks";
                    break;
                case VehicleClassification.Powersport:
                    if (modRedBook.Statics.Cycle)
                    {
                        vehicleAllowed = true;
                    }

                    modRedBook.Statics.VehicleType = "M";
                    vehicleTypeDescription = "motorcycles";
                    break;
                case VehicleClassification.RecreationalVehicles:
                    if (modRedBook.Statics.RV)
                    {
                        vehicleAllowed = true;
                    }

                    modRedBook.Statics.VehicleType = "R";
                    vehicleTypeDescription = "recreational vehicles";
                    break;
                case VehicleClassification.TruckBodies:
                    if (modRedBook.Statics.TruckBody)
                    {
                        vehicleAllowed = true;
                    }

                    modRedBook.Statics.VehicleType = "B";
                    vehicleTypeDescription = "truck bodies";
                    break;
                default:
                    vehicleAllowed = false;
                    break;
            }

            if (!vehicleAllowed)
            {
                FCMessageBox.Show("You are not set up to access " + vehicleTypeDescription, MsgBoxStyle.Exclamation,
                    "Invalid vehicle type");
                return false;
            }

            var configurations = blueBookRepository.VehicleConfigurations.GetConfigurations(
                    new VehicleConfigurationFilter(vehicle.ModelId, 0, "", "", 0, 0));

            modRedBook.Statics.VehicleYear = vehicle.ModelYear;
			modRedBook.Statics.VehicleMake = vehicle.ManufacturerName;
            modRedBook.Statics.VehicleModel = vehicle.ModelName;

            if (vehicles.Count() > 1)
            {
                FillMultipleVehicleListBox(vehicles);
                var multView = new MultipleVehicleViewModel(blueBookRepository);
                multView.SetConfigurations(configurations);
                //this.Hide();
                //frmMultiple.InstancePtr.Show();
                //frmMultiple.InstancePtr.vs1.Focus();
                //this.Hide();
                StaticSettings.GlobalCommandDispatcher.Send(new ShowMultipleVehicleModels());
            }
            else
            {
                ShowOptions(vehicle.ConfigurationId);
            }

            return true;
        }

        public void FillMultipleVehicleListBox(IEnumerable<VehicleInformation> vehicles)
        {
            frmMultiple.InstancePtr.vs1.Rows = 1;
            int row = 0;
            var isFirst = true;
            foreach (var vehicle in vehicles)
            {
                var vehicleSpecs = blueBookRepository.VehicleSpecs.GetVehicleSpecs(vehicle.ConfigurationId);
                if (vehicleSpecs != null)
                {
                    if (isFirst)
                    {
                        SetupGrid(vehicleSpecs.ClassificationId);
                        FillMultipleMakeInformation(vehicle.ManufacturerName, vehicle.ModelYear.ToString());
                        //frmMultiple.InstancePtr.lblMfg.Text = vehicle.ManufacturerName;
                        //    frmMultiple.InstancePtr.lblYear.Text = vehicle.ModelYear.ToString();
                        modRedBook.Statics.StandardOptions = vehicleSpecs.Specs.FirstOrDefault(s => s.Name == "equipmentCode")?.GetValue() ?? "";
                        isFirst = false;
                    }
                    AddToMultipleGrid(vehicleSpecs, vehicle.VinModelNumber);
                }
            }


            frmMultiple.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, frmMultiple.InstancePtr.vs1.Rows - 1, 6, 1);
        }

        private void AddToMultipleGrid(VehicleConfigurationSpecs vehicleSpecs, string vinModelNumber)
        {
            var specs = vehicleSpecs.Specs;
            frmMultiple.InstancePtr.vs1.Rows++;
            int row = frmMultiple.InstancePtr.vs1.Rows - 1;
            modRedBook.Statics.VehiclesInGrid[row] = vehicleSpecs.ConfigurationId;
            frmMultiple.InstancePtr.vs1.RowData(row, vehicleSpecs.ConfigurationId);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, vinModelNumber);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, vehicleSpecs.ModelName);
            switch (vehicleSpecs.ClassificationId)
            {
                case VehicleClassificationNumber.PassengerVehicles:
                    AddPassengerToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.CommercialTrucks:
                    AddHeavyTruckToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.Powersport:
                    AddMotorcycleToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.RecreationalVehicles:
                    AddRecreationalVehicleToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.TruckBodies:
                    AddTruckBodyToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.CommercialTrailers:
                    AddTrailerToMultipleGrid(vehicleSpecs, row);
                    break;
            }
        }

        private void AddTrailerToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var trailer = vSpecs.ToTrailerData();
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, trailer.Manufacturer);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, trailer.Year.ToString());
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, trailer.Section);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, trailer.Width);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, trailer.Length);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 5, trailer.Vin);
        }

        private void AddTruckBodyToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var truckBodyData = vSpecs.ToTruckBodyData();
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, truckBodyData.Manufacturer);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, truckBodyData.Year.ToString());
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, "");
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, truckBodyData.Type);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, truckBodyData.Description);
        }

        private void AddRecreationalVehicleToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var rv = vSpecs.ToRv();
            if (rv.Inches.ToIntegerValue() != 0)
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, rv.Feet.GetValueOrDefault() + "' " + rv.Inches.Trim() + Convert.ToChar(34).ToString());
            }
            else
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, rv.Feet.GetValueOrDefault() + "'");
            }
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, rv.Model);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, rv.Description);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, rv.Weight);
        }

        private void AddMotorcycleToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var cycle = vSpecs.ToCycle();
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, cycle.ModelNumber);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, cycle.Cylinders);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, cycle.Description);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, cycle.Displacement);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, cycle.Weight);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 6, cycle.Color);
        }

        private void AddHeavyTruckToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var specs = vSpecs.Specs;
            var heavyTruck = vSpecs.ToHeavyTruck();
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, heavyTruck.Description);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, heavyTruck.EngineDescription);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, heavyTruck.TransmissionDescription);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 5, heavyTruck.GrossVehicleWeight);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 6, heavyTruck.Wheelbase);
        }

        private void AddPassengerToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, vSpecs.GetSpecByName("description"));
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, vSpecs.GetSpecByName("engineSize"));
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, vSpecs.GetSpecByName("transmissionType"));
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 5, vSpecs.GetSpecByName("gvw"));
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 6, vSpecs.GetSpecByName("passengers"));
        }

        private void SetupGrid(VehicleClassificationNumber vehicleType)
        {
            frmMultiple.InstancePtr.vs1.Rows = 1;
            switch (vehicleType)
            {
                case VehicleClassificationNumber.PassengerVehicles:
                    SetMultipleGridForPassengerVehicles();
                    break;
                case VehicleClassificationNumber.CommercialTrailers:
                    SetMultipleGridForTrailers();
                    break;
                case VehicleClassificationNumber.CommercialTrucks:
                    SetMultipleGridForTrucks();
                    break;
                case VehicleClassificationNumber.Powersport:
                    SetMultipleGridForMotorcycles();
                    break;
                case VehicleClassificationNumber.RecreationalVehicles:
                    SetMultipleGridForRecreationalVehicles();
                    break;
            }
        }
        public void SetMultipleGridForRecreationalVehicles()
        {
            frmMultiple.InstancePtr.vs1.Cols = 4;
            frmMultiple.InstancePtr.vs1.ColWidth(0, FCConvert.ToInt32(frmMultiple.InstancePtr.vs1.WidthOriginal * 0.0847278));
            frmMultiple.InstancePtr.vs1.ColWidth(1, FCConvert.ToInt32(frmMultiple.InstancePtr.vs1.WidthOriginal * 0.4554119));
            frmMultiple.InstancePtr.vs1.ColWidth(2, FCConvert.ToInt32(frmMultiple.InstancePtr.vs1.WidthOriginal * 0.3065473));
            frmMultiple.InstancePtr.vs1.ColWidth(3, FCConvert.ToInt32(frmMultiple.InstancePtr.vs1.WidthOriginal * 0.0847278));
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Size");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Weight");
        }
        private void SetMultipleGridForTruckBodies()
        {
            frmMultiple.InstancePtr.vs1.ColWidth(0, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2100);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(5, 0);
            frmMultiple.InstancePtr.vs1.ColWidth(6, 0);
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Manufacturer");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Type");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Description");
        }
        private void SetMultipleGridForTrailers()
        {
            frmMultiple.InstancePtr.vs1.ColWidth(0, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2100);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(5, 0);
            frmMultiple.InstancePtr.vs1.ColWidth(6, 0);
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Vin");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Section");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Width");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 4, "Length");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 5, "");
        }
        public void SetMultipleGridForMotorcycles(bool showYear = false)
        {
            frmMultiple.InstancePtr.vs1.ColWidth(0, 1000);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2100);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 1400);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Mdl #");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Cylinders");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Displacement");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 4, "Weight");
            if (showYear)
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "Year");
            }
            else
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "Color");
            }
        }

        public void SetMultipleGridForPassengerVehicles(bool showYear = false)
        {
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Mdl #");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Engine");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 4, "Trans.");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 5, "G.V.W.");
            if (showYear)
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "Year");
            }
            else
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "# of Pass.");
            }

            frmMultiple.InstancePtr.vs1.ColWidth(0, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2000);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(5, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(6, 900);
        }
        public void SetMultipleGridForTrucks()
        {
            frmMultiple.InstancePtr.vs1.ColWidth(0, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2100);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(5, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(6, 800);
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Mdl #");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Engine Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 4, "Trans. Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 5, "G.V.W.");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "WheelBase");
        }

        private void FillMultipleMakeInformation(string manufacturer, string year)
        {
            frmMultiple.InstancePtr.lblYear.Text = year;
            modRedBook.Statics.VehicleMake = manufacturer;
            frmMultiple.InstancePtr.lblMfg.Text = manufacturer;
        }


        private void ShowOptions(int configurationId)
        {
            int Number = 0;
            modRedBook.Statics.FromMultiple = false;
            modRedBook.Statics.VehicleMediumTruck = false;
            var vehicleSpecs = blueBookRepository.VehicleSpecs.GetVehicleSpecs(configurationId);
            modRedBook.Statics.VehicleSelectedFromMultiple = configurationId;
            if (vehicleSpecs != null)
            {
                modRedBook.Statics.CurrentVehicleObject = vehicleSpecs;
                var equipmentCode = vehicleSpecs.GetSpecByName("equipmentCode");
                if ((modRedBook.Statics.VehicleType == "C") || (modRedBook.Statics.VehicleType == "T"))
                {
                    modRedBook.Statics.StandardOptions = vehicleSpecs.GetSpecByName("equipmentCode");
                    modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                    modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                    modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                    modRedBook.GetAllRbCarInfo(vehicleSpecs);

                    if ((equipmentCode == "S") || (equipmentCode == "F"))
                    {
                        modRedBook.Statics.VehicleType = "T";
                    }
                    else if (equipmentCode.IsNullOrWhiteSpace())
                    {
                        frmFinal.InstancePtr.Show();
                        return;
                    }
                    else
                    {
                        modRedBook.Statics.VehicleType = "C";
                    }

                    var configurationOptions = blueBookRepository.Options.GetOptions(
                        new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                    if (configurationOptions.Options.Any())
                    {
                        frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                        frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                        frmNewCarOptions.InstancePtr.Show();
                    }
                    else
                    {
                        frmFinal.InstancePtr.Show();
                        return;
                    }
                }
                else if (modRedBook.Statics.VehicleType == "H")
                {

                    modRedBook.Statics.VehicleType = "H";
                    modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                    modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                    modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                    modRedBook.Statics.VehicleMediumTruck = false; //vehicleSpecs.//heavyTruck.Type == "M";

                    var configurationOptions = blueBookRepository.Options.GetOptions(
                        new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                    if (configurationOptions.Options.Any())
                    {
                        frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                        frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                        frmNewCarOptions.InstancePtr.Show();
                    }
                    else
                    {
                        frmFinal.InstancePtr.Show();
                        return;
                    }
                }
                else if (modRedBook.Statics.VehicleType == "M")
                {

                    modRedBook.Statics.VehicleType = "M";
                    modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                    modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                    modRedBook.Statics.VehicleModel = vehicleSpecs.Description;
                    modRedBook.Statics.VehicleMake = vehicleSpecs.ManufacturerName;
                    modRedBook.GetAllMotorCycleInfo(vehicleSpecs);

                    frmFinal.InstancePtr.Show();
                }
                else if (modRedBook.Statics.VehicleType == "R")
                {
                    var rv = blueBookRepository.Rvs.GetRv(modRedBook.Statics.VehicleSelectedFromMultiple);
                    modRedBook.Statics.CurrentVehicleObject = rv;
                    modRedBook.Statics.VehicleType = "R";
                    modRedBook.Statics.VehicleIDNumber = rv.Id;
                    modRedBook.Statics.VehicleYear = rv.Year.ToIntegerValue();
                    modRedBook.Statics.VehicleModel = rv.Description;
                    modRedBook.Statics.VehicleMake = rv.Manufacturer;
                    modRedBook.Statics.VModel = rv.Model;
                    modRedBook.Statics.VYear = rv.Year;
                    modRedBook.Statics.VDescription = rv.Description;
                    modRedBook.Statics.VFeet = rv.Feet.GetValueOrDefault();
                    modRedBook.Statics.VInches = rv.Inches;
                    modRedBook.Statics.VGrossVehicleWeight = rv.Weight;
                    modRedBook.Statics.VSRP = rv.Srp;
                    modRedBook.Statics.VManufacturer = rv.Manufacturer;

                    frmFinal.InstancePtr.Show();
                }
                else if (modRedBook.Statics.VehicleType == "X")
                {
                    // Trailers
                    var trailer = blueBookRepository.TrailerData.GetTrailerData(modRedBook.Statics
                        .VehicleSelectedFromMultiple);
                    modRedBook.Statics.CurrentVehicleObject = trailer;
                    modRedBook.Statics.VehicleType = "X";
                    modRedBook.Statics.VehicleIDNumber = trailer.Id;
                    modRedBook.Statics.VehicleYear = trailer.Year.GetValueOrDefault();
                    modRedBook.Statics.VehicleModel = trailer.Manufacturer;
                    modRedBook.GetAllTrailerDataInfo(trailer);
                    modRedBook.Statics.CurrentVehicleObject = trailer;

                    frmFinal.InstancePtr.Show();
                }
                else if (modRedBook.Statics.VehicleType == "B")
                {
                    // Truck Body
                    modRedBook.Statics.CurrentVehicleObject = vehicleSpecs;
                    modRedBook.Statics.VehicleType = "B";
                    modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                    modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                    modRedBook.Statics.VManufacturer = vehicleSpecs.ManufacturerName;
                    modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                    modRedBook.Statics.VDescription = vehicleSpecs.Description;
                    modRedBook.Statics.VSRP = vehicleSpecs.GetSpecByName("msrp");
                    
                    var configurationOptions = blueBookRepository.Options.GetOptions(
                        new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                    if (configurationOptions.Options.Any())
                    {
                        frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                        frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                        frmNewCarOptions.InstancePtr.Show();
                    }
                    else
                    {
                        frmFinal.InstancePtr.Show();
                        return;
                    }
                }
            }
        }
    }
}