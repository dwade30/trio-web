//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRB0000
{
	/// <summary>
	/// Summary description for frmNewCarOptions.
	/// </summary>
	partial class frmNewCarOptions
	{
        public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCComboBox cboOptions;
		public fecherFoundation.FCLabel lblTotalCost;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblVehicleModel;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblVehicleMake;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuBack;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileExcise;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdProcess = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.Command1 = new fecherFoundation.FCButton();
            this.cboOptions = new fecherFoundation.FCComboBox();
            this.lblTotalCost = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblVehicleModel = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.lblVehicleMake = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.mnuBack = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExcise = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.OptionsPanel = new Wisej.Web.FlexLayoutPanel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 508);
            this.BottomPanel.Size = new System.Drawing.Size(1004, 16);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.OptionsPanel);
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.cmdClear);
            this.ClientArea.Controls.Add(this.Command1);
            this.ClientArea.Controls.Add(this.cboOptions);
            this.ClientArea.Controls.Add(this.lblTotalCost);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblVehicleModel);
            this.ClientArea.Controls.Add(this.lblYear);
            this.ClientArea.Controls.Add(this.lblVehicleMake);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Size = new System.Drawing.Size(1024, 530);
            this.ClientArea.PanelCollapsed += new System.EventHandler(this.ClientArea_PanelCollapsed);
            this.ClientArea.Controls.SetChildIndex(this.Label7, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblVehicleMake, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblVehicleModel, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTotalCost, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboOptions, 0);
            this.ClientArea.Controls.SetChildIndex(this.Command1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdClear, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdProcess, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.OptionsPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1024, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(277, 28);
            this.HeaderText.Text = "Vehicle Options Selection";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "actionButton";
            this.cmdProcess.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdProcess.Location = new System.Drawing.Point(738, 301);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(90, 40);
            this.cmdProcess.TabIndex = 128;
            this.cmdProcess.Text = "Excise";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.AppearanceKey = "actionButton";
            this.cmdClear.Location = new System.Drawing.Point(738, 236);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(90, 40);
            this.cmdClear.TabIndex = 127;
            this.cmdClear.Text = "Clear";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // Command1
            // 
            this.Command1.AppearanceKey = "actionButton";
            this.Command1.Location = new System.Drawing.Point(738, 171);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(90, 40);
            this.Command1.TabIndex = 126;
            this.Command1.Text = "Back";
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // cboOptions
            // 
            this.cboOptions.BackColor = System.Drawing.SystemColors.Window;
            this.cboOptions.Location = new System.Drawing.Point(738, 82);
            this.cboOptions.Name = "cboOptions";
            this.cboOptions.Size = new System.Drawing.Size(189, 40);
            this.cboOptions.TabIndex = 94;
            this.cboOptions.SelectedIndexChanged += new System.EventHandler(this.cboOptions_SelectedIndexChanged);
            // 
            // lblTotalCost
            // 
            this.lblTotalCost.ForeColor = System.Drawing.Color.FromArgb(0, 0, 192);
            this.lblTotalCost.Location = new System.Drawing.Point(738, 410);
            this.lblTotalCost.Name = "lblTotalCost";
            this.lblTotalCost.Size = new System.Drawing.Size(163, 17);
            this.lblTotalCost.TabIndex = 140;
            this.lblTotalCost.Text = "0.00";
            this.lblTotalCost.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(738, 381);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(189, 15);
            this.Label4.TabIndex = 139;
            this.Label4.Text = "TOTAL COST OF OPTIONS";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(738, 57);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(171, 19);
            this.Label2.TabIndex = 138;
            this.Label2.Text = "PACKAGES AVAILABLE";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVehicleModel
            // 
            this.lblVehicleModel.Location = new System.Drawing.Point(103, 56);
            this.lblVehicleModel.Name = "lblVehicleModel";
            this.lblVehicleModel.Size = new System.Drawing.Size(405, 17);
            this.lblVehicleModel.TabIndex = 137;
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(30, 30);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(50, 17);
            this.lblYear.TabIndex = 136;
            // 
            // lblVehicleMake
            // 
            this.lblVehicleMake.Location = new System.Drawing.Point(103, 30);
            this.lblVehicleMake.Name = "lblVehicleMake";
            this.lblVehicleMake.Size = new System.Drawing.Size(405, 18);
            this.lblVehicleMake.TabIndex = 135;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(36, 83);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(30, 17);
            this.Label1.TabIndex = 134;
            this.Label1.Text = "STD";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(76, 83);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(49, 17);
            this.Label3.TabIndex = 133;
            this.Label3.Text = "EQUIP";
            // 
            // Label7
            // 
            this.Label7.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.Label7.Location = new System.Drawing.Point(105, 492);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(281, 16);
            this.Label7.TabIndex = 130;
            this.Label7.Text = "DATA PROVIDED BY PRICE DIGESTS";
            // 
            // mnuBack
            // 
            this.mnuBack.Index = 0;
            this.mnuBack.Name = "mnuBack";
            this.mnuBack.Text = "Back  to Selection Screen";
            this.mnuBack.Click += new System.EventHandler(this.mnuBack_Click);
            // 
            // mnuFileClear
            // 
            this.mnuFileClear.Index = 1;
            this.mnuFileClear.Name = "mnuFileClear";
            this.mnuFileClear.Text = "Clear Selections";
            this.mnuFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
            // 
            // mnuFileExcise
            // 
            this.mnuFileExcise.Index = 2;
            this.mnuFileExcise.Name = "mnuFileExcise";
            this.mnuFileExcise.Text = "Excise Tax";
            this.mnuFileExcise.Click += new System.EventHandler(this.mnuFileExcise_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBack,
            this.mnuFileClear,
            this.mnuFileExcise});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.OptionsPanel.AutoScroll = true;
            this.OptionsPanel.BorderStyle = Wisej.Web.BorderStyle.Solid;
            this.OptionsPanel.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.OptionsPanel.Location = new System.Drawing.Point(30, 103);
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(581, 383);
            this.OptionsPanel.TabIndex = 1001;
            this.OptionsPanel.TabStop = true;
            // 
            // frmNewCarOptions
            // 
            this.AcceptButton = this.cmdProcess;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1024, 590);
            this.FillColor = 0;
            this.Name = "frmNewCarOptions";
            this.Text = "Vehicle Options Selection";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmNewCarOptions_Load);
            this.Activated += new System.EventHandler(this.frmNewCarOptions_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmNewCarOptions_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FlexLayoutPanel OptionsPanel;
    }
}
