﻿using Autofac;

namespace TWRB0000.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VehicleSearchViewModel>().As<IVehicleSearchViewModel>();
        }
    }
}