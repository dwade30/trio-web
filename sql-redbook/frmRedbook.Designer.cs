//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRB0000
{
	/// <summary>
	/// Summary description for frmRedbook.
	/// </summary>
	partial class frmRedbook
	{
		public fecherFoundation.FCComboBox cmbCommercialTrailer;
		public fecherFoundation.FCLabel lblCommercialTrailer;
		public fecherFoundation.FCTextBox txtVIN;
		public fecherFoundation.FCTextBox txtRVSize;
		public fecherFoundation.FCButton cmdGetVehicle;
		public fecherFoundation.FCComboBox lstMake;
		public fecherFoundation.FCButton cmdSticker;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCComboBox txtYear;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSticker;
		public fecherFoundation.FCToolStripMenuItem mnuFileGet;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbCommercialTrailer = new fecherFoundation.FCComboBox();
            this.lblCommercialTrailer = new fecherFoundation.FCLabel();
            this.txtVIN = new fecherFoundation.FCTextBox();
            this.txtRVSize = new fecherFoundation.FCTextBox();
            this.cmdGetVehicle = new fecherFoundation.FCButton();
            this.lstMake = new fecherFoundation.FCComboBox();
            this.cmdSticker = new fecherFoundation.FCButton();
            this.cmdQuit = new fecherFoundation.FCButton();
            this.txtYear = new fecherFoundation.FCComboBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSticker = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileGet = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdFileGet = new fecherFoundation.FCButton();
            this.cmdFileSticker = new fecherFoundation.FCButton();
            this.cmbModels = new fecherFoundation.FCComboBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileGet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSticker)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileGet);
            this.BottomPanel.Location = new System.Drawing.Point(0, 423);
            this.BottomPanel.Size = new System.Drawing.Size(511, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbModels);
            this.ClientArea.Controls.Add(this.txtVIN);
            this.ClientArea.Controls.Add(this.txtRVSize);
            this.ClientArea.Controls.Add(this.lstMake);
            this.ClientArea.Controls.Add(this.cmbCommercialTrailer);
            this.ClientArea.Controls.Add(this.lblCommercialTrailer);
            this.ClientArea.Controls.Add(this.txtYear);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.cmdGetVehicle);
            this.ClientArea.Controls.Add(this.cmdQuit);
            this.ClientArea.Controls.Add(this.cmdSticker);
            this.ClientArea.Size = new System.Drawing.Size(531, 592);
            this.ClientArea.Controls.SetChildIndex(this.cmdSticker, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdQuit, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdGetVehicle, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label8, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label7, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCommercialTrailer, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbCommercialTrailer, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstMake, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtRVSize, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtVIN, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbModels, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileSticker);
            this.TopPanel.Size = new System.Drawing.Size(531, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileSticker, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(281, 28);
            this.HeaderText.Text = "Vehicle Selection Process";
            // 
            // cmbCommercialTrailer
            // 
            this.cmbCommercialTrailer.Items.AddRange(new object[] {
            "Commercial Trailer",
            "Truck Body",
            "Recreational Vehicles",
            "Motorcycles",
            "Heavy Truck",
            "Automobile / Light Duty Truck"});
            this.cmbCommercialTrailer.Location = new System.Drawing.Point(224, 108);
            this.cmbCommercialTrailer.Name = "cmbCommercialTrailer";
            this.cmbCommercialTrailer.Size = new System.Drawing.Size(279, 40);
            this.cmbCommercialTrailer.TabIndex = 14;
            this.cmbCommercialTrailer.Text = "Automobile / Light Duty Truck";
            this.cmbCommercialTrailer.SelectedIndexChanged += new System.EventHandler(this.cmbCommercialTrailer_SelectedIndexChanged);
            // 
            // lblCommercialTrailer
            // 
            this.lblCommercialTrailer.AutoSize = true;
            this.lblCommercialTrailer.Location = new System.Drawing.Point(30, 121);
            this.lblCommercialTrailer.Name = "lblCommercialTrailer";
            this.lblCommercialTrailer.Size = new System.Drawing.Size(151, 15);
            this.lblCommercialTrailer.TabIndex = 15;
            this.lblCommercialTrailer.Text = "SELECT VEHICLE TYPE";
            // 
            // txtVIN
            // 
            this.txtVIN.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtVIN.Location = new System.Drawing.Point(224, 21);
            this.txtVIN.MaxLength = 17;
            this.txtVIN.Name = "txtVIN";
            this.txtVIN.Size = new System.Drawing.Size(279, 40);
            this.txtVIN.TabIndex = 0;
            this.txtVIN.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtVIN_KeyPress);
            // 
            // txtRVSize
            // 
            this.txtRVSize.Enabled = false;
            this.txtRVSize.Location = new System.Drawing.Point(224, 383);
            this.txtRVSize.MaxLength = 17;
            this.txtRVSize.Name = "txtRVSize";
            this.txtRVSize.Size = new System.Drawing.Size(279, 40);
            this.txtRVSize.TabIndex = 10;
            this.txtRVSize.Visible = false;
            // 
            // cmdGetVehicle
            // 
            this.cmdGetVehicle.AppearanceKey = "actionButton";
            this.cmdGetVehicle.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdGetVehicle.Location = new System.Drawing.Point(203, 383);
            this.cmdGetVehicle.Name = "cmdGetVehicle";
            this.cmdGetVehicle.Size = new System.Drawing.Size(133, 40);
            this.cmdGetVehicle.TabIndex = 12;
            this.cmdGetVehicle.Text = "Get Vehicle";
            this.cmdGetVehicle.Visible = false;
            this.cmdGetVehicle.Click += new System.EventHandler(this.cmdGetVehicle_Click);
            // 
            // lstMake
            // 
            this.lstMake.Location = new System.Drawing.Point(224, 228);
            this.lstMake.Name = "lstMake";
            this.lstMake.Size = new System.Drawing.Size(279, 40);
            this.lstMake.Sorted = true;
            this.lstMake.TabIndex = 8;
            this.lstMake.Enter += new System.EventHandler(this.lstMake_Enter);
            // 
            // cmdSticker
            // 
            this.cmdSticker.AppearanceKey = "actionButton";
            this.cmdSticker.Location = new System.Drawing.Point(30, 383);
            this.cmdSticker.Name = "cmdSticker";
            this.cmdSticker.Size = new System.Drawing.Size(143, 40);
            this.cmdSticker.TabIndex = 11;
            this.cmdSticker.Text = "Sticker Price";
            this.cmdSticker.Visible = false;
            this.cmdSticker.Click += new System.EventHandler(this.cmdSticker_Click);
            // 
            // cmdQuit
            // 
            this.cmdQuit.AppearanceKey = "actionButton";
            this.cmdQuit.Location = new System.Drawing.Point(366, 383);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(76, 40);
            this.cmdQuit.TabIndex = 13;
            this.cmdQuit.Text = "Quit";
            this.cmdQuit.Visible = false;
            this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(224, 168);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(279, 40);
            this.txtYear.TabIndex = 7;
            this.txtYear.SelectedIndexChanged += new System.EventHandler(this.txtYear_SelectedIndexChanged);
            this.txtYear.Enter += new System.EventHandler(this.txtYear_Enter);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(151, 15);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "ENTER VIN  (IF KNOWN) ";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 73);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(96, 15);
            this.Label3.TabIndex = 20;
            this.Label3.Text = "OR SEARCH BY";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 348);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(143, 15);
            this.Label4.TabIndex = 19;
            this.Label4.Text = "FOR RV\'S ONLY";
            this.Label4.Visible = false;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 397);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(104, 15);
            this.Label5.TabIndex = 18;
            this.Label5.Text = "LENGTH IN FEET";
            this.Label5.Visible = false;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 182);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(59, 15);
            this.Label6.TabIndex = 17;
            this.Label6.Text = "YEAR";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 302);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(130, 15);
            this.Label7.TabIndex = 16;
            this.Label7.Text = "MODEL";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(30, 242);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(61, 15);
            this.Label8.TabIndex = 15;
            this.Label8.Text = "MAKE";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSticker,
            this.mnuFileGet,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSticker
            // 
            this.mnuFileSticker.Index = 0;
            this.mnuFileSticker.Name = "mnuFileSticker";
            this.mnuFileSticker.Text = "Sticker Price";
            this.mnuFileSticker.Click += new System.EventHandler(this.mnuFileSticker_Click);
            // 
            // mnuFileGet
            // 
            this.mnuFileGet.Index = 1;
            this.mnuFileGet.Name = "mnuFileGet";
            this.mnuFileGet.Text = "Get Vehicle";
            this.mnuFileGet.Click += new System.EventHandler(this.mnuFileGet_Click);
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 2;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdFileGet
            // 
            this.cmdFileGet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileGet.AppearanceKey = "acceptButton";
            this.cmdFileGet.Location = new System.Drawing.Point(172, 30);
            this.cmdFileGet.Name = "cmdFileGet";
            this.cmdFileGet.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileGet.Size = new System.Drawing.Size(137, 48);
            this.cmdFileGet.TabIndex = 1;
            this.cmdFileGet.Text = "Get Vehicle";
            this.cmdFileGet.Click += new System.EventHandler(this.mnuFileGet_Click);
            // 
            // cmdFileSticker
            // 
            this.cmdFileSticker.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileSticker.Location = new System.Drawing.Point(407, 29);
            this.cmdFileSticker.Name = "cmdFileSticker";
            this.cmdFileSticker.Size = new System.Drawing.Size(96, 24);
            this.cmdFileSticker.TabIndex = 2;
            this.cmdFileSticker.Text = "Sticker Price";
            this.cmdFileSticker.Click += new System.EventHandler(this.mnuFileSticker_Click);
            // 
            // cmbModels
            // 
            this.cmbModels.Location = new System.Drawing.Point(224, 288);
            this.cmbModels.Name = "cmbModels";
            this.cmbModels.Size = new System.Drawing.Size(279, 40);
            this.cmbModels.Sorted = true;
            this.cmbModels.TabIndex = 9;
            // 
            // frmRedbook
            // 
            this.AcceptButton = this.cmdGetVehicle;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(531, 652);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmRedbook";
            this.Text = "Vehicle Selection Process";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmRedbook_Load);
            this.Activated += new System.EventHandler(this.frmRedbook_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRedbook_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileGet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSticker)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdFileGet;
        private FCButton cmdFileSticker;
        public FCComboBox cmbModels;
    }
}
