﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;

namespace TWRB0000
{
    public class MultipleVehicleViewModel : IMultipleVehicleViewModel
    {
        private IBlueBookRepository blueBookRepository;
        private IEnumerable<VehicleConfiguration> configurations;
        private int maxPageSize = 50;
        private int pageSize = 50;
        private int offset = 0;
        public bool IsLoadingDone { get; private set; } = false;
        
        
        public MultipleVehicleViewModel(IBlueBookRepository blueBookRepository)
        {
            this.blueBookRepository = blueBookRepository;

        }

        public event EventHandler<VehicleConfigurationsReturnedArgs> VehicleConfigurationsReturned;
        public IEnumerable<VehicleConfigurationSpecs> ConfigurationSpecs { get; }
        public void SetConfigurations(IEnumerable<VehicleConfiguration> configurations)
        {
            this.configurations = configurations;
        }

        public void StartLoadingSpecs(int pageSize)
        {
            if (pageSize > maxPageSize)
            {
                this.pageSize = maxPageSize;
            }
            else
            {
                this.pageSize = pageSize;
            }

            offset = 0;
            LoadSpecs(this.pageSize, offset);
        }

        public void LoadNext()
        {
            LoadSpecs(pageSize,offset);
        }

        public void SetPageSize(int pageSize)
        {
            if (pageSize <= maxPageSize)
            {
                this.pageSize = pageSize;
            }
            else
            {
                this.pageSize = maxPageSize;
            }
        }

        public int PageSize {
            get
            {
                return pageSize;
            }
        }
        public int Offset
        {
            get
            {
                return offset;
            }
        }

        private void NewLoadSpecs(int limit, int pageOffset)
        {

        }

        private void LoadSpecs(int limit, int pageOffset)
        {
            IsLoadingDone = false;
            var currentConfigs = configurations.Skip(pageOffset).Take(limit);
            offset += limit;
            var currentSpecs =
                blueBookRepository.VehicleSpecs.GetVehicleListSpecs(currentConfigs.Select(o => o.ConfigurationId)).ToList();

            foreach (var currentSpec in currentSpecs)
            {
                var currentConfig = currentConfigs.FirstOrDefault(s => s.ConfigurationId == currentSpec.ConfigurationId);
                if (currentConfig != null)
                {
                    currentSpec.Specs.Add(new VehicleSpec(){Name = "ModelNumberFromVin",Value = currentConfig.VinModelNumber});
                }
            }

            if (currentSpecs.Count() < limit)
            {
                IsLoadingDone = true;
            }

            if (VehicleConfigurationsReturned != null)
            {
                VehicleConfigurationsReturned(this,new VehicleConfigurationsReturnedArgs(IsLoadingDone,currentSpecs));
            }
        }
    }
}