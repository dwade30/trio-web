//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using Autofac;
using SharedApplication.Extensions;
using TWRB0000.commands;
using TWSharedLibrary;

namespace TWRB0000
{
	public class MDIParent// : BaseForm
	{
		public MDIParent()
		{
			//
			// required for windows form designer support
			//
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;
		//=========================================================
		// Last Updated:  6/27/01
		private int[] LabelNumber = new int[200 + 1];
		int CurRow;
		int lngFCode;
		const string strTrio = "TRIO Software Corporation";
		public bool blnFirstTimeIn;

        public void ClearLabels()
		{
        }

		private void SetMenuOptions(string strMenu)
		{
            ClearLabels();
        }

		public void Menu1()
		{
            object ans;
			string strRec1 = "";
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
                var searchViewModel = StaticSettings.GlobalCommandDispatcher.Send(new GetVehicleSearchViewModel()).Result;
                frmRedbook.InstancePtr.ViewModel = searchViewModel;
				frmRedbook.InstancePtr.Show();
			}
			else
			{
			}
		}

		public void Menu2()
		{
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "Main")
            {
                modGNBas.WriteYY();
                FCUtils.CloseFormsOnProject();
                if (modRedBook.Statics.MVFlag == false && !modRedBook.Statics.blnFromWindowsCR && !modRedBook.Statics.blnFromDOS)
                {
                    App.MainForm.EndWaitRBModule();
                }
                else
                {
                    App.MainForm.EndWaitRBModule();
                }
            }
            else
            {
            }
		}

		public void Menu3()
		{
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
			}
			else
			{
			}
		}

		

        private void MDIParent_Activated(object sender, System.EventArgs e)
		{
            modArchiveImage.ShowArchiveImage(ref modGlobalConstants.Statics.gstrArchiveYear);
            {
                //frmRedbook.InstancePtr.Show();
                var searchViewModel = StaticSettings.GlobalCommandDispatcher.Send(new GetVehicleSearchViewModel()).Result;
                frmRedbook.InstancePtr.ViewModel = searchViewModel;
                if (modRedBook.Statics.MVFlag == true)
                {
                    int ii;
                    cSettingsController set = new cSettingsController();
                    var vin = (set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK", "") != "" ? set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK", "") : "");
                    if (vin.HasText())
                    {
                        searchViewModel.Vin = vin;
                        if (searchViewModel.SearchByVin())
                        {
                            return;
                        }
                    }
                }
                frmRedbook.InstancePtr.Show();
			}
        }

		//private void MDIParent_Load(object sender, System.EventArgs e)
		public void Init(bool addExitMenu )
		{
            int counter;
            App.MainForm.Caption = "BLUE BOOK";
            string assemblyName = this.GetType().Assembly.GetName().Name;
            if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
            {
                App.MainForm.ApplicationIcons.Add(assemblyName, "icon-blue-book");
            }
            App.MainForm.NavigationMenu.Owner = this;
            App.MainForm.NavigationMenu.OriginName = "Redbook";
			modGlobalFunctions.LoadTRIOColors();

			string strReturn = "";
			if (strReturn == string.Empty)
				strReturn = "False";
			modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
            GetMainMenu(addExitMenu);
            MDIParent_Activated(null, null);
            App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "Maximize Forms", ref strReturn);
            modGlobalConstants.Statics.boolMaxForms = FCConvert.ToBoolean(modRegistry.GetRegistryKey("Maximize Forms", "", FCConvert.ToString(false)));
        }

        private void mnuFExit_Click(object sender, System.EventArgs e)
		{
            modGNBas.WriteYY();
            App.MainForm.EndWaitRBModule();
        }

		private void mnuFileFormsPrintForms_Click(object sender, System.EventArgs e)
		{
			frmScreenPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
        }

		public void GetMainMenu(bool addExitMenu = false)
		{

			string strTemp = "";
			ClearLabels();
            bool boolDisable = false;
            string menu = "Main";
            string imageKey = "";
            int menuCount = 2;
            for (CurRow = 1; CurRow <= menuCount; CurRow++)
            {
                strTemp = "";
                lngFCode = 0;
				switch (CurRow)
				{
                    case 1:
                        strTemp = "Select Vehicle";
                        break;
					case 2:
						{
                            if (addExitMenu)
                            {
                                strTemp = "Exit Blue Book";
                            }

                            break;
						}
					default:
						{
							strTemp = "";
							break;
						}
				}

                if (strTemp != "")
                {
                    FCMenuItem newItem =
                        App.MainForm.NavigationMenu.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 1, imageKey);
                }
            }
        }

        private void DisableMenuOption(ref bool boolState, ref int intRow)
		{
			boolState = !boolState;
        }
    }
}
