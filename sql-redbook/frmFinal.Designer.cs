﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWRB0000
{
	/// <summary>
	/// Summary description for frmFinal.
	/// </summary>
	partial class frmFinal
	{
		public fecherFoundation.FCComboBox cmbMillRate;
		public fecherFoundation.FCLabel lblMillRate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTotalValuation;
		public fecherFoundation.FCButton cmdBack;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel lblEngineDesc;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel lblEngine;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblModel;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblManufacturer;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel lblTransMake;
		public fecherFoundation.FCLabel lblTransDesc;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel lblNetWeight;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCButton cmdSticker;
		public fecherFoundation.FCButton cmdSelection;
		public fecherFoundation.FCButton cmdStart;
		public fecherFoundation.FCLabel lblSRP;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblOptionCosts;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblTotalValuation_0;
		public fecherFoundation.FCLabel lblYearRate;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblTotalExcise;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblStickerPrice;
		public fecherFoundation.FCLabel Label16;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileBackStart;
		public fecherFoundation.FCToolStripMenuItem mnuFileBackSelection;
		public fecherFoundation.FCToolStripMenuItem mnuBackOption;
		public fecherFoundation.FCToolStripMenuItem mnuFileSticker;
		public fecherFoundation.FCToolStripMenuItem mnuFileProcess;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbMillRate = new fecherFoundation.FCComboBox();
            this.lblMillRate = new fecherFoundation.FCLabel();
            this.cmdBack = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.lblEngineDesc = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.lblEngine = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.lblModel = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.lblManufacturer = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.lblTransMake = new fecherFoundation.FCLabel();
            this.lblTransDesc = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.lblNetWeight = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.cmdSticker = new fecherFoundation.FCButton();
            this.cmdSelection = new fecherFoundation.FCButton();
            this.cmdStart = new fecherFoundation.FCButton();
            this.lblSRP = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblOptionCosts = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblTotalValuation_0 = new fecherFoundation.FCLabel();
            this.lblYearRate = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblTotalExcise = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.lblStickerPrice = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBackStart = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBackSelection = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBackOption = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSticker = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileProcess = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdFileProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 533);
            this.BottomPanel.Size = new System.Drawing.Size(865, 90);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.ClientArea.Controls.Add(this.cmdBack);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cmbMillRate);
            this.ClientArea.Controls.Add(this.lblMillRate);
            this.ClientArea.Controls.Add(this.cmdSticker);
            this.ClientArea.Controls.Add(this.cmdSelection);
            this.ClientArea.Controls.Add(this.cmdStart);
            this.ClientArea.Controls.Add(this.lblSRP);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblOptionCosts);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.lblTotalValuation_0);
            this.ClientArea.Controls.Add(this.lblYearRate);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.lblTotalExcise);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.lblStickerPrice);
            this.ClientArea.Controls.Add(this.Label16);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(885, 630);
            this.ClientArea.Controls.SetChildIndex(this.Label16, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStickerPrice, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label9, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTotalExcise, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblYearRate, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTotalValuation_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblOptionCosts, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSRP, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdStart, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdSelection, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdSticker, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblMillRate, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbMillRate, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdBack, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(885, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(189, 28);
            this.HeaderText.Text = "Vehicle Valuation";
            // 
            // cmbMillRate
            // 
            this.cmbMillRate.Items.AddRange(new object[] {
            "0.024    Year 1 ",
            "0.0175  Year 2",
            "0.0135  Year 3",
            "0.010    Year 4",
            "0.0065  Year 5",
            "0.004    Year 6"});
            this.cmbMillRate.Location = new System.Drawing.Point(661, 31);
            this.cmbMillRate.Name = "cmbMillRate";
            this.cmbMillRate.Size = new System.Drawing.Size(179, 40);
            this.cmbMillRate.TabIndex = 28;
            this.cmbMillRate.SelectedIndexChanged += new System.EventHandler(this.optMillRate_CheckedChanged);
            // 
            // lblMillRate
            // 
            this.lblMillRate.Location = new System.Drawing.Point(475, 45);
            this.lblMillRate.Name = "lblMillRate";
            this.lblMillRate.Size = new System.Drawing.Size(116, 17);
            this.lblMillRate.TabIndex = 12;
            this.lblMillRate.Text = "CHANGE MILL RATE";
            // 
            // cmdBack
            // 
            this.cmdBack.AppearanceKey = "actionButton";
            this.cmdBack.Location = new System.Drawing.Point(454, 493);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(192, 40);
            this.cmdBack.TabIndex = 27;
            this.cmdBack.Text = "Back to Option Screen";
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.lblEngineDesc);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Controls.Add(this.lblDescription);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.lblEngine);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.lblModel);
            this.Frame1.Controls.Add(this.lblYear);
            this.Frame1.Controls.Add(this.lblManufacturer);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Controls.Add(this.lblTransMake);
            this.Frame1.Controls.Add(this.lblTransDesc);
            this.Frame1.Controls.Add(this.Label13);
            this.Frame1.Controls.Add(this.lblNetWeight);
            this.Frame1.Controls.Add(this.Label14);
            this.Frame1.Controls.Add(this.Label15);
            this.Frame1.Location = new System.Drawing.Point(30, 31);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(415, 456);
            this.Frame1.TabIndex = 10;
            this.Frame1.Text = "Vehicle Information";
            // 
            // lblEngineDesc
            // 
            this.lblEngineDesc.Location = new System.Drawing.Point(188, 323);
            this.lblEngineDesc.Name = "lblEngineDesc";
            this.lblEngineDesc.Size = new System.Drawing.Size(207, 15);
            this.lblEngineDesc.TabIndex = 42;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 323);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(150, 15);
            this.Label10.TabIndex = 41;
            this.Label10.Text = "ENGINE DESCRIPTION";
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(20, 139);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(375, 94);
            this.lblDescription.TabIndex = 25;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 104);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(150, 15);
            this.Label8.TabIndex = 24;
            this.Label8.Text = "DESCRIPTION";
            // 
            // lblEngine
            // 
            this.lblEngine.Location = new System.Drawing.Point(188, 288);
            this.lblEngine.Name = "lblEngine";
            this.lblEngine.Size = new System.Drawing.Size(207, 15);
            this.lblEngine.TabIndex = 23;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 288);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(150, 15);
            this.Label7.TabIndex = 22;
            this.Label7.Text = "ENGINE MAKE";
            // 
            // lblModel
            // 
            this.lblModel.Location = new System.Drawing.Point(110, 74);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(194, 24);
            this.lblModel.TabIndex = 21;
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(20, 44);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(87, 24);
            this.lblYear.TabIndex = 20;
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.Location = new System.Drawing.Point(110, 44);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(285, 24);
            this.lblManufacturer.TabIndex = 19;
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 353);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(150, 15);
            this.Label11.TabIndex = 18;
            this.Label11.Text = "TRANSMISSION MAKE";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 388);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(150, 15);
            this.Label12.TabIndex = 17;
            this.Label12.Text = "TRANSMISSION DESCR";
            // 
            // lblTransMake
            // 
            this.lblTransMake.Location = new System.Drawing.Point(188, 353);
            this.lblTransMake.Name = "lblTransMake";
            this.lblTransMake.Size = new System.Drawing.Size(207, 15);
            this.lblTransMake.TabIndex = 16;
            // 
            // lblTransDesc
            // 
            this.lblTransDesc.Location = new System.Drawing.Point(188, 388);
            this.lblTransDesc.Name = "lblTransDesc";
            this.lblTransDesc.Size = new System.Drawing.Size(207, 15);
            this.lblTransDesc.TabIndex = 15;
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 423);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(150, 15);
            this.Label13.TabIndex = 14;
            this.Label13.Text = "NET WEIGHT";
            // 
            // lblNetWeight
            // 
            this.lblNetWeight.Location = new System.Drawing.Point(188, 423);
            this.lblNetWeight.Name = "lblNetWeight";
            this.lblNetWeight.Size = new System.Drawing.Size(207, 15);
            this.lblNetWeight.TabIndex = 13;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 253);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(150, 15);
            this.Label14.TabIndex = 12;
            this.Label14.Text = "DESCRIPTION";
            this.Label14.Visible = false;
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(20, 288);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(375, 85);
            this.Label15.TabIndex = 11;
            this.Label15.Visible = false;
            // 
            // cmdSticker
            // 
            this.cmdSticker.AppearanceKey = "actionButton";
            this.cmdSticker.Location = new System.Drawing.Point(661, 493);
            this.cmdSticker.Name = "cmdSticker";
            this.cmdSticker.Size = new System.Drawing.Size(142, 40);
            this.cmdSticker.TabIndex = 2;
            this.cmdSticker.Text = "Sticker Price";
            this.ToolTip1.SetToolTip(this.cmdSticker, "Calculates Excise Based on Sticker Price.");
            this.cmdSticker.Click += new System.EventHandler(this.cmdSticker_Click);
            // 
            // cmdSelection
            // 
            this.cmdSelection.AppearanceKey = "actionButton";
            this.cmdSelection.Location = new System.Drawing.Point(193, 493);
            this.cmdSelection.Name = "cmdSelection";
            this.cmdSelection.Size = new System.Drawing.Size(241, 40);
            this.cmdSelection.TabIndex = 1;
            this.cmdSelection.Text = "Back to Selection Screen";
            this.cmdSelection.Click += new System.EventHandler(this.cmdSelection_Click);
            // 
            // cmdStart
            // 
            this.cmdStart.AppearanceKey = "actionButton";
            this.cmdStart.Location = new System.Drawing.Point(30, 493);
            this.cmdStart.Name = "cmdStart";
            this.cmdStart.Size = new System.Drawing.Size(143, 40);
            this.cmdStart.TabIndex = 0;
            this.cmdStart.Text = "Back to Start";
            this.cmdStart.Click += new System.EventHandler(this.cmdStart_Click);
            // 
            // lblSRP
            // 
            this.lblSRP.Location = new System.Drawing.Point(661, 91);
            this.lblSRP.Name = "lblSRP";
            this.lblSRP.Size = new System.Drawing.Size(179, 24);
            this.lblSRP.TabIndex = 40;
            this.lblSRP.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(475, 96);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(50, 19);
            this.Label2.TabIndex = 39;
            this.Label2.Text = "A.D.P";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(475, 140);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(59, 19);
            this.Label3.TabIndex = 38;
            this.Label3.Text = "OPTIONS";
            // 
            // lblOptionCosts
            // 
            this.lblOptionCosts.Location = new System.Drawing.Point(661, 135);
            this.lblOptionCosts.Name = "lblOptionCosts";
            this.lblOptionCosts.Size = new System.Drawing.Size(179, 24);
            this.lblOptionCosts.TabIndex = 37;
            this.lblOptionCosts.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(475, 184);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(50, 19);
            this.Label4.TabIndex = 36;
            this.Label4.Text = "TOTAL";
            // 
            // lblTotalValuation_0
            // 
            this.lblTotalValuation_0.Location = new System.Drawing.Point(661, 179);
            this.lblTotalValuation_0.Name = "lblTotalValuation_0";
            this.lblTotalValuation_0.Size = new System.Drawing.Size(179, 24);
            this.lblTotalValuation_0.TabIndex = 35;
            this.lblTotalValuation_0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblYearRate
            // 
            this.lblYearRate.Location = new System.Drawing.Point(704, 267);
            this.lblYearRate.Name = "lblYearRate";
            this.lblYearRate.Size = new System.Drawing.Size(136, 24);
            this.lblYearRate.TabIndex = 34;
            this.lblYearRate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(475, 316);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(90, 17);
            this.Label6.TabIndex = 33;
            this.Label6.Text = "TOTAL EXCISE";
            // 
            // lblTotalExcise
            // 
            this.lblTotalExcise.Location = new System.Drawing.Point(661, 311);
            this.lblTotalExcise.Name = "lblTotalExcise";
            this.lblTotalExcise.Size = new System.Drawing.Size(179, 24);
            this.lblTotalExcise.TabIndex = 32;
            this.lblTotalExcise.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(661, 267);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(13, 22);
            this.Label5.TabIndex = 31;
            this.Label5.Text = "X";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(475, 228);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(104, 20);
            this.Label9.TabIndex = 30;
            this.Label9.Text = "STICKER PRICE";
            this.Label9.Visible = false;
            // 
            // lblStickerPrice
            // 
            this.lblStickerPrice.Location = new System.Drawing.Point(661, 223);
            this.lblStickerPrice.Name = "lblStickerPrice";
            this.lblStickerPrice.Size = new System.Drawing.Size(179, 24);
            this.lblStickerPrice.TabIndex = 29;
            this.lblStickerPrice.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(30, 6);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(312, 19);
            this.Label16.TabIndex = 28;
            this.Label16.Text = "DATA PROVIDED BY PRICE DIGESTS";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileBackStart,
            this.mnuFileBackSelection,
            this.mnuBackOption,
            this.mnuFileSticker,
            this.mnuFileProcess});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileBackStart
            // 
            this.mnuFileBackStart.Index = 0;
            this.mnuFileBackStart.Name = "mnuFileBackStart";
            this.mnuFileBackStart.Text = "Back to Start";
            this.mnuFileBackStart.Click += new System.EventHandler(this.mnuFileBackStart_Click);
            // 
            // mnuFileBackSelection
            // 
            this.mnuFileBackSelection.Index = 1;
            this.mnuFileBackSelection.Name = "mnuFileBackSelection";
            this.mnuFileBackSelection.Text = "Back to Selection Screen";
            this.mnuFileBackSelection.Click += new System.EventHandler(this.mnuFileBackSelection_Click);
            // 
            // mnuBackOption
            // 
            this.mnuBackOption.Index = 2;
            this.mnuBackOption.Name = "mnuBackOption";
            this.mnuBackOption.Text = "Back to Option Screen";
            this.mnuBackOption.Click += new System.EventHandler(this.mnuBackOption_Click);
            // 
            // mnuFileSticker
            // 
            this.mnuFileSticker.Index = 3;
            this.mnuFileSticker.Name = "mnuFileSticker";
            this.mnuFileSticker.Text = "Sticker Price";
            this.mnuFileSticker.Click += new System.EventHandler(this.mnuFileSticker_Click);
            // 
            // mnuFileProcess
            // 
            this.mnuFileProcess.Index = 4;
            this.mnuFileProcess.Name = "mnuFileProcess";
            this.mnuFileProcess.Text = "Process";
            this.mnuFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
            // 
            // cmdFileProcess
            // 
            this.cmdFileProcess.AppearanceKey = "acceptButton";
            this.cmdFileProcess.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdFileProcess.Location = new System.Drawing.Point(366, 23);
            this.cmdFileProcess.Name = "cmdFileProcess";
            this.cmdFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileProcess.Size = new System.Drawing.Size(107, 48);
            this.cmdFileProcess.TabIndex = 0;
            this.cmdFileProcess.Text = "Process";
            this.cmdFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
            // 
            // frmFinal
            // 
            this.AcceptButton = this.cmdFileProcess;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(885, 695);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmFinal";
            this.Text = "Vehicle Valuation";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmFinal_Load);
            this.Activated += new System.EventHandler(this.frmFinal_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFinal_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdFileProcess;
    }
}
