//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRB0000
{
	/// <summary>
	/// Summary description for frmSticker.
	/// </summary>
	partial class frmSticker
	{
		public fecherFoundation.FCButton cmdCalculate;
		public fecherFoundation.FCTextBox txtPrice;
		public fecherFoundation.FCLabel Label3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdCalculate = new fecherFoundation.FCButton();
            this.txtPrice = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 275);
            this.BottomPanel.Size = new System.Drawing.Size(342, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdCalculate);
            this.ClientArea.Controls.Add(this.txtPrice);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Size = new System.Drawing.Size(342, 215);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(342, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(150, 30);
            this.HeaderText.Text = "Sticker Price";
            // 
            // cmdCalculate
            // 
            this.cmdCalculate.AppearanceKey = "acceptButton";
            this.cmdCalculate.Location = new System.Drawing.Point(30, 139);
            this.cmdCalculate.Name = "cmdCalculate";
            this.cmdCalculate.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdCalculate.Size = new System.Drawing.Size(69, 48);
            this.cmdCalculate.TabIndex = 2;
            this.cmdCalculate.Text = "OK";
            this.cmdCalculate.Click += new System.EventHandler(this.cmdCalculate_Click);
            // 
            // txtPrice
            // 
            this.txtPrice.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrice.Location = new System.Drawing.Point(30, 79);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(196, 48);
            this.txtPrice.TabIndex = 1;
            this.txtPrice.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtPrice.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPrice_KeyPress);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(284, 29);
            this.Label3.Text = "ENTER THE STICKER PRICE OF THE VEHICLE";
            // 
            // frmSticker
            // 
            this.AcceptButton = this.cmdCalculate;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(342, 275);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSticker";
            this.Text = "Sticker Price";
            this.Load += new System.EventHandler(this.frmSticker_Load);
            this.Activated += new System.EventHandler(this.frmSticker_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSticker_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}