//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRB0000
{
	public partial class frmMultiple : BaseForm
    {
        private int highestRowChecked = 0;
        private bool headerInfoLoaded = false;
		public IMultipleVehicleViewModel ViewModel { get; set; }
        public void SetViewModel(IMultipleVehicleViewModel viewModel)
        {
            headerInfoLoaded = false;
            ViewModel = viewModel;
            ViewModel.VehicleConfigurationsReturned += ViewModel_VehicleConfigurationsReturned;
            ViewModel?.SetPageSize(40);
            lblLoading.Visible = true;
            lblLoading.Text = "Loading records 1 to 40";
           // vs1.ShowLoader = true;
            ViewModel?.LoadNext();
		}
		public frmMultiple()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
        }

        

        
        private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.Shown += FrmMultiple_Shown;
            vs1.Scroll += Vs1_Scroll;
            this.vs1.DoubleClick += vs1_DblClick;
        }

        private void Vs1_Scroll(object sender, ScrollEventArgs e)
        {
     //       var highestRow = vs1.Rows - 1;
     //       if (vs1.IsRowDisplayed(highestRow))
     //       {
     //           if (!(ViewModel?.IsLoadingDone?? false))
     //           {
     //               if (highestRowChecked < highestRow)
     //               {
     //                   highestRowChecked = highestRow;
     //                   vs1.ShowLoader = true;
					//	Application.StartTask(() => ViewModel.LoadNext());
					//}
     //           }
     //       }
        }

        private void FrmMultiple_Shown(object sender, EventArgs e)
        {
            if (showFinalFormImmediately)
            {
                vs1_DblClick(null, null);
                showFinalFormImmediately = false;
            }
        }

        public static frmMultiple InstancePtr
		{
			get
			{
				return (frmMultiple)Sys.GetInstance(typeof(frmMultiple));
			}
		}

		protected frmMultiple _InstancePtr = null;
		//=========================================================
		//int searchsize;
		//int searchsizemax;
		//int searchsizemin;
        private bool showFinalFormImmediately = false;
		bool MultipleFormLoaded;
        private IBlueBookRepository blueBookRepository;

		private void frmMultiple_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				vs1_DblClick(null, null);
			}
		}

		private void cmdBack_Click(object sender, System.EventArgs e)
		{
			this.Close();
			frmRedbook.InstancePtr.Show();
		}

		public void cmdBack_Click()
		{
			cmdBack_Click(cmdBack, new System.EventArgs());
		}

		private void frmMultiple_Activated(object sender, System.EventArgs e)
		{
			modGlobalRoutines.ForceFormToResize(this);
            vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vs1.Rows - 1, vs1.Cols - 1, 1);

   //         if (MultipleFormLoaded == true && this.Visible == true)
			//{
				vs1.Focus();
                this.Update();
			//}
            if (modRedBook.Statics.VehicleType == "X" && vs1.Rows == 2)
			{
				// there is only one vehicle found
				vs1.Select(1, 1);
                MultipleFormLoaded = false;
                showFinalFormImmediately = true;
            }
			vs1.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
        }

		private void frmMultiple_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				modRedBook.RedbookShow();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmMultiple_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			MultipleFormLoaded = true;
			modGlobalFunctions.SetTRIOColors(this);

            if (blueBookRepository == null)
            {
                blueBookRepository = StaticSettings.GlobalCommandDispatcher.Send(new GetBlueBookRepository()).Result;
            }

			if (modRedBook.Statics.VehicleType == "X")
			{
				// trailer
				vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.3014184));
				vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.0854287));
				vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.2143778));
				vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.0854287));
				vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.0854287));
				vs1.ColWidth(5, FCConvert.ToInt32(vs1.WidthOriginal * 0.1754287));
			}
			else if (modRedBook.Statics.VehicleType == "B")
			{
				// Truck Body
				vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
				vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
				vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
				vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.18));
				vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
			}
			else if (modRedBook.Statics.VehicleType == "R")
			{
				vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.0847278));
				vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.4554119));
				vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.3065473));
				vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.0847278));
			}
			else
			{
				vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.09));
				vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.2143778));
				vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.31));
				vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.0854287));
				if (vs1.Cols > 4)
				{
					vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.0854287));
					vs1.ColWidth(5, FCConvert.ToInt32(vs1.WidthOriginal * 0.0854287));
				}
				if (vs1.Cols > 6)
				{
					vs1.ColWidth(6, FCConvert.ToInt32(vs1.WidthOriginal * 0.0967117));
				}
			}
        }

        private void ViewModel_VehicleConfigurationsReturned(object sender, VehicleConfigurationsReturnedArgs e)
        {
           // vs1.ShowLoader = false;
			
            Application.Update(this);
			if (e != null)
            {
                if (e.ConfigurationSpecs.Any())
                {
      //              if (!headerInfoLoaded)
      //              {
						//LoadVehicleMetaInformation(e.ConfigurationSpecs.FirstOrDefault());
      //              }
					FillGrid(e.ConfigurationSpecs);
					vs1.Refresh();
                    lblLoading.Text = "Loading records " + (ViewModel.Offset + 1) + " to " +
                                      (ViewModel.Offset + ViewModel.PageSize);
                    Application.Update(this);
                    if (!ViewModel.IsLoadingDone)
                    {
                        
                        Application.StartTask(() => ViewModel.LoadNext());
                    }
                    else
                    {
                        lblLoading.Visible = false;
                    }
                }
                else
                {
                    lblLoading.Visible = false;
                }
            }
        }

        private void FillGrid(IEnumerable<VehicleConfigurationSpecs> configurationSpecs)
        {
            foreach (var configurationSpec in configurationSpecs)
            {
                AddToGrid(configurationSpec);
            }
        }

        private void AddToGrid(VehicleConfigurationSpecs configurationSpecs)
        {
            if (configurationSpecs == null)
            {
                return;
            }
            vs1.Rows += 1;
            var row = vs1.Rows - 1;

            vs1.RowData(row, configurationSpecs.ConfigurationId);
            vs1.TextMatrix(row, 0, configurationSpecs.GetSpecByName("ModelNumberFromVin"));
            vs1.TextMatrix(row, 1, configurationSpecs.ModelName);
			switch (configurationSpecs.ClassificationId)
            {
				case VehicleClassificationNumber.PassengerVehicles:
                    AddPassengerVehicleToGrid(configurationSpecs,row);
                    break;
				case VehicleClassificationNumber.CommercialTrucks:
                    AddCommercialTruckToGrid(configurationSpecs, row);
                    break;
				case VehicleClassificationNumber.Powersport:
                    AddMotorcycleToGrid(configurationSpecs, row);
                    break;
				case VehicleClassificationNumber.CommercialTrailers:
                    AddTrailerToGrid(configurationSpecs,row);
                    break;
				case VehicleClassificationNumber.TruckBodies:
                    AddTruckBodyToGrid(configurationSpecs, row);
                    break;
				case VehicleClassificationNumber.RecreationalVehicles:
                    AddRecreationalVehicleToGrid(configurationSpecs, row);
                    break;
            }
        }

        private void AddTrailerToGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var trailer = vSpecs.ToTrailerData();
            //vs1.TextMatrix(row, 0, trailer.Manufacturer);
            //vs1.TextMatrix(row, 1, trailer.Year.ToString());
            vs1.TextMatrix(row, 2, trailer.Section);
            vs1.TextMatrix(row, 3, trailer.Width);
            vs1.TextMatrix(row, 4, trailer.Length);
            //vs1.TextMatrix(row, 5, trailer.Vin);
            vs1.RowData(row, trailer.Id);
        }

        private void AddTruckBodyToGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var truckBodyData = vSpecs.ToTruckBodyData();
            vs1.TextMatrix(row, 0, truckBodyData.Manufacturer);
            //vs1.TextMatrix(row, 1, truckBodyData.Year.ToString());
            //frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, truckBodyData.Model);
            vs1.TextMatrix(row, 2, truckBodyData.Type);
            vs1.TextMatrix(row, 3, truckBodyData.Description);
        }

        private void AddRecreationalVehicleToGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var rv = vSpecs.ToRv();
   
            if (rv.Inches.ToIntegerValue() != 0)
            {
                vs1.TextMatrix(row, 0, rv.Feet.GetValueOrDefault() + "' " + rv.Inches.Trim() + FCConvert.ToString(Convert.ToChar(34)));
            }
            else
            {
                vs1.TextMatrix(row, 0, rv.Feet.GetValueOrDefault() + "'");
            }
            vs1.TextMatrix(row, 2, rv.Description);
            vs1.TextMatrix(row, 3, rv.Weight);

		}

        private void AddPassengerVehicleToGrid(VehicleConfigurationSpecs vSpecs,int row)
        {
            vs1.TextMatrix(row, 2, vSpecs.GetSpecByName("description"));
            vs1.TextMatrix(row, 3, vSpecs.GetSpecByName("engineSize"));
            vs1.TextMatrix(row, 4, vSpecs.GetSpecByName("transmissionType"));
            vs1.TextMatrix(row, 5, vSpecs.GetSpecByName("gvw"));
            vs1.TextMatrix(row, 6, vSpecs.GetSpecByName("passengers"));

		}

        private void AddCommercialTruckToGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var heavyTruck = vSpecs.ToHeavyTruck();
            vs1.TextMatrix(row, 2, heavyTruck.Description);
            vs1.TextMatrix(row, 3, heavyTruck.EngineDescription);
            vs1.TextMatrix(row, 4, heavyTruck.TransmissionDescription);
            vs1.TextMatrix(row, 5, heavyTruck.GrossVehicleWeight);
            vs1.TextMatrix(row, 6, heavyTruck.Wheelbase);

        }

        private void AddMotorcycleToGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var cycle = vSpecs.ToCycle();
            vs1.TextMatrix(row, 0, cycle.ModelNumber);
            vs1.TextMatrix(row, 1, cycle.Cylinders);
            vs1.TextMatrix(row, 2, cycle.Description);
            vs1.TextMatrix(row, 3, cycle.Displacement);
            vs1.TextMatrix(row, 4, cycle.Weight);
            vs1.TextMatrix(row, 6, cycle.Color);
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
				modRedBook.RedbookShow();
		}

		private void frmMultiple_Resize(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			if (modRedBook.Statics.VehicleType == "X")
			{
                // trailer
                vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
                vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.3014184));
				
				vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
				vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.12));
				vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.12));
				//vs1.ColWidth(5, FCConvert.ToInt32(vs1.WidthOriginal * 0.1754287));
			}
			else if (modRedBook.Statics.VehicleType == "B")
			{
				// Truck Body
				vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
				vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.2));
				vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.2));
				vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.28));
				//vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.28));
			}
			else if (modRedBook.Statics.VehicleType == "R")
			{
				vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.0847278));
				vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.4554119));
				vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.3065473));
				vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.0847278));
			}
			else
			{
				vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.09));
				vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
				vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.31));
				vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.13));
				if (vs1.Cols > 4)
				{
					vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.13));
					vs1.ColWidth(5, FCConvert.ToInt32(vs1.WidthOriginal * 0.08));
				}
				if (vs1.Cols > 6)
				{
					vs1.ColWidth(6, FCConvert.ToInt32(vs1.WidthOriginal * 0.07));
				}
			}
		}

		private void mnuFileBack_Click(object sender, System.EventArgs e)
		{
			if (cmdBack.Enabled)
			{
				cmdBack_Click();
			}
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			// vehicleselectedfrommultiple will contain the unique
			// ID number from the rbcars table.
            // vbPorter upgrade warning: Number As int	OnWriteFCConvert.ToInt32(
			int Number = 0;
			modRedBook.Statics.VehicleMediumTruck = false;
			if (vs1.MouseRow > 0 || modRedBook.Statics.VehicleType == "X")
			{
				modRedBook.Statics.FromMultiple = true;
				Number = vs1.Row;
				if (Number == 0)
					return;
				if (vs1.Row > 0)
				{
					modRedBook.Statics.VehicleSelectedFromMultiple = FCConvert.ToInt32(vs1.RowData(vs1.Row));
				}
				else
				{
					MessageBox.Show("Invalid row selection.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				//modRedBook.GetAllVehicleInfo();
				// this info  will be used after the options screen
				//clsDRWrapper rsCheckType = new clsDRWrapper();
                var vehicleSpecs = blueBookRepository.VehicleSpecs.GetVehicleSpecs(modRedBook.Statics
                    .VehicleSelectedFromMultiple);
                
				if (vehicleSpecs != null)
                {
                    modRedBook.Statics.CurrentVehicleObject = vehicleSpecs;
					var equipmentCode = vehicleSpecs.GetSpecByName("equipmentCode");
                    if ((modRedBook.Statics.VehicleType == "C") || (modRedBook.Statics.VehicleType == "T"))
                    {
                        modRedBook.Statics.StandardOptions = vehicleSpecs.GetSpecByName("equipmentCode");
                        modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                        modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                        modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                        modRedBook.GetAllRbCarInfo(vehicleSpecs);

                        if ((equipmentCode == "S") || (equipmentCode == "F"))
                        {
                            modRedBook.Statics.VehicleType = "T";
                        }
                        else if (equipmentCode.IsNullOrWhiteSpace())
                        {
                            this.Hide();
                            frmFinal.InstancePtr.CameFrom = "Multiple";
                            frmFinal.InstancePtr.Show();
                            return;
                        }
                        else
                        {
                            modRedBook.Statics.VehicleType = "C";
                        }

                        this.Hide();
                        var configurationOptions = blueBookRepository.Options.GetOptions(
                            new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                        if (configurationOptions.Options.Any())
                        {
                            frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                            frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                            frmNewCarOptions.InstancePtr.Show();
                        }
                        else
                        {
                            frmFinal.InstancePtr.CameFrom = "Multiple";
                            frmFinal.InstancePtr.Show();
                            return;
                        }
                    }
                    else if (modRedBook.Statics.VehicleType == "H")
                    {
                        
                        modRedBook.Statics.VehicleType = "H";
                        modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                        modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                        modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                        modRedBook.Statics.VehicleMediumTruck = false; //vehicleSpecs.//heavyTruck.Type == "M";
                        modRedBook.Statics.FromMultiple = true;
                        
                        this.Hide();
                        var configurationOptions = blueBookRepository.Options.GetOptions(
                            new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                        if (configurationOptions.Options.Any())
                        {
                            frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                            frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                            frmNewCarOptions.InstancePtr.Show();
                        }
                        else
                        {
                            frmFinal.InstancePtr.CameFrom = "Multiple";
                            frmFinal.InstancePtr.Show();
                            return;
                        }
                    }
                    else if (modRedBook.Statics.VehicleType == "M")
                    {

                        modRedBook.Statics.VehicleType = "M";
                        modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                        modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                        modRedBook.Statics.VehicleModel = vehicleSpecs.Description;
                        modRedBook.Statics.VehicleMake = vehicleSpecs.ManufacturerName;
                        modRedBook.GetAllMotorCycleInfo(vehicleSpecs);

                            this.Hide();
                            frmFinal.InstancePtr.CameFrom = "Multiple";
                        frmFinal.InstancePtr.Show();
                    }
                    else if (modRedBook.Statics.VehicleType == "R")
                    {
                        var rv = vehicleSpecs.ToRv();// blueBookRepository.Rvs.GetRv(modRedBook.Statics.VehicleSelectedFromMultiple);
                        modRedBook.Statics.VehicleType = "R";
                        modRedBook.Statics.VehicleIDNumber = rv.Id;
                        modRedBook.Statics.VehicleYear = rv.Year.ToIntegerValue();
                        modRedBook.Statics.VehicleModel = rv.Description;
                        modRedBook.Statics.VehicleMake = rv.Manufacturer;
                        modRedBook.Statics.VModel = rv.Model;
                        modRedBook.Statics.VYear = rv.Year;
                        modRedBook.Statics.VDescription = rv.Description;
                        modRedBook.Statics.VFeet = rv.Feet.GetValueOrDefault();
                        modRedBook.Statics.VInches = rv.Inches;
                        modRedBook.Statics.VGrossVehicleWeight = rv.Weight;
                        modRedBook.Statics.VSRP = rv.Srp;
                        modRedBook.Statics.VManufacturer = rv.Manufacturer;
                        this.Hide();
                        frmFinal.InstancePtr.CameFrom = "Multiple";
                        frmFinal.InstancePtr.Show();
                    }
                    else if (modRedBook.Statics.VehicleType == "X")
                    {
                        // Trailers
                        var trailer = blueBookRepository.TrailerData.GetTrailerData(modRedBook.Statics
                            .VehicleSelectedFromMultiple);
                        modRedBook.Statics.VehicleType = "X";
                        modRedBook.Statics.VehicleIDNumber = trailer.Id;
                        modRedBook.Statics.VehicleYear = trailer.Year.GetValueOrDefault();
                        modRedBook.Statics.VehicleModel = trailer.Manufacturer;
                        modRedBook.Statics.FromMultiple = true;
                        modRedBook.GetAllTrailerDataInfo(trailer);
                        this.Hide();
                        frmFinal.InstancePtr.CameFrom = "Multiple";
                        frmFinal.InstancePtr.Show();
                    }
                    else if (modRedBook.Statics.VehicleType == "B")
                    {
						// Truck Body
                        modRedBook.Statics.VehicleType = "B";
                        modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                        modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                        modRedBook.Statics.VManufacturer = vehicleSpecs.ManufacturerName;
                        modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                        modRedBook.Statics.VDescription = vehicleSpecs.Description;
                        modRedBook.Statics.FromMultiple = true;
                        modRedBook.Statics.VSRP = vehicleSpecs.GetSpecByName("msrp");
                        this.Hide();
                        var configurationOptions = blueBookRepository.Options.GetOptions(
                            new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                        if (configurationOptions.Options.Any())
                        {
                            frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                            frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                            frmNewCarOptions.InstancePtr.Show();
                        }
                        else
                        {
                            frmFinal.InstancePtr.CameFrom = "Multiple";
                            frmFinal.InstancePtr.Show();
                            return;
                        }
                    }
                }
            }
		}
	}
}
