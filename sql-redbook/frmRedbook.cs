//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using System.Linq;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.HeavyTrucks;
using SharedApplication.BlueBook.Services;
using SharedApplication.Extensions;
using TWSharedLibrary;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.Rbcars;
using SharedApplication.BlueBook.Rvs;
using SharedApplication.BlueBook.TruckBodies;
using SharedApplication.TaxCollections.AccountPayment;
using TWRB0000.commands;

namespace TWRB0000
{
    public partial class frmRedbook : BaseForm
    {
        //private VehicleSearchViewModel viewModel;
        public frmRedbook()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        public IVehicleSearchViewModel ViewModel { get; set; }
        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            lstMake.SelectedIndexChanged += LstMake_SelectedIndexChanged;
        }

        private void LstMake_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadModelList();
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmRedbook InstancePtr
        {
            get { return (frmRedbook) Sys.GetInstance(typeof(frmRedbook)); }
        }

        protected frmRedbook _InstancePtr = null;
        bool MainFormLoaded;
        bool initialization = true;
        int fnx;
        bool blnYearSet;
        int lngKey;
        private IBlueBookRepository blueBookRepository;

        //private void SearchByVin()
        //{
        //    if (txtVIN.Text.Length < 17)
        //    {
        //        switch (txtVIN.Text)
        //        {
        //            //case "11":
        //            //    txtVIN.Text = "1FMCU9GX2DUA41457";
        //            //    break;
        //            //case "12":
        //            //    txtVIN.Text = "1GTR2VE76CZ213183";
        //            //    break;
        //            //case "13":
        //            //    txtVIN.Text = "JN8AS5MV7CW401309";
        //            //    break;
        //            //case "14":
        //            //    txtVIN.Text = "5FNRL5H64CB025560";
        //            //    break;
        //            case "L":
        //                cSettingsController set = new cSettingsController();
        //                var lastvin = set.GetSettingValue("LastVIN", "RedBook", "User", TWSharedLibrary.Variables.Statics.UserID, "");
        //                frmWait.InstancePtr.Unload();
        //                if (lastvin.IsNullOrWhiteSpace())
        //                {
        //                    MessageBox.Show("There is no stored last VIN used.", "No Stored VIN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //                }
        //                else
        //                {
        //                    txtVIN.Text = lastvin;
        //                }
        //                return;
        //                break;
        //            default:
        //                MessageBox.Show("The VIN you have entered is less than 17 characters long. Please check your number.", "VIN Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //                txtVIN.Focus();
        //                return;
        //        }
        //    }

        //    var vehicles = blueBookRepository.VehicleInformation.GetVehicleInformation(txtVIN.Text).ToList();
        //    if (!vehicles.Any())
        //    {
        //        MessageBox.Show("No vehicle found for that VIN", "No Vehicle Found", MessageBoxButtons.OK);
        //        return;
        //    }

        //    modRedBook.Statics.VIN = txtVIN.Text;
        //    ShowVehicles(vehicles);

        //}


        //private void ShowVehicles(IEnumerable<VehicleInformation> vehicles)
        //{
        //    if (!vehicles.Any())
        //    {
        //        return;
        //    }
        //    var vehicle = vehicles.FirstOrDefault();
        //    var vehicleAllowed = false;
        //    var vehicleTypeDescription = "";
        //    switch ((VehicleClassification)vehicle.ClassificationId)
        //    {
        //        case VehicleClassification.CommercialTrucks:
        //            if (modRedBook.Statics.HeavyTruck )
        //            {
        //                vehicleAllowed = true;
        //            }

        //            modRedBook.Statics.VehicleType = "H";
        //            vehicleTypeDescription = "heavy trucks";
        //            break;
        //        case VehicleClassification.CommercialTrailers:
        //            if (modRedBook.Statics.Trailer)
        //            {
        //                vehicleAllowed = true;
        //            }

        //            modRedBook.Statics.VehicleType = "X";
        //            vehicleTypeDescription = "commercial trailers";
        //            break;
        //        case VehicleClassification.PassengerVehicles:
        //            if (modRedBook.Statics.Auto)
        //            {
        //                vehicleAllowed = true;
        //            }

        //            modRedBook.Statics.VehicleType = "C";
        //            vehicleTypeDescription = "automobiles and light trucks";
        //            break;
        //        case VehicleClassification.Powersport:
        //            if (modRedBook.Statics.Cycle)
        //            {
        //                vehicleAllowed = true;
        //            }

        //            modRedBook.Statics.VehicleType = "M";
        //            vehicleTypeDescription = "motorcycles";
        //            break;
        //        case VehicleClassification.RecreationalVehicles:
        //            if (modRedBook.Statics.RV)
        //            {
        //                vehicleAllowed = true;
        //            }

        //            modRedBook.Statics.VehicleType = "R";
        //            vehicleTypeDescription = "recreational vehicles";
        //            break;
        //        case VehicleClassification.TruckBodies:
        //            if (modRedBook.Statics.TruckBody)
        //            {
        //                vehicleAllowed = true;
        //            }

        //            modRedBook.Statics.VehicleType = "B";
        //            vehicleTypeDescription = "truck bodies";
        //            break;
        //        default:
        //            vehicleAllowed = false;
        //        break;
        //    }

        //    if (!vehicleAllowed)
        //    {
        //        FCMessageBox.Show("You are not set up to access " + vehicleTypeDescription, MsgBoxStyle.Exclamation,
        //            "Invalid vehicle type");
        //        return;
        //    }

        //    var configurations = blueBookRepository.VehicleConfigurations.GetConfigurations(
        //            new VehicleConfigurationFilter(vehicle.ModelId, 0, "", "",0,0));

        //    modRedBook.Statics.VehicleYear = vehicle.ModelYear;
        //    modRedBook.Statics.VehicleModel = vehicle.ModelName;

        //    if (vehicles.Count() > 1 )
        //    {
        //        FillMultipleVehicleListBox(vehicles);
        //        var multView = new MultipleVehicleViewModel(blueBookRepository);
        //        multView.SetConfigurations(configurations);
        //        //this.Hide();
        //        //frmMultiple.InstancePtr.Show();
        //        //frmMultiple.InstancePtr.vs1.Focus();
        //        this.Hide();
        //        StaticSettings.GlobalCommandDispatcher.Send(new ShowMultipleVehicleModels());
        //    }
        //    else
        //    {
        //        ShowOptions(vehicle.ConfigurationId);
        //    }
        //}

        private void cmdGetVehicle_Click(object sender, System.EventArgs e)
    {
        frmMultiple.InstancePtr.Unload();
        frmNewCarOptions.InstancePtr.Unload();
        modRedBook.Statics.FromMultiple = false;

            if (txtVIN.Text.HasText() && txtVIN.Text != "*")
            {
                ViewModel.Vin = txtVIN.Text;
                if (ViewModel.SearchByVin())
                {
                        Hide();
                        return;
                }
                else
                {
                    txtVIN.Focus();
                }
                //SearchByVin();
            }
            else
            {
                NewVehicleSearch();
            }
            return;
    }

    public void cmdGetVehicle_Click()
    {
        cmdGetVehicle_Click(cmdGetVehicle, new System.EventArgs());
    }

    private void cmdQuit_Click(object sender, System.EventArgs e)
    {
        List<FCForm> twrbForms = new List<FCForm>();
        foreach (FCForm frm in FCGlobal.Statics.Forms)
        {
            if (frm.GetType().Assembly.FullName.ToUpper().StartsWith("TWRB0000"))
            {
                twrbForms.Add(frm);
            }
        }

        while (twrbForms.Count > 0)
        {
            twrbForms[0].Close();
            twrbForms.RemoveAt(0);
        }
    }

        public void cmdQuit_Click()
        {
            cmdQuit_Click(cmdQuit, new System.EventArgs());
        }

        private void cmdSticker_Click(object sender, System.EventArgs e)
        {
            if (txtYear.Text != "")
            {
	            modRedBook.Statics.FromMultiple = false;
	            modRedBook.Statics.StickerButton = true;
                frmFinal.InstancePtr.Show();
            }
            else
            {
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Please enter a Year for the Sticker Price.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void cmdSticker_Click()
        {
            cmdSticker_Click(cmdSticker, new System.EventArgs());
        }


        private void frmRedbook_Activated(object sender, System.EventArgs e)
        {
            modRedBook.Statics.FromMultiple = false;
            if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
            {
                return;
            }

            if (!blnYearSet)
            {
                blnYearSet = true;
                txtYear.SelectedIndex = 2;
            }
            frmMultiple.InstancePtr.Unload();
            ClearLocalVariables();
            modRedBook.ClearVariables();
            cmbCommercialTrailer.Enabled = true;

            cmbCommercialTrailer.Clear();
            if (modRedBook.Statics.Auto == true)
            {
                if (!cmbCommercialTrailer.Items.Contains("Automobile / Light Duty Truck"))
                {
                    cmbCommercialTrailer.Items.Add("Automobile / Light Duty Truck");
                }
                //if (!cmbCommercialTrailer.Items.Contains("Light Duty Truck"))
                //{
                //    cmbCommercialTrailer.Items.Add("Light Duty Truck");
                //}
            }
            if (modRedBook.Statics.HeavyTruck == true)
            {
                if (!cmbCommercialTrailer.Items.Contains("Heavy Truck"))
                {
                    cmbCommercialTrailer.Items.Add("Heavy Truck");
                }
            }
            if (modRedBook.Statics.Cycle == true)
            {
                if (!cmbCommercialTrailer.Items.Contains("Motorcycles"))
                {
                    cmbCommercialTrailer.Items.Add("Motorcycles");
                }
            }
            if (modRedBook.Statics.RV == true)
            {
                if (!cmbCommercialTrailer.Items.Contains("Recreational Vehicles"))
                {
                    cmbCommercialTrailer.Items.Add("Recreational Vehicles");
                }
            }
            if (modRedBook.Statics.TruckBody == true)
            {
                if (!cmbCommercialTrailer.Items.Contains("Truck Body"))
                {
                    cmbCommercialTrailer.Items.Add("Truck Body");
                }
            }
            if (modRedBook.Statics.Trailer == true)
            {
                if (!cmbCommercialTrailer.Items.Contains("Commercial Trailer"))
                {
                    cmbCommercialTrailer.Items.Add("Commercial Trailer");
                }
            }

            if (cmbCommercialTrailer.Items.Count > 0)
            {
                if (cmbCommercialTrailer.SelectedIndex < 0)
                {
                    cmbCommercialTrailer.SelectedIndex = 0;
                }
            }
            txtVIN.Focus();
            //FC:FINAL:BSE:#4370 - disable selected index changed when form is first loaded
            initialization = false;
            //if (modRedBook.Statics.MVFlag == true)
            //{
            //    int ii;
            //    cSettingsController set = new cSettingsController();
            //    var vin = (set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK", "") != "" ? set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK", "") : "");
            //    if (vin.HasText())
            //    {
            //        SelectVIN(vin);
            //        return;
            //    }

            //    var savedYear = set.GetSettingValue("YEAR", "TWGNENTY", "REDBOOK", "");
            //    if (savedYear.HasText())
            //    {
            //        SetYear(savedYear.ToIntegerValue());
            //        var make = set.GetSettingValue("MAKE", "TWGNENTY", "REDBOOK", "").Left(4).ToLower();
            //        for (ii = 0; ii <= lstMake.Items.Count - 1; ii++)
            //        {
            //            if (Strings.Mid(lstMake.Items[ii].ToString(), 1, 4).ToLower() == make)
            //            {
            //                lstMake.SelectedIndex = ii;
            //                break;
            //            }
            //        }

            //        if (lstMake.Text.HasText())
            //        {
            //            var model = set.GetSettingValue("MODEL", "TWGNENTY", "REDBOOK", "");
            //            if (model.HasText())
            //            {
            //                SetModel(model);
            //            }
            //        }
            //    }
            //}

            
        }

        private void SetYear(int year)
        {
            if (year < 1900)
            {
                return;
            }
            for (int x = 0; x <= txtYear.Items.Count;x ++)
            {
                if (txtYear.Items[x].ToString() == year.ToString())
                {
                    txtYear.SelectedIndex = x;
                    break;
                }
            }
        }

        //private void SelectVIN(string vin)
        //{
        //    txtVIN.Text = vin;
        //    SearchByVin();
        //}

        private void SetModel(string modelName)
        {
            for (int x = 0; x < cmbModels.Items.Count; x++)
            {
                if (cmbModels.Items[x].ToString().ToLower() == modelName.ToLower())
                {
                    cmbModels.SelectedIndex = x;
                    break;
                }
            }
        }

        private void frmRedbook_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                cmdQuit_Click();
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmRedbook_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (blueBookRepository == null)
                {
                    blueBookRepository =
                        StaticSettings.GlobalCommandDispatcher.Send(new GetBlueBookRepository()).Result;
                }
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Unable to create API connector" + "/r/n" + ex.Message,
                    MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Error");
            }


            MainFormLoaded = true;
           // LoadCarMakeList();
            LoadYearList();
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            blnYearSet = false;
            

            initialization = false;
        }

        public void LoadCarMakeList()
        {
            var makes = blueBookRepository.Makes.GetCarAndLightTruckManufacturers().ToList();
            lstMake.Clear();
            if (makes.Any())
            {
                foreach (var make in makes)
                {
                    lstMake.AddItem(make);
                }
            }
            //lstMake.SelectedIndex = 0;
        }


       

    public void FillMultipleVehicleListBox(IEnumerable<VehicleInformation> vehicles)
        {
            frmMultiple.InstancePtr.vs1.Rows = 1;
            int row = 0;
            var isFirst = true;
            foreach (var vehicle in vehicles)
            {
                var vehicleSpecs = blueBookRepository.VehicleSpecs.GetVehicleSpecs(vehicle.ConfigurationId);
                if (vehicleSpecs != null)
                {
                    if (isFirst)
                    {
                        SetupGrid(vehicleSpecs.ClassificationId);
                        FillMultipleMakeInformation(vehicle.ManufacturerName, vehicle.ModelYear.ToString());
                    //frmMultiple.InstancePtr.lblMfg.Text = vehicle.ManufacturerName;
                    //    frmMultiple.InstancePtr.lblYear.Text = vehicle.ModelYear.ToString();
                        modRedBook.Statics.StandardOptions = vehicleSpecs.Specs.FirstOrDefault(s => s.Name == "equipmentCode")?.GetValue() ?? "";
                        isFirst = false;
                    }
                    AddToMultipleGrid(vehicleSpecs, vehicle.VinModelNumber);
                }
            }


            frmMultiple.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, frmMultiple.InstancePtr.vs1.Rows - 1, 6, 1);
        }

        private void AddToMultipleGrid(VehicleConfigurationSpecs vehicleSpecs,string vinModelNumber)
        {
            var specs = vehicleSpecs.Specs;
            frmMultiple.InstancePtr.vs1.Rows++;
            int row = frmMultiple.InstancePtr.vs1.Rows - 1;
            modRedBook.Statics.VehiclesInGrid[row] = vehicleSpecs.ConfigurationId;
            lngKey = vehicleSpecs.ConfigurationId;
            frmMultiple.InstancePtr.vs1.RowData(row, lngKey);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, vinModelNumber);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, vehicleSpecs.ModelName);
            switch ( vehicleSpecs.ClassificationId)
            {
                case VehicleClassificationNumber.PassengerVehicles:
                    AddPassengerToMultipleGrid( vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.CommercialTrucks:
                    AddHeavyTruckToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.Powersport:
                    AddMotorcycleToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.RecreationalVehicles:
                    AddRecreationalVehicleToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.TruckBodies:
                    AddTruckBodyToMultipleGrid(vehicleSpecs, row);
                    break;
                case VehicleClassificationNumber.CommercialTrailers:
                    AddTrailerToMultipleGrid(vehicleSpecs, row);
                    break;
            }
        }

        private void AddTrailerToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var trailer = vSpecs.ToTrailerData();
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, trailer.Manufacturer);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, trailer.Year.ToString());
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, trailer.Section);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, trailer.Width);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, trailer.Length);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 5, trailer.Vin);
        }

        private void AddTruckBodyToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var truckBodyData = vSpecs.ToTruckBodyData();
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, truckBodyData.Manufacturer);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, truckBodyData.Year.ToString());
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, "");
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, truckBodyData.Type);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, truckBodyData.Description);
        }

        private void AddRecreationalVehicleToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var rv = vSpecs.ToRv();
            if (rv.Inches.ToIntegerValue() != 0)
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, rv.Feet.GetValueOrDefault() + "' " + rv.Inches.Trim() + FCConvert.ToString(Convert.ToChar(34)));
            }
            else
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, rv.Feet.GetValueOrDefault() + "'");
            }
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, rv.Model);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, rv.Description);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, rv.Weight);
        }

        private void AddMotorcycleToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var cycle = vSpecs.ToCycle();
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, cycle.ModelNumber);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, cycle.Cylinders);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, cycle.Description);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, cycle.Displacement);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, cycle.Weight);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 6, cycle.Color);
        }

        private void AddHeavyTruckToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            var specs = vSpecs.Specs;
            var heavyTruck = vSpecs.ToHeavyTruck();
            //frmMultiple.InstancePtr.vs1.TextMatrix(row, 0, heavyTruck.ModelNumber);
            //frmMultiple.InstancePtr.vs1.TextMatrix(row, 1, heavyTruck.Make);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2, heavyTruck.Description);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, heavyTruck.EngineDescription);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, heavyTruck.TransmissionDescription);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 5, heavyTruck.GrossVehicleWeight);
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 6, heavyTruck.Wheelbase);
            //frmMultiple.InstancePtr.vs1.TextMatrix(row, 6, heavyTruck.Year);
        }

        private void AddPassengerToMultipleGrid(VehicleConfigurationSpecs vSpecs, int row)
        {
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 2,  vSpecs.GetSpecByName("description"));
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 3, vSpecs.GetSpecByName( "engineSize"));
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 4, vSpecs.GetSpecByName( "transmissionType"));
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 5,  vSpecs.GetSpecByName("gvw"));
            frmMultiple.InstancePtr.vs1.TextMatrix(row, 6, vSpecs.GetSpecByName("passengers"));
            //frmMultiple.InstancePtr.vs1.TextMatrix(row, 6, vSpecs.ModelYear.ToString());
        }

        private void SetupGrid(VehicleClassificationNumber vehicleType)
        {
            frmMultiple.InstancePtr.vs1.Rows = 1;
            switch (vehicleType)
            {
                case VehicleClassificationNumber.PassengerVehicles:
                    SetMultipleGridForPassengerVehicles();
                    break;
                case VehicleClassificationNumber.CommercialTrailers:
                    SetMultipleGridForTrailers();
                    break;
                case VehicleClassificationNumber.CommercialTrucks:
                    SetMultipleGridForTrucks();
                    break;
                case VehicleClassificationNumber.Powersport:
                    SetMultipleGridForMotorcycles();
                    break;
                case VehicleClassificationNumber.RecreationalVehicles:
                    SetMultipleGridForRecreationalVehicles();
                    break;
            }
        }

        private void FillMultipleMakeInformation(string manufacturer, string year)
        {
            try
            {
                frmMultiple.InstancePtr.lblYear.Text = year;
                modRedBook.Statics.VehicleMake = manufacturer;
                frmMultiple.InstancePtr.lblMfg.Text = manufacturer;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error trying to show multiple options in FillMultipleMakeInformation",
                    MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Error");
            }
        }

        private string TypeCodeFromVehicleClassification((int classificationId, int categoryId) vehicleIds)
        {
            switch (vehicleIds.classificationId)
            {
                case (int)VehicleClassificationNumber.CommercialTrucks:
                    return "H";
                case (int)VehicleClassificationNumber.PassengerVehicles:
                    return "C";
                case (int)VehicleClassificationNumber.Powersport:
                    return "M";
                case (int)VehicleClassificationNumber.CommercialTrailers:
                    return "X";
                case (int)VehicleClassificationNumber.RecreationalVehicles:
                    return "R";
                case (int)VehicleClassificationNumber.TruckBodies:
                    return "B";
            }

            if (vehicleIds.categoryId == (int) VehicleCategoryNumber.MotorCycles)
            {
                return "M";
            }
            return "";
        }
        public void NewVehicleSearch()
        {
            modRedBook.Statics.SearchYear = txtYear.Text;
            modRedBook.Statics.SearchMake = lstMake.Text;
            modRedBook.Statics.SearchModel = cmbModels.Text;//txtModel.Text;
            if (modRedBook.Statics.SearchYear.IsNullOrWhiteSpace() ||
                modRedBook.Statics.SearchMake.IsNullOrWhiteSpace() || modRedBook.Statics.SearchYear.ToIntegerValue() < 1900)
            {
                MessageBox.Show("Please enter a Year / Make for your search.", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var currentVehicleType = GetCurrentVehicleType();

            if (modRedBook.Statics.SearchModel.IsNullOrWhiteSpace() && currentVehicleType.classificationId != VehicleClassificationNumber.RecreationalVehicles.ToInteger())
            {
                MessageBox.Show("Please enter a model for your search.", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            modRedBook.Statics.SearchMake = lstMake.Text.Trim();
            modRedBook.Statics.SearchType = TypeCodeFromVehicleClassification(currentVehicleType);
            modRedBook.Statics.VehicleType = modRedBook.Statics.SearchType;
            var configurations = blueBookRepository.VehicleConfigurations.GetConfigurations(
                new VehicleConfigurationFilter(0, modRedBook.Statics.SearchYear.ToIntegerValue(),
                    modRedBook.Statics.SearchModel, modRedBook.Statics.SearchMake, currentVehicleType.categoryId,
                    currentVehicleType.classificationId));
            if (configurations.Any())
            {
                var firstVehicle = configurations.FirstOrDefault();
                if (configurations.Count() > 1)
                {
                    modRedBook.Statics.VehicleIDNumber = firstVehicle.ConfigurationId;
                    FillMultipleMakeInformation(lstMake.Text, txtYear.Text);
                    SetMultipleGrid((VehicleClassificationNumber)currentVehicleType.classificationId);
                    
                    var multView = new MultipleVehicleViewModel(blueBookRepository);
                    multView.SetConfigurations(configurations);
                    this.Hide();
                    frmMultiple.InstancePtr.Show();
                    frmMultiple.InstancePtr.SetViewModel(multView);
                }
                else
                {
                    ShowOptions(firstVehicle.ConfigurationId);
                }
            }
            else
            {
                MessageBox.Show("There was no match for your search.", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return;
        }


        private void SetMultipleGrid(VehicleClassificationNumber classification)
        {
            switch (classification)
            {
                case VehicleClassificationNumber.CommercialTrucks:
                    SetMultipleGridForTrucks();
                    break;
                case VehicleClassificationNumber.PassengerVehicles:
                    SetMultipleGridForPassengerVehicles();
                    break;
                case VehicleClassificationNumber.Powersport:
                    SetMultipleGridForMotorcycles();
                    break;
                case VehicleClassificationNumber.RecreationalVehicles:
                    SetMultipleGridForRecreationalVehicles();
                    break;
                case VehicleClassificationNumber.CommercialTrailers:
                    SetMultipleGridForTrailers();
                    break;
                case VehicleClassificationNumber.TruckBodies:
                    SetMultipleGridForTruckBodies();
                    break;
            }
        }
        private void SetMultipleGridForTruckBodies()
        {
            frmMultiple.InstancePtr.vs1.ColWidth(0, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2100);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(5, 0);
            frmMultiple.InstancePtr.vs1.ColWidth(6, 0);
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Manufacturer");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Type");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Description");
        }
        private void SetMultipleGridForTrailers()
        {
            frmMultiple.InstancePtr.vs1.ColWidth(0, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2100);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(5, 0);
            frmMultiple.InstancePtr.vs1.ColWidth(6, 0);
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Vin");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Section");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Width");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 4, "Length");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 5, "");
        }
        public void SetMultipleGridForMotorcycles(bool showYear = false)
        {
            frmMultiple.InstancePtr.vs1.ColWidth(0, 1000);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2100);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 1400);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Mdl #");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Cylinders");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Displacement");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 4, "Weight");
            if (showYear)
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "Year");
            }
            else
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "Color");
            }
        }

        public void SetMultipleGridForPassengerVehicles(bool showYear = false)
        {
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Mdl #");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Engine");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 4, "Trans.");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 5, "G.V.W.");
            if (showYear)
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "Year");
            }
            else
            {
                frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "# of Pass.");
            }

            frmMultiple.InstancePtr.vs1.ColWidth(0, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2000);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(5, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(6, 900);
        }
        public void SetMultipleGridForTrucks()
        {
            frmMultiple.InstancePtr.vs1.ColWidth(0, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(1, 2100);
            frmMultiple.InstancePtr.vs1.ColWidth(2, 2800);
            frmMultiple.InstancePtr.vs1.ColWidth(3, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(4, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(5, 800);
            frmMultiple.InstancePtr.vs1.ColWidth(6, 800);
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Mdl #");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Engine Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 4, "Trans. Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 5, "G.V.W.");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 6, "WheelBase");
        }
        
        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
			//FC:FINAL:DSE:#i2191 clear static variables on exit
			modRedBook.Statics.StickerFormLoaded = false;
            if (modRedBook.Statics.bolClearing == true)
                return;
        }

        private void lstMake_Enter(object sender, System.EventArgs e)
        {
            cmbCommercialTrailer.Enabled = true;
        }

        private void mnuFileExit_Click(object sender, System.EventArgs e)
        {
            cmdQuit_Click();
        }

        private void mnuFileGet_Click(object sender, System.EventArgs e)
        {
            if (cmdGetVehicle.Enabled)
            {
                cmdGetVehicle_Click();
            }
        }

        private void mnuFileSticker_Click(object sender, System.EventArgs e)
        {
            if (cmdSticker.Enabled)
            {
                cmdSticker_Click();
            }
        }

        private void optCarLightTruck_CheckedChanged(object sender, System.EventArgs e)
        {
            txtRVSize.Enabled = false;
            cmbModels.Enabled = true;
            txtRVSize.Text = "";
            LoadCarMakeList();
            lstMake.Focus();
        }

        private void optCommercialTrailer_CheckedChanged(object sender, System.EventArgs e)
        {
            LoadTrailerMakeList();
            //cmbModels.Enabled = false;
        }

        private void optCycles_CheckedChanged(object sender, System.EventArgs e)
        {
            txtRVSize.Enabled = false;
            cmbModels.Enabled = true;
            LoadCyclesMakeList();
            lstMake.Focus();
        }

        private void optHeavyTruck_CheckedChanged(object sender, System.EventArgs e)
        {
            txtRVSize.Enabled = false;
            cmbModels.Enabled = true;
            LoadHeavyTruckMakeList();
            lstMake.Focus();
            modGlobalRoutines.Statics.gboolTruckBodyCombination = false;
            modGlobalRoutines.Statics.gboolSaveTruckBody = false;
        }

        public void LoadHeavyTruckMakeList()
        {
            var heavyTruckMakes = blueBookRepository.HeavyTruckMakes.GetHeavyTruckMakes();
            lstMake.Clear();
            if (heavyTruckMakes.Any())
            {
                foreach (var heavyTruckMake in heavyTruckMakes)
                {
                    lstMake.AddItem(heavyTruckMake.ManufacturerLong);
                }
                lstMake.SelectedIndex = 0;
            }
            
        }

        public void LoadModelList()
        {
            if (lstMake.Text.IsNullOrWhiteSpace() || txtYear.Text.IsNullOrWhiteSpace())
            {
                return;
            }

            var vehicleType = GetCurrentVehicleType();
            var configurations =
                blueBookRepository.VehicleConfigurations.GetConfigurations(
                    new VehicleConfigurationFilter(0, txtYear.Text.ToIntegerValue(), "", lstMake.Text,vehicleType.categoryId,vehicleType.classificationId)).Select(c => c.ModelName).Distinct();
            cmbModels.Clear();
            if (configurations.Any())
            {
                if (vehicleType.classificationId == VehicleClassificationNumber.RecreationalVehicles.ToInteger())
                {
                    cmbModels.AddItem("");
                }
                foreach (var configuration in configurations)
                {
                    cmbModels.AddItem(configuration);
                }

                cmbModels.SelectedIndex = 0;
            }
        }

        public (int classificationId, int categoryId) GetCurrentVehicleType()
        {
            int classificationId = 0;
            int categoryId = 0;
            switch (cmbCommercialTrailer?.Text.ToLower()??"")
            {
                case @"automobile / light duty truck":
                    classificationId = 4;
                    break;
                case @"light duty truck":
                    categoryId = 25;
                    break;
                case "heavy truck":
                    classificationId = 2;
                    break;
                case "recreational vehicles":
                    classificationId = 6;
                    break;
                case "commercial trailer":
                    classificationId = 7;
                    break;
                case "motorcycles":
                    categoryId = 29;
                    break;
                case "truck body":
                    classificationId = 8;
                    break;
            }

            return (classificationId: classificationId, categoryId: categoryId);
        }
        
        public void LoadCyclesMakeList()
        {
            var cycleMakes = blueBookRepository.CycleMakes.GetCycleMakes();
            lstMake.Clear();
            if (cycleMakes.Any())
            {
                foreach (var cycleMake in cycleMakes)
                {
                    lstMake.AddItem(cycleMake.ManufacturerLong);
                }
            }
            lstMake.SelectedIndex = 0;
        }

        private void optRV_CheckedChanged(object sender, System.EventArgs e)
        {
            cmbModels.Enabled = true;
            LoadRVMakeList();
            txtRVSize.Enabled = true;
            lstMake.Focus();
        }

        private void optTruckBody_CheckedChanged(object sender, System.EventArgs e)
        {
            cmbModels.Enabled = true;
            LoadTruckBodyMakeList();
        }

        private void txtVIN_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
            {
                KeyAscii = KeyAscii - 32;
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtYear_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cmbCommercialTrailer.Text.IsNullOrWhiteSpace())
            {
                return;
            }
            if (cmbCommercialTrailer.Text == "Commercial Trailer")
            {
                LoadTrailerMakeList();
            }
            else if (cmbCommercialTrailer.Text == "Truck Body")
            {
                LoadTruckBodyMakeList();
            }
            else
            {
                LoadModelList();
            }
        }

        private void txtYear_Enter(object sender, System.EventArgs e)
        {
            cmbCommercialTrailer.Enabled = true;
        }

        public void LoadTruckBodyMakeList()
        {
            lstMake.Clear();
            var manufacturers =
                blueBookRepository.TruckBodyDatas.GetTruckBodyManufacturers(txtYear.Text.ToIntegerValue());
            if (manufacturers.Any())
            {
                foreach (var manufacturer in manufacturers)
                {
                    lstMake.AddItem(manufacturer.Name);
                }
                lstMake.SelectedIndex = 0;
            }
        }

        public void LoadTrailerMakeList()
        {
            lstMake.Clear();
            var makes = blueBookRepository.TrailerData.GetTrailerManufacturersForYear(txtYear.Text.ToIntegerValue());
            if (makes.Any())
            {
                lstMake.Clear();
                foreach (var make in makes)
                {
                    lstMake.AddItem(make.Name);
                }
                lstMake.SelectedIndex = 0;
            }
        }

        public void LoadRVMakeList()
        {
            var rvMakes = blueBookRepository.RvMakes.GetRvMakes().ToList();
            if (rvMakes.Any())
            {
                lstMake.Clear();
                //lstMake.AddItem("");
                foreach (var rvMake in rvMakes)
                {
                 lstMake.AddItem(rvMake.ManufacturerLong);   
                }
                //lstMake.SelectedIndex = 0;
            }
        }

        public void SetMultipleGridForRecreationalVehicles()
        {
            frmMultiple.InstancePtr.vs1.Cols = 4;
            frmMultiple.InstancePtr.vs1.ColWidth(0, FCConvert.ToInt32(frmMultiple.InstancePtr.vs1.WidthOriginal * 0.0847278));
            frmMultiple.InstancePtr.vs1.ColWidth(1, FCConvert.ToInt32(frmMultiple.InstancePtr.vs1.WidthOriginal * 0.4554119));
            frmMultiple.InstancePtr.vs1.ColWidth(2, FCConvert.ToInt32(frmMultiple.InstancePtr.vs1.WidthOriginal * 0.3065473));
            frmMultiple.InstancePtr.vs1.ColWidth(3, FCConvert.ToInt32(frmMultiple.InstancePtr.vs1.WidthOriginal * 0.0847278));
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 0, "Size");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 1, "Model");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 2, "Description");
            frmMultiple.InstancePtr.vs1.TextMatrix(0, 3, "Weight");
        }

        public void LoadYearList()
        {
            // vbPorter upgrade warning: CYear As int	OnWriteFCConvert.ToInt32(
            int CYear;
            CYear = DateTime.Now.Year;
            fnx = 0;
            for (fnx = 2; fnx >= -29; fnx--)
            {
                //App.DoEvents();
                txtYear.AddItem(FCConvert.ToString(CYear + fnx));
            }
            // fnx
        }

        public void ClearLocalVariables()
        {
            if (MainFormLoaded == true)
            {
                modRedBook.Statics.VIN = "";
                txtVIN.SelectionStart = 0;
                txtVIN.SelectionLength = txtVIN.Text.Length;
                txtVIN.Focus();
                txtVIN.SelectionLength = txtVIN.Text.Length;
            }
        }

        private void cmbCommercialTrailer_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //FC:FINAL:BSE:#4370 - disable selected index changed when form is first loaded
            if (initialization)
            {
                return;
            }
            cmbModels.Clear();
            if (cmbCommercialTrailer.Text == "Commercial Trailer")
            {
                optCommercialTrailer_CheckedChanged(sender, e);
            }
            else if (cmbCommercialTrailer.Text == "Truck Body")
            {
                optTruckBody_CheckedChanged(sender, e);
            }
            else if (cmbCommercialTrailer.Text == "Recreational Vehicles")
            {
                optRV_CheckedChanged(sender, e);
            }
            else if (cmbCommercialTrailer.Text == "Motorcycles")
            {
                optCycles_CheckedChanged(sender, e);
            }
            else if (cmbCommercialTrailer.Text == "Heavy Truck")
            {
                optHeavyTruck_CheckedChanged(sender, e);
            }
            else if (cmbCommercialTrailer.Text == "Automobile / Light Duty Truck")
            {
                optCarLightTruck_CheckedChanged(sender, e);
            }
            else if (cmbCommercialTrailer.Text == "Light Duty Truck")
            {
                throw new NotImplementedException();
            }
        }

        private void ShowOptions(int configurationId)
        {
            int Number = 0;
            modRedBook.Statics.FromMultiple = false;
            modRedBook.Statics.VehicleMediumTruck = false;
            frmFinal.InstancePtr.CameFrom = "Search";
            var vehicleSpecs = blueBookRepository.VehicleSpecs.GetVehicleSpecs(configurationId);
            modRedBook.Statics.VehicleSelectedFromMultiple = configurationId;
            if (vehicleSpecs != null)
            {
                modRedBook.Statics.CurrentVehicleObject = vehicleSpecs;
                var equipmentCode = vehicleSpecs.GetSpecByName("equipmentCode");
                if ((modRedBook.Statics.VehicleType == "C") || (modRedBook.Statics.VehicleType == "T"))
                {
                    modRedBook.Statics.StandardOptions = vehicleSpecs.GetSpecByName("equipmentCode");
                    modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                    modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                    modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                    modRedBook.GetAllRbCarInfo(vehicleSpecs);

                    if ((equipmentCode == "S") || (equipmentCode == "F"))
                    {
                        modRedBook.Statics.VehicleType = "T";
                    }
                    else if (equipmentCode.IsNullOrWhiteSpace())
                    {
                        InstancePtr.Hide();
                        frmFinal.InstancePtr.Show();
                        return;
                    }
                    else
                    {
                        modRedBook.Statics.VehicleType = "C";
                    }

                    InstancePtr.Hide();
                    var configurationOptions = blueBookRepository.Options.GetOptions(
                        new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                    if (configurationOptions.Options.Any())
                    {
                        frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                        frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                        frmNewCarOptions.InstancePtr.Show();
                    }
                    else
                    {
                        frmFinal.InstancePtr.Show();
                        return;
                    }
                }
                else if (modRedBook.Statics.VehicleType == "H")
                {

                    modRedBook.Statics.VehicleType = "H";
                    modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                    modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                    modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                    modRedBook.Statics.VehicleMediumTruck = false; //vehicleSpecs.//heavyTruck.Type == "M";

                    InstancePtr.Hide();
                    var configurationOptions = blueBookRepository.Options.GetOptions(
                        new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                    if (configurationOptions.Options.Any())
                    {
                        frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                        frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                        frmNewCarOptions.InstancePtr.Show();
                    }
                    else
                    {
                        frmFinal.InstancePtr.Show();
                        return;
                    }
                }
                else if (modRedBook.Statics.VehicleType == "M")
                {

                    modRedBook.Statics.VehicleType = "M";
                    modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                    modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                    modRedBook.Statics.VehicleModel = vehicleSpecs.Description;
                    modRedBook.Statics.VehicleMake = vehicleSpecs.ManufacturerName;
                    modRedBook.GetAllMotorCycleInfo(vehicleSpecs);

                    InstancePtr.Hide();
                    frmFinal.InstancePtr.Show();
                }
                else if (modRedBook.Statics.VehicleType == "R")
                {
                    var rv = blueBookRepository.Rvs.GetRv(modRedBook.Statics.VehicleSelectedFromMultiple);
                    modRedBook.Statics.CurrentVehicleObject = rv;
                    modRedBook.Statics.VehicleType = "R";
                    modRedBook.Statics.VehicleIDNumber = rv.Id;
                    modRedBook.Statics.VehicleYear = rv.Year.ToIntegerValue();
                    modRedBook.Statics.VehicleModel = rv.Description;
                    modRedBook.Statics.VehicleMake = rv.Manufacturer;
                    modRedBook.Statics.VModel = rv.Model;
                    modRedBook.Statics.VYear = rv.Year;
                    modRedBook.Statics.VDescription = rv.Description;
                    modRedBook.Statics.VFeet = rv.Feet.GetValueOrDefault();
                    modRedBook.Statics.VInches = rv.Inches;
                    modRedBook.Statics.VGrossVehicleWeight = rv.Weight;
                    modRedBook.Statics.VSRP = rv.Srp;
                    modRedBook.Statics.VManufacturer = rv.Manufacturer;
                    InstancePtr.Hide();
                    frmFinal.InstancePtr.Show();
                }
                else if (modRedBook.Statics.VehicleType == "X")
                {
                    // Trailers
                    var trailer = blueBookRepository.TrailerData.GetTrailerData(modRedBook.Statics
                        .VehicleSelectedFromMultiple);
                    modRedBook.Statics.CurrentVehicleObject = trailer;
                    modRedBook.Statics.VehicleType = "X";
                    modRedBook.Statics.VehicleIDNumber = trailer.Id;
                    modRedBook.Statics.VehicleYear = trailer.Year.GetValueOrDefault();
                    modRedBook.Statics.VehicleModel = trailer.Manufacturer;
                    modRedBook.GetAllTrailerDataInfo(trailer);
                    modRedBook.Statics.CurrentVehicleObject = trailer;
                    InstancePtr.Hide();
                    frmFinal.InstancePtr.Show();
                }
                else if (modRedBook.Statics.VehicleType == "B")
                {
                    // Truck Body
                    modRedBook.Statics.CurrentVehicleObject = vehicleSpecs;
                    modRedBook.Statics.VehicleType = "B";
                    modRedBook.Statics.VehicleIDNumber = vehicleSpecs.ConfigurationId;
                    modRedBook.Statics.VehicleYear = vehicleSpecs.ModelYear;
                    modRedBook.Statics.VManufacturer = vehicleSpecs.ManufacturerName;
                    modRedBook.Statics.VehicleModel = vehicleSpecs.ModelName;
                    modRedBook.Statics.VDescription = vehicleSpecs.Description;
                    modRedBook.Statics.VSRP = vehicleSpecs.GetSpecByName("msrp");
                    InstancePtr.Hide();
                    var configurationOptions = blueBookRepository.Options.GetOptions(
                        new OptionFilterCriteria(vehicleSpecs.SizeClassId, vehicleSpecs.ModelYear));
                    if (configurationOptions.Options.Any())
                    {
                        frmNewCarOptions.InstancePtr.CurrentConfiguration = vehicleSpecs;
                        frmNewCarOptions.InstancePtr.CurrentConfigurationOptions = configurationOptions;
                        frmNewCarOptions.InstancePtr.Show();
                    }
                    else
                    {
                        frmFinal.InstancePtr.Show();
                        return;
                    }
                }
            }
        }
    }
}
