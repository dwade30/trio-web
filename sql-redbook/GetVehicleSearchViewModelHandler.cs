﻿using SharedApplication.BlueBook;
using SharedApplication.Messaging;
using TWRB0000.commands;

namespace TWRB0000
{
    public class GetVehicleSearchViewModelHandler : CommandHandler<GetVehicleSearchViewModel,IVehicleSearchViewModel>
    {
        private IVehicleSearchViewModel viewModel;

        public GetVehicleSearchViewModelHandler(IVehicleSearchViewModel viewModel)
        {
            this.viewModel = viewModel;
        }
        protected override IVehicleSearchViewModel Handle(GetVehicleSearchViewModel command)
        {
            return viewModel;
        }
    }
}