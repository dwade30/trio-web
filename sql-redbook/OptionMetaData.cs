﻿namespace TWRB0000
{
    public class OptionMetaData
    {
        public bool IsWheelOption { get; set; } = false;
        public bool IsBrakeOption { get; set; } = false;
        public string Description { get; set; } = "";
        public decimal Price { get; set; } = 0;
    }
}