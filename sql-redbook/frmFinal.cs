//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using BlueBook;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;

namespace TWRB0000
{
	public partial class frmFinal : BaseForm
	{
		public frmFinal()
		{
            InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null )
				_InstancePtr = this;

			lblTotalValuation = new System.Collections.Generic.List<FCLabel>();
			lblTotalValuation.AddControlArrayElement(lblTotalValuation_0, 0);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFinal InstancePtr
		{
			get
			{
				return (frmFinal)Sys.GetInstance(typeof(frmFinal));
			}
		}

        public string CameFrom { get; set; } = "";
		protected frmFinal _InstancePtr = null;
		//=========================================================
		bool FormLoaded;
		int newvsrp;
		int StickerPrice;
        string XferExcise = new string('\0', 7);
        string XferValue = new string('\0', 9);
        string XferExciseYear = new string('\0', 1);
        string XferYear = new string('\0', 4);
        string XferMake = new string('\0', 4);
        string XferModel = new string('\0', 6);
        float MillRate;

	

		private void cmdBack_Click(object sender, System.EventArgs e)
		{
            modRedBook.Statics.StickerFormLoaded = false;
            switch (CameFrom.ToLower())
            {
				case "options":
                    frmNewCarOptions.InstancePtr.KeepData = true;
                    frmNewCarOptions.InstancePtr.OptionFormLoaded = true;
                    frmNewCarOptions.InstancePtr.Show();
					break;
                default:
                    return;
                    break;
            }
            
			frmFinal.InstancePtr.Close();
		}

		public void cmdBack_Click()
		{
			cmdBack_Click(cmdBack, new System.EventArgs());
		}

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
            string strMilYear = new string('\0', 1);
            int strBase = 0;
			string w3rec = new string('\0', 241);
			string w4rec = new string('\0', 241);
			int fnx;
			string strRec3 = new string('\0', 241);
			string strRec5A = new string('\0', 241);
			string strReturnMake = "";
            string strMakeCode = "";
            cSettingsController set = new cSettingsController();
			if (!modGlobalRoutines.Statics.gboolSaveTruckBody)
			{
				// If VehicleType = "T" And TruckBody Then
				if (modRedBook.Statics.VehicleMediumTruck && modRedBook.Statics.TruckBody)
				{
					if (MessageBox.Show("Do you want to add the excise of a Truck Body?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// SAVE ALL OF THE INFORMATION TO THE TABLE FOR THE TRUCK ITSELF
						modGlobalRoutines.Statics.gboolTruckBodyCombination = true;
						modRedBook.Statics.gstrTruckVIN = modRedBook.Statics.VIN;
						modRedBook.Statics.gstrTruckYEAR = FCConvert.ToString(modRedBook.Statics.VehicleYear);
                        switch (modRedBook.Statics.VehicleType)
                        {
							case "T":
                                var lightTruck = ((VehicleConfigurationSpecs) modRedBook.Statics.CurrentVehicleObject).ToRbcar();
                                modRedBook.Statics.gstrTruckMAKE = lightTruck.Manufacturer;
                                modRedBook.Statics.gstrTruckMODEL = lightTruck.ModelNumber;
                                break;
							    
							case "H":
                                var heavyTruck = ((VehicleConfigurationSpecs) modRedBook.Statics.CurrentVehicleObject)
                                    .ToHeavyTruck();
                                modRedBook.Statics.gstrTruckMAKE = heavyTruck.Manufacturer;
                                modRedBook.Statics.gstrTruckMODEL = heavyTruck.ModelNumber;
                                break;
							case "B":
                                var truckBody = ((VehicleConfigurationSpecs) modRedBook.Statics.CurrentVehicleObject).ToTruckBodyData();
                                modRedBook.Statics.gstrTruckMAKE = truckBody.Manufacturer;
                                modRedBook.Statics.gstrTruckMODEL = truckBody.Model;
                                break;
							default:
                                modRedBook.Statics.gstrTruckMAKE ="";
                                modRedBook.Statics.gstrTruckMODEL = "";
                                break;
						}

						
					}
				}
				for (fnx = 0; fnx <= 5; fnx++)
				{
					if (cmbMillRate.SelectedIndex == fnx)
					{
						XferExciseYear = (fnx + 1).ToString();
						break;
					}
				}
				// fnx
				XferYear = modRedBook.Statics.VehicleYear.ToString();
				XferValue = "000000000";
				XferExcise = "0000000";
				if (Conversion.Val(modRedBook.Statics.VSRP) != 0)
				{
					newvsrp = FCConvert.ToInt32(Conversion.Val(modRedBook.Statics.VSRP)) + FCConvert.ToInt32(modRedBook.Statics.TotalCost);
					var intValue = FCUtils.iDiv(((MillRate * newvsrp) * 100), 1);
					XferExcise = modGNBas.PadToString(intValue, 6);
					XferValue = modGNBas.PadToString(newvsrp, 9);
				}
				XferMake = modRedBook.Statics.VehicleMake;
				XferModel = modRedBook.Statics.VehicleModel;
				if (cmbMillRate.Text == "0.024    Year 1 ")
					strMilYear = "1";
				if (cmbMillRate.Text == "0.0175  Year 2")
					strMilYear = "2";
				if (cmbMillRate.Text == "0.0135  Year 3")
					strMilYear = "3";
				if (cmbMillRate.Text == "0.010    Year 4")
					strMilYear = "4";
				if (cmbMillRate.Text == "0.0065  Year 5")
					strMilYear = "5";
				if (cmbMillRate.Text == "0.004    Year 6")
					strMilYear = "6";
				if (XferValue != "")
				{
					strBase = FCConvert.ToInt32(Strings.Right(XferValue, 7));
				}
				else
				{
					strBase = FCConvert.ToInt32("0");
				}
				modRedBook.Statics.gdblTruckBodyCombination = FCConvert.ToDouble(strBase);
				FCFileSystem.FileClose();
				/*? On Error Resume Next  */
				set.SaveSetting("", "MVFLAG", "TWGNENTY", "REDBOOK");
				set.SaveSetting(modRedBook.Statics.VIN, "VIN", "TWGNENTY", "REDBOOK");
				if (StickerPrice != 0)
				{
					set.SaveSetting(FCConvert.ToString(StickerPrice), "BASE", "TWGNENTY", "REDBOOK");
					set.SaveSetting(FCConvert.ToString(modRedBook.Statics.VehicleYear), "YEAR", "TWGNENTY", "REDBOOK");
					set.SaveSetting("", "MAKE", "TWGNENTY", "REDBOOK");
					set.SaveSetting("", "MODEL", "TWGNENTY", "REDBOOK");
				}
				else
				{
					set.SaveSetting(FCConvert.ToString(strBase), "BASE", "TWGNENTY", "REDBOOK");
					set.SaveSetting(FCConvert.ToString(modRedBook.Statics.VehicleYear), "YEAR", "TWGNENTY", "REDBOOK");
					if (modRedBook.Statics.VManufacturer != "")
					{
						if (modRedBook.Statics.VManufacturer.Length > 4)
						{
							strMakeCode = Strings.Left(fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(modRedBook.Statics.VManufacturer)), 4);
						}
						else
						{
							strMakeCode = fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(modRedBook.Statics.VManufacturer));
						}
					}
					else
					{
						if (modRedBook.Statics.VehicleMake.Length > 4)
						{
							strMakeCode = Strings.Left(fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(modRedBook.Statics.VehicleMake)), 4);
						}
						else
						{
							strMakeCode = fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(modRedBook.Statics.VehicleMake));
						}
					}
					strReturnMake = modGlobalFunctions.ReturnCorrectedMakeCode(modRedBook.Statics.VehicleType, modRedBook.Statics.VehicleMake, strMakeCode);
					set.SaveSetting(strReturnMake, "MAKE", "TWGNENTY", "REDBOOK");
					
					set.SaveSetting(fecherFoundation.Strings.UCase(modRedBook.Statics.VehicleModel), "MODEL", "TWGNENTY", "REDBOOK");
				}
				set.SaveSetting(strMilYear, "MILYEAR", "TWGNENTY", "REDBOOK");
				if (modRedBook.Statics.blnFromWindowsCR)
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\RB\\", "Excise", Strings.Right(lblTotalExcise.Text, lblTotalExcise.Text.Length - 1));
				}
				if (modGlobalRoutines.Statics.gboolTruckBodyCombination)
				{
					modGlobalRoutines.Statics.gboolSaveTruckBody = true;
					frmRedbook.InstancePtr.Show();
					frmRedbook.InstancePtr.cmbCommercialTrailer.Text = "Truck Body";
					frmRedbook.InstancePtr.txtVIN.Text = string.Empty;
					modRedBook.ClearVariables();
					modRedBook.Statics.VIN = string.Empty;
					frmFinal.InstancePtr.Close();
					return;
				}
			}
			else
			{
				// SAVE THE INFORMATION FOR THE BODY
				string dblExcise = "";
				//clsDRWrapper rsData = new clsDRWrapper();
				
				for (fnx = 0; fnx <= 5; fnx++)
				{
					if (cmbMillRate.SelectedIndex == fnx)
					{
						XferExciseYear = (fnx + 1).ToString();
						break;
					}
				}
				// fnx
				XferYear = modRedBook.Statics.VehicleYear.ToString();
				XferValue = "000000000";
				XferExcise = "0000000";
				if (Conversion.Val(modRedBook.Statics.VSRP) != 0)
				{
					newvsrp = FCConvert.ToInt32(Conversion.Val(modRedBook.Statics.VSRP)) + FCConvert.ToInt32(modRedBook.Statics.TotalCost);
					var intValue = FCUtils.iDiv(((MillRate * newvsrp) * 100), 1);
					XferExcise = modGNBas.PadToString(intValue, 6);
					XferValue = modGNBas.PadToString(newvsrp, 9);
				}
				XferMake = modRedBook.Statics.VehicleMake;
				XferModel = modRedBook.Statics.VehicleModel;
				if (cmbMillRate.Text == "0.024    Year 1 ")
					strMilYear = "1";
				if (cmbMillRate.Text == "0.0175  Year 2")
					strMilYear = "2";
				if (cmbMillRate.Text == "0.0135  Year 3")
					strMilYear = "3";
				if (cmbMillRate.Text == "0.010    Year 4")
					strMilYear = "4";
				if (cmbMillRate.Text == "0.0065  Year 5")
					strMilYear = "5";
				if (cmbMillRate.Text == "0.004    Year 6")
					strMilYear = "6";
				if (XferValue != "")
				{
					strBase = FCConvert.ToInt32(Strings.Right(XferValue, 7));
				}
				else
				{
					strBase = 0;
				}
				// SEND BACK TO MV THE TOTAL VALUE OF BOTH THE TRUCK AND THE BODY\
				// CALL ID 77756 MATTHEW 10/6/2005
				modRedBook.Statics.gdblTruckBodyCombination += FCConvert.ToDouble(strBase);
				set.SaveSetting(FCConvert.ToString(modRedBook.Statics.gdblTruckBodyCombination), "BASE", "TWGNENTY", "REDBOOK");
				if (modRedBook.Statics.blnFromWindowsCR)
				{
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\RB\\", "Excise", ref dblExcise);
					lblTotalExcise.Text = lblTotalExcise.Text + dblExcise;
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\RB\\", "Excise", Strings.Right(lblTotalExcise.Text, lblTotalExcise.Text.Length - 1));
				}
			}
			set.SaveSetting("N", "VEHICLEINSYSTEM", "TWGNENTY", "REDBOOK");


            FCUtils.CloseFormsOnProject();
			if (modRedBook.Statics.MVFlag == false && !modRedBook.Statics.blnFromWindowsCR && !modRedBook.Statics.blnFromDOS)
			{
                //App.MainForm.OpenModule("TWGNENTY");
                //App.MainForm.EndWaitRBModule();
            }
			else
			{
                
				modGNBas.WriteYY();
				App.MainForm.EndWaitRBModule();
            }
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdFileProcess, new System.EventArgs());
		}

		private void cmdSelection_Click(object sender, System.EventArgs e)
		{
            if (CameFrom.ToLower() != "multiple")
            {
                return;
            }
			if (frmMultiple.InstancePtr.vs1.Rows < 1)
				modRedBook.Statics.FromMultiple = false;
			if (modRedBook.Statics.FromMultiple == true)
			{
				modRedBook.Statics.StickerFormLoaded = false;
				frmMultiple.InstancePtr.Show();
                this.Close();
				modRedBook.Statics.FromMultiple = false;
			}
			else
			{
				frmRedbook.InstancePtr.Show();
                this.Close();
            }
		}

		public void cmdSelection_Click()
		{
			cmdSelection_Click(cmdSelection, new System.EventArgs());
		}

		private void cmdStart_Click(object sender, System.EventArgs e)
		{
			modRedBook.Statics.StickerFormLoaded = false;
			frmNewCarOptions.InstancePtr.Unload();
			modRedBook.Statics.TotalCost = 0;
			frmRedbook.InstancePtr.Show();
            this.Close();
        }

		public void cmdStart_Click()
		{
			cmdStart_Click(cmdStart, new System.EventArgs());
		}

		private void cmdSticker_Click(object sender, System.EventArgs e)
		{
            frmSticker.InstancePtr.Show(FCForm.FormShowEnum.Modal, this);
			if (modRedBook.Statics.Xy == "")
			{
				Label9.Visible = false;
				lblStickerPrice.Visible = false;
				return;
			}
			if (Conversion.Val(modRedBook.Statics.Xy) > 9999999)
			{
                var Msg = "The Sticker Price " + modRedBook.Statics.Xy + " is too large.";
				MessageBox.Show(Msg, "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			StickerPrice = FCConvert.ToInt32(FCConvert.ToDouble(modRedBook.Statics.Xy));

			int srp;
			Label9.Visible = true;
			lblStickerPrice.Visible = true;
			lblStickerPrice.Text = Strings.Format(modRedBook.Statics.Xy, "Currency");
			srp = FCConvert.ToInt32(FCConvert.ToSingle(double.Parse(FCConvert.ToString(FCConvert.ToDouble(lblStickerPrice.Text)), System.Globalization.NumberStyles.Currency)));
			lblTotalExcise.Text = Strings.Format(modGNBas.Round(MillRate * srp, 2), "Currency");
			Label9.ForeColor = ColorTranslator.FromOle(0xC0);
        }

		public void cmdSticker_Click()
		{
			cmdSticker_Click(cmdSticker, new System.EventArgs());
		}

		private void frmFinal_Activated(object sender, System.EventArgs e)
		{
			// VB6 Bad Scope Dim:
			int fnx;
			int x = 0;
			// Call ForceFormToResize(Me)
			if (modRedBook.Statics.StickerFormLoaded == false)
			{
				cmbMillRate.Enabled = false;
				// fnx
				if (modRedBook.Statics.FromMultiple != true)
				{
					cmdSelection.Enabled = false;
				}
				else
				{
					cmdSelection.Enabled = true;
                }
				cmdBack.Enabled = true;
				// trorb-23  05.19.2017 kjr
				mnuBackOption.Enabled = true;
				// 
				if (fecherFoundation.Strings.Trim(modRedBook.Statics.StandardOptions) == "")
				{
					cmdBack.Enabled = false;
					mnuBackOption.Enabled = false;
				}
				if (modRedBook.Statics.VehicleType == "R" || modRedBook.Statics.VehicleType == "M")
				{
					cmdBack.Enabled = false;
					mnuBackOption.Enabled = false;
				}

				StickerPrice = 0;
				lblStickerPrice.Visible = false;
				Label9.Visible = false;
				if (modRedBook.Statics.StickerButton == false)
				{
					FillLabels();
				}
				else
				{
					modRedBook.Statics.VehicleYear = FCConvert.ToInt32(frmRedbook.InstancePtr.txtYear.Text);
					modRedBook.Statics.StickerButton = false;
					// vbPorter upgrade warning: y As int	OnWriteFCConvert.ToInt32(
					int y;
					y = ((DateTime.Now.Year - modRedBook.Statics.VehicleYear) + 1);
					switch (y)
					{
						case 0:
						case 1:
							{
								lblYearRate.Text = "0.024";
								MillRate = 0.024f;
								break;
							}
						case 2:
							{
								lblYearRate.Text = "0.0175";
								MillRate = 0.0175f;
								break;
							}
						case 3:
							{
								lblYearRate.Text = "0.0135";
								MillRate = 0.0135f;
								break;
							}
						case 4:
							{
								lblYearRate.Text = "0.010";
								MillRate = 0.01f;
								break;
							}
						case 5:
							{
								lblYearRate.Text = "0.0065";
								MillRate = 0.0065f;
								break;
							}
						default:
							{
								lblYearRate.Text = "0.004";
								MillRate = 0.004f;
								break;
							}
					}
					//end switch
					// vbPorter upgrade warning: CurYear As int	OnWriteFCConvert.ToInt32(
					int CurYear = 0;
					CurYear = DateTime.Now.Year;
					x = CurYear - modRedBook.Statics.VehicleYear;
					if (x < 0)
						x = 0;
                    cmbMillRate.Enabled = true;
                    cmbMillRate.Clear();
					if (x > 5)
					{
						x = 5;
						if (!cmbMillRate.Items.Contains("0.004    Year 6"))
						{
							cmbMillRate.Items.Add("0.004    Year 6");
						}
						cmbMillRate.Text = "0.004    Year 6";
					}

					if (x == 0 && !cmbMillRate.Items.Contains("0.024    Year 1 "))
					{
						cmbMillRate.Items.Add("0.024    Year 1 ");
						cmbMillRate.Text = "0.024    Year 1 ";
					}
					else if (x == 1 && !cmbMillRate.Items.Contains("0.0175  Year 2"))
					{
						cmbMillRate.Items.Add("0.0175  Year 2");
						cmbMillRate.Text = "0.0175  Year 2";
					}
					else if (x == 2 && !cmbMillRate.Items.Contains("0.0135  Year 3"))
					{
						cmbMillRate.Items.Add("0.0135  Year 3");
						cmbMillRate.Text = "0.0135  Year 3";
					}
					else if (x == 3 && !cmbMillRate.Items.Contains("0.010    Year 4"))
					{
						cmbMillRate.Items.Add("0.010    Year 4");
						cmbMillRate.Text = "0.010    Year 4";
					}
					else if (x == 4 && !cmbMillRate.Items.Contains("0.0065  Year 5"))
					{
						cmbMillRate.Items.Add("0.0065  Year 5");
						cmbMillRate.Text = "0.0065  Year 5";
					}
					else if (x == 5 && !cmbMillRate.Items.Contains("0.004    Year 6"))
					{
						cmbMillRate.Items.Add("0.004    Year 6");
						cmbMillRate.Text = "0.004    Year 6";
					}

					if (x < 5)
					{
						if (x + 1 == 1 && !cmbMillRate.Items.Contains("0.0175  Year 2"))
						{
							cmbMillRate.Items.Add("0.0175  Year 2");
						}
						else if (x + 1 == 2 && !cmbMillRate.Items.Contains("0.0135  Year 3"))
						{
							cmbMillRate.Items.Add("0.0135  Year 3");
						}
						else if (x + 1 == 3 && !cmbMillRate.Items.Contains("0.010    Year 4"))
						{
							cmbMillRate.Items.Add("0.010    Year 4");
						}
						else if (x + 1 == 4 && !cmbMillRate.Items.Contains("0.0065  Year 5"))
						{
							cmbMillRate.Items.Add("0.0065  Year 5");
						}
						else if (x + 1 == 5 && !cmbMillRate.Items.Contains("0.004    Year 6"))
						{
							cmbMillRate.Items.Add("0.004    Year 6");							
						}
					}
					if (x < 4)
					{
						if (x + 2 == 2 && !cmbMillRate.Items.Contains("0.0135  Year 3"))
						{
							cmbMillRate.Items.Add("0.0135  Year 3");
						}
						else if (x + 2 == 3 && !cmbMillRate.Items.Contains("0.010    Year 4"))
						{
							cmbMillRate.Items.Add("0.010    Year 4");
						}
						else if (x + 2 == 4 && !cmbMillRate.Items.Contains("0.0065  Year 5"))
						{
							cmbMillRate.Items.Add("0.0065  Year 5");
						}
						else if (x + 2 == 5 && !cmbMillRate.Items.Contains("0.004    Year 6"))
						{
							cmbMillRate.Items.Add("0.004    Year 6");							
						}
					}
					lblYear.Text = FCConvert.ToString(modRedBook.Statics.VehicleYear);
					lblManufacturer.Text = frmRedbook.InstancePtr.lstMake.Text;
					cmdSticker_Click();
				}
				cmdFileProcess.Focus();
				FormLoaded = true;
			}
			else
			{
                string selectedItem = "";
                if (cmbMillRate.Items.Count>0)
                {
                    selectedItem = FCConvert.ToString(cmbMillRate.SelectedItem);
                }

                cmbMillRate.Clear();
                cmbMillRate.Items.Add("0.024    Year 1 ");
                cmbMillRate.Items.Add("0.0175  Year 2");
                cmbMillRate.Items.Add("0.0135  Year 3");
                cmbMillRate.Items.Add("0.010    Year 4");
                cmbMillRate.Items.Add("0.0065  Year 5");
                cmbMillRate.Items.Add("0.004    Year 6");
                cmbMillRate.Text = selectedItem;
                // fnx
            }

            if (CameFrom.ToLower() == "options")
            {
                cmdBack.Visible = true;
            }
            else
            {
                cmdBack.Visible = false;
            }

            if (CameFrom.ToLower() == "multiple")
            {
                cmdSelection.Visible = true;
            }
            else
            {
                cmdSelection.Visible = false;
            }
		}

		private void frmFinal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				cmdStart_Click();
				// Call RedbookShow
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFinal_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		public void FillLabels()
		{
			this.Frame1.Text = "Vehicle Information";
			if ((modRedBook.Statics.VehicleType == "C") || (modRedBook.Statics.VehicleType == "T"))
			{
				if (modRedBook.Statics.VYear != "")
				{
					lblYear.Text = modRedBook.Statics.VYear;
				}
				else
				{
					lblYear.Text = FCConvert.ToString(modRedBook.Statics.VehicleYear);
				}
				if (modRedBook.Statics.VManufacturer != "")
				{
					lblManufacturer.Text = modRedBook.Statics.VManufacturer;
				}
				else
				{
					lblManufacturer.Text = modRedBook.Statics.VehicleMake;
				}
				if (modRedBook.Statics.VModel != "")
				{
					lblModel.Text = modRedBook.Statics.VModel;
				}
				else
				{
					lblModel.Text = modRedBook.Statics.VehicleModel;
				}
				if (modRedBook.Statics.VEngine != "")
				{
					lblEngine.Text = modRedBook.Statics.VEngine;
				}
				else
				{
					lblEngine.Text = modRedBook.Statics.VehicleEngine;
				}
				if (modRedBook.Statics.VDescription != "")
				{
					lblDescription.Text = modRedBook.Statics.VDescription;
				}
				else
				{
					lblDescription.Text = modRedBook.Statics.VehicleDescription;
				}
				lblEngineDesc.Text = "";
				Label13.Text = "Number of Pass.";
				lblNetWeight.Text = modRedBook.Statics.VNumberOfPassengers;
				Label7.Text = "Engine";
				Label11.Text = "Wheel Base";
				lblTransMake.Text = modRedBook.Statics.VWheelbase;
				lblTransDesc.Text = modRedBook.Statics.VTransmission;
				Label12.Text = "Transmission";
			}
			else if (modRedBook.Statics.VehicleType == "M")
			{
				if (modRedBook.Statics.VYear != "")
				{
					lblYear.Text = modRedBook.Statics.VYear;
				}
				else
				{
					lblYear.Text = FCConvert.ToString(modRedBook.Statics.VehicleYear);
				}
				if (modRedBook.Statics.VehicleMake != "")
				{
					lblManufacturer.Text = modRedBook.Statics.VehicleMake;
				}
				else
				{
					lblManufacturer.Text = modRedBook.Statics.VManufacturer;
				}
				if (modRedBook.Statics.VModel != "")
				{
					lblModel.Text = modRedBook.Statics.VModel;
				}
				else
				{
					lblModel.Text = modRedBook.Statics.VehicleModel;
				}
				if (modRedBook.Statics.VEngine != "")
				{
					lblEngine.Text = modRedBook.Statics.VEngine;
				}
				else
				{
					lblEngine.Text = modRedBook.Statics.VehicleEngine;
				}
				if (modRedBook.Statics.VDescription != "")
				{
					lblDescription.Text = modRedBook.Statics.VDescription;
				}
				else
				{
					lblDescription.Text = modRedBook.Statics.VehicleDescription;
				}
				Label13.Text = "Net Weight";
				lblNetWeight.Text = modRedBook.Statics.VNumberOfPassengers;
				Label7.Text = "Displacement";
				lblEngine.Text = modRedBook.Statics.VTransmission;
				Label11.Text = "Cylinders";
				lblTransMake.Text = modRedBook.Statics.VEngine;
				lblEngineDesc.Text = "";

			}
			else if (modRedBook.Statics.VehicleType == "H")
			{
				Label7.Text = "Engine Make";
				Label13.Text = "Net Weight";
				lblNetWeight.Text = modRedBook.Statics.VGrossVehicleWeight;
				Label11.Text = "Transmission Make";
				lblTransMake.Text = modRedBook.Statics.VWheelbase;
				lblTransDesc.Text = modRedBook.Statics.VTransmission;
				Label12.Text = "Transmission Desc.";
                //var truck = (HeavyTruck) modRedBook.Statics.CurrentVehicleObject ?? new HeavyTruck();
                var truck = ((VehicleConfigurationSpecs) modRedBook.Statics.CurrentVehicleObject).ToHeavyTruck();
				if (truck.Type == "H")
				{
                    modRedBook.Statics.VModel = truck.ModelNumber + "  (Heavy)";
				}
				else if (truck.Type == "M")
				{
                    modRedBook.Statics.VModel = truck.ModelNumber + "  (Medium)";
				}
				else
                {
                    modRedBook.Statics.VModel = truck.ModelNumber;
                }
				modRedBook.Statics.VYear = truck.Year;
                modRedBook.Statics.VDescription = truck.Description;
                modRedBook.Statics.VEngineMake = truck.EngineMake;
                modRedBook.Statics.VEngineDescription = truck.EngineDescription;
                modRedBook.Statics.VAxle1Make = truck.Axle1Make;
                modRedBook.Statics.VAxle1Description = truck.Axle1Description;
                modRedBook.Statics.VTransmissionDescription = truck.TransmissionDescription;
                modRedBook.Statics.VTransmissionMake = truck.TransmissionMake;
                modRedBook.Statics.VGVW = truck.GrossVehicleWeight;
                modRedBook.Statics.VWheelbase = truck.Wheelbase;
                modRedBook.Statics.VAxle2Make = truck.Axle2Make;
                modRedBook.Statics.VAxle2Description = truck.Axle2Description;
				modRedBook.Statics.VNetWeight = truck.NetWeight;
                modRedBook.Statics.VRetailPrice = truck.FactoryPrice;
                modRedBook.Statics.VSRP = truck.FactoryPrice;
				lblYear.Text = modRedBook.Statics.VYear;
				if (modRedBook.Statics.VManufacturer != "")
				{
					lblManufacturer.Text = modRedBook.Statics.VManufacturer;
				}
				else
				{
					lblManufacturer.Text = modRedBook.Statics.VehicleMake;
				}
				if (modRedBook.Statics.VModel != "")
				{
					lblModel.Text = modRedBook.Statics.VModel;
				}
				else
				{
					lblModel.Text = modRedBook.Statics.VehicleModel;
				}
				lblDescription.Text = modRedBook.Statics.VDescription;
				lblEngine.Text = modRedBook.Statics.VEngineMake;
				lblEngineDesc.Text = modRedBook.Statics.VEngineDescription;
				lblNetWeight.Text = modRedBook.Statics.VNetWeight;
				lblTransMake.Text = modRedBook.Statics.VTransmissionMake;
				lblTransDesc.Text = modRedBook.Statics.VTransmissionDescription;
			}
			else if (modRedBook.Statics.VehicleType == "R")
			{
				if (modRedBook.Statics.VYear != "")
				{
					lblYear.Text = modRedBook.Statics.VYear;
				}
				else
				{
					lblYear.Text = FCConvert.ToString(modRedBook.Statics.VehicleYear);
				}
				if (modRedBook.Statics.VManufacturer != "")
				{
					lblManufacturer.Text = modRedBook.Statics.VManufacturer;
				}
				else
				{
					lblManufacturer.Text = modRedBook.Statics.VehicleMake;
				}

				lblDescription.Text = modRedBook.Statics.VModel;
				Label8.Text = "Model";
				Label14.Text = "Description";
				Label14.Visible = true;
				Label15.Text = modRedBook.Statics.VDescription;
				Label15.Visible = true;
				Label11.Visible = false;
				Label7.Visible = false;
				Label10.Visible = false;
				lblTransMake.Visible = false;
				lblEngineDesc.Visible = false;
				lblEngine.Visible = false;

				lblTransDesc.Text = modRedBook.Statics.VGrossVehicleWeight;
				Label12.Text = "Weight";
				Label13.Text = "Size";
				if (Conversion.Val(modRedBook.Statics.VInches) != 0)
				{
					lblNetWeight.Text = FCConvert.ToString(modRedBook.Statics.VFeet) + FCConvert.ToString(Convert.ToChar(39)) + " " + modRedBook.Statics.VInches + FCConvert.ToString(Convert.ToChar(34));
				}
				else
				{
					lblNetWeight.Text = FCConvert.ToString(modRedBook.Statics.VFeet) + FCConvert.ToString(Convert.ToChar(39));
				}
				lblModel.Visible = false;
				// Label12.Text = "Transmission"
			}
			else if (modRedBook.Statics.VehicleType == "X")
			{
				// Trailer Matthew 9/8/05
				this.Frame1.Text = "Trailer Information";
				Label8.Text = "";
				Label12.Text = "";
				Label13.Text = "";
				Label14.Text = "";
				Label7.Text = "Section";
				Label10.Text = "Width";
				Label11.Text = "Length";
                var trailer = ((VehicleConfigurationSpecs) modRedBook.Statics.CurrentVehicleObject).ToTrailerData();
                //var trailer = (TrailerData)modRedBook.Statics.CurrentVehicleObject;
                if (trailer != null)
                {
                    lblEngine.Text = trailer.Section;
                    lblEngineDesc.Text = trailer.Width;
                    lblTransMake.Text = trailer.Length;
                    lblYear.Text = trailer.Year.ToString();
                    lblManufacturer.Text = trailer.Manufacturer;
                }
                else
                {
                    lblEngine.Text = "";
                    lblEngineDesc.Text = "";
                    lblTransMake.Text = "";
                    lblManufacturer.Text = "";
                }
            }
			else if (modRedBook.Statics.VehicleType == "B")
			{
				// Truck Body Matthew 9/8/05
				this.Frame1.Text = "Truck Body Information";
				Label8.Text = "";
				Label12.Text = "";
				Label13.Text = "";
				Label14.Text = "";
				Label7.Text = "Model";
				Label10.Text = "Type";
				Label11.Text = "Description";
                var truckBody = ((VehicleConfigurationSpecs) modRedBook.Statics.CurrentVehicleObject).ToTruckBodyData();
                //var truckBody =(TruckBodyData) modRedBook.Statics.CurrentVehicleObject ?? new TruckBodyData();
                lblEngine.Text = truckBody.Model;
                    lblEngineDesc.Text =truckBody.Type;
                    lblTransMake.Text =truckBody.Description;
                    lblYear.Text = truckBody.Year.ToString();
                    lblManufacturer.Text =truckBody.Manufacturer;

            }
			lblSRP.Text = Strings.Format(modRedBook.Statics.VSRP, "Currency");
			if (modRedBook.Statics.TotalCost != 0)
			{
				lblOptionCosts.Text = Strings.Format(modRedBook.Statics.TotalCost, "Currency");
			}
			else
			{
				lblOptionCosts.Text = "None Selected";
			}
			if (Conversion.Val(modRedBook.Statics.VSRP) != 0)
			{
				lblTotalValuation[0].Text = Strings.Format((FCConvert.ToInt32(modRedBook.Statics.VSRP) + modRedBook.Statics.TotalCost), "Currency");
			}
			else
			{
				lblTotalValuation[0].Text = "N/A";
			}
			// vbPorter upgrade warning: yr As int	OnWriteFCConvert.ToDouble(
			int yr;
			// yr = Val(lblYear.Text)
			yr = FCConvert.ToInt16((DateTime.Today.Year - Conversion.Val(lblYear.Text)) + 1);
			switch (yr)
			{
				case 0:
				case 1:
					{
						lblYearRate.Text = "0.024";
						MillRate = 0.024f;
						break;
					}
				case 2:
					{
						lblYearRate.Text = "0.0175";
						MillRate = 0.0175f;
						break;
					}
				case 3:
					{
						lblYearRate.Text = "0.0135";
						MillRate = 0.0135f;
						break;
					}
				case 4:
					{
						lblYearRate.Text = "0.010";
						MillRate = 0.01f;
						break;
					}
				case 5:
					{
						lblYearRate.Text = "0.0065";
						MillRate = 0.0065f;
						// Case 1995
						// lblYearRate.Text = "0.004"
						// MillRate = 0.004
						break;
					}
				default:
					{
						lblYearRate.Text = "0.004";
						MillRate = 0.004f;
						break;
					}
			}
			//end switch
			if (Conversion.Val(modRedBook.Statics.VSRP) != 0)
			{
				newvsrp = FCConvert.ToInt32(FCConvert.ToDouble(modRedBook.Statics.VSRP)) + FCConvert.ToInt32(modRedBook.Statics.TotalCost);
				lblTotalExcise.Text = Strings.Format(modGNBas.Round(MillRate * newvsrp, 2), "Currency");
			}
			else
			{
				lblTotalExcise.Text = "Not Available";
			}
			// mill MillRate stuff
			int x;
			// vbPorter upgrade warning: CurYear As int	OnWriteFCConvert.ToInt32(
			int CurYear;
			CurYear = DateTime.Now.Year;
			x = CurYear - modRedBook.Statics.VehicleYear;
            cmbMillRate.Enabled = true;
            cmbMillRate.Clear();
            if (x < 0)
				x = 0;
			if (x > 5)
			{
				x = 5;
				if (!cmbMillRate.Items.Contains("0.004    Year 6"))
				{
					cmbMillRate.Items.Add("0.004    Year 6");
				}
				cmbMillRate.Text = "0.004    Year 6";
				return;
			}

			if (x == 0 && !cmbMillRate.Items.Contains("0.024    Year 1 "))
			{
				cmbMillRate.Items.Add("0.024    Year 1 ");
				cmbMillRate.Text = "0.024    Year 1 ";
			}
			else if (x == 1 && !cmbMillRate.Items.Contains("0.0175  Year 2"))
			{
				cmbMillRate.Items.Add("0.0175  Year 2");
				cmbMillRate.Text = "0.0175  Year 2";
			}
			else if (x == 2 && !cmbMillRate.Items.Contains("0.0135  Year 3"))
			{
				cmbMillRate.Items.Add("0.0135  Year 3");
				cmbMillRate.Text = "0.0135  Year 3";
			}
			else if (x == 3 && !cmbMillRate.Items.Contains("0.010    Year 4"))
			{
				cmbMillRate.Items.Add("0.010    Year 4");
				cmbMillRate.Text = "0.010    Year 4";
			}
			else if (x == 4 && !cmbMillRate.Items.Contains("0.0065  Year 5"))
			{
				cmbMillRate.Items.Add("0.0065  Year 5");
				cmbMillRate.Text = "0.0065  Year 5";
			}
			else if (x == 5 && !cmbMillRate.Items.Contains("0.004    Year 6"))
			{
				cmbMillRate.Items.Add("0.004    Year 6");
				cmbMillRate.Text = "0.004    Year 6";
			}

			if (x < 5)
			{
				if (x + 1 == 1 && !cmbMillRate.Items.Contains("0.0175  Year 2"))
				{
					cmbMillRate.Items.Add("0.0175  Year 2");
				}
				else if (x + 1 == 2 && !cmbMillRate.Items.Contains("0.0135  Year 3"))
				{
					cmbMillRate.Items.Add("0.0135  Year 3");
				}
				else if (x + 1 == 3 && !cmbMillRate.Items.Contains("0.010    Year 4"))
				{
					cmbMillRate.Items.Add("0.010    Year 4");
				}
				else if (x + 1 == 4 && !cmbMillRate.Items.Contains("0.0065  Year 5"))
				{
					cmbMillRate.Items.Add("0.0065  Year 5");
				}
				else if (x + 1 == 5 && !cmbMillRate.Items.Contains("0.004    Year 6"))
				{
					cmbMillRate.Items.Add("0.004    Year 6");				
				}
			}
			if (x < 4)
			{
				if (x + 2 == 2 && !cmbMillRate.Items.Contains("0.0135  Year 3"))
				{
					cmbMillRate.Items.Add("0.0135  Year 3");
				}
				else if (x + 2 == 3 && !cmbMillRate.Items.Contains("0.010    Year 4"))
				{
					cmbMillRate.Items.Add("0.010    Year 4");
				}
				else if (x + 2 == 4 && !cmbMillRate.Items.Contains("0.0065  Year 5"))
				{
					cmbMillRate.Items.Add("0.0065  Year 5");
				}
				else if (x + 2 == 5 && !cmbMillRate.Items.Contains("0.004    Year 6"))
				{
					cmbMillRate.Items.Add("0.004    Year 6");
				}
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
				modRedBook.RedbookShow();
		}

		private void mnuBackOption_Click(object sender, System.EventArgs e)
		{
			if (cmdBack.Enabled)
			{
				cmdBack_Click();
			}
		}

		private void mnuFileBackSelection_Click(object sender, System.EventArgs e)
		{
			if (cmdSelection.Enabled)
			{
				cmdSelection_Click();
			}
		}

		private void mnuFileBackStart_Click(object sender, System.EventArgs e)
		{
			if (cmdStart.Enabled)
			{
				cmdStart_Click();
			}
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdDone_Click();
		}

		private void mnuFileSticker_Click(object sender, System.EventArgs e)
		{
			if (cmdSticker.Enabled)
			{
				cmdSticker_Click();
			}
		}

		private void optMillRate_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (FormLoaded == true)
			{
				int srp;
				// newvsrp '= Val(lblTotalValuation.Text)
				switch (cmbMillRate.Text)
				{
					case "0.024    Year 1 ":
						{
							MillRate = 0.024f;
							break;
						}
					case "0.0175  Year 2":
						{
							MillRate = 0.0175f;
							break;
						}
					case "0.0135  Year 3":
						{
							MillRate = 0.0135f;
							break;
						}
					case "0.010    Year 4":
						{
							MillRate = 0.01f;
							break;
						}
					case "0.0065  Year 5":
						{
							MillRate = 0.0065f;
							break;
						}
					case "0.004    Year 6":
						{
							MillRate = 0.004f;
							break;
						}
				}
				//end switch
				lblYearRate.Text = FCConvert.ToString(MillRate);
				if (StickerPrice == 0)
				{
					lblTotalExcise.Text = Strings.Format(modGNBas.Round(MillRate * FCConvert.ToInt32(newvsrp), 2), "Currency");
				}
				else
				{
					lblTotalExcise.Text = Strings.Format(modGNBas.Round(MillRate * StickerPrice, 2), "Currency");
				}
			}
		}

		private void optMillRate_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbMillRate.SelectedIndex;
			optMillRate_CheckedChanged(index, sender, e);
		}

		private void SetCustomFormColors()
		{
            Label5.ForeColor = ColorTranslator.FromOle(0x800000);
			lblTotalExcise.ForeColor = ColorTranslator.FromOle(0xC0);
			lblSRP.ForeColor = ColorTranslator.FromOle(0x800000);
			lblOptionCosts.ForeColor = ColorTranslator.FromOle(0x800000);
			lblTotalValuation[0].ForeColor = ColorTranslator.FromOle(0x800000);
			lblStickerPrice.ForeColor = ColorTranslator.FromOle(0x800000);
			lblYearRate.ForeColor = ColorTranslator.FromOle(0x800000);
		}
	}
}
