﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.CentralParties.Models;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Models;
using SharedApplication.Enums;
using SharedDataAccess;
using SharedDataAccess.CentralParties;
using SharedDataAccess.Clerk;
using Xunit;
namespace ClerkTests
{
    public class DogSearchQueryHandlerTests
    {
        [Fact]
        public void LocationSearchFindsNothingWhenNoMatches()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1),
                    LocationNum = "1", LocationStr = "South Street", NoRequiredFields = false, PartyId = 2
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, FirstName = "Some", LastName = "Body", PartyGuid =  Guid.NewGuid()});
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { LocationStreet = "Main", TypeToSearchBy = DogSearchBy.Location,MatchCriteriaType = SearchCriteriaMatchType.StartsWith, TypesOfRecords = DogSearchRecordType.All};
                    var returnedRecords =  testHandler.ExecuteQuery(testQuery);
                    Assert.False(returnedRecords.Any());
                }
                    
            }

            
        }

        [Fact]
        public void LocationSearchFindsMatchStartingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted =  false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019,8,1), LocationNum = "1", LocationStr = "South Street", NoRequiredFields =  false, PartyId = 1});
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "", LocationStr = "Main Street", LocationNum = "2", Id = 2, NoRequiredFields = false,
                    PartyId = 2, Deleted = false, DogNumbers = "2", LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017,01,01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid()});
                    partyContext.Parties.Add(new CentralParty(){Id = 1, PartyType = 1, FirstName = "Unimportant", LastName = "Party", PartyGuid =  Guid.NewGuid(), DateCreated = new DateTime(2016,1,1)});
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria(){LocationStreet = "Main", TypeToSearchBy = DogSearchBy.Location, MatchCriteriaType = SearchCriteriaMatchType.StartsWith, TypesOfRecords = DogSearchRecordType.All};
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(2,returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void LocationAndLocationNumberSearchFindsMatchStartingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid() });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1) });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { LocationStreet = "Main", LocationNumber = 1, TypeToSearchBy = DogSearchBy.Location, MatchCriteriaType = SearchCriteriaMatchType.StartsWith, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(1, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void LocationSearchFindsMatchContainingCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "North Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "North Park Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid() });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1) });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { LocationStreet = "park",  TypeToSearchBy = DogSearchBy.Location, MatchCriteriaType = SearchCriteriaMatchType.Contains, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(2, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void LocationSearchFindsMatchEndingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "South Main", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid() });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1) });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { LocationStreet = "main", LocationNumber = 1, TypeToSearchBy = DogSearchBy.Location, MatchCriteriaType = SearchCriteriaMatchType.EndsWith, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(1, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void CitySearchFindsMatchStartingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid(), Addresses = {new PartyAddress(){ Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "Metropolis", PartyId = 2, State = "Maine", Zip = "12345"}}});
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1) ,Addresses = {new PartyAddress(){ Address1 = "10 Main Street", City = "Gotham", State = "Maine", Zip = "12345", AddressType = "Primary", Id =  4, PartyId = 1}}});
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { City = "metro",TypeToSearchBy = DogSearchBy.City, MatchCriteriaType = SearchCriteriaMatchType.StartsWith, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(2, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void CitySearchFindsMatchContainingCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "Gotham", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { City = "york", TypeToSearchBy = DogSearchBy.City, MatchCriteriaType = SearchCriteriaMatchType.Contains, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(2, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void CitySearchFindsMatchEndingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "New York", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { City = "york", TypeToSearchBy = DogSearchBy.City, MatchCriteriaType = SearchCriteriaMatchType.EndsWith, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(1, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void OwnerSearchFindsMatchStartingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Newhart", FirstName = "Bobby", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "New York", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { OwnerFirstName = "Bob",OwnerLastName = "New", TypeToSearchBy = DogSearchBy.OwnerName, MatchCriteriaType = SearchCriteriaMatchType.StartsWith, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(2, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void OwnerSearchFindsMatchContainingCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Newhart", FirstName = "Bobby", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Bart", LastName = "Simpson", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "New York", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { OwnerFirstName = "Art", OwnerLastName = "Imp", TypeToSearchBy = DogSearchBy.OwnerName, MatchCriteriaType = SearchCriteriaMatchType.Contains, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(1, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void OwnerSearchFindsMatchEndingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Reddington", FirstName = "Raymond", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Stingray", LastName = "dred", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "New York", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { OwnerFirstName = "Ray", OwnerLastName = "Red", TypeToSearchBy = DogSearchBy.OwnerName, MatchCriteriaType = SearchCriteriaMatchType.EndsWith, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(1, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        public void DogNameSearchFindsMatchStartingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() {Id = 2,DogName = "Fluffy", OwnerNum = 1});
                clerkContext.DogInfos.Add(new DogInfo() {Id = 3, DogName = "Mister Fluff", OwnerNum = 2});
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria(){DogName = "Fluff",TypeToSearchBy = DogSearchBy.DogName, MatchCriteriaType =  SearchCriteriaMatchType.StartsWith, TypesOfRecords =  DogSearchRecordType.All};
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(2,returnedRecords.First().DogId);

            }
        }

        [Fact]
        public void DogNameSearchFindsMatchContainingCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1 });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2 });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria() { DogName = "Fluff", TypeToSearchBy = DogSearchBy.DogName, MatchCriteriaType = SearchCriteriaMatchType.Contains, TypesOfRecords = DogSearchRecordType.All };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Equal(2,returnedRecords.Count());
            }
        }

        [Fact]
        public void DogNameSearchFindsMatchEndingWithCriteria()

        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1 });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2 });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria() { DogName = "Fluff", TypeToSearchBy = DogSearchBy.DogName, MatchCriteriaType = SearchCriteriaMatchType.EndsWith
, TypesOfRecords = DogSearchRecordType.All };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(3, returnedRecords.First().DogId);

            }
        }

        [Fact]
        public void DogBreedSearchFindsMatchStartingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1 ,DogBreed = "Golden Retriever"});
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, DogBreed = "Retriever, Golden"});
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    Breed = "golden",
                    TypeToSearchBy = DogSearchBy.Breed,
                    MatchCriteriaType = SearchCriteriaMatchType.StartsWith
                    ,
                    TypesOfRecords = DogSearchRecordType.All
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(2, returnedRecords.First().DogId);
            }
        }
        [Fact]
        public void DogBreedSearchFindsMatchContainingCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1, DogBreed = "Golden Retriever" });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, DogBreed = "Toy Poodle Mix" });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    Breed = "poodle",
                    TypeToSearchBy = DogSearchBy.Breed,
                    MatchCriteriaType = SearchCriteriaMatchType.Contains
                    ,
                    TypesOfRecords = DogSearchRecordType.All
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(3, returnedRecords.First().DogId);
            }
        }
        [Fact]
        public void DogBreedSearchFindsMatchEndingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1, DogBreed = "Beagle Mix" });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, DogBreed = "Mixed Breed" });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    Breed = "mix",
                    TypeToSearchBy = DogSearchBy.Breed,
                    MatchCriteriaType = SearchCriteriaMatchType.EndsWith
                    ,
                    TypesOfRecords = DogSearchRecordType.All
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(2, returnedRecords.First().DogId);
            }
        }

        [Fact]
        public void DogVetSearchFindsMatchStartingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1, Dogvet = "Bangor Veterinarian"});
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, Dogvet = "Dog Docs fo Bangor" });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    Veterinarian = "bangor",
                    TypeToSearchBy = DogSearchBy.Veterinarian,
                    MatchCriteriaType = SearchCriteriaMatchType.StartsWith
                    ,
                    TypesOfRecords = DogSearchRecordType.All
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(2, returnedRecords.First().DogId);
            }
        }

        [Fact]
        public void DogVetSearchFindsMatchContainingCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1, Dogvet = "Dogs R Us" });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, Dogvet = "Metropolis Veterinarian Services" });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    Veterinarian = "vet",
                    TypeToSearchBy = DogSearchBy.Veterinarian,
                    MatchCriteriaType = SearchCriteriaMatchType.Contains

                    ,
                    TypesOfRecords = DogSearchRecordType.All
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(3, returnedRecords.First().DogId);
            }
        }

        [Fact]
        public void DogVetSearchFindsMatchEndingWithCriteria()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1, Dogvet = "Bangor Veterinarian Service" });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, Dogvet = "Service Dog Doctor" });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    Veterinarian = "service",
                    TypeToSearchBy = DogSearchBy.Veterinarian,
                    MatchCriteriaType = SearchCriteriaMatchType.EndsWith
                    ,
                    TypesOfRecords = DogSearchRecordType.All
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(2, returnedRecords.First().DogId);
            }
        }

        [Fact]
        private void RabiesTagSearchFindsMatch()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1, RabiesTagNo = "12345"});
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, RabiesTagNo = "123456"});
                clerkContext.DogInfos.Add(new DogInfo(){Id = 4,DogName = "Barkster", OwnerNum = 10,RabiesTagNo = "912345"});
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    RabiesTag = "12345",
                    TypeToSearchBy = DogSearchBy.RabiesTagNumber
                    ,
                    TypesOfRecords = DogSearchRecordType.All
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(2, returnedRecords.First().DogId);
            }
        }

        [Fact]
        private void LicenseTagSearchFindsMatch()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 1, DogName = "Fluffy", OwnerNum = 1, TagLicNum = "12345" });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, TagLicNum = "123456" });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 4, DogName = "Barkster", OwnerNum = 10, TagLicNum = "912345" });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    LicenseTag = "12345",
                    TypeToSearchBy = DogSearchBy.LicenseTagNumber
                    ,
                    TypesOfRecords = DogSearchRecordType.All
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(1, returnedRecords.First().DogId);
            }
        }

        [Fact]
        private void KennelSearchFindsMatch()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2),
                    KennelLicenses = {new KennelLicense(){DogNumbers = "1", Id = 1, LicenseId = 2, OwnerNum =  2}}
                });
                clerkContext.DogInventories.Add(new DogInventory(){Id = 2, StickerNumber = "1000", Type = "K"});
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "New York", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { KennelLicense = 1000, TypeToSearchBy = DogSearchBy.KennelLicense, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(2, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        private void KennelSearchFindsNoMatchWhenDeletedOwnersNotIncluded()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = true,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2),
                    KennelLicenses = { new KennelLicense() { DogNumbers = "1", Id = 1, LicenseId = 2, OwnerNum = 2 } }
                });
                clerkContext.DogInventories.Add(new DogInventory() { Id = 2, StickerNumber = "1000", Type = "K" });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Person", FirstName = "Some", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Another", LastName = "Party", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "New York", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { KennelLicense = 1000, TypeToSearchBy = DogSearchBy.KennelLicense, TypesOfRecords = DogSearchRecordType.All };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Empty(returnedRecords);
                    //Assert.Single(returnedRecords);
                    //Assert.Equal(2, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        private void DogSearchFindsOnlyIndividualsWhenTypeIsDogs()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1 , KennelDog =  false});
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, KennelDog = true});
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    DogName = "Fluff",
                    TypeToSearchBy = DogSearchBy.DogName,
                    MatchCriteriaType = SearchCriteriaMatchType.Contains
                    ,
                    TypesOfRecords = DogSearchRecordType.Dog
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(2, returnedRecords.First().DogId);

            }
        }

        [Fact]
        private void DogSearchFindsOnlyKennelDogsWhenTypeIsKennel()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogInfos.Add(new DogInfo() { Id = 2, DogName = "Fluffy", OwnerNum = 1, KennelDog = false });
                clerkContext.DogInfos.Add(new DogInfo() { Id = 3, DogName = "Mister Fluff", OwnerNum = 2, KennelDog = true });
                clerkContext.SaveChanges();
                var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                contextFactory.GetClerkContext().Returns(clerkContext);
                var testHandler = new DogSearchQueryHandler(contextFactory);
                var testQuery = new DogSearchCriteria()
                {
                    DogName = "Fluff",
                    TypeToSearchBy = DogSearchBy.DogName,
                    MatchCriteriaType = SearchCriteriaMatchType.Contains
                    ,
                    TypesOfRecords = DogSearchRecordType.DogsInKennels
                };
                var returnedRecords = testHandler.ExecuteQuery(testQuery);
                Assert.Single(returnedRecords);
                Assert.Equal(3, returnedRecords.First().DogId);

            }
        }

        [Fact]
        private void DogOwnerSearchFindsOnlyOwnersWithIndividualDogsWhenTypeIsDogs()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "", Id = 1, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 2,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2)
                    ,Dogs = {new DogInfo() {DogName = "Fido", Id = 2,OwnerNum = 2, KennelDog =  false} }
                });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Fonzarelli", FirstName = "Arthur", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Bart", LastName = "Simpson", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "New York", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { OwnerFirstName = "Art", TypeToSearchBy = DogSearchBy.OwnerName, MatchCriteriaType = SearchCriteriaMatchType.Contains, TypesOfRecords = DogSearchRecordType.Dog };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(2, returnedRecords.First().OwnerId);

                }
            }
        }

        [Fact]
        private void DogOwnerSearchFindsOnlyOwnersWithKennelsWhenTypeIsKennels()
        {
            using (var clerkContext = new ClerkContext(GetDBOptions<ClerkContext>()))
            {
                clerkContext.DogOwners.Add(new DogOwner() { Comments = "", Deleted = false, DogNumbers = "1", Id = 2, LastUpdated = new DateTime(2019, 8, 1), LocationNum = "1", LocationStr = "Main Street", NoRequiredFields = false, PartyId = 1 });
                clerkContext.DogOwners.Add(new DogOwner()
                {
                    Comments = "",
                    LocationStr = "Main Street",
                    LocationNum = "10",
                    Id = 1,
                    NoRequiredFields = false,
                    PartyId = 2,
                    Deleted = false,
                    DogNumbers = "2",
                    LastUpdated = new DateTime(2019, 7, 2),
                    KennelLicenses = { new KennelLicense() { DogNumbers = "3", Id = 1, LicenseId = 2, OwnerNum = 1 } }
                });
                clerkContext.DogInventories.Add(new DogInventory() { Id = 2, StickerNumber = "1000", Type = "K" });
                clerkContext.SaveChanges();
                using (var partyContext = new CentralPartyContext(GetDBOptions<CentralPartyContext>()))
                {
                    partyContext.Parties.Add(new CentralParty() { Id = 2, PartyType = 1, DateCreated = new DateTime(2017, 01, 01), LastName = "Fonzarelli", FirstName = "Arthur", PartyGuid = Guid.NewGuid(), Addresses = { new PartyAddress() { Id = 3, AddressType = "Primary", Address1 = "PO Box 1", City = "New York City", PartyId = 2, State = "Maine", Zip = "12345" } } });
                    partyContext.Parties.Add(new CentralParty() { Id = 1, PartyType = 1, FirstName = "Bart", LastName = "Simpson", PartyGuid = Guid.NewGuid(), DateCreated = new DateTime(2016, 1, 1), Addresses = { new PartyAddress() { Address1 = "10 Main Street", City = "New York", State = "Maine", Zip = "12345", AddressType = "Primary", Id = 4, PartyId = 1 } } });
                    partyContext.SaveChanges();
                    var contextFactory = NSubstitute.Substitute.For<ITrioContextFactory>();
                    contextFactory.GetClerkContext().Returns(clerkContext);
                    contextFactory.GetCentralPartyContext().Returns(partyContext);
                    var testHandler = new DogSearchQueryHandler(contextFactory);
                    var testQuery = new DogSearchCriteria() { OwnerFirstName = "Art", TypeToSearchBy = DogSearchBy.OwnerName, MatchCriteriaType = SearchCriteriaMatchType.Contains, TypesOfRecords = DogSearchRecordType.DogsInKennels };
                    var returnedRecords = testHandler.ExecuteQuery(testQuery);
                    Assert.Single(returnedRecords);
                    Assert.Equal(1, returnedRecords.First().OwnerId);

                }
            }
        }
        private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
        {
            var builder = new DbContextOptionsBuilder<TContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid
               

            return builder.Options;
        }
    }
}
