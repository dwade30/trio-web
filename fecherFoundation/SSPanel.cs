using System.Windows.Forms;
using System;
using System.Drawing;
namespace fecherFoundation
{

    public enum Constants_Alignment
    {

        /// <summary>
        /// 
        /// </summary>
        ssBottomOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssCenterBottom,

        /// <summary>
        /// 
        /// </summary>
        ssCenterMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssCenterTop,

        /// <summary>
        /// 
        /// </summary>
        ssLeftBottom,

        /// <summary>
        /// 
        /// </summary>
        ssLeftMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssLeftOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssLeftTop,

        /// <summary>
        /// 
        /// </summary>
        ssRightBottom,

        /// <summary>
        /// 
        /// </summary>
        ssRightMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssRightOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssRightTop,

        /// <summary>
        /// 
        /// </summary>
        ssTopOfCaption
    }

    public enum Constants_Bevel
    {

        /// <summary>
        /// 
        /// </summary>
        ssInsetBevel,

        /// <summary>
        /// 
        /// </summary>
        ssNoneBevel,

        /// <summary>
        /// 
        /// </summary>
        ssRaisedBevel
    }

	/// <summary>
	/// 
	/// </summary>
	public class SSPanel : Control
	{

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public uint PictureMaskColor {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public int PictureAnimationDelay {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public int PictureFrames {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public bool PictureUseMask {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public Image Picture {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public Constants_Bevel BevelOuter {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public Constants_Alignment PictureAlignment {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public bool RoundedCorners {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}
	}
}
