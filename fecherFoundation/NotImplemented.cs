﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    [System.AttributeUsage(System.AttributeTargets.Class |
                           System.AttributeTargets.Struct |
                           System.AttributeTargets.Method |
                           System.AttributeTargets.Property,
                           AllowMultiple = false)  // multiuse attribute
    ]
    public class NotImplemented : System.Attribute
    {
        public NotImplemented()
        {
        }
    }
}
