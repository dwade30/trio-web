using System;
using System.Collections;
using System.Collections.Generic;
namespace fecherFoundation
{

	/// <summary>
	/// 
	/// </summary>
    public class Panes : List<Pane>
    {
        private SSSplitter parent;
        //List<Pane> panes;

        public Panes(SSSplitter splitter)
        {
            this.parent = splitter;
            //panes = new List<Pane>();
        }

        //public new void Add(Pane pane)
        //{
        //    this.Add(pane, this[0], SplitterBarStyle.Vertical);
        //}

        public void Add(Pane pane, Pane parent, SplitterBarStyle style)
        {
            base.Add(pane);
            pane.ParentSplitter = this.parent;
            //parent.Controls.Add(pane, Convert.ToInt32(style));
        }

        public void Remove(int index)
        {
            if (index >= base.Count || index < 0)
            {
                return;
            }

            base.RemoveAt(index);
        }

        public new void Clear()
        {
            base.Clear();
        }

        public new Pane this[int index]
        {
            get
            {
                if (!CheckIndex(index))
                {
                    return null;
                }

                return base[index];
            }
        }

        public Pane this[string name]
        {
            get
            {
                foreach (Pane pane in this)
                {
                    if (pane.Name == name)
                    {
                        return pane;
                    }
                }

                return null;
            }
        }


        private bool CheckIndex(int index)
        {
            if (index >= base.Count || index < 0)
            {
                return false;
            }
            return true;
        }
    }
}
