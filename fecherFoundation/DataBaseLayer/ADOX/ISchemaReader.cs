﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.ADOX
{

    /// <summary>
    /// General interface for reading schema information from database for FC ADOX implementation
    /// One way could be to use GetSchema from DbConnection but must be implemented specific for each provider
    /// https://msdn.microsoft.com/de-de/library/kcax58fh(v=vs.110).aspx
    /// </summary>
    /// 
    public  interface ISchemaReader
    {
        List<FCTable> ReadTables();

        IEnumerable<FCColumn> ReadColumns(FCTable table);

        List<FCIndex> ReadIndexes(FCTable table);

        IEnumerable<FCColumn> ReadIndexColumns(FCIndex index);
       
    }
}
