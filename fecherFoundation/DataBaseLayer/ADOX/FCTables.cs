﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.ADOX
{
    public class FCTables : IEnumerable<FCTable>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private List<FCTable> tables = null;

        #endregion

        #region Constructors

        public FCTables(List<FCTable> tables)
        {
            this.tables = tables;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public int Count
        {
            get
            {
                return (tables == null) ? 0 : tables.Count;
            }
        }

        public FCTable this[string name]
        {
            get
            {
                return this.GetTable(name);
            }
            set
            {
                FCTable field = this.GetTable(name);
                field = value;
            }
        }

        public FCTable this[int index]
        {
            get
            {
                return this.GetTable(index);
            }
            set
            {
                FCTable field = this.GetTable(index);
                field = value;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCTable> GetEnumerator()
        {
            return tables.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal FCTable GetTable(int Index)
        {
            return tables[Index];
        }

        internal FCTable GetTable(string tableName)
        {
            tableName = tableName.ToUpper();
            return tables.FindLast(delegate(FCTable target)
            {
                return target.Name.ToUpper() == tableName;
            });
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
