﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.OleDb;
using System.Diagnostics;

namespace fecherFoundation.DataBaseLayer.ADOX
{
    /// <summary>
    /// Concret implementation to read schema information using OleDbProvider and GetSchema method from OleDbConnection
    /// Read https://duelmonsterprograming.wordpress.com/2008/07/28/using-getschema-part-01/ 
    /// for restrictions in GetSchema (2nd param) using OleDbProvider
    /// </summary>
    internal class OleDbSchemaReader : ISchemaReader
    {
        #region Private Members

        private OleDbConnection connection;

        #endregion

        #region Constructors

        public OleDbSchemaReader(OleDbConnection connection)
        {
            this.connection = connection;
        }

        #endregion

        #region ISchemaReader Interface

        public List<FCTable> ReadTables()
        {
            return (from r in connection.GetSchema("Tables").AsEnumerable()
                    select new FCTable(this, r.Field<string>("TABLE_SCHEMA"), r.Field<string>("TABLE_NAME"), r.Field<string>("TABLE_TYPE")))
                    .ToList();
        }

        public IEnumerable<FCColumn> ReadColumns(FCTable table)
        {
            return  (from r in connection.GetSchema("Columns", new string[]{null, table.Schema, table.Name}).AsEnumerable()
                    select new FCColumn(r.Field<string>("COLUMN_NAME"), r.Field<OleDbType>("DATA_TYPE")))
                    ;
        }

        public List<FCIndex> ReadIndexes(FCTable table)
        {
            Debug.WriteLine("ReadIndexes {0}.{1}", table.Schema, table.Name);

            var list = from r in connection.GetSchema("Indexes", new string[] { null, table.Schema, null, null, table.Name }).AsEnumerable()
                        select new FCIndex(this, table, r.Field<string>("INDEX_SCHEMA"), r.Field<string>("INDEX_NAME"), r.Field<bool>("PRIMARY_KEY"));

            return list.Distinct(FCIndex.GetComparer()).ToList();
        }

        public IEnumerable<FCColumn> ReadIndexColumns(FCIndex index)
        {
            return  (from r in connection.GetSchema("Indexes", new string[] { null, index.Table.Schema, index.Name, null, index.Table.Name }).AsEnumerable()
                    select index.Table.FindColumn( r.Field<string>("COLUMN_NAME")))
                    ;
        }

        #endregion

    }
}
