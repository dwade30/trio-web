﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.ADOX
{
    public class FCColumns : List<FCColumn>
    {
        public FCColumns(IEnumerable<FCColumn> list) : base(list)
        {

        }

        public FCColumn this[string name]
        {
            get
            {
                return this.FindLast(c => c.Name.ToUpper() == name.ToUpper());
            }
            set
            {
                FCColumn field = this[name];
                field = value;
            }
        }
    }
}
