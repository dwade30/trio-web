﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.OleDb;


namespace fecherFoundation.DataBaseLayer.ADOX
{
    public class FCColumn
    {

        #region Public Members

        public string Name 
        { 
            get 
            { 
                return name; 
            } 
        }

        public int Type
        {
            get
            {
                //PJ: Type mapped: ADO.NET-Driver returns WChar for NVARCHAR instead of VarWChar like ADO does. 
                if (type == OleDbType.WChar)
                {
                    return Convert.ToInt32(OleDbType.VarWChar);
                }

                return Convert.ToInt32(type);
            }
        }

        //not used anymore
        //public int DefinedSize { get; set; }

        #endregion

        #region Private Members

        private string name;

        private OleDbType type;

        #endregion

        
        #region Constructors
        
        internal FCColumn(string name, OleDbType type)
        {
            
            this.name = name;
            this.type = type;
        }

        #endregion

    }
}
