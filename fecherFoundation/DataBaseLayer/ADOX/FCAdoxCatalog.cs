﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.OleDb;

namespace fecherFoundation.DataBaseLayer.ADOX
{
    public class FCAdoxCatalog
    {
        #region Public Members

        public IDbConnection ActiveConnection 
        {
            get { return activeConnection; }
            set
            {
                if (activeConnection == null || !activeConnection.Equals(value))
                {
                    activeConnection = value;
                    ConnectionChanged();
                }
            }
        }

        public FCTables Tables
        {
            get { return tables.Value; }
            //set { tables = value; }
        }

        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        Lazy<FCTables> tables = null;

        IDbConnection activeConnection = null;

        ISchemaReader reader;

        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private void ConnectionChanged()
        {
            if (activeConnection is OleDbConnection)
            {
                reader = new OleDbSchemaReader((OleDbConnection)activeConnection);
            }
            else
            {
                throw new NotImplementedException("Currently FCAdoxCatalog only suppports OleDbConnections !");
            }

            ReassignReader(reader);
        }

        internal void ReassignReader(ISchemaReader reader)
        {
            this.reader = reader;
            this.tables = new Lazy<FCTables>(() => new FCTables(reader.ReadTables()));
        }
        
        #endregion
    }
}
