﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.OleDb;

namespace fecherFoundation.DataBaseLayer.ADOX
{
    public class FCTable
    {

        #region Public Members

        public FCColumns Columns { get { return columns.Value; } }

        public List<FCIndex> Indexes { get { return indexes.Value; } }

        public string Name { get { return name; } }

        public string Type { get { return type; } }
        
        #endregion

        #region Private Members

        private Lazy<FCColumns> columns;

        private Lazy<List<FCIndex>> indexes;

        private string schema;

        private string name;

        private string type;

        private ISchemaReader reader;

        #endregion

        #region Constructors

        internal FCTable(ISchemaReader reader, string schema, string name, string type)
        {
            this.reader = reader;
            this.schema = schema;
            this.name = name;
            this.type = type;

            columns = new Lazy<FCColumns>(() => new FCColumns(reader.ReadColumns(this)));
            indexes = new Lazy<List<FCIndex>>(() => reader.ReadIndexes(this));
        }

        #endregion

        #region Internal Members

        internal string Schema { get { return schema; } }
        
        internal FCColumn FindColumn(string columnName)
        {
            return this.Columns.Where(c => c.Name == columnName).FirstOrDefault();
        }

        #endregion

    }


}
