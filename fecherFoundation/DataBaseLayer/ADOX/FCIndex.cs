﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.OleDb;

namespace fecherFoundation.DataBaseLayer.ADOX
{
    public class FCIndex 
    {
        #region Public Members

        public FCColumns Columns { get { return columns.Value; } }

        public string Name { get { return name; } }

        public bool PrimaryKey { get { return primaryKey; } }
        
        #endregion


        #region Private Members

        private Lazy<FCColumns> columns;

        private string name;

        private bool primaryKey;

        private string schema;

        private FCTable table;

        private ISchemaReader reader;

        #endregion

        #region Internal Members

        internal string Schema { get { return schema; } }

        internal FCTable Table { get { return table; } }

        #endregion


        #region Constructors

        internal FCIndex(ISchemaReader reader, FCTable table, string schema, string name, bool primaryKey)
        {
            this.reader = reader;
            this.table = table;
            this.name = name;
            this.schema = schema;
            this.primaryKey = primaryKey;

            this.columns = new Lazy<FCColumns>(() => new FCColumns(reader.ReadIndexColumns(this)));
        }

        #endregion


        #region Internal Methods

        class FCIndexComparer : IEqualityComparer<FCIndex>
        {
            public bool Equals(FCIndex x, FCIndex y)
            {
                return (x.Schema == y.Schema && x.Name == y.Name);
            }

            public int GetHashCode(FCIndex obj)
            {
                if (obj == null)
                    return 0;
                else
                    return obj.Schema.GetHashCode() | obj.Name.GetHashCode();
            }
        }

        internal static IEqualityComparer<FCIndex> GetComparer()
        {
            return new FCIndexComparer();
        }

        #endregion

    }
}
