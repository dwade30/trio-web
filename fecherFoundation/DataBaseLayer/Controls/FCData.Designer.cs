﻿using Wisej.Web;
namespace fecherFoundation
{
    partial class FCData
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstButton = new Wisej.Web.Button();
            this.prevButton = new Wisej.Web.Button();
            this.nextButton = new Wisej.Web.Button();
            this.lastButton = new Wisej.Web.Button();
            this.dataText = new Wisej.Web.TextBox();
            this.leftButtonPanel = new Wisej.Web.Panel();
            this.textPanel = new Wisej.Web.Panel();
            this.rightButtonPanel = new Wisej.Web.Panel();
            this.leftButtonPanel.SuspendLayout();
            this.textPanel.SuspendLayout();
            this.rightButtonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // firstButton
            // 
            this.firstButton.Location = new System.Drawing.Point(0, 0);
            this.firstButton.Margin = new Wisej.Web.Padding(0);
            this.firstButton.Name = "firstButton";
            this.firstButton.Size = new System.Drawing.Size(30, 20);
            this.firstButton.TabIndex = 0;
            this.firstButton.Text = "|<";
            this.firstButton.MouseUp += new Wisej.Web.MouseEventHandler(this.dataAnyMouseUp);
            // 
            // prevButton
            // 
            this.prevButton.Location = new System.Drawing.Point(30, 0);
            this.prevButton.Margin = new Wisej.Web.Padding(0);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(30, 20);
            this.prevButton.TabIndex = 1;
            this.prevButton.Text = "<";
            this.prevButton.MouseUp += new Wisej.Web.MouseEventHandler(this.dataAnyMouseUp);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(0, 0);
            this.nextButton.Margin = new Wisej.Web.Padding(0);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(30, 20);
            this.nextButton.TabIndex = 0;
            this.nextButton.Text = ">";
            this.nextButton.MouseUp += new Wisej.Web.MouseEventHandler(this.dataAnyMouseUp);
            // 
            // lastButton
            // 
            this.lastButton.Location = new System.Drawing.Point(30, 0);
            this.lastButton.Margin = new Wisej.Web.Padding(0);
            this.lastButton.Name = "lastButton";
            this.lastButton.Size = new System.Drawing.Size(30, 20);
            this.lastButton.TabIndex = 1;
            this.lastButton.Text = ">|";
            this.lastButton.MouseUp += new Wisej.Web.MouseEventHandler(this.dataAnyMouseUp);
            // 
            // dataText
            // 
            this.dataText.Dock = Wisej.Web.DockStyle.Fill;
            this.dataText.Location = new System.Drawing.Point(0, 0);
            this.dataText.Margin = new Wisej.Web.Padding(0);
            this.dataText.Name = "dataText";
            this.dataText.Size = new System.Drawing.Size(204, 20);
            this.dataText.TabIndex = 2;
            this.dataText.MouseUp += new Wisej.Web.MouseEventHandler(this.dataAnyMouseUp);
            // 
            // leftButtonPanel
            // 
            this.leftButtonPanel.Controls.Add(this.prevButton);
            this.leftButtonPanel.Controls.Add(this.firstButton);
            this.leftButtonPanel.Location = new System.Drawing.Point(0, 0);
            this.leftButtonPanel.Margin = new Wisej.Web.Padding(0);
            this.leftButtonPanel.Name = "leftButtonPanel";
            this.leftButtonPanel.Size = new System.Drawing.Size(60, 20);
            this.leftButtonPanel.TabIndex = 1;
            // 
            // textPanel
            // 
            this.textPanel.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.textPanel.Controls.Add(this.dataText);
            this.textPanel.Location = new System.Drawing.Point(60, 0);
            this.textPanel.Name = "textPanel";
            this.textPanel.Size = new System.Drawing.Size(204, 20);
            this.textPanel.TabIndex = 2;
            // 
            // rightButtonPanel
            // 
            this.rightButtonPanel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.rightButtonPanel.Controls.Add(this.lastButton);
            this.rightButtonPanel.Controls.Add(this.nextButton);
            this.rightButtonPanel.Location = new System.Drawing.Point(264, 0);
            this.rightButtonPanel.Margin = new Wisej.Web.Padding(0);
            this.rightButtonPanel.Name = "rightButtonPanel";
            this.rightButtonPanel.Size = new System.Drawing.Size(60, 20);
            this.rightButtonPanel.TabIndex = 3;
            // 
            // FCData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.Controls.Add(this.rightButtonPanel);
            this.Controls.Add(this.textPanel);
            this.Controls.Add(this.leftButtonPanel);
            this.Margin = new Wisej.Web.Padding(0);
            this.Name = "FCData";
            this.Size = new System.Drawing.Size(324, 20);
            this.leftButtonPanel.ResumeLayout(false);
            this.textPanel.ResumeLayout(false);
            this.textPanel.PerformLayout();
            this.rightButtonPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Button firstButton;
        private Button prevButton;
        private Button nextButton;
        private Button lastButton;
        private TextBox dataText;
        private Panel leftButtonPanel;
        private Panel textPanel;
        private Panel rightButtonPanel;

    }
}
