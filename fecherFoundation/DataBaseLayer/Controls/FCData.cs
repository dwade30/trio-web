﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCData : FCUserControl
    {
        #region constructor
        public FCData()
        {
            InitializeComponent();
        }
        #endregion constructor

        #region Public Members



        #endregion

        #region Properties
        public string Caption
        {
            get
            {
                return this.dataText.Text;
            }
            set
            {
                this.dataText.Text = value;
            }
        }
        public string ConnectAsString
        {
            get
            {
                return connectAsString;
            }
            set
            {
                //if (!String.IsNullOrEmpty(value))
                //{
                //    throw new NotImplementedException();
                //}
                connectAsString = value;
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Size ClientButtonSize
        {
            get
            {
                return this.firstButton.Size;
            }
        }
        public string DatabaseName
        {
            get
            {
                return databaseName;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    throw new NotImplementedException();
                }
                databaseName = value;
            }
        }

        public string RecordSource
        {
            get
            {
                return recordSource;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    throw new NotImplementedException();
                }
                recordSource = value;
            }
        }
        #endregion

        #region Private Members
        private string connectAsString = "";
        private string databaseName = "";
        private string recordSource = null;
        #endregion

        #region Protected Methods
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            if (this.textPanel != null)
            {
                this.leftButtonPanel.Width = 60;
                this.textPanel.Width = this.Size.Width - 80;
                this.rightButtonPanel.Width = 60;
            }
        }
        #endregion

        #region private Methods
        private void dataAnyMouseUp(object sender, MouseEventArgs e)
        {
            Point locationInDataControl = this.PointToClient(((Control)sender).PointToScreen(e.Location));

            MouseEventArgs newArgs = new MouseEventArgs(e.Button, e.Clicks, locationInDataControl.X, locationInDataControl.Y, e.Delta);

            base.OnMouseUp(newArgs);
        }
        #endregion

    }
}
