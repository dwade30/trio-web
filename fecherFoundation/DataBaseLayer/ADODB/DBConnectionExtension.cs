﻿using System;
using System.Collections.Generic;
using System.Text;
using Wisej.Web;
using System.Data;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data.OleDb;
using fecherFoundation;

namespace fecherFoundation.DataBaseLayer.ADO
{
    public enum EventStatusEnum
    {
        adStatusCancel = 4,
        adStatusCantDeny = 3,
        adStatusErrorsOccurred = 2,
        adStatusOK = 1,
        adStatusUnwantedEvent = 5
    }

    /// <summary>
    /// Performs a Query and return a DataSet
    /// </summary>
    public static class DbConnectionExtension
    {
        internal static Dictionary<DbConnection, DbConnectionDefinitions> mobjDefinitions = new Dictionary<DbConnection, DbConnectionDefinitions>();
        // exception list to use if there is an error. this is used for compatibility with ADODB.
        internal static List<Exception> ExceptionList = new List<Exception>();

        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms675023(v=vs.85).aspx
        /// Using the Execute method on a Connection Object (ADO) object executes whatever query you pass to the method in the CommandText argument on the specified connection. 
        /// If the CommandText argument specifies a row-returning query, any results that the execution generates are stored in a new Recordset object. If the command is not intended 
        /// to return results (for example, an SQL UPDATE query) the provider returns Nothing as long as the option adExecuteNoRecords is specified; otherwise Execute returns a closed Recordset.
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <param name="strCommand"></param>
        /// <returns></returns>
        public static DataTable Execute(this DbConnection objDbConnection, string strCommand)
        {
            DbCommand objCommand = objDbConnection.CreateCommand();
            objCommand.CommandText = strCommand;
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);
            if (objDbConnectionDefinitions.Transaction != null)
            {
                objCommand.Transaction = objDbConnectionDefinitions.Transaction;
            }

            // if the connectiontimeout was set, put it in the command timeout as well.
            if (objDbConnectionDefinitions.ConnectionTimeout != null)
            {
                // set the command timeout
                objCommand.CommandTimeout = Convert.ToInt32(objDbConnectionDefinitions.ConnectionTimeout);
            }

            WillExecuteEventArgs eventArgs = new WillExecuteEventArgs();
            eventArgs.Source = strCommand;
            eventArgs.Connection = objDbConnection as SqlConnection;
            eventArgs.Command = objCommand as SqlCommand;

            //CHE:TEMP removed
            //ExecuteWillExecute(objDbConnectionDefinitions, eventArgs);

            if (objCommand.CommandText != eventArgs.Source)
            {
                objCommand.CommandText = eventArgs.Source;
            }

            //non query
            System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(strCommand, @"^(insert\s|update\s|delete\s|set\s)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (match.Success)
            {
                objCommand.ExecuteNonQuery();
                return null;
            }
            //query
            return objCommand.OpenDataTable();
        }
        /// <summary>
        /// Returns the list 
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <returns></returns>
        public static List<Exception> Errors(this DbConnection objDbConnection)
        {
            
            return ExceptionList;
        }

        public static int Execute(this DbConnection objDbConnection, string strCommand, ref int recordsAffected, int? options = null)
        {
            try
            {
                DbCommand objCommand = objDbConnection.CreateCommand();
                objCommand.CommandText = strCommand;
                DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);
                if (objDbConnectionDefinitions.Transaction != null)
                {
                    objCommand.Transaction = objDbConnectionDefinitions.Transaction;
                }
                // if the connectiontimeout was set, put it in the command timeout as well.
                if (objDbConnectionDefinitions.ConnectionTimeout != null)
                {
                    // set the command timeout
                    objCommand.CommandTimeout = Convert.ToInt32(objDbConnectionDefinitions.ConnectionTimeout);
                }

                WillExecuteEventArgs eventArgs = new WillExecuteEventArgs();
                eventArgs.Source = strCommand;
                eventArgs.Connection = objDbConnection as SqlConnection;
                eventArgs.Command = objCommand as SqlCommand;

                //CHE:TEMP removed
                //ExecuteWillExecute(objDbConnectionDefinitions, eventArgs);

                if (objCommand.CommandText == eventArgs.Source)
                {
                    objCommand.CommandText = eventArgs.Source;
                }

                recordsAffected = objCommand.ExecuteNonQuery();
            }
            catch (System.Exception ex)
            {
                //for AdoDbConnection compatibility
                objDbConnection.Errors().Add(ex);
                throw (ex);
            }
            return recordsAffected;
        }

        private static void ExecuteWillExecute(DbConnectionDefinitions objDbConnectionDefinitions, WillExecuteEventArgs eventArgs)
        {
            objDbConnectionDefinitions.WillExecute.Invoke(eventArgs);
        }
        
        /// <summary>
        /// opens a dattabale and fills a provided datatable.
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <param name="strSelect"></param>
        /// <param name="blnIsUpdatable"></param>
        /// <param name="DataTableToFill"></param>
        public static void OpenDataTable(this DbConnection objDbConnection, string strSelect, bool blnIsUpdatable, ref DataTable DataTableToFill, DbCommand objCommand = null)
        {
            bool blnClose = false;
            
            DbDataAdapter objAdapter = null;

            if (objDbConnection.State == ConnectionState.Closed)
            {
                objDbConnection.Open();
                blnClose = true;
            }

            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);



            if (objDbConnection is SqlConnection)
            {
                SqlCommand objSqlCommand;

                if (objCommand == null)
                {
                    objSqlCommand = new SqlCommand(strSelect, (SqlConnection)objDbConnection);
                    objSqlCommand.Transaction = (SqlTransaction)objDbConnectionDefinitions.Transaction;

                    // if the connectiontimeout was set, put it in the command timeout as well.
                    if (objDbConnectionDefinitions.ConnectionTimeout != null)
                    {
                        // set the command timeout
                        objSqlCommand.CommandTimeout = Convert.ToInt32(objDbConnectionDefinitions.ConnectionTimeout);
                    }
                }
                else
                {
                    objSqlCommand = (SqlCommand)objCommand;
                }

                objAdapter = new SqlDataAdapter(objSqlCommand);

                if (blnIsUpdatable)
                {
                    SqlCommandBuilder objCmdBuilder = new SqlCommandBuilder((SqlDataAdapter)objAdapter);
                }
            }
            else if (objDbConnection is OdbcConnection)
            {
                objAdapter = new OdbcDataAdapter(strSelect, (OdbcConnection)objDbConnection);
                if (blnIsUpdatable)
                {
                    OdbcCommandBuilder objCmdBuilder = new OdbcCommandBuilder((OdbcDataAdapter)objAdapter);
                }
            }
            else if (objDbConnection is OleDbConnection)
            {
                OleDbCommand objOleDbCommand;

                if (objCommand == null)
                {
                    objOleDbCommand = new OleDbCommand(strSelect, (OleDbConnection)objDbConnection);
                }
                else
                {
                    objOleDbCommand = (OleDbCommand)objCommand;
                }

                objOleDbCommand.Transaction = (OleDbTransaction)objDbConnectionDefinitions.Transaction;

                // if the connectiontimeout was set, put it in the command timeout as well.
                if (objDbConnectionDefinitions.ConnectionTimeout != null)
                {
                    // set the command timeout
                    objOleDbCommand.CommandTimeout = Convert.ToInt32(objDbConnectionDefinitions.ConnectionTimeout);
                }

                objAdapter = new OleDbDataAdapter(objOleDbCommand);

                if (blnIsUpdatable)
                {
                    OleDbCommandBuilder objCmdBuilder = new OleDbCommandBuilder((OleDbDataAdapter)objAdapter);
                    //CHE: wrap table and column names in square brackets  - are required if any table or column names contain spaces or "funny" characters, or if they happen to be reserved words in Access SQL
                    objCmdBuilder.QuotePrefix = "[";
                    objCmdBuilder.QuoteSuffix = "]";
                }
            }           
            else
            {
                throw new Exception("Unsupported connection type");
            }

            try
            {
                //SBE - workaround - when DataTable is refilled, if sort or filter is set, exception is thrown. Remove sort and filter before fill the DataTable
                //http://support.microsoft.com/kb/940932
                string tmpSort = DataTableToFill.DefaultView.Sort;
                string tmpFilter = DataTableToFill.DefaultView.RowFilter;
                DataTableToFill.DefaultView.Sort = string.Empty;
                DataTableToFill.DefaultView.RowFilter = string.Empty;
                
                //KPF - workaround if only Tablename is provided
                string selCmd = objAdapter.SelectCommand.CommandText.Trim().ToUpperInvariant();
                if (!((selCmd.StartsWith("SELECT") || selCmd.StartsWith("UPDATE") || selCmd.StartsWith("INSERT") || selCmd.StartsWith("DELETE"))))
                {
                    objAdapter.SelectCommand.CommandText = "Select * From " + objAdapter.SelectCommand.CommandText;
                }

                objAdapter.Fill(DataTableToFill);
                
                //SBE - restore sort and filter
                DataTableToFill.DefaultView.Sort = tmpSort;
                DataTableToFill.DefaultView.RowFilter = tmpFilter;


                //yaron - it doesn't work when calling a stored procedure. for now , just ignore the error
                try
                {
                    //VB6 recordset does not check any constraints when setting fields values, only on update the server responds
                    //objAdapter.FillSchema(DataTableToFill, SchemaType.Mapped);
                }
                catch
                {
                }

                //sets the current position to 1, as a default
                if (DataTableToFill.Rows.Count > 0)
                {
                    DataTableToFill.AbsolutePosition(1);
                }


                if (blnIsUpdatable)
                {
                    DataTableToFill.SetDataAdapter(objAdapter);
                }
            }
            finally
            {
                if (blnClose)
                {
                    objDbConnection.Close();
                }
            }
            DataTableToFill.Source(strSelect);
            
        }
        
        /// <summary>
        /// Performs a Query and return a DbConnection
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <returns></returns>
        public static DataTable OpenDataTable(this DbConnection objDbConnection, string strSelect, bool blnIsUpdatable = false)
        {
            DataTable objDataTable = new DataTable();
            OpenDataTable(objDbConnection, strSelect, blnIsUpdatable, ref objDataTable);
            return objDataTable;
        }

        public static DataTable OpenSchema(this DbConnection objDbConnecton, SchemaEnum schema, string[] objArgs = null)
        {
            DataTable dt = new DataTable();

            if (objArgs == null)
            {
                dt = objDbConnecton.GetSchema("Tables");
            }
            else
            {
                dt = objDbConnecton.GetSchema("Tables", objArgs);
            }

            //DSE in VB6, schema table is sorted
			try {
            	dt.Sort("Table_Name");
			}
			catch
			{
			}

            return dt;
        }

		/// <summary>
		/// Open a connection
		/// </summary>
		/// <param name="connection"></param>
		/// <param name="connectionString"></param>
		/// <param name="user"></param>
		/// <param name="password"></param>
		public static void Open(this DbConnection connection, string connectionString, string user, string password)
		{
			connection.ConnectionString = "user id=" + user + ";";
			connection.ConnectionString += "password=" + password + ";";
            //CHE: fix "'Provider' is an invalid connection string attribute" - remove from ConnectionString Provider for OracleConnection
            //if (connection is OracleConnection)
            //{
            //    connectionString = FCUtils.RemoveProvider(connectionString);
            //}
            connection.ConnectionString += connectionString;

            connection.Open();
		}

        /// <summary>
        /// Performs a Query and return a DbConnection
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <returns></returns>
        public static DataTable OpenDataTable(this DbConnection objDbConnection, DbCommand objCommand, bool blnIsUpdatable = false)
        {
            DataTable objDataTable = new DataTable();
            OpenDataTable(objDbConnection, objCommand.CommandText, blnIsUpdatable, ref objDataTable, objCommand);
            return objDataTable;
        }
        
        public static void CommitTransaction(this DbConnection objDbConnection)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);

            if (objDbConnectionDefinitions.Transaction != null)
            {
                objDbConnectionDefinitions.Transaction.Commit();
                objDbConnectionDefinitions.Transaction.Dispose();
                objDbConnectionDefinitions.Transaction = null;
            }
        }
        public static void RollbackTransaction(this DbConnection objDbConnection)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);

            if (objDbConnectionDefinitions.Transaction != null)
            {
                objDbConnectionDefinitions.Transaction.Rollback();
                objDbConnectionDefinitions.Transaction.Dispose();
                objDbConnectionDefinitions.Transaction = null;
            }
        }
        public static void SetTransaction(this DbConnection objDbConnection, DbTransaction objTransaction)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);

            objDbConnectionDefinitions.Transaction = objTransaction;
        }
		
		//CHE - #i179 - get transaction
        public static DbTransaction GetTransaction(this DbConnection objDbConnection)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);

            return objDbConnectionDefinitions.Transaction;
        }
        /// <summary>
        /// Sets connection Timeout
        /// </summary>
        /// <param name="objDbConnection"></param>
        public static void ConnectionTimeout(this DbConnection objDbConnection, int ConnectionTimeout)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);
            if (objDbConnectionDefinitions.ConnectionTimeout != null)
            {
                // we need to rememeber the timeout for the command
                objDbConnectionDefinitions.ConnectionTimeout = ConnectionTimeout;
            }
        }

        //IPI
        /// <summary>
        /// Gets connection Timeout
        /// </summary>
        /// <param name="objDbConnection"></param>
        public static int ConnectionTimeout(this DbConnection objDbConnection)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);
            
            return (objDbConnectionDefinitions.ConnectionTimeout.HasValue) ? objDbConnectionDefinitions.ConnectionTimeout.Value : 0;

        }

        /// <summary>
        /// Sets command Timeout
        /// </summary>
        /// <param name="objDbConnection"></param>
        public static void CommandTimeout(this DbConnection objDbConnection, int CommandTimeout)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);
            if (objDbConnectionDefinitions.CommandTimeout != null)
            {
                // we need to rememeber the timeout for the command
                objDbConnectionDefinitions.CommandTimeout = CommandTimeout;
            }
        }

        //IPI
        /// <summary>
        /// Gets command Timeout
        /// </summary>
        /// <param name="objDbConnection"></param>
        public static int CommandTimeout(this DbConnection objDbConnection)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);

            return (objDbConnectionDefinitions.CommandTimeout.HasValue) ? objDbConnectionDefinitions.CommandTimeout.Value : 0;

        }

        /// <summary>
        /// Sets The current location of the index
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <param name="CurrentLocation"></param>
        public static void CursorLocation(this DbConnection objDbConnection, CursorLocationEnum CurrentLocation)
        {
        }

        /// <summary>
        /// Sets The current location of the index
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <param name="CurrentLocation"></param>
        public static void CursorLocation(this DbConnection objDbConnection, short CurrentLocation)
        {
        }

        //IPI
        public static void DefaultDatabase(this DbConnection objDbConnection, string defaultDatabase)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);
            if (objDbConnectionDefinitions != null)
            {
                if (objDbConnection is SqlConnection)
                {
                    ((SqlConnection)objDbConnection).ChangeDatabase(defaultDatabase);
                }
                else if (objDbConnection is OleDbConnection)
                {
                    ((OleDbConnection)objDbConnection).ChangeDatabase(defaultDatabase);
                }
                objDbConnectionDefinitions.DefaultDatabase = defaultDatabase;
            }
        }

        //IPI
        public static string DefaultDatabase(this DbConnection objDbConnection)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);
            if (objDbConnectionDefinitions != null)
            {
                return objDbConnectionDefinitions.DefaultDatabase;
            }

            return "";
        }

        public static void RegisterWillExecute(this DbConnection objDbConnection, Action<WillExecuteEventArgs> willExecuteMethod)
        {
             DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objDbConnection);
             if (objDbConnectionDefinitions != null)
             {
                 objDbConnectionDefinitions.WillExecute = willExecuteMethod;
             }
        }
        /// <summary>
        /// Retrieves the Count of Indexes on tableName
        /// The Database in the Connection should be set correctly
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static int GetTableIndexCount(this DbConnection objDbConnection, string tableName)
        {
            int indexCount = 0;
            string[] restrictions = new string[] { string.Empty, string.Empty, string.Empty, string.Empty, tableName };
            DataTable schemaTable = null;
            if (objDbConnection is OleDbConnection)
            {
                schemaTable = ((OleDbConnection)objDbConnection).GetOleDbSchemaTable(OleDbSchemaGuid.Indexes, restrictions);
            }
            return indexCount;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DbConnectionDefinitions
    {

        private DbTransaction mobjTransaction = null;
        private Action<WillExecuteEventArgs> willExecute = null;

        //IPI
        private string mobjDefaultDatabase = "";

        /// <summary>
        /// Gets the definitions of the current Data Table
        /// </summary>
        /// <param name="objDbConnection"></param>
        /// <returns></returns>
        public static DbConnectionDefinitions GetDbConnectionDefinitions(DbConnection objDbConnection)
        {
            DbConnectionDefinitions objDbConnectionDefinitions;
            bool blnExists = false;

            foreach (DbConnection tmp in DbConnectionExtension.mobjDefinitions.Keys)
            {
                if (tmp == objDbConnection)
                {
                    blnExists = true;
                }
            }

            if (blnExists)
            {
                objDbConnectionDefinitions = DbConnectionExtension.mobjDefinitions[objDbConnection];
            }
            else
            {
                objDbConnectionDefinitions = new DbConnectionDefinitions();
                if (objDbConnection != null)
                {
                objDbConnection.Disposed += new EventHandler(objDbConnection_Disposed);
                DbConnectionExtension.mobjDefinitions.Add(objDbConnection, objDbConnectionDefinitions);
            }
            }
            return objDbConnectionDefinitions;
        }

        /// <summary>
        /// Removes the current DbConnection's definitions from the dictionary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void objDbConnection_Disposed(object sender, EventArgs e)
        {
            DbConnectionExtension.mobjDefinitions.Remove((DbConnection)sender);
        }

        /// <summary>
        /// Holds the current position of the DbConnection (bookmark)
        /// </summary>
        public DbTransaction Transaction
        {
            get
            {
                return mobjTransaction;
            }
            set
            {
                mobjTransaction = value;
            }
        }



        public int? ConnectionTimeout { get; set; }
        public int? CommandTimeout { get; set; }

        //IPI
        public string DefaultDatabase
        {
            get
            {
                return mobjDefaultDatabase;
            }
            set
            {
                mobjDefaultDatabase = value;
            }
        }

        internal Action<WillExecuteEventArgs> WillExecute
        {
            get
            {
                return willExecute;
            }
            set
            {
                willExecute = value;
            }
        }
    }

    #region WillExecuteEventArgs
    public class WillExecuteEventArgs : EventArgs
    {
        #region Members
        private fecherFoundation.DataBaseLayer.ADO.CursorTypeEnum cursorType;
        private fecherFoundation.DataBaseLayer.ADO.LockTypeEnum lockType;
        private fecherFoundation.DataBaseLayer.ADO.EventStatusEnum adStatus;
        #endregion

        #region Properties
        public string Source { get; set; }
        public int Options { get; set; }
        public System.Data.SqlClient.SqlCommand Command { get; set; }
        public System.Data.SqlClient.SqlConnection Connection { get; set; }

        public fecherFoundation.DataBaseLayer.ADO.CursorTypeEnum CursorType
        {
            get
            {
                return cursorType;
            }
            set
            {
                cursorType = value;
            }
        }

        public fecherFoundation.DataBaseLayer.ADO.LockTypeEnum LockType
        {
            get
            {
                return lockType;
            }
            set
            {
                lockType = value;
            }
        }

        public fecherFoundation.DataBaseLayer.ADO.EventStatusEnum AdStatus
        {
            get
            {
                return adStatus;
            }
            set
            {
                adStatus = value;
            }
        }
        #endregion
    }
    #endregion
}


