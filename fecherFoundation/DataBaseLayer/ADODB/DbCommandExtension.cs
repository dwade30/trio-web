﻿using System;
using System.Collections.Generic;
using System.Text;
using Wisej.Web;
using System.Data;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data.OleDb;
using fecherFoundation;

namespace fecherFoundation.DataBaseLayer.ADO
{


    /// <summary>
    /// Performs a Query and return a DataSet
    /// </summary>
    public static class DbCommandExtension
    {
        /// <summary>
        /// Replaces all the '?' parameter placeholders with corresponding parameter names from the parameter list
        /// </summary>
        /// <param name="objDbCommand"></param>
        private static void FillParameters(this DbCommand objDbCommand)
        {
            //SqlCommand
            string prefix = "@";
            //Oracle
            if (objDbCommand is OleDbCommand && objDbCommand.Connection.ConnectionString.Contains("Provider=OraOLEDB.Oracle"))
            {
                prefix = ":";
            }
            int paramPos = 0;
            int paramIdx = 0;
            string Command = objDbCommand.CommandText;

            while ((paramPos = Command.IndexOf('?', paramPos)) != -1)
            {
                //is there any paramater to replace with
                if (paramIdx < objDbCommand.Parameters.Count)
                {
                    //remove old parameter placeholder
                    Command = Command.Remove(paramPos, 1);
                    //insert new parameter placeholder
                    Command = Command.Insert(paramPos, String.Format("{0}{1}", prefix, objDbCommand.Parameters[paramIdx].ParameterName));
                }
                                
                ++paramPos;
                ++paramIdx;
            }

            objDbCommand.CommandText = Command;
        }

        //IPI
        public static DataTable OpenDataTable(this DbCommand objDbCommand)
        {
            objDbCommand.FillParameters();
            //CNA - Using CommandText only to retrieve the table, Command parameters are not preserved. So use the whole Command
            return objDbCommand.Connection.OpenDataTable(objDbCommand);
        }

        public static DataTable Execute(this DbCommand objCommand)
        {
            return objCommand.OpenDataTable();
        }

        public static int Execute(this DbCommand objCommand, ref int recordsAffected)
        {
            DbConnectionDefinitions objDbConnectionDefinitions = DbConnectionDefinitions.GetDbConnectionDefinitions(objCommand.Connection);
            if (objDbConnectionDefinitions.Transaction != null)
            {
                objCommand.Transaction = objDbConnectionDefinitions.Transaction;
            }
            // if the connectiontimeout was set, put it in the command timeout as well.
            if (objDbConnectionDefinitions.ConnectionTimeout != null && objCommand.CommandTimeout == null)
            {
                // set the command timeout
                objCommand.CommandTimeout = Convert.ToInt32(objDbConnectionDefinitions.ConnectionTimeout);
            }

            recordsAffected = objCommand.ExecuteNonQuery();

            return recordsAffected;
        }


    }
}
