﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
////using Extensions;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Data;
using System.Collections;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;


namespace fecherFoundation.DataBaseLayer.ADO
{
    //IPI: add delegate to support MoveComplete
    public delegate void MoveCompleteHandler();

    #region Enums

    public enum MoveDirection
    {
        First,
        Next,
        Previous,
        Last
    }

    //BAN
    public enum PersistFormatEnum
    {
        adPersistADTG,
        adPersistXML,
        adPersistADO,
        adPersistProviderSpecific
    }

    public enum BookmarkEnum
    {
        adBookmarkCurrent = 0,
        adBookmarkFirst = 1,
        adBookmarkLast = 2
    }

    public enum CursorLocationEnum
    {
        adUseNone = 1,
        adUseServer = 2,
        adUseClientBatch = 3,
        adUseClient = 3,
    }
    public enum CursorTypeEnum
    {
        adOpenUnspecified = -1,
        adOpenForwardOnly = 0,
        adOpenKeyset = 1,
        adOpenDynamic = 2,
        adOpenStatic = 3,
    }
    public enum CommandTypeEnum
    {
        adCmdUnspecified = -1,//Does not specify the command type argument.
        adCmdText = 1, //Evaluates CommandText as a textual definition of a command or stored procedure call.
        adCmdTable = 2, //Evaluates CommandText as a table name whose columns are all returned by an internally generated SQL query.
        adCmdStoredProc = 4, //Evaluates CommandText as a stored procedure name.
        adCmdUnknown = 8, //Default. Indicates that the type of command in the CommandText property is not known.
        adCmdFile = 256, //Evaluates CommandText as the file name of a persistently stored Recordset. Used with Recordset.Open or Requery only.
        adCmdTableDirect = 512 //Evaluates CommandText as a table name whose columns are all returned. Used with Recordset.Open or Requery only. 
                               //To use the Seek method, the Recordset must be opened with adCmdTableDirect. This value cannot be combined with the ExecuteOptionEnum value adAsyncExecute.
    }
    public enum PositionEnum : int
    {
        adPosBOF = -2,
        adPosEOF = -3,
        adPosUnknown = -1,
    }
    public enum LockTypeEnum
    {
        adLockUnspecified = -1,
        adLockReadOnly = 1,
        adLockPessimistic = 2,
        adLockOptimistic = 3,
        adLockBatchOptimistic = 4,
    }

    //IPI
    public enum FieldAttributeEnum
    {
        adFldUnspecified = -1,
        adFldIsNullable = 32,
        adFldMayDefer = 2,
        adFldMayBeNull = 64,
        adFldUpdatable = 4,
        adFldKeyColumn = 32768
    }

    //IPI
    public enum FilterGroupEnum
    {
        adFilterNone,
        adFilterPendingRecords,
        adFilterAffectedRecords,
        adFilterFetchedRecords,
        adFilterConflictingRecords
    }

    //BAN
    public enum AffectEnum
    {
        adAffectCurrent,
        adAffectGroup,
        adAffectAll,
        adAffectAllChapters
    }

    //BAN
    public enum ResyncEnum
    {
        adResyncUnderlyingValues,
        adResyncAllValues
    }

    //BAN
    public enum SchemaEnum
    {
        adSchemaProviderSpecific = -1,
        adSchemaAsserts = 0,
        adSchemaCatalogs = 1,
        adSchemaCharacterSets = 2,
        adSchemaCollations = 3,
        adSchemaColumns = 4,
        adSchemaCheckConstraints = 5,
        adSchemaConstraintColumnUsage = 6,
        adSchemaConstraintTableUsage = 7,
        adSchemaKeyColumnUsage = 8,
        adSchemaReferentialConstraints = 9,
        adSchemaTableConstraints = 10,
        adSchemaColumnsDomainUsage = 11,
        adSchemaIndexes = 12,
        adSchemaColumnPrivileges = 13,
        adSchemaTablePrivileges = 14,
        adSchemaUsagePrivileges = 15,
        adSchemaProcedures = 16,
        adSchemaSchemata = 17,
        adSchemaSQLLanguages = 18,
        adSchemaStatistics = 19,
        adSchemaTables = 20,
        adSchemaTranslations = 21,
        adSchemaProviderTypes = 22,
        adSchemaViews = 23,
        adSchemaViewColumnUsage = 24,
        adSchemaViewTableUsage = 25,
        adSchemaProcedureParameters = 26,
        adSchemaForeignKeys = 27,
        adSchemaPrimaryKeys = 28,
        adSchemaProcedureColumns = 29,
        adSchemaDBInfoKeywords = 30,
        adSchemaDBInfoLiterals = 31,
        adSchemaCubes = 32,
        adSchemaDimensions = 33,
        adSchemaHierarchies = 34,
        adSchemaLevels = 35,
        adSchemaMeasures = 36,
        adSchemaProperties = 37,
        adSchemaMembers = 38,
        adSchemaTrustees = 39
    }

    public enum Synchronization
    {
        TableNewRow,
        DetachedNewRow,
        DeleteRow,
        UpdateRow,
        CancelRow,
        MoveRowFromDefaultView
    }
    #endregion

    /// <summary>
    /// Extension of DataTable - added movefirst, movenext (etc.) support
    /// </summary>
    public static class DataTableExtension
    {
        /// <summary>
        /// A dictionary used to save the definition object 
        /// </summary>
        internal static Dictionary<DataTable, DataTableDefinitions> mobjDefinitions = new Dictionary<DataTable, DataTableDefinitions>();
        private static int checkFieldCache = 0;

        /// <summary>
        /// Creates a DataView for filtering and sorting
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="objDataTableDefinitions"></param>
        /// <param name="blnForceCreate"></param>
        private static void UpdateDataView(DataTable objDataTable, DataTableDefinitions objDataTableDefinitions, bool blnForceCreate, FilterGroupEnum? FilterType = null)
        {
            DataView objDataView = null;
            //BAN: set the data view in order to compare sorting
            objDataView = objDataTable.DefaultView;
            //BAN: remove sorting if necessary
            //if (FilterType.HasValue || !string.IsNullOrEmpty(objDataTableDefinitions.Sort) || !string.IsNullOrEmpty(objDataTableDefinitions.Filter) || blnForceCreate)
            if (FilterType.HasValue || !string.IsNullOrEmpty(objDataTableDefinitions.Sort) || (objDataView.Sort != objDataTableDefinitions.Sort) || !string.IsNullOrEmpty(objDataTableDefinitions.Filter) || blnForceCreate)
            {
                //CHE filter has to be set to datatable default view to be visible in datagridview when datagridview.datasource=datatable
                //objDataView = new DataView(objDataTable);
                //BAN: set dataview at the begining
                //objDataView = objDataTable.DefaultView;

                //IPI
                if (FilterType.HasValue)
                {
                    switch (FilterType)
                    {
                        case FilterGroupEnum.adFilterPendingRecords:
                            {
                                //CHE: when filter pending should contain also deleted rows
                                objDataView.RowStateFilter = DataViewRowState.ModifiedCurrent | DataViewRowState.Added | DataViewRowState.Deleted;
                                break;
                            }
                        case FilterGroupEnum.adFilterNone:
                            {
                                objDataView.RowFilter = "";
                                //SBE: removed deleted rowstate
                                objDataView.RowStateFilter = DataViewRowState.ModifiedCurrent | DataViewRowState.Added | DataViewRowState.Unchanged;
                                break;
                            }
                        case FilterGroupEnum.adFilterAffectedRecords:
                            {
                                objDataView.RowStateFilter = DataViewRowState.ModifiedCurrent;
                                break;
                            }
                        default:
                            break;
                    }
                }
                else
                {
                    //CNA Filter is always applied, when it is empty it means a reset
                    //if (!string.IsNullOrEmpty(objDataTableDefinitions.Filter))
                    //{
                    objDataView.RowFilter = objDataTableDefinitions.Filter;
                    //}
                    objDataView.Sort = objDataTableDefinitions.Sort;
                }
            }
            objDataTableDefinitions.View = objDataView;
            //BAN: after setting the filter, move to the first row of the DataView
            //APE: move to the first row of the DataView if RecordCount is not 0
            if (objDataTable.RecordCount() != 0)
            {
                objDataTable.MoveFirst();
            }
        }

        public static void AddMoveCompleteHandler(this DataTable objDataTable, MoveCompleteHandler handler)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (objDataTableDefinitions != null)
            {
                objDataTableDefinitions.MoveComplete = new MoveCompleteHandler(handler);
            }
        }

        /// <summary>
        /// Creates a new row
        /// </summary>
        /// <param name="dataTable">The current data table.</param>
        public static DataRow AddNew(this DataTable dataTable)
        {

            //if current row is marked for deletion do not update
            DataRow dataRow = GetCurrentRow(dataTable);
            if (dataRow != null && dataRow.RowState != DataRowState.Deleted)
            {
                //update the current record
                dataTable.Update();
            }

            //get the instance of the definitions class
            DataTableDefinitions dataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(dataTable);

            //NewRow will generate TableNewRow event in which the Synchronize should not be executed
            dataTableDefinitions.suppressCloneSynchronization = true;
            //IPI, SBE: when AddNew is called in the grid's AfterUpdate handler, must check if the grid already has a new row in DefaultView. Check if ParentGrid is set
            //DataRow objRow = objDataTable.NewRow();
            DataRow objRow = null;
            DataGridView grid = dataTableDefinitions.ParentGrid;
            if (grid != null && dataTable.DefaultView.Count > 0 && dataTable.DefaultView[dataTable.DefaultView.Count - 1].IsNew)
            {
                objRow = dataTable.DefaultView[dataTable.DefaultView.Count - 1].Row;
            }
            else
            {
                //CHE: do not add in DefaultView unless grid is attached
                //if (objDataTable.DefaultView.AllowNew)
                if (dataTable.DefaultView.AllowNew && grid != null)
                {
                    //SBE: set row empty values for last row before add new row
                    if (dataTable.DefaultView.Count > 0)
                    {
                        DataRow lastRow = dataTable.DefaultView[dataTable.DefaultView.Count - 1].Row;
                        dataTable.SetRowEmptyValues(lastRow);
                        //IPI: when below call DefaultView.AddNew is executed, lastRow will be committed to the Rows collection
                        // if a row is in the DataTable.Rows collection, it should be removed from NewRows
                        if (dataTableDefinitions.NewRows != null && dataTableDefinitions.NewRows.Contains(lastRow))
                        {
                            dataTableDefinitions.NewRows.Remove(lastRow);
                        }
                    }
                    //IPI: if DataTable is bound to a DGV, the current cell must reflect the new row in the DefaultView
                    DataRowView rowView = dataTable.DefaultView.AddNew();
                    objRow = rowView.Row;

                    if (grid != null && grid.CurrentCell != null)
                    {
                        foreach (DataGridViewRow row in grid.Rows)
                        {
                            if (row.DataBoundItem != null && ((DataRowView)row.DataBoundItem) == rowView)
                            {
                                grid.CurrentCell = grid[grid.CurrentCell.ColumnIndex, grid.Rows.IndexOf(row)];
                                break;
                            }
                        }
                    }
                }
                else
                {
                    objRow = dataTable.NewRow();
                }
            }

            dataTableDefinitions.suppressCloneSynchronization = false;

            //CHE: AddNew on DataTable with mandatory fields will raise exception if new empty row is added in collection, must be added later on Update
            if (dataTableDefinitions.NewRows == null)
            {
                dataTableDefinitions.NewRows = new List<DataRow>();
            }

            dataTableDefinitions.NewRows.Add(objRow);

            //CHE: do not add in DefaultView unless grid is attached, move index on last row 
            //objDataTableDefinitions.Index = objDataTable.Rows.Count + 1;
            dataTableDefinitions.Index = dataTable.Rows.Count + dataTableDefinitions.NewRows.Count;
            DataTableDefinitions.SynchronizeClones(dataTable, Synchronization.DetachedNewRow, objRow);


            return objRow;
        }

        private static DataRow GetCurrentRow(DataTable dataTable)
        {

            DataRow dataRow = null;
            //check if current row exist
            if (!EOF(dataTable) && !BOF(dataTable))
            {
                dataRow = dataTable.CurrentRow(true);
            }



            return dataRow;
        }

        /// <summary>
        /// Moves to the first record in the DataTable
        /// </summary>
        /// <param name="dataTable">The current data table.</param>
        private static void MoveInternal(DataTable dataTable, MoveDirection md, bool moveSelection = true)
        {


            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(dataTable);
            int rowsCount = dataTable.Rows.Count;

            //DSE Peformance improvement by not calling update. The Update method should be called from program
            //if (!objDataTableDefinitions.IsInternalDataAction)
            //{
            //    if (dataRow != null && dataRow.RowState != DataRowState.Deleted && dataRow.RowState != DataRowState.Unchanged)
            //    {
            //        //update the current record
            //        dataTable.Update();
            //        rowsCount = dataTable.Rows.Count;
            //    }
            //}

            //APE: before returning EOF or BOF check also if data table containes new rows added using AddNew 
            if (!objDataTableDefinitions.Deleted)
            {
                if (
                        ((rowsCount == 0 && dataTable.DefaultView.Count == 0) && (objDataTableDefinitions.NewRows != null && objDataTableDefinitions.NewRows.Count == 0)) ||
                    (md == MoveDirection.Next && objDataTableDefinitions.EOF || //&& EOF(dataTable)) ||
                    (md == MoveDirection.Previous && objDataTableDefinitions.BOF))//BOF(dataTable))
                        )
                {
                    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                }
            }

            int index = objDataTableDefinitions.Index;
            DataRow row = null;
            switch (md)
            {
                case MoveDirection.First:
                    {
                        //sets the index to the first row
                        if (objDataTableDefinitions.View == null || objDataTableDefinitions.View.Count == 0)
                        {
                            index = 1;
                        }
                        else
                        {
                            row = objDataTableDefinitions.View[0].Row;
                            objDataTableDefinitions.CurrentViewIndex = 0;
                            index = dataTable.Rows.IndexOf(row) + 1;
                        }
                        //CHE: if no rows in view bof should return true
                        if (dataTable.RecordCount() == 0)
                        {
                            objDataTableDefinitions.mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosBOF);
                            index = 0;
                        }
                        break;
                    }
                case MoveDirection.Last:
                    {
                        //CHE: sets the index to the last row, on last added if the case
                        if (dataTable.HasNewRows())
                        {
                            index = rowsCount + objDataTableDefinitions.NewRows.Count;
                        }
                        else
                        {
                            //CHE: when view (filter or sort) should go to last row in view not in table
                            if (objDataTableDefinitions.View == null || objDataTableDefinitions.View.Count == 0)
                            {
                                index = rowsCount;
                            }
                            else
                            {
                                row = objDataTableDefinitions.View[objDataTableDefinitions.View.Count - 1].Row;
                                objDataTableDefinitions.CurrentViewIndex = objDataTableDefinitions.View.Count - 1;
                                index = dataTable.Rows.IndexOf(row) + 1;
                            }
                        }

                        if (dataTable.RecordCount() == 0)
                        {
                            objDataTableDefinitions.mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosEOF);
                        }
                        break;
                    }
                case MoveDirection.Next:
                    {
                        row = GetCurrentRow(dataTable);
                        if (objDataTableDefinitions.Deleted)
                        {
                            if (!objDataTableDefinitions.EOF)//(!EOF(dataTable))
                            {

                                if (row != null && row.RowState == DataRowState.Deleted)
                                {
                                    index++;
                                }
                            }
                            objDataTableDefinitions.Deleted = false;
                        }
                        //CHE: when view (filter or sort) should go to next row in view not in table
                        else if (objDataTableDefinitions.View == null || objDataTableDefinitions.View.Count == 0)
                        {
                            //increment the index
                            index++;
                        }
                        else
                        {
                            bool isInView = false;
                            int i;

                            //CHE: if you are on BOF and you MoveNext it should go to first row from view
                            if (!objDataTableDefinitions.BOF)//(!BOF(dataTable))
                            {
                                i = objDataTableDefinitions.CurrentViewIndex;
                                isInView = true;
                                //for (i = 0; i < objDataTableDefinitions.View.Count; i++)
                                //{
                                //    if (objDataTableDefinitions.View[i].Row == row)
                                //    {
                                //        isInView = true;
                                //        break;
                                //    }
                                //}
                            }
                            else
                            {
                                isInView = true;
                                i = -1;
                            }

                            if (isInView && i < objDataTableDefinitions.View.Count - 1)
                            {
                                DataRow nextRow = objDataTableDefinitions.View[i + 1].Row;
                                ++objDataTableDefinitions.CurrentViewIndex;
                                index = dataTable.Rows.IndexOf(nextRow) + 1;
                            }
                            else
                            {
                                //SBE: check if RowState is Deleted, and move to next index
                                if (row.RowState == DataRowState.Deleted)
                                {
                                    index++;
                                }
                                else
                                {
                                    index = rowsCount + 1;
                                }
                            }
                        }

                        int totalRows = rowsCount + (dataTable.HasNewRows() ? objDataTableDefinitions.NewRows.Count : 0);

                        if (index > totalRows)
                        {
                            objDataTableDefinitions.mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosEOF);
                        }

                        break;
                    }
                case MoveDirection.Previous:
                    {
                        row = dataTable.CurrentRow();
                        if (objDataTableDefinitions.Deleted)
                        {
                            if (objDataTableDefinitions.BOF)//(!BOF(dataTable))
                            {
                                //row was removed, EOF is true or RowState is Deleted
                                //DSE Performance improvement by using properties for EOF and BOF and only computing them when necessary
                                //if (EOF(dataTable) || dataTable.CurrentRow().RowState == DataRowState.Deleted)
                                if (objDataTableDefinitions.EOF || (row != null && row.RowState == DataRowState.Deleted))
                                {
                                    index--;
                                }
                            }
                            objDataTableDefinitions.Deleted = false;
                        }
                        //CHE: when view (filter or sort) should go to previous row in view not in table
                        else if (objDataTableDefinitions.View == null || objDataTableDefinitions.View.Count == 0)
                        {
                            //decrement the index
                            index--;
                        }
                        else
                        {
                            bool isInView = false;
                            int i;
                            //CHE: if you are on EOF and you MovePrevious it should go to last row from view
                            if (!objDataTableDefinitions.EOF)//(!EOF(dataTable))
                            {
                                i = objDataTableDefinitions.CurrentViewIndex;
                                isInView = true;

                                //for (i = 0; i < objDataTableDefinitions.View.Count; i++)
                                //{
                                //    if (objDataTableDefinitions.View[i].Row == row)
                                //    {
                                //        isInView = true;
                                //        break;
                                //    }
                                //}
                            }
                            else
                            {
                                isInView = true;
                                i = objDataTableDefinitions.View.Count;
                            }
                            if (isInView && i > 0)
                            {
                                DataRow nextRow = objDataTableDefinitions.View[i - 1].Row;
                                --objDataTableDefinitions.CurrentViewIndex;
                                index = dataTable.Rows.IndexOf(nextRow) + 1;
                            }
                            else
                            {
                                index = 0;
                            }
                        }

                        if (index == 0)
                        {
                            objDataTableDefinitions.mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosBOF);
                        }
                        break;
                    }
            }

            objDataTableDefinitions.Index = index;

            if (objDataTableDefinitions.MoveComplete != null)
            {
                objDataTableDefinitions.MoveComplete();
            }

            if (moveSelection)
            {
                MoveSelection(dataTable, objDataTableDefinitions);
            }


        }

        /// <summary>
        /// Moves to the first record in the DataTable
        /// </summary>
        /// <param name="dataTable">The current data table.</param>
        public static void MoveFirst(this DataTable dataTable)
        {
            MoveInternal(dataTable, MoveDirection.First);
        }

        /// <summary>
        /// Moves to the last record in the DataTable
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static void MoveLast(this DataTable dataTable)
        {
            MoveInternal(dataTable, MoveDirection.Last);
        }

        /// <summary>
        /// Moves to the next record in the DataTable
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="moveSelection">optional, default true, specifies wheather to move selection also on grid or not</param>
        /// <returns></returns>
        public static void MoveNext(this DataTable dataTable, bool moveSelection = true)
        {
            MoveInternal(dataTable, MoveDirection.Next, moveSelection);
        }

        private static void MoveSelection(DataTable objDataTable, DataTableDefinitions objDataTableDefinitions)
        {

            if (objDataTableDefinitions.ParentGrid != null)
            {
                DataGridView grid = objDataTableDefinitions.ParentGrid;
                int dataTableDefinitionsIndex = objDataTableDefinitions.Index;
                //BAN: if grid allows the user to add new rows, when the selection will be set to the last row,
                // a new empty row will be added in the DataTable DefaultView. Setting AllowUserToAddRows to false will prevent this
                int indexAdjustment = 0;
                if (grid.AllowUserToAddRows == true)
                {
                    indexAdjustment = 1;
                }
                if ((objDataTableDefinitions.Index - 1) >= 0 && (objDataTableDefinitions.Index - 1) < grid.Rows.Count - indexAdjustment)
                {
                    //CHE: avoid null exception
                    if (grid.CurrentCell != null)
                    {
                        //EndEdit if anything in edit
                        bool endEditForCurrentRow = false;
                        foreach (DataRowView drView in objDataTable.DefaultView)
                        {
                            if (drView.IsEdit && drView.Row.RowState != DataRowState.Deleted)
                            {
                                endEditForCurrentRow = false;
                                //SBE: check if row was modified
                                for (int i = 0; i < drView.Row.ItemArray.Length; i++)
                                {
                                    object value = drView.Row.ItemArray[i];
                                    if (value != null && !Information.IsDBNull(value))
                                    {
                                        endEditForCurrentRow = true;
                                        break;
                                    }
                                }

                                if (endEditForCurrentRow)
                                {
                                    //CNA: need to set empty values for drView's cells if DataTable column does not allow DBNull
                                    objDataTable.SetRowEmptyValues(drView.Row);
                                    drView.EndEdit();
                                }
                            }
                        }
                        //move current cell based on current row in DataTable
                        //CHE: only move current cell if objDataTable.CurrentRow is valid (neither EOF nor BOF has been reached)
                        if (!(EOF(objDataTable) || BOF(objDataTable)))
                        {
                            DataRow currentRowInDataTable = objDataTable.CurrentRow();
                            foreach (DataGridViewRow row in grid.Rows)
                            {
                                if (row.DataBoundItem != null && ((DataRowView)row.DataBoundItem).Row == currentRowInDataTable)
                                {
                                    grid.CurrentCell = grid[grid.CurrentCell.ColumnIndex, grid.Rows.IndexOf(row)];
                                    break;
                                }
                            }
                        }
                    }
                    //BAN: Needed to save and reset the dataTableDefinitions index because, when setting the current cell
                    //the selected index changed event is raised before setting the new value. In FCTrueDBGrid, in the event handler
                    //the row will be the previous one then and it will reset the data table definitions index with the wrong index
                    objDataTableDefinitions.Index = dataTableDefinitionsIndex;
                }
            }


        }

        /// <summary>
        /// Moves forwards or backwards the specified number of rows in the DataTable
        /// </summary>
        /// <param name="NumRecords">Number of records to move, positive to move forwards, negative to move backwards</param>
        /// <returns></returns>
        public static void Move(this DataTable objDataTable, int NumRecords)
        {



            // Don't want to do anything if NumRecords is 0

            if (NumRecords > 0)
            {

                if (EOF(objDataTable))
                {
                    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                }

                for (int i = 1; i <= NumRecords; i++)
                {
                    MoveNext(objDataTable);

                    if (EOF(objDataTable))
                    {
                        break;
                    }
                }
            }
            else if (NumRecords < 0)
            {
                if (BOF(objDataTable))
                {
                    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                }

                for (int i = -1; i >= NumRecords; i--)
                {
                    MovePrevious(objDataTable);

                    if (BOF(objDataTable))
                    {
                        break;
                    }
                }
            }


        }

        /// <summary>
        /// Moves to the previous record in the DataTable
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static void MovePrevious(this DataTable dataTable)
        {
            MoveInternal(dataTable, MoveDirection.Previous);
        }

        /// <summary>
        /// Extension: Returns the source of the DataTable 
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static string Source(this DataTable objDataTable)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            return objDataTableDefinitions.Source;
        }

        /// <summary>
        /// Extension: sets the source of the DataTable 
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="strSource"></param>
        public static void Source(this DataTable objDataTable, string strSource)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            objDataTableDefinitions.Source = strSource;

        }


        /// <summary>
        /// Extension: Returns the current Filter
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static string Filter(this DataTable objDataTable)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            return objDataTableDefinitions.Filter;
        }

        //BAN
        public static FilterGroupEnum FilterType(this DataTable objDataTable)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            return objDataTableDefinitions.FilterType;
        }

        /// <summary>
        /// Extension: sets the current Filter
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="strFilter"></param>
        public static void Filter(this DataTable objDataTable, string strFilter)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            //objDataTableDefinitions.IsInternalDataAction = true;
            try
            {
                if (objDataTableDefinitions.Filter != strFilter)
                {
                    objDataTableDefinitions.Filter = strFilter;
                    //CNA: Force the update of DataView when filter is empty
                    //UpdateDataView(objDataTable, objDataTableDefinitions, false);
                    UpdateDataView(objDataTable, objDataTableDefinitions, string.IsNullOrEmpty(strFilter));
                }
                else
                {
                    objDataTableDefinitions.Deleted = false;
                }
            }
            finally
            {
                objDataTableDefinitions.IsInternalDataAction = false;
            }

        }

        public static object[,] GetRows(this DataTable objDataTable, int numRows)
        {
            int rowsCount = Math.Max(objDataTable.Rows.Count, numRows);
            int columnsCount = objDataTable.Columns.Count;

            object[,] data = new object[columnsCount, rowsCount];

            //copy data
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    data[j, i] = objDataTable.Rows[i][j];
                }
            }

            return data;
        }

        public static DataRowCollection GetRows(this DataTable objDataTable)
        {
            //Create a DataTable to Return
            DataTable returnTable = new DataTable();

            string columnX = objDataTable.Columns[0].ColumnName;

            //Add a Column at the beginning of the table

            //Read all DISTINCT values from columnX Column in the provided DataTale
            List<string> columnXValues = new List<string>();

            //Creates list of columns to ignore

            foreach (DataRow dr in objDataTable.Rows)
            {
                string columnXTemp = dr[columnX].ToString();
                //Verify if the value was already listed
                if (!columnXValues.Contains(columnXTemp))
                {
                    //if the value id different from others provided, add to the list of 
                    //values and creates a new Column with its value.
                    columnXValues.Add(columnXTemp);
                    returnTable.Columns.Add(columnXTemp);
                }
                else
                {
                    //Throw exception for a repeated value
                    throw new Exception("The inversion used must have " +
                                        "unique values for column " + columnX);
                }
            }

            //Add a line for each column of the DataTable

            foreach (DataColumn dc in objDataTable.Columns)
            {
                if (!columnXValues.Contains(dc.ColumnName))
                {
                    DataRow dr = returnTable.NewRow();
                    returnTable.Rows.Add(dr);
                }
            }

            //Complete the datatable with the values
            for (int i = 0; i < returnTable.Rows.Count; i++)
            {
                for (int j = 0; j < returnTable.Columns.Count; j++)
                {
                    returnTable.Rows[i][j] = objDataTable.Rows[j][i].ToString();
                }
            }

            return returnTable.Rows;
        }

        //IPI
        public static void Filter(this DataTable objDataTable, FilterGroupEnum filterType)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            if (filterType == FilterGroupEnum.adFilterNone)
            {
                objDataTableDefinitions.Filter = "";
                objDataTableDefinitions.FilterType = filterType;
                UpdateDataView(objDataTable, objDataTableDefinitions, false, filterType);
            }
            else if (objDataTableDefinitions.FilterType != filterType)
            {
                //CHE: FilterType should not affect grid, save position
                if (objDataTableDefinitions.ParentGrid != null && !BOF(objDataTable) && !EOF(objDataTable) && !objDataTableDefinitions.Deleted)
                {
                    objDataTableDefinitions.BookmarkBeforeFilterType = objDataTable.Bookmark(true);
                }
                objDataTableDefinitions.FilterType = filterType;
                UpdateDataView(objDataTable, objDataTableDefinitions, false, filterType);
            }
            else
            {
                objDataTableDefinitions.Deleted = false;
            }


        }

        /// <summary>
        /// Extension: gets the current Sort order
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static string Sort(this DataTable objDataTable)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            return objDataTableDefinitions.Sort;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static int ColumnCount(this DataTable objDataTable)
        {
            return objDataTable.Columns.Count;
        }

        /// <summary>
        /// Extension: sets the current Sort Order
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="strSort"></param>
        public static void Sort(this DataTable objDataTable, string strSort)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (objDataTableDefinitions.Sort != strSort)
            {
                objDataTableDefinitions.Sort = strSort;
                UpdateDataView(objDataTable, objDataTableDefinitions, false);
            }
            else
            {
                objDataTableDefinitions.Deleted = false;
            }

            //CHE: when sort set position on first row in view
            if (objDataTableDefinitions.View != null && objDataTableDefinitions.View.Count > 0)
            {
                DataRow row = objDataTableDefinitions.View[0].Row;
                objDataTableDefinitions.Index = objDataTable.Rows.IndexOf(row) + 1;
            }
        }

        /// <summary>
        /// Extension: Returns the number of rows. Unlike .Rows.Count also works on a filtered DataTable
        /// </summary>
        /// <param name="objDataTable"></param>
        public static int RecordCount(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            if (objDataTableDefinitions.View == null)
            {
                //CHE: not counting deleted rows unless corresponding RowStateFilter is set (cannot use DefaultView because it doesn't return as we wish) 
                //return objDataTable.Rows.Count;
                return objDataTable.DefaultViewRowsCountWithState();
            }
            else
            {
                //CHE: for filter pending add also those from NewRows
                if (objDataTableDefinitions.FilterType == FilterGroupEnum.adFilterPendingRecords)
                {
                    return objDataTableDefinitions.View.Count + (objDataTable.HasNewRows() ? objDataTableDefinitions.NewRows.Count : 0);
                }

                return objDataTableDefinitions.View.Count;
            }
        }

        private static int DefaultViewRowsCountWithState(this DataTable objDataTable)
        {

            //CHE: return rows from DefaultView
            DataView defaultView = objDataTable.DefaultView;
            //int nr = objDataTable.DefaultView.Count;
            int nr = defaultView.Count;

            //CHE: include last row which is currently added only if changes were done
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            if (objDataTableDefinitions.ParentGrid != null && objDataTable.IsNewRowAdded())
            {
                if (!objDataTableDefinitions.ParentGrid.IsCurrentRowDirty)
                {
                    nr--;
                }
            }

            //CHE: include rows from NewRows collection
            nr += objDataTableDefinitions.NewRows == null ? 0 : objDataTableDefinitions.NewRows.Count;

            //exclude deleted rows if flag not included in RowStateFilter
            if ((objDataTableDefinitions.LockType == LockTypeEnum.adLockBatchOptimistic) && (objDataTable.DefaultView.RowStateFilter & DataViewRowState.Deleted) != DataViewRowState.Deleted)
            {
                for (int i = 0; i < defaultView.Count; i++)
                {
                    DataRowState rowState = defaultView[i].Row.RowState;
                    if (rowState == DataRowState.Deleted)
                    {
                        --nr;
                    }
                }
            }



            return nr;
        }

        /// <summary>
        /// returns the current data source object, that could be binding to controls that supports binding
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static object DataSource(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            if (objDataTableDefinitions.View == null)
            {
                return objDataTable;
            }
            else
            {
                return objDataTableDefinitions.View;
            }
        }


        /// <summary>
        /// Searches a Recordset for the row that satisfies the specified criteria. 
        /// If you call the Find method on a recordset, and the current position in the recordset is at the last record or end of file (EOF), you will not find anything. 
        /// You need to call the MoveFirst method to set the current position/cursor to the beginning of the recordset.
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="strSearch"></param>
        public static void Find(this DataTable objDataTable, string strSearch)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (!EOF(objDataTable))
            {

                DataRow[] objSelectedRows = null;
                try
                {
                    objSelectedRows = objDataTable.Select(strSearch);
                }
                catch
                {
                    //if record not found, set index outside, EOF should return true
                    objDataTableDefinitions.Index = objDataTable.Rows.Count + 2;
                }

                //IPI: if no record is found, move index on EOF; + 2 ensures that index is not on a new row in DefaultView 
                if (objSelectedRows.Length == 0)
                {
                    //if record not found, set index outside, EOF should return true
                    objDataTableDefinitions.Index = objDataTable.Rows.Count + 2;
                    return;
                }

                //search in view from the current position forward
                while (!EOF(objDataTable))
                {
                    if (objSelectedRows != null && objSelectedRows.Contains(objDataTable.CurrentRow()))
                    {
                        //move also position in grid
                        MoveSelection(objDataTable, objDataTableDefinitions);
                        return;
                    }
                    //do not moveselection, in vb6 when calling find the rowcolchange does not raise for each row only after find once when the position is set to founded row
                    objDataTable.MoveNext(false);
                }

                //if record not found, set index outside, EOF should return true
                objDataTableDefinitions.Index = objDataTable.Rows.Count + 2;
            }
        }

        public static void Save(this DataTable objDataTable, string strFileName, PersistFormatEnum persistFormat)
        {

            //Set table name to avoid serializer exception
            string oldTableName = objDataTable.TableName;
            if (string.IsNullOrEmpty(objDataTable.TableName))
            {
                objDataTable.TableName = "tableName";
            }
            objDataTable.WriteXml(strFileName, XmlWriteMode.WriteSchema, true);
            if (oldTableName != objDataTable.TableName)
            {
                objDataTable.TableName = oldTableName;
            }


        }

        public static void OpenFile(this DataTable objDataTable, string strFileName)
        {
            objDataTable.ReadXml(strFileName);
        }

        /// <summary>
        /// Extension: Returns the current position
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static int AbsolutePosition(this DataTable objDataTable)
        {

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            if (objDataTableDefinitions.Deleted)
            {
                throw new Exception("Row handle refered to a deleted row or a row marked for deletion.");
            }

            return objDataTableDefinitions.AbsolutePosition;
        }

        /// <summary>
        /// Extension: sets the current position
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="intBookmark"></param>
        public static void AbsolutePosition(this DataTable objDataTable, int intBookmark)
        {

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (objDataTable.BOF(intBookmark) || objDataTable.EOF(intBookmark))
            {
                throw new Exception("Invalid bookmark");
            }

            //sets the new bookmark
            objDataTableDefinitions.Index = intBookmark;


        }

        /// <summary>
        /// Extension: Returns the current bookmark
        /// Bookmark must always return the row index in DataTable
        /// Bookmark returns null when the data table is null
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static int? Bookmark(this DataTable objDataTable, bool isEOFandBOFchecked = false)
        {
            if (objDataTable == null)
            {
                return null;
            }
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            if (objDataTableDefinitions.Deleted)
            {
                throw new Exception("Row handle refered to a deleted row or a row marked for deletion.");
            }

            DataRow currentRow = objDataTable.CurrentRow(isEOFandBOFchecked);

            int index = objDataTable.Rows.IndexOf(currentRow);

            //BAN: the current row can be present just in the DefaultView and not also in the DataTable, cases like adding a new row but not updating
            if (index == -1)
            {
                if (objDataTable.HasNewRows())
                {
                    // if a new row has been added with AddNew function, that the index is already set correctly
                    index = objDataTableDefinitions.NewRows.IndexOf(currentRow);
                    return objDataTableDefinitions.Index;
                }
            }
            //BAN: the current row can be present just in the DefaultView and not also in the DataTable, cases like adding a new row but not updating
            //the data table. In these cases, the index will be -1, but these cases occur just for the last row of the DefaultView so it should be returned
            //with -1 because the index will be incremented by one when returning
            if (index == -1 && currentRow.RowState == DataRowState.Detached)
            {
                index = objDataTable.DefaultView.Count - 1;
            }
            //If the row is not present in the DefaultView, then the row must have been added with AddNew in the NewRows collection
            if (index == -1 && objDataTableDefinitions.NewRows.Count > 0)
            {
                index = objDataTableDefinitions.NewRows.Count - 1;
            }

            return index + 1;
        }

        private static bool IsValidBookmark(DataTable objDataTable, DataTableDefinitions objDataTableDefinitions, int? intBookmark)
        {
            //The bookmark value must be checked also against the NewRows collection, because rows might have been added using the AddNew method
            //and these rows are not present in the DataTable Rows collection
            if (intBookmark < 1 || intBookmark > Math.Max(objDataTable.Rows.Count, objDataTable.DefaultView.Count) + (objDataTable.HasNewRows() ? objDataTableDefinitions.NewRows.Count : 0))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Extension: sets the current bookmark
        /// the bookmark is an index in the DataTable, but when it is set, it must update the current index in the DefaultView
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="intBookmark"></param>
        public static void Bookmark(this DataTable objDataTable, int? intBookmark)
        {
            if (intBookmark == null)
            {
                return;
            }

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            bool moveSelection = true;

            if (objDataTableDefinitions.ParentGrid != null)
            {
                if (intBookmark < 1 || intBookmark > objDataTable.Rows.Count)
                {
                    if (intBookmark == objDataTableDefinitions.ParentGrid.Rows.Count && objDataTableDefinitions.ParentGrid.AllowUserToAddRows)
                    {
                        moveSelection = false;
                    }
                }
            }
            if (moveSelection)
            {
                if (!IsValidBookmark(objDataTable, objDataTableDefinitions, intBookmark))
                {
                    throw new Exception("Invalid bookmark");
                }
            }

            //CHE: set position directly from bookmark parameter which is given as the index in table, not in view
            #region oldcode
            //DataRow bookmarkRow = null;
            //int indexInView = 1;
            ////If the bookmark is present in the DataTable Row count range, get the new index from the DefaultView
            ////Otherwise, the row was added using the AddNew method. In this case, the new index will be the parameter value
            //if (intBookmark <= objDataTable.Rows.Count)
            //{
            //    bookmarkRow = objDataTable.Rows[Convert.ToInt32(intBookmark - 1];

            //    foreach (DataRowView row in objDataTable.DefaultView)
            //    {
            //        if (row.Row == bookmarkRow)
            //        {
            //            break;
            //        }
            //        ++indexInView;
            //    }
            //}
            //else if (objDataTableDefinitions.NewRows != null && intBookmark <= objDataTableDefinitions.NewRows.Count)
            //{
            //    bookmarkRow = objDataTableDefinitions.NewRows[Convert.ToInt32(intBookmark - 1];
            //    indexInView = Convert.ToInt32(intBookmark;
            //}

            //objDataTableDefinitions.Index = indexInView;
            #endregion
            objDataTableDefinitions.Index = Convert.ToInt32(intBookmark);

            //AM: the selection is also moved when setting a bookmark
            if (moveSelection)
            {
                MoveSelection(objDataTable, objDataTableDefinitions);
            }
        }

        public static DataRow CurrentRow(this DataTable objDataTable, bool isEOFandBOFchecked = false)
        {

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (!isEOFandBOFchecked && (EOF(objDataTable) || BOF(objDataTable)))
            {
                throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
            }

            DataRow oRow = null;

            try
            {
                //return the current row
                #region Old Code
                //if (objDataTableDefinitions.View == null)
                //{
                //    oRow = objDataTable.Rows[objDataTableDefinitions.Index - 1];
                //}
                //else
                //{
                //    oRow = objDataTableDefinitions.View[objDataTableDefinitions.Index - 1].Row;
                //}
                #endregion

                if (objDataTableDefinitions.View == null)
                {
                    if (objDataTableDefinitions.Index > objDataTable.Rows.Count && objDataTable.HasNewRows())
                    {
                        oRow = objDataTableDefinitions.NewRows[objDataTableDefinitions.Index - objDataTable.Rows.Count - 1];
                    }
                    else
                    {
                        if (objDataTable.Rows.Count >= objDataTableDefinitions.Index)
                        {
                            oRow = objDataTable.Rows[objDataTableDefinitions.Index - 1];
                        }
                        else if (objDataTable.DefaultView != null && objDataTable.DefaultView.Count >= objDataTableDefinitions.Index)
                        {
                            oRow = objDataTable.DefaultView[objDataTableDefinitions.Index - 1].Row;
                        }
                    }
                }
                else
                {
                    if (objDataTableDefinitions.Index > objDataTableDefinitions.View.Count && objDataTable.HasNewRows() && objDataTableDefinitions.Index > objDataTable.Rows.Count)
                    {
                        oRow = objDataTableDefinitions.NewRows[objDataTableDefinitions.Index - objDataTable.Rows.Count - 1];
                    }
                    else
                    {
                        //CHE: Index is on DataTable, not on View
                        if (objDataTableDefinitions.Index - 1 >= 0 && objDataTableDefinitions.Index - 1 < objDataTable.Rows.Count)
                        {
                            oRow = objDataTable.Rows[objDataTableDefinitions.Index - 1];
                        }
                        //BAN: If index does not correspond to DataTable, return the row from the view
                        else
                        {
                            oRow = objDataTableDefinitions.View[objDataTableDefinitions.Index - 1].Row;
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                throw e;
            }


            return oRow;
        }

        //IPI: check if the current row is a new added row
        private static bool IsNewRowAdded(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            int currentIdx = objDataTableDefinitions.Index;

            int totalRows = objDataTable.Rows.Count + (objDataTable.HasNewRows() ? objDataTableDefinitions.NewRows.Count : 0);
            if (totalRows < objDataTable.DefaultView.Count)
            {
                totalRows = objDataTable.DefaultView.Count;
            }

            if (currentIdx < 1 || currentIdx > totalRows)
            {
                return false;
            }


            return currentIdx > objDataTable.Rows.Count;
        }

        //CHE: check if datatable has pending new rows
        public static bool HasNewRows(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (objDataTableDefinitions.NewRows != null)
            {
                return objDataTableDefinitions.NewRows.Count > 0;
            }

            return false;
        }

        /// <summary>
        /// Returns true if the current row is the first one (or if no rows are in the DataTable or the filtered DataTable)
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static bool BOF(this DataTable objDataTable)
        {
            //
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            #region Old Code
            //if (objDataTableDefinitions.View == null)
            //{
            //    return (objDataTable.Rows.Count == 0 || objDataTableDefinitions.Index == 0);                
            //}
            //else
            //{
            //    return (objDataTableDefinitions.View.Count == 0 || objDataTableDefinitions.Index == 0);                
            //}
            #endregion


            //
            //DSE Performance improvement by using properties for EOF and BOF and only computing them when necessary
            //return objDataTable.BOF(objDataTableDefinitions.Index);
            return objDataTableDefinitions.BOF;
        }

        private static bool BOF(this DataTable objDataTable, int index)
        {
            //
            try
            {
                //get the instance of the definitions class
                DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

                bool BOF = false;
                if (objDataTableDefinitions.View == null)
                {
                    //CHE: if you call movelast and there is no rows in view bof should return true
                    BOF = (objDataTable.Rows.Count == 0 || index == 0 || (index > objDataTable.Rows.Count && objDataTable.RecordCount() == 0));
                }
                else
                {
                    //IPI: if a Filter was set, check if the current row is a new added row in the DataTable
                    BOF = (objDataTableDefinitions.View.Count == 0 || index == 0);
                }

                if (BOF)
                {
                    //SBE: BOF should return false when View Count is 0, and row was deleted
                    return !(objDataTable.IsNewRowAdded() || objDataTableDefinitions.Deleted);
                }
                return BOF;
            }
            finally
            {
                //
            }
        }

        /// <summary>
        /// Returns true if the current row is the last one (or if no rows are in the DataTable or the filtered DataTable)
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static bool EOF(this DataTable objDataTable)
        {
            //
            #region Old Code
            ////get the instance of the definitions class
            //DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            //if (objDataTableDefinitions.View == null)
            //{
            //    return (objDataTable.Rows.Count == 0 || objDataTableDefinitions.Index > objDataTable.Rows.Count);
            //}
            //else
            //{
            //    return (objDataTableDefinitions.View.Count == 0 || objDataTableDefinitions.Index > objDataTableDefinitions.View.Count);
            //}
            #endregion

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            //            //DSE Performance improvement by using properties for EOF and BOF and only computing them when necessary
            //return objDataTable.EOF(objDataTableDefinitions.Index);
            return objDataTableDefinitions.EOF;
        }

        //DSE Performance improvement by using properties for EOF and BOF and only computing them when necessary
        internal static void ComputeEofBof(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            objDataTableDefinitions.EOF = objDataTable.EOF(objDataTableDefinitions.Index);
            objDataTableDefinitions.BOF = objDataTable.BOF(objDataTableDefinitions.Index);
        }

        private static bool EOF(this DataTable objDataTable, int index)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            bool EOF = false;
            if (objDataTableDefinitions.View == null)
            {
                //CHE: if you call movefirst and there is no rows in view eof should return true
                EOF = (objDataTable.Rows.Count == 0 || index > objDataTable.Rows.Count || (index == 0 && objDataTable.RecordCount() == 0));
            }
            else
            {
                EOF = (objDataTableDefinitions.View.Count == 0 || index > objDataTableDefinitions.View.Count);
            }

            if (!EOF)
            {
                return EOF;
            }

            if (objDataTableDefinitions.View == null)
            {
                if (objDataTable.IsNewRowAdded())
                {
                    if (objDataTable.HasNewRows())
                    {
                        if ((objDataTable.Rows.Count + objDataTableDefinitions.NewRows.Count) >= index)
                        {
                            return false;
                        }
                    }
                    else if (objDataTable.Rows.Count >= index)
                    {
                        return false;
                    }
                    //SBE: check counts and index on default view, when current row is the last added row
                    else if (objDataTable.DefaultView.Count >= index)
                    {
                        return false;
                    }

                }
            }
            else
            {
                if (objDataTable.IsNewRowAdded())
                {
                    if (objDataTable.HasNewRows())
                    {
                        if ((objDataTable.Rows.Count + objDataTableDefinitions.NewRows.Count) >= index)
                        {
                            return false;
                        }
                    }
                    else if (objDataTableDefinitions.View.Count >= index)
                    {
                        return false;
                    }
                }
                else
                {
                    //BAN: If the index is greater than the DataView row count it means that the DataView has a filter applied
                    //Check if the DataRow at the specified index corresponds to a DataRowView in the DataView. If the DataView has the row
                    //then EOF is false, if it does not contain it, EOF is true
                    int currentIdx = index;
                    bool found = false;
                    if (currentIdx > 0 && currentIdx <= objDataTable.Rows.Count)
                    {
                        DataRow dataRow = objDataTable.Rows[currentIdx - 1];
                        foreach (DataRowView dataRowView in objDataTableDefinitions.View)
                        {
                            if (dataRowView.Row == dataRow)
                            {
                                found = true;
                                break;
                            }
                        }

                        EOF = !found;

                        //IPI: if the index is on a new row, EOF should return false 
                        if (EOF && objDataTable.HasNewRows())
                        {
                            if ((objDataTable.Rows.Count + objDataTableDefinitions.NewRows.Count) >= index)
                            {
                                return false;
                            }
                        }

                        //SBE: If RowState is Deleted, then EOF should return false (current index is lower than rows count)
                        if (dataRow.RowState == DataRowState.Deleted)
                        {
                            EOF = false;
                        }
                    }
                }
            }

            return EOF;
        }

        internal static void SetDataAdapter(this DataTable objDataTable, DbDataAdapter objDataAdapter)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            objDataTableDefinitions.DataAdapter = objDataAdapter;
        }

        public static void Update(this DataTable objDataTable, object[] arrFields, object[] arrValues)
        {

            //CHE: set value for multiple fields
            for (int i = 0; i < Math.Min(arrFields.Length, arrValues.Length); i++)
            {
                objDataTable.Fields(arrFields[i].ToString()).Value = (arrValues[i] != null) ? arrValues[i] : DBNull.Value;
            }

            objDataTable.Update();
        }

        public static void Update(this DataTable objDataTable, object field, object value)
        {

            //CHE: set value for field
            if (field != null)
            {
                objDataTable.Fields(field.ToString()).Value = (value != null) ? value : DBNull.Value;
            }

            objDataTable.Update();
        }

        //CHE: Deletes the current record or a group of records
        public static void Delete(this DataTable objDataTable, AffectEnum affect)
        {

            switch (affect)
            {
                case AffectEnum.adAffectCurrent:
                    {
                        objDataTable.Delete();
                        break;
                    }
                default:
                    {
                        objDataTable.MoveFirst();
                        while (!EOF(objDataTable))
                        {
                            objDataTable.Delete();
                            objDataTable.MoveNext();
                        }
                        break;
                    }
            }


        }

        public static void Resync(this DataTable objDataTable, AffectEnum affect = AffectEnum.adAffectAll, ResyncEnum resync = ResyncEnum.adResyncAllValues)
        {
            //Sys.LogNotImplemented();
        }

        public static void Requery(this DataTable objDataTable)
        {
            objDataTable.Clear();
            objDataTable.GetDataAdapter().Fill(objDataTable);
        }

        /// <summary>
        /// deletes the current row
        /// </summary>
        /// <param name="objDataTable"></param>
        public static void Delete(this DataTable objDataTable)
        {

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            if (objDataTableDefinitions.Deleted)
            {
                throw new Exception("Row handle refered to a deleted row or a row marked for deletion.");
            }

            if (EOF(objDataTable) || BOF(objDataTable))
            {
                throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
            }

            //CHE: only mark for deletion, index remains the same
            //objDataTable.Rows.Remove(objDataTable.CurrentRow());
            //objDataTableDefinitions.Index = objDataTableDefinitions.Index - 1;
            //APE: preserve index when deleting. If the current cell is on the row that is being deleted, only if the DataTable is bound to a DataGridView, the index will shift due to the CurrentCellChange
            //event of the bound DataGridView
            int index = -1;
            if (objDataTableDefinitions.ParentGrid != null)
            {
                index = objDataTableDefinitions.Index;
            }
            DataRow currentRow = objDataTable.CurrentRow(true);
            currentRow.Delete();
            if (index > -1)
            {
                objDataTableDefinitions.Index = index;
            }
            objDataTableDefinitions.Deleted = true;

            //DSE rows should be physically deleted if not Batch opened
            if (objDataTableDefinitions.LockType != LockTypeEnum.adLockBatchOptimistic)
            {
                objDataTable.Update(currentRow);
            }


        }

        /// <summary>
        /// Check if RowChange is suppressed.
        /// </summary>
        /// <param name="objDatatable"></param>
        /// <returns></returns>
        public static bool IsSupressRowChange(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            return objDataTableDefinitions.SupressRowChange;
        }

        /// <summary>
        /// Determines whether [is reading data from database] [the specified object data table].
        /// </summary>
        /// <param name="objDataTable">The object data table.</param>
        /// <returns><c>true</c> if [is reading data from database] [the specified object data table]; otherwise, <c>false</c>.</returns>
        public static bool IsInternalDataAction(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            return objDataTableDefinitions.IsInternalDataAction;
        }

        /// <summary>
        /// Save the change into the database. note: the Recordset must be opened using the extension method OpenDataTable of DBConnection , with blnIsUpdatable parameter equals to True
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static int Update(this DataTable objDataTable, DataRow dataRow = null, bool inBatch = false)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            //do nothing for LockType = LockTypeEnum.adLockBatchOptimistic unless is in UpdateBatch then let the update to be executed
            if (objDataTableDefinitions.LockType == LockTypeEnum.adLockBatchOptimistic && !inBatch)
            {
                return 0;
            }

            //if a row was not provided then consider update on current row
            if (dataRow == null)
            {
                //check if current row exist
                if (EOF(objDataTable) || BOF(objDataTable))
                {
                    return 0;
                }

                dataRow = objDataTable.CurrentRow(true);
            }

            DataRowState rowState = dataRow.RowState;

            //exit if no changes
            if (rowState == DataRowState.Unchanged)
            {
                return 0;
            }

            //move from internal collection NewRows to DataTable
            if (rowState == DataRowState.Added || rowState == DataRowState.Detached)
            {
                objDataTable.UpdateNewRows(objDataTableDefinitions);
            }

            //CHE: for disconnected DataTable do nothing on update
            if (objDataTableDefinitions.DataAdapter != null && objDataTableDefinitions.DataAdapter.SelectCommand != null && objDataTableDefinitions.DataAdapter.SelectCommand.Connection == null)
            {
                return 0;
            }
            if (objDataTableDefinitions.DataAdapter != null)
            {
                return UpdateInternal(objDataTableDefinitions, dataRow);
            }
            //DSE DataTable should accept changes even if RecordSet source string is empty
            //else  if (!string.IsNullOrEmpty (objDataTable.Source()))
            //{
            //    //IPI: need to change the RowState from Modified to Unchanged
            //    objDataTable.AcceptChanges();
            //    return 0;
            //    // throw new Exception("The current DataTable is not updatable");
            //}
            //else 
            //{
            //    return 0;
            //}
            else
            {
                objDataTable.AcceptChanges();
                return 0;
            }
        }

        private static void UpdateNewRows(this DataTable objDataTable, DataTableDefinitions objDataTableDefinitions)
        {

            // Check for IsEdit and IsNew only if ParentGrid exists
            if (objDataTableDefinitions != null && objDataTableDefinitions.ParentGrid != null)
            {
                foreach (DataRowView drView in objDataTable.DefaultView)
                {
                    bool endEditForCurrentRow = false;

                    bool isEdit = drView.IsEdit;
                    bool isNew = drView.IsNew;

                    if (isEdit && !isNew)
                    {
                        endEditForCurrentRow = true;
                    }
                    else if (isEdit && isNew && drView.Row.RowState != DataRowState.Deleted)
                    {
                        //SBE: check if row was modified
                        for (int i = 0; i < drView.Row.ItemArray.Length; i++)
                        {
                            object value = drView.Row.ItemArray[i];
                            if (value != null && !Information.IsDBNull(value))
                            {
                                endEditForCurrentRow = true;
                                break;
                            }
                        }

                        if (endEditForCurrentRow)
                        {
                            //CNA: need to set empty values for drView's cells if DataTable column does not allow DBNull
                            objDataTable.SetRowEmptyValues(drView.Row);
                            drView.EndEdit();
                            //IPI: when EndEdit is executed, drView.Row will be committed to the Rows collection
                            // if a row is in the DataTable.Rows collection, it should be removed from NewRows
                            if (objDataTableDefinitions.NewRows != null && objDataTableDefinitions.NewRows.Contains(drView.Row))
                            {
                                objDataTableDefinitions.NewRows.Remove(drView.Row);
                            }
                        }
                    }
                }
            }

            //CHE: AddNew on DataTable with mandatory fields will raise exception if new empty row is added in collection, must be added later on Update
            if (objDataTable.HasNewRows())
            {
                //DSE improve performance by not using .AsEnumerable().ToList()
                //List<DataRow> dataTableRows = objDataTable.AsEnumerable().ToList();
                //CHE: collection is modified in RowChanging, use for instead of foreach
                for (int i = 0; i < objDataTableDefinitions.NewRows.Count; i++)
                {
                    DataRow row = objDataTableDefinitions.NewRows[i];
                    // add new row only if it does not belong to another DataTable
                    if (row.RowState == DataRowState.Detached)
                    {
                        objDataTable.SetRowEmptyValues(row);
                        objDataTable.Rows.Add(row);
                        //DSE: adjust index because of RowChanging affecting the collection
                        i--;
                    }
                }
                objDataTableDefinitions.NewRows.Clear();
            }

            //IPI: must update all clone tables as well
            DataTableDefinitions.SynchronizeClones(objDataTable, Synchronization.UpdateRow);


        }

        //CHE: move from NewRows collection to DataTable as rows having RowState = Added
        public static void UpdateNewRows(this DataTable objDataTable)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            objDataTable.UpdateNewRows(objDataTableDefinitions);
        }

        public static bool SetDataTableEmptyValues(this DataTable objDataTable)
        {

            bool valueChanged = false;

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            foreach (DataRow row in objDataTable.Rows)
            {
                valueChanged = objDataTable.SetRowEmptyValues(row) || valueChanged;
            }

            if (objDataTable.HasNewRows())
            {
                //CHE: collection is modified in RowChanging, use for instead of foreach
                //foreach (DataRow row in objDataTableDefinitions.NewRows)
                for (int i = 0; i < objDataTableDefinitions.NewRows.Count; i++)
                {
                    DataRow row = objDataTableDefinitions.NewRows[i];
                    valueChanged = objDataTable.SetRowEmptyValues(row) || valueChanged;
                }
            }


            return valueChanged;
        }

        public static bool SetRowEmptyValues(this DataTable objDataTable, DataRow row)
        {
            if (row == null)
            {
                return false;
            }

            bool valueChanged = false;
            //SBE: set empty values ("" for strings or 0 for numerics), if field does not allow null values
            bool isDeleted = (row.RowState == DataRowState.Deleted);

            if (!isDeleted)
            {
                Type columnType = null;
                foreach (Field col in objDataTable.Fields())
                {
                    if (Information.IsDBNull(row[col.ColumnName]) && !col.AllowDBNull)
                    {
                        columnType = objDataTable.Columns[col.ColumnName].DataType;
                        if (columnType == typeof(System.String))
                        {
                            row[col.ColumnName] = "";
                        }
                        else if (columnType.IsValueType)
                        {
                            row[col.ColumnName] = Activator.CreateInstance(columnType);
                        }
                        valueChanged = true;
                    }
                }
            }


            return valueChanged;
        }

        private static int UpdateInternal(DataTableDefinitions objDataTableDefinitions, DataRow dataRow)
        {
            string sql = "";
            if (objDataTableDefinitions != null && objDataTableDefinitions.DataAdapter != null && objDataTableDefinitions.DataAdapter.SelectCommand != null)
            {
                sql = objDataTableDefinitions.DataAdapter.SelectCommand.CommandText;
            }


            DataTable objDataTable = dataRow.Table;
            int affectedRows = 0;

            try
            {
                // Generate update command for single row DataTable, because if the datatable has already been committed to the database
                // and an update statement was performed on a column in from the database table, the DataTable will not be in sync with the changes
                //CHE: removed condition, query too complex error received if this is not executed due to objDataTable.PrimaryKey.Count=0 
                //if (objDataTable.PrimaryKey != null && objDataTable.PrimaryKey.Count() > 0 && objDataTable.Rows.Count == 1)
                if (objDataTable.PrimaryKey != null && objDataTable.Rows.Count == 1)
                {
                    DataRow row = objDataTable.Rows[0];
                    if (row.HasVersion(DataRowVersion.Original) && row.HasVersion(DataRowVersion.Current))
                    {
                        DbConnection conn = objDataTable.ActiveConnection();
                        DbCommand cmd = null;
                        string text = "";
                        object originalValue;
                        object currentVersion;

                        if (conn is SqlConnection)
                        {
                            SqlCommandBuilder builder = new SqlCommandBuilder(objDataTableDefinitions.DataAdapter as SqlDataAdapter);
                            builder.SetAllValues = false;
                            builder.ConflictOption = ConflictOption.OverwriteChanges;
                            cmd = builder.GetUpdateCommand(true);
                            text = cmd.CommandText;

                            foreach (DataColumn col in objDataTable.Columns)
                            {
                                originalValue = row[col, DataRowVersion.Original];
                                currentVersion = row[col, DataRowVersion.Current];

                                if (currentVersion.Equals(originalValue))
                                {
                                    text = text.Replace("[" + col.ColumnName + "] = @" + col.ColumnName + ", ", "").Replace(", [" + col.ColumnName + "] = @" + col.ColumnName, "");
                                }
                            }
                        }
                        else if (conn is OleDbConnection)
                        {
                            //CHE: for tables with large number of columns (e.g. 80) when calling DataAdapter.Update error "Query too complex" is received because where clause contains too many conditions;
                            //modify UpdateCommand to contain only PK column in the where clause
                            OleDbCommandBuilder builder = new OleDbCommandBuilder(objDataTableDefinitions.DataAdapter as OleDbDataAdapter);
                            //     Specifies whether all column values in an update statement are included or
                            //     only changed ones.
                            builder.SetAllValues = false;
                            //     All update and delete statements include only System.Data.DataTable.PrimaryKey
                            //     columns in the WHERE clause. If no System.Data.DataTable.PrimaryKey is defined,
                            //     all searchable columns are included in the WHERE clause.
                            builder.ConflictOption = ConflictOption.OverwriteChanges;
                            //CHE: wrap table and column names in square brackets  - are required if any table or column names contain spaces or "funny" characters, or if they happen to be reserved words in Access SQL
                            builder.QuotePrefix = "[";
                            builder.QuoteSuffix = "]";

                            cmd = builder.GetUpdateCommand(true);

                            text = cmd.CommandText;
                            foreach (DataColumn col in objDataTable.Columns)
                            {
                                originalValue = row[col, DataRowVersion.Original];
                                currentVersion = row[col, DataRowVersion.Current];

                                if (currentVersion.Equals(originalValue))
                                {
                                    text = text.Replace("[" + col.ColumnName + "] = ?, ", "").Replace(", [" + col.ColumnName + "] = ?", "");
                                }
                            }
                        }                      
                        cmd.CommandText = text;

                        objDataTableDefinitions.DataAdapter.UpdateCommand = cmd;
                    }
                }

                bool isAdded = dataRow.RowState == DataRowState.Added;
                if (objDataTableDefinitions.DataAdapter != null)
                {
                    affectedRows = objDataTableDefinitions.DataAdapter.Update(new DataRow[] { dataRow });
                }

                //SBE: if table contains the IdentityColumn, update each row. On INSERT get the last inserted ID and update DataTable
                if (objDataTableDefinitions.IdentityColumn != null && isAdded && !string.IsNullOrEmpty(objDataTable.TableName))
                {
                    string strSQLGetIdentityValue = string.IsNullOrEmpty(objDataTable.TableName) ? string.Empty : String.Format("SELECT IDENT_CURRENT('{0}') AS ID", objDataTable.TableName);

                    //SBE: execute statement to get the value for identity column
                    if (affectedRows > 0)
                    {
                        DbConnection currentConnection = objDataTable.ActiveConnection();
                        DbCommand cmd = null;

                        if (currentConnection is System.Data.SqlClient.SqlConnection) 
						{
                            cmd = new SqlCommand();
                            strSQLGetIdentityValue = string.IsNullOrEmpty(objDataTable.TableName) ? string.Empty : String.Format("SELECT IDENT_CURRENT('{0}') AS ID", objDataTable.TableName);
                        }
                        else if (currentConnection is System.Data.OleDb.OleDbConnection) 
						{
                            cmd = new OleDbCommand();
                            strSQLGetIdentityValue = "SELECT @@Identity";
                        }

                        cmd.Connection = currentConnection;
                        cmd.Transaction = cmd.Connection.GetTransaction();
                        cmd.CommandText = strSQLGetIdentityValue;
                        object idValue = cmd.ExecuteScalar();
                        dataRow[objDataTableDefinitions.IdentityColumn.ColumnName] = idValue;
                        // BAN - if the changes are not accepted, the row will be marked as modified, and if an update is performed on the DataTable,
                        // the identity column will be seen as changed and the data adapter will try to update it in the database, thus returning a concurrency violation
                        dataRow.AcceptChanges();
                    }
                }

            }
            //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key                
            //e.g. Dynamic SQL generation for the UpdateCommand is not supported against a SelectCommand that does not return any key column information.
            catch (InvalidOperationException ex)
            {
                //clear errors
                if (objDataTable.HasErrors)
                {
                    DataRow[] rowsInError = objDataTable.GetErrors();
                    for (int i = 0; i < rowsInError.Length; i++)
                    {
                        //foreach (DataColumn myCol in objDataTable.Columns)
                        //{
                        //    Console.WriteLine(myCol.ColumnName + " " +
                        //    rowsInError[i].GetColumnError(myCol));
                        //}

                        rowsInError[i].ClearErrors();
                    }
                }

                //CHE: in UpdateBatch before calling FillSchema replace null field with default value from SQL server if field does not accept null
                foreach (Field f in objDataTable.Fields())
                {
                    if (!f.AllowDBNull)
                    {
                        try
                        {
                            foreach (DataRow dr in objDataTable.Rows)
                            {
                                if (dr.RowState != DataRowState.Deleted && dr[f.ColumnName] == DBNull.Value && f.DefaultValue != null)
                                {
                                    string value = f.DefaultValue.TrimStart(new char[] { '(' }).TrimEnd(new char[] { ')' });
                                    if (f.Type == Convert.ToInt32(SqlDbType.Bit) && value != null)
                                    {
                                        dr[f.ColumnName] = Field.BoolFromString(value);
                                    }
                                    else
                                    {
                                        dr[f.ColumnName] = value;
                                    }
                                }
                            }
                        }
                        catch
                        {
                        }
                    }
                }

                //CHE: schema is necessary in UpdateBatchBuilder e.g. to skip identity column
                objDataTableDefinitions.DataAdapter.FillSchema(objDataTable, SchemaType.Mapped);

                //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
                affectedRows = UpdateTry(dataRow, false/*(objDataTable.ActiveConnection() is OracleConnection)*/);

                if (affectedRows == -1)
                {
                    string msg = "fecherFoundation - Fatal error on update recordset!";

                    //THIS MESSAGE BOX SHOULD NEVER BE DISPLAYED; IF IT DOES A FIX NEEDS TO BE FOUNDED ON CUSTOMER CODE 
                    MessageBox.Show(ex.Message, msg, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }

                //BAN: accept all changes in the data table, this way the row states will become unchanged, so if the same data table will have rows changed (added, modified, deleted)
                //and the UpdateBatch is called again, the previous rows will not influence the new call
                objDataTable.AcceptChanges();

            }
            //CHE: FilterType should not affect grid, restore position
            finally
            {
                if (objDataTableDefinitions.ParentGrid != null && objDataTableDefinitions.BookmarkBeforeFilterType != null && objDataTable.FilterType() != FilterGroupEnum.adFilterNone)
                {
                    objDataTable.Filter(FilterGroupEnum.adFilterNone);
                    //CHE: restore position only if it is valid
                    if (IsValidBookmark(objDataTable, objDataTableDefinitions, objDataTableDefinitions.BookmarkBeforeFilterType))
                    {
                        objDataTable.Bookmark(objDataTableDefinitions.BookmarkBeforeFilterType);
                    }
                    objDataTableDefinitions.BookmarkBeforeFilterType = null;
                }


            }
            return affectedRows;
        }

        public static int GetIndex(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            return objDataTableDefinitions.Index;

        }

        //CHE: set position in datatable
        public static void SetIndex(this DataTable objDataTable, int index)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            objDataTableDefinitions.Index = index;

        }

        //CNA: Implemented AddNew based on column and value arrays
        public static void AddNew(this DataTable dataTable, object[] columns = null, object[] values = null)
        {

            //if current row is marked for deletion do not update
            DataRow dataRow = GetCurrentRow(dataTable);
            if (dataRow != null && dataRow.RowState != DataRowState.Deleted)
            {
                //update the current record
                dataTable.Update();
            }

            //get the instance of the definitions class
            DataTableDefinitions dataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(dataTable);
            // add the row to the new datatable
            DataRow objRow = dataTable.NewRow();
            //CNA: need to set empty values for drView's cells if DataTable column does not allow DBNull
            dataTable.SetRowEmptyValues(objRow);
            //BAN: add the row before setting the values, this way, the row will be added in the cloned tables
            dataTable.Rows.Add(objRow);

            if (columns != null && values != null)
            {
                for (int i = 0; i < columns.Length; i++)
                {
                    if (columns[i] is int)
                    {
                        objRow[Convert.ToInt32(columns[i])] = values[i];
                    }
                    else
                    {
                        objRow[columns[i].ToString()] = values[i];
                    }
                }
            }

            dataTableDefinitions.Index = dataTable.Rows.Count;


        }

        //CNA: Implemented AddNew based on column and value
        public static void AddNew(this DataTable dataTable, object column = null, object value = null)
        {

            //if current row is marked for deletion do not update
            DataRow dataRow = GetCurrentRow(dataTable);
            if (dataRow != null && dataRow.RowState != DataRowState.Deleted)
            {
                //update the current record
                dataTable.Update();
            }

            //get the instance of the definitions class
            DataTableDefinitions dataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(dataTable);
            // add the row to the new datatable
            DataRow objRow = dataTable.NewRow();

            if (column != null && value != null)
            {
                objRow[column.ToString()] = value;
            }

            dataTable.Rows.Add(objRow);
            dataTableDefinitions.Index = dataTable.Rows.Count;


        }

        // check 
        public static bool IsNumeric(this DataColumn col)
        {
            if (col == null)
                return false;
            
            return col.DataType.IsNumeric();
        }

        public static void Open(this DataTable objDataTable, string Source = null, object ActiveConnection = null, CursorTypeEnum? CursorType = CursorTypeEnum.adOpenUnspecified, LockTypeEnum? LockType = LockTypeEnum.adLockUnspecified, int? Options = -1)
        {


            //check unused connections
            if (checkFieldCache > 1000)
            {
                foreach (DataTableDefinitions dtf in new System.Collections.ArrayList(DataTableExtension.mobjDefinitions.Values))
                {
                    if (dtf.connection != null && dtf.connection.State != ConnectionState.Open)
                    {
                        dtf.connection = null;
                    }
                }
                checkFieldCache = 0;
            }
            checkFieldCache++;

            // if we receive a connection get the table 
            DbConnection objDbConnection = null;
            try
            {
                if (!string.IsNullOrEmpty(Source) && !FilePathHasInvalidChars(Source) && File.Exists(Source))
                {
                    objDataTable.OpenFile(Source);
                    return;
                }


                //get the instance of the definitions class

                DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);


                //BAN: Clear the DataTable before opening it
                //CHE: no need to clear table if the call is from UpdateTryCatch - DataTableExtension (for getting autoincrement values back), otherwise it throws exception when calling adapter.fill
                if (!string.IsNullOrEmpty(Source) || (objDataTable.Source() != "" && !objDataTableDefinitions.SupressRowChange))
                {
                    objDataTable.Clear();
                    objDataTable.Columns.Clear();
                }


                if (LockType != null)
                {
                    objDataTable.LockType((LockTypeEnum)LockType);
                }


                //preserve connection if parameter was not specified
                if (ActiveConnection == null)
                {
                    ActiveConnection = objDataTable.ActiveConnection();
                }



                //preserve source if parameter was not specified
                if (Source == null && objDataTable.Source() != "")
                {
                    Source = objDataTable.Source();
                }

                if (ActiveConnection is System.Data.SqlClient.SqlConnection)
                {
                    objDbConnection = (System.Data.SqlClient.SqlConnection)ActiveConnection;
                }
                else if (ActiveConnection is System.Data.OleDb.OleDbConnection)
                {
                    objDbConnection = (System.Data.OleDb.OleDbConnection)ActiveConnection;
                }
                //IPI: both Source & ActiveConnection are null when working with disconnected recordsets
                else if (ActiveConnection != null)
                {

                    // if we recieve connection string, we should open the connection according to the provided connection string and fetch the table.
                    objDbConnection = new System.Data.OleDb.OleDbConnection();

                    objDbConnection.ConnectionString = (string)ActiveConnection;
                    objDbConnection.Open();
                }

                bool isUpdateable = true;

                //IPI: set the DataTable Source
                objDataTableDefinitions.Source = Source;




                //clear rows - in VB6 recordset was previously closed
                if (objDataTable.Rows.Count > 0)
                {
                    DataGridView dgv = objDataTableDefinitions.ParentGrid;
                    if (dgv != null && dgv.DataSource == objDataTable)
                    {
                        dgv.DataSource = null;
                        objDataTable.Rows.Clear();
                        dgv.DataSource = objDataTable;
                    }
                    else
                    {
                        objDataTable.Rows.Clear();
                    }

                    objDataTableDefinitions.Index = 0;
                }


                //IPI: both Source & ActiveConnection are null when working with disconnected recordsets
                if (Source != null)
                {
                    DataTableDefinitions.GetDataTableDefinitions(objDataTable).IsInternalDataAction = true;
                    objDbConnection.OpenDataTable(Source, isUpdateable, ref objDataTable);
                }


                //DSE Performance improvement by using properties for EOF and BOF and only computing them when necessary
                objDataTable.ComputeEofBof();
                if (objDataTable.Rows.Count > 0)
                {

                    objDataTable.MoveFirst();

                }



                //IPI: initialize Fields list


                if (Source != null)
                {

                    //CHE: set TableName necessary in UpdateBatchBuilder; identify first "from" in string to set correct TableName
                    System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(Source, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*) where", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (match.Success && match.Groups[1].Index == Source.ToLower().IndexOf("from ") + 5)
                    {
                        objDataTable.TableName = match.Groups[1].Value;
                    }
                    else
                    {
                        System.Text.RegularExpressions.Match match1 = System.Text.RegularExpressions.Regex.Match(Source, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*) order by", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        if (match1.Success && match1.Groups[1].Index == Source.ToLower().IndexOf("from ") + 5)
                        {
                            objDataTable.TableName = match1.Groups[1].Value;
                        }
                        else
                        {
                            //CHE: treat alias
                            System.Text.RegularExpressions.Match match2 = System.Text.RegularExpressions.Regex.Match(Source, "from ([a-zA-Z0-9üöäÄÜÖß_\\. ]*) where", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                            if (match2.Success && match2.Groups[1].Index == Source.ToLower().IndexOf("from ") + 5)
                            {
                                if (match2.Groups[1].Value.Contains(" "))
                                {
                                    objDataTable.TableName = match2.Groups[1].Value.Substring(0, match2.Groups[1].Value.IndexOf(" "));
                                }
                                else
                                {
                                    objDataTable.TableName = match2.Groups[1].Value;
                                }
                            }
                            else
                            {
                            }
                        }
                    }

                    objDataTableDefinitions.connString = objDbConnection.ConnectionString;

                    //use act-conn for evtl. loading fields
                    objDataTableDefinitions.connection = objDbConnection;

                    DataTableDefinitions.GetDataTableDefinitions(objDataTable).IsInternalDataAction = false;
                }
            }
            catch (System.Exception ex)
            {
                if (objDbConnection != null)
                {
                    //for AdoDbConnection compatibility
                    objDbConnection.Errors().Add(ex);
                }
                throw(ex);
            }

        }

        internal static DbDataAdapter GetDataAdapter(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            return objDataTableDefinitions.DataAdapter;
        }

        /// <summary>
        /// Update batch with no parameters, sends the information into the database.
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static int UpdateBatch(this DataTable objDataTable)
        {
            // run the UpdateBatch without all parameters
            return UpdateBatch(objDataTable, null);
        }

        public static int UpdateBatch(this DataTable objDataTable, object arrFields)
        {
            string sql = "";
            if (objDataTable != null && objDataTable.GetDataAdapter() != null && objDataTable.GetDataAdapter().SelectCommand != null)
            {
                sql = objDataTable.GetDataAdapter().SelectCommand.CommandText;
            }

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            //do nothing unless LockType = LockTypeEnum.adLockBatchOptimistic
            if (objDataTableDefinitions.LockType != LockTypeEnum.adLockBatchOptimistic)
            {
                return 0;
            }

            if (objDataTableDefinitions.DataAdapter != null)
            {
                //CHE: do nothing for disconnected recordset
                if (objDataTableDefinitions.DataAdapter.SelectCommand.Connection == null)
                {
                    return 0;
                }

                //move from NewRows collection to the DataTable
                objDataTable.UpdateNewRows(objDataTableDefinitions);

                //call update on each row
                int affectedRows = 0;
                foreach (DataRow dr in objDataTable.Rows)
                {
                    //DSE: update only if row was changed!
                    if (dr.RowState != DataRowState.Unchanged)
                    {
                        affectedRows += objDataTable.Update(dr, true);
                    }
                }
                return affectedRows;
            }
            //BAN: if the data adapter is null but the DataTable state is open the UpdateBatch should return 0 and not an error
            else if (objDataTableDefinitions.DataAdapter == null && objDataTable.State() == ConnectionState.Open)
            {
                //SBE: call AcceptChanges to change RowState. Records added with AddNew must have RowState Unmodified after UpdatedBatch
                objDataTable.AcceptChanges();
                return 0;
            }
            else
            {
                throw new Exception("The current DataTable is not updatable");
            }
        }

		private static CommandDescriptor GetCommand(DataRowState drs, string tableName, string str1, string str2)
		{
			CommandDescriptor item = new CommandDescriptor();
			string str = "";
			switch (drs)
			{
				case DataRowState.Deleted:
					{
						str = String.Format("delete from {0} where {1}", tableName, str1);
						break;
					}
				case DataRowState.Modified:
					{
						str = String.Format("update {0} set {1} where {2}", tableName, str2, str1);
						break;
					}
				case DataRowState.Added:
					{
						str = String.Format("insert into {0} ({1}) values ({2})", tableName, str1, str2);
						break;
					}
			}
			item.whereClause = str1;
			item.tableName = tableName;
			item.command = str;
			return item;
		}

		private static string GetWhereClause(string selectStatement)
		{
			string originalSelectStatement = selectStatement;
			selectStatement = selectStatement.ToUpper();
			string whereClause = "";
			int orderIndex = -1, whereIndex = -1;
			int whereLength = "WHERE".Length;
			//https://msdn.microsoft.com/en-us/library/h09t6a82%28v=vs.80%29.aspx
			if (selectStatement.Contains("WHERE") && !selectStatement.Contains("GROUP BY") && !selectStatement.Contains("UNION") && !selectStatement.Contains("INTO") && !selectStatement.Contains("PREFERENCE "))
			{
				whereIndex = selectStatement.IndexOf("WHERE");
				orderIndex = selectStatement.IndexOf("ORDER BY");
				if (orderIndex > -1)
				{
					whereClause = originalSelectStatement.Substring(whereIndex + whereLength, orderIndex - whereIndex - whereLength);
				}
				else
				{
					whereClause = originalSelectStatement.Substring(whereIndex + whereLength);
				}
			}
			return whereClause;
		}

        //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
        private static int UpdateTry(DataRow dataRow, bool isOracleConnection)
        {
            DataTable dataTable = dataRow.Table;
            int colCount = dataTable.Columns.Count;
            List<CommandDescriptor> commands = new List<CommandDescriptor>();
            DataRowState drs = dataRow.RowState;
            //If the TableName is not null means that the statement was executed on one table. If the statement contains join 
            //the columns must be separated by parent table, and statements need to be created individually for each table
            if (!string.IsNullOrEmpty(dataTable.TableName))
            {
                StringBuilder str1 = new StringBuilder();
                StringBuilder str2 = null;
                if (drs != DataRowState.Deleted)
                {
                    str2 = new StringBuilder();
                }
                DataColumn dataColumn = null;
                for (int columnIndex = 0; columnIndex < colCount; columnIndex++)
                {
                    dataColumn = dataTable.Columns[columnIndex];
                    if (drs == DataRowState.Deleted || drs == DataRowState.Modified)
                    {
                        str1.Append(dataColumn.ColumnName + FormatValue(isOracleConnection, dataColumn.DataType, dataRow[columnIndex, DataRowVersion.Original], true));

                        if (columnIndex < colCount - 1)
                        {
                            str1.Append(" and ");
                        }
                    }

                    //CHE: ignore ROWID in update statement for Oracle
                    if (drs == DataRowState.Modified && !dataColumn.AutoIncrement && !(isOracleConnection && dataColumn.ColumnName.ToUpper() == "ROWID"))
                    {
                        str2.Append(dataColumn.ColumnName + "=" + FormatValue(isOracleConnection, dataColumn.DataType, dataRow[columnIndex]));
                        if (columnIndex < colCount - 1)
                        {
                            str2.Append(", ");
                        }
                    }
                    if (drs == DataRowState.Added && !dataColumn.AutoIncrement)
                    {
                        str1.Append(dataColumn.ColumnName);
                        str2.Append(FormatValue(isOracleConnection, dataColumn.DataType, dataRow[columnIndex]));
                        if (columnIndex < colCount - 1)
                        {
                            str1.Append(", ");
                            str2.Append(", ");
                        }
                    }
                }
				string selectStatement = dataTable.GetDataAdapter().SelectCommand.CommandText;
				string whereClause = GetWhereClause(selectStatement);
				CommandDescriptor command = null;
				if (drs == DataRowState.Added)
				{
					command = GetCommand(drs, dataTable.TableName, str1.ToString(), str2.ToString());
				}
				else
				{
					command = GetCommand(drs, dataTable.TableName, str1.ToString() + " and " + whereClause, str2.ToString());
				}
				commands.Add(command);
            }
            else
            {
                //Get all the tables from the JOIN statement and the corresponding columns
                Dictionary<string, List<DataColumn>> columnsGroupedByTable = new Dictionary<string, List<DataColumn>>();
                for (int columnIndex = 0; columnIndex < colCount; columnIndex++)
                {
                    Field columnField = dataTable.Fields(columnIndex);
                    if (string.IsNullOrEmpty(columnField.BaseTableName))
                    {
                        continue;
                    }
                    if (!columnsGroupedByTable.ContainsKey(columnField.BaseTableName))
                    {
                        columnsGroupedByTable.Add(columnField.BaseTableName, new List<DataColumn>());
                    }
                    columnsGroupedByTable[columnField.BaseTableName].Add(dataTable.Columns[columnIndex]);
                }
                //On each data row, separate statements must be created for each individual table used in the join statement
                foreach (KeyValuePair<string, List<DataColumn>> tableWithColumns in columnsGroupedByTable)
                {
                    // generate command if row is new or deleted
                    bool buildStatement = (drs == DataRowState.Deleted || drs == DataRowState.Added);
                    if (!buildStatement)
                    {
                        // also generate command if row is modified and at least one column is changed (entire row is modified but not all tables in case of a join are modified!)
                        foreach (DataColumn col in tableWithColumns.Value)
                        {
                            int columnIndex = dataTable.Columns.IndexOf(col);
                            if (!dataRow[columnIndex].Equals(dataRow[columnIndex, DataRowVersion.Original]))
                            {
                                buildStatement = true;
                                break;
                            }
                        }
                    }

                    if (!buildStatement)
                    {
                        continue;
                    }

                    StringBuilder str1 = new StringBuilder();
                    StringBuilder str2 = null;
                    if (drs != DataRowState.Deleted)
                    {
                        str2 = new StringBuilder();
                    }
                    foreach (DataColumn col in tableWithColumns.Value)
                    {
                        int columnIndex = dataTable.Columns.IndexOf(col);
                        Field columnField = dataTable.Fields(columnIndex);
                        int columnIndexInList = tableWithColumns.Value.IndexOf(col);
                        if (drs == DataRowState.Deleted || drs == DataRowState.Modified)
                        {
                            //A column of datatype ntext cannot be used in a where clause by comparing it with a varchar
                            //a cast must be used for this operation
                            string beginCast = "";
                            string endCast = "";
                            if (columnField.DataTypeName == "ntext")
                            {
                                beginCast = "Cast(";
                                endCast = " as nvarchar(max))";
                            }

                            str1.Append(beginCast + columnField.BaseColumnName + endCast + FormatValue(isOracleConnection, col.DataType, dataRow[columnIndex, DataRowVersion.Original], true));

                            if (columnIndexInList < tableWithColumns.Value.Count - 1)
                            {
                                str1.Append(" and ");
                            }
                        }
                        if (drs == DataRowState.Modified && !dataTable.Columns[columnIndex].AutoIncrement)
                        {
                            //A column might have been selected twice. Do not add the same column twice
                            //CHE: ignore ROWID in update statement for Oracle
                            if (!str2.ToString().Contains(columnField.BaseColumnName + "=") && !(isOracleConnection && columnField.BaseColumnName.ToUpper() == "ROWID"))
                            {
                                str2.Append(columnField.BaseColumnName + "=" + FormatValue(isOracleConnection, col.DataType, dataRow[columnIndex]));
                                if (columnIndexInList < tableWithColumns.Value.Count - 1)
                                {
                                    str2.Append(", ");
                                }
                            }
                        }
                        if (drs == DataRowState.Added && !dataTable.Columns[columnIndex].AutoIncrement)
                        {
                            str1.Append(columnField.BaseColumnName);
                            str2.Append(FormatValue(isOracleConnection, col.DataType, dataRow[columnIndex]));
                            if (columnIndexInList < tableWithColumns.Value.Count - 1)
                            {
                                str1.Append(", ");
                                str2.Append(", ");
                            }
                        }
					}

					CommandDescriptor command = GetCommand(drs, tableWithColumns.Key, str1.ToString(), str2.ToString());
					commands.Add(command);
                }
            }
			return ExecuteCommands(dataTable, commands);
        }

		private static int ExecuteCommands(DataTable dataTable, List<CommandDescriptor> commands)
		{
			DbCommand com = null;
			DbConnection conn = dataTable.ActiveConnection();

			if (conn is SqlConnection)
			{
				com = new SqlCommand();
			}
			else if (conn is OleDbConnection)
			{
				com = new OleDbCommand();
			}


            com.Connection = dataTable.ActiveConnection();
			com.Transaction = com.Connection.GetTransaction();

			bool multipleRows = false;
			int affectedRows = 0;

			foreach (CommandDescriptor item in commands)
			{
				if (!string.IsNullOrEmpty(item.whereClause) && !string.IsNullOrEmpty(item.tableName))
				{
					com.CommandText = string.Format("select count(*) from {0} where {1}", item.tableName, item.whereClause);
					affectedRows = Convert.ToInt32(com.ExecuteScalar());
					if (affectedRows > 1)
					{
						multipleRows = true;
						break;
					}
				}
			}

			if (multipleRows)
			{
				//only one row should be affected; if not, update failed
				return -1;
			}

			affectedRows = 0;
			foreach (CommandDescriptor item in commands)
			{
				com.CommandText = item.command;
				int comAffectedRows = com.ExecuteNonQuery();
				if (comAffectedRows > 0)
				{
					affectedRows = comAffectedRows;
				}
			}

			return affectedRows;
		}

        //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
        private static string FormatValue(bool isOracleConnection, Type type, object p, bool compare = false)
        {
            if (p == DBNull.Value)
            {
                return (compare ? " is " : "") + "null";
            }

            string str = (compare ? " = " : "");

            if (type == typeof(string))
            {
                return str + String.Format("'{0}'", FCConvert.ToString(p).Replace("'", "''"));
            }

            if (type == typeof(bool))
            {
                return str + Convert.ToInt32(p).ToString();
            }
            //PJU: the case for Single is e.g. using a OleDbConnection with the Sql-Server DB-Column-Type real which is in .NET Single ( Value of 2.5 will be 2,5 which may cause an error if it's not corretly formated)
			if (type == typeof(decimal) || type == typeof(double) || type == typeof(Single))
            {
                return str + FCConvert.ToString(p, System.Globalization.CultureInfo.InvariantCulture);
            }

            if (type == typeof(DateTime))
            {
                if (isOracleConnection)
                {
                    return str + "TO_DATE('" + fecherFoundation.Strings.Format(p, "MM/dd/yyyy hh:mm:ss AM/PM") + "', 'MM/dd/yyyy HH:MI:SS AM')";
                }
                else
                {
                    // BAN: Use CONVERT to cast string to DATETIME, 120 is the standard of conversion used, in this case ODBC Canonical
                    // Ex.: SQL Statement: SELECT CONVERT(VARCHAR(19), GETDATE(), 120) will give the output: 1972-01-01 13:42:24
                    return str + String.Format("CONVERT(DATETIME, '{0}', 120)", System.DateTime.Parse(p.ToString()).ToString("yyyy-MM-dd HH:mm:ss"));
                }
            }

            return str + p.ToString();
        }

        /// <summary>
        /// Replace the indexer for Recordset
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="FieldName"></param>
        /// <returns></returns>
        public static Field Fields(this DataTable objDataTable, string FieldName)
        {

            //IPI: even if there are no rows, the DataTable column can be retrieved
            //if (EOF(objDataTable))
            //{
            //    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
            //}

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            //Get the value of the field in the index position.
            //IPI: retrieve the Field from the Fields list
            // setting the Field's Value will change the RowState to RowModified
            //Field objReturnField = new Field(objDataTable, objDataTable.Columns[FieldName], objDataTable.Rows[objDataTableDefinitions.Index - 1][FieldName]);
            //objReturnField.Value = objDataTable.Rows[objDataTableDefinitions.Index - 1][FieldName];
            Field objReturnField = objDataTableDefinitions.Fields.GetField(FieldName);


            return objReturnField; // objDataTable.Rows[objDataTableDefinitions.Index - 1][FieldName];

        }


        //BAN: implemented Fields method
        //public static List<Field> Fields(this DataTable objDataTable)
        public static FieldsExtension Fields(this DataTable objDataTable)
        {
            //IPI: retrieve the Fields list
            //FieldsExtension fields = new FieldsExtension(objDataTable);
            //return fields;
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            return objDataTableDefinitions.Fields;
        }

        //CHE: cancel update for current row
        public static void CancelUpdate(this DataTable objDataTable)
        {

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            //CHE: clear new rows
            if (objDataTable.HasNewRows())
            {
                objDataTableDefinitions.NewRows.Clear();
            }

            //CHE: call RejectChanges for both connected and disconnected DataTable
            if (objDataTableDefinitions.DataAdapter != null && objDataTableDefinitions.DataAdapter.SelectCommand != null)
            {
                objDataTable.RejectChanges();
            }

            //CHE: cancel for clones too when calling CancelUpdate
            DataTableDefinitions.SynchronizeClones(objDataTable, Synchronization.CancelRow);
        }

        //DSE Cancel all pending updates 
        public static void CancelBatch(this DataTable objDataTable)
        {
            objDataTable.CancelUpdate();
        }


        /// <summary>
        /// Replace the indexer for Recordset
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="FieldName"></param>
        /// <returns></returns>
        public static Field Fields(this DataTable objDataTable, int Index)
        {

            //IPI: even if there are no rows, the DataTable column can be retrieved
            //if (EOF(objDataTable))
            //{
            //    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
            //}

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            //Get the value of the field in the index position.
            //IPI: retrieve the Field from the Fields list
            // setting the Field's Value will change the RowState to RowModified
            //Field objReturnField = new Field(objDataTable, objDataTable.Columns[Index], objDataTable.Rows[objDataTableDefinitions.Index - 1][Index]);
            //objReturnField.Value = objDataTable.Rows[objDataTableDefinitions.Index - 1][Index];
            Field objReturnField = objDataTableDefinitions.Fields.GetField(Index);


            return objReturnField; // objDataTable.Rows[objDataTableDefinitions.Index - 1][FieldName];

            //return objDataTable.Rows[objDataTableDefinitions.Index - 1][Index];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="FieldName"></param>
        /// <returns></returns>
        public static void Fields(this DataTable objDataTable, string FieldName, object Value)
        {

            if (EOF(objDataTable))
            {
                throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
            }

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            //Get the value of the field in the index position.
            objDataTable.Rows[objDataTableDefinitions.Index - 1][FieldName] = (Object.Equals(Value, null) ? (object)DBNull.Value : Value);//zina


        }




        /// <summary>
        /// ActiveConnection implementation
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static void ActiveConnection(this DataTable objDataTable, DbConnection objConnection)
        {

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (objDataTableDefinitions.DataAdapter != null)
            {
                if (objDataTableDefinitions.DataAdapter.InsertCommand != null)
                    objDataTableDefinitions.DataAdapter.InsertCommand.Connection = objConnection;

                if (objDataTableDefinitions.DataAdapter.UpdateCommand != null)
                    objDataTableDefinitions.DataAdapter.UpdateCommand.Connection = objConnection;

                if (objDataTableDefinitions.DataAdapter.SelectCommand != null)
                {
                    objDataTableDefinitions.DataAdapter.SelectCommand.Connection = objConnection;
                    //CHE: set transaction if the case
                    if (objConnection != null)
                    {
                        objDataTableDefinitions.DataAdapter.SelectCommand.Transaction = objConnection.GetTransaction();
                    }
                }

                //IPI: in case of disconnected Recordset, null values are allowed
                if (objConnection == null)
                {
                    foreach (DataColumn col in objDataTable.Columns)
                    {
                        col.AllowDBNull = true;
                    }
                }
            }

        }


        /// <summary>
        /// ActiveConnection implementation
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static DbConnection ActiveConnection(this DataTable objDataTable)
        {

            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (objDataTableDefinitions.DataAdapter != null)
            {
                if (objDataTableDefinitions.DataAdapter.InsertCommand != null)
                    return objDataTableDefinitions.DataAdapter.InsertCommand.Connection;

                if (objDataTableDefinitions.DataAdapter.UpdateCommand != null)
                    return objDataTableDefinitions.DataAdapter.UpdateCommand.Connection;

                if (objDataTableDefinitions.DataAdapter.SelectCommand != null)
                    return objDataTableDefinitions.DataAdapter.SelectCommand.Connection;

            }

            return null;
        }

        /// <summary>
        /// Extension: Returns the page size of the RecordSet
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static int PageSize(this DataTable objDataTable)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            return objDataTableDefinitions.PageSize;
        }

        /// <summary>
        /// Extension: set the page size of the RecordSet
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <param name="intPageSize"></param>
        public static void PageSize(this DataTable objDataTable, int intPageSize)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            objDataTableDefinitions.PageSize = intPageSize;
        }

        public static void CursorLocation(this DataTable objDataTable, CursorLocationEnum CursorLocation)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            objDataTableDefinitions.CursorLocation = CursorLocation;
        }
        public static CursorLocationEnum CursorLocation(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            return objDataTableDefinitions.CursorLocation;
        }


        public static void LockType(this DataTable objDataTable, LockTypeEnum LockType)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            objDataTableDefinitions.LockType = LockType;
        }
        public static void CursorType(this DataTable objDataTable, CursorTypeEnum CursorType)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            objDataTableDefinitions.CursorType = CursorType;
        }
        public static CursorTypeEnum CursorType(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            return objDataTableDefinitions.CursorType;
        }
        public static LockTypeEnum LockType(this DataTable objDataTable)
        {
            //get the instance of the definitions class
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            return objDataTableDefinitions.LockType;
        }

        public static void Close(this DataTable dataTable)
        {
            if (dataTable == null)
            {
                return;
            }

            //if current row is marked for deletion do not update
            DataRow dataRow = GetCurrentRow(dataTable);
            if (dataRow != null && dataRow.RowState != DataRowState.Deleted)
            {
                //update the current record
                dataTable.Update();
            }

            dataTable.Clear();
            dataTable.Columns.Clear();

            //CHE: dispose also the adapter (DataAdapter was created in OpenDataTable)
            // "Cannot open any more tables" exception was received when calling objDataTableDefinitions.DataAdapter.Update(new DataRow[] { dataRow });
            // if many DataTables were opened and closed on MDB OleDBConnection
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(dataTable);
            if (objDataTableDefinitions.DataAdapter != null)
            {
                objDataTableDefinitions.DataAdapter.Dispose();
            }
            objDataTableDefinitions.DataAdapter = null;

            //CHE: clear fields also when closing recordset
            objDataTableDefinitions.ClearFields();

            //CHE: dispose also the object when closing the recordset to avoid memory leak
            dataTable.Dispose();
        }

        //BAN: returns the specified DataTable as a string
        public static string GetString(this DataTable objDataTable, int records = -1, string separator = ";")
        {

            StringBuilder str = new StringBuilder();
            int limit = records == -1 ? objDataTable.Rows.Count : records;
            for (int count = 0; count < limit; count++)
            {
                DataRow dr = objDataTable.Rows[count];
                for (int cells = 0; cells < objDataTable.Columns.Count; cells++)
                {
                    str.Append(dr[cells] + separator);
                }

                str.Append(System.Environment.NewLine);
            }

            string txt = str.ToString();
            txt = txt.Replace(separator + System.Environment.NewLine, System.Environment.NewLine);


            return txt;
        }

        //BAN: RowChangeEvent is not fired when changing a cell value and moving on the same row. Need to attach the ColumnChange event
        //handler in order to raise RowChangeEvent
        public static DataRowChangeEventHandler GetRowChangedEventHandler(this DataTable objDataTable)
        {

            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            return objDataTableDefinitions.RowChangeEventHandler;
        }
        public static void SetRowChangedEventHandler(this DataTable objDataTable, DataRowChangeEventHandler rowChangeEventHandler)
        {

            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            objDataTableDefinitions.RowChangeEventHandler = rowChangeEventHandler;
        }

        //CHE: explicitly remove clone from collection when object is disposed
        public static void RemoveClone(this DataTable objDataTable, DataTable clone)
        {

            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            objDataTableDefinitions.ClonedTables.Remove(clone);
        }

        public static DataTable CloneRows(this DataTable objDataTable, LockTypeEnum LockType = LockTypeEnum.adLockUnspecified)
        {
            return CloneRows(objDataTable, LockType, true, true);
        }

        //BAN
        internal static DataTable CloneRows(this DataTable objDataTable, LockTypeEnum LockType = LockTypeEnum.adLockUnspecified, bool clone = true, bool bidirectionalCloning = false)
        {

            //CHE: in VB6 clone method creates two recordset objects that point to the same recordset. 
            //This feature allows you to change the current record in the clone, 
            //while keeping the current record same in the original recordset object or vice versa.
            //e.g. changing position in clone will not affect position in original (MoveNext etc)

            //!!! This solution does not support this behaviour described in ADO Clone method documentation:
            //          Changes you make to one Recordset object are visible in all of its clones regardless of cursor type. 
            //          However, after you execute Requery on the original Recordset, the clones will no longer be synchronized to the original.


            if (objDataTable == null)
            {
                return null;
            }

            //DataTable result = objDataTable;
            DataTable result = objDataTable.Copy();

            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

            if (objDataTableDefinitions.ClonedTables == null)
            {
                objDataTableDefinitions.ClonedTables = new List<DataTable>();
            }

            Field resultIdentityColumn = null;

            //CHE;CNA: copy also custom properties
            foreach (DataTableExtension.Field f in objDataTable.Fields())
            {
                Field resultField = result.Fields(f.ColumnName);
                //IPI: IsAliased must be set first, otherwise setting BaseColumnName will generate an incorrect result for result.Fields(f.ColumnName)
                resultField.IsAliased = f.IsAliased;
                resultField.DataTypeName = f.DataTypeName;
                resultField.DefinedSize = f.DefinedSize;
                resultField.BaseTableName = f.BaseTableName;
                resultField.BaseColumnName = f.BaseColumnName;
                resultField.IsIdentity = f.IsIdentity;
                if (f.IsIdentity)
                {
                    resultIdentityColumn = resultField;
                }

                //IPI: to be able to change values in the clone table, all columns must not be readonly
                result.Columns[f.ColumnName].ReadOnly = false;
            }

            result.Filter("");

            //Unspecified type of lock. Clones inherits lock type from the original Recordset.
            if (LockType == LockTypeEnum.adLockUnspecified)
            {
                LockType = objDataTable.LockType();
            }

            result.LockType(LockType);

            //Attach all the rows from the copy DataTable to the rows in the source DataTable, because copy() 
            //copies the structure and all the data in the source DataTable but does not generate the TableNewRow event
            //in order to set the paired rows
            //APE: if the source data table contains changes that where not accepted using AcceptChanges
            //the new data will not be transfered to the cloned table. The modified values need to be copied to the resulting cloned table
            foreach (DataRow dr in objDataTable.Rows)
            {
                if (dr.RowState == DataRowState.Modified)
                {
                    DataRow clonedRow = result.Rows[objDataTable.Rows.IndexOf(dr)];
                    for (int counter = 0; counter < objDataTable.Columns.Count; counter++)
                    {
                        if (clonedRow[counter] != dr[counter])
                        {
                            clonedRow[counter] = dr[counter];
                        }
                    }
                }

                DataRow resultDataRow = result.Rows[objDataTable.Rows.IndexOf(dr)];
                dr.PairRow(resultDataRow);
            }

            //IPI: all rows in the DefaultView are also in the Rows collection except of a possible new row
            if (objDataTable.DefaultView.Count > 0 && objDataTable.DefaultView[objDataTable.DefaultView.Count - 1].IsNew)
            {
                DataRowView drView = objDataTable.DefaultView[objDataTable.DefaultView.Count - 1];
                {
                    DataRowView dr = result.DefaultView.AddNew();
                    dr.Row.ItemArray = drView.Row.ItemArray.Clone() as object[];
                    drView.Row.PairRow(dr.Row);
                }
            }

            DataTableDefinitions resultDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(result);
            resultDataTableDefinitions.IdentityColumn = resultIdentityColumn;

            //clone also NewRows
            if (objDataTableDefinitions.NewRows != null && objDataTableDefinitions.NewRows.Count > 0)
            {
                if (resultDataTableDefinitions.NewRows == null)
                {
                    resultDataTableDefinitions.NewRows = new List<DataRow>();
                }

                //CHE: collection is modified in RowChanging, use for instead of foreach
                //foreach (DataRow row in objDataTableDefinitions.NewRows)
                for (int i = 0; i < objDataTableDefinitions.NewRows.Count; i++)
                {
                    DataRow row = objDataTableDefinitions.NewRows[i];
                    DataRow dr = result.NewRow();
                    dr.ItemArray = row.ItemArray.Clone() as object[];
                    resultDataTableDefinitions.NewRows.Add(dr);
                    row.PairRow(dr);
                }
            }

            if (clone)
            {
                if (bidirectionalCloning)
                {
                    objDataTableDefinitions.ClonedTables.Add(result);
                }

                //IPI: all clones must keep reference to each other (any rows operation must be reflected in all clones)
                if (resultDataTableDefinitions.ClonedTables == null)
                {
                    resultDataTableDefinitions.ClonedTables = new List<DataTable>();
                }
                resultDataTableDefinitions.ClonedTables.Add(objDataTable);
            }

            //CHE: rows marked for deletion should not be copied when creating clone
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (result.Rows[i].RowState == DataRowState.Deleted)
                {
                    result.Rows[i].AcceptChanges();
                    //DSE: adjust index because current row is removed from the Rows list
                    --i;
                }
            }

            //IPI: after the values in the clone table have been synchronized, must adjust the ReadOnly property as in the source DataTable
            foreach (DataColumn col in result.Columns)
            {
                col.ReadOnly = objDataTable.Columns[col.ColumnName].ReadOnly;
            }

            result.SetDataAdapter(objDataTableDefinitions.DataAdapter);


            return result;
        }

        //IPI
        public static ConnectionState State(this DataTable objDataTable)
        {
            if (objDataTable.ActiveConnection() != null)
            {
                return objDataTable.ActiveConnection().State;
            }
            else
            {
                return ((objDataTable.Rows.Count == 0 && objDataTable.Columns.Count == 0) ? ConnectionState.Closed : ConnectionState.Open);
            }
        }

        /// <summary>
        /// The Status property on a Recordset object indicates the status of the current record with respect to batch updates or other bulk operations
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static DataRowState Status(this DataTable objDataTable, bool isEOFandBOFchecked = false)
        {
            //CHE: for deleted CurrentRow will throw exception
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            if (objDataTableDefinitions.Deleted)
            {
                return DataRowState.Deleted;
            }
            else if (isEOFandBOFchecked || (!isEOFandBOFchecked && !EOF(objDataTable) && !BOF(objDataTable)))
            {
                //CHE: with ADO VB6 recordset there is no Detached status, return as Added status
                if (objDataTable.CurrentRow(true).RowState == DataRowState.Detached)
                {
                    return DataRowState.Added;
                }
                return objDataTable.CurrentRow(true).RowState;
            }
            return DataRowState.Unchanged;
        }

        public static void SetParentGrid(this DataTable objDataTable, DataGridView parentGrid)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);
            objDataTableDefinitions.ParentGrid = parentGrid;
        }

        private static bool FilePathHasInvalidChars(string path)
        {
            bool ret = false;
            if (!string.IsNullOrEmpty(path))
            {
                //CHE: avoid exception to improve performance
                string lowerPath = path.Trim().ToLower();
                if (lowerPath.StartsWith("select ") || lowerPath.StartsWith("update ") || lowerPath.StartsWith("insert ") || lowerPath.StartsWith("delete ") ||
                    lowerPath.StartsWith("select\r\n") || lowerPath.StartsWith("update\r\n") || lowerPath.StartsWith("insert\r\n") || lowerPath.StartsWith("delete\r\n"))
                {
                    return true;
                }
                try
                {
                    // Careful!
                    //    Path.GetDirectoryName("C:\Directory\SubDirectory")
                    //    returns "C:\Directory", which may not be what you want in
                    //    this case. You may need to explicitly add a trailing \
                    //    if path is a directory and not a file path. As written, 
                    //    this function just assumes path is a file path.
                    string fileName = System.IO.Path.GetFileName(path);
                    string fileDirectory = System.IO.Path.GetDirectoryName(path);
                    string fullPath = Path.GetFullPath(path);

                    // we don't need to do anything else,
                    // if we got here without throwing an 
                    // exception, then the path does not
                    // contain invalid characters
                }
                catch (ArgumentException)
                {
                    // Path functions will throw this 
                    // if path contains invalid chars
                    ret = true;
                }
                catch (PathTooLongException)
                {
                    // Path functions will throw this 
                    // if path is too long
                    ret = true;
                }
            }
            return ret;
        }

        /// <summary>
        /// this is the Adodb filed replacement
        /// </summary>
        [Serializable]
        public class Field : IComparable
        {
            private DataTable mobjDataTable;

            //IPI
            public Field(DataTable objDataTable, DataColumn objColumn)
            {
                this.mobjDataTable = objDataTable;
                this.Column = new SerializableColumn(objColumn);
            }

            //CHE: implement IComparable CompareTo to provide default sort order
            public int CompareTo(object obj)
            {
                Field c = (Field)obj;

                if (this.Value == DBNull.Value)
                {
                    if (c.Value == DBNull.Value)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }

                if (c.Value == DBNull.Value)
                {
                    return 1;
                }
                else
                {
                    switch ((SqlDbType)this.Type)
                    {
                        case SqlDbType.BigInt:
                        case SqlDbType.Bit:
                        case SqlDbType.Int:
                        case SqlDbType.SmallInt:
                        case SqlDbType.TinyInt:
                            {
                                long a = Convert.ToInt64(this.Value);
                                long b = Convert.ToInt64(c.Value);
                                return a.CompareTo(b);
                            }
                        case SqlDbType.Char:
                        case SqlDbType.NVarChar:
                        case SqlDbType.NText:
                        case SqlDbType.Text:
                        case SqlDbType.VarChar:
                            {
                                string a = FCConvert.ToString(this.Value);
                                string b = FCConvert.ToString(c.Value);
                                return String.Compare(a, b);
                            }
                        case SqlDbType.Date:
                        case SqlDbType.DateTime:
                        case SqlDbType.DateTime2:
                        case SqlDbType.SmallDateTime:
                        case SqlDbType.Time:
                        case SqlDbType.Timestamp:
                            {
                                DateTime a = Convert.ToDateTime(this.Value);
                                DateTime b = Convert.ToDateTime(c.Value);
                                return a.CompareTo(b);
                            }
                        case SqlDbType.Decimal:
                        case SqlDbType.Float:
                        case SqlDbType.Real:
                            {
                                Double a = Convert.ToDouble(this.Value);
                                Double b = Convert.ToDouble(c.Value);
                                return a.CompareTo(b);
                            }
                    }
                }

                return 0;
            }

            [Serializable]
            public class SerializableColumn
            {

                public SerializableColumn(DataColumn objColumn)
                {
                    this.ColumnName = objColumn.ColumnName;
                    this.AllowDBNull = objColumn.AllowDBNull;
                    this.Table = objColumn.Table;
                    this.ReadOnly = objColumn.ReadOnly;


                }
                public string ColumnName { get; set; }
                public bool AllowDBNull { get; set; }
                public bool ReadOnly { get; set; }
                public System.Data.DataTable Table { get; set; }


            }

            public string BaseTableName
            {
                get;
                set;
            }

            public string BaseColumnName
            {
                get;
                set;
            }

            public bool IsAliased
            {
                get;
                set;
            }

            public bool IsIdentity
            {
                get;
                set;
            }
            //BAN
            //User-defined operator -
            public static Double operator -(Field field1, Field field2)
            {
                return (Convert.IsDBNull(field1.Value) ? 0 : Convert.ToDouble(field1.Value)) - (Convert.IsDBNull(field2.Value) ? 0 : Convert.ToDouble(field2.Value));
            }

            //BAN
            //User-defined operator -
            /// <summary>
            /// + operator retuns an object, Conversion is required if the result is assigned to a known type
            /// In VB6 the operation is evaluated, and the result is converted to the required type
            /// "0001" + "0002" will return "00010002" if result is assigned to a String or 10002 if result is assigned to a number
            /// "0001" + 2 will return "3" if result is assigned to a String or 3 if result is assigned to a number
            /// 1 + "0002" will return "3" if result is assigned to a String or 3 if result is assigned to a number
            /// 1 + 2 will return "3" if result is assigned to a String or 3 if result is assigned to a number
            /// </summary>
            /// <param name="field1"></param>
            /// <param name="field2"></param>
            /// <returns></returns>
            public static object operator +(Field field1, Field field2)
            {
                //SBE: if booth operands are one of the string SqlDbType(Char/NChar/VarChar/NVarChar/Text/NText), operator will return string concatenation
                if ((field1.Type == Convert.ToInt32(SqlDbType.Char) || field1.Type == Convert.ToInt32(SqlDbType.NChar)
					|| field1.Type == Convert.ToInt32(SqlDbType.VarChar) || field1.Type == Convert.ToInt32(SqlDbType.NVarChar)
					|| field1.Type == Convert.ToInt32(SqlDbType.Text) || field1.Type == Convert.ToInt32(SqlDbType.NText)
                    && (field2.Type == Convert.ToInt32(SqlDbType.Char) || field2.Type == Convert.ToInt32(SqlDbType.NChar)
					|| field2.Type == Convert.ToInt32(SqlDbType.VarChar) || field2.Type == Convert.ToInt32(SqlDbType.NVarChar)
					|| field2.Type == Convert.ToInt32(SqlDbType.Text) || field2.Type == Convert.ToInt32(SqlDbType.NText))))

				{
                    return FCConvert.ToString(field1.Value) + FCConvert.ToString(field2.Value);
                }
                else
                {
                    return (Convert.IsDBNull(field1.Value) ? 0 : Convert.ToDouble(field1.Value)) + (Convert.IsDBNull(field2.Value) ? 0 : Convert.ToDouble(field2.Value));
                }
            }

            // User-defined conversion from Field to double
            public static implicit operator double(Field field)
            {
                if (Convert.IsDBNull(field.Value)) return 0;
                return Convert.ToDouble(field.Value);
            }

            // User-defined conversion from Field to decimal
            public static implicit operator decimal(Field field)
            {
                if (Convert.IsDBNull(field.Value)) return 0;
                return Convert.ToDecimal(field.Value);
            }

            // User-defined conversion from Field to int
            public static implicit operator int(Field field)
            {
                if (Convert.IsDBNull(field.Value)) return 0;
                return Convert.ToInt32(field.Value);
            }

            // User-defined conversion from Field to short
            public static implicit operator short(Field field)
            {
                if (Convert.IsDBNull(field.Value)) return 0;
                return Convert.ToInt16(field.Value);
            }
            // User-defined conversion from Field to DateTime
            public static implicit operator DateTime(Field field)
            {
                if (Convert.IsDBNull(field.Value)) return DateTime.Parse("30/12/1899 00:00:00");
                return Convert.ToDateTime(field.Value);
            }

            // User-defined conversion from Field to string
            public static implicit operator string(Field field)
            {
                if (Convert.IsDBNull(field.Value)) return "";
                return FCConvert.ToString(field.Value);
            }

            //IPI
            // User-defined conversion from Field to bool
            public static implicit operator bool(Field field)
            {
                if (Convert.IsDBNull(field.Value)) return false;
                return Convert.ToBoolean(field.Value);
            }

            public int DefinedSize
            {
                get;
                set;
            }

            public int ActualSize
            {
                get
                {
                    if (EOF(mobjDataTable))
                    {
                        throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                    }
                    //Sys.LogNotImplemented();
                    return 0;
                }
            }

            public int Precision { get; set; }

            //CHE: in UpdateBatch before calling FillSchema replace null field with default value from SQL server if field does not accept null
            public bool AllowDBNull { get; set; }

            public string DefaultValue { get; set; }

            public int NumericScale { get; set; }

            public object OriginalValue
            {
                get
                {
                    if (EOF(mobjDataTable))
                    {
                        throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                    }

                    if (mobjDataTable == null)
                    {
                        return DBNull.Value;
                    }

                    DataRow currentRow = mobjDataTable.CurrentRow();

                    if (currentRow == null)
                    {
                        return DBNull.Value;
                    }

                    //BAN: if the DataRow does not contain an original version, trying to get the original value of a cell will result
                    //in an exception. Check if row contains original version before returning original value
                    if (currentRow.RowState != DataRowState.Added && currentRow.HasVersion(DataRowVersion.Original))
                    {
                        return currentRow[this.ColumnName, DataRowVersion.Original];
                    }
                    else
                    {
                        //APE: - in VB6 if the row does not contain an original value, and the row was added, VB6 returns null
                        //If the column accepts DBNull value, return DBNull, otherwise, return column default value for its type 
                        if (this.AllowDBNull)
                        {
                            return DBNull.Value;
                        }
                        else
                        {
                            DataColumn col = this.mobjDataTable.Columns[this.ColumnName];
                            if (col.DataType == typeof(System.String))
                            {
                                return string.Empty;
                            }
                            else if (col.DataType.IsValueType)
                            {
                                return Activator.CreateInstance(col.DataType);
                            }
                        }
                        return currentRow[this.ColumnName, DataRowVersion.Default];
                    }
                }
                set
                {
                    //Sys.LogNotImplemented();
                }
            }

            public object UnderlyingValue
            {
                get
                {
                    if (Column == null || BOF(Column.Table) || EOF(Column.Table) || Column.Table.Status(true) == DataRowState.Deleted)
                    {
                        throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                    }
                    DataRow row = Column.Table.CurrentRow(true);
                    //BAN - if the DataRow has a Detached state, the OriginalValue must be DBNull, because this means that the row was added by the user or from inside the code and it is not a row previously retrieved from the database
                    if (row.RowState == DataRowState.Detached)
                    {
                        return DBNull.Value;
                    }
                    if (row.RowState == DataRowState.Modified && row.HasVersion(DataRowVersion.Original))
                    {
                        return CustomFormat(Column, row[Column.ColumnName, DataRowVersion.Original]);
                    }
                    if (row.HasVersion(DataRowVersion.Default))
                    {
                        return CustomFormat(Column, row[Column.ColumnName, DataRowVersion.Default]);
                    }
                    else
                    {
                        return CustomFormat(Column, row[Column.ColumnName]);
                    }
                }
                set
                {
                    //Sys.LogNotImplemented();
                }
            }

            /// <summary>
            /// property neeeded to avoid exception when comparing Fields(..).Value with some value such as integer or boolean (cannot convert DBNull to...)
            /// </summary>
            public object ValueNullSafe
            {
                get
                {
                    if (this.Value != DBNull.Value)
                    {
                        return this.Value;
                    }
                    if (!string.IsNullOrEmpty(this.ColumnName))
                    {
                        DataColumn col = this.mobjDataTable.Columns[this.ColumnName];
                        if (col.DataType == typeof(System.String))
                        {
                            return string.Empty;
                        }
                        else if (col.DataType.IsValueType)
                        {
                            return Activator.CreateInstance(col.DataType);
                        }
                    }
                    return this.Value;
                }
            }

            /// <summary>
            /// get or set the value of the field
            /// </summary>
            public object Value
            {
                get
                {
                    //SBE: if RowState is Deleted Exception is thrown
                    if (Column == null || BOF(Column.Table) || EOF(Column.Table) || Column.Table.Status(true) == DataRowState.Deleted)
                    {
                        throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                    }
                    return CustomFormat(Column, Column.Table.CurrentRow(true)[Column.ColumnName]);
                }
                set
                {
                    DataTable dt = Column.Table;

                    if (Column == null || EOF(dt) || Column.ReadOnly) return;
                    if (this.Type == Convert.ToInt32(SqlDbType.Bit) && value != null)
                    {
                        dt.CurrentRow()[Column.ColumnName] = BoolFromString(value.ToString());
                    }
                    else
                    {
                        DataRow dr = dt.CurrentRow();

                        //if (value == "")
                        //{
                        //    switch (this.Type)
                        //    {
                        //        case Convert.ToInt32(SqlDbType.Char:
                        //        case Convert.ToInt32(SqlDbType.VarChar:
                        //        case Convert.ToInt32(SqlDbType.NChar:
                        //        case Convert.ToInt32(SqlDbType.NVarChar:
                        //            {
                        //                value = new string(' ', this.DefinedSize); 
                        //                break;
                        //            }
                        //    }
                        //}

                        //CNA: field does not accepts null, use DBNull instead
                        //dr[Column.ColumnName] = value;
                        //dr[Column.ColumnName] = value != null ? value : DBNull.Value;
                        // JSP: If Value is null then set Value = dbnull
                        if (value == null)
                        {
                            dr[Column.ColumnName] = DBNull.Value;
                        }
                        else
                        {
                            //CHE: if field is NChar (adWChar) when setting value fill with empty spaces if DefindeSize is set
                            if (value != null && DefinedSize > 0 && this.Type == Convert.ToInt32(SqlDbType.NChar) && value.ToString().Length < DefinedSize)
                            {
                                dr[Column.ColumnName] = value + new string(' ', DefinedSize - value.ToString().Length);
                            }
                            else if (dt.Columns[Column.ColumnName].DataType == typeof(DateTime))
                            {
                                string cellValue = FCConvert.ToString(value);
                                if (string.IsNullOrEmpty(cellValue))
                                {
                                    dr[Column.ColumnName] = DBNull.Value;
                                }
                                else
                                {
                                    DateTime dateTime = Constants.MinDate;
                                    // Try to get time part
                                    if (TryParseTime(ref cellValue, out dateTime))
                                    {
                                        // time part was present and removed from input string
                                        if (!string.IsNullOrEmpty(cellValue))
                                        {
                                            // time part was preceeded by date part, try to parse date part
                                            DateTime dateValue = Constants.MinDate;
                                            if (DateTime.TryParse(cellValue, out dateValue))
                                            {
                                                // date part parsed correctly, build final date with time
                                                dateTime = new DateTime(dateValue.Year, dateValue.Month, dateValue.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // time part was not present, build just date
                                        DateTime.TryParse(cellValue, out dateTime);
                                    }


                                    dr[Column.ColumnName] = dateTime;
                                }
                            }
                            //BAN: handle cases where dt.Fields("col").Value = enm.Member where enm is enum
                            else if (value.GetType().BaseType == typeof(Enum))
                            {
                                dr[Column.ColumnName] = Convert.ToInt32(value);
                            }
                            else if (dt.Columns[Column.ColumnName].DataType == typeof(int) && !Information.IsNumeric(value) && !Column.AllowDBNull)
                            {
                                dr[Column.ColumnName] = !string.IsNullOrEmpty(Convert.ToString(value)) ? Convert.ToInt32(value) : 0;
                            }
                            else if (dt.Columns[Column.ColumnName].DataType == typeof(int))
                            {
                                //DSE Cannot cast null and DBNull to int
                                dr[Column.ColumnName] = ((value != null) && (value != DBNull.Value)) ? (object)System.Convert.ToInt32(System.Convert.ToDouble(value)) : DBNull.Value;
                            }
                            else
                            {
                                dr[Column.ColumnName] = value != null ? value : DBNull.Value;
                            }
                        }
                    }
                }
            }

            private bool TryParseTime(ref string str, out DateTime dateTime)
            {
                string time_zone_r = @"(?:\s*(?'time_zone'UTC|GMT|CST|EST))?";

                Match m = Regex.Match(str, @"(?<=^\s*,?\s+|^\s*at\s*|^\s*[T\-]\s*)(?'hour'\d{2})\s*:\s*(?'minute'\d{2})\s*:\s*(?'second'\d{2})\s+(?'offset_sign'[\+\-])(?'offset_hh'\d{2}):?(?'offset_mm'\d{2})(?=$|[^\d\w])", RegexOptions.Compiled);
                if (!m.Success)
                {
                    //look for <date> [h]h:mm[:ss] [PM/AM] [UTC/GMT] 
                    m = Regex.Match(str, @"(?<=^\s*,?\s+|^\s*at\s*|^\s*[T\-]\s*)(?'hour'\d{1,2})\s*:\s*(?'minute'\d{2})\s*(?::\s*(?'second'\d{2}))?(?:\s*(?'ampm'AM|am|PM|pm))?" + time_zone_r + @"(?=$|[^\d\w])", RegexOptions.Compiled);
                }
                if (!m.Success)
                {
                    //look for [h]h:mm:ss [PM/AM] [UTC/GMT] <date>
                    m = Regex.Match(str, @"(?<=^|[^\d])(?'hour'\d{1,2})\s*:\s*(?'minute'\d{2})\s*(?::\s*(?'second'\d{2}))?(?:\s*(?'ampm'AM|am|PM|pm))?" + time_zone_r + @"(?=$|[\s,]+)", RegexOptions.Compiled);
                }
                if (!m.Success)
                {
                    //look for [h]h:mm:ss [PM/AM] [UTC/GMT] within <date>
                    m = Regex.Match(str, @"(?<=^|[^\d])(?'hour'\d{1,2})\s*:\s*(?'minute'\d{2})\s*(?::\s*(?'second'\d{2}))?(?:\s*(?'ampm'AM|am|PM|pm))?" + time_zone_r + @"(?=$|[\s,]+)", RegexOptions.Compiled);
                }
                if (!m.Success)
                {
                    //look for hh:mm:ss <UTC offset> 
                    m = Regex.Match(str, @"(?<=^|\s+|\s*T\s*)(?'hour'\d{2})\s*:\s*(?'minute'\d{2})\s*:\s*(?'second'\d{2})\s+(?'offset_sign'[\+\-])(?'offset_hh'\d{2}):?(?'offset_mm'\d{2})?(?=$|[^\d\w])", RegexOptions.Compiled);
                }
                if (!m.Success)
                {
                    //look for [h]h:mm[:ss] [PM/AM] [UTC/GMT]
                    m = Regex.Match(str, @"(?<=^|\s+|\s*T\s*)(?'hour'\d{1,2})\s*:\s*(?'minute'\d{2})\s*(?::\s*(?'second'\d{2}))?(?:\s*(?'ampm'AM|am|PM|pm))?" + time_zone_r + @"(?=$|[^\d\w])", RegexOptions.Compiled);
                }

                dateTime = DateTime.FromOADate(0);

                if (!m.Success)
                    return false;

                int hour = int.Parse(m.Groups["hour"].Value);
                if (hour < 0 || hour > 23)
                    return false;

                int minute = int.Parse(m.Groups["minute"].Value);
                if (minute < 0 || minute > 59)
                    return false;

                int second = 0;
                if (!string.IsNullOrEmpty(m.Groups["second"].Value))
                {
                    second = int.Parse(m.Groups["second"].Value);
                    if (second < 0 || second > 59)
                        return false;
                }

                if (string.Compare(m.Groups["ampm"].Value, "PM", true) == 0 && hour < 12)
                    hour += 12;
                else if (string.Compare(m.Groups["ampm"].Value, "AM", true) == 0 && hour == 12)
                    hour -= 12;

                dateTime = new DateTime(1899, 12, 30, hour, minute, second);

                str = str.Replace(m.Groups[0].Value, "").Trim();

                return true;
            }

            //CHE: in VB6 for datetime column rs.Field(x).Value will return only date if time is 0
            private object CustomFormat(SerializableColumn col, object colValue)
            {
                if (this.Type == Convert.ToInt32(SqlDbType.DateTime))
				{
                    string s = colValue.ToStringES();
                    if (s.EndsWith(" 12:00:00 AM"))
                    {
                        colValue = s.Substring(0, s.Length - 12);
                    }
                    else if (s.EndsWith(" 00:00:00"))
                    {
                        colValue = s.Substring(0, s.Length - 9);
                    }
                }
                else if (colValue.GetType() == typeof(decimal) && this.NumericScale > 0)
                {   //FC:FINAL: ZSA: Conversion .ToString() with NumericScale >= 100 not working correctly:
                    //Convert.ToDecimal(59961).ToString("N99") = 59,961.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
                    //Convert.ToDecimal(59961).ToString("N100") = N159961
                    if (this.NumericScale >= 100)
                    {
                        colValue = Convert.ToDecimal(colValue).ToString("N99");
                    }
                    else
                    {
                        colValue = Convert.ToDecimal(colValue).ToString("N" + this.NumericScale);
                    }                    
                    colValue = string.Format("{0:G29}", decimal.Parse(colValue.ToString()));
                }
                return colValue;
            }

            //CHE: can manage "1", "-1", "0","True","False"
            internal static bool BoolFromString(string value)
            {
                bool functionReturnValue = false;
                bool blnReturn = false;
                try
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        return blnReturn;
                    }
                    value = value.ToLower();
                    if (value != "true" && value != "false")
                    {
                        blnReturn = Convert.ToBoolean(Convert.ToInt32(value));
                    }
                    else
                    {
                        blnReturn = Convert.ToBoolean(value);
                    }
                }
                catch
                {
                    functionReturnValue = false;
                }
                finally
                {
                    functionReturnValue = blnReturn;
                }

                return functionReturnValue;
            }

            //CHE: field type as in Sql Server
            public string DataTypeName
            {
                get;
                set;
            }

            public int DataWidth
            {
                get;
                set;
            }

            private int type = 0;

            /// <summary>
            /// This is a compatibility issue 
            /// </summary>
            public int Type
            {
                set
                {
                    type = value;
                }
                get
                {
                    //CHE: if type was set in Field Append return that value
                    if (type != 0)
                    {
                        return type;
                    }

                    if (Column == null)
                    {
                        return 0;
                    }

                    //CHE: field type as in Sql Server
                    #region check DataTypeName
                    switch (this.DataTypeName)
                    {
                        case "bigint":
                            {
                                return Convert.ToInt32(SqlDbType.BigInt);
                            }
                        case "binary":
                            {
                                return Convert.ToInt32(SqlDbType.Binary);
                            }
                        case "bit":
                            {
                                return Convert.ToInt32(SqlDbType.Bit);
                            }
                        case "char":
                            {
                                return Convert.ToInt32(SqlDbType.Char);
                            }
                        case "date":
                            {
                                return Convert.ToInt32(SqlDbType.Date);
                            }
                        case "datetime":
                            {
                                return Convert.ToInt32(SqlDbType.DateTime);
                            }
                        case "datetime2":
                            {
                                return Convert.ToInt32(SqlDbType.DateTime2);
                            }
                        case "datetimeoffset":
                            {
                                return Convert.ToInt32(SqlDbType.DateTimeOffset);
                            }
                        case "decimal":
                            {
                                return Convert.ToInt32(SqlDbType.Decimal);
                            }
                        case "float":
                            {
                                return Convert.ToInt32(SqlDbType.Float);
                            }
                        case "image":
                            {
                                return Convert.ToInt32(SqlDbType.Image);
                            }
                        case "int":
                            {
                                return Convert.ToInt32(SqlDbType.Int);
                            }
                        case "money":
                            {
                                return Convert.ToInt32(SqlDbType.Money);
                            }
                        case "nchar":
                            {
                                return Convert.ToInt32(SqlDbType.NVarChar);
                            }
                        case "ntext":
                            {
                                return Convert.ToInt32(SqlDbType.NText);
                            }
                        case "nvarchar":
                            {
                                return Convert.ToInt32(SqlDbType.NVarChar);
                            }
                        case "real":
                            {
                                return Convert.ToInt32(SqlDbType.Real);
                            }
                        case "smalldatetime":
                            {
                                return Convert.ToInt32(SqlDbType.SmallDateTime);
                            }
                        case "smallint":
                            {
                                return Convert.ToInt32(SqlDbType.SmallInt);
                            }
                        case "smallmoney":
                            {
                                return Convert.ToInt32(SqlDbType.SmallMoney);
                            }
                        case "sql_variant":
                            {
                                return Convert.ToInt32(SqlDbType.Variant);
                            }
                        case "text":
                            {
                                return Convert.ToInt32(SqlDbType.Text);
                            }
                        case "time":
                            {
                                return Convert.ToInt32(SqlDbType.Time);
                            }
                        case "timestamp":
                            {
                                return Convert.ToInt32(SqlDbType.Timestamp);
                            }
                        case "tinyint":
                            {
                                return Convert.ToInt32(SqlDbType.TinyInt);
                            }
                        case "uniqueidentifier":
                            {
                                return Convert.ToInt32(SqlDbType.UniqueIdentifier);
                            }
                        case "varbinary":
                            {
                                return Convert.ToInt32(SqlDbType.VarBinary);
                            }
                        case "varchar":
                            {
                                return Convert.ToInt32(SqlDbType.VarChar);
                            }
                        case "xml":
                            {
                                return Convert.ToInt32(SqlDbType.Xml);
                            }
                    }
                    #endregion

                    Type dataType = Column.Table.Columns[Column.ColumnName].DataType;
                    return DataTableExtension.FieldsExtension.ConvertToSqlDbType(dataType);
                }
            }
            /// <summary>
            /// Attributes that match the ADODB.Field
            /// </summary>
            public int Attributes
            {
                get
                {

                    // we need to calculate attributes by the Field Attributes
                    int CalculatedAttributes = 0;
                    //CNA: return proper attribute corresponding to VB6 enum
                    //if (Column != null && Column.AllowDBNull) CalculatedAttributes = 1;
                    if (Column != null && Column.AllowDBNull) CalculatedAttributes = Convert.ToInt32(FieldAttributeEnum.adFldIsNullable);
                    return CalculatedAttributes;
                }
            }

            public SerializableColumn Column { get; set; }

            public string Name
            {
                get
                {
                    return ColumnName;
                }
                set
                {
                    ColumnName = value;
                }
            }

            public string ColumnName
            {
                get
                {
                    // return the column name of the column object

                    //return basecolumnname and not the automatically generated name
                    //e.g. for 2 tables in join having same column NR, C# will generate NR and NR1, but in VB6 both columns have ColumnName NR
                    if (!string.IsNullOrEmpty(BaseColumnName) && !IsAliased)
                    {
                        return BaseColumnName;
                    }
                    return Column.ColumnName;
                }
                set
                {
                    // set the value of the column name
                    Column.ColumnName = value;

                }
            }

        }

        public class FieldsExtension : IEnumerable<Field>
        {
            //BAN: added data table
            private DataTable mobjDataTable;
            //IPI: save the DataTable columns list
            private List<Field> mObjFields;

            public FieldsExtension(DataTable table)
            {
                this.mobjDataTable = table;
                this.mObjFields = new List<Field>();

                if (table == null)
                    return;

                foreach (DataColumn col in table.Columns)
                {
                    this.mObjFields.Add(new Field(table, col));
                }
            }

            public int Count
            {
                get { return (mObjFields == null) ? 0 : mObjFields.Count; }
            }

            //IPI: implement simple iteration
            public IEnumerator<Field> GetEnumerator()
            {
                return mObjFields.GetEnumerator();
            }

            IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            internal Field GetField(int Index)
            {
                return mObjFields[Index];
            }

            internal Field GetField(string FieldName)
            {
                FieldName = FieldName.ToUpper();
                //get last from list to be the same as in VB6 for fields having same name
                return mObjFields.FindLast(delegate(Field target)
                {
                    return target.ColumnName.ToUpper() == FieldName;
                });
            }

            /// <summary>
            /// Append a new column with column name and type, default size is 0 and default attribute is adFldUnspecified = -1
            /// </summary>
            /// <param name="columnName"></param>
            /// <param name="columnType"></param>
            public void Append(string columnName, System.Data.SqlDbType columnType)
            {
                Append(columnName, columnType, 0, FieldAttributeEnum.adFldUnspecified);
            }

            /// <summary>
            /// Append a new column with column name and type and size, default attribute is adFldUnspecified = -1
            /// </summary>
            /// <param name="columnName"></param>
            /// <param name="columnType"></param>
            /// <param name="columnSize"></param>
            public void Append(string columnName, System.Data.SqlDbType columnType, int? columnSize)
            {
                Append(columnName, columnType, columnSize, FieldAttributeEnum.adFldUnspecified);
            }

            public void Append(string columnName, OleDbType columnType, int? columnSize, FieldAttributeEnum attribute, object value = null)
            {
                Append(columnName, ConvertToSqlDbType(columnType), columnSize, attribute, value);                
            }

            ///// <summary>
            ///// Append a new column with column name and type and attribute, default size is 0
            ///// </summary>
            ///// <param name="columnName"></param>
            ///// <param name="columnType"></param>
            ///// <param name="attribute"></param>
            //public void Append(string columnName, System.Data.SqlDbType columnType, FieldAttributeEnum attribute)
            //{
            //    Append(columnName, columnType, 0, attribute);
            //}

            //BAN, SBE - added Append method
            public void Append(string columnName, System.Data.SqlDbType columnType, int? columnSize, FieldAttributeEnum attribute, object value = null)
            {
                DataColumn newColumn = new DataColumn(columnName);
                newColumn.DataType = ConvertToType(columnType);

                if (columnSize.HasValue && columnSize.Value > 0)
                {
                    if (newColumn.DataType == typeof(System.String))
                    {
                        newColumn.MaxLength = columnSize.Value;
                        ////Added support only for default FieldAttribute 0
                        //if (attribute == 0)
                        //{
                        //    newColumn.DefaultValue = "";
                        //}
                    }
                }
                ////Added support only for default FieldAttribute 0
                //if (attribute == 0)
                //{
                //    if (ConvertToType(columnType).IsValueType)
                //    {
                //        newColumn.DefaultValue = Activator.CreateInstance(ConvertToType(columnType));
                //    }
                //}

                this.mobjDataTable.Columns.Add(newColumn);
                Field f = new Field(this.mobjDataTable, newColumn);
                int fieldSize;
                if (columnSize == null)
                {
                    switch (columnType)
                    {
                        case SqlDbType.Float:
                            fieldSize = 8;
                            break;
                        case SqlDbType.Int:
                            fieldSize = 4;
                            break;
                        case SqlDbType.BigInt:
                            fieldSize = 8;
                            break;
                        case SqlDbType.TinyInt:
                            fieldSize = 1;
                            break;
                        case SqlDbType.SmallInt:
                            fieldSize = 2;
                            break;
                        case SqlDbType.Decimal:
                            fieldSize = 16;
                            break;
                        case SqlDbType.Bit:
                            fieldSize = 1;
                            break;
                        default:
                            fieldSize = 0;
                            break;
                    }
                }
                else
                {
                    fieldSize = Convert.ToInt32(columnSize);
                }
                f.DefinedSize = fieldSize;
                //SBE: check if attribute was specified, and set AllowDBNull property for column
                //CHE: set AllowDBNull on field and not on DataColumn
                // otherwise CellValidating is not raised for editable ComboBoxColumn when entered value is not in drop down list
                //ZSA:Inf #i424 Set AllowDBNull to true if attribute is unspecified
                if ((attribute == FieldAttributeEnum.adFldUnspecified) || ((attribute != FieldAttributeEnum.adFldUnspecified)
                    && ((attribute & FieldAttributeEnum.adFldIsNullable) == FieldAttributeEnum.adFldIsNullable
                    || (attribute & FieldAttributeEnum.adFldMayBeNull) == FieldAttributeEnum.adFldMayBeNull)))
                {
                    f.AllowDBNull = true;
                }
                else
                {
                    f.AllowDBNull = false;
                }
                //CHE: set type to be used when filling with spaces in Value setter
                f.Type = Convert.ToInt32(columnType);

                this.mObjFields.Add(f);
            }

            //BAN: type conversion between SqlDbType and Type
            private static Type ConvertToType(SqlDbType sqlDbType)
            {
                switch (sqlDbType)
                {
                    case SqlDbType.VarChar:
                    case SqlDbType.Text:
                    case SqlDbType.Char:
                    case SqlDbType.NVarChar:
                    case SqlDbType.NText:
                    case SqlDbType.NChar:
                        return typeof(System.String);

                    case SqlDbType.Bit:
                        return typeof(System.Boolean);

                    case SqlDbType.SmallInt:
                        return typeof(System.Int16);

                    case SqlDbType.Int:
                        return typeof(System.Int32);

                    case SqlDbType.BigInt:
                        return typeof(System.Int64);

                    //CHE: use decimal otherwise number format will not work
                    case SqlDbType.Float:
                    case SqlDbType.Decimal:
                    case SqlDbType.Money:
                    case SqlDbType.Real:
                        return typeof(System.Decimal);

                    case SqlDbType.TinyInt:
                        return typeof(System.Byte);

                    case SqlDbType.Binary:
                        //case SqlDbType.Timestamp:
                        return typeof(System.Byte[]);

                    case SqlDbType.DateTime:
                    case SqlDbType.Date:
                    case SqlDbType.Time:
                    case SqlDbType.Timestamp:
                        return typeof(System.DateTime);

                    default:
                        return null;
                }
            }

            //IPI
            internal static int ConvertToSqlDbType(Type dataType)
            {
                // SqlDbType.VarChar, SqlDbType.Text, SqlDbType.Char
                // SqlDbType.NVarChar, SqlDbType.NText, SqlDbType.NChar
                if (dataType == typeof(System.String))
                {
                    return Convert.ToInt32(SqlDbType.VarChar);
                }

                if (dataType == typeof(System.Boolean))
                {
                    return Convert.ToInt32(SqlDbType.Bit);
                }

                if (dataType == typeof(System.Int16))
                {
                    return Convert.ToInt32(SqlDbType.SmallInt);
                }

                if (dataType == typeof(System.Int32))
                {
                    return Convert.ToInt32(SqlDbType.Int);
                }

                if (dataType == typeof(System.Int64))
                {
                    return Convert.ToInt32(SqlDbType.BigInt);
                }

                if (dataType == typeof(System.Double))
                {
                    return Convert.ToInt32(SqlDbType.Float);
                }

                // SqlDbType.Money == System.Decimal
                if (dataType == typeof(System.Decimal))
                {
                    return Convert.ToInt32(SqlDbType.Decimal);
                }

                if (dataType == typeof(System.Byte))
                {
                    return Convert.ToInt32(SqlDbType.TinyInt);
                }

                if (dataType == typeof(System.Single))
                {
                    return Convert.ToInt32(SqlDbType.Real);
                }

                // SqlDbType.Timestamp == System.Byte[]
                if (dataType == typeof(System.Byte[]))
                {
                    return Convert.ToInt32(SqlDbType.Binary);
                }

                // SqlDbType.Date, SqlDbType.Time == System.DateTime
                if (dataType == typeof(System.DateTime))
                {
                    return Convert.ToInt32(SqlDbType.Date);
                }

                return 0;
            }

        internal static SqlDbType ConvertToSqlDbType(OleDbType dataType)
        {
                switch(dataType)
                {
                    case OleDbType.Empty:
                        {
                            return SqlDbType.Variant;
                        }
                    case OleDbType.SmallInt:
                        {
                            return SqlDbType.SmallInt;
                        }
                    case OleDbType.Integer:
                        {
                            return SqlDbType.Int;
                        }
                    case OleDbType.Double:
                        {
                            return SqlDbType.Real;
                        }
                    case OleDbType.Single:
                        {
                            return SqlDbType.Real;
                        }
                    case OleDbType.Currency:
                        {
                            return SqlDbType.Money;
                        }
                    case OleDbType.Date:
                        {
                            return SqlDbType.Date;
                        }
                    case OleDbType.BSTR:
                        {
                            return SqlDbType.VarChar;
                        }
                    case OleDbType.IDispatch:
                        {
                            return SqlDbType.Variant;
                        }
                    case OleDbType.Error:
                        {
                            return SqlDbType.Variant;
                        }
                    case OleDbType.Boolean:
                        {
                            return SqlDbType.Bit;
                        }
                    case OleDbType.Variant:
                        {
                            return SqlDbType.Variant;
                        }
                    case OleDbType.IUnknown:
                        {
                            return SqlDbType.Variant;
                        }
                    case OleDbType.Decimal:
                        {
                            return SqlDbType.Decimal;
                        }
                    case OleDbType.TinyInt:
                        {
                            return SqlDbType.TinyInt;
                        }
                    case OleDbType.UnsignedTinyInt:
                        {
                            return SqlDbType.TinyInt;
                        }
                    case OleDbType.UnsignedSmallInt:
                        {
                            return SqlDbType.SmallInt;
                        }
                    case OleDbType.UnsignedInt:
                        {
                            return SqlDbType.Int;
                        }
                    case OleDbType.BigInt:
                        {
                            return SqlDbType.BigInt;
                        }
                    case OleDbType.UnsignedBigInt:
                        {
                            return SqlDbType.BigInt;
                        }
                    case OleDbType.Filetime:
                        {
                            return SqlDbType.Timestamp;
                        }
                    case OleDbType.Guid:
                        {
                            return SqlDbType.UniqueIdentifier;
                        }
                    case OleDbType.Binary:
                        {
                            return SqlDbType.Binary;
                        }
                    case OleDbType.Char:
                        {
                            return SqlDbType.Char;
                        }
                    case OleDbType.WChar:
                        {
                            return SqlDbType.VarChar;
                        }
                    case OleDbType.Numeric:
                        {
                            return SqlDbType.Int;
                        }
                    case OleDbType.DBDate:
                        {
                            return SqlDbType.DateTime;
                        }
                    case OleDbType.DBTime:
                        {
                            return SqlDbType.Time;
                        }
                    case OleDbType.DBTimeStamp:
                        {
                            return SqlDbType.Timestamp;
                        }
                    case OleDbType.PropVariant:
                        {
                            return SqlDbType.Variant;
                        }
                    case OleDbType.VarNumeric:
                        {
                            return SqlDbType.Int;
                        }
                    case OleDbType.VarChar:
                        {
                            return SqlDbType.VarChar;
                        }
                    case OleDbType.LongVarChar:
                        {
                            return SqlDbType.VarChar;
                        }
                    case OleDbType.VarWChar:
                        {
                            return SqlDbType.VarChar;
                        }
                    case OleDbType.LongVarWChar:
                        {
                            return SqlDbType.VarChar;
                        }
                    case OleDbType.VarBinary:
                        {
                            return SqlDbType.VarBinary;
                        }
                    case OleDbType.LongVarBinary:
                        {
                            return SqlDbType.VarBinary;
                        }
                }
                return SqlDbType.Variant;
            }
    }

    private class CommandDescriptor
        {
            public string whereClause = "";
            public string tableName = "";
            public string command = "";
        }
    }

    /// <summary>
    /// 
    /// </summary>

    [Serializable]
    internal class DataTableDefinitions
    {

        private static Dictionary<string, DataTableExtension.FieldsExtension> fieldCache = new Dictionary<string, DataTableExtension.FieldsExtension>();

        //BAN: RowChangeEvent is not fired when changing a cell value and moving on the same row. Need to attach the ColumnChange event
        //handler in order to raise RowChangeEvent
        public DataRowChangeEventHandler RowChangeEventHandler
        {
            get;
            set;
        }

        //DSE Performance improvement by using properties for EOF and BOF and only computing them when necessary
        public bool EOF { get; set; }
        public bool BOF { get; set; }

        public int CurrentViewIndex = -1;


        //CHE: FilterType should not affect grid, save and restore position
        private int? bookmarkBeforeFilterType;

        public int? BookmarkBeforeFilterType
        {
            get { return bookmarkBeforeFilterType; }
            set { bookmarkBeforeFilterType = value; }
        }

        //CHE: AddNew on DataTable with mandatory fields will raise exception if new empty row is added in collection, must be added later on Update
        private List<DataRow> newRows;

        public List<DataRow> NewRows
        {
            get { return newRows; }
            set { newRows = value; }
        }

        private List<DataTable> clonedTables;

        public List<DataTable> ClonedTables
        {
            get { return clonedTables; }
            set { clonedTables = value; }
        }

        private DataGridView parentGrid;

        internal DataGridView ParentGrid
        {
            get { return parentGrid; }
            set { parentGrid = value; }
        }

        private DbDataAdapter mobjDataAdapter = null;
        private int mintIndex = 1;
        internal int mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosUnknown);
        private bool mblnDeleted = false;
        private string mstrFilter = "";
        private string mstrSource = "";
        private string mstrSort = "";
        internal string connString = "";
        internal DbConnection connection;
        private DataView mobjView = null;
        DataTable mobjDataTable = null;
        // set the default page size to 10.
        private int mintPageSize = 10;
        //IPI
        private FilterGroupEnum mFilterType = FilterGroupEnum.adFilterNone;

        //IPI
        internal MoveCompleteHandler MoveComplete { get; set; }

        //IPI
        private DataTableExtension.FieldsExtension mObjFields;
        internal DataTableExtension.FieldsExtension Fields
        {
            get
            {
                if (mObjFields == null)
                {
                    mObjFields = new DataTableExtension.FieldsExtension(this.mobjDataTable);
                    LoadFields();
                }
                return mObjFields;
            }
        }

        internal void ClearFields()
        {
            mObjFields = null;
            fieldCache.Clear();
        }

        private void LoadFields()
        {
            if (string.IsNullOrEmpty(connString) || string.IsNullOrEmpty(Source))
            {
                return;
            }
            string s = NormalizeSQLForCache(Source).ToUpper();
            if (fieldCache.ContainsKey(s))
            {
                // TB: in case of exactly the same SQL statement => use information from previous cached fields
                // we have to copy just the properties of previous fields and not the field itself!
                try
                {
                    connection = null;
                    DataTableExtension.FieldsExtension cacheFields = fieldCache[s];

                    for (int i = 0; i < cacheFields.Count; i++)
                    {
                        DataTableExtension.Field field = mObjFields.GetField(i);
                        DataTableExtension.Field cacheField = cacheFields.GetField(i);

                        field.AllowDBNull = cacheField.AllowDBNull;
                        field.BaseColumnName = cacheField.BaseColumnName;
                        field.BaseTableName = cacheField.BaseTableName;
                        field.Column.AllowDBNull = cacheField.Column.AllowDBNull;
                        field.ColumnName = cacheField.ColumnName;
                        field.DataTypeName = cacheField.DataTypeName;
                        field.DataWidth = cacheField.DataWidth;
                        field.DefaultValue = cacheField.DefaultValue;
                        field.DefinedSize = cacheField.DefinedSize;
                        field.IsAliased = cacheField.IsAliased;
                        field.IsIdentity = cacheField.IsIdentity;
                        field.NumericScale = cacheField.NumericScale;
                        field.Precision = cacheField.Precision;

                        if (field.IsIdentity)
                        {
                            IdentityColumn = field;
                        }
                    }
                }
                catch (Exception ex)
                {
                    fieldCache.Clear();
                    LoadFields();
                }
            }
            else
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    LoadFields(connection);
                    connection = null;
                }
            }
        }

        /// <summary>
        /// Loads the fields.
        /// </summary>
        /// <param name="pConnection">The p connection.</param>
        private void LoadFields(DbConnection pConnection)
        {
            if (pConnection == null)
            {
                return;
            }
            string s = NormalizeSQLForCache(Source).ToUpper();
            //already loaded
            if (fieldCache.ContainsKey(s))
            {
                return;
            }
            else
            {
                DbDataReader myReader = null;
                try
                {


                    mObjFields = new DataTableExtension.FieldsExtension(this.mobjDataTable);

                    IdentityColumn = null;
                    bool setDefaultValues = false;
                    List<DataTableExtension.Field> defaultValues = new List<DataTableExtension.Field>();

                    setDefaultValues = true;

                    DbCommand cmd = null;
                    if (pConnection is System.Data.SqlClient.SqlConnection)
                    {
                        cmd = new SqlCommand();
                    }
                    else if (pConnection is System.Data.OleDb.OleDbConnection)
                    {
                        cmd = new OleDbCommand();
                    }
                    else
                    {
                        cmd = new SqlCommand();
                    }

                    cmd.Connection = pConnection;
                    cmd.Transaction = pConnection.GetTransaction();
                    cmd.CommandText = Source;

                    myReader = cmd.ExecuteReader(CommandBehavior.KeyInfo);

                    //Retrieve column schema into a DataTable.
                    DataTable schemaTable = myReader.GetSchemaTable();

                    int i = 0;
                    bool allowDBNull = false;
                    //For each field in the table...

                    foreach (DataRow myField in schemaTable.Rows)
                    {

                        string name = myField[schemaTable.Columns["ColumnName"]].ToString();
                        DataTableExtension.Field f = mObjFields.GetField(name);
                        //in VB6 it is possible to have field with no name (C# generates Column1 for ColumnName)
                        if ((f != null || name == "") && i < mObjFields.Count)
                        {
                            //to set info for the correct field if multiple fields have same name
                            f = mObjFields.GetField(i);
                            allowDBNull = Convert.ToBoolean(myField[schemaTable.Columns["AllowDBNull"]]);
                            if (f.Column.AllowDBNull != allowDBNull)
                            {
                                f.Column.AllowDBNull = allowDBNull;
                            }
                            f.DefinedSize = Convert.ToInt32(myField[schemaTable.Columns["ColumnSize"]]);
                            f.BaseTableName = myField[schemaTable.Columns["BaseTableName"]].ToString();
                            if (schemaTable.Columns["IsAliased"] != null && myField[schemaTable.Columns["IsAliased"]] != DBNull.Value)
                            {
                                f.IsAliased = Convert.ToBoolean(myField[schemaTable.Columns["IsAliased"]]);
                            }
                            //CHE: IsAliased column does not exist in case of mdb, set flag if the values are different
                            if (!f.IsAliased && myField[schemaTable.Columns["BaseColumnName"]].ToString() != myField[schemaTable.Columns["ColumnName"]].ToString())
                            {
                                f.IsAliased = true;
                            }
                            //CHE: set BaseColumnName after setting IsAliased because otherwise ColumnName is incorrect in case of alias
                            f.BaseColumnName = myField[schemaTable.Columns["BaseColumnName"]].ToString();
                            //CHE: field type as in Sql Server
                            if (schemaTable.Columns["DataTypeName"] != null)
                            {
                                f.DataTypeName = myField[schemaTable.Columns["DataTypeName"]].ToString().ToLower();
                            }
                            //CHE: for TextBoxColumn in CellBeginEdit the javascript SetDataWith does not find textarea TRG_ which is created later when key is pressed 
                            // (same works fine in SetEditMask for ComboBoxColumn); 
                            // as solution for TextBoxColumn set MaxInputLength based on associated DataColumn type and size
                            switch (FCConvert.ToString(myField[schemaTable.Columns["DataType"]]))
                            {
                                case "System.Int32":
                                    {
                                        f.DataWidth = 11;
                                        break;
                                    }
                                case "System.String":
                                    {
                                        f.DataWidth = f.DefinedSize;
                                        break;
                                    }
                            }
                            if (f.Type == Convert.ToInt32(SqlDbType.Decimal))
							{
                                f.Precision = Convert.ToInt32(myField[schemaTable.Columns["NumericPrecision"]]);
                                f.NumericScale = Convert.ToInt32(myField[schemaTable.Columns["NumericScale"]]);
                            }
                            //CHE: in UpdateBatch before calling FillSchema replace null field with default value from SQL server if field does not accept null
                            f.AllowDBNull = true;
                            if (!Convert.ToBoolean(myField[schemaTable.Columns["AllowDBNull"]]) && !string.IsNullOrEmpty(f.BaseTableName) && !string.IsNullOrEmpty(f.BaseColumnName))
                            {
                                f.AllowDBNull = false;
                                defaultValues.Add(f);

                            }
                            //SBE: set the IdentityColumn
                            if ((schemaTable.Columns["IsIdentity"] != null && Convert.ToBoolean(myField[schemaTable.Columns["IsIdentity"]])) ||
                                (schemaTable.Columns["IsAutoIncrement"] != null && Convert.ToBoolean(myField[schemaTable.Columns["IsAutoIncrement"]])))
                            {
                                f.IsIdentity = true;
                                IdentityColumn = f;
                            }
                            ++i;
                        }
                    }

                    myReader.Close();
                    myReader = null;

                    fieldCache[s] = mObjFields;


                    // set default values only if table schema was read from database
                    if (setDefaultValues)
                    {

                        //CHE: in UpdateBatch before calling FillSchema replace null field with default value from SQL server if field does not accept null
                        foreach (DataTableExtension.Field f in defaultValues)
                        {
                            DbCommand scalarCmd = null;

                            if (cmd.Connection is System.Data.SqlClient.SqlConnection)
                            {
                                scalarCmd = new SqlCommand();
                            }
                            else if (cmd.Connection is System.Data.OleDb.OleDbConnection)
                            {
                                scalarCmd = new OleDbCommand();
                            }                           
                            scalarCmd.Connection = cmd.Connection;
                            scalarCmd.Transaction = cmd.Transaction;
                            if (cmd.Connection.ConnectionString.Contains("Provider=Microsoft.ACE.OLEDB.12.0"))
                            {
                                // TODO:CHE - default value in case of mdb
                            }
                            else
                            {
                                f.DefaultValue = GetDefaultValue(cmd, f, scalarCmd);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (myReader != null)
                    {
                        myReader.Close();
                    }
                }
            }
        }

        private static Dictionary<string, string> defValues = new Dictionary<string, string>();

        private static string GetDefaultValue(DbCommand cmd, fecherFoundation.DataBaseLayer.ADO.DataTableExtension.Field f, DbCommand scalarCmd)
        {
            string key = f.BaseTableName + "." + f.ColumnName;
            string defaultValue = "";

            if (!defValues.TryGetValue(key, out defaultValue))
            {
                //if (cmd.Connection.ConnectionString.Contains("Provider=OraOLEDB.Oracle") || cmd.Connection is OracleConnection)
                //{
                //    scalarCmd.CommandText = string.Format("SELECT DEFAULTVAL FROM COL WHERE TNAME = '{0}' and CNAME = '{1}'", f.BaseTableName, f.BaseColumnName);
                //}
                //else
                {
                    scalarCmd.CommandText = string.Format("SELECT COLUMN_DEFAULT FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '{0}' and COLUMN_NAME = '{1}'", f.BaseTableName, f.BaseColumnName);
                }
                defaultValue = FCConvert.ToString(scalarCmd.ExecuteScalar());

                defValues.Add(key, defaultValue);
            }
            return defaultValue;
        }

        private static string NormalizeSQLForCache(string sql)
        {
            string result = "";

            if (!string.IsNullOrEmpty(sql))
            {
                result = sql.Replace("\t", " ").Replace(System.Environment.NewLine, " ").ToUpper();
                int pos = result.LastIndexOf(" WHERE ");
                if (pos >= 0)
                {
                    result = result.Substring(0, pos);
                }
            }
            return result;
        }

        internal DataTableDefinitions(DataTable objDataTable)
        {
            mobjDataTable = objDataTable;
        }

        internal bool SupressRowChange { get; set; }

        internal bool IsInternalDataAction { get; set; }

        /// <summary>
        /// Gets the definitions of the current Data Table
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static DataTableDefinitions GetDataTableDefinitions(DataTable objDataTable)
        {
            DataTableDefinitions objDataTableDefinitions = null;



            //DSE Performance improvement by not parsing twice the hole DataTableDefinitions dictionary
            if (DataTableExtension.mobjDefinitions.ContainsKey(objDataTable))
            {

                objDataTableDefinitions = DataTableExtension.mobjDefinitions[objDataTable];
            }
            else
            {
                objDataTableDefinitions = new DataTableDefinitions(objDataTable);
                objDataTable.Disposed += new EventHandler(objDataTable_Disposed);
                objDataTable.ColumnChanged += new DataColumnChangeEventHandler(objDataTable_ColumnChanged);
                objDataTable.TableNewRow += new DataTableNewRowEventHandler(objDataTable_TableNewRow);
                objDataTable.RowDeleted += new DataRowChangeEventHandler(objDataTable_RowDeleted);
                objDataTable.RowChanging += new DataRowChangeEventHandler(objDataTable_RowChanging);
                DataTableExtension.mobjDefinitions.Add(objDataTable, objDataTableDefinitions);
            }

            return objDataTableDefinitions;
        }

        //IPI: when an EndEdit is called on a DataTable, the new row from the DefaultView is added to the DataTable.Rows collection
        // this has to be synchronized in all clone tables                
        static void objDataTable_RowChanging(object sender, DataRowChangeEventArgs e)
        {

            if (DataTableDefinitions.GetDataTableDefinitions(sender as DataTable).IsInternalDataAction)
            {
                return;
            }

            if (e.Action == DataRowAction.Add && e.Row.RowState == DataRowState.Detached)
            {
                DataTable objDataTable = sender as DataTable;
                if (objDataTable != null)
                {
                    //get the instance of the definitions class
                    DataTableDefinitions objDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(objDataTable);

                    //CHE: when EndEdit is executed, e.Row will be committed to the Rows collection
                    // if a row is in the DataTable.Rows collection, it should be removed from NewRows
                    if (objDataTableDefinitions.NewRows != null && objDataTableDefinitions.NewRows.Contains(e.Row))
                    {
                        objDataTableDefinitions.NewRows.Remove(e.Row);
                    }
                }

                SynchronizeClones((DataTable)sender, Synchronization.MoveRowFromDefaultView, e.Row);
            }
        }

        static void objDataTable_RowDeleted(object sender, DataRowChangeEventArgs e)
        {
            DataTable currentTable = (DataTable)sender;

            int rowIndex = currentTable.Rows.IndexOf(e.Row);

            SynchronizeClones(currentTable, Synchronization.DeleteRow, e.Row);
        }

        static void objDataTable_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            DataTable currentTable = (DataTable)sender;

            int rowIndex = currentTable.Rows.IndexOf(e.Row);

            SynchronizeClones(currentTable, Synchronization.TableNewRow, e.Row);

        }

        //IPI: synchronize rows add/ delete/ update in all clone tables
        internal bool suppressCloneSynchronization = false;
        public static void SynchronizeClones(DataTable source, Synchronization syncType, DataRow sourceRow = null)
        {
            DataTableDefinitions objDataTableDefinitions = DataTableExtension.mobjDefinitions[source];
            List<DataTable> clonedTables = objDataTableDefinitions.ClonedTables;

            if (clonedTables == null)
            {
                return;
            }

            //IPI: avoid infinite loop as each clone has reference to all the other clones
            if (objDataTableDefinitions.suppressCloneSynchronization)
            {
                return;
            }

            //Avoid infinite looping by setting the suppress flag to true. The source data table is contained in the clone
            //DataTable list of it`s clones for track-back referencing
            objDataTableDefinitions.suppressCloneSynchronization = true;

            int? originalIndex = 0;
            foreach (DataTable cloneTable in clonedTables)
            {
                //The cloneTable may be the parent DataTable, in this case, suppress the sync mechanism
                if (DataTableDefinitions.GetDataTableDefinitions(cloneTable).suppressCloneSynchronization)
                    continue;

                if (syncType != Synchronization.TableNewRow)
                {
                    //IPI: current row can be different in original and clone table
                    //In case of EOF or BOF the Bookmark will throw an exception (in VB6 is the same behavior)
                    if (!cloneTable.EOF() && !cloneTable.BOF())
                    {
                        //IPI: must use the index, because setting the bookmark will also change the CurrentCell in DGV
                        originalIndex = cloneTable.GetIndex();

                        if (!source.EOF() && !source.BOF())
                        {
                            //IPI: must use the index, because setting the bookmark will also change the CurrentCell in DGV
                            cloneTable.SetIndex(source.GetIndex());
                        }
                    }
                }
                switch (syncType)
                {
                    //TableNewRow is called when the row is added by editing the bound grid by the user
                    case Synchronization.TableNewRow:
                        //The new row in the clone table is added in the DefaultView of the DataTable because
                        //the functionality of the grid adds it in the grid`s data source DefaultView and not in the DataTable
                        sourceRow.PairRow(cloneTable.DefaultView.AddNew().Row);
                        break;

                    //DetachedNewRow is called when the row is created from the application using the AddNew function
                    case Synchronization.DetachedNewRow:
                        sourceRow.PairRow(cloneTable.AddNew());
                        break;

                    case Synchronization.DeleteRow:
                        foreach (DataRow pairRow in sourceRow.PairRow())
                        {
                            pairRow.Delete();
                        }
                        break;

                    case Synchronization.UpdateRow:
                        //CHE: for disconnected DataTable do nothing on update
                        if (objDataTableDefinitions.DataAdapter != null && objDataTableDefinitions.DataAdapter.SelectCommand != null && objDataTableDefinitions.DataAdapter.SelectCommand.Connection == null)
                        {
                        }
                        else
                        {
                            //CHE: do not execute Update for clone on DataAdapter (PK violation), only modify RowState
                            //cloneTable.Update();
                            cloneTable.AcceptChanges();
                        }
                        break;

                    case Synchronization.CancelRow:
                        //CHE: for disconnected DataTable do nothing on cancel
                        if (objDataTableDefinitions.DataAdapter != null && objDataTableDefinitions.DataAdapter.SelectCommand != null && objDataTableDefinitions.DataAdapter.SelectCommand.Connection == null)
                        {
                        }
                        else
                        {
                            if (cloneTable.HasNewRows())
                            {
                                DataTableDefinitions.GetDataTableDefinitions(cloneTable).NewRows.Clear();
                            }
                            cloneTable.RejectChanges();
                        }
                        break;

                    //IPI: when an EndEdit is called on a DataTable, the new row from the DefaultView is added to the DataTable.Rows collection
                    // this has to be synchronized in all clone tables
                    case Synchronization.MoveRowFromDefaultView:
                        foreach (DataRow pairRow in sourceRow.PairRow())
                        {
                            foreach (DataRowView drv in cloneTable.DefaultView)
                            {
                                if (drv.Row == pairRow)
                                {
                                    drv.EndEdit();
                                    break;
                                }
                            }
                        }
                        break;
                }

                if (syncType != Synchronization.TableNewRow && originalIndex > 0)
                {
                    //IPI: must use the index, because setting the bookmark will also change the CurrentCell in DGV
                    cloneTable.SetIndex(originalIndex.Value);

                }
            }
            objDataTableDefinitions.suppressCloneSynchronization = false;
        }

        static void objDataTable_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            DataTable currentTable = (DataTable)sender;
            DataTableDefinitions objDataTableDefinitions = DataTableExtension.mobjDefinitions[currentTable];
            List<DataTable> clonedTables = objDataTableDefinitions.ClonedTables;

            //IPI: although values are correctly modified in the DataTable/ DefaultView, the row state does not get modified
            // this will have as effect that modified bound values will not be saved in the database
            if (e.Row.RowState == DataRowState.Unchanged)
            {
                object currentValue = e.Row[e.Column, DataRowVersion.Current];
                object proposedValue = e.ProposedValue;

                if ((!Information.IsDBNull(currentValue) && Information.IsDBNull(proposedValue)) ||
                    (Information.IsDBNull(currentValue) && !Information.IsDBNull(proposedValue)) ||
                    FCConvert.ToString(currentValue) != FCConvert.ToString(proposedValue))
                {
                    e.Row.SetModified();

                    //IPI: cell value must be synchronized after SetModified because
                    // SetModified is resetting the proposed value to the original value
                    e.Row[e.Column] = e.ProposedValue;
                }
            }

            if (clonedTables == null)
            {
                return;
            }

            //Get row index from the current data table
            int rowIndex = currentTable.Rows.IndexOf(e.Row);

            string columnName = e.Column.ColumnName;

            //IPI: avoid infinite loop as each clone has reference to all the other clones
            if (!objDataTableDefinitions.suppressCloneSynchronization)
            {
                objDataTableDefinitions.suppressCloneSynchronization = true;
                try
                {
                    //Loop trough the paired rows to set the values
                    List<DataRow> pairRows = e.Row.PairRow();
                    foreach (DataRow pairRow in pairRows)
                    {
                        //CHE: skip if no changes were done
                        if (Convert.ToString(pairRow[columnName]) == Convert.ToString(e.ProposedValue))
                        {
                            continue;
                        }

                        //IPI: need to synchronize the RowState as well
                        // SetModified can only be called on DataRows with Unchanged DataRowState.
                        if (pairRow.RowState == DataRowState.Unchanged)
                        {
                            //CHE: set RowState as in original
                            if (e.Row.RowState == DataRowState.Added)
                            {
                                pairRow.SetAdded();
                            }
                            if (e.Row.RowState == DataRowState.Modified)
                            {
                                pairRow.SetModified();
                            }
                        }

                        //IPI: cell value must be synchronized after SetModified because
                        // SetModified is resetting the proposed value to the original value
                        pairRow[columnName] = e.ProposedValue;
                    }
                }
                finally
                {
                    objDataTableDefinitions.suppressCloneSynchronization = false;
                }
            }
        }

        /// <summary>
        /// Removes the current DataTable's definitions from the dictionary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void objDataTable_Disposed(object sender, EventArgs e)
        {
            DataTable dataTable = ((DataTable)sender);

            DataTableDefinitions dataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(dataTable);
            if (dataTableDefinitions.ClonedTables != null)
            {
                for (int i = 1; i < dataTableDefinitions.ClonedTables.Count; i++)
                {
                    DataTableDefinitions clonedDataTableDefinitions = DataTableDefinitions.GetDataTableDefinitions(dataTableDefinitions.clonedTables[i]);
                    clonedDataTableDefinitions.ClonedTables.Remove(dataTable);
                }

                dataTableDefinitions.ClonedTables.Clear();
            }

            DataTableExtension.mobjDefinitions.Remove((DataTable)sender);
        }

        public string Source
        {
            get
            {
                return mstrSource;
            }
            set
            {
                mstrSource = value;
            }
        }

        public string Filter
        {
            get
            {
                return mstrFilter;
            }
            set
            {
                mblnDeleted = false;
                mstrFilter = value;
            }
        }

        //IPI
        public FilterGroupEnum FilterType
        {
            get
            {
                return mFilterType;
            }
            set
            {
                mblnDeleted = false;
                mFilterType = value;
            }
        }

        public string Sort
        {
            get
            {
                return mstrSort;
            }
            set
            {
                mblnDeleted = false;
                mstrSort = value;
            }
        }

        internal bool Deleted
        {
            get
            {
                return mblnDeleted;
            }
            set
            {
                mblnDeleted = value;
            }
        }


        internal DataView View
        {
            get
            {
                return mobjView;
            }
            set
            {
                mblnDeleted = false;
                mobjView = value;
                if (mobjDataTable != null)
                {
                    mobjDataTable.ComputeEofBof();
                }
            }
        }

        /// <summary>
        /// Holds the current position of the DataTable (bookmark)
        /// </summary>
        public int Index
        {
            get
            {
                return mintIndex;
            }
            set
            {
                mblnDeleted = false;
                mintIndex = value;
                mintAbsolutePosition = value;
                if (mobjDataTable != null)
                {
                    mobjDataTable.ComputeEofBof();
                }
            }
        }
        public int AbsolutePosition
        {
            get
            {
                return mintAbsolutePosition;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException();
                }

                int intRecordCount;
                if (this.View == null)
                {
                    intRecordCount = mobjDataTable.Rows.Count;
                }
                else
                {
                    intRecordCount = this.View.Count;
                }
                if (value > intRecordCount)
                {
                    value = intRecordCount;
                }
                this.Index = value;
            }
        }



        public DbDataAdapter DataAdapter
        {
            get
            {
                return mobjDataAdapter;
            }
            set
            {
                mobjDataAdapter = value;
            }
        }

        /// <summary>
        /// Holds the page size of the recordset.
        /// </summary>
        public int PageSize
        {
            get
            {
                // return the page size
                return mintPageSize;
            }
            set
            {
                // set the page size
                mintPageSize = value;
            }
        }

        /// <summary>
        /// Holds the IdentityColumn
        /// </summary>
        public DataTableExtension.Field IdentityColumn
        {
            get;
            set;
        }

        public CursorLocationEnum CursorLocation { get; set; }

        public LockTypeEnum LockType { get; set; }
        public CursorTypeEnum CursorType { get; set; }

    }

    //BAN: A DataRow from a DataTable which has cloned DataTables will have the 
    //corresponding row in the cloned tables on the PairRow list. This list is used for referencing
    //in the cases of new row addition in the DataTable, value changin, or deleting
    //When changing a value in the DataRow, the ColumnChanged event will be triggered for it`s owning
    //DataTable, in which the paired rows will be updated with the new value.
    //When calling an extension method from the DataTable which alters the DataTable row collection or DefaultView
    //a sync method will be called from the respective method which, using the PairRow list, will update the rows accordingly
    public static class DataRowExtension
    {
        internal static Dictionary<DataRow, DataRowDefinitions> mobjDefinitions = new Dictionary<DataRow, DataRowDefinitions>();

        public static List<DataRow> PairRow(this DataRow objDataRow)
        {
            DataRowDefinitions mobjDataRowDefinitions = DataRowDefinitions.GetDataRowDefinitions(objDataRow);
            return mobjDataRowDefinitions.PairRow;
        }

        public static void PairRow(this DataRow objDataRow, DataRow pairRow)
        {
            DataRowDefinitions mobjDataRowDefinitions = DataRowDefinitions.GetDataRowDefinitions(objDataRow);
            if (!mobjDataRowDefinitions.PairRow.Contains(pairRow))
            {
                mobjDataRowDefinitions.PairRow.Add(pairRow);
            }
            //The clone row was added to the source row, the source row now must be added to the clone row for data sync possibilities
            DataRowDefinitions mobjPairDataRowDefinitions = DataRowDefinitions.GetDataRowDefinitions(pairRow);
            if (!mobjPairDataRowDefinitions.PairRow.Contains(objDataRow))
            {
                mobjPairDataRowDefinitions.PairRow.Add(objDataRow);
            }
        }
    }

    public class DataRowDefinitions
    {
        #region Members
        private DataRow mobjDataRow = null;
        private DataRow mobjPairRow = null;
        private DataRowState rowState = DataRowState.Unchanged;
        #endregion

        #region Properties

        public List<DataRow> PairRow;

        #endregion

        #region Constructor
        internal DataRowDefinitions(DataRow objDataRow)
        {
            mobjDataRow = objDataRow;
            PairRow = new List<DataRow>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get the instance of the definition class
        /// </summary>
        /// <param name="objCheckBox"></param>
        /// <returns></returns>
        public static DataRowDefinitions GetDataRowDefinitions(DataRow objDataRow)
        {
            DataRowDefinitions objDataRowDefinitions = null;
            if (DataRowExtension.mobjDefinitions.ContainsKey(objDataRow))
            {
                objDataRowDefinitions = DataRowExtension.mobjDefinitions[objDataRow];
            }
            else
            {
                objDataRowDefinitions = new DataRowDefinitions(objDataRow);
                DataRowExtension.mobjDefinitions.Add(objDataRow, objDataRowDefinitions);
            }
            return objDataRowDefinitions;
        }
        #endregion
    }
}