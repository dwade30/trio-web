﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.RDO
{
    [Serializable]
    public class FCrdoSerializableColumn
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors

        public FCrdoSerializableColumn(DataColumn objColumn, FCrdoResultset dataTable)
        {
            this.ColumnName = objColumn.ColumnName;
            this.AllowDBNull = objColumn.AllowDBNull;
            this.Table = dataTable;
            this.ReadOnly = objColumn.ReadOnly;
            this.OrdinalPosition = objColumn.Ordinal + 1;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public string ColumnName
        {
            get;
            set;
        }

        public int OrdinalPosition
        {
            get;
            set;
        }

        public bool AllowDBNull
        {
            get;
            set;
        }

        public bool ReadOnly
        {
            get;
            set;
        }

        public FCrdoResultset Table
        {
            get;
            set;
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
