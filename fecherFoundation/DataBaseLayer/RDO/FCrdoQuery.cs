﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// rdoQuery
    /// </summary>
    public class FCrdoQuery
    {
        #region Public Members

        /// <summary>
        ///Returns a specific member of an rdoParameters collection either by position or by key.
        ///This property is used to access collections of rdoParameters objects.For example, the rdoQuery object has an rdoParameters property which is the 
        ///collection of rdoParameters objects that apply to the rdoQuery object.
        /// </summary>
        public FCrdoParameters rdoParameters;        

        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private string sql = "";
        private bool storedProcedure = false;
        private int queryTimeout = -1; //Set initial value to an invalid value in case of the property was not set by the customer

        #endregion

        #region Constructors

        public FCrdoQuery()
        {
            rdoParameters = new FCrdoParameters(this);
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        //CHE - while filling with async call DoEvents and cancel if the case
        public event EventHandler Filling;

        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public FCrdoParameter this[int index]
        {
            get
            {
                return rdoParameters[index];
            }
        }

        public FCRDO.ResultsetTypeConstants CursorType
        {
            get;
            set;
        }

        public FCRDO.LockTypeConstants LockType
        {
            get;
            set;
        }

        public FCrdoConnection ActiveConnection
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that specifies the number of seconds the ODBC driver manager waits before a timeout error occurs when a query 
        /// is executed.
        /// value A Long integer representing the number of seconds the driver manager waits before timing out and returning an error. 
        /// Remarks
        /// The default QueryTimeout property setting is 30 seconds.When you're accessing an ODBC data source using the OpenResultset or Execute methods, 
        /// there may be delays due to network traffic or heavy use of the remote server perhaps caused by your query. Rather than waiting indefinitely, 
        /// use the QueryTimeout property to determine how long your application should wait before the QueryTimeout event is fired and your application 
        /// trips a trappable error. At this point you have the option to continue waiting for another 'n' seconds as determined by the QueryTimeout 
        /// property, or cancel the query in progress by using the Cancel argument in the QueryTimeout event procedure.
        /// Setting this property to 0 disables the timer so your query will run indefinitely.Setting QueryTimeout to 0 is not recommended for synchronous 
        /// operations as your application can be blocked for the entire duration of the query.
        /// When used with an rdoConnection object, the QueryTimeout property specifies a global value for all queries associated with the data source.
        /// When you use an rdoQuery object, the rdoConnection object's QueryTimeout property is used as a default value unless you specify a new value in 
        /// the rdoQuery object's QueryTimeout property.
        /// When working with asynchronous queries, the StillExecuting property remains True until the query completes, or the query timeout period is 
        /// exhausted.
        /// If the specified timeout exceeds the maximum timeout permitted by the data source, or is smaller than the minimum timeout, the driver 
        /// substitutes that value and the following error is logged to the rdoErrors collection: SQLState 01S02: "Option value changed."
        /// </summary>
        public int QueryTimeout
        {
            get
            {
                return queryTimeout;
            }
            set
            {
                queryTimeout = value;
            }
        }

        /// <summary>
        /// Returns or sets the SQL statement that defines the query executed by an rdoQuery object or a RemoteData control.
        /// value A string expression that contains a value as described in Settings. (Data type is String.)
        /// Settings
        /// The settings for value are:
        /// Setting Description
        /// A valid SQL statement An SQL query using syntax appropriate for the data source.
        /// A stored procedure The name of a stored procedure supported by the data source preceded with the keyword "Execute". 
        /// An rdoQuery The name of one of the rdoQuery objects in the rdoConnection object's rdoQueries collection. 
        /// An rdoResultset The name of one of the rdoResultset objects in the rdoConnection object's rdoResultsets collection. 
        /// A table name The name of one of the populated rdoTable objects defined in the rdoConnection object's rdoTables collection. 
        /// Remarks
        /// The SQL property contains the structured query language statement that determines how rows are selected, grouped, and ordered when you execute 
        /// a query.You can use a query to select rows to include in an rdoResultset object. You can also define action queries to modify data without 
        /// returning rows.
        /// You cannot provide a table name at design time for the SQL property.However, you can either use a simple query like SELECT* FROM <table>, or at 
        /// runtime, populate the rdoTables collection and use one of the table names returned in the collection. The rdoTables collection is populated as 
        /// soon as it is associated with an active connection and referenced.
        /// The SQL syntax used in a query must conform to the SQL dialect as defined by the data source query processor.The SQL dialect supported by the 
        /// ODBC interface is defined by the X/Open standard.Generally, a driver scans an SQL statement looking for specific escape sequences that are used 
        /// to identify non-standard operands like timestamp literals and functions.
        /// When you need to return rows from a query, you generally provide a SELECT statement in the SQL property.The SELECT statement specifies: 
        /// •The name of each column to return or "*" to indicate all columns of the specified tables are to be returned. Ambiguous column names must be 
        /// addressed to include the table name as needed.You can also specify aggregate expressions to perform arithmetic or other functions on the columns 
        /// selected.
        /// •The name of each table that is to be searched for the information requested.If you specify more than one table, you must provide a WHERE clause 
        /// to indicate which column(s) are used to cross-reference the information in the tables.Generally, these columns have the same name and meaning. 
        /// For example the CustomerID column in the Customers table and the Orders table might be referenced.
        /// •(Optionally) a WHERE clause to specify how to join the tables specified and how to limit or filter the number and types of rows returned.You 
        /// can use parameters in the WHERE clause to specify different sets of information from query to query.
        /// •(Optionally) other clauses such as ORDER BY to set a particular order for the rows or GROUP BY to structure the rows in related sets.
        /// Each SQL dialect supports different syntax and different ancillary clauses.See the documentation provided with your remote server for more details.
        /// Specifying Parameters
        /// If the SQL statement includes question mark parameter markers (?) for the query, you must provide these parameters before you execute the query.
        /// Until you reset the parameters, the same parameter values are applied each time you execute the query.To use the rdoParameters collection to 
        /// manage SQL query parameters, you must include the "?" parameter marker in the SQL statement.Input, output, input/output and return value 
        /// parameters must all be identified in this manner.In some cases, you must use the Direction property to indicate how the parameter will be used.
        /// Note When executing stored procedures that do not require parameters, do not include the parenthesis in the SQL statement.For example, to execute 
        /// the "MySP" procedure use the following syntax: { Call MySP }.
        /// Note When using Microsoft SQL Server 6 as a data source, the ODBC driver automatically sets the Direction property.You also do not need to set 
        /// the Direction property for input parameters, as this is the default setting.
        /// If the user changes the parameter value, you can re-apply the parameter value and re-execute the query by using the Requery method against the 
        /// rdoResultset(MyRs).Cpw(0) = Text1.Text
        /// MyRs.Requery
        /// You can also specify parameters in any SQL statement by concatenating the parameters to the SQL statement string. For example, to submit a 
        /// query using this technique, you can use the following code:
        /// QSQL$ = "SELECT * FROM Authors WHERE Au_Lname = '" & Text.Text & "'"
        /// Set CPw = cn.CreateQuery("", QSQL$)
        /// Set MyRs = Cpw.OpenResultSet()
        /// In this case, the rdoParameters collection is not created and cannot be referenced. To change the query parameter, you must rebuild the SQL 
        /// statement with the new parameter value each time the query is executed, or before you use the Requery method.
        /// The SQL statement may include an ORDER BY clause to change the order of the rows returned by the rdoResultset or a WHERE clause to filter the rows.
        /// Note   You can't use the rdoTable object names until the rdoTables collection is referenced. When your code references the rdoTables collection 
        /// by enumerating one or more of its members, RDO queries the data source for table meta data. This results in population of the rdoTables 
        /// collection. This means that you cannot simply provide a table name for the value argument without first enumerating the rdoTables collection.
        /// RemoteData Control
        /// When used with the RemoteData control, the SQL property specifies the source of the data rows accessible through bound controls on your form.
        /// If you set the SQL property to an SQL statement that returns rows or to the name of an existing rdoQuery, all columns returned by the rdoResultset 
        /// are visible to the bound controls associated with the RemoteData control.
        /// After changing the value of the SQL property at run time, you must use the Refresh method to activate the change.
        /// Note Whenever your rdoQuery or SQL statement returns a value from an expression, the column name of the expression is determined by the wording 
        /// of the SQL query. In most cases you'll want to alias expressions so you know the name of the column to bind to the bound control.
        /// Make sure each bound control has a valid setting for its DataField property.If you change the setting of a RemoteData control's SQL property 
        /// and then use Refresh, the rdoResultset identifies the new object. This may invalidate the DataField settings of bound controls and cause a 
        /// trappable error.
        /// </summary>
        public string SQL
        {
            get
            {
                return sql;
            }
            set
            {
                sql = value;
                //parse to SQL provider from ODBC call syntax
                if (sql.ToUpper().Contains(" CALL "))
                {
                    this.storedProcedure = true;
                    sql = sql.TrimStart(new char[] { '{', '?', ' ', '='}).TrimEnd(new char[] { '}', ' ', '?', ',', ')', '('});
                    //remove "call "
                    sql = sql.Substring(5);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int RowsetSize
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value indicating the maximum number of rows to be returned from a query or processed in an action query.
        /// value A Long expression as described in Settings.
        /// The setting for value ranges from 0 to any number.If value is set to 0, no limit is placed on the number of rows returned (default). Setting 
        /// value to a negative number is invalid and is automatically reset to 0.
        /// Remarks
        /// The MaxRows property limits the number of rows processed by the remote server.When MaxRows is set to a value greater than 0, only 'n' rows are 
        /// processed.When executing a query that returns rows, it means that only the first 'n' rows are returned.When executing an action query, it means 
        /// that only the first 'n' rows are updated, inserted or deleted.
        /// This property is useful in situations where limited resources prohibit management of large numbers of result set rows.By setting MaxRows to 1 on 
        /// an action query, you can be assured that no more than one row will be affected by the operation.
        /// </summary>
        public int MaxRows
        {
            get;
            set;
        }

        #endregion

        #region Internal Properties

        internal SqlCommand Command;

        #endregion

        #region Public Methods

        public void Execute()
        {
            int noCountValue = -1;
            SqlCommand noCountCommand = null;
            if (this.Command != null)
            {
                this.Command.CommandText = this.SQL;
                if (QueryTimeout != -1)
                {
                    this.Command.CommandTimeout = QueryTimeout;
                }
                if (this.IsStoredProcedure())
                {
                    this.Command.CommandType = CommandType.StoredProcedure;
                }
                this.Command.Transaction = this.ActiveConnection.trans;
                if (this.Command.Parameters.Count > 0)
                {
                    FixParameters();
                }

                if (this.Command.CommandType == CommandType.StoredProcedure)
                {
                    noCountCommand = this.Command.Connection.CreateCommand();
                    noCountCommand.Transaction = this.ActiveConnection.Trans;
                    noCountCommand.CommandType = CommandType.Text;

                    noCountCommand.CommandText = "select @@OPTIONS & 512";
                    noCountValue = Convert.ToInt32(noCountCommand.ExecuteScalar());
                    noCountCommand.CommandText = "SET NOCOUNT ON";
                    noCountCommand.ExecuteNonQuery();
                }

                try
                {
                    FCRDO.rdoEngine.rdoErrors.Clear();
                    this.Command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    FCrdoError rdoError = new FCrdoError();
                    rdoError.Number = ex.Number;
                    rdoError.Description = ex.Message;
                    rdoError.Source = ex.Source;
                    FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                    throw ex;
                }

                if (this.Command.CommandType == CommandType.StoredProcedure && noCountValue == 0)
                {
                    noCountCommand.CommandText = "SET NOCOUNT OFF";
                    noCountCommand.ExecuteNonQuery();
                }
            }
        }

        public void Close()
        {
        }

        private FCrdoResultset resultset;

        /// <summary>
        /// Creates a new rdoResultset object.
        /// Set variable = connection.OpenResultset(name[, type[, locktype[, option]]])
        /// Set variable = object.OpenResultset([type[, locktype[, option]]])
        /// variable An object expression that evaluates to an rdoResultset object. 
        /// connection An object expression that evaluates to an existing rdoConnection object you want to use to create the new rdoResultset.
        /// object An object expression that evaluates to an existing rdoQuery or rdoTable object you want to use to create the new rdoResultset.
        /// name A String that specifies the source of the rows for the new rdoResultset.This argument can specify the name of an rdoTable object,
        /// the name of an rdoQuery, or an SQL statement that might return rows.
        /// type A Variant or constant that specifies the type of cursor to create as indicated in Settings.
        /// locktype A Variant or constant that specifies the type of concurrency control.If you dont specify a locktype, rdConcurReadOnly is assumed.
        /// option A Variant or constant that specifies characteristics of the new rdoResultset.
        /// Settings 
        /// •name
        /// The name argument is used when the OpenResultset method is used against the rdoConnection object, and no query has been pre-defined.In this 
        /// case, name typically contains a row-returning SQL query.The query can contain more than one SELECT statement, or a combination of action 
        /// queries and SELECT statements, but not just action queries, or a trappable error will result. See the SQL property for additional details.
        /// •Cursor type
        /// Note Not all types of cursors and concurrency are supported by every ODBC data source driver. See rdoResultset for more information. In 
        /// addition, not all types of cursor drivers support SQL statements that return more than one set of results. For example, server-side cursors 
        /// do not support queries that contain more than one SELECT statement.
        /// The type argument specifies the type of cursor used to manage the result set. If you dont specify a type, OpenResultset creates a forward-only
        /// rdoResultset. Not all ODBC data sources or drivers can implement all of the cursor types. If your driver cannot implement the type chosen, a 
        /// warning message is generated and placed in the rdoErrors collection.Use one of the following result set type constants that defines the 
        /// cursor type of the new rdoResultset object. For additional details on types of cursors, see the CursorType property.
        /// type Constant Value Description
        /// rdOpenForwardOnly 0 (Default) Opens a forward-only-type rdoResultset object. 
        /// rdOpenKeyset 1 Opens a keyset-type rdoResultset object. 
        /// rdOpenDynamic 2 Opens a dynamic-type rdoResultset object.  
        /// rdOpenStatic 3 Opens a static-type rdoResultset object. 
        /// •Concurrency LockType
        /// In order to maintain adequate control over the data being updated, RDO provides a number of concurrency options that control how other users 
        /// are granted, or refused access to the data being updated.In many cases, when you lock a particular row using one of the LockType settings, the 
        /// remote engine might also lock the entire page containing the row.If too many pages are locked, the remote engine might also escalate the page 
        /// lock to a table lock to improve overall system performance.
        /// Not all lock types are supported on all data sources.For example, for SQL Server and Oracle servers, static-type rdoResultset objects can 
        /// only support rdConcurValues or rdConcurReadOnly.For additional details on the types of concurrency, see the LockType property.
        /// locktype Constant Value Description
        /// rdConcurReadOnly 1 (Default) Read-only.
        /// rdConcurLock 2 Pessimistic concurrency.
        /// rdConcurRowVer 3 Optimistic concurrency based on row ID. 
        /// rdConcurValues 4 Optimistic concurrency based on row values.  
        /// rdConcurBatch 5 Optimistic concurrency using batch mode updates.Status values returned for each row successfully updated. 
        /// •Other options
        /// If you use the rdAsyncEnable option, control returns to your application as soon as the query is begun, but before a result set is available.
        /// To test for completion of the query, use the StillExecuting property. The rdoResultset object is not valid until StillExecuting returns False. 
        /// You can also use the QueryComplete event to determine when the query is ready to process.Until the StillExecuting property returns True, 
        /// you cannot reference any other property of the uninitialized rdoResultset object and only the Cancel and Close methods are valid.
        /// If you use the rdExecDirect option, RDO uses the SQLExecDirect ODBC API function to execute the query.In this case, no temporary stored 
        /// procedure is created to execute the query.This option can save time if you dont expect to execute the query more than a few times in the 
        /// course of your application.In addition, when working with queries that should not be run as stored procedures but executed directly, this 
        /// option is mandatory.For example, in queries that create temporary tables for use by subsequent queries, you must use the rdExecDirect option.
        /// You can use the following constants for the options argument:
        /// Constant Value Description
        /// rdAsyncEnable 32 Execute operation asynchronously.
        /// rdExecDirect 64 (Default.) Bypass creation of a stored procedure to execute the query.Uses SQLExecDirect instead of SQLPrepare and SQLExecute. 
        /// Remarks
        /// If the OpenResultset method succeeds, RDO instantiates a new rdoResultset object and appends it to the rdoResultsets collection even if no 
        /// rows are returned by the query.If the query fails to compile or execute due to a syntax error, permissions problem or other error, the 
        /// rdoResultset is not created and a trappable error is fired.The rdoResultset topic contains additional details on rdoResultset behavior and 
        /// managing the rdoResultsets collection.
        /// Note   RDO 2.0 behaves differently than RDO 1.0 in how it handles orphaned references to rdoResultset objects. When you Set a variable already 
        /// assigned to an rdoResultset object with another rdoResultset object using the OpenResultset method, the existing rdoResultset object is closed 
        /// and dropped from the rdoResultsets collection.In RDO 1.0, the existing object remained open and was left in the rdoResultsets collection.
        /// Note Before you can use the name of a base table in the name argument, you must first use the Refresh method against the rdoTables collection 
        /// to populate it.You can also populate the rdoTables collection by referencing one of its members by its ordinal number. For example, referencing 
        /// rdoTables(0) will populate the entire collection.
        /// Executing Multiple Operations on a Connection
        /// If there is an unpopulated rdoResultset pending on a data source that can only support a single operation on an rdoConnection object, you 
        /// cannot create additional rdoQuery or rdoResultset objects using the OpenResultset method, or use the Refresh method on the rdoTable object 
        /// until the rdoResultset is flushed, closed, or fully populated.For example, when using SQL Server 4.2 as a data source, you cannot create an 
        /// additional rdoResultset object until you move to the last row of the last result set of the current rdoResultset object. To populate the 
        /// result set, use the MoreResults method to move through all pending result sets, or use the Cancel or Close method on the rdoResultset to 
        /// flush all pending result sets.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="lockType"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public FCrdoResultset OpenResultset(FCRDO.ResultsetTypeConstants? type = null, FCRDO.LockTypeConstants? lockType = null, FCRDO.OptionConstants? option = null)
        {
            if (type == null)
            {
                type = this.CursorType;
            }
            if (lockType == null)
            {
                lockType = this.LockType;
            }

            resultset = new FCrdoResultset(this);
            SqlDataAdapter adapter;

            if (this.Command == null)
            {
                this.Command = new SqlCommand("", this.ActiveConnection.connection);
            }

            int noCountValue = -1;
            SqlCommand noCountCommand = null;

            //The source can be a table name, a query name, or an SQL statement that returns records 
            if (SQL.ToUpper().StartsWith("SELECT "))
            {
                //CHE: use MaxRows property to limit the result
                if (MaxRows != 0 && !SQL.ToUpper().Contains(" TOP "))
                {
                    SQL = "select top " + MaxRows + SQL.Substring(6);
                }

                //CHE: use % in wildcard instead of * used in VB6
                Regex regex = new Regex(@"(.* )(like '.*')(.*)", RegexOptions.IgnoreCase);
                foreach (Match m in regex.Matches(SQL))
                {
                    string condition = m.Groups[2].Value;
                    if (condition.Contains('*'))
                    {
                        condition = condition.Replace('*', '%');
                        SQL = m.Groups[1].Value + condition + m.Groups[3].Value;
                    }
                }

                adapter = new SqlDataAdapter(SQL, this.ActiveConnection.connection);

                //CHE: set TableName necessary in UpdateBatchBuilder; identify first "from" in string to set correct TableName
                string SourceLowercase = SQL.ToLower();

                System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(SQL, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*) where", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (match.Success && match.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                {
                    resultset.TableName = match.Groups[1].Value;
                }
                else
                {
                    System.Text.RegularExpressions.Match match1 = System.Text.RegularExpressions.Regex.Match(SQL, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*) order by", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (match1.Success && match1.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                    {
                        resultset.TableName = match1.Groups[1].Value;
                    }
                    else
                    {
                        //CHE: treat alias
                        System.Text.RegularExpressions.Match match2 = System.Text.RegularExpressions.Regex.Match(SQL, "from ([a-zA-Z0-9üöäÄÜÖß_\\. ]*) where", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        if (match2.Success && match2.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                        {
                            if (match2.Groups[1].Value.Contains(" "))
                            {
                                resultset.TableName = match2.Groups[1].Value.Substring(0, match2.Groups[1].Value.IndexOf(" "));
                            }
                            else
                            {
                                resultset.TableName = match2.Groups[1].Value;
                            }
                        }
                        else
                        {
                        }
                    }
                }
            }
            else if (this.IsStoredProcedure())
            {
                noCountCommand = this.Command.Connection.CreateCommand();
                noCountCommand.Transaction = this.ActiveConnection.Trans;
                noCountCommand.CommandType = CommandType.Text;

                noCountCommand.CommandText = "select @@OPTIONS & 512";
                noCountValue = Convert.ToInt32(noCountCommand.ExecuteScalar());
                noCountCommand.CommandText = "SET NOCOUNT ON";
                noCountCommand.ExecuteNonQuery();

                this.Command.CommandText = SQL;
                this.Command.CommandType = CommandType.StoredProcedure;
                FixParameters();

                adapter = new SqlDataAdapter(this.Command);
            }
            else
            {
                adapter = new SqlDataAdapter(string.Format("select * from {0}", SQL), this.ActiveConnection.connection);
                resultset.TableName = SQL;
            }

            if (adapter.SelectCommand == null)
            {
                //This builds the update and Delete queries for the table in the above SQL. this only works if the select is a single table. 
                SqlCommandBuilder oleDbCommandBuilder = new SqlCommandBuilder(adapter);
            }

            if (adapter.SelectCommand != null)
            {
                adapter.SelectCommand.Transaction = this.ActiveConnection.trans;
                if (this.QueryTimeout != -1)
                {
                    adapter.SelectCommand.CommandTimeout = this.QueryTimeout;
                }
            }
            
            //CHE - moved DataAdapter set, is needed in Filling event
            resultset.DataAdapter = adapter;

            if (option != FCRDO.OptionConstants.rdAsyncEnable)
            {
                try
                {
                    FCRDO.rdoEngine.rdoErrors.Clear();
                    adapter.Fill(resultset);
                }
                catch (SqlException ex)
                {
                    FCrdoError rdoError = new FCrdoError();
                    rdoError.Number = ex.Number;
                    rdoError.Description = ex.Message;
                    rdoError.Source = ex.Source;
                    FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                    throw ex;
                }
            }
            else
            {
                //CHE - Fill in different thread
                resultset.backgroundWorker1 = new BackgroundWorker();
                resultset.backgroundWorker1.DoWork += backgroundWorker1_DoWork;
                resultset.backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
                resultset.backgroundWorker1.WorkerSupportsCancellation = true; //Allow for the process to be cancelled
                resultset.backgroundWorker1.RunWorkerAsync();
            }

            resultset.Source = this.SQL;

            if (this.Command.CommandType == CommandType.StoredProcedure && noCountValue == 0)
            {
                noCountCommand.CommandText = "SET NOCOUNT OFF";
                noCountCommand.ExecuteNonQuery();
            }

            //CHE - moved DataAdapter set, is needed in Filling event
            //resultset.DataAdapter = adapter;

            this.ActiveConnection.rdoResultSets.AddResultset(resultset);

            if (SQL.Length > 256)
            {
                resultset.Name = SQL.Substring(0, 256);
            }
            else
            {
                resultset.Name = SQL;
            }

            return resultset;
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                FCRDO.rdoEngine.rdoErrors.Clear();
                resultset.DataAdapter.Fill(resultset);
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("Operation cancelled by user"))
                {
                    //A severe error occurred on the current command.  The results, if any, should be discarded.
                    //Operation cancelled by user.
                }
                else
                {
                    FCrdoError rdoError = new FCrdoError();
                    rdoError.Number = ex.Number;
                    rdoError.Description = ex.Message;
                    rdoError.Source = ex.Source;
                    FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                    throw ex;
                }
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //lblStatus.Text = "Process was cancelled";
            }
            else if (e.Error != null)
            {
                //lblStatus.Text = "There was an error running the process. The thread aborted";
            }
            else
            {
                //lblStatus.Text = "Process was completed";
            }
        }

        private void FixParameters()
        {
            //You need to define a length when specifying the varchar parameter
            foreach (SqlParameter parameter in this.Command.Parameters)
            {
                //remove the automatic given parameter name
                if (parameter.ParameterName.StartsWith("Parameter"))
                {
                    parameter.ParameterName = "";
                }
                if (parameter.SqlDbType == SqlDbType.VarChar || parameter.SqlDbType == SqlDbType.NVarChar)
                {
                    if (parameter.Size == 0)
                    {
                        parameter.Size = 50;
                    }
                }
                //CHE: fix InvalidCastException: Could not convert parameter from DateTime to Byte[]
                else if (parameter.SqlDbType == SqlDbType.Timestamp)
                {
                    parameter.SqlDbType = SqlDbType.DateTime;
                }

                if (parameter.Value == null)
                {
                    //CHE: fix null date in parameters list: in VB6 when defining Dim x as Date, x has a value of 30.12.1899 00:00:00, including in struct
                    if (parameter.SqlDbType == SqlDbType.DateTime && !parameter.IsNullable)
                    {
                        parameter.Value = Constants.MinDate;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                }
            }
            this.Command.Prepare();
        }

        private bool IsStoredProcedure()
        {
            bool ret = storedProcedure;
            if (!ret && this.Command != null)
            {
                string selCmd = this.Command.CommandText.Trim().ToUpperInvariant();
                if (!((selCmd.StartsWith("SELECT") || selCmd.StartsWith("UPDATE") || selCmd.StartsWith("INSERT") || selCmd.StartsWith("DELETE"))))
                {
                    ret = true;
                }
            }
            return ret;
        }

        #endregion
    }
}
