﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.DataBaseLayer.RDO
{
    public class FCrdoErrors
    {
        #region Private Members

        private List<FCrdoError> errors;

        #endregion

        #region Constructors

        public FCrdoErrors()
        {
            this.errors = new List<FCrdoError>();
        }

        #endregion

        #region Properties

        public int Count
        {
            get
            {
                return errors.Count;
            }
        }

        public FCrdoError this[int index]
        {
            get
            {
                return errors[index];
            }
        }

        #endregion

        #region Public Methods

        public void Add(FCrdoError rdoError)
        {
            errors.Add(rdoError);
        }

        public void Clear()
        {
            errors.Clear();
        }

        #endregion
    }
}
