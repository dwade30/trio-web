﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// rdoColumn
    /// </summary>
    public class FCrdoColumn
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCrdoResultset dataTable;

        #endregion

        #region Constructors

        public FCrdoColumn(FCrdoResultset dataTable, DataColumn objColumn)
        {
            this.dataTable = dataTable;
            this.Column = new FCrdoSerializableColumn(objColumn, dataTable);
            this.Name = objColumn.ColumnName;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        
        public enum AttributesSettings
        {
            rdAutoIncrColumn = 16,
            rdFixedColumn = 1,
            rdTimestampColumn = 64,
            rdUpdatableColumn = 32,
            rdVariableColumn = 2
        }

        #endregion

        #region Properties

        /// <summary>
        /// Database column name.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        public int ColumnSize
        {
            get;
            set;
        }

        /// <summary>
        /// 1 based
        /// </summary>
        public int OrdinalPosition
        {
            get
            {
                return this.Column.OrdinalPosition;
            }
        }

        public string SourceColumn
        {
            get
            {
                return this.BaseColumnName;
            }
        }

        public object Value
        {
            get
            {
                //SBE: if RowState is Deleted Exception is thrown
                Column.Table.IsEOFandBOFchecked = true;
                if (Column == null || Column.Table.BOF || Column.Table.EOF || Column.Table.Status == DataRowState.Deleted)
                {
                    Column.Table.IsEOFandBOFchecked = false;
                    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                }
                object value = Column.Table.CurrentRow[Column.ColumnName];
                Column.Table.IsEOFandBOFchecked = false;
                return value;
            }
            set
            {
                FCrdoResultset dt = Column.Table;

                if (Column == null || dt.EOF || Column.ReadOnly) return;
                if (this.Type == FCRDO.DataTypeConstants.rdTypeBIT && value != null)
                {
                    dt.CurrentRow[Column.ColumnName] = FCGlobal.BoolFromString(value.ToString());
                }
                else
                {
                    DataRow dr = dt.CurrentRow;

                    //if (value == "")
                    //{
                    //    switch (this.Type)
                    //    {
                    //        case Convert.ToInt32(SqlDbType.Char:
                    //        case Convert.ToInt32(SqlDbType.VarChar:
                    //        case Convert.ToInt32(SqlDbType.NChar:
                    //        case Convert.ToInt32(SqlDbType.NVarChar:
                    //            {
                    //                value = new string(' ', this.DefinedSize); 
                    //                break;
                    //            }
                    //    }
                    //}

                    //CNA: field does not accepts null, use DBNull instead
                    //dr[Column.ColumnName] = value;
                    //dr[Column.ColumnName] = value != null ? value : DBNull.Value;
                    // JSP: If Value is null then set Value = dbnull
                    if (value == null)
                    {
                        dr[Column.ColumnName] = DBNull.Value;
                    }
                    else
                    {
                        //CHE: if field is NChar (adWChar) when setting value fill with empty spaces if DefindeSize is set
                        //if (value != null && DefinedSize > 0 && this.Type == Convert.ToInt32(SqlDbType.NChar && value.ToString().Length < DefinedSize)
                        //{
                        //    dr[Column.ColumnName] = value + new string(' ', DefinedSize - value.ToString().Length);
                        //}
                        //else 
                        if (dt.Columns[Column.ColumnName].DataType == typeof(DateTime))
                        {
                            string cellValue = FCConvert.ToString(value);
                            if (string.IsNullOrEmpty(cellValue))
                            {
                                dr[Column.ColumnName] = DBNull.Value;
                            }
                            else
                            {
                                dr[Column.ColumnName] = DateTime.Parse(cellValue);
                            }
                        }
                        else
                        {
                            //CHE: throw exception when setting a string field with some value larger than its size
                            if (dt.Columns[Column.ColumnName].DataType == typeof(String) && this.Size > 0 && value.ToString().Length > this.Size)
                            {
                                throw new Exception("The field is too small to accept the amount of data you attempted to add");
                            }
                            dr[Column.ColumnName] = value;
                        }
                    }
                }
            }
        }


        /// <summary>
        /// property neeeded to avoid exception when comparing Fields(..).Value with some value such as integer or boolean (cannot convert DBNull to...)
        /// </summary>
        public object ValueNullSafe
        {
            get
            {
                if (this.Value != DBNull.Value)
                {
                    return this.Value;
                }

                switch (this.Type)
                {
                    case FCRDO.DataTypeConstants.rdTypeDATE:
                    case FCRDO.DataTypeConstants.rdTypeTIME:
                    case FCRDO.DataTypeConstants.rdTypeTIMESTAMP:
                        {
                            return DateTime.FromOADate(0);
                        }
                    case FCRDO.DataTypeConstants.rdTypeNUMERIC:
                    case FCRDO.DataTypeConstants.rdTypeDECIMAL:
                    case FCRDO.DataTypeConstants.rdTypeINTEGER:
                    case FCRDO.DataTypeConstants.rdTypeSMALLINT:
                    case FCRDO.DataTypeConstants.rdTypeFLOAT:
                    case FCRDO.DataTypeConstants.rdTypeREAL:
                    case FCRDO.DataTypeConstants.rdTypeDOUBLE:
                    case FCRDO.DataTypeConstants.rdTypeBIGINT:
                    case FCRDO.DataTypeConstants.rdTypeTINYINT:
                        {
                            return 0;
                        }
                    case FCRDO.DataTypeConstants.rdTypeBIT:
                        {
                            return false;
                        }
                    case FCRDO.DataTypeConstants.rdTypeCHAR:
                    case FCRDO.DataTypeConstants.rdTypeVARCHAR:
                    case FCRDO.DataTypeConstants.rdTypeLONGVARCHAR:
                        return "";
                    case FCRDO.DataTypeConstants.rdTypeBINARY:
                    case FCRDO.DataTypeConstants.rdTypeVARBINARY:
                    case FCRDO.DataTypeConstants.rdTypeLONGVARBINARY:
                    default:
                        return this.Value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Size
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public FCRDO.DataTypeConstants Type
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public AttributesSettings Attributes
        {
            // TODO:CHE
            get;
            set;
        }

        public bool ChunkRequired
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Required
        {
            get;
            set;
        }

        #endregion

        #region Internal Properties

        internal string DefaultValue
        {
            get;
            set;
        }

        internal string BaseTableName
        {
            get;
            set;
        }

        internal string BaseColumnName
        {
            get;
            set;
        }

        internal bool IsAliased
        {
            get;
            set;
        }

        internal FCrdoSerializableColumn Column
        {
            get;
            set;
        }

        internal string ColumnName
        {
            get
            {
                // return the column name of the column object

                //return basecolumnname and not the automatically generated name
                //e.g. for 2 tables in join having same column NR, C# will generate NR and NR1, but in VB6 both columns have ColumnName NR
                if (!string.IsNullOrEmpty(BaseColumnName) && !IsAliased)
                {
                    return BaseColumnName;
                }
                return Column.ColumnName;
            }
            set
            {
                // set the value of the column name
                Column.ColumnName = value;

            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// User-defined operator -
        /// </summary>
        /// <param name="field1"></param>
        /// <param name="field2"></param>
        /// <returns></returns>
        public static Double operator -(FCrdoColumn field1, FCrdoColumn field2)
        {
            return (Convert.IsDBNull(field1.Value) ? 0 : Convert.ToDouble(field1.Value)) - (Convert.IsDBNull(field2.Value) ? 0 : Convert.ToDouble(field2.Value));
        }

        /// <summary>
        /// User-defined operator +
        /// + operator retuns an object, Conversion is required if the result is assigned to a known type
        /// In VB6 the operation is evaluated, and the result is converted to the required type
        /// "0001" + "0002" will return "00010002" if result is assigned to a String or 10002 if result is assigned to a number
        /// "0001" + 2 will return "3" if result is assigned to a String or 3 if result is assigned to a number
        /// 1 + "0002" will return "3" if result is assigned to a String or 3 if result is assigned to a number
        /// 1 + 2 will return "3" if result is assigned to a String or 3 if result is assigned to a number
        /// </summary>
        /// <param name="column1"></param>
        /// <param name="column2"></param>
        /// <returns></returns>
        public static object operator +(FCrdoColumn column1, FCrdoColumn column2)
        {
            //SBE: if booth operands are one of the string SqlDbType(Char/NChar/VarChar/NVarChar/Text/NText), operator will return string concatenation
            if ((column1.Type == FCRDO.DataTypeConstants.rdTypeCHAR || column1.Type == FCRDO.DataTypeConstants.rdTypeVARCHAR)
                && (column2.Type == FCRDO.DataTypeConstants.rdTypeCHAR || column2.Type == FCRDO.DataTypeConstants.rdTypeVARCHAR
                ))
            {
                return FCConvert.ToString(column1.Value) + FCConvert.ToString(column2.Value);
            }
            else
            {
                return (Convert.IsDBNull(column1.Value) ? 0 : Convert.ToDouble(column1.Value)) + (Convert.IsDBNull(column2.Value) ? 0 : Convert.ToDouble(column2.Value));
            }
        }

        /// <summary>
        /// User-defined conversion from FCrdoColumn to double
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static implicit operator double(FCrdoColumn FCrdoColumn)
        {
            if (Convert.IsDBNull(FCrdoColumn.Value)) return 0;
            return Convert.ToDouble(FCrdoColumn.Value);
        }

        /// <summary>
        /// User-defined conversion from FCrdoColumn to decimal
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static implicit operator decimal(FCrdoColumn FCrdoColumn)
        {
            if (Convert.IsDBNull(FCrdoColumn.Value)) return 0;
            return Convert.ToDecimal(FCrdoColumn.Value);
        }

        /// <summary>
        /// User-defined conversion from FCrdoColumn to int
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static implicit operator int(FCrdoColumn FCrdoColumn)
        {
            if (Convert.IsDBNull(FCrdoColumn.Value)) return 0;
            return Convert.ToInt32(FCrdoColumn.Value);
        }

        /// <summary>
        /// User-defined conversion from FCrdoColumn to short
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static implicit operator short(FCrdoColumn FCrdoColumn)
        {
            if (Convert.IsDBNull(FCrdoColumn.Value)) return 0;
            return Convert.ToInt16(FCrdoColumn.Value);
        }

        /// <summary>
        /// User-defined conversion from FCrdoColumn to DateTime
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static implicit operator DateTime(FCrdoColumn FCrdoColumn)
        {
            if (Convert.IsDBNull(FCrdoColumn.Value)) return DateTime.Parse("30/12/1899 00:00:00");
            return Convert.ToDateTime(FCrdoColumn.Value);
        }

        /// <summary>
        /// User-defined conversion from FCrdoColumn to string
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static implicit operator string(FCrdoColumn FCrdoColumn)
        {
            if (Convert.IsDBNull(FCrdoColumn.Value)) return "";
            return FCConvert.ToString(FCrdoColumn.Value);
        }

        /// <summary>
        /// User-defined conversion from FCrdoColumn to bool
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static implicit operator bool(FCrdoColumn FCrdoColumn)
        {
            if (Convert.IsDBNull(FCrdoColumn.Value)) return false;
            return Convert.ToBoolean(FCrdoColumn.Value);
        }

        /// <summary>
        /// overload operator < 
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator <(FCrdoColumn FCrdoColumn, int value)
        {
            return GetColumnValueAsDouble(FCrdoColumn) < value;
        }

        /// <summary>
        /// overload operator > 
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator >(FCrdoColumn FCrdoColumn, int value)
        {
            return GetColumnValueAsDouble(FCrdoColumn) > value;
        }

        /// <summary>
        /// overload operator /
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object operator /(FCrdoColumn FCrdoColumn, int value)
        {
            return GetColumnValueAsDouble(FCrdoColumn) / value;
        }

        /// <summary>
        /// overload operator == 
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator ==(FCrdoColumn FCrdoColumn, int value)
        {
            return GetColumnValueAsDouble(FCrdoColumn) == value;
        }

        /// <summary>
        /// overload operator != 
        /// </summary>
        /// <param name="FCrdoColumn"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator !=(FCrdoColumn FCrdoColumn, int value)
        {
            return GetColumnValueAsDouble(FCrdoColumn) != value;
        }

        /// <summary>
        /// overload operator == 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static bool operator ==(int value, FCrdoColumn FCrdoColumn)
        {
            return GetColumnValueAsDouble(FCrdoColumn) == value;
        }

        /// <summary>
        /// overload operator != 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="FCrdoColumn"></param>
        /// <returns></returns>
        public static bool operator !=(int value, FCrdoColumn FCrdoColumn)
        {
            return GetColumnValueAsDouble(FCrdoColumn) != value;
        }
        
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private static double GetColumnValueAsDouble(FCrdoColumn column)
        {
            return Convert.IsDBNull(column.Value) ? 0 : Convert.ToDouble(column.Value);
        }
        
        #endregion
    }
}
