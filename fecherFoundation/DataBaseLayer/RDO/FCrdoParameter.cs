﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// rdoParameter
    /// </summary>
    public class FCrdoParameter
    {
        #region Private Members

        private FCrdoQuery query;
        private SqlParameter parameter;

        #endregion

        #region Constructors

        public FCrdoParameter(FCrdoQuery query)
        {
            this.query = query;
            this.parameter = new SqlParameter();
            if (this.query.Command == null)
            {
                this.query.Command = new SqlCommand("", this.query.ActiveConnection.connection);
                if (query.QueryTimeout != -1)
                {
                    this.query.Command.CommandTimeout = query.QueryTimeout;
                }
            }
            this.query.Command.Parameters.Add(this.parameter);
        }

        #endregion

        #region Properties

        public object Value
        {
            get
            {
                return parameter.Value;
            }
            set
            {
                parameter.Value = value;
            }
        }

        /// <summary>
        /// Type property on the rdoParameter object to indicate the datatype of a specific procedure argumen
        /// </summary>
        public FCRDO.DataTypeConstants Type
        {
            set
            {
                parameter.SqlDbType = FCRDO.ConvertToSqlType(value);
            }
        }

        public FCRDO.DirectionConstants Direction
        {
            set
            {
                parameter.Direction = FCRDO.ConvertToParameterDirection(value);
            }
        }

        public string ParameterName
        {
            get
            {
                return parameter.ParameterName;
            }
            set
            {
                parameter.ParameterName = value;
            }
        }

        #endregion
    }
}
