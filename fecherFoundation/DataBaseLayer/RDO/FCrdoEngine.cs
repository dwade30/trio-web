﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.DataBaseLayer.RDO
{
    public class FCrdoEngine
    {
        #region Public Members

        public FCrdoErrors rdoErrors = new FCrdoErrors();

        #endregion

        #region Private Members

        private FCrdoEnvironments fcrdoEnvironments;

        #endregion

        #region Properties

        public FCrdoEnvironments rdoEnvironments
        {
            get
            {
                if (fcrdoEnvironments == null)
                {
                    fcrdoEnvironments = new FCrdoEnvironments(this);
                }
                return fcrdoEnvironments;
            }
            set
            {
                fcrdoEnvironments = value;
            }
        }

        public FCRDO.CursorDriverConstants rdoDefaultCursorDriver
        {
            get;
            set;
        }

        public string rdoDefaultUser
        {
            get;
            set;
        }

        public string rdoDefaultPassword
        {
            get;
            set;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates a new rdoEnvironment object.
        /// Set variable = rdoCreateEnvironment(name, user, password)
        /// variable An object expression that evaluates to an rdoEnvironment object. 
        /// name A String variable that uniquely names the new rdoEnvironment object. See the Name property for details on valid rdoEnvironment names.
        /// user A String variable that identifies the owner of the new rdoEnvironment object. See the UserName property for more information.
        /// password A String variable that contains the password for the new rdoEnvironment object. The password can be up to 14 characters long and 
        /// can include any characters except ASCII character 0 (null). 
        /// Remarks
        /// The rdoEnvironment object defines a transaction, user, and password context.When the rdoEngine is initialized, a default rdoEnvironments(0) is created 
        /// automatically with the rdoDefaultUser and rdoDefaultPassword user name and password.If you need to define alternate transaction scopes that contain 
        /// specific rdoConnection objects, or specific users, use the rdoCreateEnvironment method and specify the specific users for the environment.You can then 
        /// open connections against this new environment.
        /// Unlike the other methods you use to create Remote Data Objects, rdoCreateEnvironment requires that you provide all of its parts.If you dont provide all of 
        /// the parts, the object wont be added to the collection.In addition, rdoEnvironment objects arent permanent and cant be saved.Once you create an rdoEnvironment 
        /// object, you can only modify the UserName and Timeout property settings.
        /// You dont have to append the new rdoEnvironment object to a collection before you can use it it is automatically appended to the rdoEnvironments collection.
        /// If name refers to an object that is already a member of the rdoEnvironments collection, a trappable error occurs.
        /// Once you use rdoCreateEnvironment to create a new rdoEnvironment object, an rdoEnvironment session is started, and you can refer to the rdoEnvironment 
        /// object in your application.
        /// To remove an rdoEnvironment object from the rdoEnvironments collection, use the Close method on the rdoEnvironment object. You cannot remove rdoEnvironments(0).
        /// </summary>
        /// <param name="name"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public FCrdoEnvironment rdoCreateEnvironment(string name, string user, string password)
        {
            FCrdoEnvironment rdoEnvironment = new FCrdoEnvironment(name, user, password);
            this.rdoEnvironments.Add(rdoEnvironment);
            return rdoEnvironment;
        }

        #endregion
    }
}
