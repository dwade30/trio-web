﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// rdoTable
    /// </summary>
    public class FCrdoTable : FCrdoResultset
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors

        public FCrdoTable(FCrdoConnection rdoConnection) : base(rdoConnection)
        {
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Database table name once the rdoTables collection is populated.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// For an rdoTable object, the Type property returns a String. The settings for value are determined by the data source driver.
        /// Typically, this string value is "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY". "LOCAL TEMPORARY", "ALIAS", "SYNONYM" or some other data source-specific type identifier.
        /// </summary>
        public string Type
        {
            get;
            set;
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods

        internal FCrdoColumn CreateColumn(string name = "", FCRDO.DataTypeConstants type = FCRDO.DataTypeConstants.rdTypeVARCHAR, int? size = 0)
        {
            DataColumn newColumn = new DataColumn(name);
            Type colType = ConvertToType(type);
            newColumn.DataType = colType;

            if (size.HasValue && size.Value > 0)
            {
                if (newColumn.DataType == typeof(System.String))
                {
                    newColumn.MaxLength = size.Value;
                    newColumn.DefaultValue = "";
                }
            }

            if (colType.IsValueType)
            {
                newColumn.DefaultValue = Activator.CreateInstance(colType);
            }

            this.Columns.Add(newColumn);
            FCrdoColumn newField = new FCrdoColumn(this, newColumn);
            newField.Size = base.GetFieldSize(size, type);

            //CHE: set type to be used when filling with spaces in Value setter
            newField.Type = type;

            return newField;
        }

        internal static Type ConvertToType(FCRDO.DataTypeConstants dataType)
        {
            switch (dataType)
            {
                case FCRDO.DataTypeConstants.rdTypeCHAR:
                case FCRDO.DataTypeConstants.rdTypeVARCHAR:
                    return typeof(System.String);

                case FCRDO.DataTypeConstants.rdTypeBIT:
                    return typeof(System.Boolean);

                case FCRDO.DataTypeConstants.rdTypeSMALLINT:
                    return typeof(System.Int16);

                case FCRDO.DataTypeConstants.rdTypeINTEGER:
                    return typeof(System.Int32);

                case FCRDO.DataTypeConstants.rdTypeBIGINT:
                case FCRDO.DataTypeConstants.rdTypeLONGVARBINARY:
                    return typeof(System.Int64);

                //CHE: use decimal otherwise number format will not work
                case FCRDO.DataTypeConstants.rdTypeFLOAT:
                case FCRDO.DataTypeConstants.rdTypeDECIMAL:
                case FCRDO.DataTypeConstants.rdTypeDOUBLE:
                case FCRDO.DataTypeConstants.rdTypeNUMERIC:
                    return typeof(System.Decimal);

                case FCRDO.DataTypeConstants.rdTypeBINARY:
                case FCRDO.DataTypeConstants.rdTypeVARBINARY:
                    return typeof(System.Byte[]);

                case FCRDO.DataTypeConstants.rdTypeDATE:
                case FCRDO.DataTypeConstants.rdTypeTIME:
                case FCRDO.DataTypeConstants.rdTypeTIMESTAMP:
                    return typeof(System.DateTime);

                default:
                    return null;
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
