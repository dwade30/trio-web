﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.DataBaseLayer.RDO
{
    public class FCrdoError
    {
        #region Public Members

        public int Number;
        public string Description = string.Empty;
        public string Source = string.Empty;
        public string SQLState = string.Empty;

        #endregion
    }
}
