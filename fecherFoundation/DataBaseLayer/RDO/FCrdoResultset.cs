﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// rdoResultset
    /// </summary>
    public class FCrdoResultset : DataTable
    {
        #region Public Members

        public BackgroundWorker backgroundWorker1;

        #endregion

        #region Internal Members

        internal DbDataAdapter DataAdapter = null;
        internal DataGridView ParentGrid;
        internal List<DataRow> NewRows;
        internal string Source;

        #endregion

        #region Private Members

        private int mintIndex = 1;
        internal int mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosUnknown);
        private bool mblnDeleted = false;
        private DataView mobjView = null;
        private FCrdoColumns columns;
        private bool isEOFandBOFchecked = false;
        private bool SkipFields_i = false;
        private SqlCommandBuilder builder = null;
        private int queryTimeOut = -1;

        #endregion

        #region Constructors

        public FCrdoResultset(FCrdoConnection rdoConnection)
        {
            this.ActiveConnection = rdoConnection;
            queryTimeOut = rdoConnection.QueryTimeout;
        }

        public FCrdoResultset(FCrdoQuery rdoQuery)
        {
            this.ActiveConnection = rdoQuery.ActiveConnection;
            queryTimeOut = rdoQuery.QueryTimeout;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum MoveDirection
        {
            First,
            Next,
            Previous,
            Last
        }

        public enum PositionEnum : int
        {
            adPosBOF = -2,
            adPosEOF = -3,
            adPosUnknown = -1,
        }

        #endregion

        #region Properties

        public bool Updatable
        {
            get;
            set;
        }

        /// <summary>
        /// First 256 characters of the SQL query.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Holds the current position of the DataTable (bookmark)
        /// </summary>
        public int Index
        {
            get
            {
                return mintIndex;
            }
            set
            {
                mblnDeleted = false;
                mintIndex = value;
                mintAbsolutePosition = value;
            }
        }

        public int AbsolutePosition
        {
            get
            {
                return mintAbsolutePosition;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException();
                }

                int intRecordCount;
                if (this.View == null)
                {
                    intRecordCount = this.Rows.Count;
                }
                else
                {
                    intRecordCount = this.View.Count;
                }
                if (value > intRecordCount)
                {
                    value = intRecordCount;
                }
                this.Index = value;
            }
        }

        public int Bookmark
        {
            get
            {
                if (Deleted)
                {
                    throw new Exception("Row handle refered to a deleted row or a row marked for deletion.");
                }

                DataRow currentRow = CurrentRow;

                int index = this.Rows.IndexOf(currentRow);

                //BAN: the current row can be present just in the DefaultView and not also in the DataTable, cases like adding a new row but not updating
                if (index == -1)
                {
                    if (this.HasNewRows())
                    {
                        // if a new row has been added with AddNew function, that the index is already set correctly
                        index = NewRows.IndexOf(currentRow);
                        return Index;
                    }
                }
                //BAN: the current row can be present just in the DefaultView and not also in the DataTable, cases like adding a new row but not updating
                //the data table. In these cases, the index will be -1, but these cases occur just for the last row of the DefaultView so it should be returned
                //with -1 because the index will be incremented by one when returning
                if (index == -1 && currentRow.RowState == DataRowState.Detached)
                {
                    index = this.DefaultView.Count - 1;
                }
                //If the row is not present in the DefaultView, then the row must have been added with AddNew in the NewRows collection
                if (index == -1 && NewRows.Count > 0)
                {
                    index = NewRows.Count - 1;
                }

                return index + 1;
            }
            set
            {
            }
        }

        /// <summary>
        /// Returns a Boolean value that indicates whether a query is still executing.
        /// True The query is still executing. 
        /// False The query is ready to return the result set.
        /// Remarks
        /// Use the StillExecuting property to determine if a query is ready to return the first result set. Until the StillExecuting property is False, 
        /// the associated object cannot be accessed. However, unless you use the rdAsyncEnable option, your application will block until the query is 
        /// completed and ready to process the result set.
        /// Once the StillExecuting property returns False, the first or next result set is ready for processing.When you use the MoreResults method to 
        /// complete processing of a result set, the StillExecuting property is reset to True while the next result sets is retrieved.
        /// The StillExecuting property also changes to True when you execute a Move method.For example executing MoveLast against an rdoResultset resets 
        /// the StillExecuting property to True as long as RDO continues to fetch rows from the remote server.
        /// You can also use the QueryComplete event to indicate when a query has completed and the associated rdoResultset object is ready to process.
        /// Use the Cancel method to terminate processing of an executing query, including all statements in a batch query.
        /// </summary>
        public bool StillExecuting
        {
            get
            {
                if (backgroundWorker1 != null)
                {
                    return backgroundWorker1.IsBusy;
                }
                return false;
            }
        }

        public FCrdoConnection ActiveConnection
        {
            get;
            set;
        }

        public bool BOF
        {
            get
            {
                bool BOF = false;
                if (View == null)
                {
                    //CHE: if you call movelast and there is no rows in view bof should return true
                    BOF = (this.Rows.Count == 0 || Index == 0 || (Index > this.Rows.Count && this.RowCount == 0));
                }
                else
                {
                    //IPI: if a Filter was set, check if the current row is a new added row in the DataTable
                    BOF = (View.Count == 0 || Index == 0);
                }

                if (BOF)
                {
                    //SBE: BOF should return false when View Count is 0, and row was deleted
                    return !(IsNewRowAdded() || Deleted);
                }
                return BOF;
            }
        }

        public bool EOF
        {
            get
            {
                bool EOF = false;
                if (View == null)
                {
                    //CHE: if you call movefirst and there is no rows in view eof should return true
                    EOF = (this.Rows.Count == 0 || Index > this.Rows.Count || (Index == 0 && this.RowCount == 0));
                }
                else
                {
                    EOF = (View.Count == 0 || Index > View.Count);
                }

                if (!EOF)
                {
                    return EOF;
                }

                if (View == null)
                {
                    if (this.IsNewRowAdded())
                    {
                        if (this.HasNewRows())
                        {
                            if ((this.Rows.Count + NewRows.Count) >= Index)
                            {
                                return false;
                            }
                        }
                        else if (this.Rows.Count >= Index)
                        {
                            return false;
                        }
                        //SBE: check counts and Index on default view, when current row is the last added row
                        else if (this.DefaultView.Count >= Index)
                        {
                            return false;
                        }

                    }
                }
                else
                {
                    if (this.IsNewRowAdded())
                    {
                        if (this.HasNewRows())
                        {
                            if ((this.Rows.Count + NewRows.Count) >= Index)
                            {
                                return false;
                            }
                        }
                        else if (View.Count >= Index)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //BAN: If the Index is greater than the DataView row count it means that the DataView has a filter applied
                        //Check if the DataRow at the specified Index corresponds to a DataRowView in the DataView. If the DataView has the row
                        //then EOF is false, if it does not contain it, EOF is true
                        int currentIdx = Index;
                        bool found = false;
                        if (currentIdx > 0 && currentIdx <= this.Rows.Count)
                        {
                            DataRow dataRow = this.Rows[currentIdx - 1];
                            foreach (DataRowView dataRowView in View)
                            {
                                if (dataRowView.Row == dataRow)
                                {
                                    found = true;
                                    break;
                                }
                            }

                            EOF = !found;

                            //IPI: if the Index is on a new row, EOF should return false 
                            if (EOF && this.HasNewRows())
                            {
                                if ((this.Rows.Count + NewRows.Count) >= Index)
                                {
                                    return false;
                                }
                            }

                            //SBE: If RowState is Deleted, then EOF should return false (current Index is lower than rows count)
                            if (dataRow.RowState == DataRowState.Deleted)
                            {
                                EOF = false;
                            }
                        }
                    }
                }

                return EOF;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public FCrdoColumns rdoColumns
        {
            get
            {
                if (columns == null)
                {
                    columns = new FCrdoColumns(this);
                    if (SkipFields == false)
                    {
                        //set fields properties
                        SetFields();
                    }
                }
                return columns;
            }
            set
            {
                columns = value;
            }
        }

        /// <summary>
        /// When set to true the field information fo the record is not loaded
        /// Set only true, when recordset is not used for update or insert
        /// </summary>
        public bool SkipFields
        {
            get
            {
                return SkipFields_i;
            }
            set
            {
                SkipFields_i = value;
            }
        }

        public int RowCount
        {
            get
            {
                if (View == null)
                {
                    //CHE: not counting deleted rows unless corresponding RowStateFilter is set (cannot use DefaultView because it doesn't return as we wish) 
                    //return thisRows.Count;
                    return DefaultViewRowsCountWithState();
                }
                else
                {
                    ////CHE: for filter pending add also those from NewRows
                    //if (FilterType == FilterGroupEnum.adFilterPendingRecords)
                    //{
                    //    return View.Count + (HasNewRows() ? NewRows.Count : 0);
                    //}

                    return View.Count;
                }
            }
        }        

        public FCrdoColumn this[int index]
        {
            get
            {
                return this.rdoColumns[index];
            }
            set
            {
                this.rdoColumns[index] = value;
            }
        }

        public FCrdoColumn this[string name]
        {
            get
            {
                return this.rdoColumns[name];
            }
            set
            {
                this.rdoColumns[name] = value;
            }
        }

        #endregion

        #region Internal Properties

        internal DataRowState Status
        {
            get
            {
                //CHE: for deleted CurrentRow will throw exception
                if (Deleted)
                {
                    return DataRowState.Deleted;
                }
                else if (isEOFandBOFchecked || (!isEOFandBOFchecked && !this.EOF && !this.BOF))
                {
                    isEOFandBOFchecked = true;
                    DataRowState rowState;
                    //CHE: with ADO VB6 recordset there is no Detached status, return as Added status
                    if (this.CurrentRow.RowState == DataRowState.Detached)
                    {
                        rowState = DataRowState.Added;
                    }
                    rowState = this.CurrentRow.RowState;
                    isEOFandBOFchecked = false;
                    return rowState;
                }
                return DataRowState.Unchanged;
            }
        }

        internal FCrdoColumn IdentityColumn
        {
            get;
            set;
        }

        internal bool IsEOFandBOFchecked
        {
            get
            {
                return isEOFandBOFchecked;
            }
            set
            {
                isEOFandBOFchecked = value;
            }
        }

        internal bool Deleted
        {
            get
            {
                return mblnDeleted;
            }
            set
            {
                mblnDeleted = value;
            }
        }

        internal DataRow CurrentRow
        {
            get
            {
                if (!isEOFandBOFchecked && (EOF || BOF))
                {
                    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                }

                DataRow oRow = null;

                try
                {
                    //return the current row
                    if (View == null)
                    {
                        if (Index > this.Rows.Count && this.HasNewRows())
                        {
                            oRow = NewRows[Index - this.Rows.Count - 1];
                        }
                        else
                        {
                            if (this.Rows.Count >= Index)
                            {
                                oRow = this.Rows[Index - 1];
                            }
                            else if (this.DefaultView != null && this.DefaultView.Count >= Index)
                            {
                                oRow = this.DefaultView[Index - 1].Row;
                            }
                        }
                    }
                    else
                    {
                        if (Index > View.Count && this.HasNewRows() && Index > this.Rows.Count)
                        {
                            oRow = NewRows[Index - this.Rows.Count - 1];
                        }
                        else
                        {
                            //CHE: Index is on DataTable, not on View
                            if (Index - 1 >= 0 && Index - 1 < this.Rows.Count)
                            {
                                oRow = this.Rows[Index - 1];
                            }
                            //BAN: If index does not correspond to DataTable, return the row from the view
                            else
                            {
                                oRow = View[Index - 1].Row;
                            }
                        }
                    }
                }

                catch (System.Exception e)
                {
                    throw e;
                }

                return oRow;
            }
        }

        internal DataView View
        {
            get
            {
                return mobjView;
            }
            set
            {
                mblnDeleted = false;
                mobjView = value;
            }
        }

        #endregion

        #region Public Methods

        public DataRow AddNew()
        {
            //NewRow will generate TableNewRow event in which the Synchronize should not be executed
            //suppressCloneSynchronization = true;
            //IPI, SBE: when AddNew is called in the grid's AfterUpdate handler, must check if the grid already has a new row in DefaultView. Check if ParentGrid is set
            //DataRow objRow = this.NewRow();
            DataRow objRow = null;
            DataGridView grid = ParentGrid;
            if (grid != null && this.DefaultView.Count > 0 && this.DefaultView[this.DefaultView.Count - 1].IsNew)
            {
                objRow = this.DefaultView[this.DefaultView.Count - 1].Row;
            }
            else
            {
                //CHE: do not add in DefaultView unless grid is attached
                //if (this.DefaultView.AllowNew)
                if (this.DefaultView.AllowNew && grid != null)
                {
                    //SBE: set row empty values for last row before add new row
                    if (this.DefaultView.Count > 0)
                    {
                        DataRow lastRow = this.DefaultView[this.DefaultView.Count - 1].Row;
                        this.SetRowEmptyValues(lastRow);
                        //IPI: when below call DefaultView.AddNew is executed, lastRow will be committed to the Rows collection
                        // if a row is in the DataTable.Rows collection, it should be removed from NewRows
                        if (NewRows != null && NewRows.Contains(lastRow))
                        {
                            NewRows.Remove(lastRow);
                        }
                    }
                    //IPI: if DataTable is bound to a DGV, the current cell must reflect the new row in the DefaultView
                    DataRowView rowView = this.DefaultView.AddNew();
                    objRow = rowView.Row;

                    if (grid != null && grid.CurrentCell != null)
                    {
                        foreach (DataGridViewRow row in grid.Rows)
                        {
                            if (row.DataBoundItem != null && ((DataRowView)row.DataBoundItem) == rowView)
                            {
                                grid.CurrentCell = grid[grid.CurrentCell.ColumnIndex, grid.Rows.IndexOf(row)];
                                break;
                            }
                        }
                    }
                }
                else
                {
                    objRow = this.NewRow();
                }
            }

            //suppressCloneSynchronization = false;

            //CHE: AddNew on DataTable with mandatory fields will raise exception if new empty row is added in collection, must be added later on Update
            if (NewRows == null)
            {
                NewRows = new List<DataRow>();
            }

            NewRows.Add(objRow);

            //CHE: do not add in DefaultView unless grid is attached, move index on last row 
            //Index = this.Rows.Count + 1;
            Index = this.Rows.Count + NewRows.Count;
            //SynchronizeClones(this, Synchronization.DetachedNewRow, objRow);
            return objRow;
        }

        /// <summary>
        /// Enables changes to data values in the current row of an updatable rdoResultset object.
        /// Before you use the Edit method, the data columns of an rdoResultset are read-only.Executing the Edit method copies the current row from an 
        /// updatable rdoResultset object to the copy buffer for subsequent editing. Changes made to the current rows columns are copied to the copy 
        /// buffer.After you make the desired changes to the row, use the Update method to save your changes or the CancelUpdate method to discard 
        /// them.The current row remains current after you use Edit.
        /// Caution If you edit a row, and then perform any operation that repositions the current row pointer to another row without first using 
        /// Update, your changes to the edited row are lost without warning.In addition, if you close object, or end the procedure which declares the 
        /// result set or the parent rdoConnection object, your edited row might be discarded without warning.
        /// You cannot use the Edit method if the EditMode property of the rdoResultset object indicates that an Edit or AddNew operation is in progress.
        /// When the rdoResultset objects LockEdits property setting is True (pessimistically locked) , all rows in the rdoResultset objects rowset 
        /// are locked as soon as the cursor is opened and remain locked until the cursor is closed.The number of rows in the rowset is determined by 
        /// the RowsetSize property.Since many remote data sources use page locking schemes, pessimistic locking also locks all data pages of the 
        /// table(s) containing a row fetched by the rdoResultset.
        /// If the LockEdits property setting is False(optimistically locked), the individual row or the data page containing the row is locked and 
        /// the new row is compared with the pre-edited row just before its updated in the database.If the row has changed since you last used the 
        /// Edit method, the Update operation fails with a trappable error.
        /// Note Not all data sources use page locking schemes to manage data concurrency.In some cases, data is locked on a row-by-row basis, 
        /// therefore locks only affect the specific rowset being edited.
        /// Using Edit produces an error under any of the following conditions: 
        /// •There is no current row.
        /// •The connection or rdoResultset is read-only.
        /// •No columns in the row are updatable.
        /// •The EditMode property indicates that an AddNew or Edit is already in progress.
        /// •Another user has locked the row or data page containing your row and the LockEdits property is True.
        /// </summary>
        public void Edit()
        {
        }

        /// <summary>
        /// Saves the contents of the copy buffer row to a specified updatable rdoResultset object and discards the copy buffer.
        /// Use Update to save the current row and any changes youve made to it to the underlying database table(s). Changes you make to the rdoResultset 
        /// after using the AddNew or Edit methods can be lost if you do not use the Update method before the application ends.
        /// Note When you use the ClientBatch cursor library, all updates to the base tables are deferred until you use the BatchUpdate method.In this 
        /// case, the Update method updates the local rdoResultset, but does not update the base tables.These changes can be lost if the application 
        /// ends before the BatchUpdate method has been completed.
        /// Changes to the current row are lost if: 
        /// •You use the Edit or AddNew method, and then reposition the current row pointer to another row without first using Update.
        /// •You use Edit or AddNew, and then use Edit or AddNew again without first using Update.
        /// •You cancel the update with the CancelUpdate method.
        /// •You set the Bookmark property to another row.
        /// •You close the result set referred to by object without first using Update.
        /// •The application ends before the Update method is executed, as when system power is interrupted.
        /// To edit a row, use the Edit method to copy the contents of the current row to the copy buffer.If you dont use AddNew or Edit first, an error 
        /// occurs when you use Update or attempt to add a new row.
        /// To add a new row, use the AddNew method to initialize and activate the copy buffer.
        /// Using Update produces an error under any of the following conditions: 
        /// •There is no current row.
        /// •The connection or rdoResultset is read-only.
        /// •No columns in the row are updatable.
        /// •You do not have an Edit or AddNew operation pending. 
        /// •Another user has locked the row or data page containing your row.
        /// •The user does not have permission to perform the operation.
        /// •Depending on the driver and type of cursor being used, you might not be able to use the cursor to update the primary key.         /// 
        /// </summary>
        public void Update()
        {
            //check if current row exist
            if (this.EOF || this.BOF)
            {
                return;
            }

            this.isEOFandBOFchecked = true;
            DataRow dataRow = this.CurrentRow;
            this.isEOFandBOFchecked = false;

            DataRowState rowState = dataRow.RowState;

            //exit if no changes
            if (rowState == DataRowState.Unchanged)
            {
                return;
            }

            //move from internal collection NewRows to DataTable
            if (rowState == DataRowState.Added || rowState == DataRowState.Detached)
            {
                this.UpdateNewRows();
            }

            //CHE: for disconnected DataTable do nothing on update
            if (DataAdapter != null && DataAdapter.SelectCommand != null && DataAdapter.SelectCommand.Connection == null)
            {
                return;
            }
            if (DataAdapter != null)
            {
                UpdateInternal(dataRow);
                return;
            }
            else if (!string.IsNullOrEmpty(this.Source))
            {
                //IPI: need to change the RowState from Modified to Unchanged
                this.AcceptChanges();
                return;
                // throw new Exception("The current DataTable is not updatable");
            }
            else
            {
                return;
            }
        }

        public void Cancel()
        {
            if (backgroundWorker1 != null && backgroundWorker1.IsBusy)
            {
                //cancel the fill
                if (this.DataAdapter != null && this.DataAdapter.SelectCommand != null)
                {
                    this.DataAdapter.SelectCommand.Cancel();
                    this.DataAdapter = null;
                }
                backgroundWorker1.CancelAsync();
            }
        }

        public object[,] GetRows(int numRows = 0)
        {
            // TODO:CHE
            return null;
        }

        public void Close()
        {
            this.Clear();
            this.Columns.Clear();
            //CHE: dispose also the object when closing the recordset to avoid memory leak
            this.Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Delete()
        {
            if (Deleted)
            {
                throw new Exception("Row handle refered to a deleted row or a row marked for deletion.");
            }

            if (EOF || BOF)
            {
                throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
            }

            //CHE: only mark for deletion, index remains the same
            //this.Rows.Remove(this.CurrentRow());
            //Index = Index - 1;
            //APE: preserve index when deleting. If the current cell is on the row that is being deleted, only if the DataTable is bound to a DataGridView, the index will shift due to the CurrentCellChange
            //event of the bound DataGridView
            int index = -1;
            if (ParentGrid != null)
            {
                index = Index;
            }
            isEOFandBOFchecked = true;
            this.CurrentRow.Delete();
            isEOFandBOFchecked = false;
            if (index > -1)
            {
                Index = index;
            }

            //CHE - row should be physically deleted - see summary comments
            this.Update();

            Deleted = true;
        }

        public void MoveLast(bool moveSelection = true)
        {
            MoveInternal(MoveDirection.Last);
        }

        public void MoveFirst(bool moveSelection = true)
        {
            MoveInternal(MoveDirection.First, moveSelection);
        }

        public void MoveNext(bool moveSelection = true)
        {
            MoveInternal(MoveDirection.Next, moveSelection);
        }

        public void MovePrevious(bool moveSelection = true)
        {
            MoveInternal(MoveDirection.Previous);
        }

        public void Move(int step)
        {
            // TODO:CHE
        }

        #endregion

        #region Internal Methods
        
        internal int GetFieldSize(int? size, FCRDO.DataTypeConstants type)
        {
            if (size.HasValue)
            {
                return size.Value;
            }

            switch (type)
            {
                case FCRDO.DataTypeConstants.rdTypeINTEGER:
                    return 4;

                case FCRDO.DataTypeConstants.rdTypeBIGINT:
                case FCRDO.DataTypeConstants.rdTypeLONGVARBINARY:
                case FCRDO.DataTypeConstants.rdTypeFLOAT:
                case FCRDO.DataTypeConstants.rdTypeDECIMAL:
                    return 8;

                case FCRDO.DataTypeConstants.rdTypeDOUBLE:
                case FCRDO.DataTypeConstants.rdTypeNUMERIC:
                    return 16;

                case FCRDO.DataTypeConstants.rdTypeBIT:
                case FCRDO.DataTypeConstants.rdTypeCHAR:
                    return 1;

                default:
                    return 0;
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private bool SetRowEmptyValues(DataRow row)
        {
            if (row == null)
            {
                return false;
            }

            bool valueChanged = false;
            //SBE: set empty values ("" for strings or 0 for numerics), if field does not allow null values
            bool isDeleted = (row.RowState == DataRowState.Deleted);

            if (!isDeleted)
            {
                Type columnType = null;
                foreach (FCrdoColumn col in this.rdoColumns)
                {
                    if (Information.IsDBNull(row[col.ColumnName]))
                    {
                        if (col.Required)
                        {
                            columnType = this.Columns[col.ColumnName].DataType;
                            if (columnType == typeof(System.String))
                            {
                                row[col.ColumnName] = "";
                            }
                            else if (columnType.IsValueType)
                            {
                                row[col.ColumnName] = Activator.CreateInstance(columnType);
                            }
                        }

                        //CHE: set default value (regardless the Required)
                        if (!string.IsNullOrEmpty(col.DefaultValue))
                        {
                            row[col.ColumnName] = col.DefaultValue;
                        }

                        valueChanged = true;
                    }
                }
            }

            return valueChanged;
        }

        //check if the current row is a new added row
        private bool IsNewRowAdded()
        {
            int currentIdx = Index;

            int totalRows = this.Rows.Count + (HasNewRows() ? NewRows.Count : 0);
            if (totalRows < this.DefaultView.Count)
            {
                totalRows = this.DefaultView.Count;
            }

            if (currentIdx < 1 || currentIdx > totalRows)
            {
                return false;
            }

            return currentIdx > this.Rows.Count;
        }

        private bool HasNewRows()
        {
            if (NewRows != null)
            {
                return NewRows.Count > 0;
            }
            return false;
        }

        private void UpdateNewRows()
        {
            foreach (DataRowView drView in this.DefaultView)
            {
                bool endEditForCurrentRow = false;

                if (drView.IsEdit && !drView.IsNew)
                {
                    endEditForCurrentRow = true;
                }
                else if (drView.IsEdit && drView.IsNew && drView.Row.RowState != DataRowState.Deleted)
                {
                    //SBE: check if row was modified
                    for (int i = 0; i < drView.Row.ItemArray.Length; i++)
                    {
                        object value = drView.Row.ItemArray[i];
                        if (value != null && !Information.IsDBNull(value))
                        {
                            endEditForCurrentRow = true;
                            break;
                        }
                    }

                    if (endEditForCurrentRow)
                    {
                        //CNA: need to set empty values for drView's cells if DataTable column does not allow DBNull
                        this.SetRowEmptyValues(drView.Row);
                        drView.EndEdit();
                        //IPI: when EndEdit is executed, drView.Row will be committed to the Rows collection
                        // if a row is in the DataTable.Rows collection, it should be removed from NewRows
                        if (NewRows != null && NewRows.Contains(drView.Row))
                        {
                            NewRows.Remove(drView.Row);
                        }
                    }
                }
            }

            //CHE: AddNew on DataTable with mandatory fields will raise exception if new empty row is added in collection, must be added later on Update
            if (this.HasNewRows())
            {
                List<DataRow> dataTableRows = this.AsEnumerable().ToList();
                //CHE: collection is modified in RowChanging, use for instead of foreach
                //foreach (DataRow row in NewRows)
                for (int i = 0; i < NewRows.Count; i++)
                {
                    DataRow row = NewRows[i];
                    //BAN: add the new row only if the row is not already contained in the DataTable
                    if (!dataTableRows.Contains(row))
                    {
                        this.SetRowEmptyValues(row);
                        this.Rows.Add(row);
                    }
                }
                NewRows.Clear();
            }

            //IPI: must update all clone tables as well
            //SynchronizeClones(this, Synchronization.UpdateRow);
        }

        private int UpdateInternal(DataRow dataRow)
        {
            int affectedRows = 0;
            try
            {
                if (builder == null)
                {
                    //CHE: for tables with large number of columns (e.g. 80) when calling DataAdapter.Update error "Query too complex" is received because where clause contains too many conditions;
                    //modify UpdateCommand to contain only PK column in the where clause
                    builder = new SqlCommandBuilder(DataAdapter as SqlDataAdapter);
                    //     Specifies whether all column values in an update statement are included or
                    //     only changed ones.
                    builder.SetAllValues = false;
                    //     All update and delete statements include only System.Data.DataTable.PrimaryKey
                    //     columns in the WHERE clause. If no System.Data.DataTable.PrimaryKey is defined,
                    //     all searchable columns are included in the WHERE clause.
                    builder.ConflictOption = ConflictOption.OverwriteChanges;
                    //PJU: wrap table and column names in square brackets  - are required if any table or column names contain spaces or "funny" characters, or if they happen to be reserved words in Access SQL
                    builder.QuotePrefix = "[";
                    builder.QuoteSuffix = "]";
                    DataAdapter.UpdateCommand = builder.GetUpdateCommand();
                    DataAdapter.InsertCommand = builder.GetInsertCommand();
                }

                bool isAdded = dataRow.RowState == DataRowState.Added;
                affectedRows = DataAdapter.Update(this);


                //SBE: if table contains the IdentityColumn, update each row. On INSERT get the last inserted ID and update DataTable
                if (!object.Equals(IdentityColumn, null) && isAdded && !string.IsNullOrEmpty(this.TableName))
                {
                    string strSQLGetIdentityValue = "SELECT @@Identity";

                    //SBE: execute statement to get the value for identity column
                    if (affectedRows > 0)
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = this.ActiveConnection.connection as SqlConnection;

                        //che:transaction
                        cmd.Transaction = this.ActiveConnection.Trans;
                        if (this.queryTimeOut != -1)
                        {
                            cmd.CommandTimeout = this.queryTimeOut;
                        }
                        cmd.CommandText = strSQLGetIdentityValue;
                        try
                        {
                            FCRDO.rdoEngine.rdoErrors.Clear();
                            object idValue = cmd.ExecuteScalar();
                            dataRow[IdentityColumn.ColumnName] = idValue;
                            //BAN: avoid error on future update for identity column that is seen as modified 
                            dataRow.AcceptChanges();
                        }
                        catch (SqlException ex)
                        {
                            FCrdoError rdoError = new FCrdoError();
                            rdoError.Number = ex.Number;
                            rdoError.Description = ex.Message;
                            rdoError.Source = ex.Source;
                            FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                            throw ex;
                        }
                    }
                }
            }
            //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key                
            //e.g. Dynamic SQL generation for the UpdateCommand is not supported against a SelectCommand that does not return any key column information.
            catch (InvalidOperationException ex)
            {
                //clear errors
                if (this.HasErrors)
                {
                    DataRow[] rowsInError = this.GetErrors();
                    for (int i = 0; i < rowsInError.Length; i++)
                    {
                        //foreach (DataColumn myCol in this.Columns)
                        //{
                        //    Console.WriteLine(myCol.ColumnName + " " +
                        //    rowsInError[i].GetColumnError(myCol));
                        //}

                        rowsInError[i].ClearErrors();
                    }
                }

                //CHE: in UpdateBatch before calling FillSchema replace null field with default value from SQL server if field does not accept null
                foreach (FCrdoColumn f in this.rdoColumns)
                {
                    if (f.Required)
                    {
                        try
                        {
                            foreach (DataRow dr in this.Rows)
                            {
                                if (dr.RowState != DataRowState.Deleted && dr[f.ColumnName] == DBNull.Value && f.DefaultValue != null)
                                {
                                    string value = f.DefaultValue.TrimStart(new char[] { '(' }).TrimEnd(new char[] { ')' }); ;
                                    if (f.Type == FCRDO.DataTypeConstants.rdTypeBIT && value != null)
                                    {
                                        dr[f.ColumnName] = FCGlobal.BoolFromString(value);
                                    }
                                    else
                                    {
                                        dr[f.ColumnName] = value;
                                    }
                                }
                            }
                        }
                        catch
                        {
                        }
                    }
                }

                //CHE: schema is necessary in UpdateBatchBuilder e.g. to skip identity column
                DataAdapter.FillSchema(this, SchemaType.Mapped);

                //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
                affectedRows = UpdateTry(dataRow);

                if (affectedRows == -1)
                {
                    //THIS MESSAGE BOX SHOULD NEVER BE DISPLAYED; IF IT DOES A FIX NEEDS TO BE FOUNDED ON CUSTOMER CODE 
                    MessageBox.Show(ex.Message, "fecherFoundation - Fatal error on update recordset!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }

                //BAN: accept all changes in the data table, this way the row states will become unchanged, so if the same data table will have rows changed (added, modified, deleted)
                //and the UpdateBatch is called again, the previous rows will not influence the new call
                this.AcceptChanges();
            }
            //CHE: FilterType should not affect grid, restore position
            finally
            {
                //if (ParentGrid != null && BookmarkBeforeFilterType != null && this.FilterType != FilterGroupEnum.adFilterNone)
                //{
                //    this.FilterType = FilterGroupEnum.adFilterNone;
                //    //CHE: restore position only if it is valid
                //    if (IsValidBookmark(BookmarkBeforeFilterType))
                //    {
                //        this.Bookmark = BookmarkBeforeFilterType;
                //    }
                //    BookmarkBeforeFilterType = null;
                //}
            }

            return affectedRows;
        }

        private int DefaultViewRowsCountWithState()
        {
            //CHE: return rows from DefaultView
            int nr = this.DefaultView.Count;

            //CHE: include last row which is currently added only if changes were done
            if (ParentGrid != null && this.IsNewRowAdded())
            {
                if (!ParentGrid.IsCurrentRowDirty)
                {
                    nr--;
                }
            }

            //CHE: include rows from NewRows collection
            nr += NewRows == null ? 0 : NewRows.Count;

            //exclude deleted rows if flag not included in RowStateFilter
            if ((this.DefaultView.RowStateFilter & DataViewRowState.Deleted) != DataViewRowState.Deleted)
            {
                foreach (DataRowView drv in this.DefaultView)
                {
                    if (drv.Row.RowState == DataRowState.Deleted)
                    {
                        --nr;
                    }
                }
            }

            return nr;
        }

        //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
        private int UpdateTry(DataRow dataRow)
        {
            int colCount = this.Columns.Count;
            List<CommandType> commands = new List<CommandType>();

            DataRowState drs = dataRow.RowState;
            //If the TableName is not null means that the statement was executed on one table. If the statement contains join 
            //the columns must be separated by parent table, and statements need to be created individually for each table
            if (!string.IsNullOrEmpty(this.TableName))
            {
                StringBuilder str1 = new StringBuilder();
                StringBuilder str2 = null;
                if (drs != DataRowState.Deleted)
                {
                    str2 = new StringBuilder();
                }
                DataColumn dataColumn = null;
                for (int columnIndex = 0; columnIndex < colCount; columnIndex++)
                {
                    dataColumn = this.Columns[columnIndex];
                    if (drs == DataRowState.Deleted || drs == DataRowState.Modified)
                    {
                        str1.Append("`" + dataColumn.ColumnName + "`" + FormatValue(dataColumn.DataType, dataRow[columnIndex, DataRowVersion.Original], true));

                        if (columnIndex < colCount - 1)
                        {
                            str1.Append(" and ");
                        }
                    }
                    if (drs == DataRowState.Modified && !dataColumn.AutoIncrement)
                    {
                        str2.Append("`" + dataColumn.ColumnName + "`" + "=" + FormatValue(dataColumn.DataType, dataRow[columnIndex]));
                        if (columnIndex < colCount - 1)
                        {
                            str2.Append(", ");
                        }
                    }
                    if (drs == DataRowState.Added && !dataColumn.AutoIncrement)
                    {
                        str1.Append("`" + dataColumn.ColumnName + "`");
                        str2.Append(FormatValue(dataColumn.DataType, dataRow[columnIndex]));
                        if (columnIndex < colCount - 1)
                        {
                            str1.Append(", ");
                            str2.Append(", ");
                        }
                    }
                }

                CommandType item = new CommandType();
                string str = "";
                switch (drs)
                {
                    case DataRowState.Deleted:
                        {
                            str = String.Format("delete from {0} where {1}", this.TableName, str1.ToString());
                            item.whereClause = str1.ToString();
                            item.tableName = this.TableName;
                            break;
                        }
                    case DataRowState.Modified:
                        {
                            str = String.Format("update {0} set {1} where {2}", this.TableName, str2.ToString(), str1.ToString());
                            item.whereClause = str1.ToString();
                            item.tableName = this.TableName;
                            break;
                        }
                    case DataRowState.Added:
                        {
                            str = String.Format("insert into {0} ({1}) values ({2})", this.TableName, str1.ToString(), str2.ToString());
                            break;
                        }
                }
                item.command = str;
                commands.Add(item);
            }
            else
            {
                //Get all the tables from the JOIN statement and the corresponding columns
                Dictionary<string, List<DataColumn>> columnsGroupedByTable = new Dictionary<string, List<DataColumn>>();
                for (int count = 0; count < this.Columns.Count; count++)
                {
                    FCrdoColumn columnField = this.rdoColumns[count];
                    if (string.IsNullOrEmpty(columnField.BaseTableName))
                    {
                        continue;
                    }
                    if (!columnsGroupedByTable.ContainsKey(columnField.BaseTableName))
                    {
                        columnsGroupedByTable.Add(columnField.BaseTableName, new List<DataColumn>());
                    }
                    columnsGroupedByTable[columnField.BaseTableName].Add(this.Columns[count]);
                }
                //On each data row, separate statements must be created for each individual table used in the join statement
                foreach (KeyValuePair<string, List<DataColumn>> tableWithColumns in columnsGroupedByTable)
                {
                    StringBuilder str1 = new StringBuilder();
                    StringBuilder str2 = null;
                    if (drs != DataRowState.Deleted)
                    {
                        str2 = new StringBuilder();
                    }
                    foreach (DataColumn col in tableWithColumns.Value)
                    {
                        int columnIndex = this.Columns.IndexOf(col);
                        FCrdoColumn columnField = this.rdoColumns[columnIndex];
                        int columnIndexInList = tableWithColumns.Value.IndexOf(col);
                        if (drs == DataRowState.Deleted || drs == DataRowState.Modified)
                        {
                            //A column of datatype ntext cannot be used in a where clause by comparing it with a varchar
                            //a cast must be used for this operation
                            string beginCast = "";
                            string endCast = "";
                            //if (columnField.Type == FCRDO.DataTypeConstants.dbMemo)
                            //{
                            //    beginCast = "Cast(";
                            //    endCast = " as nvarchar(max))";
                            //}

                            str1.Append(beginCast + "`" + columnField.BaseColumnName + "`" + endCast + FormatValue(col.DataType, dataRow[columnIndex, DataRowVersion.Original], true));

                            if (columnIndexInList < tableWithColumns.Value.Count - 1)
                            {
                                str1.Append(" and ");
                            }
                        }
                        if (drs == DataRowState.Modified && !this.Columns[columnIndex].AutoIncrement)
                        {
                            //A column might have been selected twice. Do not add the same column twice
                            if (!str2.ToString().Contains("`" + columnField.BaseColumnName + "`" + "="))
                            {
                                str2.Append("`" + columnField.BaseColumnName + "`" + "=" + FormatValue(col.DataType, dataRow[columnIndex]));
                                if (columnIndexInList < tableWithColumns.Value.Count - 1)
                                {
                                    str2.Append(", ");
                                }
                            }
                        }
                        if (drs == DataRowState.Added && !this.Columns[columnIndex].AutoIncrement)
                        {
                            str1.Append("`" + columnField.BaseColumnName + "`");
                            str2.Append(FormatValue(col.DataType, dataRow[columnIndex]));
                            if (columnIndexInList < tableWithColumns.Value.Count - 1)
                            {
                                str1.Append(", ");
                                str2.Append(", ");
                            }
                        }
                    }

                    CommandType item = new CommandType();
                    string str = "";
                    switch (drs)
                    {
                        case DataRowState.Deleted:
                            {
                                str = String.Format("delete from {0} where {1}", tableWithColumns.Key, str1.ToString());
                                item.whereClause = str1.ToString();
                                item.tableName = tableWithColumns.Key;
                                break;
                            }
                        case DataRowState.Modified:
                            {
                                str = String.Format("update {0} set {1} where {2}", tableWithColumns.Key, str2.ToString(), str1.ToString());
                                item.whereClause = str1.ToString();
                                item.tableName = tableWithColumns.Key;
                                break;
                            }
                        case DataRowState.Added:
                            {
                                str = String.Format("insert into {0} ({1}) values ({2})", tableWithColumns.Key, str1.ToString(), str2.ToString());
                                break;
                            }
                    }
                    item.command = str;
                    commands.Add(item);
                }
            }

            SqlCommand com = new SqlCommand();
            com.Connection = this.ActiveConnection.connection as SqlConnection;

            if (com.Connection.State == ConnectionState.Closed)
            {
                com.Connection.Open();
            }

            //che:transaction
            com.Transaction = this.ActiveConnection.Trans;

            if(this.queryTimeOut != -1)
            {
                com.CommandTimeout = queryTimeOut;
            }

            bool multipleRows = false;
            int affectedRows = 0;

            foreach (CommandType item in commands)
            {
                if (!string.IsNullOrEmpty(item.whereClause) && !string.IsNullOrEmpty(item.tableName))
                {
                    com.CommandText = string.Format("select count(*) from {0} where {1}", item.tableName, item.whereClause);
                    try
                    {
                        FCRDO.rdoEngine.rdoErrors.Clear();
                        affectedRows = Convert.ToInt32(com.ExecuteScalar());
                    }
                    catch (SqlException ex)
                    {
                        FCrdoError rdoError = new FCrdoError();
                        rdoError.Number = ex.Number;
                        rdoError.Description = ex.Message;
                        rdoError.Source = ex.Source;
                        FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                        throw ex;
                    }
                    if (affectedRows > 1)
                    {
                        multipleRows = true;
                        break;
                    }
                }
            }

            if (multipleRows)
            {
                //only one row should be affected; if not, update failed
                return -1;
            }

            affectedRows = 0;
            foreach (CommandType item in commands)
            {
                com.CommandText = item.command;
                try
                {
                    FCRDO.rdoEngine.rdoErrors.Clear();
                    int comAffectedRows = com.ExecuteNonQuery();
                    if (comAffectedRows > 0)
                    {
                        affectedRows = comAffectedRows;
                    }
                }
                catch (SqlException ex)
                {
                    FCrdoError rdoError = new FCrdoError();
                    rdoError.Number = ex.Number;
                    rdoError.Description = ex.Message;
                    rdoError.Source = ex.Source;
                    FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                    throw ex;
                }
            }

            return affectedRows;
        }

        //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
        private string FormatValue(Type type, object p, bool compare = false)
        {
            if (p == DBNull.Value)
            {
                return (compare ? " is " : "") + "null";
            }

            string str = (compare ? " = " : "");

            if (type == typeof(string))
            {
                return str + String.Format("'{0}'", FCConvert.ToString(p).Replace("'", "''"));
            }

            if (type == typeof(bool))
            {
                //CHE: use true/false values not 1/0
                //return str + Convert.ToInt32(p).ToString();
                return str + p.ToString();
            }

            if (type == typeof(decimal) || type == typeof(float) || type == typeof(double))
            {
                return str + FCConvert.ToString(p, System.Globalization.CultureInfo.InvariantCulture);
            }

            if (type == typeof(DateTime))
            {
                return str + String.Format("#{0}#", ((DateTime)p).ToString("yyyy-MM-dd HH:mm:ss"));
            }

            return str + p.ToString();
        }

        private void SetFields()
        {
            //CHE: implement SetFields using DataReader, SQLConnection.GetSchema throws exception in case of a transaction
            //DataTable tableColumns = this.ActiveConnection.connection.GetSchema("Columns", new string[] { null, null, this.TableName });
            if (this.TableName == "")
            {
                return;
            }

            SqlDataReader myReader = null;

            try
            {
                SqlCommand cmd = new SqlCommand();

                cmd.Connection = this.ActiveConnection.connection;
                cmd.Transaction = this.ActiveConnection.Trans;
                cmd.CommandText = this.Source;

                //CHE: improve performance - add dummy where 1=0
                bool replace = false;
                string result = cmd.CommandText;
                //do nothing for group by
                if (result.ToUpper().IndexOf(" GROUP BY ") == -1)
                {
                    result = this.Source.TrimEnd(new char[] { ' ', ';' });
                    //remove order by
                    int pos = result.ToUpper().IndexOf(" ORDER BY ");
                    if (pos > 0)
                    {
                        result = cmd.CommandText.Substring(0, pos);
                    }
                    pos = result.ToUpper().IndexOf(" WHERE ");
                    //do nothing if multiple where
                    if (pos != -1 && result.Substring(pos+6).ToUpper().IndexOf(" WHERE ") == -1)
                    {
                        if (pos > 0)
                        {
                            //replace where
                            result = cmd.CommandText.Substring(0, pos) + " where 1=0"; // and (" + cmd.CommandText.Substring(pos + 7) + ")";
                            replace = true;
                        }
                        else
                        {
                            //add where
                            result += " where 1=0";
                            replace = true;
                        }
                    }
                }
                if (replace)
                {
                    cmd.CommandText = result;
                }

                if (queryTimeOut != -1)
                {
                    cmd.CommandTimeout = queryTimeOut;
                }

                try
                {
                    FCRDO.rdoEngine.rdoErrors.Clear();
                    myReader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
                }
                catch (SqlException ex)
                {
                    FCrdoError rdoError = new FCrdoError();
                    rdoError.Number = ex.Number;
                    rdoError.Description = ex.Message;
                    rdoError.Source = ex.Source;
                    FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                    throw ex;
                }

                //Retrieve column schema into a DataTable.
                DataTable tableColumns = myReader.GetSchemaTable();

                FCrdoTable tableDef = null;
                if (tableColumns.Rows.Count > 0)
                {
                    tableDef = this as FCrdoTable;
                }

                int size = 0;
                FCrdoColumn field = null;
                FCRDO.DataTypeConstants fieldType;
                string colName = "";

                foreach (DataRow row in tableColumns.Rows)
                {
                    size = 0;

                    if (!row["ColumnSize"].Equals(DBNull.Value))
                    {
                        size = Convert.ToInt32(row["ColumnSize"]);
                    }
                    field = null;
                    colName = row["ColumnName"].ToString();
                    fieldType = GetDataType(row["DataTypeName"].ToString());
                    if (tableDef != null)
                    {
                        field = tableDef.CreateColumn(colName, fieldType, size);
                        //IPI: VB6 Append function creates the column in the DB if it does not exist; but in this case, we iterate through the columns collection retrieved from DB
                        // so all columns already exist in the DB 
                        tableDef.rdoColumns.Append(field, false);
                    }
                    else
                    {
                        field = this.rdoColumns[colName];
                        if (!Object.Equals(field, null))
                        {
                            field.Type = fieldType;
                            field.Size = this.GetFieldSize(size, fieldType);
                            field.ColumnSize = size;
                        }
                    }
                    if (!Object.Equals(field, null))
                    {
                        field.BaseTableName = this.TableName;
                        field.BaseColumnName = row["ColumnName"].ToString();
                        field.Required = Convert.ToBoolean(row["AllowDBNull"].ToString());
                        //if (!row["COLUMN_DEFAULT"].Equals(DBNull.Value))
                        //{
                        //    string colDef = row["COLUMN_DEFAULT"].ToString();
                        //    if (colDef.Contains("CREATE DEFAULT FALSE AS 0"))
                        //    {
                        //        field.DefaultValue = "0";
                        //    }
                        //    else if (colDef.Contains("CREATE DEFAULT TRUE AS 1"))
                        //    {
                        //        field.DefaultValue = "1";
                        //    }
                        //    else
                        //    {
                        //        field.DefaultValue = row["COLUMN_DEF"].ToString();
                        //    }
                        //}
                        // set identity column for autonumber
                        if (Convert.ToBoolean(row["IsIdentity"].ToString()))
                        {
                            this.IdentityColumn = field;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (myReader != null)
                {
                    myReader.Close();
                }
            }
        }

        private FCRDO.DataTypeConstants GetDataType(string typeName)
        {
            switch (typeName)
            {
                case "datetime":
                    {
                        return FCRDO.DataTypeConstants.rdTypeTIMESTAMP;
                    }
                case "varchar":
                    {
                        return FCRDO.DataTypeConstants.rdTypeVARCHAR;
                    }
                case "char":
                    {
                        return FCRDO.DataTypeConstants.rdTypeCHAR;
                    }
                case "tinyint":
                    {
                        return FCRDO.DataTypeConstants.rdTypeTINYINT;
                    }
                case "smallint":
                    {
                        return FCRDO.DataTypeConstants.rdTypeSMALLINT;
                    }
                case "numeric":
                    {
                        return FCRDO.DataTypeConstants.rdTypeNUMERIC;
                    }
                case "int":
                case "int identity":
                case "uniqueidentifier":
                    {
                        return FCRDO.DataTypeConstants.rdTypeINTEGER;
                    }
                case "money":
                case "decimal":
                    {
                        return FCRDO.DataTypeConstants.rdTypeDECIMAL;
                    }
                case "byte":
                case "bit":
                    {
                        return FCRDO.DataTypeConstants.rdTypeBIT;
                    }
            }

            //user defined type
            SqlCommand comm = new SqlCommand("", this.ActiveConnection.connection);
            comm.Transaction = this.ActiveConnection.trans;
            if (queryTimeOut != -1)
            {
                comm.CommandTimeout = queryTimeOut;
            }
            comm.CommandText = "select st.name" +
                " from sys.types as t JOIN sys.systypes AS ST on t.system_type_id = st.xtype" +
                " where t.is_user_defined = 1 and t.name<> st.name and t.name = '" + typeName +"'";
            return GetDataType(comm.ExecuteScalar().ToString());
        }

        private void MoveInternal(MoveDirection md, bool moveSelection = true)
        {
            //APE: before returning EOF or BOF check also if data table containes new rows added using AddNew 
            if (!this.Deleted)
            {
                if (
                    ((this.Rows.Count == 0 && this.DefaultView.Count == 0) && (this.NewRows != null && this.NewRows.Count == 0)) ||
                    (md == MoveDirection.Next && this.EOF) ||
                    (md == MoveDirection.Previous && this.BOF)
                    )
                {
                    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                }
            }

            int index = this.Index;
            DataRow row = null;
            switch (md)
            {
                case MoveDirection.First:
                    {
                        //sets the index to the first row
                        if (this.View == null || this.View.Count == 0)
                        {
                            index = 1;
                        }
                        else
                        {
                            row = this.View[0].Row;
                            index = this.Rows.IndexOf(row) + 1;
                        }
                        //CHE: if no rows in view bof should return true
                        if (this.RowCount == 0)
                        {
                            this.mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosBOF);
                            index = 0;
                        }
                        break;
                    }
                case MoveDirection.Last:
                    {
                        //CHE: sets the index to the last row, on last added if the case
                        if (this.HasNewRows())
                        {
                            index = this.Rows.Count + this.NewRows.Count;
                        }
                        else
                        {
                            //CHE: when view (filter or sort) should go to last row in view not in table
                            if (this.View == null || this.View.Count == 0)
                            {
                                index = this.Rows.Count;
                            }
                            else
                            {
                                row = this.View[this.View.Count - 1].Row;
                                index = this.Rows.IndexOf(row) + 1;
                            }
                        }

                        if (this.RowCount == 0)
                        {
                            this.mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosEOF);
                        }
                        break;
                    }
                case MoveDirection.Next:
                    {
                        if (this.Deleted)
                        {
                            if (!this.EOF)
                            {
                                DataRow currentRow = this.CurrentRow;
                                if (currentRow.RowState == DataRowState.Deleted)
                                {
                                    index++;
                                }
                            }
                            this.Deleted = false;
                        }
                        //CHE: when view (filter or sort) should go to next row in view not in table
                        else if (this.View == null || this.View.Count == 0)
                        {
                            //increment the index
                            index++;
                        }
                        else
                        {
                            bool isInView = false;
                            int i;

                            //CHE: if you are on BOF and you MoveNext it should go to first row from view
                            row = this.CurrentRow;
                            if (!this.BOF)
                            {
                                for (i = 0; i < this.View.Count; i++)
                                {
                                    if (this.View[i].Row == row)
                                    {
                                        isInView = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                isInView = true;
                                i = -1;
                            }

                            if (isInView && i < this.View.Count - 1)
                            {
                                DataRow nextRow = this.View[i + 1].Row;
                                index = this.Rows.IndexOf(nextRow) + 1;
                            }
                            else
                            {
                                //SBE: check if RowState is Deleted, and move to next index
                                if (row.RowState == DataRowState.Deleted)
                                {
                                    index++;
                                }
                                else
                                {
                                    index = this.Rows.Count + 1;
                                }
                            }
                        }

                        int totalRows = this.Rows.Count + (this.HasNewRows() ? this.NewRows.Count : 0);
                        if (index > totalRows)
                        {
                            this.mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosEOF);
                        }

                        break;
                    }
                case MoveDirection.Previous:
                    {
                        if (this.Deleted)
                        {
                            if (!this.BOF)
                            {
                                //row was removed, EOF is true or RowState is Deleted
                                if (this.EOF || this.CurrentRow.RowState == DataRowState.Deleted)
                                {
                                    index--;
                                }
                            }
                            this.Deleted = false;
                        }
                        //CHE: when view (filter or sort) should go to previous row in view not in table
                        else if (this.View == null || this.View.Count == 0)
                        {
                            //decrement the index
                            index--;
                        }
                        else
                        {
                            bool isInView = false;
                            int i;
                            //CHE: if you are on EOF and you MovePrevious it should go to last row from view
                            if (!this.EOF)
                            {
                                row = this.CurrentRow;
                                for (i = 0; i < this.View.Count; i++)
                                {
                                    if (this.View[i].Row == row)
                                    {
                                        isInView = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                isInView = true;
                                i = this.View.Count;
                            }
                            if (isInView && i > 0)
                            {
                                DataRow nextRow = this.View[i - 1].Row;
                                index = this.Rows.IndexOf(nextRow) + 1;
                            }
                            else
                            {
                                index = 0;
                            }
                        }

                        if (index == 0)
                        {
                            this.mintAbsolutePosition = Convert.ToInt32(PositionEnum.adPosBOF);
                        }
                        break;
                    }
            }

            this.Index = index;

            if (moveSelection)
            {
                MoveSelection();
            }
        }

        private void MoveSelection()
        {
            if (ParentGrid != null)
            {
                DataGridView grid = ParentGrid;
                int dataTableDefinitionsIndex = Index;
                //BAN: if grid allows the user to add new rows, when the selection will be set to the last row,
                // a new empty row will be added in the DataTable DefaultView. Setting AllowUserToAddRows to false will prevent this
                int indexAdjustment = 0;
                if (grid.AllowUserToAddRows == true)
                {
                    indexAdjustment = 1;
                }
                if ((Index - 1) >= 0 && (Index - 1) < grid.Rows.Count - indexAdjustment)
                {
                    //CHE: avoid null exception
                    if (grid.CurrentCell != null)
                    {
                        //EndEdit if anything in edit
                        bool endEditForCurrentRow = false;
                        foreach (DataRowView drView in this.DefaultView)
                        {
                            if (drView.IsEdit && drView.Row.RowState != DataRowState.Deleted)
                            {
                                endEditForCurrentRow = false;
                                //SBE: check if row was modified
                                for (int i = 0; i < drView.Row.ItemArray.Length; i++)
                                {
                                    object value = drView.Row.ItemArray[i];
                                    if (value != null && !Information.IsDBNull(value))
                                    {
                                        endEditForCurrentRow = true;
                                        break;
                                    }
                                }

                                if (endEditForCurrentRow)
                                {
                                    //CNA: need to set empty values for drView's cells if DataTable column does not allow DBNull
                                    this.SetRowEmptyValues(drView.Row);
                                    drView.EndEdit();
                                }
                            }
                        }
                        //move current cell based on current row in DataTable
                        //CHE: only move current cell if this.CurrentRow is valid (neither EOF nor BOF has been reached)
                        if (!(EOF || BOF))
                        {
                            DataRow currentRowInDataTable = this.CurrentRow;
                            foreach (DataGridViewRow row in grid.Rows)
                            {
                                if (row.DataBoundItem != null && ((DataRowView)row.DataBoundItem).Row == currentRowInDataTable)
                                {
                                    grid.CurrentCell = grid[grid.CurrentCell.ColumnIndex, grid.Rows.IndexOf(row)];
                                    break;
                                }
                            }
                        }
                    }
                    //BAN: Needed to save and reset the dataTableDefinitions index because, when setting the current cell
                    //the selected index changed event is raised before setting the new value. In FCTrueDBGrid, in the event handler
                    //the row will be the previous one then and it will reset the data table definitions index with the wrong index
                    Index = dataTableDefinitionsIndex;
                }
            }
        }
        #endregion

        private class CommandType
        {
            public string whereClause = "";
            public string tableName = "";
            public string command = "";
        }
    }
}
