﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.DataBaseLayer.RDO
{
    public class FCrdoEnvironment
    {
        #region Public Members

        public List<FCrdoConnection> rdoConnections;

        #endregion

        #region Constructors

        public FCrdoEnvironment(string name, string user, string password)
        {
            this.Name = name;
            this.UserName = user;
            this.Password = password;
            this.rdoConnections = new List<FCrdoConnection>();
        }

        #endregion

        #region Properties

        public int CursorDriver
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// rdoEnvironments(0) rdoEngine Set to "Default_Environment." 
        /// rdoEnvironments(1-n) name argument of rdoCreateEnvironment.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        #endregion


    }
}
