﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace fecherFoundation.DataBaseLayer.RDO
{
    public class FCrdoEnvironments : IEnumerable<FCrdoEnvironment>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private List<FCrdoEnvironment> rdoEnvironments;
        private FCrdoEngine engine;

        #endregion

        #region Constructors

        public FCrdoEnvironments(FCrdoEngine engine)
        {
            this.engine = engine;
            this.rdoEnvironments = new List<FCrdoEnvironment>();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public FCrdoEnvironment this[string name]
        {
            get
            {
                return this.GetrdoEnvironment(name);
            }
            set
            {
                FCrdoEnvironment field = this.GetrdoEnvironment(name);
                field = value;
            }
        }

        public FCrdoEnvironment this[int index]
        {
            get
            {
                return this.GetrdoEnvironment(index);
            }
            set
            {
                FCrdoEnvironment field = this.GetrdoEnvironment(index);
                field = value;
            }
        }

        public int Count
        {
            get
            {
                return (rdoEnvironments == null) ? 0 : rdoEnvironments.Count;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        public void Add(FCrdoEnvironment rdoEnvironment)
        {
            rdoEnvironments.Add(rdoEnvironment);
        }

        public void Remove(string name)
        {
            rdoEnvironments.Remove(GetrdoEnvironment(name));
        }

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCrdoEnvironment> GetEnumerator()
        {
            return rdoEnvironments.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal FCrdoEnvironment GetrdoEnvironment(int Index)
        {
            return rdoEnvironments[Index];
        }

        internal FCrdoEnvironment GetrdoEnvironment(string rdoEnvironmentName)
        {
            rdoEnvironmentName = rdoEnvironmentName.ToUpper();
            //get last from list to be the same as in VB6 for fields having same name
            return rdoEnvironments.FindLast(delegate (FCrdoEnvironment target)
            {
                return target.Name.ToUpper() == rdoEnvironmentName;
            });
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
