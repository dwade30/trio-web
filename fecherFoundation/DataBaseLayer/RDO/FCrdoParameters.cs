﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// rdoColumns
    /// </summary>
    public class FCrdoParameters : IEnumerable<FCrdoParameter>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private List<FCrdoParameter> parameters;
        private FCrdoQuery query;

        #endregion

        #region Constructors

        public FCrdoParameters(FCrdoQuery query)
        {
            this.query = query;
            this.parameters = new List<FCrdoParameter>();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public int Count
        {
            get
            {
                return (parameters == null) ? 0 : parameters.Count;
            }
        }

        public FCrdoParameter this[int index]
        {
            get
            {
                return this.GetParameter(index);
            }
            set
            {
                FCrdoParameter parameter = this.GetParameter(index);
                parameter = value;
            }
        }        

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCrdoParameter> GetEnumerator()
        {
            return parameters.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal void Append(FCrdoParameter column, bool createColumnInDB)
        {
            parameters.Add(column);
        }

        internal FCrdoParameter GetParameter(int index)
        {
            if (index >= parameters.Count)
            {
                FCrdoParameter param = new FCrdoParameter(query);
                parameters.Add(param);
            }
            return parameters[index];
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private bool fieldExists(DbConnection connection, string tableName, string fieldName)
        {
            DataTable tblColumns = connection.GetSchema("Columns");
            DataRow[] tblColumnsRows = tblColumns.Select("COLUMN_NAME = '" + fieldName + "' AND TABLE_NAME = '" + tableName + "'");
            return tblColumnsRows.Length > 0;
        }

        private string ConvertToString(FCRDO.DataTypeConstants dataType)
        {
            switch (dataType)
            {
                case FCRDO.DataTypeConstants.rdTypeDATE:
                    return "datetime";
                case FCRDO.DataTypeConstants.rdTypeTIME:
                    return "time";
                case FCRDO.DataTypeConstants.rdTypeTIMESTAMP:
                    return "timestamp";
                case FCRDO.DataTypeConstants.rdTypeINTEGER:
                case FCRDO.DataTypeConstants.rdTypeBIGINT:
                    return "int";
                case FCRDO.DataTypeConstants.rdTypeTINYINT:
                    return "tinyint";
                case FCRDO.DataTypeConstants.rdTypeBIT:
                    return "bit";
                case FCRDO.DataTypeConstants.rdTypeFLOAT:
                case FCRDO.DataTypeConstants.rdTypeDECIMAL:
                case FCRDO.DataTypeConstants.rdTypeDOUBLE:
                case FCRDO.DataTypeConstants.rdTypeNUMERIC:
                    return "float";
                case FCRDO.DataTypeConstants.rdTypeCHAR:
                    return "varchar";
                case FCRDO.DataTypeConstants.rdTypeBINARY:
                case FCRDO.DataTypeConstants.rdTypeVARBINARY:
                    return "varbinary";
                default:
                    return "";
            }
        }

        #endregion
    }
}
