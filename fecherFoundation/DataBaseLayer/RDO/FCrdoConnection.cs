﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// rdoConnection
    /// </summary>
    public class FCrdoConnection
    {
        #region Public Members
        #endregion

        #region Internal Members

        internal SqlConnection connection;
        internal SqlTransaction trans;

        #endregion

        #region Private Members

        private FCrdoResultsets fcrdoResultsets;
        private int queryTimeout = -1; //Set initial value to an invalid value in case of the property was not set by the customer

        #endregion

        #region Constructors

        public FCrdoConnection()
        {
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// The ODBC connection handle
        /// </summary>
        public int hDbc
        {
            get
            {
                if (this.connection != null)
                {
                    return this.connection.GetHashCode();
                }
                return 0;
            }
        }

        /// <summary>
        /// Returns a specific member of an rdoTables collection either by position or by key.
        /// This property is used to access collections of rdoTables objects.For example, the rdoConnection object has an rdoTables property which is 
        /// the collection of rdoTables objects that apply to the rdoConnection object.
        /// </summary>
        public FCrdoTables rdoTables
        {
            get;
            set;
        }

        /// <summary>
        /// Returns a specific member of an rdoQueries collection either by position or by key.
        /// This property is used to access collections of rdoQueries objects.For example, the rdoConnection object has an rdoQueries property which is 
        /// the collection of rdoQueries objects that apply to the rdoConnection object.
        /// </summary>
        public FCrdoQueries rdoQueries
        {
            get;
            set;
        }

        /// <summary>
        /// Returns a specific member of an rdoResultSets collection either by position or by key.
        /// This property is used to access collections of rdoResultSets objects.For example, the rdoConnection object has an rdoResultSets property 
        /// which is the collection of rdoResultSets objects that apply to the rdoConnection object.
        /// </summary>
        public FCrdoResultsets rdoResultSets
        {
            get
            {
                if (fcrdoResultsets == null)
                {
                    fcrdoResultsets = new FCrdoResultsets(this);
                }
                return fcrdoResultsets;
            }
            set
            {
                fcrdoResultsets = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that provides information about the source of an open rdoConnection. The Connect property contains the ODBC connect string. 
        /// This property is always readable, but cannot be changed after the connection is established.
        /// The Connect property return value is a String expression composed of zero or more parameters separated by semicolons, as described in Remarks.
        /// Remarks
        /// When used with the rdoQuery or rdoConnection objects, this property is read-only unless created as a stand-alone object when it is read-write until the 
        /// connection is established.The Connect property becomes read-write when the rdoConnection object is closed.When used with the RemoteData control, this property 
        /// is read-write.
        /// The Connect property is used to pass additional information to and from the ODBC driver manager to establish a connection with a data source.The Connect 
        /// property holds the ODBC connect string which is also used as an argument to the OpenConnection method. When used with a stand-alone rdoConnection or rdoQuery 
        /// objects, the Connect property is used by the EstablishConnection method.
        /// Except when associated with the RemoteData control, once a connection is made, the Connect property is completed with the values optionally supplied by the 
        /// user and the ODBC driver manager. The Connect property of the rdoQuery contains this amended connect string.
        /// The RemoteData control's Connect property is not changed after the connection is established. However, the completed connect string can be extracted from 
        /// the RemoteData control's Connection property.For example:
        /// FullConnect = MSRDC1.Connection.Connect
        /// The following table details valid ODBC connect string arguments and typical usage.Note that each parameter is delineated with a semi-colon (;).
        /// ODBC Connect String Arguments
        /// Parameter Specifies Example
        /// DSN Registered ODBC data source by name.DSN=MyDataSource; 
        /// (If specified when establishing a DSN-less connection, DSN must be the last argument)
        /// UID User name of a recognized user of the database UID=Victoria; 
        /// PWD Password associated with user name PWD=ChemMajor; 
        /// DRIVER Description of driver. (Note brackets for driver names that include spaces.) DRIVER={SQL Server};
        /// DATABASE Default database to use once connected DATABASE = Pubs;
        /// SERVER Name of remote server SERVER = SEQUEL;
        /// WSID Workstation ID(your system's Net name) WSID=MYP5 
        /// APP Application name.At design time this is set to your project name.At runtime this is your.exe name. APP= Accounting
        /// Note Some ODBC drivers require different parameters not shown in this list.
        /// For example, to set the Connect property of a RemoteData control you could use code like the following:
        /// Dim Cnct As String
        /// Cnct = "DSN=WorkData;UID=Chrissy;" & "PWD=MIDFLD;DATABASE=WorkDB;"
        /// RemoteData1.Connect = Cnct
        /// RemoteData1.SQL = "Select Name, City " & " From Teams Where Type = 12"
        /// RemoteData1.Refresh
        /// You can use this same connect string to establish a new connection:
        /// Dim Cn As rdoConnection
        /// Set Cn = rdoEnvironments(0).OpenConnection("", rdDriverNoPrompt, True, Cnct$)
        /// Note Valid parameters are determined by the ODBC driver.The parameters shown in the preceding example are supported by the Microsoft SQL Server ODBC driver.ODBC,
        /// LOGINTIMEOUT and DBQ are not valid parameters of the RemoteData control or the rdoConnection object's Connect property. These parameters are supported by the 
        /// Microsoft Jet database engine, and not by the ODBC driver. To set login timeout delay, you must use the LoginTimeout property of the rdoEnvironment object.
        /// Capturing Missing Arguments
        /// If the connect string is null, the information provided by the DSN is incomplete, or invalid arguments are provided, the connection cannot be established.If 
        /// your code sets the prompt argument of the OpenConnection method or the RemoteData control's Prompt property to prohibit user completion of missing ODBC connect 
        /// arguments, a trappable error is triggered. Otherwise the ODBC driver manager displays a dialog box to gather missing information from the user. Depending on 
        /// the setting of the Prompt argument of the OpenConnection or EstablishConnection methods, these dialogs capture the DSN from a list of registered ODBC data 
        /// sources.Names presented to the user, and optionally, the user ID and password. If the connection fails to succeed using these user-provided values, the dialogs 
        /// are presented again until the connection succeeds or the user cancels the operation. In some cases, the user can create their own DSN using these dialogs.
        /// If a password is required, but not provided in the Connect property setting, a login dialog box is displayed the first time a table is accessed by the ODBC 
        /// driver and each time the connection is closed and reopened.
        /// Connecting with Domain-Managed Security
        /// When connecting to ODBC data sources that support domain-managed security, set the UID and PWD parameters to "". In this case, the Windows NT user name and 
        /// password are passed to the data source for validation.This strategy permits access to the data source by users with access to the NT domain through 
        /// authenticated workstation logons.
        /// You can set the Connect property for an rdoConnection object by providing a connect argument to the OpenConnection method. Once the connection is established, 
        /// you can check the Connect property setting to determine the DSN, database, user name, password, or ODBC data source of the database.
        /// Registering Data Source Names
        /// Before you can establish a connection using a Data Source Name(DSN), you must either manually register the DSN using the Windows control panel application or 
        /// use the rdoRegisterDataSource method.This process establishes the server name, driver name and other options used when referencing this data source.
        /// Establishing DSN-Less Connections
        /// Under the right circumstances you might not need to pre-register a DSN before connecting.If the following conditions are met, RDO can establish a DSN-less 
        /// connection using the RemoteData control, or the OpenConnection or EstablishConnection methods with a fully-populated Connect property or connect string: 
        /// •The connection uses the default named-pipes networking protocol.
        /// •The connection does not set the OEMTOANSI option.
        /// •You specify the name of the server using the SERVER argument in the connect string.
        /// •You specify the name of the ODBC driver using the DRIVER argument in the connect string.
        /// •You set the DSN argument in the connect string (or wherever it appears as in the DataSourceName property of the RemoteData control) to an empty string. The 
        /// empty DSN argument must be specified as the last parameter of the connect string. 
        /// </summary>
        public string Connect
        {
            get;
            set;
        }

        /// <summary>
        /// Data source name (DSN) used for connection.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that specifies the number of seconds the ODBC driver manager waits before a timeout error occurs when a query 
        /// is executed.
        /// value A Long integer representing the number of seconds the driver manager waits before timing out and returning an error. 
        /// Remarks
        /// The default QueryTimeout property setting is 30 seconds.When you're accessing an ODBC data source using the OpenResultset or Execute methods, 
        /// there may be delays due to network traffic or heavy use of the remote server perhaps caused by your query. Rather than waiting indefinitely, 
        /// use the QueryTimeout property to determine how long your application should wait before the QueryTimeout event is fired and your application 
        /// trips a trappable error. At this point you have the option to continue waiting for another 'n' seconds as determined by the QueryTimeout 
        /// property, or cancel the query in progress by using the Cancel argument in the QueryTimeout event procedure.
        /// Setting this property to 0 disables the timer so your query will run indefinitely.Setting QueryTimeout to 0 is not recommended for synchronous 
        /// operations as your application can be blocked for the entire duration of the query.
        /// When used with an rdoConnection object, the QueryTimeout property specifies a global value for all queries associated with the data source.
        /// When you use an rdoQuery object, the rdoConnection object's QueryTimeout property is used as a default value unless you specify a new value in 
        /// the rdoQuery object's QueryTimeout property.
        /// When working with asynchronous queries, the StillExecuting property remains True until the query completes, or the query timeout period is 
        /// exhausted.
        /// If the specified timeout exceeds the maximum timeout permitted by the data source, or is smaller than the minimum timeout, the driver 
        /// substitutes that value and the following error is logged to the rdoErrors collection: SQLState 01S02: "Option value changed."
        /// </summary>
        public int QueryTimeout
        {
            get
            {
                return queryTimeout;
            }
            set
            {
                queryTimeout = value;
            }
        } 

        /// <summary>
        /// Returns or sets a value that specifies the number of seconds the ODBC driver manager waits before a timeout error occurs when a connection 
        /// is opened.
        /// value A Long integer representing the number of seconds the driver manager waits before timing out and returning an error. 
        /// Remarks
        /// If value is 0, no timeout occurs and an error does not occur if a connection cannot be established.If you are not using asynchronous 
        /// connections, this might cause your application to block indefinitely.
        /// When you're attempting to connect to an ODBC database, such as SQL Server, there may be delays due to network traffic or heavy use of the 
        /// ODBC data source. Rather than waiting indefinitely, you can specify how long to wait before the ODBC driver manager produces an error.
        /// The default timeout value is either 15 seconds or a value set by the rdoDefaultLoginTimeout property.When used with an rdoEnvironment object, 
        /// the LoginTimeout property specifies a global value for all login operations associated with the rdoEnvironment.The LoginTimeout setting of 
        /// on an rdoConnection object overrides the default value.
        /// If the specified timeout exceeds the maximum timeout in the data source, or is smaller than the minimum timeout, the driver substitutes 
        /// that value and the following error is logged in the rdoErrors collection: SQLState 01S02 "Option value changed."
        /// Typically, a connection to a remote server on a Local Area Network (LAN) takes under eight seconds to complete. Remote Access Service (RAS) 
        /// or Internet connections can take far longer depending on Wide Area Network bandwidth, load and other factors.
        /// </summary>
        public int LoginTimeout
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that specifies the type of cursor to be created.
        /// value An Integer or constant as described in Settings.
        /// Settings
        /// Constant Value Description
        /// rdUseIfNeeded 0 The ODBC driver will choose the appropriate style of cursors. Server-side cursors are used if they are available.
        /// rdUseOdbc  1 RemoteData will use the ODBC cursor library.
        /// rdUseServer  2 Use server-side cursors. 
        /// rdUseClientBatch 3 RDO will use the optimistic batch cursor library. 
        /// Remarks
        /// The CursorDriver property only affects connections established after the CursorDriver property has been set the property is read-only on 
        /// existing connections.
        /// When the initial (default), and each subsequent rdoEnvironment object is created, the CursorDriver property is set from the rdoEngine 
        /// object's rdoDefaultCursorDriver property which is set using the same constants.
        /// Choosing a Cursor Driver
        /// Choosing the right cursor driver can have a significant impact on the overall performance of your application, what resources are consumed 
        /// by the cursor, and limit the type or complexity of the cursors you create.Each type of cursor has its own benefits and limitations.In many 
        /// cases, the best choice is no cursor at all because your application often does not need to scroll through the data or perform update operations 
        /// against a keyset.
        /// The following paragraphs outline the functionality and suggested purposes for each of the cursor types.
        /// •Server-Side Cursors •This cursor library maintains the cursor keyset on the server (in TempDB) which eliminates the need to transmit the 
        /// keyset to the workstation where it consumes needed resources.However, this cursor driver consumes TempDB space on the remote server so this 
        /// database must be expanded to meet this requirement.Cursors created with the server-side driver cannot contain more than one SELECT statement 
        /// if they do, a trappable error is fired.You can still use the server-side cursor driver with multiple result set queries if you disable the 
        /// cursor by creating a forward-only, read-only cursor with a rowset size of one. Not all remote servers support server-side cursors. Note that 
        /// server-side cursors are enabled when using either rdUseIfNeeded or rdUseServer against Microsoft SQL Server databases.
        /// •ODBC Client-Side Cursors •This cursor library builds keysets on the workstation in local RAM overflowing to disk if necessary.Because of 
        /// this design considerably more network operations must be performed to initially create the keyset, but with small cursors this should not 
        /// impose a significant load on the workstation or network.ODBC client-side cursors do not impose any type of restriction on the type of query 
        /// executed. This option gives better performance for small result sets, but degrades quickly for larger result sets.
        /// •Client-Batch Cursors •This cursor library is designed to deal with the special requirements of optimistic batch updates and several other 
        /// more complex cursor features. Client-batch cursors are required for dissociate connections, batch mode, and multi-table updates. This cursor 
        /// also supports delayed BLOB column fetch, buffered cursors, and additional control over updates.This library is somewhat larger than the others, 
        /// but also performs better in many situations. 
        /// •The No-Cursor Option •In cases where you need to fetch rows quickly, or perform action queries against the database without the overhead of 
        /// a cursor, you can choose to instruct RDO to bypass creation of a cursor. Basically, this option creates a forward-only, read-only result set 
        /// with a RowsetSize set to 1. This option can improve performance in many operations. While you cannot update rows or scroll between rows with 
        /// this cursor, you can submit independent action queries to manipulate data.This option is especially useful when accessing data through stored 
        /// procedures. 
        /// </summary>
        public FCRDO.CursorDriverConstants CursorDriver
        {
            // TODO:CHE
            get;
            set;
        }


        #endregion

        #region Internal Properties

        internal SqlTransaction Trans
        {
            get
            {
                return trans;
            }

            set
            {
                trans = value;
            }
        }

        #endregion

        #region Public Methods

        public void BeginTrans()
        {
            trans = this.connection.BeginTransaction();
        }

        /// <summary>
        /// 
        /// </summary>
        public void CommitTrans()
        {
            if (trans != null)
            {
                trans.Commit();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RollbackTrans()
        {
            if (trans != null)
            {
                trans.Rollback();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="readOnly"></param>
        /// <param name="options"></param>
        public void EstablishConnection(FCRDO.PromptConstants prompt, bool readOnly = false, int options = 0)
        {
            this.connection = new System.Data.SqlClient.SqlConnection(this.Connect);
            this.connection.Open();
        }

        public void Close()
        {
            if (this.connection != null && connection.State != ConnectionState.Closed)
            {
                this.connection.Close();
            }
        }

        /// <summary>
        /// Runs an action query or executes an SQL statement that does not return rows.
        /// connection.Execute source[, options]
        /// query.Execute[options]
        /// connection An object expression that evaluates to the rdoConnection object on which the query will run.
        /// query An object expression that evaluates to the rdoQuery object whose SQL property setting specifies the SQL statement to execute.
        /// source A string expression that contains the action query to execute or the name of an rdoQuery.
        /// options A Variant or constant that determines how the query is run, as specified in Settings.
        /// You can use the following constants for the options argument:
        /// rdAsyncEnable 32 Execute operation asynchronously.
        /// rdExecDirect 64 (Default.) Bypass creation of a stored procedure to execute the query.Uses SQLExecDirect instead of SQLPrepare and SQLExecute. 
        /// Remarks
        /// It is recommended that you use the Execute method only for action queries. Because an action query doesnt return any rows, Execute doesnt return an 
        /// rdoResultset. You can use the Execute method on queries that execute multiple statements, but none of these batched statements should return rows.To execute 
        /// multiple result set queries that are a combination of action and SELECT queries, use the OpenResultset method.
        /// Use the RowsAffected property of the rdoConnection or rdoQuery object to determine the number of rows affected by the most recent Execute method. RowsAffected 
        /// contains the number of rows deleted, updated, or inserted when executing an action query.When you use the Execute method to run an rdoQuery, the RowsAffected 
        /// property of the rdoQuery object is set to the number of rows affected.
        /// Options
        /// To execute the query asynchronously, use the rdAsyncEnable option (which is set by default). If set, the data source query processor immediately begins to 
        /// process the query and returns to your application before the query is complete.Use the StillExecuting property to determine when the query processor is ready 
        /// to return the results from the query.Use the Cancel method to terminate processing of an asynchronous query.
        /// To bypass creation of a temporary stored procedure to execute the query, use the rdExecDirect option.This option is required when the query contains references 
        /// to transactions or temporary tables that only exist in the context of a single operation.For example, if you include a Begin Transaction TSQL statement in your 
        /// query or reference a temporary table, you must use rdExecDirect to ensure that the remote engine is not confused when these objects are left pending at the 
        /// end of the query.
        /// While it is possible to execute stored procedures using the Execute method, it is not recommended because the procedures return value and output parameters 
        /// are discarded and the procedure cannot return rows.Use the OpenResultset method against an rdoQuery to execute stored procedures.
        /// Note When executing stored procedures that do not require parameters, do not include the parenthesis in the SQL statement.For example, to execute the "MySP" 
        /// procedure use the following syntax: {Call MySP }.
        /// Also, a call like:
        /// rCn.Execute SqlStatement, rdAsyncEnable + rdExecDirect
        /// allows only one outstanding request and allows Visual Basic code to overlap with SQL Server processing, but doesn't allow multiple outstanding SQL Server 
        /// requests.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="options"></param>
        public void Execute(string source, FCRDO.OptionConstants options = FCRDO.OptionConstants.rdAsyncEnable)
        {
            // TODO:CHE options
            SqlCommand command = new SqlCommand(source, connection);
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            command.Transaction = trans;
            if (QueryTimeout != -1)
            {
                command.CommandTimeout = this.QueryTimeout;
            }

            try
            {
                FCRDO.rdoEngine.rdoErrors.Clear();
                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                FCrdoError rdoError = new FCrdoError();
                rdoError.Number = ex.Number;
                rdoError.Description = ex.Message;
                rdoError.Source = ex.Source;
                FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                throw ex;
            }
        }

        /// <summary>
        /// Creates a new rdoResultset object.
        /// Syntax
        /// Set variable = connection.OpenResultset(name[, type[, locktype[, option]]])
        /// Set variable = object.OpenResultset([type[, locktype[, option]]])
        /// connection An object expression that evaluates to an existing rdoConnection object you want to use to create the new rdoResultset.
        /// object An object expression that evaluates to an existing rdoQuery or rdoTable object you want to use to create the new rdoResultset.
        /// name A String that specifies the source of the rows for the new rdoResultset.This argument can specify the name of an rdoTable object, the name of an rdoQuery, 
        /// or an SQL statement that might return rows.
        /// type A Variant or constant that specifies the type of cursor to create as indicated in Settings.
        /// locktype A Variant or constant that specifies the type of concurrency control.If you dont specify a locktype, rdConcurReadOnly is assumed.
        /// option A Variant or constant that specifies characteristics of the new rdoResultset.
        /// Settings 
        /// •name
        /// The name argument is used when the OpenResultset method is used against the rdoConnection object, and no query has been pre-defined.In this case, name 
        /// typically contains a row-returning SQL query.The query can contain more than one SELECT statement, or a combination of action queries and SELECT statements, 
        /// but not just action queries, or a trappable error will result. See the SQL property for additional details.
        /// •Cursor type
        /// Note Not all types of cursors and concurrency are supported by every ODBC data source driver. See rdoResultset for more information. In addition, not all 
        /// types of cursor drivers support SQL statements that return more than one set of results. For example, server-side cursors do not support queries that contain 
        /// more than one SELECT statement.
        /// The type argument specifies the type of cursor used to manage the result set. If you dont specify a type, OpenResultset creates a forward-only rdoResultset. 
        /// Not all ODBC data sources or drivers can implement all of the cursor types. If your driver cannot implement the type chosen, a warning message is generated and 
        /// placed in the rdoErrors collection.Use one of the following result set type constants that defines the cursor type of the new rdoResultset object. For 
        /// additional details on types of cursors, see the CursorType property.
        /// type Constant Value Description
        /// rdOpenForwardOnly 0 (Default) Opens a forward-only-type rdoResultset object. 
        /// rdOpenKeyset 1 Opens a keyset-type rdoResultset object. 
        /// rdOpenDynamic 2 Opens a dynamic-type rdoResultset object.  
        /// rdOpenStatic 3 Opens a static-type rdoResultset object. 
        /// •Concurrency LockType
        /// In order to maintain adequate control over the data being updated, RDO provides a number of concurrency options that control how other users are granted, 
        /// or refused access to the data being updated.In many cases, when you lock a particular row using one of the LockType settings, the remote engine might also 
        /// lock the entire page containing the row.If too many pages are locked, the remote engine might also escalate the page lock to a table lock to improve overall 
        /// system performance.
        /// Not all lock types are supported on all data sources.For example, for SQL Server and Oracle servers, static-type rdoResultset objects can only support 
        /// rdConcurValues or rdConcurReadOnly.For additional details on the types of concurrency, see the LockType property.
        /// locktype Constant Value Description
        /// rdConcurReadOnly 1 (Default) Read-only.
        /// rdConcurLock 2 Pessimistic concurrency.
        /// rdConcurRowVer 3 Optimistic concurrency based on row ID. 
        /// rdConcurValues 4 Optimistic concurrency based on row values.  
        /// rdConcurBatch 5 Optimistic concurrency using batch mode updates.Status values returned for each row successfully updated. 
        /// •Other options
        /// If you use the rdAsyncEnable option, control returns to your application as soon as the query is begun, but before a result set is available.To test for 
        /// completion of the query, use the StillExecuting property. The rdoResultset object is not valid until StillExecuting returns False. You can also use the 
        /// QueryComplete event to determine when the query is ready to process.Until the StillExecuting property returns True, you cannot reference any other property 
        /// of the uninitialized rdoResultset object and only the Cancel and Close methods are valid.
        /// If you use the rdExecDirect option, RDO uses the SQLExecDirect ODBC API function to execute the query.In this case, no temporary stored procedure is created 
        /// to execute the query.This option can save time if you dont expect to execute the query more than a few times in the course of your application.In addition, 
        /// when working with queries that should not be run as stored procedures but executed directly, this option is mandatory.For example, in queries that create 
        /// temporary tables for use by subsequent queries, you must use the rdExecDirect option.
        /// You can use the following constants for the options argument:
        /// Constant Value Description
        /// rdAsyncEnable 32 Execute operation asynchronously.
        /// rdExecDirect 64 (Default.) Bypass creation of a stored procedure to execute the query.Uses SQLExecDirect instead of SQLPrepare and SQLExecute. 
        /// Remarks
        /// If the OpenResultset method succeeds, RDO instantiates a new rdoResultset object and appends it to the rdoResultsets collection even if no rows are returned 
        /// by the query.If the query fails to compile or execute due to a syntax error, permissions problem or other error, the rdoResultset is not created and a trappable 
        /// error is fired.The rdoResultset topic contains additional details on rdoResultset behavior and managing the rdoResultsets collection.
        /// Note   RDO 2.0 behaves differently than RDO 1.0 in how it handles orphaned references to rdoResultset objects. When you Set a variable already assigned to an 
        /// rdoResultset object with another rdoResultset object using the OpenResultset method, the existing rdoResultset object is closed and dropped from the 
        /// rdoResultsets collection.In RDO 1.0, the existing object remained open and was left in the rdoResultsets collection.
        /// Note Before you can use the name of a base table in the name argument, you must first use the Refresh method against the rdoTables collection to populate it.
        /// You can also populate the rdoTables collection by referencing one of its members by its ordinal number. For example, referencing rdoTables(0) will populate the 
        /// entire collection.
        /// Executing Multiple Operations on a Connection
        /// If there is an unpopulated rdoResultset pending on a data source that can only support a single operation on an rdoConnection object, you cannot create 
        /// additional rdoQuery or rdoResultset objects using the OpenResultset method, or use the Refresh method on the rdoTable object until the rdoResultset is flushed, 
        /// closed, or fully populated.For example, when using SQL Server 4.2 as a data source, you cannot create an additional rdoResultset object until you move to the 
        /// last row of the last result set of the current rdoResultset object. To populate the result set, use the MoreResults method to move through all pending result 
        /// sets, or use the Cancel or Close method on the rdoResultset to flush all pending result sets.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="options"></param>
        /// <param name="LockEdit"></param>
        /// <returns></returns>
        public FCrdoResultset OpenResultset(string name, FCRDO.ResultsetTypeConstants type = FCRDO.ResultsetTypeConstants.rdOpenKeyset, FCRDO.LockTypeConstants LockEdit = FCRDO.LockTypeConstants.rdConcurReadOnly, FCRDO.OptionConstants options = FCRDO.OptionConstants.rdExecDirect)
        {
            FCrdoResultset resultset = new FCrdoResultset(this);
            SqlDataAdapter adapter;

            int noCountValue = -1;
            SqlCommand noCountCommand = null;

            //The source can be a table name, a query name, or an SQL statement that returns records 
            string upperName = name.Trim().ToUpper();
            if (upperName.StartsWith("SELECT "))
            {
                //CHE: use % in wildcard instead of * used in VB6
                Regex regex = new Regex(@"(.* )(like '.*')(.*)", RegexOptions.IgnoreCase);
                foreach (Match m in regex.Matches(name))
                {
                    string condition = m.Groups[2].Value;
                    if (condition.Contains('*'))
                    {
                        condition = condition.Replace('*', '%');
                        name = m.Groups[1].Value + condition + m.Groups[3].Value;
                    }
                }

                adapter = new SqlDataAdapter(name, connection);

                //CHE: set TableName necessary in UpdateBatchBuilder; identify first "from" in string to set correct TableName
                string SourceLowercase = name.ToLower();

                System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(name, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*) where", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (match.Success && match.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                {
                    resultset.TableName = match.Groups[1].Value;
                }
                else
                {
                    System.Text.RegularExpressions.Match match1 = System.Text.RegularExpressions.Regex.Match(name, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*) order by", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (match1.Success && match1.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                    {
                        resultset.TableName = match1.Groups[1].Value;
                    }
                    else
                    {
                        //CHE: treat alias
                        System.Text.RegularExpressions.Match match2 = System.Text.RegularExpressions.Regex.Match(name, "from ([a-zA-Z0-9üöäÄÜÖß_\\. ]*) where", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        if (match2.Success && match2.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                        {
                            if (match2.Groups[1].Value.Contains(" "))
                            {
                                resultset.TableName = match2.Groups[1].Value.Substring(0, match2.Groups[1].Value.IndexOf(" "));
                            }
                            else
                            {
                                resultset.TableName = match2.Groups[1].Value;
                            }
                        }
                        else
                        {
                            System.Text.RegularExpressions.Match match3 = System.Text.RegularExpressions.Regex.Match(name, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*);", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                            if (match3.Success && match3.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                            {
                                resultset.TableName = match3.Groups[1].Value;
                            }
                        }
                    }
                }
            }
            else if (upperName.StartsWith("BEGIN"))
            {
                noCountCommand = this.connection.CreateCommand();
                noCountCommand.Transaction = trans;
                noCountCommand.CommandType = CommandType.Text;

                noCountCommand.CommandText = "select @@OPTIONS & 512";
                noCountValue = Convert.ToInt32(noCountCommand.ExecuteScalar());
                noCountCommand.CommandText = "SET NOCOUNT ON";
                noCountCommand.ExecuteNonQuery();
                adapter = new SqlDataAdapter(name, connection);
            }
            else
            {
                adapter = new SqlDataAdapter(string.Format("select * from {0}", name), connection);
                resultset.TableName = name;
            }

            if (resultset.TableName.ToLower().StartsWith("dbo."))
            {
                resultset.TableName = resultset.TableName.Substring(4);
            }

            //This builds the update and Delete queries for the table in the above SQL. this only works if the select is a single table. 
            SqlCommandBuilder oleDbCommandBuilder = new SqlCommandBuilder(adapter);

            if (adapter.SelectCommand != null)
            {
                adapter.SelectCommand.Transaction = trans;
                if (QueryTimeout != -1)
                {
                    adapter.SelectCommand.CommandTimeout = this.QueryTimeout;
                }
            }

            adapter.Fill(resultset);

            resultset.Source = name;

            if (upperName.StartsWith("BEGIN") && noCountValue == 0)
            {
                noCountCommand.CommandText = "SET NOCOUNT OFF";
                noCountCommand.ExecuteNonQuery();
            }

            resultset.DataAdapter = adapter;

            this.rdoResultSets.AddResultset(resultset);

            if (name.Length > 256)
            {
                resultset.Name = name.Substring(0, 256);
            }
            else
            {
                resultset.Name = name;
            }

            return resultset;
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
