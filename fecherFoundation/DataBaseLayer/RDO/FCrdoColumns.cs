﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// rdoColumns
    /// </summary>
    public class FCrdoColumns :  IEnumerable<FCrdoColumn>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private List<FCrdoColumn> columns;
        private FCrdoResultset dataTable;

        #endregion

        #region Constructors

        public FCrdoColumns(FCrdoResultset table)
        {
            this.dataTable = table;
            this.columns = new List<FCrdoColumn>();

            if (table == null)
                return;

            foreach (DataColumn col in table.Columns)
            {
                this.columns.Add(new FCrdoColumn(table, col));
            }
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public int Count
        {
            get
            {
                return (columns == null) ? 0 : columns.Count;
            }
        }

        public FCrdoColumn this[int index]
        {
            get
            {
                return this.GetColumn(index);
            }
            set
            {
                FCrdoColumn column = this.GetColumn(index);
                column = value;
            }
        }

        public FCrdoColumn this[string name]
        {
            get
            {
                return this.GetColumn(name);
            }
            set
            {
                FCrdoColumn column = this.GetColumn(name);
                column = value;
            }
        }
        
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCrdoColumn> GetEnumerator()
        {
            return columns.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal void Append(FCrdoColumn column, bool createColumnInDB)
        {
            columns.Add(column);

            //IPI: VB6 Append function creates the column in the DB if it does not exist; but when retrieving all fields from the DB, we iterate through the columns collection returned by schema table
            // so all columns already exist in the DB 
            if (!createColumnInDB)
            {
                return;
            }

            //JEI
            using (SqlConnection connection = new SqlConnection(this.dataTable.ActiveConnection.connection.ConnectionString))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                //if column already exists, don't add it again
                if (fieldExists(connection, this.dataTable.TableName, column.Name))
                {
                    return;
                }

                DbTransaction trans = connection.BeginTransaction();
                try
                {
                    DbCommand command = connection.CreateCommand();
                    command.Transaction = trans;
                    string NotNull = column.Required ? "NOT NULL" : "";
                    string tableName = this.dataTable.TableName;
                    string columnName = column.Name;
                    string columnType = ConvertToString(column.Type);
                    if (columnType == "varchar" || columnType == "float")
                    {
                        command.CommandText = "ALTER TABLE " + tableName + " Add Column " + columnName + " " + columnType + "(" + column.Size + ") " + NotNull;
                    }
                    else
                    {
                        command.CommandText = "ALTER TABLE " + tableName + " Add Column " + columnName + " " + columnType + " " + NotNull;
                    }

                    try
                    {
                        FCRDO.rdoEngine.rdoErrors.Clear();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        FCrdoError rdoError = new FCrdoError();
                        rdoError.Number = ex.Number;
                        rdoError.Description = ex.Message;
                        rdoError.Source = ex.Source;
                        FCRDO.rdoEngine.rdoErrors.Add(rdoError);

                        throw ex;
                    }

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        internal FCrdoColumn GetColumn(int index)
        {
            return columns[index];
        }

        internal FCrdoColumn GetColumn(string columnName)
        {
            columnName = columnName.ToUpper();
            //get last from list to be the same as in VB6 for fields having same name
            return columns.FindLast(delegate(FCrdoColumn target)
            {
                return target.Name.ToUpper() == columnName;
            });
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private bool fieldExists(DbConnection connection, string tableName, string fieldName)
        {
            DataTable tblColumns = connection.GetSchema("Columns");
            DataRow[] tblColumnsRows = tblColumns.Select("COLUMN_NAME = '" + fieldName + "' AND TABLE_NAME = '" + tableName + "'");
            return tblColumnsRows.Length > 0;
        }

        private string ConvertToString(FCRDO.DataTypeConstants dataType)
        {
            switch (dataType)
            {
                case FCRDO.DataTypeConstants.rdTypeDATE:
                    return "datetime";
                case FCRDO.DataTypeConstants.rdTypeTIME:
                    return "time";
                case FCRDO.DataTypeConstants.rdTypeTIMESTAMP:
                    return "timestamp";
                case FCRDO.DataTypeConstants.rdTypeINTEGER:
                case FCRDO.DataTypeConstants.rdTypeBIGINT:
                    return "int";
                case FCRDO.DataTypeConstants.rdTypeTINYINT:
                    return "tinyint";
                case FCRDO.DataTypeConstants.rdTypeBIT:
                    return "bit";
                case FCRDO.DataTypeConstants.rdTypeFLOAT:
                case FCRDO.DataTypeConstants.rdTypeDECIMAL:
                case FCRDO.DataTypeConstants.rdTypeDOUBLE:
                case FCRDO.DataTypeConstants.rdTypeNUMERIC:
                    return "float";
                case FCRDO.DataTypeConstants.rdTypeCHAR:
                    return "varchar";
                case FCRDO.DataTypeConstants.rdTypeBINARY:
                case FCRDO.DataTypeConstants.rdTypeVARBINARY:
                    return "varbinary";
                default:
                    return "";
            }
        }

        #endregion
    }
}
