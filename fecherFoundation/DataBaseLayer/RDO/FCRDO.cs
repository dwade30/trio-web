﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// https://msdn.microsoft.com/en-us/library/aa266545(v=vs.60).aspx
    /// </summary>
    public static class FCRDO
    {
        #region Public Members

        public static FCrdoEngine rdoEngine
        {
            get
            {
                if (rdoEngine1 == null)
                {
                    rdoEngine1 = new FCrdoEngine();
                }
                return rdoEngine1;
            }
            set { rdoEngine1 = value; }
        }

        #endregion

        #region Private Members

        private static FCrdoEngine rdoEngine1;

        #endregion

        #region Enums            

        public enum CursorDriverConstants
        {
            rdUseIfNeeded = 0, //The ODBC driver will choose the appropriate style of cursors. Server-side cursors are used if they are available. 
            rdUseOdbc = 1, //RemoteData will use the ODBC cursor library.  
            rdUseServer = 2, //Use server-side cursors. 
            rdUseClientBatch = 3 //RDO will use the optimistic batch cursor library. 
        }        

        public enum DataTypeConstants
        {
            rdTypeCHAR = 1, //Fixed-length character string. Length set by Size property.
            rdTypeNUMERIC = 2, //Signed, exact, numeric value with precision p and scale s(1 ≤ p ≤15; 0 ≤ s ≤ p). 
            rdTypeDECIMAL = 3, //Signed, exact, numeric value with precision p and scale s(1 ≤ p ≤15; 0 ≤ s ≤ p). 
            rdTypeINTEGER = 4, //Signed, exact numeric value with precision 10, scale 0 (signed: -231  n  231-1; unsigned:  0  n  232-1).
            rdTypeSMALLINT = 5, //Signed, exact numeric value with precision 5, scale 0 (signed: -32,768  n  32,767, unsigned: 0  n  65,535). 
            rdTypeFLOAT = 6, //Signed, approximate numeric value with mantissa precision 15 (zero or absolute value 10-308 to 10308). 
            rdTypeREAL = 7, //Signed, approximate numeric value with mantissa precision 7 (zero or absolute value 10-38 to 1038). 
            rdTypeDOUBLE = 8, //Signed, approximate numeric value with mantissa precision 15 (zero or absolute value 10-308 to 10308). 
            rdTypeDATE = 9, //Date data source dependent.
            rdTypeTIME = 10, //Time data source dependent. 
            rdTypeTIMESTAMP = 11, //TimeStamp data source dependent. 
            rdTypeVARCHAR = 12, //Variable-length character string. Maximum length 255. 
            rdTypeLONGVARCHAR = -1, //Variable-length character string. Maximum length determined by data source. 
            rdTypeBINARY = -2, //Fixed-length binary data.Maximum length 255. 
            rdTypeVARBINARY = -3, //iable-length binary data.Maximum length 255. 
            rdTypeLONGVARBINARY = -4, //Variable-length binary data.Maximum data source dependent. 
            rdTypeBIGINT = -5, //Signed, exact numeric value with precision 19 (signed) or 20 (unsigned), scale 0; (signed: -263 ≤ n ≤ 263-1; unsigned: 0 ≤ n ≤ 264-1). 
            rdTypeTINYINT = -6, //Signed, exact numeric value with precision 3, scale 0; (signed: -128 ≤ n ≤ 127, unsigned: 0 ≤ n ≤ 255). 
            rdTypeBIT = -7 //Single binary digit.
        }

        public enum DirectionConstants
        {
            rdParamInput = 0, //(Default) The parameter is used to pass information to the procedure. 
            rdParamInputOutput = 1, //The parameter is used to pass information both to and from the procedure. 
            rdParamOutput = 2, //The parameter is used to return information from the procedure as in an output parameter in SQL. 
            rdParamReturnValue = 3 //The parameter is used to return the return status value from a procedure. 
        }

        public enum ResultsetTypeConstants
        {
            rdOpenForwardOnly = 0, //Fixed set, non-scrolling.
            rdOpenStatic = 3, //A static-type rdoResultset.
            rdOpenKeyset = 1, //(Default)A keyset-type rdoResultset.
            rdOpenDynamic = 2 //Updatable, dynamic set, scrollable query result set cursor
        }

        public enum LockTypeConstants
        {
            rdConcurReadOnly = 1, //(Default) Cursor is read-only. No updates are allowed. 
            rdConcurLock = 2, //Pessimistic concurrency. 
            rdConcurRowVer = 3, //Optimistic concurrency based on row ID. 
            rdConcurValues = 4, //Optimistic concurrency based on row values. 
            rdConcurBatch = 5 //Optimistic concurrency using batch mode updates.Status values returned for each row successfully updated. 
        }

        public enum OptionConstants
        {
            rdAsyncEnable = 32, //Execute operation asynchronously. 
            rdExecDirect = 64 //(Default.) Bypass creation of a stored procedure to execute the query. Uses SQLExecDirect instead of SQLPrepare and SQLExecute. 
        }

        public enum PromptConstants
        {
            rdDriverPrompt = 0, //The driver manager displays the ODBC Data Sources dialog box. The connection string used to establish the connection is constructed from the data source name(DSN) selected and completed by the user via the dialog boxes.Or, if no DSN is chosen and the DataSourceName property is empty, the default DSN is used.
            rdDriverNoPrompt = 1, //The driver manager uses the connection string provided in connect.If sufficient information is not provided, the OpenConnection method returns a trappable error.
            rdDriverComplete = 2, //If the connection string provided includes the DSN keyword, the driver manager uses the string as provided in connect, otherwise it behaves as it does when rdDriverPrompt is specified.
            rdDriverCompleteRequired = 3 //(Default) Behaves like rdDriverComplete except the driver disables the controls for any information not required to complete the connection.
        }

        #endregion

        #region Public Methods

        public static SqlDbType ConvertToSqlType(DataTypeConstants dataType)
        {
            SqlDbType oleType = SqlDbType.VarChar;
            switch (dataType)
            {
                case FCRDO.DataTypeConstants.rdTypeCHAR:
                    {
                        oleType = SqlDbType.Char;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeNUMERIC:
                    {
                        oleType = SqlDbType.Real;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeDECIMAL:
                    {
                        oleType = SqlDbType.Decimal;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeINTEGER:
                    {
                        oleType = SqlDbType.Int;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeSMALLINT:
                    {
                        oleType = SqlDbType.SmallInt;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeFLOAT:
                    {
                        oleType = SqlDbType.Real;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeREAL:
                    {
                        oleType = SqlDbType.Real;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeDOUBLE:
                    {
                        oleType = SqlDbType.Real;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeDATE:
                    {
                        oleType = SqlDbType.Date;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeTIME:
                    {
                        oleType = SqlDbType.Time;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeTIMESTAMP:
                    {
                        oleType = SqlDbType.Timestamp;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeVARCHAR:
                    {
                        oleType = SqlDbType.VarChar;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeLONGVARCHAR:
                    {
                        oleType = SqlDbType.Text;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeBINARY:
                    {
                        oleType = SqlDbType.Binary;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeVARBINARY:
                    {
                        oleType = SqlDbType.VarBinary;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeLONGVARBINARY:
                    {
                        oleType = SqlDbType.VarBinary;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeBIGINT:
                    {
                        oleType = SqlDbType.BigInt;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeTINYINT:
                    {
                        oleType = SqlDbType.TinyInt;
                        break;
                    }
                case FCRDO.DataTypeConstants.rdTypeBIT:
                    {
                        oleType = SqlDbType.Bit;
                        break;
                    }
            }
            return oleType;
        }

        public static ParameterDirection ConvertToParameterDirection(FCRDO.DirectionConstants rdoDirection)
        {
            ParameterDirection direction = ParameterDirection.InputOutput;
            switch(rdoDirection)
            {
                case DirectionConstants.rdParamInput:
                    {
                        direction = ParameterDirection.Input;
                        break;
                    }
                case DirectionConstants.rdParamInputOutput:
                    {
                        direction = ParameterDirection.InputOutput;
                        break;
                    }
                case DirectionConstants.rdParamOutput:
                    {
                        direction = ParameterDirection.Output;
                        break;
                    }
                case DirectionConstants.rdParamReturnValue:
                    {
                        direction = ParameterDirection.ReturnValue;
                        break;
                    }
            }
            return direction;
        }

        #endregion

    }
}
