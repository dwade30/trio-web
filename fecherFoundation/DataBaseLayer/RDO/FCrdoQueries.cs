﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// 
    /// </summary>
    public class FCrdoQueries : IEnumerable<FCrdoQuery>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private List<FCrdoQuery> queries;

        #endregion

        #region Constructors

        public FCrdoQueries()
        {
            this.queries = new List<FCrdoQuery>();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Returns the number of objects in the specified collection. Read-only.
        /// </summary>
        public int Count
        {
            get
            {
                return (queries == null) ? 0 : queries.Count;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCrdoQuery> GetEnumerator()
        {
            return queries.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal void AddrdoQuery(FCrdoQuery query)
        {
            queries.Add(query);
        }

        internal void RemoverdoQuery(FCrdoQuery query)
        {
            queries.Remove(query);
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
