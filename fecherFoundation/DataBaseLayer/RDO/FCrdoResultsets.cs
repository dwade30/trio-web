﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer.RDO
{
    /// <summary>
    /// DAO.rdoResultsets
    /// </summary>
    public class FCrdoResultsets : IEnumerable<FCrdoResultset>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private List<FCrdoResultset> recordsets;
        private FCrdoConnection connection;

        #endregion

        #region Constructors

        public FCrdoResultsets(FCrdoConnection connection)
        {
            this.connection = connection;
            this.recordsets = new List<FCrdoResultset>();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Returns the number of objects in the specified collection. Read-only.
        /// </summary>
        public int Count
        {
            get
            {
                return (recordsets == null) ? 0 : recordsets.Count;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCrdoResultset> GetEnumerator()
        {
            return recordsets.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal void AddResultset(FCrdoResultset recordset)
        {
            recordsets.Add(recordset);
        }

        internal void RemoveResultset(FCrdoResultset recordset)
        {
            recordsets.Remove(recordset);
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
