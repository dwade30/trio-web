﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    /// FCDataTable interface
    /// </summary>
    public interface IFCDataTable
    {
        IFCDataTable CloneRows();
    }
}
