﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    public static class FCDAO
    {
        #region Public Members

        public static FCDBEngine DBEngine
        {
            get
            {
                if (dbEngine == null)
                {
                    dbEngine = new FCDBEngine();
                }
                return dbEngine;
            }
            set { dbEngine = value; }
        }

        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private static FCDBEngine dbEngine;

        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
