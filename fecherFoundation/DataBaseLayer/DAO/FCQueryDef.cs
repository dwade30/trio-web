﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.DataBaseLayer
{
    public class FCQueryDef
    {
        public string Name { get; private set; }
        public string Query { get; private set; }
        public FConnection Connection { get; private set; }
        public object[] Parameters;


        public FCQueryDef(string name, string query, FConnection connection)
        {
            Connection = connection;
            Name = name;
            Query = query;
            Parameters = new object[100];


        }
        public FCRecordset OpenRecordset()
        {
            Connection.Open();

            return Connection.OpenRecordset(Query, RecordsetTypeEnum.dbOpenTable);

        }

        private string parameterizeSql()
        {


            if (Query.ToLower().Contains("exec"))
            {
                Query = Query.Replace("(", " ");
                Query = Query.Replace(")", " ");
            }
            StringBuilder retval = new StringBuilder(Query);
            int pos = 0;
            int index = 0;
            decimal decvalue = 0;
            DateTime dtvalue;
            String sValue = "";
            bool docontinue = true;

            int poskomma = 0;

            while (retval.ToString().IndexOf('?', pos) > 0)
            {
                pos = retval.ToString().IndexOf('?', pos);
                retval.Remove(pos, 1);

                if (Parameters[index] == null)
                {
                    retval.Insert(pos, "default"); ;
                }
                else
                {
                    poskomma = Parameters[index].ToString().IndexOf(',');

                    docontinue = true;

                    if (Parameters[index].ToString().Length > 0)
                    {
                        if (Parameters[index].ToString().Substring(0, 1) == "!")
                        {
                            // Force String with ! at the beginning
                            // Cut !
                            sValue = Parameters[index].ToString().Substring(1);
                            retval.Insert(pos, String.Format("'{0}'", sValue));
                            docontinue = false;
                        }
                    }

                    if (docontinue == true)
                    {
                        if (Parameters[index] is DateTime)
                        {
                            retval.Insert(pos, String.Format("'{0}'", ((DateTime)Parameters[index]).ToString(@"yyyy-MM-dd\THH:mm:ss"))); ;
                        }
                        else if (Parameters[index] is Decimal)
                        {
                            retval.Insert(pos, String.Format("'{0}'", FormatDecimalForDB((Decimal)Parameters[index]))); ;
                        }
                        // now try to convert the values
                        // first exact Date in 01.01.2000
                        else if (DateTime.TryParseExact(Parameters[index].ToString(), "dd.MM.yyyy", new CultureInfo("de-DE"), DateTimeStyles.None, out dtvalue))
                        {
                            retval.Insert(pos, String.Format("'{0}'", (dtvalue.ToString(@"yyyy-MM-dd\THH:mm:ss")))); ;
                        }
                        // Test if string contains a comma
                        else if (poskomma > 0)
                        {
                            // Try to convert to a decimal
                            if (Decimal.TryParse(Parameters[index].ToString(), out decvalue))
                            {
                                retval.Insert(pos, String.Format("'{0}'", FormatDecimalForDB(decvalue)));
                            }
                            else
                            {
                                retval.Insert(pos, String.Format("'{0}'", Parameters[index].ToString()));
                            }
                        }
                        else if (Parameters[index] is String)
                        {
                            retval.Insert(pos, String.Format("'{0}'", Parameters[index].ToString()));
                        }
                        else
                        {
                            retval.Insert(pos, String.Format("'{0}'", Parameters[index].ToString()));
                        }
                    }
                }
                index++;

            }

            return retval.ToString();


        }
        /// <summary>
        /// Format a Decimal Value to write it into the Database
        /// Normaly the Dec.ToString Returns a Value like 12.123,12 Database would save 12,12312
        /// Because auf that we have to delete the dots and then replace the comma with dot
        /// </summary>
        /// <param name="Wert"></param>
        /// <returns></returns>
        private string FormatDecimalForDB(Decimal Wert)
        {
            string ReturnWert = Wert.ToString();

            // Now remove the dots
            int pos = 0;
            while (ReturnWert.ToString().IndexOf('.', pos) > 0)
            {
                pos = ReturnWert.ToString().IndexOf('.', pos);
                ReturnWert.Remove(pos, 1);
            }

            // Replce the comma with a dot

            ReturnWert = ReturnWert.Replace(',', '.');

            return ReturnWert;
        }

        public FCRecordset OpenRecordset(RecordsetTypeEnum rsType)
        {
            return Connection.OpenRecordset(parameterizeSql(), rsType);
        }


        // public void Execute()
        // { }

        public void Execute(bool ExecuteCommand = false)
        {
            if (ExecuteCommand)
            {
                string SqlWithParam;
                SqlWithParam = parameterizeSql();

                Connection.DB.Execute(SqlWithParam);
            }
        }

    }
}
