﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    /// DAO.TableDef
    /// </summary>
    public class FCTableDef : FCRecordset
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        private List<FCIndex> indexes;
        #endregion

        #region Constructors

        public FCTableDef(FCDatabase database) : base(database)
        {
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public int Attributes
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns a value that indicates whether you can change a DAO object. Read-only Boolean.
        /// Syntax
        /// expression .Updatable
        /// expression A variable that represents a TableDef object.
        /// Remarks
        /// The Updatable property setting is always True for a newly created TableDef object and False for a linked TableDef object. A new TableDef object can be appended only to a database for which the current user has write permission.
        /// </summary>
        public bool Updatable
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the name of the specified object. Read/write String.
        /// Syntax
        /// expression .Name
        /// expression A variable that represents a TableDef object.
        /// Remarks
        /// The maximum length for the name of a TableDef object is 64 characters.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the total number of records in a TableDef object. Read-only Long.
        /// Syntax
        /// expression .RecordCount
        /// expression A variable that represents a TableDef object.
        /// Remarks
        /// A Recordset or TableDef object with no records has a RecordCount property setting of 0.
        /// When you work with linkedTableDef objects, the RecordCount property setting is always –1.
        /// </summary>
        public int RecordCount
        {
            get
            {
                return this.Rows.Count;
            }
        }

        /// <summary>
        /// Returns an Indexes collection that contains all of the stored Index objects for the specified table. 
        /// Read-only.
        /// </summary>
        public List<FCIndex> Indexes
        {
            get
            {
                if (this.indexes == null)
                {
                    indexes = new List<FCIndex>();
                }
                return this.indexes;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Creates a new Field object (Microsoft Access workspaces only).
        /// Name - Optional Variant - A String that uniquely names the new Field object. See the Name property for details on valid Field names.
        /// Type - Optional Variant - A constant that determines the data type of the new Field object. See the Type property for valid data types.
        /// Size - Optional Variant - An Integer that indicates the maximum size, in bytes, of a Field object that contains text. See the Size property for valid size values. 
        /// This argument is ignored for numeric and fixed-width fields.
        /// Remarks
        /// You can use the CreateField method to create a new field, as well as specify the name, data type, and size of the field. If you omit one or more of the optional parts when you use CreateField, 
        /// you can use an appropriate assignment statement to set or reset the corresponding property before you append the new object to a collection. After you append the new object, you can alter some 
        /// but not all of its property settings. See the individual property topics for more details.
        /// The type and Size arguments apply only to Field objects in a TableDef object. These arguments are ignored when a Field object is associated with an Index or Relation object.
        /// If Name refers to an object that is already a member of the collection, a run-time error occurs when you use the Append method.
        /// To remove a Field object from a Fields collection, use the Delete method on the collection. You can't delete a Field object from a TableDef object's Fields collection after you create an index 
        /// that references the field.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public FCField CreateField(string name = "", DataTypeEnum type = DataTypeEnum.dbText, int? size = null)
        {
            DataColumn newColumn = new DataColumn(name);
            Type colType = ConvertToType(type);
            newColumn.DataType = colType;

            if (size.HasValue && size.Value > 0)
            {
                if (newColumn.DataType == typeof(System.String))
                {
                    newColumn.MaxLength = size.Value;
                    newColumn.DefaultValue = "";
                }
            }

            if (colType.IsValueType)
            {
                newColumn.DefaultValue = Activator.CreateInstance(colType);
            }

            this.Columns.Add(newColumn);
            FCField newField = new FCField(this, newColumn);
            newField.Size = base.GetFieldSize(size, type);

            //CHE: set type to be used when filling with spaces in Value setter
            newField.Type = type;

            return newField;
        }

        /// <summary>
        /// type conversion between DataTypeEnum and Type
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public static Type ConvertToType(DataTypeEnum dataType)
        {
            switch (dataType)
            {
                case DataTypeEnum.dbChar:
                case DataTypeEnum.dbComplexText:
                case DataTypeEnum.dbGUID:
                case DataTypeEnum.dbMemo:
                case DataTypeEnum.dbText:
                    return typeof(System.String);

                case DataTypeEnum.dbBoolean:
                    return typeof(System.Boolean);

                case DataTypeEnum.dbByte:
                case DataTypeEnum.dbComplexByte:
                    return typeof(System.Int16);

                case DataTypeEnum.dbInteger:
                case DataTypeEnum.dbComplexInteger:
                    return typeof(System.Int32);

                case DataTypeEnum.dbBigInt:
                case DataTypeEnum.dbLong:
                case DataTypeEnum.dbLongBinary:
                case DataTypeEnum.dbComplexGUID:
                case DataTypeEnum.dbComplexLong:
                    return typeof(System.Int64);

                //CHE: use decimal otherwise number format will not work
                case DataTypeEnum.dbSingle:
                case DataTypeEnum.dbDecimal:
                case DataTypeEnum.dbCurrency:
                case DataTypeEnum.dbFloat:
                case DataTypeEnum.dbDouble:
                case DataTypeEnum.dbComplexDecimal:
                case DataTypeEnum.dbComplexDouble:
                case DataTypeEnum.dbComplexSingle:
                case DataTypeEnum.dbNumeric:
                    return typeof(System.Decimal);

                case DataTypeEnum.dbBinary:
                case DataTypeEnum.dbVarBinary:
                case DataTypeEnum.dbAttachment:
                    return typeof(System.Byte[]);

                case DataTypeEnum.dbDate:
                case DataTypeEnum.dbTime:
                case DataTypeEnum.dbTimeStamp:
                    return typeof(System.DateTime);

                default:
                    return null;
            }
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
