﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    /// DAO.Recordsets
    /// </summary>
    public class FCRecordsets : IEnumerable<FCRecordset>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCDatabase database;
        private List<FCRecordset> recordsets;

        #endregion

        #region Constructors

        public FCRecordsets(FCDatabase database)
        {
            this.database = database;
            this.recordsets = new List<FCRecordset>();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Returns the number of objects in the specified collection. Read-only.
        /// </summary>
        public int Count
        {
            get
            {
                return (recordsets == null) ? 0 : recordsets.Count;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCRecordset> GetEnumerator()
        {
            return recordsets.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal void AddRecordset(FCRecordset recordset)
        {
            recordsets.Add(recordset);
        }

        internal void RemoveRecordset(FCRecordset recordset)
        {
            recordsets.Remove(recordset);
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
