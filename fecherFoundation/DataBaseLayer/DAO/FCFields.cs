﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.OleDb;

namespace fecherFoundation.DataBaseLayer
{
    public class FCFields : IEnumerable<FCField>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCRecordset dataTable;
        private List<FCField> fields;

        #endregion

        #region Constructors

        public FCFields(FCRecordset table)
        {
            this.dataTable = table;
            this.fields = new List<FCField>();

            if (table == null)
                return;

            foreach (DataColumn col in table.Columns)
            {
                this.fields.Add(new FCField(table, col));
            }
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public FCField this[string name]
        {
            get
            {
                return this.GetField(name);
            }
            set
            {
                FCField field = this.GetField(name);
                field = value;
            }
        }

        public FCField this[int index]
        {
            get
            {
                return this.GetField(index);
            }
            set
            {
                FCField field = this.GetField(index);
                field = value;
            }
        }

        public int Count
        {
            get 
            { 
                return (fields == null) ? 0 : fields.Count; 
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Adds a new Field to the Fields collection.
        /// Syntax
        /// expression .Append(Object)
        /// expression A variable that represents a Fields object.
        /// Parameters
        /// Name
        /// Required/Optional
        /// Data Type
        /// Description
        /// Object
        /// Required
        /// Object
        /// An object variable that represents the field being appended to the collection.
        /// Remarks
        /// You can use the Append method to add a new table to a database, add a field to a table, and add a field to an index.
        /// The appended object becomes a persistent object, stored on disk, until you delete it by using the Delete method.
        /// The addition of a new object occurs immediately, but you should use the Refresh method on any other collections that may be affected by changes to the database structure.
        /// If the object you're appending isn't complete (such as when you haven't appended any Field objects to a Fields collection of an Index object before it's appended to an Indexes collection) 
        /// or if the properties set in one or more subordinate objects are incorrect, using the Append method causes an error. For example, if you haven’t specified a field type and then try to append 
        /// the Field object to the Fields collection in a TableDef object, using the Append method triggers a run-time error.
        /// </summary>
        /// <param name="field"></param>
        public void Append(FCField field)
        {
            Append(field, true);
        }

        /// <summary>
        /// Deletes a Field from the Fields collection.
        /// Syntax
        /// expression .Delete(Name)
        /// expression A variable that represents a Fields object.
        /// Parameters
        /// Name
        /// Required/Optional
        /// Data Type
        /// Description
        /// Name
        /// Required
        /// String
        /// The field to delete.
        /// Remarks
        /// The deletion of a stored object occurs immediately, but you should use the Refresh method on any other collections that may be affected by changes to the database structure.
        /// </summary>
        /// <param name="name"></param>
        public void Delete(string name)
        {
            fields.Remove(GetField(name));
        }

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCField> GetEnumerator()
        {
            return fields.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal FCField GetField(int Index)
        {
            return fields[Index];
        }

        internal FCField GetField(string FieldName)
        {
            FieldName = FieldName.ToUpper();
            //get last from list to be the same as in VB6 for fields having same name
            return fields.FindLast(delegate(FCField target)
            {
                return target.Name.ToUpper() == FieldName;
            });
        }

        internal void Append(FCField field, bool createColumnInDB)
        {
            fields.Add(field);

            //IPI: VB6 Append function creates the column in the DB if it does not exist; but when retrieving all fields from the DB, we iterate through the columns collection returned by schema table
            // so all columns already exist in the DB 
            if (!createColumnInDB)
            {
                return;
            }

            //JEI
            using (OleDbConnection connection = new OleDbConnection(this.dataTable.Database.Connection.ConnectionString))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                //if column already exists, don't add it again
                if (fieldExists(connection, this.dataTable.TableName, field.Name))
                {
                    return;
                }
                
                DbTransaction trans = connection.BeginTransaction();
                try
                {
                    DbCommand command = connection.CreateCommand();
                    command.Transaction = trans;
                    string NotNull = field.Required ? "NOT NULL" : "";
                    string tableName = this.dataTable.TableName;
                    string columnName = field.Name;
                    string columnType = ConvertToString(field.Type);
                    if (columnType == "varchar")
                    {
                        command.CommandText = "ALTER TABLE [" + tableName + "] Add Column [" + columnName + "] " + columnType + "(" + field.Size + ") " + NotNull;
                    }
                    else
                    {
                        command.CommandText = "ALTER TABLE [" + tableName + "] Add Column [" + columnName + "] " + columnType + " " + NotNull;
                    }

                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private string ConvertToString(DataTypeEnum dataType)        
        {
            switch (dataType)
            {
                case DataTypeEnum.dbCurrency:
                    return "money";
                case DataTypeEnum.dbDate:
                    return "datetime";
                case DataTypeEnum.dbTime:
                    return "time";
                case DataTypeEnum.dbTimeStamp:
                    return "timestamp";
                case DataTypeEnum.dbInteger:
                case DataTypeEnum.dbComplexInteger:
                case DataTypeEnum.dbBigInt:
                case DataTypeEnum.dbLong:
                    return "int";
                case DataTypeEnum.dbByte:
                    return "tinyint";
                case DataTypeEnum.dbBoolean:
                    return "bit";
                case DataTypeEnum.dbSingle:
                case DataTypeEnum.dbDecimal:
                case DataTypeEnum.dbFloat:
                case DataTypeEnum.dbDouble:
                case DataTypeEnum.dbComplexDecimal:
                case DataTypeEnum.dbComplexDouble:
                case DataTypeEnum.dbComplexSingle:
                case DataTypeEnum.dbNumeric:
                    return "float";                
                case DataTypeEnum.dbChar:
                case DataTypeEnum.dbComplexText:
                case DataTypeEnum.dbGUID:
                case DataTypeEnum.dbMemo:
                case DataTypeEnum.dbText:
                    return "varchar";                  
                case DataTypeEnum.dbComplexByte:       
                case DataTypeEnum.dbLongBinary:
                case DataTypeEnum.dbComplexGUID:
                case DataTypeEnum.dbComplexLong:
                case DataTypeEnum.dbBinary:
                case DataTypeEnum.dbVarBinary:
                case DataTypeEnum.dbAttachment:
                    return "varbinary";
                default:
                    return "";
            }
        }

        private bool fieldExists(DbConnection connection, string tableName, string fieldName)
        {
            DataTable tblColumns = connection.GetSchema("Columns");
            DataRow[] tblColumnsRows = tblColumns.Select("COLUMN_NAME = '" + fieldName + "' AND TABLE_NAME = '" + tableName + "'");            
            return tblColumnsRows.Length > 0;
        }


        #endregion
    }
}
