﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    /// DAO.Databases
    /// </summary>
    public class FCDatabases : IEnumerable<FCDatabase>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCDBEngine dbEngine;
        private List<FCDatabase> databases;

        #endregion

        #region Constructors

        public FCDatabases(FCDBEngine dbEngine)
        {
            this.dbEngine = dbEngine;
            this.databases = new List<FCDatabase>();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Returns the number of objects in the specified collection. Read-only.
        /// </summary>
        public int Count
        {
            get
            {
                return (databases == null) ? 0 : databases.Count;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCDatabase> GetEnumerator()
        {
            return databases.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal void AddDatabase(FCDatabase database)
        {
            databases.Add(database);
        }

        internal void RemoveDatabase(FCDatabase database)
        {
            databases.Remove(database);
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
