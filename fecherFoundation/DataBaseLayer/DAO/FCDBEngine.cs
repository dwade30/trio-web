﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    ///  DAO.DBEngine
    /// </summary>
    public class FCDBEngine
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCDatabases databases;

        #endregion

        #region Constructors

        public FCDBEngine()
        {
            databases = new FCDatabases(this);
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum IdleSettings
        {
            dbFreeLocks = 1,
            dbRefreshCache = 8
        }

        #endregion

        #region Properties
        #endregion

        #region Internal Properties

        internal FCDatabases Databases
        {
            get
            {
                return databases;
            }
            set
            {
                databases = value;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// expression .OpenDatabase(Name, Options, ReadOnly, Connect)
        /// expression A variable that represents a DBEngine object.
        /// Name - String - the name of an existing Microsoft Access database file, or the data source name (DSN) of an ODBC data source. See the Name property for more information about setting this value.
        /// Options - Optional Variant - Sets various options for the database, as specified in Remarks.
        /// ReadOnly -  Optional Variant - True if you want to open the database with read-only access, or False (default) if you want to open the database with read/write access.
        /// Connect - Optional Variant - Specifies various connection information, including passwords.
        /// You can use the following values for the options argument.
        /// True - Opens the database in exclusive mode.
        /// False - (Default) Opens the database in shared mode.
        /// When you open a database, it is automatically added to the Databases collection.
        /// Some considerations apply when you use dbname:
        /// If it refers to a database that is already open for access by another user, an error occurs.
        /// If it doesn't refer to an existing database or valid ODBC data source name, an error occurs.
        /// If it's a zero-length string ("") and connect is "ODBC;" , a dialog box listing all registered ODBC data source names is displayed so the user can select a database.
        /// To close a database, and thus remove the Database object from the Databases collection, use the Close method on the object.
        /// Note
        /// When you access a Microsoft Access database engine-connected ODBC data source, you can improve your application's performance by opening a Database object connected to the ODBC data source, 
        /// rather than by linking individual TableDef objects to specific tables in the ODBC data source.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="options"></param>
        /// <param name="readOnly"></param>
        /// <param name="connect"></param>
        /// <returns></returns>
        public FCDatabase OpenDatabase(string name, object options = null, object readOnly = null, string connect = "")
        {
            FCDatabase database = new FCDatabase(this, name, options, readOnly, connect);
            
            databases.AddDatabase(database);
            return database;
        }

        /// <summary>
        /// Copies and compacts a closed database, and gives you the option of changing its version, collating order, and encryption. (Microsoft Access workspaces only). .
        /// expression .CompactDatabase(SrcName, DstName, DstLocale, Options, password)
        /// SrcName
        /// Required String
        /// Identifies an existing, closed database. It can be a full path and file name, such as "C:\db1.mdb". If the file name has an extension, you must specify it. If your network supports it, 
        /// you can also specify a network path, such as "\\server1\share1\dir1\db1.mdb"
        /// DstName
        /// Required String
        /// the file name (and path) of the compacted database that you're creating. You can also specify a network path. You can't use this argument to specify the same database file as SrcName.
        /// DstLocale
        /// Optional Variant
        /// A string expression that specifies a collating order for creating DstName, as specified in Remarks.
        /// If you omit this argument, the locale of DstName is the same as SrcName.
        /// You can also create a password for DstName by concatenating the password string (starting with ";pwd=") with a constant in the DstLocale argument, like this: dbLangSpanish & ";pwd=NewPassword".
        /// If you want to use the same DstLocale as SrcName (the default value), but specify a new password, simply enter a password string for DstLocale: ";pwd=NewPassword"
        /// Options
        /// Optional Variant
        /// A constant or combination of constants that indicates one or more options, as specified in Remarks. You can combine options by summing the corresponding constants.
        /// password
        /// Optional Variant
        /// A string expression containing a password, if the database is password protected. The string ";pwd=" must precede the actual password. If you include a password setting in DstLocale, 
        /// this setting is ignored.
        /// </summary>
        /// <param name="srcName"></param>
        /// <param name="dstName"></param>
        /// <param name="dstLocale"></param>
        /// <param name="options"></param>
        /// <param name="password"></param>
        public void CompactDatabase(string srcName, string dstName, object dstLocale = null, object options = null, object password = null)
        {
            string connectionString = GetConnectionString(srcName);

            //make sure there's no open connections to your db before calling this method
            foreach (FCDatabase database in databases)
            {
                if (database.Connection.ConnectionString == connectionString)
                {
                    database.Close();
                }
            }

            //create an instance of a Jet Replication Object
            object objJRO = Activator.CreateInstance(Type.GetTypeFromProgID("JRO.JetEngine"));

            //filling Parameters array
            object[] oParams = new object[] { connectionString, GetConnectionString(dstName) };

            //invoke a CompactDatabase method of a JRO object, pass Parameters array
            objJRO.GetType().InvokeMember("CompactDatabase", System.Reflection.BindingFlags.InvokeMethod, null, objJRO, oParams);

            //clean up (just in case)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(objJRO);
            objJRO = null;
        }

        /// <summary>
        /// Creates a new Database object, saves the database to disk, and returns an opened Database object (Microsoft Access workspaces only). .
        /// Name
        /// Required
        /// String
        /// A String up to 255 characters long that is the name of the database file that you're creating. It can be the full path and file name. 
        /// If your network supports it, you can also specify a network path, such as "\\server1\share1\dir1\db1". You can only create Microsoft Access database files with this method.
        /// Locale
        /// Required
        /// String
        /// A string expression that specifies a collating order for creating the database, as specified in Settings. You must supply this argument or an error occurs.
        /// You can also create a password for the new Database object by concatenating the password string (starting with ";pwd=" ) with a constant in the locale argument, like this:
        /// dbLangSpanish & ";pwd=NewPassword"
        /// If you want to use the default locale, but specify a password, simply enter a password string for the locale argument:
        /// ";pwd=NewPassword"
        /// Use strong passwords that combine upper- and lowercase letters, numbers, and symbols. Weak passwords don't mix these elements. Strong password: Y6dh!et5. Weak password: House27. Use a strong password that you can remember so that you don't have to write it down.
        /// Option
        /// Optional
        /// Variant
        /// A constant or combination of constants that indicates one or more options, as specified in Settings. You can combine options by summing the corresponding constants.
        /// Remarks
        /// You can use one of the following constants for the locale argument to specify the CollatingOrder property of text for string comparisons.
        /// Constant
        /// Collating order
        /// dbLangGeneral
        /// English, German, French, Portuguese, Italian, and Modern Spanish
        /// dbLangArabic
        /// Arabic
        /// dbLangChineseSimplified
        /// Simplified Chinese
        /// dbLangChineseTraditional
        /// Traditional Chinese
        /// dbLangCyrillic
        /// Russian
        /// dbLangCzech
        /// Czech
        /// dbLangDutch
        /// Dutch
        /// dbLangGreek
        /// Greek
        /// dbLangHebrew
        /// Hebrew
        /// dbLangHungarian
        /// Hungarian
        /// dbLangIcelandic
        /// Icelandic
        /// dbLangJapanese
        /// Japanese
        /// dbLangKorean
        /// Korean
        /// dbLangNordic
        /// Nordic languages (Microsoft Jet database engine version 1.0 only)
        /// dbLangNorwDan
        /// Norwegian and Danish
        /// dbLangPolish
        /// Polish
        /// dbLangSlovenian
        /// Slovenian
        /// dbLangSpanish
        /// Traditional Spanish
        /// dbLangSwedFin
        /// Swedish and Finnish
        /// dbLangThai
        /// Thai
        /// dbLangTurkish
        /// Turkish
        /// You can use one or more of the following constants in the options argument to specify which version the data format should have and whether or not to encrypt the database.
        /// Constant
        /// Description
        /// dbEncrypt
        /// Creates an encrypted database.
        /// dbVersion10
        /// Creates a database that uses the Microsoft Jet database engine version 1.0 file format.
        /// dbVersion11
        /// Creates a database that uses the Microsoft Jet database engine version 1.1 file format.
        /// dbVersion20
        /// Creates a database that uses the Microsoft Jet database engine version 2.0 file format.
        /// dbVersion30
        /// Creates a database that uses the Microsoft Jet database engine version 3.0 file format (compatible with version 3.5).
        /// dbVersion40
        /// Creates a database that uses the Microsoft Jet database engine version 4.0 file format.
        /// dbVersion120
        /// Creates a database that uses the Microsoft Access database engine version 12.0 file format.
        /// If you omit the encryption constant, CreateDatabase creates an un-encrypted database.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="locale"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public FCDatabase CreateDatabase(string name, string locale, object option = null)
        {
            // TODO:CHE - change implementation for other cases on other projects
            if (locale != "dbLangGeneral" || option != null)
            {
                throw new NotImplementedException();
            }

            //copy file from embedded resource
            using (var resource = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("fecherFoundation.DataBaseLayer.EmptyDatabaseAccess2002-2003.mdb"))
            {
                using (var file = new FileStream(name, FileMode.Create, FileAccess.Write))
                {
                    resource.CopyTo(file);
                }
            }

            //open database
            return this.OpenDatabase(name);
        }

        /// <summary>
        /// 
        /// </summary>
        public void BeginTrans()
        {
            // TODO:CHE
        }

        /// <summary>
        /// 
        /// </summary>
        public void CommitTrans()
        {
            // TODO:CHE
        }

        /// <summary>
        /// 
        /// </summary>
        public void Rollback()
        {
            // TODO:CHE
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        public void Idle(IdleSettings action)
        {
            // TODO:CHE
        }

        #endregion

        #region Internal Methods

        /// <summary>
        /// get connection string for mdb database 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal string GetConnectionString(string name, string options = "")
        {
            // change "Jet OLEDB:Engine Type=5" to an appropriate value or leave it as is if your db is JET4X format (access 2000,2002)
            // (yes, jetengine5 is for JET4X, no misprint here)
            //string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", name);
            string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", name);
            if (!String.IsNullOrEmpty(options))
            {
                connectionString += options.Replace("PWD", "Jet OLEDB:Database Password");
            }
            return connectionString;
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
