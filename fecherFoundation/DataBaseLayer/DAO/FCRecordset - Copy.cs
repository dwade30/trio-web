﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    /// DAO.Recordset
    /// </summary>
    public class FCRecordset : DataTable, IFCDataTable
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCDatabase database;
        private FCFields fields;

        //CHE: FilterType should not affect grid, save and restore position
        private int? bookmarkBeforeFilterType;

        private DataGridView parentGrid;

        //synchronize rows add/ delete/ update in all clone tables
        internal bool suppressCloneSynchronization = false;

        //AddNew on DataTable with mandatory fields will raise exception if new empty row is added in collection, must be added later on Update
        private List<DataRow> newRows;

        private DbDataAdapter mobjDataAdapter = null;
        private int mintIndex = 1;
        internal int mintAbsolutePosition = (int)PositionEnum.adPosUnknown;
        private bool mblnDeleted = false;
        private string mstrFilter = "";
        private string mstrSource = "";
        private string mstrSort = "";
        private DataView mobjView = null;

        private List<FCRecordset> clonedRecordsets;

        private FilterGroupEnum mFilterType = FilterGroupEnum.adFilterNone;

        private bool isEOFandBOFchecked = false;

        #endregion

        #region Constructors

        //needed for GetChanges http://social.msdn.microsoft.com/Forums/es-ES/8ee691c5-9eab-433e-a73f-a9ae590e504b/problem-with-getchanges-from-a-datatable-object?forum=vblanguage
        public FCRecordset()
        {
        }

        public FCRecordset(FCDatabase database)
        {
            this.database = database;
            this.ColumnChanged += new DataColumnChangeEventHandler(FCRecordset_ColumnChanged);
            this.TableNewRow += new DataTableNewRowEventHandler(FCRecordset_TableNewRow);
            this.RowDeleted += new DataRowChangeEventHandler(FCRecordset_RowDeleted);
            this.RowChanging += new DataRowChangeEventHandler(FCRecordset_RowChanging);
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum MoveDirection
        {
            First,
            Next,
            Previous,
            Last
        }

        public enum FilterGroupEnum
        {
            adFilterNone,
            adFilterPendingRecords,
            adFilterAffectedRecords,
            adFilterFetchedRecords,
            adFilterConflictingRecords
        }

        public enum Synchronization
        {
            TableNewRow,
            DetachedNewRow,
            DeleteRow,
            UpdateRow,
            MoveRowFromDefaultView
        }

        public enum PositionEnum : int
        {
            adPosBOF = -2,
            adPosEOF = -3,
            adPosUnknown = -1,
        }

        /// <summary>
        /// Specifies the type of record locking used when opening a recordset.
        ///dbOptimistic
        ///3
        ///Optimistic concurrency based on record ID. Cursor compares record ID in old and new records to determine if changes have been made since the record was last accessed.
        ///dbOptimisticBatch
        ///5
        ///Enables batch optimistic updates (ODBCDirect workspaces only).
        ///dbOptimisticValue
        ///1
        ///Optimistic concurrency based on record values. Cursor compares data values in old and new records to determine if changes have been made since the record was last accessed (ODBCDirect workspaces only).
        ///Note
        ///ODBCDirect workspaces are not supported in Microsoft Access 2013. Use ADO if you want to access external data sources without using the Microsoft Access database engine.
        ///dbPessimistic
        ///2
        ///Pessimistic concurrency. Cursor uses the lowest level of locking sufficient to ensure that the record can be updated.
        /// </summary>
        public enum LockTypeEnum
        {
            dbOptimistic = 3,
            dbOptimisticBatch = 5,
            dbOptimisticValue = 1,
            dbPessimistic = 2
        }

        #endregion

        #region Properties

        public FCDatabase Database
        {
            get
            {
                return database;
            }
            set
            {
                database = value;
            }
        }

        /// <summary>
        /// A Fields collection contains all stored Field objects of an Index, QueryDef, Recordset, Relation, or TableDef object.
        /// Remarks
        /// The Fields collections of the Index, QueryDef, Relation, and TableDef objects contain the specifications for the fields those objects represent. The Fields collection of a Recordset 
        /// object represents the Field objects in a row of data, or in a record. You use the Field objects in a Recordset object to read and to set values for the fields in the current record of 
        /// the Recordset object.
        /// To refer to a Field object in a collection by its ordinal number or by its Name property setting, use any of the following syntax forms:
        /// Fields(0)
        /// Fields("name")
        /// Fields![name]
        /// With the same syntax forms, you can also refer to the Value property of a Field object that you create and append to a Fields collection. The context of the field reference will determine 
        /// whether you are referring to the Field object or the Value property of the Field object.
        /// </summary>
        public FCFields Fields
        {
            get
            {
                if (fields == null)
                {
                    fields = new FCFields(this);
                    //set fields properties
                    SetFields();                    
                }
                return fields;
            }
            set
            {
                fields = value;
            }
        }

        public int? BookmarkBeforeFilterType
        {
            get { return bookmarkBeforeFilterType; }
            set { bookmarkBeforeFilterType = value; }
        }

        /// <summary>
        /// Sets or returns a bookmark that uniquely identifies the current record in a Recordset object.
        /// Syntax
        /// expression .Bookmark
        /// expression A variable that represents a Recordset object.
        /// Remarks
        /// For a Recordset object based entirely on Microsoft Access database engine tables, the value of the Bookmarkable property is True, and you can use the Bookmark 
        /// property with that Recordset. Other database products may not support bookmarks, however. For example, you can't use bookmarks in any Recordset object based on 
        /// a linked Paradox table that has no primary key.
        /// When you create or open a Recordset object, each of its records already has a unique bookmark. You can save the bookmark for the current record by assigning the 
        /// value of the Bookmark property to a variable. To quickly return to that record at any time after moving to a different record, set the Recordset object's Bookmark
        /// property to the value of that variable.
        /// There is no limit to the number of bookmarks you can establish. To create a bookmark for a record other than the current record, move to the desired record and assign 
        /// the value of the Bookmark property to a String variable that identifies the record.
        /// To make sure the Recordset object supports bookmarks, check the value of its Bookmarkable property before you use the Bookmark property. If the Bookmarkable property 
        /// is False, the Recordset object doesn't support bookmarks, and using the Bookmark property results in a trappable error.
        /// If you use the Clone method to create a copy of a Recordset object, the Bookmark property settings for the original and the duplicate Recordset objects are identical 
        /// and can be used interchangeably. However, you can't use bookmarks from different Recordset objects interchangeably, even if they were created by using the same object 
        /// or the same SQL statement.
        /// If you set the Bookmark property to a value that represents a deleted record, a trappable error occurs.
        /// The value of the Bookmark property isn't the same as a record number.
        /// </summary>
        public int? Bookmark
        {
            get
            {
                if (Deleted)
                {
                    throw new Exception("Row handle refered to a deleted row or a row marked for deletion.");
                }

                DataRow currentRow = CurrentRow;

                int index = this.Rows.IndexOf(currentRow);

                //BAN: the current row can be present just in the DefaultView and not also in the DataTable, cases like adding a new row but not updating
                if (index == -1)
                {
                    if (this.HasNewRows())
                    {
                        // if a new row has been added with AddNew function, that the index is already set correctly
                        index = NewRows.IndexOf(currentRow);
                        return Index;
                    }
                }
                //BAN: the current row can be present just in the DefaultView and not also in the DataTable, cases like adding a new row but not updating
                //the data table. In these cases, the index will be -1, but these cases occur just for the last row of the DefaultView so it should be returned
                //with -1 because the index will be incremented by one when returning
                if (index == -1 && currentRow.RowState == DataRowState.Detached)
                {
                    index = this.DefaultView.Count - 1;
                }
                //If the row is not present in the DefaultView, then the row must have been added with AddNew in the NewRows collection
                if (index == -1 && NewRows.Count > 0)
                {
                    index = NewRows.Count - 1;
                }

                return index + 1;
            }
            set
            {
            }
        }

        public LockTypeEnum LockType 
        { 
            get; 
            set; 
        }

        public string Source
        {
            get
            {
                return mstrSource;
            }
            set
            {
                mstrSource = value;
            }
        }

        public string Filter
        {
            get
            {
                return mstrFilter;
            }
            set
            {
                mblnDeleted = false;
                mstrFilter = value;
            }
        }

        //IPI
        public FilterGroupEnum FilterType
        {
            get
            {
                return mFilterType;
            }
            set
            {
                mblnDeleted = false;

                if (value == FilterGroupEnum.adFilterNone)
                {
                    Filter = "";
                    mFilterType = value;
                    UpdateDataView(false, mFilterType);
                }
                else if (mFilterType != value)
                {
                    //CHE: FilterType should not affect grid, save position
                    if (ParentGrid != null && !BOF && !EOF && !Deleted)
                    {
                        isEOFandBOFchecked = true;
                        BookmarkBeforeFilterType = this.Bookmark;
                        isEOFandBOFchecked = false;
                    }
                    mFilterType = value;
                    UpdateDataView(false, mFilterType);
                }
                else
                {
                    Deleted = false;
                }
            }
        }

        public string Sort
        {
            get
            {
                return mstrSort;
            }
            set
            {
                mblnDeleted = false;
                mstrSort = value;
            }
        }

        public DbDataAdapter DataAdapter
        {
            get
            {
                return mobjDataAdapter;
            }
            set
            {
                mobjDataAdapter = value;
            }
        }

        internal DataView View
        {
            get
            {
                return mobjView;
            }
            set
            {
                mblnDeleted = false;
                mobjView = value;
            }
        }

        /// <summary>
        /// Holds the current position of the DataTable (bookmark)
        /// </summary>
        public int Index
        {
            get
            {
                return mintIndex;
            }
            set
            {
                mblnDeleted = false;
                mintIndex = value;
                mintAbsolutePosition = value;
            }
        }
        public int AbsolutePosition
        {
            get
            {
                return mintAbsolutePosition;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException();
                }

                int intRecordCount;
                if (this.View == null)
                {
                    intRecordCount = this.Rows.Count;
                }
                else
                {
                    intRecordCount = this.View.Count;
                }
                if (value > intRecordCount)
                {
                    value = intRecordCount;
                }
                this.Index = value;
            }
        }

        public List<DataRow> NewRows
        {
            get { return newRows; }
            set { newRows = value; }
        }

        /// <summary>
        /// Returns a value that indicates whether you can change a DAO object. Read-only Boolean.
        /// Remarks
        /// Snapshot- and forward-only–type Recordset objects always return False.
        /// Many types of objects can contain fields that can't be updated. For example, you can create a 
        /// dynaset-type Recordset object in which only some fields can be changed. These fields can be fixed 
        /// or contain data that increments automatically, or the dynaset can result from a query that combines 
        /// updatable and nonupdatable tables.
        /// If the object contains only read-only fields, the value of the Updatable property is False. When one 
        /// or more fields are updatable, the property's value is True. You can edit only the updatable fields. 
        /// A trappable error occurs if you try to assign a new value to a read-only field.
        /// Because an updatable object can contain read-only fields, check the DataUpdatable property of each field 
        /// in the Fields collection of a Recordset object before you edit a record.
        /// </summary>
        public bool Updatable
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the name of the specified object. Read-only String.
        /// Remarks
        /// The Name property of a Recordset object opened by using an SQL statement is the first 256 characters 
        /// of the SQL statement.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates whether a particular record was found by using the Seek method or one of the Find methods 
        /// (Microsoft Access workspaces only).
        /// Remarks
        /// When you open or create a Recordset object, its NoMatch property is set to False.
        /// To locate a record, use the Seek method on a table-type Recordset object or one of the Find methods 
        /// on a dynaset- or snapshot-type Recordset object. Check the NoMatch property setting to see whether 
        /// the record was found.
        /// If the Seek or Find method is unsuccessful and the NoMatch property is True, the current record will 
        /// no longer be valid. Be sure to obtain the current record's bookmark before using the Seek method or a 
        /// Find method if you'll need to return to that record.
        /// Note
        /// Using any of the Move methods on a Recordset object won't affect its NoMatch property setting.
        /// </summary>
        public bool NoMatch
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the number of records accessed in a Recordset object, or the total number of records in a 
        /// table-type Recordset object. or TableDef object. Read-only Long.
        /// Remarks
        /// Use the RecordCount property to find out how many records in a Recordset or TableDef object have been 
        /// accessed. The RecordCount property doesn't indicate how many records are contained in a dynaset–, 
        /// snapshot–, or forward–only–type Recordset object until all records have been accessed. Once the last 
        /// record has been accessed, the RecordCount property indicates the total number of undeleted records in 
        /// the Recordset or TableDef object. To force the last record to be accessed, use the MoveLast method on 
        /// the Recordset object. You can also use an SQL Count function to determine the approximate number of 
        /// records your query will return.
        /// Note
        /// Using the MoveLast method to populate a newly opened Recordset negatively impacts performance. Unless it 
        /// is necessary to have an accurate RecordCount as soon as you open a Recordset, it's better to wait until 
        /// you populate the Recordset with other portions of code before checking the RecordCount property.
        /// As your application deletes records in a dynaset-type Recordset object, the value of the RecordCount 
        /// property decreases. However, records deleted by other users aren't reflected by the RecordCount property 
        /// until the current record is positioned to a deleted record. If you execute a transaction that affects the
        /// RecordCount property setting and you subsequently roll back the transaction, the RecordCount property 
        /// won't reflect the actual number of remaining records.
        /// The RecordCount property of a snapshot– or forward–only–type Recordset object isn't affected by changes 
        /// in the underlying tables.
        /// A Recordset or TableDef object with no records has a RecordCount property setting of 0.
        /// Using the Requery method on a Recordset object resets the RecordCount property just as if the query were 
        /// re-executed.
        /// </summary>
        public int RecordCount
        {
            get
            {
                if (View == null)
                {
                    //CHE: not counting deleted rows unless corresponding RowStateFilter is set (cannot use DefaultView because it doesn't return as we wish) 
                    //return thisRows.Count;
                    return DefaultViewRowsCountWithState();
                }
                else
                {
                    //CHE: for filter pending add also those from NewRows
                    if (FilterType == FilterGroupEnum.adFilterPendingRecords)
                    {
                        return View.Count + (HasNewRows() ? NewRows.Count : 0);
                    }

                    return View.Count;
                }
            }
        }

        /// <summary>
        /// Returns a value that indicates whether the current record position is before the first record in a Recordset object. Read-only Boolean.
        /// You can use the BOF and EOF properties to determine whether a Recordset object contains records or whether you've gone beyond the limits of a Recordset object when you move from record to record.
        /// The location of the current record pointer determines the BOF and EOF return values.
        /// If either the BOF or EOF property is True, there is no current record.
        /// If you open a Recordset object containing no records, the BOF and EOF properties are set to True, and the Recordset object's RecordCount property setting is 0. When you open a Recordset object 
        /// that contains at least one record, the first record is the current record and the BOF and EOF properties are False; they remain False until you move beyond the beginning or end of the Recordset 
        /// object by using the MovePrevious or MoveNext method, respectively. When you move beyond the beginning or end of the Recordset, there is no current record or no record exists.
        /// If you delete the last remaining record in the Recordset object, the BOF and EOF properties may remain False until you attempt to reposition the current record.
        /// If you use the MoveLast method on a Recordset object containing records, the last record becomes the current record; if you then use the MoveNext method, the current record becomes invalid and the 
        /// EOF property is set to True. Conversely, if you use the MoveFirst method on a Recordset object containing records, the first record becomes the current record; if you then use the MovePrevious method, 
        /// there is no current record and the BOF property is set to True.
        /// Typically, when you work with all the records in a Recordset object, your code will loop through the records by using the MoveNext method until the EOF property is set to True.
        /// If you use the MoveNext method while the EOF property is set to True or the MovePrevious method while the BOF property is set to True, an error occurs.
        /// An OpenRecordset method internally invokes a MoveFirst method. Therefore, using an OpenRecordset method on an empty set of records sets the BOF and EOF properties to True.
        /// All Move methods that successfully locate a record will set both BOF and EOF to False.
        /// In a Microsoft Access workspace, if you add a record to an empty Recordset, BOF will become False, but EOF will remain True, indicating that the current position is at the end of Recordset.
        /// Any Delete method, even if it removes the only remaining record from a Recordset, won't change the setting of the BOF or EOF property.
        /// </summary>
        public bool BOF
        {
            get
            {
                bool BOF = false;
                if (View == null)
                {
                    //CHE: if you call movelast and there is no rows in view bof should return true
                    BOF = (this.Rows.Count == 0 || Index == 0 || (Index > this.Rows.Count && this.RecordCount == 0));
                }
                else
                {
                    //IPI: if a Filter was set, check if the current row is a new added row in the DataTable
                    BOF = (View.Count == 0 || Index == 0);
                }

                if (BOF)
                {
                    //SBE: BOF should return false when View Count is 0, and row was deleted
                    return !(IsNewRowAdded() || Deleted);
                }
                return BOF;
            }
        }

        /// <summary>
        /// Returns a value that indicates whether the current record position is after the last record in a Recordset object. Read-only Boolean.
        /// You can use the BOF and EOF properties to determine whether a Recordset object contains records or whether you've gone beyond the limits of a Recordset object when you move from record to record.
        /// The location of the current record pointer determines the BOF and EOF return values.
        /// If either the BOF or EOF property is True, there is no current record.
        /// If you open a Recordset object containing no records, the BOF and EOF properties are set to True, and the Recordset object's RecordCount property setting is 0. When you open a Recordset object 
        /// that contains at least one record, the first record is the current record and the BOF and EOF properties are False; they remain False until you move beyond the beginning or end of the Recordset object 
        /// by using the MovePrevious or MoveNext method, respectively. When you move beyond the beginning or end of the Recordset, there is no current record or no record exists.
        /// If you delete the last remaining record in the Recordset object, the BOF and EOF properties may remain False until you attempt to reposition the current record.
        /// If you use the MoveLast method on a Recordset object containing records, the last record becomes the current record; if you then use the MoveNext method, the current record becomes invalid and the 
        /// EOF property is set to True. Conversely, if you use the MoveFirst method on a Recordset object containing records, the first record becomes the current record; if you then use the MovePrevious method, 
        /// there is no current record and the BOF property is set to True.
        /// Typically, when you work with all the records in a Recordset object, your code will loop through the records by using the MoveNext method until the EOF property is set to True.
        /// If you use the MoveNext method while the EOF property is set to True or the MovePrevious method while the BOF property is set to True, an error occurs.
        /// An OpenRecordset method internally invokes a MoveFirst method. Therefore, using an OpenRecordset method on an empty set of records sets the BOF and EOF properties to True.
        /// All Move methods that successfully locate a record will set both BOF and EOF to False.
        /// In a Microsoft Access workspace, if you add a record to an empty Recordset, BOF will become False, but EOF will remain True, indicating that the current position is at the end of Recordset.
        /// Any Delete method, even if it removes the only remaining record from a Recordset, won't change the setting of the BOF or EOF property.
        /// </summary>
        public bool EOF
        {
            get
            {
                bool EOF = false;
                if (View == null)
                {
                    //CHE: if you call movefirst and there is no rows in view eof should return true
                    EOF = (this.Rows.Count == 0 || Index > this.Rows.Count || (Index == 0 && this.RecordCount == 0));
                }
                else
                {
                    EOF = (View.Count == 0 || Index > View.Count);
                }

                if (!EOF)
                {
                    return EOF;
                }

                if (View == null)
                {
                    if (this.IsNewRowAdded())
                    {
                        if (this.HasNewRows())
                        {
                            if ((this.Rows.Count + NewRows.Count) >= Index)
                            {
                                return false;
                            }
                        }
                        else if (this.Rows.Count >= Index)
                        {
                            return false;
                        }
                        //SBE: check counts and Index on default view, when current row is the last added row
                        else if (this.DefaultView.Count >= Index)
                        {
                            return false;
                        }

                    }
                }
                else
                {
                    if (this.IsNewRowAdded())
                    {
                        if (this.HasNewRows())
                        {
                            if ((this.Rows.Count + NewRows.Count) >= Index)
                            {
                                return false;
                            }
                        }
                        else if (View.Count >= Index)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //BAN: If the Index is greater than the DataView row count it means that the DataView has a filter applied
                        //Check if the DataRow at the specified Index corresponds to a DataRowView in the DataView. If the DataView has the row
                        //then EOF is false, if it does not contain it, EOF is true
                        int currentIdx = Index;
                        bool found = false;
                        if (currentIdx > 0 && currentIdx <= this.Rows.Count)
                        {
                            DataRow dataRow = this.Rows[currentIdx - 1];
                            foreach (DataRowView dataRowView in View)
                            {
                                if (dataRowView.Row == dataRow)
                                {
                                    found = true;
                                    break;
                                }
                            }

                            EOF = !found;

                            //IPI: if the Index is on a new row, EOF should return false 
                            if (EOF && this.HasNewRows())
                            {
                                if ((this.Rows.Count + NewRows.Count) >= Index)
                                {
                                    return false;
                                }
                            }

                            //SBE: If RowState is Deleted, then EOF should return false (current Index is lower than rows count)
                            if (dataRow.RowState == DataRowState.Deleted)
                            {
                                EOF = false;
                            }
                        }
                    }
                }

                return EOF;
            }
        }

        public List<FCRecordset> ClonedRecordsets
        {
            get { return clonedRecordsets; }
            set { clonedRecordsets = value; }
        }

        /// <summary>
        /// Holds the IdentityColumn
        /// </summary>
        public FCField IdentityColumn
        {
            get;
            set;
        }

        #endregion

        #region Internal Properties

        internal bool IsEOFandBOFchecked
        {
            get
            {
                return isEOFandBOFchecked;
            }
            set
            {
                isEOFandBOFchecked = value;
            }
        }

        internal DataRowState Status
        {
            get
            {
                //CHE: for deleted CurrentRow will throw exception
                if (Deleted)
                {
                    return DataRowState.Deleted;
                }
                else if (isEOFandBOFchecked || (!isEOFandBOFchecked && !this.EOF && !this.BOF))
                {
                    isEOFandBOFchecked = true;
                    DataRowState rowState;
                    //CHE: with ADO VB6 recordset there is no Detached status, return as Added status
                    if (this.CurrentRow.RowState == DataRowState.Detached)
                    {
                        rowState = DataRowState.Added;
                    }
                    rowState = this.CurrentRow.RowState;
                    isEOFandBOFchecked = false;
                    return rowState;
                }
                return DataRowState.Unchanged;
            }
        }

        internal DbConnection ActiveConnection
        {
            get
            {
                if (DataAdapter != null)
                {
                    if (DataAdapter.InsertCommand != null)
                        return DataAdapter.InsertCommand.Connection;

                    if (DataAdapter.UpdateCommand != null)
                        return DataAdapter.UpdateCommand.Connection;

                    if (DataAdapter.SelectCommand != null)
                        return DataAdapter.SelectCommand.Connection;

                }
                return null;
            }
        }

        internal DataRow CurrentRow
        {
            get
            {
                //che:try
                //if (!IsNewRowAdded())
                {
                    if (!isEOFandBOFchecked && (EOF || BOF))
                    {
                        throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                    }
                }

                DataRow oRow = null;

                try
                {
                    //return the current row
                    if (View == null)
                    {
                        if (Index > this.Rows.Count && this.HasNewRows())
                        {
                            oRow = NewRows[Index - this.Rows.Count - 1];
                        }
                        else
                        {
                            if (this.Rows.Count >= Index)
                            {
                                oRow = this.Rows[Index - 1];
                            }
                            else if (this.DefaultView != null && this.DefaultView.Count >= Index)
                            {
                                oRow = this.DefaultView[Index - 1].Row;
                            }
                        }
                    }
                    else
                    {
                        if (Index > View.Count && this.HasNewRows() && Index > this.Rows.Count)
                        {
                            oRow = NewRows[Index - this.Rows.Count - 1];
                        }
                        else
                        {
                            //CHE: Index is on DataTable, not on View
                            if (Index - 1 >= 0 && Index - 1 < this.Rows.Count)
                            {
                                oRow = this.Rows[Index - 1];
                            }
                            //BAN: If index does not correspond to DataTable, return the row from the view
                            else
                            {
                                oRow = View[Index - 1].Row;
                            }
                        }
                    }
                }

                catch (System.Exception e)
                {
                    throw e;
                }

                return oRow;
            }
        }

        internal bool Deleted
        {
            get
            {
                return mblnDeleted;
            }
            set
            {
                mblnDeleted = value;
            }
        }

        internal DataGridView ParentGrid
        {
            get { return parentGrid; }
            set { parentGrid = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Moves to the first record in a specified Recordset object and make that record the current record.
        /// Use the Move methods to move from record to record without applying a condition.
        /// If you edit the current record, be sure you use the Update method to save the changes before you move to another record. If you move to another record without updating, 
        /// your changes are lost without warning.
        /// When you open a Recordset, the first record is current and the BOF property is False. If the Recordset contains no records, the BOF property is True, and there is no current record.
        /// If the first or last record is already current when you use MoveFirst or MoveLast, the current record doesn't change.
        /// If recordset refers to a table-type Recordset (Microsoft Access workspaces only), movement follows the current index. You can set the current index by using the Index property. 
        /// If you don't set the current index, the order of returned records is undefined.
        /// You can't use the MoveFirst, MoveLast, and MovePrevious methods on a forward–only–type Recordset object.
        /// To move the position of the current record in a Recordset object a specific number of records forward or backward, use the Move method.
        /// </summary>
        public void MoveFirst(bool moveSelection = true)
        {
            MoveInternal(MoveDirection.First, moveSelection);
        }

        /// <summary>
        /// Moves to the last record in a specified Recordset object and make that record the current record.
        /// Options - Optional Long - Set to dbRunAsync to rune the call to MoveLast asynchronously.
        /// Remarks
        /// Use the Move methods to move from record to record without applying a condition.
        /// If you edit the current record, be sure you use the Update method to save the changes before you move to another record. If you move to another record without updating, 
        /// your changes are lost without warning.
        /// When you open a Recordset, the first record is current and the BOF property is False. If the Recordset contains no records, the BOF property is True, and there is no current record.
        /// If the first or last record is already current when you use MoveFirst or MoveLast, the current record doesn't change.
        /// If recordset refers to a table-type Recordset (Microsoft Access workspaces only), movement follows the current index. You can set the current index by using the Index property. 
        /// If you don't set the current index, the order of returned records is undefined.
        /// Note
        /// You can use the MoveLast method to fully populate a dynaset- or snapshot-type Recordset to provide the current number of records in the Recordset. However, if you use MoveLast in this way, 
        /// you can slow down your application's performance. You should only use MoveLast to get a record count if it is absolutely necessary to obtain an accurate record count on a newly opened Recordset. 
        /// If you use the dbRunAsync constant with MoveLast, the method call is asynchronous. You can use the StillExecuting property to determine when the Recordset is fully populated, and you can use the Cancel method to terminate execution of the asynchronous MoveLast method call.
        /// You can't use the MoveFirst, MoveLast, and MovePrevious methods on a forward–only–type Recordset object.
        /// To move the position of the current record in a Recordset object a specific number of records forward or backward, use the Move method.
        /// </summary>
        /// <param name="options"></param>
        public void MoveLast(int options = 0)
        {
            MoveInternal(MoveDirection.Last);
        }

        /// <summary>
        /// Moves to the next record in a specified Recordset object and make that record the current record.
        /// Remarks
        /// Use the Move methods to move from record to record without applying a condition.
        /// If you edit the current record, be sure you use the Update method to save the changes before you move to another record. If you move to another record without updating, 
        /// your changes are lost without warning.
        /// When you open a Recordset, the first record is current and the BOF property is False. If the Recordset contains no records, the BOF property is True, and there is no current record.
        /// If you use MoveNext when the last record is current, the EOF property is True, and there is no current record. If you use MoveNext again, an error occurs, and EOF remains True.
        /// If recordset refers to a table-type Recordset (Microsoft Access workspaces only), movement follows the current index. You can set the current index by using the Index property. 
        /// If you don't set the current index, the order of returned records is undefined.
        /// You can't use the MoveFirst, MoveLast, and MovePrevious methods on a forward–only–type Recordset object.
        /// To move the position of the current record in a Recordset object a specific number of records forward or backward, use the Move method.
        /// </summary>
        public void MoveNext(bool moveSelection = true)
        {
            MoveInternal(MoveDirection.Next, moveSelection);
        }

        /// <summary>
        /// Moves to the previous record in a specified Recordset object and make that record the current record.
        /// Remarks
        /// Use the Move methods to move from record to record without applying a condition.
        /// If you edit the current record, be sure you use the Update method to save the changes before you move to another record. If you move to another record without updating, 
        /// your changes are lost without warning.
        /// When you open a Recordset, the first record is current and the BOF property is False. If the Recordset contains no records, the BOF property is True, and there is no current record.
        /// If you use MovePrevious when the first record is current, the BOF property is True, and there is no current record. If you use MovePrevious again, an error occurs, and BOF remains True.
        /// If recordset refers to a table-type Recordset (Microsoft Access workspaces only), movement follows the current index. You can set the current index by using the Index property. 
        /// If you don't set the current index, the order of returned records is undefined.
        /// You can't use the MoveFirst, MoveLast, and MovePrevious methods on a forward–only–type Recordset object.
        /// To move the position of the current record in a Recordset object a specific number of records forward or backward, use the Move method.
        /// </summary>
        public void MovePrevious()
        {
            MoveInternal(MoveDirection.Previous);
        }

        /// <summary>
        /// Closes an open Recordset.
        /// Remarks
        /// If the Recordset object is already closed when you use Close, a run-time error occurs.
        /// If you try to close a Connection object while it has any open Recordset objects, the Recordset objects will be closed and any pending updates or edits will be canceled. 
        /// Similarly, if you try to close a Workspace object while it has any open Connection objects, those Connection objects will be closed, which will close their Recordset objects.
        /// An alternative to the Close method is to set the value of an object variable to Nothing (Set dbsTemp = Nothing).
        /// </summary>
        public void Close()
        {
            this.Clear();
            this.Columns.Clear();
        }


        /// <summary>
        /// Creates a duplicate Recordset object that refers to the original Recordset object.
        /// Syntax
        /// expression .Clone
        /// expression A variable that represents a Recordset object.
        /// Return Value
        /// Recordset
        /// Remarks
        /// Use the Clone method to create multiple, duplicate Recordset objects. Each Recordset can have its own current record. Using Clone by itself doesn't change the data in the objects 
        /// or in their underlying structures. When you use the Clone method, you can share bookmarks between two or more Recordset objects because their bookmarks are interchangeable.
        /// You can use the Clone method when you want to perform an operation on a Recordset that requires multiple current records. This is faster and more efficient than opening a second 
        /// Recordset. When you create a Recordset with the Clone method, it initially lacks a current record. To make a record current before you use the Recordset clone, you must set the 
        /// Bookmark property or use one of the Move methods, one of the Find methods, or the Seek method.
        /// Using the Close method on either the original or duplicate object doesn't affect the other object. For example, using Close on the original Recordset doesn't close the clone.
        /// Note
        /// Closing a clone recordset within a pending transaction will cause an implicit Rollback operation.
        /// When you clone a table-type Recordset object in a Microsoft Access workspace, the Index property setting is not cloned on the new copy of the recordset. You must copy the Index 
        /// property setting manually.
        /// </summary>
        /// <returns></returns>
        public IFCDataTable CloneRows()
        {
            //CHE: in VB6 clone method creates two recordset objects that point to the same recordset. 
            //This feature allows you to change the current record in the clone, 
            //while keeping the current record same in the original recordset object or vice versa.
            //e.g. changing position in clone will not affect position in original (MoveNext etc)

            //!!! This solution does not support this behaviour described in ADO Clone method documentation:
            //          Changes you make to one Recordset object are visible in all of its clones regardless of cursor type. 
            //          However, after you execute Requery on the original Recordset, the clones will no longer be synchronized to the original.

            //DataTable result = objDataTable;
            // TODO:CHE - Clone
            FCRecordset result = this;//FCRecordset result = this.Copy();

            if (ClonedRecordsets == null)
            {
                ClonedRecordsets = new List<FCRecordset>();
            }

            //CHE;CNA: copy also custom properties
            foreach (FCField f in this.Fields)
            {
                //IPI: IsAliased must be set first, otherwise setting BaseColumnName will generate an incorrect result for result.Fields(f.ColumnName)
                result.Fields[f.ColumnName].IsAliased = f.IsAliased;

                result.Fields[f.ColumnName].Type = f.Type;
                result.Fields[f.ColumnName].BaseTableName = f.BaseTableName;
                result.Fields[f.ColumnName].BaseColumnName = f.BaseColumnName;

                //IPI: to be able to change values in the clone table, all columns must not be readonly
                result.Columns[f.ColumnName].ReadOnly = false;
            }

            result.Filter = "";

            result.LockType = LockType;

            //Attach all the rows from the copy DataTable to the rows in the source DataTable, because copy() 
            //copies the structure and all the data in the source DataTable but does not generate the TableNewRow event
            //in order to set the paired rows
            //APE: if the source data table contains changes that where not accepted using AcceptChanges
            //the new data will not be transfered to the cloned table. The modified values need to be copied to the resulting cloned table
            foreach (DataRow dr in this.Rows)
            {
                if (dr.RowState == DataRowState.Modified)
                {
                    DataRow clonedRow = result.Rows[this.Rows.IndexOf(dr)];
                    for (int counter = 0; counter < this.Columns.Count; counter++)
                    {
                        if (clonedRow[counter] != dr[counter])
                        {
                            clonedRow[counter] = dr[counter];
                        }
                    }
                }

                DataRow resultDataRow = result.Rows[this.Rows.IndexOf(dr)];
                dr.PairRow(resultDataRow);
            }

            //IPI: all rows in the DefaultView are also in the Rows collection except of a possible new row
            if (this.DefaultView.Count > 0 && this.DefaultView[this.DefaultView.Count - 1].IsNew)
            {
                DataRowView drView = this.DefaultView[this.DefaultView.Count - 1];
                {
                    DataRowView dr = result.DefaultView.AddNew();
                    dr.Row.ItemArray = drView.Row.ItemArray.Clone() as object[];
                    drView.Row.PairRow(dr.Row);
                }
            }

            //clone also NewRows
            if (NewRows != null && NewRows.Count > 0)
            {
                if (result.NewRows == null)
                {
                    result.NewRows = new List<DataRow>();
                }

                //CHE: collection is modified in RowChanging, use for instead of foreach
                //foreach (DataRow row in NewRows)
                for (int i = 0; i < NewRows.Count; i++)
                {
                    DataRow row = NewRows[i];
                    DataRow dr = result.NewRow();
                    dr.ItemArray = row.ItemArray.Clone() as object[];
                    result.NewRows.Add(dr);
                    row.PairRow(dr);
                }
            }

            ClonedRecordsets.Add(result);

            //IPI: all clones must keep reference to each other (any rows operation must be reflected in all clones)
            if (result.ClonedRecordsets == null)
            {
                result.ClonedRecordsets = new List<FCRecordset>();
            }
            result.ClonedRecordsets.Add(this);

            //CHE: rows marked for deletion should not be copied when creating clone
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (result.Rows[i].RowState == DataRowState.Deleted)
                {
                    result.Rows[i].AcceptChanges();
                }
            }

            //IPI: after the values in the clone table have been synchronized, must adjust the ReadOnly property as in the source DataTable
            foreach (DataColumn col in result.Columns)
            {
                col.ReadOnly = this.Columns[col.ColumnName].ReadOnly;
            }

            result.DataAdapter = DataAdapter;

            return result;
        }

        /// <summary>
        /// Creates a new record for an updatable Recordset object.
        /// Syntax
        /// expression .AddNew
        /// expression A variable that represents a Recordset object.
        /// Remarks
        /// Use the AddNew method to create and add a new record in the Recordset object named by recordset. This method sets the fields to default values, and if no default values are specified, 
        /// it sets the fields to Null (the default values specified for a table-type Recordset).
        /// After you modify the new record, use the Update method to save the changes and add the record to the Recordset. No changes occur in the database until you use the Update method.
        /// Note
        /// If you issue an AddNew and then perform any operation that moves to another record, but without using Update, your changes are lost without warning. In addition, if you close the Recordset 
        /// or end the procedure that declares the Recordset or its Database object, the new record is discarded without warning.
        /// Note
        /// When you use AddNew in a Microsoft Access workspace and the database engine has to create a new page to hold the current record, page locking is pessimistic. If the new record fits in an 
        /// existing page, page locking is optimistic.
        /// If you haven't moved to the last record of your Recordset, records added to base tables by other processes may be included if they are positioned beyond the current record. If you add a record 
        /// to your own Recordset, however, the record is visible in the Recordset and included in the underlying table where it becomes visible to any new Recordset objects.
        /// The position of the new record depends on the type of Recordset:
        /// In a dynaset-type Recordset object, records are inserted at the end of the Recordset, regardless of any sorting or ordering rules that were in effect when the Recordset was opened.
        /// In a table-type Recordset object whose Index property has been set, records are returned in their proper place in the sort order. If you haven't set the Index property, new records are returned 
        /// at the end of the Recordset.
        /// The record that was current before you used AddNew remains current. If you want to make the new record current, you can set the Bookmark property to the bookmark identified by the LastModified 
        /// property setting.
        /// Note
        /// To add, edit, or delete a record, there must be a unique index on the record in the underlying data source. If not, a "Permission denied" error will occur on the AddNew, Delete, or Edit method 
        /// call in a Microsoft Access workspace.
        /// </summary>
        public DataRow AddNew()
        {
            //NewRow will generate TableNewRow event in which the Synchronize should not be executed
            suppressCloneSynchronization = true;
            //IPI, SBE: when AddNew is called in the grid's AfterUpdate handler, must check if the grid already has a new row in DefaultView. Check if ParentGrid is set
            //DataRow objRow = this.NewRow();
            DataRow objRow = null;
            DataGridView grid = ParentGrid;
            if (grid != null && this.DefaultView.Count > 0 && this.DefaultView[this.DefaultView.Count - 1].IsNew)
            {
                objRow = this.DefaultView[this.DefaultView.Count - 1].Row;
            }
            else
            {
                //CHE: do not add in DefaultView unless grid is attached
                //if (this.DefaultView.AllowNew)
                if (this.DefaultView.AllowNew && grid != null)
                {
                    //SBE: set row empty values for last row before add new row
                    if (this.DefaultView.Count > 0)
                    {
                        DataRow lastRow = this.DefaultView[this.DefaultView.Count - 1].Row;
                        this.SetRowEmptyValues(lastRow);
                        //IPI: when below call DefaultView.AddNew is executed, lastRow will be committed to the Rows collection
                        // if a row is in the DataTable.Rows collection, it should be removed from NewRows
                        if (NewRows != null && NewRows.Contains(lastRow))
                        {
                            NewRows.Remove(lastRow);
                        }
                    }
                    //IPI: if DataTable is bound to a DGV, the current cell must reflect the new row in the DefaultView
                    DataRowView rowView = this.DefaultView.AddNew();
                    objRow = rowView.Row;

                    if (grid != null && grid.CurrentCell != null)
                    {
                        foreach (DataGridViewRow row in grid.Rows)
                        {
                            if (row.DataBoundItem != null && ((DataRowView)row.DataBoundItem) == rowView)
                            {
                                grid.CurrentCell = grid[grid.CurrentCell.ColumnIndex, grid.Rows.IndexOf(row)];
                                break;
                            }
                        }
                    }
                }
                else
                {
                    objRow = this.NewRow();
                }
            }

            suppressCloneSynchronization = false;

            //CHE: AddNew on DataTable with mandatory fields will raise exception if new empty row is added in collection, must be added later on Update
            if (NewRows == null)
            {
                NewRows = new List<DataRow>();
            }

            NewRows.Add(objRow);

            //CHE: do not add in DefaultView unless grid is attached, move index on last row 
            //Index = this.Rows.Count + 1;
            Index = this.Rows.Count + NewRows.Count;
            SynchronizeClones(this, Synchronization.DetachedNewRow, objRow);
            return objRow;
        }

        /// <summary>
        /// synchronize clones
        /// </summary>
        /// <param name="source"></param>
        /// <param name="syncType"></param>
        /// <param name="sourceRow"></param>
        public void SynchronizeClones(FCRecordset source, Synchronization syncType, DataRow sourceRow = null)
        {
            List<FCRecordset> clonedRecordsets = ClonedRecordsets;

            if (clonedRecordsets == null)
            {
                return;
            }

            //IPI: avoid infinite loop as each clone has reference to all the other clones
            if (suppressCloneSynchronization)
            {
                return;
            }

            //Avoid infinite looping by setting the suppress flag to true. The source data table is contained in the clone
            //DataTable list of it`s clones for track-back referencing
            suppressCloneSynchronization = true;

            int? originalIndex = 0;
            foreach (FCRecordset cloneRecordset in clonedRecordsets)
            {
                //The cloneTable may be the parent DataTable, in this case, suppress the sync mechanism
                if (cloneRecordset.suppressCloneSynchronization)
                    continue;

                if (syncType != Synchronization.TableNewRow)
                {
                    //IPI: current row can be different in original and clone table
                    //In case of EOF or BOF the Bookmark will throw an exception (in VB6 is the same behavior)
                    if (!cloneRecordset.EOF && !cloneRecordset.BOF)
                    {
                        //IPI: must use the index, because setting the bookmark will also change the CurrentCell in DGV
                        originalIndex = cloneRecordset.Index;

                        if (!source.EOF && !source.BOF)
                        {
                            //IPI: must use the index, because setting the bookmark will also change the CurrentCell in DGV
                            cloneRecordset.Index = source.Index;
                        }
                    }
                }
                DataRow DataRow = null;
                switch (syncType)
                {
                    //TableNewRow is called when the row is added by editing the bound grid by the user
                    case Synchronization.TableNewRow:
                        //The new row in the clone table is added in the DefaultView of the DataTable because
                        //the functionality of the grid adds it in the grid`s data source DefaultView and not in the DataTable
                        DataRow = cloneRecordset.DefaultView.AddNew().Row;
                        sourceRow.PairRow(DataRow);
                        break;

                    //DetachedNewRow is called when the row is created from the application using the AddNew function
                    case Synchronization.DetachedNewRow:
                        sourceRow.PairRow(cloneRecordset.AddNew());
                        break;

                    case Synchronization.DeleteRow:
                        foreach (DataRow pairRow in sourceRow.PairRow())
                        {
                            pairRow.Delete();
                        }
                        break;

                    case Synchronization.UpdateRow:
                        //CHE: for disconnected DataTable do nothing on update
                        if (DataAdapter != null && DataAdapter.SelectCommand != null && DataAdapter.SelectCommand.Connection == null)
                        {
                        }
                        else
                        {
                            //CHE: do not execute Update for clone on DataAdapter (PK violation), only modify RowState
                            //cloneTable.Update();
                            cloneRecordset.AcceptChanges();
                        }
                        break;

                    //IPI: when an EndEdit is called on a DataTable, the new row from the DefaultView is added to the DataTable.Rows collection
                    // this has to be synchronized in all clone tables
                    case Synchronization.MoveRowFromDefaultView:
                        foreach (DataRow pairRow in sourceRow.PairRow())
                        {
                            foreach (DataRowView drv in cloneRecordset.DefaultView)
                            {
                                if (drv.Row == pairRow)
                                {
                                    drv.EndEdit();
                                    break;
                                }
                            }
                        }
                        break;
                }

                if (syncType != Synchronization.TableNewRow && originalIndex > 0)
                {
                    //IPI: must use the index, because setting the bookmark will also change the CurrentCell in DGV
                    cloneRecordset.Index = originalIndex.Value;

                }
            }
            suppressCloneSynchronization = false;
        }

        /// <summary>
        /// expression A variable that represents a Recordset object.
        /// UpdateType - Optional Long
        /// A UpdateTypeEnum constant indicating the type of update, as specified in Settings (ODBCDirect workspaces only).
        /// Force - Optional Boolean
        /// A Boolean value indicating whether or not to force the changes into the database, regardless of whether the underlying data has been changed by another user 
        /// since the AddNew, Delete, or Edit call. If True, the changes are forced and changes made by other users are simply overwritten. If False (default), changes made by another user while the update is pending will cause the update to fail for those changes that are in conflict. No error occurs, but the BatchCollisionCount and BatchCollisions properties will indicate the number of conflicts and the rows affected by conflicts, respectively (ODBCDirect workspaces only).
        /// Remarks
        /// Use Update to save the current record and any changes you've made to it.
        /// Important
        /// Changes to the current record are lost if:
        /// You use the Edit or AddNew method, and then move to another record without first using Update.
        /// You use Edit or AddNew, and then use Edit or AddNew again without first using Update.
        /// You set the Bookmark property to another record.
        /// You close the Recordset without first using Update.
        /// You cancel the Edit operation by using CancelUpdate.
        /// To edit a record, use the Edit method to copy the contents of the current record to the copy buffer. If you don't use Edit first, an error occurs when you use Update or attempt to change a 
        /// field's value.
        /// In an ODBCDirect workspace, you can do batch updates, provided the cursor library supports batch updates, and the Recordset was opened with the optimistic batch locking option.
        /// In a Microsoft Access workspace, when the Recordset object's LockEdits property setting is True (pessimistically locked) in a multiuser environment, the record remains locked from the time 
        /// Edit is used until the Update method is executed or the edit is canceled. If the LockEdits property setting is False (optimistically locked), the record is locked and compared with the 
        /// pre-edited record just before it is updated in the database. If the record has changed since you used the Edit method, the Update operation fails. Microsoft Access database engine-connected 
        /// ODBC and installable ISAM databases always use optimistic locking. To continue the Update operation with your changes, use the Update method again. To revert to the record as the other user 
        /// changed it, refresh the current record by using Move 0.
        /// Note
        /// To add, edit, or delete a record, there must be a unique index on the record in the underlying data source. If not, a "Permission denied" error will occur on the AddNew, Delete, or Edit 
        /// method call in a Microsoft Access workspace, or an "Invalid argument" error will occur on the Update call in an ODBCDirect workspace.
        /// </summary>
        /// <param name="updateType"></param>
        /// <param name="force"></param>
        public int Update(int updateType = 1, bool force = false)
        {
            //check if current row exist
            if (this.EOF || this.BOF)
            {
                return 0;
            }

            this.isEOFandBOFchecked = true;
            DataRow dataRow =  this.CurrentRow;
            this.isEOFandBOFchecked = false;

            DataRowState rowState = dataRow.RowState;

            //exit if no changes
            if (rowState == DataRowState.Unchanged)
            {
                return 0;
            }

            //move from internal collection NewRows to DataTable
            if (rowState == DataRowState.Added || rowState == DataRowState.Detached)
            {
                this.UpdateNewRows();
            }

            //CHE: for disconnected DataTable do nothing on update
            if (DataAdapter != null && DataAdapter.SelectCommand != null && DataAdapter.SelectCommand.Connection == null)
            {
                return 0;
            }
            if (DataAdapter != null)
            {
                return UpdateInternal(dataRow);
            }
            else if (!string.IsNullOrEmpty(this.Source))
            {
                //IPI: need to change the RowState from Modified to Unchanged
                this.AcceptChanges();
                return 0;
                // throw new Exception("The current DataTable is not updatable");
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Retrieves multiple rows from a Recordset object.
        /// expression .GetRows(NumRows)
        /// NumRows - Optional Variant
        /// The number of rows to retrieve.
        /// Return Value - Variant
        /// Remarks
        /// Use the GetRows method to copy records from a Recordset. GetRows returns a two-dimensional array. The first subscript identifies the field and the second identifies the row number. 
        /// For example, intField represents the field, and intRecord identifies the row number:
        /// avarRecords(intField, intRecord)
        /// To get the first field value in the second row returned, use code like the following:
        /// field1 = avarRecords(0,1)
        /// To get the second field value in the first row, use code like the following:
        /// field2 = avarRecords(1,0)
        /// The avarRecords variable automatically becomes a two-dimensional array when GetRows returns data.
        /// If you request more rows than are available, then GetRows returns only the number of available rows. You can use the Visual Basic for Applications UBound function to determine how many 
        /// rows GetRows actually retrieved, because the array is sized to fit the number of returned rows. For example, if you returned the results into a Variant called varA, you could use the following 
        /// code to determine how many rows were actually returned:
        /// numReturned = UBound(varA,2) + 1
        /// You need to use "+ 1" because the first row returned is in the 0 element of the array. The number of rows that you can retrieve is constrained by the amount of available memory. 
        /// You shouldn't use GetRows to retrieve an entire table into an array if it is large.
        /// Because GetRows returns all fields of the Recordset into the array, including Memo and Long Binary fields, you might want to use a query that restricts the fields returned.
        /// After you call GetRows, the current record is positioned at the next unread row. That is, GetRows has the same effect on the current record as Move numrows.
        /// If you are trying to retrieve all the rows by using multiple GetRows calls, use the EOF property to be sure that you're at the end of the Recordset. GetRows returns less than the number 
        /// requested if it's at the end of the Recordset, or if it can't retrieve a row in the range requested. For example, if you're trying to retrieve 10 records, but you can't retrieve the fifth 
        /// record, GetRows returns four records and makes the fifth record the current record. This will not generate a run-time error. This might occur if another user deletes a record in a dynaset-type 
        /// Recordset. See the example for a demonstration of how to handle this.
        /// </summary>
        /// <param name="numRows"></param>
        /// <returns></returns>
        public object[,] GetRows(object numRows = null)
        {
            int rowsCount = this.Rows.Count;
            int columnsCount = this.Columns.Count;

            object[,] data = new object[rowsCount, columnsCount];

            //copy data
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    data[i, j] = this.Rows[i][j];
                }
            }

            return data;
        }

        /// <summary>
        /// Locates the first record in a dynaset- or snapshot-type Recordset object that satisfies the specified criteria and makes that record the current record (Microsoft Access workspaces only).
        /// expression .FindFirst(Criteria)
        /// Criteria - Required String
        /// A String used to locate the record. It is like the WHERE clause in an SQL statement, but without the word WHERE.
        /// Remarks
        /// If you want to include all the records in your search — not just those that meet a specific condition — use the Move methods to move from record to record. To locate a record in a table-type 
        /// Recordset, use the Seek method.
        /// If a record matching the criteria isn't located, the current record pointer is unknown, and the NoMatch property is set to True. If recordset contains more than one record that satisfies 
        /// the criteria, FindFirst locates the first occurrence, FindNext locates the next occurrence, and so on.
        /// Each of the Find methods begins its search from the location and in the direction specified in the following table.
        /// Find method Begins searching at Search direction
        /// FindFirst Beginning of recordset End of recordset
        /// FindLast End of recordset Beginning of recordset
        /// FindNext Current record End of recordset
        /// FindPrevious Current record Beginning of recordset
        /// Using one of the Find methods isn't the same as using a Move method, however, which simply makes the first, last, next, or previous record current without specifying a condition. 
        /// You can follow a Find operation with a Move operation.
        /// Always check the value of the NoMatch property to determine whether the Find operation has succeeded. If the search succeeds, NoMatch is False. If it fails, NoMatch is True and the current 
        /// record isn't defined. In this case, you must position the current record pointer back to a valid record.
        /// Using the Find methods with Microsoft Access database engine-connected ODBC-accessed recordsets can be inefficient. You may find that rephrasing your criteria to locate a specific record is 
        /// faster, especially when working with large recordsets.
        /// When working with Microsoft Access database engine-connected ODBC databases and large dynaset-type Recordset objects, you might discover that using the Find methods or using the Sort or 
        /// Filter property is slow. To improve performance, use SQL queries with customized ORDER BY or WHERE clauses, parameter queries, or QueryDef objects that retrieve specific indexed records.
        /// You should use the U.S. date format (month-day-year) when you search for fields containing dates, even if you're not using the U.S. version of the Microsoft Access database engine; otherwise, 
        /// the data may not be found. Use the Visual Basic Format function to convert the date. For example:
        /// VBA
        /// rstEmployees.FindFirst "HireDate > #" & Format(mydate, 'm-d-yy' ) & "#" 
        /// If criteria is composed of a string concatenated with a non-integer value, and the system parameters specify a non-U.S. decimal character such as a comma 
        /// (for example, strSQL = "PRICE > " & lngPrice, and lngPrice = 125,50), an error occurs when you try to call the method. This is because during concatenation, the number will be converted 
        /// to a string using your system's default decimal character, and Microsoft Access SQL only accepts U.S. decimal characters.
        /// Note
        /// For best performance, the criteria should be in either the form "field = value" where field is an indexed field in the underlying base table, or "field LIKE prefix" where field is an 
        /// indexed field in the underlying base table and prefix is a prefix search string (for example, "ART*" ).
        /// In general, for equivalent types of searches, the Seek method provides better performance than the Find methods. This assumes that table-type Recordset objects alone can satisfy your needs.
        /// </summary>
        /// <param name="criteria"></param>
        public void FindFirst(string criteria)
        {
            NoMatch = false;

            //do not moveselection, in vb6 when calling find the rowcolchange does not raise for each row only after find once when the position is set to founded row
            this.MoveFirst(false);

            if (!EOF)
            {

                DataRow[] objSelectedRows = null;
                try
                {
					//DSE: VB6 DAO FindFirst accepts criteria such as "column like 1", but here it throws exception e.g. "Cannot perform 'Like' operation on System.Int32 and System.Int16"
					//Use regular expressions to replace such cases with "column = 1"
					if(criteria.ToLower().Contains("like"))
                    {
                        string regexPatern = "(?<start>^| )[lL][iI][kK][eE][\\s]+'*(?<number>[0-9]+)'*";
						string regexReplacement = "${start}= ${number}";
						criteria = System.Text.RegularExpressions.Regex.Replace(criteria, regexPatern, regexReplacement);

                    }
					objSelectedRows = this.Select(criteria);
                }
                catch
                {
                    //if record not found, for DAO set index at the beginning
                    this.MoveFirst(false);
                    NoMatch = true;
                    return;
                }

                //IPI: if no record is found, move index on EOF; + 2 ensures that index is not on a new row in DefaultView 
                if (objSelectedRows.Length == 0)
                {
                    //if record not found, for DAO set index at the beginning
                    this.MoveFirst(false);
                    NoMatch = true;
                    return;
                }
                
                //search in view from the current position forward
                while (!this.EOF)
                {
                    if (objSelectedRows != null && objSelectedRows.Contains(this.CurrentRow))
                    {
                        //move also position in grid
                        MoveSelection();
                        return;
                    }
                    //do not moveselection, in vb6 when calling find the rowcolchange does not raise for each row only after find once when the position is set to founded row
                    this.MoveNext(false);
                }

                //if record not found, for DAO set index at the beginning
                this.MoveFirst(false);
            }

            NoMatch = true;
        }

        /// <summary>
        /// You can delete an existing record in a table or dynaset-type Recordset object by using the Delete method. You cannot delete records from a snapshot-type Recordset object.
        /// When you use the Delete method, the Microsoft Access database engine immediately deletes the current record without any warning or prompting. Deleting a record does not 
        /// automatically cause the next record to become the current record; to move to the next record you must use the MoveNext method. However, keep in mind that after you have 
        /// moved off the deleted record, you cannot move back to it.
        /// If you try to access a record after deleting it on a table-type Recordset, you will see error 3167, "Record is deleted." On a dynaset, you will see error 3021, "No current 
        /// record."
        /// If you have a Recordset clone positioned at the deleted record and you try to read its value, you will see error 3167 regardless of the type of Recordset object. Trying 
        /// to use a bookmark to move to a deleted record will also result in error 3167.
        /// </summary>
        public void Delete()
        {
            if (Deleted)
            {
                throw new Exception("Row handle refered to a deleted row or a row marked for deletion.");
            }

            if (EOF || BOF)
            {
                throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
            }

            //CHE: only mark for deletion, index remains the same
            //this.Rows.Remove(this.CurrentRow());
            //Index = Index - 1;
            //APE: preserve index when deleting. If the current cell is on the row that is being deleted, only if the DataTable is bound to a DataGridView, the index will shift due to the CurrentCellChange
            //event of the bound DataGridView
            int index = -1;
            if (ParentGrid != null)
            {
                index = Index;
            }
            isEOFandBOFchecked = true;
            this.CurrentRow.Delete();
            isEOFandBOFchecked = false;
            if (index > -1)
            {
                Index = index;
            }

            //CHE - row should be physically deleted - see summary comments
            this.Update();

            Deleted = true;
        }

        /// <summary>
        /// Copies the current record from an updatable Recordset object to the copy buffer for subsequent editing.
        /// expression .Edit
        /// Remarks
        /// Once you use the Edit method, changes made to the current record's fields are copied to the copy buffer. After you make the desired changes to the record, use the Update method to save your 
        /// changes.
        /// The current record remains current after you use Edit.
        /// Note
        /// If you edit a record and then perform any operation that moves to another record, but without first using Update, your changes are lost without warning. In addition, if you close recordset or 
        /// end the procedure which declares the Recordset or the parent Database or Connection object, your edited record is discarded without warning.
        /// Using Edit produces an error if:
        /// There is no current record.
        /// The Connection, Database, or Recordset object was opened as read-only.
        /// No fields in the record are updatable.
        /// The Database or Recordset was opened for exclusive use by another user (Microsoft Access workspace).
        /// Another user has locked the page containing your record (Microsoft Access workspace).
        /// In a Microsoft Access workspace, when the Recordset object's LockEdits property setting is True (pessimistically locked) in a multiuser environment, the record remains locked from the time 
        /// Edit is used until the update is complete. If the LockEdits property setting is False (optimistically locked), the record is locked and compared with the pre-edited record just before it's 
        /// updated in the database. If the record has changed since you used the Edit method, the Update operation fails with a run-time error if you use OpenRecordset without specifying dbSeeChanges. 
        /// By default, Microsoft Access database engine-connected ODBC and installable ISAM databases always use optimistic locking.
        /// Note
        /// To add, edit, or delete a record, there must be a unique index on the record in the underlying data source. If not, a "Permission denied" error will occur on the AddNew, Delete, or Edit method 
        /// call in a Microsoft Access workspace.
        /// </summary>
        public void Edit()
        {
        }

        //check if datatable has pending new rows
        public bool HasNewRows()
        {
            if (NewRows != null)
            {
                return NewRows.Count > 0;
            }
            return false;
        }

        #endregion

        #region Internal Methods

        internal int GetFieldSize(int? size, DataTypeEnum type)
        {
            if (size.HasValue)
            {
                return size.Value;
            }

            switch (type)
            {
                case DataTypeEnum.dbInteger:
                case DataTypeEnum.dbComplexInteger:
                    return 4;
                    
                case DataTypeEnum.dbBigInt:
                case DataTypeEnum.dbLong:
                case DataTypeEnum.dbLongBinary:
                case DataTypeEnum.dbComplexGUID:
                case DataTypeEnum.dbComplexLong:
                case DataTypeEnum.dbSingle:
                case DataTypeEnum.dbDecimal:
                case DataTypeEnum.dbCurrency:
                case DataTypeEnum.dbFloat:
                    return 8;
                    
                case DataTypeEnum.dbDouble:
                case DataTypeEnum.dbComplexDecimal:
                case DataTypeEnum.dbComplexDouble:
                case DataTypeEnum.dbComplexSingle:
                case DataTypeEnum.dbNumeric:
                    return 16;
                    
                case DataTypeEnum.dbBoolean:
                case DataTypeEnum.dbChar:
                case DataTypeEnum.dbByte:
                    return 1;
                    
                //JEI dbText should be not zero
                case DataTypeEnum.dbText:
                    return size.Value;
                    
                default:
                    return 0;                    
            }            
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private void MoveSelection()
        {
            if (ParentGrid != null)
            {
                DataGridView grid = ParentGrid;
                int dataTableDefinitionsIndex = Index;
                //BAN: if grid allows the user to add new rows, when the selection will be set to the last row,
                // a new empty row will be added in the DataTable DefaultView. Setting AllowUserToAddRows to false will prevent this
                int indexAdjustment = 0;
                if (grid.AllowUserToAddRows == true)
                {
                    indexAdjustment = 1;
                }
                if ((Index - 1) >= 0 && (Index - 1) < grid.Rows.Count - indexAdjustment)
                {
                    //CHE: avoid null exception
                    if (grid.CurrentCell != null)
                    {
                        //EndEdit if anything in edit
                        bool endEditForCurrentRow = false;
                        foreach (DataRowView drView in this.DefaultView)
                        {
                            if (drView.IsEdit && drView.Row.RowState != DataRowState.Deleted)
                            {
                                endEditForCurrentRow = false;
                                //SBE: check if row was modified
                                for (int i = 0; i < drView.Row.ItemArray.Length; i++)
                                {
                                    object value = drView.Row.ItemArray[i];
                                    if (value != null && !Information.IsDBNull(value))
                                    {
                                        endEditForCurrentRow = true;
                                        break;
                                    }
                                }

                                if (endEditForCurrentRow)
                                {
                                    //CNA: need to set empty values for drView's cells if DataTable column does not allow DBNull
                                    this.SetRowEmptyValues(drView.Row);
                                    drView.EndEdit();
                                }
                            }
                        }
                        //move current cell based on current row in DataTable
                        //CHE: only move current cell if this.CurrentRow is valid (neither EOF nor BOF has been reached)
                        if (!(EOF || BOF))
                        {
                            DataRow currentRowInDataTable = this.CurrentRow;
                            foreach (DataGridViewRow row in grid.Rows)
                            {
                                if (row.DataBoundItem != null && ((DataRowView)row.DataBoundItem).Row == currentRowInDataTable)
                                {
                                    grid.CurrentCell = grid[grid.CurrentCell.ColumnIndex, grid.Rows.IndexOf(row)];
                                    break;
                                }
                            }
                        }
                    }
                    //BAN: Needed to save and reset the dataTableDefinitions index because, when setting the current cell
                    //the selected index changed event is raised before setting the new value. In FCTrueDBGrid, in the event handler
                    //the row will be the previous one then and it will reset the data table definitions index with the wrong index
                    Index = dataTableDefinitionsIndex;
                }
            }
        }

        private int UpdateInternal(DataRow dataRow)
        {
            int affectedRows = 0;
            try
            {
                //CHE: for tables with large number of columns (e.g. 80) when calling DataAdapter.Update error "Query too complex" is received because where clause contains too many conditions;
                //modify UpdateCommand to contain only PK column in the where clause
                OleDbCommandBuilder builder = new OleDbCommandBuilder(DataAdapter as OleDbDataAdapter);
                //     Specifies whether all column values in an update statement are included or
                //     only changed ones.
                builder.SetAllValues = false;
                //     All update and delete statements include only System.Data.DataTable.PrimaryKey
                //     columns in the WHERE clause. If no System.Data.DataTable.PrimaryKey is defined,
                //     all searchable columns are included in the WHERE clause.
                builder.ConflictOption = ConflictOption.OverwriteChanges;
                DataAdapter.UpdateCommand = builder.GetUpdateCommand();

                bool isAdded = dataRow.RowState == DataRowState.Added;
                affectedRows = DataAdapter.Update(this);


                //SBE: if table contains the IdentityColumn, update each row. On INSERT get the last inserted ID and update DataTable
                if (!object.Equals(IdentityColumn, null) && isAdded && !string.IsNullOrEmpty(this.TableName))
                {
                    string strSQLGetIdentityValue = "SELECT @@Identity";

                    //SBE: execute statement to get the value for identity column
                    if (affectedRows > 0)
                    {
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.Connection = this.ActiveConnection as OleDbConnection;
                        // TODO:CHE - GetTransaction
                        //cmd.Transaction = cmd.Connection.GetTransaction();
                        cmd.CommandText = strSQLGetIdentityValue;
                        object idValue = cmd.ExecuteScalar();
                        dataRow[IdentityColumn.ColumnName] = idValue;
                        //BAN: avoid error on future update for identity column that is seen as modified 
                        dataRow.AcceptChanges();
                    }
                }
            }
            //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key                
            //e.g. Dynamic SQL generation for the UpdateCommand is not supported against a SelectCommand that does not return any key column information.
            catch (InvalidOperationException ex)
            {
                //clear errors
                if (this.HasErrors)
                {
                    DataRow[] rowsInError = this.GetErrors();
                    for (int i = 0; i < rowsInError.Length; i++)
                    {
                        //foreach (DataColumn myCol in this.Columns)
                        //{
                        //    Console.WriteLine(myCol.ColumnName + " " +
                        //    rowsInError[i].GetColumnError(myCol));
                        //}

                        rowsInError[i].ClearErrors();
                    }
                }

                //CHE: in UpdateBatch before calling FillSchema replace null field with default value from SQL server if field does not accept null
                foreach (FCField f in this.Fields)
                {
                    if (f.Required)
                    {
                        try
                        {
                            foreach (DataRow dr in this.Rows)
                            {
                                if (dr.RowState != DataRowState.Deleted && dr[f.ColumnName] == DBNull.Value && f.DefaultValue != null)
                                {
                                    string value = f.DefaultValue.TrimStart(new char[] { '(' }).TrimEnd(new char[] { ')' }); ;
                                    if (f.Type == DataTypeEnum.dbBoolean && value != null)
                                    {
                                        dr[f.ColumnName] = FCGlobal.BoolFromString(value);
                                    }
                                    else
                                    {
                                        dr[f.ColumnName] = value;
                                    }
                                }
                            }
                        }
                        catch
                        {
                        }
                    }
                }

                //CHE: schema is necessary in UpdateBatchBuilder e.g. to skip identity column
                DataAdapter.FillSchema(this, SchemaType.Mapped);

                //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
                affectedRows = UpdateTry(dataRow);

                if (affectedRows == -1)
                {
                    //THIS MESSAGE BOX SHOULD NEVER BE DISPLAYED; IF IT DOES A FIX NEEDS TO BE FOUNDED ON CUSTOMER CODE 
                    MessageBox.Show(ex.Message, "fecherFoundation - Fatal error on update recordset!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }

                //BAN: accept all changes in the data table, this way the row states will become unchanged, so if the same data table will have rows changed (added, modified, deleted)
                //and the UpdateBatch is called again, the previous rows will not influence the new call
                this.AcceptChanges();
            }
            //CHE: FilterType should not affect grid, restore position
            finally
            {
                if (ParentGrid != null && BookmarkBeforeFilterType != null && this.FilterType != FilterGroupEnum.adFilterNone)
                {
                    this.FilterType = FilterGroupEnum.adFilterNone;
                    //CHE: restore position only if it is valid
                    if (IsValidBookmark(BookmarkBeforeFilterType))
                    {
                        this.Bookmark = BookmarkBeforeFilterType;
                    }
                    BookmarkBeforeFilterType = null;
                }
            }

            return affectedRows;
        }

        //check if the current row is a new added row
        private bool IsNewRowAdded()
        {
            int currentIdx = Index;

            int totalRows = this.Rows.Count + (HasNewRows() ? NewRows.Count : 0);
            if (totalRows < this.DefaultView.Count)
            {
                totalRows = this.DefaultView.Count;
            }

            if (currentIdx < 1 || currentIdx > totalRows)
            {
                return false;
            }

            return currentIdx > this.Rows.Count;
        }

        private int DefaultViewRowsCountWithState()
        {
            //CHE: return rows from DefaultView
            int nr = this.DefaultView.Count;

            //CHE: include last row which is currently added only if changes were done
            if (ParentGrid != null && this.IsNewRowAdded())
            {
                if (!ParentGrid.IsCurrentRowDirty)
                {
                    nr--;
                }
            }

            //CHE: include rows from NewRows collection
            nr += NewRows == null ? 0 : NewRows.Count;

            //exclude deleted rows if flag not included in RowStateFilter
            if ((this.DefaultView.RowStateFilter & DataViewRowState.Deleted) != DataViewRowState.Deleted)
            {
                foreach (DataRowView drv in this.DefaultView)
                {
                    if (drv.Row.RowState == DataRowState.Deleted)
                    {
                        --nr;
                    }
                }
            }

            return nr;
        }

        private void UpdateNewRows()
        {
            foreach (DataRowView drView in this.DefaultView)
            {
                bool endEditForCurrentRow = false;

                if (drView.IsEdit && !drView.IsNew)
                {
                    endEditForCurrentRow = true;
                }
                else if (drView.IsEdit && drView.IsNew && drView.Row.RowState != DataRowState.Deleted)
                {
                    //SBE: check if row was modified
                    for (int i = 0; i < drView.Row.ItemArray.Length; i++)
                    {
                        object value = drView.Row.ItemArray[i];
                        if (value != null && !Information.IsDBNull(value))
                        {
                            endEditForCurrentRow = true;
                            break;
                        }
                    }

                    if (endEditForCurrentRow)
                    {
                        //CNA: need to set empty values for drView's cells if DataTable column does not allow DBNull
                        this.SetRowEmptyValues(drView.Row);
                        drView.EndEdit();
                        //IPI: when EndEdit is executed, drView.Row will be committed to the Rows collection
                        // if a row is in the DataTable.Rows collection, it should be removed from NewRows
                        if (NewRows != null && NewRows.Contains(drView.Row))
                        {
                            NewRows.Remove(drView.Row);
                        }
                    }
                }
            }

            //CHE: AddNew on DataTable with mandatory fields will raise exception if new empty row is added in collection, must be added later on Update
            if (this.HasNewRows())
            {
                List<DataRow> dataTableRows = this.AsEnumerable().ToList();
                //CHE: collection is modified in RowChanging, use for instead of foreach
                //foreach (DataRow row in NewRows)
                for (int i = 0; i < NewRows.Count; i++)
                {
                    DataRow row = NewRows[i];
                    //BAN: add the new row only if the row is not already contained in the DataTable
                    if (!dataTableRows.Contains(row))
                    {
                        this.SetRowEmptyValues(row);
                        this.Rows.Add(row);
                    }
                }
                NewRows.Clear();
            }

            //IPI: must update all clone tables as well
            SynchronizeClones(this, Synchronization.UpdateRow);
        }

        private bool IsValidBookmark(int? intBookmark)
        {
            //The bookmark value must be checked also against the NewRows collection, because rows might have been added using the AddNew method
            //and these rows are not present in the DataTable Rows collection
            if (intBookmark < 1 || intBookmark > Math.Max(this.Rows.Count, this.DefaultView.Count) + (this.HasNewRows() ? NewRows.Count : 0))
            {
                return false;
            }
            return true;
        }


        //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
        private int UpdateTry(DataRow dataRow)
        {
            int colCount = this.Columns.Count;
            List<CommandType> commands = new List<CommandType>();

            DataRowState drs = dataRow.RowState;
            //If the TableName is not null means that the statement was executed on one table. If the statement contains join 
            //the columns must be separated by parent table, and statements need to be created individually for each table
            if (!string.IsNullOrEmpty(this.TableName))
            {
                StringBuilder str1 = new StringBuilder();
                StringBuilder str2 = null;
                if (drs != DataRowState.Deleted)
                {
                    str2 = new StringBuilder();
                }
                DataColumn dataColumn = null;
                for (int columnIndex = 0; columnIndex < colCount; columnIndex++)
                {
                    dataColumn = this.Columns[columnIndex];
                    if (drs == DataRowState.Deleted || drs == DataRowState.Modified)
                    {
                        str1.Append("`" + dataColumn.ColumnName + "`" + FormatValue(dataColumn.DataType, dataRow[columnIndex, DataRowVersion.Original], true));

                        if (columnIndex < colCount - 1)
                        {
                            str1.Append(" and ");
                        }
                    }
                    if (drs == DataRowState.Modified && !dataColumn.AutoIncrement)
                    {
                        str2.Append("`" + dataColumn.ColumnName + "`" + "=" + FormatValue(dataColumn.DataType, dataRow[columnIndex]));
                        if (columnIndex < colCount - 1)
                        {
                            str2.Append(", ");
                        }
                    }
                    if (drs == DataRowState.Added && !dataColumn.AutoIncrement)
                    {
                        str1.Append("`" + dataColumn.ColumnName + "`");
                        str2.Append(FormatValue(dataColumn.DataType, dataRow[columnIndex]));
                        if (columnIndex < colCount - 1)
                        {
                            str1.Append(", ");
                            str2.Append(", ");
                        }
                    }
                }

                CommandType item = new CommandType();
                string str = "";
                switch (drs)
                {
                    case DataRowState.Deleted:
                        {
                            str = String.Format("delete from {0} where {1}", this.TableName, str1.ToString());
                            item.whereClause = str1.ToString();
                            item.tableName = this.TableName;
                            break;
                        }
                    case DataRowState.Modified:
                        {
                            str = String.Format("update {0} set {1} where {2}", this.TableName, str2.ToString(), str1.ToString());
                            item.whereClause = str1.ToString();
                            item.tableName = this.TableName;
                            break;
                        }
                    case DataRowState.Added:
                        {
                            str = String.Format("insert into {0} ({1}) values ({2})", this.TableName, str1.ToString(), str2.ToString());
                            break;
                        }
                }
                item.command = str;
                commands.Add(item);
            }
            else
            {
                //Get all the tables from the JOIN statement and the corresponding columns
                Dictionary<string, List<DataColumn>> columnsGroupedByTable = new Dictionary<string, List<DataColumn>>();
                for (int count = 0; count < this.Columns.Count; count++)
                {
                    FCField columnField = this.Fields[count];
                    if (string.IsNullOrEmpty(columnField.BaseTableName))
                    {
                        continue;
                    }
                    if (!columnsGroupedByTable.ContainsKey(columnField.BaseTableName))
                    {
                        columnsGroupedByTable.Add(columnField.BaseTableName, new List<DataColumn>());
                    }
                    columnsGroupedByTable[columnField.BaseTableName].Add(this.Columns[count]);
                }
                //On each data row, separate statements must be created for each individual table used in the join statement
                foreach (KeyValuePair<string, List<DataColumn>> tableWithColumns in columnsGroupedByTable)
                {
                    StringBuilder str1 = new StringBuilder();
                    StringBuilder str2 = null;
                    if (drs != DataRowState.Deleted)
                    {
                        str2 = new StringBuilder();
                    }
                    foreach (DataColumn col in tableWithColumns.Value)
                    {
                        int columnIndex = this.Columns.IndexOf(col);
                        FCField columnField = this.Fields[columnIndex];
                        int columnIndexInList = tableWithColumns.Value.IndexOf(col);
                        if (drs == DataRowState.Deleted || drs == DataRowState.Modified)
                        {
                            //A column of datatype ntext cannot be used in a where clause by comparing it with a varchar
                            //a cast must be used for this operation
                            string beginCast = "";
                            string endCast = "";
                            if (columnField.Type == DataTypeEnum.dbMemo)
                            {
                                beginCast = "Cast(";
                                endCast = " as nvarchar(max))";
                            }

                            str1.Append(beginCast + "`" + columnField.BaseColumnName + "`" + endCast + FormatValue(col.DataType, dataRow[columnIndex, DataRowVersion.Original], true));

                            if (columnIndexInList < tableWithColumns.Value.Count - 1)
                            {
                                str1.Append(" and ");
                            }
                        }
                        if (drs == DataRowState.Modified && !this.Columns[columnIndex].AutoIncrement)
                        {
                            //A column might have been selected twice. Do not add the same column twice
                            if (!str2.ToString().Contains("`" + columnField.BaseColumnName + "`" + "="))
                            {
                                str2.Append("`" + columnField.BaseColumnName + "`" + "=" + FormatValue(col.DataType, dataRow[columnIndex]));
                                if (columnIndexInList < tableWithColumns.Value.Count - 1)
                                {
                                    str2.Append(", ");
                                }
                            }
                        }
                        if (drs == DataRowState.Added && !this.Columns[columnIndex].AutoIncrement)
                        {
                            str1.Append("`" + columnField.BaseColumnName + "`");
                            str2.Append(FormatValue(col.DataType, dataRow[columnIndex]));
                            if (columnIndexInList < tableWithColumns.Value.Count - 1)
                            {
                                str1.Append(", ");
                                str2.Append(", ");
                            }
                        }
                    }

                    CommandType item = new CommandType();
                    string str = "";
                    switch (drs)
                    {
                        case DataRowState.Deleted:
                            {
                                str = String.Format("delete from {0} where {1}", tableWithColumns.Key, str1.ToString());
                                item.whereClause = str1.ToString();
                                item.tableName = tableWithColumns.Key;
                                break;
                            }
                        case DataRowState.Modified:
                            {
                                str = String.Format("update {0} set {1} where {2}", tableWithColumns.Key, str2.ToString(), str1.ToString());
                                item.whereClause = str1.ToString();
                                item.tableName = tableWithColumns.Key;
                                break;
                            }
                        case DataRowState.Added:
                            {
                                str = String.Format("insert into {0} ({1}) values ({2})", tableWithColumns.Key, str1.ToString(), str2.ToString());
                                break;
                            }
                    }
                    item.command = str;
                    commands.Add(item);
                }
            }

            OleDbCommand com = new OleDbCommand();
            com.Connection = this.ActiveConnection as OleDbConnection;

            if (com.Connection.State == ConnectionState.Closed)
            {
                com.Connection.Open();
            }
            // TODO:CHE - GetTransaction
            //com.Transaction = com.Connection.GetTransaction() as SqlTransaction;

            bool multipleRows = false;
            int affectedRows = 0;

            foreach (CommandType item in commands)
            {
                if (!string.IsNullOrEmpty(item.whereClause) && !string.IsNullOrEmpty(item.tableName))
                {
                    com.CommandText = string.Format("select count(*) from {0} where {1}", item.tableName, item.whereClause);
                    affectedRows = Convert.ToInt32(com.ExecuteScalar());
                    if (affectedRows > 1)
                    {
                        multipleRows = true;
                        break;
                    }
                }
            }

            if (multipleRows)
            {
                //only one row should be affected; if not, update failed
                return -1;
            }

            affectedRows = 0;
            foreach (CommandType item in commands)
            {
                com.CommandText = item.command;
                int comAffectedRows = com.ExecuteNonQuery();
                if (comAffectedRows > 0)
                {
                    affectedRows = comAffectedRows;
                }
            }

            return affectedRows;
        }
        
        //CHE: generate statements if update was not automatically generated with commandbuilder e.g. table with no primary key
        private string FormatValue(Type type, object p, bool compare = false)
        {
            if (p == DBNull.Value)
            {
                return (compare ? " is " : "") + "null";
            }

            string str = (compare ? " = " : "");

            if (type == typeof(string))
            {
                return str + String.Format("'{0}'", FCConvert.ToString(p).Replace("'", "''"));
            }

            if (type == typeof(bool))
            {
                return str + Convert.ToInt32(p).ToString();
            }

            if (type == typeof(decimal) || type == typeof(float) || type == typeof(double))
            {
                return str + FCConvert.ToString(p, System.Globalization.CultureInfo.InvariantCulture);
            }

            if (type == typeof(DateTime))
            {
                return str + String.Format("#{0}#", ((DateTime)p).ToString("yyyy-MM-dd HH:mm:ss"));
            }

            return str + p.ToString();
        }

        private void FCRecordset_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            FCRecordset currentRecordset = sender as FCRecordset;
            List<FCRecordset> clonedRecordsets = ClonedRecordsets;

            //IPI: although values are correctly modified in the DataTable/ DefaultView, the row state does not get modified
            // this will have as effect that modified bound values will not be saved in the database
            if (e.Row.RowState == DataRowState.Unchanged)
            {
                object currentValue = e.Row[e.Column, DataRowVersion.Current];
                object proposedValue = e.ProposedValue;

                if ((!Information.IsDBNull(currentValue) && Information.IsDBNull(proposedValue)) ||
                    (Information.IsDBNull(currentValue) && !Information.IsDBNull(proposedValue)) ||
                    FCConvert.ToString(currentValue) != FCConvert.ToString(proposedValue))
                {
                    e.Row.SetModified();

                    //IPI: cell value must be synchronized after SetModified because
                    // SetModified is resetting the proposed value to the original value
                    e.Row[e.Column] = e.ProposedValue;
                }
            }

            if (clonedRecordsets == null)
            {
                return;
            }

            //Get row index from the current data table
            int rowIndex = currentRecordset.Rows.IndexOf(e.Row);

            string columnName = e.Column.ColumnName;

            //IPI: avoid infinite loop as each clone has reference to all the other clones
            if (!suppressCloneSynchronization)
            {
                currentRecordset.suppressCloneSynchronization = true;
                try
                {
                    //Loop trough the paired rows to set the values
                    DataRow DataRow = e.Row;
                    List<DataRow> pairRows = DataRow.PairRow();
                    foreach (DataRow pairRow in pairRows)
                    {
                        //IPI: need to synchronize the RowState as well
                        // SetModified can only be called on DataRows with Unchanged DataRowState.
                        if (pairRow.RowState == DataRowState.Unchanged)
                        {
                            //CHE: set RowState as in original
                            if (e.Row.RowState == DataRowState.Added)
                            {
                                pairRow.SetAdded();
                            }
                            if (e.Row.RowState == DataRowState.Modified)
                            {
                                pairRow.SetModified();
                            }
                        }

                        //IPI: cell value must be synchronized after SetModified because
                        // SetModified is resetting the proposed value to the original value
                        pairRow[columnName] = e.ProposedValue;
                    }
                }
                finally
                {
                    currentRecordset.suppressCloneSynchronization = false;
                }
            }
        }

        //IPI: when an EndEdit is called on a DataTable, the new row from the DefaultView is added to the DataTable.Rows collection
        // this has to be synchronized in all clone tables                
        private void FCRecordset_RowChanging(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Add && e.Row.RowState == DataRowState.Detached)
            {
                FCRecordset currentRecordset = sender as FCRecordset;
                if (currentRecordset != null)
                {
                    //CHE: when EndEdit is executed, e.Row will be committed to the Rows collection
                    // if a row is in the DataTable.Rows collection, it should be removed from NewRows
                    if (NewRows != null && NewRows.Contains(e.Row))
                    {
                        NewRows.Remove(e.Row);
                    }
                }
                DataRow DataRow = e.Row;
                SynchronizeClones(currentRecordset, Synchronization.MoveRowFromDefaultView, DataRow);
            }
        }

        private void FCRecordset_RowDeleted(object sender, DataRowChangeEventArgs e)
        {
            FCRecordset currentRecordset = sender as FCRecordset;

            int rowIndex = currentRecordset.Rows.IndexOf(e.Row);

            DataRow DataRow = e.Row;
            SynchronizeClones(currentRecordset, Synchronization.DeleteRow, DataRow);
        }

        private void FCRecordset_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            FCRecordset currentRecordset = sender as FCRecordset;

            int rowIndex = currentRecordset.Rows.IndexOf(e.Row);

            DataRow DataRow = e.Row;
            SynchronizeClones(currentRecordset, Synchronization.TableNewRow, DataRow);

        }

        private void UpdateDataView(bool blnForceCreate, FilterGroupEnum? FilterType = null)
        {
            DataView objDataView = null;
            //BAN: set the data view in order to compare sorting
            objDataView = this.DefaultView;
            //BAN: remove sorting if necessary
            //if (FilterType.HasValue || !string.IsNullOrEmpty(Sort) || !string.IsNullOrEmpty(Filter) || blnForceCreate)
            if (FilterType.HasValue || !string.IsNullOrEmpty(Sort) || (objDataView.Sort != Sort) || !string.IsNullOrEmpty(Filter) || blnForceCreate)
            {
                //CHE filter has to be set to datatable default view to be visible in datagridview when datagridview.datasource=datatable
                //objDataView = new DataView(objDataTable);
                //BAN: set dataview at the begining
                //objDataView = this.DefaultView;

                //IPI
                if (FilterType.HasValue)
                {
                    switch (FilterType)
                    {
                        case FilterGroupEnum.adFilterPendingRecords:
                            {
                                //CHE: when filter pending should contain also deleted rows
                                objDataView.RowStateFilter = DataViewRowState.ModifiedCurrent | DataViewRowState.Added | DataViewRowState.Deleted;
                                break;
                            }
                        case FilterGroupEnum.adFilterNone:
                            {
                                objDataView.RowFilter = "";
                                //SBE: removed deleted rowstate
                                objDataView.RowStateFilter = DataViewRowState.ModifiedCurrent | DataViewRowState.Added | DataViewRowState.Unchanged;
                                break;
                            }
                        case FilterGroupEnum.adFilterAffectedRecords:
                            {
                                objDataView.RowStateFilter = DataViewRowState.ModifiedCurrent;
                                break;
                            }
                        default:
                            break;
                    }
                }
                else
                {
                    //CNA Filter is always applied, when it is empty it means a reset
                    //if (!string.IsNullOrEmpty(Filter))
                    //{
                    objDataView.RowFilter = Filter;
                    //}
                    objDataView.Sort = Sort;
                }
            }
            View = objDataView;
            //BAN: after setting the filter, move to the first row of the DataView
            //APE: move to the first row of the DataView if RecordCount is not 0
            if (this.RecordCount != 0)
            {
                this.MoveFirst();
            }
        }

        private bool SetRowEmptyValues(DataRow row)
        {
            if (row == null)
            {
                return false;
            }

            bool valueChanged = false;
            //SBE: set empty values ("" for strings or 0 for numerics), if field does not allow null values
            bool isDeleted = (row.RowState == DataRowState.Deleted);

            if (!isDeleted)
            {
                Type columnType = null;
                foreach (FCField col in this.Fields)
                {
                    if (Information.IsDBNull(row[col.ColumnName]))
                    {
                        if (col.Required)
                        {
                            columnType = this.Columns[col.ColumnName].DataType;
                            if (columnType == typeof(System.String))
                            {
                                row[col.ColumnName] = "";
                            }
                            else if (columnType.IsValueType)
                            {
                                row[col.ColumnName] = Activator.CreateInstance(columnType);
                            }
                        }

                        //CHE: set default value (regardless the Required)
                        if (!string.IsNullOrEmpty(col.DefaultValue))
                        {
                            row[col.ColumnName] = col.DefaultValue;
                        }

                        valueChanged = true;
                    }
                }
            }

            return valueChanged;
        }

        private void SetFields()
        {
            using (OleDbConnection connection = new OleDbConnection(this.database.Connection.ConnectionString))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                DataTable tableColumns = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, this.TableName, null });
                FCTableDef tableDef = null;
                if (tableColumns.Rows.Count > 0)
                {
                    tableDef = this as FCTableDef;
                }

                int size = 0;
                FCField field = null;
                OleDbType dataType;
                DataTypeEnum fieldType;
                string colName = "";                

                foreach (DataRow row in tableColumns.Rows)
                {
                    size = 0;

                    if (!row["NUMERIC_PRECISION"].Equals(DBNull.Value))
                    {
                        size = Convert.ToInt32(row["NUMERIC_PRECISION"]); 
                    }
                    else 
                    {
                        if (!row["CHARACTER_MAXIMUM_LENGTH"].Equals(DBNull.Value))
                        {
                            size = Convert.ToInt32(row["CHARACTER_MAXIMUM_LENGTH"]);
                        }
                    }
                    field = null;
                    colName = row["COLUMN_NAME"].ToString();
                    dataType = (OleDbType)row["DATA_TYPE"];
                    fieldType = GetDataType(dataType);
                    if (tableDef != null)
                    {
                        field = tableDef.CreateField(colName, fieldType, size);
                        //IPI: VB6 Append function creates the column in the DB if it does not exist; but in this case, we iterate through the columns collection retrieved from DB
                        // so all columns already exist in the DB 
                        tableDef.Fields.Append(field, false);
                    }
                    else
                    {
                        field = this.fields[colName];
                        if (!Object.Equals(field, null))
                        {
                            field.Type = fieldType;
                            field.Size = this.GetFieldSize(size, fieldType);
                        }
                    }
                    if (!Object.Equals(field, null))
                    {
                        field.Required = !Convert.ToBoolean(row["IS_NULLABLE"]);
                        if (Convert.ToBoolean(row["COLUMN_HASDEFAULT"]))
                        {
                            field.DefaultValue = row["COLUMN_DEFAULT"].ToString();
                        }
                        //set identity column for autonumber
                        if (dataType == OleDbType.Integer && row["COLUMN_FLAGS"].ToString() == "90")
                        {
                            this.IdentityColumn = field;
                        }
                    }
                }                
            }
        }

        private DataTypeEnum GetDataType(OleDbType oleDbType)
        {
            switch (oleDbType)
            {
                case OleDbType.BigInt:
                    {
                        return DataTypeEnum.dbBigInt;
                    }
                case OleDbType.Binary:
                    {
                        return DataTypeEnum.dbBinary;
                    }
                case OleDbType.Boolean:
                    {
                        return DataTypeEnum.dbBoolean;
                    }
                case OleDbType.BSTR:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.Char:
                    {
                        return DataTypeEnum.dbChar;
                    }
                case OleDbType.Currency:
                    {
                        return DataTypeEnum.dbDecimal;
                    }
                case OleDbType.Date:
                    {
                        return DataTypeEnum.dbDate;
                    }
                case OleDbType.DBDate:
                    {
                        return DataTypeEnum.dbDate;
                    }
                case OleDbType.DBTime:
                    {
                        return DataTypeEnum.dbTime;
                    }
                case OleDbType.DBTimeStamp:
                    {
                        return DataTypeEnum.dbTimeStamp;
                    }
                case OleDbType.Decimal:
                    {
                        return DataTypeEnum.dbDecimal;
                    }
                case OleDbType.Double:
                    {
                        return DataTypeEnum.dbDouble;
                    }
                case OleDbType.Empty:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.Error:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.Filetime:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.Guid:
                    {
                        return DataTypeEnum.dbGUID;
                    }
                case OleDbType.IDispatch:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.Integer:
                    {
                        return DataTypeEnum.dbInteger;
                    }
                case OleDbType.IUnknown:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.LongVarBinary:
                    {
                        return DataTypeEnum.dbLongBinary;
                    }
                case OleDbType.LongVarChar:
                    {
                        return DataTypeEnum.dbChar;
                    }
                case OleDbType.LongVarWChar:
                    {
                        return DataTypeEnum.dbChar;
                    }
                case OleDbType.Numeric:
                    {
                        return DataTypeEnum.dbNumeric;
                    }
                case OleDbType.PropVariant:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.Single:
                    {
                        return DataTypeEnum.dbSingle;
                    }
                case OleDbType.SmallInt:
                    {
                        return DataTypeEnum.dbByte;
                    }
                case OleDbType.TinyInt:
                    {
                        return DataTypeEnum.dbByte;
                    }
                case OleDbType.UnsignedBigInt:
                    {
                        return DataTypeEnum.dbBigInt;
                    }
                case OleDbType.UnsignedInt:
                    {
                        return DataTypeEnum.dbBigInt;
                    }
                case OleDbType.UnsignedSmallInt:
                    {
                        return DataTypeEnum.dbInteger;
                    }
                case OleDbType.UnsignedTinyInt:
                    {
                        return DataTypeEnum.dbInteger;
                    }
                case OleDbType.VarBinary:
                    {
                        return DataTypeEnum.dbVarBinary;
                    }
                case OleDbType.VarChar:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.Variant:
                    {
                        return DataTypeEnum.dbVarBinary;
                    }
                case OleDbType.VarNumeric:
                    {
                        return DataTypeEnum.dbNumeric;
                    }
                case OleDbType.VarWChar:
                    {
                        return DataTypeEnum.dbText;
                    }
                case OleDbType.WChar:
                    {
                        return DataTypeEnum.dbText;
                    }
            }
            return DataTypeEnum.dbText;
        }

        private DataRow GetCurrentRow()
        {
            DataRow dataRow = null;
            //check if current row exist
            if (!this.EOF && !this.BOF)
            {
                isEOFandBOFchecked = true;
                dataRow = this.CurrentRow;
                isEOFandBOFchecked = false;
            }
            return dataRow;
        }

        private void MoveInternal(MoveDirection md, bool moveSelection = true)
        {
            //APE: before returning EOF or BOF check also if data table containes new rows added using AddNew 
            if (!this.Deleted)
            {
                if (
                    ((this.Rows.Count == 0 && this.DefaultView.Count == 0) && (this.NewRows != null && this.NewRows.Count == 0)) ||
                    (md == MoveDirection.Next && this.EOF) ||
                    (md == MoveDirection.Previous && this.BOF)
                    )
                {
                    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                }
            }

            int index = this.Index;
            DataRow row = null;
            switch (md)
            {
                case MoveDirection.First:
                    {
                        //sets the index to the first row
                        if (this.View == null || this.View.Count == 0)
                        {
                            index = 1;
                        }
                        else
                        {
                            row = this.View[0].Row;
                            index = this.Rows.IndexOf(row) + 1;
                        }
                        //CHE: if no rows in view bof should return true
                        if (this.RecordCount == 0)
                        {
                            this.mintAbsolutePosition = (int)PositionEnum.adPosBOF;
                            index = 0;
                        }
                        break;
                    }
                case MoveDirection.Last:
                    {
                        //CHE: sets the index to the last row, on last added if the case
                        if (this.HasNewRows())
                        {
                            index = this.Rows.Count + this.NewRows.Count;
                        }
                        else
                        {
                            //CHE: when view (filter or sort) should go to last row in view not in table
                            if (this.View == null || this.View.Count == 0)
                            {
                                index = this.Rows.Count;
                            }
                            else
                            {
                                row = this.View[this.View.Count - 1].Row;
                                index = this.Rows.IndexOf(row) + 1;
                            }
                        }

                        if (this.RecordCount == 0)
                        {
                            this.mintAbsolutePosition = (int)PositionEnum.adPosEOF;
                        }
                        break;
                    }
                case MoveDirection.Next:
                    {
                        if (this.Deleted)
                        {
                            if (!this.EOF)
                            {
                                DataRow currentRow = this.CurrentRow;
                                if (currentRow.RowState == DataRowState.Deleted)
                                {
                                    index++;
                                }
                            }
                            this.Deleted = false;
                        }
                        //CHE: when view (filter or sort) should go to next row in view not in table
                        else if (this.View == null || this.View.Count == 0)
                        {
                            //increment the index
                            index++;
                        }
                        else
                        {
                            bool isInView = false;
                            int i;

                            //CHE: if you are on BOF and you MoveNext it should go to first row from view
                            row = this.CurrentRow;
                            if (!this.BOF)
                            {
                                for (i = 0; i < this.View.Count; i++)
                                {
                                    if (this.View[i].Row == row)
                                    {
                                        isInView = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                isInView = true;
                                i = -1;
                            }

                            if (isInView && i < this.View.Count - 1)
                            {
                                DataRow nextRow = this.View[i + 1].Row;
                                index = this.Rows.IndexOf(nextRow) + 1;
                            }
                            else
                            {
                                //SBE: check if RowState is Deleted, and move to next index
                                if (row.RowState == DataRowState.Deleted)
                                {
                                    index++;
                                }
                                else
                                {
                                    index = this.Rows.Count + 1;
                                }
                            }
                        }

                        int totalRows = this.Rows.Count + (this.HasNewRows() ? this.NewRows.Count : 0);
                        if (index > totalRows)
                        {
                            this.mintAbsolutePosition = (int)PositionEnum.adPosEOF;
                        }

                        break;
                    }
                case MoveDirection.Previous:
                    {
                        if (this.Deleted)
                        {
                            if (!this.BOF)
                            {
                                //row was removed, EOF is true or RowState is Deleted
                                if (this.EOF || this.CurrentRow.RowState == DataRowState.Deleted)
                                {
                                    index--;
                                }
                            }
                            this.Deleted = false;
                        }
                        //CHE: when view (filter or sort) should go to previous row in view not in table
                        else if (this.View == null || this.View.Count == 0)
                        {
                            //decrement the index
                            index--;
                        }
                        else
                        {
                            bool isInView = false;
                            int i;
                            //CHE: if you are on EOF and you MovePrevious it should go to last row from view
                            if (!this.EOF)
                            {
                                row = this.CurrentRow;
                                for (i = 0; i < this.View.Count; i++)
                                {
                                    if (this.View[i].Row == row)
                                    {
                                        isInView = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                isInView = true;
                                i = this.View.Count;
                            }
                            if (isInView && i > 0)
                            {
                                DataRow nextRow = this.View[i - 1].Row;
                                index = this.Rows.IndexOf(nextRow) + 1;
                            }
                            else
                            {
                                index = 0;
                            }
                        }

                        if (index == 0)
                        {
                            this.mintAbsolutePosition = (int)PositionEnum.adPosBOF;
                        }
                        break;
                    }
            }

            this.Index = index;

            if (moveSelection)
            {
                MoveSelection();
            }
        }

        #endregion

        private class CommandType
        {
            public string whereClause = "";
            public string tableName = "";
            public string command = "";
        }
    }
}
