﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    /// DAO.Field
    /// </summary>
    public class FCField : IComparable
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCRecordset dataTable;

        #endregion

        #region Constructors

        public FCField(FCRecordset dataTable, DataColumn objColumn)
        {
            this.dataTable = dataTable;
            this.Column = new FCSerializableColumn(objColumn, dataTable);
            this.Name = objColumn.ColumnName;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        /// <summary>
        /// dbAutoIncrField
        /// The field value for new records is automatically incremented to a unique Long integer that can't be changed (in a Microsoft Access workspace, supported only for Microsoft Access database 
        /// engine database tables).
        /// dbDescending
        /// The field is sorted in descending (Z to A or 100 to 0) order; this option applies only to a Field object in a Fields collection of an Index object. If you omit this constant, the field is 
        /// sorted in ascending (A to Z or 0 to 100) order. This is the default value for Index and TableDef fields (Microsoft Access workspaces only)..
        /// dbFixedField
        /// The field size is fixed (default for Numeric fields).
        /// dbHyperlinkField
        /// The field contains hyperlink information (Memo fields only).
        /// dbSystemField
        /// The field stores replication information for replicas; you can't delete this type of field (Microsoft Access workspace only).
        /// dbUpdatableField
        /// The field value can be changed.
        /// dbVariableField
        /// The field size is variable (Text fields only).
        /// </summary>
        public enum FieldAttributeEnum
        {
            dbAutoIncrField = 16,
            dbDescending = 1,
            dbFixedField = 1,
            dbHyperlinkField = 32768,
            dbSystemField = 8192,
            dbUpdatableField = 32,
            dbVariableField = 2
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns or sets the name of the specified object. Read/write String if the object has not been appended to a collection. Read-only String if the object has been appended to a collection.
        /// Syntax
        /// expression .Name
        /// expression A variable that represents a Field object.
        /// Remarks
        /// The maximum length for the name of a Field object is 64 characters.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Sets or returns a value that indicates the operational type or data type of an object. Read/write Integer.
        /// Syntax
        /// expression .Type
        /// expression A variable that represents a Field object.
        /// Remarks
        /// The setting or return value is a constant that indicates an operational or data type. For a Field object, this property is read/write until the object is appended to a collection 
        /// or to another object, after which it's read-only.
        /// For a Field object, the possible settings and return values are described in the following table.
        /// Constant
        /// Description
        /// dbBigInt
        /// Big Integer
        /// dbBinary
        /// Binary
        /// dbBoolean
        /// Boolean
        /// dbByte
        /// Byte
        /// dbChar
        /// Char
        /// dbCurrency
        /// Currency
        /// dbDate
        /// Date/Time
        /// dbDecimal
        /// Decimal
        /// dbDouble
        /// Double
        /// dbFloat
        /// Float
        /// dbGUID
        /// GUID
        /// dbInteger
        /// Integer
        /// dbLong
        /// Long
        /// dbLongBinary
        /// Long Binary (OLE Object)
        /// dbMemo
        /// Memo
        /// dbNumeric
        /// Numeric
        /// dbSingle
        /// Single
        /// dbText
        /// Text
        /// dbTime
        /// Time
        /// dbTimeStamp
        /// Time Stamp
        /// dbVarBinary
        /// VarBinary
        /// When you append a new Field, Parameter, or Property object to the collection of an Index, QueryDef, Recordset, or TableDef object, an error occurs if the underlying database 
        /// doesn't support the data type specified for the new object.
        /// </summary>
        public DataTypeEnum Type
        {
            get;
            set;
        }

        /// <summary>
        /// Sets or returns a value that indicates one or more characteristics of a Field object. Read/write Long.
        /// Syntax
        /// expression .Attributes
        /// expression A variable that represents a Field object.
        /// Remarks
        /// The value specifies characteristics of the field represented by the Field object and can be a combination of these constants.
        /// Constant
        /// Description
        /// dbAutoIncrField
        /// The field value for new records is automatically incremented to a unique Long integer that can't be changed (in a Microsoft Access workspace, supported only for Microsoft Access database engine database tables).
        /// dbDescending
        /// The field is sorted in descending (Z to A or 100 to 0) order; this option applies only to a Field object in a Fields collection of an Index object. If you omit this constant, the field is sorted in ascending (A to Z or 0 to 100) order. This is the default value for Index and TableDef fields (Microsoft Access workspaces only)..
        /// dbFixedField
        /// The field size is fixed (default for Numeric fields).
        /// dbHyperlinkField
        /// The field contains hyperlink information (Memo fields only).
        /// dbSystemField
        /// The field stores replication information for replicas; you can't delete this type of field (Microsoft Access workspace only).
        /// dbUpdatableField
        /// The field value can be changed.
        /// dbVariableField
        /// The field size is variable (Text fields only).
        /// For an object not yet appended to a collection, this property is read/write. For an appended Field object, the availability of the Attributes property depends on the object that contains the Fields collection.
        /// If the Field object belongs to an
        /// Then Attributes is
        /// Index object
        /// Read/write until the TableDef object that the Index object is appended to is appended to a Database object; then the property is read-only.
        /// QueryDef object
        /// Read-only
        /// Recordset object
        /// Read-only
        /// Relation object
        /// Not supported
        /// TableDef object
        /// Read/write
        /// When you set multiple attributes, you can combine them by summing the appropriate constants. Any invalid values are ignored without producing an error.
        /// </summary>
        public int Attributes
        {
            get;
            set;
        }

        /// <summary>
        /// Sets or returns a value that indicates the maximum size, in bytes, of a Field object.
        /// Syntax
        /// expression .Size
        /// expression A variable that represents a Field object.
        /// Remarks
        /// For an object not yet appended to the Fields collection, this property is read/write.
        /// For fields (other than Memo type fields) that contain character data, the Size property indicates the maximum number of characters that the field can hold. For numeric fields, 
        /// the Size property indicates how many bytes of storage are required.
        /// Use of the Size property depends on the object that contains the Fields collection to which the Field object is appended, as shown in the following table.
        /// Object appended to
        /// Usage
        /// Index
        /// Not supported
        /// QueryDef
        /// Read-only
        /// Recordset
        /// Read-only
        /// Relation
        /// Not supported
        /// TableDef
        /// Read-only
        /// When you create a Field object with a data type other than Text, the Type property setting automatically determines the Size property setting; you don't need to set it. 
        /// For a Field object with the Text data type, however, you can set Size to any integer up to the maximum text size (255 for Microsoft Access databases). If you do not set the size, the field 
        /// will be as large as the database allows.
        /// For Long Binary and Memo Field objects, Size is always set to 0. Use the FieldSize property of the Field object to determine the size of the data in a specific record. The maximum size of 
        /// a Long Binary or Memo field is limited only by your system resources or the maximum size that the database allows.
        /// </summary>
        public int Size
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the number of bytes used in the database (rather than in memory) of a Memo or Long Binary Field object in the Fields collection of a Recordset object.
        /// Syntax
        /// expression .FieldSize
        /// expression A variable that represents a Field object.
        /// Remarks
        /// You can use FieldSize with the AppendChunk and GetChunk methods to manipulate large fields.
        /// Because the size of a Long Binary or Memo field can exceed 64K, you should assign the value returned by FieldSize to a variable large enough to store a Long variable.
        /// To determine the size of a Field object other than Memo and Long Binary types, use the Size property.
        /// If the database server or ODBC driver does not support server-side cursors.
        /// If you are using the ODBC cursor library (that is, the DefaultCursorDriver property is set to dbUseODBC, or to dbUseDefault when the server does not support server-side cursors).
        /// If you are using a cursorless query (that is, the DefaultCursorDriver property is set to dbUseNoCursor).
        /// The FieldSize property and the VBA Len() or LenB() functions may return different values as the length of the same string. Strings are stored in a Microsoft Access database in 
        /// multi-byte character set (MBCS) form, but exposed through VBA in Unicode format. As a result, the Len() function will always return the number of characters, LenB will always return 
        /// the number of characters X 2 (Unicode uses two bytes for each character), but FieldSize will return some value in between if the string has any MBCS characters. For example, given a string 
        /// consisting of three normal characters and two MBCS characters, Len() will return 5, LenB() will return 10, and FieldSize will return 7, the sum of 1 for each normal character and 2 for each 
        /// MBCS character.
        /// </summary>
        public int FieldSize
        {
            get;
            set;
        }

        /// <summary>
        /// Sets or returns a value that indicates whether a zero-length string ("") is a valid setting for the Value property of the Field object with a Text or Memo data type 
        /// (Microsoft Access workspaces only).
        /// Syntax
        /// expression .AllowZeroLength
        /// expression A variable that represents a Field object.
        /// Remarks
        /// For an object not yet appended to the Fields collection, this property is read/write.
        /// Once appended to a Fields collection, the availability of the AllowZeroLength property depends on the object that contains the Fields collection, as shown in the following table.
        /// If the Fields collection belongs to an
        /// Then AllowZeroLength is
        /// Index object
        /// Not supported
        /// QueryDef object
        /// Read-only
        /// Recordset object
        /// Read-only
        /// Relation object
        /// Not supported
        /// TableDef object
        /// Read/write
        /// You can use this property along with the Required, ValidateOnSet, or ValidationRule property to validate a value in a field.
        /// </summary>
        public bool AllowZeroLength
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Sets or returns a value that indicates whether a Field object requires a non-Null value.
        /// Syntax
        /// expression .Required
        /// expression A variable that represents a Field object.
        /// Remarks
        /// For a Field not yet appended to the Fields collection, this property is read/write.
        /// The availability of the Required property depends on the object that contains the Fields collection, as shown in the following table.
        /// If the Fields collection belongs to a
        /// Then Required is
        /// Index object
        /// Not supported
        /// QueryDef object
        /// Read-only
        /// Recordset object
        /// Read-only
        /// Relation object
        /// Not supported
        /// TableDef object
        /// Read/write
        /// You can use the Required property along with the AllowZeroLength, ValidateOnSet, or ValidationRule property to determine the validity of the Value property setting for that Field object. 
        /// If the Required property is set to False, the field can contain null values as well as values that meet the conditions specified by the AllowZeroLength and ValidationRule property settings.
        /// Note
        /// When you can set this property for either an Index object or a Field object, set it for the Field object. The validity of the property setting for a Field object is checked before that of an Index object.
        /// </summary>
        public bool Required
        {
            get;
            set;
        }

        /// <summary>
        /// Sets or returns the default value of a Field object. For a Field object not yet appended to the Fields collection, this property is read/write (Microsoft Access workspaces only).
        /// Syntax
        /// expression .DefaultValue
        /// expression A variable that represents a Field object.
        /// Remarks
        /// The setting or return value is a String data type that can contain a maximum of 255 characters. It can be either text or an expression. If the property setting is an expression, 
        /// it can't contain user-defined functions, Microsoft Access database engine SQL aggregate functions, or references to queries, forms, or other Field objects.
        /// Note
        /// You can also set the DefaultValue property of a Field object on a TableDef object to a special value called "GenUniqueID( )". This causes a random number to be assigned to this 
        /// field whenever a new record is added or created, thereby giving each record a unique identifier. The field's Type property must be Long.
        /// The availability of the DefaultValue property depends on the object that contains the Fields collection, as shown in the following table.
        /// If the Fields collection belongs to an
        /// Then DefaultValue is
        /// Index object
        /// Not supported
        /// QueryDef object
        /// Read-only
        /// Recordset object
        /// Read-only
        /// Relation object
        /// Not supported
        /// TableDef object
        /// Read/write
        /// When a new record is created, the DefaultValue property setting is automatically entered as the value for the field. You can change the field value by setting its Value property.
        /// The DefaultValue property doesn't apply to AutoNumber and Long Binary fields.
        /// </summary>
        public string DefaultValue
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Syntax
        /// expression .OriginalValue
        /// expression A variable that represents a Field object.
        /// Remarks
        /// During an optimistic batch update, a collision may occur where a second client modifies the same field and record in between the time the first client retrieves the data and the 
        /// first client's update attempt. The OriginalValue property contains the value of the field at the time the last batch Update began. If this value does not match the value actually in the 
        /// database when the batch Update attempts to write to the database, a collision occurs. When this happens, the new value in the database will be accessible through the VisibleValue property.
        /// </summary>
        public object OriginalValue
        {
            get;
            set;
        }

        /// <summary>
        /// Sets or returns the value of an object. Read/write Variant.
        /// Syntax
        /// expression .Value
        /// expression A variable that represents a Field object.
        /// Remarks
        /// The setting or return value is a Variant data type that evaluates to a value appropriate for the data type, as specified by the Type property of an object.
        /// Generally, the Value property is used to retrieve and alter data in Recordset objects.
        /// The Value property is the default property of the Field, Parameter, and Property objects. Therefore, you can set or return the value of one of these objects by referring to them directly 
        /// instead of specifying the Value property.
        /// Trying to set or return the Value property in an inappropriate context (for example, the Value property of a Field object in the Fields collection of a TableDef object) will cause a trappable error.
        /// Note
        /// When reading decimal values from a Microsoft SQL Server database, they will be formatted using scientific notation through a Microsoft Access workspace, but will appear as normal decimal values 
        /// through an ODBCDirect workspace.
        /// </summary>
        public object Value
        {
            get
            {
                //SBE: if RowState is Deleted Exception is thrown
                Column.Table.IsEOFandBOFchecked = true;
                if (Column == null || Column.Table.BOF || Column.Table.EOF || Column.Table.Status == DataRowState.Deleted)
                {
                    Column.Table.IsEOFandBOFchecked = false;
                    throw new Exception("Either BOF or EOF is True, or the current record has been deleted");
                }
                object value = Column.Table.CurrentRow[Column.ColumnName];
                Column.Table.IsEOFandBOFchecked = false;
                return value;
            }
            set
            {
                FCRecordset dt = Column.Table;

                if (Column == null || dt.EOF || Column.ReadOnly) return;
                if (this.Type == DataTypeEnum.dbBoolean && value != null)
                {
                    dt.CurrentRow[Column.ColumnName] = FCGlobal.BoolFromString(value.ToString());
                }
                else
                {
                    DataRow dr = dt.CurrentRow;

                    //if (value == "")
                    //{
                    //    switch (this.Type)
                    //    {
                    //        case Convert.ToInt32(SqlDbType.Char:
                    //        case Convert.ToInt32(SqlDbType.VarChar:
                    //        case Convert.ToInt32(SqlDbType.NChar:
                    //        case Convert.ToInt32(SqlDbType.NVarChar:
                    //            {
                    //                value = new string(' ', this.DefinedSize); 
                    //                break;
                    //            }
                    //    }
                    //}

                    //CNA: field does not accepts null, use DBNull instead
                    //dr[Column.ColumnName] = value;
                    //dr[Column.ColumnName] = value != null ? value : DBNull.Value;
                    // JSP: If Value is null then set Value = dbnull
                    if (value == null)
                    {
                        dr[Column.ColumnName] = DBNull.Value;
                    }
                    else
                    {
                        //CHE: if field is NChar (adWChar) when setting value fill with empty spaces if DefindeSize is set
                        //if (value != null && DefinedSize > 0 && this.Type == Convert.ToInt32(SqlDbType.NChar && value.ToString().Length < DefinedSize)
                        //{
                        //    dr[Column.ColumnName] = value + new string(' ', DefinedSize - value.ToString().Length);
                        //}
                        //else 
                        if (dt.Columns[Column.ColumnName].DataType == typeof(DateTime))
                        {
                            string cellValue = FCConvert.ToString(value);
                            if (string.IsNullOrEmpty(cellValue))
                            {
                                dr[Column.ColumnName] = DBNull.Value;
                            }
                            else
                            {
                                dr[Column.ColumnName] = DateTime.Parse(cellValue);
                            }
                        }
                        else
                        {
                            //CHE: throw exception when setting a string field with some value larger than its size
                            if (dt.Columns[Column.ColumnName].DataType == typeof(String) && this.Size > 0 && value.ToString().Length > this.Size)
                            {
                                throw new Exception("The field is too small to accept the amount of data you attempted to add");
                            }
                            dr[Column.ColumnName] = value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// property neeeded to avoid exception when comparing Fields(..).Value with some value such as integer or boolean (cannot convert DBNull to...)
        /// </summary>
        public object ValueNullSafe
        {
            get
            {
                if (this.Value != DBNull.Value)
                {
                    return this.Value;
                }

                switch (this.Type)
                {
                    case DataTypeEnum.dbDate:
                    case DataTypeEnum.dbTime:
                    case DataTypeEnum.dbTimeStamp:
                        {
                            return DateTime.FromOADate(0);
                        }
                    case DataTypeEnum.dbByte:
                    case DataTypeEnum.dbCurrency:
                    case DataTypeEnum.dbInteger:
                    case DataTypeEnum.dbComplexInteger:
                    case DataTypeEnum.dbBigInt:
                    case DataTypeEnum.dbLong:
                    case DataTypeEnum.dbSingle:
                    case DataTypeEnum.dbDecimal:
                    case DataTypeEnum.dbFloat:
                    case DataTypeEnum.dbDouble:
                    case DataTypeEnum.dbComplexDecimal:
                    case DataTypeEnum.dbComplexDouble:
                    case DataTypeEnum.dbComplexSingle:
                    case DataTypeEnum.dbNumeric:
                        {
                            return 0;
                        }
                    case DataTypeEnum.dbBoolean:
                        {
                            return false;
                        }
                    case DataTypeEnum.dbChar:
                    case DataTypeEnum.dbComplexText:
                    case DataTypeEnum.dbGUID:
                    case DataTypeEnum.dbMemo:
                    case DataTypeEnum.dbText:
                        return "";
                    case DataTypeEnum.dbComplexByte:
                    case DataTypeEnum.dbLongBinary:
                    case DataTypeEnum.dbComplexGUID:
                    case DataTypeEnum.dbComplexLong:
                    case DataTypeEnum.dbBinary:
                    case DataTypeEnum.dbVarBinary:
                    case DataTypeEnum.dbAttachment:
                    default:
                        return this.Value;
                }
            }
        }
        #endregion

        #region Internal Properties

        internal string BaseTableName
        {
            get;
            set;
        }

        internal string BaseColumnName
        {
            get;
            set;
        }

        internal bool IsAliased
        {
            get;
            set;
        }

        internal FCSerializableColumn Column
        { 
            get; 
            set; 
        }

        internal string ColumnName
        {
            get
            {
                // return the column name of the column object

                //return basecolumnname and not the automatically generated name
                //e.g. for 2 tables in join having same column NR, C# will generate NR and NR1, but in VB6 both columns have ColumnName NR
                if (!string.IsNullOrEmpty(BaseColumnName) && !IsAliased)
                {
                    return BaseColumnName;
                }
                return Column.ColumnName;
            }
            set
            {
                // set the value of the column name
                Column.ColumnName = value;

            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// implement IComparable CompareTo to provide default sort order
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            FCField c = (FCField)obj;

            if (this.Value == DBNull.Value)
            {
                if (c.Value == DBNull.Value)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }

            if (c.Value == DBNull.Value)
            {
                return 1;
            }
            else
            {
                switch (this.Type)
                {
                    case DataTypeEnum.dbBigInt:
                    case DataTypeEnum.dbBinary:
                    case DataTypeEnum.dbInteger:
                    case DataTypeEnum.dbByte:
                        {
                            long a = Convert.ToInt64(this.Value);
                            long b = Convert.ToInt64(c.Value);
                            return a.CompareTo(b);
                        }
                    case DataTypeEnum.dbChar:
                    case DataTypeEnum.dbMemo:
                    case DataTypeEnum.dbText:
                        {
                            string a = FCConvert.ToString(this.Value);
                            string b = FCConvert.ToString(c.Value);
                            return String.Compare(a, b);
                        }
                    case DataTypeEnum.dbDate:
                    case DataTypeEnum.dbTime:
                    case DataTypeEnum.dbTimeStamp:
                        {
                            DateTime a = Convert.ToDateTime(this.Value);
                            DateTime b = Convert.ToDateTime(c.Value);
                            return a.CompareTo(b);
                        }
                    case DataTypeEnum.dbDecimal:
                    case DataTypeEnum.dbFloat:
                    case DataTypeEnum.dbSingle:
                    case DataTypeEnum.dbDouble:
                        {
                            Double a = Convert.ToDouble(this.Value);
                            Double b = Convert.ToDouble(c.Value);
                            return a.CompareTo(b);
                        }
                }
            }

            return 0;
        }

        /// <summary>
        /// User-defined operator -
        /// </summary>
        /// <param name="field1"></param>
        /// <param name="field2"></param>
        /// <returns></returns>
        public static Double operator -(FCField field1, FCField field2)
        {
            return (Convert.IsDBNull(field1.Value) ? 0 : Convert.ToDouble(field1.Value)) - (Convert.IsDBNull(field2.Value) ? 0 : Convert.ToDouble(field2.Value));
        }

        /// <summary>
        /// User-defined operator +
        /// + operator retuns an object, Conversion is required if the result is assigned to a known type
        /// In VB6 the operation is evaluated, and the result is converted to the required type
        /// "0001" + "0002" will return "00010002" if result is assigned to a String or 10002 if result is assigned to a number
        /// "0001" + 2 will return "3" if result is assigned to a String or 3 if result is assigned to a number
        /// 1 + "0002" will return "3" if result is assigned to a String or 3 if result is assigned to a number
        /// 1 + 2 will return "3" if result is assigned to a String or 3 if result is assigned to a number
        /// </summary>
        /// <param name="field1"></param>
        /// <param name="field2"></param>
        /// <returns></returns>
        public static object operator +(FCField field1, FCField field2)
        {
            //SBE: if booth operands are one of the string SqlDbType(Char/NChar/VarChar/NVarChar/Text/NText), operator will return string concatenation
            if ((field1.Type == DataTypeEnum.dbChar || field1.Type == DataTypeEnum.dbText
                || field1.Type == DataTypeEnum.dbMemo)
                && (field2.Type == DataTypeEnum.dbChar || field2.Type == DataTypeEnum.dbText
                || field2.Type == DataTypeEnum.dbMemo))
            {
                return FCConvert.ToString(field1.Value) + FCConvert.ToString(field2.Value);
            }
            else
            {
                return (Convert.IsDBNull(field1.Value) ? 0 : Convert.ToDouble(field1.Value)) + (Convert.IsDBNull(field2.Value) ? 0 : Convert.ToDouble(field2.Value));
            }
        }

        /// <summary>
        /// User-defined conversion from FCField to double
        /// </summary>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static implicit operator double(FCField FCField)
        {
            if (Convert.IsDBNull(FCField.Value)) return 0;
            return Convert.ToDouble(FCField.Value);
        }

        /// <summary>
        /// User-defined conversion from FCField to decimal
        /// </summary>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static implicit operator decimal(FCField FCField)
        {
            if (Convert.IsDBNull(FCField.Value)) return 0;
            return Convert.ToDecimal(FCField.Value);
        }

        /// <summary>
        /// User-defined conversion from FCField to int
        /// </summary>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static implicit operator int(FCField FCField)
        {
            if (Convert.IsDBNull(FCField.Value)) return 0;
            return Convert.ToInt32(FCField.Value);
        }

        /// <summary>
        /// User-defined conversion from FCField to short
        /// </summary>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static implicit operator short(FCField FCField)
        {
            if (Convert.IsDBNull(FCField.Value)) return 0;
            return Convert.ToInt16(FCField.Value);
        }

        /// <summary>
        /// User-defined conversion from FCField to DateTime
        /// </summary>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static implicit operator DateTime(FCField FCField)
        {
            if (Convert.IsDBNull(FCField.Value)) return DateTime.Parse("30/12/1899 00:00:00");
            return Convert.ToDateTime(FCField.Value);
        }

        /// <summary>
        /// User-defined conversion from FCField to string
        /// </summary>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static implicit operator string(FCField FCField)
        {
            if (Convert.IsDBNull(FCField.Value)) return "";
            return FCConvert.ToString(FCField.Value);
        }

        /// <summary>
        /// User-defined conversion from FCField to bool
        /// </summary>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static implicit operator bool(FCField FCField)
        {
            if (Convert.IsDBNull(FCField.Value)) return false;
            return Convert.ToBoolean(FCField.Value);
        }

        /// <summary>
        /// overload operator < 
        /// </summary>
        /// <param name="FCField"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator <(FCField FCField, int value)
        {
            return GetFieldValueAsDouble(FCField) < value;
        }

        /// <summary>
        /// overload operator > 
        /// </summary>
        /// <param name="FCField"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator >(FCField FCField, int value)
        {
            return GetFieldValueAsDouble(FCField) > value;
        }

        /// <summary>
        /// overload operator /
        /// </summary>
        /// <param name="FCField"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object operator /(FCField FCField, int value)
        {
            return GetFieldValueAsDouble(FCField) / value;
        }

        /// <summary>
        /// overload operator == 
        /// </summary>
        /// <param name="FCField"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator ==(FCField FCField, int value)
        {
            return GetFieldValueAsDouble(FCField) == value;
        }

        /// <summary>
        /// overload operator != 
        /// </summary>
        /// <param name="FCField"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator !=(FCField FCField, int value)
        {
            return GetFieldValueAsDouble(FCField) != value;
        }

        /// <summary>
        /// overload operator == 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static bool operator ==(int value, FCField FCField)
        {
            return GetFieldValueAsDouble(FCField) == value;
        }

        /// <summary>
        /// overload operator != 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="FCField"></param>
        /// <returns></returns>
        public static bool operator !=(int value, FCField FCField)
        {
            return GetFieldValueAsDouble(FCField) != value;
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private static double GetFieldValueAsDouble(FCField field)
        {
            return Convert.IsDBNull(field.Value) ? 0 : Convert.ToDouble(field.Value);
        }

        #endregion


    }
}
