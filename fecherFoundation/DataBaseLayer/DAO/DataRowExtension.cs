﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    //BAN: A DataRow from a DataTable which has cloned DataTables will have the 
    //corresponding row in the cloned tables on the PairRow list. This list is used for referencing
    //in the cases of new row addition in the DataTable, value changin, or deleting
    //When changing a value in the DataRow, the ColumnChanged event will be triggered for it`s owning
    //DataTable, in which the paired rows will be updated with the new value.
    //When calling an extension method from the DataTable which alters the DataTable row collection or DefaultView
    //a sync method will be called from the respective method which, using the PairRow list, will update the rows accordingly
    public static class DataRowExtension
    {
        internal static Dictionary<DataRow, DataRowDefinitions> mobjDefinitions = new Dictionary<DataRow, DataRowDefinitions>();

        public static List<DataRow> PairRow(this DataRow objDataRow)
        {
            DataRowDefinitions mobjDataRowDefinitions = DataRowDefinitions.GetDataRowDefinitions(objDataRow);
            return mobjDataRowDefinitions.PairRow;
        }

        public static void PairRow(this DataRow objDataRow, DataRow pairRow)
        {
            DataRowDefinitions mobjDataRowDefinitions = DataRowDefinitions.GetDataRowDefinitions(objDataRow);
            if (!mobjDataRowDefinitions.PairRow.Contains(pairRow))
            {
                mobjDataRowDefinitions.PairRow.Add(pairRow);
            }
            //The clone row was added to the source row, the source row now must be added to the clone row for data sync possibilities
            DataRowDefinitions mobjPairDataRowDefinitions = DataRowDefinitions.GetDataRowDefinitions(pairRow);
            if (!mobjPairDataRowDefinitions.PairRow.Contains(objDataRow))
            {
                mobjPairDataRowDefinitions.PairRow.Add(objDataRow);
            }
        }
    }

    public class DataRowDefinitions
    {
        #region Members
        private DataRow mobjDataRow = null;
        private DataRow mobjPairRow = null;
        private DataRowState rowState = DataRowState.Unchanged;
        #endregion

        #region Properties

        public List<DataRow> PairRow;

        #endregion

        #region Constructor
        internal DataRowDefinitions(DataRow objDataRow)
        {
            mobjDataRow = objDataRow;
            PairRow = new List<DataRow>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get the instance of the definition class
        /// </summary>
        /// <param name="objCheckBox"></param>
        /// <returns></returns>
        public static DataRowDefinitions GetDataRowDefinitions(DataRow objDataRow)
        {
            DataRowDefinitions objDataRowDefinitions = null;
            if (DataRowExtension.mobjDefinitions.ContainsKey(objDataRow))
            {
                objDataRowDefinitions = DataRowExtension.mobjDefinitions[objDataRow];
            }
            else
            {
                objDataRowDefinitions = new DataRowDefinitions(objDataRow);
                DataRowExtension.mobjDefinitions.Add(objDataRow, objDataRowDefinitions);
            }
            return objDataRowDefinitions;
        }
        #endregion
    }
}
