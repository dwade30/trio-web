﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    /// DAO.TableDefs
    /// </summary>
    public class FCTableDefs : IEnumerable<FCTableDef>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCDatabase database;
        private List<FCTableDef> tableDefs;

        #endregion

        #region Constructors

        public FCTableDefs(FCDatabase database)
        {
            this.database = database;
            this.tableDefs = new List<FCTableDef>();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Get's Table-Name
        /// </summary>
        /// <param name="tableName"></param>
        public FCTableDef this[string tableName] {
            get {
                return this.GetTableDef(tableName);
            }
        }

        /// <summary>
        /// Returns the number of objects in the specified collection. Read-only.
        /// </summary>
        public int Count
        {
            get 
            { 
                return (tableDefs == null) ? 0 : tableDefs.Count; 
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Adds a new TableDef to the TableDefs collection.
        /// </summary>
        /// <param name="tableDef"></param>
        public void Append(FCTableDef tableDef)
        {
            tableDefs.Add(tableDef);
        }

        /// <summary>
        /// Deletes the specified TableDef object from the TableDefs collection.
        /// </summary>
        /// <param name="tableDefName"></param>
        public void Delete(string tableDefName)
        {
            tableDefs.Remove(GetTableDef(tableDefName));
        }

        /// <summary>
        /// implement simple iteration
        /// </summary>
        /// <returns></returns>
        public IEnumerator<FCTableDef> GetEnumerator()
        {
            return tableDefs.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Internal Methods

        internal void AddTableDef(FCTableDef tableDef)
        {
            tableDefs.Add(tableDef);
        }

        internal void RemoveTableDef(FCTableDef tableDef)
        {
            tableDefs.Remove(tableDef);
        }

        internal FCTableDef GetTableDef(int Index)
        {
            return tableDefs[Index];
        }

        internal FCTableDef GetTableDef(string tableDefName)
        {
            return tableDefs.FindLast(delegate(FCTableDef target)
            {
                return target.Name == tableDefName;
            });
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
