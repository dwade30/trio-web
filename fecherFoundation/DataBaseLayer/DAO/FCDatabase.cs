﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.IO;

namespace fecherFoundation.DataBaseLayer
{
    /// <summary>
    /// DAO.Database
    /// https://msdn.microsoft.com/en-us/library/ee292001(v=office.12).aspx
    /// </summary>
    public class FCDatabase : DataSet
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCDBEngine dbEngine;
        private FCRecordsets recordsets;
        private FCTableDefs tableDefs;
        private OleDbConnection connection;

        //CHE: save password and database full path
        private string openDatabaseName = "";
        private object openDatabaseOptions = null;
        private object openDatabaseReadOnly = null;
        private string openDatabaseConnect = "";
        private string openDatabasePassword = "";

        // CHE Transactions

        private OleDbTransaction trans;

        #endregion

        #region Constructors

        public FCDatabase(FCDBEngine dbEngine, string name, object options, object readOnly, string connect)
        {
            this.dbEngine = dbEngine;
            this.connection = new System.Data.OleDb.OleDbConnection(dbEngine.GetConnectionString(name, connect));
            
            //CHE: save password and database full path
            int position = connect.IndexOf("PWD=");
            if (position > 0)
            {
                this.openDatabasePassword = connect.Substring(position + 4);
            }
            this.openDatabaseName = name;
            this.openDatabaseOptions = options;
            this.openDatabaseReadOnly = readOnly;
            this.openDatabaseConnect = connect;

            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        /// <summary>
        /// DAO.RecordsetOptionEnum
        /// </summary>
        public enum RecordsetOptionSettings
        {
            dbAppendOnly = 8,
            dbConsistent = 32,
            dbDenyRead = 2,
            dbDenyWrite = 1,
            dbExecDirect = 2048,
            dbFailOnError = 128,
            dbForwardOnly = 256,
            dbInconsistent = 16,
            dbReadOnly = 4,
            dbRunAsync = 1024,
            dbSeeChanges = 512,
            dbSQLPassThrough = 64
        }

        /// <summary>
        /// DAO.RecordsetTypeEnum
        /// </summary>
        public enum RecordsetTypeSettings
        {
            dbOpenDynamic = 16,
            dbOpenDynaset = 2,
            dbOpenForwardOnly = 8,
            dbOpenSnapshot = 4,
            dbOpenTable = 1
        }
        
        /// <summary>
        /// DAO.DatabaseTypeEnum
        /// </summary>
        public enum DatabaseTypeSettings
        {
            dbDecrypt = 4,
            dbEncrypt = 2,
            dbVersion10 = 1,
            dbVersion11 = 8,
            dbVersion20 = 16,
            dbVersion30 = 32,
            dbVersion40 = 64
        }

        /// <summary>
        /// DAO.CollatingOrderEnum
        /// </summary>
        public enum CollatingOrderSettings
        {
            dbSortArabic = 1025,
            dbSortChineseSimplified = 2052,
            dbSortChineseTraditional = 1028,
            dbSortCyrillic = 1049,
            dbSortCzech = 1029,
            dbSortDutch = 1043,
            dbSortGeneral = 1033,
            dbSortGreek = 1032,
            dbSortHebrew = 1037,
            dbSortHungarian = 1038,
            dbSortIcelandic = 1039,
            dbSortJapanese = 1041,
            dbSortKorean = 1042,
            dbSortNeutral = 1024,
            dbSortNorwdan = 1030,
            dbSortPDXIntl = 1033,
            dbSortPDXNor = 1030,
            dbSortPDXSwe = 1053,
            dbSortPolish = 1045,
            dbSortSlovenian = 1060,
            dbSortSpanish = 1034,
            dbSortSwedFin = 1053,
            dbSortThai = 1054,
            dbSortTurkish = 1055,
            dbSortUndefined = -1
        }

        /// <summary>
        /// DAO.TableDefAttributeEnum
        /// </summary>
        public enum TableDefAttributeSettings
        {
            dbAttachedODBC = 536870912,
            dbAttachedTable = 1073741824,
            dbAttachExclusive = 65536,
            dbAttachSavePWD = 131072,
            dbHiddenObject = 1,
            dbSystemObject = -2147483646
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Connect
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int QueryTimeout
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns the name of the specified object. Read-only String.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Returns a TableDefs collection that contains all of the TableDef objects stored in the specified database. Read-only.
        /// Syntax
        /// expression .TableDefs
        /// expression A variable that represents a Database object.
        /// </summary>
        public FCTableDefs TableDefs
        {
            get
            {
                FillTableDefs();
                return tableDefs;
            }
            set
            {
                tableDefs = value;
            }
        }


        /// <summary>
        /// Returns a Recordsets collection that contains all of the open recordsets in the for the specified database. Read-only.
        /// Syntax
        /// expression .Recordsets
        /// expression A variable that represents a Database object.
        /// </summary>
        public FCRecordsets Recordsets
        {
            get
            {
                if (recordsets == null)
                {
                    recordsets = new FCRecordsets(this);
                }
                return recordsets;
            }
            set
            {
                recordsets = value;
            }
        }

        /// <summary>
        /// Syntax
        /// expression .Connection
        /// expression A variable that represents a Database object.
        /// Remarks
        /// Use the Connection property to obtain a reference to a Connection object that corresponds to the Database. In DAO, a Connection object and its corresponding Database 
        /// object are simply two different object variable references to the same object. The Database property of a Connection object and the Connection property of a Database object make 
        /// it easier to change connections to an ODBC data source through the Microsoft Access database engine to use ODBCDirect.
        /// </summary>
        public OleDbConnection Connection
        {
            get
            {
                return connection;
            }
            set
            {
                connection = value;
            }
        }
        
        //che:transaction
        public OleDbTransaction Trans
        {
            get
            {
                return trans;
            }

            set
            {
                trans = value;
            }
        }


        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        //che:transactions
        public void BeginTrans()
        {
            trans = this.connection.BeginTransaction();
        }

        public void CommitTrans()
        {
            if (trans != null)
            {
                trans.Commit();
            }
        }

        public void RollbackTrans()
        {
            if (trans != null)
            {
                trans.Rollback();
            }
        }



        /// <summary>
        /// execute scalar
        /// </summary>
        /// <param name="query"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public object ExecuteScalar(string query, object options = null)
        {
            OleDbCommand command = new OleDbCommand(query, connection);
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            //che:transaction
            command.Transaction = trans;

            return command.ExecuteScalar();
        }

        /// <summary>
        /// execute reader
        /// </summary>
        /// <param name="query"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public OleDbDataReader ExecuteReader(string query, object options = null)
        {
            OleDbCommand command = new OleDbCommand(query, connection);
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            //che:transaction
            command.Transaction = trans;

            return command.ExecuteReader();
        }

        /// <summary>
        /// Creates a new Recordset object and appends it to the Recordsets collection.
        /// expression .OpenRecordset(Name, Type, Options, LockEdit)
        /// Name - String - The source of the records for the new Recordset. The source can be a table name, a query name, or an SQL statement that returns records. 
        /// For table-type Recordset objects in Microsoft Access database engine databases, the source can only be a table name.
        /// Type - Optional Variant - A RecordsetTypeEnum constant that indicates the type of Recordset to open.
        /// Note
        /// If you open a Recordset in a Microsoft Access workspace and you don't specify a type, OpenRecordset creates a table-type Recordset, if possible. If you specify a linked table or query, 
        /// OpenRecordset creates a dynaset-type Recordset.
        /// Options - Optional Variant - A combination of RecordsetOptionEnum constants that specify characteristics of the new Recordset.
        /// Note
        /// The constants dbConsistent and dbInconsistent are mutually exclusive, and using both causes an error. Supplying a LockEdit argument 
        /// when Options uses the dbReadOnly constant also causes an error.
        /// LockEdit - Optional Variant - A LockTypeEnum constant that determines the locking for the Recordset.
        /// Note
        /// You can use dbReadOnly in either the Options argument or the LockedEdit argument, but not both. If you use it for both arguments, a run-time error occurs.
        /// Remarks
        /// Typically, if the user gets this error while updating a record, your code should refresh the contents of the fields and retrieve the newly modified values. 
        /// If the error occurs while deleting a record, your code could display the new record data to the user and a message indicating that the data has recently changed. 
        /// At this point, your code can request a confirmation that the user still wants to delete the record.
        /// You should also use the dbSeeChanges constant if you open a Recordset in a Microsoft Access database engine-connected ODBC workspace against a Microsoft SQL Server 6.0 
        /// (or later) table that has an IDENTITY column, otherwise an error may result.
        /// Opening more than one Recordset on an ODBC data source may fail because the connection is busy with a prior OpenRecordset call. One way around this is to fully populate the 
        /// Recordset by using the MoveLast method as soon as the Recordset is opened.
        /// Closing a Recordset with the Close method automatically deletes it from the Recordsets collection.
        /// Note
        /// If source refers to an SQL statement composed of a string concatenated with a non-integer value, and the system parameters specify a non-U.S. decimal character 
        /// such as a comma (for example, strSQL = "PRICE > " & lngPrice, and lngPrice = 125,50), an error occurs when you try to open the Recordset. This is because during concatenation, the number 
        /// will be converted to a string using your system's default decimal character, and SQL only accepts U.S. decimal characters.
        /// </summary>
        /// <returns></returns>
        public FCRecordset OpenRecordset(string name, object type = null, RecordsetOptionSettings options = RecordsetOptionSettings.dbFailOnError, object LockEdit = null)
        {
            FCRecordset recordset = new FCRecordset(this);
            OleDbDataAdapter adapter;

            //The source can be a table name, a query name, or an SQL statement that returns records 
            if (name.ToUpper().StartsWith("SELECT "))
            {
                //CHE: use % in wildcard instead of * used in VB6
                Regex regex = new Regex(@"(.* )(like '.*')(.*)", RegexOptions.IgnoreCase);
                foreach (Match m in regex.Matches(name))
                {
                    string condition = m.Groups[2].Value;
                    if (condition.Contains('*'))
                    {
                        condition = condition.Replace('*', '%');
                        name = m.Groups[1].Value + condition + m.Groups[3].Value;
                    }
                }

                adapter = new OleDbDataAdapter(name, connection);

                //CHE: set TableName necessary in UpdateBatchBuilder; identify first "from" in string to set correct TableName
                string SourceLowercase = name.ToLower();

                System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(name, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*) where", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (match.Success && match.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                {
                    recordset.TableName = match.Groups[1].Value;
                }
                else
                {
                    System.Text.RegularExpressions.Match match1 = System.Text.RegularExpressions.Regex.Match(name, "from ([a-zA-Z0-9üöäÄÜÖß_\\.]*) order by", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (match1.Success && match1.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                    {
                        recordset.TableName = match1.Groups[1].Value;
                    }
                    else
                    {
                        //CHE: treat alias
                        System.Text.RegularExpressions.Match match2 = System.Text.RegularExpressions.Regex.Match(name, "from ([a-zA-Z0-9üöäÄÜÖß_\\. ]*) where", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        if (match2.Success && match2.Groups[1].Index == SourceLowercase.IndexOf("from ") + 5)
                        {
                            if (match2.Groups[1].Value.Contains(" "))
                            {
                                recordset.TableName = match2.Groups[1].Value.Substring(0, match2.Groups[1].Value.IndexOf(" "));
                            }
                            else
                            {
                                recordset.TableName = match2.Groups[1].Value;
                            }
                        }
                        else
                        {
                        }
                    }
                }
            }
            else
            {
                adapter = new OleDbDataAdapter(string.Format("select * from {0}", name), connection);
                recordset.TableName = name;
            }

            //This builds the update and Delete queries for the table in the above SQL. this only works if the select is a single table. 
            OleDbCommandBuilder oleDbCommandBuilder = new OleDbCommandBuilder(adapter);

            if (adapter.SelectCommand != null)
            {
                adapter.SelectCommand.Transaction = trans;
            }

            adapter.Fill(recordset);

            recordset.DataAdapter = adapter;

            this.Recordsets.AddRecordset(recordset);

            if (name.Length > 256)
            {
                recordset.Name = name.Substring(0, 256);
            }
            else
            {
                recordset.Name = name;
            }

            if ((options & RecordsetOptionSettings.dbDenyWrite) == RecordsetOptionSettings.dbDenyWrite ||
                (options & RecordsetOptionSettings.dbReadOnly) == RecordsetOptionSettings.dbReadOnly)
            {
                recordset.Updatable = false;
            }
            else
            {
                recordset.Updatable = true;
            }

            return recordset;
        }

        /// <summary>
        /// Runs an action query or executes an SQL statement on the specified object.
        /// expression .Execute(Query, Options)
        /// Query - Required String
        /// Options - Optional Variant
        /// Remarks
        /// You can use the following RecordsetOptionEnumconstants for options.
        /// dbDenyWrite
        /// Denies write permission to other users (Microsoft Access workspaces only).
        /// dbInconsistent
        /// (Default) Executes inconsistent updates (Microsoft Access workspaces only).
        /// dbConsistent
        /// Executes consistent updates (Microsoft Access workspaces only).
        /// dbSQLPassThrough
        /// Executes an SQL pass-through query. Setting this option passes the SQL statement to an ODBC database for processing (Microsoft Access workspaces only).
        /// dbFailOnError
        /// Rolls back updates if an error occurs (Microsoft Access workspaces only).
        /// dbSeeChanges
        /// Generates a run-time error if another user is changing data you are editing (Microsoft Access workspaces only).
        /// dbRunAsync
        /// Executes the query asynchronously (ODBCDirect Connection and QueryDef objects only).
        /// dbExecDirect
        /// Executes the statement without first calling SQLPrepare ODBC API function (ODBCDirect Connection and QueryDef objects only).
        /// Note
        /// ODBCDirect workspaces are not supported in Microsoft Access 2013. Use ADO if you want to access external data sources without using the Microsoft Access database engine.
        /// Note
        /// The constants dbConsistent and dbInconsistent are mutually exclusive. You can use one or the other, but not both in a given instance of OpenRecordset. Using both dbConsistent and 
        /// dbInconsistent causes an error.
        /// The Execute method is valid only for action queries. If you use Execute with another type of query, an error occurs. Because an action query doesn't return any records, Execute doesn't 
        /// return a Recordset. (Executing an SQL pass-through query in an ODBCDirect workspace will not return an error if a Recordset isn't returned.)
        /// Use the RecordsAffected property of the Connection, Database, or QueryDef object to determine the number of records affected by the most recent Execute method. For example, RecordsAffected 
        /// contains the number of records deleted, updated, or inserted when executing an action query. When you use the Execute method to run a query, the RecordsAffected property of the QueryDef 
        /// object is set to the number of records affected.
        /// In a Microsoft Access workspace, if you provide a syntactically correct SQL statement and have the appropriate permissions, the Execute method won't fail — even if not a single row can be 
        /// modified or deleted. Therefore, always use the dbFailOnError option when using the Execute method to run an update or delete query. This option generates a run-time error and rolls back all 
        /// successful changes if any of the records affected are locked and can't be updated or deleted.
        /// In earlier versions of the Microsoft Jet database engine, SQL statements were automatically embedded in implicit transactions. If part of a statement executed with dbFailOnError failed, 
        /// the entire statement would be rolled back. To improve performance, these implicit transactions were removed starting with version 3.5. If you are updating older DAO code, be sure to consider 
        /// using explicit transactions around Execute statements.
        /// For best performance in a Microsoft Access workspace, especially in a multiuser environment, nest the Execute method inside a transaction. Use the BeginTrans method on the current Workspace 
        /// object, then use the Execute method, and complete the transaction by using the CommitTrans method on the Workspace. This saves changes on disk and frees any locks placed while the query 
        /// is running.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="options"></param>
        public void Execute(string query, RecordsetOptionSettings options = RecordsetOptionSettings.dbFailOnError)
        {
            // TODO:CHE options

            query = InjectPassword(query);

            OleDbCommand command = new OleDbCommand(query, connection);
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            //che:transaction
            command.Transaction = trans;

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Execute query with parameters
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        public void ExecuteWithParameters(string query, params object[] parameters)
        {
            query = InjectPassword(query);

            OleDbCommand command = new OleDbCommand(query, connection);
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            int i=0;
            foreach (var p in parameters)
            {
                if (p.GetType() == typeof(String))
                {
                    command.Parameters.Add("@p" + i, OleDbType.VarChar).Value = p;
                }
                else if (p.GetType() == typeof(Boolean))
                {
                    command.Parameters.Add("@p" + i, OleDbType.Boolean).Value = p;
                }
                else if (p.GetType() == typeof(Int32))
                    {
                        command.Parameters.Add("@p" + i, OleDbType.Integer).Value = p;
                    }
                    else
                    {
                        // TODO:CHE - other types
                        throw new NotImplementedException();
                    }
                i++;
            }
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Creates a new TableDef object (Microsoft Access workspaces only). .
        /// Syntax
        /// expression .CreateTableDef(Name, Attributes, SourceTableName, Connect)
        /// expression A variable that represents a Database object.
        /// Parameters
        /// Name
        /// Required/Optional
        /// Data Type
        /// Description
        /// Name
        /// Optional
        /// Variant
        /// A Variant (String subtype) that uniquely names the new TableDef object. See the Name property for details on valid TableDef names.
        /// Attributes
        /// Optional
        /// Variant
        /// A constant or combination of constants that indicates one or more characteristics of the new TableDef object. See the Attributes property for more information.
        /// SourceTableName
        /// Optional
        /// Variant
        /// A Variant (String subtype) containing the name of a table in an external database that is the original source of the data. The source string becomes the SourceTableName property 
        /// setting of the new TableDef object.
        /// Connect
        /// Optional
        /// Variant
        /// A Variant (String subtype) containing information about the source of an open database, a database used in a pass-through query, or a linked table. See the Connect property for more 
        /// information about valid connection strings.
        /// Return Value
        /// TableDef
        /// Remarks
        /// If you omit one or more of the optional parts when you use the CreateTableDef method, you can use an appropriate assignment statement to set or reset the corresponding property before you 
        /// append the new object to a collection. After you append the object, you can alter some but not all of its properties. See the individual property topics for more details.
        /// If name refers to an object that is already a member of the collection, or you specify an invalid property in the TableDef or Field object you're appending, a run-time error occurs when you 
        /// use the Append method. Also, you can't append a TableDef object to the TableDefs collection until you define at least one Field for the TableDef object.
        /// To remove a TableDef object from the TableDefs collection, use the Delete method on the collection.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="attributes"></param>
        /// <param name="sourceTableName"></param>
        /// <param name="connect"></param>
        /// <returns></returns>
        public FCTableDef CreateTableDef(string name = "", object attributes = null, object sourceTableName = null, object connect = null)
        {
            FCTableDef tableDef = new FCTableDef(this);
            tableDef.Name = name;
            tableDef.TableName = name;

            using (OleDbConnection connection = new OleDbConnection(this.Connection.ConnectionString))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                DbTransaction trans = connection.BeginTransaction();
                try
                {
                    DbCommand command = connection.CreateCommand();
                    command.Transaction = trans;
                    command.CommandText = "CREATE TABLE [" + name + "]";

                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return tableDef;
        }

        /// <summary>
        /// Closes an open Database.
        /// Syntax
        /// expression .Close
        /// expression A variable that represents a Database object.
        /// Remarks
        /// If the Database object is already closed when you use Close, a run-time error occurs.
        /// An alternative to the Close method is to set the value of an object variable to Nothing (Set dbsTemp = Nothing).
        /// </summary>
        public void Close()
        {
            if (connection.State == System.Data.ConnectionState.Open)
            {
                connection.Close();

                //che:transaction
                trans =  null;

                dbEngine.Databases.RemoveDatabase(this);
            }
        }

        public void FillTableDefs()
        {
            if (tableDefs != null)
            {
                return;
            }

            tableDefs = new FCTableDefs(this);

            // Microsoft Access provider factory
            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");

            DataTable indexes = null;
            DataTable tables = null;
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = this.Connection.ConnectionString;
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                tables = connection.GetSchema("Tables");
                indexes = connection.GetSchema("Indexes");

                string tableName = "";
                string pkName = "";
                FCTableDef tableDef = null;
                for (int i = 0; i < tables.Rows.Count; i++)
                {
                    tableName = tables.Rows[i][2].ToString();
                    if (tableName.Length > 3)
                    {
                        pkName = tableName.Remove(3);
                    }
                    else
                    {
                        if (tableName.Length > 3)
                        {
                            pkName = tableName.Remove(3);
                        }
                        else
                        {
                            pkName = tableName;
                        }
                    }

                    //if it is customer table and does not have a pk then create one
                    bool isCustomerTable = tables.Select("TABLE_TYPE = 'TABLE' AND TABLE_NAME = '" + tableName + "'").Count() == 1;
                    //CHE: one or more primary key columns
                    bool hasPrimaryKey = indexes.Select("PRIMARY_KEY = TRUE AND TABLE_NAME = '" + tableName + "'").Count() >= 1;
                    if (isCustomerTable && !hasPrimaryKey)
                    {
                        //CHE: cannot have multiple autonumber columns
                        DataRow[] dr = connection.GetSchema("Columns").Select("TABLE_NAME = '" + tableName + "' AND COLUMN_FLAGS = 90 AND DATA_TYPE=3");

                        if (dr.Length == 0)
                        {
                            DbTransaction trans = connection.BeginTransaction();
                            try
                            {
                                //create new PK column
                                DbCommand command = connection.CreateCommand();
                                command.Transaction = trans;
                                command.CommandText = "ALTER TABLE " + tableName + " Add Column PK_" + pkName + " int NOT NULL IDENTITY (1,1)";
                                command.ExecuteNonQuery();
                                //create new PK constraint
                                command.CommandText = "ALTER TABLE " + tableName + " ADD CONSTRAINT PK_" + pkName + " PRIMARY KEY CLUSTERED ( PK_" + pkName + " )";
                                command.ExecuteNonQuery();

                                trans.Commit();
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                            }
                        }
                    }

                    tableDef = new FCTableDef(this);
                    tableDef.Name = tableName;
                    tableDef.TableName = tableName;
                    this.TableDefs.AddTableDef(tableDef);
                }
                connection.Close();
            }
        }

        /// <summary>
        /// Changes the password of an existing Microsoft Access database engine database (Microsoft Access workspaces only).
        /// Syntax 
        /// expression .NewPassword(bstrOld, bstrNew)
        /// expression An expression that returns a Database object.
        /// bstrOld 
        /// Required
        /// String 
        /// The current setting of the Password property of the Database object.
        /// bstrNew 
        /// Required
        /// String 
        /// The new setting of the Password property of the Database object.
        /// Use strong passwords that combine upper- and lowercase letters, numbers, and symbols. Weak passwords don't mix these elements. Strong password: Y6dh!et5. 
        /// Weak password: House27. Use a strong password that you can remember so that you don't have to write it down.
        /// Remarks 
        /// The bstrOld and bstrNew strings can be up to 20 characters long and can include any characters except the ASCII character 0 (null). To clear the password, 
        /// use a zero-length string ("") for bstrNew.
        /// Passwords are case-sensitive.
        /// If a database has no password, the Microsoft Access database engine will automatically create one by passing a zero-length string ("") for the old password.
        /// Important note 
        /// If you lose your password, you can never open the database again.
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        public void NewPassword(string oldPassword, string newPassword)
        {
            //ALTER DATABASE PASSWORD `currentpassword` `` - set password
            //ALTER DATABASE PASSWORD `newpassword` `currentpassword` - change password
            //ALTER DATABASE PASSWORD `` `newpassword`
            
            //close current connection
            this.Close();

            //open in exclusive mode
            this.connection.ConnectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Jet OLEDB:Database Password={0};Mode=Share Exclusive;", this.openDatabaseName, this.openDatabasePassword);
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            //change password
            this.Execute(String.Format("ALTER DATABASE PASSWORD `{0}` `{1}`", newPassword, oldPassword));

            //save new password
            this.openDatabasePassword = newPassword;

            //connect with new password without exclusive rights
            this.connection.ConnectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Jet OLEDB:Database Password={0};", this.openDatabaseName, this.openDatabasePassword);
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }   

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private string InjectPassword(string query)
        {
            //CHE: modify query to inject password for cases like
            //SELECT * INTO tbl_Mitarbeiter FROM tbl_Mitarbeiter IN 'D:\Grunddaten_Admin.mdb'
            // has to be modified as
            //SELECT * INTO tbl_Mitarbeiter FROM [MS Access;Database=D:\Grunddaten_Admin.mdb;PWD=XXXX].tbl_Mitarbeiter

            string table = "";
            string mdb = "";
            string pwd = "";
            Regex regex = new Regex(@"FROM\s(.*)\sIN\s'(.*\.mdb)'", RegexOptions.IgnoreCase);
            Match m = regex.Match(query);
            if (m.Success)
            {
                table = m.Groups[1].Value;
                mdb = m.Groups[2].Value;
                //search pwd
                pwd = "";
                FileInfo path1=new FileInfo(mdb);
                foreach (FCDatabase db in this.dbEngine.Databases)
                {
                    FileInfo path2 = new FileInfo(db.openDatabaseName);
                    if (path1.FullName.ToUpper() == path2.FullName.ToUpper())
                    {
                        pwd = db.openDatabasePassword;
                        break;
                    }
                }

                if (!string.IsNullOrWhiteSpace(pwd))
                {
                    query = query.Replace(m.Groups[0].Value, "FROM [MS Access;Database=" + mdb + ";PWD=" + pwd + "]." + table);
                }
            }
            return query;
        }

        #endregion
    }
}
