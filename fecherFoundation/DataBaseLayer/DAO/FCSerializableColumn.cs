﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace fecherFoundation.DataBaseLayer
{
    [Serializable]
    public class FCSerializableColumn
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors

        public FCSerializableColumn(DataColumn objColumn, FCRecordset dataTable)
        {
            this.ColumnName = objColumn.ColumnName;
            this.AllowDBNull = objColumn.AllowDBNull;
            this.Table = dataTable;
            this.ReadOnly = objColumn.ReadOnly;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public string ColumnName 
        { 
            get; 
            set; 
        }

        public bool AllowDBNull 
        { 
            get; 
            set; 
        }

        public bool ReadOnly 
        { 
            get; 
            set; 
        }

        public FCRecordset Table 
        { 
            get; 
            set; 
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
