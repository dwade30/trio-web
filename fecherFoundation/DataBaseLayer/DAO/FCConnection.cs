﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Odbc;

namespace fecherFoundation.DataBaseLayer
{

    /// <summary>
    /// FCConnection
    /// </summary>
    public class FConnection 
    {
        public FCDatabase DB { get; private set; }


        public FConnection(FCDatabase db)
        {
            DB = db;

        }
        public FCQueryDef CreateFCQueryDef(string name, string query)
        {
            return new FCQueryDef(name,query,this);
        }
        public void Open()
        {
            DB.Connection.Open();
        }

        public FCRecordset OpenRecordset(string name,  RecordsetTypeEnum typename)
        {
            
           return DB.OpenRecordset(name,typename, RecordsetOptionEnum.dbFailOnError,null);

        }

        
    }
}
