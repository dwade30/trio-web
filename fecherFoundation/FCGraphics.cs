﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Globalization;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCGraphics
    {
        #region Members
        public const float TwipsPerInch = 1440f;
        // TODO:IPI - must check if Tab size 8 is correct
        private const string TabReplacement = "        ";

        private CoordinateSpace coordinateSpace;
        private GraphicsFactory graphicsFactory;
        private bool isPrinter = false;
        #endregion

        #region Constructors
        public FCGraphics(GraphicsFactory graphicsFactory)
        {
            this.graphicsFactory = graphicsFactory;
            this.isPrinter = false;
        }

        public FCGraphics(Graphics g, GraphicsFactory graphicsFactory)
            : this(graphicsFactory)
        {
            InitializeCoordinateSpace(g, Rectangle.Empty, 0, 0);
        }

        /// <summary>
        /// This constructor is used by the printer
        /// </summary>
        /// <param name="graphicsFactory"></param>
        /// <param name="coordinateSpace"></param>
        public FCGraphics(GraphicsFactory graphicsFactory, CoordinateSpace coordinateSpace)
            : this(graphicsFactory)
        {
            this.coordinateSpace = coordinateSpace;
            this.isPrinter = true;
        }
        #endregion

        #region Properties

        //CHE: use TabStops in report
        public float FirstTabOffset
        {
            get;
            set;
        }

        //CHE: use TabStops in report
        public float[] TabStops
        {
            get;
            set;
        }

        public float TwipsPerPixelX
        {
            get
            {
                return TwipsPerInch / coordinateSpace.DpiX;
            }
        }

        public float TwipsPerPixelY
        {
            get
            {
                return TwipsPerInch / coordinateSpace.DpiY;
            }
        }

        #endregion

        #region Methods
        public void PaintPicture(Graphics graphics, Rectangle marginBounds, float currentX, float currentY, Image picture, float x1, float y1, float width1 = float.NaN, float height1 = float.NaN, float x2 = 0, float y2 = 0, float width2 = float.NaN, float height2 = float.NaN)
        {
            InitializeCoordinateSpace(graphics, marginBounds, currentX, currentY);
            PaintPicture(graphics, picture, x1, y1, width1, height1, x2, y2, width2, height2);
        }

        /// <summary>
        /// Prints the contents of an image file on a page.
        /// </summary>
        /// <param name="picture"><see cref="T:System.Drawing.Image"/> value representing the image to be printed.</param><param name="x1">Single value indicating the horizontal destination coordinates where the image will be printed. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measure used.</param><param name="y1">Single value indicating the vertical destination coordinates where the image will be printed. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measure used.</param><param name="width1">Optional. Single value indicating the destination width of the picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If the destination width is larger or smaller than the source width, picture is stretched or compressed to fit. If omitted, the source width is used.</param><param name="height1">Optional. Single value indicating the destination height of the picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If the destination height is larger or smaller than the source height, picture is stretched or compressed to fit. If omitted, the source height is used.</param><param name="x2">Optional. Single values indicating the coordinates (x-axis) of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, 0 is assumed.</param><param name="y2">Optional. Single values indicating the coordinates (y-axis) of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, 0 is assumed.</param><param name="width2">Optional. Single value indicating the source width of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, the entire source width is used.</param><param name="height2">Optional. Single value indicating the source height of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, the entire source height is used.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void PaintPicture(Graphics graphics, Image picture, float x1, float y1, float width1 = float.NaN, float height1 = float.NaN, float x2 = 0, float y2 = 0, float width2 = float.NaN, float height2 = float.NaN)
        {
            if (picture == null)
                Information.Err().Raise(91);
            float num1 = coordinateSpace.ScaleX(picture.Width / picture.HorizontalResolution, 5, coordinateSpace.ScaleMode);
            float num2 = coordinateSpace.ScaleY(picture.Height / picture.VerticalResolution, 5, coordinateSpace.ScaleMode);
            if (float.IsNaN(width2))
                width2 = num1 - x2;
            if (float.IsNaN(height2))
                height2 = num2 - y2;
            if (float.IsNaN(width1))
                width1 = width2;
            if (float.IsNaN(height1))
                height1 = height2;
            Rectangle rect1 = new Rectangle();
            rect1.X = checked(Convert.ToInt32(Math.Round(Math.Round((double)coordinateSpace.ToPixelsX(x1)))));
            rect1.Y = checked(Convert.ToInt32(Math.Round(Math.Round((double)coordinateSpace.ToPixelsY(y1)))));
            rect1.Width = checked(Convert.ToInt32(Math.Round(Math.Round((double)coordinateSpace.ToPixelsWidth(width1)))));
            rect1.Height = checked(Convert.ToInt32(Math.Round(Math.Round((double)coordinateSpace.ToPixelsHeight(height1)))));
            coordinateSpace.NormalizeRectangle(ref rect1);
            Rectangle rect2 = new Rectangle();
            rect2.X = checked(Convert.ToInt32(Math.Round(Math.Round(unchecked((double)coordinateSpace.ScaleX(x2 - coordinateSpace.ScaleLeft, coordinateSpace.ScaleMode, (short)5) * (double)picture.HorizontalResolution)))));
            rect2.Y = checked(Convert.ToInt32(Math.Round(Math.Round(unchecked((double)coordinateSpace.ScaleY(y2 - coordinateSpace.ScaleTop, coordinateSpace.ScaleMode, (short)5) * (double)picture.VerticalResolution)))));
            rect2.Width = checked(Convert.ToInt32(Math.Round(Math.Round(unchecked((double)coordinateSpace.ScaleX(width2, coordinateSpace.ScaleMode, (short)5) * (double)picture.HorizontalResolution)))));
            rect2.Height = checked(Convert.ToInt32(Math.Round(Math.Round(unchecked((double)coordinateSpace.ScaleY(height2, coordinateSpace.ScaleMode, (short)5) * (double)picture.VerticalResolution)))));
            coordinateSpace.NormalizeRectangle(ref rect2);
            Rectangle rectangle1 = new Rectangle();
            rectangle1.X = 0;
            rectangle1.Y = 0;
            rectangle1.Width = picture.Width;
            rectangle1.Height = picture.Height;
            rectangle1.X = checked(rectangle1.X - rect2.X);
            rectangle1.Y = checked(rectangle1.Y - rect2.Y);
            rect2.X = 0;
            rect2.Y = 0;
            Rectangle rectangle2 = Rectangle.Intersect(rect2, rectangle1);
            if (rectangle2.Width <= 0 || rectangle2.Height <= 0)
                Information.Err().Raise(5);
            bool flipX = false;
            if (Math.Sign(width1) != Math.Sign(width2))
                flipX = true;
            bool flipY = false;
            if (Math.Sign(height1) != Math.Sign(height2))
                flipY = true;
            bool flip = flipX | flipY;

            using (MemoryStream memoryStream = this.GetMemoryStream(picture, flip, rect2, rectangle1))
            {
                using (Image inMemoryImage = this.GetInMemoryImage(picture, memoryStream, flip, rect2, rectangle1))
                {
                    //if (isPrinter)
                    {
                        this.FlipImage(inMemoryImage, flipX, flipY);
                    }
                    graphics.DrawImage(inMemoryImage, rect1);
                }
            }
        }

        public void LineInternal(Graphics graphics, Rectangle marginBounds, ref float currentX, ref float currentY, bool relativeStart, float x1, float y1, bool relativeEnd, float x2, float y2, int color = -1, bool box = false, bool fill = false, bool invert = false, Control control = null)
        {
            InitializeCoordinateSpace(graphics, marginBounds, currentX, currentY);
            LineInternal(graphics, relativeStart, x1, y1, relativeEnd, x2, y2, color, box, fill, invert, control);
            currentX = coordinateSpace.CurrentX;
            currentY = coordinateSpace.CurrentY;
        }

        public float TextHeight(Graphics graphics, string text, Rectangle clientRectangle)
        {
            if (string.IsNullOrEmpty(text))
            {
                return 0;
            }
            Rectangle rectangle = new Rectangle(clientRectangle.X, clientRectangle.Y, Math.Max(clientRectangle.Width, 10000), Math.Max(clientRectangle.Height, 10000));
            Region region = GetTextRegion(graphics, text, graphicsFactory.Font, Brushes.Transparent, rectangle);
            return this.coordinateSpace.FromPixelsHeight(region.GetBounds(graphics).Height);
        }

        public float TextWidth(Graphics graphics, string text, Rectangle clientRectangle)
        {
            if (string.IsNullOrEmpty(text))
            {
                return 0;
            }
            Rectangle rectangle = new Rectangle(clientRectangle.X, clientRectangle.Y, Math.Max(clientRectangle.Width, 10000), Math.Max(clientRectangle.Height, 10000));
            if (!text.Contains(' ') && !text.Contains(Constants.vbTab))
            {
                Region region = GetTextRegion(graphics, text, graphicsFactory.Font, Brushes.Transparent, rectangle);
                return this.coordinateSpace.FromPixelsWidth(region.GetBounds(graphics).Width);
            }
            else
            {
                //CHE: if text contains spaces or tabs then use bounds to get correct value
                Region regionBounded = GetTextRegion(graphics, "A" + text + "A", graphicsFactory.Font, Brushes.Transparent, rectangle);
                Region regionBounds = GetTextRegion(graphics, "AA", graphicsFactory.Font, Brushes.Transparent, rectangle);
                return this.coordinateSpace.FromPixelsWidth(regionBounded.GetBounds(graphics).Width - regionBounds.GetBounds(graphics).Width);
            }
        }

        public float ScaleHeight
        {
            get
            {
                return this.coordinateSpace.ScaleHeight / (this.coordinateSpace.DpiY / 100.0f);
            }
            set
            {
                this.coordinateSpace.ScaleHeight = value * (this.coordinateSpace.DpiY / 100.0f);
            }
        }

        public float ScaleWidth
        {
            get
            {
                return this.coordinateSpace.ScaleWidth / (this.coordinateSpace.DpiX / 100.0f);
            }
            set
            {
                this.coordinateSpace.ScaleWidth = value * (this.coordinateSpace.DpiX / 100.0f);
            }
        }

        public float ScaleTop
        {
            get
            {
                return this.coordinateSpace.ScaleTop;
            }
            set
            {
                this.coordinateSpace.ScaleTop = value;
            }
        }

        public float ScaleLeft
        {
            get
            {
                return this.coordinateSpace.ScaleLeft;
            }
            set
            {
                this.coordinateSpace.ScaleLeft = value;
            }
        }

        public ScaleModeConstants ScaleMode
        {
            get
            {
                return (ScaleModeConstants)this.coordinateSpace.ScaleMode;
            }
            set
            {
                this.coordinateSpace.ScaleMode = Convert.ToInt32(value);
            }
        }

        public float ScaleX(float value, int fromScale, int toScale)
        {
            return this.coordinateSpace.ScaleX(value, fromScale, toScale);
        }

        public float ScaleY(float value, int fromScale, int toScale)
        {
            return this.coordinateSpace.ScaleY(value, fromScale, toScale);
        }

        public float ScaleX(float value, ScaleModeConstants fromScale, ScaleModeConstants toScale)
        {
            return this.coordinateSpace.ScaleX(value, Convert.ToInt32(fromScale), Convert.ToInt32(toScale));
        }

        public float ScaleY(float value, ScaleModeConstants fromScale, ScaleModeConstants toScale)
        {
            return this.coordinateSpace.ScaleY(value, Convert.ToInt32(fromScale), Convert.ToInt32(toScale));
        }

        public ScaleModeConstants GetParentScaleMode(Control c)
        {
            ScaleModeConstants scaleMode = ScaleModeConstants.vbTwips;

            Control parent = c.Parent;
            while (parent != null)
            {
                FCForm form = parent as FCForm;
                if (form != null)
                {
                    return form.ScaleMode!= 0 ? form.ScaleMode : ScaleModeConstants.vbTwips;
                }
                FCPictureBox pictureBox = parent as FCPictureBox;
                if (pictureBox != null)
                {
                    return pictureBox.ScaleMode != 0 ? pictureBox.ScaleMode : ScaleModeConstants.vbTwips;
                }
                parent = parent.Parent;
            }
            return scaleMode;
        }

        private SizeF MeasureString(Graphics graphics, string line, StringFormat format)
        {
            SizeF sizeF1;
            if (Strings.Trim(line).Length == 0)
            {
                string text1 = "W";
                string text2 = line + text1;
                SizeF sizeF2 = graphics.MeasureString(text1, graphicsFactory.Font, int.MaxValue, format);
                sizeF1 = graphics.MeasureString(text2, graphicsFactory.Font, int.MaxValue, format);
                sizeF1.Width = sizeF1.Width - sizeF2.Width;
            }
            else
            {
                sizeF1 = graphics.MeasureString(line, graphicsFactory.Font, int.MaxValue, format);
            }
            return sizeF1;
        }

        public void Output(Graphics graphics, Rectangle marginBounds, ref float currentX, ref float currentY, object source, bool newLine, params object[] args)
        {
            InitializeCoordinateSpace(graphics, marginBounds, currentX, currentY);
            Output(graphics, source, newLine, args);
            currentX = coordinateSpace.CurrentX;
            currentY = coordinateSpace.CurrentY;
        }

        public void Output(Graphics graphics, object source, bool newLine, params object[] args)
        {

            StringBuilder stringBuilder = new StringBuilder(this.TabLayout(args));
            stringBuilder.Replace("\r\n", "\n");
            stringBuilder.Replace("\r", "\n");
            string[] strArray = stringBuilder.ToString().Split(new char[1] { '\n' });
            int num1 = 0;
            int num2 = checked(strArray.Length - 2);
            int index = num1;
            while (index <= num2)
            {
                this.PrintLine(graphics, source, strArray[index], true);
                checked { ++index; }
            }
            if (checked(strArray.Length - 1) < 0)
            {
                return;
            }
            this.PrintLine(graphics, source, strArray[checked(strArray.Length - 1)], newLine);
        }

        /// <summary>
        /// Prints a circle, an ellipse, or an arc on a page, specifying whether the center point is relative to the current location.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the center of the circle, ellipse, or arc is printed relative to the coordinates specified in the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the object.</param><param name="x">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="y">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startangle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void Circle(Graphics graphics, bool relativeStart, float x, float y, float radius, int color = -1, float startAngle = float.NaN, float endAngle = float.NaN, float aspect = 1f)
        {
            // TODO
            //if ((double)Math.Abs(startAngle) >= 2.0 * Math.PI | (double)Math.Abs(endAngle) >= 2.0 * Math.PI)
            //    Information.Err().Raise(380);
            if (relativeStart)
            {
                x += coordinateSpace.CurrentX;
                y += coordinateSpace.CurrentY;
            }
            coordinateSpace.CurrentX = x;
            coordinateSpace.CurrentY = y;
            if (color == -1)
                color = 0;
            RectangleF rect = this.CalculateCircleBounds(x, y, radius, aspect);
            if (float.IsNaN(startAngle) & float.IsNaN(endAngle))
            {
                using (Brush fillBrush = this.graphicsFactory.CreateFillBrush())
                    graphics.FillEllipse(fillBrush, rect);
                using (Pen drawPen = this.graphicsFactory.CreateDrawPen(color))
                    graphics.DrawEllipse(drawPen, rect);
            }
            else
            {
                if (float.IsNaN(startAngle))
                    startAngle = 0.0f;
                if (float.IsNaN(endAngle))
                    endAngle = 0.0f;
                float startAngle1 = this.ToDegree(startAngle);
                float sweepAngle = this.CalculateSweep(startAngle, endAngle);
                if ((double)startAngle < 0.0 && (double)endAngle < 0.0)
                {
                    using (Brush fillBrush = this.graphicsFactory.CreateFillBrush())
                        graphics.FillPie(fillBrush, rect.X, rect.Y, rect.Width, rect.Height, startAngle1, sweepAngle);
                }
                using (Pen drawPen = this.graphicsFactory.CreateDrawPen(color))
                {
                    graphics.DrawArc(drawPen, rect, startAngle1, sweepAngle);
                    float x1 = rect.X + rect.Width / 2f;
                    float y1 = rect.Y + rect.Height / 2f;
                    if ((double)startAngle < 0.0)
                    {
                        PointF pointF = this.PointOnCircle(rect, startAngle);
                        graphics.DrawLine(drawPen, x1, y1, pointF.X, pointF.Y);
                    }
                    if ((double)endAngle >= 0.0)
                        return;
                    PointF pointF1 = this.PointOnCircle(rect, endAngle);
                    graphics.DrawLine(drawPen, x1, y1, pointF1.X, pointF1.Y);
                }
            }
        }

        /// <summary>
        /// Prints a single point in a specified color on a page, optionally specifying a point relative to the current coordinates.
        /// </summary>
        /// <param name="relativeStart">Boolean value indicating whether the coordinates are relative to the current graphics position (as set by <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/>, <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/>).</param><param name="x">Single value indicating the horizontal coordinates of the point to print.</param><param name="y">Single value indicating the vertical coordinates of the point to print.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color specified for the point. If this parameter is omitted, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ForeColor"/> property setting is used.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void PSet(Graphics graphics, bool relativeStart, float x, float y, int color)
        {
            if (color == -1)
                color = graphicsFactory.ForeColor;
            if (relativeStart)
            {
                x += coordinateSpace.CurrentX;
                y += coordinateSpace.CurrentY;
            }
            coordinateSpace.CurrentX = x;
            coordinateSpace.CurrentY = y;
            int num1 = checked(Convert.ToInt32(Math.Round(Math.Round((double)this.coordinateSpace.ToPixelsX(x)))));
            int num2 = checked(Convert.ToInt32(Math.Round(Math.Round((double)this.coordinateSpace.ToPixelsY(y)))));
            if (Convert.ToInt32(graphicsFactory.DrawWidth) == 1)
            {
                using (Bitmap bitmap = new Bitmap(1, 1))
                {
                    bitmap.SetPixel(0, 0, ColorTranslator.FromOle(color));
                    graphics.DrawImage((Image)bitmap, num1, num2);
                }
            }
            else
            {
                using (Pen drawPen = this.graphicsFactory.CreateDrawPen(color))
                    graphics.DrawLine(drawPen, num1, num2, checked(num1 + 1), num2);
            }
        }

        //CHE: use Color because ColorTranslator.ToOle is testing if the color is a standard system color otherwise it's using ToWin32 conversion which loses the alpha component for transparency
        public void PSet(Graphics graphics, bool relativeStart, float x, float y, Color? color)
        {
            if (color == null)
                color = ColorTranslator.FromOle(graphicsFactory.ForeColor);

            Color color1 = (Color)color;
            if (relativeStart)
            {
                x += coordinateSpace.CurrentX;
                y += coordinateSpace.CurrentY;
            }
            coordinateSpace.CurrentX = x;
            coordinateSpace.CurrentY = y;
            int num1 = checked(Convert.ToInt32(Math.Round(Math.Round((double)this.coordinateSpace.ToPixelsX(x)))));
            int num2 = checked(Convert.ToInt32(Math.Round(Math.Round((double)this.coordinateSpace.ToPixelsY(y)))));
            if (Convert.ToInt32(graphicsFactory.DrawWidth) == 1)
            {
                using (Bitmap bitmap = new Bitmap(1, 1))
                {
                    bitmap.SetPixel(0, 0, color1);
                    graphics.DrawImage((Image)bitmap, num1, num2);
                }
            }
            else
            {
                using (Pen drawPen = this.graphicsFactory.CreateDrawPen(color1))
                    graphics.DrawLine(drawPen, num1, num2, checked(num1 + 1), num2);
            }
        }

        /// <summary>
        /// Returns, as a long integer, the red-green-blue (RGB) color of the specified point on a Form or PictureBox
        /// </summary>
        /// <param name="image"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Point(Image image, float x, float y)
        {
            if (image != null)
            {
                int num1 = checked(Convert.ToInt32(Math.Round(Math.Round((double)this.coordinateSpace.ToPixelsX(x)))));
                int num2 = checked(Convert.ToInt32(Math.Round(Math.Round((double)this.coordinateSpace.ToPixelsY(y)))));
                Bitmap bmp = image as Bitmap;
                if (bmp != null)
                {
                    return ColorTranslator.ToOle(bmp.GetPixel(num1, num2));
                }
            }
            return -1;
        }
        
        //CHE: use Color because ColorTranslator.ToOle is testing if the color is a standard system color otherwise it's using ToWin32 conversion which loses the alpha component for transparency
        public Color? PointColor(Image image, float x, float y)
        {
            if (image != null)
            {
                int num1 = checked(Convert.ToInt32(Math.Round(Math.Round((double)this.coordinateSpace.ToPixelsX(x)))));
                int num2 = checked(Convert.ToInt32(Math.Round(Math.Round((double)this.coordinateSpace.ToPixelsY(y)))));
                Bitmap bmp = image as Bitmap;
                if (bmp != null)
                {
                    return bmp.GetPixel(num1, num2);
                }
            }
            return null;
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the starting coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed.</param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed.</param><param name="relativeEnd">Boolean. If this parameter is set to true, the ending coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. you cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public void LineInternal(Graphics graphics, bool relativeStart, float x1, float y1, bool relativeEnd, float x2, float y2, int color = -1, bool box = false, bool fill = false, bool invert = false, Control control = null)
        {
            if (color == -1)
                // MW after https://msdn.microsoft.com/en-us/library/aa230480(v=vs.60).aspx ForeColor is used if not specified
                color = graphicsFactory.ForeColor;
            if (relativeStart)
            {
                x1 += coordinateSpace.CurrentX;
                y1 += coordinateSpace.CurrentY;
            }
            if (relativeEnd)
            {
                x2 += x1;
                y2 += y1;
            }
            coordinateSpace.CurrentX = x2;
            coordinateSpace.CurrentY = y2;
            if (box && x1 != x2 && y1 != y2)
            {
                RectangleF rect = this.CalculateBoxBounds(x1, y1, x2, y2);
                if (fill)
                {
                    using (Brush drawBrush = graphicsFactory.CreateDrawBrush(color))
                    {
                        graphics.FillRectangle(drawBrush, rect);
                    }
                }
                else
                {
                    if (!invert)
                    {
                        using (Brush fillBrush = graphicsFactory.CreateFillBrush())
                        {
                            graphics.FillRectangle(fillBrush, rect);
                        }
                    }
                    else
                    {
                        FCPictureBox picBox = control as FCPictureBox;
                        if (picBox != null && picBox.Image != null)
                        {
                            using (Bitmap bitmapImage = picBox.Image.Clone() as Bitmap)
                            {
                                // create some image attributes
                                ImageAttributes attributes = new ImageAttributes();
                                attributes.SetColorMatrix(FCUtils.GetNegativColorMatrix());
                                graphics.DrawImage(bitmapImage, new Rectangle(Convert.ToInt32(rect.X), Convert.ToInt32(rect.Y), Convert.ToInt32(rect.Width), Convert.ToInt32(rect.Height)), rect.X, rect.Y, rect.Width, rect.Height, GraphicsUnit.Pixel, attributes);
                            }
                        }
                    }
                    using (Pen drawPen = graphicsFactory.CreateDrawPen(color))
                    {
                        graphics.DrawRectangle(drawPen, rect.X, rect.Y, rect.Width, rect.Height);
                    }
                }
            }
            else
            {
                PointF pt1 = new PointF();
                pt1.X = (float)coordinateSpace.ToPixelsX(x1);
                pt1.Y = (float)coordinateSpace.ToPixelsY(y1);
                PointF pt2 = new PointF();
                pt2.X = (float)coordinateSpace.ToPixelsX(x2);
                pt2.Y = (float)coordinateSpace.ToPixelsY(y2);
                if ((double)Math.Abs(pt1.X - pt2.X) < graphicsFactory.DrawWidth && (double)Math.Abs(pt1.Y - pt2.Y) < graphicsFactory.DrawWidth)
                {
                    pt2.X = pt2.X + graphicsFactory.DrawWidth;
                }
                using (Pen drawPen = graphicsFactory.CreateDrawPen(color))
                {
                    graphics.DrawLine(drawPen, pt1, pt2);
                }
            }
        }

        private bool UseInMemoryMetafile(Image picture, bool flip)
        {
            return !flip && picture is Metafile;
        }

        private static Region GetTextRegion(Graphics g, string s, Font font, Brush brush, RectangleF layoutRectangle)
        {
            var format = new StringFormat();

            //CHE: need to replace tabs which are not drawn to spaces to be able to calculate their width as well
            s = s.Replace("\t", TabReplacement);

            if (s.Contains(System.Environment.NewLine))
            {
                SizeF stringSize = g.MeasureString(s, font);
                g.DrawString(s, font, brush, stringSize.Width, stringSize.Height, format);
                return new Region(new RectangleF(new Point(0, 0), stringSize));
            }
            else
            {
                format.SetMeasurableCharacterRanges(new[] { new CharacterRange(0, s.Length) });
                g.DrawString(s, font, brush, layoutRectangle, format);
                return g.MeasureCharacterRanges(s, font, layoutRectangle, format)[0];
            }
        }

        private RectangleF CalculateBoxBounds(float x1, float y1, float x2, float y2)
        {
            x1 = coordinateSpace.ToPixelsX(x1);
            y1 = coordinateSpace.ToPixelsY(y1);
            x2 = coordinateSpace.ToPixelsX(x2);
            y2 = coordinateSpace.ToPixelsY(y2);
            RectangleF rectangleF = new Rectangle();
            rectangleF.X = (float)Math.Min(x1, x2);
            rectangleF.Y = (float)Math.Min(y1, y2);
            rectangleF.Width = (float)Math.Abs(x2 - x1);
            rectangleF.Height = (float)Math.Abs(y2 - y1);
            return rectangleF;
        }

        private MemoryStream GetMemoryStream(Image picture, bool flip, Rectangle srcRect, Rectangle pictureRect)
        {
            if (picture == null)
                return (MemoryStream)null;
            if (!this.UseInMemoryMetafile(picture, flip))
                return (MemoryStream)null;
            MemoryStream memoryStream = new MemoryStream();
            using (Graphics graphics1 = Graphics.FromHwndInternal(IntPtr.Zero))
            {
                using (Metafile metafile = new Metafile((Stream)memoryStream, graphics1.GetHdc(), srcRect, MetafileFrameUnit.Pixel))
                {
                    using (Graphics graphics2 = Graphics.FromImage((Image)metafile))
                        graphics2.DrawImageUnscaled(picture, pictureRect.X, pictureRect.Y);
                }
            }
            memoryStream.Position = 0L;
            return memoryStream;
        }

        private Image GetInMemoryImage(Image picture, MemoryStream stream, bool flip, Rectangle srcRect, Rectangle pictureRect)
        {
            if (picture == null)
                return (Image)null;
            if (this.UseInMemoryMetafile(picture, flip))
            {
                if (stream == null)
                    return (Image)null;
                else
                    return (Image)new Metafile((Stream)stream);
            }
            else
            {
                Bitmap bitmap = new Bitmap(srcRect.Width, srcRect.Height);
                using (Graphics graphics = Graphics.FromImage((Image)bitmap))
                {
                    graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                    graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
                    graphics.DrawImage(picture, pictureRect.X, pictureRect.Y, bitmap.Width, bitmap.Height);
                }
                return (Image)bitmap;
            }
        }

        private void FlipImage(Image image, bool flipX, bool flipY)
        {
            if (flipX && flipY)
                image.RotateFlip(RotateFlipType.Rotate180FlipNone);
            else if (flipX)
            {
                image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }
            else
            {
                if (!flipY)
                    return;
                image.RotateFlip(RotateFlipType.Rotate180FlipX);
            }
        }

        public void InitializeCoordinateSpace(Rectangle marginBounds, float currentX, float currentY)
        {
            InitializeCoordinateSpace(coordinateSpace.DpiX, coordinateSpace.DpiY, marginBounds, currentX, currentY);
        }

        public void InitializeCoordinateSpace(Graphics g, Rectangle marginBounds, float currentX, float currentY)
        {
            InitializeCoordinateSpace(g.DpiX, g.DpiY, marginBounds, currentX, currentY);
        }
        public void InitializeCoordinateSpace(float DpiX, float DpiY, Rectangle marginBounds, float currentX, float currentY)
        {
            if (coordinateSpace == null)
            {
                coordinateSpace = new CoordinateSpace(marginBounds, DpiX, DpiY);
            }
            else
            {
                coordinateSpace.SetMarginBounds(marginBounds);
                coordinateSpace.SetDpi(DpiX, DpiY);
            }
            coordinateSpace.CurrentX = currentX;
            coordinateSpace.CurrentY = currentY;
        }

        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        private void PrintLine(Graphics graphics, object source, string line, bool newLine)
        {
            //FC:FINAL:RPU:#i1598 - Actualize graphics because after generating NewPage its arguments will throw ArgumentException
            graphics = ((FCPrinter)source).GetGraphics();
            if (line == null)
                Information.Err().Raise(380);
            if (line.Length > 10000)
                line = line.Substring(0, 10000);
            using (StringFormat stringFormat = graphicsFactory.CreateStringFormat())
            {
                SizeF sizeF = MeasureString(graphics, line, stringFormat);
                if ((double)coordinateSpace.ToPixelsY(coordinateSpace.CurrentY) + (double)sizeF.Height > (double)coordinateSpace.ToPixelsHeight(coordinateSpace.ScaleHeight))
                {
                    if (source is FCPrinter)
                    {
                        ((FCPrinter)source).NewPage();
                        //FC:FINAL:RPU:#i1598 - Actualize graphics after generating NewPage
                        graphics = ((FCPrinter)source).GetGraphics();
                        coordinateSpace.CurrentY = 0.0f;
                    }

                }
                RectangleF rect = new RectangleF();
                rect.X = (float)coordinateSpace.ToPixelsX(coordinateSpace.CurrentX);
                rect.Y = (float)coordinateSpace.ToPixelsY(coordinateSpace.CurrentY);
                rect.Width = sizeF.Width;
                rect.Height = sizeF.Height;
                if (graphicsFactory.RightToLeft)
                    rect.X = (float)coordinateSpace.ToPixelsWidth(coordinateSpace.ScaleWidth) - rect.X;
                if (!graphicsFactory.FontTransparent)
                {
                    using (Brush drawBrush = graphicsFactory.CreateDrawBrush(graphicsFactory.FillColor))
                        graphics.FillRectangle(drawBrush, rect);
                }
                using (Brush drawBrush = graphicsFactory.CreateDrawBrush(graphicsFactory.ForeColor))
                {
                    //CHE: use TabStops in report
                    if (this.TabStops != null)
                    {
                        stringFormat.SetTabStops(this.FirstTabOffset, this.TabStops);
                    }
                    else
                    {
                        stringFormat.SetTabStops(0, new float[0]);
                    }

                    graphics.DrawString(line, graphicsFactory.Font, drawBrush, rect.X, rect.Y, stringFormat);
                }
                if (newLine)
                {
                    coordinateSpace.CurrentX = 0.0f;
                    coordinateSpace.CurrentY = coordinateSpace.CurrentY + coordinateSpace.FromPixelsHeight(sizeF.Height);
                    if ((double)coordinateSpace.ToPixelsY(coordinateSpace.CurrentY) <= (double)coordinateSpace.ToPixelsHeight(coordinateSpace.ScaleHeight))
                        return;
                    if (source is FCPrinter)
                    {
                        ((FCPrinter)source).NewPage();
                        //FC:FINAL:RPU:#i1598 - Actualize graphics after generating NewPage
                        graphics = ((FCPrinter)source).GetGraphics();
                    }
                }
                else
                {
                    coordinateSpace.CurrentX += coordinateSpace.FromPixelsWidth(sizeF.Width);
                }
            }
        }

        private string TabLayout(params object[] Args)
        {
            if (Args == null)
                return "";
            string Layout = (string)null;
            int CurrentCol = 1;
            bool flag = true;
            int num1 = 0;
            int num2 = ((Array)Args).UBound();
            int index = num1;
            while (index <= num2)
            {
                object objectValue = RuntimeHelpers.GetObjectValue(Args[index]);
                if (objectValue is TabInfo)
                {
                    object obj = objectValue;
                    TabInfo tabInfo1 = new TabInfo();
                    TabInfo tabInfo2 = obj != null ? (TabInfo)obj : tabInfo1;
                    this.AddTabToLayout(ref Layout, ref CurrentCol, Convert.ToInt32(tabInfo2.Column));
                    flag = true;
                }
                else if (objectValue is SpcInfo)
                {
                    object obj = objectValue;
                    SpcInfo spcInfo1 = new SpcInfo();
                    SpcInfo spcInfo2 = obj != null ? (SpcInfo)obj : spcInfo1;
                    if (Convert.ToInt32(spcInfo2.Count) > 0)
                    {
                        Layout = Layout + Strings.Space(Convert.ToInt32(spcInfo2.Count));
                        checked { CurrentCol += Convert.ToInt32(spcInfo2.Count); }
                    }
                    flag = true;
                }
                else
                {
                    if (!flag)
                        this.AddTabToLayout(ref Layout, ref CurrentCol, -1);
                    string str = objectValue != DBNull.Value ? (objectValue != null ? Convert.ToString(objectValue) : "") : "Null";
                    if (objectValue != null)
                    {
                        if (objectValue is DateTime)
                            str = str + " ";
                        else if (objectValue is byte || objectValue is short || (objectValue is int || objectValue is long) || (objectValue is float || objectValue is double || (objectValue is Decimal || objectValue.GetType().IsEnum)))
                        {
                            if (string.Compare(Convert.ToString(Strings.GetChar(str, 1)), CultureInfo.CurrentCulture.NumberFormat.NegativeSign, false) != 0)
                                str = " " + str;
                            str = str + " ";
                        }
                    }
                    Layout = Layout + str;
                    int num3 = Strings.InStrRev(str, "\r", -1);
                    int num4 = Strings.InStrRev(str, "\n", -1);
                    if (num3 > 0 || num4 > 0)
                    {
                        int num5 = Convert.ToInt32(num3 > num4 ? (object)num3 : (object)num4);
                        CurrentCol = checked(str.Length - num5 + 1);
                    }
                    else
                        checked { CurrentCol += str.Length; }
                    flag = false;
                }
                checked { ++index; }
            }
            return Layout;
        }

        private void AddTabToLayout(ref string Layout, ref int CurrentCol, int Column)
        {
            if (Column < 0)
            {
                int num = checked((unchecked(checked(CurrentCol - 1) / 14) + 1) * 14 + 1);
                Layout = Layout + Strings.Space(checked(num - CurrentCol));
                CurrentCol = num;
            }
            else
            {
                int num = Column;
                if (num < 1)
                    num = 1;
                Layout = num < CurrentCol ? Layout + "\r\n" + Strings.Space(checked(num - 1)) : Layout + Strings.Space(checked(num - CurrentCol));
                CurrentCol = num;
            }
        }

        private PointF PointOnCircle(RectangleF rect, float angle)
        {
            float num1 = rect.Width / 2f;
            float num2 = rect.Height / 2f;
            float num3 = rect.X + num1;
            float num4 = rect.Y + num2;
            angle = 6.283185f - Math.Abs(angle);
            float num5 = (float)Math.Tan((double)angle);
            float num6 = (float)Math.Sqrt((double)num1 * (double)num1 * (double)num2 * (double)num2 / ((double)num1 * (double)num1 * (double)num5 * (double)num5 + (double)num2 * (double)num2));
            if (Math.PI / 2.0 < (double)angle && (double)angle < 3.0 * Math.PI / 2.0)
                num6 = -num6;
            PointF pointF = new PointF();
            pointF.X = num3 + num6;
            pointF.Y = num4 + num6 * num5;
            return pointF;
        }

        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        private RectangleF CalculateCircleBounds(float x, float y, float radius, float aspect)
        {
            if ((double)radius < 0.0)
                Information.Err().Raise(380);
            float num1 = this.coordinateSpace.ToPixelsWidth(radius);
            float num2;
            float num3;
            if ((double)aspect > 0.0)
            {
                if ((double)aspect >= 1.0)
                {
                    num2 = num1 / aspect;
                    num3 = num1;
                }
                else
                {
                    num2 = num1;
                    num3 = num1 * aspect;
                }
            }
            else
            {
                aspect = -aspect;
                num2 = num1;
                num3 = num1 * aspect;
            }
            float num4 = Math.Abs(num2);
            float num5 = Math.Abs(num3);
            float num6 = this.coordinateSpace.ToPixelsX(x);
            float num7 = this.coordinateSpace.ToPixelsY(y);
            RectangleF rectangleF = new Rectangle();
            rectangleF.X = num6 - num4;
            rectangleF.Y = num7 - num5;
            rectangleF.Width = num4 * 2f;
            rectangleF.Height = num5 * 2f;
            return rectangleF;
        }

        private float ToDegree(float angle)
        {
            angle = 6.283185f - Math.Abs(angle);
            return (float)((double)angle * 180.0 / Math.PI);
        }

        private float CalculateSweep(float startAngle, float endAngle)
        {
            float num1 = this.ToDegree(startAngle);
            float num2 = this.ToDegree(endAngle);
            if ((double)num1 > (double)num2)
                return num2 - num1;
            else
                return (float)((double)num2 - (double)num1 - 360.0);
        }
        #endregion
    }
}
