﻿using System;
using System.Collections.Generic;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCSelectClientPrinter : IDisposable
    {
        public FCSelectClientPrinter()
        {
            selectPrinterDialog = new FCSelectClientPrinterDialog();
            selectPrinterDialog.selectPrinter = this;
        }

        private FCSelectClientPrinterDialog selectPrinterDialog;
        private string selectedPrinter;
        public string SelectedPrinter
        {
            get
            {
                return selectedPrinter;
            }
            internal set
            {
                selectedPrinter = value;
            }
        }

        public void ShowDialog()
        {
            this.selectPrinterDialog.ShowDialog();
        }

        public void AddPrinter(string printerName)
        {
            this.selectPrinterDialog.AddClientPrinter(printerName);
        }

        public void Dispose()
        {
            selectPrinterDialog.Dispose();
        }
    }

    internal partial class FCSelectClientPrinterDialog : Form
    {
        internal FCSelectClientPrinter selectPrinter;
        public FCSelectClientPrinterDialog()
        {
            InitializeComponent();
            this.selectPrinter = selectPrinter;
        }

        internal void AddClientPrinter(string printerName)
        {
            this.listView1.Items.Add(printerName);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndex >= 0)
            {
                this.selectPrinter.SelectedPrinter = listView1.Items[listView1.SelectedIndex].Text;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Please select a printer");
            }
        }
    }
}
