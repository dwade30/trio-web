﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public class ParameterObject
    {
        public ParameterObject(dynamic value, bool isRefParameter)
        {
            Value = value;
            IsRefParameter = isRefParameter;
        }
        public dynamic Value { get; set; }
        public bool IsRefParameter { get; set; }

    }


}
