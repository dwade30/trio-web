﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.WebSockets;
using Microsoft.Ajax.Utilities;
using Wisej.Core;
using Wisej.Web;

namespace fecherFoundation
{
    public class PrintingViaWebSockets : IHttpHandler
    {
        public static Dictionary<string, InterCommunicationContexts> ContextHolder = new Dictionary<string, InterCommunicationContexts>();
        private bool firstConnect = false;
        //public string MachineName = string.Empty;
        public string logFile = string.Empty;
        public static bool IsWebSocketConnectionOpen(string machineName)
        {
            // no machine name being used? Connection not open
            if (machineName.IsNullOrWhiteSpace())
            {
                return false;
            }

            // ContextHolder dead? Or is it alive but doesn't have a reference to that machine still? Connection is not open
            if (ContextHolder == null || !ContextHolder.ContainsKey(machineName)) return false;

            // we can get the context from the ContextHolder, but maybe it has no value
            var webSocketContext = ContextHolder[machineName].WebSocketContext;

            // return true only if the websocket exists and is open
            try
            {
                return webSocketContext?.WebSocket != null &&
                       webSocketContext.WebSocket.State == WebSocketState.Open;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public PrintingViaWebSockets()
        {

        }
        #region IHttpHandler
        /// <summary>
        /// First entry when console application connects to WebApp
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (context.IsWebSocketRequest)
                {
                    //us a fistConnect Session variable to print on ClientApp a "Connection established" message
                    //TODO can be surely improved
                    firstConnect = true;
                    context.AcceptWebSocketRequest(ProcessWebSocketRequest);
                }
                else
                {
                    context.Response.StatusCode = 400;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                context.Response.Write(e.GetBaseException().Message);
                context.Response.StatusCode = 500;
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
        #endregion

        /// <summary>
        /// Handling WebSockets communication from the client
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task ProcessWebSocketRequest(AspNetWebSocketContext context)
        {
            //accessing this method for first time we need to save the user, to access the ContextHolder dictionary
            var machineName = context.QueryString["machine"];
            InterCommunicationContexts newContext = new InterCommunicationContexts();
            //saving the WebSocketConext to send data from Wisej app over WebSocketContext to ClientApp
            newContext.WebSocketContext = context;
            newContext.Locker = new object();

            try
            {
                if (ContextHolder.ContainsKey(machineName))
                {
                    //close old WebSocket connection
                    try
                    {
                        if (ContextHolder[machineName].WebSocketContext != null)
                        {
                            if (ContextHolder[machineName].WebSocketContext
                                    .WebSocket != null &&
                                ContextHolder[machineName].WebSocketContext
                                    .WebSocket.State == WebSocketState.Open)
                            {
                                await CloseWebSocketConnection(machineName);
                            }
                        }
                    }
                    catch (ObjectDisposedException ex)
                    {
                        //if the websocket is disposed, it will cause an error but you just need to replace it, not fail
                    }
                    ContextHolder[machineName].WebSocketContext = context;
                }
                else
                {
                    ContextHolder.Add(machineName, newContext);
                }
            }
            catch (Exception e)
            {
                HandleException(e,machineName);
                return;
            }

            WebSocket socket = context.WebSocket;

            while (socket.State == WebSocketState.Open)
            {
                if (socket.State != WebSocketState.Open)
                {
                    break;
                }
                else if (firstConnect)
                {
                    firstConnect = false;
                    await SendResponseThroughWebSocketContext("Connection established for user " + context.QueryString["machine"],machineName);
                }

                bool interfaceUnlocked = false;
                string currentUser = "";
                try
                {
                    // receive the full message
                    while (socket.State == WebSocketState.Open)
                    {
                        currentUser = context.QueryString["machine"];

                        //WriteToLogFile("ReceiveAsync ", currentUser);
                        var fullMessage = await ReceiveFullMessage(socket, CancellationToken.None);
                        WebSocketReceiveResult result = fullMessage.Item1;
                        //if client closed the connection we have to remove the entry from our ContextHolder
                        if (result.CloseStatus == WebSocketCloseStatus.NormalClosure)
                        {
                            if (ContextHolder.ContainsKey(currentUser))
                            {
                                ContextHolder.Remove(currentUser);
                            }
                            WriteToLogFile("NormalClosure ", currentUser);
                            //socket.Dispose();
                            return;
                        }
                        else if (socket.State != WebSocketState.Open)
                        {
                            if (ContextHolder.ContainsKey(currentUser))
                            {
                                ContextHolder.Remove(currentUser);
                            }
                            //socket.Dispose();
                            return;
                        }
                        //extract informations form client and release WebApp loop
                        //string incoming = Encoding.UTF8.GetString(buffer, 0, result.Count);
                        string incoming = Encoding.UTF8.GetString(fullMessage.Item2.ToArray(), 0, fullMessage.Item2.Count());
                        if (incoming.StartsWith("{"))
                        {
                            WriteToLogFile("process incoming ", currentUser);
                            dynamic dynPO = null;
                            try
                            {
                                dynPO = WisejSerializer.Parse(incoming);
                            }
                            catch (Exception ex)
                            {
                                //cannot parse result
                            }
                            if (dynPO != null)
                            {
                                ParameterObjects resultPO = ContextHolder[currentUser].poResult;
                                resultPO.ReturnValue = Convert.ToInt32(dynPO.ReturnValue);
                                resultPO.FunctionToExecute = Enum.Parse(typeof(FunctionToExecute), dynPO.FunctionToExecute, true);

                                for (int i = 0; i < resultPO.Parameter.Length; i++)
                                {
                                    if (dynPO.Parameter?[i] != null)
                                    {
                                        resultPO.Parameter[i] = new ParameterObject(dynPO.Parameter[i].Value, (bool)dynPO.Parameter[i].IsRefParameter);
                                    }
                                }
                                ContextHolder[currentUser].poResult = resultPO;
                                if (ContextHolder[currentUser].poResult != null)
                                {
                                    {
                                        //http://www.c-sharpcorner.com/UploadFile/1d42da/threading-with-monitor-in-C-Sharp/
                                        lock (ContextHolder[currentUser].Locker)
                                        {
                                            //release the blocking loop on WebApp
                                            ContextHolder[currentUser].ResultReceived = true;
                                            Monitor.Pulse(ContextHolder[currentUser].Locker);
                                        }
                                        interfaceUnlocked = true;
                                    }
                                }
                            }
                        }

                        if (result.EndOfMessage)
                        {
                            break;
                        }
                    }
                }
                catch (Exception e2)
                {
                    HandleException(e2,currentUser);
                    if (!interfaceUnlocked)
                    {
                        if (ContextHolder.ContainsKey(currentUser))
                        {
                            //http://www.c-sharpcorner.com/UploadFile/1d42da/threading-with-monitor-in-C-Sharp/
                            lock (ContextHolder[currentUser].Locker)
                            {
                                //release the blocking loop on WebApp
                                ContextHolder[currentUser].ResultReceived = true;
                                Monitor.Pulse(ContextHolder[currentUser].Locker);
                            }
                        }
                        interfaceUnlocked = true;
                    }
                    break;
                }
                finally
                {
                    if (!interfaceUnlocked)
                    {
                        //http://www.c-sharpcorner.com/UploadFile/1d42da/threading-with-monitor-in-C-Sharp/
                        if (ContextHolder.ContainsKey(currentUser))
                        {
                            lock (ContextHolder[currentUser].Locker)
                            {
                                //release the blocking loop on WebApp
                                ContextHolder[currentUser].ResultReceived = true;
                                Monitor.Pulse(ContextHolder[currentUser].Locker);
                            }
                        }

                        interfaceUnlocked = true;
                    }
                }
            }

        }

        //https://www.codetinkerer.com/2018/06/05/aspnet-core-websockets.html
        private async Task<(WebSocketReceiveResult, IEnumerable<byte>)> ReceiveFullMessage(WebSocket socket, CancellationToken cancelToken)
        {
            WebSocketReceiveResult response;
            var message = new List<byte>();

            var buffer = new byte[4096];
            do
            {
                response = await socket.ReceiveAsync(new ArraySegment<byte>(buffer), cancelToken);
                message.AddRange(new ArraySegment<byte>(buffer, 0, response.Count));
            }
            while (!response.EndOfMessage);

            return (response, message);
        }

        public async Task SendResponseThroughWebSocketContext(string message,string machineName)
        {
            string currentUser = machineName;
            //if CurrentUser client is not registered, we cannot send a Response to it
            if (currentUser == null || !ContextHolder.ContainsKey(currentUser))
            {
                WriteToLogFile("!ContextHolder.ContainsKey ", currentUser);
                return;
            }

            if (ContextHolder[currentUser] != null)
            {
                try
                {
                    WebSocket socket = ContextHolder[currentUser].WebSocketContext.WebSocket;
                    if (socket.State == WebSocketState.Open)
                    {
                        WriteToLogFile("SendResponseThroughWebSocketContext ", currentUser);
                        ArraySegment<byte> outputBuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));
                        await socket.SendAsync(outputBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                    }
                }
                catch (Exception e)
                {
                    HandleException(e,currentUser);
                }
            }
        }

        public static async Task SendResponseThroughWisejContext(ParameterObjects p, string machineName)
        {
            string currentUser = machineName;
            string message = string.Empty;

            //if CurrentUser client is not registered, we cannot send a Response to it
            if (currentUser == null || !ContextHolder.ContainsKey(currentUser))
            {
                WriteToLogFile("!ContextHolder.ContainsKey ", currentUser);
                return;
            }

            // save the WisejContext
            //if (ContextHolder[currentUser].WisejContext == null)
            //{
            //    ContextHolder[currentUser].WisejContext = Application.Current;
            //}

            if (ContextHolder[currentUser] != null)
            {
                ContextHolder[currentUser].poResult = p;

                try
                {
                    WebSocket socket = ContextHolder[currentUser].WebSocketContext.WebSocket;
                    if (socket.State == WebSocketState.Open)
                    {
                        ContextHolder[currentUser].ResultReceived = false;
                        message = Wisej.Core.WisejSerializer.Serialize(p, WisejSerializerOptions.None | WisejSerializerOptions.IgnoreNulls);

                        WriteToLogFile("SendAsync ", currentUser);

                        ArraySegment<byte> outputBuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));
                        await socket.SendAsync(outputBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                    }
                }
                catch (Exception e)
                {
                    HandleException(e,machineName);
                }
            }
        }

        /// <summary>
        /// This function will be called when executing a print command against the printer on client
        /// Therefore a ParamterObjects has to be given to the function which holds the FunctionToExecute and the 
        /// parameters.
        /// The ParameterObjects instance will be send to client, before it will be serialized with the WisjSerializer
        /// As long as no result receives the Server, the WebApp will be blocked
        /// </summary>
        /// <param name="p"></param>
        public static Exception CheckResultReceived(ref ParameterObjects p, string machineName)
        {
            string currentUser = machineName;
            if (currentUser == null || !ContextHolder.ContainsKey(currentUser))
            {
                currentUser = currentUser ?? "null";
                WriteToLogFile("CheckResultReceived: User not registered", currentUser);
                p.ReturnValue = 0;
                return new Exception($"User '{currentUser}' not registered");
            }

            WriteToLogFile("CheckResultReceived start ", currentUser);
            SendResponseThroughWisejContext(p,currentUser);

            //http://www.c-sharpcorner.com/UploadFile/1d42da/threading-with-monitor-in-C-Sharp/
            lock (ContextHolder[currentUser].Locker)
            {
                Monitor.Wait(ContextHolder[currentUser].Locker);
            }

            WriteToLogFile("CheckResultReceived end ", currentUser);
            p = ContextHolder[currentUser].poResult;
            if (ContextHolder[currentUser].Exception != null)
            {
                p.ReturnValue = 0;
            }
            //if something went wrong, we return the exception
            return ContextHolder[currentUser].Exception;

        }
        /// <summary>
        /// Close the WS from the WebApp. 
        /// Calling this from a button click will send a close to the ClientApp, there the Receiving loop will be skiped and the 
        /// WebSocket connection will also send NormalClosure via CloseOutputAsync call, then the WebApp receives it and 
        /// can also break the Receiving loop
        /// </summary>
        public async Task CloseWebSocketConnection(string currentUser = "")
        {
            if (string.IsNullOrEmpty(currentUser))
            {
                return;
            }

            try
            {
                if (ContextHolder.ContainsKey(currentUser) && ContextHolder[currentUser].WebSocketContext.WebSocket != null &&
                    ContextHolder[currentUser].WebSocketContext.WebSocket.State == WebSocketState.Open)
                {
                    await ContextHolder[currentUser].WebSocketContext.WebSocket
                        .CloseOutputAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                    ContextHolder.Remove(currentUser);
                }
            }
            catch (ObjectDisposedException ex)
            {

            }
        }

        public static void WriteToLogFile(string message, string currentUser)
        {
            Debug.WriteLine(currentUser == null ? $"NULL user:{message}" : $"{currentUser}:{message}");
        }

        private static void HandleException(Exception e,string currentUser)
        {
            if (string.IsNullOrEmpty(currentUser))
            {
                return;
            }

            if (!ContextHolder.ContainsKey(currentUser))
            {
                return;
            }
            ContextHolder[currentUser].Exception = e;
            ContextHolder[currentUser].ResultReceived = true;
        }

    }

    public class PrintingViaWebSocketsVariables
    {
        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }

        public class StaticVariables
        {
            private string machineName;
            public string MachineName
            {
                get
                {
                    return this.machineName;
                }
                set
                {
                    this.machineName = value;
                }
            }
        }
    }
}
