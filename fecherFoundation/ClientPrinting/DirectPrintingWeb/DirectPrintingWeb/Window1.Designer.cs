﻿namespace DirectPrintingWeb
{
    partial class Window1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSend = new Wisej.Web.Button();
            this.txtSend = new Wisej.Web.TextBox();
            this.lbReceive = new Wisej.Web.ListBox();
            this.txtUser = new Wisej.Web.TextBox();
            this.btnSetCurrentUser = new Wisej.Web.Button();
            this.btnGetClientPrinters = new Wisej.Web.Button();
            this.btnPrintPDF = new Wisej.Web.Button();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(290, 102);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(205, 23);
            this.btnSend.TabIndex = 0;
            this.btnSend.Text = "Send to printer";
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // txtSend
            // 
            this.txtSend.Location = new System.Drawing.Point(63, 103);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(204, 22);
            this.txtSend.TabIndex = 1;
            // 
            // lbReceive
            // 
            this.lbReceive.Location = new System.Drawing.Point(63, 259);
            this.lbReceive.Name = "lbReceive";
            this.lbReceive.Size = new System.Drawing.Size(615, 249);
            this.lbReceive.TabIndex = 2;
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(63, 24);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(204, 22);
            this.txtUser.TabIndex = 3;
            // 
            // btnSetCurrentUser
            // 
            this.btnSetCurrentUser.Location = new System.Drawing.Point(290, 23);
            this.btnSetCurrentUser.Name = "btnSetCurrentUser";
            this.btnSetCurrentUser.Size = new System.Drawing.Size(205, 22);
            this.btnSetCurrentUser.TabIndex = 4;
            this.btnSetCurrentUser.Text = "SetCurrentUser";
            this.btnSetCurrentUser.Click += new System.EventHandler(this.btnSetCurrentUser_Click);
            // 
            // btnGetClientPrinters
            // 
            this.btnGetClientPrinters.Location = new System.Drawing.Point(290, 68);
            this.btnGetClientPrinters.Name = "btnGetClientPrinters";
            this.btnGetClientPrinters.Size = new System.Drawing.Size(205, 23);
            this.btnGetClientPrinters.TabIndex = 5;
            this.btnGetClientPrinters.Text = "Get Client Printers";
            this.btnGetClientPrinters.Click += new System.EventHandler(this.btnGetClientPrinters_Click);
            // 
            // btnPrintPDF
            // 
            this.btnPrintPDF.Location = new System.Drawing.Point(291, 175);
            this.btnPrintPDF.Name = "btnPrintPDF";
            this.btnPrintPDF.Size = new System.Drawing.Size(203, 23);
            this.btnPrintPDF.TabIndex = 6;
            this.btnPrintPDF.Text = "Close WS";
            this.btnPrintPDF.Click += new System.EventHandler(this.btnPrintPDF_Click);
            // 
            // Window1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1170, 626);
            this.Controls.Add(this.btnPrintPDF);
            this.Controls.Add(this.btnGetClientPrinters);
            this.Controls.Add(this.btnSetCurrentUser);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.lbReceive);
            this.Controls.Add(this.txtSend);
            this.Controls.Add(this.btnSend);
            this.Name = "Window1";
            this.Text = "Window1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Wisej.Web.Button btnSend;
        private Wisej.Web.TextBox txtSend;
        private Wisej.Web.ListBox lbReceive;
        private Wisej.Web.TextBox txtUser;
        private Wisej.Web.Button btnSetCurrentUser;
        private Wisej.Web.Button btnGetClientPrinters;
        private Wisej.Web.Button btnPrintPDF;
    }
}

