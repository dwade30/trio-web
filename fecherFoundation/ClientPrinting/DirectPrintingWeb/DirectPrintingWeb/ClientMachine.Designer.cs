﻿namespace DirectPrintingWeb
{
    partial class ClientMachine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtClientMachine = new Wisej.Web.TextBox();
            this.btnSaveClientMachineName = new Wisej.Web.Button();
            this.SuspendLayout();
            // 
            // txtClientMachine
            // 
            this.txtClientMachine.Location = new System.Drawing.Point(114, 123);
            this.txtClientMachine.Name = "txtClientMachine";
            this.txtClientMachine.Size = new System.Drawing.Size(255, 22);
            this.txtClientMachine.TabIndex = 0;
            this.txtClientMachine.Watermark = "Enter client machine name";
            // 
            // btnSaveClientMachineName
            // 
            this.btnSaveClientMachineName.Location = new System.Drawing.Point(111, 172);
            this.btnSaveClientMachineName.Name = "btnSaveClientMachineName";
            this.btnSaveClientMachineName.Size = new System.Drawing.Size(258, 28);
            this.btnSaveClientMachineName.TabIndex = 1;
            this.btnSaveClientMachineName.Text = "Save client machine name";
            this.btnSaveClientMachineName.Click += new System.EventHandler(this.btnSaveClientMachineName_Click);
            // 
            // ClientMachine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 480);
            this.Controls.Add(this.btnSaveClientMachineName);
            this.Controls.Add(this.txtClientMachine);
            this.Name = "ClientMachine";
            this.Text = "ClientMachine";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Wisej.Web.TextBox txtClientMachine;
        private Wisej.Web.Button btnSaveClientMachineName;
    }
}