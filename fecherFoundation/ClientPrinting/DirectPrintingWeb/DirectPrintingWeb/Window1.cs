﻿
using fecherFoundation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Wisej.Web;
using Global;
using System.Text;

namespace DirectPrintingWeb
{
    public partial class Window1 : Form
    {
        

        public Window1()
        {
            InitializeComponent();
            Wisej.Base.Cookie c = Application.Cookies.Get("clientMachineName");
            if (c != null && c.Value != null)
            {
                string ClientMachineName = Application.Cookies.Get("clientMachineName").Value;
                if (!string.IsNullOrEmpty(ClientMachineName))
                {
                    this.txtUser.Text = ClientMachineName;
                }
                else
                {
                    this.txtUser.Text = string.Empty;
                }
            }
           
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            SendStringToPrinter(lbReceive.SelectedItem.ToString(), this.txtSend.Text);
        }

        private void btnSetCurrentUser_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtUser.Text))
            {
                ClientMachine m = new ClientMachine();
                m.ShowDialog(this);
                string ClientMachineName = Application.Cookies.Get("clientMachineName").Value;
                if (!string.IsNullOrEmpty(ClientMachineName))
                {
                    this.txtUser.Text = ClientMachineName;
                }
            }
            PrintingViaWebSockets.PrintingViaWebSocketsInstance.MachineName = this.txtUser.Text;
        }

        private void btnGetClientPrinters_Click(object sender, EventArgs e)
        {
            List<Printer> printers = new List<Printer>();
            ParameterObjects po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printers, true);
            po.FunctionToExecute = FunctionToExecute.GetPrinters;
            this.lbReceive.Items.Clear();
            Exception returnException = fecherFoundation.PrintingViaWebSockets.PrintingViaWebSocketsInstance.CheckResultReceived(ref po);
            if (returnException == null)
            {
                try
                {
                    if (po.Parameter[0].Value.GetType() == typeof(Wisej.Core.DynamicObject[]))
                    {
                        Printer p = null;
                        for (int i = 0; i < po.Parameter[0].Value.Length; i++)
                        {
                            p = new Printer();
                            p.Name = po.Parameter[0].Value[i].Name;
                            p.DriverName = po.Parameter[0].Value[i].DriverName;
                            p.PortName = po.Parameter[0].Value[i].PortName;
                            printers.Add(p);
                        }
                    }
                    foreach (var item in printers)
                    {
                        this.lbReceive.Items.Add(item.Name);
                    }
                }
                catch (Exception ex)
                {
                    PrintingViaWebSockets.PrintingViaWebSocketsInstance.WriteToLogFile("Exception: " + ex.Message, PrintingViaWebSockets.PrintingViaWebSocketsInstance.MachineName);
                }
            }
            else
            {
                this.lbReceive.Items.Add(returnException.Message);
            }
        }


        // SendBytesToPrinter()
        // When the function is given a printer name and an unmanaged array
        // of bytes, the function sends those bytes to the print queue.
        // Returns true on success, false on failure.
        public static bool SendBytesToPrinter(string szPrinterName, byte[] pBytes, Int32 dwCount)
        {
            Int32 dwError = 0, dwWritten = 0;
            int hPrinter = 0;
            clsPrinterFunctions.DOCINFO di = new clsPrinterFunctions.DOCINFO();
            bool bSuccess = false; // Assume failure unless you specifically succeed.
            di.pDocName = "My C#.NET RAW Document";
            di.pDatatype = "RAW";

            // Open the printer.
            if (clsPrinterFunctions.OpenPrinter(szPrinterName, ref hPrinter, 0) > 0)
            {
                // Start a document.
                if (clsPrinterFunctions.StartDocPrinter(hPrinter, 1, ref di) > 0)
                {
                    // Start a page.
                    if (clsPrinterFunctions.StartPagePrinter(hPrinter) > 0)
                    {
                        // Write your bytes.
                        bSuccess = clsPrinterFunctions.WritePrinter(hPrinter, Encoding.UTF8.GetString(pBytes), dwCount, ref dwWritten) > 0;
                        clsPrinterFunctions.EndPagePrinter(hPrinter);
                    }
                    clsPrinterFunctions.EndDocPrinter(hPrinter);
                }
                clsPrinterFunctions.ClosePrinter(hPrinter);
            }
            // If you did not succeed, GetLastError may give more information
            // about why not.
            if (bSuccess == false)
            {
                //dwError = Marshal.GetLastWin32Error();
            }
            return bSuccess;
        }

        public static bool SendFileToPrinter(string szPrinterName, string szFileName)
        {
            Byte[] bytes;
            bool bSuccess = false;
            int nLength;

            // Open the file.
            using (FileStream fs = new FileStream(szFileName, FileMode.Open))
            using (BinaryReader br = new BinaryReader(fs))
            {
                bytes = new Byte[fs.Length];
                nLength = Convert.ToInt32(fs.Length);
                // Read the contents of the file into the array.
                bytes = br.ReadBytes(nLength);
                
            }
            bSuccess = SendBytesToPrinter(szPrinterName, bytes, nLength);
            // Free the unmanaged memory that you allocated earlier.
            //Marshal.FreeCoTaskMem(pUnmanagedBytes);
            return bSuccess;
        }
        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
            Int32 dwCount;
            dwCount = szString.Length;
            SendBytesToPrinter(szPrinterName, Encoding.UTF8.GetBytes(szString), dwCount);
            return true;
        }

        private void btnPrintPDF_Click(object sender, EventArgs e)
        {
            //SendFileToPrinter(lbReceive.SelectedItem.ToString(), Application.MapPath("Files/Plan.pdf"));
            PrintingViaWebSockets.PrintingViaWebSocketsInstance.CloseWebSocketConnection();
            PrintingViaWebSockets.PrintingViaWebSocketsInstance.WriteToLogFile(": User logged out " + DateTime.Now.ToShortTimeString().ToString(), PrintingViaWebSockets.PrintingViaWebSocketsInstance.MachineName);
        }
    }
}
