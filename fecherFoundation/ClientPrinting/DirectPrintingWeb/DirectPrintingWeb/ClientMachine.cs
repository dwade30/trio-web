﻿using System;
using Wisej.Web;

namespace DirectPrintingWeb
{
    public partial class ClientMachine : Form
    {
        public ClientMachine()
        {
            InitializeComponent();
        }

        private void btnSaveClientMachineName_Click(object sender, EventArgs e)
        {
            Application.Cookies.Add("clientMachineName", txtClientMachine.Text, DateTime.Now.AddMonths(1));
        }
    }
}
