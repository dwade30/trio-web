﻿namespace fecherFoundation
{
    partial class FCSelectClientPrinterDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new Wisej.Web.ListView();
            this.columnHeader1 = new Wisej.Web.ColumnHeader();
            this.btnOk = new Wisej.Web.Button();
            this.btnCancel = new Wisej.Web.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new Wisej.Web.ColumnHeader[] {
            this.columnHeader1});
            this.listView1.Location = new System.Drawing.Point(30, 20);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(450, 300);
            this.listView1.TabIndex = 0;
            this.listView1.View = Wisej.Web.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Image = null;
            this.columnHeader1.Name = "columnHeader1";
            this.columnHeader1.Text = "Printer name";
            this.columnHeader1.Width = 400;
            // 
            // btnOk
            // 
            this.btnOk.AppearanceKey = "actionButton";
            this.btnOk.DialogResult = Wisej.Web.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(30, 341);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 40);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AppearanceKey = "actionButton";
            this.btnCancel.DialogResult = Wisej.Web.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(136, 341);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 40);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FCSelectClientPrinter
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(508, 404);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.listView1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FCSelectClientPrinter";
            this.Text = "Select printer";
            this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.ListView listView1;
        private Wisej.Web.ColumnHeader columnHeader1;
        private Wisej.Web.Button btnOk;
        private Wisej.Web.Button btnCancel;
    }
}