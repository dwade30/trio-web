﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    //Enum to avoid work with strings to determine the FunctionToExecute
    public enum FunctionToExecute
    {
        AddForm,
        ClosePrinter,
        DeleteForm,
        DocumentProperties,
        EndDocPrinter,
        EndPagePrinter,
        EnumForms,
        GetForm,
        GetPrinters,
        OpenPrinter,
        SetForm,
        StartDocPrinter,
        StartPagePrinter,
        WritePrinter,
        PrintRtf,
        PrintTiff,
        PrintRDF,
        PrinterForward,
        PrinterReverse,
        PrintXs,
        DownloadTRIOSketchFile
       , GetMachinesPrinters
        , SaveMachinesPrinters,
        GetApplicationKey,
        SaveApplicationKey
    }

    public class ParameterObjects
    {
        public FunctionToExecute FunctionToExecute { get; set; }
        public int ReturnValue { get; set; }
        public ParameterObject[] Parameter = new ParameterObject[6];
    }
}
