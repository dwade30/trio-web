﻿
using System;
using System.Threading;

namespace DirectPrintingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.Sleep(1000);
            Console.WriteLine("Waiting for connection ...");
            Communication.Connect("ws://" + args[0]).Wait();
            Console.ReadKey();
        }
    }
}
