﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DirectPrintingApp
{
    public class clsPrinterFunctions
    {
        private static int printerHandle = 0;

        // DEFINED TYPE FOR THE WRITEPRINTER API FUNCTION
        [StructLayout(LayoutKind.Sequential)]
        private struct DOCINFO
        {
            public string pDocName;
            public string pOutputFile;
            public string pDatatype;
        };
        #region API FUNCTION CALLS
        [DllImport("winspool.drv")]
        private static extern int ClosePrinter(int hPrinter);
        [DllImport("winspool.drv")]
        private static extern int EndDocPrinter(int hPrinter);
        [DllImport("winspool.drv")]
        private static extern int EndPagePrinter(int hPrinter);
        //VBtoInfo: 0/2
        [DllImport("winspool.drv", EntryPoint = "OpenPrinterA")]
        private static extern int OpenPrinter(string pPrinterName, out int phPrinter, int pDefault);
        [DllImport("winspool.drv", EntryPoint = "StartDocPrinterA")]
        private static extern int StartDocPrinter(int hPrinter, int Level, ref DOCINFO pDocInfo);
        [DllImport("winspool.drv")]
        private static extern int StartPagePrinter(int hPrinter);
        [DllImport("winspool.drv")]
        private static extern int WritePrinter(int hPrinter, byte[] pBuf, int cdBuf, ref int pcWritten);
        [DllImport("winspool.drv", EntryPoint = "EnumFormsA")]
        public static extern int EnumForms(int hPrinter, int Level, ref byte pForm, int cbBuf, ref int pcbNeeded, ref int pcReturned);
        [DllImport("winspool.drv", EntryPoint = "AddFormA")]
        public static extern int AddForm(int hPrinter, int Level, ref byte pForm);
        //VBtoInfo: 0/2
        [DllImport("winspool.drv", EntryPoint = "DeleteFormA")]
        public static extern int DeleteForm(int hPrinter, IntPtr pFormName);
        //VBtoInfo: 0/4
        [DllImport("winspool.drv", EntryPoint = "DocumentPropertiesA")]
        public static extern int DocumentProperties(int hwnd, int hPrinter, string pDeviceName, ref short pDevModeOutput, ref short pDevModeInput, int fMode);
        [DllImport("winspool.drv", EntryPoint = "GetFormA")]
        public static extern int GetForm(int hPrinter, string pFormName, int Level, ref byte pForm, int cbBuf, ref int pcbNeeded);
        [DllImport("winspool.drv", EntryPoint = "SetFormA")]
        public static extern int SetForm(int hPrinter, string pFormName, int Level, ref byte pForm);

        #endregion

        public static async Task ExecutePrintFunction(ParameterObjects po)
        {
            int returnValaue = 0;
            try
            {
                switch (po.FunctionToExecute)
                {
                    case FunctionToExecute.ClosePrinter:
                        returnValaue = ClosePrinter(po.Parameter[0].Value);
                        break;
                    case FunctionToExecute.EndDocPrinter:
                        returnValaue = EndDocPrinter(po.Parameter[0].Value);
                        break;
                    case FunctionToExecute.EndPagePrinter:
                        returnValaue = EndPagePrinter(po.Parameter[0].Value);
                        break;
                    case FunctionToExecute.OpenPrinter:
                        int refPHPrinter = po.Parameter[1].Value;
                        returnValaue = OpenPrinter(po.Parameter[0].Value, out refPHPrinter, po.Parameter[2].Value);
                        po.Parameter[1].Value = refPHPrinter;
                        if (returnValaue != 0)
                        {
                            printerHandle = refPHPrinter;
                        }
                        break;
                    case FunctionToExecute.StartDocPrinter:
                        DOCINFO refDOCINFO;
                        refDOCINFO.pDatatype = po.Parameter[2].Value.pDatatype;
                        refDOCINFO.pDocName = po.Parameter[2].Value.pDocName;
                        refDOCINFO.pOutputFile = po.Parameter[2].Value.pOutputFile;
                        returnValaue = StartDocPrinter(po.Parameter[0].Value, po.Parameter[1].Value, ref refDOCINFO);
                        po.Parameter[2].Value = refDOCINFO;
                        break;
                    case FunctionToExecute.StartPagePrinter:
                        returnValaue = StartPagePrinter(po.Parameter[0].Value);
                        break;
                    case FunctionToExecute.WritePrinter:
                        int refPCWritten = po.Parameter[3].Value;
                        byte[] textBuffer = Encoding.UTF8.GetBytes(po.Parameter[1].Value);
                        returnValaue = WritePrinter(po.Parameter[0].Value, textBuffer, po.Parameter[2].Value, ref refPCWritten);
                        po.Parameter[3].Value = refPCWritten;
                        //don't send data to server
                        po.Parameter[1].Value = new byte[0];
                        break;
                    case FunctionToExecute.EnumForms:
                        byte refPForm = po.Parameter[2].Value;
                        int refPCBNeeded = po.Parameter[4].Value;
                        int refPCReturned = po.Parameter[5].Value;
                        returnValaue = EnumForms(po.Parameter[0].Value, po.Parameter[1].Value, ref refPForm, po.Parameter[3].Value, ref refPCBNeeded, ref refPCReturned);
                        po.Parameter[2].Value = refPForm;
                        po.Parameter[4].Value = refPCBNeeded;
                        po.Parameter[5].Value = refPCReturned;
                        break;
                    case FunctionToExecute.AddForm:
                        byte refPForm1 = po.Parameter[2].Value;
                        returnValaue = AddForm(po.Parameter[0].Value, po.Parameter[1].Value, ref refPForm1);
                        po.Parameter[2].Value = refPForm1;
                        break;
                    case FunctionToExecute.DeleteForm:
                        //returnValaue = DeleteForm(po.Parameter[0].Value, (IntPtr)po.Parameter[1].Value);
                        break;
                    case FunctionToExecute.DocumentProperties:
                        short refPDevModeOutput = po.Parameter[3].Value;
                        short refPDevModeInput = po.Parameter[4].Value;
                        returnValaue = DocumentProperties((int)po.Parameter[0].Value, po.Parameter[1].Value, po.Parameter[2].Value, ref refPDevModeOutput, ref refPDevModeInput, po.Parameter[5].Value);
                        po.Parameter[3].Value = refPDevModeOutput;
                        po.Parameter[4].Value = refPDevModeInput;
                        break;
                    case FunctionToExecute.GetForm:
                        byte refPForm2 = po.Parameter[3].Value;
                        int refPCBNeeded1 = po.Parameter[5].Value;
                        returnValaue = GetForm(po.Parameter[0].Value, po.Parameter[1].Value, po.Parameter[2].Value, ref refPForm2, po.Parameter[4].Value, ref refPCBNeeded1);
                        po.Parameter[3].Value = refPForm2;
                        po.Parameter[5].Value = refPCBNeeded1;
                        break;
                    case FunctionToExecute.SetForm:
                        byte refPForm3 = po.Parameter[3].Value;
                        returnValaue = SetForm(po.Parameter[0].Value, po.Parameter[1].Value, po.Parameter[2].Value, ref refPForm3);
                        po.Parameter[3].Value = refPForm3;
                        break;
                    case FunctionToExecute.GetPrinters:
                        List<Printer> printers = new List<Printer>();
                        returnValaue = GetPrinters(ref printers);
                        po.Parameter[0].Value = printers;
                        break;
                    default:
                        break;
                }
            }
            catch(Exception e)
            {
                ClosePrinter(printerHandle);
                Console.WriteLine(e.Message);
            }
            for (int i = 0; i < po.Parameter.Length; i++)
            {
                if (po.Parameter[i] != null && !po.Parameter[i].IsRefParameter)
                {
                    po.Parameter[i].Value = null;
                }
            }
            po.ReturnValue = returnValaue;
            await Communication.Send(WisejSerializer.Serialize(po, WisejSerializerOptions.None | WisejSerializerOptions.IgnoreNulls));
        }

        private static int GetPrinters(ref List<Printer> printers)
        {
            ObjectQuery oquery = new ObjectQuery("SELECT * FROM Win32_Printer");
            ManagementObjectSearcher mosearcher = new ManagementObjectSearcher(oquery);
            foreach (ManagementObject mo in mosearcher.Get())
            {
                //check for virtual printers
                //https://msdn.microsoft.com/en-us/library/aa394363(v=vs.85).aspx
                string port = mo["PortName"].ToString();
                if (port != "nul:" && //send to onenote has this port
                    port != "PORTPROMPT:" && //microsoft xps and microsoft print to pdf has this port
                    port != "SHRFAX:") // fax printer has this port
                {                    
                    Printer p = new Printer();
                    p.Name = mo["Name"].ToString();
                    p.DriverName = mo["DriverName"].ToString();
                    p.PortName = mo["PortName"].ToString();
                    printers.Add(p);
                }
            }
            return 1;
        }

    }
}
