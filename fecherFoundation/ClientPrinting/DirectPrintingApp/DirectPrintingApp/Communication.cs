﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DirectPrintingApp
{
    public static class Communication
    {
        private static object consoleLock = new object();
        private const int receiveChunkSize = 0x2000;
        private const int BufferAmplifier = 20;
        private const bool verbose = true;
        private static readonly TimeSpan delay = TimeSpan.FromMilliseconds(1000);

        private static ClientWebSocket webSocket = null;


        public static async Task Connect(string uri)
        {            
            if (webSocket != null && webSocket.State == WebSocketState.Open)
            {
                await webSocket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
            }

            try
            {
                Console.WriteLine("hostname: {0}", Dns.GetHostName());
                Console.WriteLine("Copy hostname and follow the steps in the web application, after doing this press any key to go on.");
                Console.ReadKey();
                uri += Dns.GetHostName();
                webSocket = new ClientWebSocket();
                await webSocket.ConnectAsync(new Uri(uri), CancellationToken.None);
                //await Task.WhenAll(Receive(webSocket), Send(webSocket));
                await Task.WhenAll(Receive(webSocket));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex);
            }
            finally
            {
                if (webSocket != null)
                {
                    await webSocket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                    //webSocket.Dispose();
                }
                Console.WriteLine();

                lock (consoleLock)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("WebSocket closed.");
                    Console.ResetColor();
                }
            }
        }

        private static async Task Send(ClientWebSocket webSocket)
        {
            //MemoryStream Buffer = new MemoryStream();
            while (webSocket.State == WebSocketState.Open)
            {
                ArraySegment<byte> outputBuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(Console.ReadLine()));
                await webSocket.SendAsync(outputBuffer, WebSocketMessageType.Binary, false, CancellationToken.None);
                LogStatus(false, outputBuffer.ToArray<byte>(), outputBuffer.Count);
            }
        }

        public static async Task Send(string message)
        {
            if (webSocket.State == WebSocketState.Open)
            {
                ArraySegment<byte> outputBuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));
                await webSocket.SendAsync(outputBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                //LogStatus(false, outputBuffer.ToArray<byte>(), outputBuffer.Count);
            }
        }

        private static async Task Receive(ClientWebSocket webSocket)
        {
            var temporaryBuffer = new byte[receiveChunkSize];
            var buffer = new byte[1024 * 1024]; // 1MB buffer
            var offset = 0;

            //byte[] buffer = new byte[receiveChunkSize];
            //byte[] bigbuffer = new byte[receiveChunkSize];
            while (webSocket.State == WebSocketState.Open || webSocket.State == WebSocketState.CloseSent)
            {
                WebSocketReceiveResult result;
                //data will be send into chunks, if the data is to big, so we need to collect them all
                do
                {
                    result = await webSocket.ReceiveAsync(new ArraySegment<byte>(temporaryBuffer), CancellationToken.None);
                    temporaryBuffer.CopyTo(buffer, offset);
                    offset += result.Count;
                    temporaryBuffer = new byte[receiveChunkSize];
                } while (!result.EndOfMessage);
                if (result.CloseStatus == WebSocketCloseStatus.NormalClosure)
                {

                }
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    Console.WriteLine("Close loop complete");
                    break;
                }
                else
                {
                    string incoming = Encoding.UTF8.GetString(buffer, 0, offset);
                    

                    if (incoming.StartsWith("{"))
                    {
                        try
                        {
                            dynamic dynPO = WisejSerializer.Parse(incoming);
                            if (dynPO != null)
                            {
                                ParameterObjects po = new ParameterObjects();
                                po.FunctionToExecute = Enum.Parse(typeof(FunctionToExecute), dynPO.FunctionToExecute, true);
                                for (int i = 0; i < po.Parameter.Length; i++)
                                {
                                    if (dynPO.Parameter?[i] != null)
                                    {
                                        po.Parameter[i] = new ParameterObject((object)dynPO.Parameter[i].Value, (bool)dynPO.Parameter[i].IsRefParameter);
                                    }

                                }
                                await clsPrinterFunctions.ExecutePrintFunction(po);
                            }

                        }
                        catch (Exception e)
                        {
                            LogStatus(true, Encoding.UTF8.GetBytes(e.Message), e.Message.Length);
                        }
                    }                    
                    LogStatus(true, buffer, result.Count);

                    buffer = new byte[1024 * 1024];
                    offset = 0;
                }
            }
        }

        private static void LogStatus(bool receiving, byte[] buffer, int length)
        {
            lock (consoleLock)
            {
                Console.ForegroundColor = receiving ? ConsoleColor.Green : ConsoleColor.Gray;
                Console.WriteLine("{0} {1} bytes... ", receiving ? "Received" : "Sent", length);

                if (verbose)
                    //Console.WriteLine(BitConverter.ToString(buffer, 0, length));
                    Console.WriteLine(Encoding.UTF8.GetString(buffer, 0, length));

                Console.ResetColor();
            }
        }
    }
}
