using DirectPrintingApp;
using Microsoft.Win32;
using SharedApplication.Telemetry;
using System;
using System.Configuration;
using System.Deployment.Application;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using TRIOAssistant.Properties;

namespace TRIOAssistant
{
    public partial class TRIOAssistant : Form
    {
        //use the Publisher name and Product name from Publish settings. Cannot be loaded programatically
        //Please update this lines when Publisher/Product is changed in Publish settings
        const string PUBLISHER_NAME = "Harris Computer";
        const string PRODUCT_NAME = "TRIOAssistant";
        private string miniServerPort = "44850";
        const string PRINTING_SERVICE_HANDLER = "printingviawebsockets.ashx";
        private winskt.WinSkt appWinSketch;
        private string sketchTempFolder = string.Empty;
        private Bitmap lastSketchBitmap = null;
        private dynamic sketchData;
        private delegate void SafeCallDelegate(string text);

        private int checkPrint;
        private int tiffCurrentTop = 0;
        string websocketURL = string.Empty;
        internal string PrintTiffFileName = "";
        private long sktFileLastUpdateTicks = 0;
        private bool allowVisible;
        private bool allowClose;
        private bool forceDisconnect = false;

        public string SketchFileName { get; set; }

        public Version AssemblyVersion => ApplicationDeployment.CurrentDeployment.CurrentVersion;

        public ITelemetryService _Telemetry;

        public TRIOAssistant()
        {
            InitializeComponent();
            TRIOAssistant_Init();
        }

        protected override void SetVisibleCore(bool value)
        {
            if (!allowVisible)
            {
                value = false;
                if (!IsHandleCreated) CreateHandle();
            }
            base.SetVisibleCore(value);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (!allowClose)
            {
                MinimizeToTray();
                e.Cancel = true;
            }
            base.OnFormClosing(e);
        }

        public void LogError(string message)
        {
            logTextBox.AppendText(
                string.Format("{0}:{1} - {2}{3}", DateTime.Now.ToShortTimeString(), "ERROR", message,
                    Environment.NewLine), Color.Red);
            
            ShowNotifyIconMessage(ToolTipIcon.Error, message.Length > 15 ? message.Substring(0, 13) + " ..." : message);
        }

        private void ShowNotifyIconMessage(ToolTipIcon icon, string message)
        {
            if (notifyIcon1.Visible)
            {
                notifyIcon1.BalloonTipIcon = icon;
                notifyIcon1.BalloonTipText = message.Length > 15 ? message.Substring(0, 13) + " ..." : message;
                notifyIcon1.BalloonTipTitle = "Error in TRIO Assistant";
                notifyIcon1.ShowBalloonTip(2000);
            }
        }

        public void LogText(string message)
        {
            logTextBox.AppendText(string.Format("{0}:{1} - {2}{3}", DateTime.Now.ToShortTimeString(), "LOG", message, Environment.NewLine));
        }

        private void TRIOAssistant_Init()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //add TRIO Print assistant to Windows startup
            string configKey = "AddedToWindowsStartup";
            string keyValue = GetOrCreateConfig(config, configKey);
            if (string.IsNullOrEmpty(keyValue) || keyValue != "true")
            {
                try
                {
                    using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
                    {
                        //delete previous startup settings (it was set to the exe instead of appref-ms
                        key.DeleteValue("TRIO Print Assistant", false);
                        string startPath =
                            Environment.GetFolderPath(Environment.SpecialFolder.Programs)
                            + $@"\{PUBLISHER_NAME}\{PRODUCT_NAME}.appref-ms";
                        key.SetValue("TRIO Assistant", "\"" + startPath + "\"");
                    }
                    SaveConfigValue(config, configKey, "true");
                    MessageBox.Show("TRIO Print assistant was added to Windows Startup", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please run TRIO Print Assistant with admin rights, in order to add it to Windows Startup", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            if (!Properties.Settings.Default.Upgraded)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.Upgraded = true;
                Properties.Settings.Default.Save();
            }

            clsPrinterFunctions.GetApplicationKey(ref keyValue);
            textBox1.Text = Convert.ToString(keyValue);
            MiniServer.ApplicationKey = keyValue;
            
            MiniServer.Listen(new[] { @"http://localhost:" + miniServerPort + @"/machineid/" });
            var appSettings = GetApplicationSettings();
            keyValue = appSettings.WebSocketURL;
            string baseUrl = "";
            string printingServiceHandler = "printingviawebsockets.ashx";
            if (keyValue.Contains("//") && keyValue.ToLower().Contains(printingServiceHandler))
            {
                int startIndex = keyValue.IndexOf("//");
                startIndex = startIndex + 2;
                int length = keyValue.Length - startIndex - printingServiceHandler.Length;
                baseUrl = keyValue.Substring(startIndex, length);
            }
            textBox3.Text = baseUrl;

            chkSecureConnection.Checked = appSettings.UseWSS;


            websocketURL = keyValue;
            notifyIcon1.Icon = Icon.FromHandle(Resources.TRIOAssistantDisconnected.GetHicon());

            // set the title so it tells us which version this is
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                Text = $"TRIO Assistant v{AssemblyVersion.ToString()}";
            }

            //// set up telemetry
            //var telemInit = new TelemetryInitialization
            //{
            //    DeviceType = "Client PC",
            //    //FC:FINAL:SBE - HttpContext.Current is null when WebSocket protocol is enabled on web server
            //    //DeviceId = HttpContext.Current.Server.MachineName,
            //    DeviceId = Environment.MachineName,
            //    WiseJSessionId = string.Empty,
            //    Environment = string.Empty,
            //    Username = Environment.UserName,
            //    UserAgent = string.Empty,
            //    AppVersion = $"{Application.ProductVersion}",
            //    Browser = string.Empty,
            //    UserHostAddress = string.Empty,
            //    UserHostName = string.Empty,
            //    ApplicationSource = "Trio Assistant"
            //};

            //_Telemetry = new TelemetryService(telemInit);
            _Telemetry = Program.telemetryService;
        }

        private string GetOrCreateConfig(Configuration config, string configKey)
        {
            if (!config.AppSettings.Settings.AllKeys.Contains(configKey))
            {
                config.AppSettings.Settings.Add(configKey, "");
            }

            var appConfigSetting = config.AppSettings.Settings[configKey];
            if (appConfigSetting == null)
            {
                appConfigSetting = new KeyValueConfigurationElement(configKey, string.Empty);
                config.AppSettings.Settings.Add(appConfigSetting);
            }
            config.Save();
            return appConfigSetting.Value;
        }


        private void SaveConfigValue(Configuration config, string configKey, string keyValue)
        {
            var appConfigSetting = config.AppSettings.Settings[configKey];
            appConfigSetting.Value = keyValue;
            config.Save();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Connect();
        }

        internal void Connect(bool autoRetry = false)
        {
            try
            {
                var webSocketServiceURL = textBox2.Text.Trim();
                string uri = webSocketServiceURL + "?machine=" + textBox1.Text;
                Communication.Connect(uri, autoRetry);
                //save config with the new uri, if value was changed
                if (websocketURL != webSocketServiceURL)
                {
                    //Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    //string configKey = "WebSocketURL";
                    //GetOrCreateConfig(config, configKey);
                    //SaveConfigValue(config, configKey, webSocketServiceURL);

                    //Properties.Settings.Default.WebSocketURL = webSocketServiceURL;
                    //Properties.Settings.Default.UseWSS = chkSecureConnection.Checked;

                    //Properties.Settings.Default.Save();
                    SaveApplicationSettings(new TrioAssistantAppSettings(){UseWSS = chkSecureConnection.Checked, WebSocketURL = webSocketServiceURL});
                    websocketURL = webSocketServiceURL;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eror connecting to the web server", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void OnConnected()
        {
            button1.Enabled = false;
            button2.Enabled = true;
            notifyIcon1.Icon = Icon.FromHandle(Resources.TRIOAssistantConnected.GetHicon());
            connectionStatusLabel.Text = "Connected";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(textBox1.Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Copy Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void OnDisconnected(bool fromAutoReconnectAttempt = false)
        {
            Icon iconDisconnected = null;
            try
            {
                button1.Enabled = true;
                button2.Enabled = false;
                connectionStatusLabel.Text = "Disconnected";

                // create the icon using special code that will handle dispose
                iconDisconnected =  Utils.BitmapToIcon(Resources.TRIOAssistantDisconnected);
                // use the icon and then you can dispose of it in the finally
                notifyIcon1.Icon = iconDisconnected;

                if (!fromAutoReconnectAttempt)
                {
                    if (!forceDisconnect)
                    {
                        disconnectTimer.Interval = 5000;
                        disconnectTimer.Start();
                    }
                }
                else
                {
                    if (connectionStatusLabel.Text != "Connected")
                    {
                        ShowNotifyIconMessage(ToolTipIcon.Warning, "TRIO Assistant disconnected from server. Please reconnect");
                    }
                }
            }
            catch (Exception e)
            {
                ShowNotifyIconMessage(ToolTipIcon.Error, $"ERROR: {e.GetBaseException().Message}");
            }
            finally
            {
                iconDisconnected?.Dispose();
            }
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            forceDisconnect = true;
            await Communication.Disconnect();
            OnDisconnected();
            forceDisconnect = false;
        }

        private async void TRIOAssistant_FormClosed(object sender, FormClosedEventArgs e)
        {
            var webSocketServiceURL = textBox2.Text.Trim();
            //save config with the new uri, if value was changed
            if (websocketURL != webSocketServiceURL)
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string configKey = "WebSocketURL";
                SaveConfigValue(config, configKey, webSocketServiceURL);
                websocketURL = webSocketServiceURL;
            }
            await Communication.Disconnect();
        }

        private void TRIOAssistant_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;
            }
        }

        internal void MinimizeToTray()
        {
            Hide();
            WindowState = FormWindowState.Minimized;
            notifyIcon1.Visible = true;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowMe();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ShowMe();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            allowClose = true;
            Close();
        }

        private void printRTFDocument_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            checkPrint = 0;
        }

        private void printRTFDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            checkPrint = richTextBoxPrintCtrl1.Print(checkPrint, richTextBoxPrintCtrl1.TextLength, e);

            // Check for more pages
            if (checkPrint < richTextBoxPrintCtrl1.TextLength)
            {
                e.HasMorePages = true;
            }
            else
            {
                e.HasMorePages = false;
            }
        }

        private void printTIFFDocument_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            tiffCurrentTop = 0;
        }

        private void printTIFFDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string tiffFileName = PrintTiffFileName;
            Image tiffBitmap = Image.FromFile(tiffFileName);
            e.Graphics.DrawImage(tiffBitmap, 0, 0);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == NativeMethods.WM_SHOWME)
            {
                string clipBoardText = Clipboard.GetText();
                string[] lines = clipBoardText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                //get the file path from another process, and restore clipboard text
                if (lines.Length > 1)
                {
                    string recievedString = lines[0] + Environment.NewLine;
                    clipBoardText = clipBoardText.Remove(0, recievedString.Length);
                    Clipboard.SetText(clipBoardText);
                }
                string filePath = lines[0];
                SketchFileName = filePath;
                LoadSketchFile();
                ShowMe();
            }
            base.WndProc(ref m);
        }

        private void ShowMe()
        {
            allowVisible = true;

            Show();
            WindowState = FormWindowState.Normal;
            //notifyIcon1.Visible = false;
            bool top = TopMost;
            TopMost = true;
            TopMost = top;
        }

        internal void LoadSketchFile()
        {
            if (File.Exists(SketchFileName))
            {
                LogSketchAction($" -- Opening    TRIO sketch file --------------- * {SketchFileName} *");
                try
                {
                    string datestamp = DateTime.Now.Ticks.ToString();
                    sketchTempFolder = Directory.CreateDirectory("C:\\triosql\\sketches\\" + datestamp).FullName;

                    using (ZipArchive archive = ZipFile.Open(SketchFileName, ZipArchiveMode.Read))
                    {
                        archive.ExtractToDirectory(sketchTempFolder);
                    }
                    var dataFiles = Directory.GetFiles(sketchTempFolder, "*.data");
                    string sketchDataSerialized = File.ReadAllText(dataFiles[0]);
                    sketchData = WisejSerializer.Parse(sketchDataSerialized);

                    foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinSkt"))
                    {
                        process.Kill();
                    }

                    appWinSketch = null;
                    appWinSketch = new winskt.WinSkt();
                    appWinSketch.OpenSketch(Path.Combine(sketchTempFolder, sketchData.fileName));
                    appWinSketch.FileNo = sketchData.data.fileNo;
                    appWinSketch.City = sketchData.data.city;
                    appWinSketch.State = sketchData.data.state;
                    appWinSketch.ZipCode = sketchData.data.zipcode;
                    appWinSketch.Borrower = sketchData.data.borrower;
                    appWinSketch.ClientAddress = sketchData.data.clientAddress;
                    appWinSketch.PropertyAddress = sketchData.data.propertyAddress;
                    //appWinSketch.UpdateImage += new winskt.IWinSktEvents_UpdateImageEventHandler(AppWinSketch_UpdateImage);
                    appWinSketch.UpdateImage -= AppWinSketch_UpdateImage;
                    appWinSketch.UpdateImage += AppWinSketch_UpdateImage;

                    LogSketchAction($" -- Winsketch started and loaded sketch file -- * {SketchFileName} *");
                }
                catch (Exception ex)
                {
                    LogSketchAction($"!!! ERROR opening sketch file   --------------- * {SketchFileName} *");
                    LogSketchAction($"    ERROR details: {ex.ToString()}");
                }
            }
        }

        private void AppWinSketch_UpdateImage(short pageno)
        {
            if (pageno > 0) return;
            string logText = string.Empty;
            var logSketchActionDelegate = new SafeCallDelegate(LogSketchAction);
            if (appWinSketch.GetImage(pageno))
            {
                try
                {
                    Bitmap newImage = Utils.GetImageFromClipboard();
                    if (newImage != null && !newImage.EqualsWith(lastSketchBitmap))
                    {
                        logText = $" -- Updated WinSketch file      ---------------";
                        Invoke(logSketchActionDelegate, new object[] { logText });
                        //save to file
                        string bitmapFileName = sketchData.fileName.Replace(".skt", ".bmp");
                        string bitmapFilePath = Path.Combine(sketchTempFolder, bitmapFileName);
                        if (File.Exists(bitmapFilePath))
                        {
                            File.Delete(bitmapFilePath);
                        }
                        using (var croppedBitmap = Utils.CropWhiteSpace(newImage))
                        {
                            croppedBitmap.Save(Path.Combine(sketchTempFolder, bitmapFileName));
                        }
                        logText = $" -- Extracting image from WinSketch file ----";
                        Invoke(logSketchActionDelegate, new object[] { logText });

                        object calculations = null;
                        if (appWinSketch.GetCalculations(out calculations))
                        {
                            string calculationFile = sketchData.fileName.Replace(".skt", ".skt-calculations");
                            string calculationFilePath = Path.Combine(sketchTempFolder, calculationFile);
                            if (File.Exists(calculationFilePath))
                            {
                                File.Delete(calculationFilePath);
                            }
                            string calculationData = WisejSerializer.Serialize(calculations);
                            using (var textFileWriter = File.CreateText(calculationFilePath))
                            {
                                textFileWriter.Write(calculationData);
                            }
                        }
                        sktFileLastUpdateTicks = DateTime.Now.Ticks;
                        Task.Run(() => Upload(sketchTempFolder, SketchFileName, sketchData.fileName, sketchData.uploadUrl, sketchData.serverDestinationFolder));
                        //Upload(SketchFileName, sketchData.fileName, sketchData.uploadUrl, sketchData.serverDestinationFolder);
                        if (lastSketchBitmap != null)
                        {
                            lastSketchBitmap.Dispose();
                            lastSketchBitmap = null;
                        }
                        lastSketchBitmap = newImage;
                    }
                }
                catch (Exception ex)
                {
                    logText = $"!!! ERROR updating sketch file   -------------- * {SketchFileName} *";
                    Invoke(logSketchActionDelegate, new object[] { logText });
                    logText = $"    ERROR details: {ex.ToString()}";
                    Invoke(logSketchActionDelegate, new object[] { logText });
                }
            }
        }

        public async Task<bool> Upload(string sketchTempFolder, string filePath, string fileName, string uploadUrl, string serverDestinationFolder)
        {
            string logText = string.Empty;
            var logSketchActionDelegate = new SafeCallDelegate(LogSketchAction);
            try
            {
                //check if skt file is already created. When a new file is created during save, the AppWinSketch_UpdateImage event has to be finished
                while (!File.Exists(Path.Combine(sketchTempFolder, fileName)))
                {
                    await Task.Delay(200);
                }
                //check if skt file is modufied on disk. When a skt file is saved, the AppWinSketch_UpdateImage event has to be finished before the file is updated on disk
                var fileInfo = new FileInfo(Path.Combine(sketchTempFolder, fileName));
                while (fileInfo.LastWriteTime.Ticks < sktFileLastUpdateTicks)
                {
                    await Task.Delay(200);
                    fileInfo = new FileInfo(Path.Combine(sketchTempFolder, fileName));
                }

                //zip and upload to server
                if (File.Exists(SketchFileName))
                {
                    File.Delete(SketchFileName);
                }
                ZipFile.CreateFromDirectory(sketchTempFolder, SketchFileName);

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("serverDestinationFolder", serverDestinationFolder);
                    client.DefaultRequestHeaders.Add("fileName", fileName);
                    using (var content = new MultipartFormDataContent())
                    {
                        using (var stream = new FileStream(filePath, FileMode.Open))
                        {
                            content.Add(new StreamContent(stream), "trio-skt", Path.GetFileName(filePath));
                            using (var message = await client.PostAsync(uploadUrl, content))
                            {
                                var input = await message.Content.ReadAsStringAsync();
                                if (message.IsSuccessStatusCode)
                                {
                                    logText = $" -- Uploaded WinSketch file      --------------";
                                    Invoke(logSketchActionDelegate, new object[] { logText });
                                    return true;
                                }
                                else
                                {
                                    logText = $"!!! FAILED uploading sketch file --------------";
                                    Invoke(logSketchActionDelegate, new object[] { logText });
                                    logText = $" HTTP ERROR {message.StatusCode} {await message.Content.ReadAsStringAsync()}";
                                    Invoke(logSketchActionDelegate, new object[] { logText });
                                    logText = $" ERROR details: Upload URL: {uploadUrl}; Server folder: {serverDestinationFolder}; File path: {filePath}";
                                    Invoke(logSketchActionDelegate, new object[] { logText });
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logText = $"!!! ERROR uploading sketch file   -------------- * {SketchFileName} *";
                Invoke(logSketchActionDelegate, new object[] { logText });
                logText = $"    ERROR details: {ex.ToString()}";
                Invoke(logSketchActionDelegate, new object[] { logText });
                return false;
            }
        }

        private void LogSketchAction(string text)
        {
            var item = listView1.Items.Add($"[{DateTime.Now.ToShortDateString()} {DateTime.Now.ToShortTimeString()}]");
            item.SubItems.Add(text);
            listView1.Columns[1].Width = Math.Max(listView1.Columns[1].Width, TextRenderer.MeasureText(text, listView1.Font).Width + 15);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                string clipBoardText = string.Empty;
                foreach (ListViewItem item in listView1.SelectedItems)
                {
                    clipBoardText += item.Text + item.SubItems[1].Text + Environment.NewLine;
                }
                Clipboard.SetText(clipBoardText);
            }
            else
            {
                MessageBox.Show("No logs selected. Please select winsketch logs first", "Select logs", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            FillServiceHostEntry();
        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            ShowMe();
        }

        private void chkSecureConnection_CheckedChanged(object sender, EventArgs e)
        {
            FillServiceHostEntry();
        }

        private void FillServiceHostEntry()
        {
            string connectionType = "";

            if (chkSecureConnection.Checked)
            {
                connectionType = "wss://";
            }
            else
            {
                connectionType = "ws://";
            }

            if (textBox3.Text.EndsWith("/"))
            {
                textBox2.Text = connectionType + textBox3.Text + PRINTING_SERVICE_HANDLER;
            }
            else
            {
                textBox2.Text = connectionType + textBox3.Text + "/" + PRINTING_SERVICE_HANDLER;
            }
        }

        private void disconnectTimer_Tick(object sender, EventArgs e)
        {
            disconnectTimer.Stop();
            Connect(true);
        }

        private TrioAssistantAppSettings GetApplicationSettings()
        {
            var keyPath = clsPrinterFunctions.GetApplicationSettingsFolder();
            var fileName = "applicationSettings.json";
            var filePath = Path.Combine(keyPath, fileName);

            if (File.Exists(filePath))
            {
                using (var appSettingsFile = File.OpenText(filePath))
                {
                    // get key
                    var jsonData = appSettingsFile.ReadToEnd();
                    System.Text.Json.JsonSerializer.Deserialize<TrioAssistantAppSettings>(jsonData);
                    var appSettings =  System.Text.Json.JsonSerializer.Deserialize<TrioAssistantAppSettings>(jsonData);
                    return appSettings;
                }

            }
            else
            {
                var useWSS =  Properties.Settings.Default.UseWSS;
                var webSocketUrl = Properties.Settings.Default.WebSocketURL;
                var appSettings = new TrioAssistantAppSettings(){UseWSS = useWSS, WebSocketURL = webSocketUrl};
                SaveApplicationSettings(appSettings);
                return appSettings;
            }
        }

        private void SaveApplicationSettings(TrioAssistantAppSettings appSettings)
        {
            try
            {
                var keyPath = clsPrinterFunctions.GetApplicationSettingsFolder();
                var fileName = "applicationSettings.json";
                var filePath = Path.Combine(keyPath, fileName);
                var jsonData = System.Text.Json.JsonSerializer.Serialize<TrioAssistantAppSettings>(appSettings);
                using (Stream fileStream = File.Open(filePath, FileMode.Create))
                {
                    fileStream.Write(Encoding.UTF8.GetBytes(jsonData), 0, jsonData.Length);
                }
            }
            catch
            {

            }
        }

        private class TrioAssistantAppSettings
        {
            public string WebSocketURL { get; set; } = "";
            public bool UseWSS { get; set; } = false;
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }

        
    }
}
