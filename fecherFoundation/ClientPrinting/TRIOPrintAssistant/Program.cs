﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DirectPrintingApp;
using SharedApplication.Telemetry;

namespace TRIOAssistant
{
	static class Program
	{
		internal static TRIOAssistant MainForm;
		internal static ITelemetryService telemetryService;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			string fileName = string.Empty;
			var commandLineParams = AppDomain.CurrentDomain.SetupInformation.ActivationArguments?.ActivationData;
			if (commandLineParams != null && commandLineParams.Length > 0)
			{
				var arg = commandLineParams[0];
				System.Uri uri = new System.Uri(arg);
				fileName = uri.LocalPath;
			}

			bool createdNew = true;
			using (Mutex mutex = new Mutex(true, "TRIOAssistant", out createdNew))
			{
				if (createdNew)
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
                    var telemInit = new TelemetryInitialization
                    {
                        DeviceType = "Client PC",
                        //FC:FINAL:SBE - HttpContext.Current is null when WebSocket protocol is enabled on web server
                        //DeviceId = HttpContext.Current.Server.MachineName,
                        DeviceId = Environment.MachineName,
                        WiseJSessionId = string.Empty,
                        Environment = string.Empty,
                        Username = Environment.UserName,
                        UserAgent = string.Empty,
                        AppVersion = $"{Application.ProductVersion}",
                        Browser = string.Empty,
                        UserHostAddress = string.Empty,
                        UserHostName = string.Empty,
                        ApplicationSource = "Trio Assistant"
                    };
                    telemetryService = new TelemetryService(telemInit);
					MainForm = new TRIOAssistant();
					MainForm.SketchFileName = fileName;
					MainForm.LoadSketchFile();
					MainForm.MinimizeToTray();
					MainForm.Connect();
					Application.Run(MainForm);
				}
				else
				{
					Process current = Process.GetCurrentProcess();
					foreach (Process process in Process.GetProcessesByName(current.ProcessName))
					{
						if (process.Id != current.Id)
						{
                            try
                            {
                                string currentClipBoardText = Clipboard.GetText();
							    //concatenate the file path with current clipboard text, if clipboard already contains a text
							    if (!string.IsNullOrEmpty(currentClipBoardText))
							    {
								    fileName = fileName + Environment.NewLine + currentClipBoardText;
							    }

                                Clipboard.SetText(fileName);
                            }
                            catch (Exception ex)
                            {

                            }

                            NativeMethods.PostMessage((IntPtr) NativeMethods.HWND_BROADCAST, NativeMethods.WM_SHOWME,
								IntPtr.Zero, IntPtr.Zero);
							break;
						}
					}
				}
			}

			//Application.EnableVisualStyles();
			//Application.SetCompatibleTextRenderingDefault(false);
			//MainForm = new TRIOAssistant();
			//Application.Run(MainForm);
		}
	}
}
