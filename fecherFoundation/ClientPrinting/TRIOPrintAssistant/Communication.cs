﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRIOAssistant;

namespace DirectPrintingApp
{
    public static class Communication
    {
        private const int receiveChunkSize = 0x2000;
        private const int BufferAmplifier = 20;
        private const bool verbose = true;
        private static readonly TimeSpan delay = TimeSpan.FromMilliseconds(1000);

        private static ClientWebSocket webSocket = null;


        public static async Task Connect(string uri, bool autoRetry = false)
        {
            await CloseSocketConnection();

            try
            {
                //Console.WriteLine("hostname: {0}", Dns.GetHostName());
                //Console.WriteLine("Copy hostname and follow the steps in the web application, after doing this press any key to go on.");
                //Console.ReadKey();
                //uri += Dns.GetHostName();
                webSocket = new ClientWebSocket();
                await webSocket.ConnectAsync(new Uri(uri), CancellationToken.None);
                TRIOAssistant.Program.MainForm.OnConnected();
                autoRetry = false;
                //await Task.WhenAll(Receive(webSocket), Send(webSocket));
                await Task.WhenAll(Receive(webSocket));
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && (ex.InnerException is System.Net.Sockets.SocketException))
                {
                    TRIOAssistant.Program.MainForm.LogText("Socket Error Code: " + (ex.InnerException as System.Net.Sockets.SocketException).SocketErrorCode.ToString());
                    TRIOAssistant.Program.telemetryService.TrackException(ex.InnerException);
                }
                else
                {
                    TRIOAssistant.Program.MainForm.LogError(ex.ToString());
                    TRIOAssistant.Program.telemetryService.TrackException(ex);
                }
               
                TRIOAssistant.Program.MainForm.OnDisconnected(autoRetry);
               
                
                //Console.WriteLine("Exception: {0}", ex);
            }
            finally
            {
                await CloseSocketConnection();

                if (!autoRetry)
                {
	                TRIOAssistant.Program.MainForm.LogText("WebSocket closed.");
                }

                TRIOAssistant.Program.MainForm.OnDisconnected(autoRetry);
            }
        }

        private static async Task CloseSocketConnection()
        {
            if (webSocket != null && webSocket.State == WebSocketState.Open)
            {
                await webSocket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
            }
        }

        private static async Task Send(ClientWebSocket webSocket)
        {
            //MemoryStream Buffer = new MemoryStream();
            while (webSocket.State == WebSocketState.Open)
            {
                ArraySegment<byte> outputBuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(Console.ReadLine()));
                await webSocket.SendAsync(outputBuffer, WebSocketMessageType.Binary, false, CancellationToken.None);
                LogStatus(false, outputBuffer.ToArray<byte>(), outputBuffer.Count);
            }
        }

        public static async Task Send(string message)
        {
            if (webSocket.State == WebSocketState.Open)
            {
                ArraySegment<byte> outputBuffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));
                await webSocket.SendAsync(outputBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                //LogStatus(false, outputBuffer.ToArray<byte>(), outputBuffer.Count);
            }
        }

        public static async Task Disconnect()
        {
            await CloseSocketConnection();
        }

        private static bool CanDisconnect()
        {
            bool ret = false;

            ret = webSocket != null 
                  && webSocket.State == WebSocketState.Open;

            return ret;
        }

        private static async Task Receive(ClientWebSocket webSocket)
        {
            //var temporaryBuffer = new byte[receiveChunkSize];
            //var buffer = new byte[1024 * 1024]; // 1MB buffer
            //var offset = 0;

            //byte[] buffer = new byte[receiveChunkSize];
            //byte[] bigbuffer = new byte[receiveChunkSize];
            while (webSocket.State == WebSocketState.Open || webSocket.State == WebSocketState.CloseSent)
            {
                WebSocketReceiveResult result;
                ////data will be send into chunks, if the data is to big, so we need to collect them all
                //do
                //{
                //    result = await webSocket.ReceiveAsync(new ArraySegment<byte>(temporaryBuffer), CancellationToken.None);
                //    temporaryBuffer.CopyTo(buffer, offset);
                //    offset += result.Count;
                //    temporaryBuffer = new byte[receiveChunkSize];
                //} while (!result.EndOfMessage);
                var fullMessage = await ReceiveFullMessage(webSocket, CancellationToken.None);
                result = fullMessage.Item1;
                if (result.CloseStatus == WebSocketCloseStatus.NormalClosure)
                {

                }
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    TRIOAssistant.Program.MainForm.LogText("Close loop complete");
                    break;
                }
                else
                {
                    string incoming = Encoding.UTF8.GetString(fullMessage.Item2.ToArray(), 0, fullMessage.Item2.Count());
                    if (incoming.StartsWith("{"))
                    {
                        try
                        {
                            dynamic dynPO = WisejSerializer.Parse(incoming);
                            if (dynPO != null)
                            {
                                ParameterObjects po = new ParameterObjects();
                                po.FunctionToExecute = Enum.Parse(typeof(FunctionToExecute), dynPO.FunctionToExecute, true);
                                for (int i = 0; i < po.Parameter.Length; i++)
                                {
                                    if (dynPO.Parameter?[i] != null)
                                    {
                                        po.Parameter[i] = new ParameterObject((object)dynPO.Parameter[i].Value, (bool)dynPO.Parameter[i].IsRefParameter);
                                    }

                                }
                                await clsPrinterFunctions.ExecutePrintFunction(po);
                            }

                        }
                        catch (Exception e)
                        {
                            LogStatus(true, Encoding.UTF8.GetBytes(e.Message), e.Message.Length);
                            TRIOAssistant.Program.telemetryService.TrackException(e);
                        }
                    }
                    else
                    {
                        await Communication.Send("file transfer done");
                    }                    
                    LogStatus(true, fullMessage.Item2.ToArray(), fullMessage.Item2.Count());

                    //buffer = new byte[1024 * 1024];
                    //offset = 0;
                }
            }
        }

        //https://www.codetinkerer.com/2018/06/05/aspnet-core-websockets.html
        private static async Task<(WebSocketReceiveResult, IEnumerable<byte>)> ReceiveFullMessage(WebSocket socket, CancellationToken cancelToken)
        {
            WebSocketReceiveResult response;
            var message = new List<byte>();

            var buffer = new byte[4096];
            do
            {
                response = await socket.ReceiveAsync(new ArraySegment<byte>(buffer), cancelToken);
                message.AddRange(new ArraySegment<byte>(buffer, 0, response.Count));
            }
            while (!response.EndOfMessage);

            return (response, message);
        }

        private static void LogStatus(bool receiving, byte[] buffer, int length)
        {
            Program.MainForm.LogText(string.Format("{0} {1} bytes... ", receiving ? "Received" : "Sent", length));

            if (verbose)
                Program.MainForm.LogText(Encoding.UTF8.GetString(buffer, 0, length));
        }
    }
}
