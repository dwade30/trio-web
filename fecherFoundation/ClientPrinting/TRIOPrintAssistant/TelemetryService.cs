﻿using System;
using System.Reflection;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using SharedApplication.Telemetry;

namespace TRIOAssistant
{
    public class TelemetryService : ITelemetryService
    {
        private TelemetryClient _telemetry;

        public TelemetryService(TelemetryInitialization initialization)
        {
           _telemetry = new TelemetryClient();
            Initialize(initialization);
        }
        private void Initialize(TelemetryInitialization initialization)
        {
           _telemetry.Context.User.Id = initialization.Username;
           _telemetry.Context.Device.Id = initialization.DeviceId;

           _telemetry.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
           _telemetry.Context.Device.Model = string.Empty;
           _telemetry.Context.Device.Type = initialization.DeviceType;

           _telemetry.Context.Location.Ip = initialization.UserHostAddress;
           _telemetry.Context.User.AuthenticatedUserId = initialization.UserHostName;

           //_telemetry.Context.Device.Type = $"{ua.Device.Family}";
           _telemetry.Context.User.UserAgent = initialization.UserAgent;
           _telemetry.Context.Component.Version = initialization.AppVersion;
           _telemetry.Context.Session.Id = initialization.WiseJSessionId;
           _telemetry.Context.Device.OemName = string.Empty;
           
           //
           

           Type type = typeof(TelemetryInitialization);
           PropertyInfo[] properties = type.GetProperties();
           foreach (PropertyInfo property in properties)
           {
               _telemetry.Context.GlobalProperties.Add(property.Name, property.GetValue(initialization, null)?.ToString());
           }
            
           var appStartedMsg = $"Application telemetry started.";
            _telemetry.TrackTrace(appStartedMsg, SeverityLevel.Information);
        }

        public void TrackHit(string pageName)
        {
            _telemetry.TrackPageView(pageName);
        }

        public  void TrackException(Exception exception)
        {
            _telemetry.TrackException(exception);
        }

        public  void TrackTrace(string traceText)
        {
            _telemetry.TrackTrace(traceText);
        }

        public  void TrackEvent(string eventName)
        {
            _telemetry.TrackEvent(eventName);
        }

        public  void TrackTiming(string title, long milliseconds)
        {
            _telemetry.GetMetric(title).TrackValue(milliseconds);
        }

        public void TrackTiming(string title, DateTime startDateTime, DateTime endDateTime)
        {
            _telemetry.GetMetric(title).TrackValue((endDateTime - startDateTime).Milliseconds);
        }
    }
}
