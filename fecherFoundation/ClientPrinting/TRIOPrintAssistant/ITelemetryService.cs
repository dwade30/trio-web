﻿using System;

namespace SharedApplication.Telemetry
{
    public interface ITelemetryService
    {
        void TrackException(Exception exception);
        void TrackTrace(string traceText);
        void TrackEvent(string eventName);
        void TrackTiming(string title, long milliseconds);
        void TrackTiming(string title, DateTime startDateTime, DateTime endDateTime);
        void TrackHit(string pageName);
    }
}