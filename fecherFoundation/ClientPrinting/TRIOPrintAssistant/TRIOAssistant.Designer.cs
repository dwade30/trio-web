﻿namespace TRIOAssistant
{
    partial class TRIOAssistant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TRIOAssistant));
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.connectionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.logTextBox = new System.Windows.Forms.RichTextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.printRTFDocument = new System.Drawing.Printing.PrintDocument();
			this.panel1 = new System.Windows.Forms.Panel();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.button4 = new System.Windows.Forms.Button();
			this.printTIFFDocument = new System.Drawing.Printing.PrintDocument();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.chkSecureConnection = new System.Windows.Forms.CheckBox();
			this.disconnectTimer = new System.Windows.Forms.Timer(this.components);
			this.richTextBoxPrintCtrl1 = new TRIOAssistantControls.RichTextBoxPrintCtrl();
			this.statusStrip.SuspendLayout();
			this.contextMenuStrip1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.AutoSize = true;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.button1.Location = new System.Drawing.Point(485, 118);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(79, 30);
			this.button1.TabIndex = 0;
			this.button1.Text = "Connect";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.textBox1.Location = new System.Drawing.Point(165, 86);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(472, 26);
			this.textBox1.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label1.Location = new System.Drawing.Point(12, 89);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(147, 20);
			this.label1.TabIndex = 2;
			this.label1.Text = "TRIO Assistant Key";
			// 
			// button2
			// 
			this.button2.AutoSize = true;
			this.button2.Enabled = false;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.button2.Location = new System.Drawing.Point(570, 118);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(99, 30);
			this.button2.TabIndex = 3;
			this.button2.Text = "Disconnect";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// statusStrip
			// 
			this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionStatusLabel});
			this.statusStrip.Location = new System.Drawing.Point(0, 631);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(691, 22);
			this.statusStrip.SizingGrip = false;
			this.statusStrip.TabIndex = 4;
			// 
			// connectionStatusLabel
			// 
			this.connectionStatusLabel.Name = "connectionStatusLabel";
			this.connectionStatusLabel.Size = new System.Drawing.Size(88, 17);
			this.connectionStatusLabel.Text = "Not Connected";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label2.Location = new System.Drawing.Point(12, 55);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(151, 20);
			this.label2.TabIndex = 5;
			this.label2.Text = "TRIO Service HOST";
			// 
			// textBox2
			// 
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.textBox2.Location = new System.Drawing.Point(165, 52);
			this.textBox2.Name = "textBox2";
			this.textBox2.ReadOnly = true;
			this.textBox2.Size = new System.Drawing.Size(504, 26);
			this.textBox2.TabIndex = 6;
			// 
			// logTextBox
			// 
			this.logTextBox.Location = new System.Drawing.Point(12, 190);
			this.logTextBox.Name = "logTextBox";
			this.logTextBox.Size = new System.Drawing.Size(657, 160);
			this.logTextBox.TabIndex = 7;
			this.logTextBox.Text = "";
			// 
			// button3
			// 
			this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
			this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.button3.Location = new System.Drawing.Point(643, 86);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(26, 26);
			this.button3.TabIndex = 8;
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "TRIO Print Assistant";
			this.notifyIcon1.Visible = true;
			this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
			this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.toolStripMenuItem2});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(104, 54);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(103, 22);
			this.toolStripMenuItem1.Text = "Open";
			this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(100, 6);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(103, 22);
			this.toolStripMenuItem2.Text = "Exit";
			this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
			// 
			// printRTFDocument
			// 
			this.printRTFDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.printRTFDocument_BeginPrint);
			this.printRTFDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printRTFDocument_PrintPage);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.listView1);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Location = new System.Drawing.Point(12, 374);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(657, 192);
			this.panel1.TabIndex = 10;
			// 
			// listView1
			// 
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
			this.listView1.Font = new System.Drawing.Font("Courier New", 8.25F);
			this.listView1.FullRowSelect = true;
			this.listView1.HideSelection = false;
			this.listView1.Location = new System.Drawing.Point(0, 33);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(657, 159);
			this.listView1.TabIndex = 13;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Time";
			this.columnHeader1.Width = 100;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Log";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(161, 20);
			this.label4.TabIndex = 12;
			this.label4.Text = "WinSketch file editing";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label3.Location = new System.Drawing.Point(12, 167);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(139, 20);
			this.label3.TabIndex = 11;
			this.label3.Text = "Direct Printing Log";
			// 
			// button4
			// 
			this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.button4.Location = new System.Drawing.Point(416, 572);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(253, 31);
			this.button4.TabIndex = 12;
			this.button4.Text = "Copy selected logs to Clipboard";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// printTIFFDocument
			// 
			this.printTIFFDocument.DocumentName = "tiffDocument";
			this.printTIFFDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.printTIFFDocument_BeginPrint);
			this.printTIFFDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printTIFFDocument_PrintPage);
			// 
			// textBox3
			// 
			this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.textBox3.Location = new System.Drawing.Point(165, 18);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(504, 26);
			this.textBox3.TabIndex = 14;
			this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.label5.Location = new System.Drawing.Point(12, 21);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(125, 20);
			this.label5.TabIndex = 13;
			this.label5.Text = "TRIO Base URL";
			// 
			// chkSecureConnection
			// 
			this.chkSecureConnection.AutoSize = true;
			this.chkSecureConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.chkSecureConnection.Location = new System.Drawing.Point(300, 122);
			this.chkSecureConnection.Name = "chkSecureConnection";
			this.chkSecureConnection.Size = new System.Drawing.Size(161, 24);
			this.chkSecureConnection.TabIndex = 15;
			this.chkSecureConnection.Text = "Connect with WSS";
			this.chkSecureConnection.UseVisualStyleBackColor = true;
			this.chkSecureConnection.CheckedChanged += new System.EventHandler(this.chkSecureConnection_CheckedChanged);
			// 
			// disconnectTimer
			// 
			this.disconnectTimer.Interval = 2000;
			this.disconnectTimer.Tick += new System.EventHandler(this.disconnectTimer_Tick);
			// 
			// richTextBoxPrintCtrl1
			// 
			this.richTextBoxPrintCtrl1.Location = new System.Drawing.Point(355, 154);
			this.richTextBoxPrintCtrl1.Name = "richTextBoxPrintCtrl1";
			this.richTextBoxPrintCtrl1.Size = new System.Drawing.Size(314, 30);
			this.richTextBoxPrintCtrl1.TabIndex = 9;
			this.richTextBoxPrintCtrl1.Text = "";
			this.richTextBoxPrintCtrl1.Visible = false;
			// 
			// TRIOAssistant
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(691, 653);
			this.Controls.Add(this.chkSecureConnection);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.richTextBoxPrintCtrl1);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.logTextBox);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.statusStrip);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "TRIOAssistant";
			this.Text = "TRIO Assistant";
			this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TRIOAssistant_FormClosed);
			this.Resize += new System.EventHandler(this.TRIOAssistant_Resize);
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.contextMenuStrip1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel connectionStatusLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.RichTextBox logTextBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        internal TRIOAssistantControls.RichTextBoxPrintCtrl richTextBoxPrintCtrl1;
        internal System.Drawing.Printing.PrintDocument printRTFDocument;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button button4;
        internal System.Drawing.Printing.PrintDocument printTIFFDocument;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox chkSecureConnection;
		private System.Windows.Forms.Timer disconnectTimer;
	}
}

