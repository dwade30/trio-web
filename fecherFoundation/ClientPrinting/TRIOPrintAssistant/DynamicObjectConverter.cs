﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Diagnostics;
using System.Reflection;

namespace DirectPrintingApp
{
    /// <summary>
    /// Converts dynamic objects to other representations.
    /// </summary>
    public class DynamicObjectConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return
                destinationType == typeof(InstanceDescriptor)
                || base.CanConvertTo(context, destinationType);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return
                sourceType == typeof(string)
                || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (!(value is string))
                return base.ConvertFrom(context, culture, value);

            return WisejSerializer.Parse((string)value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == null)
                throw new ArgumentNullException("destinationType");

            if (destinationType == typeof(InstanceDescriptor) && value != null)
            {
                if (_parseMethod == null)
                    _parseMethod = typeof(WisejSerializer).GetMethod("Parse", new Type[] { typeof(string) });

                Debug.Assert(_parseMethod != null);

                return new InstanceDescriptor(_parseMethod, new[] { WisejSerializer.Serialize(value) });
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        private static MethodInfo _parseMethod = null;

    }
}
