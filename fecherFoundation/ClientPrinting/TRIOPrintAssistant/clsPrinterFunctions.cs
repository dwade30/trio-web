﻿using fecherFoundation;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Document;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Printing;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DirectPrintingApp
{
    public class clsPrinterFunctions
    {
        private static int printerHandle = 0;

        // DEFINED TYPE FOR THE WRITEPRINTER API FUNCTION
        [StructLayout(LayoutKind.Sequential)]
        private struct DOCINFO
        {
            public string pDocName;
            public string pOutputFile;
            public string pDatatype;
        };
        #region API FUNCTION CALLS
        [DllImport("winspool.drv")]
        private static extern int ClosePrinter(int hPrinter);
        [DllImport("winspool.drv")]
        private static extern int EndDocPrinter(int hPrinter);
        [DllImport("winspool.drv")]
        private static extern int EndPagePrinter(int hPrinter);
        //VBtoInfo: 0/2
        [DllImport("winspool.drv", EntryPoint = "OpenPrinterA")]
        private static extern int OpenPrinter(string pPrinterName, out int phPrinter, int pDefault);
        [DllImport("winspool.drv", EntryPoint = "StartDocPrinterA")]
        private static extern int StartDocPrinter(int hPrinter, int Level, ref DOCINFO pDocInfo);
        [DllImport("winspool.drv")]
        private static extern int StartPagePrinter(int hPrinter);
        [DllImport("winspool.drv")]
        private static extern int WritePrinter(int hPrinter, byte[] pBuf, int cdBuf, ref int pcWritten);
        [DllImport("winspool.drv", EntryPoint = "EnumFormsA")]
        public static extern int EnumForms(int hPrinter, int Level, ref byte pForm, int cbBuf, ref int pcbNeeded, ref int pcReturned);
        [DllImport("winspool.drv", EntryPoint = "AddFormA")]
        public static extern int AddForm(int hPrinter, int Level, ref byte pForm);
        //VBtoInfo: 0/2
        [DllImport("winspool.drv", EntryPoint = "DeleteFormA")]
        public static extern int DeleteForm(int hPrinter, IntPtr pFormName);
        //VBtoInfo: 0/4
        [DllImport("winspool.drv", EntryPoint = "DocumentPropertiesA")]
        public static extern int DocumentProperties(int hwnd, int hPrinter, string pDeviceName, ref short pDevModeOutput, ref short pDevModeInput, int fMode);
        [DllImport("winspool.drv", EntryPoint = "GetFormA")]
        public static extern int GetForm(int hPrinter, string pFormName, int Level, ref byte pForm, int cbBuf, ref int pcbNeeded);
        [DllImport("winspool.drv", EntryPoint = "SetFormA")]
        public static extern int SetForm(int hPrinter, string pFormName, int Level, ref byte pForm);

        #endregion

        public static async Task ExecutePrintFunction(ParameterObjects po)
        {
            var returnValue = 0;

            var mainForm = TRIOAssistant.Program.MainForm;

            var parameter0 = po.Parameter[0]?.Value;
            var parameter1 = po.Parameter[1]?.Value;
            var parameter2 = po.Parameter[2]?.Value;
            var parameter3 = po.Parameter[3]?.Value;
            var parameter4 = po.Parameter[4]?.Value;
            var parameter5 = po.Parameter[5]?.Value;

            try
            {
                
                switch (po.FunctionToExecute)
                {
                    case FunctionToExecute.ClosePrinter:
                        returnValue = ClosePrinter(parameter0);
                        break;
                    case FunctionToExecute.EndDocPrinter:
                        returnValue = EndDocPrinter(parameter0);
                        break;
                    case FunctionToExecute.EndPagePrinter:
                        returnValue = EndPagePrinter(parameter0);
                        break;
                    case FunctionToExecute.OpenPrinter:
                        returnValue = executeOpenPrinter(po, parameter1, parameter0, parameter2);

                        break;
                    case FunctionToExecute.StartDocPrinter:
                        returnValue = executeStartDocPrinter(po, parameter2, parameter0, parameter1);

                        break;
                    case FunctionToExecute.StartPagePrinter:
                        returnValue = StartPagePrinter(parameter0);
                        break;
                    case FunctionToExecute.WritePrinter:
                        RawPrinterHelper.SendStringToPrinter(parameter0, parameter1);
                        break;
                    case FunctionToExecute.EnumForms:
                        returnValue = executeEnumForms(po, parameter2, parameter4, parameter5, parameter0, parameter1, parameter3);

                        break;
                    case FunctionToExecute.AddForm:
                        returnValue = executeAddForm(po, parameter2, parameter0, parameter1);

                        break;
                    case FunctionToExecute.DeleteForm:
                        //returnValaue = DeleteForm(po.Parameter[0].Value, (IntPtr)po.Parameter[1].Value);
                        break;
                    case FunctionToExecute.DocumentProperties:
                        returnValue = executeDocumentProperties(po, parameter3, parameter4, parameter0, parameter1, parameter2, parameter5);

                        break;
                    case FunctionToExecute.GetForm:
                        returnValue = executeGetForm(po, parameter3, parameter5, parameter0, parameter1, parameter2, parameter4);

                        break;

                    case FunctionToExecute.SetForm:
                        returnValue = executeSetForm(po, parameter3, parameter0, parameter1, parameter2);

                        break;

                    case FunctionToExecute.GetPrinters:
                        returnValue = executeGetPrinters(po);

                        break;

                    case FunctionToExecute.GetMachinesPrinters:
                        returnValue = executeGetMachinesPrinters(po, parameter0);

                        break;

                    case FunctionToExecute.SaveMachinesPrinters:
                        returnValue = executeSaveMachinesPrinters(parameter0);

                        break;

                    case FunctionToExecute.GetApplicationKey:
                        returnValue = executeGetApplicationKey(po, parameter0);

                        break;

                    case FunctionToExecute.SaveApplicationKey:
                        returnValue = executeSaveApplicationKey(parameter0);

                        break;

                    case FunctionToExecute.PrintXs:
                        executePrintXs(parameter1, parameter0);

                        break;

                    case FunctionToExecute.PrinterReverse:
                        executePrinterReverse(parameter0);

                        break;

                    case FunctionToExecute.PrinterForward:
                        executePrinterForward(parameter0);

                        break;

                    case FunctionToExecute.PrintRtf:
                        returnValue = rtfPrinting(mainForm, parameter1, parameter2, parameter0);

                        break;
                    case FunctionToExecute.PrintTiff:
                        returnValue = tiffPrinting(mainForm, parameter1, parameter0, parameter2);

                        break;
                    case FunctionToExecute.PrintRDF:
                        returnValue = rdfPrinting(po, mainForm, parameter1, parameter2, parameter0, parameter3);

                        break;
                    case FunctionToExecute.DownloadTRIOSketchFile:
                        returnValue = downloadTRIOSketchFile(mainForm, parameter0);

                        break;
                }
            }
            catch (Exception e)
            {
                ClosePrinter(printerHandle);
                mainForm.LogError(e.Message);
                TRIOAssistant.Program.telemetryService.TrackException(e);
            }

            ClearParameters(po);
            po.ReturnValue = returnValue;

            await Communication.Send(WisejSerializer.Serialize(po, WisejSerializerOptions.None | WisejSerializerOptions.IgnoreNulls));
        }

        private static void executePrinterForward(dynamic parameter0)
        {
            StringBuilder sb;
            sb = new StringBuilder();

            SetScroll_Forward(sb);

            RawPrinterHelper.SendStringToPrinter((string) parameter0, sb.ToString());
        }

        private static void executePrinterReverse(dynamic parameter0)
        {
            StringBuilder sb;
            sb = new StringBuilder();

            SetCpi_10(sb);

            SetScroll_Backward(sb);

            RawPrinterHelper.SendStringToPrinter((string) parameter0, sb.ToString());
        }

        private static void executePrintXs(dynamic parameter1, dynamic parameter0)
        {
            StringBuilder sb;
            sb = new StringBuilder();

            SetCpi_10(sb);
            SetFont_Courier(sb);

            sb.Append((string) parameter1); // Xs

            SetScroll_Forward(sb);

            RawPrinterHelper.SendStringToPrinter((string) parameter0, sb.ToString());
        }

        private static int executeOpenPrinter(ParameterObjects po, dynamic parameter1, dynamic parameter0, dynamic parameter2)
        {
            int returnValue;
            int refPHPrinter = parameter1;
            returnValue = OpenPrinter(parameter0, out refPHPrinter, parameter2);
            po.Parameter[1].Value = refPHPrinter;

            if (returnValue != 0)
            {
                printerHandle = refPHPrinter;
            }

            return returnValue;
        }

        private static int executeStartDocPrinter(ParameterObjects po, dynamic parameter2, dynamic parameter0, dynamic parameter1)
        {
            int returnValue;
            DOCINFO refDOCINFO;
            refDOCINFO.pDatatype = parameter2.pDatatype;
            refDOCINFO.pDocName = parameter2.pDocName;
            refDOCINFO.pOutputFile = parameter2.pOutputFile;
            returnValue = StartDocPrinter(parameter0, parameter1, ref refDOCINFO);
            po.Parameter[2].Value = refDOCINFO;

            return returnValue;
        }

        private static int executeEnumForms(ParameterObjects po, dynamic parameter2, dynamic parameter4, dynamic parameter5, dynamic parameter0, dynamic parameter1, dynamic parameter3)
        {
            int returnValue;
            byte refPForm = parameter2;
            int refPCBNeeded = parameter4;
            int refPCReturned = parameter5;
            returnValue = EnumForms(parameter0, parameter1, ref refPForm, parameter3, ref refPCBNeeded, ref refPCReturned);
            po.Parameter[2].Value = refPForm;
            po.Parameter[4].Value = refPCBNeeded;
            po.Parameter[5].Value = refPCReturned;

            return returnValue;
        }

        private static int executeAddForm(ParameterObjects po, dynamic parameter2, dynamic parameter0, dynamic parameter1)
        {
            int returnValue;
            byte refPForm1 = parameter2;
            returnValue = AddForm(parameter0, parameter1, ref refPForm1);
            po.Parameter[2].Value = refPForm1;

            return returnValue;
        }

        private static int executeDocumentProperties(ParameterObjects po, dynamic parameter3, dynamic parameter4, dynamic parameter0, dynamic parameter1, dynamic parameter2, dynamic parameter5)
        {
            int returnValue;
            short refPDevModeOutput = parameter3;
            short refPDevModeInput = parameter4;
            returnValue = DocumentProperties(Convert.ToInt32(parameter0), parameter1, parameter2, ref refPDevModeOutput, ref refPDevModeInput, parameter5);
            po.Parameter[3].Value = refPDevModeOutput;
            po.Parameter[4].Value = refPDevModeInput;

            return returnValue;
        }

        private static int executeSaveMachinesPrinters(dynamic parameter0)
        {
            int returnValue;
            string machinesPrinters = parameter0;
            SaveChosenPrinters(machinesPrinters);
            returnValue = 1;

            return returnValue;
        }

        private static int executeGetMachinesPrinters(ParameterObjects po, dynamic parameter0)
        {
            int returnValue;
            string refPrinterList = parameter0;
            returnValue = GetChosenPrinters(ref refPrinterList);
            po.Parameter[0].Value = refPrinterList;

            return returnValue;
        }

                private static int executeSaveApplicationKey(dynamic parameter0)
        {
            int returnValue;
            string key = parameter0;
            SaveApplicationKey(key);
            returnValue = 1;

            return returnValue;
        }

        private static int executeGetApplicationKey(ParameterObjects po, dynamic parameter0)
        {
            
            int returnValue;
            string key = string.Empty;
            returnValue = GetApplicationKey(ref key);
            po.Parameter[0].Value = key;

            return returnValue;
        }

        private static int executeGetPrinters(ParameterObjects po)
        {
            int returnValue;
            var printers = new List<Printer>();
            returnValue = GetPrinters(ref printers);
            po.Parameter[0].Value = printers;

            return returnValue;
        }

        private static int executeSetForm(ParameterObjects po, dynamic parameter3, dynamic parameter0, dynamic parameter1, dynamic parameter2)
        {
            int returnValue;
            byte refPForm3 = parameter3;
            returnValue = SetForm(parameter0, parameter1, parameter2, ref refPForm3);
            po.Parameter[3].Value = refPForm3;

            return returnValue;
        }

        private static int executeGetForm(ParameterObjects po, dynamic parameter3, dynamic parameter5, dynamic parameter0, dynamic parameter1, dynamic parameter2, dynamic parameter4)
        {
            int returnValue;
            byte refPForm2 = parameter3;
            int refPCBNeeded1 = parameter5;
            returnValue = GetForm(parameter0, parameter1, parameter2, ref refPForm2, parameter4, ref refPCBNeeded1);
            po.Parameter[3].Value = refPForm2;
            po.Parameter[5].Value = refPCBNeeded1;

            return returnValue;
        }

        private static int rtfPrinting(TRIOAssistant.TRIOAssistant mainForm, dynamic parameter1, dynamic parameter2, dynamic parameter0)
        {
            int returnValue;
            mainForm.LogText("START RTF Printing");
            mainForm.richTextBoxPrintCtrl1.Rtf = parameter1;

            //apply page settings from ActiveReports
            var margins = parameter2?.Margins;
            ApplyPageSettings_RTF(mainForm, margins, parameter2);

            mainForm.printRTFDocument.PrinterSettings.PrinterName = parameter0;
            mainForm.printRTFDocument.Print();
            mainForm.LogText("END RTF Printing");
            returnValue = 1;

            return returnValue;
        }

        private static int tiffPrinting(TRIOAssistant.TRIOAssistant mainForm, dynamic parameter1, dynamic parameter0, dynamic parameter2)
        {
            int returnValue;
            mainForm.LogText("START TIFF Printing");

            //download tiff file from server
            string downloadURL = parameter1;
            var outPath = DownloadFile(downloadURL);

            mainForm.PrintTiffFileName = outPath;

            //set printer properties
            mainForm.printTIFFDocument.PrinterSettings.PrinterName = parameter0;

            //apply page settings from ActiveReports
            var margins = parameter2?.Margins;
            ApplyPageSettings_TIFF(margins, mainForm, parameter2);

            mainForm.printTIFFDocument.Print();
            mainForm.LogText("END TIFF Printing");
            returnValue = 1;

            return returnValue;
        }

        private static int rdfPrinting(ParameterObjects po, TRIOAssistant.TRIOAssistant mainForm, dynamic parameter1, dynamic parameter2, dynamic parameter0, dynamic parameter3)
        {
            int returnValue;
            mainForm.LogText("START RDF Printing");

            string downloadURL = parameter1;
            var outPath = DownloadFile(downloadURL);

            var margins = parameter2?.Margins;
            ApplyPageSettings_RDF(margins, parameter2, out float marginsLeft, out float marginsRight, out float marginsTop, out float marginsBottom, out PaperSize paperSize);

            var oTemp = new GrapeCity.ActiveReports.SectionReport();
            LoadAndPrep_RDF(oTemp.Document, po, outPath, parameter0, paperSize, marginsLeft, marginsRight, marginsTop, marginsBottom, parameter3);

            oTemp.Document.Load(outPath);
            oTemp.Document.Printer.PrinterSettings.Duplex = Duplex.Vertical;
            oTemp.Document.Print(false, false, true);
            oTemp.Dispose();

            mainForm.LogText("END RDF Printing");
            returnValue = 1;

            return returnValue;
        }

        private static int downloadTRIOSketchFile(TRIOAssistant.TRIOAssistant mainForm, dynamic parameter0)
        {
            int returnValue;
            mainForm.LogText("START Downloading TRIO Sketch file");

            string downloadURL = parameter0;
            var outPath = DownloadFile(downloadURL);

            mainForm.SketchFileName = outPath;
            mainForm.LoadSketchFile();

            mainForm.LogText("END downloading TRIO Sketch file");
            returnValue = 1;

            return returnValue;
        }

        private static void SetScroll_Backward(StringBuilder sb)
        {
            sb.Append((char)27);
            sb.Append((char)106); // "j"
            sb.Append((char)108);
        }

        private static void SetScroll_Forward(StringBuilder sb)
        {
            sb.Append((char)27);
            sb.Append((char)74); // "J"
            sb.Append((char)108);
        }

        private static void SetFont_Courier(StringBuilder sb)
        {
            sb.Append((char)27);  // ESC
            sb.Append((char)107); // set font
            sb.Append((char)48);  // Courier
            sb.Append((char)13);  // Go
        }

        private static void SetCpi_10(StringBuilder sb)
        {
            sb.Append((char)27); // ESC
            sb.Append((char)80); // select 10 cpi
            sb.Append((char)13); // Go
        }

        private static void SetCpi_12(StringBuilder sb)
        {
            sb.Append((char)27); // ESC
            sb.Append((char)77); // select 12 cpi
            sb.Append((char)13); // Go
        }

        private static void ClearParameters(ParameterObjects po)
        {
            foreach (var parm in po.Parameter.Where(parm => parm != null && !parm.IsRefParameter))
            {
                parm.Value = null;
            }
        }

        private static string DownloadFile(string downloadURL)
        {
            var outPath = System.IO.Path.GetTempFileName();

            using (var webClient = new System.Net.WebClient())
            {
                webClient.DownloadFile(downloadURL, outPath);
            }

            return outPath;
        }

        private static SectionDocument LoadAndPrep_RDF(SectionDocument document, ParameterObjects po, string rdfFilePath, dynamic parameter0, PaperSize paperSize, dynamic marginsLeft, float marginsRight, float marginsTop, float marginsBottom, dynamic parameter3)
        {
            //var document = new SectionDocument();
            document.Load(rdfFilePath);
            document.Printer.PrinterName = parameter0;
            document.Printer.PaperSize = paperSize;

            if (document.Printer.PrinterSettings.CanDuplex)
            {
                document.Printer.PrinterSettings.Duplex = Duplex.Default;
            }
            document.PrintOptions.Margin = new GrapeCity.ActiveReports.Extensibility.Printing.Margin(marginsLeft, marginsRight, marginsTop, marginsBottom);

            if (po.Parameter.Length >= 4 && po.Parameter[3] != null)
            {
	            document.Printer.PrinterSettings.PrintRange = PrintRange.SomePages;
                document.Printer.PrinterSettings.FromPage = parameter3.FromPage;
                document.Printer.PrinterSettings.ToPage = parameter3.ToPage;
            }

            return document;
        }

        #region Apply Page Settings

        private static void ApplyPageSettings_RDF(dynamic margins, dynamic parameter2, out float marginsLeft, out float marginsRight, out float marginsTop, out float marginsBottom, out PaperSize paperSize)
        {
            marginsLeft = Convert.ToSingle(margins.Left);
            marginsRight = Convert.ToSingle(margins.Right);
            marginsTop = Convert.ToSingle(margins.Top);
            marginsBottom = Convert.ToSingle(margins.Bottom);

            int paperWidth = Convert.ToInt32(Convert.ToDecimal(parameter2.PaperWidth) * 100);
            int paperHeight = Convert.ToInt32(Convert.ToDecimal(parameter2.PaperHeight) * 100);
            paperSize = new PaperSize("TRIOAssistant", paperWidth, paperHeight);

        }

        private static void ApplyPageSettings_TIFF(dynamic margins, TRIOAssistant.TRIOAssistant mainForm, dynamic parameter2)
        {
            int marginsLeft = Convert.ToInt32(margins.Left * 100);
            int marginsRight = Convert.ToInt32(margins.Right * 100);
            int marginsTop = Convert.ToInt32(margins.Top * 100);
            int marginsBottom = Convert.ToInt32(margins.Bottom * 100);

            //set print document properties
            var defaultPageSettings = mainForm.printTIFFDocument.DefaultPageSettings;
            defaultPageSettings.Margins.Left = marginsLeft;
            defaultPageSettings.Margins.Right = marginsRight;
            defaultPageSettings.Margins.Top = marginsTop;
            defaultPageSettings.Margins.Bottom = marginsBottom;
            int paperWidth = Convert.ToInt32(Convert.ToDecimal(parameter2.PaperWidth) * 100);
            int paperHeight = Convert.ToInt32(Convert.ToDecimal(parameter2.PaperHeight) * 100);
            var paperSize = new PaperSize("TRIOAssistant", paperWidth, paperHeight);
            defaultPageSettings.PaperSize = paperSize;
        }

        private static void ApplyPageSettings_RTF(TRIOAssistant.TRIOAssistant mainForm, dynamic margins, dynamic parameter2)
        {
            var defaultPageSettings = mainForm.printRTFDocument.DefaultPageSettings;
            defaultPageSettings.Margins.Left = Convert.ToInt32(margins.Left * 100);
            defaultPageSettings.Margins.Right = Convert.ToInt32(margins.Right * 100);
            defaultPageSettings.Margins.Top = Convert.ToInt32(margins.Top * 100);
            defaultPageSettings.Margins.Bottom = Convert.ToInt32(margins.Bottom * 100);
            int paperWidth = Convert.ToInt32(Convert.ToDecimal(parameter2.PaperWidth) * 100);
            int paperHeight = Convert.ToInt32(Convert.ToDecimal(parameter2.PaperHeight) * 100);
            defaultPageSettings.PaperSize = new PaperSize("TRIOAssistant", paperWidth, paperHeight);
        }

        #endregion

        private static int GetPrinters(ref List<Printer> printers)
        {

            printers.Clear();

            var printServer = new PrintServer();
            PrintQueueCollection printQueueCollection = printServer.GetPrintQueues(
                new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });

            var defaultPrintQueue = ""; //printServer.DefaultPrintQueue;
            var defaultName = ""; //defaultPrintQueue.FullName;

            // get all printers installed (from the users perspective)
            //var printerNames = PrinterSettings.InstalledPrinters;

            // var availablePrinters = new List<string>();   // = printerNames.Cast<string>()

            foreach (var queue in printQueueCollection)
            {
                try
                {
                    var capabilities = queue.GetPrintCapabilities();
                    var maxHorizontal = 0;
                    if (capabilities.PageResolutionCapability.Count == 0) maxHorizontal = 5;

                    foreach (var pageRes in capabilities.PageResolutionCapability)
                    {
                        switch (pageRes.QualitativeResolution)
                        {
                            case PageQualitativeResolution.Default:
                            case PageQualitativeResolution.Draft:
                                maxHorizontal = maxHorizontal == 0 ? 300 : maxHorizontal;

                                break;
                            case PageQualitativeResolution.Normal:
                            case PageQualitativeResolution.High:
                                maxHorizontal = 1200;

                                break;
                        }
                    }

                    string printerName = queue.FullName;

                    if (printerName.Contains("ipp:") || printerName.Contains("http:"))
                    {
                        printerName = printerName.Substring(2).Replace("\\", "/");
                    }


                    var canDuplex = capabilities.DuplexingCapability.Contains(Duplexing.TwoSidedLongEdge);
                    printers.Add(new Printer
                    {
                        Name = printerName,
                        Default = false, //queue.FullName == defaultName,
                        PortName = queue.QueuePort?.Name,
                        CanDuplex = canDuplex,
                        IsDotMatrix = maxHorizontal > 0 && maxHorizontal < 300 && !canDuplex
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                    TRIOAssistant.Program.telemetryService.TrackException(e);
                }
            }
            //.Select(printerName =>
            // {

            //     if (printerName.Contains("ipp:") || printerName.Contains("http:"))
            //     {
            //         printerName = printerName.Substring(2);
            //     }

            //     var matchPrintServer = Regex.Match(printerName, @"(?<printserver>(http:.*|ipp:.*))\\(?<printer>.*)");
            //     var matchUNC = Regex.Match(printerName, @"(?<machine>\\\\.*?)\\(?<queue>.*)");

            //     PrintQueue queue;

            //     try
            //     {
            //         var printServerName = "";

            //         if (matchPrintServer.Success)
            //         {
            //             var printSvr = matchPrintServer.Groups["printserver"].Value?.Replace("ipp:", "http:");

            //             try
            //             {
            //                 printServerName = $"{printSvr}/printers/{matchPrintServer.Groups["printer"].Value}/.printer";
            //                 MessageBox.Show($"...hitting {printServerName}");
            //                 queue = new PrintServer(printServerName).GetPrintQueues().FirstOrDefault();
            //             }
            //             catch (Exception e)
            //             {
            //                 printServerName = $"{matchPrintServer.Groups["printserver"].Value}";
            //                 MessageBox.Show($"...didn't work so hitting {printServerName}");
            //                 queue = new PrintServer(printServerName).GetPrintQueue($"{matchPrintServer.Groups["printer"].Value}");
            //             }
            //         }

            //         else
            //         {
            //             if (matchUNC.Success)
            //             {
            //                 printServerName = $"{matchUNC.Groups["machine"].Value}";
            //                 queue = new PrintServer(printServerName).GetPrintQueue(matchUNC.Groups["queue"].Value);
            //             }
            //             else
            //             {
            //                 queue = printServer.GetPrintQueue(printerName);
            //                 printServerName = printerName;
            //             }
            //         }

            //         if (queue == null)
            //         {
            //             return null;
            //         }

            //         var capabilities = queue.GetPrintCapabilities();
            //         var maxHorizontal = 0;
            //         if (capabilities.PageResolutionCapability.Count == 0) maxHorizontal = 5;

            //         foreach (var pageRes in capabilities.PageResolutionCapability)
            //         {
            //             switch (pageRes.QualitativeResolution)
            //             {
            //                 case PageQualitativeResolution.Default:
            //                 case PageQualitativeResolution.Draft:
            //                     maxHorizontal = maxHorizontal == 0 ? 300 : maxHorizontal;

            //                     break;
            //                 case PageQualitativeResolution.Normal:
            //                 case PageQualitativeResolution.High:
            //                     maxHorizontal = 1200;

            //                     break;
            //             }
            //         }

            //         var canDuplex = capabilities.DuplexingCapability.Contains(Duplexing.TwoSidedLongEdge);

            //         return new Printer
            //         {
            //             Name = printServerName,
            //             Default = queue.FullName == defaultName,
            //             PortName = queue.QueuePort?.Name,
            //             CanDuplex = canDuplex,
            //             IsDotMatrix = maxHorizontal > 0 && maxHorizontal < 300 && !canDuplex
            //         };
            //     }
            //     catch (Exception e)
            //     {
            //         MessageBox.Show($"Error: {e.GetBaseException().Message}");

            //         Console.WriteLine(e);
            //         Program.telemetryService?.TrackException(e);

            //         return new Printer
            //         {
            //             Name = "ERROR",
            //             Default = false,
            //             PortName = e.GetBaseException().Message,
            //             CanDuplex = false,
            //             IsDotMatrix = false
            //         };
            //     }
            // })
            //.ToArray();

            //foreach (var printer in availablePrinters.Where(printer => printer.Name != "ERROR"))
            //{
            //    printers.Add(printer);
            //    Console.WriteLine(printer);
            //}

            return 1;

        }
        /// <summary>
        /// Gets the set of printers chosen for this machine
        /// </summary>
        /// <param name="json"></param>
        /// <returns>List of printers in a json string</returns>
        private static int GetChosenPrinters(ref string json)
        {
            // if file exists
            if (File.Exists("printers.txt"))
            {
                // open printers file
                using (var printersFile = File.OpenText("printers.txt"))
                {
                    // get printers list
                    var printerJson = printersFile.ReadToEnd();
                    json = printerJson;
                }
            }
            else
            {
                json = JsonConvert.SerializeObject(new List<Tuple<string, string>>());
            }

            return 1;
        }

        private static void SaveChosenPrinters(string printerList)
        {
            // create or open the printers file
            using (Stream fileStream = File.Open("printers.txt", FileMode.Create))
            {
                fileStream.Write(Encoding.UTF8.GetBytes(printerList), 0, printerList.Length);
            }
        }

        public static string GetApplicationSettingsFolder()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            var keyPath = Path.Combine(path, "TrioAssistant");
            try
            {
                if (!Directory.Exists(keyPath))
                {
                    Directory.CreateDirectory(keyPath);
                }

                return keyPath;
            }
            catch
            {
                return "";
            }
        }
        
        public static int GetApplicationKey(ref string key)
        {
            var keyPath = GetApplicationSettingsFolder();
            var fileName = "appkey.txt";
            var filePath = Path.Combine(keyPath, fileName);

            if (File.Exists(filePath))
            {
                using (var appkeyFile = File.OpenText(filePath))
                {
                    // get key
                    key = appkeyFile.ReadToEnd();
                    return 1;
                }

            }
            else
            {
                if (File.Exists(fileName))
                {
                    filePath = fileName;
                    using (var appkeyFile = File.OpenText(filePath))
                    {
                        // get key
                        key = appkeyFile.ReadToEnd();
                        SaveApplicationKey(key);
                        return 1;
                    }
                }
                
            }
            if (string.IsNullOrWhiteSpace(key))
            {

                // create the file and save a new value
                key =  Guid.NewGuid().ToString().ToUpper().Replace("-", "");
                SaveApplicationKey(key);

                return 1;
            }

            return 0;
        }

        private static void SaveApplicationKey(string key)
        {
            var keyPath = GetApplicationSettingsFolder();
            var fileName = "appkey.txt";
            var filePath = Path.Combine(keyPath, fileName);
            // create or open the printers file
            using (Stream fileStream = File.Open(filePath, FileMode.Create))
            {
                fileStream.Write(Encoding.UTF8.GetBytes(key), 0, key.Length);
            }
        }
    }
}
