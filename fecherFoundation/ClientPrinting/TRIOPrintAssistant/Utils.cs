﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TRIOAssistant
{
    public static class Utils
    {
        public static bool EqualsWith(this Bitmap bitmap1, Bitmap bitmap2)
        {
            if ((bitmap1 == null) != (bitmap2 == null)) return false;
            if (bitmap1.Size != bitmap2.Size) return false;

            var bitmap1Data = bitmap1.LockBits(new Rectangle(new Point(0, 0), bitmap1.Size), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            var bitmap2Data = bitmap2.LockBits(new Rectangle(new Point(0, 0), bitmap2.Size), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

            try
            {
                IntPtr bitmap1DataScan0 = bitmap1Data.Scan0;
                IntPtr bitmap2DataScan0 = bitmap2Data.Scan0;

                int stride = bitmap1Data.Stride;
                int length = stride * bitmap1.Height;

                return NativeMethods.memcmp(bitmap1DataScan0, bitmap2DataScan0, length) == 0;
            }
            finally
            {
                bitmap1.UnlockBits(bitmap1Data);
                bitmap2.UnlockBits(bitmap2Data);
            }
        }

        public static Bitmap GetImageFromClipboard()
        {
            NativeMethods.OpenClipboard(IntPtr.Zero);
            IntPtr hemf = NativeMethods.GetClipboardData(NativeMethods.CF_ENHMETAFILE);
            NativeMethods.CloseClipboard();
            if (hemf != IntPtr.Zero)
            {   
                Metafile mf = new Metafile(hemf, true); // true means that we don't need to release the hemf ourselves
                MetafileHeader header = mf.GetMetafileHeader();

                float scale = 2f;
                var width = (int)Math.Round((scale * mf.Width / header.DpiX * 100), 0, MidpointRounding.ToEven);
                var height = (int)Math.Round((scale * mf.Height / header.DpiY * 100), 0, MidpointRounding.ToEven);

                Bitmap bitmap = new Bitmap((int)(width), (int)(height));
                {
                    using (Graphics g = Graphics.FromImage(bitmap))
                    {
                        g.Clear(Color.White);
                        g.ScaleTransform(scale, scale);
                        g.DrawImage(mf, 0, 0);
                    }
                }
                return bitmap;
            }

            return null;
        }

        private static int topmost = 0, bottommost = 0, leftmost = 0, rightmost = 0;
        public static Bitmap CropWhiteSpace(Bitmap bmp, int padding = 5)
        {
            int w = bmp.Width;
            int h = bmp.Height;
            int white = 0xffffff;
            topmost = bottommost = leftmost = rightmost = 0;

            Func<int, bool> allWhiteRow = r =>
            {
                for (int i = 0; i < w; ++i)
                    if ((bmp.GetPixel(i, r).ToArgb() & white) != white)
                        return false;
                return true;
            };

            Func <int, bool> allWhiteColumn = c =>
            {
                for (int i = topmost; i < bottommost; ++i)
                    if ((bmp.GetPixel(c, i).ToArgb() & white) != white)
                        return false;
                return true;
            };

            //int topmost = 0;
            for (int row = 0; row < h; ++row)
            {
                if (!allWhiteRow(row))
                    break;
                topmost = row;
            }

            //int bottommost = 0;
            for (int row = h - 1; row >= 0; --row)
            {
                if (!allWhiteRow(row))
                    break;
                bottommost = row;
            }

            //int leftmost = 0, rightmost = 0;
            for (int col = 0; col < w; ++col)
            {
                if (!allWhiteColumn(col))
                    break;
                leftmost = col;
            }

            for (int col = w - 1; col >= 0; --col)
            {
                if (!allWhiteColumn(col))
                    break;
                rightmost = col;
            }

            //add padding
            leftmost = (leftmost > padding) ? leftmost - padding : 0;
            topmost = (topmost > padding) ? topmost - padding : 0;
            rightmost = (w - rightmost > padding) ? rightmost + padding : w;
            bottommost = (h - bottommost > padding) ? bottommost + padding : h;

            if (rightmost == 0) rightmost = w; // As reached left
            if (bottommost == 0) bottommost = h; // As reached top.

            int croppedWidth = rightmost - leftmost;
            int croppedHeight = bottommost - topmost;

            if (croppedWidth == 0) // No border on left or right
            {
                leftmost = 0;
                croppedWidth = w;
            }

            if (croppedHeight == 0) // No border on top or bottom
            {
                topmost = 0;
                croppedHeight = h;
            }

            try
            {
                var target = new Bitmap(croppedWidth, croppedHeight);
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(bmp,
                      new RectangleF(0, 0, croppedWidth, croppedHeight),
                      new RectangleF(leftmost, topmost, croppedWidth, croppedHeight),
                      GraphicsUnit.Pixel);
                }
                return target;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight),
                  ex);
            }
        }

        /// <summary>
        /// Create an Icon that calls DestroyIcon() when the Destructor is called.
        /// Unfortunately Icon.FromHandle() initializes with the internal Icon-constructor Icon(handle, false), which sets the internal value "ownHandle" to false
        /// This way because of the false, DestroyIcon() is not called as can be seen here:
        /// https://referencesource.microsoft.com/#System.Drawing/commonui/System/Drawing/Icon.cs,f2697049dea34e7c,references
        /// To get around this we get the constructor internal Icon(IntPtr handle, bool takeOwnership) from Icon through reflection and initialize that way
        /// </summary>
        public static Icon BitmapToIcon(Bitmap bitmap)
        {
            Type[] cargt = new[] { typeof(IntPtr), typeof(bool) };
            ConstructorInfo ci = typeof(Icon).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, cargt, null);
            object[] cargs = new[] { (object)bitmap.GetHicon(), true };
            Icon icon = (Icon)ci.Invoke(cargs);
            return icon;
        }
    }
}
