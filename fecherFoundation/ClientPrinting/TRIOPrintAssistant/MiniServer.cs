﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace TRIOAssistant
{
    public static  class MiniServer
    {

        public static string ApplicationKey { get; set; } = "";
        
        /// <summary>
        /// Listens to the given uri
        /// </summary>
        /// <param name="prefixes"></param>
        public static void Listen(string[] prefixes)
        {
            HttpListener listener = new HttpListener();
            foreach (string s in prefixes)
            {
                listener.Prefixes.Add(s);
            }

            listener.Start();
            StartListening(listener);
            //while (true)
            //{
            //    IAsyncResult result = listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);
            //    result.AsyncWaitHandle.WaitOne();
            //}
        }

        private static void StartListening(HttpListener listener)
        {
            IAsyncResult result = listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);
        }

        public static void ListenerCallback(IAsyncResult result)
        {
            HttpListener listener = (HttpListener)result.AsyncState;
            HttpListenerContext context = listener.EndGetContext(result);
            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;

            //if (request.HttpMethod == "OPTIONS")
            //{
                response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
                response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
                response.AddHeader("Access-Control-Max-Age", "1728000");
            //}
            //else
            //{

            //}
            response.AppendHeader("Access-Control-Allow-Origin", "*");

            string responseString = "{\"machineId\":\"" + ApplicationKey + "\"}";
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            response.ContentType = @"application/json";
            response.StatusCode = 200;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();
            StartListening(listener);
        }
    }
}