﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DirectPrintingApp;
using Microsoft.Win32;

namespace TRIOPrintAssistant
{
    public partial class PrintAssistant : Form
    {
        private int checkPrint;
        string websocketURL = string.Empty;
        public PrintAssistant()
        {
            InitializeComponent();
        }

        public void LogError(string message)
        {
            logTextBox.AppendText(string.Format("{0}:{1} - {2}{3}", DateTime.Now.ToShortTimeString(), "ERROR", message, Environment.NewLine), Color.Red);
        }

        public void LogText(string message)
        {
            logTextBox.AppendText(string.Format("{0}:{1} - {2}{3}", DateTime.Now.ToShortTimeString(), "LOG", message, Environment.NewLine));
        }

        private void PrintAssistant_Load(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //add TRIO Print assistant to Windows startup
            string configKey = "AddedToWindowsStartup";
            string keyValue = GetOrCreateConfig(config, configKey);
            if (string.IsNullOrEmpty(keyValue) || keyValue != "true")
            {
                try
                {
                    using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
                    {
                        key.SetValue("TRIO Print Assistant", "\"" + Application.ExecutablePath + "\"");
                    }
                    SaveConfigValue(config, configKey, "true");
                    MessageBox.Show("TRIO Print assistant was added to Windows Startup", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please run TRIO Print Assistant with admin rights, in order to add it to Windows Startup", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            //check if print assistant key exist in the app.config file, and generate a new key if it doesn't exist
            configKey = "ApplicationKey";
            keyValue = GetOrCreateConfig(config, configKey);
            if (string.IsNullOrEmpty(keyValue))
            {
                keyValue = Guid.NewGuid().ToString().ToUpper().Replace("-", "");
                SaveConfigValue(config, configKey, keyValue);
            }
            this.textBox1.Text = Convert.ToString(keyValue);

            //check if websocket url is saved in the convig, and load it from config file
            configKey = "WebSocketURL";
            keyValue = GetOrCreateConfig(config, configKey);
            this.textBox2.Text = keyValue;
            websocketURL = keyValue;

            this.Connect();
        }

        private string GetOrCreateConfig(Configuration config, string configKey)
        {
            if (!config.AppSettings.Settings.AllKeys.Contains(configKey))
            {
                config.AppSettings.Settings.Add(configKey, "");
            }

            var appConfigSetting = config.AppSettings.Settings[configKey];
            if (appConfigSetting == null)
            {
                appConfigSetting = new KeyValueConfigurationElement(configKey, string.Empty);
                config.AppSettings.Settings.Add(appConfigSetting);
                config.Save();
            }
            return appConfigSetting.Value;
        }


        private void SaveConfigValue(Configuration config, string configKey, string keyValue)
        {
            var appConfigSetting = config.AppSettings.Settings[configKey];
            appConfigSetting.Value = keyValue;
            config.Save();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Connect();
        }

        private void Connect()
        {
            try
            {
                var webSocketServiceURL = this.textBox2.Text.Trim();
                string uri = webSocketServiceURL + "?machine=" + this.textBox1.Text;
                Communication.Connect(uri);
                button1.Enabled = false;
                button2.Enabled = true;
                connectionStatusLabel.Text = "Connected";

                //save config with the new uri, if value was changed
                if (websocketURL != webSocketServiceURL)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    string configKey = "WebSocketURL";
                    SaveConfigValue(config, configKey, webSocketServiceURL);
                    websocketURL = webSocketServiceURL;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eror connecting to the web server", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.textBox1.Text);
        }

        internal void WebSocketClosed()
        {
            button1.Enabled = true;
            button2.Enabled = false;
            connectionStatusLabel.Text = "Disconnected";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Communication.Disconnect();
            button1.Enabled = true;
            button2.Enabled = false;
            connectionStatusLabel.Text = "Disconnected";
        }

        private void PrintAssistant_FormClosed(object sender, FormClosedEventArgs e)
        {
            var webSocketServiceURL = this.textBox2.Text.Trim();
            //save config with the new uri, if value was changed
            if (websocketURL != webSocketServiceURL)
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string configKey = "WebSocketURL";
                SaveConfigValue(config, configKey, webSocketServiceURL);
                websocketURL = webSocketServiceURL;
            }
            Communication.Disconnect();
        }

        private void PrintAssistant_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void printDocument1_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            checkPrint = 0;
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            checkPrint = richTextBoxPrintCtrl1.Print(checkPrint, richTextBoxPrintCtrl1.TextLength, e);

            // Check for more pages
            if (checkPrint < richTextBoxPrintCtrl1.TextLength)
            {
                e.HasMorePages = true;
            }
            else
            {
                e.HasMorePages = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            TRIOPrintAssistant.Program.MainForm.LogText("START RTF Printing");
            //TRIOPrintAssistant.Program.MainForm.richTextBoxPrintCtrl1.Rtf = po.Parameter[1].Value;
            //Microsoft Print to PDF
            TRIOPrintAssistant.Program.MainForm.printDocument1.PrinterSettings.PrinterName = "Dell 2330dn Laser Printer";
            TRIOPrintAssistant.Program.MainForm.printDocument1.Print();
            TRIOPrintAssistant.Program.MainForm.LogText("END RTF Printing");
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
