﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    /// <summary>
    /// A class which holds client printer information
    /// </summary>
    public class Printer
    {
        public string Name { get; set; }
        public string DriverName { get; set; }
        public string PortName { get; set; }
        public bool Default { get; set; }
        public bool IsDotMatrix { get; set; }
        public bool CanDuplex { get; set; }
    }
}
