﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Wisej.Core;

namespace fecherFoundation
{
    /// <summary>
    /// is used in PrintingViaWebSockets to save and restore the Contexts over the Thread bounds
    /// </summary>
    public class InterCommunicationContexts
    {
        public WebSocketContext WebSocketContext { get; set; }
        public IWisejComponent WisejContext { get; set; }
        public ParameterObjects poResult { get; set; }
        public bool ResultReceived { get; set; }
        //public string ErrorMessage { get; set; }
        //public string ErrorStackTrace { get; set; }
        public Exception Exception { get; set; }
        public Object Locker { get; set; }
    }
}
