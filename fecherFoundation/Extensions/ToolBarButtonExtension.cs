﻿#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Resources;
#endif

namespace fecherFoundation.Extensions
{
    /// <summary>
    /// ToolBarButtonExtension class
    /// </summary>
    public static class ToolBarButtonExtension
    {
        /// <summary>
        /// Sets the Description for the ToolBarButton
        /// </summary>
        /// <param name="objToolBarButton">The obj tool bar button.</param>
        /// <param name="strDescription">The STR description.</param>
        public static void Description(this ToolBarButton objToolBarButton, string strDescription)
        {
            //get the ToolBarButtonDefinitions
            ToolBarButtonDefinitions objToolBarButtonDefinitions = ToolBarButtonDefinitions.GetToolBarButtonDefinitions(objToolBarButton);
            
            //set the value of the Description property in the definitions
            objToolBarButtonDefinitions.Description = strDescription;
        }

        public static string Description(this ToolBarButton objToolBarButton)
        {
            //get the ToolBarButtonDefinitions
            ToolBarButtonDefinitions objToolBarButtonDefinitions = ToolBarButtonDefinitions.GetToolBarButtonDefinitions(objToolBarButton);

            //return the value of the Description property from the definitions
            return objToolBarButtonDefinitions.Description;
        }
    }

    public class ToolBarButtonDefinitions : ControlDefinitions
    {
        public static ToolBarButtonDefinitions GetToolBarButtonDefinitions(ToolBarButton objToolBarButton)
        {
            ToolBarButtonDefinitions objToolBarButtonDefinitions;

            #if Winforms
            objToolBarButtonDefinitions = new ToolBarButtonDefinitions();
            #else
            if (objToolBarButton.SystemTag == null)
            {
                objToolBarButtonDefinitions = new ToolBarButtonDefinitions();

                objToolBarButton.SystemTag = objToolBarButtonDefinitions;
            }
            else
            {
                objToolBarButtonDefinitions = (ToolBarButtonDefinitions)objToolBarButton.SystemTag;
            }
            #endif
            return objToolBarButtonDefinitions;
        }

        private string strDescription;

        public string Description
        {
            get
            {
                return strDescription;
            }
            set
            {
                strDescription = value;
            }
        }
    }
}
