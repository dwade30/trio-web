﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace fecherFoundation.Extensions
{
    public static class ListExtension
    {
        public static void Load<T>(this List<T> list, int index) where T : Component
        {
            FCUtils.Load(list, index);
        }
        public static void Unload<T>(this List<T> list, int index) where T : Component
        {
            FCUtils.Unload(list, index);
        }
        public static void AddControlArrayElement<T>(this List<T> list, T element, int index) where T : Component
        {
            //ensure position
            for (int i = list.Count; i <= index; i++)
            {
                list.Add(null);
            }
            //add element in list
            list[index] = element;
            //set ControlArray
            element.SetControlArray(list);
        }

        public static short GetIndex<T>(this List<T> list, T element) where T : Component
        {
            return (short)list.IndexOf(element);
        }
    }
}
