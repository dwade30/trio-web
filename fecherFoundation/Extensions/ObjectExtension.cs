﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation.Extensions
{
    public static class ObjectExtension
    {
        /// <summary>
        /// extension needed because FCListBox is derived from ListView and FCListBox.Items(i).Text returns ListViewItem, not the correct Text of the item that corresponds to ListBox.Items(i).Text
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToStringX(this object obj)
        {
            if (obj.GetType() == typeof(ListViewItem))
            {
                return ((ListViewItem)obj).Text;
            }

            else return obj.ToString();
        }
    }
}
