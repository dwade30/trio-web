﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

namespace fecherFoundation.Extensions
{
    public static class GlobalExtension
    {
        /// <summary>
        /// Loads the specified obj form.
        /// </summary>
        /// <param name="objForm">The obj form.</param>
        public static void Load(object objForm)
        {
        }

        /// <summary>
        /// Uns the load.
        /// </summary>
        /// <param name="objForm">The obj form.</param>
        public static void Unload(object objForm)
        {
        }

#if Winforms
        /// <summary>
        /// Loads the res picture.
        /// </summary>
        /// <param name="objIndex">Index of the obj.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        public static Image LoadResPicture(object objIndex, int format)
#else
            public static Image LoadResPicture(int intIndex, int format)
#endif
        {
            return null;
        }
       

        /// <summary>
        /// PRinters colection
        /// </summary>
        public static Dictionary<String, PrinterHelper> mobjPrinters;

        /// <summary>
        /// Gets the printers.
        /// </summary>
        public static Dictionary<String, PrinterHelper> Printers
        {
            get
            {
                //If no printers collection
                if (mobjPrinters == null)
                {
                    //New List of printers
                    mobjPrinters = new Dictionary<String, PrinterHelper>();
                }


                //Go over the instaled printers
                foreach (string strPrinterName in PrinterSettings.InstalledPrinters)
                {
                    if (mobjPrinters.ContainsKey(strPrinterName))
                    {
                        //Create a new Printer object
                        mobjPrinters.Add(strPrinterName, new PrinterHelper(strPrinterName));
                    }
                }
                //Return the printers
                return mobjPrinters;
            }

        }

        /// <summary>
        /// The default printer
        /// </summary>
        public static PrinterHelper mobjPrinter;

        /// <summary>
        /// The default printer
        /// </summary>
        public static PrinterHelper Printer
        {
            get
            {
                //If we found the printer
                if (mobjPrinter == null)
                {
                    //Get the printer setting
                    PrinterSettings objPrinterSettings = new PrinterSettings();

                    //Make sure that no printer name is given
                    objPrinterSettings.PrinterName = null;

                    //Get the default printer
                    mobjPrinter = new PrinterHelper(objPrinterSettings.PrinterName);
                }
                //Return printer
                return mobjPrinter;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PrinterHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public PrintDocument objPrintDocument;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrinterHelper"/> class.
        /// </summary>
        /// <param name="strPrinterName">Name of the STR printer.</param>
        internal PrinterHelper(string strPrinterName)
        {
            //Create a print document
            objPrintDocument = new PrintDocument();

            //Set the printer name to the printer settings in the document
            this.objPrintDocument.PrinterSettings.PrinterName = strPrinterName;
        }

    }
}
