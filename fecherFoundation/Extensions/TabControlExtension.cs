﻿using System;
using System.ComponentModel;
using System.Drawing;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif

namespace fecherFoundation.Extensions
{
    public static class TabControlExtension
    {
        public static void TabVisible(this TabControl objTabControl, int intIndex, bool boolState)
        {
            //BAN - TabPage has a Visible property but it is set to non browsable
            objTabControl.TabPages[intIndex].Visible = boolState;
        }

        public static bool TabVisible(this TabControl objTabControl, int intIndex)
        {
            //BAN - TabPage has a Visible property but it is set to non browsable
            return objTabControl.TabPages[intIndex].Visible;
        }

        //BAN - dynamically create tab pages
        public static int CreatePages(this TabControl objTabControl, int count)
        {
            try
            {
                for (int counter = 0; counter < count; counter++)
                {
                    objTabControl.TabPages.Add(new TabPage());
                }

                return objTabControl.TabPages.Count;
            }
            catch
            {
                return 0;
            }
        }

        public static readonly object TabPageVisibilityExtenderKey = new object();
        public static readonly object TabPageIsEnabledExtenderKey = new object();

        /// <summary>
        /// Sets the color of the TabControl TabPages.
        /// </summary>
        /// <param name="objTabControl">The tab control object.</param>
        /// <param name="objColor">Color to set to the TabPages of the TabControl</param>
        public static void SetForeColor(this TabControl objTabControl, Color objColor)
        {
            // Get the collection of TabPages from the TabControl
            TabControl.TabPageCollection objTabPageCollection = objTabControl.TabPages;

            // If the TabCollection is not NULL
            if (objTabPageCollection != null)
            {
                // Loop through all the TabPages
                foreach (TabPage objTabPage in objTabPageCollection)
                {
                    // Set the fore color to the TabPage
                    objTabPage.ForeColor = objColor;
                }
            }
        }


        /// <summary>
        /// Sets the TabEnabled property.
        /// </summary>
        /// <param name="objTabControl">The tab control.</param>
        /// <param name="intIndex">The Index.</param>
        /// <param name="blnIsTabEnabled">if set to <c>true</c>the tab will be enabled.</param>
        public static void SetTabEnabled(this TabControl objTabControl, int intIndex, bool blnIsTabEnabled)
        {
            // If TabControl is NOT NULL
            if (objTabControl == null)
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
            else
            {
                // If TabPage should be enabled
                if (blnIsTabEnabled)
                {
                    // If tab is set to "Enabled", then it should be removed from the extender
                    GenericExtender.RemoveValueFromExtender(objTabControl, TabPageIsEnabledExtenderKey, intIndex);

                    // If no values exist in disabled tab pages Extender
                    if (!GenericExtender.ValueExistInExtender(objTabControl, TabPageIsEnabledExtenderKey))
                    {
                        // Sign out from the "Selecting" event
                        objTabControl.Selecting -= objTabControl_Selecting;
                    }
                }
                else
                {
                    // Set the value in to the cache
                    GenericExtender.SetValue(objTabControl, TabPageIsEnabledExtenderKey, intIndex, blnIsTabEnabled);

                    // Sign up for the "Selecting" event
                    objTabControl.Selecting += objTabControl_Selecting;
                }
            }
        }

        /// <summary>
        /// Handles the Selecting event of the objTabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TabControlCancelEventArgs"/> instance containing the event data.</param>
        public static void objTabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            // Cast sender as Component
            Component objComponent = sender as Component;

            // If Component is NULL
            if (objComponent == null)
            {
                return;
            }

            // Check if such index exists in the Component Indexer
            bool blnValueExistInExtender = GenericExtender.ValueExistInExtender(objComponent, TabPageIsEnabledExtenderKey, e.TabPageIndex);

            // if value already exists in extender
            if (blnValueExistInExtender)
            {
                // Check if the TabPage should be Enabled
                bool blnIsTabEnabled = GenericExtender.GetOrCreateValue<bool>(objComponent, TabPageIsEnabledExtenderKey, e.TabPageIndex, false);

                // If tab should be disabled
                if (!blnIsTabEnabled)
                {
                    // Don't activate the clicked TabPage
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Sets the tab visible.
        /// </summary>
        /// <param name="objTabControl">The  tab control.</param>
        /// <param name="blnIsVisible">if set to <c>true</c> visible tab.</param>
        public static void SetTabVisible(this TabControl objTabControl, int intIndex, bool blnIsVisible)
        {

        }
        /// <summary>
        /// Gets the tab visibility.
        /// </summary>
        /// <param name="objTabControl">The tab control.</param>
        /// <param name="intIndex">Index of the tab page.</param>
        /// <returns></returns>
        public static bool GetTabVisible(this TabControl objTabControl, int intIndex)
        {
            // Initialize a value for tab visibility
            bool blnIsTabVisible = false;

            // Try get the TabPage from the extender cache
            TabPage objTabPage = GenericExtender.GetOrCreateValue<TabPage>(objTabControl, TabPageVisibilityExtenderKey, intIndex, false);

            // if the TabPage is not in the cache
            if (objTabPage == null)
            {
                // Set visible to true (if it's not in the cache, it's visible on TabControl)
                blnIsTabVisible = true;
            }
            return blnIsTabVisible;
        }

        /// <summary>
        /// Sets the tabs per row.
        /// </summary>
        /// <param name="objTabControl">The tab control.</param>
        /// <param name="intTabsPerRow">The tabs per row.</param>
        public static void SetTabsPerRow(this TabControl objTabControl, int intTabsPerRow)
        {
            // If has more tab pages than single row can contain
            if (intTabsPerRow < objTabControl.TabCount)
            {
                // Set multiline properties
                objTabControl.Multiline = true;
                objTabControl.SizeMode = TabSizeMode.Fixed;
                objTabControl.ItemSize = new Size(objTabControl.DisplayRectangle.Width / intTabsPerRow, objTabControl.ItemSize.Height);
            }
            else
            {
                // Set single-line properties
                objTabControl.Multiline = false;
            }

        }

        /// <summary>
        ///  Returns the top coordinate of the internal area of the control
        /// </summary>
        /// <param name="objTabControl">Tab Control</param>
        /// <returns>top from client internal area</returns>
        public static int GetClientTop(this TabControl objTabControl)
        {

            // Check for null reference
            if (objTabControl != null)
            {

                // Check if there is tab pages and check aligment
                if (objTabControl.TabCount > 0 && objTabControl.Alignment == TabAlignment.Top)
                {

                    // Create tab page object
                    TabPage objTabPage = objTabControl.TabPages[0];

                    // Create rectangle object
                    Rectangle objRectangle = objTabPage.Bounds;

                    // Get button height
                    int intHeight = objRectangle.Y;

                    // Get control location
                    Point objPoint = objTabControl.Location;

                    // Calculate top
                    return objPoint.Y + intHeight;

                }

                else
                {

                    // If tab control tabs count is 0 then return default tab control top
                    return objTabControl.Top + 2;

                }

            }

            // If object is null throw exception
            throw new NullReferenceException();
        }
        
        /// <summary>
        /// Sets top coordinate of the internal area of the control
        /// </summary>
        /// <param name="objTabControl">The tab conrol</param>
        /// <param name="intClientTop">Internal area top</param>
        public static void SetClientTop(this TabControl objTabControl, int intClientTop)
        {

            // Check for null reference
            if (objTabControl != null)
            {

                // Check alignment and check if there are some tab pages
                if (objTabControl.Alignment == TabAlignment.Top && objTabControl.TabCount > 0)
                {

                    // Create tab page object
                    TabPage objTabPage = objTabControl.TabPages[0];

                    // Create rectangle object
                    Rectangle objRectangle = objTabPage.Bounds;

                    // Get button height
                    int intHeight = objRectangle.Y;

                    //calculate client top
                    objTabControl.Top = intClientTop - intHeight;
                }

                else
                {
                    // If TabControl alignment is not top or tab pages count is 0 then client.top is equal tab control top - 2 because of bounds
                    objTabControl.Top = intClientTop - 2;
                }
            }
        }
        
        /// <summary>
        /// Returns the top coordinate of the internal area of the control
        /// </summary>
        /// <param name="objTabControl">The tab control</param>
        /// <returns>left from client internal area </returns>
        public static int GetClientLeft(this TabControl objTabControl)
        {

            // Check for null reference
            if (objTabControl != null)
            {

                // Check number of tabs
                if (objTabControl.TabCount > 0)
                {

                    // Check alignment
                    if (objTabControl.Alignment == TabAlignment.Left)
                    {

                        // Create tab page object
                        TabPage objTabPage = objTabControl.TabPages[0];

                        // Create rectangle object
                        Rectangle objRectangle = objTabPage.Bounds;

                        // Get button height
                        int intWidth = objRectangle.X;

                        // Get control location
                        Point objPoint = objTabControl.Location;

                        // Calculate client left
                        return intWidth + objPoint.X;

                    }

                    else
                    {

                        // If alignment is top,right or bottom return default tab control left
                        return objTabControl.Left + 2;

                    }
                }
            }

            // If object is null throw new exception
            throw new NullReferenceException();
        }

        /// <summary>
        /// Sets left position for tab control internal area
        /// </summary>
        /// <param name="objTabControl">The tab control</param>
        /// <param name="intClientLeft">Left position of internal area</param>
        public static void SetClientLeft(this TabControl objTabControl, int intClientLeft)
        {
            // Check for null reference 
            if (objTabControl != null)
            {

                // Check if there are some tab pages and check aligment 
                if (objTabControl.TabCount > 0 && objTabControl.Alignment == TabAlignment.Left)
                {

                    // Create tab page object
                    TabPage objTabPage = objTabControl.TabPages[0];

                    // Create rectangle object
                    Rectangle objRectangle = objTabPage.Bounds;

                    // Get button height
                    int intWidth = objRectangle.X;

                    // Set client left
                    objTabControl.Left = intClientLeft - intWidth;
                }

                else
                {
                    // If alignment is not left or there aren't tab pages then client left is equal to tab control left -2 because of bounds
                    objTabControl.Left = intClientLeft - 2;
                }

            }
        }

        /// <summary>
        /// Returns the height of the internal area of the control
        /// </summary>
        /// <param name="objTabControl">The obj tab control.</param>
        /// <returns></returns>
        /// <exception cref="System.NullReferenceException"></exception>
        public static int GetClientHeight(this TabControl objTabControl)
        {

            // Check for null reference 
            if (objTabControl != null)
            {

                if (objTabControl.TabCount > 0)
                {
                    // Create tab page object
                    TabPage objTabPage = objTabControl.TabPages[0];

                    // Create rectangle object
                    Rectangle objRectangle = objTabPage.Bounds;

                    // Get button height
                    return objRectangle.Height + 4;
                }
                else
                {
                    return objTabControl.Height;
                }
            }

            // If object is null throw exception
            throw new NullReferenceException();

        }

        /// <summary>
        /// Sets the height of the client.
        /// </summary>
        /// <param name="objTabControl">The obj tab control.</param>
        /// <param name="intHeight">Height of the int.</param>
        public static void SetClientHeight(this TabControl objTabControl, int intHeight)
        {
            // Check for null refernece
            if (objTabControl != null)
            {

                // Check aligment and tab count 
                if (objTabControl.TabCount > 0 && objTabControl.Alignment == TabAlignment.Top)
                {
                    // Create tab page object
                    TabPage objTabPage = objTabControl.TabPages[0];

                    // Create rectangle object
                    Rectangle objRectangle = objTabPage.Bounds;

                    // Get button height
                    objTabControl.Height = intHeight + objRectangle.Y;
                }

                else if (objTabControl.TabCount > 0 && objTabControl.Alignment == TabAlignment.Bottom)
                {
                    // Create tab page object
                    TabPage objTabPage = objTabControl.TabPages[0];

                    // Get tab control height
                    int intTabControlHeight = objTabControl.Height;

                    // Get tab page height
                    int intTabPageHeight = objTabPage.Height;

                    // Get button height
                    int intTabButtonHeight = intTabControlHeight - intTabPageHeight - 4;

                    // Set tab control height
                    objTabControl.Height = intHeight + intTabButtonHeight;
                }

                else
                {
                    // If alignment is not top and tab count is 0 then client height is equal to tab control height 
                    objTabControl.Height = intHeight + 4;
                }
            }
        }

        /// <summary>
        ///  Returns the height of the internal area of the control
        /// </summary>
        /// <param name="objTabControl"></param>
        /// <returns></returns>
        public static int GetClientWidth(this TabControl objTabControl)
        {

            // Check for null reference 
            if (objTabControl != null)
            {

                // Create tab page object
                TabPage objTabPage = objTabControl.TabPages[0];

                // Create rectangle object
                Rectangle objRectangle = objTabPage.Bounds;

                if (objTabControl.Alignment == TabAlignment.Right || objTabControl.Alignment == TabAlignment.Left)
                {
                    // Get button height
                    return objRectangle.Width;
                }
                else
                {
                    return objRectangle.Width + 4;
                }
            }

            // If object is null throw exception
            throw new NullReferenceException();

        }


        /// <summary>
        /// Sets the width of the client.
        /// </summary>
        /// <param name="objTabControl">The obj tab control.</param>
        /// <param name="intClientWidth">Width of the int client.</param>
        public static void SetClientWidth(this TabControl objTabControl, int intClientWidth)
        {

            // Check for null reference
            if (objTabControl != null)
            {

                // Check tab count and alignment
                if (objTabControl.TabCount > 0 && objTabControl.Alignment == TabAlignment.Left || objTabControl.Alignment == TabAlignment.Right)
                {

                    // Create tab page object
                    TabPage objTabPage = objTabControl.TabPages[0];

                    // Get tab page width
                    int intTabPageWidth = objTabPage.Width;

                    // Get tab control width
                    int intTabControlWidth = objTabControl.Width;

                    // Calculate button width
                    int intTabButtonsWidth = intTabControlWidth - intTabPageWidth;

                    // Set tab control width
                    objTabControl.Width = intClientWidth + intTabButtonsWidth;

                }

                else
                {
                    // If alignment is top or bottom or there aren't tab pages client width is equal to tab control width + 4 because of bounds
                    objTabControl.Width = intClientWidth + 4;
                }
            }
        }

        /// <summary>
        /// Sets tab key
        /// </summary>
        /// <param name="objTabControl"></param>
        /// <param name="intTabIndex"></param>
        /// <param name="strKey"></param>
        public static void SetTabKey(this TabControl objTabControl, int intTabIndex, string strKey)
        {

            // Check for null reference 
            if (objTabControl != null)
            {
                // Check for valid index
                if (intTabIndex >= 0 && intTabIndex < objTabControl.TabCount)
                {
                    // Create tab object with that index
                    TabPage objTabPage = objTabControl.TabPages[intTabIndex];

                    // Set key to tab with that index
                    objTabPage.Name = strKey;
                }

                else
                {
                    // If index is out of range throw exception
                    throw new IndexOutOfRangeException();
                }
            }

            else
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Gets tab page key
        /// </summary>
        /// <param name="objTabControl">Tab Control</param>
        /// <param name="intTabIndex">tab page index</param>
        /// <returns>tab page key</returns>
        public static string GetTabKey(this TabControl objTabControl, int intTabIndex)
        {
            // Check for null reference
            if (objTabControl == null)
            {
                // If object is null throw 
                throw new NullReferenceException();
            }
            else
            {

                // Check for valid index
                if (intTabIndex >= 0 && intTabIndex < objTabControl.TabCount)
                {
                    // Create tab page object
                    TabPage objTabPage = objTabControl.TabPages[intTabIndex];

                    // Return tab page key
                    return objTabPage.Name;
                }
                else
                {
                    // If index is not valid throw exception
                    throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
