﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Wisej.Web;


namespace fecherFoundation.Extensions
{
    /// <summary>
    /// TreeNodeExtension class
    /// </summary>
    public static class TreeNodeExtension
    {
        public static readonly object objSortedNode = new object();

        /// <summary>
        /// Set the expanded state of TreeNode
        /// </summary>
        /// <param name="objTreeNode"></param>
        /// <param name="boolValue"></param>
        public static void Expanded(this TreeNode objTreeNode, bool boolValue)
        {
            //check the value we recieve
            if (boolValue)
            {
                //if the value is true, expand the treenode
                objTreeNode.Expand();
            }
            else
            {
                //if the value is false, colapse the treenode
                objTreeNode.Collapse();
            }
        }

        /// <summary>
        /// Get the expanded state of TreeNode
        /// </summary>
        /// <param name="objTreeNode"></param>
        /// <returns></returns>
        public static bool Expanded(this TreeNode objTreeNode)
        {
            //return the expanded state of the node
            return objTreeNode.IsExpanded;
        }

        /// <summary>
        /// Sorts the nodes of a node
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="blnValue">bool value</param>
        public static void SetSorted(this TreeNode objTreeNode, bool blnValue)
        {
            // Null reference checking
            if (objTreeNode != null)
            {
                // Get TreeView control 
                TreeView objTreeView = objTreeNode.TreeView;

                // Get current node index
                int intIndex = objTreeNode.Index;

                // If true
                if (blnValue)
                {

                    // Begin update on tree view
                    objTreeView.BeginUpdate();

                    // Get the nodes of the node
                    TreeNodeCollection objNodeCollection = objTreeNode.Nodes;

                    // Check if collection has items
                    if (objNodeCollection.Count > 0)
                    {

                        // Declare new list
                        List<TreeNode> objListNodes = new List<TreeNode>();

                        // Add all tree nodes to our list
                        foreach (TreeNode objNode in objNodeCollection)
                        {
                            objListNodes.Add(objNode);
                        }

                        // Sort it
                        var sortedList = objListNodes.OrderBy(t => t.Text).ToList();

                        // Clear the tree node collection
                        objTreeNode.Nodes.Clear();

                        // Add the sorted nodes to the nodes collection
                        TreeNode[] arrNodes = sortedList.ToArray();
                        objTreeNode.Nodes.AddRange(arrNodes);

                    }

                    // End update
                    objTreeNode.TreeView.EndUpdate();

                }

                // Set bool value for Sorted
                GenericExtender.SetValue(objTreeView, objSortedNode, intIndex, blnValue);
            }
            else
            {
                // Throw an exception
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Set the selected state of TreeNode
        /// </summary>
        /// <param name="objTreeNode"></param>
        /// <param name="boolValue"></param>
        public static void Selected(this TreeNode objTreeNode, bool boolValue)
        {
            //check the value we recieve
            if (boolValue)
            {
                //if the value is true, select the treenode
                objTreeNode.TreeView.SelectedNode = objTreeNode;
            }
            else
            {
                //if the value is false, set selected node to null
                objTreeNode.TreeView.SelectedNode = null;
            }
        }

        /// <summary>
        /// Sets the selected node.
        /// </summary>
        /// <param name="objTreeNode">The  tree node object.</param>
        /// <param name="intIndex">Index of the int.</param>
        /// <exception cref="System.NullReferenceException"></exception>
        public static void SetSelected(this TreeNode objTreeNode, bool blnValue)
        {
            // Check for null reference 
            if (objTreeNode != null)
            {
                // Get tree view control
                TreeView objTreeView = objTreeNode.TreeView;

                // Set selected node 
                objTreeView.SelectedNode = objTreeNode;

            }
            else
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Get the selected state of TreeNode
        /// </summary>
        /// <param name="objTreeNode"></param>
        /// <returns></returns>
        public static bool Selected(this TreeNode objTreeNode)
        {
            //return the selected state of the node
            return objTreeNode.IsSelected;
        }

        private static List<TreeNode> GetNodes(this TreeNode node)
        {
            List<TreeNode> nodes = new List<TreeNode>();
            foreach (TreeNode n in node.Nodes)
            {
                nodes.Add(n);
                if (n.Nodes.Count > 0)
                {
                    nodes.AddRange(GetNodes(n));
                }
            }
            return nodes;
        }

        public static List<TreeNode> GetAllNodes(this TreeView tree)
        {
            List<TreeNode> nodes = new List<TreeNode>();
            foreach (TreeNode n in tree.Nodes)
            {
                nodes.Add(n);
                if (n.Nodes.Count > 0)
                {
                    nodes.AddRange(GetNodes(n));
                }
            }

            return nodes;
        }
        /// <summary>
        /// Sets the node font bold.
        /// </summary>
        /// <param name="objNode">The node object.</param>
        /// <param name="blnValue">if set to <c>true</c> [BLN value].</param>
        /// <exception cref="System.NullReferenceException"></exception>
        public static void SetNodeFontBold(this TreeNode objNode, bool blnValue)
        {
            // Check for null reference
            if (objNode != null)
            {

                // Get node font
                Font objFont = objNode.NodeFont;

                // If the objFont is null then node font is equal to TreeView control font
                if (objFont == null)
                {
                    // Get TreeView control that this node is attached to.
                    TreeView objTreeView = objNode.TreeView;

                    // Get TreeView control font
                    objFont = objTreeView.Font;
                }

                // This will be false if both values are false
                if (objFont.Bold | blnValue)
                {
                    // If blnValue is true set bold otherwise set bold to false
                    FontStyle objFontStyle = (blnValue) ? objFont.Style | FontStyle.Bold : objFont.Style ^ FontStyle.Bold;

                    //BAN - if we set the font to bold, the text will not be displayed entirely. We need to reset the text after setting the font

                    string objNodeText = objNode.Text;

                    // Set new font
                    Font font = new Font(objFont, objFontStyle);
                    FCUtils.AddFont(font);
                    objNode.NodeFont = font;

                    objNode.Text = objNodeText;
                }
            }

            else
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
        }
    }
}
