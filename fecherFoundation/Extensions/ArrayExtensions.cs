﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation.Extensions
{
    public static class ArrayExtensions
    {

        /// <summary>
        /// Helping extension method for VB Roslyn code fix VB0004 & VB0004d.
        /// Deep copies an array by calling Clone for each element.
        /// <seealso cref="https://subversion.fecher.eu/svn/tools/trunk/porting/fecherFoundation.Analyzer"/>
        /// </summary>
        /// <typeparam name="T">type of elements (must implement ICloneable)</typeparam>
        /// <param name="source">souce array</param>
        /// <returns>deep copy of source array</returns>
        public static T[] DeepCopy<T>(this T[] source) where T : ICloneable
        {
            if (source == null) return null;

            T[] copy = new T[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                copy[i] = (T)source[i].Clone();
            }
            return copy;
        }

        /// <summary>
        /// Reads a 2-dimensional array from a BinaryReader
        /// </summary>
        /// <param name="array"></param>
        /// <param name="reader"></param>
        /// <param name="dim1"></param>
        /// <param name="dim2"></param>
        public static bool Read<T>(this T[,] array, System.IO.BinaryReader reader, int iMin = 0, int jMin = 0)
        {
            try
            {
                string givenType = typeof(T).ToString().ToLower();
                int dim1 = array.GetLength(0);
                int dim2 = array.GetLength(1);
                for (int j = jMin; j < dim2; j++)
                {
                    for (int i = iMin; i < dim1; i++)
                    {
                        switch (givenType)
                        {
                            case "system.int32":
                            {
                                array[i, j] = (T) Convert.ChangeType(reader.ReadInt32(), typeof(T));
                                break;
                            }
                            case "system.datetime":
                            {
                                array[i, j] = (T) Convert.ChangeType((new DateTime(reader.ReadInt64())), typeof(T));
                                break;
                            }
                            default:
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool Write(this int[,] array, System.IO.BinaryWriter writer, int iMin = 0, int jMin = 0)
        {
            try
            {
                int dim1 = array.GetLength(0);
                int dim2 = array.GetLength(1);
                for (int j = jMin; j < dim2; j++)
                {
                    for (int i = iMin; i < dim1; i++)
                    {
                        writer.Write(array[i, j]);
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool Write(this DateTime[,] array, System.IO.BinaryWriter writer, int iMin = 0, int jMin = 0)
        {
            try
            {
                int dim1 = array.GetLength(0);
                int dim2 = array.GetLength(1);
                for (int j = jMin; j < dim2; j++)
                {
                    for (int i = iMin; i < dim1; i++)
                    {
                        writer.Write(array[i, j].Ticks);
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Resize 2D-Array and keep content
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original"></param>
        /// <param name="newFirstDim"></param>
        /// <param name="newSecondDim"></param>
        public static void ResizeArray<T>(ref T[,] original, int newFirstDim, int newSecondDim)
        {
            var newArray = new T[newFirstDim, newSecondDim];
            int origFirstDimCount = original != null ? original.GetUpperBound(0) : -1;
            int origSecondDomCount = original != null ? original.GetLength(1) : 0;
            for (int dim = 0; dim <= origFirstDimCount; dim++) {
                Array.Copy(original, dim * origSecondDomCount, newArray, dim * newSecondDim, origSecondDomCount);
            }
            original = newArray;
        }
    }
}
