﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wisej.Web;

namespace fecherFoundation.Extensions
{
    /// <summary>
    /// TreeNodeCollectionExtensions class
    /// </summary>
    public static class TreeNodeCollectionExtensions
    {
		#region Methods (16) 

		// Public Methods (12) 

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNodeCollection object</param>
        /// <param name="strRelative">Relative string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <returns>Created TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, string strRelative , TreeRelationshipConstants enumRelationship, string strKey, string strText)
        {
            // Find the node in the collection based on relative string
            TreeNode objNode = objTreeNode.FindNode(strRelative);

            // Call AddNode overload
            return objTreeNode.AddNode(enumRelationship, objNode, strKey, strText);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="objRelative">Relative TreeNode</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <returns></returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, TreeRelationshipConstants enumRelationship, TreeNode objRelative, string strKey, string strText = "")
        {
            // The variable that will hold the returning result
            TreeNode objNode = null;

            // Check if there is a relative object specified
            if (objRelative == null)
            {
                // If no relative found, add the TreeNode at the end of the TreeView
                objNode = objTreeNode.Add(strKey, strText);
            }

            else
            {
                // Check for the relationship
                switch (enumRelationship)
                {
                    // Case child
                    case TreeRelationshipConstants.tvwChild:

                        // Cache nodes collection on relative node
                        TreeNodeCollection objRelativeNodes = objRelative.Nodes;

                        // Add the Node to the Nodes collection of the specified relative Node
                        objNode = objRelativeNodes.Add(strKey, strText);

                        break;

                    // Case first
                    case TreeRelationshipConstants.tvwFirst:

                        // Check if the relative Node has parent
                        if (objRelative.Parent == null)
                        {
                            // If it doesn't have parent then add it at the beginning of the tree
                            objTreeNode.Insert(0, new TreeNode(strText));

                            // Assign the newly created TreeNode to the returning variable
                            objNode = objTreeNode[0];
                        }
                        else
                        {
                            // Cache relative node parent
                            TreeNode objRelativeParent = objRelative.Parent;

                            // Cache relative node parent nodes collection
                            TreeNodeCollection objRelativeParentNodes = objRelativeParent.Nodes;

                            // If the relative node has parent, add the TreeNode to the beginning of its parents Nodes collection
                            objRelativeParentNodes.Insert(0, new TreeNode(strText));

                            // Assign the newly created TreeNode to the returning variable
                            objNode = objRelativeParent.Nodes[0];
                        }

                        //Assign the Name property with the value passed from the Key parameter
                        objNode.Name = strKey;

                        break;
                    // Case Last
                    case TreeRelationshipConstants.tvwLast:

                        // Check if the relative Node has parent
                        if (objRelative.Parent == null)
                        {
                            // If it doesn't have a parent add it at the end of the TreeView
                            objNode = objTreeNode.Add(strKey, strText);
                        }

                        else
                        {
                            // Cache relative node parent
                            TreeNode objRelativeParent = objRelative.Parent;

                            // Cache relative node parent nodes collection
                            TreeNodeCollection objRelativeParentNodes = objRelativeParent.Nodes;

                            // If it has parent add it at the end of the child nodes collection
                            objNode = objRelativeParentNodes.Add(strKey, strText);
                        }

                        break;
                    // Case Next
                    case TreeRelationshipConstants.tvwNext:

                        // Check if the relative node has parent
                        if (objRelative.Parent == null)
                        {
                            // If it doesn't have, check if the selected node is the last
                            if (objRelative.Index + 1 == objTreeNode.Count)
                            {
                                // If it's the last add it at the end
                                objNode = objTreeNode.Add(strKey, strText);
                            }

                            else
                            {
                                // If the selected node is not the last add it on the next index
                                objTreeNode.Insert(objRelative.Index + 1, new TreeNode(strText));

                                // Assign the newly created TreeNode to the return variable
                                objNode = objTreeNode[objRelative.Index + 1];

                                // Assign the Name property with the value passed from the Key parameter
                                objNode.Name = strKey;
                            }
                        }

                        else
                        {
                            // If it has parent check if it is the last node in the collection
                            if (objRelative.Index + 1 == objTreeNode.Count)
                            {
                                // Cache relative node parent
                                TreeNode objRelativeParent = objRelative.Parent;

                                // Cache relative node parent nodes collection
                                TreeNodeCollection objRelativeParentNodes = objRelativeParent.Nodes;

                                // If it's the last add the new node at the end
                                objNode = objRelativeParentNodes.Add(strKey, strText);
                            }
                            else
                            {
                                // Cache relative node parent
                                TreeNode objRelativeParent = objRelative.Parent;

                                // Cache relative node parent nodes collection
                                TreeNodeCollection objRelativeParentNodes = objRelativeParent.Nodes;

                                // If it's not the last, insert it after the selected index
                                objRelativeParentNodes.Insert(objRelative.Index + 1, new TreeNode(strText));

                                // Assign the newly created TreeNode to the return variable
                                objNode = objRelativeParent.Nodes[objRelative.Index + 1];

                                // Assign the Name property with the value passed from the Key parameter
                                objNode.Name = strKey;
                            }
                        }
                        break;
                    // Case Previous
                    case TreeRelationshipConstants.tvwPrevious:

                        // Check if the relative object has parent
                        if (objRelative.Parent == null)
                        {
                            // If it doesn't have a parent add new node at the root of the tree with same index as the selected node
                            objTreeNode.Insert(objRelative.Index, new TreeNode(strText));

                            // Assign the newly created TreeNode to the return variable
                            objNode = objTreeNode[objRelative.Index];
                        }
                        else
                        {
                            // Cache relative node parent
                            TreeNode objRelativeParent = objRelative.Parent;

                            // Cache relative node parent nodes collection
                            TreeNodeCollection objRelativeParentNodes = objRelativeParent.Nodes;

                            // If it has a parent add the node to its parents node collection
                            objRelativeParentNodes.Insert(objRelative.Index, new TreeNode(strText));

                            // Assign the newly created TreeNode to the return variable
                            objNode = objRelativeParent.Nodes[objRelative.Index];
                        }

                        // Assign the Name property with the value passed from the Key parameter
                        objNode.Name = strKey;
                        break;

                    default:
                        break;
                }
            }
             
            // Return the new node
            return objNode;
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="strRelative">Relative Node</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey)
        {
            // Find the node in the collection based on relative string
            TreeNode objNode = objTreeNode.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeNode.AddNode(objNode, enumRelationship, strKey, strText, strImageKey, strImageKey);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="strRelative">Relative TreeNode string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="intImageIndex">ImageIndex value</param>
        /// <param name="intSelectedImageIndex">Selected ImageIndex value</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex)
        {
            // Find the node in the collection based on relative string
            TreeNode objNode = objTreeNode.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeNode.AddNode(objNode, enumRelationship, strKey, strText, intImageIndex, intSelectedImageIndex);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="strRelative">Relative node string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <param name="intSelectedImageIndex">Selected ImageIndex value</param>
        /// <returns></returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, int intSelectedImageIndex)
        {
            // Find the node in the collection based on relative string
            TreeNode objNode = objTreeNode.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeNode.AddNode(objNode, enumRelationship, strKey, strText, strImageKey, intSelectedImageIndex);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="strRelative">Relative node string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="intImageIndex">ImageIndex value</param>
        /// <param name="strSelectedImageKey">Selected ImageKey value</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, string strSelectedImageKey)
        {
            // Find the node in the collection based on relative string
            TreeNode objNode = objTreeNode.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeNode.AddNode(objNode, enumRelationship, strKey, strText, intImageIndex, strSelectedImageKey);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="strRelative">Relative node string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <param name="strSelectedImageKey">Selected ImageKey value</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            // Find the node in the collection based on relative string
            TreeNode objNode = objTreeNode.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeNode.AddNode(objNode, enumRelationship, strKey, strText, strImageKey, strSelectedImageKey);
        }

        /// <summary>
        /// Add tree node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="objRelative">Relative object</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <param name="strSelectedImageKey">Selected ImageKey value</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            // Add the node without the image details
            TreeNode objNode = objTreeNode.AddNode(enumRelationship, objRelative, strKey, strText);

            // Check if we assign ImageKey
            if (strImageKey != null)
            {
                objNode.ImageKey = strImageKey;
            }

            // Check if we assign SelectedImageKey
            if (strSelectedImageKey != null)
            {
                objNode.SelectedImageKey = strSelectedImageKey;
            }

            // Return the node
            return objNode;
        }

        /// <summary>
        /// Finds a node in the collection
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="strNodeName">Node name</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode FindNode(this TreeNodeCollection objTreeNode, string strNodeName)
        {
            //Search the node collection and take the first node or default
            TreeNode objFindNode = objTreeNode.Find(strNodeName, true).FirstOrDefault();

            //Return tree node object
            return objFindNode;
        }

        /// <summary>
        /// Gets the number of all nodes.
        /// </summary>
        /// <param name="objTreeNodes">The object tree nodes.</param>
        /// <returns></returns>
        public static int GetNumberOfAllNodes(this TreeNodeCollection objTreeNodes)
        {
            // Declare variable sum where we will store our calculations
            int intSum = 0;

            // Loop through all nodes in tree view
            foreach (TreeNode node in objTreeNodes)
            {
                // Sum is equal to number of all sub nodes in the node plus one for the root node
                intSum += GetNodes(node) + 1;
            }

            // Return the number of all nodes
            return intSum;
        }

        /// <summary>
        /// Remove node from collection
        /// </summary>
        /// <param name="objNodeCollection">TreeNode object</param>
        /// <param name="strNodeName">NodeName value</param>
        /// <returns>bool</returns>
        public static void Remove(this TreeNodeCollection objNodeCollection, string strNodeName)
        {
            // Check for null reference
            if (objNodeCollection != null && objNodeCollection.Count > 0)
            {
                // Find the node to remove by name
                TreeNode objNodeToRemove = objNodeCollection.FindNode(strNodeName);

                // Null reference checking
                if (objNodeToRemove != null)
                {
                    // Remove it
                    objNodeToRemove.Remove();
                }
            }
            else
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Removes the node.
        /// </summary>
        /// <param name="objNodeCollection">The object node collection.</param>
        /// <param name="intIndex">Index of the int.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.NullReferenceException"></exception>
        public static void Remove(this TreeNodeCollection objNodeCollection, int intIndex)
        {
            // Check for null reference
            if (objNodeCollection != null)
            {
                // Get the number of all nodes
                int intNumberOfAllNodes = objNodeCollection.GetNumberOfAllNodes();

                // Check if index is valid
                if (intIndex < intNumberOfAllNodes  && intIndex >= 0)
                {
                    //Find the node to remove with our extension method
                    TreeNode objNodeToRemove = objNodeCollection[intIndex];

                    // Null reference checking
                    if (objNodeToRemove != null)
                    {
                        // Remove it
                        objNodeToRemove.Remove();
                    }
                }
                else
                {
                    // If index is out of range throw exception
                    throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
        }
		// Private Methods (4) 

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeNode">TreeNode collection object</param>
        /// <param name="objRelative">Relative object</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="intImageIndex">ImageIndex value</param>
        /// <param name="intSelectedImageIndex">Selected ImageIndex value</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex)
        {
             // Add the node without the image details
            TreeNode objNode = objTreeNode.AddNode(enumRelationship, objRelative, strKey, strText);

            // Assign ImageIndex
            if (intImageIndex != -1)
            {
                objNode.ImageIndex = intImageIndex;
            }

            // Assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }
            else
            {
                objNode.SelectedImageIndex = intImageIndex;
            }

            // Return the node
            return objNode;
        }

        /// <summary>
        /// Add TreeNode to the TreeView
        /// </summary>
        /// <param name="objTreeNode">ThreeNode object</param>
        /// <param name="objRelative">Relative TreeNode object</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <param name="intSelectedImageIndex">Selected ImageIndex value</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, int intSelectedImageIndex)
        {
            // Add the node without the image details
            TreeNode objNode = objTreeNode.AddNode(enumRelationship, objRelative, strKey, strText);

            // Assign ImageKey
            if (strImageKey != null)
            {
                objNode.ImageKey = strImageKey;
            }

            // Assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }
            else
            {
                objNode.SelectedImageKey = strImageKey;
            }

            // Return the node
            return objNode;
        }

        /// <summary>
        /// Add TreeNode object to the TreeView
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="objRelative">Relative TreeNode object</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="intImageIndex">ImageIndex value</param>
        /// <param name="strSelectedImageKey">ImageKey value</param>
        /// <returns>TreeNode object</returns>
        public static TreeNode AddNode(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, string strSelectedImageKey)
        {
            // Add the node without the image details
            TreeNode objNode = objTreeNode.AddNode(enumRelationship, objRelative, strKey, strText);

            // Assign ImageIndex
            if (intImageIndex != -1)
            {
                objNode.ImageIndex = intImageIndex;
            }

            // Assign Selected ImageKey
            if (strSelectedImageKey != null)
            {
                objNode.SelectedImageKey = strSelectedImageKey;
            }

            // Return the node
            return objNode;
        }

        /// <summary>
        /// Gets the number of sub nodes.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns></returns>
        public static int GetNodes(TreeNode node)
        {
            // Declare a variable where we will store number of sub nodes
            int intSubNodesSum = 0;

            // Calculate number of sub nodes
            intSubNodesSum += node.GetNodeCount(true);

            // Return number of sub nodes
            return intSubNodesSum;
        }

        /// <summary>
        /// Gets TreeNode from collection by index
        /// </summary>
        /// <param name="objTreeNodeCollection">TreeNode collection</param>
        /// <param name="intIndex">Index value</param>
        /// <returns>Tree node object</returns>
        public static TreeNode Item(this TreeNodeCollection objTreeNodeCollection, int intIndex)
        {
            // Null reference checking and check if there are nodes in the collection
            if (objTreeNodeCollection != null && objTreeNodeCollection.Count > 0)
            {
                // List to keeps all tree nodes
                List<TreeNode> objNodesCollection = new List<TreeNode>();

                // Make collection empty
                objNodesCollection.Clear();

                // Loops through the root nodes
                foreach (TreeNode objChild in objTreeNodeCollection)
                {
                    // Call the method for recursion
                    TreeViewExtension.BuildChildNodesList(objChild, objNodesCollection);
                }

                // Check if index is into range
                if (intIndex >= 0 || intIndex < objNodesCollection.Count)
                {
                    // Return the TreeNode found
                    return objNodesCollection[intIndex];
                }
                else
                {
                    // Throw argument out of range
                    throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                //Throw null reference exception
                throw new NullReferenceException();
            }
        }

		#endregion Methods 
    }
}
