﻿using System;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif

namespace fecherFoundation.Extensions
{
    public static class TabPageExtension
    {
        /// <summary>
        /// Determines whether [is tab selected] [the specified tab control object].
        /// </summary>
        /// <param name="objTabControl">The tab control object.</param>
        /// <param name="intTabIndex">Index of the int tab.</param>
        /// <returns>
        ///   <c>true</c> if [is tab selected] [the specified tab control object]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.NullReferenceException"></exception>
        public static bool GetSelected(this TabPage objTabPage)
        {

            // Cast to tab control
            TabControl objTabControl = objTabPage.Parent as TabControl;

            // Check for null reference
            if (objTabControl != null)
            {
                // If objTabPage is selected tab return true otherwise return false 
                if (objTabControl.SelectedTab.Equals(objTabPage))
                {
                    return true;
                }
                return false;
            }
            // If object is null throw exception
            throw new NullReferenceException();
        }

        /// <summary>
        /// Sets selected tab.
        /// </summary>
        /// <param name="objTabPage">The tab page object.</param>
        /// <exception cref="System.NullReferenceException"></exception>
        public static void SetSelected(this TabPage objTabPage)
        {
            // Cast to tab control
            TabControl objTabControl = objTabPage.Parent as TabControl;

            // Check for null reference
            if (objTabControl != null)
            {
                // Set objTabPage to be the selected tab in the tab control
                objTabControl.SelectedTab = objTabPage;
            }
            else
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Returns the key for the specified tab page object.
        /// </summary>
        /// <param name="objTabPage">The tab page object.</param>
        /// <returns></returns>
        public static string GetKey(this TabPage objTabPage)
        {
            // Check for null reference
            if (objTabPage != null)
            {
                // Return tab page key
                return objTabPage.Name;
            }
            // If object is null 
            throw new NullReferenceException();
        }

        /// <summary>
        /// Sets the key.
        /// </summary>
        /// <param name="objTabPage">The tab page object.</param>
        /// <param name="strKey">The key.</param>
        /// <exception cref="System.NullReferenceException"></exception>
        public static void SetKey(this TabPage objTabPage, string strKey)
        {
            // Check for null reference 
            if (objTabPage != null)
            {
                // Set tab page key
                objTabPage.Name = strKey;
            }
            else
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <param name="objTabPage">The tab page object.</param>
        /// <returns></returns>
        /// <exception cref="System.NullReferenceException"></exception>
        public static int GetIndex(this TabPage objTabPage)
        {
            // Get the TabControl
            TabControl objTabControl = objTabPage.Parent as TabControl;

            // Check for null reference
            if (objTabControl != null)
            {
                // Get collection of tab pages
                TabControl.TabPageCollection objTabCollection = objTabControl.TabPages;

                // Return tab page index 
                return objTabCollection.IndexOf(objTabPage);
            }

            // If object is null throw exception
            throw new NullReferenceException();
        }
    }
}
