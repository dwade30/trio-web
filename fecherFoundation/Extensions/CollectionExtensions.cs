﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.Extensions
{
    public static class CollectionExtensions
    {
        #region Methods

        public static object Item(this Microsoft.VisualBasic.Collection collection, object index)
        {
            return collection[index];
        }

        #endregion
    }
}
