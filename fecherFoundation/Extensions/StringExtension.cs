﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace fecherFoundation.Extensions
{
    public static class StringExtension
    {
        public static Double ToDoubleSafe(string value)
        {
            double num;
            return (double.TryParse(value, NumberStyles.Any, (IFormatProvider)null, out num) ? num : double.NaN);
        }

        public static string ByteArrayToString(byte[] array)
        {
            if (array != null)
            {
                byte[] buffer;
                if ((array.Length % 2) == 0)
                {
                    buffer = array;
                }
                else
                {
                    buffer = new byte[array.Length + 1];
                    Array.Copy(array, buffer, array.Length);
                }
                return Encoding.Unicode.GetString(buffer);
            }
            return string.Empty;
        }

        public static bool Like(string value, string pattern)
        {
            Trace.WriteLine("WARNING: Using VB6 Like operator pattern this affects performace. Pattern used " + pattern);
            pattern = pattern.Replace("*", ".*");
            pattern = pattern.Replace('?', '.');
            return Regex.Match(value, pattern).Success;
        }

        public static string MidAssignment(string str, int start, string val)
        {
            return MidAssignment(str, start, 2147483647, val);
        }

        public static string MidAssignment(string str, int start, int length, string val)
        {
            int num = Math.Min(length, Math.Min(val.Length, str.Length - (start - 1)));
            return (str.Substring(0, start - 1) + val.Substring(0, num) + str.Substring((start - 1) + num));
        }
 

    }
}
