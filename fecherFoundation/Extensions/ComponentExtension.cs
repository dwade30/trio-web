﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;

namespace fecherFoundation.Extensions
{
    public static class ComponentExtension
    {
        internal static Dictionary<Component, ComponentDefinitions> mobjDefinitions = new Dictionary<Component, ComponentDefinitions>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="component"></param>
        /// <param name="list"></param>
        public static void SetControlArray(this Component component, IList list)
        {
            ComponentDefinitions compDef = ComponentDefinitions.GetComponentDefinitions(component);
            compDef.ControlArray = list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public static IList GetControlArray(this Component component)
        {
            ComponentDefinitions compDef = ComponentDefinitions.GetComponentDefinitions(component);
            return compDef.ControlArray;
        }

        public static bool AutoRedraw(this Component objComponent, bool blnValue)
        {
            return true;
        }
        
        /// <summary>
        /// Gets the index of the control in control array.
        /// </summary>
        /// <param name="objComponent">The control.</param>
        /// <returns></returns>
        public static int GetIndex(this Component objComponent)
        {
            IList controlArray = ComponentExtension.GetControlArray(objComponent);
            if (controlArray != null)
            {
                return ComponentExtension.GetControlArray(objComponent).IndexOf(objComponent);
            }
            return -1;
        }
    }
    

    public class ComponentDefinitions
    {
        private IList controlArray; 

        /// <summary>
        /// Get the instance of the definition class
        /// </summary>
        /// <param name="objLabel"></param>
        /// <returns></returns>
        public static ComponentDefinitions GetComponentDefinitions(Component objComponent)
        {
            ComponentDefinitions objComponentDefinitions;

            if (ComponentExtension.mobjDefinitions.ContainsKey(objComponent))
            {
                objComponentDefinitions = ComponentExtension.mobjDefinitions[objComponent];
            }
            else
            {
                objComponentDefinitions = new ControlDefinitions();
                objComponent.Disposed += objComponent_Disposed;
                ComponentExtension.mobjDefinitions.Add(objComponent, objComponentDefinitions);
            }
            return objComponentDefinitions;
        }

        private static void objComponent_Disposed(object sender, EventArgs e)
        {
            Component objComponent = sender as Component;
            if (objComponent == null)
            {
                return;
            }
            //CHE: clean the event registration
            objComponent.Disposed -= objComponent_Disposed;
            if (ComponentExtension.mobjDefinitions.ContainsKey(objComponent))
            {
                //CHE: clear ControlArray
                ComponentExtension.mobjDefinitions[objComponent].ControlArray = null;
                ComponentExtension.mobjDefinitions.Remove(objComponent);
            }
        }

        public IList ControlArray
        {
            get { return controlArray; }
            set { controlArray = value; }
        }
    }
}
