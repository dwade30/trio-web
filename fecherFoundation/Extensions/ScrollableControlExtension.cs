﻿using System;
using System.Drawing;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif

namespace fecherFoundation.Extensions
{
    public static class ScrollableControlExtension
    {
        public static Size mobjAutoScrollMinSize;

        /// <summary>
        /// Extension : Gets the minimum size of the auto-scroll.
        /// </summary>
        /// <param name="objControl">The current control.</param>
        /// <returns>Property value.</returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static Size AutoScrollMinSize(this Control objControl)
        {
            return mobjAutoScrollMinSize;
        }

        /// <summary>
        /// Extension : Sets the minimum size of the auto-scroll.
        /// </summary>
        /// <param name="objControl">The current control.</param>
        /// <param name="sizValue">Property value.</param>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void AutoScrollMinSize(this Control objControl, Size objValue)
        {
            mobjAutoScrollMinSize = objValue;
        }
    }
}
