﻿#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif

namespace fecherFoundation.Extensions
{
    /// <summary>
    /// StatusBarPanelExtension class
    /// </summary>
    public static class StatusBarPanelExtension
    {
        /// <summary>
        /// Returns the Enabled value
        /// </summary>
        /// <param name="objStatusBarPanel"></param>
        /// <returns></returns>
        public static bool Enabled(this StatusBarPanel objStatusBarPanel)
        {
            //get the status bar panel definition
            StatusBarPanelDefinitions objStatusBarPanelDefinitions = StatusBarPanelDefinitions.GetStatusBarPanelDefinitions(objStatusBarPanel);
            
            //return the value
            return objStatusBarPanelDefinitions.Enabled;
        }

        /// <summary>
        /// Sets the Enabled value
        /// </summary>
        /// <param name="objStatusBarPanel"></param>
        /// <param name="boolValue"></param>
        public static void Enabled(this StatusBarPanel objStatusBarPanel, bool boolValue)
        {
            //get the status bar panel definition
            StatusBarPanelDefinitions objStatusBarPanelDefinitions = StatusBarPanelDefinitions.GetStatusBarPanelDefinitions(objStatusBarPanel);
            
            //set the value of the enabled property
            objStatusBarPanelDefinitions.Enabled = boolValue;
        }

    }

    public class StatusBarPanelDefinitions : ControlDefinitions
    {
        public static StatusBarPanelDefinitions GetStatusBarPanelDefinitions(StatusBarPanel objStatusBarPanel)
        {
            StatusBarPanelDefinitions objStatusBarPanelDefinitions;

            #if Winforms
            objStatusBarPanelDefinitions = new StatusBarPanelDefinitions();
            #else
            if (objStatusBarPanel.SystemTag == null)
            {
                objStatusBarPanelDefinitions = new StatusBarPanelDefinitions();

                objStatusBarPanel.SystemTag = objStatusBarPanelDefinitions;
            }
            else
            {
                objStatusBarPanelDefinitions = (StatusBarPanelDefinitions)objStatusBarPanel.SystemTag;
            }
            #endif
            return objStatusBarPanelDefinitions;
        }

        public bool mboolEnabled = true;

        public bool Enabled
        {
            get
            {
                return mboolEnabled;
            }
            set
            {
                mboolEnabled = value;
            }
        }
    }
}
