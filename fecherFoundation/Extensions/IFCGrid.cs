﻿namespace fecherFoundation.Extensions
{
    public interface IFCGrid
    {
        void SetDisableEvents(bool disableEvents);
    }
}
