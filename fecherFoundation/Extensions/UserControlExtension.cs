﻿using System.Collections.Generic;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Resources;
#endif

namespace fecherFoundation.Extensions
{
    public static class UserControlExtension
    {
        /// <summary>
        /// Gets the PropertyBag Directionary asosicated with the current UserControl
        /// </summary>
        /// <param name="objUserControl">The UserControl object.</param>
        /// <returns></returns>
        public static Dictionary<string, object> PropertyBag(this UserControl objUserControl)
        {
            //get the UserControlDefinition object
            UserControlDefinitions objUserControlDefinition = UserControlDefinitions.GetUserControlDefinitions(objUserControl);

            if (objUserControlDefinition.mobjPropertyBag == null)
                objUserControlDefinition.mobjPropertyBag = new Dictionary<string, object>();
            //return the set value for the indexer
            return objUserControlDefinition.mobjPropertyBag;
        }

    }

    public class UserControlDefinitions : ComponentDefinitions
    {
        /// <summary>
        /// Get the instance of the definition class
        /// </summary>
        /// <param name="objLabel"></param>
        /// <returns></returns>
        public static UserControlDefinitions GetUserControlDefinitions(UserControl objUserControl)
        {
            UserControlDefinitions objUserControlDefinitions;

#if Winforms
            objUserControlDefinitions = new UserControlDefinitions();
#else
            if (objUserControl.SystemTag == null)
            {
                objUserControlDefinitions = new UserControlDefinitions();

                objUserControl.SystemTag = objUserControlDefinitions;
            }
            else
            {
                objUserControlDefinitions = (UserControlDefinitions)objUserControl.SystemTag;
            }
#endif
            return objUserControlDefinitions;
        }

        public Dictionary<string, object> mobjPropertyBag = null;

    }
}
