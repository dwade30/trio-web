using System.Drawing;
using System.Windows.Forms;

namespace fecherFoundation.Extensions
{
    public static class Screen
    {
        /// <summary>
        /// Returns the form that is the active window.
        /// </summary>
        /// <value>
        /// The active form.
        /// </value>
        public static Form ActiveForm
        {
            get
            {
                // Traverse all application forms
                foreach (Form objForm in Application.OpenForms)
                {
                    // Check if form is active
                    if (objForm.ContainsFocus)
                    {
                        return objForm;
                    }
                }
                return null;
            }
        }
        /// <summary>
        /// Returns the control that has the focus.
        /// </summary>
        /// <value>
        /// The active control.
        /// </value>
        public static Control ActiveControl
        {
            get
            {
                // Get active form
                Form objForm = ActiveForm;

                if (objForm != null)
                {
                    // Get active control
                    return objForm.ActiveControl;
                }

                return null;
            }
        }
        /// <summary>
        /// Gets the screen height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public static int Height
        {
            get
            {
                return System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
            }
        }
        /// <summary>
        /// Gets the screen width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public static int Width
        {
            get
            {
                return System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            }
        }
        /// <summary>
        /// Sets the custom cursor for active form.
        /// </summary>
        /// <value>
        /// The mouse pointer.
        /// </value>
        public static Image MouseIcon
        {
            set
            {
                // Get active form
                Form objForm = ActiveForm;
                if (objForm == null)
                {
                    return;
                }

                // Set cursor
                objForm.SetMouseIcon(value);
            }
        }
        /// <summary>
        /// Gets or sets the cursor for active form.
        /// In VB6 the cursor is set on the entire application not on one particular form
        /// </summary>
        /// <value>
        /// The mouse pointer.
        /// </value>
        public static Cursor MousePointer
        {
            get
            {                
                // Retrieve cursor                
                return Cursor.Current;
            }
            set
            {
                // Set cursor
                Cursor.Current = value;
            }
        }
    }
}
