﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace fecherFoundation.Extensions
{
    public static class ColorExtensions
    {
        public static Color InvertColorFromRGB(this Color color)
        {
            Color newColor;
            newColor = Color.FromArgb((byte)~color.R, (byte)~color.G, (byte)~color.B);
            return newColor;
        }
    }
}
