﻿using System;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif

namespace fecherFoundation.Extensions
{
    /// <summary>
    /// ExceptionExtension Class
    /// </summary>
    public static class ExceptionExtension
    {
        public static int LineNumber(this Exception e)
        {
            int linenum = 0;
            try
            {
                //linenum = Convert.ToInt32(e.StackTrace.Substring(e.StackTrace.LastIndexOf(":line") + 5));

                //For Localized Visual Studio ... In other languages stack trace  doesn't end with ":Line 12"
                linenum = Convert.ToInt32(e.StackTrace.Substring(e.StackTrace.LastIndexOf(' ')));
            }
            catch
            {
                //Stack trace is not available!
            }
            return linenum;
        }

        public static string LineNumberString(this Exception e)
        {
            return " LineNumber: " + e.LineNumber().ToString();
        }

        /// <summary>
        /// Gets the number from exception.
        /// </summary>
        /// <param name="err">The err.</param>
        /// <returns></returns>
        //CHE: was implemented, remove Obsolete comment
        //[Obsolete("Not implemented. Added for migration compatibility")]
        public static int GetNumberFromException(Exception err)
        {
            //BAN - use Marshal in order to get the exception number
            //throw new NotImplementedException();
            return System.Runtime.InteropServices.Marshal.GetHRForException(err);
        }

        /// <summary>
        /// Gets the help context from exception.
        /// </summary>
        /// <param name="err">The err.</param>
        /// <returns></returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        public static int GetHelpContextFromException(Exception err)
        {
            //CHE - use Marshal in order to get the exception number
            //throw new NotImplementedException();
            return System.Runtime.InteropServices.Marshal.GetHRForException(err);
        }

        /// <summary>
        /// Gets the last DLL error from exception.
        /// </summary>
        /// <param name="err">The err.</param>
        /// <returns></returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        public static int GetLastDllErrorFromException(Exception err)
        {
            throw new NotImplementedException();
        }
    }
}
