﻿using System;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif

namespace fecherFoundation.Extensions
{
    public static class ColumnHeaderExtension
    {
        /// <summary>
        /// Sets the distance between the internal left edge of an object and the left edge of its container
        /// </summary>
        /// <param name="objColumnHeader">The ColumnHeader</param>
        /// <param name="intValue">the distance</param>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void Left(this ColumnHeader objColumnHeader, int intValue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the distance between the internal left edge of an object and the left edge of its container
        /// </summary>
        /// <param name="objColumnHeader">The ColumnHeader</param>
        /// <returns>the distance</returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static int Left(this ColumnHeader objColumnHeader)
        {
            throw new NotImplementedException();
        }
    }
}
