﻿using System.Collections.Generic;
using Wisej.Web;

namespace fecherFoundation.Extensions
{
    //#region Enums
    // moved to FCTreeView

    ///// <summary>
    ///// TreeLineStyleConstants enum
    ///// </summary>
    //public enum TreeLineStyleConstants
    //{
    //    tvwTreeLines = 0,
    //    tvwRootLines = 1
    //}

    //public enum TreeStyleConstants : int
    //{
    //    tvwPictureText = 1,
    //    tvwPlusMinusText = 2,
    //    tvwPlusPictureText = 3,
    //    tvwTextOnly = 0,
    //    tvwTreelinesPictureText = 5,
    //    tvwTreelinesPlusMinusPictureText = 7,
    //    tvwTreelinesPlusMinusText = 6,
    //    tvwTreelinesText = 4
    //}

    //public enum TreeRelationshipConstants
    //{
    //    tvwChild = 4,
    //    tvwFirst = 0,
    //    tvwLast = 1,
    //    tvwNext = 2,
    //    tvwPrevious = 3
    //}
    //#endregion

    /// <summary>
    /// TreeViewExtension class
    /// </summary>
    public static class TreeViewExtension
    {
        public static readonly object SingleSel = new object();

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="intImageIndex">Index of the int image.</param>
        /// <param name="intSelectedImageIndex">Index of the int selected image.</param>
        /// <returns></returns>
        public static TreeNode Add(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex = -1)
        {
            //the variable that will hold the returning result
            TreeNode objNode = null;

            //add the node without the image details
            objNode = objTreeNode.Add(objRelative, enumRelationship, strKey, strText);

            //assign ImageIndex
            if(intImageIndex != -1)
            objNode.ImageIndex = intImageIndex;

            //assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }
            else
            {
                objNode.SelectedImageIndex = intImageIndex;
            }

            //return the node
            return objNode;
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="strImageKey">The STR image key.</param>
        /// <param name="intSelectedImageIndex">Index of the int selected image.</param>
        /// <returns></returns>
        public static TreeNode Add(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, int intSelectedImageIndex)
        {
            //the variable that will hold the returning result
            TreeNode objNode = null;

            //add the node without the image details
            objNode = objTreeNode.Add(objRelative, enumRelationship, strKey, strText);

            //assign ImageIndex
            if (strImageKey != null)
                objNode.ImageKey = strImageKey;

            //assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }
            else
            {
                objNode.SelectedImageKey = strImageKey;
            }

            //return the node
            return objNode;
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="intImageIndex">Index of the int image.</param>
        /// <param name="strSelectedImageKey">The STR selected image key.</param>
        /// <returns></returns>
        public static TreeNode Add(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, string strSelectedImageKey)
        {
            //the variable that will hold the returning result
            TreeNode objNode = null;

            //add the node without the image details
            objNode = objTreeNode.Add(objRelative, enumRelationship, strKey, strText);

            //assign ImageIndex
            if (intImageIndex != -1)
                objNode.ImageIndex = intImageIndex;

            //assign SelectedImageIndex
            if (strSelectedImageKey != null)
                objNode.SelectedImageKey = strSelectedImageKey;

            //return the node
            return objNode;
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="strImageKey">The STR image key.</param>
        /// <param name="strSelectedImageKey">The STR selected image key.</param>
        /// <returns></returns>
        public static TreeNode Add(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            //the variable that will hold the returning result
            TreeNode objNode = null;

            //add the node without the image details
            objNode = objTreeNode.Add(objRelative, enumRelationship, strKey, strText);

            //check if we assign ImageKey
            if (strImageKey != null)
                objNode.ImageKey = strImageKey;

            //check if we assign SelectedImageKey
            if (strSelectedImageKey != null)
                objNode.SelectedImageKey = strSelectedImageKey;

            //return the node
            return objNode;
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <returns></returns>
        public static TreeNode Add(this TreeNodeCollection objTreeNode, TreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText)
        {
            //the variable that will hold the returning result
            TreeNode objNode = null;

            //check if there is a relative object specified
            if (objRelative == null)
            {
                //if no relative found, add the TreeNode at the end of the TreeView
                objNode = objTreeNode.Add(strKey, strText);
            }
            else
            {
                //check for the relationship
                switch (enumRelationship)
                {
                    //case child
                    case TreeRelationshipConstants.tvwChild:

                        //add the Node to the Nodes collection of the specified relative Node
                        objNode = objRelative.Nodes.Add(strKey, strText);
                        break;

                    //case first
                    case TreeRelationshipConstants.tvwFirst:

                        //check if the relative Node has parent
                        if (objRelative.Parent == null)
                        {
                            //if it doesn't have parent then add it at the begining of the tree
                            objTreeNode.Insert(0, new TreeNode(strText));
                            //assign the newly created TreeNode to the returning variable
                            objNode = objTreeNode[0];
                        }
                        else
                        {
                            //if the relative node has parent, add the TreeNode to the begining of its parents Nodes collection
                            objRelative.Parent.Nodes.Insert(0, new TreeNode(strText));

                            //assign the newly created TreeNode to the returning variable
                            objNode = objRelative.Parent.Nodes[0];
                        }
                        //assign the Name property with the value passed from the Key paramether
                        objNode.Name = strKey;
                        break;

                    //case Last
                    case TreeRelationshipConstants.tvwLast:

                        //check if the relative Node has parent
                        if (objRelative.Parent == null)
                        {
                            //if it doesn't have a parent add it at the end of the TreeView
                            objNode = objTreeNode.Add(strKey, strText);
                        }
                        else
                        {
                            //it it has parent add it at the end of the child nodes collection
                            objNode = objRelative.Parent.Nodes.Add(strKey, strText);
                        }
                        break;

                    //case Next
                    case TreeRelationshipConstants.tvwNext:

                        //check if the relative node has parent
                        if (objRelative.Parent == null)
                        {
                            //if it doesn't have, check if the selected node is the last
                            if (objRelative.Index + 1 == objTreeNode.Count)
                            {
                                //if it's the last add it at the end
                                objNode = objTreeNode.Add(strKey, strText);
                            }
                            else
                            {
                                //if the selected node is not the last add it on the next index
                                objTreeNode.Insert(objRelative.Index + 1, new TreeNode(strText));

                                //asign the newly created TreeNode to the return variable
                                objNode = objTreeNode[objRelative.Index + 1];

                                //assign the Name property with the value passed from the Key paramether
                                objNode.Name = strKey;
                            }
                        }
                        else
                        {
                            //if it has parent check if it is the last node in the collection
                            if (objRelative.Index + 1 == objTreeNode.Count)
                            {
                                //if it's the last add the new node at the end
                                objNode = objRelative.Parent.Nodes.Add(strKey, strText);
                            }
                            else
                            {
                                //if it's not the last, insert it after the selected index
                                objRelative.Parent.Nodes.Insert(objRelative.Index + 1, new TreeNode(strText));

                                //asign the newly created TreeNode to the return variable
                                objNode = objRelative.Parent.Nodes[objRelative.Index + 1];

                                //assign the Name property with the value passed from the Key paramether
                                objNode.Name = strKey;
                            }
                        }

                        break;

                    //case Previous
                    case TreeRelationshipConstants.tvwPrevious:

                        //check if the realtive object has parent
                        if (objRelative.Parent == null)
                        {
                            //if it doesn't have a parent add new node at the root of the tree with same index as the selected node
                            objTreeNode.Insert(objRelative.Index, new TreeNode(strText));

                            //asign the newly created TreeNode to the return variable
                            objNode = objTreeNode[objRelative.Index];
                        }
                        else
                        {
                            //if it has a parent add the node to its parents node collection
                            objRelative.Parent.Nodes.Insert(objRelative.Index, new TreeNode(strText));

                            //asign the newly created TreeNode to the return variable
                            objNode = objRelative.Parent.Nodes[objRelative.Index];
                        }
                        //assign the Name property with the value passed from the Key paramether
                        objNode.Name = strKey;
                        break;

                    default:
                        break;
                }
            }

            //return the new node
            return objNode;
        }

        /// <summary>
        /// public method used recursively to pass through the nodes in the treeview
        /// </summary>
        /// <param name="objNode">Parent TreeNode object</param>
        /// <param name="objNodesCollection">TreeNode collection to store all treenodes</param>
        internal static void BuildChildNodesList(TreeNode objNode, List<TreeNode> objNodesCollection)
        {
            // Add TreeNode to the collection
            objNodesCollection.Add(objNode);

            // Loop through the nodes
            foreach (TreeNode objChild in objNode.Nodes)
            {
                // If this node object has child call the method again
                BuildChildNodesList(objChild, objNodesCollection);
            }
        }

        /// <summary>
        /// Sets the style of the TreeView
        /// </summary>
        /// <param name="objTreeView"></param>
        /// <param name="enuTreeStyle"></param>
        public static void Style(this FCTreeView objTreeView, TreeStyleConstants enuTreeStyle)
        {
            //get the Tree View definition
            TreeViewDefinitions objTreeViewDefinitions = TreeViewDefinitions.GetTreeViewDefinitions(objTreeView);

            //save the curent ImageList to the definition
            objTreeViewDefinitions.ImageList = objTreeView.ImageList;
            
            //check what style is being applied
            switch (enuTreeStyle)
            {
                case TreeStyleConstants.tvwTextOnly:
                    //hide lines
                    objTreeView.ShowLines = false;
                    
                    //hide plusminus
                    objTreeView.ShowPlusMinus = false;

                    //remove the ImageList from the TreeView
                    objTreeView.ImageList = null;
                    break;
                case TreeStyleConstants.tvwPictureText:
                    //hide lines
                    objTreeView.ShowLines = false;

                    //hide plusminus
                    objTreeView.ShowPlusMinus = false;

                    //restore the previosly saved ImageList
                    objTreeView.ImageList = objTreeViewDefinitions.ImageList;
                    break;
                case TreeStyleConstants.tvwPlusMinusText:
                    //hide lines
                    objTreeView.ShowLines = false;

                    //show plusminus
                    objTreeView.ShowPlusMinus = true;

                    //remove the ImageList from the TreeView
                    objTreeView.ImageList = null;
                    break;
                case TreeStyleConstants.tvwPlusPictureText:
                    //hide lines
                    objTreeView.ShowLines = false;

                    //show plusminus
                    objTreeView.ShowPlusMinus = true;

                    //restore the previosly saved ImageList
                    objTreeView.ImageList = objTreeViewDefinitions.ImageList;
                    break;
                case TreeStyleConstants.tvwTreelinesText:
                    //show lines
                    objTreeView.ShowLines = true;

                    //hide plusminus
                    objTreeView.ShowPlusMinus = false;

                    //remove the ImageList from the TreeView
                    objTreeView.ImageList = null;
                    break;
                case TreeStyleConstants.tvwTreelinesPictureText:
                    //show lines
                    objTreeView.ShowLines = true;

                    //hide plusminus
                    objTreeView.ShowPlusMinus = false;

                    //restore the previosly saved ImageList
                    objTreeView.ImageList = objTreeViewDefinitions.ImageList;
                    break;
                case TreeStyleConstants.tvwTreelinesPlusMinusText:
                    //show lines
                    objTreeView.ShowLines = true;

                    //show plusminus
                    objTreeView.ShowPlusMinus = true;

                    //remove the ImageList from the TreeView
                    objTreeView.ImageList = null;
                    break;
                case TreeStyleConstants.tvwTreelinesPlusMinusPictureText:
                    //show lines
                    objTreeView.ShowLines = true;

                    //show plusminus
                    objTreeView.ShowPlusMinus = true;

                    //restore the previosly saved ImageList
                    objTreeView.ImageList = objTreeViewDefinitions.ImageList;
                    break;
            }

            //save the Style value in the definition
            objTreeViewDefinitions.Style = enuTreeStyle;
        }

        /// <summary>
        /// Gets the Style of the TreeView
        /// </summary>
        /// <param name="objTreeView"></param>
        /// <returns></returns>
        public static TreeStyleConstants Style(this TreeView objTreeView)
        {
            //get the instance for the definition
            TreeViewDefinitions objTreeViewDefinitions = TreeViewDefinitions.GetTreeViewDefinitions(objTreeView);

            //return the saved value for the Style
            return objTreeViewDefinitions.Style;
        }

        public static TreeNode Nodes(this TreeView objTreeView, int index)
        {
            TreeNode objNode = null;

            objNode = objTreeView.Nodes[index];

            return objNode;
        }

        public static TreeNode Nodes(this TreeView objTreeView, string key)
        {
            TreeNode objNode = null;

            objNode = objTreeView.Nodes[key];

            if (objNode == null && !string.IsNullOrEmpty(key))
            {
                foreach (TreeNode node in objTreeView.GetAllNodes())
                {
                    if (node.Tag == key || node.Name == key)
                    {
                        objNode = node;
                        break;
                    }
                }
            }

            return objNode;
        }

        /// <summary>
        /// Sets the single sel.
        /// </summary>
        /// <param name="objTreeView">The obj tree view.</param>
        /// <param name="blnValue">if set to <c>true</c> [BLN value].</param>
        public static void SetSingleSel(this TreeView objTreeView, bool blnValue)
        {

            // Check for null reference
            if (objTreeView != null)
            {

                // If value is true attach the mouse click event and set the value 
                if (blnValue)
                {
                    // Attaching mouse click event 
                    objTreeView.NodeMouseClick += new TreeNodeMouseClickEventHandler(objTreeView_NodeMouseClick);

                    // Set the value
                    GenericExtender.SetValue(objTreeView, SingleSel, blnValue);
                }

                // If the value is false then detach even and set the value to false
                else
                {
                    // Detaching the event
                    objTreeView.NodeMouseClick -= objTreeView_NodeMouseClick;

                    // Save value as false
                    GenericExtender.SetValue(objTreeView, SingleSel, blnValue);
                }
            }
        }

        /// <summary>
        /// Handles the NodeMouseClick event of the objTreeView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TreeNodeMouseClickEventArgs"/> instance containing the event data.</param>
        static void objTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

            // Cast sender as TreeView 
            TreeView objTreeView = sender as TreeView;

            // Get the SingleSel value 
            bool blnSingleSel = GenericExtender.GetOrCreateValue<bool>(objTreeView, SingleSel, true);

            // Get tree node collection
            TreeNodeCollection objTreeNodeCollection = objTreeView.Nodes;

            // If SingleSel value is true 
            if (blnSingleSel)
            {

                // Get current selected node
                TreeNode objNode = e.Node;

                // Make sure that is parent 
                if (objNode.Parent == null)
                {

                    // Loop through all nodes 
                    foreach (TreeNode objTreeNode in objTreeNodeCollection)
                    {

                        // If current node is parent then collapse else leave it expanded
                        if (objTreeNode.Parent == null)
                        {
                            objTreeNode.Collapse(true);
                        }

                    }

                    // Expand the clicked node
                    objNode.Expand();

                    // Find the root and expand
                    while (objNode.Parent != null)
                    {
                        objNode = objNode.Parent;

                        objNode.Expand();

                    }
                }

                // If node is not a parent then collapse all nodes
                else
                {


                    // Expand current clicked node 
                    objNode.Expand();

                    // Expand all nodes until you reach the root
                    while (objNode.Parent != null)
                    {

                        objNode = objNode.Parent;

                        objNode.Expand();

                    }

                    TreeNode objCurrentRoot = objNode;

                    // Loop through all nodes 
                    foreach (TreeNode objTreeNode in objTreeNodeCollection)
                    {
                        if (objTreeNode != objCurrentRoot)
                        {
                            objTreeNode.Collapse();
                        }
                    }
                }
            }

        }
    }

    public class TreeViewDefinitions : ControlDefinitions
    {
        public static TreeViewDefinitions GetTreeViewDefinitions(TreeView objTreeView)
        {
            TreeViewDefinitions objTreeViewDefinitions;
            objTreeViewDefinitions = new TreeViewDefinitions();
            return objTreeViewDefinitions;
        }

        public ImageList mobjImageList = null;
        public TreeStyleConstants mobjStyle = TreeStyleConstants.tvwTreelinesPlusMinusPictureText;

        public ImageList ImageList
        {
            get
            {
                return mobjImageList;
            }
            set
            {
                if (value != null)
                    mobjImageList = value;
            }
        }

        public TreeStyleConstants Style
        {
            get
            {
                return mobjStyle;
            }
            set
            {
                mobjStyle = value;
            }
        }

    }
}
