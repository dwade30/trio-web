﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fecherFoundation.Extensions
{
    public static class XmlDocumentExtensions
    {
        public static XmlNodeList GetElementsByTagNameOrXPath(this XmlDocument xmlDocument, string nameOrXPath)
        {
            if (!string.IsNullOrEmpty(nameOrXPath))
            {
                //if XPath, use SelectNodes
                if (nameOrXPath.Contains("/"))
                {
                    return xmlDocument.SelectNodes(nameOrXPath);
                }
                else
                {
                    return xmlDocument.GetElementsByTagName(nameOrXPath);
                }
            }
            return null;
        }
    }
}
