﻿using System;

namespace fecherFoundation.Extensions
{
    public static class ArrayExtensionMethods
    {
        public static T[] ToArray<T>(this Array array)
        {
            var startIndex = array.GetLowerBound(0);
            var len = array.Length + startIndex;
            T[] res = new T[len];

            for (int i = startIndex; i < len; i++)
            {
                res[i] = (T)array.GetValue(i);
            }

            return res;
        }

        //CHE - #i12 standard array function CopyTo does not support destination length lower than source length
        public static void CopyTo(this Array source, Array destination)
        {
            //CHE - #i90 - allocate if necessary for array of non-value type
            Type t = destination.GetType().GetElementType();
            if (!t.IsValueType && t != typeof(string))
            {
                for (int j = 0; j < destination.Length; j++)
                {
                    if (destination.GetValue(j) == null)
                    {
                        destination.SetValue(Activator.CreateInstance(destination.GetType().GetElementType()), j);
                    }
                }
            }

            Array.Copy(source, destination, Math.Min(source.Length, destination.Length));
        }
    }
}
