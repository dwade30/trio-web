﻿#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Resources;
#endif

namespace fecherFoundation.Extensions
{

    
    /// <summary>
    /// Extension of DataGridView - added movefirst, movenext (etc.) support
    /// </summary>
    public static class DataGridViewExtension
    {
        /// <summary>
        /// Gets the number of fixed (frozen) rows
        /// </summary>
        /// <param name="objDataGridView"></param>
        /// <returns></returns>
        public static int FixedRows (this DataGridView objDataGridView)
        {
            int i=0;
            int rc = 0;
            for (i = 0; i < objDataGridView.Rows.Count && objDataGridView.Rows [i].Frozen; i++)
            {
                rc++;
            }
            return rc;
        }

        /// <summary>
        /// Sets the number of fixed (frozen) rows
        /// </summary>
        /// <param name="objDataGridView"></param>
        /// <param name="value"></param>
        public static void FixedRows(this DataGridView objDataGridView, int value)
        {
            int i = 0;
            for (i = 0; i < objDataGridView.Rows.Count; i++)
            {
                if (objDataGridView.Rows[i].Frozen)
                {
                    objDataGridView.Rows[i].Frozen = false;
                }
            }
            for (i = 0; i < objDataGridView.Rows.Count && i <= value -1; i++)
            {
                objDataGridView.Rows[i].Frozen = true;
            }
        }
        /// <summary>
        /// Gets the number of fixed (frozen) columns
        /// </summary>
        /// <param name="objDataGridView"></param>
        /// <returns></returns>
        public static int FixedCols(this DataGridView objDataGridView)
        {
            int i = 0;
            int rc = 0;
            for (i = 0; i < objDataGridView.Columns.Count && objDataGridView.Columns[i].Frozen; i++)
            {
                rc++;
            }
            return rc;
        }

        /// <summary>
        /// Sets the number of fixed (frozen) columns
        /// </summary>
        /// <param name="objDataGridView"></param>
        /// <param name="value"></param>
        public static void FixedCols(this DataGridView objDataGridView, int value)
        {
            int i = 0;
            for (i = 0; i < objDataGridView.Columns.Count; i++)
            {
                if (objDataGridView.Columns[i].Frozen)
                {
                    objDataGridView.Columns[i].Frozen = false;
                }
            }
            for (i = 0; i < objDataGridView.Columns.Count && i <= value - 1; i++)
            {
                objDataGridView.Columns[i].Frozen = true;
            }
        }
    }
   
}