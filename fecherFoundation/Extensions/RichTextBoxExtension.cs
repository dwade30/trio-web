﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation.Extensions
{
    public static class RichTextBoxExtension
    {
        // TODO:BAN
        public static void SetSelectionFontName(this RichTextBox objControl, string fontName)
        {
        }

        // TODO:BAN
        public static void SetSelectionFontSize(this RichTextBox objControl, int fontSize)
        {
        }

        public static void SetSelectionFontBold(this RichTextBox objControl, bool blnBold)
        {
            objControl.SelectionFont = ControlExtension.SetFontProperty(objControl.SelectionFont, blnBold, FontStyle.Bold);
        }

        public static void SetSelectionFontItalic(this RichTextBox objControl, bool blnItalic)
        {
            objControl.SelectionFont = ControlExtension.SetFontProperty(objControl.SelectionFont, blnItalic, FontStyle.Italic);
        }

        public static void SetSelectionFontUnderline(this RichTextBox objControl, bool blnUnderline)
        {
            objControl.SelectionFont = ControlExtension.SetFontProperty(objControl.SelectionFont, blnUnderline, FontStyle.Underline);
        }
    }
}
