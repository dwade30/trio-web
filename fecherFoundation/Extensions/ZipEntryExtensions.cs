﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    /// <summary>
    /// Provides static methods for creating, extracting, and opening zip archives.
    /// </summary>
    public class ZipFile
    {
        private static readonly char s_pathSeperator = '/';

        /// <summary>
        /// Opens a zip archive at the specified path and in the specified mode.
        /// </summary>
        /// <param name="archiveFileName">he path to the archive to open, specified as a relative or absolute path. A relative path is interpreted as relative to the current working directory.</param>
        /// <param name="mode">One of the enumeration values that specifies the actions which are allowed on the entries in the opened archive.</param>
        /// <returns>The opened zip archive.</returns>
        public static ZipArchive Open(string archiveFileName, ZipArchiveMode mode)
        {
            return Open(archiveFileName, mode, null);
        }

        /// <summary>
        /// Opens a zip archive at the specified path and in the specified mode.
        /// </summary>
        /// <param name="archiveFileName">he path to the archive to open, specified as a relative or absolute path. A relative path is interpreted as relative to the current working directory.</param>
        /// <param name="mode">One of the enumeration values that specifies the actions which are allowed on the entries in the opened archive.</param>
        /// <param name="entryNameEncoding">The encoding to use when reading or writing entry names in this archive. Specify a value for this parameter only when an encoding is required for interoperability with zip archive tools and libraries that do not support UTF-8 encoding for entry names.</param>
        /// <returns>The opened zip archive.</returns>
        public static ZipArchive Open(string archiveFileName, ZipArchiveMode mode, Encoding entryNameEncoding)
        {
            FileMode mode2;
            FileAccess access;
            FileShare share;
            switch ((int)mode)
            {
                case 0:
                    mode2 = FileMode.Open;
                    access = FileAccess.Read;
                    share = FileShare.Read;
                    break;
                case 1:
                    mode2 = FileMode.CreateNew;
                    access = FileAccess.Write;
                    share = FileShare.None;
                    break;
                case 2:
                    mode2 = FileMode.OpenOrCreate;
                    access = FileAccess.ReadWrite;
                    share = FileShare.None;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("mode");
            }
            FileStream fileStream = null;
            try
            {
                fileStream = File.Open(archiveFileName, mode2, access, share);
                return new ZipArchive((Stream)fileStream, mode, false, entryNameEncoding);
            }
            catch
            {
                fileStream?.Dispose();
                throw;
            }
        }

        public static void CreateFromDirectory(string sourceDirectoryName, string destinationArchiveFileName)
        {
            DoCreateFromDirectory(sourceDirectoryName, destinationArchiveFileName, null, false, null);
        }

        private static void DoCreateFromDirectory(string sourceDirectoryName, string destinationArchiveFileName, CompressionLevel? compressionLevel, bool includeBaseDirectory, Encoding entryNameEncoding)
        {
            sourceDirectoryName = Path.GetFullPath(sourceDirectoryName);
            destinationArchiveFileName = Path.GetFullPath(destinationArchiveFileName);
            ZipArchive val = Open(destinationArchiveFileName, ZipArchiveMode.Create, entryNameEncoding);
            try
            {
                bool flag = true;
                DirectoryInfo directoryInfo = new DirectoryInfo(sourceDirectoryName);
                string fullName = directoryInfo.FullName;
                if (includeBaseDirectory && directoryInfo.Parent != null)
                {
                    fullName = directoryInfo.Parent.FullName;
                }
                char c;
                foreach (FileSystemInfo item in directoryInfo.EnumerateFileSystemInfos("*", SearchOption.AllDirectories))
                {
                    flag = false;
                    int length = item.FullName.Length - fullName.Length;
                    string text;
                    text = EntryFromPath(item.FullName, fullName.Length, length);
                    if (item is FileInfo)
                    {
                        ZipFileExtensions.DoCreateEntryFromFile(val, item.FullName, text, compressionLevel);
                    }
                    else
                    {
                        DirectoryInfo directoryInfo2 = item as DirectoryInfo;
                        if (directoryInfo2 != null && IsDirEmpty(directoryInfo2))
                        {
                            ZipArchive obj = val;
                            string str = text;
                            c = s_pathSeperator;
                            obj.CreateEntry(str + c.ToString());
                        }
                    }
                }
                if (includeBaseDirectory & flag)
                {
                    string text = EntryFromPath(directoryInfo.Name, 0, directoryInfo.Name.Length);
                    ZipArchive obj2 = val;
                    string str2 = text;
                    c = s_pathSeperator;
                    obj2.CreateEntry(str2 + c.ToString());
                }
            }
            finally
            {
                ((IDisposable)val)?.Dispose();
            }
        }

        private static string EntryFromPath(string entry, int offset, int length)
        {
            while (length > 0 && (entry[offset] == Path.DirectorySeparatorChar || entry[offset] == Path.AltDirectorySeparatorChar))
            {
                offset++;
                length--;
            }
            if (length == 0)
            {
                return string.Empty;
            }
            char[] array = entry.ToCharArray(offset, length);
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == Path.DirectorySeparatorChar || array[i] == Path.AltDirectorySeparatorChar)
                {
                    array[i] = s_pathSeperator;
                }
            }
            return new string(array);
        }

        private static bool IsDirEmpty(DirectoryInfo possiblyEmptyDir)
        {
            using (IEnumerator<FileSystemInfo> enumerator = possiblyEmptyDir.EnumerateFileSystemInfos("*", SearchOption.AllDirectories).GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    FileSystemInfo current = enumerator.Current;
                    return false;
                }
            }
            return true;
        }
    }

    public static class ZipFileExtensions
    {
        public static ZipArchiveEntry CreateEntryFromFile(this ZipArchive destination, string sourceFileName, string entryName)
        {
            return DoCreateEntryFromFile(destination, sourceFileName, entryName, null);
        }

        public static ZipArchiveEntry CreateEntryFromFile(this ZipArchive destination, string sourceFileName, string entryName, CompressionLevel compressionLevel)
        {
            return DoCreateEntryFromFile(destination, sourceFileName, entryName, compressionLevel);
        }

        internal static ZipArchiveEntry DoCreateEntryFromFile(ZipArchive destination, string sourceFileName, string entryName, CompressionLevel? compressionLevel)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination");
            }
            if (sourceFileName == null)
            {
                throw new ArgumentNullException("sourceFileName");
            }
            if (entryName == null)
            {
                throw new ArgumentNullException("entryName");
            }
            using (Stream stream = File.Open(sourceFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                ZipArchiveEntry val = compressionLevel.HasValue ? destination.CreateEntry(entryName, compressionLevel.Value) : destination.CreateEntry(entryName);
                DateTime dateTime = File.GetLastWriteTime(sourceFileName);
                if (dateTime.Year < 1980 || dateTime.Year > 2107)
                {
                    dateTime = new DateTime(1980, 1, 1, 0, 0, 0);
                }
                val.LastWriteTime = (DateTimeOffset)dateTime;
                using (Stream destination2 = val.Open())
                {
                    stream.CopyTo(destination2);
                }
                return val;
            }
        }

        public static void ExtractToDirectory(this ZipArchive source, string destinationDirectoryName)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (destinationDirectoryName == null)
            {
                throw new ArgumentNullException("destinationDirectoryName");
            }
            DirectoryInfo directoryInfo = Directory.CreateDirectory(destinationDirectoryName);
            string fullName = directoryInfo.FullName;
            foreach (ZipArchiveEntry current in source.Entries)
            {
                string fullPath = Path.GetFullPath(Path.Combine(fullName, current.FullName));
                if (!fullPath.StartsWith(fullName, StringComparison.OrdinalIgnoreCase))
                {
                    throw new IOException("Extracting Zip entry would have resulted in a file outside the specified destination directory.");
                }
                if (Path.GetFileName(fullPath).Length == 0)
                {
                    if (current.Length != 0L)
                    {
                        throw new IOException("Zip entry name ends in directory separator character but contains data.");
                    }
                    Directory.CreateDirectory(fullPath);
                }
                else
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
                    current.ExtractToFile(fullPath, false);
                }
            }
        }

        public static void ExtractToFile(this ZipArchiveEntry source, string destinationFileName)
        {
            source.ExtractToFile(destinationFileName, false);
        }

        public static void ExtractToFile(this ZipArchiveEntry source, string destinationFileName, bool overwrite)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (destinationFileName == null)
            {
                throw new ArgumentNullException("destinationFileName");
            }
            FileMode mode = overwrite ? FileMode.Create : FileMode.CreateNew;
            using (Stream stream = File.Open(destinationFileName, mode, FileAccess.Write, FileShare.None))
            {
                using (Stream stream2 = source.Open())
                {
                    stream2.CopyTo(stream);
                }
            }
            File.SetLastWriteTime(destinationFileName, source.LastWriteTime.DateTime);
        }
    }
}
