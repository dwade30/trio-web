﻿namespace fecherFoundation.Extensions
{
    public static class SystemInformation
    {
        /// <summary>
        /// Gets the default time (in milliseconds) for Windows7 that can elapse between a first click and a second click for the OS to consider the mouse action a double-click.
        /// </summary>
        public static int DoubleClickTime
        {
            get
            {
                return 500;
            }
        }
    }
}
