﻿using GrapeCity.ActiveReports.SectionReportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation.Extensions
{
    public static class ActiveReportsExtensions
    {
        /// <summary>
        /// JEI:HARRIS:IT110 bug in AR, when using Text, WordWrap is not working
        /// therefore we use the Html property, but then we have to replace the line breaks with
        /// the HTML 'br' tag
        /// </summary>
        /// <param name="richTextBox"></param>
        /// <param name="text"></param>
        public static void SetHtmlText(this RichTextBox richTextBox, string text)
        {
			string fontStyle = "font-family:" + richTextBox.Font.Name + "; font-size:" + richTextBox.Font.SizeInPoints + "px; "+ (richTextBox.Font.Bold ? " font-weight:bold;" : "") + (richTextBox.Font.Italic ? " font-style:italic;" : "");
			richTextBox.Html = "<pre style='" + fontStyle + "'>" + text + "</pre>";
        }
    }
}
