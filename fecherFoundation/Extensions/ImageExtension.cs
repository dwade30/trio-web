﻿using System;
using System.Drawing;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif

namespace fecherFoundation.Extensions
{
    public static class ImageExtension
    {
        public static void Stretch(this PictureBox objPictureBox, bool booValue)
        {
            if (booValue)
            {
                objPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else
            {
                objPictureBox.SizeMode = PictureBoxSizeMode.Normal;
            }

        }

        public static bool Stretch(this PictureBox objPictureBox)
        {
            if (objPictureBox.SizeMode == PictureBoxSizeMode.StretchImage)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Creates an icon from a ListImage object in an ImageList control
        /// </summary>
        /// <param name="objImage">The obj image.</param>
        /// <returns></returns>
        /// <exception cref="System.NullReferenceException"></exception>
        public static Icon ExtractIcon(this Image objImage)
        {

            // Check for null reference
            if (objImage == null)
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }

            else
            {

                // Convert image to bitmap
                Bitmap objBitmap = objImage as Bitmap;

                // Create handle 
                IntPtr objHandle = objBitmap.GetHicon();

                // Convert to icon
                Icon objIcon = Icon.FromHandle(objHandle);

                // Return the icon
                return objIcon;
            }
        }
    }
}
