﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using Wisej.Web;
using System.Reflection;
using System.Collections;
using Wisej.Web.Ext.CustomProperties;

namespace fecherFoundation.Extensions
{
    public enum LineBoxStyle
    {
        None,
        Box,
        BoxFill
    }

    public enum ZOrderConstants
    {
        //     Equivalent to the Visual Basic 6.0 constant vbBringToFront.
        BringToFront = 0,
        //     Equivalent to the Visual Basic 6.0 constant vbSend ToBack.
        SendToBack = 1
    }

    public static class ControlExtension
    {
        public static string mstrAccessibleName = "";
        public static string mstrAccessibleDescription = "";
        public static AccessibleRole menmAccessibleRole;
        public static bool mblnInvokeRequired;
        private static readonly object ToolTipExtenderKey = new object();
        internal static Dictionary<Control, ControlDefinitions> mobjDefinitions = new Dictionary<Control, ControlDefinitions>();

        /// <summary>
        /// Extension : Gets the name of the control used by accessibility client applications.
        /// </summary>
        /// <param name="objControl">The current control.</param>
        /// <returns>Property value.</returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static string AccessibleName(this Control objControl)
        {
            return mstrAccessibleName;
        }

        /// <summary>
        /// Extension : Sets the name of the control used by accessibility client applications.
        /// </summary>
        /// <param name="objControl">The current control.</param>
        /// <param name="strValue">Property value.</param>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void AccessibleName(this Control objControl, string strValue)
        {
            mstrAccessibleName = strValue;
        }


        /// <summary>
        /// Extension : Gets the description of the control used by accessibility client applications.
        /// </summary>
        /// <param Description="objControl">The current control.</param>
        /// <returns>Property value.</returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static string AccessibleDescription(this Control objControl)
        {
            return mstrAccessibleDescription;
        }

        /// <summary>
        /// Extension : Sets the description of the control used by accessibility client applications.
        /// </summary>
        /// <param Description="objControl">The current control.</param>
        /// <param Description="strValue">Property value.</param>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void AccessibleDescription(this Control objControl, string strValue)
        {
            mstrAccessibleDescription = strValue;
        }

        /// <summary>
        /// Extension : Gets the accessible role of the control.
        /// </summary>
        /// <param Description="objControl">The current control.</param>
        /// <returns>Property value.</returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static AccessibleRole AccessibleRole(this Control objControl)
        {
            return menmAccessibleRole;
        }

        /// <summary>
        /// Extension : Sets the accessible role of the control.
        /// </summary>
        /// <param Description="objControl">The current control.</param>
        /// <param Description="roleValue">Property value.</param>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void AccessibleRole(this Control objControl, AccessibleRole roleValue)
        {
            menmAccessibleRole = roleValue;
        }

        /// <summary>
        /// Extension : Gets a value indicating whether the caller must call an invoke method when making method calls to the control because the caller is on a different thread than the one the control was created on.
        /// </summary>
        /// <param name="objControl">The current control.</param>
        /// <returns>Property value.</returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static bool InvokeRequired(this Control objControl)
        {
            return mblnInvokeRequired;
        }

        /// <summary>
        /// Extension : Sets a value indicating whether the caller must call an invoke method when making method calls to the control because the caller is on a different thread than the one the control was created on.
        /// </summary>
        /// <param name="objControl">The current control.</param>
        /// <param name="blnValue">Property value.</param>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void InvokeRequired(this Control objControl, bool blnValue)
        {
            mblnInvokeRequired = blnValue;
        }

        public static void Appearance(this Control objControl, int intValue)
        {
            //Do nothing
        }


        public static int Appearance(this Control objControl)
        {
            return 1;
        }

        /// <summary>
        /// Sets the MaskColor property
        /// </summary>
        /// <param name="objControl"></param>
        /// <param name="objColor"></param>
        public static void MaskColor(this Control objControl, Color objColor)
        {
            //get the instance of the definitions class
            ControlDefinitions objControlDefinitions = ControlDefinitions.GetControlDefinitions(objControl);

            //save the new value for the MaskColor
            objControlDefinitions.MaskColor = objColor;
        }

        /// <summary>
        /// Gets the MaskColor property
        /// </summary>
        /// <param name="objControl"></param>
        /// <returns></returns>
        public static Color MaskColor(this Control objControl)
        {
            //get the instance of the definitions class
            ControlDefinitions objControlDefinitions = ControlDefinitions.GetControlDefinitions(objControl);
            
            //return the value for the MaskColor
            return objControlDefinitions.MaskColor; 
        }

        /// <summary>
        /// Sets the UseMaskColor property
        /// </summary>
        /// <param name="objControl"></param>
        /// <param name="boolValue"></param>
        public static void UseMaskColor(this Control objControl, bool boolValue)
        {
            //get the instance of the definitions class
            ControlDefinitions objControlDefinitions = ControlDefinitions.GetControlDefinitions(objControl);

            //set the value for the UseMaskColor
            objControlDefinitions.UseMaskColor = boolValue;
        }

        /// <summary>
        /// Gets the UseMaskColor property
        /// </summary>
        /// <param name="objControl"></param>
        /// <returns></returns>
        public static bool UseMaskColor(this Control objControl)
        {
            //get the instance of the definitions class
            ControlDefinitions objControlDefinitions = ControlDefinitions.GetControlDefinitions(objControl);

            //return the value from the definition
            return objControlDefinitions.UseMaskColor;
        }

        /// <summary>
        /// Sets the DisabledPicture property. Not implemented.
        /// </summary>
        /// <param name="objControl"></param>
        /// <param name="objImage"></param>
        [Obsolete("Not implemented. Added for migration compatibility")]
        public static void DisabledPicture(this Control objControl, Image objImage)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the DisabledPicture property. Not implemented.
        /// </summary>
        /// <param name="objControl"></param>
        /// <returns></returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        public static Image DisabledPicture(this Control objControl)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets the DownPicture property. Not implemented.
        /// </summary>
        /// <param name="objControl"></param>
        /// <param name="objImage"></param>
        [Obsolete("Not implemented. Added for migration compatibility")]
        public static void DownPicture(this Control objControl, Image objImage)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the DownPicture property. Not implemented.
        /// </summary>
        /// <param name="objControl"></param>
        /// <returns></returns>
        [Obsolete("Not implemented. Added for migration compatibility")]
        public static Image DownPicture(this Control objControl)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets the custom mouse cursor.
        /// Replace VB6 MouseIcon property
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="objImage">The image.</param>
        public static void SetMouseIcon(this Control objControl, Image objImage)
        {
            // Check input
            if (objImage == null)
            {
                return;
            }
            try
            {
                // Create bitmap
                Bitmap bmp = new Bitmap(objImage);
                SetMouseIcon(objControl, bmp);

            }
            catch
            {
                // Disregard cursor processing errors
            }
        }
        /// <summary>
        /// Sets the custom mouse cursor.
        /// Replace VB6 MouseIcon property
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="objBitmap">The bitmap.</param>
        public static void SetMouseIcon(this Control objControl, Bitmap objBitmap)
        {
            // Check input
            if (objBitmap == null)
            {
                return;
            }
            try
            {
                // Set cursor
                objControl.Cursor = new Cursor(objBitmap);

            }
            catch
            {
                // Disregard cursor processing errors
            }
        }
        /// <summary>
        /// Sets the font bold.
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="blnBold">If set to <c>true</c> convert font to bold.</param>
        public static void SetFontBold(this Control objControl, bool blnBold)
        {
            SetFontProperty(objControl, blnBold, FontStyle.Bold);
        }
        /// <summary>
        /// Sets the font italic.
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="blnItalic">If set to <c>true</c> convert font to italic.</param>
        public static void SetFontItalic(this Control objControl, bool blnItalic)
        {
            SetFontProperty(objControl, blnItalic, FontStyle.Italic);
        }
        /// <summary>
        /// Sets the font strikeout.
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="blnStrikeout">If set to <c>true</c> convert font to strikeout.</param>
        public static void SetFontStrikeout(this Control objControl, bool blnStrikeout)
        {
            SetFontProperty(objControl, blnStrikeout, FontStyle.Strikeout);
        }
        /// <summary>
        /// Sets the font underline.
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="blnUnderline">If set to <c>true</c> convert font to underline.</param>
        public static void SetFontUnderline(this Control objControl, bool blnUnderline)
        {
            SetFontProperty(objControl, blnUnderline, FontStyle.Underline);
        }

        public static void SetFontName(this Control objControl, string fontName)
        {
            Font oldFont = objControl.Font;

            if (objControl.Font.FontFamily.Name == fontName)
            {
                return;
            }

            FontFamily fontFamily = new FontFamily(fontName);
            //CHE: need to cast to FCForm/FCPictureBox otherwise new Font property of FCForm/FCPictureBox is not used when accessing through Control
            FCForm frm = objControl as FCForm;
            if (frm != null)
            {
                frm.Font = FCUtils.CreateFont(fontFamily, objControl.Font.Size, objControl.Font.Style);
            }
            else
            {
                FCPictureBox pic = objControl as FCPictureBox;
                if (pic != null)
                {
                    pic.Font = FCUtils.CreateFont(fontFamily, objControl.Font.Size, objControl.Font.Style);
                }
                else
                {
                    objControl.Font = FCUtils.CreateFont(fontFamily, objControl.Font.Size, objControl.Font.Style);
                }
            }
            FCUtils.DisposeFont(objControl, oldFont);
        }

        public static void SetFontSize(this Control objControl, float fontSize)
        {
            Font oldFont = objControl.Font;

            if (objControl.Font.Size == fontSize)
            {
                return;
            }

            FontFamily fontFamily = new FontFamily(objControl.Font.Name);
            //CHE: need to cast to FCForm/FCPictureBox otherwise new Font property of FCForm/FCPictureBox is not used when accessing through Control
            FCForm frm = objControl as FCForm;
            if (frm != null)
            {
                frm.Font = FCUtils.CreateFont(fontFamily, fontSize, objControl.Font.Style);
            }
            else
            {
                FCPictureBox pic = objControl as FCPictureBox;
                if (pic != null)
                {
                    pic.Font = FCUtils.CreateFont(fontFamily, fontSize, objControl.Font.Style);
                }
                else
                {
                    objControl.Font = FCUtils.CreateFont(fontFamily, fontSize, objControl.Font.Style);
                }
            }
            FCUtils.DisposeFont(objControl, oldFont);
        }

        /// <summary>
        /// Sets the font property.
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="blnSet">if set to <c>true</c> set specified font property.</param>
        /// <param name="objStyle">The font style to set.</param>
        private static void SetFontProperty(Control objControl, bool blnSet, FontStyle objStyle)
        {
            //CHE: need to cast to FCForm/FCPictureBox otherwise new Font property of FCForm/FCPictureBox is not used when accessing through Control
            FCForm frm = objControl as FCForm;
            if (frm != null)
            {
                frm.Font = SetFontProperty(objControl.Font, blnSet, objStyle);
            }
            else
            {
                FCPictureBox pic = objControl as FCPictureBox;
                if (pic != null)
                {
                    pic.Font = SetFontProperty(objControl.Font, blnSet, objStyle);
                }
                else
                {
                    objControl.Font = SetFontProperty(objControl.Font, blnSet, objStyle);
                }
            }
        }

        /// <summary>
        /// Sets the font property.
        /// </summary>
        /// <param name="objFont">The font.</param>
        /// <param name="blnSet">if set to <c>true</c> [BLN set].</param>
        /// <param name="objStyle">The style.</param>
        /// <returns></returns>
        internal static Font SetFontProperty(Font objFont, bool blnSet, FontStyle objStyle)
        {
            FontStyle objFontStyle = (blnSet) ? objFont.Style | objStyle : objFont.Style & ~objStyle;
            Font font = new Font(objFont, objFontStyle);
            FCUtils.AddFont(font);
            return font;
        }

        /// <summary>
        /// Center control inside container
        /// </summary>
        /// <param name="control">Control to be centered</param>
        /// <param name="container">Parent container control</param>
        /// <param name="hCenter">Horizontal center flag</param>
        /// <param name="vCenter">Vertical center flag</param>
        public static void CenterToContainer(this Control control, Control container, bool hCenter = true, bool vCenter = true, bool maximize = false)
        {
            if (maximize)
            {
                control.Location = new Point(20, 0);
                control.Size = new Size(container.Width - 50, container.Height);
            }
            else
            {
                if (vCenter)
                {
                    if (control.Height < container.Height)
                    {
                        control.Top = (container.Height - control.Height) / 2;
                    }
                    else
                    {
                        control.Top = 0;
                    }
                }
                if (hCenter)
                {
                    if (control.Width < container.Width)
                    {
                        control.Left = (container.Width - control.Width) / 2;
                    }
                    else
                    {
                        control.Left = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Attach client event to limit user input only to numbers and dot character
        /// </summary>
        /// <param name="control"></param>
        /// <param name="allowDot"></param>
        public static void AllowOnlyNumericInput(this Control control, bool allowDot = false)
        {
            if (allowDot)
            {
                if (control.UserData.AllowOnlyNumbersAndOneDot == null)
                {                    
                    control.ClientEvents.Add(ControlExtension.Statics.ClientEventAllowOnlyNumbersAndOneDot);
                    control.Eval(@"this.getContentElement().addListener('paste', TextBox_OnPasteWithDot)");
                    control.UserData.AllowOnlyNumbersAndOneDot = true;
                }
            }
            else
            {
                if (control.UserData.AllowOnlyNumbers == null)
                {
                    control.ClientEvents.Add(ControlExtension.Statics.ClientEventAllowOnlyNumbers);
                    control.Eval(@"this.getContentElement().addListener('paste', TextBox_OnPaste)");
                    control.UserData.AllowOnlyNumbers = true;
                }
            }
        }

        public static void RemoveClientEvents(this Control control)
        {
            control.ClientEvents.Clear();
            control.UserData.AllowOnlyNumbers = null;
            control.UserData.AllowOnlyNumbersAndOneDot = null;
        }

        /// <summary>
        /// Attach client event for removing non numeric characters form control. Used for grid editors, before cells enters in edit mode
        /// </summary>
        /// <param name="control"></param>
        public static void RemoveAlphaCharactersOnValueChange(this Control control)
        {
            JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
            if (control.UserData.RemoveAlphaCharactersOnValueChange == null)
            {
                JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(control);
                clientEvents.Add(ControlExtension.Statics.ClientEventValueChange_RemoveAlphaCharacters);
                control.UserData.RemoveAlphaCharactersOnValueChange = true;
            }
        }

        /// <summary>
        /// Add custom keys to be checked on client side
        /// </summary>
        /// <param name="control">Text Editing control</param>
        /// <param name="allowedKeys">Allowed keys parameter (range of keycodes, key codes or key identifiers). Sample: "'Left','Right','Delete','Home','End','Insert',8,9,13,48..57,'.'"</param>
        public static void AllowKeysOnClientKeyPress(this Control control, string allowedKeys, bool disablePaste = true)
        {
            if (control.UserData.AllowedKeysOnClientKeyPress != null)
            {
                if (Convert.ToString(control.UserData.AllowedKeysOnClientKeyPress) == allowedKeys)
                {
                    //allowedKeys is already set
                    return;
                }
            }

            //set keypress client event
            JavaScript.ClientEvent currentKeyPressEvent = null;

            foreach (var jsEvent in control.ClientEvents)
            {
                if (jsEvent.Event == "keypress" && jsEvent.JavaScript.StartsWith("////AllowedKeysOnClientKeyPress"))
                {
                    currentKeyPressEvent = jsEvent;
                    break;
                }
            }
            //remove key press event on client side if allowedKeys is empty
            if (string.IsNullOrEmpty(allowedKeys))
            {
                if (currentKeyPressEvent != null)
                {
                    control.ClientEvents.Remove(currentKeyPressEvent);
                    control.UserData.AllowedKeysOnClientKeyPress = null;
                    return;
                }
            }

            control.UserData.AllowedKeysOnClientKeyPress = allowedKeys;
            //generate keypress event based on allowedKeys parameter
            string keyPressJS = "////AllowedKeysOnClientKeyPress\r\n if (";
            List<string> keyConditions = new List<string>();
            foreach (var condition in allowedKeys.Split(','))
            {
                if (condition.StartsWith("'"))
                {
                    keyConditions.Add("e._identifier == " + condition);
                }
                else if (condition.Contains(".."))
                {
                    string[] range = condition.Split(new string[] { ".." }, StringSplitOptions.None);
                    keyConditions.Add("(e._keyCode >= " + range[0] + " && e._keyCode <= " + range[1] + ")");
                }
                else
                {
                    keyConditions.Add("e._keyCode == " + condition);
                }
            }
            keyPressJS = keyPressJS + string.Join(" || ", keyConditions);
            keyPressJS = keyPressJS + ") {\r\n}\r\n";
            keyPressJS = keyPressJS + "else { e._preventDefault = true; }";

            //set keypress event to control
            bool addEvent = false;
            if (currentKeyPressEvent == null)
            {
                currentKeyPressEvent = new JavaScript.ClientEvent();
                currentKeyPressEvent.Event = "keypress";
                addEvent = true;
            }
            currentKeyPressEvent.JavaScript = keyPressJS;
            if (addEvent)
            {
                control.ClientEvents.Add(currentKeyPressEvent);
                if (disablePaste)
                {
                    control.Eval(@"this.getContentElement().addListener('paste', TextBox_OnPaste)");
                }
            }
        }

        public static void OLEDrag(this Control objControl)
        {
        }

        public static void ValidateControls(this Control objControl)
        {
        }

        public static void WhatsThisMode(this Control objControl)
        {
        }

        public static void ZOrder(this Control objControl, ZOrderConstants position = ZOrderConstants.BringToFront)
        {
            switch (position)
            {
                case ZOrderConstants.BringToFront:
                    objControl.BringToFront();
                    break;
                case ZOrderConstants.SendToBack:
                    objControl.SendToBack();
                    break;
            }
        }

        //Properties remap
        public static void CurrentX(this Control objControl, Single intValue)
        {
        }

        public static Single CurrentX(this Control objControl)
        {
            return 0;
        }

        public static void CurrentY(this Control objControl, Single intValue)
        {
        }

        public static Single CurrentY(this Control objControl)
        {
            return 0;
        }

        public static void DrawMode(this Control objControl, Int16 intValue)
        {
        }

        public static Int16 DrawMode(this Control objControl)
        {
            return 13;
        }

        public static void DrawStyle(this Control objControl, Int16 intValue)
        {
        }

        public static Int16 DrawStyle(this Control objControl)
        {
            return 0;
        }

        public static void DrawWidth(this Control objControl, Int16 intValue)
        {
        }

        public static Int16 DrawWidth(this Control objControl)
        {
            return 1;
        }

        public static void FillColor(this Control objControl, Color objColor)
        {
            objControl.BackColor = objColor;
        }

        public static Color FillColor(this Control objControl)
        {
            return objControl.BackColor;
        }

        public static void FillStyle(this Control objControl, Int16 intValue)
        {
        }

        public static Int16 FillStyle(this Control objControl)
        {
            return 1;
        }

        public static void FontTransparent(this Control objControl, bool intValue)
        {
        }

        public static bool FontTransparent(this Control objControl)
        {
            return true;
        }

        //IPI
        public static object InvokeMember(this Control ctrl, string name)
        {
            if (ctrl != null)
            {
                MemberInfo[] members = TypeManager.GetMember(ctrl.GetType(), name);
                if (members != null && members.Length > 0)
                {
                    if (members[0].MemberType == MemberTypes.Field)
                    {
                        return ReadField(name, ctrl, members);
                    }
                    else if (members[0].MemberType == MemberTypes.Property)
                    {
                        return ReadProperty(name, ctrl, members);
                    }
                    return null;
                }
            }

            throw new MissingMemberException(ctrl == null ? "<null>" : ctrl.GetType().FullName, name);
        }

        public static bool InvokeMethod(this Control ctrl, string name, object[] paramList)
        {
            ctrl.GetType().GetMethod(name).Invoke(ctrl, paramList);
            return true;
        }

        public static bool InvokeMethodSafe(this Control ctrl, string name, object[] paramList, BindingFlags invokeAttr)
        {
            MethodInfo method = ctrl.GetType().GetMethod(name, invokeAttr);
            if (method != null)
            {
                method.Invoke(ctrl, paramList);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Reads the field value
        /// </summary>
        /// <returns></returns>
        public static object ReadField(string name, Control target, MemberInfo[] members)
        {
            FieldInfo field = members[0] as FieldInfo;
            if (field == null)
                throw new InvalidOperationException(String.Format("Member is not a field", name));

            return field.GetValue(target);
        }

        /// <summary>
        /// Reads the property value
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static object ReadProperty(string name, Control target, MemberInfo[] members)
        {
            PropertyInfo property = null;
            int propIndex = -1;
            // select the property
            for (int i = 0; i < members.Length; i++)
            {
                property = members[i] as PropertyInfo;
                if (property != null)
                {
                    ParameterInfo[] parameters = property.GetGetMethod(true).GetParameters();
                    if (parameters.Length == 0)
                    {
                        propIndex = i;
                        break;
                    }
                }
            }

            if (propIndex < 0)
                throw new InvalidOperationException(String.Format("Mismatching Property Arguments", name));

            return property.GetValue(target, null);
        }

        //CHE get all controls of the control
        public static List<Control> GetAllControls(this Control container)
        {
            List<Control> controls = new List<Control>();
            Control.ControlCollection collection = container.Controls;
            //CHE: in case of FCTabControl iterate through TabPages not Controls collection because they may differ in case of "hidden" tabs
            if (container.GetType() == typeof(FCTabControl))
            {
                FCTabPageCollection tabpages = ((FCTabControl)container).TabPages;
                foreach (Control c in tabpages)
                {
                    GetAllControls(c, ref controls);
                }
            }
            else
            {
                foreach (Control c in container.Controls)
                {
                    GetAllControls(c, ref controls);
                }
            }
            return controls;
        }

        //CHE: get control searched by name
        public static Control GetAllControls(this Control container, string key)
        {
            List<Control> controls = container.GetAllControls();
            foreach (Control c in controls)
            {
                //FC:FINAL:DSE Control name is case insensitive
                if (c.Name.ToUpper() == key.ToUpper())
                {
                    return c;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the font property.
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="objStyle">The style to check.</param>
        /// <returns></returns>
        public static bool GetFontProperty(this Control objControl, FontStyle objStyle)
        {
            return (objControl.Font.Style & objStyle) > 0;
        }

        /// <summary>
        /// Sets the container.
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="onjNewContainer">The new container.</param>
        public static void SetContainer(this Control objControl, Control onjNewContainer)
        {
            // Clear current container
            objControl.Parent.Controls.Remove(objControl);
            // Add control to the new container
            onjNewContainer.Controls.Add(objControl);
        }

        /// <summary>
        /// Gets the size from points.
        /// </summary>
        /// <param name="objControl">The control.</param>
        /// <param name="fltX1">The x1.</param>
        /// <param name="fltY1">The y1.</param>
        /// <param name="fltX2">The x2.</param>
        /// <param name="fltY2">The y2.</param>
        public static void GetSizeFromPoints(this Control objControl, float fltX1, float fltY1, float fltX2, float fltY2)
        {
            objControl.Size = new Size(Convert.ToInt32(Math.Abs(fltX2 - fltX1)), Convert.ToInt32(Math.Abs(fltY2 - fltY1)));
        }

        /// <summary>
        /// Gets the alignment.
        /// </summary>
        /// <param name="enmContentAlignment">The content alignment.</param>
        /// <returns></returns>
        public static HorizontalAlignment GetAlignment(ContentAlignment enmContentAlignment)
        {
            switch (enmContentAlignment)
            {
                // Center alignment
                case ContentAlignment.BottomCenter:
                case ContentAlignment.MiddleCenter:
                case ContentAlignment.TopCenter:
                    return HorizontalAlignment.Center;

                // Left alignment
                case ContentAlignment.BottomLeft:
                case ContentAlignment.MiddleLeft:
                case ContentAlignment.TopLeft:
                    return HorizontalAlignment.Left;
            }

            return HorizontalAlignment.Right;
        }

        /// <summary>
        /// Set the data field of the control
        /// </summary>
        /// <param name="control"></param>
        /// <param name="dataField"></param>
        public static void DataField(this Control control, string dataField)
        {
            ControlDefinitions ctrlDef = ControlDefinitions.GetControlDefinitions(control);
            ctrlDef.DataField = dataField;
        }

        public static string DataField(this Control control)
        {
            ControlDefinitions ctrlDef = ControlDefinitions.GetControlDefinitions(control);
            return ctrlDef.DataField;
        }

        /// <summary>
        /// Returns the name of the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static string GetName(this Control control)
        {
            //JEI:avoid exception if control is null
            if (control == null)
            {
                return string.Empty;
            }
            int lineIdx = control.Name.LastIndexOf("_");
            if (lineIdx == -1)
            {
                return control.Name;
            }
            else
            {
                return control.Name.Substring(0, lineIdx);
            }
        }

        #region Private Methods

        private static void GetAllControls(Control c, ref List<Control> controls)
        {
            //CHE: in VB6 does not exist control for tab page, exclude it from list of controls
            Type cType = c.GetType();
            if (cType != typeof(FCTabPage) && cType != typeof(TabPage))
            {
                controls.Add(c);
            }
            if (c.Controls.Count > 0 && c.GetType().BaseType != typeof(UserControl))
            {
                controls.AddRange(GetAllControls(c));
            }
        }

        /// <summary>
        /// Prints lines on a page.
        /// </summary>
        /// <param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> property values.</param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> property values.</param>
        private static void LineInternal(Control control, float x2, float y2)
        {
            LineInternal(control, true, 0.0f, 0.0f, false, x2, y2, -1, false, false);
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed. </param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed. </param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. You cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        private static void LineInternal(Control control, float x1, float y1, float x2, float y2, int color = -1, bool box = false, bool fill = false)
        {
            LineInternal(control, false, x1, y1, false, x2, y2, color, box, fill);
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the starting coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed.</param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed.</param><param name="relativeEnd">Boolean. If this parameter is set to true, the ending coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. you cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        private static void LineInternal(Control control, bool relativeStart, float x1, float y1, bool relativeEnd, float x2, float y2, int color = -1, bool box = false, bool fill = false)
        {
            //get the instance of the definitions class
            ControlDefinitions objControlDefinitions = ControlDefinitions.GetControlDefinitions(control);

            if (color == -1)
                color = !box ? 0 : Convert.ToInt32(control.ForeColor);
            if (relativeStart)
            {
                x1 += control.CurrentX();
                y1 += control.CurrentY();
            }
            if (relativeEnd)
            {
                x2 += x1;
                y2 += y1;
            }
            control.CurrentX(x2);
            control.CurrentY(y2);
            Graphics graphics = objControlDefinitions.m_printerObject.GetGraphics();
            if (box)
            {
                RectangleF rect = CalculateBoxBounds(control, x1, y1, x2, y2);
                if (fill)
                {
                    using (Brush drawBrush = objControlDefinitions.m_graphicsFactory.CreateDrawBrush(color))
                        graphics.FillRectangle(drawBrush, rect);
                }
                else
                {
                    using (Brush fillBrush = objControlDefinitions.m_graphicsFactory.CreateFillBrush())
                        graphics.FillRectangle(fillBrush, rect);
                    using (Pen drawPen = objControlDefinitions.m_graphicsFactory.CreateDrawPen(color))
                        graphics.DrawRectangle(drawPen, rect.X, rect.Y, rect.Width, rect.Height);
                }
            }
            else
            {
                PointF pt1 = new PointF();
                pt1.X = objControlDefinitions.m_coordinateSpace.ToPixelsX(x1);
                pt1.Y = objControlDefinitions.m_coordinateSpace.ToPixelsY(y1);
                PointF pt2 = new PointF();
                pt2.X = objControlDefinitions.m_coordinateSpace.ToPixelsX(x2);
                pt2.Y = objControlDefinitions.m_coordinateSpace.ToPixelsY(y2);
                if ((double)Math.Abs(pt1.X - pt2.X) < 1.0 && (double)Math.Abs(pt1.Y - pt2.Y) < 1.0)
                    pt2.X = pt2.X + 1f;
                using (Pen drawPen = objControlDefinitions.m_graphicsFactory.CreateDrawPen(color))
                    graphics.DrawLine(drawPen, pt1, pt2);
            }
        }

        private static RectangleF CalculateBoxBounds(Control control, float x1, float y1, float x2, float y2)
        {
            //get the instance of the definitions class
            ControlDefinitions objControlDefinitions = ControlDefinitions.GetControlDefinitions(control);

            x1 = objControlDefinitions.m_coordinateSpace.ToPixelsX(x1);
            y1 = objControlDefinitions.m_coordinateSpace.ToPixelsY(y1);
            x2 = objControlDefinitions.m_coordinateSpace.ToPixelsX(x2);
            y2 = objControlDefinitions.m_coordinateSpace.ToPixelsY(y2);
            RectangleF rectangleF = new Rectangle();
            rectangleF.X = Math.Min(x1, x2);
            rectangleF.Y = Math.Min(y1, y2);
            rectangleF.Width = Math.Abs(x2 - x1);
            rectangleF.Height = Math.Abs(y2 - y1);
            return rectangleF;
        }

        #endregion

        public class StaticVariables
        {
            private JavaScript globalJavaScript;
            private CustomProperties globalCustomProperties;
            private JavaScript.ClientEvent clientEventValueChange_RemoveAlphaCharacters;
            private JavaScript.ClientEvent clientEventAllowOnlyNumbers;
            private JavaScript.ClientEvent clientEventAllowOnlyNumbersAndOneDot;

            public JavaScript GlobalJavaScript
            {
                get
                {
                    if (globalJavaScript == null)
                    {
                        globalJavaScript = new JavaScript();
                    }
                    return globalJavaScript;
                }
            }

            internal JavaScript.ClientEvent ClientEventValueChange_RemoveAlphaCharacters
            {
                get
                {
                    if (clientEventValueChange_RemoveAlphaCharacters == null)
                    {
                        clientEventValueChange_RemoveAlphaCharacters = new JavaScript.ClientEvent();
                        clientEventValueChange_RemoveAlphaCharacters.Event = "changeValue";
                        clientEventValueChange_RemoveAlphaCharacters.JavaScript = "RemoveAlphaCharacters(this, e);";
                    }
                    return clientEventValueChange_RemoveAlphaCharacters;
                }
            }

            internal JavaScript.ClientEvent ClientEventAllowOnlyNumbers
            {
                get
                {
                    if (clientEventAllowOnlyNumbers == null)
                    {
                        clientEventAllowOnlyNumbers = new JavaScript.ClientEvent();
                        clientEventAllowOnlyNumbers.Event = "keypress";
                        clientEventAllowOnlyNumbers.JavaScript = "TextBox_KeyPress_AllowOnlyNumbers(this, e);";
                    }
                    return clientEventAllowOnlyNumbers;
                }
            }

            internal JavaScript.ClientEvent ClientEventAllowOnlyNumbersAndOneDot
            {
                get
                {
                    if (clientEventAllowOnlyNumbersAndOneDot == null)
                    {
                        clientEventAllowOnlyNumbersAndOneDot = new JavaScript.ClientEvent();
                        clientEventAllowOnlyNumbersAndOneDot.Event = "keypress";
                        clientEventAllowOnlyNumbersAndOneDot.JavaScript = "TextBox_KeyPress_AllowNumbersAndOneDot(this, e);";
                    }
                    return clientEventAllowOnlyNumbersAndOneDot;
                }
            }


            public CustomProperties GlobalCustomProperties
            {
                get
                {
                    if (globalCustomProperties == null)
                    {
                        globalCustomProperties = new CustomProperties();
                    }
                    return globalCustomProperties;
                }
            }
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }

    public class ControlDefinitions : ComponentDefinitions
    {
        /// <summary>
        /// Get the instance of the definition class
        /// </summary>
        /// <param name="objLabel"></param>
        /// <returns></returns>
        public static ControlDefinitions GetControlDefinitions(Control objControl)
        {
            ControlDefinitions objControlDefinitions;

            if (ControlExtension.mobjDefinitions.ContainsKey(objControl))
            {
                objControlDefinitions = ControlExtension.mobjDefinitions[objControl];
            }
            else
            {
                objControlDefinitions = new ControlDefinitions();

                //CHE: initialize objects needed in LineInternal
                objControlDefinitions.m_printerObject = new PrinterObject();
                objControlDefinitions.m_graphicsFactory = new GraphicsFactory();
                objControlDefinitions.m_coordinateSpace = new CoordinateSpace(objControlDefinitions.m_printerObject.PrinterModel.MarginBounds, objControlDefinitions.m_printerObject.DpiX, objControlDefinitions.m_printerObject.DpiY);

                objControl.Disposed += objControl_Disposed;
                ControlExtension.mobjDefinitions.Add(objControl, objControlDefinitions);
            }
            return objControlDefinitions;
        }

        static void objControl_Disposed(object sender, EventArgs e)
        {
            ControlExtension.mobjDefinitions.Remove((Control)sender);
        }

        private bool mboolUseMaskColor;
        private Color mobjMaskColor;
        private string dataField;

        public bool UseMaskColor
        {
            get
            {
                return mboolUseMaskColor;
            }
            set
            {
                mboolUseMaskColor = value;
            }
        }

        public Color MaskColor
        {
            get
            {
                return mobjMaskColor;
            }
            set
            {
                mobjMaskColor = value;
            }
        }

        public string DataField
        {
            get { return dataField; }
            set { dataField = value; }
        }

        //CHE: objects needed in LineInternal
        internal CoordinateSpace m_coordinateSpace;
        internal GraphicsFactory m_graphicsFactory;
        internal PrinterObject m_printerObject;

    }
}
