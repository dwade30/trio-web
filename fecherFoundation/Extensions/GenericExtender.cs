using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace fecherFoundation.Extensions
{
    public static class GenericExtender
    {
        /// <summary>
        /// The extenders cache
        /// </summary>
        private static Dictionary<IDisposable, Dictionary<object, object>> mobjExtenders = new Dictionary<IDisposable, Dictionary<object, object>>();

        /// <summary>
        /// Gets the or create extender.
        /// </summary>
        /// <param name="objComponent">The component.</param>
        /// <returns></returns>
        private static Dictionary<object, object> GetOrCreateExtender(Component objComponent)
        {
            // Get global cache
            Dictionary<object, object> objComponentExtender = null;

            // Look for component extender
            if (!mobjExtenders.TryGetValue(objComponent, out objComponentExtender))
            {
                // Create component extender store if not found
                objComponentExtender = new Dictionary<object, object>();
                mobjExtenders.Add(objComponent, objComponentExtender);

                // Register for cleanup
                objComponent.Disposed += objComponent_Disposed;
            }

            return objComponentExtender;
        }

        /// <summary>
        /// Handles the Disposed event of the objComponent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        static void objComponent_Disposed(object sender, EventArgs e)
        {
            Component objComponent = sender as Component;
            if (objComponent == null)
            {
                return;
            }
            // Clean the event registration
            objComponent.Disposed -= objComponent_Disposed;

            // Remove instance extension from cache
            mobjExtenders.Remove(objComponent);
        }


        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="objComponent">The obj component.</param>
        /// <param name="objKey">The key.</param>
        /// <param name="objValue">The value.</param>
        public static void SetValue(Component objComponent, object objKey, object objValue)
        {
            // Set a simple (non-indexed) value in the extender
            SetValue(objComponent, objKey, null, objValue);
        }


        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The key.</param>
        /// <param name="objIndex">Index of the parametrized property.</param>
        /// <param name="objValue">The value.</param>
        public static void SetValue(Component objComponent, object objKey, object objIndex, object objValue)
        {
            // if value already cached
            if (GenericExtender.ValueExistInExtender(objComponent, objKey, objIndex))
            {
                // Erase it to avoid duplications or for override purposes
                GenericExtender.RemoveValueFromExtender(objComponent, objKey, objIndex);
            }

            // Get the extender for the Component (if missing, create new one)
            Dictionary<object, object> objComponentExtender = GetOrCreateExtender(objComponent);

            // Exit method if coudn't get nor create extender for the component
            if (objComponentExtender == null)
            {
                return;
            }

            // Process indexed property
            if (objIndex != null)
            {
                object objExtenderValues = null;
                Dictionary<object, object> objValuesStorage = null;

                // Get property storage
                if (objComponentExtender.TryGetValue(objKey, out objExtenderValues))
                {
                    // Use existing storage collection
                    objValuesStorage = objExtenderValues as Dictionary<object, object>;
                }
                else
                {
                    // Initialize storage
                    objValuesStorage = new Dictionary<object, object>();

                    // Save storage to global cache
                    objComponentExtender.Add(objKey, objValuesStorage);
                }
                // Update property (indexer) value
                objValuesStorage.Add(objIndex, objValue);
            }
            else
            {
                // Set a simple extension value
                objComponentExtender.Add(objKey, objValue);
            }
        }


        /// <summary>
        /// Gets or creates the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The  key.</param>
        /// <param name="blnCreate">if set to <c>true</c> create value if value not found.</param>
        /// <returns></returns>
        public static T GetOrCreateValue<T>(Component objComponent, object objKey, bool blnCreate = true) where T : new()
        {
            return GetOrCreateValue<T>(objComponent, objKey, null, blnCreate);
        }


        /// <summary>
        /// Gets the or create value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The key.</param>
        /// <param name="objIndex">Index of the parameterized property.</param>
        /// <param name="blnCreate">if set to <c>true</c>create value if value not found.</param>
        /// <returns></returns>
        public static T GetOrCreateValue<T>(Component objComponent, object objKey, object objIndex, bool blnCreate = true) where T : new()
        {
            return GetValue<T>(objComponent, objKey, objIndex, blnCreate, delegate { return new T(); });
        }


        /// <summary>
        /// Gets the value or default. Use to retrieve primitive types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The key.</param>
        /// <param name="objIndex">Index of the obj.</param>
        /// <param name="objDefaultValue">The default value.</param>
        /// <returns></returns>
        public static T GetValueOrDefault<T>(Component objComponent, object objKey, object objIndex, T objDefaultValue)
        {
            return GetValue<T>(objComponent, objKey, objIndex, true, delegate { return objDefaultValue; });
        }


        /// <summary>
        /// Gets the value or default. Use to retrieve primitive types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The key.</param>
        /// <param name="objDefaultValue">The default value.</param>
        /// <returns></returns>
        public static T GetValueOrDefault<T>(Component objComponent, object objKey, T objDefaultValue)
        {
            return GetValueOrDefault<T>(objComponent, objKey, null, objDefaultValue);
        }


        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The key.</param>
        /// <param name="objIndex">Index of the obj.</param>
        /// <param name="blnCreate">if set to <c>true</c> [BLN create].</param>
        /// <param name="objCreateValue">The create value.</param>
        /// <returns></returns>
        private static T GetValue<T>(Component objComponent, object objKey, object objIndex, bool blnCreate, Func<T> objCreateValue) 
        {
            // Initialize component extensions store
            Dictionary<object, object> objComponentExtender = GetOrCreateExtender(objComponent);

            // Exit method if couldn't get nor create extender for the component
            if (objComponentExtender == null)
            {
                return default(T);
            }

            object objValue = null;
            object objExtenderValues = null;

            // Initialize a ValuesStorage object
            Dictionary<object, object> objValuesStorage = null;

            // If an indexer was passed to the method
            if (objIndex != null)
            {
                // If value is not found for the specific key
                if (!objComponentExtender.TryGetValue(objKey, out objExtenderValues))
                {
                    if (blnCreate)
                    {
                        // Cast the value to a collection
                        objValuesStorage = new Dictionary<object, object>();

                        // Set value to a new T instance
                        objValue = objCreateValue();

                        // Add the new T instance to the ValueStorage
                        objValuesStorage.Add(objIndex, objValue);

                        // Add new extender to component extender object
                        objComponentExtender.Add(objKey, objValuesStorage);
                    }
                }
                // if there is already an ExtenderValue associated with the objKey
                else
                {
                    // Cast the ExtenderValue to a Dictionary type
                    objValuesStorage = objExtenderValues as Dictionary<object, object>;

                    // Get the value that was requested from the method
                    if (!objValuesStorage.TryGetValue(objIndex, out objValue) && blnCreate)
                    {
                        // Set value to a new T instance
                        objValue = objCreateValue();

                        // Add the new T instance to the ValueStorage
                        objValuesStorage.Add(objIndex, objValue);
                    }
                }
            }
            // if NO indexer was passed to the method
            else
            {
                if (!objComponentExtender.TryGetValue(objKey, out objValue) && blnCreate)
                {
                    // Set value to a new T instance
                    objValue = objCreateValue();

                    // Add the new T instance to the ValueStorage
                    objComponentExtender.Add(objKey, objValue);
                }
            }

            try
            {
                return (T)objValue;
            }
            catch
            {
            }
            return default(T);
        }


        /// <summary>
        /// Removes the value from extender.
        /// </summary>
        /// <param name="objComponent">The component object.</param>
        /// <param name="objKey">The key object.</param>
        /// <param name="objIndex">Index of the value.</param>
        public static void RemoveValueFromExtender(Component objComponent, object objKey, object objIndex = null)
        {
            // Get the extender object
            Dictionary<object, object> objExtender = GetOrCreateExtender(objComponent);

            // if Extender object is NOT NULL
            if (objExtender != null && objKey != null)
            {
                // if Index object is NOT NULL
                if (objIndex != null)
                {
                    object objExtenderValues;

                    // Initialize dictionary object
                    Dictionary<object, object> objValuesStorage = null;

                    // If the value exists with that key
                    if (objExtender.TryGetValue(objKey, out objExtenderValues))
                    {
                        // Cast the variable
                        objValuesStorage = objExtenderValues as Dictionary<object, object>;

                        // Remove the item from the collection
                        objValuesStorage.Remove(objIndex);
                    }
                }
                else
                {
                    // Remove the value from the Extenders object
                    objExtender.Remove(objKey);
                }
            }
        }


        /// <summary>
        /// Values the exist in extender.
        /// </summary>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The key.</param>
        /// <param name="objIndex">The Index.</param>
        /// <returns></returns>
        internal static bool ValueExistInExtender(Component objComponent, object objKey, object objIndex = null)
        {
            // Get the extender object
            Dictionary<object, object> objExtender = GetOrCreateExtender(objComponent);

            // in Componen Extender is NULL
            if (objExtender == null)
            {
                return false;
            }

            // Initialize variable for the Values
            object objExtenderValues = null;

            // Initialize variable for the "ValueExist" flag
            bool blnValueExist = false;

            // if component and key are NOT NULL
            if (objComponent != null && objKey != null)
            {
                // if Component Extender has value associated with the Key
                if (objExtender.TryGetValue(objKey, out objExtenderValues))
                {
                    // If indexed property
                    if (objIndex != null)
                    {
                        // Cast the extender values to a dictionary  
                        Dictionary<object, object> objValueStorage = objExtenderValues as Dictionary<object, object>;

                        // Check if there is a Value in the Storage Container
                        blnValueExist = objValueStorage.ContainsKey(objIndex);
                    }

                    // if simple property
                    else
                    {
                        // Value exists
                        blnValueExist = true;
                    }
                }
            }
            return blnValueExist;
        }

    }

}
