﻿using System;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif

namespace fecherFoundation.Extensions
{
   public static  class TabPagesExtension
    {

        /// <summary>
        /// Removes the tab by index.
        /// </summary>
        /// <param name="objTabPageCollection">The obj tab page collection.</param>
        /// <param name="intIndex">Index of the int.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.NullReferenceException"></exception>
        public static void RemoveTab(this TabControl.TabPageCollection objTabPageCollection, int intIndex)
        {
            // Check for null reference
            if (objTabPageCollection == null)
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }

            // Check if index is valid 
            if (objTabPageCollection.Count > intIndex && intIndex >= 0)
            {
                // Remove the tab with given index
                objTabPageCollection.Remove(objTabPageCollection[intIndex]);
            }
            else
            {
                // If index is not valid throw exception
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Removes the tab.
        /// </summary>
        /// <param name="objTabPageCollection">The obj tab page collection.</param>
        /// <param name="strKey">The STR key.</param>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.NullReferenceException"></exception>
        public static void RemoveTab(this TabControl.TabPageCollection objTabPageCollection, string strKey)
        {

            // Check for null reference
            if (objTabPageCollection == null)
            {
                // If object is null throw exception
                throw new NullReferenceException();
            }
            // Check for empty string
            if (strKey != string.Empty)
            {
                // Remove tab with that key
                objTabPageCollection.RemoveByKey(strKey);
            }
        }

        /// <summary>
        /// Adds the tab.
        /// </summary>
        /// <param name="objTabPageCollection">The tab collection object.</param>
        /// <param name="intIndex">The index of the tab.</param>
        /// <param name="strKey">The key string.</param>
        /// <param name="strCaption">The caption string.</param>
        /// <param name="intImage">The image index number.</param>
        public static void AddTab(this TabControl.TabPageCollection objTabPageCollection, int intIndex = -1, string strKey = "", string strCaption = "", int intImage = -1)
        {

            // If there is an index, we will use the Insert method
            if (intIndex > 0)
            {
                // If there is a key
                if (string.IsNullOrEmpty(strKey))
                {
                    // If there isn't an image index number
                    if (intImage == -1)
                    {
                        // Using the Insert(int index, string text) overload
                        objTabPageCollection.Insert(intIndex, strCaption);
                    }
                    // If there is an image index number
                    else
                    {
                        // Using the Insert(int index, string key, string text, int imageIndex) overload
                        objTabPageCollection.Insert(intIndex, string.Empty, strCaption, intImage);
                    }
                }
                // If we have a key
                else
                {
                    // If there isn't an image index number
                    if (intImage == -1)
                    {
                        // Using the Insert(int index, string key, string text) overload
                        objTabPageCollection.Insert(intIndex, strKey, strCaption);
                    }
                    // If there is an image index number
                    else
                    {
                        // Using the Insert(int index, string key, string text, int imageIndex) overload
                        objTabPageCollection.Insert(intIndex, strKey, strCaption, intImage);
                    }
                }
            }
            // If there isn't an index, we will use the Add method
            else
            {
                // If there isn't have a key
                if (string.IsNullOrEmpty(strKey))
                {
                    // If there isn't an image index number
                    if (intImage == -1)
                    {
                        // Using the Add(string text) overload
                        objTabPageCollection.Add(strCaption);
                    }
                    // If there is an image index number
                    else
                    {
                        // Using the Add(string key, string text, int imageIndex) overload
                        objTabPageCollection.Add(string.Empty, strCaption, intImage);
                    }
                }
                // If there is a key
                else
                {
                    // If there isn't have an image index number
                    if (intImage == -1)
                    {
                        // Using the Add(string key, string text) overload
                        objTabPageCollection.Add(strKey, strCaption);
                    }
                    // If there is an image index number
                    else
                    {
                        // Using the Add(string key, string text, string imageIndex overload
                        objTabPageCollection.Add(strKey, strCaption, intImage);
                    }
                }

            }
        }

        /// <summary>
        /// Adds the tab.
        /// </summary>
        /// <param name="objTabPageCollection">The tab collection object.</param>
        /// <param name="objTabPage">The tab page object.</param>
        public static void AddTab(this TabControl.TabPageCollection objTabPageCollection, TabPage objTabPage)
        {
            // Check for null reference
            if (objTabPageCollection != null)
            {
                // Add the tab page object to the tab page collection
                objTabPageCollection.Add(objTabPage);
            }
        }

    }
}
