﻿#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif
using System.Drawing;

namespace fecherFoundation.Extensions
{
    public static class GraphicsExtension
    {
        public static void DrawLine(this Control objControl, float X1, float Y1, float X2, float Y2, Color objBorderColor, float flBorderWidth)
        {
            Graphics g;
            g = objControl.CreateGraphics();
            Pen pen = new Pen(objBorderColor, flBorderWidth);
            g.DrawLine(pen, X1, Y1, X2, Y2);
        }
    }
}
