﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace fecherFoundation.Extensions
{
    public static class ReflectionExtension
    {

        public static object Invoke(object obj, string memberName, object[] parameters)
        {
            bool invoked = false;
            return Invoke(obj, memberName, parameters, out invoked);
        }

        public static object Invoke(object obj, string memberName, object[] parameters, out bool Invoked)
        {
            Type[] types = null;
            if (parameters != null)
            {
                types = new Type[parameters.Length];
                for (int i = 0; i < parameters.Length; i++)
                {
                    types[i] = parameters[i].GetType();
                }
            }
            return Invoke(obj, memberName, parameters, types, out Invoked);
        }

        public static object Invoke(object obj, string memberName, object[] parameters, Type[] types, out bool Invoked)
        {
            object obj2 = null;
            Invoked = false;
            if (obj == null)
            {
                throw new Exception("Object required");
            }
            
            if (!Invoked)
            {
                MethodInfo method = null;
                if (types != null)
                {
                    method = obj.GetType().GetMethod(memberName, types);
                }
                else
                {
                    method = obj.GetType().GetMethod(memberName);
                }
                if (method != null)
                {
                    obj2 = method.Invoke(obj, parameters);
                    Invoked = true;
                }
                else
                {
                    if (types != null)
                    {
                        method = obj.GetType().GetMethod(memberName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.IgnoreCase, null, types, null);
                    }
                    else
                    {
                        method = obj.GetType().GetMethod(memberName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.IgnoreCase);
                    }
                    if (method != null)
                    {
                        obj2 = method.Invoke(obj, parameters);
                        Invoked = true;
                    }
                    else
                    {
                        method = obj.GetType().GetMethod(memberName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.IgnoreCase);
                        if (method != null)
                        {
                            try
                            {
                                obj2 = method.Invoke(obj, parameters);
                                Invoked = true;
                            }
                            catch
                            {
                            }
                        }
                    }
                }
            }
            if (!Invoked)
            {
                throw new Exception("Object doesn't support this property or method '" + memberName + "'");
            }
            return obj2;
        }

        public static object GetMember(object obj, string propName)
        {
            return GetMember(obj, propName, null, Type.GetType("object"));
        }

        public static object GetMember(object obj, string propName, object[] indexes)
        {
            return GetMember(obj, propName, indexes, Type.GetType("object"));
        }

        public static object GetMember(object obj, string propName, Type expectedType)
        {
            return GetMember(obj, propName, null, expectedType);
        }

        public static object GetMember(object obj, string propName, object[] indexes, Type expectedType)
        {
            object obj2 = null;
            FieldInfo field = null;
            PropertyInfo property = null;
            List<string> list = new List<string>();
            bool memberFound = false;
            if (obj == null)
            {
                throw new Exception("Object required");
            }
            
            if (!memberFound)
            {
                list.Add(propName);
                list.Add(propName.ToLower());
                list.Add("get_" + propName);
                foreach (string str in list)
                {
                    field = obj.GetType().GetField(str);
                    if (field != null)
                    {
                        if ((field.FieldType.IsArray && (indexes != null)) && (indexes.Length > 0))
                        {
                            int[] array = new int[indexes.Length];
                            indexes.CopyTo(array, 0);
                            obj2 = ((Array)field.GetValue(obj)).GetValue(array);
                            memberFound = true;
                        }
                        else
                        {
                            obj2 = field.GetValue(obj);
                            memberFound = true;
                        }
                    }
                    else
                    {
                        try
                        {
                            property = obj.GetType().GetProperty(str);
                        }
                        catch (AmbiguousMatchException)
                        {
                            property = obj.GetType().GetProperty(str, BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.IgnoreCase);
                        }
                        catch (System.Exception exception)
                        {
                            throw exception;
                        }
                        if (property != null)
                        {
                            if ((property.PropertyType.IsArray && (indexes != null)) && (indexes.Length > 0))
                            {
                                obj2 = property.GetValue(obj, indexes);
                                memberFound = true;
                            }
                            else
                            {
                                obj2 = property.GetValue(obj, null);
                                memberFound = true;
                            }
                        }
                    }
                    if (memberFound)
                    {
                        break;
                    }
                }
            }
            if (!memberFound)
            {
                foreach (string str2 in list)
                {
                    obj2 = Invoke(obj, str2, indexes, out memberFound);
                    if (memberFound)
                    {
                        break;
                    }
                }
            }
            if (!memberFound)
            {
                throw new Exception("Object doesn't support this property or method '" + propName + "'");
            }
            return obj2;
        }


        public static void SetMember(object obj, string propName, object value)
        {
            SetMember(obj, propName, null, value, Type.GetType("object"));
        }

        public static void SetMember(object obj, string propName, object[] indexes, object value)
        {
            SetMember(obj, propName, indexes, value, Type.GetType("object"));
        }

        public static void SetMember(object obj, string propName, object value, Type expectedType)
        {
            SetMember(obj, propName, null, value, expectedType);
        }

        public static void SetMember(object obj, string propName, object[] indexes, object value, Type expectedType)
        {
            FieldInfo field = null;
            PropertyInfo property = null;
            List<string> list = new List<string>();
            bool invoked = false;
            if (obj == null)
            {
                throw new Exception("Object required");
            }
            list.Add(propName);
            list.Add(propName.ToLower());
            list.Add("set_" + propName);
            foreach (string str in list)
            {
                field = obj.GetType().GetField(str);
                if (field != null)
                {
                    if ((field.FieldType.IsArray && (indexes != null)) && (indexes.Length > 0))
                    {
                        int[] numArray = new int[indexes.Length];
                        indexes.CopyTo(numArray, 0);
                        Array array = (Array)field.GetValue(obj);
                        if (!array.GetType().GetElementType().Equals(value.GetType()))
                        {
                            value = Convert.ChangeType(value, array.GetType().GetElementType());
                        }
                        array.SetValue(value, numArray);
                        field.SetValue(obj, array);
                        invoked = true;
                    }
                    else
                    {
                        if (!field.FieldType.Equals(value.GetType()))
                        {
                            value = Convert.ChangeType(value, field.FieldType);
                        }
                        field.SetValue(obj, value);
                        invoked = true;
                    }
                }
                else
                {
                    try
                    {
                        property = obj.GetType().GetProperty(str);
                    }
                    catch (AmbiguousMatchException)
                    {
                        property = obj.GetType().GetProperty(str, BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.IgnoreCase);
                    }
                    catch (System.Exception exception)
                    {
                        throw exception;
                    }
                    if (property != null)
                    {
                        if ((property.PropertyType.IsArray && (indexes != null)) && (indexes.Length > 0))
                        {
                            if (!property.PropertyType.GetElementType().Equals(value.GetType()))
                            {
                                value = Convert.ChangeType(value, property.PropertyType.GetElementType());
                            }
                            property.SetValue(obj, value, indexes);
                            invoked = true;
                        }
                        else
                        {
                            if (!property.PropertyType.Equals(value.GetType()))
                            {
                                value = Convert.ChangeType(value, property.PropertyType);
                            }
                            property.SetValue(obj, value, null);
                            invoked = true;
                        }
                    }
                }
                if (invoked)
                {
                    break;
                }
            }
            if (!invoked)
            {
                object[] destinationArray = new object[indexes.Length + 1];
                Array.Copy(indexes, 0, destinationArray, 0, indexes.Length);
                destinationArray[destinationArray.Length - 1] = value;
                foreach (string str2 in list)
                {
                    Invoke(obj, str2, destinationArray, out invoked);
                    if (invoked)
                    {
                        break;
                    }
                }
            }
            if (!invoked)
            {
                throw new Exception("Object doesn't support this property or method '" + propName + "'");
            }
        }
    }
}
