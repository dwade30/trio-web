﻿#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif
using System;
using System.Reflection;

namespace fecherFoundation.Extensions
{
    public class ContainerExtension
    {
        public static int GetControlIndex(object ControlObject)
        {
            string str;
            return GetControlIndex(ControlObject, out str);
        }

        public static int GetControlIndex(object ControlObject, out string ArrayName)
        {
            Form ownerForm = null;
            object obj2 = null;
            ArrayName = string.Empty;
            if (ControlObject == null)
            {
                throw new Exception("Object Variable or With block variable not set");
            }
            ownerForm = GetOwnerForm(ControlObject);
            if (ownerForm == null)
            {
                throw new Exception("The owner form of the control couldn't be found");
            }
            foreach (FieldInfo info in ownerForm.GetType().GetFields())
            {
                if (info.FieldType.IsArray)
                {
                    Array array = (Array)info.GetValue(ownerForm);
                    if (array.Rank <= 1)
                    {
                        for (int i = array.GetLowerBound(0); i <= array.GetUpperBound(0); i++)
                        {
                            obj2 = array.GetValue(i);
                            if (obj2 != null)
                            {
                                if (!obj2.GetType().Equals(ControlObject.GetType()))
                                {
                                    break;
                                }
                                if (obj2.Equals(ControlObject))
                                {
                                    ArrayName = info.Name;
                                    return i;
                                }
                            }
                        }
                    }
                }
            }
            throw new Exception("Object not an array");
        }

        public static Form GetOwnerForm(object ControlObject)
        {
            Form form = null;
            ToolStripItem ownerItem = null;
            if (ControlObject is Control)
            {
                return ((Control)ControlObject).FindForm();
            }
            if (!(ControlObject is ToolStripItem))
            {
                return form;
            }
            ownerItem = (ToolStripItem)ControlObject;
            while (ownerItem.OwnerItem != null)
            {
                ownerItem = ownerItem.OwnerItem;
            }
            return ownerItem.Owner.FindForm();
        }

        public static void LoadControl(Form FormContainer, string ControlName, int Index)
        {
            
        }

 

 


 

 


 

    }
}
