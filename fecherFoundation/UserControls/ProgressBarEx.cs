using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Common.Resources;
using Gizmox.WebGUI.VisualBasic.Compatibility.Extensions;
#endif

namespace fecherFoundation
{
	/// <summary>
	/// A replacement for the default ProgressBar control.
	/// </summary>
	[DefaultEvent("ValueChanged")]
	public class ProgressBarEx : System.Windows.Forms.UserControl
	{

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		public System.ComponentModel.Container components = null;

        /// <summary>
        /// Scrolling member set to default value
        /// </summary>
        public ScrollingConstants menumScrolling = ScrollingConstants.ScrollingStandard;

        /// <summary>
        /// Gets/Sets scrolling property
        /// </summary>
        [DefaultValue(0)]
        [Category("Behavior")]
        [Description("Gets/Sets scrolling property")]
        public ScrollingConstants Scrolling
        {
            get { return menumScrolling; }
            set { menumScrolling = value; }
        }

        public AppearanceConstants Appearance { get; set; }

        /// <summary>
		/// Create the control and initialize it.
		/// </summary>
		public ProgressBarEx()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			this.SetStyle(ControlStyles.DoubleBuffer, true);
			this.SetStyle(ControlStyles.ResizeRedraw, true);
			this.SetStyle(ControlStyles.Selectable, true);
			this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			this.SetStyle(ControlStyles.UserPaint, true);
			this.BackColor = Color.Transparent;
			this.Padding = new Padding(0);
			this.Margin = new Padding(0);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		public void InitializeComponent()
		{
			// 
			// ProgressBar
			// 
			this.Name = "ProgressBar";
			this.Size = new System.Drawing.Size(264, 32);
			this.Paint += new PaintEventHandler(ProgressBar_Paint);
		}

		/// <summary>
		/// Holds value that is displayed on the progress bar.
		/// </summary>
		public int mValue = 0;
		
        /// <summary>
		/// The value that is displayed on the progress bar.
		/// </summary>
		[Category("Value"), DefaultValue(0), Description("The value that is displayed on the progress bar.")]
		public int Value {
			get { return mValue; }
			set {
                // Check if value is into range
				if (value > MaxValue || value < MinValue)
				{
					return;
				}
				
                // Set value
                mValue = value;			
               
				
                // Raise event
                if (ValueChanged != null)
				{
                    ValueChanged(this, new System.EventArgs());
				}

			    // Call invalidate
                this.Invalidate();
			}
		}

		/// <summary>
		/// Holds the maximum value for the Value property.
		/// </summary>
		public int mMaxValue = 100;
		/// <summary>
		/// The maximum value for the Value property.
		/// </summary>
		[Category("Value"), DefaultValue(100), Description("The maximum value for the Value property.")]
		public int MaxValue {
			get { return mMaxValue; }
			set {
				mMaxValue = value;
				
                // Check if value is greater than max value
                if (value > MaxValue)
				{
					Value = MaxValue;
				}
                
                // Raise event
                if (MaxChanged != null)
				{
                    MaxChanged(this, EventArgs.Empty);
				}

                // Call invalidate
				this.Invalidate();
			}
		}

		/// <summary>
		/// Holds the minimum value for the Value property.
		/// </summary>
		public int mMinValue = 0;
		/// <summary>
		/// The minimum value for the Value property.
		/// </summary>
		[Category("Value"), DefaultValue(0), Description("The minimum value for the Value property.")]
		public int MinValue {
			get { return mMinValue; }
			set {
               
				mMinValue = value;
				
                // Check if value is less than min
                if (value < MinValue)
				{
					Value = MinValue;
				}                

                // Raise event
                if (MaxChanged != null)
				{
                    MaxChanged(this, EventArgs.Empty);
				}

                // Call invalidate
				this.Invalidate();
			}
		}

		/// <summary>
		/// Draws the background.
		/// </summary>
		/// <param name="g">The Graphics object.</param>
		public void DrawBackground(Graphics g)
		{
            // Rectangle object
			Rectangle r = this.ClientRectangle;

            // Draw a rectangle
			g.FillRectangle(new SolidBrush(this.BackColor), r);
		}

		/// <summary>
		/// Draws the bar.
		/// </summary>
		/// <param name="g">The Graphics object.</param>
        public void DrawBar(Graphics objGraphics)
		{
            // Declare variables
            int barWidth;
            int blocksWidth;
            int blocksCount;

            // If scrolling is set to standard
            if (this.Scrolling == ScrollingConstants.ScrollingStandard)
            {
                // Calculating bar width and percent to draw
                barWidth = this.Height / 2;
                blocksWidth = (int)(Value * 1f / (MaxValue - MinValue) * this.Width);
                blocksCount = blocksWidth / barWidth;
                blocksWidth = blocksWidth - (blocksCount * 3) / 2;
                blocksCount = blocksWidth / barWidth;
                for (int i = 0; i < blocksCount; i++)
                {
                    Rectangle r = new Rectangle(i * barWidth + i * 3, 0, barWidth, this.Height);
                    objGraphics.FillRectangle(new SolidBrush(ForeColor), r);
                }
            }
            else
            {
                float percent = (float)(Value - MinValue) / (float)(MaxValue - MinValue);
                
                Rectangle rect = this.ClientRectangle;

                // Calculate area for drawing the progress.
                rect.Width = (int)((float)rect.Width * percent);

                // Draw the progress meter.
                objGraphics.FillRectangle(new SolidBrush(ForeColor), rect);
            }
		}

		/// <summary>
		/// Handles the Paint event of the ProgressBar control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="PaintEventArgs"/> instance containing the event data.</param>
		public void ProgressBar_Paint(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
			
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
			
            // Draw the background
            DrawBackground(e.Graphics);
			
            // Draw the bars
            DrawBar(e.Graphics);
		}

		
		/// <summary>
		/// When the Value property is changed.
		/// </summary>
		public event EventHandler ValueChanged;

		
		/// <summary>
		/// When the MinValue property is changed.
		/// </summary>
		public event EventHandler MinChanged;

		
		/// <summary>
		/// When the MaxValue property is changed.
		/// </summary>
		public event EventHandler MaxChanged;

	}

    /// <summary>
    /// Enum used for scrolling property
    /// </summary>
    public enum ScrollingConstants
    {
        ScrollingStandard = 0,
        ScrollingSmooth = 1
    }

    public enum AppearanceConstants
    {
        ccFlat = 0,
        cc3D = 1
    }
}
