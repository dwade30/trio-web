﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Reflection.Emit;


namespace fecherFoundation
{
    /// <summary>
    /// Updown class
    /// </summary>
    [DefaultEvent("Change")]
    [DefaultProperty("Value")]
    [ToolboxBitmap(typeof(UpDown), "updownicon")]
    public partial class UpDown : UserControl
    {
		#region Fields (3) 

        /// <summary>
        /// Max value member with default value 10
        /// </summary>
        private long mlngMax = 10;

        /// <summary>
        /// Min value member with default value 0
        /// </summary>
        private long mlngMin = 0;

        /// <summary>
        /// Value member with default value 0
        /// </summary>
        private long mlngValue = 0;

        /// <summary>
        /// Wrap member with default value false
        /// </summary>
        private bool mbnlWrap = false;

        /// <summary>
        /// Increment step for UpDown with default value 1
        /// </summary>
        private int mintIncrement = 1;

        /// <summary>
        /// Set alignment for UpDown with default value to right side
        /// </summary>
        private BuddyAlignments menumAlignment= BuddyAlignments.Right;
    
        /// <summary>
        /// Set orientation on UpDown control with default value to vertical
        /// </summary>
        private UpDownOrientation menumOrientation = UpDownOrientation.Vertiacl;

        /// <summary>
        /// AutoBuddy member with default value false
        /// </summary>
        private bool mblnAutoBuddy = false;

        /// <summary>
        /// BuddyControl member
        /// </summary>
        private Control mobjBuddyControl;

        /// <summary>
        /// BuddyProperty member with default value null
        /// </summary>
        private object mobjBuddyProperty = null;

        /// <summary>
        /// Acceleration member with default value 1
        /// </summary>
        private int mintAcceleration = 1;

        // Indicate if orientation is vertical or horizontal
        private bool mblnVertical = true;

        // Indicate if btnUp is pressed or btnDown
        private bool mbtnPressUp = false;

        /// <summary>
        /// Get/Set the rate at which the value (position) changes
        /// </summary>
        /// [Category("Behavior")]
        [Description("Get/Set the rate at which the value (position) changes.")]
        [DefaultValue(1)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)] 
        public int Acceleration
        {
            get { return mintAcceleration; }
            set { mintAcceleration = value; }
        }

        /// <summary>
        /// Get/Set which property to use to synchronize with Buddy Control
        /// </summary>
        /// [Category("Behavior")]
        [Description("Get/Set which property to use to synchronize with buddy control.")]
        [DefaultValue(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]      
        public object BuddyPoperty
        {
            get { return mobjBuddyProperty; }
            set { mobjBuddyProperty = value; }
        }

        /// <summary>
        /// Get/Set the auto-buddy flag, to automatically select the buddy control
        /// </summary>
        /// [Category("Behavior")]
        [Description("Get/Set the auto-buddy flag, to automatically select the buddy control.")]
        [DefaultValue(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool AutoBuddy
        {
            get { return mblnAutoBuddy; }
            set
            {
                if (value)
                {
                    // Get auto buddy control
                    mobjBuddyControl=AutoBuddyControl();

                    // Set buddy control for the UpDown control
                    this.BuddyControl = mobjBuddyControl;
                }

                mblnAutoBuddy = value;
            }
        }

        /// <summary>
        /// Get or set control orientation 
        /// </summary>
        /// [Category("Behavior")]
        [Description("Get/Set the orientation of the UpDown control.")]
        [DefaultValue(1)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public UpDownOrientation Orientaion
        {
            get { return menumOrientation; }
            set
            {
                // Check if orientation is horizontal set flag to false
                if (value == UpDownOrientation.Horizontal)
                {
                    // Control has horizontal orientation
                    mblnVertical = false;
                }
                else
                {
                    // Control has vertical orientation
                    mblnVertical = true;
                    
                }

                menumOrientation = value;

                // Resize control to draw correct orientation
                UpDown_Resize(this, EventArgs.Empty);

            }
        }

        /// <summary>
        /// Gets or sets the alignment of the UpDown control with its buddy control
        /// </summary>
        [Category("Behavior")]
        [Description("Get/Set the alignment of the UpDown control with its buddy control.")]
        [DefaultValue(1)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public BuddyAlignments Alignment
        {
            get { return menumAlignment; }
            set { menumAlignment = value; }
        }

        /// <summary>
        /// Get or set the amount by which the position changes on each click.
        /// </summary>
        [Category("Behavior")]
        [Description("Get/Set the amount by which the position changes on each click.")]
        [DefaultValue(1)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int Increment
        {
            get { return mintIncrement; }
            set { mintIncrement = value; }
        }

        /// <summary>
        /// Gets/Sets the control used as the buddy control.
        /// </summary>
        [Category("Behavior")]
        [Description("Gets/Sets the control used as the buddy control.")]
        //[DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public object BuddyControl
        {
            get { return mobjBuddyControl.Name; }
            set
            {
                Control objControl = value as Control;

                if (objControl != null)
                {
                    mobjBuddyControl = objControl;

                    SetBuddyControl(objControl);
                }
            }
        }

        /// <summary>
        /// Sets the buddy control
        /// </summary>
        /// <param name="value">Control object</param>
        private void SetBuddyControl(Control value)
        {
            //Suspend redrawing
            this.SuspendLayout();

            //Set top position like buddy control top position
            this.Top = value.Top;

            //Set the height same like buddy control height
            this.Height = value.Height;
            
            //Set left position to left plus width of the buddy control
            this.Left = value.Left + value.Width + 1;

            // Check if alignment is set to left side
            if (menumAlignment == BuddyAlignments.Left)
            {
                // Set location next to buddy control from left side
                this.Location = new Point(mobjBuddyControl.Location.X - this.Size.Width, this.Location.Y);
            }
            else
            {
                // Set location next to buddy control from right side
                this.Location = new Point(mobjBuddyControl.Location.X + mobjBuddyControl.Width, this.Location.Y);
            }

            //Resume redrawing
            this.ResumeLayout();
        }

        /// <summary>
        /// SyncBuddy member
        /// </summary>
        private bool mblnSyncBuddy = false;

        /// <summary>
        /// Gets/Sets a value that determines whether the UpDown control synchronizes the Value property with a property in the buddy control.
        /// </summary>
        [Category("Behavior")]
        [Description("Gets/Sets a value that determines whether the UpDown control synchronizes the Value property with a property in the buddy control.")]
        [DefaultValue(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool SyncBuddy
        {
            get { return mblnSyncBuddy; }
            set 
            {
                mblnSyncBuddy = value;
                
                //if vlaue is true
                if (value)
                {
                    //Register valuechanged event 
                    this.ValueChanged += new EventHandler(UpDown_ValueChanged);
                }
                else
                {
                    // Unregister valuechanged event 
                    this.ValueChanged -= new EventHandler(UpDown_ValueChanged);
                }
            }
        }

        /// <summary>
        /// Handles value changed event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event data</param>
        private void UpDown_ValueChanged(object sender, EventArgs e)
        {
            //Null reference checking
            if (this.mobjBuddyControl != null)
            {
                // Synchronize default buddy property
                SetBuddyProperty();

                
            }
        }

        /// <summary>
        /// Synchronize buddy property
        /// </summary>
        public void SetBuddyProperty()
        {

            try
            {
                // Get name of property used for synchronies buddy control
                string strValue = this.BuddyPoperty.ToString();

                // Get type of the control used as buddy control
                Type objBuddyControlType = mobjBuddyControl.GetType();

                // Get property info for declared buddy property
                PropertyInfo objPropertyInfo = objBuddyControlType.GetProperty(strValue);

                // Get type of property
                Type objPropertyType = objPropertyInfo.PropertyType;

                // Check if property is color
                if (objPropertyType.Name == "Color")
                {
                    // Create color from current value of UpDown control
                    Color objColor = ColorTranslator.FromWin32(Convert.ToInt32(this.Value));

                    // Set color in the property
                    mobjBuddyControl.GetType().GetProperty(strValue).SetValue(mobjBuddyControl, objColor, null);
                }
                else
                {
                    // Set value in the property
                    mobjBuddyControl.GetType().GetProperty(strValue).SetValue(mobjBuddyControl, Convert.ChangeType(this.Value, objPropertyType), null);
                }
            }
            catch
            {
            }
        }

		#endregion Fields 

		#region Constructors (1) 

        /// <summary>
        /// Initializes a new instance of the <see cref="UpDown"/> class.
        /// </summary>
        public UpDown()
        {
            InitializeComponent();

            this.ValueChanged += new EventHandler(UpDown_ValueChanged);
        }

		#endregion Constructors 

		#region Properties (3) 

        /// <summary>
        /// Get or set whether the value (position) will wrap from max to min (or min to max)
        /// </summary>
        [DefaultValue(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Description("Get/Set whether the value will wrap from max to min")]
        [Category("Behavior")]
        public bool Wrap
        {
            get { return mbnlWrap; }
            set { mbnlWrap = value; }
        }

        /// <summary>
        /// Get/Set the upper bound of the scroll range
        /// </summary>
        [DefaultValue(10)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Description("Get/Set the upper bound of the scroll range")]
        [Category("Behavior")]
        public long Max
        {
            get { return mlngMax; }
            set 
            { 
                mlngMax = value;

                //Min was changed than change value in control
                if (value > Min)
                {
                    this.Value = Min;
                }
            }
        }

        /// <summary>
        /// Get/Set the lower bound of the scroll range
        /// </summary>
        [DefaultValue(0)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Description("Get/Set the lower bound of the scroll range")]
        [Category("Behavior")]
        public long Min
        {
            get { return mlngMin; }
            set 
            { 
                mlngMin = value;

                //If value is grater than max or value in control is less than min need to change value in control
                if (value > Max)
                {
                    this.Value = Max;
                } 
                else if (this.Value < value)
                {
                    this.Value = value;
                }
             
            }
        }

        /// <summary>
        /// Get/Set the current position in the scroll range
        /// </summary>
        [DefaultValue(0)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Description("Get/Set the current position in the scroll range")]
        [Category("Behavior")]
        public long Value
        {
            get { return mlngValue; }
            set 
            {
                    mlngValue = value;

                    //Raise value changed event
                    OnValueChanged(EventArgs.Empty);
                
            }
        }

		#endregion Properties 

		#region Delegates and Events (3) 

		// Events (3) 

        /// <summary>
        /// Chnage event
        /// </summary>
        [Category("Behavior")]
        public event EventHandler Change;

        /// <summary>
        /// DownClick event
        /// </summary>
        [Category("Behavior")]
        public event EventHandler DownClick;

        /// <summary>
        /// UpClick event
        /// </summary>
        [Category("Behavior")]
        public event EventHandler UpClick;

        /// <summary>
        /// ValueChanged event used for syncbuddy property
        /// </summary>
        private event EventHandler ValueChanged;

        /// <summary>
        /// Raises valuechanged event
        /// </summary>
        /// <param name="e"></param>
        private void OnValueChanged(EventArgs e)
        {
            //null reference checking
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }

		#endregion Delegates and Events 

		#region Methods (9) 

		// Protected Methods (3) 

        /// <summary>
        /// Raises change event
        /// </summary>
        /// <param name="e">Event data</param>
        protected void OnChange(EventArgs e)
        {
            //null reference checking
            if (Change != null)
            {
                Change(this, e);
            }
        }

        /// <summary>
        /// Raises downclick event
        /// </summary>
        /// <param name="e">Event data</param>
        protected void OnDownClick(EventArgs e)
        {
            //null reference checking
            if (DownClick != null)
            {
                DownClick(this, e);
            }
        }

        /// <summary>
        /// Raises upclick event
        /// </summary>
        /// <param name="e">Event data</param>
        protected void OnUpClick(EventArgs e)
        {
            //null reference checking
            if (UpClick != null)
            {
                UpClick(this, e);
            }
        }
		// Private Methods (6) 

        /// <summary>
        /// Handles button up click
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event data</param>
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (Wrap)
            {
                //check if decrementing value is greater than min value
                if (this.Value - this.Increment >= this.Min)
                {
                    //decrement value
                    this.Value -= this.Increment;
                }
                else
                {
                    // Change value to max
                    this.Value = Max;
                }
            }
            else
            {
                //check if decrementing value is greater than min value
                if (this.Value - this.Increment >= this.Min)
                {
                    //decrement value
                    this.Value -= this.Increment;
                }
            }
            //raise change event
            OnChange(e);

            //raise downclick event
            OnDownClick(e);
        }

        /// <summary>
        /// Handles button down paint event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event data</param>
        private void btnDown_Paint(object sender, PaintEventArgs e)
        {
            //create rectangle object
            Rectangle objRC = new Rectangle();

            //set width and height of the rectangle
            objRC.Width = objRC.Height = btnUp.ClientRectangle.Width > btnUp.ClientRectangle.Height ? btnUp.ClientRectangle.Height - 5 : btnUp.ClientRectangle.Width - 5;
            
            //set rectangle location
            objRC.Location = new Point(btnUp.ClientRectangle.Width / 2 - objRC.Width / 2, btnUp.ClientRectangle.Height / 2 - objRC.Height / 2);

            // Check if orientation is vertical draw down arrow
            if (mblnVertical)
            {
                //draw the down arrow
                DrawTriangle(e.Graphics, objRC, ArrowDirection.Down);
            }
            else
            {
                //draw the left arrow
                DrawTriangle(e.Graphics, objRC, ArrowDirection.Left);
            }
        }

        /// <summary>
        /// Handles button up click event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event data</param>
        private void btnUp_Click(object sender, EventArgs e)
        {
            // Check if wrap is true to  wrap from max to min
            if (Wrap)
            {
                // Check if incrementing value is less than max value
                if (this.Value + this.Increment <= this.Max)
                {
                    //increment value
                    this.Value += this.Increment;
                }
                else
                {
                    // Change value to min
                    this.Value = Min;
                }
            }
            else
            {
                // Check if incrementing value is less than max value
                if (this.Value + this.Increment <= this.Max)
                {
                    // Increment value
                    this.Value += this.Increment;
                }
            }
            //raise change event
            OnChange(e);

            //raise up click event
            OnUpClick(e);
        }

        /// <summary>
        /// Handles button down paint event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event data</param>
        private void btnUp_Paint(object sender, PaintEventArgs e)
        {
            //create rectangle object
            Rectangle objRC = new Rectangle();

            //set width and height of the rectangle
            objRC.Width = objRC.Height = btnUp.ClientRectangle.Width > btnUp.ClientRectangle.Height ? btnUp.ClientRectangle.Height - 5: btnUp.ClientRectangle.Width - 5;

            //set rectangle location
            objRC.Location = new Point(btnUp.ClientRectangle.Width / 2 - objRC.Width / 2, btnUp.ClientRectangle.Height / 2 - objRC.Height / 2);

            // Check if orientation is vertical to draw correct arrow
            if (mblnVertical)
            {
                // Vertical orientation draw up arrow
                DrawTriangle(e.Graphics, objRC, ArrowDirection.Up);
            }
            else
            {
                // Horizontal orientation draw right arrow
                DrawTriangle(e.Graphics, objRC, ArrowDirection.Right);
            }
        }

        /// <summary>
        /// function used to draw an arrow
        /// </summary>
        /// <param name="g">Graphics object</param>
        /// <param name="rect">Rectabgle object</param>
        /// <param name="direction">Direction value</param>
        private void DrawTriangle(Graphics g, Rectangle rect, ArrowDirection direction)
        {
            //get half width from the rectangle
            int halfWidth = rect.Width / 2;

            //get half height from the rectangle
            int halfHeight = rect.Height / 2;
            
            // declare 3 points object used for arrow triangle
            Point p0 = Point.Empty;
            Point p1 = Point.Empty;
            Point p2 = Point.Empty;

            //check the direction
            switch (direction)
            {
                // if it is up
                case ArrowDirection.Up:
                    //calculate the positions for the points
                    p0 = new Point(rect.Left + halfWidth, rect.Top);
                    p1 = new Point(rect.Left, rect.Bottom);
                    p2 = new Point(rect.Right, rect.Bottom);
                    break;
                //if it is down
                case ArrowDirection.Down:
                    //calculate the positions for the points
                    p0 = new Point(rect.Left + halfWidth, rect.Bottom);
                    p1 = new Point(rect.Left, rect.Top);
                    p2 = new Point(rect.Right, rect.Top);
                    break;
                // if it is left
                case ArrowDirection.Left:
                    //calculate the positions for the points
                    p0 = new Point(rect.Left, rect.Top + halfWidth);
                    p1 = new Point(rect.Right, rect.Top);
                    p2 = new Point(rect.Right, rect.Bottom);
                    break;
                // if it is right
                case ArrowDirection.Right:
                    //calculate the positions for the points
                    p0 = new Point(rect.Right, rect.Top + halfWidth);
                    p1 = new Point(rect.Left, rect.Top);
                    p2 = new Point(rect.Left, rect.Bottom);
                    break;
            }

            //draw a polygon using the points
            g.FillPolygon(Brushes.Black, new Point[] { p0, p1, p2 });
        }

        /// <summary>
        /// Handles user control resize event
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event data</param>
        private void UpDown_Resize(object sender, EventArgs e)
        {
            // Set control 
            SetUpDownOrientaion();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetUpDownOrientaion()
        {
         
            if (mblnVertical)
            {
                // Set control width to 17 pixel
                this.Width = 17;

                //set button up height to half height of the user control
                btnUp.Height = this.Height / 2;

                //set button down height to half height of the user control
                btnDown.Height = this.Height / 2;

                //set button width fixed to 17 pixels i.e. 255 twips
                btnUp.Width = 17;

                //set button width fixed to 17 pixels i.e. 255 twips
                btnDown.Width = 17;

                // Set dock style to top for btnUp for vertical orientation
                btnUp.Dock = DockStyle.Top;

                // Set dock style to bottom for btnDown for vertical orientation
                btnDown.Dock = DockStyle.Bottom;
            }
            else
            {
                // Set control width to 17 pixel
                this.Width = 17;

                // Set button up height to height of the user control
                btnUp.Height = this.Height;

                // Set button down height to height of the user control
                btnDown.Height = this.Height ;

                // Set button width fixed to half width of the user control
                btnUp.Width = this.Width / 2;

                // Set button width fixed to half width of the user control
                btnDown.Width = this.Width / 2;

                // Set dock style to left for btnUp for horizontal orientation
                btnUp.Dock = DockStyle.Right;

                // Set dock style to right for btnDown for horizontal orientation
                btnDown.Dock = DockStyle.Left;
            }
        }

        /// <summary>
        /// Find control as auto buddy control.
        /// </summary>
        /// <returns>Return control with the closest tab index to the UpDown control tab index</returns>
        private Control AutoBuddyControl()
        {
            // Get all controls on form where is UpDown control
            Control.ControlCollection objControlCollection = this.Parent.Controls;
            
            // Create sorted dictionary to store all controls with tab index less than tab index on the UpDown control
            SortedDictionary<int, Control> objDict = new SortedDictionary<int, Control>();

            // Go though control collection
            foreach (Control objControl in objControlCollection)
            {
                // Find all with tab index less than this tab index
                if (objControl.GetType().Name != "UpDown" && objControl.TabIndex < this.TabIndex)
                {
                    // Add control to sorted dictionary
                    objDict.Add(objControl.TabIndex, objControl);
                }
            }

            // Count number of find controls
            int intNumbControls = objDict.Count();

            // Check if list is empty
            if (intNumbControls != 0)
            {
                // Return control with the closest tab index to the UpDown control tab index
                return objDict.Values.Last();
            }
            else
            {
                // Return control with the closest tab index to the UpDown control tab index
                return null;
            }
        }

		#endregion Methods 

        public event MouseEventHandler MouseDownEx
        {
            add
            {
                btnDown.MouseDown += value;
                btnUp.MouseDown += value;
            }
            remove
            {
                btnDown.MouseDown -= value;
                btnUp.MouseDown -= value;
            }
        }

        public event MouseEventHandler MouseUpEx
        {
            add
            {
                btnUp.MouseUp += value;
                btnDown.MouseUp += value;
            }
            remove
            {
                btnDown.MouseUp -= value;
                btnUp.MouseUp -= value;
            }
        }

        /// <summary>
        /// Handles the Tick event of the timerPress control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void timerPress_Tick(object sender, EventArgs e)
        {
            // Check which button was pressed
            if (mbtnPressUp)
            {
                // btnUp pressed increment value
                btnUp.PerformClick();
            }
            else
            {
                // btnDown pressed decrement value
                btnDown.PerformClick();
            }
        }

        /// <summary>
        /// Handles the MouseUp event of the btnUp control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void btnUp_MouseUp(object sender, MouseEventArgs e)
        {
            // Stop timer when button was released
            timerPress.Enabled = false;
        }

        /// <summary>
        /// Handles the MouseDown event of the btnUp control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void btnUp_MouseDown(object sender, MouseEventArgs e)
        {
            // Check if property Acceleration has value larger than 0
            if (mintAcceleration > 0)
            {
                // btnUp press set flag to this button
                mbtnPressUp = true;

                // Start timer
                timerPress.Enabled = true;
            }
        }

        /// <summary>
        /// Handles the MouseDown event of the btnDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void btnDown_MouseDown(object sender, MouseEventArgs e)
        {
            // Check if property Acceleration has value larger than 0
            if (mintAcceleration > 0)
            {
                // btnDown press set flag to this button
                mbtnPressUp = false;

                // Start timer
                timerPress.Enabled = true;
            }
        }

        /// <summary>
        /// Handles the MouseUp event of the btnDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void btnDown_MouseUp(object sender, MouseEventArgs e)
        {
            // Stop timer when button was released
            timerPress.Enabled = false;
        }
   }

    /// <summary>
    /// Enum used for arrow direction
    /// </summary>
    public enum ArrowDirection
    {
        Up = 1,
        Down = 2,
        Left =3,
        Right= 4
    }

    /// <summary>
    /// BuddyProperties enum
    /// </summary>
    public enum BuddyProperties
    {
        Text = 0
    }

    public enum UpDownOrientation
    {
        Horizontal=0,
        Vertiacl=1
    }

    /// <summary>
    /// BuddyAlignments enum
    /// </summary>
    public enum BuddyAlignments
    {
        Left = 0,
        Right = 1
    }
}
