﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    public class Sys
    {
        // we need a default client session in case code that access globals is invoke at design time
        private static Dictionary<string, object> ClientSession = new Dictionary<string, object>();

        /// <summary>
        /// Retrieves the singleton instance of the specified class from the
        /// session's repository.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static object GetInstance(Type T)
        {
            Debug.Assert(T != null);

            object value = null;
            string key = T.Assembly.GetName().Name + "." + T.FullName;
            dynamic session = Application.Session;

            if (session != null)
            {
                value = session[key];
            }
            else
            {
                // if there is no context, we use a static repository to avoid NullObject exceptions
                // when in design mode or shutting down the server.
                if (ClientSession.TryGetValue(key, out value))
                    return value;
            }

            if (value == null && !T.IsAbstract)
            {
                try
                {
                    // we can only use the default constructor
                    ConstructorInfo ctor = T.GetConstructor(new Type[0]);
                    if (ctor != null)
                    {
                        value = ctor.Invoke(null);
                        SaveInstance(value);
                    }
                }
                catch { }
            }

            return value;
        }

        public static void ClearInstance(object value)
        {
            Type t = value.GetType();
            string key = t.Assembly.GetName().Name + "." + t.FullName;
            dynamic session = Application.Session;

            if (session != null)
            {
                session[key] = null;
            }
            else
            {
                // if there is no session, we use a static repository to avoid NullObject exceptions
                // when in design mode or shutting down the server.
                ClientSession[key] = null;
            }
        }

        public static void ClearInstanceOfType(Type T)
        {
            string key = T.Assembly.GetName().Name + "." + T.FullName;
            dynamic session = Application.Session;
            if (session != null)
            {
                session[key] = null;
            }
            else
            {
                ClientSession[key] = null;
            }
        }

        /// <summary>
        /// Stores the singleton instance in the session's repository.
        /// </summary>
        /// <param name="value"></param>
        public static void SaveInstance(object value)
        {
            Type t = value.GetType();
            string key = t.Assembly.GetName().Name + "." + t.FullName;
            dynamic session = Application.Session;

            if (session != null)
            {
                session[key] = value;
            }
            else
            {
                // if there is no session, we use a static repository to avoid NullObject exceptions
                // when in design mode or shutting down the server.
                ClientSession[key] = value;
            }
        }

        public static void SaveInstance(string key, object value)
        {
            dynamic session = Application.Session;

            if (session != null)
            {
                session[key] = value;
            }
            else
            {
                // if there is no session, we use a static repository to avoid NullObject exceptions
                // when in design mode or shutting down the server.
                ClientSession[key] = value;
            }
        }
    }
}
