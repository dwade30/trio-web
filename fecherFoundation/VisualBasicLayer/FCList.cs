﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation.VisualBasicLayer
{
    public class FCList<T> : System.Collections.Generic.List<T>
    {
        /// <summary>
        /// In VB6 the Count Property only returns the number of Items which are unequal null
        /// </summary>
        public new int Count
        {
            get
            {
                int count = 0;
                foreach (var item in this)
                {
                    if (item != null)
                    {
                        count++;
                    }
                }
                return count;
            }
        }
    }
}
