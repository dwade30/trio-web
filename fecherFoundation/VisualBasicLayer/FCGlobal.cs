﻿using fecherFoundation.VisualBasicLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// Sets, returns or initialiazes global information : Printer, Forms (collection), Screen.
    /// </summary>
    public static class FCGlobal
    {
        #region Public Members

        /// <summary>
        /// Sets, returns or initializes a Printer object to mimic VB6 printer object. 
        /// </summary>
        public static FCPrinter Printer
        {
            get
            {
                if (printer == null)
                {
                    printer = new FCPrinter();
                }
                return printer;
            }
            set { printer = value; }
        }

        //TODO
        public static List<FCPrinter> Printers
        {
            get
            {
                if (printers == null)
                {
                    printers = new List<FCPrinter>();
                    foreach (var win32Printer in Win32Printer.GetList())
                    {
                        FCPrinter fcPrinter = new FCPrinter(win32Printer.Name);
                        fcPrinter.Win32Printer = win32Printer;
                        printers.Add(fcPrinter);
                    }
                }

                return printers;
            }
        }

        /// <summary>
        /// Gets, sets or initializes Screen to provide conversion methods between VB6 and C# related to screen size and mouse pointer information.
        /// </summary>
        public static FCScreen Screen
        {
            get
            {
                if (screen == null)
                {
                    screen = new FCScreen();
                }
                return screen;
            }
            set { screen = value; }
        }

        #endregion

        #region Internal Members

        internal static bool applicationStarted = false;

        #endregion

        #region Private Members

        private static FCPrinter printer;
        private static List<FCPrinter> printers;
        private static FCScreen screen;
        

        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public static bool InApplicationExit = false;

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Loads a string from a resource (.RES) file and returns it as a property of a control.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string LoadResString(int id)
        {
            // TODO:CHE
            return "";
        }

        public static void ApplicationExit()
        {
            //FC:FINAL:IPI - do not exit the application, show the Login form on sign out
            //InApplicationExit = true;            
            //Wisej.Web.Application.Exit();            
        }

        public static void ApplicationStart()
        {
            //CHE: in VB6 abbreviated month does not contain "."
            CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture.Clone() as CultureInfo;
            var dtfi = ci.DateTimeFormat;
            string[] newAbbreviatedMonthNames = dtfi.AbbreviatedMonthNames.Clone() as string[];
            for (int i = 0; i <= 12; i++)
            {
                newAbbreviatedMonthNames[i] = newAbbreviatedMonthNames[i].Replace(".", "");
            }
            dtfi.AbbreviatedMonthNames = newAbbreviatedMonthNames;
            dtfi.AbbreviatedMonthGenitiveNames = dtfi.AbbreviatedMonthNames;
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            applicationStarted = true;
        }

        /// <summary>
        /// bool from string; can manage "1", "-1", "0","True","False"
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool BoolFromString(string value)
        {
            bool functionReturnValue = false;
            bool blnReturn = false;
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    return blnReturn;
                }
                value = value.ToLower();
                //CHE: treat german values wahr and falsch to avoid exception
                if (value == "falsch")
                {
                    return false;
                }
                else if (value == "wahr")
                {
                    return true;
                }
                if (value != "true" && value != "false")
                {
                    blnReturn = Convert.ToBoolean(Convert.ToInt32(value));
                }
                else
                {
                    blnReturn = Convert.ToBoolean(value);
                }
            }
            catch
            {
                functionReturnValue = false;
            }
            finally
            {
                functionReturnValue = blnReturn;
            }

            return functionReturnValue;
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

        public class StaticVariables
        {
            private FormsCollection formsCollection;

            /// <summary>
            /// Gets or initializes the FormsCollection.
            /// </summary>
            public FormsCollection Forms
            {
                get
                {
                    if (formsCollection == null)
                    {
                        formsCollection = new FormsCollection();
                    }
                    return formsCollection;
                }
            }

        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
