﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public class FCFixedString
    {
        #region Private Members

        private int size = 0;
        private string value = "";

        #endregion

        #region Constructors

        public FCFixedString(int size)
        {
            this.size = size;
            this.value = new string('\0', size);
        }

        #endregion

        #region Properties

        public int Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }

        public int Length
        {
            get
            {
                return this.value.Length;
            }
        }

        public string Value
        {
            get
            {
                return value;
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                //when changing value the size remains the same, it is filled with spaces if the case
                this.value = value.Substring(0, Math.Min(size, value.Length));
                if (this.value.Length != size)
                {
                    this.value += new string(' ', size - value.Length);
                }
            }
        }

        #endregion

        #region Protected Methods

        public static implicit operator string(FCFixedString fixedString)
        {
            return fixedString.Value;
        }

        public override string ToString()
        {
            return this.value;
        }

        #endregion
    }

}
