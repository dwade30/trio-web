﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace fecherFoundation
{
    public class VBArray<T>
    {
        private MemberInfo[] memberInfo;

        /// <summary>
        /// The offsets
        /// </summary>
        internal readonly VBArrayBounds[] marrBounds;

        /// <summary>
        /// The original array
        /// </summary>
        public T[] marrInternalArray;
        
        //FC:FINAL:CHE - get VBArray length
        public int Length
        {
            get
            {
                return marrInternalArray.Length;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VBArray{T}"/> class.
        /// </summary>
        /// <param name="arrBounds">The arr bounds.</param>
        public VBArray(params VBArrayBounds[] arrBounds) : this(null, arrBounds)
        {
            
        }

        public VBArray(VBArray<T> preservedArray, params VBArrayBounds[] arrBounds)
        {
            arrBounds = arrBounds ?? new VBArrayBounds[] { };

            // Set the indexer bounds
            marrBounds = arrBounds;

            // The total length of the .net array
            int intCompatibleArrayLength = 1;

            VerifyArrayBounds(preservedArray);

            // Loop through the bounds
            foreach (VBArrayBounds objBounds in arrBounds)
            {
                // Multiply by the length to get the full length of multi-dimesional arrays
                intCompatibleArrayLength *= objBounds.Length;
            }
            
            //FC:FINAL:JEI
            if (arrBounds.Length > 1 && arrBounds[0].LowerBound > 0)
            {
                // if LowerBound > 0 we have to increase the marrInternalArray about the missing rows by adding additional columns
                int intOffset = 0;
                // Example: arrBounds[0].Length == 2 and arrBounds[1].Length == 5 but arrBounds[0].LowerBound == 1
                // so it would look like this:
                // null null    null    null    null
                // x    x       x       x       x
                // x    x       x       x       x
                // so we need to have an array of Length 15, but with the Loop above we only get an array of Length 10 ( = 2*5)
                // to fix this Length-missmatch we have to add an offset which will be calculated next
                // increasing the marrInternalArray about (arrBounds[0].LowerBound * arrBounds[1].Length) items (1*5 = 5), will give us the needed space
                intOffset += arrBounds[0].LowerBound * (arrBounds[1].Length);
                intCompatibleArrayLength += intOffset;
            }

            // Initialize the .Net array
            marrInternalArray = new T[intCompatibleArrayLength];

            //FC:FINAL:CHE - instantiate objects where is the case; string type has no parameterless constructor
            bool isStringType = (typeof(T) == typeof(string));
            T emptyString = isStringType ? (T)(object)"" : default(T);
            for (int i = 0; i < intCompatibleArrayLength; i++)
            {
                marrInternalArray[i] = isStringType ? emptyString : Activator.CreateInstance<T>();
            }

            // Copy the preserved array
            CopyPreservedArray(preservedArray);
        }

        public bool CopyTo(VBArray<T> destination)
        {
            MemberInfo[] memberInfo = GetMembers();

            object[] data = FormatterServices.GetObjectData(this, memberInfo);
            FormatterServices.PopulateObjectMembers(destination, memberInfo, data);
            return true;
        }

        protected MemberInfo[] GetMembers()
        {
            if (memberInfo == null)
            {
                Type type = this.GetType();
                memberInfo = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            }
            return memberInfo;
        }


        //FC:FINAL:CHE - get array T[] from VBArray<T>
        public static T[] GetArray(VBArray<T> vbArray)
        {
            return vbArray.marrInternalArray;
        }

        //FC:FINAL:CHE - clear array T[] from VBArray<T>
        public static void Clear(VBArray<T> vbArray, int index, int length)
        {
            Array.Clear(vbArray.marrInternalArray, index, length);
        }

        /// <summary>
        /// Verifies the array bounds.
        /// </summary>
        /// <param name="preservedArray">The preserved array.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// It is not possible to change the number of dimensions in a preserved array.
        /// or
        /// It is not possible to change the last dimension's lower bound of a preserved array.
        /// or
        /// It is not possible to the dimension's bounds in a preserved array.
        /// </exception>
        private void VerifyArrayBounds(VBArray<T> preservedArray)
        {
            if (preservedArray != null)
            {
                // Check that the dimension number wasn't changed
                if (preservedArray.marrBounds.Length != marrBounds.Length)
                    throw new ArgumentOutOfRangeException("It is not possible to change the number of dimensions in a preserved array.");

                // If we have bounds defined
                if (preservedArray.marrBounds.Length > 0)
                {
                    // Loop through the bounds
                    for (int i = 0; i < marrBounds.Length; i++)
                    {
                        // The new array bounds
                        var newBounds = marrBounds[i];

                        // The old array bounds
                        var oldBounds = preservedArray.marrBounds[i];

                        // If the bounds are changed, throw exception
                        if (newBounds.LowerBound != oldBounds.LowerBound)
                            throw new ArgumentOutOfRangeException("It is not possible to the dimension's bounds in a preserved array.");

                        // If this is the last bound
                        if (i < marrBounds.Length - 1)
                        {
                            // If the lower bound was changed, throw exception
                            if (newBounds.UpperBound != oldBounds.UpperBound)
                                throw new ArgumentOutOfRangeException("It is not possible to change the last dimension's lower bound of a preserved array.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Copies the preserved array.
        /// </summary>
        /// <param name="preservedArray">The preserved array.</param>
        private void CopyPreservedArray(VBArray<T> preservedArray)
        {
            // If we have a valid preserved array
            if (preservedArray != null)
            {
               
                // The old bounds and the old indecses array
                var oldBounds = preservedArray.marrBounds;
                int[] oldIndex = new int[oldBounds.Length];

                int offset = 0;
                switch (oldBounds.Length)
                {
                    case 1:
                        offset = 0;
                        break;

                    case 2:
                        offset = oldBounds[0].LowerBound * oldBounds[1].Length;
                        break;

                    default:
                        throw new System.NotImplementedException();
                }

                // Loop through the preserved array
                for (int i = offset; i < preservedArray.marrInternalArray.Length; i++) 
                {                    
                    // Get the current item
                    T item = preservedArray.marrInternalArray[i];                    

                    //CHE: fix for bidimensional arrays
                    #region old code
                    //// Loop through the old bounds
                    //for (int j = 0; j < oldBounds.Length; j++)
                    //{
                    //    // Get the old dimension bounds
                    //    var dimensionBounds = oldBounds[j];

                    //    // Get the dimension index
                    //    int dimensionIndex = offset % dimensionBounds.Length + dimensionBounds.LowerBound;

                    //    // Set the old index
                    //    oldIndex[j] = dimensionIndex;

                    //    offset -= dimensionIndex;
                    //}

                    //// If this is the last new dimension and it's bigger then the upperbound of the old last dimension
                    //// disregard the item
                    //if (oldIndex.Last() > this.marrBounds.Last().UpperBound)
                    //    continue;
                    #endregion

                    switch (oldBounds.Length)
                    {
                        case 1:
                            {
                                // Set the old index
                                oldIndex[0] = i + oldBounds[0].LowerBound;

                                // If this is the last new dimension and it's bigger then the upperbound of the old last dimension
                                // disregard the item
                                if (oldIndex.Last() > this.marrBounds.Last().UpperBound)
                                    continue;

                                break;
                            }
                        case 2:
                            {   
                                // Set the old index
                                oldIndex[0] = i / oldBounds[1].Length;
                                oldIndex[1] = i % oldBounds[1].Length + oldBounds[1].LowerBound;

                                // If this is the last new dimension and it's bigger then the upperbound of the old last dimension
                                // disregard the item
                                if (oldIndex[0] > this.marrBounds[0].UpperBound || oldIndex[1] > this.marrBounds[1].UpperBound)
                                    continue;

                                break;
                            }
                        default:
                            {
                                throw new System.NotImplementedException();
                            }
                    }
                    //CHE: end - fix for bidimensional arrays

                    // Insert the item
                    this[oldIndex] = item;
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="T"/> with the specified indexer.
        /// </summary>
        /// <value>
        /// The <see cref="T"/>.
        /// </value>
        /// <param name="arrIndexer">The indexer.</param>
        /// <returns></returns>
        public T this[params int[] arrIndexer]
        {
            get
            {
                // Compute the .NET array indexer by using the offsets
                int intRealIndex = GetRealIndexer(arrIndexer);

                // Return the item from the .NET array
                return marrInternalArray[intRealIndex];
            }
            set
            {
                // Compute the .NET array indexer by using the offsets
                int intRealIndex = GetRealIndexer(arrIndexer);

                // Set the item in the .NET array
                marrInternalArray[intRealIndex] = value;
            }
        }

        /// <summary>
        /// Gets the real indexer.
        /// </summary>
        /// <param name="arrIndexer">The indexer.</param>
        /// <returns></returns>
        private int GetRealIndexer(int[] arrIndexer)
        {
            // If we didn't specify indexers
            if (arrIndexer == null || arrIndexer.Length == 0)
                throw new ArgumentOutOfRangeException("At least one dimension must be specified.");

            // If the amount of dimensions doesn't match the amount of offsets
            if (arrIndexer.Length != marrBounds.Length)
                throw new ArgumentOutOfRangeException("The number of dimensions specified doesn't match the expected number.");

            //CHE: check bounds when accessing array
            for (int i = 0; i < arrIndexer.Length; i++)
            {
                if (arrIndexer[i] < marrBounds[i].LowerBound || arrIndexer[i] > marrBounds[i].UpperBound)
                {
                    throw new IndexOutOfRangeException("Index is outside the bounds of array");
                }
            }

            // Get the first index
            int intRealIndex = arrIndexer[0] - marrBounds[0].LowerBound;

            //CHE: fix for bidimensional arrays
            #region old code
            //int multiplier = 1;

            //// Loop through the indexer array (for multi-dimension compatibility)
            //for (int i = 1; i < arrIndexer.Length; i++)
            //{
            //    // Multiply by the first dimension length
            //    multiplier *= marrBounds[i - 1].Length;

            //    // Get the indexer
            //    int intIndexer = arrIndexer[i];

            //    // Get the respective offset
            //    VBArrayBounds objOffset = marrBounds[i];

            //    // Get the offsetted index
            //    int zeroBasedIndex = intIndexer - objOffset.LowerBound;

            //    // Add to the big array index
            //    intRealIndex += multiplier * zeroBasedIndex;
            //}
            #endregion

            switch (arrIndexer.Length)
            {
                case 1:
                    {
                        break;
                    }
                case 2:
                    {
                        intRealIndex = arrIndexer[0] * (marrBounds[1].UpperBound - marrBounds[1].LowerBound + 1) + (arrIndexer[1] - marrBounds[1].LowerBound);
                        break;
                    }
                default:
                    {
                        int multiplier = 1;

                        // Loop through the indexer array (for multi-dimension compatibility)
                        for (int i = 1; i < arrIndexer.Length; i++)
                        {
                            // Multiply by the first dimension length
                            multiplier *= marrBounds[i - 1].Length;

                            // Get the indexer
                            int intIndexer = arrIndexer[i];

                            // Get the respective offset
                            VBArrayBounds objOffset = marrBounds[i];

                            // Get the offsetted index
                            int zeroBasedIndex = intIndexer - objOffset.LowerBound;

                            // Add to the big array index
                            intRealIndex += multiplier * zeroBasedIndex;
                        }
                        break;
                    }
            }
            //CHE: end -fix for bidimensional arrays

            // Return the .Net array index
            return intRealIndex;
        }
    }
}
