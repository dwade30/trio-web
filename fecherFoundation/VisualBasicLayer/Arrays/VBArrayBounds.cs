﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// A class for specifying array bounds
    /// </summary>
    public class VBArrayBounds
    {
        /// <summary>
        /// Gets or sets the lower bound.
        /// </summary>
        /// <value>
        /// The lower bound.
        /// </value>
        internal int LowerBound { get; set; }

        /// <summary>
        /// Gets or sets the upper bound.
        /// </summary>
        /// <value>
        /// The upper bound.
        /// </value>
        internal int UpperBound { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="VBArrayBounds"/> class.
        /// </summary>
        /// <param name="lBound">The lower bound.</param>
        /// <param name="uBound">The upper bound.</param>
        public VBArrayBounds(int lBound, int uBound)
        {
            LowerBound = lBound;
            UpperBound = uBound;
        }

        /// <summary>
        /// Gets the length.
        /// </summary>
        /// <value>
        /// The length.
        /// </value>
        internal int Length { get { return UpperBound - LowerBound + 1; } }
    }
}
