﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public class ArrayUtils<T> where T : class
    {
        public static T Redim(Array oldArray, bool preserve, params int[] lengths)
        {
            Type elementType = typeof(T).GetElementType();

            if (oldArray == null || !preserve || oldArray.Length == 0)
                return Array.CreateInstance(elementType, lengths) as T;

            int rank = oldArray.Rank;

            if (rank != lengths.Length)
                throw new ArgumentException("Old and new array ranks must be the same.");

            for (int i = 0; i < oldArray.Rank - 1; i++)
            {
                if (oldArray.GetLength(i) != lengths[i])
                    throw new ArgumentException("All array dimentions except the last must be of the same length.");
            }

            Array newArray = Array.CreateInstance(elementType, lengths);

            int[] copiedLengths = lengths.Select(l => l - 1).ToArray();
            int newLastLength = lengths.Last() - 1,
                oldLastLength = oldArray.GetLength(rank - 1) - 1;

            copiedLengths[rank - 1] = Math.Min(oldLastLength, newLastLength);

            int[] indicies = Enumerable.Repeat(0, rank).ToArray();

            bool done = false;

            while (!done)
            {
                object value = oldArray.GetValue(indicies);
                newArray.SetValue(value, indicies);

                IncrementIndicies(indicies, copiedLengths, out done);
            }

            return newArray as T;
        }

        public static T RedimPreserve(Array oldArray, params int[] lengths)
        {
            return Redim(oldArray, preserve: true, lengths: lengths);
        }

        private static void IncrementIndicies(int[] indicies, int[] lengths, out bool done)
        {
            done = false;

            int index = 0;

            do
            {
                indicies[index]++;

                if (indicies[index] > lengths[index])
                {
                    indicies[index] = 0;
                    index++;
                }
                else
                    break;
            }
            while (index < indicies.Length);

            if (index >= indicies.Length)
                done = true;
        }
    }
}
