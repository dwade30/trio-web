﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    /// <summary>
    /// Provides methods for conversion between VB6 and C# resolution differences and mouse pointer information.
    /// </summary>
    public class FCScreen
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private MousePointerConstants mousePointer = MousePointerConstants.vbDefault;

        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Returns or sets the mousePointer as in VB6, depending on a translator method.
        /// </summary>
        public MousePointerConstants MousePointer
        {
            get
            {
                return mousePointer;
            }
            set
            {
                mousePointer = value;
                //Cursor.Current = FCUtils.GetTranslatedCursor(value);
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        public int WidthOriginal
        {
            get
            {
                return Convert.ToInt32(this.Width * TwipsPerPixelX);
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        public int HeightOriginal
        {
            get
            {
                return Convert.ToInt32(this.Height * TwipsPerPixelY);
            }
        }

        /// <summary>
        /// Returns a constant as ratio between twips and pixels on the X axis.
        /// </summary>
        public static int TwipsPerPixelX
        {
            get
            {

                //JEI: creating a new FCForm will fire all events from the TabbedMDI, 
                // it's like you create a new MDIChild, so the focus will be reseted and Validation-Events will be fired
                return 15;
                //using (FCForm f = new FCForm())
                //{
                //    return f.FormTwipsPerPixelX;
                //}
            }
        }

        /// <summary>
        /// Returns a constant as ratio between twips and pixels on the Y axis.
        /// </summary>
        public static int TwipsPerPixelY
        {
            get
            {
                //JEI: creating a new FCForm will fire all events from the TabbedMDI, 
                // it's like you create a new MDIChild, so the focus will be reseted and Validation-Events will be fired
                return 15;
                //using (FCForm f = new FCForm())
                //{
                //    return f.FormTwipsPerPixelY;
                //}
            }
        }

        /// <summary>
        /// Gets the screen height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height
        {
            get
            {
                return Wisej.Web.Screen.WorkingArea.Height;
            }
        }

        /// <summary>
        /// Gets the screen width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width
        {
            get
            {
                return Wisej.Web.Screen.WorkingArea.Width;
            }
        }

        /// <summary>
        /// Returns the control that has the focus. When a form is referenced, as in ChildForm.ActiveControl, ActiveControl specifies the control that would have the 
        /// focus if the referenced form were active. Not available at design time; read-only at run time.
        /// Remarks
        /// You can use ActiveControl to access a control's properties or to invoke its methods: For example, Screen.ActiveControl.Tag = "0". A run-time error occurs 
        /// if all controls on the form are invisible or disabled.
        /// Each form can have an active control (Form.ActiveControl), regardless of whether or not the form is active. You can write code that manipulates the active 
        /// control on each form in your application even when the form isn't the active form.
        /// This property is especially useful in a multiple-document interface (MDI) application where a button on a toolbar must initiate an action on a control in an 
        /// MDI child form. When a user clicks the Copy button on the toolbar, your code can reference the text in the active control on the MDI child form, as in 
        /// ActiveForm.ActiveControl.SelText.
        /// Note   If you plan to pass Screen.ActiveControl to a procedure, you must declare the argument in that procedure with the clause As Control rather than 
        /// specifying a control type (As TextBox or As ListBox) even if ActiveControl always refers to the same type of control.
        /// </summary>
        public Control ActiveControl
        {
            get
            {
                Form activeForm = Form.ActiveForm;
                if (activeForm != null)
                {
                    if (activeForm.IsMdiContainer)
                    {
                        Form activeChildForm = activeForm.ActiveMdiChild;
                        if (activeChildForm != null)
                        {
                            return activeChildForm.ActiveControl;
                        }
                    }
                    
                    return activeForm.ActiveControl;
                }
                return null;
            }
            set
            {
                // TODO:CHE
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
      
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
