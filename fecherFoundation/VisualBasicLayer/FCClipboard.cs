﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    /// <summary>
    /// VB.Global.Clipboard
    /// </summary>
    public static class FCClipboard
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        /// <summary>
        /// VB6 data format values.
        /// </summary>
        public enum ClipBoardConstants
        {
            vbCFBitmap = 2,
            vbCFDIB = 8,
            vbCFEMetafile = 14,
            vbCFFiles = 15,
            vbCFLink = -16640,// (&HFFFFBF00)
            vbCFMetafile = 3,
            vbCFPalette = 9,
            vbCFRTF = -16639,// (&HFFFFBF01)
            vbCFText = 1
        }
        #endregion

        #region Properties
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieves data from the Clipboard in the specified format.
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public static Image GetDataImage(ClipBoardConstants format)
        {
            return (Image)GetData(format);
        }

        /// <summary>
        /// Retrieves data from the Clipboard in the specified format.
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public static object GetData(ClipBoardConstants format)
        {
            return Clipboard.GetData(GetDataFormat(format));
        }

        /// <summary>
        /// Clears the Clipboard and then adds data in the specified format.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="format"></param>
        public static void SetData(object content, ClipBoardConstants format)
        {
            Clipboard.SetData(GetDataFormat(format), content);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        /// <summary>
        /// Returns the equivalent C# dataformat based on VB6 values.
        /// </summary>
        /// <param name="format">ClipBoardConstans type (VB6 format values enum).</param>
        /// <returns></returns>
        private static string GetDataFormat(ClipBoardConstants format)
        {
            switch (format)
            {
                case ClipBoardConstants.vbCFBitmap: return DataFormats.Bitmap;
                case ClipBoardConstants.vbCFDIB: return DataFormats.Dib;
                case ClipBoardConstants.vbCFEMetafile: return DataFormats.EnhancedMetafile;
                case ClipBoardConstants.vbCFRTF: return DataFormats.Rtf;
                case ClipBoardConstants.vbCFText: return DataFormats.Text;
                default: throw new NotImplementedException();
            }
        }
        #endregion
    }
}
