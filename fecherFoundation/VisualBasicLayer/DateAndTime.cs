﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public static class DateAndTime
    {
        public static DateTime Today
        {
            get
            {
                return DateTime.Today;
            }
        }

        public static DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        public static TimeSpan TimeOfDay
        {
            get
            {
                return DateTime.Now.TimeOfDay;
            }
        }

        public static int Weekday(DateTime value)
        {
            return ((int)value.DayOfWeek) + 1;
        }

        public static string WeekdayName(int value)
        {
            DayOfWeek day = (DayOfWeek)value;
            return day.ToString();
        }

        public static int Year(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Year;
        }

        public static int Month(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Month;
        }

        public static int Day(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Day;
        }

        public static int Hour(TimeSpan value)
        {
            if (value == TimeSpan.MinValue)
            {
                return 0;
            }

            DateTime date = new DateTime(1, 1, 1, 0, 0, 0);
            date = date + value;

            return date.Hour;
        }

        public static int Hour(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Hour;
        }

        public static int Minute(TimeSpan value)
        {
            if (value == TimeSpan.MinValue)
            {
                return 0;
            }

            DateTime date = new DateTime(1, 1, 1, 0, 0, 0);
            date = date + value;

            return date.Minute;
        }

        public static int Minute(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Minute;
        }

        public static int Second(TimeSpan value)
        {
            if (value == TimeSpan.MinValue)
            {
                return 0;
            }

            DateTime date = new DateTime(1, 1, 1, 0, 0, 0);
            date = date + value;

            return date.Second;
        }

        public static int Second(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Second;
        }

        public static DateTime DateValue(string val)
        {
            DateTime date;

            DateTime.TryParse(val, out date);

            return date;
        }

        public static DateTime TimeValue(string val)
        {
            return DateTime.ParseExact(val, "HH:mm:ss", CultureInfo.InvariantCulture);
        }

        public static DateTime DateSerial(int year, int month, int day)
        {
            // ?? test If month == 0 too, same functionality like day
            if (day == 0)
            {
                return new DateTime(year, month, day + 1).AddDays(-1);
            }

            return new DateTime(year, month, day);
        }

        public static DateTime TimeSerial(int hour, int minute, int second)
        {
            return new DateTime(1, 1, 1, hour, minute, second);
        }

        public static int DateDiff(string interval, object start, object end)
        {
            DateTime dateStart;
            DateTime dateEnd;

            if (start == null || end == null)
            {
                return 0;
            }

            if (DateTime.TryParse(start.ToString(), out dateStart) && DateTime.TryParse(end.ToString(), out dateEnd))
            {
                return DateDiff(interval, dateStart, dateEnd);
            }
            return 0;
        }

        public static int DateDiff(string interval, object start, DateTime end)
        {
            DateTime date;

            if (start == null)
            {
                return 0;
            }

            if (DateTime.TryParse(start.ToString(), out date))
            {
                return DateDiff(interval, date, end);
            }

            return 0;
        }

        public static int DateDiff(string interval, DateTime start, DateTime end)
        {
            TimeSpan diff = end - start;
            // JSP/Fecher correction: consider interval too
            // possible values for interval
            // "d" = Tag
            // "y" = Tag innerhalb des Jahrs
            // "h" = Stunde
            // "n" = Minute
            // "m" = Monat
            // "q" = Quartal
            // "s" = Sekunde
            // "w" = Woche
            // "ww" = Kalenderwoche
            // "yyyy" = Jahr

            switch (interval.ToLower())
            {
                case "d":
                    return Convert.ToInt32(diff.TotalDays);

                case "y":
                    // perhaps wrong, but I didn't find any explication what the difference in  "Days of year" means
                    return Convert.ToInt32(diff.TotalDays);

                case "h":
                    return Convert.ToInt32(diff.TotalHours);

                case "n":
                    return Convert.ToInt32(diff.TotalMinutes);

                case "m":
                    // Month ==> days / (365/12)
                    return Convert.ToInt32(diff.TotalDays / 30.42);

                case "q":
                    // Quarter ==> days / (365/4)
                    return Convert.ToInt32(diff.TotalDays / 91.25);

                case "s":
                    return Convert.ToInt32(diff.TotalSeconds);

                case "w":
                    return Convert.ToInt32(diff.Days / 7);

                case "ww":
                    // perhaps wrong, but I didn't find any explication what the difference in  "calenderweeks" means
                    // or where is the difference to "Difference in weeks"
                    return Convert.ToInt32(diff.Days / 7);

                case "yyyy":
                    return Convert.ToInt32(diff.Days / 365);

                default:
                    return Convert.ToInt32(diff.TotalSeconds);
            }
        }

        public static DateTime DateAdd(string interval, int number, string date)
        {
            DateTime newDate;

            if (DateTime.TryParse(date, out newDate))
            {
                newDate = DateAdd(interval, number, newDate);
            }

            return newDate;
        }

        public static DateTime DateAdd(string interval, int number, object date)
        {
            if (date != null)
            {
                return DateAdd(interval, number, date.ToString());
            }

            return DateAdd(interval, number, "");
        }

        public static DateTime DateAdd(string interval, object date, int number)
        {
            DateTime newDate = new DateTime(1, 1, 1);

            if (date == null)
            {
                return newDate;
            }

            if (DateTime.TryParse(date.ToString(), out newDate))
            {
                newDate = DateAdd(interval, number, newDate);
            }

            return newDate;
        }

        public static DateTime DateAdd(string interval, DateTime date, int number)
        {
            return DateAdd(interval, number, date);
        }

        public static DateTime DateAdd(string interval, int number, DateTime date)
        {
            if (date == DateTime.MinValue)
            {
                return date;
            }

            switch (interval)
            {
                case "d":
                    return date.AddDays(number);

                case "s":
                    return date.AddSeconds(number);

                case "n":
                    return date.AddMinutes(number);

                case "h":
                    return date.AddHours(number);

                case "q":
                    return date.AddMonths(3 * number);

                case "m":
                    return date.AddMonths(number);

                case "ww":
                    return date.AddDays(7 * number);

                case "yyyy":
                    return date.AddYears(number);
            }

            return date;
        }
    }
}
