﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public class Financial
    {
		/// <summary>
		/// Calculates the interest part of a payment for a given period.
		/// </summary>
		/// <param name="rate"> Required. The interest rate per period.</param>
		/// <param name="per"> Required. The period for which you want to find the interest and must be in the range 1 to nper.</param>
		/// <param name="nper">Required. The total number of payment periods in an annuity.</param>
		/// <param name="pv"> Required. The present value, or the lump-sum amount that a series of future payments is worth right now.</param>
		/// <param name="fv">Optional. The future value, or a cash balance you want to attain after the last payment is made. If fv is omitted, it is assumed to be 0 (the future value of a loan, for example, is 0).</param>
		/// <param name="due">Optional. The number 0 or 1 and indicates when payments are due. If due is omitted, it is assumed to be 0.</param>
		/// <returns></returns>
		public static double IPmt(double rate, double per, double nper, double pv, double fv = 0, double due = 0)
		{
			if (per < 1 || per > nper)
			{
				FCMessageBox.Show("The payment period must be between 1 and total number of payments", MsgBoxStyle.Exclamation);
				return 0;
			}
			else if (rate == 0)
			{
				// no message in VB6
				//FCMessageBox.Show("This implementation doesn’t handle zero interest rate", MsgBoxStyle.Exclamation);
				return 0;
			}

			double payment = Pmt(rate, nper, pv);
			fv = FV(rate, (per - 1), payment, pv);

			return (fv * rate);
        }

		/// <summary>
		/// Calculates the payment for a loan based on constant payments and a constant interest rate.
		/// </summary>
		/// <param name="rate">Required. The interest rate for the loan.</param>
		/// <param name="nper">Required. The total number of payments for the loan.</param>
		/// <param name="pv"> Required. The present value, or the total amount that a series of future payments is worth now; also known as the principal.</param>
		/// <returns></returns>
		public static double Pmt(double rate, double nper, double pv)
		{
			double temp = System.Math.Pow((rate + 1), nper);

			return ((-pv * temp) / ((temp - 1)) * rate);
		}

		/// <summary>
		/// Calculates the future value of an investment based on a constant interest rate. You can use FV with either periodic, constant payments, or a single lump sum payment.
		/// </summary>
		/// <param name="rate"> Required. The interest rate per period.</param>
		/// <param name="nper">Required. The total number of payment periods in an annuity.</param>
		/// <param name="payment">Required. The payment made each period. It cannot change over the life of the annuity. Typically, payment contains principal and interest but no other fees or taxes. If payment is omitted, you must include the pv argument.</param>
		/// <param name="pv"> Optional. The present value, or the lump-sum amount that a series of future payments is worth right now. If pv is omitted, it is assumed to be 0 (zero), and you must include the pmt argument.</param>
		/// <returns></returns>
		public static double FV(double rate, double nper, double payment, double pv)
		{
			double temp = System.Math.Pow(rate + 1, nper);

			return ((-pv) * temp) - ((payment / rate) * (temp - 1));
		}
	}
}
