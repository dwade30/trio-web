﻿using System;

namespace fecherFoundation
{
  /// <summary>
  /// Summary description for InputBox.
  /// </summary>
  public class InputBoxDialog : Wisej.Web.Form
  {
 
    #region Windows Contols and Constructor
 
    private fecherFoundation.FCLabel lblPrompt;
    private fecherFoundation.FCButton btnOK;
    private Wisej.Web.TextBox txtInput;
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;
 
    public InputBoxDialog()
    {
      //
      // Required for Windows Form Designer support
      //
      InitializeComponent();
 
      //
      // TODO: Add any constructor code after InitializeComponent call
      //
    }
 
    #endregion
 
    #region Dispose
 
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }
 
    #endregion
 
    #region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
			this.lblPrompt = new fecherFoundation.FCLabel();
			this.txtInput = new Wisej.Web.TextBox();
			this.btnOK = new fecherFoundation.FCButton();
			this.btnCancel = new fecherFoundation.FCButton();
			((System.ComponentModel.ISupportInitialize)(this.btnOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
			this.SuspendLayout();
			// 
			// lblPrompt
			// 
			this.lblPrompt.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lblPrompt.Location = new System.Drawing.Point(19, 22);
			this.lblPrompt.Name = "lblPrompt";
			this.lblPrompt.Size = new System.Drawing.Size(368, 85);
			this.lblPrompt.TabIndex = 3;
			// 
			// txtInput
			// 
			this.txtInput.Location = new System.Drawing.Point(19, 119);
			this.txtInput.Name = "txtInput";
			this.txtInput.Size = new System.Drawing.Size(368, 40);
			this.txtInput.TabIndex = 0;
			// 
			// btnOK
			// 
			this.btnOK.AppearanceKey = "actionButton";
			this.btnOK.Location = new System.Drawing.Point(403, 22);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 40);
			this.btnOK.TabIndex = 4;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.BtnOKClick);
			// 
			// btnCancel
			// 
			this.btnCancel.AppearanceKey = "actionButton";
			this.btnCancel.DialogResult = Wisej.Web.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(403, 70);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 40);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
			// 
			// InputBoxDialog
			// 
			this.AcceptButton = this.btnOK;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(507, 189);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.txtInput);
			this.Controls.Add(this.lblPrompt);
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "InputBoxDialog";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "InputBox";
			this.Load += new System.EventHandler(this.InputBox_Load);
			((System.ComponentModel.ISupportInitialize)(this.btnOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

    }
    private fecherFoundation.FCButton btnCancel;
    #endregion
 
    #region Private Variables
    string formCaption = string.Empty;
    string formPrompt = string.Empty;
    string inputResponse = string.Empty;
    string defaultValue = string.Empty;
    #endregion
 
    #region Public Properties
    public string FormCaption
    {
      get{return formCaption;}
      set{formCaption = value;}
    } // property FormCaption
    public string FormPrompt
    {
      get{return formPrompt;}
      set{formPrompt = value;}
    } // property FormPrompt
    public string InputResponse
    {
      get{return inputResponse;}
      set{inputResponse = value;}
    } // property InputResponse
    public string DefaultValue
    {
      get{return defaultValue;}
      set{defaultValue = value;}
    } // property DefaultValue
 
    #endregion
 
    #region Form and Control Events
    private void InputBox_Load(object sender, System.EventArgs e)
    {
      this.txtInput.Text=defaultValue;
      this.lblPrompt.Text=formPrompt;
      this.Text=formCaption;
      this.txtInput.SelectionStart=0;
      this.txtInput.SelectionLength=this.txtInput.Text.Length;
      this.txtInput.Focus();
    }
    #endregion
 
 
 
    void BtnOKClick(object sender, EventArgs e)
    {
    	InputResponse = this.txtInput.Text;
    	this.Close();
    }
 
    void BtnCancelClick(object sender, EventArgs e)
    {
    	this.Close();
    }
  }
}