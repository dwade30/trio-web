﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace fecherFoundation
{
    internal enum PrinterObjectState
    {
        NoPrint,
        SetupPage,
        PaintPage,
    }

    public class FCPrinter
    {
        #region members
        private CoordinateSpace coordinateSpace;
        private GraphicsFactory graphicsFactory;
        private PrinterObject printerObject;
        private const int MaxStringLength = 10000;
        private FCGraphics fcGraphics;
        private List<string> fonts;
        #endregion

        #region Properties

        public List<string> Fonts
        {
            get
            {
                if (this.fonts == null)
                {
                    this.fonts = new List<string>();
                    for (int i = 0; i < this.graphicsFactory.FontCount; i++)
                    {
                        this.fonts.Add(this.graphicsFactory.get_Fonts(i));
                    }
                }

                return this.fonts;
            }
        }

        //CHE: use TabStops in report
        public float FirstTabOffset
        {
            get;
            set;
        }

        //CHE: use TabStops in report
        public float[] TabStops
        {
            get;
            set;
        }
                
        /// <summary>
        /// Gets or sets a value specifying whether the output will print in color or in monochrome on a device that supports color.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short ColorMode
        {
            get
            {
                return this.printerObject.PrinterModel.ColorMode;
            }
            set
            {
                this.CheckWithinPage("ColorMode");
                this.printerObject.PrinterModel.ColorMode = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines the number of copies to be printed.
        /// </summary>
        /// 
        /// <returns>
        /// A Short representing the number of copies to be printed.
        /// </returns>
        public short Copies
        {
            get
            {
                return this.printerObject.PrinterModel.Copies;
            }
            set
            {
                this.CheckWithinPage("Copies");
                this.printerObject.PrinterModel.Copies = value;
            }
        }

        /// <summary>
        /// Gets or sets the horizontal coordinates for the next printing or drawing method.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        public float CurrentX
        {
            get
            {
                return this.coordinateSpace.CurrentX;
            }
            set
            {
                this.coordinateSpace.CurrentX = value;
            }
        }

        /// <summary>
        /// Gets or sets the vertical coordinates for the next printing or drawing method.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        public float CurrentY
        {
            get
            {
                return this.coordinateSpace.CurrentY;
            }
            set
            {
                this.coordinateSpace.CurrentY = value;
            }
        }

        internal Win32Printer Win32Printer { get; set; }

        /// <summary>
        /// Converts the value for the width or height of a Form, PictureBox, or Printer from one of the ScaleMode property's unit of measure to another. Doesn't support named arguments.
        /// object.ScaleX (width, fromscale, toscale)
        /// object.ScaleY (height, fromscale, toscale)
        /// width	Required. Specifies, for object, the number of units of measure to be converted.
        /// height	Required. Specifies, for object, the number of units of measure to be converted.
        /// fromscale	Optional. A constant or value specifying the coordinate system from which width or height of object is to be converted, as described in Settings. The possible values of fromscale 
        /// are the same as for the ScaleMode property, plus the new value of HiMetric.
        /// toscale	Optional. A constant or value specifying the coordinate system to which width or height of object is to be converted, as described in Settings. The possible values of toscale are 
        /// the same as for the ScaleMode property, plus the new value of HiMetric.
        /// The settings for fromscale and toscale are:
        /// vbUser	0	User-defined: indicates that the width or height of object is set to a custom value.
        /// vbTwips	1	Twip (1440 twips per logical inch; 567 twips per logical centimeter).
        /// vbPoints	2	Point (72 points per logical inch).
        /// vbPixels	3	Pixel (smallest unit of monitor or printer resolution).
        /// vbCharacters	4	Character (horizontal = 120 twips per unit; vertical = 240 twips per unit).
        /// vbInches	5	Inch.
        /// vbMillimeters	6	Millimeter.
        /// vbCentimeters	7	Centimeter.
        /// vbHimetric	8	HiMetric. If fromscale is omitted, HiMetric is assumed as the default.
        /// vbContainerPosition	9	Determines control's position.
        /// vbContainerSize	10	Determines control's size.
        /// Remarks
        /// The ScaleX and ScaleY methods take a value (width or height), with its unit of measure specified by fromscale, and convert it to the corresponding value for the unit of measure 
        /// specified by toscale.
        /// You can also use ScaleX and ScaleY with the PaintPicture method.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="fromScale"></param>
        /// <param name="toScale"></param>
        /// <returns></returns>
        public float ScaleX(float width, ScaleModeConstants fromScale, ScaleModeConstants toScale)
        {
            return this.ScaleX(width, Convert.ToInt32(fromScale), Convert.ToInt32(toScale));
        }

        /// <summary>
        /// Converts the value for the width or height of a Form, PictureBox, or Printer from one of the ScaleMode property's unit of measure to another. Doesn't support named arguments.
        /// object.ScaleX (width, fromscale, toscale)
        /// object.ScaleY (height, fromscale, toscale)
        /// width	Required. Specifies, for object, the number of units of measure to be converted.
        /// height	Required. Specifies, for object, the number of units of measure to be converted.
        /// fromscale	Optional. A constant or value specifying the coordinate system from which width or height of object is to be converted, as described in Settings. The possible values of fromscale 
        /// are the same as for the ScaleMode property, plus the new value of HiMetric.
        /// toscale	Optional. A constant or value specifying the coordinate system to which width or height of object is to be converted, as described in Settings. The possible values of toscale are 
        /// the same as for the ScaleMode property, plus the new value of HiMetric.
        /// The settings for fromscale and toscale are:
        /// vbUser	0	User-defined: indicates that the width or height of object is set to a custom value.
        /// vbTwips	1	Twip (1440 twips per logical inch; 567 twips per logical centimeter).
        /// vbPoints	2	Point (72 points per logical inch).
        /// vbPixels	3	Pixel (smallest unit of monitor or printer resolution).
        /// vbCharacters	4	Character (horizontal = 120 twips per unit; vertical = 240 twips per unit).
        /// vbInches	5	Inch.
        /// vbMillimeters	6	Millimeter.
        /// vbCentimeters	7	Centimeter.
        /// vbHimetric	8	HiMetric. If fromscale is omitted, HiMetric is assumed as the default.
        /// vbContainerPosition	9	Determines control's position.
        /// vbContainerSize	10	Determines control's size.
        /// Remarks
        /// The ScaleX and ScaleY methods take a value (width or height), with its unit of measure specified by fromscale, and convert it to the corresponding value for the unit of measure 
        /// specified by toscale.
        /// You can also use ScaleX and ScaleY with the PaintPicture method.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="fromScale"></param>
        /// <param name="toScale"></param>
        /// <returns></returns>
        public float ScaleY(float height, ScaleModeConstants fromScale, ScaleModeConstants toScale)
        {
            return this.ScaleY(height, Convert.ToInt32(fromScale), Convert.ToInt32(toScale));
        }

        /// <summary>
        /// Converts the value for the width of a page from one of the units of measure of the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property to another.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        /// <param name="value">Specify the number of units of measure to be converted.</param><param name="fromScale">Optional. A constant or value specifying the coordinate system from which the width of the object is to be converted. The possible values of <paramref name="fromScale"/> are the same as those for the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property.</param><param name="toScale">Optional. A constant or value specifying the coordinate system to which the width of the object is to be converted. The possible values of <paramref name="toScale"/> are the same as those for the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public float ScaleX(float value, int fromScale = 8, int toScale = -1)
        {
            return this.coordinateSpace.ScaleX(value, fromScale, toScale);
        }

        /// <summary>
        /// Converts the value for the height of a page from one of the units of measure of the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property to another.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        /// <param name="value">Specify the number of units of measure to be converted.</param><param name="fromScale">Optional. A constant or value specifying the coordinate system from which the height of the object is to be converted. The possible values of <paramref name="fromScale"/> are the same as those for the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property.</param><param name="toScale">Optional. A constant or value specifying the coordinate system to which the height of the object is to be converted. The possible values of <paramref name="toScale"/> are the same as those for the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public float ScaleY(float value, int fromScale = 8, int toScale = -1)
        {
            return this.coordinateSpace.ScaleY(value, fromScale, toScale);
        }

        /// <summary>
        /// Gets the name of the current printer.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a String.
        /// </returns>
        public string DeviceName
        {
            get
            {
                return this.printerObject.PrinterModel.DeviceName;
            }
            set
            {
                //TODO
            }
        }

        //TODO
        public string DriverName
        {
            get; set;
        }

        //TODO
        public string Port
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the document name to display (for example, in a print status dialog box or printer queue) while printing the document.
        /// </summary>
        /// 
        /// <returns>
        /// A String to display while printing the document. The default is "document".
        /// </returns>
        public string DocumentName
        {
            get
            {
                return this.printerObject.DocumentName;
            }
            set
            {
                this.CheckWithinPage("DocumentName");
                this.printerObject.DocumentName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines the line style for output from graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short DrawStyle
        {
            get
            {
                return this.graphicsFactory.DrawStyle;
            }
            set
            {
                this.graphicsFactory.DrawStyle = value;
            }
        }

        /// <summary>
        /// Gets or sets the line width for output from graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short DrawWidth
        {
            get
            {
                return this.graphicsFactory.DrawWidth;
            }
            set
            {
                this.graphicsFactory.DrawWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether a page is printed on both sides (if the printer supports this feature).
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short Duplex
        {
            get
            {
                return this.printerObject.PrinterModel.Duplex;
            }
            set
            {
                this.CheckWithinPage("Duplex");
                this.printerObject.PrinterModel.Duplex = value;
            }
        }

        /// <summary>
        /// Gets or sets the color that is used to fill in shapes created by using the <see cref="Overload:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Circle"/> and <see cref="Overload:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Line"/> graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns an Integer.
        /// </returns>
        public int FillColor
        {
            get
            {
                return this.graphicsFactory.FillColor;
            }
            set
            {
                this.graphicsFactory.FillColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the pattern used to fill shapes created by using the <see cref="M:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Circle(System.Boolean,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single,System.Single)"/> and <see cref="M:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Line(System.Boolean,System.Single,System.Single,System.Boolean,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean)"/> graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short. The default is 1.
        /// </returns>
        public short FillStyle
        {
            get
            {
                return this.graphicsFactory.FillStyle;
            }
            set
            {
                this.graphicsFactory.FillStyle = value;
            }
        }

        /// <summary>
        /// Gets or sets a FontFamily by name.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a <see cref="T:System.Drawing.Font"/>.
        /// </returns>
        public Font Font
        {
            get
            {
                return this.graphicsFactory.Font;
            }
            set
            {
                this.graphicsFactory.Font = value;
            }
        }

        /// <summary>
        /// Gets or sets the bold font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        public bool FontBold
        {
            get
            {
                return this.graphicsFactory.FontBold;
            }
            set
            {
                this.graphicsFactory.FontBold = value;
            }
        }

        /// <summary>
        /// Returns the number of fonts available for the current display device or active printer.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short FontCount
        {
            get
            {
                return this.graphicsFactory.FontCount;
            }
        }

        /// <summary>
        /// Gets or sets the italic font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        public bool FontItalic
        {
            get
            {
                return this.graphicsFactory.FontItalic;
            }
            set
            {
                this.graphicsFactory.FontItalic = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the font in which text is displayed for a printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a String.
        /// </returns>
        public string FontName
        {
            get
            {
                return this.graphicsFactory.FontName;
            }
            set
            {
                this.graphicsFactory.FontName = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.graphicsFactory.FontSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the strikethrough font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public bool FontStrikethru
        {
            get
            {
                return this.graphicsFactory.FontStrikethru;
            }
            set
            {
                this.graphicsFactory.FontStrikethru = value;
            }
        }

        /// <summary>
        /// Returns a handle provided by the Microsoft Windows operating environment to the device context of an object.
        /// Remarks
        /// This property is a Windows operating environment device context handle. The Windows operating environment manages the system display by assigning a device context for the Printer object and 
        /// for each form and PictureBox control in your application. You can use the hDC property to refer to the handle for an object's device context. This provides a value to pass to Windows API calls.
        /// With a CommonDialog control, this property returns a device context for the printer selected in the Print dialog box when the cdlReturnDC flag is set or an information context when the cdlReturnIC 
        /// flag is set.
        /// Note   The value of the hDC property can change while a program is running, so don't store the value in a variable; instead, use the hDC property each time you need it.
        /// The AutoRedraw property can cause the hDC property setting to change. If AutoRedraw is set to True for a form or PictureBox container, hDC acts as a handle to the device context of the persistent 
        /// graphic (equivalent to the Image property). When AutoRedraw is False, hDC is the actual hDC value of the Form window or the PictureBox container. The hDC property setting may change while the 
        /// program is running regardless of the AutoRedraw setting.
        /// If the HasDC property is set to False, a new device context will be created by the system and the value of the hDC property will change each time it is called.
        /// </summary>
        [Obsolete("Not supported.")]
        public int hDC
        {
            get
            {
                return this.printerObject.GetGraphics().GetHdc().ToInt32();
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether background graphics on a Printer object are printed behind text characters.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        public bool FontTransparent
        {
            get
            {
                return this.graphicsFactory.FontTransparent;
            }
            set
            {
                this.graphicsFactory.FontTransparent = value;
            }
        }

        /// <summary>
        /// Gets or sets the underlined font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        public bool FontUnderline
        {
            get
            {
                return this.graphicsFactory.FontUnderline;
            }
            set
            {
                this.graphicsFactory.FontUnderline = value;
            }
        }

        /// <summary>
        /// Gets or sets the color in which text and graphics are printed.
        /// </summary>
        /// 
        /// <returns>
        /// Returns an Integer.
        /// </returns>
        public int ForeColor
        {
            get
            {
                return this.graphicsFactory.ForeColor;
            }
            set
            {
                this.graphicsFactory.ForeColor = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether the Printer object always points to the same printer or changes the printer it points to if you change the default printer setting 
        /// in the operating system's Control Panel. Not available at design time.
        /// Syntax
        /// object.TrackDefault [= boolean]
        /// The TrackDefault property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// boolean	A Boolean expression specifying the printer object points to, as described in Settings.
        /// Settings
        /// The settings for boolean are:
        /// Setting	Description
        /// True	(Default) The Printer object changes the printer it points to when you change the default printer settings in the operating system's Control Panel.
        /// False	The Printer object continues to point to the same printer even though you change the default printer settings in the operating system's Control Panel.
        /// Remarks
        /// Changing the TrackDefault property setting while a print job is in progress sends an implicit EndPage statement to the Printer object.
        /// </summary>
        public bool TrackDefault
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the height of a page.
        /// </summary>
        /// 
        /// <returns>
        /// Returns an Integer.
        /// </returns>
        public int Height
        {
            get
            {
                return this.printerObject.PrinterModel.Height;
            }
            set
            {
                this.CheckWithinPage("Height");
                this.printerObject.PrinterModel.Height = value;
                this.coordinateSpace.SetMarginBounds(this.printerObject.PrinterModel.MarginBounds);
            }
        }

        /// <summary>
        /// Returns a value that determines whether the currently selected printer is defined as the default printer in Control Panel.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        public bool IsDefaultPrinter
        {
            get
            {
                return this.printerObject.PrinterModel.IsDefaultPrinter;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether documents are printed in portrait or landscape mode.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short Orientation
        {
            get
            {
                return this.printerObject.PrinterModel.Orientation;
            }
            set
            {
                this.CheckWithinPage("Orientation");
                this.printerObject.PrinterModel.Orientation = value;
                this.coordinateSpace.SetMarginBounds(this.printerObject.PrinterModel.MarginBounds);
                //CHE: in VB6 when Printer.Orientation is set from code, even if not changed, the ForeColor is reset to 0 (did not find any documentation on this, maybe there are other cases when ForeColor is reset too)
                this.graphicsFactory.ForeColor = 0;
            }
        }

        /// <summary>
        /// Returns the page number of the page that is currently being printed.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short Page
        {
            get
            {
                return checked((short)this.printerObject.PageNumber);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the default paper bin on the printer from which paper is fed during print operations.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short PaperBin
        {
            get
            {
                return this.printerObject.PrinterModel.PaperBin;
            }
            set
            {
                this.CheckWithinPage("PaperBin");
                this.printerObject.PrinterModel.PaperBin = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the paper size for the current printer.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short PaperSize
        {
            get
            {
                return this.printerObject.PrinterModel.PaperSize;
            }
            set
            {
                this.CheckWithinPage("PaperSize");
                this.printerObject.PrinterModel.PaperSize = value;
                this.coordinateSpace.SetMarginBounds(this.printerObject.PrinterModel.MarginBounds);
            }
        }

        public double PaperHeight { get; set; }

        public double PaperWidth { get; set; }

        /// <summary>
        /// Gets or sets a value that determines whether the print output is directed to a printer, to a print preview window, or to a file.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a <see cref="T:System.Drawing.Printing.PrintAction"/> enumeration.
        /// </returns>
        public PrintAction PrintAction
        {
            get
            {
                return this.printerObject.PrintAction;
            }
            set
            {
                this.CheckWithinPage("PrintAction");
                this.printerObject.PrintAction = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that specifies the file name of an Encapsulated PostScript file and the path to which the file will be saved when the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.PrintAction"/> property is set to PrintToFile.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a String that contains a file path and name.
        /// </returns>
        public string PrintFileName
        {
            get
            {
                return this.printerObject.PrintFileName;
            }
            set
            {
                this.CheckWithinPage("PrintFileName");
                this.printerObject.PrintFileName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates the printer resolution.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public short PrintQuality
        {
            get
            {
                return this.printerObject.PrinterModel.PrintQuality;
            }
            set
            {
                this.CheckWithinPage("PrintQuality");
                this.printerObject.PrinterModel.PrintQuality = value;
            }
        }

        /// <summary>
        /// Gets or sets a Boolean value that indicates the text display direction on a right-to-left system.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        public bool RightToLeft
        {
            get
            {
                return this.graphicsFactory.RightToLeft;
            }
            set
            {
                this.graphicsFactory.RightToLeft = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of units for the vertical measurement of the page when you use graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        public float ScaleHeight
        {
            get
            {
                return this.coordinateSpace.ScaleHeight;
            }
            set
            {
                this.coordinateSpace.ScaleHeight = value;
            }
        }

        /// <summary>
        /// Gets or sets the horizontal coordinates for the left edge of the page when you are using graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        public float ScaleLeft
        {
            get
            {
                return this.coordinateSpace.ScaleLeft;
            }
            set
            {
                this.coordinateSpace.ScaleLeft = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the unit of measurement for the coordinates of an object when you are using graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        public int ScaleMode
        {
            get
            {
                return this.coordinateSpace.ScaleMode;
            }
            set
            {
                this.coordinateSpace.ScaleMode = value;
            }
        }

        /// <summary>
        /// Gets or sets the vertical coordinates for the top edge of the page when you are using graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        public float ScaleTop
        {
            get
            {
                return this.coordinateSpace.ScaleTop;
            }
            set
            {
                this.coordinateSpace.ScaleTop = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of units for the horizontal measurement of the page when you use graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        public float ScaleWidth
        {
            get
            {
                return this.coordinateSpace.ScaleWidth;
            }
            set
            {
                this.coordinateSpace.ScaleWidth = value;
            }
        }

        /// <summary>
        /// Gets a value indicating the number of twips per pixel for an object measured horizontally.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public float TwipsPerPixelX
        {
            get
            {
                return this.coordinateSpace.TwipsPerPixelX;
            }
        }

        /// <summary>
        /// Gets a value indicating the number of twips per pixel for an object measured vertically.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public float TwipsPerPixelY
        {
            get
            {
                return this.coordinateSpace.TwipsPerPixelY;
            }
        }

        /// <summary>
        /// Gets or sets the width of a page.
        /// </summary>
        /// 
        /// <returns>
        /// Returns an Integer.
        /// </returns>
        public int Width
        {
            get
            {
                return this.printerObject.PrinterModel.Width;
            }
            set
            {
                this.CheckWithinPage("Width");
                this.printerObject.PrinterModel.Width = value;
                this.coordinateSpace.SetMarginBounds(this.printerObject.PrinterModel.MarginBounds);
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> class.
        /// </summary>
        public FCPrinter()
        {
            this.printerObject = new PrinterObject();
            this.Initialize();
        }

        public FCPrinter(string printerName)
        {
            this.printerObject = new PrinterObject(printerName);
            this.Initialize();
        }

        public FCPrinter(string printerName, bool trackDefault, short orientation)
        {
            this.printerObject = new PrinterObject(printerName);
            this.Initialize();
            //CHE: keep property if already set
            this.TrackDefault = trackDefault;
            this.Orientation = orientation;
        }
        #endregion

        #region Public methods

       

        /// <summary>
        /// get Graphics
        /// </summary>
        /// <returns></returns>
        public Graphics GetGraphics()
        {
            return this.printerObject.GetGraphics();
        }

        /// <summary>
        /// Returns the fontfamily based on integer index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string get_Fonts(int index)
        {
            return this.graphicsFactory.get_Fonts(index);
        }

        /// <summary>
        /// Prints a circle, an ellipse, or an arc on a page.
        /// </summary>
        /// <param name="x">Single value indicating the horizontal coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="y">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Circle(double x, double y, double radius, int color = -1, double startAngle = float.NaN, double endAngle = float.NaN, double aspect = 1f)
        {
            this.Circle(false, x, y, radius, color, startAngle, endAngle, aspect);
        }

        public void Circle(LineDrawStep step1, float x, float y, float radius, int color, float start, float end, float aspect)
        {
            this.Circle(step1 == LineDrawStep.Relative, x, y, radius, color, start, end, aspect);
        }

        /// <summary>
        /// Prints a circle, an ellipse, or an arc on a page, specifying whether the center point is relative to the current location.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the center of the circle, ellipse, or arc is printed relative to the coordinates specified in the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the object.</param><param name="x">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="y">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startangle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void Circle(bool relativeStart, double x, double y, double radius, int color = -1, double startAngle = float.NaN, double endAngle = float.NaN, double aspect = 1f)
        {
            fcGraphics.Circle(this.printerObject.GetGraphics(), relativeStart, (float)x, (float)y, (float)radius, color, (float)startAngle, (float)endAngle, (float)aspect);
        }

        /// <summary>
        /// Ends a print operation sent to the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object, releasing the document to the print device or spooler.
        /// </summary>
        public void EndDoc()
        {
            try
            {
                this.printerObject.EndDoc();
            }
            finally
            {
                this.CurrentX = 0.0f;
                this.CurrentY = 0.0f;
            }
        }

        /// <summary>
        /// Immediately stops the current print job.
        /// </summary>
        public void KillDoc()
        {
            this.printerObject.KillDoc();
        }

        /// <summary>
        /// Prints lines on a page.
        /// </summary>
        /// <param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> property values.</param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> property values.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(double x2, double y2)
        {
            this.Line(true, 0.0f, 0.0f, false, x2, y2, -1, false, false);
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed. </param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed. </param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. You cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(double x1, double y1, double x2, double y2, int color = -1, bool box = false, bool fill = false)
        {
            this.Line(false, x1, y1, false, x2, y2, color, box, fill);
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the starting coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed.</param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed.</param><param name="relativeEnd">Boolean. If this parameter is set to true, the ending coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. you cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public void Line(bool relativeStart, double x1, double y1, bool relativeEnd, double x2, double y2, int color = -1, bool box = false, bool fill = false)
        {
            fcGraphics.LineInternal(printerObject.GetGraphics(), relativeStart, (float)x1, (float)y1, relativeEnd, (float)x2, (float)y2, color, box, fill);
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        /// <param name="Rectangle"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, int Color, LineDrawConstants Rectangle)
        {

            this.Line(X1, Y1, X2, Y2, Color, Rectangle == LineDrawConstants.B || Rectangle == LineDrawConstants.BF, Rectangle == LineDrawConstants.BF);
  
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2)
        {
            Color Color = default(Color);
            Line(StepPos1, X1, Y1, StepPos2, X2, Y2, ColorTranslator.ToOle(Color));
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, int Color)
        {
            LineDrawConstants Rectangle = LineDrawConstants.L;
            Line(StepPos1, X1, Y1, StepPos2, X2, Y2, Color, Rectangle);
        }

        /// <summary>
        /// Stops the printing on the current page and resumes printing on a new page.
        /// </summary>
        public void NewPage()
        {
            this.printerObject.NewPage();
            this.CurrentX = 0.0f;
            this.CurrentY = 0.0f;
        }

        /// <summary>
        /// Prints the contents of an image file on a page.
        /// </summary>
        /// <param name="picture"><see cref="T:System.Drawing.Image"/> value representing the image to be printed.</param><param name="x1">Single value indicating the horizontal destination coordinates where the image will be printed. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measure used.</param><param name="y1">Single value indicating the vertical destination coordinates where the image will be printed. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measure used.</param><param name="width1">Optional. Single value indicating the destination width of the picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If the destination width is larger or smaller than the source width, picture is stretched or compressed to fit. If omitted, the source width is used.</param><param name="height1">Optional. Single value indicating the destination height of the picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If the destination height is larger or smaller than the source height, picture is stretched or compressed to fit. If omitted, the source height is used.</param><param name="x2">Optional. Single values indicating the coordinates (x-axis) of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, 0 is assumed.</param><param name="y2">Optional. Single values indicating the coordinates (y-axis) of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, 0 is assumed.</param><param name="width2">Optional. Single value indicating the source width of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, the entire source width is used.</param><param name="height2">Optional. Single value indicating the source height of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, the entire source height is used.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void PaintPicture(Image objPicture, double intX1, double intY1, double? intWidth1 = null, double? intHeight1 = null, double? intX2 = null, double? intY2 = null, double? intWidth2 = null, double? intHeight2 = null, long? lngOpcode = null)
        {
            if (intWidth1 == null)
            {
                intWidth1 = float.NaN;
            }
            if (intHeight1 == null)
            {
                intHeight1 = float.NaN;
            }
            if (intX2 == null)
            {
                intX2 = 0;
            }
            if (intY2 == null)
            {
                intY2 = 0;
            }
            if (intWidth2 == null)
            {
                intWidth2 = float.NaN;
            }
            if (intHeight2 == null)
            {
                intHeight2 = float.NaN;
            }
            if (lngOpcode == null)
            {
                lngOpcode = 0;
            }
            fcGraphics.PaintPicture(printerObject.GetGraphics(), objPicture, (float)intX1, (float)intY1, (float)intWidth1, (float)intHeight1, (float)intX2, (float)intY2, (float)intWidth2, (float)intHeight2);
        }

        /// <summary>
        /// Prints text to a page.
        /// </summary>
        /// <param name="args">A parameter array containing optional printing parameters.</param>
        public void Print(params object[] args)
        {
            //CHE: use TabStops in report
            fcGraphics.FirstTabOffset = this.FirstTabOffset;
            fcGraphics.TabStops = this.TabStops;
            fcGraphics.Output(printerObject.GetGraphics(), this, true, args);
        }

        /// <summary>
        /// Prints text to a page without adding a carriage return.
        /// </summary>
        /// <param name="args">A parameter array containing optional printing parameters.</param>
        public void Write(params object[] args)
        {
            fcGraphics.Output(printerObject.GetGraphics(), this, false, args);
        }

        /// <summary>
        /// Prints a single point in a specified color on a page.
        /// </summary>
        /// <param name="x">Single value indicating the horizontal coordinates of the point to print.</param><param name="y">Single value indicating the vertical coordinates of the point to print.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color specified for the point. If this parameter is omitted, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ForeColor"/> property setting is used.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void PSet(double x, double y, int color = -1)
        {
            this.PSet(false, (float)x, (float)y, color);
        }

        /// <summary>
        /// Prints a single point in a specified color on a page, optionally specifying a point relative to the current coordinates.
        /// </summary>
        /// <param name="relativeStart">Boolean value indicating whether the coordinates are relative to the current graphics position (as set by <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/>, <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/>).</param><param name="x">Single value indicating the horizontal coordinates of the point to print.</param><param name="y">Single value indicating the vertical coordinates of the point to print.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color specified for the point. If this parameter is omitted, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ForeColor"/> property setting is used.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void PSet(bool relativeStart, double x, double y, int color)
        {
            fcGraphics.PSet(printerObject.GetGraphics(), relativeStart, (float)x, (float)y, color);
        }

        /// <summary>
        /// Returns the height of a text string as it would be printed in the current font.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        /// <param name="text">The String to be measured.</param>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public float TextHeight(string text)
        {
            if (text == null)
                Information.Err().Raise(380);
            if (text.Length == 0)
                text = " ";
            using (Graphics measurementGraphics = this.printerObject.CreateMeasurementGraphics())
            {
                measurementGraphics.PageUnit = GraphicsUnit.Pixel;
                return fcGraphics.TextHeight(measurementGraphics, text, new Rectangle(0, 0, this.Width, this.Height));
            }
        }

        /// <summary>
        /// Returns the width of a text string as it would be printed in the current font.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single
        /// </returns>
        /// <param name="text">The String to be measured.</param>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public float TextWidth(string text)
        {
            if (text == null)
                Information.Err().Raise(380);
            using (Graphics measurementGraphics = this.printerObject.CreateMeasurementGraphics())
            {
                measurementGraphics.PageUnit = GraphicsUnit.Pixel;
                return fcGraphics.TextWidth(measurementGraphics, text, new Rectangle(0, 0, this.Width, this.Height));
            }
        }
        #endregion

        #region Private methods
        private void AddTabToLayout(ref string Layout, ref int CurrentCol, int Column)
        {
            if (Column < 0)
            {
                int num = checked((unchecked(checked(CurrentCol - 1) / 14) + 1) * 14 + 1);
                Layout = Layout + Strings.Space(checked(num - CurrentCol));
                CurrentCol = num;
            }
            else
            {
                int num = Column;
                if (num < 1)
                    num = 1;
                Layout = num < CurrentCol ? Layout + "\r\n" + Strings.Space(checked(num - 1)) : Layout + Strings.Space(checked(num - CurrentCol));
                CurrentCol = num;
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        private void CheckWithinPage(string value)
        {
            if (this.printerObject.State != PrinterObjectState.PaintPage)
                return;
            Information.Err().Raise(396);
        }

        private void Initialize()
        {
            this.graphicsFactory = new GraphicsFactory(true);
            this.coordinateSpace = new CoordinateSpace(this.printerObject.PrinterModel.MarginBounds, this.printerObject.DpiX, this.printerObject.DpiY);
            this.fcGraphics = new FCGraphics(graphicsFactory, coordinateSpace);
        }
        #endregion
    }

    public struct SpcInfo
    {
        /// <summary>
        /// This field supports the Visual Basic Print and PrintLine functions.
        /// </summary>
        /// <filterpriority>1</filterpriority>
        public short Count;
    }

    public struct TabInfo
    {
        /// <summary>
        /// This field supports the Visual Basic Print and PrintLine functions.
        /// </summary>
        /// <filterpriority>1</filterpriority>
        public short Column;
    }
}
