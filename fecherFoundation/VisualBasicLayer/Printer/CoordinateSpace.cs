﻿using System;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace fecherFoundation
{
    public class CoordinateSpace
    {
        private int m_scaleMode;
        private float m_originX;
        private float m_originY;
        private float m_positionX;
        private float m_positionY;
        private float m_userWidth;
        private float m_userHeight;
        private Rectangle m_marginBounds;

        /// <summary>
        /// Gets or sets the horizontal resolution of this Graphics.
        /// </summary>
        public float DpiX { get; set; }

        /// <summary>
        /// Gets or sets the vertical resolution of this Graphics.
        /// </summary>
        public float DpiY { get; set; }

        /// <summary>
        /// Gets or sets the horizontal coordinates for the next printing or drawing method.
        /// </summary>
        public float CurrentX
        {
            get
            {
                return this.FromPixelsX(this.m_positionX);
            }
            set
            {
                this.m_positionX = this.ToPixelsX(value);
            }
        }

        /// <summary>
        /// Gets or sets the vertical coordinates for the next printing or drawing method.
        /// </summary>
        public float CurrentY
        {
            get
            {
                return this.FromPixelsY(this.m_positionY);
            }
            set
            {
                this.m_positionY = this.ToPixelsY(value);
            }
        }

        /// <summary>
        /// Gets or sets the number of units for the vertical measurement of the page when you use graphics methods.
        /// </summary>
        public float ScaleHeight
        {
            get
            {
                if (this.m_scaleMode == 0)
                    return this.m_userHeight;
                else
                    return (float)this.m_marginBounds.Height * this.ZoomY;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                this.CheckOverflow(value);
                if (this.NearZero(value))
                    Information.Err().Raise(380);
                this.ScaleMode = 0;
                this.m_userHeight = value;
            }
        }

        /// <summary>
        /// Gets or sets the horizontal coordinates for the left edge of the page when you are using graphics methods.
        /// </summary>
        public float ScaleLeft
        {
            get
            {
                return this.m_originX;
            }
            set
            {
                this.CheckOverflow(value);
                this.ScaleMode = 0;
                this.m_originX = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the unit of measurement for the coordinates of an object when you are using graphics methods.
        /// </summary>
        public int ScaleMode
        {
            get
            {
                return this.m_scaleMode;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                int currentValue = Convert.ToInt32(value);
                if (currentValue < 0 || currentValue > 7)
                    Information.Err().Raise(380);
                if (currentValue != 0)
                {
                    this.m_originX = 0.0f;
                    this.m_originY = 0.0f;
                    this.m_userWidth = 0.0f;
                    this.m_userHeight = 0.0f;
                }
                else if (this.m_scaleMode != 0)
                {
                    this.m_originX = 0.0f;
                    this.m_originY = 0.0f;
                    this.m_userWidth = this.ScaleWidth;
                    this.m_userHeight = this.ScaleHeight;
                }
                this.m_scaleMode = value;
            }
        }

        /// <summary>
        /// Gets or sets the vertical coordinates for the top edge of the page when you are using graphics methods.
        /// </summary>
        public float ScaleTop
        {
            get
            {
                return this.m_originY;
            }
            set
            {
                this.CheckOverflow(value);
                this.ScaleMode = 0;
                this.m_originY = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of units for the horizontal measurement of the page when you use graphics methods.
        /// </summary>
        public float ScaleWidth
        {
            get
            {
                if (this.m_scaleMode == 0)
                    return this.m_userWidth;
                else
                    return (float)this.m_marginBounds.Width * this.ZoomX;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                this.CheckOverflow(value);
                if (this.NearZero(value))
                    Information.Err().Raise(380);
                this.ScaleMode = 0;
                this.m_userWidth = value;
            }
        }

        /// <summary>
        /// Gets a value indicating the number of twips per pixel for an object measured horizontally.
        /// </summary>
        public float TwipsPerPixelX
        {
            get
            {
                return FCGraphics.TwipsPerInch / this.DpiX;
            }
        }

        /// <summary>
        /// Gets a value indicating the number of twips per pixel for an object measured vertically.
        /// </summary>
        public float TwipsPerPixelY
        {
            get
            {
                return FCGraphics.TwipsPerInch / this.DpiY;
            }
        }

        /// <summary>
        /// Gets the scalemode for the X axis.
        /// </summary>
        private float ZoomX
        {
            get
            {
                return this.get_Zoom(this.m_scaleMode, true);
            }
        }

        /// <summary>
        /// Gets the scalemode for the Y axis.
        /// </summary>
        private float ZoomY
        {
            get
            {
                return this.get_Zoom(this.m_scaleMode, false);
            }
        }

        /// <summary>
        /// CoordinateSpace constructor.
        /// </summary>
        /// <param name="marginBounds">Area that represents the portion of the page inside the margins.</param>
        /// <param name="dpiX">Horizontal resolution.</param>
        /// <param name="dpiY">Vertical resolution.</param>
        public CoordinateSpace(Rectangle marginBounds, float dpiX, float dpiY)
        {
            this.SetMarginBounds(marginBounds);
            this.SetDpi(dpiX, dpiY);
            this.m_scaleMode = 1;
            this.m_positionX = 0.0f;
            this.m_positionY = 0.0f;
            this.m_originX = 0.0f;
            this.m_originY = 0.0f;
            this.m_userWidth = 0.0f;
            this.m_userHeight = 0.0f;
        }

        /// <summary>
        /// Defines the coordinate system of the Printer object.
        /// </summary>
        public void Scale()
        {
            this.ScaleMode = 1;
        }

        /// <summary>
        /// Defines the coordinate system of the Printer object.
        /// </summary>
        /// <param name="x1">Single value indicating the horizontal coordinates that defines the upper-left corner of the object. Parentheses must enclose the values.</param>
        /// <param name="y1">Single value indicating the vertical coordinates that defines the upper-left corner of the object. Parentheses must enclose the values.</param>
        /// <param name="x2">Single value indicating the horizontal coordinates that defines the lower-right corner of the object. Parentheses must enclose the values.</param>
        /// <param name="y2">Single value indicating the vertical coordinates that defines the lower-right corner of the object. Parentheses must enclose the values.</param>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void Scale(float x1, float y1, float x2, float y2)
        {
            this.CheckOverflow(x1);
            this.CheckOverflow(y1);
            this.CheckOverflow(x2);
            this.CheckOverflow(y2);
            if (this.NearZero(x2 - x1))
                Information.Err().Raise(380);
            if (this.NearZero(y2 - y1))
                Information.Err().Raise(380);
            this.m_originX = x1;
            this.m_originY = y1;
            this.m_userWidth = x2 - x1;
            this.m_userHeight = y2 - y1;
            this.m_scaleMode = 0;
        }

        /// <summary>
        /// Compares the top and bottom values, and swaps them if the top is greater than the bottom. 
        /// Similarly, it swaps the left and right values if the left is greater than the right.
        /// This function is useful when dealing with different mapping modes and inverted rectangles.
        /// </summary>
        /// <param name="rect"></param>
        public void NormalizeRectangle(ref Rectangle rect)
        {
            rect.X = Math.Min(rect.X, checked(rect.X + rect.Width));
            rect.Y = Math.Min(rect.Y, checked(rect.Y + rect.Height));
            rect.Width = Math.Abs(rect.Width);
            rect.Height = Math.Abs(rect.Height);
        }

        /// <summary>
        /// Converts the value for the width of a page from one of the units of measure of the ScaleMode property to another.
        /// </summary>
        /// <param name="value">Specify the number of units of measure to be converted.</param>
        /// <param name="fromScale"> A constant or value specifying the coordinate system from which the width of the object is to be converted. The possible values of fromScale are the same as those for the ScaleMode property.</param>
        /// <param name="toScale">A constant or value specifying the coordinate system to which the width of the object is to be converted. The possible values of toScale are the same as those for the ScaleMode property.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public float ScaleX(float value, int fromScale, int toScale)
        {
            return Scale(value, fromScale, toScale, true);
        }

        /// <summary>
        /// Converts the value for the height of a page from one of the units of measure of the ScaleMode property to another.
        /// </summary>
        /// <param name="value">Specify the number of units of measure to be converted.</param>
        /// <param name="fromScale"> A constant or value specifying the coordinate system from which the width of the object is to be converted. The possible values of fromScale are the same as those for the ScaleMode property.</param>
        /// <param name="toScale">A constant or value specifying the coordinate system to which the width of the object is to be converted. The possible values of toScale are the same as those for the ScaleMode property.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public float ScaleY(float value, int fromScale, int toScale)
        {
            return Scale(value, fromScale, toScale, false);
        }

        /// <summary>
        /// Converts to height measurement from pixels.
        /// </summary>
        /// <param name="height">Float.</param>
        /// <returns></returns>
        public float FromPixelsHeight(float height)
        {
            return (float)((double)height * (double)this.ZoomY / ((double)this.DpiY / 100.0));
        }

        /// <summary>
        /// Converts to width measurement from pixels.
        /// </summary>
        /// <param name="width">Float.</param>
        /// <returns></returns>
        public float FromPixelsWidth(float width)
        {
            return (float)((double)width * (double)this.ZoomX / ((double)this.DpiX / 100.0));
        }

        /// <summary>
        /// Converts from pixels to X coordinate.
        /// </summary>
        /// <param name="x">Float.</param>
        /// <returns></returns>
        public float FromPixelsX(float x)
        {
            return this.m_originX + (float)((double)x * (double)this.ZoomX / ((double)this.DpiX / 100.0));
        }

        /// <summary>
        /// Converts from pixels to Y coordinates.
        /// </summary>
        /// <param name="y">Float.</param>
        /// <returns></returns>
        public float FromPixelsY(float y)
        {
            return this.m_originY + (float)((double)y * (double)this.ZoomY / ((double)this.DpiY / 100.0));
        }

        /// <summary>
        /// Sets the horizontal and vertical resolution.
        /// </summary>
        /// <param name="dpiX">Float. Horizontal resolution.</param>
        /// <param name="dpiY">Float. Vertical resolution.</param>
        public void SetDpi(float dpiX, float dpiY)
        {
            this.DpiX = dpiX;
            this.DpiY = dpiY;
        }

        /// <summary>
        /// Sets the rectangular area that represents the portion of the page inside the margins.
        /// </summary>
        /// <param name="marginBounds"></param>
        public void SetMarginBounds(Rectangle marginBounds)
        {
            this.m_marginBounds = marginBounds;
        }

        /// <summary>
        /// Converts to height in pixels from other measurements.
        /// </summary>
        /// <param name="height">Float.</param>
        /// <returns></returns>
        public float ToPixelsHeight(float height)
        {
            float num = (float)((double)height / (double)this.ZoomY * ((double)this.DpiY / 100.0));
            this.CheckOverflow(num);
            return num;
        }

        /// <summary>
        /// Converts to width in pixels from other measurements.
        /// </summary>
        /// <param name="width">Float.</param>
        /// <returns></returns>
        public float ToPixelsWidth(float width)
        {
            float num = (float)((double)width / (double)this.ZoomX * ((double)this.DpiX / 100.0));
            this.CheckOverflow(num);
            return num;
        }

        /// <summary>
        /// Converts into pixel value on X axis of coordinates.
        /// </summary>
        /// <param name="x">Float.</param>
        /// <returns></returns>
        public float ToPixelsX(float x)
        {
            //AM: calculate also for 0
            //if(x==0)
            //{
            //    return 0;
            //}
            float num = (float)(((double)x - (double)this.m_originX) / (double)this.ZoomX * ((double)this.DpiX / 100.0));
            this.CheckOverflow(num);
            return num;
        }

        /// <summary>
        /// Converts into pixel value on Y axis of coordinates.
        /// </summary>
        /// <param name="y">Float.</param>
        /// <returns></returns>
        public float ToPixelsY(float y)
        {
            //AM: calculate also for 0
            //if (y == 0)
            //{
            //    return 0;
            //}
            float num = (float)(((double)y - (double)this.m_originY) / (double)this.ZoomY * ((double)this.DpiY / 100.0));
            this.CheckOverflow(num);
            return num;
        }

        /// <summary>
        /// Checks if number would generate overflow.
        /// </summary>
        /// <param name="value">Float.</param>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        private void CheckOverflow(float value)
        {
            if ((double)Math.Abs(value) <= 2147483648.0)
                return;
            Information.Err().Raise(6);
        }

        /// <summary>
        /// Checks if number is close to zero.
        /// </summary>
        /// <param name="value">Float.</param>
        /// <returns></returns>
        private bool NearZero(float value)
        {
            return (double)Math.Abs(value) < 0.0 * Math.PI;
        }

        /// <summary>
        /// Converts value from one scale to another.
        /// </summary>
        /// <param name="value">Float. Value to scale.</param>
        /// <param name="fromScale">Int. Original scale of value.</param>
        /// <param name="toScale">Int. Target scale of value.</param>
        /// <param name="scaleX">Bool. Scale on X or Y.</param>
        /// <returns></returns>
        private float Scale(float value, int fromScale, int toScale, bool scaleX)
        {
            if (toScale == -1)
                toScale = this.ScaleMode;
            if (fromScale < 0 || fromScale > 8)
                Information.Err().Raise(380);
            if (toScale < 0 || toScale > 8)
                Information.Err().Raise(380);
            if ((fromScale == 0 || toScale == 0) && this.ScaleMode != 0)
                Information.Err().Raise(5);
            this.CheckOverflow(value);
            return value * this.get_Zoom(toScale, scaleX) / this.get_Zoom(fromScale, scaleX);
        }

        /// <summary>
        /// Returns zoom value.
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="zoomX"></param>
        /// <returns></returns>
        private float get_Zoom(int mode, bool zoomX)
        {
            switch (mode)
            {
                case 0:
                    if (zoomX)
                    {
                        return this.m_userWidth / (float)this.m_marginBounds.Width;
                    }
                    else
                    {
                        return this.m_userHeight / (float)this.m_marginBounds.Height;
                    }
                case 1:
                    return 14.4f;
                case 2:
                    return 0.72f;
                case 3:
                    return 0.01f * (zoomX ? this.DpiX : this.DpiY);
                case 4:
                    return (zoomX ? 0.12f : 0.06f);
                case 5:
                    return 0.01f;
                case 6:
                    return 0.254f;
                case 7:
                    return 0.0254f;
                case 8:
                    return 25.4f;
                default:
                    return 0.0f;
            }
        }        
    }
}
