﻿using System.Drawing;
using System.Drawing.Printing;

namespace fecherFoundation
{
    /// <summary>
    /// Properties regarding the printed page settings and image associated with it.
    /// </summary>
    internal class PrintPageInfo
    {
        private PageSettings m_pageSettings;
        private Image m_image;

        /// <summary>
        /// Gets or sets value for the Image property.
        /// </summary>
        public Image Image
        {
            get
            {
                return this.m_image;
            }
            set
            {
                this.m_image = value;
            }
        }

        /// <summary>
        /// Gets or sets value for PageSettings property which contains settings that apply to a single, printed page.
        /// </summary>
        public PageSettings PageSettings
        {
            get
            {
                return this.m_pageSettings;
            }
            set
            {
                this.m_pageSettings = value;
            }
        }
    }
}
