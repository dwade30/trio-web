﻿using System.Collections.Generic;
using System.Drawing.Printing;

namespace fecherFoundation
{
    internal class PlaybackPrintDocument : PrintDocument
    {
        private int m_current;
        private List<PrintPageInfo> m_pageList;

        public PlaybackPrintDocument()
        {
            this.m_pageList = new List<PrintPageInfo>();
        }

        public void AddPage(PrintPageInfo pageInfo)
        {
            this.m_pageList.Add(pageInfo);
        }

        public void Clear()
        {
            List<PrintPageInfo>.Enumerator enumerator = new List<PrintPageInfo>.Enumerator();
            try
            {
                enumerator = this.m_pageList.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    PrintPageInfo current = enumerator.Current;
                    if (current.Image != null)
                        current.Image.Dispose();
                }
            }
            finally
            {
                enumerator.Dispose();
            }
            this.m_pageList.Clear();
        }

        /// <summary>
        /// Occurs when the Print method is called and before the first page of the document prints.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnBeginPrint(PrintEventArgs e)
        {
            if (this.m_pageList.Count == 0)
                e.Cancel = true;
            this.m_current = 0;
        }

        /// <summary>
        /// Occurs when the output to print for the current page is needed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPrintPage(PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(this.m_pageList[this.m_current].Image, 0, 0);
            this.m_current = checked(this.m_current + 1);
            if (this.m_current >= this.m_pageList.Count)
                return;
            e.HasMorePages = true;
        }

        /// <summary>
        /// Occurs immediately before each PrintPage event.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnQueryPageSettings(QueryPageSettingsEventArgs e)
        {
            e.PageSettings = this.m_pageList[this.m_current].PageSettings;
        }
    }
}
