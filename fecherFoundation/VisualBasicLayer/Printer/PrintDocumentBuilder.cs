﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

namespace fecherFoundation
{
    internal class PrintDocumentBuilder
    {
        private PrintPageInfo m_currentPageInfo;
        private PrintPageEventArgs m_currentPrintPageEventArgs;
        private List<PrintPageInfo> m_pageList;
        private PreviewPrintController m_printController;
        private PlaybackPrintDocument m_printDocument;

        /// <summary>
        /// The EndPage function notifies the device that the application has finished writing to a page. This function is typically used to direct the device driver to advance to a new page.
        /// </summary>
        public void EndPage()
        {
            this.m_pageList.Add(this.m_currentPageInfo);
            this.m_printController.OnEndPage((PrintDocument)this.m_printDocument, this.m_currentPrintPageEventArgs);
        }

        /// <summary>
        /// Occurs when the last page of the document has printed.
        /// </summary>
        public void EndPrint()
        {
            this.m_printController.OnEndPrint((PrintDocument)this.m_printDocument, new PrintEventArgs());
            PreviewPageInfo[] previewPageInfo = this.m_printController.GetPreviewPageInfo();
            int num1 = 0;
            int num2 = checked(this.m_pageList.Count - 1);
            int index = num1;
            while (index <= num2)
            {
                PrintPageInfo pageInfo = this.m_pageList[index];
                pageInfo.Image = previewPageInfo[index].Image;
                this.m_printDocument.AddPage(pageInfo);
                checked { ++index; }
            }
        }

        /// <summary>
        /// Gets the document to be printed.
        /// </summary>
        /// <returns></returns>
        public PlaybackPrintDocument GetPrintDocument()
        {
            return this.m_printDocument;
        }

        /// <summary>
        /// Starts the document's printing process.
        /// </summary>
        /// <param name="printerSettings"></param>
        public void StartPrint(PrinterSettings printerSettings)
        {
            this.m_pageList = new List<PrintPageInfo>();
            this.m_printController = new PreviewPrintController();
            this.m_printDocument = new PlaybackPrintDocument();
            this.m_printDocument.PrinterSettings = (PrinterSettings)printerSettings.Clone();
            this.m_printController.OnStartPrint((PrintDocument)this.m_printDocument, new PrintEventArgs());
        }

        /// <summary>
        /// This function prepares the printer driver to accept data.
        /// </summary>
        /// <param name="pageSettings">Specifies settings that apply to a single, printed page.</param>
        /// <param name="marginBounds">Gets the rectangular area that represents the portion of the page inside the margins.</param>
        /// <param name="pageBounds">Gets the rectangular area that represents the total area of the page.</param>
        /// <returns></returns>
        public Graphics StartPage(PageSettings pageSettings, Rectangle marginBounds, Rectangle pageBounds)
        {
            this.m_currentPageInfo = new PrintPageInfo();
            this.m_currentPageInfo.PageSettings = (PageSettings)pageSettings.Clone();
            this.m_currentPrintPageEventArgs = new PrintPageEventArgs((Graphics)null, marginBounds, pageBounds, pageSettings);
            Graphics graphics = this.m_printController.OnStartPage((PrintDocument)this.m_printDocument, this.m_currentPrintPageEventArgs);
            graphics.PageUnit = GraphicsUnit.Pixel;
            return graphics;
        }
    }
}
