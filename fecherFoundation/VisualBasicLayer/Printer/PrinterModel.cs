﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;
using System.Runtime.CompilerServices;

namespace fecherFoundation
{
    internal class PrinterModel
    {
        private PrinterSettings m_printerSettings;
        private PageSettings m_pageSettings;
        private Rectangle m_marginBounds;
        private Rectangle m_pageBounds;

        /// <summary>
        /// Gets or sets a value specifying whether the output will print in color or in monochrome on a device that supports color.
        /// </summary>
        public short ColorMode
        {
            get
            {
                return this.m_pageSettings.Color ? (short)2 : (short)1;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                if (Convert.ToInt32(value) == 2)
                {
                    if (!this.m_printerSettings.SupportsColor)
                        Information.Err().Raise(380);
                    this.m_pageSettings.Color = true;
                }
                else if (Convert.ToInt32(value) == 1)
                    this.m_pageSettings.Color = false;
                else
                    Information.Err().Raise(380);
            }
        }

        /// <summary>
        /// Gets or sets a value that determines the number of copies to be printed.
        /// </summary>
        public short Copies
        {
            get
            {
                return this.m_printerSettings.Copies;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                if (Convert.ToInt32(value) <= 0)
                    Information.Err().Raise(380);
                if (Convert.ToInt32(value) > this.m_printerSettings.MaximumCopies)
                    Information.Err().Raise(380);
                this.m_printerSettings.Copies = value;
            }
        }

        /// <summary>
        /// Gets the name of the current printer.
        /// </summary>
        public string DeviceName
        {
            get
            {
                return this.m_printerSettings.PrinterName;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether a page is printed on both sides(if the printer supports this feature).
        /// </summary>
        public short Duplex
        {
            get
            {
                switch (this.m_printerSettings.Duplex)
                {
                    case System.Drawing.Printing.Duplex.Simplex:
                        return (short)1;
                    case System.Drawing.Printing.Duplex.Vertical:
                        return (short)3;
                    case System.Drawing.Printing.Duplex.Horizontal:
                        return (short)2;
                    default:
                        return (short)1;
                }
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                if (Convert.ToInt32(value) == 1)
                    this.m_printerSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                else if (Convert.ToInt32(value) == 3)
                {
                    if (!this.m_printerSettings.CanDuplex)
                        Information.Err().Raise(380);
                    this.m_printerSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                }
                else if (Convert.ToInt32(value) == 2)
                {
                    if (!this.m_printerSettings.CanDuplex)
                        Information.Err().Raise(380);
                    this.m_printerSettings.Duplex = System.Drawing.Printing.Duplex.Horizontal;
                }
                else
                    Information.Err().Raise(380);
            }
        }

        /// <summary>
        /// Gets or sets the height of a page.
        /// </summary>
        public int Height
        {
            get
            {
                return checked(Convert.ToInt32(Math.Round((double)unchecked((float)this.m_pageBounds.Height * 14.4f))));
            }
            set
            {
                int num = checked(Convert.ToInt32(Math.Round((double)unchecked((float)value / 14.4f))));
                if ((double)num <= (double)this.m_pageSettings.HardMarginY)
                    return;
                this.m_pageSettings.PaperSize = new PaperSize()
                {
                    RawKind = 256,
                    Width = this.m_pageBounds.Width,
                    Height = num
                };
                this.UpdatePageBounds();
            }
        }

        /// <summary>
        /// Returns a value that determines whether the currently selected printer is defined as the default printer in Control Panel.
        /// </summary>
        public bool IsDefaultPrinter
        {
            get
            {
                return this.m_printerSettings.IsDefaultPrinter;
            }
        }

        /// <summary>
        /// Gets the rectangular area that represents the portion of the page inside the margins.
        /// </summary>
        public Rectangle MarginBounds
        {
            get
            {
                return this.m_marginBounds;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether documents are printed in portrait or landscape mode.
        /// </summary>
        public short Orientation
        {
            get
            {
                return this.m_pageSettings.Landscape ? (short)2 : (short)1;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                if (Convert.ToInt32(value) == 2)
                    this.m_pageSettings.Landscape = true;
                else if (Convert.ToInt32(value) == 1)
                    this.m_pageSettings.Landscape = false;
                else
                    Information.Err().Raise(380);
                this.UpdatePageBounds();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the default paper bin on the printer from which paper is fed during print operations.
        /// </summary>
        public short PaperBin
        {
            get
            {
                return checked((short)this.m_pageSettings.PaperSource.RawKind);
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                PaperSource paperSource1 = (PaperSource)null;
                try
                {
                    foreach (PaperSource paperSource2 in this.m_printerSettings.PaperSources)
                    {
                        if (paperSource2.RawKind == Convert.ToInt32(value))
							paperSource1 = paperSource2;
                    }
                }
                finally
                {
                    IEnumerator enumerator = null;
                    if (enumerator is IDisposable)
                        (enumerator as IDisposable).Dispose();
                }
                if (paperSource1 == null)
                    Information.Err().Raise(380);
                this.m_pageSettings.PaperSource = paperSource1;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the paper size for the current printer.
        /// </summary>
        public short PaperSize
        {
            get
            {
                return checked((short)this.m_pageSettings.PaperSize.RawKind);
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                PaperSize paperSize1 = (PaperSize)null;
                try
                {
                    foreach (PaperSize paperSize2 in this.m_printerSettings.PaperSizes)
                    {
                        if (paperSize2.RawKind == Convert.ToInt32(value))
							paperSize1 = paperSize2;
                    }
                    if (paperSize1 == null)
                    {
                        paperSize1 = new PaperSize();
                        paperSize1.Width = this.m_pageBounds.Width;
                        paperSize1.Height = this.m_pageBounds.Height;
                    }
                }
                finally
                {
                    IEnumerator enumerator = null;
                    if (enumerator is IDisposable)
                        (enumerator as IDisposable).Dispose();
                }
                if (paperSize1 == null)
                    Information.Err().Raise(380);
                this.m_pageSettings.PaperSize = paperSize1;
                this.UpdatePageBounds();
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates the printer resolution.
        /// </summary>
        public short PrintQuality
        {
            get
            {
                if (this.m_pageSettings.PrinterResolution.Kind == PrinterResolutionKind.Custom)
                    return checked((short)this.m_pageSettings.PrinterResolution.X);
                else
                    return checked((short)this.m_pageSettings.PrinterResolution.Kind);
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                PrinterResolution printerResolution1 = (PrinterResolution)null;
                try
                {
                    foreach (PrinterResolution printerResolution2 in this.m_printerSettings.PrinterResolutions)
                    {
                        if (Convert.ToInt32(value) < 0)
                        {
                            if (printerResolution2.Kind == (PrinterResolutionKind)value)
                                printerResolution1 = printerResolution2;
                        }
                        else if (printerResolution2.X == Convert.ToInt32(value) | printerResolution2.Y == Convert.ToInt32(value))
							printerResolution1 = printerResolution2;
                    }
                }
                finally
                {
                    IEnumerator enumerator = null;
                    if (enumerator is IDisposable)
                        (enumerator as IDisposable).Dispose();
                }
                if (printerResolution1 == null)
                    Information.Err().Raise(380);
                this.m_pageSettings.PrinterResolution = printerResolution1;
            }
        }

        /// <summary>
        /// Gets the printer that prints the document.
        /// </summary>
        public PrinterSettings PrinterSettings
        {
            get
            {
                return this.m_printerSettings;
            }
        }

        /// <summary>
        /// Gets the page settings for the current page.
        /// </summary>
        public PageSettings PageSettings
        {
            get
            {
                return this.m_pageSettings;
            }
        }

        /// <summary>
        /// Gets the rectangular area that represents the total area of the page.
        /// </summary>
        public Rectangle PageBounds
        {
            get
            {
                return this.m_pageBounds;
            }
        }

        /// <summary>
        /// Gets or sets the width of a page.
        /// </summary>
        public int Width
        {
            get
            {
                return checked(Convert.ToInt32(Math.Round((double)unchecked((float)this.m_pageBounds.Width * 14.4f))));
            }
            set
            {
                int num = checked(Convert.ToInt32(Math.Round((double)unchecked((float)value / 14.4f))));
                if ((double)num <= (double)this.m_pageSettings.HardMarginX)
                    return;
                this.m_pageSettings.PaperSize = new PaperSize()
                {
                    RawKind = 256,
                    Width = num,
                    Height = this.m_pageBounds.Height
                };
                this.UpdatePageBounds();
            }
        }

        /// <summary>
        /// Constructor for the PrinterModel class.
        /// </summary>
        /// <param name="printerName">String. Used to initialize the PrinterName property.</param>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public PrinterModel(string printerName)
        {
            this.m_printerSettings = new PrinterSettings();
            this.m_printerSettings.PrinterName = printerName;
            if (!this.m_printerSettings.IsValid)
                Information.Err().Raise(482);
            this.m_pageSettings = new PageSettings(this.m_printerSettings);
            this.UpdatePageBounds();
        }

        /// <summary>
        /// Updates the current page bounds.
        /// </summary>
        private void UpdatePageBounds()
        {
            this.m_pageBounds = this.m_pageSettings.Bounds;
            RectangleF printableArea = this.m_pageSettings.PrintableArea;
            if (!this.m_pageSettings.Landscape)
                this.m_marginBounds = new Rectangle(checked(Convert.ToInt32(Math.Round((double)printableArea.X))), checked(Convert.ToInt32(Math.Round((double)printableArea.Y))), checked(Convert.ToInt32(Math.Round((double)printableArea.Width))), checked(Convert.ToInt32(Math.Round((double)printableArea.Height))));
            else
                this.m_marginBounds = new Rectangle(checked(Convert.ToInt32(Math.Round((double)printableArea.Y))), checked(Convert.ToInt32(Math.Round((double)printableArea.X))), checked(Convert.ToInt32(Math.Round((double)printableArea.Height))), checked(Convert.ToInt32(Math.Round((double)printableArea.Width))));
        }
    }
}
