﻿using System;
using System.Collections;
using System.Management;
using System.Collections.Generic;
using System.Reflection;

namespace fecherFoundation
{

    public enum PrinterStatus
    {
        Other = 1,
        Unknown,
        Idle,
        Printing,
        Warmup,
        Stopped,
        Offline,
        Degraded
    }

    public class Win32Printer
    {
        public string DriverName = string.Empty;
        public string Location = string.Empty;
        public string Name = string.Empty;
        public bool Network;
        public string PortName = string.Empty;
        public string ServerName = string.Empty;
        public bool Shared;
        public PrinterStatus Status;
        public bool WorkOffline;

        public static List<Win32Printer> GetList()
        {
            string query = "Select * From Win32_Printer";

            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);

            ManagementObjectCollection results = searcher.Get();

            List<Win32Printer> list = new List<Win32Printer>(results.Count);

            foreach (var o in results)
            {
                var obj = (ManagementObject) o;
                Win32Printer entry = new Win32Printer();

                foreach (FieldInfo field in typeof(Win32Printer).GetFields())
                    field.SetValue(entry, ConvertValue(obj[field.Name], field.FieldType));

                list.Add(entry);
            }
            return list;
        }

        private static object ConvertValue(object value, Type type)
        {
            if (value != null)
            {
                if (type == typeof(DateTime))
                {
                    string time = value.ToString();
                    time = time.Substring(0, time.IndexOf("."));
                    return DateTime.ParseExact(time, "yyyyMMddHHmmss", null);
                }
                else if (type == typeof(long))
                    return Convert.ToInt64(value);
                else if (type == typeof(int))
                    return Convert.ToInt32(value);
                else if (type == typeof(short))
                    return Convert.ToInt16(value);
                else if (type == typeof(string))
                    return value.ToString();
                else if (type == typeof(PrinterStatus))
                    return (PrinterStatus)Enum.Parse(typeof(PrinterStatus), value.ToString());
            }
            return null;
        }
    }
}