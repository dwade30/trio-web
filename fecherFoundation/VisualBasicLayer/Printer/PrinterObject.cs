﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Runtime.CompilerServices;
using Wisej.Web;

namespace fecherFoundation
{
    /// <summary>
    /// Provides a Printer object for use by upgraded Visual Basic 6.0 printing code.
    /// </summary>
    internal class PrinterObject
    {
        private float m_dpiX;
        private float m_dpiY;
        private string m_documentName;
        private Graphics m_graphics;
        private int m_pageNumber;
        private PrintAction m_printAction;
        private PrintDocumentBuilder m_printDocumentBuilder;
        private PrinterModel m_printerModel;
        private string m_printerName;
        private string m_printFileName;
        private PrinterObjectState m_state;

        /// <summary>
        /// Gets or sets the document name to display (for example, in a print status dialog box or printer queue) while printing the document.
        /// </summary>
        public string DocumentName
        {
            get
            {
                return this.m_documentName;
            }
            set
            {
                if (value == null)
                    value = "";
                this.m_documentName = value;
            }
        }

        /// <summary>
        /// Gets the horizontal resolution of this Graphics.
        /// </summary>
        public float DpiX
        {
            get
            {
                return this.m_dpiX;
            }
        }

        /// <summary>
        /// Gets the vertical resolution of this Graphics.
        /// </summary>
        public float DpiY
        {
            get
            {
                return this.m_dpiY;
            }
        }

        /// <summary>
        /// Returns the page number of the page that is currently being printed.
        /// </summary>
        public int PageNumber
        {
            get
            {
                return this.m_pageNumber;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the print output is directed to a printer, to a print preview window, or to a file.
        /// </summary>
        public PrintAction PrintAction
        {
            get
            {
                return this.m_printAction;
            }
            set
            {
                if (value != PrintAction.PrintToFile && value != PrintAction.PrintToPreview && value != PrintAction.PrintToPrinter)
                    throw new ArgumentOutOfRangeException("value");
                this.m_printAction = value;
            }
        }

        /// <summary>
        /// Gets the value of the Printer Model.
        /// </summary>
        public PrinterModel PrinterModel
        {
            get
            {
                return this.m_printerModel;
            }
        }

        /// <summary>
        /// Gets or sets a value that specifies the file name of an Encapsulated PostScript file and the path to which the file will be saved when the PrintAction property is set to PrintToFile.
        /// </summary>
        public string PrintFileName
        {
            get
            {
                return this.m_printFileName;
            }
            set
            {
                this.m_printFileName = value;
            }
        }

        /// <summary>
        /// Gets the current state of the PrinterObject (NoPrint, SetupPage, PaintPage).
        /// </summary>
        public PrinterObjectState State
        {
            get
            {
                return this.m_state;
            }
        }

        public string DefaultPrinterName
        {
            get
            {
                return Statics.defaultPrinterName;
            }
            set
            {
                Statics.defaultPrinterName = value;
            }
        }

        /// <summary>
        /// Default constructor using the GetDefaultPrinterName as this.m_printerName.
        /// </summary>
        public PrinterObject()
        {
            this.m_printerName = this.GetDefaultPrinterName();
            this.SetUp();
            this.SetUpOnce();
        }

        /// <summary>
        /// Overloaded constructor that can receive the printer name as parameter.
        /// </summary>
        /// <param name="printerName">String. Passes value to m_printerName.</param>
        public PrinterObject(string printerName)
        {
            this.m_printerName = printerName;
            this.SetUp();
            this.SetUpOnce();
        }

        /// <summary>
        /// Returns a Graphics that contains printer information.
        /// </summary>
        /// <returns></returns>
        public Graphics CreateMeasurementGraphics()
        {
            return this.m_printerModel.PrinterSettings.CreateMeasurementGraphics(this.m_printerModel.PageSettings);
        }

        /// <summary>
        /// get Graphics
        /// </summary>
        /// <returns></returns>
        public Graphics GetGraphics()
        {
            if (this.m_state == PrinterObjectState.NoPrint)
            {
                this.m_printDocumentBuilder.StartPrint(this.m_printerModel.PrinterSettings);
                this.m_state = PrinterObjectState.SetupPage;
            }
            if (this.m_state == PrinterObjectState.SetupPage)
            {
                this.m_graphics = this.m_printDocumentBuilder.StartPage(this.m_printerModel.PageSettings, this.m_printerModel.MarginBounds, this.m_printerModel.PageBounds);
                this.m_state = PrinterObjectState.PaintPage;
            }
            return this.m_graphics;
        }

        /// <summary>
        /// Ends a print operation sent to the Printer object, releasing the document to the print device or spooler.
        /// </summary>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void EndDoc()
        {
            if (this.m_state == PrinterObjectState.NoPrint)
                return;
            if (this.m_state == PrinterObjectState.PaintPage)
                this.m_printDocumentBuilder.EndPage();
            this.m_printDocumentBuilder.EndPrint();
            PlaybackPrintDocument printDocument = this.m_printDocumentBuilder.GetPrintDocument();
            printDocument.DocumentName = this.DocumentName;
            try
            {
                if (this.m_printAction == PrintAction.PrintToFile)
                {
                    if (!this.IsValidFileName(this.m_printFileName))
                        throw new IOException("Printer FileName is invalid");
                }
                try
                {
                    switch (this.m_printAction)
                    {
                        case PrintAction.PrintToFile:
                            printDocument.PrinterSettings.PrintToFile = true;
                            printDocument.PrinterSettings.PrintFileName = this.m_printFileName;
                            new FileStream(this.PrintFileName, FileMode.Create).Close();
                            printDocument.Print();
                            break;
                        case PrintAction.PrintToPreview:
                            // TODO
                            //int num = (int)new PrintPreviewDialog()
                            //{
                            //    Document = ((PrintDocument)printDocument)
                            //}.ShowDialog();
                            break;
                        case PrintAction.PrintToPrinter:
                            printDocument.Print();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Information.Err().Raise(482);
                }
            }
            finally
            {
                printDocument.Clear();
                this.SetUp();
            }
        }

        /// <summary>
        /// Immediately stops the current print job.
        /// </summary>
        public void KillDoc()
        {
            if (this.m_state == PrinterObjectState.NoPrint)
                return;
            if (this.m_state == PrinterObjectState.PaintPage)
                this.m_printDocumentBuilder.EndPage();
            this.m_printDocumentBuilder.EndPrint();
            this.m_printDocumentBuilder.GetPrintDocument().Clear();
            this.SetUp();
        }

        /// <summary>
        /// Stops the printing on the current page and resumes printing on a new page.
        /// </summary>
        public void NewPage()
        {
            if (this.m_state == PrinterObjectState.NoPrint)
            {
                this.m_printDocumentBuilder.StartPrint(this.m_printerModel.PrinterSettings);
                this.m_state = PrinterObjectState.SetupPage;
            }
            if (this.m_state == PrinterObjectState.PaintPage)
                this.m_printDocumentBuilder.EndPage();
            else if (this.m_state == PrinterObjectState.SetupPage)
            {
                this.m_graphics = this.m_printDocumentBuilder.StartPage(this.m_printerModel.PageSettings, this.m_printerModel.MarginBounds, this.m_printerModel.PageBounds);
                this.m_printDocumentBuilder.EndPage();
            }
            this.m_pageNumber = checked(this.m_pageNumber + 1);
            this.m_state = PrinterObjectState.SetupPage;
            this.m_graphics = (Graphics)null;
        }

        /// <summary>
        /// Validates a file name.
        /// </summary>
        /// <param name="fileName">String. File to be validated.</param>
        /// <returns></returns>
        private bool IsValidFileName(string fileName)
        {
            if (fileName == null)
                return false;
            fileName = fileName.Trim();
            if (string.Compare(fileName, "", false) == 0)
                return false;
            bool flag;
            try
            {
                string fullPath = Path.GetFullPath(fileName);
                if (Directory.Exists(fullPath))
                {
                    flag = false;
                    goto label_11;
                }
                else if (fullPath.StartsWith("\\\\.\\", StringComparison.CurrentCulture))
                {
                    flag = false;
                    goto label_11;
                }
            }
            catch (ArgumentException ex)
            {   
                flag = false;
                goto label_11;
            }
            catch (IOException ex)
            {
                flag = false;
                goto label_11;
            }
            return true;
        label_11:
            return flag;
        }

        /// <summary>
        /// Returns the default printer name, if it's invalid it raises an error.
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        private string GetDefaultPrinterName()
        {
            //TODO - default printer will be stored in user settings
            //FC:TEMP:SBE - Harris #97 - Cannot get the Defaultprinter, when application is deployed on IIS and runs with ApplicationPoolIdentity user account
            /*
            PrinterSettings printerSettings = new PrinterSettings();
            if (!printerSettings.IsValid)
                Information.Err().Raise(482);
            return printerSettings.PrinterName;
            */
            //FC:BBE:TEMP:#i700 - Adds a temporary implementation for the default printer if it is null. In future, the default printer should be saved in the user settings.
            //FC:FINAl:SBE - Harris - #334 - get the first printer from Global.Printers instead of showing a printer dialog. In many cases a printer dialog is already displayed later.
            if (FCGlobal.Printers.Count > 0)
            {
                foreach(var printer in FCGlobal.Printers)
                {
                    string port = printer.Win32Printer.PortName;
                    if (port != "nul:" && //send to onenote has this port
                        port != "PORTPROMPT:" && //microsoft xps and microsoft print to pdf has this port
                        port != "SHRFAX:") // fax printer has this port
                    {
                        DefaultPrinterName = printer.DeviceName;
                        break;
                    }
                }
            }
            if (String.IsNullOrEmpty(DefaultPrinterName))
            {
                var printDialog = new FCSelectPrinter();
                var dialogResult = printDialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {

                    DefaultPrinterName = printDialog.PrinterName;
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    Information.Err().Raise(482);
                }
            }

            return DefaultPrinterName;
        }

        private void SetUp()
        {
            this.m_printerModel = new PrinterModel(this.m_printerName);
            this.m_printDocumentBuilder = new PrintDocumentBuilder();
            this.m_pageNumber = 1;
            this.m_state = PrinterObjectState.NoPrint;
        }

        private void SetUpOnce()
        {
            using (Graphics measurementGraphics = this.m_printerModel.PrinterSettings.CreateMeasurementGraphics())
            {
                this.m_dpiX = measurementGraphics.DpiX;
                this.m_dpiY = measurementGraphics.DpiY;
            }
            this.m_printAction = PrintAction.PrintToPrinter;
            this.m_documentName = "document";
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }

        public class StaticVariables
        {
            public string defaultPrinterName = string.Empty;
        }
    }
}
