﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.CompilerServices;
using System.Linq;

namespace fecherFoundation
{
    public class GraphicsFactory
    {
        private bool isPrinter = false;
        private int m_foreColor;
        private short m_drawStyle;
        private short m_drawWidth;
        private int m_fillColor;
        private short m_fillStyle;
        private Font m_font;
        private bool m_rightToLeft;
        private bool m_fontTransparent;

        /// <summary>
        /// Gets or sets a value that determines the line style for output from graphics methods.
        /// </summary>
        public short DrawStyle
        {
            get
            {
                return this.m_drawStyle;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                int minValue = Convert.ToInt32(Enum.GetValues(typeof(DrawStyleConstants)).Cast<DrawStyleConstants>().Min());
                int maxValue = Convert.ToInt32(Enum.GetValues(typeof(DrawStyleConstants)).Cast<DrawStyleConstants>().Max());
                if (Convert.ToInt32(value) < minValue || Convert.ToInt32(value) > maxValue)
                    Information.Err().Raise(380);
                this.m_drawStyle = value;
            }
        }

        /// <summary>
        /// Gets or sets the line width for output from graphics methods.
        /// </summary>
        public short DrawWidth
        {
            get
            {
                return this.m_drawWidth;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                if (Convert.ToInt32(value) <= 0)
                    Information.Err().Raise(380);
                this.m_drawWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets the color that is used to fill in shapes created by using the Circle and Line graphics methods.
        /// </summary>
        public int FillColor
        {
            get
            {
                return this.m_fillColor;
            }
            set
            {
                this.m_fillColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the pattern used to fill shapes created by using the Circle and Line graphics methods.
        /// </summary>
        public short FillStyle
        {
            get
            {
                return this.m_fillStyle;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                if (Convert.ToInt32(value) < 0 || Convert.ToInt32(value) > 7)
                    Information.Err().Raise(380);
                this.m_fillStyle = value;
            }
        }

        /// <summary>
        /// Gets or sets a FontFamily by name
        /// </summary>
        public Font Font
        {
            get
            {
                return this.m_font;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                if (value == null)
                    Information.Err().Raise(380);
                if (!this.m_font.Equals((object)value))
                {
                    m_font.Dispose();
                }
                this.m_font = value.Clone() as Font;
            }
        }

        /// <summary>
        /// Gets or sets the bold font style.
        /// </summary>
        public bool FontBold
        {
            get
            {
                return this.m_font.Bold;
            }
            set
            {
                this.SetFontStyle(FontStyle.Bold, value);
            }
        }

        /// <summary>
        /// Returns the number of fonts available for the current display device or active printer.
        /// </summary>
        public short FontCount
        {
            get
            {
                return checked((short)FontFamily.Families.Length);
            }
        }

        /// <summary>
        /// Gets or sets the italic font style.
        /// </summary>
        public bool FontItalic
        {
            get
            {
                return this.m_font.Italic;
            }
            set
            {
                this.SetFontStyle(FontStyle.Italic, value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the font in which text is displayed for a printing operation.
        /// </summary>
        public string FontName
        {
            get
            {
                return this.m_font.Name;
            }
            set
            {
                try
                {
                    using (FontFamily family = new FontFamily(value))
                    {
                        FontStyle fontStyle1 = this.MakeFontStyle(FontStyle.Regular, FontStyle.Bold, this.FontBold);
                        if (!family.IsStyleAvailable(fontStyle1))
                            fontStyle1 = this.MakeFontStyle(fontStyle1, FontStyle.Bold, !this.FontBold);
                        FontStyle fontStyle2 = this.MakeFontStyle(fontStyle1, FontStyle.Italic, this.FontItalic);
                        if (!family.IsStyleAvailable(fontStyle2))
                            fontStyle2 = this.MakeFontStyle(fontStyle2, FontStyle.Italic, !this.FontItalic);
                        FontStyle fontStyle3 = this.MakeFontStyle(fontStyle2, FontStyle.Underline, this.FontUnderline);
                        if (!family.IsStyleAvailable(fontStyle3))
                            fontStyle3 = this.MakeFontStyle(fontStyle3, FontStyle.Underline, !this.FontUnderline);
                        FontStyle fontStyle4 = this.MakeFontStyle(fontStyle3, FontStyle.Strikeout, this.FontStrikethru);
                        if (!family.IsStyleAvailable(fontStyle4))
                            fontStyle4 = this.MakeFontStyle(fontStyle4, FontStyle.Strikeout, !this.FontStrikethru);
                        this.m_font.Dispose();
                        this.m_font = FCUtils.CreateFont(family, this.FontSize, fontStyle4, isPrinter);
                    }
                }
                catch (ArgumentException ex)
                {
                    this.m_font.Dispose();
                    this.m_font = (Font)SystemFonts.DefaultFont.Clone();
                }
            }
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        public float FontSize
        {
            get
            {
                return this.m_font.SizeInPoints;
            }
            [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
            set
            {
                if ((double)value <= 0.0001)
                    Information.Err().Raise(380);
                this.m_font.Dispose();
                this.m_font = FCUtils.CreateFont(this.m_font.Name, value, this.m_font.Style, isPrinter);
            }
        }

        /// <summary>
        /// Gets or sets the strikethrough font style.
        /// </summary>
        public bool FontStrikethru
        {
            get
            {
                return this.m_font.Strikeout;
            }
            set
            {
                this.SetFontStyle(FontStyle.Strikeout, value);
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether background graphics on a Printer object are printed behind text characters.
        /// </summary>
        public bool FontTransparent
        {
            get
            {
                return this.m_fontTransparent;
            }
            set
            {
                this.m_fontTransparent = value;
            }
        }

        /// <summary>
        /// Gets or sets the underlined font style.
        /// </summary>
        public bool FontUnderline
        {
            get
            {
                return this.m_font.Underline;
            }
            set
            {
                this.SetFontStyle(FontStyle.Underline, value);
            }
        }

        /// <summary>
        /// Gets or sets the color in which text and graphics are printed.
        /// </summary>
        public int ForeColor
        {
            get
            {
                return this.m_foreColor;
            }
            set
            {
                this.m_foreColor = value;
            }
        }

        /// <summary>
        /// Gets or sets a Boolean value that indicates the text display direction on a right-to-left system.
        /// </summary>
        public bool RightToLeft
        {
            get
            {
                return this.m_rightToLeft;
            }
            set
            {
                this.m_rightToLeft = value;
            }
        }

        /// <summary>
        /// GraphicsFactory constructor.
        /// </summary>
        /// <param name="isPrinter"></param>
        public GraphicsFactory(bool isPrinter = false)
        {
            this.isPrinter = isPrinter;
            this.m_foreColor = 0;
            this.m_drawStyle = (short)0;
            this.m_drawWidth = (short)1;
            this.m_fillColor = 0;
            this.m_fillStyle = (short)1;
            this.m_font = (Font)SystemFonts.DefaultFont.Clone();
            this.m_rightToLeft = false;
            this.m_fontTransparent = true;
        }

        /// <summary>
        /// Returns the FontFamily name based on the index supplied.
        /// </summary>
        /// <param name="index">Integer.</param>
        /// <returns></returns>
        public string get_Fonts(int index)
        {
            return FontFamily.Families[index].Name;
        }

        /// <summary>
        /// Creates a simple brush, which paints in a solid color.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public Brush CreateDrawBrush(int color)
        {
            return (Brush)new SolidBrush(ColorTranslator.FromOle(color));
        }

        /// <summary>
        /// Creates a simple pen used to draw lines, curves, and to outline shapes.
        /// </summary>
        /// <param name="color">Integer for choosing color.</param>
        /// <returns></returns>
        public Pen CreateDrawPen(int color)
        {
            Pen pen = new Pen(ColorTranslator.FromOle(color), (float)this.DrawWidth);
            SetPen(pen);
            return pen;
        }

        /// <summary>
        /// Creates a simple pen used to draw lines, curves, and to outline shapes.
        /// </summary>
        /// <param name="color">Color type for choosing predefined colors.</param>
        /// <returns></returns>
        public Pen CreateDrawPen(Color color)
        {
            Pen pen = new Pen(color, (float)this.DrawWidth);
            SetPen(pen);
            return pen;
        }

        /// <summary>
        /// Creates a HatchBrush. Similar to a SolidBrush, but it allows you to select from a large variety of preset patterns to paint with, rather than a solid color.
        /// </summary>
        /// <returns></returns>
        public Brush CreateFillBrush()
        {
            Color color = Convert.ToInt32(this.FillStyle) != 1 ? ColorTranslator.FromOle(this.FillColor) : Color.Transparent;
            Brush brush;
            if (Convert.ToInt32(this.FillStyle) == 0 || Convert.ToInt32(this.FillStyle) == 1)
            {
                brush = (Brush)new SolidBrush(color);
            }
            else
            {
                HatchStyle hatchstyle = new HatchStyle();
                switch (this.FillStyle)
                {
                    case (short)2:
                        hatchstyle = HatchStyle.Horizontal;
                        break;
                    case (short)3:
                        hatchstyle = HatchStyle.Vertical;
                        break;
                    case (short)4:
                        hatchstyle = HatchStyle.ForwardDiagonal;
                        break;
                    case (short)5:
                        hatchstyle = HatchStyle.BackwardDiagonal;
                        break;
                    case (short)6:
                        hatchstyle = HatchStyle.Cross;
                        break;
                    case (short)7:
                        hatchstyle = HatchStyle.DiagonalCross;
                        break;
                }
                brush = (Brush)new HatchBrush(hatchstyle, color, Color.Transparent);
            }
            return brush;
        }

        /// <summary>
        /// Returns stringFormat based on FormatFlags.
        /// </summary>
        /// <returns></returns>
        public StringFormat CreateStringFormat()
        {
            StringFormat stringFormat = (StringFormat)StringFormat.GenericTypographic.Clone();
            stringFormat.FormatFlags = !this.m_rightToLeft ? stringFormat.FormatFlags & ~StringFormatFlags.DirectionRightToLeft : stringFormat.FormatFlags | StringFormatFlags.DirectionRightToLeft;
            return stringFormat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fontStyle"></param>
        /// <param name="fontStyleBit"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private FontStyle MakeFontStyle(FontStyle fontStyle, FontStyle fontStyleBit, bool value)
        {
            if (value)
                fontStyle |= fontStyleBit;
            else
                fontStyle &= ~fontStyleBit;
            return fontStyle;
        }

        /// <summary>
        /// Sets the current fontStyle.
        /// </summary>
        /// <param name="fontStyleBit"></param>
        /// <param name="value"></param>
        private void SetFontStyle(FontStyle fontStyleBit, bool value)
        {
            FontStyle style = this.MakeFontStyle(this.m_font.Style, fontStyleBit, value);
            if (!this.m_font.FontFamily.IsStyleAvailable(style))
                return;
            this.m_font.Dispose();
            this.m_font = FCUtils.CreateFont(this.FontName, this.FontSize, style, isPrinter);
        }

        /// <summary>
        /// After pen creation set it's remaining properties. The DashStyle property allows you to choose between solid, dotted, dashed,
        ///  or custom dashed lines, and the DashCap property allows you to customize the ends of the dashes in your lines.
        /// </summary>
        /// <param name="pen"></param>
        private void SetPen(Pen pen)
        {
            if (Convert.ToInt32(this.DrawWidth) != 1)
			{
                pen.DashStyle = DashStyle.Solid;
            }
            else
            {
                switch (this.DrawStyle)
                {
                    case (short)0:
                        pen.DashStyle = DashStyle.Solid;
                        break;
                    case (short)1:
                        pen.DashStyle = DashStyle.Dash;
                        break;
                    case (short)2:
                        pen.DashStyle = DashStyle.Dot;
                        break;
                    case (short)3:
                        pen.DashStyle = DashStyle.DashDot;
                        break;
                    case (short)4:
                        pen.DashStyle = DashStyle.DashDotDot;
                        break;
                }
            }
            pen.StartCap = LineCap.Round;
            pen.EndCap = LineCap.Round;
        }
    }
}
