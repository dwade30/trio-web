﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{

    public class ElementAccess<T> : DynamicObject
    {
        T[] array;
        int index;

        object element;
        Type elementType;

        public ElementAccess(T[] a, int i)
        {
            this.array = a;
            this.index = i;

            elementType = array.GetValue(i).GetType();
        }

        //public ElementAccess(object e)
        //{
        //    element = e;

        //    elementType = e.GetType();
        //}

        public override bool TryConvert(ConvertBinder binder, out object result)
        {
            if(binder.Type.IsAssignableFrom(elementType))
            {
                result = Convert.ChangeType(array[index], binder.Type);
                return true;
            }
            return base.TryConvert(binder, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            var ptr = __makeref(array[index]);

            var fi = elementType.GetField(binder.Name);
            if (fi != null)
            {
                value = Convert.ChangeType(value, fi.FieldType);

                //fi.SetValue(array[index], value);
                fi.SetValueDirect(ptr, value);
                return true;
            }
            return false;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;

            var fi = elementType.GetField(binder.Name);
            if (fi != null)
            {
                result = fi.GetValue(array[index]);
                return true;
            }
            return false;
        }

    }


    public class FCArray<T> : IEnumerable<T>
    {
        private T[] array = null;
        private int min = 0;
        private int max = 0;

        public FCArray(ref T[] array, int lowerBound, int upperBound)
        {
            if (array == null)
            {
                array = new T[(upperBound - lowerBound) + 1];
            }
            this.array = array;
            this.min = lowerBound;
            this.max = upperBound;
        }


        public T[] GetArray()
        {
            return this.array;
        }


        public dynamic this[int i]
        {
            get
            {
                if (i < min || i > max)
                    throw new IndexOutOfRangeException();

                //return new ElementAccess<T>(this.array[i]);

                if (this.array[i - min].GetType().IsPrimitive)
                {
                    return this.array[i - min];
                }
                else
                {
                    return new ElementAccess<T>(this.array, i - min);
                }
            }
            set
            {
                if (i < min || i > max)
                    throw new IndexOutOfRangeException();

                this.array[i - min] = value;
            }
        }

        public int LowerBound
        {
            get { return this.min; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)array).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)array).GetEnumerator();
        }
    }
}
