﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// Values indicating permission for modifying data in a Connection, Record, or Stream object.
    /// </summary>
    public enum ConnectModeEnum
    {
        adModeUnknown = 0,
        adModeRead = 1,
        adModeWrite = 2,
        adModeReadWrite = 3,
        adModeShareDenyRead = 4,
        adModeShareDenyWrite = 8,
        adModeShareExclusive = 12,
        adModeShareDenyNone = 16,
        adModeRecursive = 0x400000
    }
}
