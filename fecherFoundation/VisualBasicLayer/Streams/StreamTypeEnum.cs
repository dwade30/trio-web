﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// Specifies the type of data stored in a Stream object.
    /// </summary>
    public enum StreamTypeEnum
    {
        adTypeBinary = 1,
        adTypeText = 2

    }
}
