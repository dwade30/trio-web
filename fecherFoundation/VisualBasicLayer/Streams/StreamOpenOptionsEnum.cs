﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// Specifies options for opening a Stream object. The values can be combined with an OR operation.
    /// </summary>
    public enum  StreamOpenOptionsEnum
    {
        adOpenStreamUnspecified = -1,
        adOpenStreamAsync = 1,
        adOpenStreamFromRecord = 4
    }
}
