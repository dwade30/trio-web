﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// Specifies whether the whole stream or the next line should be read from a Stream object.
    /// </summary>
    public enum StreamReadEnum
    {
        adReadAll = -1,
        adReadLine = -2
    }
}
