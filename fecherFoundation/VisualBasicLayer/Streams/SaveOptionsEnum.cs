﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// Specifies whether a file should be created or overwritten when saving from a Stream object. The values can be adSaveCreateNotExist or adSaveCreateOverWrite.
    /// </summary>
    public enum SaveOptionsEnum
    {
        adSaveCreateNotExist = 1,
        adSaveCreateOverWrite = 2
    }
}
