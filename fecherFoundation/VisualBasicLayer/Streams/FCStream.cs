﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace fecherFoundation
{

    /// <summary>
    /// FecherFoundation replacement for ADO stream object VB6
    /// comments from http://www.w3schools.com/asp/ado_ref_stream.asp
    /// 
    /// The ADO Stream Object is used to read, write, and manage a stream of binary data or text.
    ///
    /// </summary>
    public class FCStream : IDisposable
    {

        private MemoryStream stream;

        private StreamTypeEnum type = StreamTypeEnum.adTypeText;
        /// <summary>
        /// The Position property sets or returns a long value that indicates the current position (in bytes) from the beginning of a Stream object.
        /// </summary>
        public long Position 
        {
            get { return stream == null ? 0 : stream.Position; }
            set { if (stream != null) stream.Position = value; }
        }

        /// <summary>
        /// The Size property returns a long value that indicates the size (in bytes) of a Stream object.
        /// 
        /// Note: This property can only be used on an open Stream object.
        /// Note: This property returns -1 if the size of the stream is unknown.
        /// </summary>
        public long Size 
        {
            get { return stream == null ? 0 : stream.Length; }
        }

        /// <summary>
        /// The Type property sets or returns a StreamTypeEnum value that specifies the type of data in a Stream object. Default is adTypeText.
        /// Note: This property is read/write when the position is at the beginning of the Stream (Position=0), otherwise it is read-only.
        /// </summary>
        public StreamTypeEnum Type 
        {
            get 
            {
                return type; 
            }
            set 
            {
                if (Position == 0 && (value != type))
                {
                    type = value;
                }
            }
        }

        /// <summary>
        /// Copies the specified number of characters or bytes (depending on Type) in the Stream to another Stream object.
        /// </summary>
        /// <param name="destStream">Required. Where to copy the Stream (contains a reference to an open Stream object)</param>
        /// <param name="charNumber">Optional. An integer that specifies the number of characters or bytes to be copied from the current position in the source Stream to the destination Stream. Default is -1 (will copy all data from the current position to EOS)
        /// Note: If the specified number is greater than the available number of bytes/characters until EOS, then only bytes/characters from the current position to EOS are copied</param>
        public void CopyTo(FCStream destStream, int charNumber = -1)
        {
            EnsureStream();

            if(charNumber == -1)
            {
                stream.CopyTo(destStream.stream);
            }
            else
            {
                //FC:TODO copy more effective 
                int b = 0;
                for (int i = 0; (i < charNumber) && (b != -1); i++ )
                {
                    b = stream.ReadByte();
                    if(b != -1)
                        destStream.stream.WriteByte((byte)b);
                }
            }

        }

        /// <summary>
        /// The Close method is used to close a Connection object, a Record object, a Recordset object, or a Stream object to free system resources. 
        /// 
        /// Note: When an object is closed, it will not be removed from the memory. It is possible to change the property settings and open it again later.  
        /// </summary>
        public void Close()
        {
            if (stream != null)
            {
                stream.Close();
                stream = null;
            }
        }

        /// <summary>
        /// The Open method is used to open a Stream object.
        /// 
        /// Note: Currently only a call with default parameters is supported ! Just call .Open()
        /// </summary>
        /// <param name="source"></param>
        /// <param name="mode">Optional. A  ConnectModeEnum value that specifies the access mode for a Stream object. Default is adModeUnknown </param>
        /// <param name="opt">Optional. A  StreamOpenOptionsEnum value that specifies options for opening a Stream object. Default is adOpenStreamUnspecified </param>
        /// <param name="username">Optional. A name of a user who can access the Stream object. If Source is an already opened Record, this parameter is not specified </param>
        /// <param name="psword"> Optional. A password that validates the username. If Source is an already opened Record, this parameter is not specified </param>
        public void Open(
                        object source = null, 
                        ConnectModeEnum mode = ConnectModeEnum.adModeUnknown, 
                        StreamOpenOptionsEnum opt = StreamOpenOptionsEnum.adOpenStreamUnspecified,
                        string username = null,
                        string psword = null
        )
        {
            if (source != null || mode != ConnectModeEnum.adModeUnknown || opt != StreamOpenOptionsEnum.adOpenStreamUnspecified || username != null || psword != null)
                throw new NotImplementedException("FCStream.Open at the moment we only can handle default values for parameter, just call Open()");

            stream = new MemoryStream();



        }

        /// <summary>
        /// The Read method is used to read the entire stream or a specified number of bytes from a binary Stream object and returns the resulting data as a variant.
        /// Note: This method is only used with binary Stream objects, for text Stream objects, use the ReadText method.
        /// </summary>
        /// <param name="numbytes">Optional. The number of bytes to read from the file, or a StreamReadEnum value. Default is adReadAll
        /// If you have specified more than the number of bytes left in the Stream, only the bytes remaining are returned.</param>
        /// <returns></returns>
        public object Read(long numbytes = (long)StreamReadEnum.adReadAll)
        {
            EnsureStream();
            
            if (Type != StreamTypeEnum.adTypeBinary)
            {
                throw new NotSupportedException("method is only supported by stream with type adTypeBinary");
            }

            long bytesLeft = (stream.Length - stream.Position);

            if(numbytes == (long)StreamReadEnum.adReadAll)
            {
                numbytes = bytesLeft;
            }
            if(numbytes > bytesLeft)
            {
                numbytes = bytesLeft;
            }

            byte[] buffer = new byte[numbytes];

            stream.Read(buffer, 0, Convert.ToInt32(numbytes));
            
            return buffer;
        }


        /// <summary>
        /// The Write method is used to write binary data to a binary Stream object.
        /// 
        /// If there is data in the Stream object and the current position is EOS, the new data will be appended beyond the existing data. If the current position is not EOS, the existing data will be overwritten. 
        /// If you write past EOS, the size of the Stream will increase. EOS will be set to the last byte in the Stream. If you don't write past EOS, the current position will be set to the next byte after the newly added data. Previously existing data will not be truncated. Call the SetEOS method to truncate. 
        /// Note: This method is used only with binary Stream objects, for textStream objects, use the WriteText method.
        /// </summary>
        /// <param name="buffer">Required. An array of bytes to be written to a binary Stream object</param>
        public void Write(byte[] buffer)
        {
            EnsureStream();

            if(Type != StreamTypeEnum.adTypeBinary)
            {
                throw new NotSupportedException("method is only supported by stream with type adTypeBinary");
            }
            stream.Write(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// The SaveToFile method is used to save the binary contents of an open Stream object to a local file.
        /// Note: After a call to this method, the current position in the stream is set to the beginning of the stream (Position=0).
        /// </summary>
        /// <param name="filename">Required. The name of the file to save the contents of the Stream object</param>
        /// <param name="option">Optional. A SaveOptionsEnum value that specifies whether a file should be created if it does not exist or overwritten. Default is adSaveCreateNotExist.</param>
        public void SaveToFile(string filename, SaveOptionsEnum option = SaveOptionsEnum.adSaveCreateNotExist)
        {
            EnsureStream();
            
            if(option == SaveOptionsEnum.adSaveCreateNotExist && File.Exists(filename))
            {
                throw new IOException(String.Format("File '{0}' already exists. Cannot overwrite with option adSaveCreateNotExist", filename));
            }

            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write))
            {
                byte[] bytes = new byte[stream.Length];
                bytes = stream.ToArray();
                fs.Write(bytes, 0, bytes.Length);
                //stream.CopyTo(fs);
                fs.SetLength(stream.Length);
            }

            this.Position = 0;
        }


        /// <summary>
        /// The LoadFromFile method is used to load the contents of an existing file into an open Stream object. All existing data in the Stream object will be overwritten.
        /// 
        /// Note: After a call to this method, the current position is set to the beginning of the Stream (Position=0).
        /// </summary>
        /// <param name="filename">Required. The name of an existing file to be loaded into a the Stream object</param>
        public void LoadFromFile(string filename)
        {
            EnsureStream();

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                fs.CopyTo(stream);
            }

            this.Position = 0;
        }



        #region IDisposable interface

        public void Dispose()
        {
            if(stream != null) stream.Dispose();
        }

        #endregion

        #region private methods

        /// <summary>
        /// Check if stream exists, if null throw exception.
        /// </summary>
        private void EnsureStream()
        {
            if (stream == null)
                throw new ObjectDisposedException(this.ToString(), "object is closed");
        }

        #endregion
    }
}
