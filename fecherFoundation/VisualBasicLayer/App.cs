﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public static class App
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        /// <summary>
        /// Store temporary updated title, not persisted.
        /// </summary>
        private static string mstrTitle = null;

        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the application title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public static string Title
        {
            get
            {
                // Null reference check
                if (mstrTitle == null)
                {
                    // Initialize title 
                    mstrTitle = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
                }

                // Return title
                return mstrTitle;
            }
            set
            {
                // Set title
                mstrTitle = value;
            }
        }

        /// <summary>
        /// HelpFile
        /// </summary>
        public static string HelpFile
        {
            get;
            set;
        }

        public static int hInstance
        {
            get;
            set;
        }

        /// <summary>
        /// ThreadID
        /// </summary>
        public static int ThreadID
        {
            get
            {
                return AppDomain.GetCurrentThreadId();
            }
        }

        /// <summary>
        /// Gets the value of the major component of the version number for the current System.Version object.
        /// </summary>
        public static int Major
        {
            get
            {
                return System.Reflection.Assembly.GetCallingAssembly().GetName().Version.Major;
            }
        }

        /// <summary>
        /// Gets the value of the minor component of the version number for the current System.Version object.
        /// </summary>
        public static int Minor
        {
            get
            {
                return System.Reflection.Assembly.GetCallingAssembly().GetName().Version.Minor;
            }
        }

        /// <summary>
        /// Gets the value of the revision component of the version number for the current System.Version object.
        /// </summary>
        public static int Revision
        {
            get
            {
                return System.Reflection.Assembly.GetCallingAssembly().GetName().Version.Revision;
            }
        }

        /// <summary>
        /// Returns the name of the executable file for the current project.  If running in the development environment, returns the name of the project.
        /// </summary>
        public static string EXEName
        {
            get
            {
                return System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            }
        }

        public static string AssemblyTitle
        {
            get
            {
                var assemblyTitle= System.Reflection.Assembly.GetCallingAssembly().GetCustomAttributes(typeof(System.Reflection.AssemblyTitleAttribute), false).ToArray();
                return (assemblyTitle[0] as System.Reflection.AssemblyTitleAttribute).Title;
            }
        }

        public static string ProductName
        {
            get
            {
                return System.Reflection.Assembly.GetCallingAssembly().GetName().FullName;
            }
        }

        public static FCMainForm MainForm
        {
            get
            {
                return FCMainForm.InstancePtr;
            }
        }

        public static int LogMode { get; set; }
        public static string Path
        {
            get
            {
                return Wisej.Web.Application.StartupPath;
            }

        }


        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Logs an event in the application's log target
        /// </summary>
        /// <param name="logBuffer"></param>
        /// <param name="eventType"></param>
        public static void LogEvent(string logBuffer, object eventType = null)
        {
            string sSource;
            string sLog;
            string sEvent;

            sSource = App.Title;
            sLog = logBuffer;
            sEvent = Convert.ToString(eventType);

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, sEvent);
            EventLog.WriteEntry(sSource, sEvent,
                EventLogEntryType.Warning, 234);
        }
    

        /// <summary>
        /// Check if previous instance running.
        /// </summary>
        /// <returns><c>True</c> if previous instance running, otherwise <c>false</c>.</returns>
        public static bool PrevInstance()
        {
            // Get current process name
            string strCurrentProcessName = Process.GetCurrentProcess().ProcessName;

            // Get all running processes with this name
            Process[] arrProcesses = Process.GetProcessesByName(strCurrentProcessName);

            // Check processes number
            return (arrProcesses.Length > 1);
        }

        // TODO
        public static void DoEvents()
        {
        }
        
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
