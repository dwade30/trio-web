﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Wisej.Web;

namespace fecherFoundation.VisualBasicLayer
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{FileStream}")]
    public class FCFileHandle : ISerializable, IXmlSerializable
    {
        // null value
        public readonly static FCFileHandle Null = new FCFileHandle();
	    // internal FCFile instance
        [NonSerialized]
        private FCFile m_fcFile;
		private string currentString = "";
        private char lastChar;

        #region Properties

        /// <summary>
        /// Checks if this FCFileHandle object contains a null value.
        /// </summary>
        /// <returns>bool</returns>
        public bool IsNull
        {
            [DebuggerStepThrough]
            get { return m_fcFile == null; }
        }

        /// <summary>
        /// Returns true if the file was opened in binary mode.
        /// </summary>
        public bool IsBinary
        {
            get
            {
                if (IsNull)
                    return false;

                return m_fcFile.IsBinary;
            }
        }

        /// <summary>
        /// Returns the wrapped FileStream object.
        /// </summary>
        public FileStream FileStream
        {
            get
            {
                if (IsNull)
                    return null;

                return m_fcFile.FileStream;
            }
        }

        public char LastChar
        {
            get
            {
                return lastChar;
            }
        }

        #endregion

        #region Sal Functions

        /// <summary>
        /// Closes a file.
        /// </summary>
        /// <returns>bOk</returns>
        public bool Close()
        {
            if (!IsNull)
            {
                try
                {
					if (!string.IsNullOrEmpty(currentString))
					{
						PutString(currentString, false);
					}
                    m_fcFile.Close();
                    return true;
                }
                catch (IOException e)
                {
                }
                finally
                {
                    m_fcFile = null;
                }
            }

            return false;
        }

        /// <summary>
        /// deletes a file.
        /// </summary>
        /// <param name="fileName">The name of the file to delete or test.</param>
        /// <param name="flags">A constant that specifies the style in which to open the file. nStyle can be one or more styles combined using the bitwise OR (|) operator.</param>
        /// <returns>bOk</returns>
        public bool Open(string fileName, int flags)
        {
            if (fileName == null || fileName.Length == 0)
                return false;

            try
            {
                // execute flags that require a single
                // operation and don't really open the file
                if ((flags & Sys.OF_Delete) == Sys.OF_Delete)
                {
                    bool exists = File.Exists(fileName);
                    File.Delete(fileName);
                    return exists;
                }

                if ((flags & Sys.OF_Parse) == Sys.OF_Parse)
                {
                    File.GetAttributes(fileName);
                    return true;
                }

                if ((flags & Sys.OF_Exist) == Sys.OF_Exist)
                {
                    return File.Exists(fileName) || Directory.Exists(fileName);
                }
     
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Opens, re-opens, creates a file.
        /// </summary>
        /// <param name="fileName">The name of the file to open, create</param>
        /// <param name="flags">A constant that specifies the style in which to open the file. nStyle can be one or more styles combined using the bitwise OR (|) operator.</param>
        /// <returns>bOk</returns>
        public bool Open(string fileName, OpenMode Mode, OpenAccess Access, OpenShare Share)
        {
            // detect the encoding
            // NOTE: CTD doesn't encode anything, it's only ASCII
            Encoding encoding = Encoding.Default;
            //if ((flags & Sys.OF_Unicode) == Sys.OF_Unicode)
            //    encoding = Encoding.Unicode;
            //else if ((flags & Sys.OF_UTF7) == Sys.OF_UTF7)
            //    encoding = Encoding.UTF7;
            //else if ((flags & Sys.OF_UTF8) == Sys.OF_UTF8)
            //    encoding = Encoding.UTF8;

            return Open(fileName, Mode, Access, Share, encoding);
        }

        /// <summary>
        /// Opens, re-opens, creates a file.
        /// </summary>
        /// <param name="fileName">The name of the file to open, create.</param>
        /// <param name="flags">A constant that specifies the style in which to open the file. nStyle can be one or more styles combined using the bitwise OR (|) operator.</param>
        /// <param name="encoding">Encoder used to read/write characters.</param>
        /// <returns>bOk</returns>
        public bool Open(string fileName, OpenMode Mode, OpenAccess Access, OpenShare Share, Encoding encoding)
        {
            if (fileName == null || fileName.Length == 0)
                return false;
            //FC:FINAL:AM: use the UserData folder if no folder is specified
            if (!Path.IsPathRooted(fileName))
            {
                fileName = Path.Combine(FCFileSystem.Statics.UserDataFolder, fileName);
            }

            try
            {
                // execute flags that require a single
                // operation and don't really open the file
                //if ((flags & Sys.OF_Delete) == Sys.OF_Delete)
                //{
                //    bool exists = File.Exists(fileName);
                //    File.Delete(fileName);
                //    return exists;
                //}

                //if ((flags & Sys.OF_Parse) == Sys.OF_Parse)
                //{
                //    File.GetAttributes(fileName);
                //    return true;
                //}

                //if ((flags & Sys.OF_Exist) == Sys.OF_Exist)
                //{
                //    return File.Exists(fileName) || Directory.Exists(fileName);
                //}

                FileAccess access = FileAccess.Read;
                if (Access == OpenAccess.Write)
                    access = FileAccess.Write;
                else if (Mode == OpenMode.Append)
                    access = FileAccess.Write;
                else if (Access == OpenAccess.ReadWrite)
                    access = FileAccess.ReadWrite;
                else if (Access == OpenAccess.Default)
                    access = FileAccess.ReadWrite;

                FileMode mode = FileMode.Open;
                if (Mode == OpenMode.Output)
                    mode = FileMode.Create;
                else if (Mode == OpenMode.Append)
                    mode = FileMode.Append;
                else if (Mode == OpenMode.Input)
                    mode = FileMode.Open;
                else if (Mode == OpenMode.Random || Mode == OpenMode.Binary)
                    mode = FileMode.OpenOrCreate;

                FileShare share = FileShare.ReadWrite;
                if (Share == OpenShare.LockRead)
                    share = FileShare.Write;
                else if (Share == OpenShare.Shared)
                    share = FileShare.ReadWrite;
                else if (Share == OpenShare.LockWrite)
                    share = FileShare.Read;
                else if (Share == OpenShare.LockReadWrite)
                    share = FileShare.None;

                // fix the access
                if (mode == FileMode.Create || mode == FileMode.Append)
                    access |= FileAccess.Write;

                // remember if opened in binary mode
                bool isBinary = (Mode == OpenMode.Binary);

                // create the inner FCFile instance
                m_fcFile = new FCFile(fileName, mode, access, share, encoding, isBinary);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
          
        }

        /// <summary>
        /// Returns the next character in an open file.
        /// </summary>
        /// <param name="hFile">The handle of the open file.</param>
        /// <returns>nChar</returns>
        public int GetChar()
        {
            int c = int.MinValue;
            GetChar(ref c);
            return c;
        }

        /// <summary>
        /// Returns the next character in an open file.
        /// </summary>
        /// <param name="hFile">The handle of the open file.</param>
        /// <returns>nChar</returns>
        public bool GetChar(ref int nChar)
        {
            if (IsNull)
                return false;

            int chr = -1;
            try
            {
                chr = m_fcFile.GetChar();
                if (chr == -1)
                    return false;

                nChar = chr;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns the next line from an open file.
        /// Lines are terminated by \r\n or \r or \n
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public String GetString(int maxLength)
        {
            String line = "";
            GetString(ref line, maxLength);
            return line;
        }

        /// <summary>
        /// Returns the next line from an open file.
        /// Lines are terminated by \r\n or \r or \n
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public bool GetString(ref String line, int maxLength)
        {
            if (IsNull)
                return false;

            try
            {
                // NOTE: In CTD the maximum length includes the \0 terminator
                --maxLength;

                line = m_fcFile.GetString(maxLength);
                return !(line == null);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Writes a character to an open file.
        /// </summary>
        /// <param name="nChar">The ANSI numeric value of the character to write to hFile.</param>
        /// <returns>bOk</returns>
        public bool PutChar(int c)
        {
            if (this.IsNull)
                return false;

            try
            {
                m_fcFile.WritePreamble();
                lastChar = (char)c;

                return m_fcFile.PutChar(c);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Writes a string to an open file.
        /// </summary>
        /// <param name="sString">The string to write.</param>
        /// <returns>bOk</returns>
        public bool PutString(string text, bool newLine = true)
        {
			if (this.IsNull || text == null)
			{
				return false;
			}
            try
            {
                m_fcFile.WritePreamble();
                if (newLine)
                {
                    lastChar = '\n';
                }
                else
                {
                    lastChar = String.IsNullOrEmpty(text) ? '\0' : text[text.Length - 1];
                }
                return m_fcFile.PutString(text, newLine);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Reads a buffer of characters from an open file to a string .
        /// </summary>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public String Read(int maxLength)
        {
            String buffer = "";
            Read(ref buffer, maxLength);
            return buffer;
        }

        /// <summary>
        /// Read the entire stream into a string
        /// </summary>
        /// <returns></returns>
        public String ReadAll()
        {
            return m_fcFile.ReadAll();
        }

        /// <summary>
        /// Reads a buffer of characters from an open file to a string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public int Read(ref String buffer, int maxLength)
        {
            buffer = String.Empty;
            if (this.IsNull)
                return 0;

            try
            {
                // if the file has been opened in binary mode, always read bytes
                if (this.IsBinary)
                {
                    byte[] bytes = m_fcFile.ReadBytes(maxLength);
                    if (bytes == null)
                        return 0;

                    //buffer = new SalString(bytes);
                    buffer = Convert.ToString(bytes);
                    return bytes.Length;
                }
                else
                {
                    // if not opened in binary mode, read characters
                    char[] chars = m_fcFile.ReadChars(maxLength);
                    if (chars == null)
                        return 0;

                    //buffer = new SalString(new string(chars));
                    buffer = new String(chars);
                    return chars.Length;
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        /// <summary>
        /// Reads a buffer of characters from an open file to a binary buffer.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        //public int Read(ref Binary buffer, int maxLength)
        //{
        //    buffer = SalBinary.Empty;
        //    if (this.IsNull)
        //        return 0;

        //    try
        //    {
        //        byte[] bytes = m_fcFile.ReadBytes(maxLength);
        //        if (bytes == null)
        //            return 0;

        //        buffer = new SalBinary(bytes);
        //        return bytes.Length;
        //    }
        //    catch (Exception e)
        //    {
        //        HandleException(e);
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Positions the file pointer in an open file. The next file operation (such as a read
        /// or write) takes place at this new location.
        /// </summary>
        /// <param name="nBytes">The specific position of the file pointer; the number of bytes from nPosition where the next file operation will take place.</param>
        /// <param name="nPosition">The general position of the file pointer; one of the following values: FILE_SeekBegin FILE_SeekCurrent FILE_SeekEnd</param>
        /// <returns>bOk</returns>
        public bool Seek(int bytes, int position)
        {
            if (IsNull)
                return false;

            try
            {
                SeekOrigin origin;
                switch (position)
                {
                    case Sys.FILE_SeekBegin:
                        origin = SeekOrigin.Begin;
                        break;

                    case Sys.FILE_SeekCurrent:
                        origin = SeekOrigin.Current;
                        break;

                    case Sys.FILE_SeekEnd:
                        origin = SeekOrigin.End;
                        break;

                    default:
                        return false;
                }
                m_fcFile.Seek(bytes, origin);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns the current position in an open file.
        /// </summary>
        /// <returns>nPos</returns>
        public long Tell()
        {
            if (IsNull)
                return -1;

            try
            {
                return m_fcFile.Tell();
            }
            catch (Exception e)
            {
                 return -1;
            }
        }

        /// <summary>
        /// Writes a string to an open file.
        /// </summary>
        /// <param name="str">The string to write to hFile.</param>
        /// <param name="length">The number of bytes to write.</param>
        /// <returns></returns>
        public int Write(String str, int length)
        {
            if (this.IsNull)
                return 0;

            if (str == null)
                return 0;

            try
            {
                if (this.IsBinary)
                {
                    //byte[] buffer = str.GetBlob();
                     //return m_fcFile.Write(buffer, length);
                    return 0;
                }
                else
                {
                    // write encoding preamble if not binary file
                    m_fcFile.WritePreamble();

                    //length = Math.Min(str.Value.Length, length);
                    //char[] buffer = str.Value.ToCharArray(0, length);
                    length = Math.Min(str.Length, length);
                    char[] buffer = str.ToCharArray(0, length);
                    return m_fcFile.Write(buffer, length);
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        /// <summary>
        /// Writes a binary buffer to an open file.
        /// </summary>
        /// <param name="binary">The string to write to hFile.</param>
        /// <param name="length">The number of bytes to write.</param>
        /// <returns></returns>
        //public int Write(SalBinary binary, int length)
        //{
        //    if (this.IsNull)
        //        return 0;

        //    if (binary.IsNull)
        //        return 0;

        //    try
        //    {
        //        byte[] buffer = binary.Value;
        //        return m_fcFile.Write(buffer, length);
        //    }
        //    catch (Exception e)
        //    {
        //        HandleException(e);
        //        return 0;
        //    }
        //}
        #endregion

        #region Static file functions

        /// <summary>
        /// Copies the contents of one file (source) to another file (destination).
        /// </summary>
        /// <param name="sSourcePath">The full path name of the source file.</param>
        /// <param name="sDestPath">The full path name of the destination file.</param>
        /// <param name="bOverWrite">Specifies whether (TRUE) or not (FALSE) to overwrite the destination file. If the destination file already exists and bOverWrite is FALSE, then SalFileCopy fails, and returns FILE_CopyExist. If the destination file already exists and bOverWrite is TRUE, then SalFileCopy succeeds and the destination file is overwritten.</param>
        /// <returns>nStatus</returns>
        public static int Copy(String sSourcePath, String sDestPath, bool bOverWrite)
        {
            try
            {
                File.Copy(sSourcePath, sDestPath, bOverWrite);
                return Sys.FILE_CopyOK;
            }
            catch (Exception e)
            {
                if (!File.Exists(sSourcePath))
                    return Sys.FILE_CopySrc;
                if (File.Exists(sDestPath) && !bOverWrite)
                    return Sys.FILE_CopyExist;
                if (!File.Exists(sDestPath))
                    return Sys.FILE_CopyDest;
                else
                    return Sys.FILE_CopyWrite;
            }
        }

        /// <summary>
        /// Creates a directory.
        /// </summary>
        /// <param name="sDir">The full path name of the new directory.</param>
        /// <returns>bOk</returns>
        public static bool CreateDirectory(String sDir)
        {
            try
            {
                Directory.CreateDirectory(sDir);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Takes VB6-Attribute, gives NET-Attribute
        /// In VB FileAttribute Enum the value for Normal is 0 and in System.IO.FileAttributes value for Normal is 128
        /// </summary>
        /// <param name="vB6Attribute"></param>
        /// <returns></returns>
        public static FileAttributes GetCorrectFileAttribute(FileAttribute vB6Attribute, bool isDrive = false)
        {
            #region set FileAttributtes
            FileAttributes newAttributes = FileAttributes.Normal;
            if ((vB6Attribute & FileAttribute.Directory) == FileAttribute.Directory && !isDrive)
            {
                newAttributes |= FileAttributes.Directory;
            }
            if ((vB6Attribute & FileAttribute.Archive) == FileAttribute.Archive)
            {
                newAttributes |= FileAttributes.Archive;
            }
            if ((vB6Attribute & FileAttribute.Hidden) == FileAttribute.Hidden)
            {
                newAttributes |= FileAttributes.Hidden;
            }
            if ((vB6Attribute & FileAttribute.Normal) == FileAttribute.Normal)
            {
                newAttributes |= FileAttributes.Normal;
            }
            if ((vB6Attribute & FileAttribute.ReadOnly) == FileAttribute.ReadOnly)
            {
                newAttributes |= FileAttributes.ReadOnly;
            }
            if ((vB6Attribute & FileAttribute.System) == FileAttribute.System)
            {
                newAttributes |= FileAttributes.System;
            }
            if ((vB6Attribute & FileAttribute.Volume) == FileAttribute.Volume && isDrive)
            {
                //newAttributes |= FileAttributes.Directory;
                newAttributes |= FileAttributes.Device;
            }

            // correct the initial value of newAttributes
            if (!((vB6Attribute & FileAttribute.Normal) == FileAttribute.Normal))
            {
                newAttributes &= ~FileAttributes.Normal;
            }
            #endregion
            return newAttributes;
        }

        /// <summary>
        /// Takes NET-Attribute, gives VB6-Attribute
        /// In VB FileAttribute Enum the value for Normal is 0 and in System.IO.FileAttributes value for Normal is 128
        /// </summary>
        /// <param name="netAttribute"></param>
        /// <returns></returns>
        public static FileAttribute GetCorrectFileAttribute(FileAttributes netAttribute, bool isDrive = false)
        {
            #region set FileAttributtes
            FileAttribute newAttributes = FileAttribute.Normal;
            if ((netAttribute & FileAttributes.Directory) == FileAttributes.Directory && !isDrive)
            {
                newAttributes |= FileAttribute.Directory;
            }
            if ((netAttribute & FileAttributes.Archive) == FileAttributes.Archive)
            {
                newAttributes |= FileAttribute.Archive;
            }
            if ((netAttribute & FileAttributes.Hidden) == FileAttributes.Hidden)
            {
                newAttributes |= FileAttribute.Hidden;
            }
            if ((netAttribute & FileAttributes.Normal) == FileAttributes.Normal)
            {
                newAttributes |= FileAttribute.Normal;
            }
            if ((netAttribute & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                newAttributes |= FileAttribute.ReadOnly;
            }
            if ((netAttribute & FileAttributes.System) == FileAttributes.System)
            {
                newAttributes |= FileAttribute.System;
            }
            if ((netAttribute & FileAttributes.Directory) == FileAttributes.Directory && isDrive)
            {
                newAttributes |= FileAttribute.Volume;
            }

            // correct the initial value of newAttributes
            if (!((netAttribute & FileAttributes.Normal) == FileAttributes.Normal))
            {
                newAttributes &= ~FileAttribute.Normal;
            }

            #endregion
            return (FileAttribute)newAttributes;
        }

        /// <summary>
        /// Gets the full path name of the current working directory.
        /// </summary>
        /// <param name="sPath">The full path name, including the drive letter, of the current working directory.</param>
        /// <returns>bOk</returns>
        public static bool GetCurrentDirectory(ref String sPath)
        {
            try
            {
                sPath = Directory.GetCurrentDirectory();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the modification date and time of the specified file.
        /// </summary>
        /// <param name="sFilename">The name of the file whose modification date you want.</param>
        /// <param name="dtDateTime">The modification date and time of sFilename.</param>
        /// <returns>bOk</returns>
        public static bool GetDateTime(String sFilename, ref DateTime dtDateTime)
        {
            try
            {
                if (!File.Exists(sFilename))
                    return false;

                dtDateTime = File.GetLastWriteTime(sFilename);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the letter of the default (current) disk drive.
        /// </summary>
        /// <returns>sDriveLetter</returns>
        public static String GetDrive()
        {
            char[] cd = new char[2];
            try
            {
                cd[0] = Application.StartupPath.ToUpper()[0];
                if (cd[0].CompareTo('A') < 0 || cd[0].CompareTo('Z') > 0)
                    cd[0] = '@';
            }
            catch (Exception e)
            {
                cd[0] = '@';
            }
            cd[1] = ':';

            return new string(cd);
        }

        /// <summary>
        /// Sets the current disk drive to the specified drive letter.
        /// </summary>
        /// <param name="sDriveLetter">The new disk drive letter. The length of this parameter's value is one character.</param>
        /// <returns>bOk</returns>
        public static bool SetDrive(String sDriveLetter)
        {
            try
            {
                if (string.IsNullOrEmpty(sDriveLetter))
                    return false;
                if (Directory.GetCurrentDirectory().StartsWith(sDriveLetter))
                    return true;

                Directory.SetCurrentDirectory(sDriveLetter.Substring(0, 1) + ":\\");
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Changes the current working directory. If the specified path does not contain a drive
        /// letter, the default drive's current directory is changed. Otherwise, the specified
        /// drive's current directory is changed and the specified drive is made the
        /// current drive.
        /// </summary>
        /// <param name="sPath">The path name of the new current working directory.</param>
        /// <returns>bOk</returns>
        public static bool SetCurrentDirectory(String sPath)
        {
            try
            {
                FileAttributes attr = File.GetAttributes(sPath);

                //detect whether its a directory or file
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    Directory.SetCurrentDirectory(sPath);
                }
                else
                {
                    Directory.SetCurrentDirectory(Path.GetDirectoryName(sPath));
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sets the modification date and time of the specified file.
        /// </summary>
        /// <param name="sFilename">The name of the file whose modification date you want to set.</param>
        /// <param name="dtDateTime">The modification date and time.</param>
        /// <returns>bOk</returns>
        public static bool SetDateTime(String sFilename, DateTime dtDateTime)
        {
            try
            {
                if (!File.Exists(sFilename))
                    return false;

                File.SetLastWriteTime(sFilename, dtDateTime);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes a directory.
        /// </summary>
        /// <param name="sDir">The full path name of the directory to delete.</param>
        /// <returns>bOk</returns>
        public static bool RemoveDirectory(String sDir)
        {
            try
            {
                Directory.Delete(sDir);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

		#endregion

		#region Internal Methods
		internal void PutString(string value, int tabPosition, bool newLine)
		{
			if (value == null)
			{
				return;
			}
			if (tabPosition == -1)
			{
				//If tabPosition is not set, continue writing
				tabPosition = currentString.Length;
			}
			if (tabPosition < currentString.Length)
			{
				//If TabPosition is smaller than current line last position, start writing in new line (flush currentString with newline)
				PutString(currentString, true);
				currentString = "";
			}
			//This is not else
			if (tabPosition > currentString.Length)
			{
				currentString += new string(' ', tabPosition - currentString.Length);
			}
			currentString += value;
			if (newLine)
			{
				PutString(currentString, true);
				currentString = "";
			}
		}
		#endregion

		public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
        
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {

        }


    }

    public static class ExtensionMethods
    {
        #region Extension Methods
        public static bool HasOneFlagActive(this FileAttributes attribute, FileAttributes netAttribute)
        {
            bool returnValue = false;
            if ((netAttribute & FileAttributes.Directory) == attribute)
            {
                returnValue = true;
            }
            if ((netAttribute & FileAttributes.Archive) == attribute)
            {
                returnValue = true;
            }
            if ((netAttribute & FileAttributes.Hidden) == attribute)
            {
                returnValue = true;
            }
            if ((netAttribute & FileAttributes.Normal) == attribute)
            {
                returnValue = true;
            }
            if ((netAttribute & FileAttributes.ReadOnly) == attribute)
            {
                returnValue = true;
            }
            if ((netAttribute & FileAttributes.System) == attribute)
            {
                returnValue = true;
            }
            if ((netAttribute & FileAttributes.Directory) == attribute)
            {
                returnValue = true;
            }
            return returnValue;
        }
        #endregion
    }
}
