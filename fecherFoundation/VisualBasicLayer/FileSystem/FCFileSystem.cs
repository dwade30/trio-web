﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using fecherFoundation.VisualBasicLayer;
using System.Runtime.InteropServices;
using Wisej.Web;

namespace fecherFoundation.VisualBasicLayer
{
    public class FCFileSystem
    {
        #region Private fields
        private static List<FCFileHandle> fileList = new List<FCFileHandle>();
        private static List<string> foundFiles = new List<string>();
        private const string TabMarker = "***###***TABMARKER";
        #endregion

        #region Public Methods

        /// <summary>
        /// Returns the drive name of a given path.
        /// </summary>
        /// <param name="path">The path specifier.</param>
        /// <returns></returns>
        public string GetDriveName(string path)
        {
            string driveName = "";
            if (Path.IsPathRooted(path))
            {
                driveName = Path.GetPathRoot(path);
                driveName = driveName.Substring(0, driveName.IndexOf(":") + ":".Length);
            }
            return driveName;
        }

        /// <summary>
        /// Obtains a reference to a Drive object for the specified drive.
        /// </summary>
        /// <param name="driveSpecifier">The drive name</param>
        /// <returns></returns>
        public DriveInfo GetDrive(string driveSpecifier)
        {
            DriveInfo drive = null;
            //The driveSpecifier must consist only in a letter from a to z
            driveSpecifier = driveSpecifier.Replace(":", "");
            driveSpecifier = driveSpecifier.Replace(@"\", "");
            //Check if drive exist and assign it to our variable
            foreach (DriveInfo dr in DriveInfo.GetDrives())
            {
                if (dr.Name.Contains(driveSpecifier))
                {
                    return dr;
                }
            }
            return drive;
        }

        /// <summary>
        /// Removes a given file
        /// </summary>
        /// <param name="fileName">The name of file</param>
        public static void DeleteFile(string fileName)
        {
            fileName = Path.Combine(Statics.UserDataFolder, fileName);
            File.Delete(fileName);
        }

        public static bool DirectoryExists(string directory)
        {
            directory = Path.Combine(Statics.UserDataFolder, directory);
            return Directory.Exists(directory);
        }

        public static void CreateDirectory(string directory)
        {
            directory = Path.Combine(Statics.UserDataFolder, directory);
            Directory.CreateDirectory(directory);
        }

        public static bool FileExists(string fileName)
        {
            fileName = Path.Combine(Statics.UserDataFolder, fileName);
            if (File.Exists(fileName))
            {
                return true;
            }
            return false;
        }
        public static FileStream GetFileStream(int fileNumber)
        {
            FCFileHandle handle = fileList[fileNumber];
            return handle.FileStream;
        }

        // null value
        //public readonly static FCFileSystem Null = new FCFileSystem();



        // Summary:
        //     Changes the current directory or folder. The My feature gives you better
        //     productivity and performance in file I/O operations than the ChDir function.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.CurrentDirectory
        //     .
        //
        // Parameters:
        //   Path:
        //     Required. A String expression that identifies which directory or folder becomes
        //     the new default directory or folder. Path may include the drive. If no drive
        //     is specified, ChDir changes the default directory or folder on the current
        //     drive.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     Path is empty.
        //
        //   System.IO.FileNotFoundException:
        //     Invalid drive is specified, or drive is unavailable.
        public static void ChDir(string path)
        {
            if (!FCFileHandle.SetCurrentDirectory(path))
            {
                Information.Err().Raise(76);
            }
        }

        //
        // Summary:
        //     Changes the current drive.
        //
        // Parameters:
        //   Drive:
        //     Required. String expression that specifies an existing drive. If you supply
        //     a zero-length string (""), the current drive does not change. If the Drive
        //     argument is a multiple-character string, ChDrive uses only the first letter.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     Invalid drive is specified, or drive is unavailable.
        public static void ChDrive(char drive)
        {
            FCFileHandle.SetDrive(drive.ToString());
        }

        //
        // Summary:
        //     Changes the current drive.
        //
        // Parameters:
        //   Drive:
        //     Required. String expression that specifies an existing drive. If you supply
        //     a zero-length string (""), the current drive does not change. If the Drive
        //     argument is a multiple-character string, ChDrive uses only the first letter.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     Invalid drive is specified, or drive is unavailable.
        public static void ChDrive(string drive)
        {
            FCFileHandle.SetDrive(drive);
        }

        //
        // Summary:
        //     Returns a string representing the current path. The Microsoft.VisualBasic.FileIO.FileSystem
        //     gives you better productivity and performance in file I/O operations than
        //     CurDir. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.CurrentDirectory.
        //
        // Returns:
        //     A string representing the current path.
        public static string CurDir()
        {
            //return Directory.GetCurrentDirectory();
            return Statics.UserDataFolder;
        }

        //
        // Summary:
        //     Returns a string representing the current path. The Microsoft.VisualBasic.FileIO.FileSystem
        //     gives you better productivity and performance in file I/O operations than
        //     CurDir. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.CurrentDirectory.
        //
        // Parameters:
        //   Drive:
        //     Optional. Char expression that specifies an existing drive. If no drive is
        //     specified, or if Drive is a zero-length string (""), CurDir returns the path
        //     for the current drive.
        //
        // Returns:
        //     A string representing the current path.
        public static string CurDir(char drive)
        {
            return Directory.GetCurrentDirectory();
        }

        //
        // Summary:
        //     Returns a string representing the name of a file, directory, or folder that
        //     matches a specified pattern or file attribute, or the volume label of a drive.
        //     The Microsoft.VisualBasic.FileIO.FileSystem gives you better productivity
        //     and performance in file I/O operations than the Dir function. See Microsoft.VisualBasic.FileIO.FileSystem.GetDirectoryInfo(System.String)
        //     for more information.
        //
        // Returns:
        //     A string representing the name of a file, directory, or folder that matches
        //     a specified pattern or file attribute, or the volume label of a drive.
        public static string Dir()
        {
            if (foundFiles.Count > 0)
            {
                string returnValue = foundFiles[0];
                foundFiles.RemoveAt(0);
                return returnValue;
            }
            return "";
        }

        //
        // Summary:
        //     Returns a string representing the name of a file, directory, or folder that
        //     matches a specified pattern or file attribute, or the volume label of a drive.
        //     The Microsoft.VisualBasic.FileIO.FileSystem gives you better productivity
        //     and performance in file I/O operations than the Dir function. See Microsoft.VisualBasic.FileIO.FileSystem.GetDirectoryInfo(System.String)
        //     for more information.
        //
        // Parameters:
        //   PathName:
        //     Optional. String expression that specifies a file name, directory or folder
        //     name, or drive volume label. A zero-length string ("") is returned if PathName
        //     is not found.
        //
        //   Attributes:
        //     Optional. Enumeration or numeric expression whose value specifies file attributes.
        //     If omitted, Dir returns files that match PathName but have no attributes.
        //
        // Returns:
        //     A string representing the name of a file, directory, or folder that matches
        //     a specified pattern or file attribute, or the volume label of a drive.
        public static string Dir(string pathName, FileAttribute attributes = FileAttribute.Normal)
        {
            if (string.IsNullOrEmpty(pathName))
            {
                //CHE: in VB6 Dir("") should return filename from current path, not the path
                pathName = CurDir();
                //IPI: if pathName does not end in "\", Path.GetFileName will not return correctly only the FileName as expected
                if (!pathName.EndsWith("\\"))
                {
                    pathName += "\\";
                }
            }
            string strDirectory = Path.GetDirectoryName(pathName);
            string strFile = Path.GetFileName(pathName);
            string strDrive = Path.GetPathRoot(pathName);
            string returnValue = "";

            if (!strFile.Contains("*") && !File.Exists(pathName) && !Directory.Exists(strDirectory) && !Directory.Exists(strDrive))
            {
                return "";
            }

            FileAttributes newAttributes = FCFileHandle.GetCorrectFileAttribute(attributes, pathName.Length > 3 ? false : true);

            if (!strFile.Contains("*") && File.Exists(pathName) && (attributes == FileAttribute.Normal || File.GetAttributes(pathName).HasFlag(newAttributes)))
            {
                foundFiles.Clear();
                foundFiles.Add(pathName);
                return Path.GetFileName(pathName);
            }

            // if we have a drive as parameter
            if (string.IsNullOrEmpty(strDirectory))
            {
                strDirectory = strDrive;
            }

            //FCFileHandle.SetCurrentDirectory(strDirectory);
            DirectoryInfo objDir = new DirectoryInfo(strDirectory);

            foundFiles.Clear();

            // if we have a file wildcard
            if (strFile.Contains("*"))
            {
                //DSE Exception if directory does not exists
                if (!Directory.Exists(strDirectory))
                {
                    return "";
                }
                foreach (FileInfo objFile in objDir.GetFiles(strFile))
                {
                    if (attributes == FileAttribute.Normal || objFile.Attributes.HasFlag(newAttributes))
                    {
                        foundFiles.Add(objFile.Name);
                    }
                }
            }
            //if we have a folder or drive
            else if ((newAttributes & FileAttributes.Directory) == FileAttributes.Directory || (newAttributes & FileAttributes.Device) == FileAttributes.Device)
            {
                //CHE: if C:\test1 exists and C:\test1\test2 does not exists, Dir("C:\test1\test2", FileAttribute.Directory) must return ""
                //For this case strDirectory is C:\test1 and condition Directory.Exists(strDirectory) is not enough
                //JEI: if strDirectory == C:\test1 and pathName == C:\test1\ then the "if" should return true
                if (Directory.Exists(strDirectory) && (strDirectory == pathName.TrimEnd(new char[] { '\\' }) || (newAttributes & FileAttributes.Device) == FileAttributes.Device))
                {
                    //CHE: for Directory attribute when calling Dir with a valid directory ending with \ should return "."
                    if ((newAttributes & FileAttributes.Directory) == FileAttributes.Directory && pathName.EndsWith(@"\"))
                    {
                        return ".";
                    }
                    //IPI: if the folder exists, Dir function returns the first file or folder found in strDirectory 
                    foreach (string directoryItem in Directory.GetDirectories(strDirectory))
                    {
                        foundFiles.Add(Path.GetFileName(directoryItem));
                    }
                    foreach (string directoryItem in Directory.GetFiles(strDirectory))
                    {
                        foundFiles.Add(Path.GetFileName(directoryItem));
                    }

                    //CHE: return folder name if exists
                    if ((newAttributes & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        if (foundFiles.Count > 0)
                        {
                            foundFiles.Sort();
                            return Dir();
                        }
                        return "";
                    }
                }
                //JEI: in vb6 when having attribute == vbDirectory and pathName doesn't end with "\" e.g. "c:\temp" then the foldername is returned -> "temp"
                else if (Directory.Exists(pathName) && !string.IsNullOrEmpty(strFile))
                {
                    return strFile;
                }
                //CHE: when calling with existing file and attribute directory has to return that file name without the path
                else if (Directory.Exists(strDirectory) && !string.IsNullOrEmpty(strFile))
                {
                    return strFile;
                }
            }
            //if Dir is called without attributes and pathName only contains a folder, for that in VB6 the first file within the folder is returned
            // to get the first file of an folder with this preconditions the folder needs to be given like this c:\temp\
            // if it is the case then strFile == "", if it is not the case strFile should contain the folder name
            else if (string.IsNullOrEmpty(strFile))
            {
                if (Directory.Exists(objDir.FullName))
                {
                    foreach (FileInfo objFile in objDir.GetFiles())
                    {
                        foundFiles.Add(objFile.Name);
                    }
                }
            }

            returnValue = foundFiles.Count > 0 ? foundFiles[0] : "";
            if (!string.IsNullOrEmpty(returnValue))
                foundFiles.RemoveAt(0);
            return returnValue;
        }

        //
        // Summary:
        //     Returns a Boolean value True when the end of a file opened for Random or
        //     sequential Input has been reached.
        //
        // Parameters:
        //   fileNumber:
        //     Required. An Integer that contains any valid file number.
        //
        // Returns:
        //     Returns a Boolean value True when the end of a file opened for Random or
        //     sequential Input has been reached.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static bool EOF(int fileNumber)
        {
            bool atEnd = false;
            if (fileList[fileNumber].FileStream.CanSeek)
            {
                atEnd = (fileList[fileNumber].FileStream.Position >= fileList[fileNumber].FileStream.Length - 1);
            }
            return atEnd;
        }

        //
        // Summary:
        //     Returns an enumeration representing the file mode for files opened using
        //     the FileOpen function. The Microsoft.VisualBasic.FileIO.FileSystem gives
        //     you better productivity and performance in file I/O operations than the FileAttr
        //     function. See Microsoft.VisualBasic.FileIO.FileSystem.GetFileInfo(System.String)
        //     for more information.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Integer. Any valid file number.
        //
        // Returns:
        //     The following enumeration values indicate the file access mode:ValueMode1OpenMode.Input2OpenMode.Output4OpenMode.Random8OpenMode.Append32OpenMode.Binary
        public static OpenMode FileAttr(int fileNumber)
        {
            // TODO not used
            return OpenMode.Input;
        }

        public static string Tab(int count)
        {
            return TabMarker + (count - 1);
        }

        //
        // Summary:
        //     Concludes input/output (I/O) to a file opened using the FileOpen function.
        //     My gives you better productivity and performance in file I/O operations.
        //     See Microsoft.VisualBasic.FileIO.FileSystem for more information.
        //
        // Parameters:
        //   fileNumbers:
        //     Optional. Parameter array of 0 or more channels to be closed.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        public static void FileClose(params int[] fileNumbers)
        {
            //FC:FINAL:MSH - i.issue #1330: close all files and clear file list if method called without input params
            if (fileNumbers != null && fileNumbers.Length > 0)
            {
                foreach (int item in fileNumbers)
                {
                    if (fileList.Count > item)
                    {
                        //FC:FINAL:SBE - avoid null reference exception
                        if (fileList[item] != null)
                        {
                            fileList[item].Close();
                        }
                        //FC:FINAL:MSH - i.issue #1835: closing file won't be executed if close files one by one (for example, if call FileClose(97) and after this call FileClose(98) then the last file won't be closed because number of items in list was changed)
                        //fileList.RemoveAt(item);
                        fileList[item] = null;
                    }
                }
            }
            else
            {
                for (int i = 0; i < fileList.Count; i++)
                {
                    if (fileList[i] != null)
                    {
                        fileList[i].Close();
                    }
                }
                fileList.Clear();
            }
            //FC:FINAL:MSH - i.issue #1835: clear list if all items in list are empty to avoid problem with extra items number
            if (fileList != null && fileList.Count > 0 && fileList.All(x => x == null))
            {
                fileList.Clear();
            }
        }

        //
        // Summary:
        //     Copies a file. The Microsoft.VisualBasic.FileIO.FileSystem gives you better
        //     productivity and performance in file I/O operations than FileCopy. See Microsoft.VisualBasic.FileIO.FileSystem.CopyFile(System.String,System.String)
        //     for more information.
        //
        // Parameters:
        //   Source:
        //     Required. String expression that specifies the name of the file to be copied.
        //     Source may include the directory or folder, and drive, of the source file.
        //
        //   Destination:
        //     Required. String expression that specifies the destination file name. Destination
        //     may include the directory or folder, and drive, of the destination file.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     Source or Destination is invalid or not specified.
        //
        //   System.IO.IOException:
        //     File is already open.
        //
        //   System.IO.FileNotFoundException:
        //     File does not exist.
        public static void FileCopy(string source, string destination)
        {
            //FC:FINAL:MSH - in FCFileSystem is used 'UserData' folder if no folder is specified 
            if (!Path.IsPathRooted(source))
            {
                source = Path.Combine(Statics.UserDataFolder, source);
            }
            FCFileHandle.Copy(source, destination, true);
        }

        //
        // Summary:
        //     Returns a Date value that indicates the date and time a file was written
        //     to. The My feature gives you better productivity and performance in file
        //     I/O operations than FileDateTime. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.GetFileInfo(System.String)
        //
        // Parameters:
        //   PathName:
        //     Required. String expression that specifies a file name. PathName may include
        //     the directory or folder, and the drive.
        //
        // Returns:
        //     Date value that indicates the date and time a file was created or last modified.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     PathName is invalid or contains wildcards.
        //
        //   System.IO.FileNotFoundException:
        //     Target file does not exist.
        public static DateTime FileDateTime(string pathName)
        {
            return File.GetLastWriteTime(pathName);
        }

        #region FileGet is not used
        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref bool value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref byte value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref char value, long RecordNumber = -1)
        {
            // TODO
        }


        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref DateTime value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref decimal value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref double value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref float value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref int value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref long value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref short value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref ValueType value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        //   StringIsFixedLength:
        //     Optional. Applies only when writing a string. Specifies whether to write
        //     a two-byte descriptor for the string that describes the length. The default
        //     is False.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref string value, long RecordNumber = -1, bool StringIsFixedLength = false)
        {
            using (FileStream fs = FCFileSystem.GetFileStream(fileNumber))
            {
                if (RecordNumber != -1)
                {
                    fs.Position = RecordNumber;
                }
                using (BinaryReader reader = new BinaryReader(fs))
                {
                    value = reader.ReadString();
                }
            }
        }

        public static void FileGet(int fileNumber, ref byte[] value)
        {
            using (FileStream fs = FCFileSystem.GetFileStream(fileNumber))
            {
                using (var ms = new MemoryStream())
                {
                    fs.CopyTo(ms);
                    value = ms.ToArray();
                }
            }
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGet.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        //
        //   ArrayIsDynamic:
        //     Optional. Applies only when writing an array. Specifies whether the array
        //     is to be treated as dynamic and whether an array descriptor describing the
        //     size and bounds of the array is necessary.
        //
        //   StringIsFixedLength:
        //     Optional. Applies only when writing a string. Specifies whether to write
        //     a two-byte descriptor for the string that describes the length. The default
        //     is False.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileGet(int fileNumber, ref Array value, long RecordNumber = -1, bool ArrayIsDynamic = false, bool StringIsFixedLength = false)
        {
            // TODO
        }

        /// <summary>
        /// Reads data from an open disk file into a variable. The My feature gives you
        ///  better productivity and performance in file I/O operations than FileGetObject.
        ///  For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        /// </summary>
        /// <param name="fileNumber">Required. Any valid file number.</param>
        /// <param name="structure">Required. Valid variable name into which data is read.</param>
        /// <param name="RecordNumber">Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts</param>
        /// <returns></returns>
        public static ValueType FileGet(int fileNumber, Type structure, long RecordNumber = -1)
        {
            // TODO
            return 0;
        }

        //
        // Summary:
        //     Reads data from an open disk file into a variable. The My feature gives you
        //     better productivity and performance in file I/O operations than FileGetObject.
        //     For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name into which data is read.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which reading starts.
        public static void FileGetObject<T>(int fileNumber, T structure)
        {
            FCFileHandle handle = fileList[fileNumber];
            BinaryReader reader = new BinaryReader(handle.FileStream);

            byte[] bytes = reader.ReadBytes(Marshal.SizeOf(structure));

            GCHandle gcHandle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            Marshal.PtrToStructure(gcHandle.AddrOfPinnedObject(), structure);
            gcHandle.Free();
        }

        /// <summary>
        ///     Reads data from an open disk file into a variable. The My feature gives you
        ///     better productivity and performance in file I/O operations than FileGetObject.
        ///     For more information, see Microsoft.VisualBasic.FileIO.FileSystem./// 
        /// </summary>
        /// <typeparam name="T">type of value</typeparam>
        /// <param name="fileNumber">Required. Any valid file number.</param>
        /// <param name="value">Required. Valid variable name into which data is read.</param>
        /// <param name="RecordNumber">Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.</param>
        public static void FileGet<T>(int fileNumber, ref T value, long RecordNumber = -1) where T : struct
        {
            var file = fileList[fileNumber];
            var size = Marshal.SizeOf(value);
            var buf = new byte[size];

            if (RecordNumber < 0)
            {
                //FC:FINAL:MSH - i.issue #1837: 'Position' value should be non-negative
                //file.FileStream.Position = RecordNumber;
                file.FileStream.Position = 0;
            }
            else
            {
                //FC:FINAL:DSE:#2009 Compute real file position
                //file.FileStream.Position = RecordNumber;
                file.FileStream.Position = RecordNumber > 0 ? size * (RecordNumber - 1) : 0;
            }

            var ptr = GCHandle.Alloc(buf, GCHandleType.Pinned);
            try
            {
                file.FileStream.Read(buf, 0, size);
                FCUtils.GetStructFromByte(ref value, ptr.AddrOfPinnedObject());
            }
            finally
            {
                ptr.Free();
            }
        }


        #endregion

        //
        // Summary:
        //     Returns a Long value that specifies the length of a file in bytes. The My
        //     feature gives you better productivity and performance in file I/O operations
        //     than FileLen. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.GetFileInfo(System.String).
        //
        // Parameters:
        //   PathName:
        //     Required. String expression that specifies a file. PathName may include the
        //     directory or folder, and the drive.
        //
        // Returns:
        //     Long value that specifies the length of a file in bytes.
        //
        // Exceptions:
        //   System.IO.FileNotFoundException:
        //     File does not exist.
        public static long FileLen(string pathName)
        {
            //FC:FINAL:MSH - in original exception won't be threw if file not found(Although should be threw an exception, will be returned 0 length)
            //return new FileInfo(pathName).Length;
            try
            {
                return new FileInfo(pathName).Length;
            }
            catch (FileNotFoundException)
            {
                return 0;
            }
        }

        //
        // Summary:
        //     Opens a file for input or output. The My feature gives you better productivity
        //     and performance in file I/O operations than FileOpen. For more information,
        //     see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number. Use the FreeFile function to obtain the
        //     next available file number.
        //
        //   FileName:
        //     Required. String expression that specifies a file name—may include directory
        //     or folder, and drive.
        //
        //   Mode:
        //     Required. Enumeration specifying the file mode: Append, Binary, Input, Output,
        //     or Random. For more information, see Microsoft.VisualBasic.OpenMode .
        //
        //   Access:
        //     Optional. Enumeration specifying the operations permitted on the open file:
        //     Read, Write, or ReadWrite. Defaults to ReadWrite. For more information, see
        //     Microsoft.VisualBasic.OpenAccess .
        //
        //   Share:
        //     Optional. Enumeration specifying the operations not permitted on the open
        //     file by other processes: Shared, Lock Read, Lock Write, and Lock Read Write.
        //     Defaults to Lock Read Write. For more information, see Microsoft.VisualBasic.OpenShare
        //     .
        //
        //   RecordLength:
        //     Optional. Number less than or equal to 32,767 (bytes). For files opened for
        //     random access, this value is the record length. For sequential files, this
        //     value is the number of characters buffered.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     Invalid Access, Share, or Mode.
        //
        //   System.ArgumentException:
        //     WriteOnly file is opened for Input.
        //
        //   System.ArgumentException:
        //     ReadOnly file is opened for Output.
        //
        //   System.ArgumentException:
        //     ReadOnly file is opened for Append.
        //
        //   System.ArgumentException:
        //     Record length is negative (and not equal to -1).
        //
        //   System.IO.IOException:
        //     fileNumber is invalid (<-1 or >255), or fileNumber is already in use.
        //
        //   System.IO.IOException:
        //     FileName is already open, or FileName is invalid.
        public static void FileOpen(int fileNumber, string FileName, OpenMode Mode, OpenAccess Access = OpenAccess.Default, OpenShare Share = OpenShare.Default, int RecordLength = -1)
        {
            FCFileHandle newFile = new FCFileHandle();
            if (newFile.Open(FileName, Mode, Access, Share))
            {
                //CHE: add null to have the correct index when fileNumber is given without FreeFile
                for (int i = fileList.Count; i < fileNumber; i++)
                {
                    fileList.Add(null);
                }
                fileList.Insert(fileNumber, newFile);
            }
            else
            {
                //CHE: throw exception, do not show message, maybe it is catched with Resume Next
                //FCMessageBox.Show("Path not found: " + FileName);
                throw new FileNotFoundException("Path not found: " + FileName);
            }
        }

        #region FilePut is not used
        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, bool value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, byte value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, char value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, DateTime value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, decimal value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, double value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, float value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, int value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, long value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, short value, long RecordNumber = -1)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut<T>(int fileNumber, T value, long RecordNumber = -1) where T : struct
        {
            FCFileHandle file = fileList[fileNumber];
            int size = Marshal.SizeOf(value);

            byte[] bData = FCUtils.GetByteFromStruct(value);
            file.FileStream.Write(bData, 0, size);
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem..
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        //   StringIsFixedLength:
        //     Optional. Applies only when writing a string. Specifies whether to write
        //     a two-byte string length descriptor for the string to the file. The default
        //     is False.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, string value, long RecordNumber = -1, bool StringIsFixedLength = false)
        {
            FCFileHandle file = fileList[fileNumber];
            byte[] bytes = Encoding.GetEncoding(Encoding.Default.CodePage).GetBytes(value);
            file.FileStream.Write(bytes, 0, bytes.Length);
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePut. For more
        //     information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        //
        //   ArrayIsDynamic:
        //     Optional. Applies only when writing an array. Specifies whether the array
        //     is to be treated as dynamic, and whether to write an array descriptor for
        //     the string that describes the length.
        //
        //   StringIsFixedLength:
        //     Optional. Applies only when writing a string. Specifies whether to write
        //     a two-byte string length descriptor for the string to the file. The default
        //     is False.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     RecordNumber < 1 and not equal to -1.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FilePut(int fileNumber, Array value, long RecordNumber = -1, bool ArrayIsDynamic = false, bool StringIsFixedLength = false)
        {
            // TODO
        }

        //
        // Summary:
        //     Writes data from a variable to a disk file. The My feature gives you better
        //     productivity and performance in file I/O operations than FilePutObject. For
        //     more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Valid variable name that contains data written to disk.
        //
        //   RecordNumber:
        //     Optional. Record number (Random mode files) or byte number (Binary mode files)
        //     at which writing starts.
        public static void FilePutObject(int fileNumber, object value, long RecordNumber = -1)
        {
            // TODO
        }
        #endregion

        //
        // Summary:
        //     Assigns an output line width to a file opened by using the FileOpen function.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   RecordWidth:
        //     Required. Numeric expression in the range 0–255, inclusive, which indicates
        //     how many characters appear on a line before a new line is started. If RecordWidth
        //     equals 0, there is no limit to the length of a line. The default value for
        //     RecordWidth is 0.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void FileWidth(int fileNumber, int RecordWidth)
        {
            // TODO
        }

        //
        // Summary:
        //     Returns an Integer value that represents the next file number available for
        //     use by the FileOpen function.
        //
        // Returns:
        //     Returns an Integer value that represents the next file number available for
        //     use by the FileOpen function.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     More than 255 files are in use.
        public static int FreeFile()
        {
            return fileList.Count;
        }

        //
        // Summary:
        //     Returns a FileAttribute value that represents the attributes of a file, directory,
        //     or folder. The My feature gives you better productivity and performance in
        //     file I/O operations than FileAttribute. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   PathName:
        //     Required. String expression that specifies a file, directory, or folder name.
        //     PathName can include the directory or folder, and the drive.
        //
        // Returns:
        //     The value returned by GetAttr is the sum of the following enumeration values:ValueConstantDescriptionNormalvbNormalNormal.ReadOnlyvbReadOnlyRead-only.HiddenvbHiddenHidden.SystemvbSystemSystem
        //     file.DirectoryvbDirectoryDirectory or folder.ArchivevbArchiveFile has changed
        //     since last backup.AliasvbAliasFile has a different name.NoteThese enumerations
        //     are specified by the Visual Basic language. The names can be used anywhere
        //     in your code in place of the actual values.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     Pathname is invalid or contains wildcards.
        //
        //   System.IO.FileNotFoundException:
        //     Target file does not exist.
        public static FileAttribute GetAttr(string pathName)
        {
            FileAttributes returnValue = File.GetAttributes(pathName);
            if (pathName.Length > 3)
            {
                return FCFileHandle.GetCorrectFileAttribute(returnValue);
            }
            else
            {
                // then we have a drive
                return FCFileHandle.GetCorrectFileAttribute(returnValue, true);
            }
        }

        #region Input-functions
        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref bool value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToBoolean(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref byte value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToByte(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref char value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToChar(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref DateTime value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToDateTime(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref decimal value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToDecimal(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref double value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToDouble(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref float value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToSingle(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref int value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToInt32(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref long value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToInt64(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref object value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = OutputArray[0];
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref short value)
        {
            dynamic[] OutputArray = FCFileSystem.Input(fileNumber, value);
            value = Convert.ToInt16(OutputArray[0]);
        }

        //
        // Summary:
        //     Reads data from an open sequential file and assigns the data to variables.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   value:
        //     Required. Variable that is assigned the values read from the file—cannot
        //     be an array or object variable.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Input(int fileNumber, ref string value)
        {
            //The Line Input # statement reads from a file one character at a time until it encounters a carriage return (Chr(13)) or carriage returnlinefeed (Chr(13) + Chr(10)) sequence. 
            //Carriage returnlinefeed sequences are skipped rather than appended to the character string.
            //CHE: There is no theoretical limit, I use short.MaxValue
            value = fileList[fileNumber].GetString(short.MaxValue);
        }

        /// <summary>
        /// Reads data from an open sequential file and assigns the data to variables.
        /// </summary>
        /// <param name="fileNumber"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static dynamic[] Input(int fileNumber, params object[] list)
        {
            string[] strInputList = fileList[fileNumber].GetString(255).Trim().Split(new char[] { ',' });
            List<dynamic> OutputList = new List<dynamic>();

            int intResult = int.MinValue;
            float floatResult = float.NaN;
            double doubleResult = double.NaN;
            short shortValue = short.MinValue;
            long longValue = long.MinValue;
            decimal decimalValue = decimal.MinValue;
            DateTime datetimeValue = DateTime.FromOADate(0);
            char charValue = char.MinValue;
            byte byteValue = byte.MinValue;
            bool boolValue = false;
            //object objectValue = null;
            int i = 0;
            foreach (var strItem in strInputList)
            {
                var item = strItem;

                if (item.StartsWith(".") && item.Length > 1)
                {
                    string value = item.Substring(1);
                    if (Information.IsNumeric(value))
                    {
                        item = "0." + value;
                    }
                }
                else if (item.StartsWith("-.") && item.Length > 2)
                {
                    string value = item.Substring(2);
                    if (Information.IsNumeric(value))
                    {
                        item = "-0." + value;
                    }
                }

                if (int.TryParse(item, out intResult))
                {
                    OutputList.Add(intResult);
                }
                else if (float.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out floatResult))
                {
                    OutputList.Add(floatResult);
                }
                else if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out doubleResult))
                {
                    OutputList.Add(doubleResult);
                }
                else if (short.TryParse(item, out shortValue))
                {
                    OutputList.Add(shortValue);
                }
                else if (long.TryParse(item, out longValue))
                {
                    OutputList.Add(longValue);
                }
                else if (decimal.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out decimalValue))
                {
                    OutputList.Add(decimalValue);
                }
                else if (DateTime.TryParse(item, out datetimeValue))
                {
                    OutputList.Add(datetimeValue);
                }
                else if (char.TryParse(item, out charValue))
                {
                    OutputList.Add(charValue);
                }
                else if (byte.TryParse(item, out byteValue))
                {
                    OutputList.Add(byteValue);
                }
                else if (bool.TryParse(item, out boolValue))
                {
                    OutputList.Add(boolValue);
                }
                else
                {
                    //CHE: return 0 if value is empty string and type is Decimal
                    if (string.IsNullOrEmpty(item) && (list[i].GetType() == typeof(Decimal) || list[i].GetType() == typeof(Single) || list[i].GetType() == typeof(Double)))
                    {
                        item = "0";
                    }
                    OutputList.Add(item);
                }
                i++;
            }

            #region check if types of parameters are equal with types from file - it could work, but TranspositionTool made some parameters as object, and so the check thorws exception            
            //int i = 0;
            //foreach (object item in OutputList)
            //{
            //    if (!(item.GetType() == list[i].GetType()))
            //    {
            //        throw new NotSupportedException("Type mismatch between Parameters and Variables which where read from file.");
            //    }
            //    i++;
            //}
            #endregion

            #region solution using an object array which holds the parameters and is assigned with ref - didn't worked
            //int i = 0;
            //foreach (var strItem in strInputList)
            //{
            //    if (int.TryParse(strItem, out intResult))
            //    {
            //        list[i] = intResult;
            //    }
            //    else if (float.TryParse(strItem, out floatResult))
            //    {
            //        list[i] = floatResult;
            //    }
            //    else if (double.TryParse(strItem, out doubleResult))
            //    {
            //        list[i] = doubleResult;
            //    }
            //    else if (short.TryParse(strItem, out shortValue))
            //    {
            //        list[i] = shortValue;
            //    }
            //    else if (long.TryParse(strItem, out longValue))
            //    {
            //        list[i] = longValue;
            //    }
            //    else if (decimal.TryParse(strItem, out decimalValue))
            //    {
            //        list[i] = decimalValue;
            //    }
            //    else if (DateTime.TryParse(strItem, out datetimeValue))
            //    {
            //        list[i] = datetimeValue;
            //    }
            //    else if (char.TryParse(strItem, out charValue))
            //    {
            //        list[i] = charValue;
            //    }
            //    else if (byte.TryParse(strItem, out byteValue))
            //    {
            //        list[i] = byteValue;
            //    }
            //    else if (bool.TryParse(strItem, out boolValue))
            //    {
            //        list[i] = boolValue;
            //    }
            //    else
            //    {
            //        list[i] = strItem;
            //    }

            //    i++;
            //}
            #endregion

            return OutputList.ToArray();
        }



        //
        // Summary:
        //     Returns String value that contains characters from a file opened in Input
        //     or Binary mode. The My feature gives you better productivity and performance
        //     in file I/O operations than InputString. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   CharCount:
        //     Required. Any valid numeric expression specifying the number of characters
        //     to read.
        //
        // Returns:
        //     Returns String value that contains characters from a file opened in Input
        //     or Binary mode. The My feature gives you better productivity and performance
        //     in file I/O operations than InputString.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.ArgumentException:
        //     CharCount < 0 or > 214.
        public static string InputString(int fileNumber, int CharCount)
        {
            // TODO
            return "";
        }
        #endregion

        //
        // Summary:
        //     Deletes files from a disk. The My feature gives you better productivity and
        //     performance in file I/O operations than Kill. For more information, see Microsoft.VisualBasic.FileIO.FileSystem
        //     .
        //
        // Parameters:
        //   PathName:
        //     Required. String expression that specifies one or more file names to be deleted.
        //     PathName can include the directory or folder, and the drive.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     Target file(s) open.
        //
        //   System.IO.FileNotFoundException:
        //     Target file(s) not found.
        //
        //   System.Security.SecurityException:
        //     Permission denied.
        public static void Kill(string pathName)
        {
            if (string.IsNullOrEmpty(pathName))
            {
                return;
            }

            string strDirectory = Path.GetDirectoryName(pathName);
            string strFile = Path.GetFileName(pathName);
            string strDrive = Path.GetPathRoot(pathName);

            //CHE: CurDir is not changed in VB6 when calling Kill

            //if (Directory.Exists(strDirectory))
            //{
            //    FCFileHandle.SetCurrentDirectory(strDirectory);
            //}
            //else
            //{
            //    string crrDir = "";
            //    FCFileHandle.GetCurrentDirectory(ref crrDir);
            //    FCFileHandle.SetCurrentDirectory(crrDir);
            //}

            if (strFile.Contains("*."))
            {
                DirectoryInfo objDir = new DirectoryInfo(strDirectory);

                foreach (FileInfo objFile in objDir.GetFiles(strFile))
                {
                    objFile.Delete();
                }
            }
            else
            {
                new FileInfo(pathName).Delete();
            }

        }

        //
        // Summary:
        //     Reads a single line from an open sequential file and assigns it to a String
        //     variable.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        // Returns:
        //     Reads a single line from an open sequential file and assigns it to a String
        //     variable.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     End of file reached.
        //
        //   System.IO.IOException:
        //     fileNumber does not exist.
        public static string LineInput(int fileNumber)
        {
            FCFileHandle handle = fileList[fileNumber];
            return handle.GetString(short.MaxValue);
        }

        //
        // Summary:
        //     Returns a Long value that specifies the current read/write position in an
        //     open file.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid Integer file number.
        //
        // Returns:
        //     Returns a Long value that specifies the current read/write position in an
        //     open file.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static long Loc(int fileNumber)
        {
            return fileList[fileNumber].FileStream.Position;
        }

        //
        // Summary:
        //     Controls access by other processes to all or part of a file opened by using
        //     the Open function. The My feature gives you better productivity and performance
        //     in file I/O operations than Lock and Unlock. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Lock(int fileNumber)
        {
            // TODO not used
        }

        //
        // Summary:
        //     Controls access by other processes to all or part of a file opened by using
        //     the Open function. The My feature gives you better productivity and performance
        //     in file I/O operations than Lock and Unlock. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   Record:
        //     Optional. Number of the only record or byte to lock or unlock
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Lock(int fileNumber, long Record)
        {
            // TODO not used
        }

        //
        // Summary:
        //     Controls access by other processes to all or part of a file opened by using
        //     the Open function. The My feature gives you better productivity and performance
        //     in file I/O operations than Lock and Unlock. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   FromRecord:
        //     Optional. Number of the first record or byte to lock or unlock.
        //
        //   ToRecord:
        //     Optional. Number of the last record or byte to lock or unlock.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Lock(int fileNumber, long FromRecord, long ToRecord)
        {
            fileList[fileNumber].FileStream.Lock(FromRecord, ToRecord - FromRecord);
        }

        //
        // Summary:
        //     Returns a Long representing the size, in bytes, of a file opened by using
        //     the FileOpen function. The My feature gives you better productivity and performance
        //     in file I/O operations than LOF. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. An Integer that contains a valid file number.
        //
        // Returns:
        //     Returns a Long representing the size, in bytes, of a file opened by using
        //     the FileOpen function. The My feature gives you better productivity and performance
        //     in file I/O operations than LOF.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static long LOF(int fileNumber)
        {
            return fileList[fileNumber].FileStream.Length;
        }

        //
        // Summary:
        //     Creates a new directory. The My feature gives you better productivity and
        //     performance in file I/O operations than MkDir. For more information, see
        //     Microsoft.VisualBasic.FileIO.FileSystem.CreateDirectory(System.String).
        //
        // Parameters:
        //   Path:
        //     Required. String expression that identifies the directory to be created.
        //     The Path may include the drive. If no drive is specified, MkDir creates the
        //     new directory on the current drive.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     Path is not specified or is empty.
        //
        //   System.Security.SecurityException:
        //     Permission denied.
        //
        //   System.IO.IOException:
        //     Directory already exists.
        public static void MkDir(string path)
        {
            FCFileHandle.CreateDirectory(path);
        }

        //
        // Summary:
        //     Writes display-formatted data to a sequential file.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   Output:
        //     Optional. Zero or more comma-delimited expressions to write to a file.The
        //     Output argument settings are:
        public static void Print(int fileNumber, params object[] output)
        {
            PrintInternal(fileNumber, false, output);
        }

        //
        // Summary:
        //     Writes display-formatted data to a sequential file.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   Output:
        //     Optional. Zero or more comma-delimited expressions to write to a file.The
        //     Output argument settings are:
        public static void PrintLine(int fileNumber, params object[] output)
        {
            PrintInternal(fileNumber, true, output);
        }

        //
        // Summary:
        //     Renames a disk file or directory. The My feature gives you better productivity
        //     and performance in file I/O operations than Rename. For more information,
        //     see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   OldPath:
        //     Required. String expression that specifies the existing file name and location.
        //     OldPath may include the directory, and drive, of the file.
        //
        //   NewPath:
        //     Required. String expression that specifies the new file name and location.
        //     NewPath may include directory and drive of the destination location. The
        //     file name specified by NewPath cannot already exist.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     Path is invalid.
        //
        //   System.IO.FileNotFoundException:
        //     OldPath file does not exist.
        //
        //   System.IO.IOException:
        //     NewPath file already exists.
        //
        //   System.IO.IOException:
        //     Access is invalid.
        //
        //   System.IO.IOException:
        //     Cannot rename to different device.
        public static void Rename(string oldPath, string newPath)
        {
            if (File.Exists(newPath))
                File.Delete(newPath); // Delete the existing file if exists
            File.Move(oldPath, newPath); // Rename the oldFileName into newFileName
        }

        //
        // Summary:
        //     Closes all disk files opened by using the FileOpen function. The My feature
        //     gives you better productivity and performance in file I/O operations than
        //     Reset. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        public static void Reset()
        {
            foreach (FCFileHandle item in fileList)
            {
                item.Close();
            }
        }

        /// <summary>
        /// Opens or creates a file for writing.
        /// </summary>
        /// <param name="fileName">file name with extension</param>
        /// <returns></returns>
        public static StreamWriter CreateTextFile(string fileName)
        {
            fileName = Path.Combine(Statics.UserDataFolder, fileName);
            return File.CreateText(fileName);
        }

        //
        // Summary:
        //     Removes an existing directory. The My feature gives you better productivity
        //     and performance in file I/O operations than RmDir. For more information,
        //     see Overload:Microsoft.VisualBasic.FileIO.FileSystem.DeleteDirectory.
        //
        // Parameters:
        //   Path:
        //     Required. String expression that identifies the directory or folder to be
        //     removed. Path can include the drive. If no drive is specified, RmDir removes
        //     the directory on the current drive.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     Path is not specified or is empty.
        //
        //   System.IO.IOException:
        //     Target directory contains files.
        //
        //   System.IO.FileNotFoundException:
        //     Directory does not exist.
        public static void RmDir(string path)
        {
            Directory.Delete(path);
        }

        //
        // Summary:
        //     Returns a Long specifying the current read/write position in a file opened
        //     by using the FileOpen function, or sets the position for the next read/write
        //     operation in a file opened by using the FileOpen function. The My feature
        //     gives you better productivity and performance in file I/O operations than
        //     Seek. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. An Integer that contains a valid file number.
        //
        // Returns:
        //     Returns a Long specifying the current read/write position in a file opened
        //     by using the FileOpen function, or sets the position for the next read/write
        //     operation in a file opened by using the FileOpen function.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static long Seek(int fileNumber)
        {
            return fileList[fileNumber].FileStream.Position;
        }

        //
        // Summary:
        //     Returns a Long specifying the current read/write position in a file opened
        //     by using the FileOpen function, or sets the position for the next read/write
        //     operation in a file opened by using the FileOpen function. The My feature
        //     gives you better productivity and performance in file I/O operations than
        //     Seek. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. An Integer that contains a valid file number.
        //
        //   Position:
        //     Required. Number in the range 1–2,147,483,647, inclusive, that indicates
        //     where the next read/write operation should occur.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Seek(int fileNumber, long position)
        {
            fileList[fileNumber].FileStream.Seek(position, SeekOrigin.Current);
        }

        //
        // Summary:
        //     Sets attribute information for a file. The My feature gives you better productivity
        //     and performance in file I/O operations than SetAttr. For more information,
        //     see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   PathName:
        //     Required. String expression that specifies a file name. PathName can include
        //     directory or folder, and drive.
        //
        //   Attributes:
        //     Required. Constant or numeric expression, whose sum specifies file attributes.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     PathName invalid or does not exist.
        //
        //   System.ArgumentException:
        //     Attribute type is invalid.
        public static void SetAttr(string pathName, FileAttribute attributes)
        {
            if (pathName.Length > 3)
            {
                File.SetAttributes(pathName, FCFileHandle.GetCorrectFileAttribute(attributes));
            }
            else
            {
                // then we have a drive
                File.SetAttributes(pathName, FCFileHandle.GetCorrectFileAttribute(attributes, true));
            }
        }

        public static int Spc(int count)
        {
            throw new NotImplementedException();
        }

        //
        // Summary:
        //     Controls access by other processes to all or part of a file opened by using
        //     the Open function. The My feature gives you better productivity and performance
        //     in file I/O operations than Lock and Unlock. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Unlock(int fileNumber)
        {
            // TODO not used
        }

        //
        // Summary:
        //     Controls access by other processes to all or part of a file opened by using
        //     the Open function. The My feature gives you better productivity and performance
        //     in file I/O operations than Lock and Unlock. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   Record:
        //     Optional. Number of the only record or byte to lock or unlock
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Unlock(int fileNumber, long Record)
        {
            // TODO not used
        }

        //
        // Summary:
        //     Controls access by other processes to all or part of a file opened by using
        //     the Open function. The My feature gives you better productivity and performance
        //     in file I/O operations than Lock and Unlock. For more information, see Microsoft.VisualBasic.FileIO.FileSystem.
        //
        // Parameters:
        //   fileNumber:
        //     Required. Any valid file number.
        //
        //   FromRecord:
        //     Optional. Number of the first record or byte to lock or unlock.
        //
        //   ToRecord:
        //     Optional. Number of the last record or byte to lock or unlock.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Unlock(int fileNumber, long FromRecord, long ToRecord)
        {
            fileList[fileNumber].FileStream.Unlock(FromRecord, ToRecord - FromRecord);
        }

        //
        // Summary:
        //     Writes data to a sequential file. Data written with Write is usually read
        //     from a file by using Input.
        //     Unlike the Print function, the Write function inserts commas between items and quotation marks around strings as they are written to the file
        //
        // Parameters:
        //   fileNumber:
        //     Required. An Integer expression that contains any valid file number.
        //
        //   Output:
        //     Optional. One or more comma-delimited expressions to write to a file.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     fileNumber does not exist.
        //
        //   System.IO.IOException:
        //     File mode is invalid.
        public static void Write(int fileNumber, params object[] output)
        {
            string strOutput = "";
            if (fileList[fileNumber].FileStream.Position != 0 && fileList[fileNumber].LastChar != '\n')
            {
                strOutput += ",";
            }

            foreach (var strItem in output)
            {
                //FC:FINAL:CHN - issue #1085: Incorrect export data to file.
                if (strItem.GetType() == typeof(byte))
                {
                    strOutput += ((byte)strItem).ToString(CultureInfo.InvariantCulture) + ",";
                }
                else if (strItem.GetType() == typeof(int))
                {
                    strOutput += Convert.ToInt32(strItem).ToString(CultureInfo.InvariantCulture) + ",";
                }
                else if (strItem.GetType() == typeof(long))
                {
                    strOutput += ((long)strItem).ToString(CultureInfo.InvariantCulture) + ",";
                }
                else if (strItem.GetType() == typeof(float))
                {
                    strOutput += ((float)strItem).ToString(CultureInfo.InvariantCulture) + ",";
                }
                else if (strItem.GetType() == typeof(double))
                {
                    strOutput += ((double)strItem).ToString(CultureInfo.InvariantCulture) + ",";
                }
                else if (strItem.GetType() == typeof(decimal))
                {
                    strOutput += ((decimal)strItem).ToString(CultureInfo.InvariantCulture) + ",";
                }
                else if (strItem.GetType() == typeof(bool))
                {
                    if (((bool)strItem) == true)
                    {
                        strOutput += "#TRUE#";
                    }
                    else
                    {
                        strOutput += "#FALSE#";
                    }
                }
                else
                {
                    strOutput += "\"" + strItem.ToString() + "\",";
                }
            }
            strOutput = strOutput.Remove(strOutput.LastIndexOf(","));
            fileList[fileNumber].PutString(strOutput, false);
        }

        //
        // Summary:
        //     Writes data to a sequential file. Data written with Write is usually read
        //     from a file by using Input.
        //     WriteLine inserts a newline character(that is, a carriage return/line feed, or Chr(13) + Chr(10)), after it has written the final character in Output to the file.
        //
        // Parameters:
        //   fileNumber:
        //     Required. An Integer expression that contains any valid file number.
        //
        //   Output:
        //     Optional. One or more comma-delimited expressions to write to a file.
        public static void WriteLine(int fileNumber, params object[] output)
        {
            Write(fileNumber, output);
            fileList[fileNumber].PutChar('\r');
            fileList[fileNumber].PutChar('\n');
        }

        /// <summary>
        /// Opens an existing file for reading
        /// If the file doesn't contain the full path it will be taken from the UserData folder
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static StreamReader OpenText(string path)
        {
            if (!Path.IsPathRooted(path))
            {
                path = Path.Combine(Statics.UserDataFolder, path);
            }
            return File.OpenText(path);
        }
        #endregion

        #region Private methods
        private static void PrintInternal(int fileNumber, bool newline, params object[] output)
        {
            if (fileList.Count <= 0 || fileNumber < 0 || fileNumber >= fileList.Count)
            {
                //throw exception, do not show message, maybe it is catched with Resume Next
                //FCMessageBox.Show("Error in FCFileSystem.Print");
                throw new Exception("Error in FCFileSystem.Print");
                return;
            }
            int tabPosition = -1;
            FCFileHandle handle = fileList[fileNumber];
            string currentValue = "";
            for (int i = 0; i < output.Length; i++)
            {
                currentValue = Convert.ToString(output[i]);
                if (currentValue.StartsWith(TabMarker))
                {
                    tabPosition = Convert.ToInt32(currentValue.Substring(TabMarker.Length));
                }
                else
                {
                    if (i < output.Length - 1)
                    {
                        handle.PutString(currentValue, tabPosition, false);
                    }
                    else
                    {
                        handle.PutString(currentValue, tabPosition, newline);
                    }
                    tabPosition = -1;
                }
            }
        }
        #endregion

        public class StaticVariables
        {
            public string UserDataFolder;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }


    // Summary:
    //     Indicates the file attributes to use when calling file-access functions.
    [Flags]
    public enum FileAttribute
    {
        // Summary:
        //     Normal (default for Dir and SetAttr). No special characteristics apply to
        //     this file. This member is equivalent to the Visual Basic constant vbNormal.
        //Normal = 0,
        Normal = 64,
        //
        // Summary:
        //     Read only. This member is equivalent to the Visual Basic constant vbReadOnly.
        ReadOnly = 1,
        //
        // Summary:
        //     Hidden. This member is equivalent to the Visual Basic constant vbHidden.
        Hidden = 2,
        //
        // Summary:
        //     System file. This member is equivalent to the Visual Basic constant vbSystem.
        System = 4,
        //
        // Summary:
        //     Volume label. This attribute is not valid when used with SetAttr. This member
        //     is equivalent to the Visual Basic constant vbVolume.
        Volume = 8,
        //
        // Summary:
        //     Directory or folder. This member is equivalent to the Visual Basic constant
        //     vbDirectory.
        Directory = 16,
        //
        // Summary:
        //     File has changed since last backup. This member is equivalent to the Visual
        //     Basic constant vbArchive.
        Archive = 32,
    }

    // Summary:
    //     Indicates how to open a file when calling file-access functions.
    public enum OpenAccess
    {
        // Summary:
        //     Read and write access permitted. This is the default.
        Default = -1,
        //
        // Summary:
        //     Read access permitted.
        Read = 1,
        //
        // Summary:
        //     Write access permitted.
        Write = 2,
        //
        // Summary:
        //     Read and write access permitted.
        ReadWrite = 3,
    }

    // Summary:
    //     Indicates how to open a file when calling file-access functions.
    public enum OpenShare
    {
        // Summary:
        //     LockReadWrite. This is the default.
        Default = -1,
        //
        // Summary:
        //     Other processes cannot read or write to the file.
        LockReadWrite = 0,
        //
        // Summary:
        //     Other processes cannot write to the file.
        LockWrite = 1,
        //
        // Summary:
        //     Other processes cannot read the file.
        LockRead = 2,
        //
        // Summary:
        //     Any process can read or write to the file.
        Shared = 3,
    }

    // Summary:
    //     Indicates how to open a file when calling file-access functions.
    public enum OpenMode
    {
        // Summary:
        //     File opened for read access.
        Input = 1,
        //
        // Summary:
        //     File opened for write access.
        Output = 2,
        //
        // Summary:
        //     File opened for random access.
        Random = 4,
        //
        // Summary:
        //     File opened to append to it. Default.
        Append = 8,
        //
        // Summary:
        //     File opened for binary access.
        Binary = 32,
    }
}
