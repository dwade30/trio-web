﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation.VisualBasicLayer
{
    internal static class Sys
    {
        /// <summary>
        /// This constant is returned when the Sal.FileCopy function fails because it can not open the destination file. Value = 2
        /// </summary>
        public const int FILE_CopyDest = 2;
        /// <summary>
        /// This constant is returned when the Sal.FileCopy function fails because the destination file already exists and the bOverWrite parameter is FALSE. Value = 3
        /// </summary>
        public const int FILE_CopyExist = 3;
        /// <summary>
        /// This constant is returned when the Sal.FileCopy function successfully copies the contents of the source file to the destination file. Value = 0
        /// </summary>
        public const int FILE_CopyOK = 0;
        /// <summary>
        /// This constant is returned when the Sal.FileCopy function fails while reading the source file. Value = 4
        /// </summary>
        public const int FILE_CopyRead = 4;
        /// <summary>
        /// This constant is returned when the Sal.FileCopy function fails because the source file can not be opened. Value = 1
        /// </summary>
        public const int FILE_CopySrc = 1;
        /// <summary>
        /// This constant is returned when the Sal.FileCopy function fails while writing to the destination file. Value = 5
        /// </summary>
        public const int FILE_CopyWrite = 5;
        /// <summary>
        /// Use this constant with the Sal.FileSeek function to position the file pointer at the beginning of a file. Value = 0
        /// </summary>
        public const int FILE_SeekBegin = 0;
        /// <summary>
        /// Use this constant with the Sal.FileSeek function to position the file pointer at its current location in the file. Value = 1
        /// </summary>
        public const int FILE_SeekCurrent = 1;
        /// <summary>
        /// Use this constant with the Sal.FileSeek function to position the file pointer at the end of the file. Value = 2
        /// </summary>
        public const int FILE_SeekEnd = 2;
        /// <summary>
        /// This constant instructs to open a file in unicode mode. 
        /// Default translations of carriage return/line feed combinations are suppressed. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions.
        /// </summary>
        public const int OF_Unicode = 0x20000;
        /// <summary>
        /// This constant instructs to open a file in UTF7 mode. 
        /// Default translations of carriage return/line feed combinations are suppressed. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions.
        /// </summary>
        public const int OF_UTF7 = 0x40000;
        /// <summary>
        /// This constant instructs to open a file in UTF8 mode. 
        /// Default translations of carriage return/line feed combinations are suppressed. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions.
        /// </summary>
        public const int OF_UTF8 = 0x80000;
        /// <summary>
        /// This constant instructs to open a file in UTF16 mode. 		
        /// </summary>
        public const int OF_UTF16 = 0x20000;
        /// <summary>
        /// This constant instructs to create a new file. 
        /// If one already exists, it is truncated to zero length. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 0x1000
        /// </summary>
        public const int OF_Binary = 0x10000;
        /// <summary>
        /// This constant instructs to create a new file. 
        /// If one already exists, it is truncated to zero length. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 0x1000
        /// </summary>
        public const int OF_Create = 0x1000;
        /// <summary>
        /// This constant instructs to open and then close a file. 
        /// Use it to test if a file exists. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 0x4000
        /// </summary>
        public const int OF_Exist = 0x4000;
        /// <summary>
        /// This constant prepares to open a file by parsing the file specifications, but carries out no action. 
        /// Use it if you need to verify a file specification without actually opening the file. Use this constant
        /// with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 0x100
        /// </summary>
        public const int OF_Parse = 0x0100;
        /// <summary>
        /// This constant instructs to delete a file. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 0x200
        /// </summary>
        public const int OF_Delete = 0x0200;
        /// <summary>
        /// This constant instructs to open a file for reading only. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 0
        /// </summary>
        public const int OF_Read = 0;
        /// <summary>
        /// This constant instructs to open a file for writing only. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 1
        /// </summary>
        public const int OF_Write = 1;
        /// <summary>
        /// This constant instructs to open a file for reading and writing. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 2
        /// </summary>
        public const int OF_ReadWrite = 2;
        /// <summary>
        /// This constant instructs to open a file for appending. The file pointer is positioned at the end of the file. 
        /// You can combine File Open style constants using the bitwise OR (|) operator. Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 3
        /// </summary>
        public const int OF_Append = 3;
        /// <summary>
        /// This constant instructs to open a file for reading and appending of new data. 
        /// The file pointer is positioned at the end of the file so that you can read/write the opened file. 
        /// Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 4
        /// </summary>
        public const int OF_ReadAppend = 4;
        /// <summary>
        /// Use this constant only with the Sal.FileOpenExt function to instruct to open a file using the name specified in a previous Sal.FileOpen call. 
        /// You must have previously opened the file with a legitimate file name and used the same file handle. 
        /// When first opens the file, it stores the full path name internally. 
        /// The purpose of the OF_Reopen constant is to guarantee that subsequent opens of the file use this fully qualified name, 
        /// regardless of whether directories and drives have changed since the original Sal.FileOpen. Value = 0x8000
        /// </summary>
        [Obsolete("Not supported.")]
        public const int OF_Reopen = 0x8000;
        /// <summary>
        /// This constant instructs to open a file in compatibility mode. This lets any computer open a file any number of times. 
        /// Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 0
        /// </summary>
        public const int OF_Share_Compat = 0;
        /// <summary>
        /// This constant instructs to open a file without denying other applications read and write access to it. 
        /// Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 64
        /// </summary>
        public const int OF_Share_Deny_None = 64;
        /// <summary>
        /// This constant instructs to open a file and deny other applications read access to it. 
        /// Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 48
        /// </summary>
        public const int OF_Share_Deny_Read = 48;
        /// <summary>
        /// This constant instructs to open a file, denying other applications write access to it. 
        /// Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 32
        /// </summary>
        public const int OF_Share_Deny_Write = 32;
        /// <summary>
        /// This constant instructs to open a file, denying other applications read and write access to it. 
        /// Use this constant with the Sal.FileOpen and Sal.FileOpenExt functions. Value = 16
        /// </summary>
        public const int OF_Share_Exclusive = 16;
    }
}
