﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace fecherFoundation.VisualBasicLayer
{
	internal class FCFile
	{
		// internal IO objects
		private FileStream m_stream;
		private BinaryReader m_reader;
		private BinaryWriter m_writer;
		private Encoding m_encoding;
		// binary mode flag
		private bool m_isBinary;

		/// <summary>
		/// Opens, re-opens, creates, or deletes a file.
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="mode"></param>
		/// <param name="access"></param>
		public FCFile(string fileName, FileMode mode, FileAccess access, FileShare share, Encoding encoding, bool binary)
		{
            string directoryName = Path.GetDirectoryName(fileName);
            m_isBinary = binary;
			m_encoding = encoding;

            //CHE: create file if does not exist
            if (!File.Exists(fileName) && (mode == FileMode.Create || mode == FileMode.OpenOrCreate))
            {
                //FC:FINAL:RPU: Create directory if does not exist
                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }
                m_stream = File.Create(fileName);
            }
            else
            {
                m_stream = File.Open(fileName, mode, access, share);
            }
			
			if ((access & FileAccess.Read) == FileAccess.Read)
				m_reader = new BinaryReader(m_stream, encoding);

			if ((access & FileAccess.Write) == FileAccess.Write)
				m_writer = new BinaryWriter(m_stream, encoding);

			if (m_reader != null && !binary)
			{
				m_encoding = ReadPreamble();
				if (m_encoding != encoding)
					m_reader = new BinaryReader(m_stream, m_encoding);
			}
		}


		/// <summary>
		/// Returns the wrapped FileStream object.
		/// </summary>
		public FileStream FileStream
		{
			get { return m_stream; }
		}

		/// <summary>
		/// Returns the encoding.
		/// </summary>
		public Encoding Encoding
		{
			get { return m_encoding; }
		}

		/// <summary>
		/// Returns true if the file was opened in binary mode.
		/// </summary>
		public bool IsBinary
		{
			get { return m_isBinary; }
		}

		/// <summary>
		/// Closes the file.
		/// </summary>
		public void Close()
		{
			m_stream.Close();
		}

		/// <summary>
		/// Returns the next character in an open file.
		/// </summary>
		/// <returns>byte</returns>
		public int GetChar()
		{
			try
			{
				if (m_reader == null)
					return -1;

				if (m_isBinary)
					return m_reader.ReadByte();
				else
					return m_reader.ReadChar();
			}
			catch (EndOfStreamException)
			{
				return -1;
			}
		}

		/// <summary>
		/// Returns the next line from an open file.
		/// </summary>
		/// <param name="size">The maximum number of bytes to read.</param>
		/// <returns>string</returns>
		public string GetString(int size)
		{
			if (m_reader == null)
				return null;

			if (size < 1)
				return string.Empty;
			
			int len = 0;
			bool lf = false;
			bool eol = false;
			bool eof = false;
			char[] buf = new char[size];
			while (!eol && !eof && len < size)
			{
				int c = GetChar();
				buf[len++] = (char)c;

				switch (c)
				{
					case -1:
						len--;
						eof = true;
						break;

					case '\n':
						len--;
						eol = true;
						break;

					case '\r':
						lf = true;
						break;
						
					default:
						lf = false;
						break;
				}
			}

			if (eof && len == 0)
				return null;

			// if the line terminated in the middle or \r\n, skip the next \n
			if (lf && !eol)
			{
				long pos = m_reader.BaseStream.Position;
				eol = GetChar() == '\n';
				if (!eol)
					m_reader.BaseStream.Position = pos;
			}
			if (lf && eol)
				len--;

			return new string(buf, 0, len);
		}

		/// <summary>
		/// Writes a character to an open file.
		/// <param name="nChar"></param>
		/// <returns></returns>
		public bool PutChar(int c)
		{ 
			if (m_writer == null)
				return false;

			if (m_isBinary)
				m_writer.Write((byte)c);
			else
				m_writer.Write((char)c);

			return true;
		}

		/// <summary>
		/// Writes a string to an open file.
		/// </summary>
		/// <param name="str"></param>
		public bool PutString(string str, bool newLine = true)
		{
			if (str == null)
				return true;

			// write all chars in the string
			foreach(char c in str)
			{
				if (!this.PutChar(c))
					return false;
			}

            // write line terminator
            if (newLine)
            {
                this.PutChar('\r');
                this.PutChar('\n');
            }
			return true;
		}

		/// <summary>
		/// Reads a buffer of bytes from an open file.
		/// </summary>
		/// <param name="size"></param>
		/// <returns></returns>
		public byte[] ReadBytes(int size)
		{
			if (m_reader == null)
				return null;

			if (size < 1)
				return null;

			byte[] buf = m_reader.ReadBytes(size);
			return (buf.Length == 0) ? null : buf;
		}

		/// <summary>
		/// Reads a buffer of characters from an open file.
		/// </summary>
		/// <param name="size"></param>
		/// <returns></returns>
		public char[] ReadChars(int size)
		{
			if (m_reader == null)
				return null;

			if (size < 0)
				return null;

			char[] buf = m_reader.ReadChars(size);
			return (buf.Length == 0) ? null : buf;
		}

        /// <summary>
        /// Read the entire stream into a string
        /// </summary>
        /// <returns></returns>
        public string ReadAll()
        {
            if (m_reader == null)
                return null;
            string chars = "";
            int charNumber = 0;
            do
            {
                charNumber = m_reader.Read();
                if (charNumber != -1)
                {
                    chars += (char)charNumber;
                }
            }
            while (charNumber != -1);

            return chars;
        }

		/// <summary>
		/// Positions the file pointer in an open file. The next file operation (such as a read
		/// or write) takes place at this new location.
		/// </summary>
		/// <param name="bytes"></param>
		/// <param name="position"></param>
		public void Seek(long offset, SeekOrigin origin)
		{
			m_stream.Seek(offset, origin);
		}

		/// <summary>
		/// Returns the current position in an open file.
		/// </summary>
		/// <returns>position</returns>
		public long Tell()
		{
			return m_stream.Position;
		}

		/// <summary>
		/// Writes a sequence of bytes to the open file.
		/// </summary>
		/// <param name="buf"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		public int Write(byte[] buf, int size)
		{
			if (buf == null || size < 1)
				return 0;

			if (m_writer == null)
				return 0;

			size = Math.Min(buf.Length, size);
			m_writer.Write(buf, 0, size);
			return size;
		}

		/// <summary>
		/// Writes a sequence of characters to the open file.
		/// </summary>
		/// <param name="buf"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		public int Write(char[] buf, int size)
		{
			if (buf == null || size < 1)
				return 0;

			if (m_writer == null)
				return 0;

			size = Math.Min(buf.Length, size);
			m_writer.Write(buf, 0, size);
			return size;
		}

		/// <summary>
		/// Writes the encoding preamble to the file only if the
		/// stream is at position 0.
		/// </summary>
		internal void WritePreamble()
		{
			if (m_stream.Position == 0)
			{
				byte[] bom = m_encoding.GetPreamble();
				if (bom != null && bom.Length > 0)
					m_stream.Write(bom, 0, bom.Length);
			}
		}

		/// <summary>
		/// Reads the preamble and detects the encoding.
		/// </summary>
		private Encoding ReadPreamble()
		{
			byte[] bom = new byte[3];
			int count = m_stream.Read(bom, 0, 3);

			if (count >= 2)
			{
				Encoding[] encodings = new Encoding[] { Encoding.Unicode, Encoding.UTF7, Encoding.UTF8 };
				foreach (Encoding enc in encodings)
				{
					byte[] preamble = enc.GetPreamble();
					if (EqualPreambles(bom, preamble))
					{
						// set the position after the preamble
						m_stream.Seek(preamble.Length, SeekOrigin.Begin);
						// return the encoding detected using the preamble
						return enc;
					}
				}
			}
			
			// reset to the beginning, if the preamble is not recognized
			m_stream.Seek(0, SeekOrigin.Begin);
			// return the default encoding
			return m_encoding;
		}

        /// <summary>
        /// Checks if preambles are equal, returns bool.
        /// </summary>
        /// <param name="bom"></param>
        /// <param name="preamble"></param>
        /// <returns></returns>
		private bool EqualPreambles(byte[] bom, byte[] preamble)
		{
			if (preamble.Length > bom.Length)
				return false;
			if (preamble.Length == 0 || bom.Length == 0)
				return false;

			for (int i = 0; i < preamble.Length; i++)
				if (bom[i] != preamble[i])
					return false;

			return true;
		}
	}
}
