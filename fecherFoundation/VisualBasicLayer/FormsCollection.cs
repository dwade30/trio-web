﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation.VisualBasicLayer
{   
    /// <summary>
    /// Extends CollectionBase providing a strongly typed collection.
    /// Provides Add / Remove / this.[index] functionality.
    /// </summary>
    public class FormsCollection : CollectionBase
    {   
        /// <summary>
        /// Adds a form to the collection if it does not already exist in it.
        /// </summary>
        /// <param name="f">FCForm type parameter.</param>
        public void Add(FCForm f)
        {
            if (!base.List.Contains(f))
            {
                base.List.Add(f);
            }
        }

        /// <summary>
        /// Removes a form from the collection if it the list contains is. 
        /// </summary>
        /// <param name="f">FCForm type parameter.</param>
        public void Remove(FCForm f)
        {
            if (base.List.Contains(f))
            {
                base.List.Remove(f);
            }
        }

        /// <summary>
        /// Returns a form from the collection based on an integer index.
        /// </summary>
        /// <param name="index">Integer index.</param>
        /// <returns>base.List[index] as FCForm</returns>
        public FCForm this[int index]
        {
            get
            {
                return base.List[index] as FCForm;
            }
        }
    }
}
