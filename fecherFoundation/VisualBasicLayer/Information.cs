﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Wisej.Web;
using Microsoft.Win32;
using System.Collections.Generic;
using fecherFoundation;
using System.Text.RegularExpressions;
using System.Reflection;
using fecherFoundation.Extensions;

namespace fecherFoundation
{

    #region enums
    public enum CompareConstants : int
    {
        vbBinaryCompare = 0,
        vbTextCompare = 1
    }

    [FlagsAttribute]
    public enum VbStrConv
    {
        Lowercase,
        ProperCase,
        Uppercase
    }
    #endregion

    public static class Information
    {
        private static ErrInformation errInf = new ErrInformation();

        /// <summary>
        /// Dictionary for the Color translation
        /// Key is the VB-Color, Value is the c# Color
        /// </summary>
        public static Dictionary<long, long> FCColor = new Dictionary<long, long>()
        {
            {Constants.vbColorControlDarkDark,    System.Drawing.SystemColors.ControlDarkDark.ToArgb()},
            {Constants.vbColorControlLightLight,   System.Drawing.SystemColors.ControlLightLight.ToArgb()},
            {Constants.vbColorControlLight,          System.Drawing.SystemColors.ControlLight.ToArgb()},
            {Constants.vbColorControlDark,          System.Drawing.SystemColors.ControlDark.ToArgb()},
            {Constants.vbColorActiveBorder,         System.Drawing.SystemColors.ActiveBorder.ToArgb()},
            {Constants.vbColorActiveCaption,        System.Drawing.SystemColors.ActiveCaption.ToArgb()},
            {Constants.vbColorAppWorkspace,      System.Drawing.SystemColors.AppWorkspace.ToArgb()},
            {Constants.vbColorControl,                 System.Drawing.SystemColors.Control.ToArgb()},
            {Constants.vbColorControlText,          System.Drawing.SystemColors.ControlText.ToArgb()},
            {Constants.vbColorDesktop,               System.Drawing.SystemColors.Desktop.ToArgb()},
            {Constants.vbColorGrayText,              System.Drawing.SystemColors.GrayText.ToArgb()},
            {Constants.vbColorHighlight,              System.Drawing.SystemColors.Highlight.ToArgb()},
            {Constants.vbColorHighlightText,        System.Drawing.SystemColors.HighlightText.ToArgb()},
            {Constants.vbColorInactiveBorder,      System.Drawing.SystemColors.InactiveBorder.ToArgb()},
            {Constants.vbColorInactiveCaptionText, System.Drawing.SystemColors.InactiveCaptionText.ToArgb()},
            {Constants.vbColorInactiveCaption,     System.Drawing.SystemColors.InactiveCaption.ToArgb()},
            {Constants.vbColorInfo,                    System.Drawing.SystemColors.Info.ToArgb()},
            {Constants.vbColorInfoText,              System.Drawing.SystemColors.InfoText.ToArgb()},
            {Constants.vbColorMenu,                  System.Drawing.SystemColors.Menu.ToArgb()},
            {Constants.vbColorMenuText,            System.Drawing.SystemColors.MenuText.ToArgb()},
            {Constants.vbColorScrollBar,              System.Drawing.SystemColors.ScrollBar.ToArgb()},
            {Constants.vbColorActiveCaptionText, System.Drawing.SystemColors.ActiveCaptionText.ToArgb()},
            {Constants.vbColorWindow,               System.Drawing.SystemColors.Window.ToArgb()},
            {Constants.vbColorWindowFrame,     System.Drawing.SystemColors.WindowFrame.ToArgb()}
        };

        /// <summary>
        /// Searches in the FCColor Dic. if the vbcolor exists and returns the corresponding c# color.
        /// </summary>
        /// <param name="vbColor"></param>
        /// <returns></returns>
        public static long ConvertVBColor(long vbColor)
        {
            if (FCColor.ContainsKey(vbColor))
            {
                return FCColor[vbColor];
            }
            else
            {
                return vbColor;
            }
        }

        public class ErrInformation : Exception 
        {
            public int Number;
            public string Description = "";
            public int LastDLLError;
            public string HelpFile = "";
            public string HelpContext = "";

            /// <summary>
            /// Generates a run-time error; can be used instead of the Error statement.
            /// </summary>
            /// <param name="value">Long integer that identifies the error. Visual Basic errors are in the range 0–65535; the range 0–512 is reserved for system errors; the range 513–65535 is available for user-defined errors as well. However, when you set the Number property for an error that you are creating, add your error code number to the vbObjectError constant. For example, to generate the error number 1000, assign vbObjectError + 1000 to the Number property.</param>
            public void Raise(int value)
            {
                throw new Exception(value.ToString());
            }

            /// <summary>
            /// Generates a run-time error; can be used instead of the Error statement.
            /// </summary>
            /// <param name="number">Long integer that identifies the error. Visual Basic errors are in the range 0–65535; the range 0–512 is reserved for system errors; the range 513–65535 is available for user-defined errors as well. However, when you set the Number property for an error that you are creating, add your error code number to the vbObjectError constant. For example, to generate the error number 1000, assign vbObjectError + 1000 to the Number property.</param>
            /// <param name="source">String expression naming the object or application that generated the error. When setting this property for an object, use the form project.class. If Source is not specified, the process ID of the current Visual Basic project is used.</param>
            /// <param name="description">String expression describing the error. If unspecified, the value in the Number property is examined. If it can be mapped to a Visual Basic run-time error code, the string that would be returned by the Error function is used as the Description property. If there is no Visual Basic error corresponding to the Number property, the "Application-defined or object-defined error" message is used.</param>
            /// <param name="helpFile"></param>
            /// <param name="helpContext"></param>
            public void Raise(int number, string source = "", string description = "", string helpFile = "", string helpContext = "")
            {
                errInf.Number = number;
                if (source != "")
                {
                    errInf.Source = source;
                }
                if (description != "")
                {
                    errInf.Description = description;
                }
                if (helpFile != "")
                {
                    errInf.HelpFile = helpFile;
                }
                if (helpContext != "")
                {
                    errInf.HelpContext = helpContext;
                }
                throw errInf;
            }

            public void Clear()
            {
                Number = 0;
                Description = "";
                Source = "";
            }
        }

        // TODO:BAN
        /// <summary>
        /// Returns an integer indicating the line number of the last executed statement. Read-only.
        /// </summary>
        /// <returns></returns>
        public static int Erl()
        {
            return 1;
        }

        /// <summary>
        /// Returns a Boolean value indicating whether an expression has no object assigned to it.
        /// </summary>
        /// <param name="obj">Object expression.</param>
        /// <returns></returns>
        public static bool IsNothing(object obj)
        {
            return obj == null;
        }

        /// <summary>
        /// Returns True if an identifier represents an object variable
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsObject(object obj)
        {
            return obj != null;
        }

        /// <summary>
        /// Returns a String value containing data-type information about a variable.
        /// </summary>
        /// <param name="obj">Object variable. If Option Strict is Off, you can pass a variable of any data type except a structure.</param>
        /// <returns></returns>
        public static string TypeName(object obj)
        {
            if (obj == null)
            {
                return "";
            }

            return obj.GetType().UnderlyingSystemType.Name;
        }

        public static ErrInformation Err()
        {
            return errInf;
        }

        public static ErrInformation Err(int errorNumber)
        {
            errInf.Number = errorNumber;
            return errInf;
        }

        public static ErrInformation Err(Exception err)
        {
            if (err != null)
            {
                ErrInformation errorInf = err as ErrInformation;
                if (err is ArgumentException)
                {
                    errInf.Number = 5;
                }
                else if (errorInf != null)
                {
                    errInf.Number = errorInf.Number;
                    errInf.Description = errorInf.Description;
                    errInf.HelpContext = errorInf.HelpContext;
                    errInf.HelpFile = errorInf.HelpFile;
                }
                else
                {
                    errInf.Number = 0;
                    var w32ex = err as System.ComponentModel.Win32Exception;
                    if (w32ex == null)
                    {
                        w32ex = err.InnerException as System.ComponentModel.Win32Exception;
                    }
                    if (w32ex != null)
                    {
                        errInf.Number = w32ex.ErrorCode;
                    }
                    else
                    {
                        //Oracle.DataAccess.Client.OracleException oracleException = err as Oracle.DataAccess.Client.OracleException;
                        //if (oracleException != null)
                        //{
                        //    errInf.Number = oracleException.ErrorCode;
                        //}
                        //else
                        {
                            System.Data.SqlClient.SqlException sqlException = err as System.Data.SqlClient.SqlException;
                            if (sqlException != null)
                            {
                                errInf.Number = sqlException.ErrorCode;
                                if(sqlException.Errors.Count > 0)
                                {
                                    errInf.Number = sqlException.Errors[0].Number + fecherFoundation.Constants.vbObjectError;
                                }
                            }
                        }
                    }
                }
                if (errorInf == null)
                {
                    if (errInf.Number == 0)
                    {
                        errInf.Number = err.HResult;
                    }
                    errInf.Description = err.Message;
                }
                errInf.Source = err.Source;
            }

            return errInf;
        }
        
        /// <summary>
        /// Returns the highest available subscript for the indicated dimension of an array.
        /// </summary>
        /// <param name="arr">Array of any data type. The array in which you want to find the highest possible subscript of a dimension.</param>
        /// <returns></returns>
        public static int UBound(this Array arr, int dimension = 1)
        {
            if (arr == null)
            {
                return -1;
            }
            return arr.GetLength(dimension - 1) - 1;
        }

        /// <summary>
        /// Returns the highest available subscript for the indicated dimension of an array.
        /// </summary>
        /// <param name="arr">Array of any data type. The array in which you want to find the highest possible subscript of a dimension.</param>
        /// <returns></returns>
        public static int UBound<T>(this List<T> arr)
        {
            if (arr == null)
            {
                return 0;
            }

            return arr.Count- 1;
        }

        /// <summary>
        /// Returns the lowest available subscript for the indicated dimension of an array.
        /// </summary>
        /// <param name="arr">Array of any data type. The array in which you want to find the lowest possible subscript of a dimension.</param>
        /// <returns></returns>
        public static int LBound(this Array arr)
        {
            return 0;
        }

        /// <summary>
        /// Returns the lowest available subscript for the indicated dimension of an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arr">Array of any data type. The array in which you want to find the lowest possible subscript of a dimension.</param>
        /// <returns></returns>
        public static int LBound<T>(this List<T> arr)
        {
            return 0;
        }

        /// <summary>
        /// Returns a Boolean value that indicates whether an expression evaluates to the System.DBNull class.
        /// </summary>
        /// <param name="obj">Object expression.</param>
        /// <returns></returns>
        public static bool IsDBNull(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            return obj == DBNull.Value;
        }

        /// <summary>
        /// Returns a Boolean value indicating whether an expression has no object assigned to it.
        /// </summary>
        /// <param name="obj">Object expression.</param>
        /// <returns></returns>
        public static bool IsNothingOrDBNull(object obj)
        {
            if (obj == null)
            {
                return true;
            }

            return obj == DBNull.Value;
        }

        /// <summary>
        /// Returns a Boolean value indicating whether an expression can be evaluated as a number.
        /// </summary>
        /// <param name="obj">Object expression.</param>
        /// <returns></returns>
        public static bool IsNumeric(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            double value;

            //JEI:IIT604: in VB6 IsNumeric is also true for such values: 
            // "123.", ".123", "123+", "123-", "123, ", "123 -", "123.12", "123,12", "+123", "-123", "+ 123", "- 123"
            string pattern = @"^[+-]?[ ]*\.*[0-9]+([ ]*[+-]*|[.,]+[0-9]*)$";

            if (double.TryParse(obj.ToString(), out value))
            {
                return true;
            }
            else
            {
                return Regex.IsMatch(obj.ToString(), pattern);
            }
        }

        /// <summary>
        /// Returns a Boolean value indicating whether an expression represents a valid Date value.
        /// </summary>
        /// <param name="obj">Object expression.</param>
        /// <param name="forceVB6Behavior">Optional flag parameter to force the VB6 behavior. Default value is false</param>
        /// <returns></returns>
        public static bool IsDate(object obj, bool forceVB6Behavior = false)
        {
			//DDU add check for DBNull and string cases
            if (obj == null || obj == DBNull.Value)
            {
                return false;
            }
			if (obj is string)
			{
				if (string.IsNullOrEmpty((string)obj))
				{
					return false;
				}
				//DDU add check for 00/00/0000 date
				else if ((string)obj == "00/00/0000")
				{
					return false;
				}
			}
            //P2218:SBE:#3742 - force the same behavior as VB6 application. For the "041218" string, IsDate() function returns false in VB6
            //For any integer number, IsDate() function returns false in VB6
            if (forceVB6Behavior)
            {
                string pattern = "^[0-9]+$";
                if (Regex.IsMatch(obj.ToString(), pattern))
                {
                    return false;
                }
            }
            return FCConvert.ToDateTime(obj) > DateTime.FromOADate(0);
		}

        public static bool IsDate(object obj, out DateTime date)
        {
            date = DateTime.FromOADate(0);
            //DDU add check for DBNull and string cases
            if (obj == null || obj == DBNull.Value)
            {
                return false;
            }
            if (obj is string)
            {
                if (string.IsNullOrEmpty((string)obj))
                {
                    return false;
                }
                //DDU add check for 00/00/0000 date
                else if ((string)obj == "00/00/0000")
                {
                    return false;
                }
            }
            date = FCConvert.ToDateTime(obj);
            return date > DateTime.FromOADate(0);
        }

        /// <summary>
        /// Returns an Integer value representing an RGB color value from a set of red, green and blue color components.
        /// </summary>
        /// <param name="R">Integer in the range 0–255, inclusive, that represents the intensity of the red component of the color.</param>
        /// <param name="G">Integer in the range 0–255, inclusive, that represents the intensity of the green component of the color.</param>
        /// <param name="B">Integer in the range 0–255, inclusive, that represents the intensity of the blue component of the color.</param>
        /// <returns></returns>
        public static int RGB(int R, int G, int B)
        {
            return ColorTranslator.ToOle(Color.FromArgb(R, G, B));
        }

        /// <summary>
        /// Returns an Integer value representing the RGB color code corresponding to the specified color number.
        /// </summary>
        /// <param name="color">Integer in the range 0 - 15</param>
        /// <returns></returns>
        public static int QBColor(int color)
        {
            if (!(color >= 0 && color <= 15))
            {
                throw new ArgumentException();
            }
            int result = -1;
            switch (color)
            {
                case 0:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.Black);
                        break;
                    }
                case 1:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.DarkBlue);
                        break;
                    }
                case 2:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
                        break;
                    }
                case 3:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.DarkCyan);
                        break;
                    }
                case 4:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.DarkRed);
                        break;
                    }
                case 5:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.DarkMagenta);
                        break;
                    }
                case 6:
                    {
                        result = ColorTranslator.ToOle(System.Windows.Forms.ControlPaint.Dark(System.Drawing.Color.Yellow));
                        break;
                    }
                case 7:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.White);
                        break;
                    }
                case 8:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.Gray);
                        break;
                    }
                case 9:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.Blue);
                        break;
                    }
                case 10:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.Green);
                        break;
                    }
                case 11:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.Cyan);
                        break;
                    }
                case 12:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.Red);
                        break;
                    }
                case 13:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.Magenta);
                        break;
                    }
                case 14:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                        break;
                    }
                case 15:
                    {
                        result = ColorTranslator.ToOle(System.Drawing.Color.White);
                        break;
                    }
            }
            return result;
        }
    }
    public static class Strings
    {
        /// <summary>
        /// Returns a String value containing the name of the specified month.
        /// </summary>
        /// <param name="month">Integer. The numeric designation of the month, from 1 through 13; 1 indicates January and 12 indicates December. You can use the value 13 with a 13-month calendar. If your system is using a 12-month calendar and Month is 13, MonthName returns an empty string.</param>
        /// <param name="abbreviate">Boolean value that indicates if the month name is to be abbreviated. If omitted, the default is False, which means the month name is not abbreviated.</param>
        /// <returns></returns>
        public static string MonthName(int month, bool abbreviate = false)
        {
            if (!abbreviate)
            {
                return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
            }
            else
            {
                return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month);
            }
        }

        /// <summary>
        /// Returns a string consisting of the specified number of spaces.
        /// </summary>
        /// <param name="spaces">Integer expression. The number of spaces you want in the string.</param>
        /// <returns></returns>
        public static string Space(int spaces)
        {
            return new String(' ', spaces);
        }

        /// <summary>
        /// Returns a string formatted according to instructions contained in a format String expression.
        /// </summary>
        /// <param name="value">Any valid expression.</param>
        /// <param name="format">A valid named or user-defined format String expression.</param>
        /// <returns></returns>
        public static string Format(object value, string format)
        {
            if (value == null)
            {
                return "";
            }

            return Format(value.ToString(), format);
        }

        /// <summary>
        /// Returns a string formatted according to instructions contained in a format String expression.
        /// </summary>
        /// <param name="value">Any valid expression.</param>
        /// <param name="format">A valid named or user-defined format String expression.</param>
        /// <returns></returns>
        public static string Format(string value, string format)
        {
			//DDU:HARRIS:#i2321 - implement format as in vb6
			if (format.Contains("&"))
			{
				int index = 0;
				string returned = "";
				for (int i = 0; i < format.Length; i++)
				{
					if (format[i] == '&')
					{
						returned += value[index];
						index++;
					}
					else
					{
						returned += format[i];
					}
				}
				return returned;
			}

            if (format.ToLower() == "currency") format = "C";
            string retVal = value;
            if (string.IsNullOrEmpty(value))
            {
                return "";
            }

			//FC:FINAL:DDU:#i1828 - implement '!' vb6 format character
			bool forceLeft = false;
			string tempFormat = "";
			foreach (var item in format.ToCharArray())
			{
				if (item == '!')
				{
					forceLeft = true;
				}
				else
				{
					tempFormat += item;
				}
			}
			format = tempFormat;

            int atCount = 0;
            foreach (var item in format.ToCharArray())
            {
                if (item == '@') atCount++;
            }
            if (atCount == format.Length)
            {
                retVal = String.Format("{0," + atCount + "}", value);
            }
            else
            {
                DateTime date;

                // JSP: Test is String is empty or unvalid: when yes VB6 returns 30.12.1899 as Startdat
                //if (DateTime.TryParse(value, out date) == false)
                // {
                //    value = "30.12.1899"; 
                //}

                DateTime.TryParse(value, out date);

                if (date == DateTime.MinValue || Information.IsNumeric(value))
                {
                    long longValue = 0;
                    int intValue = 0;
                    double doubleValue = 0;
                    float floatValue = 0;
                    if (date == DateTime.MinValue && !format.Contains("#") && !format.Contains("0") && format != "C")
                    {
                        //FC:FINAL:SBE - Harris #444 - Format using a double value and DateTimeFormat string, should convert the decimal to DateTime, and then format it
                        double dateTimeDoubleValue = 0;
                        if (Double.TryParse(value, out dateTimeDoubleValue))
                        {
                            //DateTime originDate = new DateTime(1899, 12, 30);
                            DateTime originDate = DateTime.FromOADate(dateTimeDoubleValue);
                            if (format != "")
                            {
                                retVal = originDate.ToString(format);
                            }
                            else
                            {
                                retVal = originDate.ToShortDateString();
                            }
                        }
                    }
                    else if (double.TryParse(value, out doubleValue))
                    {
                        retVal = doubleValue.ToString(format);
                    }
                    else if (long.TryParse(value, out longValue))
                    {
                        retVal = longValue.ToString(format);
                    }
                    else if (int.TryParse(value, out intValue))
                    {
                        retVal = intValue.ToString(format);
                    }
                    else
                    {
                        retVal = value;
                    }
                }
                else
                {
                    //CHE: fix AM/PM format
                    if (format.EndsWith(" AM/PM"))
                    {
                        format = format.Substring(0, format.Length - 5) + "tt";
                    }
					//FC:FINAL:MSH - issue #1775: in original date will be parsed with any type of string format ("mm/dd/yyyy" or "MM/dd/yyyy"), but C# has more strict rules for this conversion
					//Also check that format contain date format (not only time, for example "h:mm tt")
					if(format.ToLower().Contains("d") || format.ToLower().Contains("y"))
					{
						for(int i = 0; i < format.Length; i++)
						{
							if (format[i] == 'h' || format[i] == 'H')
								break;
							if(format[i] == 'm')
							{
								format = format.Insert(i, "M").Remove(i + 1, 1);
							}
							else if (format[i] == 'D')
							{
								format = format.Insert(i, "d").Remove(i + 1, 1);
							}
							else if (format[i] == 'Y')
							{
								format = format.Insert(i, "y").Remove(i + 1, 1);
							}
						}
					}

                    retVal = date.ToString(format);
                }
            }

			//FC:FINAL:DDU:#i1828 - implement '!' vb6 format character
			if (atCount > 0 && forceLeft)
			{
				retVal = retVal.Trim();
				retVal = retVal.PadRight(atCount);
			}

            return retVal;

        }

        /// <summary>
        /// Returns a string formatted according to instructions contained in a format String expression.
        /// </summary>
        /// <param name="value">Any valid expression.</param>
        /// <param name="format">A valid named or user-defined format String expression.</param>
        /// <returns></returns>
        public static string Format(DateTime value, string format)
        {
            if (value == DateTime.MinValue)
            {
                return "";
            }

            //DSE fix VB6 format variables
            switch (format.ToUpper())
            {
                case "SHORT DATE":
                    format = "d";
                    break;
                case "LONG DATE":
                    format = "D";
                    break;
                case "MEDIUM DATE":
                    string dateSeparator = CultureInfo.CurrentCulture.DateTimeFormat.DateSeparator;
                    format = "dd" + dateSeparator + "MMM" + dateSeparator + "yy";
					break;
				default:
					{
						//FC:FINAL:MSH - issue #1775: in original date will be parsed with any type of string format ("mm/dd/yyyy" or "MM/dd/yyyy"), but C# has more strict rules for this conversion
						//Also check that format contain date format (not only time, for example "h:mm tt")
						if (format.ToLower().Contains("d") || format.ToLower().Contains("y"))
						{
							for (int i = 0; i < format.Length; i++)
							{
								if (format[i] == 'h' || format[i] == 'H')
									break;
								if (format[i] == 'm')
								{
									format = format.Insert(i, "M").Remove(i + 1, 1);
								}
								else if (format[i] == 'D')
								{
									format = format.Insert(i, "d").Remove(i + 1, 1);
								}
								else if (format[i] == 'Y')
								{
									format = format.Insert(i, "y").Remove(i + 1, 1);
								}
							}
						}
						//HARRIS:DDU: check if format has AM/PM time format and set it to correct "tt"
						if (format.EndsWith(" AM/PM"))
						{
							format = format.Substring(0, format.Length - 5) + "tt";
						}
					}
                    break;
            }

            return value.ToString(format);
        }

        /// <summary>
        /// Returns a string or character containing the specified string converted to uppercase.
        /// </summary>
        /// <param name="str">Any valid String or Char expression.</param>
        /// <returns></returns>
        public static string UCase(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            //FC:FINAL:SBE - "µ".ToUpper() -> "Μ", "µ".ToUpperInvariant() -> "µ"
            //return str.ToUpper();
            return str.ToUpperInvariant();
        }

        /// <summary>
        /// Returns a string or character containing the specified string converted to uppercase.
        /// </summary>
        /// <param name="chr">Any valid String or Char expression.</param>
        /// <returns></returns>
        public static char UCase(char chr)
        {
            return Char.ToUpper(chr);
        }

        /// <summary>
        /// Returns a string or character converted to lowercase.
        /// </summary>
        /// <param name="str">Any valid String or Char expression.</param>
        /// <returns></returns>
        public static string LCase(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            return str.ToLower();
        }

        /// <summary>
        /// Returns a string in which a specified substring has been replaced with another substring a specified number of times.
        /// </summary>
        /// <param name="str">String expression containing substring to replace.</param>
        /// <param name="toReplace">Substring being searched for.</param>
        /// <param name="toReplaceWith">Replacement substring.</param>
        /// <returns></returns>
        public static string Replace(string str, string toReplace, string toReplaceWith)
        {
            return Replace(str, toReplace, toReplaceWith, 0);
        }

        /// <summary>
        /// Returns a string in which a specified substring has been replaced with another substring a specified number of times.
        /// </summary>
        /// <param name="str">String expression containing substring to replace.</param>
        /// <param name="toReplace">Substring being searched for.</param>
        /// <param name="toReplaceWith">Replacement substring.</param>
        /// <param name="start">Position within Expression where substring search is to begin. If omitted, 1 is assumed.</param>
        /// <param name="length">Number of substring substitutions to perform. If omitted, the default value is –1, which means "make all possible substitutions."</param>
        /// <param name="Compare">Numeric value indicating the kind of comparison to use when evaluating substrings. See Settings for values.</param>
        /// <returns></returns>
        public static string Replace(string str, string toReplace, string toReplaceWith, int start = 0, int length = 0, CompareConstants Compare = CompareConstants.vbBinaryCompare)
        {
            // TODO:CHE - Compare
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            return str.Replace(toReplace, toReplaceWith);
        }

        /// <summary>
        /// Returns an integer specifying the start position of the first occurrence of one string within another.
        /// </summary>
        /// <param name="str">String expression being searched.</param>
        /// <param name="search">String expression sought.</param>
        /// <returns></returns>
        public static int InStr(string str, string search)
        {
            return InStr(1, str, search);
        }

        /// <summary>
        /// Returns an integer specifying the start position of the first occurrence of one string within another.
        /// </summary>
        /// <param name="str">String expression being searched.</param>
        /// <param name="search">String expression sought.</param>
        /// <param name="vbCompare">Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
        /// <returns></returns>
        public static int InStr(string str, string search, CompareConstants? vbCompare = null)
        {
            return InStr(1, str, search, vbCompare);
        }

        /// <summary>
        /// Returns an integer specifying the start position of the first occurrence of one string within another.
        /// </summary>
        /// <param name="start">Numeric expression that sets the starting position for each search. If omitted, search begins at the first character position. The start index is 1-based.</param>
        /// <param name="str">String expression being searched.</param>
        /// <param name="search">String expression sought.</param>
        /// <param name="vbCompare">Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
        /// <returns></returns>
        public static int InStr(int start, string str, string search, CompareConstants? vbCompare = null)
        {
            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            if (string.IsNullOrEmpty(search))
            {
                return 0;
            }

            if (search.Length > str.Length)
            {
                return 0;
            }

            //CHE: in VB6 InStr is case sensitive if no compare parameter is given, but if compare parameter is given then it is not case sensitive
            //e.g. InStr(1, "abcXx","x") = 5 not 4
            //e.g. InStr(1, "abcXx","x", vbTextCompare) = 4 not 5

            //1)	a = InStr(1, "AbcXx", "x")
            //2)	a = InStr(1, "AbcXx", "x", vbTextCompare)
            //3)	a = InStr(1, "AbcXx", "x", vbBinaryCompare)

            //1)	and 3) produce the same result 5(which means case sensitive)
            //2)	produces 4 as result (case insensitive)

            //tool translates:
            //1) a = Strings.InStr(1, "AbcXx", "x", CompareConstants.vbBinaryCompare);
            //2) a = Strings.InStr(1, "AbcXx", "x", CompareConstants.vbTextCompare);
            //3) a = Strings.InStr(1, "AbcXx", "x", CompareConstants.vbBinaryCompare);

            int foundAt = -1;
            if (start == 0)
            {
                foundAt = str.IndexOf(search, start, vbCompare == CompareConstants.vbTextCompare ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture);
            }
            else
            {
                foundAt = str.IndexOf(search, start - 1, vbCompare == CompareConstants.vbTextCompare ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture);
            }

            if (foundAt == -1)
            {
                return 0;
            }

            return foundAt + 1;
        }

        /// <summary>
        /// Returns a string that contains a copy of a specified string without leading or trailing spaces (Trim).
        /// </summary>
        /// <param name="str">Any valid String expression. If str equals Nothing, the function returns an empty string.</param>
        /// <returns></returns>
        public static string Trim(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            return str.Trim();
        }

        public static string RTrim(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            return str.TrimEnd();
        }

        /// <summary>
        /// Returns a string containing a copy of a specified string with no leading spaces (LTrim).
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string LTrim(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            return str.TrimStart();
        }

        /// <summary>
        /// Returns a string that contains a specified number of characters from the left side of a string.
        /// </summary>
        /// <param name="str">String expression from which the leftmost characters are returned.</param>
        /// <param name="pos">Integer expression. Numeric expression indicating how many characters to return. If zero, a zero-length string ("") is returned. If greater than or equal to the number of characters in str, the complete string is returned.</param>
        /// <returns></returns>
        public static string Left(string str, int pos)
        {
            if (pos < 0)
            {
                throw new Exception();
            }

            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            if (pos > str.Length)
            {
                return str;
            }

            return str.Substring(0, pos);
        }

        /// <summary>
        /// Returns a string containing a specified number of characters from the right side of a string.
        /// </summary>
        /// <param name="str">String expression from which the rightmost characters are returned.</param>
        /// <param name="pos">Integer. Numeric expression indicating how many characters to return. If 0, a zero-length string ("") is returned. If greater than or equal to the number of characters in str, the entire string is returned.</param>
        /// <returns></returns>
        public static string Right(string str, int pos)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            if (pos < 0)
            {
                throw new Exception();
            }

            if (pos > str.Length)
            {
                return str;
            }

            return str.Substring(str.Length - pos);
        }

        /// <summary>
        /// Returns a string containing a specified number of characters from a string.
        /// </summary>
        /// <param name="str">String expression from which characters are returned.</param>
        /// <param name="pos">Integer expression. Starting position of the characters to return. If Start is greater than the number of characters in str, the Mid function returns a zero-length string (""). Start is one based.</param>
        /// <returns></returns>
        public static string Mid(string str, int pos)
        {
            if (str == null)
            {
                return "";
            }
            return Mid(str, pos, str.Length);
        }

        /// <summary>
        /// Returns a string containing a specified number of characters from a string.
        /// </summary>
        /// <param name="str">String expression from which characters are returned.</param>
        /// <param name="pos">Integer expression. Starting position of the characters to return. If Start is greater than the number of characters in str, the Mid function returns a zero-length string (""). Start is one based.</param>
        /// <param name="len">Integer expression. Number of characters to return. If omitted or if there are fewer than Length characters in the text (including the character at position Start), all characters from the start position to the end of the string are returned.</param>
        /// <returns></returns>
        public static string Mid(string str, int pos, int len)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }
            if (pos - 1 > str.Length)
            {
                return "";
            }
            //CHE: in VB6 does not throw exception if the last parameter is too large and exceeds the string length 
            if (pos - 1 + len > str.Length)
            {
                return str.Substring(pos - 1);
            }
            return str.Substring(pos - 1, len);
        }


        public static void MidSet(ref string str, int pos, string newStr)
        {
            //FC:FINAL:MSH - issue #1345: in VB6 Mid is equivalent of SubString in .Net and Mid returns string, which is started from 'pos' value to the end of the string 
            //MidSet(ref str, pos, 1, newStr);
            MidSet(ref str, pos, newStr.Length, newStr);
        }

        public static void MidSet(ref string str, int pos, int len, string newStr)
        {
            if(String.IsNullOrEmpty(newStr))
            {
                return;
            }
            string result = "";
			int originalLength = str?.Length ?? 0;
            result = str.Substring(0, pos -1);
            if (len < newStr.Length)
            {
                newStr = newStr.Substring(0, len);
            }
            result += newStr;
            if (result.Length > (str?.Length ?? 0))
            {
                result = result.Substring(0, str?.Length ?? 0);
            }
            else
            {
                result += str.Substring(pos + len - 1);
                if (result.Length > str.Length)
                {
                    result = result.Substring(0, str.Length);
                }
            }
			//HARRIS:DDU:#2333 - reset the length to original one to don't set errors
			if (result.Length < originalLength)
			{
				result += Strings.StrDup(originalLength - result.Length, " ");
			}
			str = result;
            
        }

        /// <summary>
        /// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        /// </summary>
        /// <param name="val">Any valid String expression or variable name. If Expression is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.</param>
        /// <returns></returns>
        public static int Len(object val)
        {
            if (val == null)
            {
                return 0;
            }
			//HARRIS:DDU:implement Len to work as in vb6 with different classes
			Type type = val.GetType();
			if (Type.GetTypeCode(type) != TypeCode.Object)
			{

				if (Type.GetTypeCode(type) == TypeCode.Boolean || Type.GetTypeCode(type) == TypeCode.Byte || Type.GetTypeCode(type) == TypeCode.Char ||
					Type.GetTypeCode(type) == TypeCode.DateTime || Type.GetTypeCode(type) == TypeCode.Decimal || Type.GetTypeCode(type) == TypeCode.Double ||
					Type.GetTypeCode(type) == TypeCode.Int16 || Type.GetTypeCode(type) == TypeCode.Int32 || Type.GetTypeCode(type) == TypeCode.Int64 ||
					Type.GetTypeCode(type) == TypeCode.SByte || Type.GetTypeCode(type) == TypeCode.Single || Type.GetTypeCode(type) == TypeCode.String ||
					Type.GetTypeCode(type) == TypeCode.UInt16 || Type.GetTypeCode(type) == TypeCode.UInt32 || Type.GetTypeCode(type) == TypeCode.UInt64)
				{
					return Len(val.ToString());
				}
			}
			else
			{
				int returnsNumber = 0;
				FieldInfo[] fields = type.GetFields();
				for (int i = 0; i < fields.Length; i++)
				{
					var result = type.InvokeMember(fields[i].Name, BindingFlags.GetField, null, val, null);
					returnsNumber += result.ToString().Length;
				}
				return returnsNumber;
			}
			return 0;
        }

        /// <summary>
        /// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
        /// </summary>
        /// <param name="str">Any valid String expression or variable name. If Expression is of type Object, the Len function returns the size as it will be written to the file by the FilePut function.</param>
        /// <returns></returns>
        public static int Len(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            return str.Length;
        }

        /// <summary>
        /// Returns the character associated with the specified character code.
        /// </summary>
        /// <param name="value">An Integer expression representing the code point, or character code, for the character. If CharCode is outside the valid range, an ArgumentException error occurs. The valid range for Chr is 0 through 255, and the valid range for ChrW is -32768 through 65535.</param>
        /// <returns></returns>
        public static char Chr(int value)
        {
            if (value < 0)
            {
                return '\0';
            }

            return (char)value;
        }

        /// <summary>
        /// Returns an Integer value that represents the character code corresponding to a character.
        /// </summary>
        /// <param name="value">Any valid Char or String expression. If String is a String expression, only the first character of the string is used for input. If String is Nothing or contains no characters, an ArgumentException error occurs.</param>
        /// <returns></returns>
        public static int Asc(char value)
        {
            return Convert.ToInt32(value);
        }

        /// <summary>
        /// Returns an Integer value that represents the character code corresponding to a character.
        /// </summary>
        /// <param name="value">Any valid Char or String expression. If String is a String expression, only the first character of the string is used for input. If String is Nothing or contains no characters, an ArgumentException error occurs.</param>
        /// <returns></returns>
        public static int Asc(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return 0;
            }

            if (value.Length > 1)
            {
                return 0;
            }

            return Convert.ToInt32(value[0]);
        }

        /// <summary>
        /// Returns a string or object consisting of the specified character repeated the specified number of times.
        /// </summary>
        /// <param name="len">Integer expression. The length to the string to be returned.</param>
        /// <param name="value">Any valid Char, String, or Object expression. Only the first character of the expression will be used. If Character is of type Object, it must contain either a Char or a String value. </param>
        /// <returns></returns>
        public static string StrDup(int len, string value)
        {
            if (len < 0) len = 0;
            return string.Join("", Enumerable.Repeat(FCConvert.ToString(value), len));
        }

        /// <summary>
        /// Returns a zero-based, one-dimensional array containing a specified number of substrings.
        /// </summary>
        /// <param name="str">String expression containing substrings and delimiters.</param>
        /// <returns></returns>
        public static string[] Split(string str)
        {
            return Split(str, " ");
        }

        /// <summary>
        /// Returns a zero-based, one-dimensional array containing a specified number of substrings.
        /// </summary>
        /// <param name="str">String expression containing substrings and delimiters.</param>
        /// <param name="splitBy">Any string of characters used to identify substring limits. If Delimiter is omitted, the space character (" ") is assumed to be the delimiter.</param>
        /// <param name="limit">Maximum number of substrings into which the input string should be split. The default, –1, indicates that the input string should be split at every occurrence of the Delimiter string.</param>
        /// <returns></returns>
        public static string[] Split(string str, string splitBy, int limit = -1, CompareConstants compare = CompareConstants.vbTextCompare)
        {
            if (string.IsNullOrEmpty(str))
            {
                return new string[0];
            }

            if (limit == -1)
            {
                return str.Split(new string[] { splitBy }, StringSplitOptions.None);
            }
            else
            {
                return str.Split(new string[] { splitBy }, limit, StringSplitOptions.None);
            }
        }

        /// <summary>
        /// Returns a left-aligned string containing the specified string, adjusted to the specified length.
        /// </summary>
        /// <param name="source">String expression. String variable to be adjusted.</param>
        /// <param name="length">Integer expression. Length of returned string.</param>
        /// <returns></returns>
        public static string LSet(string source, int length)
        {
            if (length == 0)
            {
                return "";
            }
            if (source == null)
            {
                return new string(' ', length);
            }
            if (length > source.Length)
            {
                return source.PadRight(length);
            }
            return source.Substring(0, length);
        }

        /// <summary>
        /// Returns a right-aligned string containing the specified string adjusted to the specified length. 
        /// </summary>
        /// <param name="source">String expression. String to be adjusted.</param>
        /// <param name="length">Integer expression. Length of returned string.</param>
        /// <returns></returns>
        public static string RSet(string source, int length)
        {
            if (length == 0)
            {
                return "";
            }
            if (source == null)
            {
                return new string(' ', length);
            }
            if (length > source.Length)
            {
                return source.PadLeft(length);
            }
            return source.Substring(0, length);
        }

        /// <summary>
        /// Returns the position of the first occurrence of one string within another, starting from the right side of the string.
        /// </summary>
        /// <param name="StringCheck">String expression being searched.</param>
        /// <param name="StringMatch">String expression being searched for.</param>
        /// <param name="Start">Numeric expression setting the one-based starting position for each search, starting from the left side of the string. If Start is omitted then –1 is used, meaning the search begins at the last character position. Search then proceeds from right to left.</param>
        /// <param name="Compare">Numeric value indicating the kind of comparison to use when evaluating substrings. If omitted, a binary comparison is performed. See Settings for values.</param>
        /// <returns></returns>
        public static int InStrRev(string StringCheck, string StringMatch, int Start = -1, CompareConstants Compare = CompareConstants.vbBinaryCompare)            
        {
            // TODO:CHE - Compare
            try
            {
                if (Start == 0 || Start < -1)
                {
                    return -1;
                }
                else
                {
                    int num = StringCheck != null ? StringCheck.Length : 0;
                    if (Start == -1)
                        Start = num;
                    if (Start > num || num == 0)
                        return 0;
                    if (StringMatch == null || StringMatch.Length == 0)
                        return Start;
                    else
                        return checked(StringCheck.LastIndexOf(StringMatch, Start - 1, Start) + 1);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns a Char value representing the character from the specified index in the supplied string.
        /// </summary>
        /// <param name="str">Any valid String expression.</param>
        /// <param name="Index">Integer expression. The (1-based) index of the character in str to be returned.</param>
        /// <returns></returns>
        public static char GetChar(string str, int Index)
        {
            if (str == null)
                return '\0';
            else if (Index < 1)
                return '\0';
            else
            {
                if (Index <= str.Length)
                    return str[checked(Index - 1)];
            }

            return '\0';
        }

        /// <summary>
        /// The LenB function in earlier versions of Visual Basic returns the number of bytes in a string rather than characters. It is used primarily for converting strings in double-byte character set (DBCS) applications.
        /// </summary>
        /// <param name="value">Any valid String expression or variable name.</param>
        /// <returns></returns>
        public static int LenB(string value)
        {
            //CHE
            //int returnValue = 1;
            //TODO:PJ
            int returnValue = 0;
            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    Byte[] valueBytes = FCUtils.GetBytes(value);
                    returnValue = valueBytes.Length;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return returnValue;
        }

        /// <summary>
        /// The MidB function in previous versions of Visual Basic returns a string in bytes rather than characters. It is used primarily for converting strings in double-byte character set (DBCS) applications.
        /// </summary>
        /// <param name="value">String expression from which characters are returned.</param>
        /// <param name="start">Integer expression. Starting position of the characters to return. If Start is greater than the number of characters in str, the Mid function returns a zero-length string (""). Start is one based.</param>
        /// <param name="length">Integer expression. Number of characters to return. If omitted or if there are fewer than Length characters in the text (including the character at position Start), all characters from the start position to the end of the string are returned.</param>
        /// <returns></returns>
        public static string MidB(string value, int start, int length = 1)
        {
            //CHE
            string returnValue = "";
            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    Byte[] valueBytes = FCUtils.GetBytes(value);
                    Byte[] result = new Byte[Math.Min(length, valueBytes.Length - start + 1)];
                    for (int i = 0; i < result.Length; i++)
                    {
                        result[i] = valueBytes[start + i - 1];
                    }
                    returnValue = FCUtils.GetString(result);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return returnValue;
        }

        public static void MidBSet(ref string value, int start, string newValue)
        {
            //CHE
            try
            {
                Byte[] valueBytes = FCUtils.GetBytes(value);
                Byte[] newValueBytes = FCUtils.GetBytes(newValue);
                Byte[] result = new Byte[valueBytes.Length];
                for (int i = 0; i < start - 1; i++)
                {
                    result[i] = valueBytes[i];
                }
                if (newValueBytes != null)
                {
                    for (int i = 0; i < newValueBytes.Length; i++)
                    {
                        result[i + start - 1] = newValueBytes[i];
                    }
                    for (int i = start + newValueBytes.Length - 1; i < valueBytes.Length; i++)
                    {
                        result[i] = valueBytes[i];
                    }
                }
                value = FCUtils.GetString(result);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// The LeftB function in earlier versions of Visual Basic returns a string in bytes instead of characters. It is primarily used for converting strings in double-byte character set (DBCS) applications. 
        /// </summary>
        /// <param name="str">String expression from which the leftmost characters are returned.</param>
        /// <param name="length">Integer expression. Numeric expression indicating how many characters to return. If zero, a zero-length string ("") is returned. If greater than or equal to the number of characters in str, the complete string is returned.</param>
        /// <returns></returns>
        public static string LeftB(string str, int length)
        {
            //CHE
            string returnValue = "";
            if (!string.IsNullOrEmpty(str))
            {
                try
                {
                    Byte[] bytes = FCUtils.GetBytes(str);
                    int min = Math.Min(length, str.Length * sizeof(char));
                    Byte[] result = new Byte[min];
                    for (int i = 0; i < min; i++)
                    {
                        result[i] = bytes[i];
                    }
                    returnValue = FCUtils.GetString(result);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return returnValue;
        }

        /// <summary>
        /// The RightB function in earlier versions of Visual Basic returns a string in bytes, rather than characters. It is used primarily for converting strings in double-byte character set (DBCS) applications.
        /// </summary>
        /// <param name="str">String expression from which the rightmost characters are returned.</param>
        /// <param name="length">Integer. Numeric expression indicating how many characters to return. If 0, a zero-length string ("") is returned. If greater than or equal to the number of characters in str, the entire string is returned.</param>
        /// <returns></returns>
        public static string RightB(string str, int length)
        {
            //CHE
            string returnValue = "";
            if (!string.IsNullOrEmpty(str))
            {
                try
                {
                    Byte[] bytes = FCUtils.GetBytes(str);
                    int min = length;
                    Byte[] result = new Byte[min];
                    for (int i = 0; i < min; i++)
                    {
                        result[min - i - 1] = bytes[bytes.Length - i - 1];
                    }
                    returnValue = FCUtils.GetString(result);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return returnValue;
        }

        /// <summary>
        /// The InStrB function in previous versions of Visual Basic returns a number of bytes rather than a character position. It is used primarily for converting strings in double-byte character set (DBCS) applications. 
        /// </summary>
        /// <param name="str1">String expression being searched.</param>
        /// <param name="str2">String expression sought.</param>
        /// <returns></returns>
        public static int InStrB(string str1, string str2)
        {
            //CHE
            int returnValue = -1;
            if (!string.IsNullOrEmpty(str1) && !string.IsNullOrEmpty(str2))
            {
                try
                {
                    Byte[] str1Bytes = FCUtils.GetBytes(str1);
                    Byte[] str2Bytes = FCUtils.GetBytes(str2);

                    int i = 0;
                    while (i < str1Bytes.Length)
                    {
                        if (str1Bytes[i] == str2Bytes[0])
                        {
                            int j = 0;
                            returnValue = i; //First Position of the same Byte
                            bool ok = true;
                            while (i < str1Bytes.Length && j < str2Bytes.Length)
                            {
                                if (str1Bytes[i] != str2Bytes[j])
                                {
                                    ok = false;
                                    break;
                                }
                                i++; j++;
                            }
                            if (ok && j != str2Bytes.Length)
                            {
                                ok = false;
                            }
                            if (ok)
                            {
                                return 1;
                            }
                        }
                        i++;
                    }

                    return 0;
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return returnValue;
        }

        public static string FormatCurrency(double v1, int v2)
        {
            return v1.ToString("N" + v2.ToString());
        }

        /// <summary>
        /// Formats expression as currency
        /// </summary>
        /// <param name="Expression"></param>
        /// <param name="NumDigitsAfterDecimal"></param>
        /// <param name="IncludeLeadingDigit"></param>
        /// <param name="UseParensForNegativeNumbers"></param>
        /// <param name="GroupDigits"></param>
        /// <returns></returns>
        public static string FormatCurrency(decimal Expression, int NumDigitsAfterDecimal = 0, VbTriState IncludeLeadingDigit = VbTriState.vbUseDefault, VbTriState UseParensForNegativeNumbers = VbTriState.vbUseDefault, VbTriState GroupDigits = VbTriState.vbUseDefault)
        {
			//FC:FINAL:DDU:#i1827 - implemented FormatCurrency
			string result = "";
			result = Expression.ToString("C" + NumDigitsAfterDecimal);

			if (IncludeLeadingDigit == VbTriState.vbFalse && Expression < 1 && result.Contains("0."))
			{
				result = result.Remove(result.IndexOf("0."), 1);
			}

			if (UseParensForNegativeNumbers == VbTriState.vbFalse && result.Contains("(") && result.Contains(")"))
			{
				result = result.TrimStart('(');
				result = result.TrimEnd(')');
				char[] x = result.ToCharArray();
				if (!char.IsDigit(x[0]) && x[0] != '.')
				{
					result = result.Insert(1, "-");
				}
				else
				{
					result = "-" + result;
				}
			}

			if (GroupDigits == VbTriState.vbFalse)
			{
				result = result.Replace(",", "");
			}

			return result;
        }

        /// <summary>
        /// Returns -1, 0, or 1, based on the result of a string comparison.
        /// </summary>
        /// <param name="String1">Required. Any valid String expression.</param>
        /// <param name="String2">Required. Any valid String expression.</param>
        /// <param name="Compare">Optional. Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
        /// <returns></returns>
        public static int StrComp(string String1, string String2, CompareConstants Compare = CompareConstants.vbBinaryCompare)
        {
            try
            {
                if (Compare == CompareConstants.vbBinaryCompare)
                {
                    return CompareString(String1, String2, false);
                }

                return CompareString(String1, String2, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns -1, 0, or 1, based on the result of a string comparison.
        /// </summary>
        /// <param name="Left">Required. Any valid String expression.</param>
        /// <param name="Right">Required. Any valid String expression.</param>
        /// <param name="TextCompare">Optional. Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
        /// <returns></returns>
        public static int CompareString(string Left, string Right, bool TextCompare)
        {
            if (Left == Right)
                return 0;
            if (Left == null)
                return Right.Length == 0 ? 0 : -1;
            else if (Right == null)
            {
                return Left.Length == 0 ? 0 : 1;
            }
            else
            {
                int num = !TextCompare ? string.CompareOrdinal(Left, Right) : System.Threading.Thread.CurrentThread.CurrentCulture.CompareInfo.Compare(Left, Right, CompareOptions.IgnoreCase | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth);
                if (num == 0)
                    return 0;
                return num > 0 ? 1 : -1;
            }
        }

        public static bool CompareString(string sL, string sOperation, string sR)
        {
            int iRet = string.Compare(sL, sR);
            switch (sOperation)
            {
                case ">": return iRet > 0;
                case "<": return iRet < 0;
                case ">=": return iRet >= 0;
                case "<=": return iRet <= 0;
                case "<>": return iRet != 0;
                case "=": return iRet == 0;
            }
            return false;
        }

        /// <summary>
        /// TODO: Implement Method
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string StrP(object obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str">Required. String expression to be converted.</param>
        /// <param name="conversion">Required. VbStrConv member. The enumeration value specifying the type of conversion to perform.</param>
        /// <returns>Returns a string converted as specified.</returns>
        public static string StrConv(string str, VbStrConv conversion)
        {
            if (String.IsNullOrEmpty(str))
            {
                return str;
            }
            switch (conversion)
            {
                case VbStrConv.Lowercase: return str.ToLower(); break;
                case VbStrConv.ProperCase: return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower()); break;
                case VbStrConv.Uppercase: return str.ToUpper(); break;
                default: return str;
            }
        }
    }

    public static class Conversion
    {
        /// <summary>
        /// Returns a string representing the hexadecimal value of a number.
        /// </summary>
        /// <param name="obj">Any valid numeric expression or String expression.</param>
        /// <returns></returns>
        public static string Hex(object obj)
        {
            if (obj == null)
            {
                return "";
            }

            return Hex(Convert.ToInt32(obj));
        }

        /// <summary>
        /// Returns a string representing the hexadecimal value of a number.
        /// </summary>
        /// <param name="obj">Any valid numeric expression or String expression.</param>
        /// <returns></returns>
        public static string Hex(int obj)
        {
            return obj.ToString("X");
        }

        /// <summary>
        /// Return the integer portion of a number.
        /// </summary>
        /// <param name="value">A number of type Double or any valid numeric expression. If Number contains Nothing, Nothing is returned.</param>
        /// <returns></returns>
        public static double Int(double value)
        {
            return Math.Floor(value);
        }

        /// <summary>
        /// Returns the numbers contained in a string as a numeric value of appropriate type.
        /// </summary>
        /// <param name="value">Any valid String expression, Object variable, or Char value. If Expression is of type Object, its value must be convertible to String or an ArgumentException error occurs.</param>
        /// <returns></returns>
        public static double Val(object Expression)
        {
            string text = Expression as string;
            if (text != null)
            {
                return Conversion.Val(text);
            }
            if (Expression is char)
            {
                return (double)Conversion.Val((char)Expression);
            }
            //AM:HARRIS:#2639 - Val returns 0 for boolean values
            if (Expression is bool)
            {
                return 0;
            }
            if (VersionedIsNumeric(Expression))
            {
                return ConversionsToDouble(Expression);
            }
            string inputStr = "";
            try
            {
                inputStr = ConversionsToString(Expression);
            }
            catch (StackOverflowException ex)
            {
                throw ex;
            }
            catch (OutOfMemoryException ex2)
            {
                throw ex2;
            }
            //catch (ThreadAbortException ex3)
            //{
            //    throw ex3;
            //}
            catch (Exception)
            {
            //    throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_InvalidValueType2", new string[]
            //    {
            //"Expression",
            //Utils.VBFriendlyName(Expression)
            //    })), 438);
            }
            return Conversion.Val(inputStr);
        }

        public static double Val(string InputStr)
        {
            int num;
            if (InputStr == null)
            {
                num = 0;
            }
            else
            {
                num = InputStr.Length;
            }
            checked
            {
                int i;
                char c;
                for (i = 0; i < num; i++)
                {
                    c = InputStr[i];
                    char c2 = c;
                    if (c2 != '\t')
                    {
                        if (c2 != '\n')
                        {
                            if (c2 != '\r')
                            {
                                if (c2 != ' ')
                                {
                                    if (c2 != '\u3000')
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (i >= num)
                {
                    return 0.0;
                }
                c = InputStr[i];
                if (c == '&')
                {
                    return ConversionHexOrOctValue(InputStr, i + 1);
                }
                bool flag = false;
                bool flag2 = false;
                bool flag3 = false;
                double num2 = 0.0;
                c = InputStr[i];
                if (c == '-')
                {
                    flag3 = true;
                    i++;
                }
                else if (c == '+')
                {
                    i++;
                }
                int num3 = 0;
                double num4 = 0;
                int num5 = 0;
                while (i < num)
                {
                    c = InputStr[i];
                    char c3 = c;
                    if (c3 != '\t')
                    {
                        if (c3 != '\n')
                        {
                            if (c3 != '\r')
                            {
                                if (c3 != ' ')
                                {
                                    if (c3 != '\u3000')
                                    {
                                        if (c3 == '0')
                                        {
                                            if (num3 != 0 || flag)
                                            {
                                                num4 = unchecked(num4 * 10.0 + (double)c - 48.0);
                                                i++;
                                                num3++;
                                                continue;
                                            }
                                            i++;
                                            continue;
                                        }
                                        else
                                        {
                                            if (c3 >= '1' && c3 <= '9')
                                            {
                                                num4 = unchecked(num4 * 10.0 + (double)c - 48.0);
                                                i++;
                                                num3++;
                                                continue;
                                            }
											if (c3 == ',')
											{
												i++;
												continue;
											}
                                            if (c3 != '.')
                                            {
                                                if (c3 != 'e')
                                                {
                                                    if (c3 != 'E')
                                                    {
                                                        if (c3 != 'd')
                                                        {
                                                            if (c3 != 'D')
                                                            {
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                flag2 = true;
                                                i++;
                                                break;
                                            }
                                            i++;
                                            if (!flag)
                                            {
                                                flag = true;
                                                num5 = num3;
                                                continue;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i++;
                }
                int num6 = 0;
                if (flag)
                {
                    num6 = num3 - num5;
                }
                if (flag2)
                {
                    bool flag4 = false;
                    bool flag5 = false;
                    while (i < num)
                    {
                        c = InputStr[i];
                        char c4 = c;
                        if (c4 != '\t')
                        {
                            if (c4 != '\n')
                            {
                                if (c4 != '\r')
                                {
                                    if (c4 != ' ')
                                    {
                                        if (c4 != '\u3000')
                                        {
                                            if (c4 >= '0' && c4 <= '9')
                                            {
                                                num2 = unchecked(num2 * 10.0 + (double)c - 48.0);
                                                i++;
                                                continue;
                                            }
                                            if (c4 == '+')
                                            {
                                                if (!flag4)
                                                {
                                                    flag4 = true;
                                                    i++;
                                                    continue;
                                                }
                                                break;
                                            }
                                            else
                                            {
                                                if (c4 == '-' && !flag4)
                                                {
                                                    flag4 = true;
                                                    flag5 = true;
                                                    i++;
                                                    continue;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        i++;
                    }
                    unchecked
                    {
                        if (flag5)
                        {
                            num2 += (double)num6;
                            num4 *= Math.Pow(10.0, -num2);
                        }
                        else
                        {
                            num2 -= (double)num6;
                            num4 *= Math.Pow(10.0, num2);
                        }
                    }
                }
                else if (flag && num6 != 0)
                {
                    num4 /= Math.Pow(10.0, (double)num6);
                }
                if (double.IsInfinity(num4))
                {
                    //throw ExceptionUtils.VbMakeException(6);
                }
                if (flag3)
                {
                    num4 = unchecked(-num4);
                }
                char c5 = c;
                if (c5 == '%')
                {
                    if (num6 > 0)
                    {
                        //throw ExceptionUtils.VbMakeException(13);
                    }
                    num4 = (double)((short)Math.Round(num4));
                }
                else if (c5 == '&')
                {
                    if (num6 > 0)
                    {
                        //throw ExceptionUtils.VbMakeException(13);
                    }
                    num4 = (double)(Convert.ToInt32(Math.Round(num4)));
                }
                else if (c5 == '!')
                {
                    num4 = (double)((float)num4);
                }
                else if (c5 == '@')
                {
                    num4 = Convert.ToDouble(new decimal(num4));
                }
                return num4;
            }
        }

        public static int Val(char Expression)
        {
            if (Expression >= '1' && Expression <= '9')
            {
                return Convert.ToInt32((checked(Expression - '0')));
            }
            return 0;
        }

        //FC:FINAL:RPU:#346: Make CCur function as in VB6 
        /// <summary>
        ///  Converts an expression to a Currency
        /// </summary>
        /// <param name="Expression"></param>
        /// <returns></returns>
        public static Decimal CCur(string Expression)
        {
			//FC:FINAL:BB:#i2213 - fix Conversion.CCur to convert format strings like "$#,##0.00"
			//Decimal number = Math.Round(Decimal.Parse(Expression, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture), 4);
			Decimal number = Decimal.Parse(Expression, System.Globalization.NumberStyles.Currency);

			// Decimal.Parse with NumberStyles.Currency returns format string like "0.####" by dafault
			//return Decimal.Parse(number.ToString("0.####"));
			return number;
        }

        //FC:FINAL:RPU:#i1029: Make CLng function as in VB6 
        /// <summary>
        ///   Converts an expression to a Long
        /// </summary>
        /// <param name="Expression"></param>
        /// <returns></returns>
        public static long CLng(object expression)
        {
            if (expression is string)
            {
                string var = expression as string;
                var = var.Replace(",", "");
                if (var.Contains("."))
                {
                    return Convert.ToInt64(Convert.ToDouble(var));
                }
                return Convert.ToInt64(var);
            }
            return Convert.ToInt64(expression);
        }

        //FC:FINAL:CHN - issue #1383: Add CDbl function as in VB6.
        /// <summary>
        ///   Converts an expression to a Long
        /// </summary>
        /// <param name="Expression"></param>
        /// <returns></returns>
        public static double CDbl(object expression)
        {
            if (expression is string)
            {
                string var = expression as string;
                var = var.Replace(",", "");
                if (var.Contains("."))
                {
                    return Convert.ToDouble(var);
                }
                return Convert.ToDouble(var);
            }
            return Convert.ToDouble(expression);
        }
        /// <summary>
        /// Returns a Date value containing the date information represented by a string, with the time information set to midnight (00:00:00).
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime DateValue(object value)
        {
            if (value is DateTime)
            {
                return (DateTime)value;
            }
            else if (value is string)
            {
                return DateTime.Parse((string)value);
            }
            return DateTime.FromOADate(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime TimeValue(object value)
        {
            if (value is DateTime)
            {
                return (DateTime)value;
            }
            else if (value is string)
            {
                return DateTime.Parse((string)value);
            }
            return DateTime.FromOADate(0);
        }

        /// <summary>
        /// Returns a String representation of a number.
        /// </summary>
        /// <param name="value">An Object containing any valid numeric expression.</param>
        /// <returns>When numbers are converted to strings, a leading space is always reserved for the sign of Number. If Number is positive, the returned string contains a leading space, and the plus sign is implied. A negative number will include the minus sign (-) and no leading space.</returns>
        public static string Str(object value)
        {
            decimal val = Convert.ToDecimal(value);
            if (val > 0)
            {
                return " " + val.ToString();
            }
            else
            {
                return val.ToString();
            }

        }

        #region Used for Val function

        [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        private static extern int LCMapStringA(int Locale, int dwMapFlags, [MarshalAs(UnmanagedType.LPArray)] byte[] lpSrcStr, int cchSrc, [MarshalAs(UnmanagedType.LPArray)] byte[] lpDestStr, int cchDest);

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int LCMapString(int Locale, int dwMapFlags, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpSrcStr, int cchSrc, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDestStr, int cchDest);

        private static double ConversionHexOrOctValue(string InputStr, int i)
        {
            int num = 0;
            int length = InputStr.Length;
            char c = InputStr[i];
            long num3 = 0;
            checked
            {
                i++;
                if (c != 'H')
                {
                    if (c != 'h')
                    {
                        if (c != 'O')
                        {
                            if (c != 'o')
                            {
                                return 0.0;
                            }
                        }
                        while (i < length && num < 22)
                        {
                            c = InputStr[i];
                            i++;
                            char c2 = c;
                            if (c2 != '\t')
                            {
                                if (c2 != '\n')
                                {
                                    if (c2 != '\r')
                                    {
                                        if (c2 != ' ')
                                        {
                                            if (c2 != '\u3000')
                                            {
                                                int num2;
                                                if (c2 == '0')
                                                {
                                                    if (num == 0)
                                                    {
                                                        continue;
                                                    }
                                                    num2 = 0;
                                                }
                                                else
                                                {
                                                    if (c2 < '1' || c2 > '7')
                                                    {
                                                        break;
                                                    }
                                                    num2 = Convert.ToInt32(c - '0');
                                                }
                                                if (num3 >= 1152921504606846976L)
                                                {
                                                    num3 = (num3 & 1152921504606846975L) * 8L;
                                                    num3 |= 1152921504606846976L;
                                                }
                                                else
                                                {
                                                    num3 *= 8L;
                                                }
                                                num3 += unchecked((long)num2);
                                                num++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (num == 22)
                        {
                            i++;
                            if (i < length)
                            {
                                c = InputStr[i];
                            }
                        }
                        if (num3 <= 4294967296L)
                        {
                            if (num3 > 65535L || c == '&')
                            {
                                if (num3 > 2147483647L)
                                {
                                    num3 = -2147483648L + (num3 & 2147483647L);
                                }
                            }
                            else if ((num3 > 255L || c == '%') && num3 > 32767L)
                            {
                                num3 = -32768L + (num3 & 32767L);
                            }
                        }
                        unchecked
                        {
                            if (c == '%')
                            {
                                num3 = (long)(checked((short)num3));
                            }
                            else if (c == '&')
                            {
                                num3 = (long)(checked(Convert.ToInt32(num3)));
                            }
                            return (double)num3;
                        }
                    }
                }
                while (i < length && num < 17)
                {
                    c = InputStr[i];
                    i++;
                    char c3 = c;
                    if (c3 != '\t')
                    {
                        if (c3 != '\n')
                        {
                            if (c3 != '\r')
                            {
                                if (c3 != ' ')
                                {
                                    if (c3 != '\u3000')
                                    {
                                        int num2;
                                        if (c3 == '0')
                                        {
                                            if (num == 0)
                                            {
                                                continue;
                                            }
                                            num2 = 0;
                                        }
                                        else if (c3 >= '1' && c3 <= '9')
                                        {
                                            num2 = Convert.ToInt32(c - '0');
                                        }
                                        else if (c3 >= 'A' && c3 <= 'F')
                                        {
                                            num2 = Convert.ToInt32(c - '7');
                                        }
                                        else
                                        {
                                            if (c3 < 'a' || c3 > 'f')
                                            {
                                                break;
                                            }
                                            num2 = Convert.ToInt32(c - 'W');
                                        }
                                        if (num == 15 && num3 > 576460752303423487L)
                                        {
                                            num3 = (num3 & 576460752303423487L) * 16L;
                                            num3 |= -9223372036854775808L;
                                        }
                                        else
                                        {
                                            num3 *= 16L;
                                        }
                                        num3 += unchecked((long)num2);
                                        num++;
                                    }
                                }
                            }
                        }
                    }
                }
                if (num == 16)
                {
                    i++;
                    if (i < length)
                    {
                        c = InputStr[i];
                    }
                }
                if (num <= 8)
                {
                    if (num > 4 || c == '&')
                    {
                        if (num3 > 2147483647L)
                        {
                            num3 = -2147483648L + (num3 & 2147483647L);
                        }
                    }
                    else if ((num > 2 || c == '%') && num3 > 32767L)
                    {
                        num3 = -32768L + (num3 & 32767L);
                    }
                }
            }
            if (c == '%')
            {
                num3 = (long)(checked((short)num3));
            }
            else if (c == '&')
            {
                num3 = (long)(checked(Convert.ToInt32(num3)));
            }
            return (double)num3;
        }

        private static bool VersionedIsNumeric(object Expression)
        {
            IConvertible convertible = Expression as IConvertible;
            if (convertible == null)
            {
                return false;
            }
            switch (convertible.GetTypeCode())
            {
                case TypeCode.Boolean:
                    return true;
                case TypeCode.Char:
                case TypeCode.String:
                    {
                        string value = convertible.ToString(null);
                        try
                        {
                            long num = 0;
                            if (UtilsIsHexOrOctValue(value, ref num))
                            {
                                bool result = true;
                                return result;
                            }
                        }
                        catch (FormatException var_5_81)
                        {
                            bool result = false;
                            return result;
                        }
                        double num2 = 0;
                        return ConversionsTryParseDouble(value, ref num2);
                    }
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return true;
            }
            return false;
        }

        private static double ConversionsToDouble(object Value)
        {
            return ConversionsToDouble(Value, null);
        }

        private static double ConversionsToDouble(object Value, NumberFormatInfo NumberFormat)
        {
            if (Value == null)
            {
                return 0.0;
            }
            IConvertible convertible = Value as IConvertible;
            if (convertible != null)
            {
                switch (convertible.GetTypeCode())
                {
                    case TypeCode.Boolean:
                        //if (Value is bool)
                        //{
                        //    return -((bool)Value > false);
                        //}
                        //return -(convertible.ToBoolean(null) > false);
                        return Convert.ToDouble(Value);
                    case TypeCode.SByte:
                        if (Value is sbyte)
                        {
                            return (double)((sbyte)Value);
                        }
                        return (double)convertible.ToSByte(null);
                    case TypeCode.Byte:
                        if (Value is byte)
                        {
                            return (double)((byte)Value);
                        }
                        return (double)convertible.ToByte(null);
                    case TypeCode.Int16:
                        if (Value is short)
                        {
                            return (double)((short)Value);
                        }
                        return (double)convertible.ToInt16(null);
                    case TypeCode.UInt16:
                        if (Value is ushort)
                        {
                            return (double)((ushort)Value);
                        }
                        return (double)convertible.ToUInt16(null);
                    case TypeCode.Int32:
                        if (Value is int)
                        {
                            return (double)(Convert.ToInt32(Value));
                        }
                        return (double)convertible.ToInt32(null);
                    case TypeCode.UInt32:
                        if (Value is uint)
                        {
                            return (uint)Value;
                        }
                        return convertible.ToUInt32(null);
                    case TypeCode.Int64:
                        if (Value is long)
                        {
                            return (double)((long)Value);
                        }
                        return (double)convertible.ToInt64(null);
                    case TypeCode.UInt64:
                        if (Value is ulong)
                        {
                            return (ulong)Value;
                        }
                        return convertible.ToUInt64(null);
                    case TypeCode.Single:
                        if (Value is float)
                        {
                            return (double)((float)Value);
                        }
                        return (double)convertible.ToSingle(null);
                    case TypeCode.Double:
                        if (Value is double)
                        {
                            return (double)Value;
                        }
                        return convertible.ToDouble(null);
                    case TypeCode.Decimal:
                        if (Value is decimal)
                        {
                            return convertible.ToDouble(null);
                        }
                        return Convert.ToDouble(convertible.ToDecimal(null));
                    case TypeCode.String:
                        return ConversionsToDouble(convertible.ToString(null), NumberFormat);
                }
            }
            //    throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", new string[]
            //    {
            //Utils.VBFriendlyName(Value),
            //"Double"
            //    }));
            return 0;
        }

        private static string ConversionsToString(object Value)
        {
            if (Value == null)
            {
                return null;
            }
            string text = Value as string;
            if (text != null)
            {
                return text;
            }
            IConvertible convertible = Value as IConvertible;
            if (convertible != null)
            {
                switch (convertible.GetTypeCode())
                {
                    case TypeCode.Boolean:
                        return Convert.ToString(convertible.ToBoolean(null));
                    case TypeCode.Char:
                        return Convert.ToString(convertible.ToChar(null));
                    case TypeCode.SByte:
                        return Convert.ToString(Convert.ToInt32(convertible.ToSByte(null)));
                    case TypeCode.Byte:
                        return Convert.ToString(convertible.ToByte(null));
                    case TypeCode.Int16:
                        return Convert.ToString(Convert.ToInt32(convertible.ToInt16(null)));
                    case TypeCode.UInt16:
                        return Convert.ToString((uint)convertible.ToUInt16(null));
                    case TypeCode.Int32:
                        return Convert.ToString(convertible.ToInt32(null));
                    case TypeCode.UInt32:
                        return Convert.ToString(convertible.ToUInt32(null));
                    case TypeCode.Int64:
                        return Convert.ToString(convertible.ToInt64(null));
                    case TypeCode.UInt64:
                        return Convert.ToString(convertible.ToUInt64(null));
                    case TypeCode.Single:
                        return Convert.ToString(convertible.ToSingle(null));
                    case TypeCode.Double:
                        return Convert.ToString(convertible.ToDouble(null));
                    case TypeCode.Decimal:
                        return Convert.ToString(convertible.ToDecimal(null));
                    case TypeCode.DateTime:
                        return Convert.ToString(convertible.ToDateTime(null));
                    case TypeCode.String:
                        return convertible.ToString(null);
                }
            }
            else
            {
                char[] array = Value as char[];
                if (array != null)
                {
                    return new string(array);
                }
            }
            //    throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", new string[]
            //    {
            //Utils.VBFriendlyName(Value),
            //"String"
            //    }));
            return "";
        }

        private static bool ConversionsTryParseDouble(string Value, ref double Result)
        {
            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            NumberFormatInfo numberFormat = cultureInfo.NumberFormat;
            NumberFormatInfo normalizedNumberFormat = ConversionsGetNormalizedNumberFormat(numberFormat);
            Value = UtilsToHalfwidthNumbers(Value, cultureInfo);
            if (numberFormat == normalizedNumberFormat)
            {
                return double.TryParse(Value, NumberStyles.Any, normalizedNumberFormat, out Result);
            }
            bool result;
            try
            {
                Result = double.Parse(Value, NumberStyles.Any, normalizedNumberFormat);
                result = true;
            }
            catch (FormatException var_4_41)
            {
                try
                {
                    result = double.TryParse(Value, NumberStyles.Any, numberFormat, out Result);
                }
                catch (ArgumentException var_5_53)
                {
                    result = false;
                }
            }
            catch (StackOverflowException ex)
            {
                throw ex;
            }
            catch (OutOfMemoryException ex2)
            {
                throw ex2;
            }
            //catch (ThreadAbortException ex3)
            //{
            //    throw ex3;
            //}
            catch (Exception var_9_68)
            {
                result = false;
            }
            return result;
        }

        private static NumberFormatInfo ConversionsGetNormalizedNumberFormat(NumberFormatInfo InNumberFormat)
        {
            if (InNumberFormat.CurrencyDecimalSeparator != null && InNumberFormat.NumberDecimalSeparator != null && InNumberFormat.CurrencyGroupSeparator != null && InNumberFormat.NumberGroupSeparator != null && InNumberFormat.CurrencyDecimalSeparator.Length == 1 && InNumberFormat.NumberDecimalSeparator.Length == 1 && InNumberFormat.CurrencyGroupSeparator.Length == 1 && InNumberFormat.NumberGroupSeparator.Length == 1 && InNumberFormat.CurrencyDecimalSeparator[0] == InNumberFormat.NumberDecimalSeparator[0] && InNumberFormat.CurrencyGroupSeparator[0] == InNumberFormat.NumberGroupSeparator[0] && InNumberFormat.CurrencyDecimalDigits == InNumberFormat.NumberDecimalDigits)
            {
                return InNumberFormat;
            }
            checked
            {
                if (InNumberFormat.CurrencyDecimalSeparator != null && InNumberFormat.NumberDecimalSeparator != null && InNumberFormat.CurrencyDecimalSeparator.Length == InNumberFormat.NumberDecimalSeparator.Length && InNumberFormat.CurrencyGroupSeparator != null && InNumberFormat.NumberGroupSeparator != null && InNumberFormat.CurrencyGroupSeparator.Length == InNumberFormat.NumberGroupSeparator.Length)
                {
                    int arg_124_0 = 0;
                    int num = InNumberFormat.CurrencyDecimalSeparator.Length - 1;
                    for (int i = arg_124_0; i <= num; i++)
                    {
                        if (InNumberFormat.CurrencyDecimalSeparator[i] != InNumberFormat.NumberDecimalSeparator[i])
                        {
                            goto IL_18E;
                        }
                    }
                    int arg_15F_0 = 0;
                    int num2 = InNumberFormat.CurrencyGroupSeparator.Length - 1;
                    for (int i = arg_15F_0; i <= num2; i++)
                    {
                        if (InNumberFormat.CurrencyGroupSeparator[i] != InNumberFormat.NumberGroupSeparator[i])
                        {
                            goto IL_18E;
                        }
                    }
                    return InNumberFormat;
                }
                IL_18E:
                NumberFormatInfo numberFormatInfo = (NumberFormatInfo)InNumberFormat.Clone();
                NumberFormatInfo numberFormatInfo2 = numberFormatInfo;
                numberFormatInfo2.CurrencyDecimalSeparator = numberFormatInfo2.NumberDecimalSeparator;
                numberFormatInfo2.CurrencyGroupSeparator = numberFormatInfo2.NumberGroupSeparator;
                numberFormatInfo2.CurrencyDecimalDigits = numberFormatInfo2.NumberDecimalDigits;
                return numberFormatInfo;
            }
        }

        private static string UtilsToHalfwidthNumbers(string s, CultureInfo culture)
        {
            int lCID = culture.LCID;
            int num = lCID & 1023;
            if (num != 4 && num != 17 && num != 18)
            {
                return s;
            }
            return StringsvbLCMapString(culture, 4194304, s);
        }

        private static string StringsvbLCMapString(CultureInfo loc, int dwMapFlags, string sSrc)
        {
            int num;
            if (sSrc == null)
            {
                num = 0;
            }
            else
            {
                num = sSrc.Length;
            }
            if (num == 0)
            {
                return "";
            }
            int lCID = loc.LCID;
            Encoding encoding = Encoding.GetEncoding(loc.TextInfo.ANSICodePage);
            int num2;
            string result;
            if (!encoding.IsSingleByte)
            {
                string s = sSrc;
                byte[] bytes = encoding.GetBytes(s);
                num2 = LCMapStringA(lCID, dwMapFlags, bytes, bytes.Length, null, 0);
                byte[] array = new byte[checked(num2 - 1 + 1)];
                num2 = LCMapStringA(lCID, dwMapFlags, bytes, bytes.Length, array, num2);
                result = encoding.GetString(array);
                return result;
            }
            result = new string(' ', num);
            num2 = LCMapString(lCID, dwMapFlags, ref sSrc, num, ref result, num);
            return result;
        }

        private static bool UtilsIsHexOrOctValue(string Value, ref long i64Value)
        {
            int length = Value.Length;
            checked
            {
                int i = 0;
                while (i < length)
                {
                    char c = Value[i];
                    if (c == '&' && i + 2 < length)
                    {
                        c = char.ToLower(Value[i + 1], CultureInfo.InvariantCulture);
                        string value = UtilsToHalfwidthNumbers(Value.Substring(i + 2), System.Threading.Thread.CurrentThread.CurrentCulture);
                        if (c == 'h')
                        {
                            i64Value = Convert.ToInt64(value, 16);
                        }
                        else
                        {
                            if (c != 'o')
                            {
                                throw new FormatException();
                            }
                            i64Value = Convert.ToInt64(value, 8);
                        }
                        return true;
                    }
                    if (c != ' ' && c != '\u3000')
                    {
                        return false;
                    }
                    i++;
                }
                return false;
            }
        }

        #endregion
    }

    public static class StringType
    {
        public static void MidStmtStr(ref string dest, int startPosition, int maxInsertLength, string insert)
        {
            int length = -1;
            if (dest != null)
            {
                length = dest.Length;
            }
            int num = -1;
            if (insert != null)
            {
                num = insert.Length;
            }
            checked
            {
                startPosition--;
                if (startPosition < 0 || startPosition >= length)
                {
                    throw new Exception("Start");
                }
                if (maxInsertLength < 0)
                {
                    throw new Exception("length");
                }
                if (num > maxInsertLength)
                {
                    num = maxInsertLength;
                }
                if (num > length - startPosition)
                {
                    num = length - startPosition;
                }
                if (num == 0)
                {
                    return;
                }
                StringBuilder stringBuilder = new StringBuilder(length);
                if (startPosition > 0)
                {
                    stringBuilder.Append(dest, 0, startPosition);
                }
                stringBuilder.Append(insert, 0, num);
                int num2 = length - (startPosition + num);
                if (num2 > 0)
                {
                    stringBuilder.Append(dest, startPosition + num, num2);
                }
                dest = stringBuilder.ToString();
            }
        }
    }

    public static class Interaction
    {
        /// <summary>
        /// Displays a prompt in a dialog box, waits for the user to input text or click a button, and then returns a string containing the contents of the text box.
        /// </summary>
        /// <param name="prompt">String expression displayed as the message in the dialog box. The maximum length of Prompt is approximately 1024 characters, depending on the width of the characters used. If Prompt consists of more than one line, you can separate the lines using a carriage return character (Chr(13)), a line feed character (Chr(10)), or a carriage return/line feed combination (Chr(13) & Chr(10)) between each line.</param>
        /// <param name="title">String expression displayed in the title bar of the dialog box. If you omit Title, the application name is placed in the title bar.</param>
        /// <param name="defaultValue">String expression displayed in the text box as the default response if no other input is provided. If you omit DefaultResponse, the displayed text box is empty.</param>
        /// <returns></returns>
        public static string InputBox(string prompt, string title, string defaultValue = "")
        {
            InputBoxDialog ib = new InputBoxDialog();
            ib.FormPrompt = prompt;
            ib.FormCaption = title;
            ib.DefaultValue = defaultValue;
            ib.ShowDialog();
            string s = ib.InputResponse;
            ib.Close();
            return s;
        }

        /// <summary>
        /// Returns the argument portion of the command line used to start Visual Basic or an executable program developed with Visual Basic.
        /// </summary>
        /// <returns></returns>
        public static string Command()
        {
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                return Environment.GetCommandLineArgs()[1];
            }
           
            return "";
        }

        /// <summary>
        /// It was originally called as:
        /// Interaction.Shell(string.Format("explorer.exe {0}", basPublics.PUB_PfadExport), AppWinStyle.NormalFocus)
        /// The changes: AppWinStyle -> ProcesswindowStyle
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="windowStyle"></param>
        /// <returns></returns>
        public static int Shell(string cmd, ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal, bool param1 = false, int param2 = 0)
        {
            cmd = cmd.ToLower();
            string[] args = cmd.Split(new string[] { ".exe " }, StringSplitOptions.None);

            string path = args[0];

            if (!path.EndsWith(".exe"))
            {
                path += ".exe";
            }
            
            string arguments = "";
            if (args.Length > 1)
            {
                arguments = args[1];
            }

            ProcessStartInfo pInfo = new ProcessStartInfo();
            pInfo.Arguments = arguments;
            pInfo.FileName = path;
            pInfo.WindowStyle = windowStyle;

            Process p = Process.Start(pInfo);
            //p.WaitForExit();
            return p.Id;
        }

        /// <summary>
        /// Returns the string associated with an operating-system environment variable. 
        /// </summary>
        /// <param name="str">Expression that evaluates either a string containing the name of an environment variable, or an integer corresponding to the numeric order of an environment string in the environment-string table.</param>
        /// <returns></returns>
        public static string Environ(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            return Environment.GetEnvironmentVariable(str);
        }

        /// <summary>
        /// Activates an application that is already running.
        /// </summary>
        /// <param name="process">String expression specifying the title in the title bar of the application you want to activate. You can use the title assigned to the application when it was launched.</param>
        /// <param name="wait"></param>
        public static void AppActivate(string process, bool wait = false)
        {
            //TODO @@TB please implement wait param
            Process[] prc = Process.GetProcessesByName(process);
            if (prc.Length > 0)
            {
                Externals.USER32.SetForegroundWindow(prc[0].MainWindowHandle);
            }
        }

        /// <summary>
        /// Activates an application that is already running.
        /// </summary>
        /// <param name="process">Integer specifying the Win32 process ID number assigned to this process. You can use the ID returned by the Shell Function, provided it is not zero.</param>
        /// <param name="wait"></param>
        public static void AppActivate(int process, bool wait = false)
        {
            //TODO @@TB please implement wait param
            Process prc = Process.GetProcessById(process);
            if (prc != null)
            {
                Externals.USER32.SetForegroundWindow(prc.MainWindowHandle);
            }
        }

        /// <summary>
        /// Selects and returns a value from a list of arguments.
        /// </summary>
        /// <param name="index">Numeric expression that results in a value between 1 and the number of elements passed in the Choice argument.</param>
        /// <param name="choice">Object parameter array. You can supply either a single variable or an expression that evaluates to the Object data type, to a list of Object variables or expressions separated by commas, or to a single-dimensional array of Object elements.</param>
        /// <returns></returns>
        public static T Choose<T>(int index, params T[] choice)
        {
            return choice[index - 1];
        }

        /// <summary>
        /// Sounds a tone through the computer's speaker.
        /// </summary>
        public static void Beep()
        {
            Console.Beep();
        }

        /// <summary>
        /// Creates and returns a reference to a COM object. CreateObject cannot be used to create instances of classes in Visual Basic unless those classes are explicitly exposed as COM components.
        /// </summary>
        /// <param name="name">String. The program ID of the object to create.</param>
        /// <returns></returns>
        public static object CreateObject(string name)
        {
            return Activator.CreateInstance(Type.GetTypeFromProgID(name));
        }

        /// <summary>
        /// Returns a key setting value from an application's entry in the Windows registry.
        /// </summary>
        /// <param name="name">String expression containing the name of the application or project whose key setting is requested.</param>
        /// <param name="key">String expression containing the name of the key setting to return.</param>
        /// <param name="value">String expression containing the name of the section in which the key setting is found.</param>
        /// <param name="defaultValue">Expression containing the value to return if no value is set in the Key setting. If omitted, Default is assumed to be a zero-length string ("").</param>
        /// <returns></returns>
        public static string GetSetting(string name, string key, string value, string defaultValue = "")
        {
            string currentKey = "";
            using (RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\VB and VBA Program Settings\" + name + "\\" + key, true))
            {
                if (reg != null)
                {
                    currentKey = reg.GetValue(value, true).ToString();
                    reg.Close();
                    reg.Dispose();
                }
            }

            if (string.IsNullOrEmpty(currentKey))
            {
                return defaultValue;
            }

            return currentKey;
        }

        public static string[] GetSubKeys(string name, string key)
        {
            string[] subKeys = new string[0];
            using (RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\VB and VBA Program Settings\" + name + "\\" + key, true))
            {
                if (reg != null)
                {
                    subKeys = reg.GetValueNames();
                }
            }
            return subKeys;
        }

        public static bool CreateSetting(string name, string key)
        {
            using (RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\VB and VBA Program Settings\"))
            {
                if (reg != null)
                {
                    RegistryKey subKey = Registry.CurrentUser.CreateSubKey(@"Software\VB and VBA Program Settings\" + name + "\\" + key);
                    if (subKey != null)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Saves or creates an application entry in the Windows registry.
        /// </summary>
        /// <param name="name">String expression containing the name of the application or project to which the setting applies.</param>
        /// <param name="key">String expression containing the name of the key setting being saved.</param>
        /// <param name="value">String expression containing the name of the section in which the key setting is being saved.</param>
        /// <param name="newVal">Expression containing the value to which Key is being set.</param>
        public static void SaveSettingWithCreate(string name, string key, string value, string newVal)
        {
            using (RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\VB and VBA Program Settings\" + name + "\\" + key, true))
            {
                if (reg == null)
                {
                    if (!CreateSetting(name, key))
                    {
                        return;
                    }
                }
            }
            SaveSetting(name, key, value, newVal);
        }

        /// <summary>
        /// Saves or creates an application entry in the Windows registry.
        /// </summary>
        /// <param name="name">String expression containing the name of the application or project to which the setting applies.</param>
        /// <param name="key">String expression containing the name of the key setting being saved.</param>
        /// <param name="value">String expression containing the name of the section in which the key setting is being saved.</param>
        /// <param name="newVal">Expression containing the value to which Key is being set.</param>
        public static void SaveSetting(string name, string key, string value, string newVal)
        {
            using (RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\VB and VBA Program Settings\" + name + "\\" + key, true))
            {
                if (reg != null)
                {
                    reg.SetValue(value, newVal);
                    reg.Close();
                    reg.Dispose();
                }
            }
        }

        public static void DeleteKey(string key)
        {
            using (RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\VB and VBA Program Settings\", true))
            {
                if (reg != null)
                {
                    reg.DeleteSubKeyTree(key);
                }
            }
        }

        /// <summary>
        /// Deletes a section or key setting from an application's entry in the Windows registry.
        /// </summary>
        /// <param name="name">String expression containing the name of the application or project to which the section or key setting applies.</param>
        /// <param name="key">String expression containing the name of the key setting being deleted.</param>
        /// <param name="value">String expression containing the name of the section from which the key setting is being deleted. If only AppName and Section are provided, the specified section is deleted along with all related key settings.</param>
        public static void DeleteSetting(string name, string key, string value)
        {
            using (RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\VB and VBA Program Settings\" + name + "\\" + key, true))
            {
                if (reg != null)
                {
                    if (reg.GetSubKeyNames().Contains(value))
                    {
                        reg.DeleteSubKey(value);
                        reg.Close();
                        reg.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Deletes a section or key setting from an application's entry in the Windows registry.
        /// </summary>
        /// <param name="name">String expression containing the name of the application or project to which the section or key setting applies.</param>
        /// <param name="key">String expression containing the name of the key setting being deleted.</param>
        /// <param name="value">String expression containing the name of the section from which the key setting is being deleted. If only AppName and Section are provided, the specified section is deleted along with all related key settings.</param>
        public static void DeleteValue(string name, string key, string value)
        {
            using (RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\VB and VBA Program Settings\" + name + "\\" + key, true))
            {
                if (reg != null)
                {
                    if (reg.GetValueNames().Contains(value))
                    {
                        reg.DeleteValue(value);
                        reg.Close();
                        reg.Dispose();
                    }
                }
            }
        }
    }

    public static class Support
    {
        /// <summary>
        /// Sends one or more keystrokes to the active window, as if typed on the keyboard.
        /// </summary>
        /// <param name="key">A String that defines the keys to send.</param>
        /// <param name="wait">A Boolean that specifies whether or not to wait for keystrokes to get processed before the application continues. True by default.</param>
        public static void SendKeys(string key, bool wait = false, Form executeKeyForm = null)
        {
            if (key.ToUpper() == "{TAB}")
            {   
                Form activeForm = ActiveMdiChildOrForm;
                activeForm?.SelectNextControl(activeForm.ActiveControl, true, true, true, true);
            }
            else if (key.ToUpper() == "+{TAB}")
            {
                Form activeForm = ActiveMdiChildOrForm;
                activeForm?.SelectNextControl(activeForm.ActiveControl, false, true, true, true);
            }
            else if (key.StartsWith("{F") && key.Length > 3)
            {
                string shortcutString = key.Replace("{", "").Replace("}", "");
                Shortcut shortcutKey = Shortcut.None;
                if (Enum.TryParse<Shortcut>(shortcutString, out shortcutKey))
                {
                    if (executeKeyForm == null)
                    {
                        executeKeyForm = ActiveMdiChildOrForm;
                    }
                    if (executeKeyForm != null)
                    {
                        //check if any button has the shortcut
                        bool shortcutFound = false;
                        foreach (var control in executeKeyForm.GetAllControls())
                        {
                            //FC:FINAL:SBE - Harris #3956 - add option to not execute the validation on ActiveControl. In VB6 when SendKeys(button.Shortcut) is executed, the validation on ActiveControl is not triggered
                            if (control is FCButton)
                            {
                                FCButton fcButton = control as FCButton;
                                if (fcButton.Shortcut == shortcutKey)
                                {
                                    if (fcButton.Visible && fcButton.Enabled)
                                    {
                                        fcButton.ExecuteClick(false);
                                    }
                                    shortcutFound = true;
                                    break;
                                }
                            }
                            else if (control is Button)
                            {
                                Button button = control as Button;
                                if (button.Shortcut == shortcutKey)
                                {
                                    if (button.Visible && button.Enabled)
                                    {
                                        button.PerformClick();
                                    }
                                    shortcutFound = true;
                                    break;
                                }
                            }
                        }
                        //check if any menu has the shortcut
                        if (!shortcutFound && executeKeyForm.Menu != null)
                        {
                            MenuItem menuItemWithShortCutKey = GetMeuItemWithShortCut(executeKeyForm.Menu.MenuItems, shortcutKey);
                            if (menuItemWithShortCutKey != null && menuItemWithShortCutKey.Enabled && menuItemWithShortCutKey.Visible)
                            {
                                menuItemWithShortCutKey.PerformClick();
                            }
                        }
                    }
                }
            }
        }

        private static MenuItem GetMeuItemWithShortCut(Menu.MenuItemCollection menuItemCollection, Shortcut shortcutKey)
        {
            MenuItem menuItemWithShortCutKey = null;
            foreach (MenuItem menuItem in menuItemCollection)
            {
                if (menuItem.Shortcut == shortcutKey)
                {
                    menuItemWithShortCutKey = menuItem;
                    break;
                }
                else
                {
                    menuItemWithShortCutKey = GetMeuItemWithShortCut(menuItem.MenuItems, shortcutKey);
                }
            }
            return menuItemWithShortCutKey;
        }

        private static Form ActiveMdiChildOrForm
        {
            get
            {
                Form activeForm = Form.ActiveForm;
                if (activeForm != null)
                {
                    //ActiveForm might be the MDI; in this case ActiveControl is the active MDI child
                    if (activeForm.ActiveControl is Form)
                    {
                        activeForm = activeForm.ActiveControl as Form;
                    }
                }
                return activeForm;
            }
        }

        //JEI:HARRIS: used for SendKeys
        public static void InvokeKeyPress(Control c, KeyPressEventArgs e)
        {
            MethodInfo dynMethod = c.GetType().GetMethod("OnKeyDown", BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(KeyEventArgs) }, null);
            KeyEventArgs args = new KeyEventArgs((Keys)Convert.ToInt32(e.KeyChar));
            dynMethod?.Invoke(c, new object[] { args });
            if (c.HasChildren)
            {
                foreach (Control ctrl in c.Controls)
                {
                    if (ctrl.Visible && ctrl.GetType() != typeof(TextBox))
                    {
                        InvokeKeyPress(ctrl, e);
                    }
                }
            }

        }
    }

    //public static class DateAndTime
    //{
    //    /// <summary>
    //    /// Returns a Variant (Long) specifying the number of time intervals between two specified dates.
    //    /// </summary>
    //    /// <param name="interval">Required. String expression that is the interval of time you use to calculate the difference between date1 and date2.</param>
    //    /// <param name="date1"></param>
    //    /// <param name="date2"></param>
    //    /// <param name="firstdayofweek"></param>
    //    /// <param name="firstweekofyear"></param>
    //    /// <returns></returns>
    //    public static long DateDiff(string interval, DateTime date1, DateTime date2, int firstdayofweek = Constants.vbSunday, int firstweekofyear = Constants.vbFirstJan1)
    //    {
    //        TimeSpan ts = date2 - date1;
    //        double result = 0;

    //        switch (interval)
    //        {
    //            case "s":
    //                result = ts.TotalSeconds; break;
    //            case "n":
    //                result = ts.TotalMinutes; break;
    //            case "h":
    //                result = ts.TotalHours; break;
    //            case "d":
    //                result = ts.TotalDays; break;
    //            case "m":
    //                result = (date2.Month - date1.Month) + (12 * (date2.Year - date1.Year)); break;
    //            case "yyyy":
    //                result = date2.Year - date1.Year; break;
    //            default:
    //                result = ts.TotalDays; break;
    //        }

    //        return (long)result;
    //    }

    //    private static long Fix(double Number)
    //    {
    //        if (Number >= 0)
    //        {
    //            return (long)Math.Floor(Number);
    //        }
    //        return (long)Math.Ceiling(Number);
    //    }
    //}

    public static class DateAndTime
    {
        /// <summary>
        /// Gets current date.
        /// </summary>
        public static DateTime Today
        {
            get
            {
                return DateTime.Today;
            }
        }

        /// <summary>
        /// Gets a System.DateTime object that is set to the current date and time on this
        /// computer, expressed as the local time.
        /// </summary>
        public static DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Gets the time of day for this instance.
        /// </summary>
        public static DateTime TimeOfDay
        {
            get
            {
                return new DateTime(1899, 12, 30, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            }
        }

        /// <summary>
        /// Returns a Single representing the number of seconds elapsed since midnight.
        /// </summary>
        /// <returns></returns>
        public static double Timer()
        {
            return DateTime.Now.TimeOfDay.TotalSeconds;
        }

        /// <summary>
        /// Returns an Integer value containing a number representing the day of the week.
        /// </summary>
        /// <param name="value">Required. Date value for which you want to determine the day of the week.</param>
        /// <param name="day">Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week. If not specified, FirstDayOfWeek.Sunday is used.</param>
        /// <returns></returns>
        public static int Weekday(DateTime value, DayOfWeek day = DayOfWeek.Monday)
        {
            return (Convert.ToInt32(value.DayOfWeek) + 1);
        }

        /// <summary>
        /// Returns a String value containing the name of the specified weekday.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string WeekdayName(int value)
        {
            DayOfWeek day = (DayOfWeek)value;
            return day.ToString();
        }

        /// <summary>
        /// Returns the year in the specified DateTime.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Year(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Year;
        }

        /// <summary>
        /// Returns the Month in the specified DateTime
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Month(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Month;
        }

        /// <summary>
        /// Returns the Day in the specified DateTime
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Day(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Day;
        }

        /// <summary>
        /// Gets the hours component of the time interval represented by the current TimeSpan value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Hour(TimeSpan value)
        {
            if (value == TimeSpan.MinValue)
            {
                return 0;
            }

            DateTime date = new DateTime(1, 1, 1, 0, 0, 0);
            date = date + value;

            return date.Hour;
        }

        /// <summary>
        /// Gets the hours component of the time interval represented by the current DateTime value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Hour(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Hour;
        }

        /// <summary>
        /// Gets the Minutes component of the time interval represented by the current TimeSpan value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Minute(TimeSpan value)
        {
            if (value == TimeSpan.MinValue)
            {
                return 0;
            }

            DateTime date = new DateTime(1, 1, 1, 0, 0, 0);
            date = date + value;

            return date.Minute;
        }

        /// <summary>
        /// Gets the Minutes component of the time interval represented by the current DateTime value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Minute(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Minute;
        }

        /// <summary>
        /// Gets the Seconds component of the time interval represented by the current TimeSpan value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Second(TimeSpan value)
        {
            if (value == TimeSpan.MinValue)
            {
                return 0;
            }

            DateTime date = new DateTime(1, 1, 1, 0, 0, 0);
            date = date + value;

            return date.Second;
        }

        /// <summary>
        /// Gets the Seconds component of the time interval represented by the current DateTime value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Second(DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                return 0;
            }

            return value.Second;
        }

        /// <summary>
        /// Returns a Date value containing the date information represented by a string, with the time information set to midnight (00:00:00).
        /// </summary>
        /// <param name="val">Required. String expression representing a date/time value from 00:00:00 on January 1 of the year 1 through 23:59:59 on December 31, 9999.</param>
        /// <returns></returns>
        public static DateTime DateValue(string val)
        {
            return FCConvert.ToDateTime(val);
        }

		public static DateTime TimeValue(DateTime val)
		{
			return DateAndTime.TimeValue(val.ToString("HH:mm:ss"));
		}

        /// <summary>
        /// Returns a Date value containing the time information represented by a string, with the date information set to January 1 of the year 1.
        /// </summary>
        /// <param name="val">Required. String expression representing a date/time value from 00:00:00 on January 1 of the year 1 through 23:59:59 on December 31, 9999.</param>
        /// <returns></returns>
        public static DateTime TimeValue(string val)
        {
			//FC:FINAL:DSE TimeValue should include Date(0) and not Today as date value
			TimeSpan timeSpan = DateTime.ParseExact(val, "HH:mm:ss", CultureInfo.InvariantCulture) - DateTime.Today;
			return DateTime.FromOADate(0) + timeSpan;
        }

        /// <summary>
        /// Returns a Date value representing a specified year, month, and day, with the time information set to midnight (00:00:00).
        /// </summary>
        /// <param name="year">Required. Integer expression from 1 through 9999. However, values below this range are also accepted. If Year is 0 through 99, it is interpreted as being between 1930 and 2029, as explained in the "Remarks" section below. If Year is less than 1, it is subtracted from the current year.</param>
        /// <param name="month">Required. Integer expression from 1 through 12. However, values outside this range are also accepted. The value of Month is offset by 1 and applied to January of the calculated year. In other words, (Month - 1) is added to January. The year is recalculated if necessary. The following results illustrate this effect:
        /// If Month is 1, the result is January of the calculated year.
        /// If Month is 0, the result is December of the previous year.
        /// If Month is -1, the result is November of the previous year.
        /// If Month is 13, the result is January of the following year.</param>
        /// <param name="day">Required. Integer expression from 1 through 31. However, values outside this range are also accepted. The value of Day is offset by 1 and applied to the first day of the calculated month. In other words, (Day - 1) is added to the first of the month. The month and year are recalculated if necessary. The following results illustrate this effect:
        /// If Day is 1, the result is the first day of the calculated month.
        /// If Day is 0, the result is the last day of the previous month.
        /// If Day is -1, the result is the penultimate day of the previous month.
        /// If Day is past the end of the current month, the result is the appropriate day of the following month. For example, if Month is 4 and Day is 31, the result is May 1.
        /// </param>
        /// <returns></returns>
        public static DateTime DateSerial(int year, int month, int day)
        {
            // ?? test If month == 0 too, same functionality like day
            if (day == 0)
            {
                return new DateTime(year, month, day + 1).AddDays(-1);
            }

            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Returns a Date value representing a specified hour, minute, and second, with the date information set relative to January 1 of the year 1.
        /// </summary>
        /// <param name="hour">Required. Integer expression from 0 through 23. However, values outside this range are also accepted.</param>
        /// <param name="minute">Required. Integer expression from 0 through 59. However, values outside this range are also accepted. The value of Minute is added to the calculated hour, so a negative value specifies minutes before that hour.</param>
        /// <param name="second">Required. Integer expression from 0 through 59. However, values outside this range are also accepted. The value of Second is added to the calculated minute, so a negative value specifies seconds before that minute.</param>
        /// <returns></returns>
        public static DateTime TimeSerial(int hour, int minute, int second)
        {
            // negative values not supported
            second = Math.Abs(second);
            minute = Math.Abs(minute);
            hour = Math.Abs(hour);
            //trunk seconds to (1 day - 1 sec)
            second = second % 86400;
            //convert seconds to minutes
            minute += (second / 60);
            second = second % 60;
            //convert minutes to hours
            hour += Convert.ToInt32(minute / 60);
            minute = minute % 60;

            return new DateTime(1, 1, 1, hour, minute, second);
        }

        /// <summary>
        /// Returns an Int value specifying the number of time intervals between two Date values.
        /// </summary>
        /// <param name="interval">Required. DateInterval enumeration value or String expression representing the time interval you want to use as the unit of difference between Date1 and Date2.</param>
        /// <param name="start">Required. Object. The first date/time value you want to use in the calculation.</param>
        /// <param name="end">Required. Object. The second date/time value you want to use in the calculation.</param>
        /// <returns></returns>
        public static int DateDiff(string interval, object start, object end)
        {
            DateTime dateStart;
            DateTime dateEnd;

            if (start == null || end == null)
            {
                return 0;
            }

            if (DateTime.TryParse(start.ToString(), out dateStart) && DateTime.TryParse(end.ToString(), out dateEnd))
            {
                return DateDiff(interval, dateStart, dateEnd);
            }
            return 0;
        }

        /// <summary>
        /// Returns an Int value specifying the number of time intervals between two Date values.
        /// </summary>
        /// <param name="interval">Required. DateInterval enumeration value or String expression representing the time interval you want to use as the unit of difference between Date1 and Date2.</param>
        /// <param name="start">Required. Object. The first date/time value you want to use in the calculation.</param>
        /// <param name="end">Required. DateTime. The second date/time value you want to use in the calculation.</param>
        /// <returns></returns>
        public static int DateDiff(string interval, object start, DateTime end)
        {
            DateTime date;

            if (start == null)
            {
                return 0;
            }

            if (DateTime.TryParse(start.ToString(), out date))
            {
                return DateDiff(interval, date, end);
            }

            return 0;
        }

        /// <summary>
        /// Returns an Int value specifying the number of time intervals between two Date values.
        /// </summary>
        /// <param name="interval">Required. DateInterval enumeration value or String expression representing the time interval you want to use as the unit of difference between Date1 and Date2.</param>
        /// <param name="start">Required. DateTime. The first date/time value you want to use in the calculation.</param>
        /// <param name="end">Required. DateTime. The second date/time value you want to use in the calculation.</param>
        /// <returns></returns>
        public static int DateDiff(string interval, DateTime start, DateTime end)
        {
            TimeSpan diff = end - start;
            // JSP/Fecher correction: consider interval too
            // possible values for interval
            // "d" = Tag
            // "y" = Tag innerhalb des Jahrs
            // "h" = Stunde
            // "n" = Minute
            // "m" = Monat
            // "q" = Quartal
            // "s" = Sekunde
            // "w" = Woche
            // "ww" = Kalenderwoche
            // "yyyy" = Jahr

            switch (interval.ToLower())
            {
                case "d":
                case "y":
                    //return Convert.ToInt32(diff.TotalDays);
                    int differenceDays = diff.Days;
                    if (differenceDays == 0)
                    {
                        if (end.Year == start.Year)
                        {
                            differenceDays = end.DayOfYear - start.DayOfYear;
                        }
                        else
                        {
                            differenceDays = end.Year - start.Year;
                        }
                    }
                    return differenceDays;

                case "h":
                    return Convert.ToInt32(diff.TotalHours);

                case "n":
                    return Convert.ToInt32(diff.TotalMinutes);

                case "m":
                    // Month ==> days / (365/12)
                    //FC:FINAL:MSH - i.issue #1344: change getting total month number, because division have problem with rounding
                    //return Convert.ToInt32(diff.TotalDays / 30.42);
                    return ((end.Year - start.Year) * 12) + end.Month - start.Month;

                case "q":
                    // Quarter ==> days / (365/4)
                    return Convert.ToInt32(diff.TotalDays / 91.25);

                case "s":
                    return Convert.ToInt32(diff.TotalSeconds);

                case "w":
                    return Convert.ToInt32(diff.Days / 7);

                case "ww":
                    // perhaps wrong, but I didn't find any explication what the difference in  "calenderweeks" means
                    // or where is the difference to "Difference in weeks"
                    return Convert.ToInt32(diff.Days / 7);

                case "yyyy":
                    return Convert.ToInt32(diff.Days / 365);

                default:
                    return Convert.ToInt32(diff.TotalSeconds);
            }
        }

        /// <summary>
        /// Returns a Date value containing a date and time value to which a specified time interval has been added.
        /// </summary>
        /// <param name="interval">Required. String expression representing the time interval you want to add.</param>
        /// <param name="number">Required. Integer. Number can be positive (to get date/time values in the future) or negative (to get date/time values in the past).</param>
        /// <param name="date">Required. String. An expression representing the date and time to which the interval is to be added.</param>
        /// <returns></returns>
        public static DateTime DateAdd(string interval, double number, string date)
        {
            DateTime newDate;

            if (DateTime.TryParse(date, out newDate))
            {
                newDate = DateAdd(interval, number, newDate);
            }

            return newDate;
        }

        /// <summary>
        /// Returns a Date value containing a date and time value to which a specified time interval has been added.
        /// </summary>
        /// <param name="interval">Required. String expression representing the time interval you want to add.</param>
        /// <param name="number">Required. Integer. Number can be positive (to get date/time values in the future) or negative (to get date/time values in the past).</param>
        /// <param name="date">Required. Object. An expression representing the date and time to which the interval is to be added.</param>
        /// <returns></returns>
        public static DateTime DateAdd(string interval, double number, object date)
        {
            DateTime newDate = DateTime.FromOADate(0);

            if (date != null && DateTime.TryParse(date.ToString(), out newDate))
            {
                newDate = DateAdd(interval, number, newDate);
            }

            return newDate;
        }

        /// <summary>
        /// Returns a Date value containing a date and time value to which a specified time interval has been added.
        /// </summary>
        /// <param name="interval">Required. String expression representing the time interval you want to add.</param>
        /// <param name="number">Required. Integer. Number can be positive (to get date/time values in the future) or negative (to get date/time values in the past).</param>
        /// <param name="date">Required. DateTime. An expression representing the date and time to which the interval is to be added.</param>
        /// <returns></returns>
        public static DateTime DateAdd(string interval, double number, DateTime date)
        {
            if (date == DateTime.MinValue)
            {
                return date;
            }

            switch (interval.ToLower())
            {
                case "d":
                    return date.AddDays(number);

                case "s":
                    return date.AddSeconds(number);

                case "n":
                    return date.AddMinutes(number);

                case "h":
                    return date.AddHours(number);

                case "q":
                    return date.AddMonths(Convert.ToInt32(3 * number));

                case "m":
                    return date.AddMonths(Convert.ToInt32(number));

                case "ww":
                    return date.AddDays(7 * number);

                case "yyyy":
                    return date.AddYears(Convert.ToInt32(number));
            }

            return date;
        }
    }

}


