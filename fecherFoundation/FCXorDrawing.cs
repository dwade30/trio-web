﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using static fecherFoundation.Externals;
using static fecherFoundation.Externals.GDI32;

namespace fecherFoundation
{
    /// <summary>
    /// Helper class for support of xor drawing because GDI+ dosn't support xor drawing at all
    /// Code from http://stackoverflow.com/questions/9034084/how-can-i-use-xor-mode-when-calling-drawrectangle-method-in-c-sharp
    /// </summary>
    public static class FCXorDrawing
    {
        private static IntPtr BeginDraw(System.Drawing.Bitmap bmp, System.Drawing.Graphics graphics, int x1, int y1, int x2, int y2, bool dash, int width, out int oldRop, out IntPtr img, out IntPtr oldpen)
        {
            var gHdc = graphics.GetHdc();
            var hdc = CreateCompatibleDC(gHdc);
            graphics.ReleaseHdc(hdc);

            img = bmp.GetHbitmap();
            SelectObject(hdc, img);

            oldpen = IntPtr.Zero;
            if (dash)
            {
                var pen = CreatePen(PenStyle.PS_DASH, 1, 0);
                oldpen = SelectObject(hdc, pen);
            }

            if(width > 1 && !dash)
            {
                var pen = CreatePen(PenStyle.PS_SOLID, width, 0);
                oldpen = SelectObject(hdc, pen);
            }


            oldRop = SetROP2(hdc, (int)BinaryRasterOperations.R2_NOTXORPEN); // Switch to inverted mode. (XOR)

            SetGraphicsMode(hdc, (int)GraphicsMode.GM_ADVANCED);
            XFORM transform = graphics.Transform;
            SetWorldTransform(hdc, ref transform);

            return hdc;
        }

        private static void FinishDraw(System.Drawing.Bitmap bmp, System.Drawing.Graphics graphics, IntPtr hdc, IntPtr oldpen, int oldRop, IntPtr img, bool dash)
        {
            SetROP2(hdc, oldRop);

            var transform = graphics.Transform;
            graphics.ResetTransform(); //in case there is transform
            var outBmp = System.Drawing.Image.FromHbitmap(img);
            //CopyChannel(bmp, outBmp, ChannelARGB.Alpha, ChannelARGB.Alpha);
            graphics.Clear(Color.Transparent);
            graphics.DrawImage(outBmp, 0, 0); //draw the xored image on the bitmap
            graphics.Transform = transform;

            if (dash) DeleteObject(SelectObject(hdc, oldpen)); //delete new pen (switch to oldpen)
            DeleteObject(img); // Delete the GDI bitmap (important).
            outBmp.Dispose(); //Important: Dispose the Image
            outBmp = null;
            DeleteObject(hdc);
        }

        public static void DrawXorLine(this System.Drawing.Graphics graphics, System.Drawing.Bitmap bmp, PointF p1, PointF p2 , bool dash = false, int width = 1)
        {
            DrawXorLine(graphics, bmp, Convert.ToInt32(p1.X), Convert.ToInt32(p1.Y), Convert.ToInt32(p2.X), Convert.ToInt32(p2.Y), dash, width);
        }

        public static void DrawXorLine(this System.Drawing.Graphics graphics, System.Drawing.Bitmap bmp, int x1, int y1, int x2, int y2, bool dash = false, int width = 1)
        {
            int oldRop;
            IntPtr oldpen, img;
            var hdc = BeginDraw(bmp, graphics, x1, y1, x2, y2, dash, width, out oldRop, out img, out oldpen);

            MoveToEx(hdc, x1, y1, IntPtr.Zero);
            LineTo(hdc, x2, y2);

            FinishDraw(bmp, graphics, hdc, oldpen, oldRop, img, dash);
        }

        public static void DrawXorRectangle(this System.Drawing.Graphics graphics, System.Drawing.Bitmap bmp, RectangleF rect, bool dash = false)
        {
            DrawXorRectangle(graphics, bmp, Convert.ToInt32(rect.X), Convert.ToInt32(rect.Y), Convert.ToInt32(rect.X+rect.Width), Convert.ToInt32(rect.Y+rect.Height), dash);
        }

        public static void DrawXorRectangle(this System.Drawing.Graphics graphics, System.Drawing.Bitmap bmp, int x1, int y1, int x2, int y2, bool dash = false)
        {
            int oldRop;
            IntPtr oldpen, img;
            var hdc = BeginDraw(bmp, graphics, x1, y1, x2, y2, dash, 1, out oldRop, out img, out oldpen);

            MoveToEx(hdc, x1, y1, IntPtr.Zero); //clockwise
            LineTo(hdc, x2, y1);
            LineTo(hdc, x2, y2);
            LineTo(hdc, x1, y2);
            LineTo(hdc, x1, y1);

            FinishDraw(bmp, graphics, hdc, oldpen, oldRop, img, dash);
        }
    }
}
