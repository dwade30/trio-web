﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security;
using System.Threading;

namespace fecherFoundation
{
	/// <summary>A Visual Basic FCCollection is an ordered set of items that can be referred to as a unit.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	public sealed class FCCollection : ICollection, IList, ISerializable, IDeserializationCallback
	{
		internal sealed class Node
		{
			internal object m_Value;
			internal string m_Key;
			internal FCCollection.Node m_Next;
			internal FCCollection.Node m_Prev;
			internal Node(string Key, object Value)
			{
				this.m_Value = Value;
				this.m_Key = Key;
			}
		}
		internal sealed class CollectionDebugView
		{
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private FCCollection m_InstanceBeingWatched;
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public object[] Items
			{
				get
				{
					checked
					{
						int count = this.m_InstanceBeingWatched.Count;
						object[] result;
						if (count == 0)
						{
							result = null;
						}
						else
						{
							object[] array = new object[count + 1];
							array[0] = "EmptyPlaceHolderMessage";
							int num = count;
							for (int i = 1; i <= num; i++)
							{
                                FCCollection.Node node = this.m_InstanceBeingWatched.InternalItemsList()[i - 1];
								array[i] = new FCCollection.KeyValuePair(node.m_Key, node.m_Value);
							}
							result = array;
						}
						return result;
					}
				}
			}
			public CollectionDebugView(FCCollection RealClass)
			{
				this.m_InstanceBeingWatched = RealClass;
			}
		}
		private sealed class FastList
		{
			private FCCollection.Node m_StartOfList;
			private FCCollection.Node m_EndOfList;
			private int m_Count;
			internal FCCollection.Node this[int Index]
			{
				get
				{
                    FCCollection.Node node = null;
                    FCCollection.Node expr_0B = this.GetNodeAtIndex(Index, ref node);
					if (expr_0B == null)
					{
						throw new ArgumentOutOfRangeException("Index");
					}
					return expr_0B;
				}
			}
			internal FastList()
			{
				this.m_Count = 0;
			}
			internal void Add(FCCollection.Node Node)
			{
				checked
				{
					if (this.m_StartOfList == null)
					{
						this.m_StartOfList = Node;
					}
					else
					{
						this.m_EndOfList.m_Next = Node;
						Node.m_Prev = this.m_EndOfList;
					}
					this.m_EndOfList = Node;
					this.m_Count++;
				}
			}
			internal int IndexOfValue(object Value)
			{
				checked
				{
                    FCCollection.Node node = this.m_StartOfList;
					int num = 0;
					int result;
					while (node != null)
					{
						if (this.DataIsEqual(node.m_Value, Value))
						{
							result = num;
							return result;
						}
						node = node.m_Next;
						num++;
					}
					result = -1;
					return result;
				}
			}
			internal void RemoveNode(FCCollection.Node NodeToBeDeleted)
			{
				this.DeleteNode(NodeToBeDeleted, NodeToBeDeleted.m_Prev);
			}
			internal FCCollection.Node RemoveAt(int Index)
			{
				checked
				{
					FCCollection.Node node = this.m_StartOfList;
					int num = 0;
					FCCollection.Node prevNode = null;
					while (num < Index && node != null)
					{
						prevNode = node;
						node = node.m_Next;
						num++;
					}
					if (node == null)
					{
						throw new ArgumentOutOfRangeException("Index");
					}
					this.DeleteNode(node, prevNode);
					return node;
				}
			}
			internal int Count()
			{
				return this.m_Count;
			}
			internal void Clear()
			{
				this.m_StartOfList = null;
				this.m_EndOfList = null;
				this.m_Count = 0;
			}
			internal void Insert(int Index, FCCollection.Node Node)
			{
				FCCollection.Node prevNode = null;
				if (Index < 0 || Index > this.m_Count)
				{
					throw new ArgumentOutOfRangeException("Index");
				}
				FCCollection.Node nodeAtIndex = this.GetNodeAtIndex(Index, ref prevNode);
				this.Insert(Node, prevNode, nodeAtIndex);
			}
			internal void InsertBefore(FCCollection.Node Node, FCCollection.Node NodeToInsertBefore)
			{
				this.Insert(Node, NodeToInsertBefore.m_Prev, NodeToInsertBefore);
			}
			internal void InsertAfter(FCCollection.Node Node, FCCollection.Node NodeToInsertAfter)
			{
				this.Insert(Node, NodeToInsertAfter, NodeToInsertAfter.m_Next);
			}
			internal FCCollection.Node GetFirstListNode()
			{
				return this.m_StartOfList;
			}
			private bool DataIsEqual(object obj1, object obj2)
			{
				return obj1 == obj2 || (obj1.GetType() == obj2.GetType() && object.Equals(obj1, obj2));
			}
			private FCCollection.Node GetNodeAtIndex(int Index, ref FCCollection.Node PrevNode)
			{
				checked
				{
					FCCollection.Node node = this.m_StartOfList;
					int num = 0;
					PrevNode = null;
					while (num < Index && node != null)
					{
						PrevNode = node;
						node = node.m_Next;
						num++;
					}
					return node;
				}
			}
			private void Insert(FCCollection.Node Node, FCCollection.Node PrevNode, FCCollection.Node CurrentNode)
			{
				checked
				{
					Node.m_Next = CurrentNode;
					if (CurrentNode != null)
					{
						CurrentNode.m_Prev = Node;
					}
					if (PrevNode == null)
					{
						this.m_StartOfList = Node;
					}
					else
					{
						PrevNode.m_Next = Node;
						Node.m_Prev = PrevNode;
					}
					if (Node.m_Next == null)
					{
						this.m_EndOfList = Node;
					}
					this.m_Count++;
				}
			}
			private void DeleteNode(FCCollection.Node NodeToBeDeleted, FCCollection.Node PrevNode)
			{
				checked
				{
					if (PrevNode == null)
					{
						this.m_StartOfList = this.m_StartOfList.m_Next;
						if (this.m_StartOfList == null)
						{
							this.m_EndOfList = null;
						}
						else
						{
							this.m_StartOfList.m_Prev = null;
						}
					}
					else
					{
						PrevNode.m_Next = NodeToBeDeleted.m_Next;
						if (PrevNode.m_Next == null)
						{
							this.m_EndOfList = PrevNode;
						}
						else
						{
							PrevNode.m_Next.m_Prev = PrevNode;
						}
					}
					this.m_Count--;
				}
			}
		}
		private struct KeyValuePair
		{
			private object m_Key;
			private object m_Value;
			public object Key
			{
				get
				{
					return this.m_Key;
				}
			}
			public object Value
			{
				get
				{
					return this.m_Value;
				}
			}
			internal KeyValuePair(object NewKey, object NewValue)
			{
				this = default(FCCollection.KeyValuePair);
				this.m_Key = NewKey;
				this.m_Value = NewValue;
			}
		}
		private const string SERIALIZATIONKEY_KEYS = "Keys";
		private const string SERIALIZATIONKEY_KEYSCOUNT = "KeysCount";
		private const string SERIALIZATIONKEY_VALUES = "Values";
		private const string SERIALIZATIONKEY_CULTUREINFO = "CultureInfo";
		private SerializationInfo m_DeserializationInfo;
		private Dictionary<string, FCCollection.Node> m_KeyedNodesHash;
		private FCCollection.FastList m_ItemsList;
		private ArrayList m_Iterators;
		private CultureInfo m_CultureInfo;
		/// <summary>Returns a specific element of a FCCollection object either by position or by key. Read-only.</summary>
		/// <returns>Returns a specific element of a FCCollection object either by position or by key. Read-only.</returns>
		/// <param name="Index">(A) A numeric expression that specifies the position of an element of the collection. <paramref name="Index" /> must be a number from 1 through the value of the collection's <see cref="P:Microsoft.VisualBasic.FCCollection.Count" /> property. Or (B) An Object expression that specifies the position or key string of an element of the collection.</param>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public dynamic this[int Index]
		{
			get
			{
				this.IndexCheck(Index);
				return this.m_ItemsList[checked(Index - 1)].m_Value;
			}
		}
		/// <summary>Returns a specific element of a FCCollection object either by position or by key. Read-only.</summary>
		/// <returns>Returns a specific element of a FCCollection object either by position or by key. Read-only.</returns>
		/// <param name="Key">A unique String expression that specifies a key string that can be used, instead of a positional index, to access an element of the collection. <paramref name="Key" /> must correspond to the <paramref name="Key" /> argument specified when the element was added to the collection.</param>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public object this[string Key]
		{
			get
			{
				if (Key == null)
				{
					throw new IndexOutOfRangeException("Argument_CollectionIndex");
				}
				FCCollection.Node node = null;
				if (!this.m_KeyedNodesHash.TryGetValue(Key, out node))
				{
					throw new ArgumentException("Argument_InvalidValue1");
				}
				return node.m_Value;
			}
		}
		/// <summary>Returns a specific element of a FCCollection object either by position or by key. Read-only.</summary>
		/// <returns>Returns a specific element of a FCCollection object either by position or by key. Read-only.</returns>
		/// <param name="Index">(A) A numeric expression that specifies the position of an element of the collection. <paramref name="Index" /> must be a number from 1 through the value of the collection's <see cref="P:Microsoft.VisualBasic.FCCollection.Count" /> property. Or (B) An Object expression that specifies the position or key string of an element of the collection.</param>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public object this[object Index]
		{
			get
			{
				object result;
				if (Index is string || Index is char || Index is char[])
				{
					string key = Convert.ToString(Index);
					result = this[key];
				}
				else
				{
					int index;
					try
					{
						index = Convert.ToInt32(Index);
					}
					catch (StackOverflowException ex)
					{
						throw ex;
					}
					catch (OutOfMemoryException ex2)
					{
						throw ex2;
					}
					catch (ThreadAbortException ex3)
					{
						throw ex3;
					}
					catch (Exception)
					{
						throw new ArgumentException("Argument_InvalidValue1");
					}
					result = this[index];
				}
				return result;
			}
		}
		/// <summary>Returns an Integer containing the number of elements in a collection. Read-only.</summary>
		/// <returns>Returns an Integer containing the number of elements in a collection. Read-only.</returns>
		/// <filterpriority>1</filterpriority>
		public int Count
		{
			get
			{
				return this.m_ItemsList.Count();
			}
		}
		int ICollection.Count
		{
			get
			{
				return this.m_ItemsList.Count();
			}
		}
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}
		bool IList.IsFixedSize
		{
			get
			{
				return false;
			}
		}
		bool IList.IsReadOnly
		{
			get
			{
				return false;
			}
		}
		object IList.this[int index]
		{
			get
			{
				return this.m_ItemsList[index].m_Value;
			}
			set
			{
				this.m_ItemsList[index].m_Value = value;
			}
		}
		/// <summary>Creates and returns a new Visual Basic <see cref="T:Microsoft.VisualBasic.FCCollection" /> object.</summary>
		public FCCollection()
		{
			this.Initialize(CultureInfo.CurrentCulture, 0);
		}
		/// <summary>Adds an element to a FCCollection object.</summary>
		/// <param name="Item">Required. An object of any type that specifies the element to add to the collection.</param>
		/// <param name="Key">Optional. A unique String expression that specifies a key string that can be used instead of a positional index to access this new element in the collection.</param>
		/// <param name="Before">Optional. An expression that specifies a relative position in the collection. The element to be added is placed in the collection before the element identified by the <paramref name="Before" /> argument. If <paramref name="Before" /> is a numeric expression, it must be a number from 1 through the value of the collection's <see cref="P:Microsoft.VisualBasic.FCCollection.Count" /> property. If <paramref name="Before" /> is a String expression, it must correspond to the key string specified when the element being referred to was added to the collection. You cannot specify both <paramref name="Before" /> and <paramref name="After" />.</param>
		/// <param name="After">Optional. An expression that specifies a relative position in the collection. The element to be added is placed in the collection after the element identified by the <paramref name="After" /> argument. If <paramref name="After" /> is a numeric expression, it must be a number from 1 through the value of the collection's Count property. If <paramref name="After" /> is a String expression, it must correspond to the key string specified when the element referred to was added to the collection. You cannot specify both <paramref name="Before" /> and <paramref name="After" />.</param>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public void Add(object Item, string Key = null, object Before = null, object After = null)
		{
			if (Before != null && After != null)
			{
				throw new ArgumentException("Collection_BeforeAfterExclusive");
			}
			FCCollection.Node node = new FCCollection.Node(Key, Item);
			if (Key != null)
			{
				try
				{
					this.m_KeyedNodesHash.Add(Key, node);
				}
				catch (ArgumentException var_1_31)
				{
					throw new ArgumentException("Collection_DuplicateKey");
				}
			}
			try
			{
				if (Before == null && After == null)
				{
					this.m_ItemsList.Add(node);
				}
				else
				{
					if (Before != null)
					{
						string text = Before as string;
						if (text != null)
						{
							FCCollection.Node nodeToInsertBefore = null;
							if (!this.m_KeyedNodesHash.TryGetValue(text, out nodeToInsertBefore))
							{
								throw new ArgumentException("Argument_InvalidValue1");
							}
							this.m_ItemsList.InsertBefore(node, nodeToInsertBefore);
						}
						else
						{
							this.m_ItemsList.Insert(checked(Convert.ToInt32(Before) - 1), node);
						}
					}
					else
					{
						string text2 = After as string;
						if (text2 != null)
						{
							FCCollection.Node nodeToInsertAfter = null;
							if (!this.m_KeyedNodesHash.TryGetValue(text2, out nodeToInsertAfter))
							{
								throw new ArgumentException("Argument_InvalidValue1");
							}
							this.m_ItemsList.InsertAfter(node, nodeToInsertAfter);
						}
						else
						{
							this.m_ItemsList.Insert(Convert.ToInt32(After), node);
						}
					}
				}
			}
			catch (OutOfMemoryException var_6_12B)
			{
				throw;
			}
			catch (ThreadAbortException var_7_12F)
			{
				throw;
			}
			catch (StackOverflowException var_8_133)
			{
				throw;
			}
			catch (Exception var_9_137)
			{
				if (Key != null)
				{
					this.m_KeyedNodesHash.Remove(Key);
				}
				throw;
			}
			//this.AdjustEnumeratorsOnNodeInserted(node);
		}
		/// <summary>Deletes all elements of a Visual Basic FCCollection object.</summary>
		/// <filterpriority>1</filterpriority>
		public void Clear()
		{
			checked
			{
				this.m_KeyedNodesHash.Clear();
				this.m_ItemsList.Clear();
				//for (int i = this.m_Iterators.Count - 1; i >= 0; i--)
				//{
				//	WeakReference weakReference = (WeakReference)this.m_Iterators[i];
				//	if (weakReference.IsAlive)
				//	{
				//		ForEachEnum forEachEnum = (ForEachEnum)weakReference.Target;
				//		if (forEachEnum != null)
				//		{
				//			forEachEnum.AdjustOnListCleared();
				//		}
				//	}
				//	else
				//	{
				//		this.m_Iterators.RemoveAt(i);
				//	}
				//}
			}
		}
		/// <summary>Returns a Boolean value indicating whether a Visual Basic FCCollection object contains an element with a specific key.</summary>
		/// <returns>Returns a Boolean value indicating whether a Visual Basic FCCollection object contains an element with a specific key.</returns>
		/// <param name="Key">Required. A String expression that specifies the key for which to search the elements of the collection.</param>
		/// <filterpriority>1</filterpriority>
		public bool Contains(string Key)
		{
			if (Key == null)
			{
				throw new ArgumentException("Argument_InvalidValue1");
			}
			return this.m_KeyedNodesHash.ContainsKey(Key);
		}
		/// <summary>Removes an element from a FCCollection object.</summary>
		/// <param name="Key">A unique String expression that specifies a key string that can be used, instead of a positional index, to access an element of the collection. <paramref name="Key" /> must correspond to the <paramref name="Key" /> argument specified when the element was added to the collection.</param>
		/// <filterpriority>1</filterpriority>
		public void Remove(string Key)
		{
			FCCollection.Node node = null;
			if (this.m_KeyedNodesHash.TryGetValue(Key, out node))
			{
				//this.AdjustEnumeratorsOnNodeRemoved(node);
				this.m_KeyedNodesHash.Remove(Key);
				this.m_ItemsList.RemoveNode(node);
				node.m_Prev = null;
				node.m_Next = null;
				return;
			}
			throw new ArgumentException("Argument_InvalidValue1");
		}
		/// <summary>Removes an element from a FCCollection object.</summary>
		/// <param name="Index">A numeric expression that specifies the position of an element of the collection. <paramref name="Index" /> must be a number from 1 through the value of the collection's <see cref="P:Microsoft.VisualBasic.FCCollection.Count" /> property.</param>
		/// <filterpriority>1</filterpriority>
		public void Remove(int Index)
		{
			this.IndexCheck(Index);
			FCCollection.Node node = this.m_ItemsList.RemoveAt(checked(Index - 1));
			//this.AdjustEnumeratorsOnNodeRemoved(node);
			if (node.m_Key != null)
			{
				this.m_KeyedNodesHash.Remove(node.m_Key);
			}
			node.m_Prev = null;
			node.m_Next = null;
		}
		/// <summary>Returns a reference to an enumerator object, which is used to iterate over a <see cref="T:Microsoft.VisualBasic.FCCollection" /> object.</summary>
		/// <returns>Returns a reference to an enumerator object, which is used to iterate over a <see cref="T:Microsoft.VisualBasic.FCCollection" /> object.</returns>
		/// <filterpriority>1</filterpriority>
		public IEnumerator GetEnumerator()
		{
            //checked
            //{
            //    for (int i = this.m_Iterators.Count - 1; i >= 0; i--)
            //    {
            //        if (!((WeakReference)this.m_Iterators[i]).IsAlive)
            //        {
            //            this.m_Iterators.RemoveAt(i);
            //        }
            //    }
            //    ForEachEnum forEachEnum = new ForEachEnum(this);
            //    WeakReference weakReference = new WeakReference(forEachEnum);
            //    forEachEnum.WeakRef = weakReference;
            //    this.m_Iterators.Add(weakReference);
            //    return forEachEnum;
            //}
            //yield return this.m_ItemsList;

            //PPJ:FINAL:BCU - #127 - return list of items in FastList class instead of FastList class instance
            List<object> items = new List<object>();
            for (int index = 0; index < this.m_ItemsList.Count(); index++)
            {
                items.Add(this.m_ItemsList[index].m_Value);
            }
            return items.GetEnumerator();
		}
        internal void RemoveIterator(WeakReference weakref)
		{
			this.m_Iterators.Remove(weakref);
		}
		internal void AddIterator(WeakReference weakref)
		{
			this.m_Iterators.Add(weakref);
		}
		internal FCCollection.Node GetFirstListNode()
		{
			return this.m_ItemsList.GetFirstListNode();
		}
		private void Initialize(CultureInfo CultureInfo, int StartingHashCapacity = 0)
		{
			if (StartingHashCapacity > 0)
			{
				this.m_KeyedNodesHash = new Dictionary<string, FCCollection.Node>(StartingHashCapacity, StringComparer.Create(CultureInfo, true));
			}
			else
			{
				this.m_KeyedNodesHash = new Dictionary<string, FCCollection.Node>(StringComparer.Create(CultureInfo, true));
			}
			this.m_ItemsList = new FCCollection.FastList();
			this.m_Iterators = new ArrayList();
			this.m_CultureInfo = CultureInfo;
		}
		//private void AdjustEnumeratorsOnNodeInserted(FCCollection.Node NewNode)
		//{
		//	this.AdjustEnumeratorsHelper(NewNode, ForEachEnum.AdjustIndexType.Insert);
		//}
		//private void AdjustEnumeratorsOnNodeRemoved(FCCollection.Node RemovedNode)
		//{
		//	this.AdjustEnumeratorsHelper(RemovedNode, ForEachEnum.AdjustIndexType.Remove);
		//}
		//private void AdjustEnumeratorsHelper(FCCollection.Node NewOrRemovedNode, ForEachEnum.AdjustIndexType Type)
		//{
		//	checked
		//	{
		//		for (int i = this.m_Iterators.Count - 1; i >= 0; i--)
		//		{
		//			WeakReference weakReference = (WeakReference)this.m_Iterators[i];
		//			if (weakReference.IsAlive)
		//			{
		//				ForEachEnum forEachEnum = (ForEachEnum)weakReference.Target;
		//				if (forEachEnum != null)
		//				{
		//					forEachEnum.Adjust(NewOrRemovedNode, Type);
		//				}
		//			}
		//			else
		//			{
		//				this.m_Iterators.RemoveAt(i);
		//			}
		//		}
		//	}
		//}
		private void IndexCheck(int Index)
		{
			if (Index < 1 || Index > this.m_ItemsList.Count())
			{
				throw new IndexOutOfRangeException("Argument_CollectionIndex");
			}
		}
		private FCCollection.FastList InternalItemsList()
		{
			return this.m_ItemsList;
		}
		private FCCollection(SerializationInfo info, StreamingContext context)
		{
			this.m_DeserializationInfo = info;
		}
		[SecurityCritical]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			checked
			{
				string[] array = new string[this.Count - 1 + 1];
				object[] array2 = new object[this.Count - 1 + 1];
				FCCollection.Node node = this.GetFirstListNode();
				int num = 0;
				while (node != null)
				{
					if (node.m_Key != null)
					{
						num++;
					}
					int num2 = 0;
					array[num2] = node.m_Key;
					array2[num2] = node.m_Value;
					num2++;
					node = node.m_Next;
				}
				info.AddValue("Keys", array, typeof(string[]));
				info.AddValue("KeysCount", num, typeof(int));
				info.AddValue("Values", array2, typeof(object[]));
				info.AddValue("CultureInfo", this.m_CultureInfo);
			}
		}
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			checked
			{
				try
				{
					CultureInfo cultureInfo = (CultureInfo)this.m_DeserializationInfo.GetValue("CultureInfo", typeof(CultureInfo));
					if (cultureInfo == null)
					{
						throw new SerializationException("Serialization_MissingCultureInfo");
					}
					string[] array = (string[])this.m_DeserializationInfo.GetValue("Keys", typeof(string[]));
					object[] array2 = (object[])this.m_DeserializationInfo.GetValue("Values", typeof(object[]));
					if (array == null)
					{
						throw new SerializationException("Serialization_MissingKeys");
					}
					if (array2 == null)
					{
						throw new SerializationException("Serialization_MissingValues");
					}
					if (array.Length != array2.Length)
					{
						throw new SerializationException("Serialization_KeyValueDifferentSizes");
					}
					int num = this.m_DeserializationInfo.GetInt32("KeysCount");
					if (num < 0 || num > array.Length)
					{
						num = 0;
					}
					this.Initialize(cultureInfo, num);
					int num2 = array.Length - 1;
					for (int i = 0; i <= num2; i++)
					{
						this.Add(array2[i], array[i], null, null);
					}
					this.m_DeserializationInfo = null;
				}
				finally
				{
					if (this.m_DeserializationInfo != null)
					{
						this.m_DeserializationInfo = null;
						this.Initialize(CultureInfo.CurrentCulture, 0);
					}
				}
			}
		}

        public void CopyTo(Array array, int index)
		{
			checked
			{
				if (array == null)
				{
					throw new ArgumentNullException("Argument_InvalidNullValue1");
				}
				if (array.Rank != 1)
				{
					throw new ArgumentException("Argument_RankEQOne1");
				}
				if (index < 0 || array.Length - index < this.Count)
				{
					throw new ArgumentException("Argument_InvalidValue1");
				}
				object[] array2 = array as object[];
				if (array2 != null)
				{
					int count = this.Count;
					for (int i = 1; i <= count; i++)
					{
						array2[index + i - 1] = this[i];
					}
					return;
				}
				int count2 = this.Count;
				for (int i = 1; i <= count2; i++)
				{
					array.SetValue(this[i], index + i - 1);
				}
			}
		}
        public int Add(object value)
		{
			this.Add(value, null, null, null);
			return checked(this.m_ItemsList.Count() - 1);
		}
        public void Insert(int index, object value)
		{
			FCCollection.Node node = new FCCollection.Node(null, value);
			this.m_ItemsList.Insert(index, node);
			//this.AdjustEnumeratorsOnNodeInserted(node);
		}
        public void RemoveAt(int index)
		{
			FCCollection.Node node = this.m_ItemsList.RemoveAt(index);
			//this.AdjustEnumeratorsOnNodeRemoved(node);
			if (node.m_Key != null)
			{
				this.m_KeyedNodesHash.Remove(node.m_Key);
			}
			node.m_Prev = null;
			node.m_Next = null;
		}
        public void Remove(object value)
		{
			int num = this.IndexOf(value);
			if (num != -1)
			{
				this.RemoveAt(num);
			}
		}

        public bool Contains(object value)
		{
			return this.IndexOf(value) != -1;
		}
		public int IndexOf(object value)
		{
			return this.m_ItemsList.IndexOfValue(value);
		}
	}
}
