﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace fecherFoundation
{
    public class PrinterProperties
    {
        #region Enums
        public enum jgexPPColorModeConstants : int
        {
            jgexPPCMMonochrome = 1,
            jgexPPCMColor = 2
        }

        public enum jgexHeaderFooterPositionConstants : int
        {
            jgexHFCenter = 1,
            jgexHFLeft = 2,
            jgexHFRight = 3
        }

        public enum jgexPPOrientationConstants : int
        {
            jgexPPPortrait = 1,
            jgexPPLandscape = 2
        }
        #endregion

        #region Members
        private GridEXPrintDocument printDocument;
        private bool pageSetupCanceled = false;
        private PrintController defaultPrintController;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets/sets the height, in twips, of the bottom margin
        /// </summary>
        public int BottomMargin
        {
            get
            {
                return this.printDocument.DefaultPageSettings.Margins.Bottom;
            }
            set
            {
                this.printDocument.DefaultPageSettings.Margins.Bottom = value;
            }
        }

        /// <summary>
        /// Gets/sets the number of card columns to be printed in a page
        /// </summary>
        public int CardColumnsPerPage
        {
            get
            {
                return this.printDocument.CardColumnsPerPage;
            }
            set
            {
                this.printDocument.CardColumnsPerPage = value;
            }
        }

        /// <summary>
        /// Gets the height of the printable area in a page.
        /// </summary>
        public float ClientHeight
        {
            get
            {
                return this.printDocument.DefaultPageSettings.PrintableArea.Height;
            }
        }

        /// <summary>
        /// Gets the width of the printable area in a page.
        /// </summary>
        public float ClientWidth
        {
            get
            {
                return this.printDocument.DefaultPageSettings.PrintableArea.Width;
            }
        }

        /// <summary>
        /// Determines whether collation should be used when printing multiple copies
        /// </summary>
        public bool Collate
        {
            get
            {
                return this.printDocument.PrinterSettings.Collate;
            }
            set
            {
                this.printDocument.PrinterSettings.Collate = value;
            }
        }

        /// <summary>
        /// Gets/sets a value that determines whether a document should be printed in color or monochrome
        /// </summary>
        public jgexPPColorModeConstants ColorMode
        {
            get
            {
                return this.printDocument.DefaultPageSettings.Color ? jgexPPColorModeConstants.jgexPPCMColor : jgexPPColorModeConstants.jgexPPCMMonochrome;
            }
            set
            {
                this.printDocument.DefaultPageSettings.Color = value == jgexPPColorModeConstants.jgexPPCMColor;
            }
        }

        /// <summary>
        /// Gets/sets the number of copies to be printed
        /// </summary>
        public short Copies
        {
            get
            {
                return this.printDocument.PrinterSettings.Copies;
            }
            set
            {
                this.printDocument.PrinterSettings.Copies = value;
            }
        }

        /// <summary>
        /// Gets/sets the name of the device a driver supports.
        /// </summary>
        public string DeviceName
        {
            get
            {
                return this.printDocument.PrinterSettings.PrinterName;
            }
            set
            {
                this.printDocument.PrinterSettings.PrinterName = value;
            }
        }

        /// <summary>
        /// Gets/sets the name of the document to be printed
        /// </summary>
        public string DocumentName
        {
            get
            {
                return this.printDocument.DocumentName;
            }
            set
            {
                this.printDocument.DocumentName = value;
            }
        }

        [Obsolete("DriverName is not available in .NET PrinterProperties")]
        public string DriverName
        {
            get { return ""; }
            set
            {
            }
        }

        /// <summary>
        /// Determines whether the grid should scale printed output to fit all visible columns in a page
        /// </summary>
        public bool FitColumns
        {
            get
            {
                return this.printDocument.FitColumns != FitColumnsMode.NoFit;
            }
            set
            {
                this.printDocument.FitColumns = value ? FitColumnsMode.SizingColumns : FitColumnsMode.NoFit;
            }
        }

        /// <summary>
        /// Gets/sets the distance from the bottom edge of the paper to the top edge of the footer.
        /// </summary>
        public int FooterDistance
        {
            get
            {
                return this.printDocument.FooterDistance;
            }
            set
            {
                this.printDocument.FooterDistance = value;
            }
        }

        /// <summary>
        /// Gets/sets the distance, in twips, from the top edge of the paper to the top edge of the header
        /// </summary>
        public int HeaderDistance
        {
            get
            {
                return this.printDocument.HeaderDistance;
            }
            set
            {
                this.printDocument.HeaderDistance = value;
            }
        }

        /// <summary>
        /// Gets/sets the width, in twips, of the left margin.
        /// </summary>
        public int LeftMargin
        {
            get
            {
                return this.printDocument.DefaultPageSettings.Margins.Left;
            }
            set
            {
                this.printDocument.DefaultPageSettings.Margins.Left = value;
            }
        }

        /// <summary>
        /// Gets/sets measurement units to be used in the Page Setup dialog
        /// </summary>
        [Obsolete(".NET does not support MeasurementUnits")]
        public object MeasurementUnits { get; set; }

        /// <summary>
        /// Determines whether documents are printed in portrait or landscape mode
        /// </summary>
        public jgexPPOrientationConstants Orientation
        {
            get
            {
                return this.printDocument.DefaultPageSettings.Landscape ? jgexPPOrientationConstants.jgexPPLandscape : jgexPPOrientationConstants.jgexPPPortrait;
            }
            set
            {
                this.printDocument.DefaultPageSettings.Landscape = value == jgexPPOrientationConstants.jgexPPLandscape;
            }
        }

        /// <summary>
        /// Gets/sets a Font object used to draw page footers in a document.
        /// </summary>
        public Font PageFooterFont
        {
            get
            {
                return this.printDocument.PageFooterFormatStyle.Font;
            }
            set
            {
                this.printDocument.PageFooterFormatStyle.Font = value;
            }
        }

        /// <summary>
        /// Gets/sets a Font object used to draw the page header in a document.
        /// </summary>
        public Font PageHeaderFont
        {
            get
            {
                return this.printDocument.PageHeaderFormatStyle.Font;
            }
            set
            {
                this.printDocument.PageHeaderFormatStyle.Font = value;
            }
        }

        /// <summary>
        /// Returns true if user pressed cancel in the Page Setup dialog
        /// </summary>
        [DefaultValue(false)]
        public bool PageSetupCanceled
        {
            get
            {
                return this.pageSetupCanceled;
            }
            set
            {
                this.pageSetupCanceled = value;
            }
        }

        /// <summary>
        /// Gets/sets a value indicating the default paper bin on the printer from which paper is fed when printing
        /// </summary>
        public PaperSourceKind PaperBin
        {
            get
            {
                return this.printDocument.PrinterSettings.DefaultPageSettings.PaperSource.Kind;
            }
            set
            {
                if (this.printDocument.PrinterSettings.PaperSources.Count == 0)
                {
                    return;
                }
                for (int count = 0; count < this.printDocument.PrinterSettings.PaperSources.Count; count++)
                {
                    if (this.printDocument.PrinterSettings.PaperSources[count].Kind == value)
                    {
                        this.printDocument.PrinterSettings.DefaultPageSettings.PaperSource = this.printDocument.PrinterSettings.PaperSources[count];
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Gets/sets the height of the paper, in hundredths of an inch
        /// </summary>
        public int PaperHeight
        {
            get
            {
                return this.printDocument.DefaultPageSettings.PaperSize.Height;
            }
            set
            {
                this.printDocument.DefaultPageSettings.PaperSize.Height = value;
            }
        }

        /// <summary>
        /// Gets/sets the paper size of the page
        /// </summary>
        public int PaperSize
        {
            // TODO:BAN - int value must be converted to PaperSize class
            //get
            //{
            //    return this.printDocument.DefaultPageSettings.PaperSize;
            //}
            //set
            //{
            //    this.printDocument.DefaultPageSettings.PaperSize = value;
            //}
            get;set;
        }

        /// <summary>
        /// Gets/sets the width of the paper, in hundredths of an inch
        /// </summary>
        public int PaperWidth
        {
            get
            {
                return this.printDocument.DefaultPageSettings.PaperSize.Width;
            }
            set
            {
                this.printDocument.DefaultPageSettings.PaperSize.Width = value;
            }
        }

        /// <summary>
        /// Determines whether the control prints preview rows or not
        /// </summary>
        [Obsolete(".NET does not support the PrintPreviewRows property")]
        public bool PrintPreviewRows { get; set; }

        /// <summary>
        /// Determines whether print progress dialog should be displayed when the control is printing a document
        /// </summary>
        public bool PrintProgressDialog
        {
            get
            {
                return this.printDocument.PrintController.GetType() == typeof(StandardPrintController);
            }
            set
            {
                // If PrintProgressDialog value to be set is false, a standart print controller needs to be set in otder for the 
                // PrintProgressDialog not to be displayed. If the value is true, the PrintProgressDialog should be displayed, 
                // thus the default print controller needs to be set
                if (!value)
                {
                    this.printDocument.PrintController = new StandardPrintController();
                }
                else
                {
                    this.printDocument.PrintController = defaultPrintController;
                }
            }

        }

        [Obsolete(".NET does not support the PrintQuality property." +
        "Use the PrinterResolution property from the DefaultPageSettings and get the resolutions from the PrinterProperties")]
        public int PrintQuality { get; set; }

        [Obsolete(".NET does not support the RepeatFrozenColumns property")]
        public bool RepeatFrozenColumns { get; set; }

        /// <summary>
        /// Determines whether column headers should appear on each page
        /// </summary>
        public bool RepeatHeaders
        {
            get
            {
                return this.printDocument.RepeatHeaders;
            }
            set
            {
                this.printDocument.RepeatHeaders = value;
            }
        }

        /// <summary>
        /// Gets/sets the width of the right margin
        /// </summary>
        public int RightMargin
        {
            get
            {
                return this.printDocument.DefaultPageSettings.Margins.Right;
            }
            set
            {
                this.printDocument.DefaultPageSettings.Margins.Right = value;
            }
        }

        /// <summary>
        /// Gets/sets the width of the top margin
        /// </summary>
        public int TopMargin
        {
            get
            {
                return this.printDocument.DefaultPageSettings.Margins.Top;
            }
            set
            {
                this.printDocument.DefaultPageSettings.Margins.Top = value;
            }
        }

        /// <summary>
        /// Determines whether system colors should be translated to gray scale colors for printing
        /// </summary>
        public bool TranslateColors
        {
            get
            {
                return this.printDocument.TranslateSystemColors;
            }
            set
            {
                this.printDocument.TranslateSystemColors = value;
            }
        }

        /// <summary>
        /// Determines whether background color should be used to when printing
        /// </summary>
        [Obsolete(".NET does not support the TransparentBackground property")]
        public bool TransparentBackground { get; set; }

        /// <summary>
        /// Gets/sets the weight of the page header font
        /// </summary>
        public int PageHeaderFontWeight { get; set; }

        /// <summary>
        /// Gets/sets the weight of the page footer font
        /// </summary>
        public int PageFooterFontWeight { get; set; }

        #endregion

        #region Internal Properties
        internal GridEXPrintDocument PrintDocument
        {
            get
            {
                return this.printDocument;
            }
        }
        #endregion

        #region Constructor
        internal PrinterProperties(GridEXPrintDocument document)
        {
            this.printDocument = document;
            defaultPrintController = this.printDocument.PrintController;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the string displayed in the page header
        /// </summary>
        /// <param name="position">The header position from where the string should be returned. Can be jgexHFCenter, jgexHFLeft, jgexHFRight</param>
        /// <returns>The string from the page header</returns>
        public string HeaderString(jgexHeaderFooterPositionConstants position)
        {
            if (position == jgexHeaderFooterPositionConstants.jgexHFCenter)
            {
                return this.printDocument.PageHeaderCenter;
            }
            else if (position == jgexHeaderFooterPositionConstants.jgexHFLeft)
            {
                return this.printDocument.PageHeaderLeft;
            }
            else
            {
                return this.printDocument.PageHeaderRight;
            }
        }

        /// <summary>
        /// Sets the string displayed in the page header
        /// </summary>
        /// <param name="position">The header position for where the string should be set. Can be jgexHFCenter, jgexHFLeft, jgexHFRight</param>
        /// <param name="footerString">The value to be set to the page header</param>
        public void HeaderString(jgexHeaderFooterPositionConstants position, string headerString)
        {
            if (position == jgexHeaderFooterPositionConstants.jgexHFCenter)
            {
                this.printDocument.PageHeaderCenter = headerString;
            }
            else if (position == jgexHeaderFooterPositionConstants.jgexHFLeft)
            {
                this.printDocument.PageHeaderLeft = headerString;
            }
            else
            {
                this.printDocument.PageHeaderRight = headerString;
            }
        }

        /// <summary>
        /// Returns the string displayed in the page footer
        /// </summary>
        /// <param name="position">The footer position from where the string should be returned. Can be jgexHFCenter, jgexHFLeft, jgexHFRight</param>
        /// <returns>The string from the page footer</returns>
        public string FooterString(jgexHeaderFooterPositionConstants position)
        {
            if (position == jgexHeaderFooterPositionConstants.jgexHFCenter)
            {
                return this.printDocument.PageFooterCenter;
            }
            else if (position == jgexHeaderFooterPositionConstants.jgexHFLeft)
            {
                return this.printDocument.PageFooterLeft;
            }
            else
            {
                return this.printDocument.PageFooterRight;
            }
        }

        /// <summary>
        /// Sets the string displayed in the page footer
        /// </summary>
        /// <param name="position">The footer position for where the string should be set. Can be jgexHFCenter, jgexHFLeft, jgexHFRight</param>
        /// <param name="footerString">The value to be set to the page footert</param>
        public void FooterString(jgexHeaderFooterPositionConstants position, string footerString)
        {
            if (position == jgexHeaderFooterPositionConstants.jgexHFCenter)
            {
                this.printDocument.PageFooterCenter = footerString;
            }
            else if (position == jgexHeaderFooterPositionConstants.jgexHFLeft)
            {
                this.printDocument.PageFooterLeft = footerString;
            }
            else
            {
                this.printDocument.PageFooterRight = footerString;
            }
        }

        /// <summary>
        /// Displays the page setup dialog
        /// </summary>
        public void PageSetup()
        {
            PageSetupDialog pageSetupDialog = new PageSetupDialog();
            pageSetupDialog.PageSettings = this.printDocument.DefaultPageSettings;
            this.pageSetupCanceled = false;
            if (pageSetupDialog.ShowDialog() == DialogResult.OK)
            {
                this.printDocument.DefaultPageSettings = pageSetupDialog.PageSettings;
            }
            else
            {
                this.pageSetupCanceled = true;
            }
        }

        #endregion
    }
}
