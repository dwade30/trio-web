﻿using System.Runtime.Remoting.Messaging;
using fecherFoundation.DataBaseLayer;
using Janus.Windows.GridEX;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;

namespace fecherFoundation
{
    public class FCGridEX : GridEX
    {
        #region Enums

        public enum jgexCursorLocationConstants : int
        {
            jgexUseClient = 3,
            jgexUseServer = 2
        }

        public enum jgexRecordsetTypeConstants : int
        {
            jgexRSADOKeyset = 1,
            jgexRSDAODynaset = 2,
            jgexRSADOStatic = 3,
            jgexRSDAOSnapshot = 4,
            jgexRSDAOTable = 1
        }

        public enum jgexOleDropModeConstants : int
        {
            jgexOleDropNone = 0,
            jgexOleDropManual = 1
        }

        public enum jgexGroupFooterStyleConstants : int
        {
            jgexNoGroupFooter = 0,
            jgexCaptionGroupFooter = 1,
            jgexTotalsGroupFooter = 2
        }

        public enum jgexLockTypeConstants : int
        {
            adLockUnspecified = -1,
            adLockReadOnly = 1,
            adLockPessimistic = 2,
            adLockOptimistic = 3,
            adLockBatchOptimistic = 4
        }

        #endregion

        #region events

        /// <summary>
        /// SelectionChanged - event raised when the user selects a different cell, or the Row/Col properties have been changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnSelectionChanged(object sender, SelectionChangedEventArgs e);

        public event OnSelectionChanged SelectionChanged;

        /// <summary>
        /// BeforeUpdate - event raised when adding or updating a record from the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnBeforeUpdate(object sender, System.ComponentModel.CancelEventArgs e);

        public event OnBeforeUpdate BeforeUpdate;

        #endregion

        #region Members

        private int lastRow = -1;
        private int lastCol = -1;
        public bool inDataSourceFunc = false;

        private jgexCursorLocationConstants cursorLocation = jgexCursorLocationConstants.jgexUseServer;
        private jgexRecordsetTypeConstants recordsetType = jgexRecordsetTypeConstants.jgexRSDAODynaset;
        private int previewRowLines = 0;
        private GridEXPrintDocument printDocument = new GridEXPrintDocument();
        private PrinterProperties printerProperties;

        #endregion

        #region Properties

        /// <summary>
        /// LastRow returns the previous current row of the grid
        /// The row may change either via user UI interaction or by speciffically changing the Row in the source code
        /// </summary>
        public int LastRow
        {
            get { return lastRow; }
        }

        /// <summary>
        /// LastCol returns the previous current column of the grid
        /// The column may change either via user UI interaction or by speciffically changing the Col in the source code
        /// </summary>
        public int LastCol
        {
            get { return lastCol; }
        }

        /// <summary>
        /// Row sets/returns the current row of the grid
        /// In VB6 - the Row is returned 1 based, replicate behavior when returning the .NET 0 based value by adding 1
        /// And substracting -1 when setting
        /// </summary>
        public new int Row
        {
            get { return base.Row + 1; }
            set
            {
                // FC:FINAL:BAN - in the activex GridEx control from VB6, even if the grid does not containt rows, the Row setter will not throw an error
                int row = value - 1;
                if (base.RowCount < row)
                {
                    return;
                }

                try
                {
                    base.Row = row;
                }
                catch (System.Exception ex) { }
            }
        }

        /// <summary>
        /// Col sets/returns the current column of the grid
        /// In VB6 - the Col is returned 1 based, replicate behavior when returning the .NET 0 based value by adding 1
        /// And substracting -1 when setting
        /// </summary>
        public new int Col
        {
            get { return base.Col + 1; }
            set { base.Col = value - 1; }
        }

        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentX { get; set; }
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentY { get; set; }
        [Obsolete("The column header height is not user adjustible in the .NET version")]
        public int ColumnHeaderHeight { get; set; }

        /// <summary>
        /// Gets/sets the column count of the current RootTable. If the RootTable does not contain column, the number of columns specified will be added
        /// as empty columns
        /// </summary>
        public int ColumnsCount
        {
            get
            {
                return this.RootTable.Columns.Count;
            }
            set
            {
                this.RootTable.Columns.Clear();
                for (int count = 0; count < value; count++)
                {
                    this.RootTable.Columns.Add();
                }
            }
        }

        /// <summary>
        /// Gets/sets the image count of the grid. If the grid does not contain any images, the image list will be filled empty images up to the value specified for the ImageCount
        /// </summary>
        public int ImageCount
        {
            get
            {
                return this.ImageList.Images.Count;
            }
            set
            {
                this.ImageList.Images.Clear();
                for (int count = 0; count < value; count++)
                {
                    this.ImageList.Images.Add(new Bitmap(1, 1));
                }
            }
        }

        /// <summary>
        /// Gets/sets the item count of the grid. If the grid does not contain any items, items will be added up to the value specified in the ItemCount
        /// </summary>
        public int ItemCount
        {
            get
            {
                return this.RowCount;
            }
            set
            {
                if (base.BoundMode == Janus.Windows.GridEX.BoundMode.Unbound)
                {
                    this.ClearItems();

                    for (int count = 0; count < value; count++)
                    {
                        this.AddItem();
                    }
                }
            }
        }

        /// <summary>
        /// Gets/sets the number of lines to be displayed in preview rows
        /// </summary>
        public int PreviewRowLines
        {
            get
            {
                return this.RootTable.PreviewRowLines;
            }
            set
            {
                previewRowLines = value;
                this.RootTable.PreviewRowLines = previewRowLines;
            }
        }

        [Obsolete(".NET does not support the Options property")]
        public int Options { get; set; }

        /// <summary>
        /// Prevents recalculation of the column layout when the recordset is created.
        /// </summary>
        public int MethodHoldFields { get; set; }

        [Obsolete("VB6 GridEX designer property that is no longer used in the .NET version")]
        public int IntPropNothing { get; set; }

        /// <summary>
        /// Gets/sets the number of Format Styles. If the FormatStyles collection is empty 
        /// The setter will create empty formats up to the value specified in the FormatStylesCount property
        /// </summary>
        public int FormatStylesCount
        {
            get
            {
                return this.FormatStyles.Count;
            }
            set
            {
                this.FormatStyles.Clear();
                for (int count = 0; count < value; count++)
                {
                    this.FormatStyles.Add();
                }
            }
        }

        [Obsolete(".NET does not support row height changing")]
        public int RowHeight { get; set; }

        [Obsolete("Returns/sets the index or key of the column used to supply a data value to another GridEX control.")]
        public string BoundColumnIndex { get; set; }
        
        [Obsolete("Returns/sets the index or key of the column that replaces Id values in a DropDown GridEX Control.")]
        public string ReplaceColumnIndex { get; set; }

        [Obsolete("Returns/sets the index or key of the column used to get values for scroll tool tips.")]
        public string ScrollToolTipColumn { get; set; }

        [Obsolete("GridEX control for .Net doesn't have support for the ScrollTooltip property")]
        public bool ScrollToolTips { get; set; }

        // TODO:BAN
        public bool FullyLoaded { get; set; }

        /// <summary>
        /// Determines whether the control should show tool tips over cells where text is not entirely displayed
        /// </summary>
        public bool ShowToolTips
        {
            get
            {
                return this.CellToolTip == Janus.Windows.GridEX.CellToolTip.TruncatedText;
            }
            set
            {
                if (value)
                {
                    this.CellToolTip = Janus.Windows.GridEX.CellToolTip.TruncatedText;
                }
                else
                {
                    this.CellToolTip = Janus.Windows.GridEX.CellToolTip.NoToolTip;
                }
            }
        }

        /// <summary>
        /// Determines wheter dragging of the selected rows are detected
        /// </summary>
        public bool DetectRowDrag { get; set; }

        /// <summary>
        /// Returns/sets a value indicating whether the Database object must be opened as read only.
        /// </summary>
        [DefaultValue(false)]
        public bool ReadOnly { get; set; }

        [Obsolete("The .NET grid control does not support row sizing. The control sizes the rows automatically, depending on their content.")]
        public bool AllowRowSizing { get; set; }

        /// <summary>
        /// Gets/sets whether or not the group condition will be applied
        /// </summary>
        public bool ApplyGroupCondition { get; set; }

        /// <summary>
        /// Determines whether the control should scroll its contents while the user moves the scroll box along the vertical scroll bar.
        /// </summary>
        public bool ContScroll
        {
            get
            {
                return this.ContinuousScroll;
            }
            set
            {
                this.ContinuousScroll = value;
            }
        }

        /// <summary>
        /// Gets/sets the PrinterProperties object of the control
        /// </summary>
        public PrinterProperties PrinterProperties
        {
            get
            {
                return this.printerProperties;
            }
            set
            {
                this.printerProperties = value;
            }
        }

        [Obsolete("Designer property that gets/sets the image for a null/DBNull value")]
        public object ImagePictureNothing { get; set; }

        /// <summary>
        /// Gets/sets the location of the cursor engine (ADO mode only)
        /// </summary>
        [DefaultValue(jgexCursorLocationConstants.jgexUseServer)]
        public jgexCursorLocationConstants CursorLocation
        {
            get
            {
                return cursorLocation;
            }
            set
            {
                this.cursorLocation = value;
            }
        }

        /// <summary>
        /// Gets/sets a value indicating the type of recordset object
        /// </summary>
        [DefaultValue(jgexRecordsetTypeConstants.jgexRSDAODynaset)]
        public jgexRecordsetTypeConstants RecordsetType
        {
            get
            {
                return this.recordsetType;
            }
            set
            {
                this.recordsetType = value;
            }
        }

        [Obsolete(".NET does not support the property OLEDropMode")]
        [DefaultValue(jgexOleDropModeConstants.jgexOleDropNone)]
        public jgexOleDropModeConstants OLEDropMode { get; set; }

        [Obsolete(".NET does not support the property GroupFooterStyle")]
        [DefaultValue(jgexGroupFooterStyleConstants.jgexNoGroupFooter)]
        public jgexGroupFooterStyleConstants GroupFooterStyle { get; set; }

        /// <summary>
        /// Gets/sets the RootTable. If the RootTable was unassigned, it will be instantiated and returned
        /// </summary>
        public new GridEXTable RootTable
        {
            get
            {
                if (base.RootTable == null)
                {
                    base.RootTable = new GridEXTable();
                }

                return base.RootTable;
            }
            set
            {
                base.RootTable = value;
            }
        }

        /// <summary>
        /// Gets/sets the DataSource of the control
        /// </summary>
        public new object DataSource
        {
            get
            {
                return base.DataSource;
            }
            set
            {
                if (value == null)
                {
                    base.DataSource = null;
                    //The grid needs to have it`s structure retrieved, and the RootTable for the grid needs to be created in order to replicate the behavior and apearance of VB6
                    base.RetrieveStructure();
                    base.RootTable = new GridEXTable();
                    return;
                }

                DataTable source = value as DataTable;

                bool needToRetrieveStructure = true;
                if (base.DataSource != null)
                {
                    DataTable gridSource = base.DataSource as DataTable;

                    if (gridSource.Columns.Count != source.Columns.Count)
                    {
                        needToRetrieveStructure = true;
                    }

                    var dtColumns = gridSource.Columns.Cast<DataColumn>();
                    var valueColumns = source.Columns.Cast<DataColumn>();

                    var exceptCount = dtColumns.Except(valueColumns, DataColumnEqualityComparer.Instance).Count();
                    needToRetrieveStructure = !(exceptCount == 0);
                }

                // JSP prevent lastRow to be changed
                this.inDataSourceFunc = true;

                // PJ:Error:EndlessLoop
                base.DataSource = source.CloneRows();
                base.SetDataBinding(source.CloneRows(), "");

                if (needToRetrieveStructure)
                {
                    base.RetrieveStructure();
                }

                // JSP prevent lastRow to be changed
                this.inDataSourceFunc = false;

                if (base.RootTable == null)
                {
                    return;
                }

                foreach (GridEXColumn col in base.RootTable.Columns)
                {
                    if (col.FormatString.ToLower() == "c")
                    {
                        col.FormatString = "#0";
                    }
                }
            }
        }

        /// <summary>
        /// Gets/sets the image list of the control. If, when getting the image list, the image list is null, a new image list will be instantiated
        /// and added as the control ImageList
        /// </summary>
        public new ImageList ImageList
        {
            get
            {
                if (base.ImageList == null)
                {
                    base.ImageList = new System.Windows.Forms.ImageList();
                }

                return base.ImageList;
            }
            set
            {
                base.ImageList = value;
            }
        }

        /// <summary>
        /// Gets/sets the DesignTimeLayout of the control
        /// </summary>
        public new GridEXLayout DesignTimeLayout
        {
            get
            {
                return base.DesignTimeLayout;
            }
            set
            {
                // After setting the design time layout, the column count, item count and bound mode will be reset to the values specified in the 
                // DesignTimeLayout from the designer
                int columnsCount = this.ColumnsCount;
                int itemCount = this.ItemCount;
                BoundMode boundMode = this.BoundMode;

                base.DesignTimeLayout = value;

                this.BoundMode = boundMode;
                this.ColumnsCount = columnsCount;
                this.ItemCount = itemCount;
                string s = ".frx\":0123";
                string s2 = ".ctx\":0123";

            }
        }

        // TODO:BAN
        public bool Redraw { get; set; }

        // TODO:BAN
        public LockTypeEnum LockType { get; set; }

        #endregion

        #region Constructor
        public FCGridEX()
            : base()
        {
            //By default in VB6, no themed areas are set
            this.ThemedAreas = ThemedArea.None;

            this.printerProperties = new PrinterProperties(this.printDocument);
        }
        #endregion

        #region Overrides
        /// <summary>
        /// The BeforeUpdate event is raised when Adding or Updating a record inside the grid
        /// </summary>
        /// <param name="e"></param>
        protected override void OnAddingRecord(System.ComponentModel.CancelEventArgs e)
        {
            base.OnAddingRecord(e);
            RaiseBeforeUpdate(e);
        }

        /// <summary>
        /// The BeforeUpdate event is raised when Adding or Updating a record inside the grid
        /// </summary>
        /// <param name="e"></param>
        protected override void OnUpdatingRecord(System.ComponentModel.CancelEventArgs e)
        {
            base.OnUpdatingRecord(e);
            RaiseBeforeUpdate(e);
        }

        /// <summary>
        /// The SelectionChanged event is raised when changing the current cell in the grid
        /// This event contains as parameterws the previously current row, previously current column and the value of the previously current column
        /// Here, the Col and Row contain the new values after the selection (the current row and current column)
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCurrentCellChanged(EventArgs e)
        {
            base.OnCurrentCellChanged(e);

            if (SelectionChanged != null)
            {
                object value = null;
                if (lastCol >= 0)
                {
                    try
                    {
                        value = this.GetValue(lastCol);
                    }
                    catch (System.Exception ex)
                    {
                        value = null;
                    }
                }
                SelectionChangedEventArgs args = new SelectionChangedEventArgs(lastCol, lastRow, value);
                // PJ:Error:EndlessLoop
                SelectionChanged(this, args);
            }
        }

        protected override void OnCurrentCellChanging(CurrentCellChangingEventArgs e)
        {
            //In order to maintain the last row and last column (previously current row and previously current column), before the CurrentCellChanging event
            //the Row and Col values are still the old one. Preserve them in order to either pass them as parameters for the SelectionChange event
            //or for the properties LastRow and LastCol
            if (inDataSourceFunc == false)
            {
                // only change the last row if not in Satasource function
                lastRow = this.Row;
                lastCol = this.Col;
            }
            base.OnCurrentCellChanging(e);
        }

        /// <summary>
        /// If the DetectRowDrag is disabled, the event should not be fired
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRowDrag(RowDragEventArgs e)
        {
            if (!DetectRowDrag)
            {
                return;
            }

            base.OnRowDrag(e);
        }

        /// <summary>
        /// If the ReadOnly property is set to true and the EditTextBox is available, the grid should not accept changes
        /// </summary>
        /// <param name="e"></param>
        protected override void OnEditModeChanged(EventArgs e)
        {
            if (this.ReadOnly && this.EditTextBox != null)
            {
                this.EditTextBox.ReadOnly = this.ReadOnly;
            }
            base.OnEditModeChanged(e);
        }

        #endregion

        #region Public methods
        public new void Update()
        {
            if (this.DataSource != null && this.DataSource is DataTable)
            {
                DataTable source = this.DataSource as DataTable;
                source.Update();
            }

            base.Update();
        }

        /// <summary>
        /// Returns true if the row index is of type group header
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        public bool IsGroupItem(int rowIndex)
        {
            return this.GetRow(rowIndex).RowType == RowType.GroupHeader;
        }

        public void RefreshSort()
        {
            // TODO:BAN
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Method used for raising the BeforeUpdate event
        /// </summary>
        /// <param name="e"></param>
        private void RaiseBeforeUpdate(CancelEventArgs e)
        {
            if (BeforeUpdate != null)
            {
                BeforeUpdate(this, e);
            }
        }
        #endregion

        #region DataColumnEqualityComparer
        private class DataColumnEqualityComparer : IEqualityComparer<DataColumn>
        {
            #region IEqualityComparer Members

            private DataColumnEqualityComparer() { }
            public static DataColumnEqualityComparer Instance = new DataColumnEqualityComparer();


            public bool Equals(DataColumn x, DataColumn y)
            {
                if (x.ColumnName != y.ColumnName)
                    return false;
                if (x.DataType != y.DataType)
                    return false;

                return true;
            }

            public int GetHashCode(DataColumn obj)
            {
                int hash = 17;
                hash = 31 * hash + obj.ColumnName.GetHashCode();
                hash = 31 * hash + obj.DataType.GetHashCode();

                return hash;
            }

            #endregion
        }
        #endregion
    }

    #region EventArgs
    public class SelectionChangedEventArgs : EventArgs
    {
        public int LastCol = -1;
        public int LastRow = -1;
        public object Value = null;

        public SelectionChangedEventArgs(int lastCol, int lastRow)
        {
            LastRow = lastRow;
            LastCol = lastCol;
        }

        public SelectionChangedEventArgs(int lastCol, int lastRow, object value)
        {
            LastRow = lastRow;
            LastCol = lastCol;
            Value = value;
        }
    }
    #endregion
}
