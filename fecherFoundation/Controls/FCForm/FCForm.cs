﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using System.Reflection;
using fecherFoundation.Extensions;
using System.Diagnostics.CodeAnalysis;
using Wisej.Core;
using System.Threading;

namespace fecherFoundation
{
    /// <summary>
    /// VB.Form
    /// </summary>
    public partial class FCForm : Form
    {
        #region Public Members

        public List<FCTimer> Timers = new List<FCTimer>();
        public List<Thread> StartedTasks = new List<Thread>();
        public bool ExitingApplication = false;
        public FCMDIForm MdiParentFCForm = null;
        //public Boolean UpdateLocked;
        /// <summary>
        /// flag used if Modal dialogs shouldn't be disposed. Flag is reset to false after form is closed.
        /// </summary>
        public bool AvoidModalDialogDispose = false;
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private Bitmap memoryImage;

        private bool inQueryunload = false;
        private bool enforceActivated = false;
        private FormWindowState internalWindowState = FormWindowState.Normal;
        private bool inWindowState = false;
        //PJ:be sure that the QueryUnload will be fired once, only (before closing the Window and not on the Unload again.
        private bool queryUnloadHandled = false;

        private bool resetBackgroundImage = false;
        private Image oldBackgroundImage = null;

        private const BindingFlags PrivateInstanceFlags = BindingFlags.NonPublic | BindingFlags.Instance;

        private bool moveable = true;

        private bool isFormClosedFromCode = false;

        private bool closingCanceled = false;

        private bool isFormLoaded = false;        

        private float scaleHeight = -1;

        private float scaleWidth = -1;

        private bool isInitializing = false;

        private CoordinateSpace coordinateSpace;

        private GraphicsFactory graphicsFactory;

        private FCGraphics fcGraphics;

        private bool loading;

        //JEI if we have a ModalDialog we have to close it and not only hide it
        private bool isModal = false;

        private bool queryUnloadWasCalled = false;
        private bool exitMdiFormFromX = false;

        //CHE: when calling Unload for some form it is closed unless CloseOnUnload flag is set to false
        private bool closeOnUnload = true;

        private MousePointerConstants mousePointer = MousePointerConstants.vbDefault;

        private bool forceResize = false;
		private bool isUnloaded = false;
        private bool formShown = false;
        private bool EscKeyPressExecuted = false;

        private static FCForm topMostWindow = null;

        #endregion

        #region Constructors

        public FCForm()
        {
            if(!FCGlobal.applicationStarted)
            {
                FCGlobal.ApplicationStart();
            }

            isInitializing = true;
            InitializeComponent();
            isInitializing = false;
            using (Graphics g = GetGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            //CHE: set default value
            this.WhatsThisHelp = false;
            this.Appearance = 1;
            this.LinkMode = LinkModeConstants.VbLinkNone;
            this.ClipControls = true;
            this.NegotiateMenus = true;
            this.AutoRedraw = false;
            this.ScaleMode = ScaleModeConstants.vbTwips;
            this.FontTransparent = true;
            this.Moveable = true;
            this.Picture = null;
            this.FillColor = 0;
            this.FillStyle = FillStyleConstants.vbFSTransparent;
            this.DrawWidth = 1;
            this.DrawStyle = 0;
            this.PaletteMode = PalleteModeConstants.vbPaletteModeHalfTone;
            this.BorderStyleOriginal = BorderStyleConstants.vbSizable;
            this.DisableMergedMenu = true;
        }

        #endregion

        #region Public Delegates

        public delegate void OnQueryUnload(object sender, FCFormClosingEventArgs e);
        public delegate void OnFormUnload(object sender, FCFormClosingEventArgs e);

        #endregion

        #region Public Events

        public event EventHandler<FCFormClosingEventArgs> QueryUnload;
        public event EventHandler<FCFormClosingEventArgs> FormUnload;
        public event EventHandler<EventArgs> FCFormActivate;
        public event EventHandler FCFormDeactivate;
        public event EventHandler<FCLinkExecuteEventArgs> LinkExecute;

        #endregion

        #region Private Events
        #endregion

        #region Enums
        /// <summary>
        /// Constants for a Form's Show method.
        /// </summary>
        public enum FormShowEnum
        {
            /// <summary>
            /// Modeless form.
            /// </summary>
            Modeless = 0,
            /// <summary>
            /// Modal form.
            /// </summary>
            Modal = 1
        }

        public enum BorderStyleConstants
        {
            vbBSNone = 0,
            vbFixedSingle = 1,
            vbSizable = 2,
            vbFixedDouble = 3,
            vbFixedToolWindow = 4,
            vbSizableToolWindow = 5
        }
    
        public enum PalleteModeConstants
        {
            vbPaletteModeHalfTone = 0,
            vbPaletteModeUseZOrder = 1,
            vbPaletteModeCustom = 2,
            vbPaletteModeContainer = 3,
            vbPaletteModeNone = 4,
            vbPaletteModeObject = 5
        }

        public enum LinkModeConstants
        {
            VbLinkNone = 0,
            VbLinkAutomatic = 1,
            VbLinkManual = 2,
            VbLinkNotify = 3
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(false)]
        public bool WhatsThisHelp
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the border style for an object. For the Form object and the TextBox control, read-only at run time.
        /// Syntax
        /// object.BorderStyle = [value]
        /// The BorderStyle property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	A value or constant that determines the border style, as described in Settings.
        /// Settings
        /// The BorderStyle property settings for a Form object are:
        /// Constant	Setting	Description
        /// vbBSNone	0	None (no border or border-related elements).
        /// vbFixedSingle	1	Fixed Single. Can include Control-menu box, title bar, Maximize button, and Minimize button. Resizable only using Maximize and Minimize buttons.
        /// vbSizable	2	(Default) Sizable. Resizable using any of the optional border elements listed for setting 1.
        /// vbFixedDouble	3	Fixed Dialog. Can include Control-menu box and title bar; can't include Maximize or Minimize buttons. Not resizable.
        /// vbFixedToolWindow	4	Fixed ToolWindow. Displays a non-sizable window with a Close button and title bar text in a reduced font size. The form does not appear in the Windows taskbar.
        /// vbSizableToolWindow	5	Sizable ToolWindow. Displays a sizable window with a Close button and title bar text in a reduced font size. The form does not appear in the Windows taskbar.
        /// 
        ///The BorderStyle property settings for MS Flex Grid, Image, Label, OLE container, PictureBox, Frame, and TextBox controls are:
        ///Setting	Description
        ///0	(Default for Image and Label controls) None.
        ///1	(Default for MS Flex Grid, PictureBox, TextBox, and OLE container controls) Fixed Single.
        ///
        ///The BorderStyle property settings for Line and Shape controls are:
        ///Constant	Setting	Description
        ///vbTransparent	0	Transparent
        ///vbBSSolid	1	(Default) Solid. The border is centered on the edge of the shape.
        ///vbBSDash	2	Dash
        ///vbBSDot	3	Dot
        ///vbBSDashDot	4	Dash-dot
        ///vbBSDashDotDot	5	Dash-dot-dot
        ///vbBSInsideSolid	6	Inside solid. The outer edge of the border is the outer edge of the shape.
        ///
        /// Remarks
        /// For a form, the BorderStyle property determines key characteristics that visually identify a form as either a general-purpose window or a dialog box. Setting 3 (Fixed Dialog) is useful for 
        /// standard dialog boxes. Settings 4 (Fixed ToolWindow) and 5 (Sizable ToolWindow) are useful for creating toolbox-style windows.
        /// MDI child forms set to 2 (Sizable) are displayed within the MDI form in a default size defined by the Windows operating environment at run time. For any other setting, the form is displayed 
        /// in the size specified at design time.
        /// Changing the setting of the BorderStyle property of a Form object may change the settings of the MinButton, MaxButton, and ShowInTaskbar properties. When BorderStyle is set to 1 (Fixed Single) 
        /// or 2 (Sizable), the MinButton, MaxButton, and ShowInTaskbar properties are automatically set to True. When BorderStyle is set to 0 (None), 3 (Fixed Dialog), 4 (Fixed ToolWindow), or 5 (Sizable 
        /// ToolWindow), the MinButton, MaxButton, and ShowInTaskbar properties are automatically set to False.
        /// Note   If a form with a menu is set to 3 (Fixed Dialog), it is displayed with a setting 1 (Fixed Single) border instead.
        /// At run time, a form is either modal or modeless, which you specify using the Show method.
        /// </summary>
        [DefaultValue(BorderStyleConstants.vbSizable)]
        public BorderStyleConstants BorderStyleOriginal
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Status, if Shown-Event was executed
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsFormShown
        {
            get
            {
                return this.formShown;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Count
        {
            get
            {
                return this.GetAllControls().Count;
            }
        }

        [DefaultValue(MousePointerConstants.vbDefault)]
        public MousePointerConstants MousePointer
        {
            get
            {
                return mousePointer;
            }
            set
            {
                mousePointer = value;
                this.Cursor = FCUtils.GetTranslatedCursor(mousePointer);
            }
        }

        /// <summary>
        /// Returns or sets an associated context number for an object. Used to provide context-sensitive Help for your application.
        /// </summary>
        public int HelpContextID
        {
            // TODO:CHE
            get;
            set;
        }

        public string LinkTopic
        {
            // TODO:LSO
            get;
            set;
        }
        /// <summary>
        /// CHE: when calling Unload for some form it is closed unless CloseOnUnload flag is set to false
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(true)]
        public bool CloseOnUnload
        {
            get { return closeOnUnload; }
            set { closeOnUnload = value; }
        }

        public new FormWindowState WindowState
        {
            get 
            { 
                return base.WindowState; 
            }
            set 
            {
                internalWindowState = value;
                inWindowState = true;
                base.WindowState = value;
                inWindowState = false;
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                return Convert.ToInt32(base.Width * fcGraphics.TwipsPerPixelX);
            }
            set
            {
                base.Width = Convert.ToInt32(value / fcGraphics.TwipsPerPixelX);
            }
        }

            /// <summary>
            /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
            /// </summary>
            [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
				return Convert.ToInt32(base.Height * fcGraphics.TwipsPerPixelY);
            }
            set
            {
				base.Height = Convert.ToInt32(value / fcGraphics.TwipsPerPixelY);
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
				return Convert.ToInt32(base.Top * fcGraphics.TwipsPerPixelY);
            }
            set
            {
				base.Top = Convert.ToInt32(value / fcGraphics.TwipsPerPixelY);
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                return Convert.ToInt32(base.Left * fcGraphics.TwipsPerPixelX);
            }
            set
            {
                base.Left = Convert.ToInt32(value / fcGraphics.TwipsPerPixelX);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsInitializing
        {
            get
            {
                return isInitializing;
            }
            set
            {
                isInitializing = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsLoaded
        {
            get
            {
                return this.isFormLoaded;
            }

        }

        /// <summary>
        /// Returns or sets the font used to display text in a control or in a run-time drawing or printing operation.
        /// Note   The FontName property is included for use with the CommonDialog control and for compatibility with earlier versions of Visual Basic. For additional functionality, 
        /// use the new Font object properties (not available for the CommonDialog control).
        /// Syntax
        /// object.FontName [= font]
        /// The FontName property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// font	A string expression specifying the font name to use.
        /// Remarks
        /// The default for this property is determined by the system. Fonts available with Visual Basic vary depending on your system configuration, display devices, and printing 
        /// devices. Font-related properties can be set only to values for which fonts exist.
        /// In general, you should change FontName before setting size and style attributes with the FontSize, FontBold, FontItalic, FontStrikethru, and FontUnderline properties.
        /// Note   At run time, you can get information on fonts available to the system through the FontCount and Fonts properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
                this.graphicsFactory.FontName = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        public new Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
                graphicsFactory.Font = value;

                //CHE: when adding a control to the form it inherits the font bold of the parent form
                if (this.Font.Bold)
                {
                    foreach (Control ctrl in this.Controls)
                    {
                        if (ctrl.Font == this.Font)
                        {
                            ctrl.SetFontBold(this.Font.Bold);
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
                this.graphicsFactory.FontBold = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                this.SetFontItalic(value);
                this.graphicsFactory.FontItalic = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                this.SetFontUnderline(value);
                this.graphicsFactory.FontUnderline = value;
            }
        }

        /// <summary>
        /// Returns or sets the paint style of controls on an MDIForm or Form object at design time. Read-only at run time.
        /// 0	Flat. Paints controls and forms without visual effects.
        /// 1	(Default) 3D. Paints controls with three-dimensional effects.
        /// Remarks
        /// If set to 1 at design time, the Appearance property draws controls with three-dimensional effects. If the form's BorderStyle property is set to Fixed Double (vbFixedDouble, or 3), the caption 
        /// and border of the form are also painted with three-dimensional effects. Setting the Appearance property to 1 also causes the form and its controls to have their BackColor property set to the 
        /// color selected for 3D Objects in the Appearance tab of the operating system's Display Properties dialog box.
        /// Setting the Appearance property to 1 for an MDIForm object affects only the MDI parent form. To have three-dimensional effects on MDI child forms, you must set each child form's Appearance 
        /// property to 1.
        /// </summary>
        [DefaultValue(1)]
        public int Appearance
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the type of link used for a DDEconversation and activates the connection as follows:
        /// Control Allows a destination control on a Visual Basic form to initiate a conversation, as specified by the control's LinkTopic and LinkItem properties.
        /// Form Allows a destination application to initiate a conversation with a Visual Basic source form, as specified by the destination application's application|topic!item expression.
        /// Syntax
        /// object.LinkMode [= number]
        /// The LinkMode property syntax has these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Number	An integer that specifies the type of connection, as described in Settings.
        /// Settings
        /// For controls used as destinations in DDE conversations, the settings for number are:
        /// Constant	Setting	Description
        /// VbLinkNone	0	(Default) None No DDE interaction.
        /// VbLinkAutomatic	1	Automatic Destination control is updated each time the linked data changes.
        /// VbLinkManual	2	Manual {Destination control is updated only when the LinkRequest method is invoked.
        /// VbLinkNotify	3	Notify A LinkNotify event occurs whenever the linked data changes, but the destination control is updated only when the LinkRequest method is invoked.
        /// For forms used as sources in DDE conversations, the settings for number are:
        /// Constant	Setting	Description
        /// VbLinkNone	0	(Default) None No DDE interaction. No destination application can initiate a conversation with the source form as the topic, and no application can poke data to the form. 
        /// If LinkMode is 0 (None) at design time, you can't change it to 1 (Source) at run time.
        /// VbLinkSource	1	Source Allows any Label, PictureBox, or TextBox control on a form to supply data to any destination application that establishes a DDE conversation with the form. If such a 
        /// link exists, Visual Basic automatically notifies the destination whenever the contents of a control are changed. In addition, a destination application can poke data to any Label, PictureBox, 
        /// or TextBox control on the form. If LinkMode is 1 (Source) at design time, you can change it to 0 (None) and back at run time.
        /// Remarks
        /// The following conditions also apply to the LinkMode property:
        /// Setting LinkMode to a nonzero value for a destination control causes Visual Basic to attempt to initiate the conversation specified in the LinkTopic and LinkItem properties. The source updates 
        /// the destination control according to the type of link specified (automatic, manual, or notify).
        /// If a source application terminates a conversation with a Visual Basic destination control, the value for that control's LinkMode setting changes to 0 (None).
        /// If you leave LinkMode for a form set to the default 0 (None) at design time, you can't change LinkMode at run time. If you want a form to act as a source, you must set LinkMode to 1 (Source) 
        /// at design time. You can then change the value of LinkMode at run time.
        /// Note   Setting a permanent data link at design time with the Paste Link command from the Edit menu also sets the LinkMode, LinkTopic, and LinkItem properties. This creates a link that is saved 
        /// with the form. Each time the form is loaded, Visual Basic attempts to re-establish the conversation.
        /// </summary>
        [DefaultValue(LinkModeConstants.VbLinkNone)]
        public LinkModeConstants LinkMode
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that determines whether graphics methods in Paint events repaint the entire object or only newly exposed areas. Also determines whether the Microsoft Windows 
        /// operating environment creates a clipping region that excludes nongraphical controls contained by the object. Read-only at run time.
        /// object.ClipControls
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Boolean	A Boolean expression that specifies how objects are repainted, as described in Settings.Settings
        /// True	(Default) Graphics methods in Paint events repaint the entire object. A clipping region is created around nongraphical controls on the form before a Paint event.
        /// False	Graphics methods in Paint events repaint only newly exposed areas. A clipping region isnt created around nongraphical controls before a Paint event. Complex forms usually load and 
        /// repaint faster when ClipControls is set to False.
        /// Remarks
        /// Clipping is the process of determining which parts of a form or container, such as a Frame or PictureBox control, are painted when the form is displayed. An outline of the form and controls 
        /// is created in memory. The Windows operating environment uses this outline to paint some parts, such as the background, without affecting other parts, such as the contents of a TextBox control. 
        /// Because the clipping region is created in memory, setting this property to False can reduce the time needed to paint or repaint a form.
        /// The clipping region includes most controls, but doesn't clip around the Image, Label, Line, or Shape controls.
        /// Avoid nesting intrinsic controls with ClipControls set to True inside a control with ClipControls set to False (for instance, a command button inside a picture box). This kind of control nesting 
        /// causes the controls to repaint incorrectly. To fix this problem, set the ClipControls property for both the container control and the nested controls to True.
        /// </summary>
        [DefaultValue(true)]
        public bool ClipControls
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Sets a value that determines whether or not a form incorporates the menus from an object on the form on the form's menu bar. Not available at run time.
        /// Settings
        /// The NegotiateMenus property has these settings:
        /// Setting	Description
        /// True	(Default) When an object on the form is active for editing, the menus of that object are displayed on the form's menu bar.
        /// False	Menus of objects on the form aren't displayed on the form's menu bar.
        /// Remarks
        /// Using the NegotiateMenus property, you determine if the menu bar of a form will share (or negotiate) space with the menus of an active object on the form. If you don't want 
        /// to include the menus of the active object on the menu bar of your form, set NegotiateMenus to False.
        /// You can't negotiate menus between an MDIForm object and an object on the MDIForm.
        /// If NegotiateMenus is set to True, the form must have a menu bar defined, even if the menu bar isn't visible. If the MDIChild property of the form is set to True, the menus of 
        /// the active object are displayed on the menu bar of the MDI parent window (MDIForm object).
        /// When NegotiateMenus is set to True, you can use the NegotiatePosition property of individual Menu controls to determine the menus that your form displays along with the menus 
        /// of the active object.
        /// </summary>
        [DefaultValue(true)]
        public bool NegotiateMenus
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the output from a graphics method to a persistent graphic.
        /// object.AutoRedraw [= boolean]
        /// Boolean	A Boolean expression that specifies how the object is repainted, as described in Settings.
        /// True	Enables automatic repainting of a Form object or PictureBox control. Graphics and text are written 
        /// to the screen and to an image stored in memory. The object doesn't receive Paint events; it's repainted when 
        /// necessary, using the image stored in memory.
        /// False	(Default) Disables automatic repainting of an object and writes graphics or text only to the screen. 
        /// Visual Basic invokes the object's Paint event when necessary to repaint the object.
        /// Remarks
        /// This property is central to working with the following graphics methods: Circle, Cls, Line, Point, Print, and PSet. 
        /// Setting AutoRedraw to True automatically redraws the output from these methods in a Form object or PictureBox control
        /// when, for example, the object is resized or redisplayed after being hidden by another object.
        /// You can set AutoRedraw in code at run time to alternate between drawing persistent graphics (such as a background or 
        /// grid) and temporary graphics. If you set AutoRedraw to False, previous output becomes part of the background screen. 
        /// When AutoRedraw is set to False, background graphics aren't deleted if you clear the drawing area with the Cls method.
        /// Setting AutoRedraw back to True and then using Cls clears the background graphics.
        /// Note   If you set the BackColor property, all graphics and text, including the persistent graphic, are erased. 
        /// In general, all graphics should be displayed using the Paint event unless AutoRedraw is set to True.
        /// To retrieve the persistent graphic created when AutoRedraw is set to True, use the Image property. To pass the 
        /// persistent graphic to a Windows API when AutoRedraw is set to True, use the object's hDC property.
        /// If you set a form's AutoRedraw property to False and then minimize the form, the ScaleHeight and ScaleWidth properties
        /// are set to icon size. When AutoRedraw is set to True, ScaleHeight and ScaleWidth remain the size of the restored window.
        /// If AutoRedraw is set to False, the Print method will print on top of graphical controls such as the Image and Shape controls.
        /// </summary>
        [DefaultValue(false)]
        public bool AutoRedraw
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns a handle to a persistent graphic; the handle is provided by the Microsoft Windows operating environment.
        /// Remarks
        /// An object's AutoRedraw property determines whether the repainting of an object occurs with a persistent graphics 
        /// or through Paint events. The Windows operating environment identifies an object's persistent graphic by assigning a 
        /// handle to it; you can use the Image property to get this handle.
        /// An Image value exists regardless of the setting for the AutoRedraw property. If AutoRedraw is True and nothing has 
        /// been drawn, the image displays only the color set by the BackColor property and the picture.
        /// You can assign the value of Image to the Picture property. The Image property also provides a value to pass to 
        /// Windows API calls.
        /// The Image, DragIcon, and Picture properties are normally used when assigning values to other properties, when saving 
        /// with the SavePicture statement, or when placing something on the Clipboard. You can't assign these to a temporary 
        /// variable, other than the Picture data type.
        /// The AutoRedraw property can cause Image, which is a handle to a bitmap, to change. When AutoRedraw is True, 
        /// an object's hDC property becomes a handle to a device context that contains the bitmap returned by Image.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Image Image
        {
            get
            {
                return base.BackgroundImage;
            }
            set
            {
                base.BackgroundImage = value;
            }

        }

        /// <summary>
        /// Gets/sets a value that determines the units of measure for object coordinates when using graphics methods or controls are positioned.
        /// Obsolete <see cref="http://msdn.microsoft.com/en-us/library/fhk6kcce(v=vs.71).aspx"/>
        /// </summary>
        [DefaultValue(ScaleModeConstants.vbTwips)]
        public ScaleModeConstants ScaleMode
        {
            get
            {
                return fcGraphics.ScaleMode;
            }
            set
            {
                fcGraphics.ScaleMode = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether background text and graphics on a Form or Printer object or a PictureBox control are displayed in the spaces around characters.
        /// Syntax
        /// object.FontTransparent [= boolean]
        /// The FontTransparent property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// boolean	A Boolean expression specifying the state of background text and graphics, as described in Settings.
        /// Settings
        /// The settings for boolean are:
        /// Setting	Description
        /// True	(Default) Permits background graphics and text to show around the spaces of the characters in a font.
        /// False	Masks existing background graphics and text around the characters of a font.
        /// Remarks
        /// Set FontTransparent at design time using the Properties window or at run time using code. Changing FontTransparent at run time doesn't affect graphics and text already drawn to Form, 
        /// Printer, or PictureBox.
        /// </summary>
        [DefaultValue(true)]
        public bool FontTransparent
        {
            get
            {
                return this.graphicsFactory.FontTransparent;
            }
            set
            {
                // TODO:CHE
                this.graphicsFactory.FontTransparent = value;
            }
        }

        /// <summary>
        /// Gets/sets a value that determines whether a form can be moved.
        /// </summary>
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool Moveable
        {
            get
            {
                return this.moveable;
            }
            set
            {
                this.moveable = value;
            }
        }

        /// <summary>
        /// Gets/sets the icon of the form
        /// </summary>
        [DefaultValue(null)]
        public Image Picture
        {
            // todo check
            get
            {
                return this.Icon;
            }
            set
            {
                // Set Icon
                this.Icon = value;
            }
        }

        /// <summary>
        /// Returns or sets the color used to fill in shapes; FillColor is also used to fill in circles and boxes created with the Circle and Line graphics methods.
        /// Syntax
        /// object.FillColor [ = value]
        /// The FillColor property syntax has these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A value or constant that determines the fill color, as described in Settings.
        /// Settings
        /// The settings for value are:
        /// Setting	Description
        /// Normal RGB colors	Colors set with the RGB or QBColor functions in code.
        /// System default colors	Colors specified with the system color constants in the Visual Basic (VB) object library in the Object Browser. The Microsoft Windows operating environment substitutes 
        /// the user's choices, as specified by the user's Control Panel settings.
        /// By default, FillColor is set to 0 (Black).
        /// Remarks
        /// Except for the Form object, when the FillStyle property is set to its default, 1 (Transparent), the FillColor setting is ignored.
        /// </summary>
        [DefaultValue(0)]
        public int FillColor
        {
            get
            {
                return this.graphicsFactory.FillColor;
            }
            set
            {
                this.graphicsFactory.FillColor = value;
            }
        }

        /// <summary>
        /// Returns or sets the pattern used to fill Shape controls as well as circles and boxes created with the Circle and Line graphics methods.
        /// Syntax
        /// object.FillStyle [= number]
        /// The FillStyle property syntax has these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Number	An integer that specifies the fill style, as described in Settings.
        /// Settings
        /// The number settings are:
        /// Constant	Setting	Description
        /// VbFSSolid	0	Solid
        /// VbFSTransparent	1	(Default) Transparent
        /// VbHorizontalLine	2	Horizontal Line
        /// VbVerticalLine	3	Vertical Line
        /// VbUpwardDiagonal	4	Upward Diagonal
        /// VbDownwardDiagonal	5	Downward Diagonal
        /// VbCross	6	Cross
        /// VbDiagonalCross	7	Diagonal Cross
        /// Remarks
        /// When FillStyle is set to 1 (Transparent), the FillColor property is ignored, except for the Form object.
        /// </summary>
        [DefaultValue(FillStyleConstants.vbFSTransparent)]
        public FillStyleConstants FillStyle
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the line width for output from graphics methods.
        /// object.DrawWidth [= size]
        /// size	A numeric expression from 1 through 32,767. This value represents the width of the line in pixels. The default is 1; that is, 1 pixel wide.
        /// Remarks
        /// Increase the value of this property to increase the width of the line. If the DrawWidth property setting is greater than 1, DrawStyle property settings 1 through 4 produce a solid line 
        /// (the DrawStyle property value isn't changed). Setting DrawWidth to 1 allows DrawStyle to produce the results shown in the DrawStyle property table.
        /// </summary>
        [DefaultValue(1)]
        public short DrawWidth
        {
            get
            {
                return graphicsFactory.DrawWidth;
            }
            set
            {
                graphicsFactory.DrawWidth = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines the line style for output from graphics methods.
        /// Syntax
        /// object.DrawStyle [= number]
        /// The DrawStyle property syntax has these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Number	An integer that specifies line style, as described in Settings.
        /// Settings
        /// The settings for number are:
        /// Constant	Setting	Description
        /// VbSolid	0	(Default) Solid
        /// VbDash	1	Dash
        /// VbDot	2	Dot
        /// VbDashDot	3	Dash-Dot
        /// VbDashDotDot	4	Dash-Dot-Dot
        /// VbInvisible	5	Transparent
        /// VbInsideSolid	6	Inside Solid
        /// Remarks
        /// If DrawWidth is set to a value greater than 1, DrawStyle settings 1 through 4 produce a solid line (the DrawStyle property value isn't changed). If DrawWidth is set to 1, DrawStyle produces 
        /// the effect described in the preceding table for each setting.
        /// </summary>
        [DefaultValue(0)]
        public short DrawStyle
        {
            get
            {
                return graphicsFactory.DrawStyle;
            }
            set
            {
                graphicsFactory.DrawStyle = value;
            }
        }

        /// <summary>
        /// Returns the horizontal coordinates for the next printing or drawing method or sets.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float CurrentX
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the vertical coordinates for the next printing or drawing method or sets.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float CurrentY
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that determines which palette to use for the controls on a object.
        /// vbPaletteModeHalfTone	0	(Default) Use the Halftone palette.
        /// vbPaletteModeUseZOrder	1	Use the palette from the topmost control that has a palette.
        /// vbPaletteModeCustom	2	Use the palette specified in the Palette property.
        /// vbPaletteModeContainer	3	Use the container's palette for containers that support ambient Palette property. Applies to UserControls only.
        /// vbPaletteModeNone	4	Do not use any palette. Applies to UserControls only.
        /// vbPaletteModeObject	5	Use the ActiveX designers palette. (Applies only to ActiveX designers which contain a palette.)
        /// Remarks
        /// If no palette is available, the halftone palette becomes the default palette.
        /// </summary>
        [DefaultValue(PalleteModeConstants.vbPaletteModeHalfTone)]
        public PalleteModeConstants PaletteMode
        {
            // TODO:CHE
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool LockControls
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns a handle provided by the Microsoft Windows operating environment to the device context of an object.
        /// Remarks
        /// This property is a Windows operating environment device context handle. The Windows operating environment manages the system display by assigning a device context for the Printer object and 
        /// for each form and PictureBox control in your application. You can use the hDC property to refer to the handle for an object's device context. This provides a value to pass to Windows API calls.
        /// With a CommonDialog control, this property returns a device context for the printer selected in the Print dialog box when the cdlReturnDC flag is set or an information context when the cdlReturnIC 
        /// flag is set.
        /// Note   The value of the hDC property can change while a program is running, so don't store the value in a variable; instead, use the hDC property each time you need it.
        /// The AutoRedraw property can cause the hDC property setting to change. If AutoRedraw is set to True for a form or PictureBox container, hDC acts as a handle to the device context of the persistent 
        /// graphic (equivalent to the Image property). When AutoRedraw is False, hDC is the actual hDC value of the Form window or the PictureBox container. The hDC property setting may change while the 
        /// program is running regardless of the AutoRedraw setting.
        /// If the HasDC property is set to False, a new device context will be created by the system and the value of the hDC property will change each time it is called.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Not supported.")]
        public int hDC
        {
            get
            {
                return this.GetGraphics().GetHdc().ToInt32();
            }
        }

        /// <summary>
        /// Return or set the number of units for the horizontal (ScaleWidth) and vertical (ScaleHeight) measurement of the interior of an object when using graphics methods or when positioning controls. 
        /// For MDIForm objects, not available at design time and read-only at run time.
        /// Syntax
        /// object.ScaleHeight [= value]
        /// object.ScaleWidth [= value]
        /// The ScaleHeight and ScaleWidth property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical measurement.
        /// Remarks
        /// You can use these properties to create a custom coordinate scale for drawing or printing. For example, the statement ScaleHeight = 100 changes the units of measure of the actual interior height 
        /// of the form. Instead of the height being n current units (twips, pixels, ...), the height will be 100 user-defined units. Therefore, a distance of 50 units is half the height/width of the object, 
        /// and a distance of 101 units will be off the object by 1 unit.
        /// Use the ScaleMode property to define a scale based on a standard unit of measurement, such as twips, points, pixels, characters, inches, millimeters, or centimeters.
        /// Setting these properties to positive values makes coordinates increase from top to bottom and left to right. Setting them to negative values makes coordinates increase from bottom to top and 
        /// right to left.
        /// Using these properties and the related ScaleLeft and ScaleTop properties, you can set up a full coordinate system with both positive and negative coordinates. All four of these Scale properties 
        /// interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting ScaleMode to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. In addition, the CurrentX and CurrentY 
        /// settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleHeight and ScaleWidth properties aren't the same as the Height and Width properties.
        /// For MDIForm objects, ScaleHeight and ScaleWidth refer only to the area not covered by PictureBox controls in the form. Avoid using these properties to size a PictureBox in the Resize event of 
        /// an MDIForm.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ScaleWidth
        {
            get
            {
                //using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.GetRectangle(), 0, 0);
                    return fcGraphics.ScaleWidth;
                }
            }
            set
            {
                //using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.GetRectangle(), 0, 0);
                    fcGraphics.ScaleWidth = value;
                }
            }
        }

        /// <summary>
        /// Return or set the number of units for the horizontal (ScaleWidth) and vertical (ScaleHeight) measurement of the interior of an object when using graphics methods or when positioning controls. 
        /// For MDIForm objects, not available at design time and read-only at run time.
        /// Syntax
        /// object.ScaleHeight [= value]
        /// object.ScaleWidth [= value]
        /// The ScaleHeight and ScaleWidth property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical measurement.
        /// Remarks
        /// You can use these properties to create a custom coordinate scale for drawing or printing. For example, the statement ScaleHeight = 100 changes the units of measure of the actual interior height 
        /// of the form. Instead of the height being n current units (twips, pixels, ...), the height will be 100 user-defined units. Therefore, a distance of 50 units is half the height/width of the object, 
        /// and a distance of 101 units will be off the object by 1 unit.
        /// Use the ScaleMode property to define a scale based on a standard unit of measurement, such as twips, points, pixels, characters, inches, millimeters, or centimeters.
        /// Setting these properties to positive values makes coordinates increase from top to bottom and left to right. Setting them to negative values makes coordinates increase from bottom to top and 
        /// right to left.
        /// Using these properties and the related ScaleLeft and ScaleTop properties, you can set up a full coordinate system with both positive and negative coordinates. All four of these Scale properties 
        /// interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting ScaleMode to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. In addition, the CurrentX and CurrentY 
        /// settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleHeight and ScaleWidth properties aren't the same as the Height and Width properties.
        /// For MDIForm objects, ScaleHeight and ScaleWidth refer only to the area not covered by PictureBox controls in the form. Avoid using these properties to size a PictureBox in the Resize event of 
        /// an MDIForm.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ScaleHeight
        {
            get
            {
                //using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.GetRectangle(), 0, 0);
					return fcGraphics.ScaleHeight - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.GetRectangle(), 0, 0);
					fcGraphics.ScaleHeight = value + FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
        }

        /// <summary>
        /// Return or set the horizontal (ScaleLeft) and vertical (ScaleTop) coordinates for the left and top edges of an object when using graphics methods or when positioning controls.
        /// Syntax
        /// object.ScaleLeft [= value]
        /// object.ScaleTop [= value]
        /// The ScaleLeft and ScaleTop property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical coordinate. The default is 0.
        /// Remarks
        /// Using these properties and the related ScaleHeight and ScaleWidth properties, you can set up a full coordinate system with both positive and negative coordinates. These four 
        /// Scale properties interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting the ScaleMode property to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. The CurrentX 
        /// and CurrentY property settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleLeft and ScaleTop properties aren't the same as the Left and Top properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ScaleLeft
        {
            get
            {
                //using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.GetRectangle(), 0, 0);
                    return fcGraphics.ScaleLeft;
                }
            }
            set
            {
                //using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.GetRectangle(), 0, 0);
                    fcGraphics.ScaleLeft = value;
                }
            }
        }

        /// <summary>
        /// Return or set the horizontal (ScaleLeft) and vertical (ScaleTop) coordinates for the left and top edges of an object when using graphics methods or when positioning controls.
        /// Syntax
        /// object.ScaleLeft [= value]
        /// object.ScaleTop [= value]
        /// The ScaleLeft and ScaleTop property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical coordinate. The default is 0.
        /// Remarks
        /// Using these properties and the related ScaleHeight and ScaleWidth properties, you can set up a full coordinate system with both positive and negative coordinates. These four 
        /// Scale properties interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting the ScaleMode property to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. The CurrentX 
        /// and CurrentY property settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleLeft and ScaleTop properties aren't the same as the Left and Top properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ScaleTop
        {
            get
            {
                //using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.GetRectangle(), 0, 0);
                    return fcGraphics.ScaleTop;
                }
            }
            set
            {
                //using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.GetRectangle(), 0, 0);
                    fcGraphics.ScaleTop = value;
                }
            }
        }


        #endregion

        #region Internal Properties

        internal int FormTwipsPerPixelX
        {
            get
            {
                return Convert.ToInt32(fcGraphics.TwipsPerPixelX);
            }
        }

        internal int FormTwipsPerPixelY
        {
            get
            {
                return Convert.ToInt32(fcGraphics.TwipsPerPixelY);
            }
        }

        #endregion

        #region Public Methods

        public float ScaleY(float value, ScaleModeConstants fromScale, ScaleModeConstants toScale)
        {
            //using (Graphics g = GetGraphics())
            {
                fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                return (float)fcGraphics.ScaleY(value, fromScale, toScale);
            }
        }

        /// <summary>
        /// Prints text to a page.
        /// </summary>
        /// <param name="args">A parameter array containing optional printing parameters.</param>
        public void Print(params object[] args)
        {
            using (Graphics g = GetGraphics())
            {
                float currentX = CurrentX, currentY = CurrentY;
                fcGraphics.Output(g, this.Bounds, ref currentX, ref currentY, this, true, args);
                CurrentX = currentX;
                CurrentY = currentY;
            }
        }

		/// <summary>
		/// In Visual Basic 6.0, used to force the Validate event for the control that has focus when a form is closed.
		/// </summary>
		public void ValidateControls()
		{
			this.Validate();
		}

        /// <summary>
        /// Returns the width of a text string as it would be printed in the current font of a Form, PictureBox, or Printer. Doesn't support named arguments.
        /// Syntax
        /// object.TextWidth(string)
        /// The TextWidth method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// String	Required. A string expression that evaluates to a string for which the text width is determined. Parentheses must surround the string expression.
        /// Remarks
        /// The width is expressed in terms of the ScaleMode property setting or Scale method coordinate system in effect for object. Use TextWidth to determine the amount 
        /// of horizontal space required to display the text. If string contains embedded carriage returns, TextWidth returns the width of the longest line.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public float TextWidth(string text)
        {
            using (Graphics g = GetGraphics())
            {
                return fcGraphics.TextWidth(g, text, this.ClientRectangle);
            }
        }

        /// <summary>
        /// Returns the height of a text string as it would be printed in the current font of a Form, PictureBox, or Printer. Doesn't support named arguments.
        /// Syntax
        /// object.TextHeight(string)
        /// The TextHeight method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the Form object with the focus is assumed to be object.
        /// String	Required. A string expression that evaluates to a string for which the text height is determined. Parentheses must enclose the string expression.
        /// Remarks
        /// The height is expressed in terms of the ScaleMode property setting or Scale method coordinate system in effect for object. Use TextHeight to determine the amount of 
        /// vertical space required to display the text. The height returned includes the normal leading space above and below the text, so you can use the height to calculate 
        /// and position multiple lines of text within object.
        /// If string contains embedded carriage returns, TextHeight returns the cumulative height of the lines, including the leading space above and below each line.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public float TextHeight(string text)
        {
            using (Graphics g = GetGraphics())
            {
                return fcGraphics.TextHeight(g, text, this.ClientRectangle);
            }
        }

        /// <summary>
        /// Sends a bit-by-bit image of a Form object to the printer.
        /// </summary>
        public void PrintForm()
        {
            //FC:FINAL:SBE - Harris #i1368 - print div on client side. function PrintDiv(divId) defined in Main project (Platform/CustomFunctions.js)
            var controlId = "id_" + this.Handle.ToString();
            Eval("PrintDiv('" + controlId + "')");
            //CaptureScreen();
            //System.Drawing.Printing.PrintDocument printDocument1 = new System.Drawing.Printing.PrintDocument();
            //printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument1_PrintPage);
            //printDocument1.Print();
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the starting coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed.</param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed.</param><param name="relativeEnd">Boolean. If this parameter is set to true, the ending coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. you cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public void Line(bool relativeStart, double x1, double y1, bool relativeEnd, double x2, double y2, int color = -1, bool box = false, bool fill = false)
        {
            using (Graphics g = GetGraphics())
            {
                float currentX = CurrentX;
                float currentY = CurrentY;
                fcGraphics.LineInternal(g, this.Bounds, ref currentX, ref currentY, relativeStart, (float)x1, (float)y1, relativeEnd, (float)x2, (float)y2, color, box, fill);
                CurrentX = currentX;
                CurrentY = currentY;
            }
        }

        /// <summary>
        /// Prints lines on a page.
        /// </summary>
        /// <param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> property values.</param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> property values.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(double x2, double y2)
        {
            this.Line(true, 0.0f, 0.0f, false, x2, y2, -1, false, false);
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed. </param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed. </param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. You cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(double x1, double y1, double x2, double y2, int color = -1, bool box = false, bool fill = false)
        {
            this.Line(false, x1, y1, false, x2, y2, color, box, fill);
        }


        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        /// <param name="Rectangle"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, Color Color, LineDrawConstants Rectangle)
        {
            this.Line(StepPos1 == LineDrawStep.Absolute ? false : true, X1, Y1, StepPos2 == LineDrawStep.Absolute ? false : true, X2, Y2, System.Drawing.ColorTranslator.ToOle(Color), Rectangle == LineDrawConstants.B || Rectangle == LineDrawConstants.BF, Rectangle == LineDrawConstants.BF);      
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        /// <param name="Rectangle"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, int Color, LineDrawConstants Rectangle)
        {
            this.Line(StepPos1, X1, Y1, StepPos2, X2, Y2, ColorTranslator.FromOle(Color), Rectangle);
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2)
        {
            Color Color = default(Color);
            Line(StepPos1, X1, Y1, StepPos2, X2, Y2, Color);
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, Color Color)
        {
            LineDrawConstants Rectangle = LineDrawConstants.L;
            Line(StepPos1, X1, Y1, StepPos2, X2, Y2, Color, Rectangle);
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, int Color)
        {
            this.Line(StepPos1, X1, Y1, StepPos2, X2, Y2, ColorTranslator.FromOle(Color));
        }

        /// <summary>
        /// Draws a circle, ellipse, or arc on an object.
        /// Syntax
        /// object.Circle [Step] (x, y), radius, [color, start, end, aspect]
        /// The Circle method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the center of the circle, ellipse, or arc is relative to the current coordinates given by the CurrentX and CurrentY 
        /// properties of object.
        /// (x, y)	Required. Single values indicating the coordinates for the center point of the circle, ellipse, or arc. The ScaleMode property of object determines the 
        /// units of measure used.
        /// radius	Required. Single value indicating the radius of the circle, ellipse, or arc. The ScaleMode property of object determines the unit of measure used.
        /// color	Optional. Long integer value indicating the RGB color of the circle's outline. If omitted, the value of the ForeColor property is used. You can use the 
        /// RGB function or QBColor function to specify the color.
        /// start, end	Optional. Single-precision values. When an arc or a partial circle or ellipse is drawn, start and end specify (in radians) the beginning and end positions 
        /// of the arc. The range for both is 2 pi radians to 2 pi radians. The default value for start is 0 radians; the default for end is 2 * pi radians.
        /// aspect	Optional. Single-precision value indicating the aspect ratio of the circle. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.
        /// Remarks
        /// To fill a circle, set the FillColor and FillStyle properties of the object on which the circle or ellipse is drawn. Only a closed figure can be filled. Closed figures 
        /// include circles, ellipses, or pie slices (arcs with radius lines drawn at both ends).
        /// When drawing a partial circle or ellipse, if start is negative, Circle draws a radius to start, and treats the angle as positive; if end is negative, Circle draws a radius 
        /// to end and treats the angle as positive. The Circle method always draws in a counter-clockwise (positive) direction.
        /// The width of the line used to draw the circle, ellipse, or arc depends on the setting of the DrawWidth property. The way the circle is drawn on the background depends on 
        /// the setting of the DrawMode and DrawStyle properties.
        /// When drawing pie slices, to draw a radius to angle 0 (giving a horizontal line segment to the right), specify a very small negative value for start, rather than zero.
        /// You can omit an argument in the middle of the syntax, but you must include the argument's comma before including the next argument. If you omit an optional argument, 
        /// omit the comma following the last argument you specify.
        /// When Circle executes, the CurrentX and CurrentY properties are set to the center point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="step1"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="radius"></param>
        /// <param name="color"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="aspect"></param>
        public void Circle(LineDrawStep step1, float x, float y, float radius, Color color, float start, float end, float aspect)
        {
            this.Circle(step1 == LineDrawStep.Relative, x, y, radius, ColorTranslator.ToOle(color), start, end, aspect);
        }

        /// <summary>
        /// Prints a circle, an ellipse, or an arc on a page.
        /// </summary>
        /// <param name="x">Single value indicating the horizontal coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="y">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Circle(double x, double y, double radius, int color = -1, double startAngle = float.NaN, double endAngle = float.NaN, double aspect = 1f)
        {
            this.Circle(false, x, y, radius, color, startAngle, endAngle, aspect);
        }

        /// <summary>
        /// Prints a circle, an ellipse, or an arc on a page, specifying whether the center point is relative to the current location.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the center of the circle, ellipse, or arc is printed relative to the coordinates specified in the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the object.</param><param name="x">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="y">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startangle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        public void Circle(bool relativeStart, double x, double y, double radius, int color = -1, double startAngle = float.NaN, double endAngle = float.NaN, double aspect = 1f)
        {
            using (Graphics g = GetGraphics())
            {
                fcGraphics.Circle(g, relativeStart, (float)x, (float)y, (float)radius, color, (float)startAngle, (float)endAngle, (float)aspect);
            }
        }

        /// <summary>
        /// Clears graphics and text generated at run time from a Form or PictureBox.
        /// Remarks
        /// Cls clears text and graphics generated at run time by graphics and printing statements. Background bitmaps set using the Picture property and controls placed on a Form at design time 
        /// aren't affected by Cls. Graphics and text placed on a Form or PictureBox while the AutoRedraw property is set to True aren't affected if AutoRedraw is set to False before Cls is invoked. 
        /// That is, you can maintain text and graphics on a Form or PictureBox by manipulating the AutoRedraw property of the object you're working with.
        /// After Cls is invoked, the CurrentX and CurrentY properties of object are reset to 0.
        /// </summary>
        /// <param name="control"></param>
        public void Cls()
        {
            using (Graphics g = GetGraphics())
            {
                g.Clear(this.BackColor);
            }
            CurrentX = 0;
            CurrentY = 0;
        }

        /// <summary>
        /// get Graphics
        /// </summary>
        /// <returns></returns>
        public Graphics GetGraphics()
        {
            //JEI:HARRIS
            return base.CreateGraphics();

            if (this.BackgroundImage == null || resetBackgroundImage)
            {
                resetBackgroundImage = false;
                oldBackgroundImage = this.BackgroundImage;
                if (this.Width !=0  || this.Height !=0)
                {
                    this.BackgroundImage = new Bitmap(this.Width, this.Height);
                }
            }
            Graphics graphic = null;

            if (this.BackgroundImage.PixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppArgb || this.Image.PixelFormat == System.Drawing.Imaging.PixelFormat.Format24bppRgb)
            {
                graphic = Graphics.FromImage(this.BackgroundImage);
            }
            else
            {
                if (this.Width != 0 || this.Height != 0)
                {
                    Bitmap newBitmap = new Bitmap(this.Width, this.Height);
                    graphic = Graphics.FromImage(newBitmap);
                    graphic.DrawImage(this.BackgroundImage, 0, 0);
                }
            }

            //CHE: draw previous image, otherwise any previous Print will be lost
            if (oldBackgroundImage != null)
            {
                graphic.DrawImage(oldBackgroundImage, 0, 0);
                oldBackgroundImage = null;
            }

            graphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;

            //try
            //{
            //    graphic = Graphics.FromImage(this.BackgroundImage);
            //}
            //catch (Exception ex)
            //{
            //    //ex.Message = "A Graphics object cannot be created from an image that has an indexed pixel format."
            //    Bitmap newBitmap = new Bitmap(this.Width, this.Height);
            //    graphic = Graphics.FromImage(newBitmap);
            //    graphic.DrawImage(this.BackgroundImage, 0, 0);
            //}
            //finally
            //{
            //    graphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
            //}
            return graphic;
        }

        /// <summary>
        /// Prints a single point in a specified color on a page, optionally specifying a point relative to the current coordinates.
        /// </summary>
        /// <param name="relativeStart">Boolean value indicating whether the coordinates are relative to the current graphics position (as set by <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/>, <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/>).</param><param name="x">Single value indicating the horizontal coordinates of the point to print.</param><param name="y">Single value indicating the vertical coordinates of the point to print.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color specified for the point. If this parameter is omitted, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ForeColor"/> property setting is used.</param>
        public void PSet(bool relativeStart, double x, double y, int color)
        {
            fcGraphics.PSet(GetGraphics(), relativeStart, (float)x, (float)y, color);
        }

        /// <summary>
		/// Forces the control to update corresponding client widget.
		/// When in DesignMode it forces a full redraw of the designer surface 
		/// for this control, including the non-client areas such as the caption of forms or panels.
		/// </summary>
        public new void Refresh() {
            base.Refresh();
            FCUtils.ApplicationUpdate(this);
        }

        public new void Show()
        {
            if (App.MainForm != null && App.MainForm == this)
            {
                base.Show();
            }
            else
            {
                Show(App.MainForm);
            }
        }

        /// <summary>
        /// Shows the form modal/modeless depending on the style specified
        /// </summary>
        /// <param name="style">How to show the form modal/modeless</param>
        /// <param name="parentForm">Parent of the modal form</param>
        public void Show(FormShowEnum style, FCForm parentForm)
        {
            Show(style, "", parentForm);
        }

        /// <summary>
        /// Shows the form modeless with the specified parent
        /// </summary>
        /// <param name="parentForm">Parent of the form</param>
        public void Show(FCForm parentForm)
        {
            //JEI:HARRIS:IT250: frmWait and frmBusy will be shown also with this method
            //but we don't want to display it as a TabbedMDI child
            if(parentForm.IsMdiContainer && !this.TopMost)
            {
                this.MdiParent = parentForm;
            }
            Show(FormShowEnum.Modeless, "", parentForm);
            //JEI:HARRIS:IT250: frmWait and frmBusy will be shown also with this method
            //but we don't want to display it as a TabbedMDI child
            if (!this.IsDisposed && parentForm.IsMdiContainer && !this.TopMost)
            {                
                //AM:HARRIS:#654 - remove fix; not needed anymore
                //parentForm.MdiClient.TabControl.SelectedTab = this.Parent as TabPage;
                //FC:FINAL:SBE - if MDIParent is not Active, Activated event will not be triggered
                if (this.MdiParent != null && !this.MdiParent.Active)
                {
                    this.MdiParent.Activate();
                }
                this.Activate();
                //P2218:AM:#3592 - update the tab control to have the form correctly selected
                FCUtils.ApplicationUpdate(parentForm.MdiClient.TabControl);
            }
        }

        /// <summary>
        /// Shows the form modal/modeless depending on the style specified
        /// </summary>
        /// <param name="style">How to show the form modal/modeless</param>
        /// <param name="ownerForm">Parent of the modal form</param>
        /// <param name="parentForm">Parent of the modal form</param>
        public void Show(FormShowEnum style, String ownerForm = "", FCForm parentForm = null)
        {
            switch (style)
            {
                case FormShowEnum.Modeless:
					//DDU:HARRIS:#i2251 - if a modeless form is hided then shown again we need to set the startposition again to center to mdiparent
					this.StartPosition = FormStartPosition.CenterParent;
                    //AM:HARRIS:#2905 - set TopMost to true when the form is not displayed as an mdi child
                    if(parentForm == null)
                    {
                        this.TopMost = true;
                    }
                    base.Show();
                    break;
                case FormShowEnum.Modal:
                    this.isModal = true;
                    this.StartPosition = FormStartPosition.CenterScreen;
                    if (!this.Visible)
                    {
						//FC:FINAL:MSH - i.issue #1839: sometimes new modal window can be overlapped by already opened modal window
						this.BringToFront();
                        this.ShowDialog(parentForm);
                    }
                    //P2218:SBE:#4583 - avoid dispose of modal dialogs, closed with Hide() method. Form controls are accessed after Hide(), and disposing the form will reset the controls
                    //AvoidModalDialogDispose flag is reset to "false", so we have to be sure we set it back to "true" when needed
                    if (!AvoidModalDialogDispose)
                    {
                        //AM: modal forms are not disposed after closing
                        this.Dispose();
                    }
                    else
                    {
                        AvoidModalDialogDispose = false;
                    }
                    break;
            }
        }

        protected override void SetVisibleCore(bool value)
        {
			//DDU:#i2336 - check if visible state can be changed
			if (!base.IsDisposed)
			{
				base.SetVisibleCore(value);
			}
            //AM:HARRIS:#1681 - fix not needed anymore (it is causing problems when a form is hidden then shown again
            //FC:FINAL:SBE - Harris #249 - sometimes the tabpage is not removed from the MdiParent when form is hidden
            //if (this.IsMdiChild)
            //{
            //    TabControl.TabPageCollection mdiTabPages = this.MdiParent?.MdiClient?.TabControl?.TabPages;
            //    TabPage formTabPage = this.Parent as TabPage;
            //    if (mdiTabPages != null && formTabPage != null)
            //    {
            //        if (value == false && mdiTabPages.Contains(formTabPage))
            //        {
            //            mdiTabPages.Remove(formTabPage);
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Displays a pop-up menu on an MDIForm or Form object at the current mouse location or at specified coordinates. Doesn't support named arguments.
        /// </summary>
        public void PopupMenu(FCToolStripMenuItem menu)
        {
            this.ContextMenu = new ContextMenu();
            this.ContextMenu.MergeMenu(menu);
            //P2218:SBE:#4619 - show popup menu inside the active control, based on current MousePosition
            //this.ContextMenu.Show(MousePosition);
            if (this.ActiveControl != null)
            {
                this.ContextMenu.Show(this.ActiveControl, this.ActiveControl.PointToClient(Control.MousePosition));
            }
            else
            {
                this.ContextMenu.Show(MousePosition);
            }
            this.ContextMenu.Popup += ContextMenu_Popup;
        }

        private void ContextMenu_Popup(object sender, EventArgs e)
		{
			//FC:FINAL:DSE:#1823 Popup menu should open only programatic
			this.ContextMenu = null;
		}

		/// <summary>
		/// Gets the count of all the controls
		/// </summary>
		/// <returns></returns>
		public int AllControlsCount()
        {
            // Count each control children recursively
            int intCount = FCUtils.AllControlsCount(this);

            return intCount;
        }

        /// <summary>
        /// Unloads the form
        /// </summary>
        /// <param name="objControl">The control.</param>
        public bool Unload(FCCloseReason closeReason = FCCloseReason.FormCode)
        {
            //CHE: execute Unload even for cases when form was displayed with ShowDialog and Hide was called
            //(on Hide the FormClosing was triggered but not the FormClosed so it was not removed from Forms collection)
			if (isUnloaded)
			//if (isUnloaded && !isFormLoaded)
			{
                return false;
			}
            bool cancel = false;

            //CHE: set flag used in QueryUnload to set CloseReason
            if (closeReason == FCCloseReason.FormCode)
            {
                isFormClosedFromCode = true;
            }
            if (!this.IsMdiContainer)
            {
                //CHE: when calling Unload for some form it is closed unless CloseOnUnload flag is set to false
                if (CloseOnUnload)
                {
                    this.Close();

                    //CHE: set approapiate flags when closing was canceled
                    cancel = closingCanceled;

                    if (!cancel)
                    {
                        this.isFormLoaded = false;
                        //CHE: remove from collection when specific unload is called, do not remove it when the form is closed such as modal forms
                        FCGlobal.Statics.Forms.Remove(this);
                    }
                }
            }
            else
            {
                //CHE: when calling Unload on MDI then QueryUnload is called first for MDI and after for all children, then Unload for children and latest MDI
                #region unload MDI from code

                this.queryUnloadWasCalled = true;
                
                //MDIForm_QueryUnload
                if (QueryUnload != null)
                {
                    FCFormClosingEventArgs formClosingEventArgs = new FCFormClosingEventArgs(closeReason, false);
                    QueryUnload(this, formClosingEventArgs);
                    cancel = formClosingEventArgs.Cancel;
                }
                if (!cancel)
                {
                    //Form_QueryUnload for children
                    foreach (Form f in this.MdiChildren)
                    {
                        FCForm fc = f as FCForm;
                        fc.queryUnloadWasCalled = true;
                        if (!object.Equals(fc, this) && fc.QueryUnload != null)
                        {
                            FCFormClosingEventArgs formClosingEventArgs = new FCFormClosingEventArgs(closeReason, false);
                            fc.QueryUnload(fc, formClosingEventArgs);
                            cancel = formClosingEventArgs.Cancel;
                            if (cancel)
                            {
                                break;
                            }
                        }
                    }
                }
                if (!cancel)
                {
                    //to get children Form_Unload followed by MDIForm_Unload
                    //CHE: when calling Unload for some form it is closed unless CloseOnUnload flag is set to false
                    if (CloseOnUnload)
                    {
                        this.Close();
                    }
                }

                //reset flag
                foreach (Form f in this.MdiChildren)
                {
                    FCForm fc = f as FCForm;
                    fc.queryUnloadWasCalled = false;
                }
                this.queryUnloadWasCalled = false;

                #endregion
            }
			isUnloaded = !cancel;
            return cancel;
        }
        
        /// <summary>
        /// call Load
        /// </summary>
        public void LoadForm()
        {
            this.OnLoad(EventArgs.Empty);
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        /// <summary>
        /// Upgrade from VB6-Compatibility-Pack; Set a Default-(Accept-) Button
        /// </summary>
        /// <param name="btn"></param>
        public void SetDefault(FCButton btn, bool status)
        {
            this.AcceptButton = null;
            if (status)
            {
                this.AcceptButton = btn;
            }
        }

        /// <summary>
        /// Enables drawing in this window (form).
        /// </summary>
        /// <remarks>
        /// Erstellt am 16.12.2015.
        /// </remarks>
        /// <revisionHistory>
        ///   <revision date="16.12.2015" version="RQ 000000" author="mdi">Erstellung.</revision>
        /// </revisionHistory>
        public void UnlockWindowUpdate()
        {
            FCUtils.UnlockWindowUpdate(this.Handle);
            //this.UpdateLocked = false;
        }

        /// <summary>
        /// Disables drawing in this window (form).
        /// </summary>
        /// <remarks>
        /// It is absolutely neccessary to call <see cref="UnlockWindowUpdate"/> after drawing operations. Otherwise the
        /// window will be locked endless.
        /// </remarks>
        /// <revisionHistory>
        ///   <revision date="16.12.2015" version="RQ 000000" author="mdi">Erstellung.</revision>
        /// </revisionHistory>
        public void LockWindowUpdate()
        {
            if (this.IsHandleCreated)
            {
                FCUtils.LockWindowUpdate(this.Handle);
                //this.UpdateLocked = true;
            }
            else
            {
                FCUtils.LockWindowUpdate(IntPtr.Zero);
            }
        }
        #endregion

        #region Internal Methods
        /// <summary>
        /// Fire the LinkExecute event 
        /// </summary>
        /// <param name="CmdStr"></param>
        /// <param name="Cancel"></param>
        internal void FireLinkExecute(string CmdStr, short Cancel)
        {
            if(this.LinkExecute != null)
            {
                FCLinkExecuteEventArgs evt = new FCLinkExecuteEventArgs(CmdStr, Cancel);
                this.LinkExecute(this, evt);
            }
        }
        #endregion

        #region Protected Methods

        protected override void OnControlAdded(ControlEventArgs e)
        {
            //CHE: when adding a control to the form it inherits the font bold of the parent form
            if (this.FontBold && e.Control.Font == this.Font)
            {
                e.Control.SetFontBold(this.FontBold);
            }

            //CHE: when adding to the form a CommandButton with Cancel property True then set CancelButton of the form
            FCButton button = e.Control as FCButton;
            if (button != null)
            {
                if (button.Cancel)
                {
                    this.CancelButton = button;
                }
                if (button.Default)
                {
                    this.AcceptButton = button;
                }
            }

            base.OnControlAdded(e);
        }

        protected override void OnResize(EventArgs e)
        {
            //CHE: do not run while in InitializeComponent
            //AM: do not run before OnLoad
            if (!this.IsHandleCreated || !this.IsLoaded)
            {
                return;
            }
            //CHE: set flag to reset the background image used for painting so that it gets the correct size when it's recreated
            //CHE: do not set resetBackgroundImage when called Resize from form Load
            //if (!forceResize)
            //{
            //    resetBackgroundImage = true;
            //}
            if (!this.IsDisposed)
            {
                base.OnResize(e);
            }
        }

		public new void Hide()
		{
            //FC:FINAL:SBE - Harris #i1801 - executing Hide during form load does not have any effect in VB6
            if (!loading)
            {
                isFormClosedFromCode = true;
                base.Hide();
				//HARRIS:DDU:#2550 - hiding form could result in re showing it again and then closing it from control menu
				isFormClosedFromCode = false;
            }
		}

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            closingCanceled = false;

            //CHE: when clicking X close button on MDI then QueryUnload is called first for MDI and after for all children, then Unload for children and latest MDI
            #region exitMdiFormFromX

            if (this.exitMdiFormFromX)
            {
                //for mdi container do not execute again form closing
                if (this.IsMdiContainer)
                {
                    e.Cancel = true;
                    return;
                }
            }
            else if (e.CloseReason == CloseReason.MdiFormClosing && this.IsMdiChild)
            {
                //user has clicked on x to close MDI
                this.exitMdiFormFromX = true;
                FCForm mdi = this.MdiParentFCForm;
                if (mdi != null)
                {
                    e.Cancel = true;
                    mdi.exitMdiFormFromX = true;
                    bool cancel = mdi.Unload(FCCloseReason.FormControlMenu);
                    //reset flags
                    if (cancel)
                    {
                        mdi.exitMdiFormFromX = false;
                    }
                    this.exitMdiFormFromX = false;
                    return;
                }
            }

            #endregion

            //isClosing = true;
            if (!queryUnloadWasCalled && !queryUnloadHandled)
            {
                if(e.CloseReason == CloseReason.None)
                {
                    isFormClosedFromCode = true;
                }
               e.Cancel = InternalOnQueryUnload(e);
            }
            if (!e.Cancel)
            {
                e.Cancel = InternalOnFormUnload(e);
                if (!e.Cancel)
                {
                    base.OnFormClosing(e);
                }
                else
                {
                    isFormClosedFromCode = false;
                }
            }
            else
            {
                isFormClosedFromCode = false;
            }
            closingCanceled = e.Cancel;
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            this.queryUnloadWasCalled = false;
            isFormClosedFromCode = false;
            
            //CHE: remove from collection when specific unload is called, do not remove it when the form is closed such as modal forms
            FCGlobal.Statics.Forms.Remove(this);

            base.OnFormClosed(e);
            
            this.isFormLoaded = false;
            
            //FC:FINAL:SBE - Harris #i349 - Dispose() is executed after Form.Show() if form is modal, and removing/adding events, and reinitializing controls is not needed
            if (!this.isModal)
            {
                //CHE: Clear the Form's controls then reintitialize them to have the same behavior as VB6. 
                //VB6 Unload command releases only the visual components, but not the other members. So disposing the Form on Closed is too much,
                //but it is necessary to reinitialize the Controls so when the form is shown next time it will be in original state.

                //remove form handlers, will be reattached on initialize component
                //TRY:AM:HARRIS:#i2302
                //EventHelper.RemoveAllEvents(this, true);

                this.DisposeControls(this);
                //this.Controls.Clear();

                //TRY:AM:HARRIS:#i2302 - don't call the InitializeComponent methods again; when accessing form.InstancePtr after the form is closed, it will create a new instance of the form anyway
                //call again to initialize as in designer
                //if (!this.IsDisposed)
                //{
                //    //CHE: restore to normal state, if maximized the size is not set correctly
                //    this.WindowState = FormWindowState.Normal;
                //    this.InvokeMethodSafe("InitializeComponent", null, PrivateInstanceFlags);
                //    this.InvokeMethodSafe("InitializeComponentEx", null, PrivateInstanceFlags);
                //}
            }

            //Exit app if all forms are closed
            if (FCGlobal.Statics.Forms.Count == 0 || ((FCGlobal.Statics.Forms[0].IsMdiContainer || FCGlobal.Statics.Forms.Count == 1) && !(FCGlobal.Statics.Forms[0] as FCForm).isFormLoaded))
            {
                FCGlobal.ApplicationExit();
            }

            if (this.isModal)
            {
                if (this.Owner != null && this.Owner.ActiveMdiChild != null)
                {
                    //FC:FINAL:SGA Harris #i2258 force activation of ActiveMdiChild in case a modal dialog is closed (as in vb6)
                    (this.Owner.ActiveMdiChild as FCForm)?.OnActivated(EventArgs.Empty);
                }
            }

            if (App.MainForm.ExitingApplication)
            {
                //P2218:SBE:#3274 - close all long running processes when form is closed (or user logs out)
                foreach (var startedTask in this.StartedTasks)
                {
                    if (startedTask.IsAlive)
                    {
                        startedTask.Abort();
                    }
                }
                this.StartedTasks.Clear();
            }
        }

        private void DisposeControls(Control parent)
        {
            foreach(Control child in parent.Controls)
            {
                this.DisposeControls(child);
                child.Dispose();
            }
            parent.Controls.Clear();
        }

        public new void Close()
        {
            //CHE: avoid exception if form was not loaded
            if (!this.IsLoaded)
            {
                return;
            }

            //KPF: added to avoid problem that a Form when not shown, only loaded,  is closed,  won´t be deleted from Appication.OpenForms
            try
            {
                IntPtr h = this.Handle;
            }
            catch (Exception)
            {
                this.Show();
            }
			isFormClosedFromCode = true;
            base.Close();
        }
        protected override void OnLoad(EventArgs e)
        {
            //FC:FINAL:MSH - in many places before showing form is used Unload() and in this case Load() of the form will never be launched
            //if (this.isUnloaded)
            //{
            //    return;
            //}
            if (isFormLoaded || FCGlobal.InApplicationExit)
            {
                return;
            }
            string assemblyName = this.GetAssemblyKey();
            if (App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
            {
                this.IconSource = App.MainForm.ApplicationIcons[assemblyName];
            }
            else
            {
                this.IconSource = App.MainForm.ApplicationIcon;
            }
            isUnloaded = false;
            //this.isInitializing = false;
            this.isFormClosedFromCode = false;
            //CHE: set flag before otherwise if base.OnLoad throws exception the flag remains unset
            isFormLoaded = true;

            //CHE: add to collection
            FCGlobal.Statics.Forms.Add(this);
            
            //IPI: .NET is resetting base.WindowState when this property is set to Maximized before the form is shown
            if (base.WindowState != internalWindowState)
            {
                this.WindowState = internalWindowState;
            }
            this.loading = true;
            base.OnLoad(e);
            this.loading = false;
            //CHE: set flag before otherwise if base.OnLoad throws exception the flag remains unset
            //loaded = true;

            //JEI: in vb6 after Form_Load a Form_Resize event is fired
            forceResize = true;
            this.OnResize(EventArgs.Empty);
            forceResize = false;

            //CHE: set MDIParent only after form is shown, if it is set before (e.g. in constructor after InitializeComponents) then the form control box is doubled - Winforms bug
            foreach (FCTimer timer in Timers)
            {
                if (timer.Enabled)
                {
                    timer.BaseEnabled = true;
                    timer.Start();
                }
            }

            //RPU: Show list of events in ToolTip
#if TEST
            Dictionary<string, Delegate[]> events;
            CopyEventHandlers c;
            string valueToAdd;
            foreach (dynamic ctrl in this.Controls)
            {

                c = new CopyEventHandlers();
                events = c.GetHandlersFrom(ctrl);
                valueToAdd = "";
                foreach (var item in events)
                {
                    valueToAdd += item.Key + System.Environment.NewLine;
                }

                try
                {
                    ctrl.ToolTipText = valueToAdd;
                }
                catch(Exception ex)
                {
                    
                }
            }
#endif

        }

        public virtual string GetAssemblyKey()
        {
            string assemblyName = this.GetType().Assembly.GetName().Name;
            return assemblyName;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            //CHE: do not raise while in InitializeComponent
            if (isInitializing)
            {
                return;
            }
            base.OnTextChanged(e);
        }
        protected override void OnActivated(EventArgs e)
        {
            if (FCGlobal.InApplicationExit)
            {
                return;
            }
            if(this.IsDisposed)
            {
                return;
            }
			if (!Visible)
			{
				enforceActivated = true;
				return;
			}
			enforceActivated = false;
            base.OnActivated(e);
            //CHE: setting WindowState is raising base.OnActivate but this is not the case in VB6
            if (FCFormActivate != null && !(loading && inWindowState))
            {
                FCFormActivate(this, e);
            }
            if (isFormLoaded)
            {
                //set the formShow-State to true in case the Form is loaded because if Loading a Form and showing it, the Shown-Event will not be fired
                this.formShown = true;
            }
        }


        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
            //CHE: define FCDeactivate event to be raised only when application is active
            if (FCFormDeactivate != null && FCUtils.ApplicationIsActivated())
            {
                FCFormDeactivate(this, e);
            }
        }

        // TODO
        //protected override void WndProc(ref Message m)
        //{
        //    // When the move message is called, if the movable property of the form is set to false
        //    // prevent the form from moving by not processing the move message
        //    if (!this.moveable)
        //    {
        //        const int WM_SYSCOMMAND = 0x0112;
        //        const int SC_MOVE = 0xF010;

        //        switch (m.Msg)
        //        {
        //            case WM_SYSCOMMAND:
        //                {
        //                    int command = m.WParam.ToInt32() & 0xfff0;
        //                    if (command == SC_MOVE)
        //                    {
        //                        return;
        //                    }
        //                    break;
        //                }
        //        }
        //    }
        //    base.WndProc(ref m);
        //}

        protected override void OnBackColorChanged(EventArgs e)
        {
            //CHE: do not run while in InitializeComponent, resetBackgroundImage should not be set
            if (!this.IsHandleCreated)
            {
                return;
            }
            base.OnBackColorChanged(e);
            this.graphicsFactory.FillColor = ColorTranslator.ToOle(this.BackColor);
            //CHE: clear to refill with new BackColor
            Cls();
            //CHE: set flag to reset the background image used for painting so that it gets the correct size when it's recreated
            resetBackgroundImage = true;
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            base.OnForeColorChanged(e);
            this.graphicsFactory.ForeColor = ColorTranslator.ToOle(this.ForeColor);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Point transformedPoint = TransformCoordinate(e.X, e.Y);
            MouseEventArgs evArg = new MouseEventArgs(e.Button, e.Clicks, transformedPoint.X, transformedPoint.Y, e.Delta);
            base.OnMouseMove(evArg);
        }

        /// <summary>
        /// prevent from executing Key-Down-Event, if a Windows-Clipboard-Key is pressed. In VB6 you get e.g. the first key, only
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            //P2218:AM:#3679 - KeyDown/Up is not fired in VB6 for TAB key
            if (e.KeyCode == Keys.Tab)
            {
                return;
            }
            //FC:FINAl:SBE - Harris #3869 - trigger KeyPress event for Esc. It is not triggered by Wisej. An ITG issue was reported (Git issue #31), but it will not be fixed ("That's as designed. The list of input keys is different from winforms.")
            if (e.KeyCode == Keys.Escape)
            {
                EscKeyPressExecuted = false;
            }
            if (!EventHelper.IsWindowsClipboardKey(e) || FCUtils.IsInputControlFocused(this) == null)
            {
                base.OnKeyDown(e);
            }
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            //P2218:AM:#3679 - KeyDown/Up is not fired in VB6 for TAB key
            if (e.KeyCode == Keys.Tab)
            {
                return;
            }
            //FC:FINAl:SBE - Harris #3869 - trigger KeyPress event for Esc. It is not triggered by Wisej. An ITG issue was reported (Git issue #31), but it will not be fixed ("That's as designed. The list of input keys is different from winforms.")
            if (e.KeyCode == Keys.Escape)
            {
                if (!EscKeyPressExecuted)
                {
                    OnKeyPress(new KeyPressEventArgs(Strings.Chr(e.KeyValue)));
                }
                EscKeyPressExecuted = true;
            }
            base.OnKeyUp(e);
        }

        /// <summary>
        /// Handles OnShown-Event: Here the formShown-Status will be set
        /// </summary>
        /// <param name="e"></param>
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            this.formShown = true;
            ////FC:FINAL:SBE - Wisej issue workaround - in some situations form Activate is not triggered. (This problem was not reproducible in a sample)
            //if (this.IsMdiChild)
            //{
            //    this.OnActivated(e);
            //}
        }
        #endregion

        #region Private Methods

        private void CaptureScreen()
        {
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            Point p = PointToScreen(new Point(this.Location.X, this.Location.Y));
            memoryGraphics.CopyFromScreen(p.X, p.Y, 0, 0, s);
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }

        private Point TransformCoordinate(int X, int Y)
        {
            Point result = new Point();

            //using (Graphics g = GetGraphics())
            {
                fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                result.X = Convert.ToInt32(fcGraphics.ScaleX(X, ScaleModeConstants.vbPixels, this.ScaleMode));
                result.Y = Convert.ToInt32(fcGraphics.ScaleY(Y, ScaleModeConstants.vbPixels, this.ScaleMode));
            }

            return result;
        }

        private void FCForm_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                //FC:BCU - modal forms should be closed when visibility is turned off
                if (this.Modal)
                {
                    base.Close();
                }
                return;
            }

            //CHE: set MDIParent only after form is shown, if it is set before (e.g. in constructor after InitializeComponents) then the form control box is doubled
            if (this.MdiParentFCForm != null)
            {
                FCUtils.LockWindowUpdate(this.MdiParentFCForm.Handle);
                this.MdiParent = this.MdiParentFCForm;
                FCUtils.UnlockWindowUpdate(this.MdiParentFCForm.Handle);
            }


			if (enforceActivated)
			{
				OnActivated(EventArgs.Empty);
			}
        }

        private bool InvokeMethodSafe(string name, object[] paramList, BindingFlags invokeAttr)
        {
            return InvokeMethodOnType(this.GetType(), name, paramList, invokeAttr);
        }

        private bool InvokeMethodOnType(Type formType, string name, object[] paramList, BindingFlags invokeAttr)
        {
            bool executed = false;
            if (formType != null && formType != typeof(Form))
            {
                MethodInfo method = formType.GetMethod(name, invokeAttr);
                formType = formType.BaseType;
                this.InvokeMethodOnType(formType, name, paramList, invokeAttr);
                if (method != null)
                {
                    method.Invoke(this, paramList);
                    executed = true;
                }
            }

            return executed;
        }

        private FCCloseReason TranslateCloseReason(CloseReason closeReason)
        {
            switch (closeReason)
            {
                case CloseReason.None:
                    {
                        return FCCloseReason.FormControlMenu;
                    }

                //Obsolete
                //case CloseReason.WindowsShutDown:
                //    {
                //        return FCCloseReason.AppWindows;
                //    }

                case CloseReason.MdiFormClosing:
                    {
                        return FCCloseReason.FormMDIForm;
                    }

                case CloseReason.UserClosing:
                    {
                        return FCCloseReason.FormControlMenu;
                    }

                //Obsolete
                //case CloseReason.TaskManagerClosing:
                //    {
                //        return FCCloseReason.AppTaskManager;
                //    }

                case CloseReason.FormOwnerClosing:
                    {
                        return FCCloseReason.FormOwner;
                    }

                //Obsolete
                //case CloseReason.ApplicationExitCall:
                //    {
                //        return FCCloseReason.AppTaskManager;
                //    }

                default:
                    {
                        return FCCloseReason.FormControlMenu;
                    }
            }
        }

        private bool InternalOnQueryUnload(FormClosingEventArgs e)
        {
            bool formClosingCancel = e.Cancel;
            if (QueryUnload != null)
            {
                FCFormClosingEventArgs formClosingEventArgs = new FCFormClosingEventArgs(isFormClosedFromCode ? FCCloseReason.FormCode : TranslateCloseReason(e.CloseReason), false);
                //CHE: prevent QueryUnload to be executed twice (e.g. when calling Application.Exit in Queryunload on client code)
                if (!inQueryunload)
                {
                    inQueryunload = true;
                    QueryUnload(this, formClosingEventArgs);
                    inQueryunload = false;
                }
                formClosingCancel = formClosingEventArgs.Cancel;
            }
            if (!formClosingCancel)
            {
                queryUnloadHandled = true;
            }
            return formClosingCancel;
        }

        private bool InternalOnFormUnload(FormClosingEventArgs e)
        {
			if (isUnloaded)
			{
				return false;
			}
            bool formClosingCancel = e.Cancel;
            if (FormUnload != null && !FCGlobal.InApplicationExit)
            {
                FCFormClosingEventArgs formClosingEventArgs = new FCFormClosingEventArgs(isFormClosedFromCode ? FCCloseReason.FormCode : TranslateCloseReason(e.CloseReason), false);
                FormUnload(this, formClosingEventArgs);
                formClosingCancel = formClosingEventArgs.Cancel;
            }
			isUnloaded = !formClosingCancel;
            return formClosingCancel;
        }

        //CHE: the bounds of the gray scrollable area in an MDI parent - exclude any menu, scroll bars, status areas or other controls
        private Rectangle GetRectangle()
        {
            PropertyInfo pi = typeof(Form).GetProperty("MdiClient", BindingFlags.Instance | BindingFlags.NonPublic);
            MdiClient mdiClient = pi.GetValue(this, null) as MdiClient;
            if (mdiClient != null)
            {
                return mdiClient.Bounds;
            }
            return this.ClientRectangle;
        }

        #endregion
    }
 }
