﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public class FCLinkExecuteEventArgs : EventArgs
    {
        #region Private Members
        private string cmdStr = "";
        private short cancel = 0;
        #endregion

        #region Constructor
        public FCLinkExecuteEventArgs(string CmdStr, short Cancel)
        {
            this.cmdStr = CmdStr;
            this.cancel = Cancel;
        }
        #endregion

        #region Properties
        public string CmdStr
        {
            get
            {
                return this.cmdStr;
            }
        }

        public short Cancel
        {
            get
            {
                return this.cancel;
            }
            set
            {
                this.cancel = value;
            }
        }
        #endregion
    }

}
