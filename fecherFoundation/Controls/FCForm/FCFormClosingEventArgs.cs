﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public class FCFormClosingEventArgs : CancelEventArgs
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCCloseReason closeReason;

        #endregion

        #region Constructors

        public FCFormClosingEventArgs(FCCloseReason CloseReason, bool Cancel)
        {
            this.closeReason = CloseReason;
            this.Cancel = Cancel;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public FCCloseReason CloseReason
        {
            get
            {
                return this.closeReason;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
