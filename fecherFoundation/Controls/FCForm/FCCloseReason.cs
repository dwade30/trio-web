﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// Close Reason enum just in VB6
    /// </summary>
    public enum FCCloseReason
    {
        //The user chose the Close command from the Control menu on the form.
        FormControlMenu = 0,
        //The Unload statement is invoked from code.
        FormCode = 1,
        //The current Microsoft Windows operating environment session is ending.
        AppWindows = 2,
        //The Microsoft Windows Task Manager is closing the application.
        AppTaskManager = 3,
        //An MDI child form is closing because the MDI form is closing.
        FormMDIForm = 4,
        //A form is closing because its owner is closing.
        FormOwner = 5
    }
}
