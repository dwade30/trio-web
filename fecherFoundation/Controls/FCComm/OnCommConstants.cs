﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public enum OnCommConstants
    {
        comEvSend = 1,
        comEvReceive = 2,
        comEvCTS = 3,
        comEvDSR = 4,
        comEvCD = 5,
        comEvRing = 6,
        comEvEOF = 7,


        comEventBreak = 1001,
        comEventCTSTO = 1002,
        comEventDSRTO = 1003,
        comEventFrame = 1004,
        comEventOverrun = 1006,
        comEventCDTO = 1007,
        comEventRxOver = 1008,
        comEventRxParity = 1009,
        comEventTxFull = 1010,
        comEventDCB = 1011
    }
}
