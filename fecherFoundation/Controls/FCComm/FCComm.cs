﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO.Ports;
using System.ComponentModel;

namespace fecherFoundation
{
    /// <summary>
    /// Replacement for MSComm control (OCX) in VB6
    /// 
    /// The MSComm control provides serial communications for your application 
    /// by allowing the transmission and reception of data through a serial port.
    /// 
    /// inspired by http://www.dreamincode.net/forums/topic/35775-serial-port-communication-in-c%23/#/
    /// </summary>
    public class FCComm : Control
    {
        #region private fields

        private string m_settings = "9600,n,8,1";
        private string m_inputBuffer;
        private short m_commPort = 1;
        private string m_portName = "COM1";

        private int m_baudRate = 9600;
        private Parity m_parity = Parity.None;
        private StopBits m_stopBits = StopBits.One;
        private int m_dataBits = 8;
        private int m_rTreshold = 0;
        private int m_sTreshold = 0;

        private SerialPort m_port = new SerialPort();

        #endregion


        #region public properties

        /// <summary>
        /// Sets and returns the state of the communications port (open or closed). Not available at design time.
        /// </summary>
        /// <remarks>
        /// Setting the PortOpen property to True opens the port. Setting it to False closes the port and clears the receive and transmit buffers. 
        /// The MSComm control automatically closes the serial port when your application is terminated.
        /// 
        /// Make sure the CommPort property is set to a valid port number before opening the port.
        /// If the CommPort property is set to an invalid port number when you try to open the port, the MSComm control generates error 68 (Device unavailable).
        /// 
        /// In addition, your serial port device must support the current values in the Settings property.
        /// If the Settings property contains communications settings that your hardware does not support, your hardware may not work correctly.
        /// 
        /// If either the DTREnable or the RTSEnable properties is set to True before the port is opened, the properties are set to False when the port is closed.Otherwise, the DTR and RTS lines remain in their previous state.
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool PortOpen
        {
            get { return m_port.IsOpen; }
            set
            {
                if(value != m_port.IsOpen)
                {
                    if(value)
                    {
                        OpenPort();
                    }
                    else
                    {
                        m_port.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Sets and returns the communications port number.
        /// </summary>
        /// <remarks>
        /// You can set value to any number between 1 and 16 at design time (the default is 1). 
        /// However, the MSComm control generates error 68 (Device unavailable) if the port does not exist when you attempt to open it with the PortOpen property.
        /// Warning You must set the CommPort property before opening the port.
        /// </remarks>
        [DefaultValue(1)]
        public short CommPort
        {
            get { return m_commPort; }
            set
            {
                m_commPort = value;
                m_portName = $"COM{value}";
            }
        }


        /// <summary>
        /// Sets and returns the baud rate, parity, data bits, and stop bits as a string.
        /// </summary>
        /// <example>fcComm.Setting = "9600,N,8,1"</example>
        [DefaultValue("9600,n,8,1")]
        public string Settings
        {
            get { return m_settings; }
            set
            {
                if (m_settings != value)
                {
                    UpdateSettings(value);
                    m_settings = value;
                }
            }
        }

        /// <summary>
        /// Returns and removes a stream of data from the receive buffer. 
        /// This property is not available at design time and is read-only at run time.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Input { get { return m_inputBuffer; } }

        /// <summary>
        /// Writes a stream of data to the transmit buffer. This property is not available at design time and is write-only at run time.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Output { set { WriteData(value); } }

        /// <summary>
        /// Returns the most recent communication event or error. This property is not available at design time and is read-only at run time.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CommEvent { get; private set; }

        /// <summary>
        /// Sets and returns the number of characters to receive before the MSComm control sets the CommEvent property to comEvReceive and generates the OnComm event.
        /// </summary>
        /// <remarks>
        /// Setting the RThreshold property to 0 (the default) disables generating the OnComm event when characters are received.
        /// Setting RThreshold to 1, for example, causes the MSComm control to generate the OnComm event every time a single character is placed in the receive buffer.
        /// </remarks>
        [DefaultValue(0)]
        public int RTreshold
        {
            get { return m_rTreshold; }
            set { if (value != 0) throw new NotImplementedException(); }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(0)]
        public int STreshold
        {
            get { return m_sTreshold; }
            set { if (value != 0) throw new NotImplementedException(); }
        }

        /// <summary>
        /// Determines whether to enable the Request To Send (RTS) line. Typically, the Request To Send signal that requests permission to transmit data is sent from a computer to its attached modem.
        /// </summary>
        [DefaultValue(false)]
        public bool RTSEnable { get; set; }

        /// <summary>
        /// Determines whether to enable the Data Terminal Ready (DTR) line during communications. Typically, the Data Terminal Ready signal is sent by a computer to its modem to indicate that the computer is ready to accept incoming transmission.
        /// </summary>
        [DefaultValue(false)]
        public bool DTREnable { get; set; }

        /// <summary>
        /// The EOFEnable property determines if the MSComm control looks for End Of File (EOF) characters during input. If an EOF character is found, the input will stop and the OnComm event will fire with the CommEvent property set to comEvEOF.
        /// </summary>
        [DefaultValue(false)]
        public bool EOFEnable { get; set; }


        // TODO:
        //public int InputMode { get; set; }

        public event EventHandler OnComm;

        #endregion

        #region ctor

        public FCComm()
        {
            m_port.DataReceived += M_port_DataReceived;

        }

        #endregion


        #region private methods

        private void UpdateSettings(string settings)
        {
            char[] sep = { ',' };
            string[] parts = settings.Split(sep);

            if (parts.Length < 4) throw new ArgumentException();

            //baud
            m_baudRate = int.Parse(parts[0]);
            // parity
            m_parity = (Parity)Enum.Parse(typeof(Parity), parts[1]);
            // data bits
            m_dataBits = int.Parse(parts[2]);
            // stop bits
            m_stopBits = (StopBits)Enum.Parse(typeof(StopBits), parts[3]);

        }

        private void M_port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            switch(e.EventType)
            {
                case SerialData.Chars:
                    CommEvent = (int)OnCommConstants.comEvReceive;
                    m_inputBuffer = m_port.ReadExisting();

                    break;

                case SerialData.Eof:
                    if (!EOFEnable) return;
                    CommEvent = (int)OnCommConstants.comEvEOF;
                    break;
            }
            RaiseOnComm();
        }

        private void RaiseOnComm()
        {
            if(OnComm != null)
            {
                OnComm(this, EventArgs.Empty);
            }
        }

        private bool OpenPort()
        {
            try
            {
                //first check if the port is already open
                //if its open then close it
                if (m_port.IsOpen == true) m_port.Close();

                //set the properties of our SerialPort Object
                m_port.BaudRate = m_baudRate;    //BaudRate
                m_port.DataBits = m_dataBits;    //DataBits
                m_port.StopBits = m_stopBits;    //StopBits
                m_port.Parity = m_parity;    //Parity
                m_port.PortName = m_portName;   //PortName
                //now open the port
                m_port.Open();
                //return true
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void WriteData(string msg)
        {
            //first make sure the port is open
            //if its not open then open it
            if (!(m_port.IsOpen == true)) OpenPort();
            //send the message to the port
            m_port.Write(msg);
        }

        #endregion

        #region IDispose

        protected override void Dispose(bool disposing)
        {
            if (m_port.IsOpen) m_port.Close();

            base.Dispose(disposing);
        }

        #endregion

    }



    class CommunicationManager
    {
        #region Manager Enums
        /// <summary>
        /// enumeration to hold our transmission types
        /// </summary>
        public enum TransmissionType { Text, Hex }

        /// <summary>
        /// enumeration to hold our message types
        /// </summary>
        public enum MessageType { Incoming, Outgoing, Normal, Warning, Error };
        #endregion

        #region Manager Variables
        //property variables
        private string _baudRate = string.Empty;
        private string _parity = string.Empty;
        private string _stopBits = string.Empty;
        private string _dataBits = string.Empty;
        private string _portName = string.Empty;
        private TransmissionType _transType;
        
        //global manager variables
        private SerialPort comPort = new SerialPort();
        #endregion

        #region Manager Properties
        /// <summary>
        /// Property to hold the BaudRate
        /// of our manager class
        /// </summary>
        public string BaudRate
        {
            get { return _baudRate; }
            set { _baudRate = value; }
        }

        /// <summary>
        /// property to hold the Parity
        /// of our manager class
        /// </summary>
        public string Parity
        {
            get { return _parity; }
            set { _parity = value; }
        }

        /// <summary>
        /// property to hold the StopBits
        /// of our manager class
        /// </summary>
        public string StopBits
        {
            get { return _stopBits; }
            set { _stopBits = value; }
        }

        /// <summary>
        /// property to hold the DataBits
        /// of our manager class
        /// </summary>
        public string DataBits
        {
            get { return _dataBits; }
            set { _dataBits = value; }
        }

        /// <summary>
        /// property to hold the PortName
        /// of our manager class
        /// </summary>
        public string PortName
        {
            get { return _portName; }
            set { _portName = value; }
        }

        /// <summary>
        /// property to hold our TransmissionType
        /// of our manager class
        /// </summary>
        public TransmissionType CurrentTransmissionType
        {
            get { return _transType; }
            set { _transType = value; }
        }

        #endregion

        #region Manager Constructors
        /// <summary>
        /// Constructor to set the properties of our Manager Class
        /// </summary>
        /// <param name="baud">Desired BaudRate</param>
        /// <param name="par">Desired Parity</param>
        /// <param name="sBits">Desired StopBits</param>
        /// <param name="dBits">Desired DataBits</param>
        /// <param name="name">Desired PortName</param>
        public CommunicationManager(string baud, string par, string sBits, string dBits, string name)
        {
            _baudRate = baud;
            _parity = par;
            _stopBits = sBits;
            _dataBits = dBits;
            _portName = name;
            //now add an event handler
            comPort.DataReceived += new SerialDataReceivedEventHandler(comPort_DataReceived);
        }

        /// <summary>
        /// Comstructor to set the properties of our
        /// serial port communicator to nothing
        /// </summary>
        public CommunicationManager()
        {
            _baudRate = string.Empty;
            _parity = string.Empty;
            _stopBits = string.Empty;
            _dataBits = string.Empty;
            _portName = "COM1";
            //add event handler
            comPort.DataReceived += new SerialDataReceivedEventHandler(comPort_DataReceived);
        }
        #endregion

        #region WriteData
        public void WriteData(string msg)
        {
            switch (CurrentTransmissionType)
            {
                case TransmissionType.Text:
                    //first make sure the port is open
                    //if its not open then open it
                    if (!(comPort.IsOpen == true)) comPort.Open();
                    //send the message to the port
                    comPort.Write(msg);
                    break;
                case TransmissionType.Hex:
                    
                    //convert the message to byte array
                    byte[] newMsg = HexToByte(msg);
                    //send the message to the port
                    comPort.Write(newMsg, 0, newMsg.Length);
                    break;
                default:
                    //first make sure the port is open
                    //if its not open then open it
                    if (!(comPort.IsOpen == true)) comPort.Open();
                    //send the message to the port
                    comPort.Write(msg);
                    break;
            }
        }
        #endregion

        #region HexToByte
        /// <summary>
        /// method to convert hex string into a byte array
        /// </summary>
        /// <param name="msg">string to convert</param>
        /// <returns>a byte array</returns>
        private byte[] HexToByte(string msg)
        {
            //remove any spaces from the string
            msg = msg.Replace(" ", "");
            //create a byte array the length of the
            //divided by 2 (Hex is 2 characters in length)
            byte[] comBuffer = new byte[msg.Length / 2];
            //loop through the length of the provided string
            for (int i = 0; i < msg.Length; i += 2)
                //convert each set of 2 characters to a byte
                //and add to the array
                comBuffer[i / 2] = (byte)Convert.ToByte(msg.Substring(i, 2), 16);
            //return the array
            return comBuffer;
        }
        #endregion

        #region ByteToHex
        /// <summary>
        /// method to convert a byte array into a hex string
        /// </summary>
        /// <param name="comByte">byte array to convert</param>
        /// <returns>a hex string</returns>
        private string ByteToHex(byte[] comByte)
        {
            //create a new StringBuilder object
            StringBuilder builder = new StringBuilder(comByte.Length * 3);
            //loop through each byte in the array
            foreach (byte data in comByte)
                //convert the byte to a string and add to the stringbuilder
                builder.Append(Convert.ToString(data, 16).PadLeft(2, '0').PadRight(3, ' '));
            //return the converted value
            return builder.ToString().ToUpper();
        }
        #endregion

        

        #region OpenPort
        public bool OpenPort()
        {
            try
            {
                //first check if the port is already open
                //if its open then close it
                if (comPort.IsOpen == true) comPort.Close();

                //set the properties of our SerialPort Object
                comPort.BaudRate = int.Parse(_baudRate);    //BaudRate
                comPort.DataBits = int.Parse(_dataBits);    //DataBits
                comPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), _stopBits);    //StopBits
                comPort.Parity = (Parity)Enum.Parse(typeof(Parity), _parity);    //Parity
                comPort.PortName = _portName;   //PortName
                //now open the port
                comPort.Open();
                //return true
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region SetParityValues
        //public void SetParityValues(object obj)
        //{
        //    foreach (string str in Enum.GetNames(typeof(Parity)))
        //    {
        //        ((ComboBox)obj).Items.Add(str);
        //    }
        //}
        #endregion

        #region SetStopBitValues
        //public void SetStopBitValues(object obj)
        //{
        //    foreach (string str in Enum.GetNames(typeof(StopBits)))
        //    {
        //        ((ComboBox)obj).Items.Add(str);
        //    }
        //}
        #endregion

        #region SetPortNameValues
        //public void SetPortNameValues(object obj)
        //{

        //    foreach (string str in SerialPort.GetPortNames())
        //    {
        //        ((ComboBox)obj).Items.Add(str);
        //    }
        //}
        #endregion

        #region comPort_DataReceived
        /// <summary>
        /// method that will be called when theres data waiting in the buffer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void comPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //determine the mode the user selected (binary/string)
            switch (CurrentTransmissionType)
            {
                //user chose string
                case TransmissionType.Text:
                    //read data waiting in the buffer
                    string msg = comPort.ReadExisting();
                    break;
                //user chose binary
                case TransmissionType.Hex:
                    //retrieve number of bytes in the buffer
                    int bytes = comPort.BytesToRead;
                    //create a byte array to hold the awaiting data
                    byte[] comBuffer = new byte[bytes];
                    //read the data and store it
                    comPort.Read(comBuffer, 0, bytes);
                    break;
                default:
                    //read data waiting in the buffer
                    string str = comPort.ReadExisting();
                    break;
            }
        }
        #endregion
    }



}
