﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;
using System.Drawing;

namespace fecherFoundation
{
    /// <summary>
    /// VB.MainMenu
    /// </summary>
    public class FCMenuStrip : MainMenu
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;
        #endregion

        #region Constructors

        public FCMenuStrip()
        {
            // TODO
            ////using (Graphics g = this.CreateGraphics())
            //{
            //    graphicsFactory = new GraphicsFactory();
            //    fcGraphics = new FCGraphics(g, graphicsFactory);
            //}
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        // TODO
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public int HeightOriginal
        //{
        //    get
        //    {
        //        //using (Graphics g = this.CreateGraphics())
        //        {
        //            fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
        //            return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
        //        }
        //    }
        //    set
        //    {
        //        //using (Graphics g = this.CreateGraphics())
        //        {
        //            fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
        //            base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
        //        }
        //    }
        //}

        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public int Index
        //{
        //    get
        //    {
        //        return this.GetIndex();
        //    }
        //}

        /// <summary>
        /// menu name
        /// </summary>
        public new string Name
        {
            // TODO:CHE
            get;
            set;
        }

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        /// <summary>
        /// Implement behavior of visibility like in VB6
        /// </summary>
        public new bool Visible
        {
            get
            {
                bool visibleState = true;
                visibleState &= (base.DesignMode || GetVisibleState());
                // TODO
                //base.Visible = visibleState;
                return visibleState;
            }
            set
            {
                //base.Visible = value;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        public bool GetVisibleState()
        {
            bool visibleStatus = true;
            MenuItemCollection menuItems = this.MenuItems;
            bool tsItemsInVisible = true;
            visibleStatus = false;
            foreach (FCToolStripMenuItem ts in menuItems)
            {
                tsItemsInVisible &= !(ts.Visible);
                if (!tsItemsInVisible)
                {
                    visibleStatus = true;
                    break;
                }
            }
            return visibleStatus;
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        #endregion
    }
}
