﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// VB.Label
    /// </summary>
    public partial class FCLabel : Label
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        private Image mouseIcon = null;
        private bool visible = true;
        private ToolTip toolTip = new ToolTip();
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private AppearanceConstants appearance = AppearanceConstants.cc3D;
		private HorizontalAlignment alignment = HorizontalAlignment.Left;
		private int backStyle = 1;
        private Color backColor;
        private string dataField;

        private bool wasAutoSize = false;

        private int borderStyle = 0;
        private bool capitalize = true;

        #endregion

        #region Constructors

        public FCLabel()
        {
            //this.BackColor = SystemColors.Control;
            InitializeComponent();
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            //this.AllowDrop = true;

            //CHE: set default value
            this.WhatsThisHelpID = 0;
            this.DragMode = DragModeConstants.vbManual;
            this.Appearance = AppearanceConstants.cc3D;
            this.BackStyle = 1;
            this.ToolTipText = "";
            this.WordWrap = false;
            this.DataField = "";
            this.BorderStyle = 0;
            this.Capitalize = true;
            //AM:HARRIS:#3468 - don't use mnemonic for labels
            this.UseMnemonic = false;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum AppearanceConstants
        {
            ccFlat = 0,
            cc3D = 1
        }

        #endregion

        #region Properties

        /// <summary>
        /// use internal flag to set Visible regardless the control is shown currently or not
        /// </summary>
        [DefaultValue(true)]
        public new bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
                base.Visible = value;
            }
        }

        /// <summary>
        /// Returns/sets an associated context number for an object.
        /// </summary>
        [DefaultValue(0)]
        public int WhatsThisHelpID
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(HorizontalAlignment.Left)]
        public HorizontalAlignment Alignment
        {
            get
			{
				return this.alignment;
			}
            set
			{
				alignment = value;
				if (alignment == HorizontalAlignment.Left)
				{
					this.ImageAlign = ContentAlignment.MiddleLeft;
					this.TextAlign = ContentAlignment.MiddleLeft;
				}
				else if (alignment == HorizontalAlignment.Right)
				{
					this.ImageAlign = ContentAlignment.MiddleRight;
					this.TextAlign = ContentAlignment.MiddleRight;
				}
				else if (alignment == HorizontalAlignment.Center)
				{
					this.ImageAlign = ContentAlignment.MiddleCenter;
					this.TextAlign = ContentAlignment.MiddleCenter;
				}
			}
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Caption
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(this.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    this.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [DefaultValue(false)]
        public new bool AutoSize
        {
            get
            {
                return base.AutoSize;
            }
            set
            {
                Size oldSize = this.Size;
                bool autoSizeRemoved = false;
                if (base.AutoSize && !value)
                {
                    autoSizeRemoved = true;
                }
                base.AutoSize = value;
                if (autoSizeRemoved)
                {
                    this.Size = oldSize;
                }
            }
        }

        /// <summary>
        /// in VB6 Width can be changed for a label with AutoSize true 
        /// </summary>
        public new int Width
        {
            get
            {
                return base.Width;
            }
            set
            {
                if (this.AutoSize && !this.DesignMode)
                {
                    this.AutoSize = false;
                    this.wasAutoSize = true;
                }
                base.Width = value;
            }
        }

        /// <summary>
        /// in VB6 Height can be changed for a label with AutoSize true 
        /// </summary>
        public new int Height
        {
            get
            {
                return base.Height;
            }
            set
            {
                if (this.AutoSize && !this.DesignMode)
                {
                    this.AutoSize = false;
                    this.wasAutoSize = true;
                }
                base.Height = value;
            }
        }

        public new string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                if (wasAutoSize)
                {
                    this.AutoSize = true;
                    wasAutoSize = false;
                }

                int previousWidth = this.Width;

                string text = value;
                
                //FC:FINAL:SBE - Harris #700 - colon has to be removed, only when it is at the end of the text
                if (Capitalize && value != null)
                {
                    text = value.ToUpper();
                    //P2218:SBE:#4506 - Period was already removed from forms during design mode (redesign or other fixes). Implementation is not needed anymore, and breaks labels with values from database
                    ////FC:FINAL:DDU - we need to let ':' text only and to remove too the '.' from the end
                    //if (text != ":")
                    //{
                    //	if (text.EndsWith(":") || text.EndsWith("."))
                    //	{
                    //		text = text.Remove(text.Length - 1);
                    //	}
                    //}
                }

                if (this.AutoSize || !this.TruncateTextToWidth)
                {
                    if (base.Text != text)
                    {
                        base.Text = text;
                    }

                    //CHE: grow to the left if align is right and autosize is set
                    if (this.AutoSize && this.TextAlign == ContentAlignment.MiddleRight)
                    {
                        int currentWidth = this.Width;
                        if (currentWidth > previousWidth)
                        {
                            this.Left -= currentWidth - previousWidth;
                        }
                        else if (currentWidth < previousWidth)
                        {
                            this.Left += previousWidth - currentWidth;
                        }
                    }
                }
                else
                {
                    this.Eval(@"FitStringToControlWidth(this,'" + text + "'," + (this.UseEllipses ? "true" : "false") +  ")",new Action<dynamic>(this.TextMeasureCallback));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(this.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    this.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether manual or automatic drag mode is used for a drag-and-drop operation.
        /// object.DragMode [= number]
        /// Number	An integer that specifies the drag mode, as described in Settings.
        /// Settings
        /// The settings for number are:
        /// Constant	Setting	Description
        /// VbManual	0	(Default) Manual requires using the Drag method to initiate a drag-and-drop operation on the source control.
        /// VbAutomatic	1	Automatic clicking the source control automatically initiates a drag-and-drop operation. OLE container controls are automatically dragged only when they don't have the focus.
        /// Remarks
        /// When DragMode is set to 1 (Automatic), the control doesn't respond as usual to mouse events. Use the 0 (Manual) setting to determine when a drag-and-drop operation begins or ends; you can use 
        /// this setting to initiate a drag-and-drop operation in response to a keyboard or menu command or to enable a source control to recognize a MouseDown event prior to a drag-and-drop operation.
        /// Clicking while the mouse pointer is over a target object or form during a drag-and-drop operation generates a DragDrop event for the target object. This ends the drag-and-drop operation. 
        /// A drag-and-drop operation may also generate a DragOver event.
        /// Note   While a control is being dragged, it can't recognize other user-initiated mouse or keyboard events (KeyDown, KeyPress or KeyUp, MouseDown, MouseMove, or MouseUp). However, the control 
        /// can receive events initiated by code or by a DDE link.
        /// </summary>
        [DefaultValue(DragModeConstants.vbManual)]
        public DragModeConstants DragMode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }
        /// <summary>
        /// Gets or sets the strikethrough font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontStrikethru
        {
            get
            {
                return this.graphicsFactory.FontStrikethru;
            }
            set
            {
                this.graphicsFactory.FontStrikethru = value;
            }
        }

        public new Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                if (wasAutoSize)
                {
                    this.AutoSize = true;
                    wasAutoSize = false;
                }
                base.Font = value;
            }
        }

        /// <summary>
        /// Returns or sets the font used to display text in a control or in a run-time drawing or printing operation.
        /// Note   The FontName property is included for use with the CommonDialog control and for compatibility with earlier versions of Visual Basic. For additional functionality, 
        /// use the new Font object properties (not available for the CommonDialog control).
        /// Syntax
        /// object.FontName [= font]
        /// The FontName property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// font	A string expression specifying the font name to use.
        /// Remarks
        /// The default for this property is determined by the system. Fonts available with Visual Basic vary depending on your system configuration, display devices, and printing 
        /// devices. Font-related properties can be set only to values for which fonts exist.
        /// In general, you should change FontName before setting size and style attributes with the FontSize, FontBold, FontItalic, FontStrikethru, and FontUnderline properties.
        /// Note   At run time, you can get information on fonts available to the system through the FontCount and Fonts properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                if (wasAutoSize)
                {
                    this.AutoSize = true;
                    wasAutoSize = false;
                }
                this.SetFontName(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                if (wasAutoSize)
                {
                    this.AutoSize = true;
                    wasAutoSize = false;
                }
                this.SetFontBold(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                if (wasAutoSize)
                {
                    this.AutoSize = true;
                    wasAutoSize = false;
                }
                this.SetFontItalic(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                if (wasAutoSize)
                {
                    this.AutoSize = true;
                    wasAutoSize = false;
                }
                this.SetFontUnderline(value);
            }
        }

        /// <summary>
        /// Gets/sets the Appearance of the label
        /// </summary>
        [DefaultValue(AppearanceConstants.cc3D)]
        public AppearanceConstants Appearance
        {
            get
            {
                return this.appearance;
            }
            set
            {
                this.appearance = value;
                //changing appearance in InitializeComponent should not change the BackColor to white for 2D
                Form f = this.FindForm();
                if (f != null && f.Created)
                {
                    if (this.appearance == AppearanceConstants.cc3D)
                    {
                        //base.FlatStyle = Wisej.Web.FlatStyle.Standard;
                        this.BackColor = SystemColors.Control;
                        if (this.BorderStyle == 1)
                        {
                            base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                        }
                        else
                        {
                            base.BorderStyle = Wisej.Web.BorderStyle.None;
                        }
                    }
                    else
                    {
                        //base.FlatStyle = Wisej.Web.FlatStyle.Flat;
                        this.BackColor = Color.White;
                        if (this.BorderStyle == 1)
                        {
                            base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                        }
                        else
                        {
                            base.BorderStyle = Wisej.Web.BorderStyle.None;
                        }
                    }
                }
            }
        }

        [Obsolete("Text in the Label control wraps by default and cannot be changed. Added for compatibility")]
        [DefaultValue(false)]
        public bool WordWrap { get; set; }

        /// <summary>
        /// Gets/sets the mouse icon to be displayed when the mouse is over the label.
        /// </summary>
        [DefaultValue(null)]
        public Image MouseIcon
        {
            get
            {
                return mouseIcon;
            }
            set
            {
                SetMouseIcon(value);
            }
        }

        //
        // Summary:
        //     Gets or sets the background color for the control.
        //
        public new Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
                backColor = this.BackColor;
            }
        }

        /// <summary>
        /// Gets/sets the style of the border
        /// </summary>
        [DefaultValue(0)]
        public new int BorderStyle
        {
            get
            {
                return borderStyle;
            }
            set
            {
                borderStyle = value;
                if (borderStyle == 0)
                {
                    base.BorderStyle = Wisej.Web.BorderStyle.None;
                }
                else if (borderStyle == 1)
                {
                    if (this.Appearance == AppearanceConstants.cc3D)
                    {
                        base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                    }
                    else
                    {
                        base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                    }
                }
                this.Invalidate();
            }
        }

        /// <summary>
        /// Indicates whether a label or the background of a character control is transparent or opaque.
        /// </summary>
        [DefaultValue(1)]
        public int BackStyle
        {
            get
            {
                return this.backStyle;
            }
            set
            {
                this.backStyle = value;
                if (this.backStyle == 1)
                {
                    base.BackColor = backColor;
                }
                else
                {
                    base.BackColor = Color.Transparent;
                }
            }
        }

        /// <summary>
        /// Gets/sets a value that binds a control to a field in the current data set.
        /// </summary>
        [DefaultValue("")]
        public string DataField
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [DefaultValue(true)]
        public bool Capitalize { get => capitalize; set => capitalize = value; }

        /// <summary>
        /// Adds ellipses to a string that has been truncated
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [DefaultValue(false)]
        public  bool UseEllipses { get; set; }

        /// <summary>
        /// Performs truncation of strings too long to fit. Only used if autosize is false
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [DefaultValue(false)]
        public bool TruncateTextToWidth { get; set; }
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
		/// Forces the control to update corresponding client widget.
		/// When in DesignMode it forces a full redraw of the designer surface 
		/// for this control, including the non-client areas such as the caption of forms or panels.
		/// </summary>
        public new void Refresh() {
            base.Refresh();
            FCUtils.ApplicationUpdate(this);
        }

        /// <summary>
        /// User-defined conversion from FCLabel to string
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        public static implicit operator string(FCLabel label)
        {
            return label.Text;
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        /// <summary>
        /// Begins a drag-and-drop operation.
        /// </summary>
        public void Drag()
        {
            this.DoDragDrop(this, System.Windows.Forms.DragDropEffects.Move);
        }

        #endregion

        #region Internal Methods

        public void TextMeasureCallback(dynamic obj)
        {
           base.Text = (string) obj;
        }
        #endregion

        #region Protected Methods

        protected override void OnMouseDown(MouseEventArgs e)
        {
            Point transformedPoint = TransformCoordinate(e.X, e.Y);
            MouseEventArgs evArg = new MouseEventArgs(e.Button, e.Clicks, transformedPoint.X, transformedPoint.Y, e.Delta);
            base.OnMouseDown(evArg);
            if (DragMode == DragModeConstants.vbAutomatic)
            {
                this.DoDragDrop(this, DragDropEffects.All);
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Point transformedPoint = TransformCoordinate(e.X, e.Y);
            MouseEventArgs evArg = new MouseEventArgs(e.Button, e.Clicks, transformedPoint.X, transformedPoint.Y, e.Delta);
            base.OnMouseMove(evArg);
        }

        protected override void OnDragDrop(DragEventArgs drgevent)
        {
            //CHE: get position relative to sender control not to screen
            Point senderRelativePoint = this.PointToClient(new Point(drgevent.X, drgevent.Y));
            Point transformedPoint = TransformCoordinate(senderRelativePoint.X, senderRelativePoint.Y);
            // TODO
            //DragEventArgs dragEvent = new DragEventArgs(drgevent.Data, drgevent.KeyState, transformedPoint.X, transformedPoint.Y, drgevent.AllowedEffect, drgevent.Effect);
            //base.OnDragDrop(dragEvent);            
        }

        protected override bool ProcessMnemonic(char charCode)
        {
            bool flag = base.ProcessMnemonic(charCode);
            //CHE: fix Winform bug: pressing mnemonic for a label should focus the next control in tab order list but is not skiping control with TabStop=false as it should
            Form form = this.FindForm();
            if (form != null)
            {
                Control ctrl = this.FindForm().ActiveControl;
                if (flag && !ctrl.TabStop && ctrl.Parent != null)
                {
                    ctrl.Parent.SelectNextControl(ctrl, true, true, true, true);
                }
            }
            return flag;
        }

        #endregion

        #region Private Methods

        private Point TransformCoordinate(int X, int Y)
        {
            Point result = new Point();
            ScaleModeConstants toScale = fcGraphics.GetParentScaleMode(this);


            //using (Graphics g = this.CreateGraphics())
            {
                fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                result.X = Convert.ToInt32(fcGraphics.ScaleX(X, ScaleModeConstants.vbPixels, toScale));
                result.Y = Convert.ToInt32(fcGraphics.ScaleY(Y, ScaleModeConstants.vbPixels, toScale));
            }

            return result;
        }



        /// <summary>
        /// Sets the cursor to the label object
        /// </summary>
        /// <param name="mouseCursor">An object which can be a Cursor, an Image or a Bitmap</param>
        private void SetMouseIcon(Image mouseCursor)
        {
            mouseIcon = mouseCursor;
            if (DesignMode || mouseCursor == null)
            {
                return;
            }
            base.Cursor = new Cursor(mouseCursor);
        }

        #endregion
    }
}
