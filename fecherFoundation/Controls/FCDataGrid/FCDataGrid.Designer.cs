﻿namespace fecherFoundation
{
    partial class FCDataGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fcButton1 = new fecherFoundation.FCButton();
            this.Grid = new fecherFoundation.FCDataGridInternal();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // fcButton1
            // 
            this.fcButton1.BackColor = System.Drawing.SystemColors.Control;
            this.fcButton1.BackgroundImageLayout = Wisej.Web.ImageLayout.Center;
            this.fcButton1.Dock = Wisej.Web.DockStyle.Top;
            this.fcButton1.Location = new System.Drawing.Point(0, 0);
            this.fcButton1.Name = "fcButton1";
            this.fcButton1.Size = new System.Drawing.Size(150, 28);
            this.fcButton1.TabIndex = 0;
            this.fcButton1.TabStop = false;
            this.fcButton1.Visible = false;
            // 
            // fcDataGridInternal1
            // 
            this.Grid.AllowUserToAddRows = false;
            this.Grid.AllowUserToDeleteRows = false;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grid.Dock = Wisej.Web.DockStyle.Fill;
            this.Grid.Location = new System.Drawing.Point(0, 28);
            this.Grid.Name = "fcDataGridInternal1";
            this.Grid.ReadOnly = true;
            this.Grid.RowHeight = 15F;
            this.Grid.RowTemplate.Height = 15;
            this.Grid.Size = new System.Drawing.Size(150, 122);
            this.Grid.TabIndex = 1;
            // 
            // FCDataGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.Controls.Add(this.Grid);
            this.Controls.Add(this.fcButton1);
            this.Name = "FCDataGrid";
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private FCButton fcButton1;
        public FCDataGridInternal Grid;
    }
}
