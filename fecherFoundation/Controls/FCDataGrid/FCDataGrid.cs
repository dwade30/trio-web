﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using System.Drawing.Design;

namespace fecherFoundation
{
    public partial class FCDataGrid : FCUserControl, ISupportInitialize
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors

        public FCDataGrid()
        {
            InitializeComponent();
            this.Caption = "";
        }

        #endregion

        #region Public Delegates

        public delegate void OnColResize(object sender, DataGridViewColumnEventArgs e);

        #endregion

        #region Public Events

        public event EventHandler Leave
        {
            add
            {
                this.Grid.Leave += value;
            }
            remove
            {
                this.Grid.Leave -= value;
            }
        }

        public event EventHandler Enter
        {
            add
            {
                this.Grid.Enter += value;
            }
            remove
            {
                this.Grid.Enter -= value;
            }
        }

        public event KeyPressEventHandler KeyPress
        {
            add
            {
                this.Grid.KeyPress += value;
            }
            remove
            {
                this.Grid.KeyPress -= value;
            }
        }

        public event EventHandler Click
        {
            add
            {
                this.Grid.Click += value;
            }
            remove
            {
                this.Grid.Click -= value;
            }
        }

        public event EventHandler DoubleClick
        {
            add
            {
                this.Grid.DoubleClick += value;
            }
            remove
            {
                this.Grid.DoubleClick -= value;
            }
        }

        public event DataGridViewCellMouseEventHandler ColumnHeaderMouseClick
        {
            add
            {
                this.Grid.ColumnHeaderMouseClick += value;
            }
            remove
            {
                this.Grid.ColumnHeaderMouseClick -= value;
            }
        }

        public event EventHandler SelectionChanged
        {
            add
            {
                this.Grid.SelectionChanged += value;
            }
            remove
            {
                this.Grid.SelectionChanged -= value;
            }
        }

        public event OnColResize ColResize
        {
            add
            {
                this.Grid.ColResize += value;
            }
            remove
            {
                this.Grid.ColResize -= value;
            }
        }

        #endregion

        #region Private Events
        #endregion

        #region Enums

        /// <summary>
        /// FCDataGrid.MarqueeStyle
        /// </summary>
        public enum MarqueeStyleSettings
        {
            dbgDottedCellBorder = 0,
            dbgSolidCellBorder = 1,
            dbgHighlightCell = 2,
            dbgHighlightRow = 3,
            dbgHighlightRowRaiseCell = 4,
            dbgNoMarquee = 5,
            dbgFloatingEditor = 6
        }

        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DataGridViewRow CurrentRow
        {
            get
            {
                return this.Grid.CurrentRow;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor("Wisej.Web.Design.DataGridViewColumnCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [MergableProperty(false)]
        public DataGridViewColumnCollection Columns
        {
            get
            {
                return this.Grid.Columns;
            }
        }

        [DefaultValue("")]
        public string Caption
        {
            get
            {
                return this.fcButton1.Text;
            }
            set
            {
                this.fcButton1.Text = value;
                if (string.IsNullOrEmpty(this.fcButton1.Text))
                {
                    this.fcButton1.Visible = false;
                }
                else
                {
                    this.fcButton1.Visible = true;
                }
            }
        }

        [DefaultValue("")]
        public string DataMember
        {
            get
            {
                return this.Grid.DataMember;
            }
            set
            {
                this.Grid.DataMember = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public dynamic DataSourceOriginal
        {
            get
            {
                return this.Grid.DataSourceOriginal;
            }
            set
            {
                this.Grid.DataSourceOriginal = value;
            }
        }

        [DefaultValue(true)]
        public bool AllowUpdate
        {
            get
            {
                return this.Grid.AllowUpdate;
            }
            set
            {
                this.Grid.AllowUpdate = value;
            }
        }

        [DefaultValue(true)]
        public bool AllowArrows
        {
            get
            {
                return this.Grid.AllowArrows;
            }
            set
            {
                this.Grid.AllowArrows = value;
            }
        }

        [DefaultValue(false)]
        public bool AllowDelete
        {
            get
            {
                return this.Grid.AllowDelete;
            }
            set
            {
                this.Grid.AllowDelete = value;
            }
        }

        [DefaultValue(false)]
        public bool AllowAddNew
        {
            get
            {
                return this.Grid.AllowAddNew;
            }
            set
            {
                this.Grid.AllowAddNew = value;
            }
        }

        /// <summary>
        /// 0 based
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Row
        {
            get
            {
                return this.Grid.Row;
            }
            set
            {
                this.Grid.Row = value;
            }
        }

        /// <summary>
        /// Returns or sets the height of all rows in the DataGrid control. RowHeight is always in the same unit of measure as the container for the DataGrid control.
        /// Remarks
        /// Users can change the RowHeight of any row at run time by placing the mouse pointer on a gridline between rows and dragging.
        /// </summary>
        [DefaultValue(15)]
        public float RowHeight
        {
            get
            {
                return this.Grid.RowHeight;
            }
            set
            {
                this.Grid.RowHeight = value;
            }
        }

        [DefaultValue(true)]
        public bool AllowRowSizing
        {
            get
            {
                return this.Grid.AllowRowSizing;
            }
            set
            {
                this.Grid.AllowRowSizing = value;
            }
        }

        [DefaultValue(true)]
        public bool AllowFocus
        {
            get
            {
                return this.Grid.AllowFocus;
            }
            set
            {
                this.Grid.AllowFocus = value;
            }
        }

        [DefaultValue(true)]
        public bool AllowSizing
        {
            get
            {
                return this.Grid.AllowSizing;
            }
            set
            {
                this.Grid.AllowSizing = value;
            }
        }

        /// <summary>
        /// Returns or sets a value indicating if record selectors are displayed in the DataGrid control or Split object.
        /// Record selectors, when displayed, appear to the left of the rows in the grid or split.
        /// If a grid contains multiple splits, then setting its RecordSelectors property has the same effect as setting the RecordSelectors property of each split individually.
        /// Note When the user selects a row by clicking its record selector, the bookmark of the selected row is added to the SelBookmarks collection.
        /// </summary>
        [DefaultValue(true)]
        public bool RecordSelectors
        {
            get
            {
                return this.Grid.RecordSelectors;
            }
            set
            {
                this.Grid.RecordSelectors = value;
            }
        }

        [DefaultValue(true)]
        public bool ColumnHeaders
        {
            get
            {
                return this.Grid.ColumnHeaders;
            }
            set
            {
                this.Grid.ColumnHeaders = value;
            }
        }

        /// <summary>
        /// Sets or returns the Marquee style for the DataGrid control or Split object.
        /// dbgDottedCellBorder 0 The current cell within the current row will be highlighted by drawing a dotted border around the cell. In Microsoft Windows terminology, 
        /// this is usually called a focus rectangle.
        /// dbgSolidCellBorder 1 The current cell within the current row will be highlighted by drawing a solid box around the current cell.This is more visible than the 
        /// dotted cell border, especially when 3-D divider properties are used for the grid. 
        /// dbgHighlightCell 2 The entire current cell will be highlighted by inverting the colors within the cell. This provides a very distinctive block-style highlight for the current cell.
        /// dbgHighlightRow 3 The entire row containing the current cell will be highlighted by inverting the colors within the row.In this mode, it is not possible to visually determine which cell 
        /// is the current cell, only the current row. When the grid or split is not editable, this setting is often preferred, since cell position is then irrelevant. 
        /// dbgHighlightRowRaiseCell 4 The entire row will be highlighted. The current cell within the row will be "raised" so that it appears distinctive.This setting doesn't appear clearly with 
        /// all background color and divider settings. The best effect is achieved by using 3-D dividers and a light gray background. 
        /// dbgNoMarquee 5 The marquee will not be shown. This setting is useful for cases where the current row is irrelevant, or where you don't want to draw the user's attention to the grid until 
        /// necessary.
        /// dbgFloatingEditor 6 The current cell will be highlighted by a floating text editor window with a blinking caret (as in Microsoft Access). This is the default setting.
        /// Remarks
        /// If a grid contains multiple splits, then setting its MarqueeStyle property has the same effect as setting the MarqueeStyle property of each split individually.
        /// Note   If the floating editor marquee setting is in effect and the current cell contains radio buttons or graphics, then a dotted focus rectangle will be displayed.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public MarqueeStyleSettings MarqueeStyle
        {
            get
            {
                return this.Grid.MarqueeStyle;
            }
            set
            {
                this.Grid.MarqueeStyle = value;
            }
        }

        [DefaultValue(false)]
        public bool Locked
        {
            get
            {
                return this.Grid.Locked;
            }
            set
            {
                this.Grid.Locked = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public object OcxState
        {
            get
            {
                return this.Grid.OcxState;
            }
            set
            {
                this.Grid.OcxState = value;
            }
        }

        /// <summary>
        /// Returns the approximate number of rows in the grid.
        /// This property returns the approximate row count used by the grid to calibrate the vertical scroll bar.
        /// ypically, the ApproxCount property is used to improve the accuracy of the vertical scroll bar. This is particularly useful for situations where the number of rows 
        /// is known in advance, such as when a grid is used in conjunction with an array.
        /// Note Getting the ApproxCount property will query the underlying data source.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ApproxCount
        {
            get
            {
                return this.Grid.ApproxCount;
            }
        }
        public object DataSource
        {
            get
            {
                return this.Grid.DataSource;
            }
            set
            {
                this.Grid.DataSource = value;
            }
        }

        /// <summary>
        /// 0 based
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Col
        {
            get
            {
                return this.Grid.Col;
            }
            set
            {
                this.Grid.Col = value;
            }
        }

        /// <summary>
        /// Sets or returns the visibility of the current cell. Not available at design time.
        /// True The current cell (indicated by the Bookmark and Col properties) is visible within the displayed area of a grid or split.
        /// False The cell is not visible. 
        /// Remarks
        /// For a DataGrid control, setting the CurrentCellVisible property to True causes the grid to scroll so that the current cell is brought into view.If a grid 
        /// contains multiple splits, then the current cell becomes visible in each split.
        /// For a Split object, setting the CurrentCellVisible property to True makes the current cell visible in that split only.
        /// In all cases, setting this property to False is meaningless and is ignored.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CurrentCellVisible
        {
            get
            {
                return this.Grid.CurrentCellVisible;
            }
            set
            {
                this.Grid.CurrentCellVisible = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelStart
        {
            get
            {
                return this.Grid.SelStart;
            }
            set
            {
                this.Grid.SelStart = value;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        public void ClearSelCols()
        {
            this.Grid.ClearSelCols();
        }

        public void BeginInit()
        {
            ((ISupportInitialize)Grid).BeginInit();
        }

        public void EndInit()
        {
            ((ISupportInitialize)Grid).EndInit();
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
