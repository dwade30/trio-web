﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;
using System.Data;
using fecherFoundation.DataBaseLayer.ADO;

namespace fecherFoundation
{
    /// <summary>
    /// MSDataGridLib
    /// https://msdn.microsoft.com/en-us/library/aa260534(v=vs.60).aspx
    /// </summary>
    public class FCDataGridInternal : DataGridView
    {
        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;
        private string caption = "";
        private FCDataGrid.MarqueeStyleSettings marqueeStyle = FCDataGrid.MarqueeStyleSettings.dbgFloatingEditor;

        private string dataMember = "";
        private dynamic dataSourceOriginal;

        private int crtCol = 0;
        private int crtRow = 0;

        private bool internalResize = false;

        private bool allowFocus = true;
        private bool locked = false;

        #endregion

        #region Constructors

        public FCDataGridInternal()
        {
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            //CHE: set default designer values
            this.AllowAddNew = false;
            this.AllowDelete = false;
            this.AllowUpdate = true;
            this.AllowRowSizing = true;
            this.AllowSizing = true;
            this.AllowFocus = true;
            this.Locked = false;
            this.RecordSelectors = true;
            this.MarqueeStyle = FCDataGrid.MarqueeStyleSettings.dbgFloatingEditor;
            this.ColumnHeaders = true;
            this.RowHeight = 15;
            this.AllowArrows = true;
            //CHE: set RowHeadersWidth, by default in .NET is 41 which is larger than in VB6
            this.RowHeadersWidth = 20;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        public event FCDataGrid.OnColResize ColResize;

        #endregion

        #region Enums
        #endregion

        #region Properties

        [DefaultValue("")]
        public new string DataMember
        {
            get
            {
                return dataMember;
            }
            set
            {
                dataMember = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public dynamic DataSourceOriginal
        {
            set
            {
                if (value == null)
                {
                    return;
                }
                DataTable data = new DataTable();
                string dataMember = DataMember;
                value.Class_GetDataMember(ref dataMember, ref data);
                this.dataSourceOriginal = value;
                BindingSource bs = new BindingSource();
                bs.DataSource = data;
                this.DataSource = bs;
                value.AddBindingSource(this.DataMember, bs, this);
            }
            get
            {
                return this.dataSourceOriginal;
            }
        }

        [DefaultValue(true)]
        public bool AllowUpdate
        {
            get
            {
                return !base.ReadOnly;
            }
            set
            {
                base.ReadOnly = !value;
            }
        }

        [DefaultValue(true)]
        public bool AllowArrows
        { //TODO: ZSA
            get; set;
        }

        [DefaultValue(false)]
        public bool AllowDelete
        {
            get
            {
                return base.AllowUserToDeleteRows;
            }
            set
            {
                base.AllowUserToDeleteRows = value;
            }
        }

        [DefaultValue(false)]
        public bool AllowAddNew
        {
            get
            {
                return base.AllowUserToAddRows;
            }
            set
            {
                base.AllowUserToAddRows = value;
            }
        }

        /// <summary>
        /// 0 based
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Row
        {
            get
            {
                return crtRow;
            }
            set
            {
                crtRow = value;

                if (this.CurrentCell != null)
                {
                    DataGridViewCell cell = this[this.CurrentCell.ColumnIndex, crtRow];
                    this.CurrentCell = cell;
                }
                else
                {
                    //at least one column
                    if (this.ColumnCount > 0)
                    {
                        DataGridViewCell cell = this[FirstVisibleCol, crtRow];
                        if (cell.Visible)
                        {
                            this.CurrentCell = cell;
                        }
                        else
                        {
                            this.CurrentCell = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns or sets the height of all rows in the DataGrid control. RowHeight is always in the same unit of measure as the container for the DataGrid control.
        /// Remarks
        /// Users can change the RowHeight of any row at run time by placing the mouse pointer on a gridline between rows and dragging.
        /// </summary>
        [DefaultValue(15)]
        public float RowHeight
        {
            get
            {
                return this.RowTemplate.Height;
            }
            set
            {
                if (value == 0)
                {
                    value = this.Font.Size + 11;
                }
                this.RowTemplate.Height = Convert.ToInt32(value);
            }
        }

        [DefaultValue(true)]
        public bool AllowRowSizing
        {
            get
            {
                return base.AllowUserToResizeRows;
            }
            set
            {
                base.AllowUserToResizeRows = false;
            }
        }

        [DefaultValue(true)]
        public bool AllowFocus
        {
            get
            {
                return allowFocus;
            }
            set
            {
                allowFocus = value;
            }
        }

        [DefaultValue(true)]
        public bool AllowSizing
        {
            get
            {
                return base.AllowUserToResizeColumns;
            }
            set
            {
                base.AllowUserToResizeColumns = value;
            }
        }

        /// <summary>
        /// Returns or sets a value indicating if record selectors are displayed in the DataGrid control or Split object.
        /// Record selectors, when displayed, appear to the left of the rows in the grid or split.
        /// If a grid contains multiple splits, then setting its RecordSelectors property has the same effect as setting the RecordSelectors property of each split individually.
        /// Note When the user selects a row by clicking its record selector, the bookmark of the selected row is added to the SelBookmarks collection.
        /// </summary>
        [DefaultValue(true)]
        public bool RecordSelectors
        {
            get
            {
                return base.RowHeadersVisible;
            }
            set
            {
                base.RowHeadersVisible = value;
            }
        }

        [DefaultValue(true)]
        public bool ColumnHeaders
        {
            get
            {
                return base.ColumnHeadersVisible;
            }
            set
            {
                base.ColumnHeadersVisible = value;
            }
        }

        /// <summary>
        /// Sets or returns the Marquee style for the DataGrid control or Split object.
        /// dbgDottedCellBorder 0 The current cell within the current row will be highlighted by drawing a dotted border around the cell. In Microsoft Windows terminology, 
        /// this is usually called a focus rectangle.
        /// dbgSolidCellBorder 1 The current cell within the current row will be highlighted by drawing a solid box around the current cell.This is more visible than the 
        /// dotted cell border, especially when 3-D divider properties are used for the grid. 
        /// dbgHighlightCell 2 The entire current cell will be highlighted by inverting the colors within the cell. This provides a very distinctive block-style highlight for the current cell.
        /// dbgHighlightRow 3 The entire row containing the current cell will be highlighted by inverting the colors within the row.In this mode, it is not possible to visually determine which cell 
        /// is the current cell, only the current row. When the grid or split is not editable, this setting is often preferred, since cell position is then irrelevant. 
        /// dbgHighlightRowRaiseCell 4 The entire row will be highlighted. The current cell within the row will be "raised" so that it appears distinctive.This setting doesn't appear clearly with 
        /// all background color and divider settings. The best effect is achieved by using 3-D dividers and a light gray background. 
        /// dbgNoMarquee 5 The marquee will not be shown. This setting is useful for cases where the current row is irrelevant, or where you don't want to draw the user's attention to the grid until 
        /// necessary.
        /// dbgFloatingEditor 6 The current cell will be highlighted by a floating text editor window with a blinking caret (as in Microsoft Access). This is the default setting.
        /// Remarks
        /// If a grid contains multiple splits, then setting its MarqueeStyle property has the same effect as setting the MarqueeStyle property of each split individually.
        /// Note   If the floating editor marquee setting is in effect and the current cell contains radio buttons or graphics, then a dotted focus rectangle will be displayed.
        /// </summary>
        [DefaultValue(FCDataGrid.MarqueeStyleSettings.dbgFloatingEditor)]
        public FCDataGrid.MarqueeStyleSettings MarqueeStyle
        {
            get
            {
                return marqueeStyle;
            }
            set
            {
                marqueeStyle = value;
                switch (value)
                {
                    case FCDataGrid.MarqueeStyleSettings.dbgHighlightRow:
                        {
                            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                            break;
                        }
                    default:
                        {
                            this.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
                            break;
                        }
                }
            }
        }

        [DefaultValue(false)]
        public bool Locked
        {
            get
            {
                return locked;
            }
            set
            {
                locked = value;
                this.ReadOnly = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        // TODO:CHE - check properties needed from frx
        public object OcxState { get; set; }

        /// <summary>
        /// Returns the approximate number of rows in the grid.
        /// This property returns the approximate row count used by the grid to calibrate the vertical scroll bar.
        /// ypically, the ApproxCount property is used to improve the accuracy of the vertical scroll bar. This is particularly useful for situations where the number of rows 
        /// is known in advance, such as when a grid is used in conjunction with an array.
        /// Note Getting the ApproxCount property will query the underlying data source.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ApproxCount
        {
            get
            {
                return base.Rows.Count;
            }
        }

        public new object DataSource
        {
            get
            {
                return base.DataSource;
            }
            set
            {
                //CHE: when AutoGenerateColumns is set all columns must be removed to be recreated according to new DataSource
                if (value != null && this.AutoGenerateColumns)
                {
                    base.DataSource = null;
                    this.Columns.Clear();
                }

                base.DataSource = value;
            }
        }

        /// <summary>
        /// 0 based
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Col
        {
            get
            {
                return crtCol;
            }
            set
            {
                crtCol = value;

                if (this.Columns[crtCol].Visible)
                {
                    if (CurrentRow != null)
                    {
                        this.CurrentCell = this.CurrentRow.Cells[crtCol];
                    }
                }
            }
        }

        /// <summary>
        /// Sets or returns the visibility of the current cell. Not available at design time.
        /// True The current cell (indicated by the Bookmark and Col properties) is visible within the displayed area of a grid or split.
        /// False The cell is not visible. 
        /// Remarks
        /// For a DataGrid control, setting the CurrentCellVisible property to True causes the grid to scroll so that the current cell is brought into view.If a grid 
        /// contains multiple splits, then the current cell becomes visible in each split.
        /// For a Split object, setting the CurrentCellVisible property to True makes the current cell visible in that split only.
        /// In all cases, setting this property to False is meaningless and is ignored.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CurrentCellVisible
        {
            get
            {
                if (this.CurrentCell != null)
                {
                    return this.CurrentCell.Visible;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (value)
                {
                    if (this.Row < 0 || this.Col < 0)
                    {
                        return;
                    }
                    else if (this.Rows.Count > Row && this.Rows[Row].Cells[Col].Visible)
                    {
                        // TODO
                        //this.FirstDisplayedScrollingRowIndex = this.Row;
                        //this.FirstDisplayedScrollingColumnIndex = this.Col;
                    }
                    return;
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelStart
        {
            get
            {
                TextBox edit = base.EditingControl as TextBox;
                if (edit != null)
                {
                    return edit.SelectionStart;
                }
                return 0;
            }
            set
            {
                TextBox edit = base.EditingControl as TextBox;
                if (edit != null)
                {
                    edit.SelectionStart = value;
                }
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        #endregion

        #region Internal Properties

        internal int FirstVisibleCol
        {
            get
            {
                foreach (DataGridViewColumn col in base.Columns)
                {
                    if (col.Visible)
                    {
                        return col.Index;
                    }
                }
                return -1;
            }
        }

        #endregion

        #region Public Methods

        public void ClearSelCols()
        {
            foreach (DataGridViewColumn col in base.Columns)
            {
                col.Selected = false;
            }
        }

        #endregion

        #region Protected Methods

        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            base.OnColumnAdded(e);
            //CHE: disable sorting - DataGrid in VB6 does not have sorting by default
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            //CHE: set default width for column in case of AutoGenerateColumns
            if (this.AutoGenerateColumns)
            {
                e.Column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                int colWidth = e.Column.Width;
                e.Column.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
                internalResize = true;
                e.Column.Width = colWidth;
                internalResize = false;
            }
        }

        protected override void OnColumnWidthChanged(DataGridViewColumnEventArgs e)
        {
            //CHE: do not execute ColResize when form is null (e.g. from InitializeComponent)
            if (this.FindForm() != null && ColResize != null && !internalResize)
            {
                ColResize(this, e);
            }
            base.OnColumnWidthChanged(e);
        }

        protected override void OnRowHeaderMouseClick(DataGridViewCellMouseEventArgs e)
        {
            ChangePositionOnDataTable();
            base.OnRowHeaderMouseClick(e);
        }

        protected override void OnCurrentCellChanged(EventArgs e)
        {
            ChangePositionOnDataTable();
            base.OnCurrentCellChanged(e);
        }

        protected override void OnDoubleClick(EventArgs e)
        {
            ChangePositionOnDataTable();
            base.OnDoubleClick(e);
        }

        protected override void OnUserDeletingRow(DataGridViewRowCancelEventArgs e)
        {
            ChangePositionOnDataTable();
            base.OnUserDeletingRow(e);
        }

        protected override void OnDataSourceChanged(EventArgs e)
        {
            base.OnDataSourceChanged(e);
            DataTable dataTable = this.DataSource as DataTable;
            if (dataTable != null)
            {
                dataTable.SetParentGrid(this);
            }
        }

        #endregion

        #region Private Methods

        private void ChangePositionOnDataTable(bool update = false)
        {

            DataTable dataTable = this.DataSource as DataTable;

            //change position on datatable
            if (dataTable != null && this.CurrentRow != null)
            {
                if (this.CurrentRow.DataBoundItem != null)
                {
                    dataTable.SetIndex(GetDataBoundIndex(this.CurrentRow.Index));
                }
                else if (this.CurrentRow.IsNewRow)
                {
                    dataTable.SetIndex(this.CurrentRow.Index + 1);
                }
            }
        }

        public int GetDataBoundIndex(int rowIndex)
        {
            if (this.DataSource == null)
            {
                return 0;
            }

            if (rowIndex < 0 || rowIndex >= this.RowCount)
            {
                return 0;
            }

            DataRowView dataRow = this.Rows[rowIndex].DataBoundItem as DataRowView;

            //use dataRow.DataView.Table to search for Row index; dataRow.DataView contains only filtered and sorted data
            DataTable dataTable = dataRow.DataView.Table;
            int i = 0;

            for (i = 0; i < dataTable.Rows.Count; i++)
            {
                if (dataTable.Rows[i] == dataRow.Row)
                {
                    return i + 1;
                }
            }

            if (dataTable.DefaultView == null)
            {
                return 0;
            }

            //if user adds a new row in the table, the new row appears only in DefaultView until the row gets validated
            for (i = 0; i < dataTable.DefaultView.Count; i++)
            {
                if (dataTable.DefaultView[i] == dataRow)
                {
                    return i + 1;
                }
            }

            return 0;
        }

        #endregion
    }
}
