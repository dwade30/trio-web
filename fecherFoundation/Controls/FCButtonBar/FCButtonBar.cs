﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.ButtonBar;

namespace fecherFoundation
{
    public partial class FCButtonBar : ButtonBar
    {

        // add groups count, large/small image width/height

        #region Enums

        public enum jsbbButtonStyleConstants : int
        {
            jsbbNone = 0,
            jsbbSingle = 1,
            jsbbDouble = 2
        }

        #endregion

        #region Members
        private jsbbButtonStyleConstants buttonStyle = jsbbButtonStyleConstants.jsbbDouble;
        #endregion

        #region Properties

        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentX { get; set; }
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentY { get; set; }

        /// <summary>
        /// SmallImagesCount gets/sets the SmallImageList image count
        /// If the SmallImageList image count was 0 before setting the SmallImagesCount property, the SmallImageList will be filled with
        /// blank bitmaps to the size of the SmallImagesCount value
        /// </summary>
        public int SmallImagesCount
        {
            get
            {
                return this.SmallImageList.Images.Count;
            }
            set
            {
                FillImageList(this.SmallImageList, value);
            }
        }

        /// <summary>
        /// LargeImagesCount gets/sets the LargeImageList image count
        /// If the SmallImageList image count was 0 before setting the LargeImagesCount property, the LargeImageList will be filled with
        /// blank bitmaps to the size of the LargeImagesCount value
        /// </summary>
        public int LargeImagesCount
        {
            get
            {
                return this.LargeImageList.Images.Count;
            }
            set
            {
                FillImageList(this.LargeImageList, value);
            }
        }

        /// <summary>
        /// ButtonStyle gets/sets if the ButtonBar will keep the item selection.
        /// If the ButtonGroupStyle is set to true, the button bar will display the clicked item as selected.
        /// If the ButtonGroupStyle is set to false, the button bar will not display the clicked item as selected.
        /// Default = false
        /// </summary>
        [DefaultValue(false)]
        public bool ButtonGroupStyle
        {
            get
            {
                return this.KeepSelection;
            }
            set
            {
                this.KeepSelection = value;
            }
        }

        /// <summary>
        /// Button Style gets/sets the 3D appearance for button group headers.
        /// </summary>
        [DefaultValue(jsbbButtonStyleConstants.jsbbDouble)]
        public jsbbButtonStyleConstants ButtonStyle
        {
            get
            {
                return this.buttonStyle;
            }
            set
            {
                this.buttonStyle = value;
                if (this.buttonStyle == jsbbButtonStyleConstants.jsbbDouble)
                {
                    this.GroupAppearance = Janus.Windows.ButtonBar.GroupAppearance.PopUp;
                }
                else
                {
                    this.GroupAppearance = Janus.Windows.ButtonBar.GroupAppearance.Light3D;
                }
            }
        }

        /// <summary>
        /// Gets/sets the SmallImageList. If the SmallImageList was unassigned, it will be instantiated and returned empty
        /// </summary>
        public new ImageList SmallImageList
        {
            get
            {
                if (base.SmallImageList == null)
                {
                    base.SmallImageList = new ImageList();
                }
                return base.SmallImageList;
            }
            set
            {
                base.SmallImageList = value;
            }
        }

        /// <summary>
        /// Gets/sets the LargeImageList. If the LargeImageList was unassigned, it will be instantiated and returned empty
        /// </summary>
        public new ImageList LargeImageList
        {
            get
            {
                if (base.LargeImageList == null)
                {
                    base.LargeImageList = new ImageList();
                }
                return base.LargeImageList;
            }
            set
            {
                base.LargeImageList = value;
            }
        }

        /// <summary>
        /// Gets/sets the height of the small image list
        /// </summary>
        public int SmallImageHeight { get; set; }
        /// <summary>
        /// Gets/sets the width of the small image list
        /// </summary>
        public int SmallImageWidth { get; set; }
        /// <summary>
        /// Gets/sets the height of the large image list
        /// </summary>
        public int LargeImageHeight { get; set; }
        /// <summary>
        /// Gets/sets the width of the large image list
        /// </summary>
        public int LargeImageWidth { get; set; }

        public int GroupCount
        {
            get
            {
                return this.Groups.Count;
            }
            set
            {
                this.Groups.Clear();
                for (int count = 0; count < value; count++)
                {
                    this.Groups.Add(new ButtonBarGroup());
                }
            }
        }

        #endregion

        #region Constructor
        public FCButtonBar()
        {
            InitializeComponent();
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Fills an image list with "count" empty bitmaps
        /// </summary>
        /// <param name="imageList">ImageList to be filled</param>
        /// <param name="count">Number of bitmaps the ImageList should contain</param>
        private void FillImageList(ImageList imageList, int count)
        {
            imageList.Images.Clear();
            for (int i = 0; i < count; i++)
            {
                imageList.Images.Add(new Bitmap(1, 1));
            }
        }
        #endregion
    }
}
