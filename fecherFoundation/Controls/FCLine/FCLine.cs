using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// VB.Line
	/// </summary>
	public class FCLine : Line
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private float x1, x2, y1, y2;
        private DrawModeConstants drawMode = DrawModeConstants.VbCopyPen;

        private Color borderColor = System.Drawing.SystemColors.WindowText;
        private BorderStyleConstants borderStyle = BorderStyleConstants.vbBSSolid;

        #endregion

        #region Constructors

        public FCLine()
        {
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            this.BackColor = System.Drawing.SystemColors.WindowText;
            //CHE: set default value
            this.DrawMode = DrawModeConstants.VbCopyPen;
            this.BorderWidth = 1;
            this.BorderColor = System.Drawing.SystemColors.WindowText;
            this.BorderStyle = BorderStyleConstants.vbBSSolid;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum BorderStyleConstants
        {
            vbTransparent = 0,
            vbBSSolid = 1,
            vbBSDash = 2,
            vbBSDot = 3,
            vbBSDashDot = 4,
            vbBSDashDotDot = 5,
            vbBSInsideSolid = 6
        }

        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets a value that determines the appearance of output from graphics method or the appearance of a Shape or Line control.
        /// object.DrawMode [= number]
        /// Number	An integer that specifies appearance, as described in Settings.
        /// Settings
        /// The settings for number are:
        /// Constant	Setting	Description
        /// VbBlackness	1	Blackness.
        /// VbNotMergePen	2	Not Merge Pen Inverse of setting 15 (Merge Pen).
        /// VbMaskNotPen	3	Mask Not Pen Combination of the colors common to the background color and the inverse of the pen.
        /// VbNotCopyPen	4	Not Copy Pen Inverse of setting 13 (Copy Pen).
        /// VbMaskPenNot	5	Mask Pen Not Combination of the colors common to both the pen and the inverse of the display.
        /// VbInvert	6	Invert Inverse of the display color.
        /// VbXorPen	7	Xor Pen Combination of the colors in the pen and in the display color, but not in both.
        /// VbNotMaskPen	8	Not Mask Pen Inverse of setting 9 (Mask Pen).
        /// VbMaskPen	9	Mask Pen Combination of the colors common to both the pen and the display.
        /// VbNotXorPen	10	Not Xor Pen Inverse of setting 7 (Xor Pen).
        /// VbNop	11	Nop No operation output remains unchanged. In effect, this setting turns drawing off.
        /// VbMergeNotPen	12	Merge Not Pen Combination of the display color and the inverse of the pen color.
        /// VbCopyPen	13	Copy Pen (Default) Color specified by the ForeColor property.
        /// VbMergePenNot	14	Merge Pen Not Combination of the pen color and the inverse of the display color.
        /// VbMergePen	15	Merge Pen Combination of the pen color and the display color.
        /// VbWhiteness	16	Whiteness.
        /// Remarks
        /// Use this property to produce visual effects with Shape or Line controls or when drawing with the graphics methods. Visual Basic compares each pixel in the draw pattern to the corresponding 
        /// pixel in the existing background and then applies bit-wise operations. For example, setting 7 (Xor Pen) uses the Xor operator to combine a draw pattern pixel with a background pixel.
        /// The exact effect of a DrawMode setting depends on the way the color of a line drawn at run time combines with colors already on the screen. Settings 1, 6, 7, 11, 13, and 16 yield the most 
        /// predictable results.
        /// </summary>
        [DefaultValue(DrawModeConstants.VbCopyPen)]
        public DrawModeConstants DrawMode
        {
            // TODO:CHE
            get
            {
                return this.drawMode;
            }
            set
            {
                this.drawMode = value;
            }
        }

        /// <summary>
        /// Returns or sets the width of a control's border.
        /// object.BorderWidth [= number]
        /// number	A numeric expression from 1 to 8192, inclusive.
        /// Remarks
        /// Use the BorderWidth and BorderStyle properties to specify the kind of border you want for a Line or Shape control. The following table shows the effect of BorderStyle settings on 
        /// the BorderWidth property:
        /// BorderStyle	Effect on BorderWidth
        /// 0	BorderWidth setting is ignored.
        /// 15	The border width expands from the center of the border; the height and width of the control are measured from the center of the border.
        /// 6	The border width expands inward on the control from the outside of the border; the height and width of the control are measured from the outside of the border.
        /// If the BorderWidth property setting is greater than 1, the only effective settings of BorderStyle are 1 (Solid) and 6 (Inside Solid).
        /// </summary>
        [DefaultValue(1)]
        public short BorderWidth
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the color of an object's border.
        /// object.BorderColor [= color]
        /// color	A value or constant that determines the border color, as described in Settings.
        /// Settings
        /// Visual Basic uses the Microsoft Windows operating environment red-green-blue (RGB) color scheme. The settings for color are:
        /// Setting	Description
        /// Normal RGB colors	Colors specified using the Color palette or by using the RGB or QBColor functions in code.
        /// System default colors	Colors specified by system color constants listed in the Visual Basic (VB) object library in the Object Browser. The system default color is specified by the 
        /// vbWindowText constant. The Windows operating environment substitutes the user's choices as specified in the Control Panel  settings.
        /// Remarks
        /// The valid range for a normal RGB color is 0 to 16,777,215 (&HFFFFFF). The high byte of a number in this range equals 0; the lower 3 bytes, from least to most significant byte, 
        /// determine the amount of red, green, and blue, respectively. The red, green, and blue components are each represented by a number between 0 and 255 (&HFF). If the high byte isn't 0, 
        /// Visual Basic uses the system colors, as defined in the user's Control Panel settings and by constants listed in the Visual Basic (VB) object library in the Object Browser.
        /// </summary>
        [DefaultValue(0)]
        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
            set
            {
                borderColor = value;
                this.BackColor = borderColor;
            }
        }

        /// <summary>
        /// Returns or sets the border style for an object. For the Form object and the TextBox control, read-only at run time.
        /// Syntax
        /// object.BorderStyle = [value]
        /// The BorderStyle property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	A value or constant that determines the border style, as described in Settings.
        /// Settings
        /// The BorderStyle property settings for a Form object are:
        /// Constant	Setting	Description
        /// vbBSNone	0	None (no border or border-related elements).
        /// vbFixedSingle	1	Fixed Single. Can include Control-menu box, title bar, Maximize button, and Minimize button. Resizable only using Maximize and Minimize buttons.
        /// vbSizable	2	(Default) Sizable. Resizable using any of the optional border elements listed for setting 1.
        /// vbFixedDouble	3	Fixed Dialog. Can include Control-menu box and title bar; can't include Maximize or Minimize buttons. Not resizable.
        /// vbFixedToolWindow	4	Fixed ToolWindow. Displays a non-sizable window with a Close button and title bar text in a reduced font size. The form does not appear in the Windows taskbar.
        /// vbSizableToolWindow	5	Sizable ToolWindow. Displays a sizable window with a Close button and title bar text in a reduced font size. The form does not appear in the Windows taskbar.
        /// 
        ///The BorderStyle property settings for MS Flex Grid, Image, Label, OLE container, PictureBox, Frame, and TextBox controls are:
        ///Setting	Description
        ///0	(Default for Image and Label controls) None.
        ///1	(Default for MS Flex Grid, PictureBox, TextBox, and OLE container controls) Fixed Single.
        ///
        ///The BorderStyle property settings for Line and Shape controls are:
        ///Constant	Setting	Description
        ///vbTransparent	0	Transparent
        ///vbBSSolid	1	(Default) Solid. The border is centered on the edge of the shape.
        ///vbBSDash	2	Dash
        ///vbBSDot	3	Dot
        ///vbBSDashDot	4	Dash-dot
        ///vbBSDashDotDot	5	Dash-dot-dot
        ///vbBSInsideSolid	6	Inside solid. The outer edge of the border is the outer edge of the shape.
        ///
        /// Remarks
        /// For a form, the BorderStyle property determines key characteristics that visually identify a form as either a general-purpose window or a dialog box. Setting 3 (Fixed Dialog) is useful for 
        /// standard dialog boxes. Settings 4 (Fixed ToolWindow) and 5 (Sizable ToolWindow) are useful for creating toolbox-style windows.
        /// MDI child forms set to 2 (Sizable) are displayed within the MDI form in a default size defined by the Windows operating environment at run time. For any other setting, the form is displayed 
        /// in the size specified at design time.
        /// Changing the setting of the BorderStyle property of a Form object may change the settings of the MinButton, MaxButton, and ShowInTaskbar properties. When BorderStyle is set to 1 (Fixed Single) 
        /// or 2 (Sizable), the MinButton, MaxButton, and ShowInTaskbar properties are automatically set to True. When BorderStyle is set to 0 (None), 3 (Fixed Dialog), 4 (Fixed ToolWindow), or 5 (Sizable 
        /// ToolWindow), the MinButton, MaxButton, and ShowInTaskbar properties are automatically set to False.
        /// Note   If a form with a menu is set to 3 (Fixed Dialog), it is displayed with a setting 1 (Fixed Single) border instead.
        /// At run time, a form is either modal or modeless, which you specify using the Show method.
        /// </summary>
        [DefaultValue(BorderStyleConstants.vbBSSolid)]
        public new BorderStyleConstants BorderStyle
        {
            get 
            { 
                return borderStyle; 
            }
            set 
            { 
                borderStyle = value; 
            }
        }

        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentX { get; set; }
        
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentY { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float X1
        {
            get
            {
                return x1;
            }
            set
            {
                x1 = value;
                SimulateLine();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float X2
        {
            get
            {
                return x2;
            }
            set
            {
                x2 = value;
                SimulateLine();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Y1
        {
            get
            {
                return y1;
            }
            set
            {
                y1 = value;
                SimulateLine();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Y2
        {
            get
            {
                return y2;
            }
            set
            {
                y2 = value;
                SimulateLine();
            }
        }

        public int LineWidth { get; set; }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private void SimulateLine()
        {
            if (!DesignMode)
            {
                //for VB.Line
                if (y1 == y2)
                {
                    //horizontal line
                    this.Left = Convert.ToInt32(FCUtils.TwipsToPixelsX(x1));
                    this.Width = Convert.ToInt32(FCUtils.TwipsToPixelsX(x2 - x1));
                    this.Top = Convert.ToInt32(FCUtils.TwipsToPixelsY(y1));
                    this.Height = this.BorderWidth;
                }
                else if (x1 == x2)
                {
                    //vertical line
                    this.Left = Convert.ToInt32(FCUtils.TwipsToPixelsX(x1));
                    this.Width = this.BorderWidth;
                    this.Top = Convert.ToInt32(FCUtils.TwipsToPixelsY(y1));
                    this.Height = Convert.ToInt32(FCUtils.TwipsToPixelsX(y2 - y1));
                }
                else
                {
                    // TODO: implement other cases
                }
            }          
        }
        
        #endregion
    }
}
