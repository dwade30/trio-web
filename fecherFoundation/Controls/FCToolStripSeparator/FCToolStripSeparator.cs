﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// VB.MenuSeparator
    /// </summary>
    public class FCToolStripSeparator : MenuItem
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private string text;
        /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// in VB6 menu item with caption "-" is interpreted as menu separator
        /// </summary>
        public new string Text
        {
            get
            {
                return "-";
            }
            set
            {
                text = value;
                //CNA: in VB6 if menu's text is "-" then is interpreted as menu separator
                // TODO
                //if (value != "-")
                //{
                //    FCToolStripMenuItem parent = this.OwnerItem as FCToolStripMenuItem;
                //    if (parent != null)
                //    {
                //        FCToolStripMenuItem toolStripMenuItem = this.Tag as FCToolStripMenuItem;
                //        //cannot recover saved menu item
                //        if (toolStripMenuItem == null)
                //        {
                //            toolStripMenuItem = new FCToolStripMenuItem();
                //        }
                //        toolStripMenuItem.Text = value;
                //        //replace in parent dropdownitems list
                //        int index = parent.DropDownItems.IndexOf(this);
                //        parent.DropDownItems.Insert(index, toolStripMenuItem);
                //        parent.DropDownItems.Remove(this);
                //        //replace in control array list
                //        List<object> list = this.GetControlArray() as List<object>;
                //        if (list != null)
                //        {
                //            index = list.IndexOf(this);
                //            list.Insert(index, toolStripMenuItem);
                //            toolStripMenuItem.SetControlArray(list);
                //            list.Remove(this);
                //        }
                //        this.Dispose();
                //    }
                //}
            }
        }

        public bool Checked
        {
            get;
            set;
        }

        public bool Enabled
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Caption
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        /// <summary>
        /// Raises the TextChanged event.
        /// </summary>
        /// <param name="e"></param>
        // TODO
        //protected override void OnTextChanged(EventArgs e)
        //{
        //    base.OnTextChanged(e);
        //    //FC:FINAL:DSE In VB6, the Text different from  "-" will change the separator into a menu item
        //    if (this.Text != "-")
        //    {
        //        ToolStripDropDownMenu parentMenu = (this.GetCurrentParent() as ToolStripDropDownMenu);
        //        if (parentMenu != null)
        //        {
        //            int itemIndex = parentMenu.Items.IndexOf(this);
        //            parentMenu.Items.Remove(this);
        //            FCToolStripMenuItem menuItem = new FCToolStripMenuItem();
        //            menuItem.Text = this.Text;
        //            parentMenu.Items.Insert(itemIndex, menuItem);
        //        }
        //    }
        //}

        /// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }
        #endregion
    }
}
