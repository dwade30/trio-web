﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCArrowContextMenu : Wisej.Web.UserPopup
    {
        private TabAlignment direction = TabAlignment.Bottom;
        private ObservableCollection<FCArrowContextMenuItem> items = new ObservableCollection<FCArrowContextMenuItem>();

        public event EventHandler ItemClick;

        public FCArrowContextMenu()
        {
            InitializeComponent();
            items.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Items_CollectionChanged);
        }

        public FCArrowContextMenu(IContainer container) : base (container)
        {
            InitializeComponent();
            items.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Items_CollectionChanged);
        }
        
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [DefaultValue(TabAlignment.Bottom)]
        public TabAlignment Direction
        {
            get
            {
                return this.direction;
            }
            set
            {
                this.direction = value;
                switch (this.direction)
                {
                    case TabAlignment.Bottom:
                        this.arrowPicture.Image = fecherFoundation.Properties.Resources.menu_arrow_down;
                        this.arrowPicture.Parent = this.panelBottom;
                        this.panelBottom.Visible = true;
                        this.panelTop.Visible = false;
                        break;
                    case TabAlignment.Top:
                        this.arrowPicture.Image = fecherFoundation.Properties.Resources.menu_arrow_up;
                        this.arrowPicture.Parent = this.panelTop;
                        this.panelBottom.Visible = false;
                        this.panelTop.Visible = true;
                        break;
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ObservableCollection<FCArrowContextMenuItem> Items
        {
            get
            {
                return this.items;
            }
            set
            {
                this.items = value;
            }
        }

        public void Clear()
        {
            this.menuPanel.Controls.Clear();
            this.Items.Clear();
        }
        
        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.menuPanel.Controls.Count == 0)
            {   
                this.Height = 29;
            }
            //this.menuPanel.Controls.Clear();
            int currentX = this.menuPanel.Height;
            FCArrowContextMenuItem arrowItem;
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (var item in e.NewItems)
                {
                    arrowItem = (FCArrowContextMenuItem) item;
                    string itemText = arrowItem.Text;
                    Label labelItem = new Label();
                    labelItem.Enabled = arrowItem.Enabled;
                    labelItem.UserData.HasSubItems = itemText.EndsWith("[+]");
                    labelItem.UserData.Expanded = false;
                    labelItem.UserData.IsChildItem = itemText.StartsWith("[--]");
                    labelItem.Text = itemText.Replace("[+]", "").Replace("[--]", "");
                    labelItem.ForeColor = System.Drawing.Color.FromName("@navigationForeColor");
                    labelItem.Font = new System.Drawing.Font("semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
                    labelItem.Top = currentX;
                    labelItem.Left = labelItem.UserData.IsChildItem ? 30 : 15;
                    labelItem.AutoSize = true;
                    labelItem.Cursor = Wisej.Web.Cursors.Hand;
                    labelItem.Click += LabelItem_Click;
                    this.menuPanel.Controls.Add(labelItem);
                    if (!itemText.Contains("[--]"))
                    {
                        currentX += labelItem.Height + 15;
                    }
                    else
                    {
                        labelItem.Visible = false;
                    }

                    //add expand collapse picture
                    if (labelItem.UserData.HasSubItems)
                    {
                        PictureBox expandCollapseIcon = new PictureBox();
                        expandCollapseIcon.Height = labelItem.Height;
                        expandCollapseIcon.Width = expandCollapseIcon.Height;
                        expandCollapseIcon.ImageSource = "node-plus?color=navigationIconForeColor";
                        expandCollapseIcon.Left = 230;
                        expandCollapseIcon.Top = labelItem.Top;
                        expandCollapseIcon.UserData.LabelItem = labelItem;
                        expandCollapseIcon.Click += ExpandCollapseIcon_Click;
                        this.menuPanel.Controls.Add(expandCollapseIcon);
                        labelItem.UserData.ExpandCollapseIcon = expandCollapseIcon;
                    }
                }
                this.Height = currentX + 18;
            }
        }

        private void ExpandCollapseIcon_Click(object sender, EventArgs e)
        {
            PictureBox expandCollapseIcon = sender as PictureBox;
            Label labelItem = expandCollapseIcon.UserData.LabelItem as Label;
            LabelItem_Click(labelItem, EventArgs.Empty);
        }

        protected void OnItemClick(object sender, EventArgs e)
        {
            if (this.ItemClick != null)
            {
                this.ItemClick(sender, e);
            }
        }

        private void LabelItem_Click(object sender, EventArgs e)
        {
            Label itemLabel = sender as Label;
            if (!itemLabel.UserData.HasSubItems)
            {
                this.OnItemClick(itemLabel.Text, e);
            }
            else
            {
                int currentHeight = this.Height;
                this.Height = 29;
                int currentX = this.menuPanel.Height;
                PictureBox expandCollapseIcon = (itemLabel.UserData.ExpandCollapseIcon as PictureBox);
                if (!itemLabel.UserData.Expanded)
                {
                    bool currentSubItems = false;
                    for (int i = 0; i < this.menuPanel.Controls.Count; i++)
                    {
                        if (this.menuPanel.Controls[i] is Label)
                        {
                            Label nextItem = this.menuPanel.Controls[i] as Label;
                            if (nextItem.UserData.IsChildItem)
                            {
                                if (currentSubItems)
                                {
                                    nextItem.Visible = true;
                                    nextItem.Top = currentX;
                                    currentX += nextItem.Height + 15;
                                }
                                else
                                {
                                    nextItem.Visible = false;
                                }
                            }
                            else
                            {
                                currentSubItems = false;
                                nextItem.Top = currentX;
                                currentX += nextItem.Height + 15;
                            }
                            if (nextItem == itemLabel)
                            {
                                currentSubItems = true;
                            }
                            else
                            {
                                nextItem.UserData.Expanded = false;
                            }

                            if (nextItem.UserData.HasSubItems)
                            {
                                string imageSource = nextItem.UserData.Expanded ? "node-minus" : "node-plus";
                                nextItem.UserData.ExpandCollapseIcon.ImageSource = imageSource + "?color=rgb(213, 218, 226)";
                                nextItem.UserData.ExpandCollapseIcon.Top = nextItem.Top;
                            }
                        }
                    }
                    itemLabel.UserData.Expanded = true;
                    expandCollapseIcon.ImageSource = "node-minus?color=rgb(213, 218, 226)";
                }
                else
                {
                    for (int i = 0; i < this.menuPanel.Controls.Count; i++)
                    {
                        if (this.menuPanel.Controls[i] is Label)
                        {
                            Label nextItem = this.menuPanel.Controls[i] as Label;
                            if (nextItem.UserData.IsChildItem)
                            {
                                nextItem.Visible = false;
                                nextItem.Top = currentX;
                            }
                            else
                            {
                                nextItem.Top = currentX;
                                currentX += nextItem.Height + 15;
                            }

                            if (nextItem.UserData.HasSubItems)
                            {
                                string imageSource = nextItem.UserData.Expanded ? "node-minus" : "node-plus";
                                nextItem.UserData.ExpandCollapseIcon.ImageSource = imageSource + "?color=rgb(213, 218, 226)";
                                nextItem.UserData.ExpandCollapseIcon.Top = nextItem.Top;
                            }
                        }
                    }
                    itemLabel.UserData.Expanded = false;
                    expandCollapseIcon.ImageSource = "node-plus?color=rgb(213, 218, 226)";
                }
                int newHeight = currentX + 18;
                int heightDiff = newHeight - currentHeight;
                if (this.Direction == TabAlignment.Bottom)
                {
                    this.Top = this.Top - heightDiff;
                }
                this.Height = newHeight;
            }
        }
    }

    public struct FCArrowContextMenuItem
    {
        public string Text;
        public bool Enabled;

        public FCArrowContextMenuItem(string text, bool enabled)
        {
            this.Text = text;
            this.Enabled = enabled;
        }
    }
}
