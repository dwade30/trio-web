﻿namespace fecherFoundation
{
    partial class FCArrowContextMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new Wisej.Web.Panel();
            this.panelBottom = new Wisej.Web.Panel();
            this.arrowPicture = new Wisej.Web.PictureBox();
            this.menuPanel = new Wisej.Web.Panel();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.arrowPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Dock = Wisej.Web.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(280, 9);
            this.panelTop.TabIndex = 0;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.arrowPicture);
            this.panelBottom.Dock = Wisej.Web.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 161);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(280, 9);
            this.panelBottom.TabIndex = 1;
            // 
            // arrowPicture
            // 
            this.arrowPicture.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.arrowPicture.Image = global::fecherFoundation.Properties.Resources.menu_arrow_down;
            this.arrowPicture.Location = new System.Drawing.Point(251, 0);
            this.arrowPicture.Name = "arrowPicture";
            this.arrowPicture.Size = new System.Drawing.Size(18, 9);
            this.arrowPicture.TabIndex = 2;
            // 
            // menuPanel
            // 
            this.menuPanel.AppearanceKey = "roundedPanel";
            this.menuPanel.BackColor = System.Drawing.Color.FromName("@navigationBackground");
            this.menuPanel.BorderStyle = Wisej.Web.BorderStyle.Solid;
            this.menuPanel.Dock = Wisej.Web.DockStyle.Fill;
            this.menuPanel.Location = new System.Drawing.Point(0, 9);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(280, 152);
            this.menuPanel.TabIndex = 2;
            // 
            // FCArrowContextMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.menuPanel);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelTop);
            this.Name = "FCArrowContextMenu";
            this.Size = new System.Drawing.Size(280, 170);
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.arrowPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.Panel panelTop;
        private Wisej.Web.Panel panelBottom;
        private Wisej.Web.PictureBox arrowPicture;
        private Wisej.Web.Panel menuPanel;
    }
}
