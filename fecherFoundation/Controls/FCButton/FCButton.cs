using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// Threed.SSCommand
    /// VB.CommandButton
    /// </summary>
    public partial class FCButton : Button, ISupportInitialize
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        private Image downpic = null;
        private ToolTip toolTip = new ToolTip();
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private Form frmMain = null;
        private int style = 0;

        private bool cancel = false;
        private bool defaultProp = false;

        private MousePointerConstants mousePointer = MousePointerConstants.vbDefault;

        private Image picture = null;
        private Image disabledPicture = null;
        private Image dragIcon;

        #endregion

        #region Constructors

        public FCButton()
        {
            InitializeComponent();
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            //CHE: set default value
            //this.Style = 0;
            this.RoundedCorners = true;
            this.Cancel = false;
            this.Default = false;
            this.EnabledChanged += FCButton_EnabledChanged;
            this.BackgroundImageLayout = Wisej.Web.ImageLayout.Center;
            this.WhatsThisHelpID = 0;

            //JSP: Set Drag Mode in Initializing
            this.DragMode = DragModeConstants.vbManual;
            this.AppearanceKey = "toolbarButton";
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        [DefaultValue(ImageLayout.Center)]
        public override ImageLayout BackgroundImageLayout
        {
            get
            {
                return base.BackgroundImageLayout;
            }

            set
            {
                base.BackgroundImageLayout = value;
            }
        }

        /// <summary>
        /// Returns/sets an associated context number for an object.
        /// </summary>
        [DefaultValue(0)]
        public int WhatsThisHelpID
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Show Rectangle on Buttons, if Focused
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        protected bool ShowFocusCues
        {
            get
            {
                return true;
            }
        }

        [DefaultValue(null)]
        public Image DownPicture
        {
            get
            {
                return downpic;
            }

            set
            {
                downpic = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        [DefaultValue(MousePointerConstants.vbDefault)]
        public MousePointerConstants MousePointer
        {
            get
            {
                return mousePointer;
            }
            set
            {
                mousePointer = value;
                this.Cursor = FCUtils.GetTranslatedCursor(mousePointer);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        [DefaultValue(null)]
        public Image Picture
        {
            get
            {
                return picture;
            }
            set
            {
                picture = value;
                if (this.Enabled)
                {
                    this.BackgroundImage = value;
                    //CHE: align text bottom center when image is set for SSCommand
                    if (value != null)
                    {
                        this.TextAlign = ContentAlignment.BottomCenter;
                    }
                }
            }
        }

        [DefaultValue(DragModeConstants.vbManual)]
        public DragModeConstants DragMode
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        [DefaultValue(null)]
        public Image DisabledPicture
        {
            get
            {
                return disabledPicture;
            }
            set
            {
                disabledPicture = value;
                if (!this.Enabled)
                {
                    this.BackgroundImage = value;
                }
            }
        }

        /// <summary>
        /// Image for Drag&Drop-Actions
        /// </summary>
        /// 
        [DefaultValue(null)]
        public Image DragIcon
        {
            get
            {
                return dragIcon;
            }
            set
            {
                FCUtils.DisposeImage(ref dragIcon);
                dragIcon = value;
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Caption
        {
            get 
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the font used to display text in a control or in a run-time drawing or printing operation.
        /// Note   The FontName property is included for use with the CommonDialog control and for compatibility with earlier versions of Visual Basic. For additional functionality, 
        /// use the new Font object properties (not available for the CommonDialog control).
        /// Syntax
        /// object.FontName [= font]
        /// The FontName property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// font	A string expression specifying the font name to use.
        /// Remarks
        /// The default for this property is determined by the system. Fonts available with Visual Basic vary depending on your system configuration, display devices, and printing 
        /// devices. Font-related properties can be set only to values for which fonts exist.
        /// In general, you should change FontName before setting size and style attributes with the FontSize, FontBold, FontItalic, FontStrikethru, and FontUnderline properties.
        /// Note   At run time, you can get information on fonts available to the system through the FontCount and Fonts properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                this.SetFontItalic(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                this.SetFontUnderline(value);
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentX { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentY { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(true)]
        public bool RoundedCorners
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value indicating whether a command button is the Cancel button on a form. This command button can be the CommandButton control or any object within an OLE container 
        /// control that behaves as a command button.
        /// object.Cancel [= boolean]
        /// The Cancel property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// boolean	A Boolean expression specifying whether the object is the Cancel button, as described in Settings.
        /// Settings
        /// The settings for boolean are:
        /// Setting	Description
        /// True	The CommandButton control is the Cancel button.
        /// False	(Default) The CommandButton control isn't the Cancel button.
        /// Remarks
        /// Use the Cancel property to give the user the option of canceling uncommitted changes and returning the form to its previous state.
        /// Only one CommandButton control on a form can be the Cancel button. When the Cancel property is set to True for one CommandButton, it's automatically set to False for all other CommandButton controls 
        /// on the form. When a CommandButton control's Cancel property setting is True and the form is the active form, the user can choose the CommandButton by clicking it, pressing the ESC key, or pressing 
        /// ENTER when the button has the focus.
        /// For OLE container controls, the Cancel property is provided only for those objects that specifically behave as command buttons.
        /// Tip   For a form that supports irreversible operations, such as deletions, it's a good idea to make the Cancel button the default button. To do this, set both the Cancel property and the 
        /// Default property to True.
        /// </summary>
        [DefaultValue(false)]
        public bool Cancel
        {
            get
            {
                return cancel;
            }
            set
            {
                cancel = value;
                //CHE: set CancelButton of the form
                if (cancel)
                {
                    Form form = this.FindForm();
                    if (form != null)
                    {
                        form.CancelButton = this;
                    }
                }
            }
        }

        /// <summary>
        /// Gets/Sets the button as default on the form.
        /// </summary>
        [DefaultValue(false)]
        public bool Default
        {
            get
            {
                return defaultProp;
            }
            set
            {
                defaultProp = value;
                //CHE: set AcceptButton of the form
                if (defaultProp)
                {
                    Form form = this.FindForm();
                    if (form != null)
                    {
                        form.AcceptButton = this;
                    }
                }
            }
        }

        /// <summary>
        /// SetValue triggers the click event for the button if the value set is true.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool SetValue
        {
            set
            {
                if (value)
                {
                    this.PerformClick();
                }
            }
        }

        /// <summary>
        ///  In VB6 for CommandButtons is a property called "Style" which has 2 states "0-Standard" and "1-Grafisch". If it is set to "1-Grafisch"
        ///  the selected BackColor is used but if it is set to "0-Standard" the default control color is used. 
        /// </summary>
        [DefaultValue(0)]
        public int Style 
        {
            get
            {
                return this.style;
            }
            set
            {
                this.style = value;
                if (value == 0)
                {
                    this.BackColor = System.Drawing.SystemColors.Control;
                }
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        public void BeginInit()
        {

        }

        public void EndInit()
        {

        }

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        /// <summary>
        /// Vb6 Invokes the OnClick event and simulates the Click visual state. ( Calls the c# PerformClick method which generates a Click event for a button. )
        /// </summary>
        public void DoClick()
        {
            this.PerformClick();
        }

        #endregion

        #region Internal Methods
        //FC:FINAL:SBE - Harris #3956 - add option to not execute the validation on ActiveControl. In VB6 when SendKeys(button.Shortcut) is executed, the validation on ActiveControl is not triggered
        internal void ExecuteClick(bool validateActiveControl = true)
        {
            if (validateActiveControl)
            {
                PerformClick();
            }
            else
            {
                OnClick(EventArgs.Empty);
            }
        }
        #endregion

        #region Protected Methods
        // JSP: DragDrop f�r Buttons
        protected override void OnMouseDown(MouseEventArgs e)
        {
            //Point transformedPoint = TransformCoordinate(e.X, e.Y);
            //MouseEventArgs evArg = new MouseEventArgs(e.Button, e.Clicks, transformedPoint.X, transformedPoint.Y, e.Delta);

            //CHE: support for selectable PictureBox
            //if (this.TabStop)
            //{
            //    this.Focus();
            //}
            var form = this.FindForm();
            FCForm fcForm = form as FCForm;
            if (fcForm == null || !fcForm.IsFormShown)
            {
                return;
            }

            //SBE - if form is not active, click event is not executed
            if (form != null && (!form.Active || (form.IsMdiChild && !form.MdiParent.Active)))
            {
                if (form.IsMdiChild)
                {
                    form.MdiParent.Activate();
                    form.Activate();
                }
                else
                {
                    form.Activate();
                }
            }

            base.OnMouseDown(e);
            if (DragMode == DragModeConstants.vbAutomatic && e.Clicks == 1)
            {
                //Set the Source-Control for Drag&Drop-Operation
                // TODO
                //Utils.FCDragDrop.Init(this, ((Image)this.dragIcon.Clone()), Cursor.Current);
                this.DoDragDrop(this, DragDropEffects.All);
            }
        }

        //protected override void OnGiveFeedback(GiveFeedbackEventArgs gfbevent)
        //{
        //    if (Utils.FCDragDrop.OriginalCursor != null)
        //    {
        //        gfbevent.UseDefaultCursors = false;
        //    }
        //    base.OnGiveFeedback(gfbevent);
        //}

        protected override void OnQueryContinueDrag(QueryContinueDragEventArgs qcdevent)
        {
            if (!(qcdevent.Action == DragAction.Continue) && Utils.FCDragDrop.OriginalCursor != null)
            {
                //Cursor.Current = Utils.FCDragDrop.OriginalCursor;
                Utils.FCDragDrop.OriginalCursor = null;
            }
            base.OnQueryContinueDrag(qcdevent);
        }

        #endregion

        #region Private Methods

        private void FCButton_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled || this.disabledPicture == null)
            {
                this.BackgroundImage = this.picture;
            }
            else
            {
                this.BackgroundImage = this.disabledPicture;
            }
        }

        #endregion
    }
}
