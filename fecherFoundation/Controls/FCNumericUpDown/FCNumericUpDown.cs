﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCNumericUpDown : UserControl
    {
        #region Private Members

        private OrientationSettings orientation = OrientationSettings.cc2orientationVertical;
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        #endregion

        #region Constructors

        public FCNumericUpDown()
        {
            InitializeComponent();

            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            this.Value = 0;
            this.Minimum = 0;
            this.Maximum = 10;
            this.Increment = 1;
            this.Resize += FCNumericUpDown_Resize;
            this.Orientation = OrientationSettings.cc2orientationVertical;

            this.button1.Click += Button1_Click;
            this.button2.Click += Button2_Click;
        }

        #endregion

        #region Public Events

        public event EventHandler ValueChanged;

        #endregion

        #region Enums

        public enum OrientationSettings
        {
            cc2orientationVertical = 0,
            cc2orientationHorizontal = 1
        }

        public enum AlignmentSettings
        {
            cc2AlignmentLeft = 0,
            cc2AlignmentRight = 1
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Not implemented. Added for migration compatibility")]
        public int OrigLeft
        {   
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Not implemented. Added for migration compatibility")]
        public int OrigRight
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Not implemented. Added for migration compatibility")]
        public int OrigTop
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Not implemented. Added for migration compatibility")]
        public int OrigBottom
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public AlignmentSettings Alignment
        {
            get;
            set;
        }

        [DefaultValue(1)]
        public int Increment
        {
            get;
            set;
        }

        [DefaultValue(0)]
        public int Value
        {
            get;
            set;
        }

        [DefaultValue(10)]
        public int Maximum
        {
            get;
            set;
        }

        [DefaultValue(0)]
        public int Minimum
        {
            get;
            set;
        }

        [DefaultValue(OrientationSettings.cc2orientationVertical)]
        public OrientationSettings Orientation
        {
            get
            {
                return orientation;
            }
            set
            {
                orientation = value;
                FCNumericUpDown_Resize(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Private Methods


        private void Button2_Click(object sender, EventArgs e)
        {
            if (this.Value < this.Maximum)
            {
                this.Value++;
            }
            if (ValueChanged != null)
            {
                ValueChanged(this, EventArgs.Empty);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (this.Value > this.Minimum)
            {
                this.Value--;
            }
            if (ValueChanged != null)
            {
                ValueChanged(this, EventArgs.Empty);
            }
        }

        private void FCNumericUpDown_Resize(object sender, EventArgs e)
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FCNumericUpDown));
            if (this.Orientation == OrientationSettings.cc2orientationVertical)
            {
                this.button1.Width = this.Width - 1;
                this.button2.Width = this.Width - 1;
                this.button1.Height = this.Height / 2 - 1;
                this.button2.Height = this.Height / 2 - 1;
                this.button1.Top = 0;
                this.button1.Left = 0;
                this.button2.Top = button1.Height + 1;
                this.button2.Left = 0;
                this.button1.BackgroundImage = new Bitmap(((System.Drawing.Image)(resources.GetObject("icon-arrow-up-b-128"))), this.button1.Size); 
                this.button2.BackgroundImage = new Bitmap(((System.Drawing.Image)(resources.GetObject("icon-arrow-down-b-128"))), this.button2.Size);
            }
            else
            {
                this.button1.Width = this.Width / 2 - 1;
                this.button2.Width = this.Width / 2 - 1;
                this.button1.Height = this.Height - 1;
                this.button2.Height = this.Height - 1;
                this.button1.Top = 0;
                this.button1.Left = 0;
                this.button2.Top = 0;
                this.button2.Left = button1.Width + 1;
                this.button1.BackgroundImage = new Bitmap(((System.Drawing.Image)(resources.GetObject("icon-arrow-left-b-128"))), this.button1.Size);
                this.button2.BackgroundImage = new Bitmap(((System.Drawing.Image)(resources.GetObject("icon-arrow-right-b-128"))), this.button2.Size);
            }
        }

        #endregion  
    }
}