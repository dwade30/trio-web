﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    public partial class FCScroll : Panel
    {

        #region Enums

        public enum ScrollTypeConstants : int
        {
            ssScrollTypeNone = 0,
            ssScrollTypeAllwaysVisible = 1,
            ssScrollTypeWhenNeeded = 2
        }

        #endregion

        #region Members

        private ScrollTypeConstants hScrollType = ScrollTypeConstants.ssScrollTypeWhenNeeded;
        private ScrollTypeConstants vScrollType = ScrollTypeConstants.ssScrollTypeWhenNeeded;
        private int vPosition;
        private int hPosition;

        #endregion

        #region Properties

        //
        // Summary:
        //     Gets or sets the tab order of the control within its container.
        //
        // Returns:
        //     The index value of the control within the set of controls within its container.
        //     The controls in the container are included in the tab order.
        public int TabIndex
        {
            get
            {
                return base.TabIndex;
            }
            set
            {
                base.TabIndex = value;
            }
        }
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentX { get; set; }
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentY { get; set; }
        [Obsolete("The property is a VB6 designer property")]
        public int _Version { get; set; }

        /// <summary>
        /// Gets or sets the distance to move a horizontal scroll bar in response to a large scroll command.
        /// </summary>
        public int HLargeChange
        {
            get
            {
                return base.HorizontalScroll.LargeChange;
            }
            set
            {
                base.HorizontalScroll.LargeChange = value;
                base.PerformLayout();
            }
        }
        /// <summary>
        /// Gets or sets the distance to move a  horizontal scroll bar in response to a small scroll command
        /// </summary>
        public int HSmallChange
        {
            get
            {
                return base.HorizontalScroll.SmallChange;
            }
            set
            {
                base.HorizontalScroll.SmallChange = value;
                base.PerformLayout();
            }
        }
        /// <summary>
        ///   /// <summary>
        /// Gets or sets the distance to move a vertical scroll bar in response to a large scroll command.
        /// </summary>
        /// </summary>
        public int VLargeChange
        {
            get
            {
                return base.VerticalScroll.LargeChange;
            }
            set
            {
                base.VerticalScroll.LargeChange = value;
                base.PerformLayout();
            }
        }
        /// <summary>
        /// Gets or sets the distance to move a  vertical scroll bar in response to a small scroll command
        /// </summary>
        public int VSmallChange
        {
            get
            {
                return base.VerticalScroll.SmallChange;
            }
            set
            {
                base.VerticalScroll.SmallChange = value;
                base.PerformLayout();
            }
        }
        [Obsolete("The functionality of this property is replaced in the AutoScroll property")]
        public int ScrollingWidth { get; set; }
        [Obsolete("The functionality of this property is replaced in the AutoScroll property")]
        public int ScrollingHeight { get; set; }      
        /// <summary
        /// Gets or sets a numeric value that represents the current position of the vertical scroll bar box.
        /// </summary>
        public int VPosition
        {
            get
            {
                return this.vPosition;
            }
            set
            {
                this.vPosition = value;
                if (this.vPosition > base.VerticalScroll.Maximum)
                {
                    this.vPosition = base.VerticalScroll.Maximum;
                    base.VerticalScroll.Value = this.vPosition;
                    base.PerformLayout();
                }
                else
                {
                    base.VerticalScroll.Value = this.vPosition;
                    base.PerformLayout();
                }
            }
        }
        /// <summary
        /// Gets or sets a numeric value that represents the current position of the horizontal scroll bar box.
        /// </summary>
        public int HPosition
        {
            get
            {
                return this.hPosition;
            }
            set
            {
                this.hPosition = value;
                if (this.hPosition > base.HorizontalScroll.Maximum)
                {
                    this.hPosition = base.HorizontalScroll.Maximum;
                    base.HorizontalScroll.Value = this.hPosition;
                    base.PerformLayout();
                }
                else 
                {
                    base.HorizontalScroll.Value = this.hPosition;
                    base.PerformLayout();
                }
            }
        }
       /// <summary>
       /// Gets/sets the vertical scroll type
       /// </summary>
        [DefaultValue(ScrollTypeConstants.ssScrollTypeWhenNeeded)]
        public ScrollTypeConstants VScrollType
        {
            get
            {
                return this.vScrollType;
            }
            set
            {
                this.vScrollType = value;
                if (this.vScrollType == ScrollTypeConstants.ssScrollTypeNone)
                {
                    base.AutoScroll = false;
                }
                else
                {
                    base.AutoScroll = true; 
                }
            }
        }
        /// <summary>
        /// Gets/sets the horizontal scroll type
        /// </summary>
        [DefaultValue(ScrollTypeConstants.ssScrollTypeWhenNeeded)]
        public ScrollTypeConstants HScrollType
        {
            get
            {
                return this.hScrollType;
            }
            set
            {
                this.hScrollType = value;
                if (this.hScrollType == ScrollTypeConstants.ssScrollTypeNone)
                {
                    base.AutoScroll = false;
                }
                else
                {
                    base.AutoScroll = true;
                }
            }
        }
        // Summary:
        //     Gets or sets a value indicating whether the container enables the user to
        //     scroll to any controls placed outside of its visible boundaries.
        //
        // Returns:
        //     true if the container enables auto-scrolling; otherwise, false. The default
        public bool AutoScroll
        {
            get
            {
                return base.AutoScroll;
            }
            set
            {
                base.AutoScroll = value;
                if (this.HScrollType == ScrollTypeConstants.ssScrollTypeNone || this.VScrollType == ScrollTypeConstants.ssScrollTypeNone)
                {
                    base.AutoScroll = false;
                }
                else
                {
                    base.AutoScroll = true;
                }
            }
        }

        // TODO:BAN
        public string TagVariant { get; set; }

        #endregion

        #region Constructor
        
        public FCScroll()
        {
            InitializeComponent();
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AutoSize = false;
            this.AutoScroll = true;
        }

        #endregion
    }
}
