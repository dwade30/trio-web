﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace fecherFoundation
{
    /// <summary>
    /// Spin.SpinButton
    /// </summary>
    public partial class FCSpinButton : UserControl
    {
        #region Private Members

        private OutriderOrientationConstants orientation = OutriderOrientationConstants._SpinVertical;
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        #endregion

        #region Constructors

        public FCSpinButton()
        {
            InitializeComponent();

            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            this.Resize += FCSpinButton_Resize;
            this.SpinOrientation = OutriderOrientationConstants._SpinVertical;
            this.button1.Paint += button1_Paint;
            this.button2.Paint += button2_Paint;

            this.button1.Click += Button1_Click;
            this.button2.Click += Button2_Click;
        }

        #endregion

        #region Public Events

        public event EventHandler SpinDown;
        public event EventHandler SpinUp;

        #endregion

        #region Enums

        public enum OutriderOrientationConstants
        {
            _SpinVertical = 0,
            _SpinHorizontal = 1
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }       
        
        [DefaultValue(OutriderOrientationConstants._SpinVertical)]
        public OutriderOrientationConstants SpinOrientation
        {
            get
            {
                return orientation;
            }
            set
            {
                orientation = value;
                FCSpinButton_Resize(this, EventArgs.Empty);
            }
        }

        public int TdThickness
        {
            get;
            set;
        }

        public int _StockProps
        {
            get;
            set;
        }

        public int ShadowThickness
        {
            get;
            set;
        }

        public Color LightColor
        {
            get;
            set;
        }

        public int Delay
        {
            get;
            set;
        }

        public Color BorderColor
        {
            get;
            set;
        }

        #endregion

        #region Private Methods

        private void FCSpinButton_Resize(object sender, EventArgs e)
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FCSpinButton));
            if (this.SpinOrientation == OutriderOrientationConstants._SpinVertical)
            {
                this.button1.Width = this.Width - 1;
                this.button2.Width = this.Width - 1;
                this.button1.Height = this.Height / 2 - 1;
                this.button2.Height = this.Height / 2 - 1;
                this.button1.Top = 0;
                this.button1.Left = 0;
                this.button2.Top = button1.Height + 1;
                this.button2.Left = 0;
            }
            else
            {
                this.button1.Width = this.Width / 2 - 1;
                this.button2.Width = this.Width / 2 - 1;
                this.button1.Height = this.Height - 1;
                this.button2.Height = this.Height - 1;
                this.button1.Top = 0;
                this.button1.Left = 0;
                this.button2.Top = 0;
                this.button2.Left = button1.Width + 1;
            }
        }

        private void button2_Paint(object sender, PaintEventArgs e)
        {
            if (this.SpinOrientation == OutriderOrientationConstants._SpinVertical)
            {
                // Create solid brush.
                using (SolidBrush blueBrush = new SolidBrush(this.ForeColor))
                {
                    // Create points that define polygon.
                    PointF point1 = new PointF(3, 0);
                    PointF point2 = new PointF(button2.Size.Width / 2, button2.Size.Height - 2);
                    PointF point3 = new PointF(button2.Size.Width - 3, 0);
                    PointF[] curvePoints = { point1, point2, point3 };

                    // Define fill mode.
                    FillMode newFillMode = FillMode.Winding;

                    // Fill polygon to screen.
                    e.Graphics.FillPolygon(blueBrush, curvePoints, newFillMode);
                }
            }
            else
            {
                // Create solid brush.
                using (SolidBrush blueBrush = new SolidBrush(this.ForeColor))
                {
                    // Create points that define polygon.
                    PointF point1 = new PointF(0, 0);
                    PointF point2 = new PointF(button2.Size.Width - 3, button2.Size.Height / 2);
                    PointF point3 = new PointF(0, button2.Size.Height - 2);
                    PointF[] curvePoints = { point1, point2, point3 };

                    // Define fill mode.
                    FillMode newFillMode = FillMode.Winding;

                    // Fill polygon to screen.
                    e.Graphics.FillPolygon(blueBrush, curvePoints, newFillMode);
                }
            }
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {
            if (this.SpinOrientation == OutriderOrientationConstants._SpinVertical)
            {
                // Create solid brush.
                using (SolidBrush blueBrush = new SolidBrush(this.ForeColor))
                {
                    // Create points that define polygon.
                    PointF point1 = new PointF(3, button1.Size.Height - 2);
                    PointF point2 = new PointF(button1.Size.Width / 2, 3);
                    PointF point3 = new PointF(button1.Size.Width - 3, button1.Size.Height - 2);
                    PointF[] curvePoints = { point1, point2, point3 };

                    // Define fill mode.
                    FillMode newFillMode = FillMode.Winding;

                    // Fill polygon to screen.
                    e.Graphics.FillPolygon(blueBrush, curvePoints, newFillMode);
                }
            }
            else
            {
                // Create solid brush.
                using (SolidBrush blueBrush = new SolidBrush(this.ForeColor))
                {
                    // Create points that define polygon.
                    PointF point1 = new PointF(3, button1.Size.Height / 2);
                    PointF point2 = new PointF(button1.Size.Width, 0);
                    PointF point3 = new PointF(button1.Size.Width, button1.Size.Height - 2);
                    PointF[] curvePoints = { point1, point2, point3 };

                    // Define fill mode.
                    FillMode newFillMode = FillMode.Winding;

                    // Fill polygon to screen.
                    e.Graphics.FillPolygon(blueBrush, curvePoints, newFillMode);
                }
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (SpinUp != null)
            {
                SpinUp(sender, e);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (SpinDown != null)
            {
                SpinDown(sender, e);
            }
        }

        #endregion  
    }
}