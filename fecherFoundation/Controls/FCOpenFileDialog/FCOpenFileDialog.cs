﻿using fecherFoundation.VisualBasicLayer;
using System;
using System.IO;
using System.Linq;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCOpenFileDialog : Form
    {
        private string filter = "";

        public FCOpenFileDialog()
        {
            InitializeComponent();
        }

        public string FileName { get; set; }

        public string Filter
        {
            get
            {
                return filter;
            }
            set
            {
                if (value != filter)
                {
                    filter = value;

                    upload1.AllowedFileTypes = String.Join(", ", filter.Split('|').Select(f => f.Trim()).Where(f => f.StartsWith("*.")).Select(f =>f.Substring(1)));
                }
            }
        }

        private void upload1_Uploaded(object sender, UploadedEventArgs e)
        {
            if (e.Files != null && e.Files.Count > 0)
            {
                System.Web.HttpPostedFile file = e.Files[0];
                string path = FCFileSystem.Statics.UserDataFolder;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                this.FileName = Path.Combine(path, file.FileName);
                using (FileStream fileStream = new FileStream(this.FileName, FileMode.Create, FileAccess.Write))
                {
                    file.InputStream.CopyTo(fileStream);
                    //FC:FINAL:JTA - Harris #i1739 - Close the InputStream. When the same file is uploaded again, exception is thrown.
                    file.InputStream.Close();
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
