﻿using System;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCMaskedTextBox : Wisej.Web.MaskedTextBox
    {
        public FCMaskedTextBox()
        {
            InitializeComponent();
        }

        public string ClipText
        {
            get
            {
                return this.UnmaskText(this.Text);
            }
        }
    }
}
