﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// VB.Image
    /// </summary>
    public class FCImage : FCPictureBox
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors

        public FCImage() : base()
        {
            this.SizeMode = PictureBoxSizeMode.Normal;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// property for VB.Image
        /// </summary>
        [DefaultValue(false)]
        public bool Stretch
        {
            get
            {
                return (this.SizeMode == PictureBoxSizeMode.StretchImage) ? true : false;
            }
            set
            {
                if (value)
                {
                    this.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                else
                {
                    this.SizeMode = PictureBoxSizeMode.AutoSize;
                    //this.SizeMode = PictureBoxSizeMode.Normal;
                }
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
