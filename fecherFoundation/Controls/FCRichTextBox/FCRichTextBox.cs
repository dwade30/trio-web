﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;
using System.Diagnostics;
using System.Drawing.Printing;

namespace fecherFoundation
{
    public partial class FCRichTextBox : TextBox/*RichTextBox*/, ISupportInitialize
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private AppearanceConstants appearance = AppearanceConstants.rtfThreeD;

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private MousePointerConstants mousePointer = MousePointerConstants.rtfDefault;

        private bool locked = false;
        
        #endregion

        #region Constructors

        public FCRichTextBox()
        {
            InitializeComponent();
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        /// <summary>
        /// Event-Handler to handle Key-Down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FCRichTextBox_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            //if Richtext-Box is configured for D&D and ReadOnly, then suppress Key-Event
            if(locked)
            {
                //e.SuppressKeyPress = true;
                e.Handled = true;
            }
            //Check, if the Clipboard-Key für Copy was pressed and copy the Text to the Clipboard
            if(EventHelper.IsWindowsClipboardKeyCtrl_Key(e, Keys.C))
            {
                this.Copy();
            }
        }
        #endregion

        #region Enums

        public enum MousePointerConstants
        {
            rtfDefault = 0,
            rtfArrow = 1,
            rtfCrosshair = 2,
            rtfIBeam = 3,
            rtfIconPointer = 4,
            rtfSizePointer = 5,
            rtfSizeNESW = 6,
            rtfSizeNS = 7,
            rtfSizeNWSE = 8,
            rtfSizeWE = 9,
            rtfUpArrow = 10,
            rtfHourglass = 11,
            rtfNoDrop = 12,
            rtfArrowHourglass = 13,
            rtfArrowQuestion = 14,
            rtfSizeAll = 15,
            rtfCustom = 99 
        }

        public enum SelAlignmentConstants
        {
            rtfLeft = 0,
            rtfRight = 1,
            rtfCenter = 2
        }

        public enum AppearanceConstants : int
        {
            rtfFlat = 0,
            rtfThreeD = 1
        }

        public enum OLEDragConstants : int
        {
            rtfOLEDragManual = 0,
            rtfOLEDragAutomatic = 1
        }

        public enum OLEDropConstants : int
        {
            rtfOLEDropNone = 0,
            rtfOLEDropManual = 1,
            rtfOLEDropAutomatic = 2
        }

        #endregion

        #region Properties
        /// <summary>
        ///  Returns/sets the number of tabs in a RichTextBox control.  Used in conjunction with the SelTab Property.
        /// </summary>
        public dynamic SelTabCount
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        [DefaultValue(MousePointerConstants.rtfDefault)]
        public MousePointerConstants MousePointer
        {
            get
            {
                return mousePointer;
            }
            set
            {
                mousePointer = value;
                this.Cursor = GetTranslatedCursor(mousePointer);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public bool SelBullet
        //{
        //    get
        //    {
        //        return base.SelectionBullet;
        //    }
        //    set
        //    {
        //        base.SelectionBullet = value;
        //    }
        //}


        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelHangingIndent
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public SelAlignmentConstants SelAlignment
        //{
        //    get
        //    {
        //        switch (this.SelectionAlignment)
        //        {
        //            case HorizontalAlignment.Left:
        //                {
        //                    return SelAlignmentConstants.rtfLeft;
        //                }
        //            case HorizontalAlignment.Right:
        //                {
        //                    return SelAlignmentConstants.rtfRight;
        //                }
        //            case HorizontalAlignment.Center:
        //                {
        //                    return SelAlignmentConstants.rtfCenter;
        //                }
        //        }
        //        return SelAlignmentConstants.rtfLeft;
        //    }
        //    set
        //    {
        //        switch (value)
        //        {
        //            case SelAlignmentConstants.rtfLeft:
        //                {
        //                    this.SelectionAlignment = HorizontalAlignment.Left;
        //                    break;
        //                }
        //            case SelAlignmentConstants.rtfRight:
        //                {
        //                    this.SelectionAlignment = HorizontalAlignment.Right;
        //                    break;
        //                }
        //            case SelAlignmentConstants.rtfCenter:
        //                {
        //                    this.SelectionAlignment = HorizontalAlignment.Center;
        //                    break;
        //                }
        //        }
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(false)]
        public bool AutoVerbMenu
        {
            // TODO:CHE
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Gets/sets the Appearance of the control
        /// </summary>
        [DefaultValue(AppearanceConstants.rtfThreeD)]
        public AppearanceConstants Apearance
        {
            get
            {
                return this.appearance;
            }
            set
            {
                this.appearance = value;
                if (this.appearance == AppearanceConstants.rtfThreeD)
                {
                    base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                }
                else
                {
                    base.BorderStyle = Wisej.Web.BorderStyle.None;
                }
            }
        }
        
        [Obsolete(".NET does not support the property OLEDragMode")]
        public OLEDragConstants OLEDragMode { get; set; }
        
        [Obsolete(".NET does not support the property OLEDropMode")]
        public OLEDropConstants OLEDropMode { get; set; }

        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public string SelFontName
        //{
        //    get
        //    {
        //        return this.SelectionFont.Name;
        //    }
        //    set
        //    {
        //        try
        //        {
        //            if (!(this.SelectionFont == null))
        //            {
        //                Font currentFont = this.SelectionFont;
        //                FontFamily newFamily = new FontFamily(value);
        //                Font font = new Font(newFamily, currentFont.Size, currentFont.Style);
        //                FCUtils.AddFont(font);
        //                this.SelectionFont = font;
        //                FCUtils.DisposeFont(null, currentFont);
        //            }
        //        }
        //        catch (System.Exception ex)
        //        {
        //            MessageBox.Show(ex.Message.ToString(), "Error");
        //        }
        //    }
        //}

        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public float SelFontSize
        //{
        //    get
        //    {
        //        return this.SelectionFont.Size;
        //    }
        //    set
        //    {
        //        try
        //        {
        //            if (!(this.SelectionFont == null))
        //            {
        //                Font currentFont = this.SelectionFont;
        //                float newSize = Convert.ToSingle(value);
        //                Font font = new Font(currentFont.FontFamily, newSize, currentFont.Style);
        //                FCUtils.AddFont(font);
        //                this.SelectionFont = font;
        //                FCUtils.DisposeFont(null, currentFont);
        //            }
        //        }
        //        catch (System.Exception ex)
        //        {
        //            MessageBox.Show(ex.Message.ToString(), "Error");
        //        }
        //    }
        //}

        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public Color SelColor
        //{
        //    get
        //    {
        //        return this.SelectionColor;
        //    }
        //    set
        //    {
        //        this.SelectionColor = value;
        //    }
        //}

        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public bool SelBold
        //{
        //    get
        //    {
        //        return this.SelectionFont.Style.HasFlag(FontStyle.Bold);
        //    }
        //    set
        //    {
        //        Font currentFont = this.SelectionFont;
        //        FontStyle newFontStyle = this.SelectionFont.Style;
        //        newFontStyle = this.SelectionFont.Style ^ FontStyle.Bold;

        //        Font font = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
        //        FCUtils.AddFont(font); 
        //        this.SelectionFont = font;
        //        FCUtils.DisposeFont(null, currentFont);
        //    }
        //}

        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public bool SelItalic
        //{
        //    get
        //    {
        //        return this.SelectionFont.Style.HasFlag(FontStyle.Italic);
        //    }
        //    set
        //    {
        //        Font currentFont = this.SelectionFont;
        //        FontStyle newFontStyle = this.SelectionFont.Style;
        //        newFontStyle = this.SelectionFont.Style ^ FontStyle.Italic;

        //        Font font = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
        //        FCUtils.AddFont(font);
        //        this.SelectionFont = font;
        //        FCUtils.DisposeFont(null, currentFont);
        //    }
        //}

        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public bool SelUnderline
        //{
        //    get
        //    {
        //        return this.SelectionFont.Style.HasFlag(FontStyle.Underline);
        //    }
        //    set
        //    {
        //        Font currentFont = this.SelectionFont;
        //        FontStyle newFontStyle = this.SelectionFont.Style;
        //        newFontStyle = this.SelectionFont.Style ^ FontStyle.Underline;

        //        Font font = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
        //        FCUtils.AddFont(font);
        //        this.SelectionFont = font;
        //        FCUtils.DisposeFont(null, currentFont);
        //    }
        //}

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string TextRTF
        {
            get
            {
                //return this.Rtf;
                return this.Text;
            }
            set
            {
                //CHE: avoid exception to improve performance
                if (value == null || !value.StartsWith(@"{\rtf"))
                {
                    this.Text = value;
                }
                else
                {
                    try
                    {
                        //this.Rtf = value;
                        this.Text = value;
                    }
                    catch (ArgumentException)
                    {
                        this.Text = value;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(false)]
        public bool Locked
        {
            get
            {
                return locked;
            }
            set
            {
                //FINAL:PJ: If DragDrop is enabled and the Field is ReadOnly then D&D is not working (see KeyPress-Event)
                //this.ReadOnly = value;
                locked = value;
                if (!this.AllowDrop || value == false)
                {
                    this.ReadOnly = value;
                }

            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelStart
        {
            get
            {
                return this.SelectionStart;
            }
            set
            {
                this.SelectionStart = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelLength
        {
            get
            {
                return this.SelectionLength;
            }
            set
            {
                this.SelectionLength = value;
            }

        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string SelText
        {
            get
            {
                return this.SelectedText;
            }
            set
            {
                this.SelectedText = value;
                //AM:HARRIS:#1993 - force the SelectionLength to set the caret position
                if (this.TextLength > 0)
                {
                    this.SelectionLength = 1;
                    this.SelectionLength = 0;
                }
                //P2218:AM:#3981 - scroll to the current line
                this.SetVerticalScroll(this.SelectionStart);

            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Sets the absolute tab positions of text in a RichTextBox control.  Used in conjunction with the SelTabCount Property
        /// </summary>
        /// <param name="sElement"></param>
        public void SelTabs(int sElement, int location)
        {
            //TODO:
        }

        /// <summary>
        /// Sends formatted text in a RichTextBox control to a device for printing.
        /// </summary>
        /// <param name="IHDC"></param>
        public void SelPrint(int IHDC, bool vStartDoc= false)
        {
            var controlId = "id_" + this.Handle.ToString();
            char[] text = this.Text.ToCharArray();
            string newText = String.Format("<tag-name style =\"white-space: pre; font-family: &quot;{0}&quot;; font-size: {1}pt;\">", this.Font.Name, this.Font.SizeInPoints);
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '\t')
                {
                    newText += "&#9;";
                }
                else if(text[i] == '\\')
                {
                    newText += "&bsol;";
                }
                else if (text[i] != '\r')
                {
                    newText += text[i] == '\n' ? "<br>" : text[i].ToString();
                }
            }
            newText += "</tag-name>";
            Eval("PrintRTB('" + controlId + "','" + newText + "')");
        }

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        public int GetLineFromChar(int position)
        {
            int line = 0;
            do
            {
                position -= this.Lines[line].Length;
                line++;
            } while (position > 0 && line < this.Lines.Length);
            return line - 1;
        }

        public void SetVerticalScroll(int position)
        {
            int line = GetLineFromChar(position);
            if (line >= 0)
            {
                this.ScrollToY(line * this.FontHeight);
            }
        }

        public void BeginInit()
        {

        }

        public void EndInit()
        {

        }

        //FC:FINAL:ZSA- #269 - Reproduce loadfile functionality from Vb6
        //public void LoadFile(string path)
        //{
        //    string contents = System.IO.File.ReadAllText(path);
        //    if (contents.StartsWith(@"{\rtf"))
        //    {
        //        base.LoadFile(path, RichTextBoxStreamType.RichText);
        //    }
        //    else
        //    {
        //        base.LoadFile(path, RichTextBoxStreamType.PlainText);
        //    }
        //}

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        protected override void OnDragEnter(DragEventArgs drgevent)
        {
            drgevent.Effect = DragDropEffects.All;
            base.OnDragEnter(drgevent);
        }
       
        #endregion


        #region Private Methods

        private Wisej.Web.Cursor GetTranslatedCursor(MousePointerConstants value)
        {
            Wisej.Web.Cursor c = null;
            switch (value)
            {
                case MousePointerConstants.rtfDefault:
                    c = Cursors.Default;
                    break;
                case MousePointerConstants.rtfArrow:
                    c = Cursors.Arrow;
                    break;
                case MousePointerConstants.rtfCrosshair:
                    c = Cursors.Cross;
                    break;
                case MousePointerConstants.rtfIBeam:
                    c = Cursors.IBeam;
                    break;
                case MousePointerConstants.rtfArrowHourglass:
                    c = Cursors.WaitCursor;
                    break;
                case MousePointerConstants.rtfIconPointer:
                    c = Cursors.Default;
                    break;
                case MousePointerConstants.rtfSizePointer:
                    c = Cursors.SizeAll;
                    break;
                case MousePointerConstants.rtfSizeNESW:
                    c = Cursors.SizeNESW;
                    break;
                case MousePointerConstants.rtfSizeWE:
                    c = Cursors.SizeWE;
                    break;
                case MousePointerConstants.rtfHourglass:
                    c = Cursors.WaitCursor;
                    break;
                case MousePointerConstants.rtfNoDrop:
                    c = Cursors.No;
                    break;
                case MousePointerConstants.rtfArrowQuestion:
                    c = Cursors.Help;
                    break;
                case MousePointerConstants.rtfSizeAll:
                    c = Cursors.SizeAll;
                    break;
                case MousePointerConstants.rtfCustom:
                    c = Cursors.Default;
                    break;
                default:
                    c = Cursors.Default;
                    break;
            }

            return c;
        }

        #endregion
    }
}
