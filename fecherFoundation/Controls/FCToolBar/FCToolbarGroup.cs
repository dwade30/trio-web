﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
	public class FCToolbarGroup
    {
		#region Private Members
        private List<ToolBarButton> buttons = new List<ToolBarButton>();
		#endregion

		#region Constructor
		public void Add(ToolBarButton button)
		{
			buttons.Add(button);
		}
		#endregion

		#region Internal Methods
        internal void Check(ToolBarButton checkedButton)
		{
			if (buttons.Contains(checkedButton))
			{
                // TODO
                //foreach (ToolBarButton button in buttons)
                //{
                //    if (button != checkedButton && button.Checked)
                //    {
                //        button.Checked = false;
                //    }
                //}
			}
		}
		#endregion
	}
}
