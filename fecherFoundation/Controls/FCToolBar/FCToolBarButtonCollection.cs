﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCToolBarButtonCollection : List<FCToolBarButton>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCToolBar owner;
        private Wisej.Web.ToolBar.ToolBarButtonCollection buttons;

        #endregion

        #region Constructors

        public FCToolBarButtonCollection(FCToolBar owner)
        {
            this.owner = owner;
            this.buttons = owner.BaseButtons;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public new FCToolBarButton this[int index]
        {
            get
            {
                if (base.Count > index)
                {
                    return base[index];
                }
                return null;
            }
            set
            {
                if (base.Count > index)
                {
                    base[index] = value;
                }
            }
        }

        public FCToolBarButton this[string key]
        {
            get
            {
                if (string.IsNullOrEmpty(key))
                {
                    return (FCToolBarButton)null;
                }
                int index = this.IndexOfKey(key);
                if (this.IsValidIndex(index))
                {
                    return this[index];
                }
                else
                {
                    return (FCToolBarButton)null;
                }
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        public new void AddRange(IEnumerable<FCToolBarButton> collection)
        {
            base.AddRange(collection);
            foreach (FCToolBarButton tb in collection)
            {
                owner.Buttons.Add(tb);
            }
        }

        public new void Add(FCToolBarButton item)
        {
            base.Add(item);
            //owner.Buttons.Add(item);
            this.buttons.Add(item);
        }

        /// <summary>
        /// Returns the index of the first occurrence of the FCToolBarButton with the specified key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual int IndexOfKey(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return -1;
            }
            for (int index = 0; index < this.Count; ++index)
            {
                if (this[index].Key == key)
                {
                    return index;
                }
            }
            return -1;
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        /// <summary>
        /// Validates the index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool IsValidIndex(int index)
        {
            if (index >= 0)
            {
                return index < this.Count;
            }

            return false;
        }

        #endregion
    }
}
