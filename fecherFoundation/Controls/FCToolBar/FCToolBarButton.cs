﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCToolBarButton : ToolBarButton
    {
        #region Private Members

        private string key;

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private ToolBarButtonStyleSettings style;

        #endregion


        #region Constructors

        public FCToolBarButton()
        {
            this.Style = ToolBarButtonStyleSettings.tbrDefault;
        }

        #endregion

        #region Enums

        public enum ToolBarButtonStyleSettings
        {
            tbrDefault = 0, //(Default) Button. The button is a regular push button.
            tbrCheck = 1, //Check. The button is a check button, which can be checked or unchecked.
            tbrButtonGroup= 2, //ButtonGroup. The button remains pressed until another button in the group is pressed. Exactly one button in the group can be pressed at any one moment.
            tbrSeparator = 3, //Separator. The button functions as a separator with a fixed width of 8 pixels.
            tbrPlaceholder = 4, //Placeholder.The button is like a separator in appearance and functionality, but has a settable width.
            tbrDropDown = 5 //MenuButton drop down.Use this style to see MenuButton objects.
        }

        public enum ToolbarButtonValueConstants
        {
            tbrPressed
        }

        #endregion

        #region Properties

        [DefaultValue(ToolBarButtonStyleSettings.tbrDefault)]
        public ToolBarButtonStyleSettings Style
        {
            get
            {
                return style;
            }
            set
            {
                style = value;
            }
        }     

        [DefaultValue(false)]
        public bool MixedState
        {
            get;
            set;
        }

        public string Key
        {
            get
            {
                //CHE: consider the name if key was not set
                if (string.IsNullOrEmpty(this.key))
                {
                    return this.Name;
                }
                return this.key;
            }
            set
            {
                this.key = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                if (this.Parent != null)
                {
                    return this.Parent.Buttons.IndexOf(this) + 1;
                }
                return 0;
            }
        }

        public ToolbarButtonValueConstants Value { get; set; }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public int WidthOriginal
        //{
        //    get
        //    {
        //        using (Graphics g = this.Parent.CreateGraphics())
        //        {
        //            if (graphicsFactory == null)
        //            {
        //                graphicsFactory = new GraphicsFactory();
        //                fcGraphics = new FCGraphics(g, graphicsFactory);
        //            }
        //            fcGraphics.InitializeCoordinateSpace(g, this.ContentRectangle, 0, 0);
        //            return Convert.ToInt32(fcGraphics.ScaleX(this.ContentRectangle.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this.Parent)));
        //        }
        //    }
        //}

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public int HeightOriginal
        //{
        //    get
        //    {
        //        using (Graphics g = this.Parent.CreateGraphics())
        //        {
        //            if (graphicsFactory == null)
        //            {
        //                graphicsFactory = new GraphicsFactory();
        //                fcGraphics = new FCGraphics(g, graphicsFactory);
        //            }
        //            fcGraphics.InitializeCoordinateSpace(g, this.ContentRectangle, 0, 0);
        //            return Convert.ToInt32(fcGraphics.ScaleY(this.ContentRectangle.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this.Parent)));
        //        }
        //    }
        //}

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public int TopOriginal
        //{
        //    get
        //    {
        //        using (Graphics g = this.Parent.CreateGraphics())
        //        {
        //            if (graphicsFactory == null)
        //            {
        //                graphicsFactory = new GraphicsFactory();
        //                fcGraphics = new FCGraphics(g, graphicsFactory);
        //            }
        //            fcGraphics.InitializeCoordinateSpace(g, this.ContentRectangle, 0, 0);
        //            return Convert.ToInt32(fcGraphics.ScaleY(this.ContentRectangle.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this.Parent))) - FCUtils.GetMainMenuHeightOriginal(this.Parent);
        //        }
        //    }
        //}

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public int LeftOriginal
        //{
        //    get
        //    {
        //        using (Graphics g = this.Parent.CreateGraphics())
        //        {
        //            if (graphicsFactory == null)
        //            {
        //                graphicsFactory = new GraphicsFactory();
        //                fcGraphics = new FCGraphics(g, graphicsFactory);
        //            }
        //            fcGraphics.InitializeCoordinateSpace(g, this.ContentRectangle, 0, 0);
        //            return Convert.ToInt32(fcGraphics.ScaleX(this.ContentRectangle.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this.Parent)));
        //        }
        //    }
        //}

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
