﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCToolBarButtonClickEventArgs : EventArgs
    {
        public FCToolBarButton Button { get; set; }
    }
}
