﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCToolBar : ToolBar, ISupportInitialize
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private bool allowCustomize = true;
        private object disabledImageList = null;
        private object hotImageList = null;
		private List<FCToolbarGroup> groups = new List<FCToolbarGroup>();

        private FCToolBarButtonCollection buttons;
        private bool inMouseDown = false; //prevents from double Execution of MouseDown before first Event has been finished

        private AlignmentSettings align = AlignmentSettings.vbAlignNone;
        #endregion

        #region Constructors

        public FCToolBar()
        {
            this.BorderStyle = BorderStyle.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Align = AlignmentSettings.vbAlignNone;

            InitializeComponent();

            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            buttons = new FCToolBarButtonCollection(this);
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        public event EventHandler<FCToolBarButtonClickEventArgs>  ButtonClick;

        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum AlignmentSettings
        {
            vbAlignNone = 0,
            vbAlignTop = 1,
            vbAlignBottom = 2,
            vbAlignLeft = 3,
            vbAlignRight = 4
        }

        #endregion

        #region Properties

        [DefaultValue(AlignmentSettings.vbAlignNone)]
        public AlignmentSettings Align
        {
            get
            {
                return this.align;
            }
            set
            {
                this.align = value;
                switch(value)
                {
                    case AlignmentSettings.vbAlignNone:
                        {
                            this.Dock = DockStyle.None;
                            break;
                        }
                    case AlignmentSettings.vbAlignTop:
                        {
                            this.Dock = DockStyle.Top;
                            break;
                        }
                    case AlignmentSettings.vbAlignBottom:
                        {
                            this.Dock = DockStyle.Bottom;
                            break;
                        }
                    case AlignmentSettings.vbAlignLeft:
                        {
                            this.Dock = DockStyle.Left;
                            break;
                        }
                    case AlignmentSettings.vbAlignRight:
                        {
                            this.Dock = DockStyle.Right;
                            break;
                        }
                }
            }
        }

        public BorderStyle BorderStyle
        {
            get;
            set;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new FCToolBarButtonCollection Buttons
        {
            get
            {
                return this.buttons;
            }
        }

        //CHE - do not serialize Items collection, Buttons is already serialized
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public override ToolStripItemCollection Items
        //{
        //    get
        //    {
        //        return base.Items;
        //    }
        //}

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [DefaultValue(0)]
        public int NumButtons
        {
            // TODO:LSO
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }
        
        /// <summary>
        /// Gets/Sets the value of AllowCustomize property
        /// </summary>
        [DefaultValue(true)]
        public bool AllowCustomize
        {
            get
            {
                return this.allowCustomize;
            }
            set
            {
                this.allowCustomize = value;
            }
        }
        /// <summary>
        /// Gets/Sets the DisabledImageList
        /// </summary>
        /// 
        [DefaultValue(null)]
        public object DisabledImageList
        {
            get
            {
                return this.disabledImageList;
            }
            set
            {
                this.disabledImageList = value;
            }
        }
        /// <summary>
        /// Gets/Sets the HotImageList
        /// </summary>
        /// 
        [DefaultValue(null)]
        public object HotImageList
        {
            get
            {
                return this.hotImageList;
            }
            set
            {
                this.hotImageList = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ButtonWidth
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>        
        public int ButtonHeight
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ButtonWidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(this.ButtonWidth, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    this.ButtonWidth = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ButtonHeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(this.ButtonHeight, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    this.ButtonHeight = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public List<FCToolbarGroup> Groups
		{
			get { return groups; }
			set { groups = value; }
		}
        #endregion

        #region Internal Properties

        internal ToolBarButtonCollection BaseButtons
        {
            get
            {
                return base.Buttons;
            }
        }

        public Size ButtonSize { get; set; }

        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            if (base.Parent != null)
            {
                //CHE: if ImageList associated with ToolBar is populated (set key for images) after setting buttons ImageKey, 
                // the images will be visible in designer but they will not be visible at runtime - reassign property to force relink
                if (this.ImageList != null)
                {
                    foreach (FCToolBarButton toolBarButton in this.Buttons)
                    {
                        string oldImageKey = toolBarButton.ImageKey;
                        if (!string.IsNullOrWhiteSpace(oldImageKey))
                        {
                            toolBarButton.ImageKey = null;
                            toolBarButton.ImageKey = oldImageKey;
                        }
                    }
                }
            }
        }

        protected override void OnButtonClick(ToolBarButtonClickEventArgs e)
        {
            foreach (FCToolbarGroup group in groups)
            {
                group.Check(e.Button);
            }

            //CHE: force validation of active control when clicking a toolbar button 
            // (ValidateChildren could not be used because is raising Validating for TextBox that was filled with DataBinding and was not changed later)
            FCForm form = this.FindForm() as FCForm;
            if (form != null && form.ActiveControl != null)
            {
                //form.ValidateChildren(ValidationConstraints.Visible);
                Type t = typeof(Control);
                System.Reflection.MethodInfo mi = t.GetMethod("NotifyValidating", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                mi.Invoke(form.ActiveControl, null);
            }

            if (ButtonClick != null)
            {
                FCToolBarButtonClickEventArgs args = new FCToolBarButtonClickEventArgs();
                args.Button = e.Button as FCToolBarButton;
                if (args.Button != null)
                {
                    ButtonClick(this, args);
                }
            }

            base.OnButtonClick(e);
        }

        /// <summary>
        /// Handles the MouseDown-Ebent
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            if(!inMouseDown)
            {
                inMouseDown = true;
                base.OnMouseDown(e);
            }
            inMouseDown = false;
        }

        public void BeginInit()
        {
        }

        // TODO
        //public void EndInit()
        //{
        //    FCToolBarButton ti;
        //    for (int i= base.Items.Count - 1; i>= 0; i--)
        //    {
        //        ti = base.Items[i] as FCToolBarButton;
        //        if (ti == null) continue;
        //        switch (ti.Style)
        //        {
        //            case FCToolBarButton.ToolBarButtonStyleSettings.tbrSeparator:
        //                {
        //                    ToolStripSeparator tl = new ToolStripSeparator();
        //                    base.Items.Insert(i, tl);
        //                    base.Items.Remove(ti);
        //                    break;
        //                }
        //            case FCToolBarButton.ToolBarButtonStyleSettings.tbrPlaceholder:
        //                {
        //                    ToolStripLabel tl = new ToolStripLabel();
        //                    tl.AutoSize = false;
        //                    tl.Width = ti.Width;
        //                    base.Items.Insert(i, tl);
        //                    base.Items.Remove(ti);
        //                    break;
        //                }
        //            default:
        //                {
        //                    ti.AutoSize = true;
        //                    break;
        //                }
        //        }
        //    }

        //}
        #endregion

        #region Private Methods
        #endregion

        void ISupportInitialize.BeginInit()
        {
            
        }

        void ISupportInitialize.EndInit()
        {
            
        }
    }
}
