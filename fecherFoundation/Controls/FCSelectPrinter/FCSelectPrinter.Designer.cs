﻿namespace fecherFoundation
{
	partial class FCSelectPrinter
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbPrinters = new Wisej.Web.ListView();
            this.columnHeader1 = new Wisej.Web.ColumnHeader();
            this.columnHeader2 = new Wisej.Web.ColumnHeader();
            this.columnHeader3 = new Wisej.Web.ColumnHeader();
            this.btnOK = new Wisej.Web.Button();
            this.btnCancel = new Wisej.Web.Button();
            this.SuspendLayout();
            // 
            // lbPrinters
            // 
            this.lbPrinters.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lbPrinters.Columns.AddRange(new Wisej.Web.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lbPrinters.Location = new System.Drawing.Point(30, 30);
            this.lbPrinters.Name = "lbPrinters";
            this.lbPrinters.Size = new System.Drawing.Size(705, 251);
            this.lbPrinters.TabIndex = 0;
            this.lbPrinters.View = Wisej.Web.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Name = "columnHeader1";
            this.columnHeader1.Text = "Printer";
            this.columnHeader1.Width = 250;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Name = "columnHeader2";
            this.columnHeader2.Text = "Driver";
            this.columnHeader2.Width = 350;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Name = "columnHeader3";
            this.columnHeader3.Text = "Port";
            this.columnHeader3.Width = 100;
            // 
            // btnOK
            // 
            this.btnOK.AppearanceKey = "actionButton";
            this.btnOK.Location = new System.Drawing.Point(289, 300);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 40);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AppearanceKey = "actionButton";
            this.btnCancel.Location = new System.Drawing.Point(384, 300);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 40);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FCSelectPrinter
            // 
            this.ClientSize = new System.Drawing.Size(763, 358);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lbPrinters);
            this.Name = "FCSelectPrinter";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Select printer";
            this.Load += new System.EventHandler(this.FCSelectPrinter_Load);
            this.ResumeLayout(false);

		}
		#endregion

		private Wisej.Web.ListView lbPrinters;
		private Wisej.Web.Button btnOK;
		private Wisej.Web.Button btnCancel;
		private Wisej.Web.ColumnHeader columnHeader1;
		private Wisej.Web.ColumnHeader columnHeader2;
		private Wisej.Web.ColumnHeader columnHeader3;
	}
}
