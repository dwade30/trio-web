﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using Wisej.Core;
using Wisej.Web;

namespace fecherFoundation
{
	public partial class FCSelectPrinter : Form
	{
		public FCSelectPrinter()
		{
			InitializeComponent();
            //FC:FINAL:MSH - issue #1017: change TopMost to overlap Wait form in some cases
            this.TopMost = true;
		}

		public string PrinterName
		{
			get;
			set;
		}

		public string Driver
		{
			get;
			set;
		}

		public string Port
		{
			get;
			set;
		}

		private void FCSelectPrinter_Load(object sender, EventArgs e)
		{
			GetPrinters();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			//Amit TAVO - On site check for null SubItems  7/21/2016
			if (lbPrinters.SelectedItems != null && lbPrinters.SelectedItems.Any())
			{
                ListViewItem selectedItem = lbPrinters.SelectedItems[0];
                if (selectedItem.SubItems != null)
                {
                    this.PrinterName = selectedItem.SubItems[0].Text;
                    this.Driver = selectedItem.SubItems[1].Text;
                    this.Port = selectedItem.SubItems[2].Text;
                }
			}
			this.DialogResult = Wisej.Web.DialogResult.OK;
			this.Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = Wisej.Web.DialogResult.Cancel;
			this.Close();
		}

		private void GetPrinters()
		{
			ObjectQuery oquery = new ObjectQuery("SELECT * FROM Win32_Printer");
			ManagementObjectSearcher mosearcher = new ManagementObjectSearcher(oquery);
            foreach (ManagementObject mo in mosearcher.Get())
            {
                //check for virtual printers
                //https://msdn.microsoft.com/en-us/library/aa394363(v=vs.85).aspx
                string port = mo["PortName"].ToString();
                if (port != "nul:" && //send to onenote has this port
                    port != "PORTPROMPT:" && //microsoft xps and microsoft print to pdf has this port
                    port != "SHRFAX:") // fax printer has this port
                {
                    string[] items = new string[3];
                    items[0] = mo["Name"].ToString();
                    items[1] = mo["DriverName"].ToString();
                    items[2] = mo["PortName"].ToString();
                    lbPrinters.Items.Add(new ListViewItem(items));
                }
			}
		}
	}
}
