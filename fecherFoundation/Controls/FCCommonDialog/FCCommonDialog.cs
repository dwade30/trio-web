﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.ComponentModel;
using System.Drawing.Printing;

namespace fecherFoundation
{
    /// <summary>
    /// MSComDlg.CommonDialog
    /// </summary>
    public class FCCommonDialog : Control
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private int min = -1;
        private int max = -1;
        private /*FontDialog*/dynamic fontDialog;
        private /*SaveFileDialog*/dynamic saveFileDialog;
        //FC:FINAl:SBE - use FCSelectPrinter form for network printers
        private FCSelectPrinter printDialog;
        private FCOpenFileDialog openFileDialog;
        private Font font;
        private PrinterOrientationConstants orientation = PrinterOrientationConstants.cdlPortrait; 

        #endregion

        #region Constructors

        public FCCommonDialog()
        {
            //CHE: set default value
            this.Orientation = PrinterOrientationConstants.cdlPortrait;
            this.Color = System.Drawing.Color.Black;
            this.FontStrikeThru = false;
            this.FontBold = false;
            this.FontItalic = false;
            this.FontUnderline = false;
            this.FontSize = 8;
            this.ForeColor = System.Drawing.Color.Black;
            this.Min = 0;
            this.Max = 0;
            this.CancelError = false;
            this.InitDir = "";
            this.Filter = "";
            this.FilterIndex = 0;
            this.FileName = "";
            this.DialogTitle = "";
            this.Flags = 0;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum PrinterOrientationConstants
        {
            cdlPortrait = 1,
            cdlLandscape = 2
        }

        /// <summary>
        /// Flags
        /// </summary>
        public enum PrinterConstants
        {
            cdlPDAllPages = 0x0,
            cdlPDCollate = 0x10,
            cdlPDDisablePrintToFile = 0x80000,
            cdlPDHelpButton = 0x800,
            cdlPDHidePrintToFile = 0x100000,
            cdlPDNoPageNums = 0x8,
            cdlPDNoSelection = 0x4,
            cdlPDNoWarning = 0x80,
            cdlPDPageNums = 0x2,
            cdlPDPrintSetup = 0x40,
            cdlPDPrintToFile = 0x20,
            cdlPDReturnDC = 0x100,
            cdlPDReturnDefault = 0x400,
            cdlPDReturnIC = 0x200,
            cdlPDSelection = 0x1,
            cdlPDUseDevModeCopies = 0x40000
        }

        /// <summary>
        /// Flags
        /// </summary>
        public enum FileOpenConstants
        {
            cdlOFNAllowMultiselect,
            cdlOFNCreatePrompt,
            cdlOFNExplorer,
            cdlOFNExtensionDifferent,
            cdlOFNFileMustExist,
            cdlOFNHelpButton,
            cdlOFNHideReadOnly,
            cdlOFNLongNames,
            cdlOFNNoChangeDir,
            cdlOFNNoDereferenceLinks,
            cdlOFNNoLongNames,
            cdlOFNNoReadOnlyReturn,
            cdlOFNNoValidate,
            cdlOFNOverwritePrompt,
            cdlOFNPathMustExist,
            cdlOFNReadOnly,
            cdlOFNShareAware,
            cdlCancel = 32755
        }

        /// <summary>
        /// Flags
        /// </summary>
        public enum FontsConstants
        {
            cdlCFANSIOnly,
		    cdlCFApply,
		    cdlCFBoth = 3,
		    cdlCFEffects = 1,
		    cdlCFFixedPitchOnly,
		    cdlCFForceFontExist = 2,
		    cdlCFHelpButton,
		    cdlCFLimitSize,
		    cdlCFNoFaceSel,
		    cdlCFNoSimulations,
		    cdlCFNoSizeSel,
		    cdlCFNoStyleSel,
		    cdlCFNoVectorFonts,
		    cdlCFPrinterFonts,
		    cdlCFScalableOnly,
		    cdlCFScreenFonts,
		    cdlCFTTOnly,
		    cdlCFWYSIWYG
        }

        #endregion

        #region Properties
        /// <summary>
        ///  Returns a handle (from Microsoft Windows) to the object's device context.
        /// </summary>
        public int hDC
        {
            //TODO:
            get;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        [DefaultValue(0)]
        public Color Color { get; set; }

        [DefaultValue(false)]
        public bool FontStrikeThru { get; set; }

        /// <summary>
        /// Returns or sets the font used to display text in a control or in a run-time drawing or printing operation.
        /// Note   The FontName property is included for use with the CommonDialog control and for compatibility with earlier versions of Visual Basic. For additional functionality, 
        /// use the new Font object properties (not available for the CommonDialog control).
        /// Syntax
        /// object.FontName [= font]
        /// The FontName property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// font	A string expression specifying the font name to use.
        /// Remarks
        /// The default for this property is determined by the system. Fonts available with Visual Basic vary depending on your system configuration, display devices, and printing 
        /// devices. Font-related properties can be set only to values for which fonts exist.
        /// In general, you should change FontName before setting size and style attributes with the FontSize, FontBold, FontItalic, FontStrikethru, and FontUnderline properties.
        /// Note   At run time, you can get information on fonts available to the system through the FontCount and Fonts properties.
        /// </summary>
        [DefaultValue("")]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
            }
        }

        [DefaultValue(false)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
            }
        }

        [DefaultValue(false)]
        public bool FontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                this.SetFontItalic(value);
            }
        }

        [DefaultValue(false)]
        public bool FontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                this.SetFontUnderline(value);
            }
        }

        [DefaultValue(8)]
        public float FontSize
        {
            get
            {
                return this.Font.Size;
            }
            set
            {
                this.SetFontSize(value);
            }
        }

        public new string Name { get; set; }

        [DefaultValue(0)]
        public new Color ForeColor { get; set; }

        public new Point Location { get; set; }

        [DefaultValue(0)]
        public int Min
        {
            get
            {
                return min;
            }
            set
            {
                min = value;
            }
        }

        [DefaultValue(0)]
        public int Max
        {
            get
            {
                return max;
            }
            set
            {
                max = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(false)]
        public bool CancelError
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(PrinterOrientationConstants.cdlPortrait)]
        public PrinterOrientationConstants Orientation
        {
            get
            {
                return this.orientation;
            }
            set
            {
                this.orientation = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue("")]
        public string InitDir
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the filters that are displayed in the Type list box of a dialog box.
        /// Syntax
        /// object.Filter [= description1 |filter1 |description2 |filter2...]
        /// The Filter property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// description	A string expression describing the type of file.
        /// filter	A string expression specifying the filename extension.
        /// Remarks
        /// A filter specifies the type of files that are displayed in the dialog box's file list box. For example, selecting the filter *.txt displays all text files.
        /// Use this property to provide the user with a list of filters that can be selected when the dialog box is displayed.
        /// Use the pipe ( | ) symbol (ASCII 124) to separate the description and filter values. Don't include spaces before or after the pipe symbol, because these spaces will be 
        /// displayed with the description and filter values.
        /// The following code shows an example of a filter that enables the user to select text files or graphic files that include bitmaps and icons:
        /// Text (*.txt)|*.txt|Pictures (*.bmp;*.ico)|*.bmp;*.ico
        /// When you specify more than one filter for a dialog box, use the FilterIndex property to determine which filter is displayed as the default.
        /// </summary>
        [DefaultValue("")]
        public string Filter
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a default filter for an Open or Save As dialog box.
        /// Syntax
        /// object.FilterIndex [= number]
        /// The FilterIndex property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// number	A numeric expression specifying a default filter.
        /// Remarks
        /// This property specifies the default filter when you use the Filter property to specify filters for an Open or Save As dialog box.
        /// The index for the first defined filter is 1.
        /// </summary>
        [DefaultValue(0)]
        public short FilterIndex
        {
            // TODO:CHE
            get;
            set;
        }

        [DefaultValue("")]
        public string FileName
        {
            get;
            set;
        }

        [DefaultValue("")]
        public string DialogTitle
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the options for the Print dialog box.
        /// object.Flags [= value]
        /// value	A constant or value specifying the options for the Print dialog box, as described in Settings.
        /// Settings
        /// The settings for value are:
        /// Constant	Value	Description
        /// cdlPDAllPages	&H0	Returns or sets the state of the All Pages option button.
        /// cdlPDCollate	&H10	Returns or sets the state of the Collate check box.
        /// cdlPDDisablePrintToFile	&H80000	Disables the Print To File check box.
        /// cdlPDHelpButton	&H800	Causes the dialog box to display the Help button.
        /// cdlPDHidePrintToFile	&H100000	Hides the Print To File check box.
        /// cdlPDNoPageNums	&H8	Disables the Pages option button and the associated edit control.
        /// cdlPDNoSelection	&H4	Disables the Selection option button.
        /// cdlPDNoWarning	&H80	Prevents a warning message from being displayed when there is no default printer.
        /// cdlPDPageNums	&H2	Returns or sets the state of the Pages option button.
        /// cdlPDPrintSetup	&H40	Causes the system to display the Print Setup dialog box rather than the Print dialog box.
        /// cdlPDPrintToFile	&H20	Returns or sets the state of the Print To File check box.
        /// cdlPDReturnDC	&H100	Returns a device context for the printer selection made in the dialog box. The device context is returned in the dialog box's hDC property.
        /// cdlPDReturnDefault	&H400	Returns default printer name.
        /// cdlPDReturnIC	&H200	Returns an information context for the printer selection made in the dialog box. An information context provides a fast way to get information 
        /// about the device without creating a device context. The information context is returned in the dialog box's hDC property.
        /// cdlPDSelection	&H1	Returns or sets the state of the Selection option button. If neither cdlPDPageNums nor cdlPDSelection is specified, the All option button is in the 
        /// selected state.
        /// CdlPDUseDevModeCopies	&H40000	If a printer driver doesn't support multiple copies, setting this flag disables the Number of copies spinner control in the Print dialog. 
        /// If a driver does support multiple copies, setting this flag indicates that the dialog box stores the requested number of copies in the Copies property.
        /// Remarks
        /// These constants are listed in the Microsoft CommonDialog Control (MSComDlg) object library in the Object Browser.
        /// You can also define selected flags. Use the Const keyword in the Declarations section of the startup form to define the flags you want to use. For example:
        /// Const ReadOnly  = &H00000001&
        /// Const Effects  = &H00000100&
        /// You can set more than one flag for a dialog box using the Or operator. For example:
        /// CommonDialog1.Flags = &H10& Or &H200&
        /// Adding the desired constant values produces the same results. The following is equivalent to the preceding example:
        /// CommonDialog1.Flags = &H210&
        /// </summary>
        [DefaultValue(0)]
        public int Flags
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Font Property.
        /// </summary>
        //public new Font Font
        //{
        //    get
        //    {
        //        if (font == null)
        //        {
        //            //font = new FontDialog().Font;
        //        }

        //        return font;
        //    }
        //    set
        //    {
        //        font = value;
        //    }
        //}

        public string DefaultExt { get; set; }
        public int FromPage { get; set; }
        public int ToPage { get; set; }
        public PrinterSettings PrinterSettings { get; set; }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Displays the CommonDialog control's Save As dialog box.
        /// Syntax
        /// object.ShowSave
        /// The object placeholder represents an object expression that evaluates to an object in the Applies To list. 
        /// </summary>
        public void ShowSave()
        {
            //saveFileDialog = new SaveFileDialog();
            if (!string.IsNullOrEmpty(DialogTitle))
            {
                saveFileDialog.Title = DialogTitle;
            }
            saveFileDialog.Filter = Filter;

            saveFileDialog.InitialDirectory = InitDir;
            string filePath = "";
            string file = "";
            if (!string.IsNullOrEmpty(FileName))
            {
                filePath = Path.GetDirectoryName(FileName);
                file = Path.GetFileName(FileName);
            }
            if (!string.IsNullOrEmpty(filePath) && filePath != InitDir)
            {
                saveFileDialog.InitialDirectory = filePath;
            }
            saveFileDialog.FileName = file;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileName = saveFileDialog.FileName;
                //in vb6 SaveFileDialog change the CurrentDirectory
                if (!string.IsNullOrEmpty(FileName))
                {
                    filePath = Path.GetDirectoryName(FileName);
                    if (filePath != "")
                    {
                        FCFileSystem.ChDir(filePath);
                    }
                }
            }
        }

        /// <summary>
        /// Displays the CommonDialog control's Printer dialog box.
        /// Syntax
        /// object.ShowPrinter
        /// The object placeholder represents an object expression that evaluates to an object in the Applies To list.
        /// </summary>
        public bool ShowPrinter()
        {
            printDialog = new FCSelectPrinter();
            
            var dialogResult = printDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                FCGlobal.Printer = new FCPrinter(printDialog.PrinterName);
                FCGlobal.Printer.TrackDefault = true;

                PrintDocument doc = new PrintDocument();
                doc.PrinterSettings.PrinterName = printDialog.PrinterName;
                this.PrinterSettings = doc.PrinterSettings;

                this.Orientation = FCGlobal.Printer.Orientation == 2 ? PrinterOrientationConstants.cdlLandscape : PrinterOrientationConstants.cdlPortrait;

                return true;
            }
            else if (dialogResult == DialogResult.Cancel && this.CancelError)
            {
                throw this.CancelException();
            }

            return false;
        }

        /// <summary>
        /// Displays the CommonDialog control's Open dialog box.
        /// Syntax
        /// object.ShowOpen
        /// The object placeholder represents an object expression that evaluates to an object in the Applies To list.
        /// </summary>
        public bool ShowOpen()
        {
            openFileDialog = new FCOpenFileDialog();
            openFileDialog.Filter = Filter;
            if (this.DialogTitle != "")
            {
                openFileDialog.Text = this.DialogTitle;
            }

            //openFileDialog.InitialDirectory = InitDir;
            string filePath = "";
            string file = "";
            if (!string.IsNullOrEmpty(FileName))
            {
                filePath = Path.GetDirectoryName(FileName);
                file = Path.GetFileName(FileName);
            }
            if (!string.IsNullOrEmpty(filePath) && filePath != InitDir)
            {
                //openFileDialog.InitialDirectory = filePath;
            }
            //openFileDialog.FileName = file;

            var dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                FileName = openFileDialog.FileName;
                //InitDir = openFileDialog.InitialDirectory;
                //in vb6 OpenFileDialog change the CurrentDirectory
                //if (!string.IsNullOrEmpty(FileName))
                //{
                //    filePath = Path.GetDirectoryName(FileName);
                //    if (filePath != "")
                //    {
                //        FCFileSystem.ChDir(filePath);
                //    }
                //}
                return true;
            }
            else if (dialogResult == DialogResult.Cancel && this.CancelError)
            {   
                throw this.CancelException();
            }

            return false;
        }

        /// <summary>
        /// Displays the CommonDialog control's Font dialog box.
        /// Syntax
        /// object.ShowFont
        /// The object placeholder represents an object expression that evaluates to an object in the Applies To list.
        /// Remarks
        /// Before you use the ShowFont method, you must set the Flags property of the CommonDialog control to one of three constants or values: 
        /// cdlCFBoth or &H3, cdlCFPrinterFonts or &H2, or cdlCFScreenFonts or &H1.  If you don't set Flags, a message box is displayed advising you that "There are no fonts installed," 
        /// and a run-time error occurs.
        /// </summary>
        public void ShowFont()
        {
            //fontDialog = new FontDialog();

            // Clear font dialog defaults
            fontDialog.ShowEffects = false;
            fontDialog.FontMustExist = false;

            FontStyle fontStyle = FontStyle.Regular;
            if (this.FontBold)
            {
                fontStyle |= FontStyle.Bold;
            }
            if (this.FontItalic)
            {
                fontStyle |= FontStyle.Italic;
            }
            if (this.FontUnderline)
            {
                fontStyle |= FontStyle.Underline;
            }
            if (this.FontStrikeThru)
            {
                fontStyle |= FontStyle.Strikeout;
            }
            using (Font newFont = new Font(this.Font, fontStyle))
            {
                fontDialog.Font = newFont;
                if ((Flags & Convert.ToInt32(FontsConstants.cdlCFBoth)) == Convert.ToInt32(FontsConstants.cdlCFBoth))
                {
                    fontDialog.ShowEffects = true;
                    fontDialog.FontMustExist = true;
                }
                else
                {
                    fontDialog.ShowEffects = ((Flags & Convert.ToInt32(FontsConstants.cdlCFEffects)) == Convert.ToInt32(FontsConstants.cdlCFEffects));
                    fontDialog.FontMustExist = ((Flags & Convert.ToInt32(FontsConstants.cdlCFForceFontExist)) == Convert.ToInt32(FontsConstants.cdlCFForceFontExist));
                }

                if (this.Min != -1)
                {
                    fontDialog.MinSize = this.Min;
                }
                if (this.Max != -1)
                {
                    fontDialog.MaxSize = this.Max;
                }

                fontDialog.ShowDialog();

                this.Font = fontDialog.Font;
                this.Color = fontDialog.Color;
                this.FontName = fontDialog.Font.Name;
                this.FontBold = fontDialog.Font.Bold;
                this.FontItalic = fontDialog.Font.Italic;
                this.FontSize = fontDialog.Font.Size;
                this.FontStrikeThru = fontDialog.Font.Strikeout;
                this.FontUnderline = fontDialog.Font.Underline;
            }
        }

        public bool GetFontProperty(FontStyle style)
        {
            switch (style)
            {
                case FontStyle.Bold:
                    {
                        return this.Font.Bold;
                    }
                case FontStyle.Italic:
                    {
                        return this.Font.Italic;
                    }
            }
            return false;
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        private Information.ErrInformation CancelException()
        {
            //https://msdn.microsoft.com/en-us/library/aa259331(v=vs.60).aspx
            var exception = Information.Err(32755);
            exception.Description = "Cancel was selected";

            return exception;
        }
        #endregion
    }
}
