﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// Threed.SSCheck
    /// VB.CheckBox
    /// </summary>
    public partial class FCCheckBox : CheckBox, ISupportInitialize
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        private ValueSettings internalValue = ValueSettings.Unchecked;
        private ToolTip toolTip = new ToolTip();
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private string dataField = "";

        #endregion

        #region Constructors

        public FCCheckBox()
        {
            //this.BackColor = SystemColors.Control;
            InitializeComponent();
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            this.ToolTipText = "";
            this.DataField = "";
            this.CheckState = Wisej.Web.CheckState.Unchecked;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum ValueSettings
        {
            Unchecked = 0,
            Checked = 1,
            Grayed = 2
        }

		#endregion

		#region Properties

		//DDU:#i2330 - fixed globally problem with getting all controls throu a foreach iteration & added DataChanged in T2KDateBox, FCCheckBox & FCRadioButton in order to discover if changes was made to the controls data
		/// <summary>
		/// Returns/sets a value indicating that data in a control has changed by some process other than by retrieving data from the current record.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool DataChanged
		{
			get; set;
		}

		/// <summary>
		/// 
		/// </summary>
		[DefaultValue(HorizontalAlignment.Left)]
        public HorizontalAlignment Alignment
        {
            // TODO:CHE
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Caption
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(ValueSettings.Unchecked)]
        public ValueSettings Value
        {
            get
            {
                return internalValue;
            }
            set
            {
                if (value != internalValue)
                {
                    switch (value)
                    {
                        case ValueSettings.Checked:
                            this.CheckState = Wisej.Web.CheckState.Checked;
                            break;
                        case ValueSettings.Unchecked:
                            this.CheckState = Wisej.Web.CheckState.Unchecked;
                            break;
                        case ValueSettings.Grayed:
                            this.CheckState = Wisej.Web.CheckState.Indeterminate;
                            break;
                    }
                    internalValue = value;
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentX { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentY { get; set; }

        /// <summary>
        /// Gets/sets the DataField associated with the control
        /// </summary>
        [DefaultValue("")]
        public string DataField
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        public void BeginInit()
        {

        }

        public void EndInit()
        {

        }

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// User-defined conversion from FCCheckBox to int
        /// </summary>
        /// <param name="checkBox"></param>
        /// <returns></returns> 
        public static implicit operator int(FCCheckBox checkBox)
        {
            return Convert.ToInt32(checkBox.CheckState);
        }

        /// <summary>
        /// User-defined conversion from FCCheckBox to bool
        /// </summary>
        /// <param name="checkBox"></param>
        /// <returns></returns>
        public static implicit operator bool(FCCheckBox checkBox)
        {
            return checkBox.Checked;
        }

        /// <summary>
        /// overload operator !
        /// </summary>
        /// <param name="checkBox"></param>
        /// <returns></returns>
        public static bool operator !(FCCheckBox checkBox)
        {
            return !checkBox.Checked;
        }

        /// <summary>
        /// overload operator &
        /// </summary>
        /// <param name="checkBox"></param>
        /// <param name="value"></param>
        /// <returns></returns> 
        public static bool operator &(FCCheckBox checkBox, bool value)
        {
            return checkBox.Checked & value;
        }

        /// <summary>
        /// overload operator ==
        /// </summary>
        /// <param name="checkBox"></param>
        /// <param name="value"></param>
        /// <returns></returns> 
        public static bool operator ==(FCCheckBox checkBox, int value)
        {
            return Convert.ToInt32(checkBox.CheckState) == value;
        }

        /// <summary>
        /// overload operator !=
        /// </summary>
        /// <param name="checkBox"></param>
        /// <param name="value"></param>
        /// <returns></returns> 
        public static bool operator !=(FCCheckBox checkBox, int value)
        {
            return Convert.ToInt32(checkBox.CheckState) != value;
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods

        protected override void OnCheckStateChanged(EventArgs e)
        {
            //CHE: do not raise while in InitializeComponent
            Form f = this.FindForm();
            if (f == null || (!f.Created && !((FCForm)f).IsLoaded))
            {
                return;
            }
            UpdateValueByCheckState();
            base.OnCheckStateChanged(e);
			this.DataChanged = true;
		}

        protected override void OnCheckedChanged(EventArgs e)
        {
            //CHE: do not raise while in InitializeComponent
            Form f = this.FindForm();
            if (f == null || (!f.Created && !((FCForm)f).IsLoaded))
            {
                return;
            }
            base.OnCheckedChanged(e);
			this.DataChanged = true;
		}

        #endregion

        #region Private Methods
        /// <summary>
        /// Update the internal Value by the CheckState
        /// </summary>
        private void UpdateValueByCheckState()
        {
            switch (this.CheckState)
            {
                case Wisej.Web.CheckState.Checked:
                    this.Value = ValueSettings.Checked;
                    break;
                case Wisej.Web.CheckState.Unchecked:
                    this.Value = ValueSettings.Unchecked;
                    break;
                case CheckState.Indeterminate:
                    this.Value = ValueSettings.Grayed;
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
