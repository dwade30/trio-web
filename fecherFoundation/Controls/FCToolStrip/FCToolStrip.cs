﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    public partial class FCToolStrip : ToolStrip
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private bool allowCustomize = true;

        #endregion

        #region Constructors

        public FCToolStrip()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// ButtonWith sets the width of all Buttons in the toolstrip
        /// Get will return the width of the first Button
        /// </summary>
        public int ButtonWidth
        {
            get
            {
                if (base.Items == null || base.Items.Count == 0)
                {
                    return base.Size.Width;
                }
                else
                {
                    return base.Items[0].Size.Width;
                }
            }

            set
            {
                foreach (ToolStripItem item in base.Items)
                {
                    item.AutoSize = false;

                    int height = item.Size.Height;

                    item.Size = new System.Drawing.Size(value, height);
                }
            }
        }

        /// <summary>
        /// ButtonHeight sets the Height of all Buttons in the toolstrip
        /// Get will return the Height of the first Button
        /// </summary>
        public int ButtonHeight
        {
            get
            {
                if (base.Items == null || base.Items.Count == 0)
                {
                    return base.Size.Height;
                }
                else
                {
                    return base.Items[0].Size.Height;
                }
            }

            set
            {
                foreach (ToolStripItem item in base.Items)
                {
                    item.AutoSize = false;

                    int width = item.Size.Width;

                    item.Size = new System.Drawing.Size(width, value);
                }
            }
        }

        /// <summary>
        /// Sets the allow customize.
        /// </summary>
        [DefaultValue(true)]
        public bool AllowCustomize
        {
            get
            {
                return this.allowCustomize;
            }
            set
            {
                this.allowCustomize = value;
                base.AllowDrop = base.AllowItemReorder = base.AllowMerge = this.allowCustomize;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
