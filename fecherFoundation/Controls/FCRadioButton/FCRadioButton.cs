﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// Threed.SSOption
    /// VB.OptionButton
    /// </summary>
    public partial class FCRadioButton : RadioButton
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private ToolTip toolTip = new ToolTip();
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private int appearance = 0;
        private Image downpic = null;
        private bool raiseCheckedChange = false;
        //DSE prevent endless loop for value changed
        bool raiseOnClick = true;
        #endregion

        #region Constructors

        public FCRadioButton()
        {
            //this.BackColor = SystemColors.Control;
            InitializeComponent();
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            this.ToolTipText = "";
            this.Style = 0;
            this.Appearance = 1;
        }

		#endregion

		#region Public Delegates
		#endregion

		#region Public Events
		#endregion

		#region Private Events
		#endregion

		#region Enums
		#endregion

		#region Properties

		//DDU:#i2330 - fixed globally problem with getting all controls throu a foreach iteration & added DataChanged in T2KDateBox, FCCheckBox & FCRadioButton in order to discover if changes was made to the controls data
		/// <summary>
		/// Returns/sets a value indicating that data in a control has changed by some process other than by retrieving data from the current record.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool DataChanged
		{
			get; set;
		}

		/// <summary>
		/// Gets or sets Checked Property.
		/// </summary>
		[DefaultValue(false)]
        public bool Value
        {
            get
            {
                return base.Checked;
            }
            set
            {
                base.Checked = value;
                //DSE prevent endless loop
                if (raiseOnClick)
                {
                    raiseOnClick = false;
                    OnClick(EventArgs.Empty);
                    raiseOnClick = true;
                }
            }
        }


        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentX { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentY { get; set; }

        /// <summary>
        /// Gets/sets the DownPicture of the FCRadioButton
        /// </summary>
        [DefaultValue(null)]
        public Image DownPicture
        {
            get
            {
                return downpic;
            }

            set
            {
                downpic = value;
            }
        }

        /// <summary>
        /// Gets/sets the Picture of the FCRadioButton
        /// </summary>
        [DefaultValue(null)]
        public Image Picture
        {
            get
            {
                return base.Image;
            }
            set
            {
                base.Image = value;
            }
        }

        /// <summary>
        /// Gets/sets the Appearance of the FCRadioButton
        /// </summary>
        [DefaultValue(0)]
        public int Style
        {
            get
            {
                return (base.Appearance == Wisej.Web.Appearance.Normal) ? 0 : 1;
            }
            set
            {
                base.Appearance = (value == 0) ? Wisej.Web.Appearance.Normal : Wisej.Web.Appearance.Button;

                if (base.Appearance == Wisej.Web.Appearance.Button)
                {
                    base.BackgroundImage = this.Picture;
                }
                else
                {
                    base.BackgroundImage = null;
                }
            }
        }

        [DefaultValue(1)]
        public new int Appearance
        {
            get
            {
                return this.appearance;
            }
            set
            {
                this.appearance = value;
                //changing appearance in InitializeComponent should not change the BackColor to white for 2D
                Form f = this.FindForm();
                if (f != null && f.Created)
                {
                    if (appearance == 0)
                    {
                        // 0 - 2D
                        this.BackColor = Color.White;
                    }
                    else
                    {
                        // 1- 3D
                        this.BackColor = SystemColors.Control;
                    }
                }
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods

        protected override void OnCheckedChanged(EventArgs e)
        {
            //CHE: do not raise while in InitializeComponent
            Form f = this.FindForm();
            if (f == null || (!f.Created && !((FCForm)f).IsLoaded))
            {
                return;
            }

            //CHE: when a radio button is activated then all the radio buttons from group receive CheckedChanged too but for those the VB6 Click event (associated with CheckChanged) should not be called
            if (!base.Checked)
            {
                return;
            }
            base.OnCheckedChanged(e);
            if (base.Appearance == Wisej.Web.Appearance.Button)
            {
                if (base.Checked)
                {
                    base.BackgroundImage = this.DownPicture;
                }
                else
                {
                    base.BackgroundImage = this.Picture;
                }
            }
        }

        #endregion

        #region Private Methods
        #endregion
    }
}
