﻿using System.Security.Cryptography.X509Certificates;

namespace fecherFoundation
{
    public class FCMenuItemExecutedArgs
    {
        public FCMenuItemExecutedArgs(string itemName, string origin,string caption ,string parentMenu)
        {
            MenuOrigin = origin;
            ItemName = itemName;
            MenuCaption = caption;
            ParentMenu = parentMenu;
        }

        public string MenuOrigin { get; set; } = "";
        public string ItemName { get; set; } = "";
        public string MenuCaption { get; set; } = "";
        public string ParentMenu { get; set; } = "";
    }
}