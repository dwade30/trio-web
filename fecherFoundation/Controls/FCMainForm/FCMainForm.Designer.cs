﻿namespace fecherFoundation
{
    partial class FCMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.navigationPanel = new Wisej.Web.Panel();
            this.navigationBottomPanel = new Wisej.Web.Panel();
            this.statusBarPanel = new Wisej.Web.Panel();
            this.statusLabel3 = new Wisej.Web.Label();
            this.statusLabel2 = new Wisej.Web.Label();
            this.statusLabel1 = new Wisej.Web.Label();
            this.currentUserPanel = new Wisej.Web.Panel();
            this.menuTree = new Wisej.Web.TreeView();
            this.favouritesPanel = new Wisej.Web.Panel();
            this.homePageButton = new fecherFoundation.FCButton();
            this.line1 = new Wisej.Web.Line();
            this.homePagePanel = new Wisej.Web.Panel();
            this.centerPanel = new Wisej.Web.Panel();
            this.modulesPanel = new Wisej.Web.Panel();
            this.pinnedModulesLabel = new Wisej.Web.Label();
            this.inactiveModulesLabel = new Wisej.Web.Label();
            this.currentModulesLabel = new Wisej.Web.Label();
            this.bottomPanel = new Wisej.Web.Panel();
            this.aboutPanel = new Wisej.Web.Panel();
            this.topPanel = new Wisej.Web.Panel();
            this.line2 = new Wisej.Web.Line();
            this.navigationPanel.SuspendLayout();
            this.navigationBottomPanel.SuspendLayout();
            this.statusBarPanel.SuspendLayout();
            this.favouritesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.homePageButton)).BeginInit();
            this.homePagePanel.SuspendLayout();
            this.centerPanel.SuspendLayout();
            this.modulesPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommonDialog1
            // 
            this.CommonDialog1.Color = System.Drawing.Color.Black;
            this.CommonDialog1.DefaultExt = null;
            this.CommonDialog1.FilterIndex = ((short)(0));
            this.CommonDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CommonDialog1.FontName = "Microsoft Sans Serif";
            this.CommonDialog1.FontSize = 8.25F;
            this.CommonDialog1.ForeColor = System.Drawing.Color.Black;
            this.CommonDialog1.FromPage = 0;
            this.CommonDialog1.Location = new System.Drawing.Point(0, 0);
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.PrinterSettings = null;
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            this.CommonDialog1.TabIndex = 0;
            this.CommonDialog1.ToPage = 0;
            // 
            // navigationPanel
            // 
            this.navigationPanel.CollapseSide = Wisej.Web.HeaderPosition.Right;
            this.navigationPanel.Controls.Add(this.navigationBottomPanel);
            this.navigationPanel.Controls.Add(this.menuTree);
            this.navigationPanel.Dock = Wisej.Web.DockStyle.Left;
            this.navigationBottomPanel.ForeColor = System.Drawing.Color.FromName("@navigationModuleNameForeColor");
            this.navigationPanel.HeaderSize = 55;
            this.navigationPanel.Location = new System.Drawing.Point(60, 0);
            this.navigationPanel.Margin = new Wisej.Web.Padding(0);
            this.navigationPanel.Name = "navigationPanel";
            this.navigationPanel.ShowCloseButton = false;
            this.navigationPanel.ShowHeader = true;
            this.navigationPanel.Size = new System.Drawing.Size(300, 503);
            this.navigationPanel.TabIndex = 4;
            // 
            // navigationBottomPanel
            // 
            this.navigationBottomPanel.BackColor = System.Drawing.Color.FromName("@navigationBackground");
            this.navigationBottomPanel.Controls.Add(this.statusBarPanel);
            this.navigationBottomPanel.Controls.Add(this.currentUserPanel);
            this.navigationBottomPanel.Dock = Wisej.Web.DockStyle.Bottom;
            this.navigationBottomPanel.Location = new System.Drawing.Point(0, 318);
            this.navigationBottomPanel.Name = "navigationBottomPanel";
            this.navigationBottomPanel.Size = new System.Drawing.Size(300, 130);
            this.navigationBottomPanel.TabIndex = 1;
            // 
            // statusBarPanel
            // 
            this.statusBarPanel.ForeColor = System.Drawing.Color.FromName("@statusBarForeColor");
            this.statusBarPanel.Controls.Add(this.line2);
            this.statusBarPanel.Controls.Add(this.statusLabel3);
            this.statusBarPanel.Controls.Add(this.statusLabel2);
            this.statusBarPanel.Controls.Add(this.statusLabel1);
            this.statusBarPanel.Location = new System.Drawing.Point(0, 85);
            this.statusBarPanel.Name = "statusBarPanel";
            this.statusBarPanel.Size = new System.Drawing.Size(300, 45);
            this.statusBarPanel.TabIndex = 1;
            // 
            // statusLabel3
            // 
            this.statusLabel3.AutoSize = true;
            this.statusLabel3.ForeColor = System.Drawing.Color.FromName("@statusBarForeColor");
            this.statusLabel3.Location = new System.Drawing.Point(20, 25);
            this.statusLabel3.Name = "statusLabel3";
            this.statusLabel3.Size = new System.Drawing.Size(219, 15);
            this.statusLabel3.TabIndex = 2;
            this.statusLabel3.Text = "";
            this.statusLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusLabel2
            // 
            this.statusLabel2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.statusLabel2.AutoSize = true;
            this.statusLabel2.ForeColor = System.Drawing.Color.FromName("@statusBarForeColor");
            this.statusLabel2.Location = new System.Drawing.Point(172, 5);
            this.statusLabel2.Name = "statusLabel2";
            this.statusLabel2.Size = new System.Drawing.Size(108, 15);
            this.statusLabel2.TabIndex = 1;
            this.statusLabel2.Text = "";
            this.statusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.statusLabel2.Click += new System.EventHandler(StatusLabel2_Click);
            // 
            // statusLabel1
            // 
            this.statusLabel1.AutoSize = true;
            this.statusLabel1.ForeColor = System.Drawing.Color.FromName("@statusBarForeColor");
            this.statusLabel1.Location = new System.Drawing.Point(20, 5);
            this.statusLabel1.Name = "statusLabel1";
            this.statusLabel1.Size = new System.Drawing.Size(108, 15);
            this.statusLabel1.TabIndex = 0;
            this.statusLabel1.Text = "";
            this.statusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.statusLabel1.Click += new System.EventHandler(StatusLabel1_Click);
            // 
            // currentUserPanel
            // 
            this.currentUserPanel.Location = new System.Drawing.Point(0, 0);
            this.currentUserPanel.Name = "currentUserPanel";
            this.currentUserPanel.Size = new System.Drawing.Size(300, 85);
            this.currentUserPanel.TabIndex = 0;
            // 
            // menuTree
            // 
            this.menuTree.Location = new System.Drawing.Point(0, 0);
            this.menuTree.Margin = new Wisej.Web.Padding(0);
            this.menuTree.Name = "menuTree";
            this.menuTree.Size = new System.Drawing.Size(300, 318);
            this.menuTree.TabIndex = 0;
            // 
            // favouritesPanel
            // 
            this.favouritesPanel.BackColor = System.Drawing.Color.FromName("@favouritesBackground");
            this.favouritesPanel.Controls.Add(this.homePageButton);
            this.favouritesPanel.Controls.Add(this.line1);
            this.favouritesPanel.Dock = Wisej.Web.DockStyle.Left;
            this.favouritesPanel.Location = new System.Drawing.Point(0, 0);
            this.favouritesPanel.Name = "favouritesPanel";
            this.favouritesPanel.Size = new System.Drawing.Size(60, 503);
            this.favouritesPanel.TabIndex = 6;
            // 
            // homePageButton
            // 
            this.homePageButton.AppearanceKey = "toolbarButton";
            this.homePageButton.BackColor = System.Drawing.Color.FromName("@homeButtonBackground");
            this.homePageButton.Location = new System.Drawing.Point(10, 10);
            this.homePageButton.Name = "homePageButton";
            this.homePageButton.Size = new System.Drawing.Size(40, 40);
            this.homePageButton.TabIndex = 0;
            this.homePageButton.Click += new System.EventHandler(this.homePageButton_Click);
            // 
            // line1
            // 
            this.line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.line1.LineSize = 1;
            this.line1.Location = new System.Drawing.Point(0, 60);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(60, 1);
            // 
            // homePagePanel
            // 
            this.homePagePanel.BackColor = System.Drawing.Color.White;
            this.homePagePanel.Controls.Add(this.centerPanel);
            this.homePagePanel.Controls.Add(this.topPanel);
            this.homePagePanel.Dock = Wisej.Web.DockStyle.Fill;
            this.homePagePanel.Location = new System.Drawing.Point(360, 0);
            this.homePagePanel.Name = "homePagePanel";
            this.homePagePanel.Size = new System.Drawing.Size(524, 503);
            this.homePagePanel.TabIndex = 1;
            this.homePagePanel.Visible = false;
            // 
            // centerPanel
            // 
            this.centerPanel.AutoScroll = true;
            this.centerPanel.Controls.Add(this.modulesPanel);
            this.centerPanel.Controls.Add(this.bottomPanel);
            this.centerPanel.Dock = Wisej.Web.DockStyle.Fill;
            this.centerPanel.Location = new System.Drawing.Point(0, 84);
            this.centerPanel.Name = "centerPanel";
            this.centerPanel.Size = new System.Drawing.Size(524, 419);
            this.centerPanel.TabIndex = 3;
            // 
            // modulesPanel
            // 
            this.modulesPanel.Controls.Add(this.pinnedModulesLabel);
            this.modulesPanel.Controls.Add(this.inactiveModulesLabel);
            this.modulesPanel.Controls.Add(this.currentModulesLabel);
            this.modulesPanel.Location = new System.Drawing.Point(0, 0);
            this.modulesPanel.Name = "modulesPanel";
            this.modulesPanel.ShowCloseButton = false;
            this.modulesPanel.Size = new System.Drawing.Size(812, 100);
            this.modulesPanel.TabIndex = 0;
            this.modulesPanel.Text = "PINNED MODULES";
            // 
            // pinnedModulesLabel
            // 
            this.pinnedModulesLabel.AutoSize = true;
            this.pinnedModulesLabel.Font = new System.Drawing.Font("default", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.pinnedModulesLabel.Location = new System.Drawing.Point(20, 0);
            this.pinnedModulesLabel.Name = "pinnedModulesLabel";
            this.pinnedModulesLabel.Size = new System.Drawing.Size(176, 22);
            this.pinnedModulesLabel.TabIndex = 0;
            this.pinnedModulesLabel.Text = "PINNED MODULES";
            this.pinnedModulesLabel.Visible = false;
            // 
            // currentModulesLabel
            // 
            this.currentModulesLabel.AutoSize = true;
            this.currentModulesLabel.Font = new System.Drawing.Font("default", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.currentModulesLabel.Location = new System.Drawing.Point(20, 100);
            this.currentModulesLabel.Name = "currentModulesLabel";
            this.currentModulesLabel.Size = new System.Drawing.Size(176, 22);
            this.currentModulesLabel.TabIndex = 1;
            this.currentModulesLabel.Text = "YOUR MODULES";
            this.currentModulesLabel.Visible = false;
            // 
            // inactiveModulesLabel
            // 
            this.inactiveModulesLabel.AutoSize = true;
            this.inactiveModulesLabel.Font = new System.Drawing.Font("default", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.inactiveModulesLabel.Location = new System.Drawing.Point(20, 200);
            this.inactiveModulesLabel.Name = "inactiveModulesLabel";
            this.inactiveModulesLabel.Size = new System.Drawing.Size(176, 22);
            this.inactiveModulesLabel.TabIndex = 2;
            this.inactiveModulesLabel.Text = "ADDITIONAL MODULES";
            this.inactiveModulesLabel.Visible = false;            
            // 
            // bottomPanel
            // 
            this.bottomPanel.BackColor = System.Drawing.Color.Transparent;
            this.bottomPanel.Controls.Add(this.aboutPanel);
            this.bottomPanel.Location = new System.Drawing.Point(0, 367);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(524, 136);
            this.bottomPanel.TabIndex = 2;
            // 
            // aboutPanel
            // 
            this.aboutPanel.Location = new System.Drawing.Point(93, 17);
            this.aboutPanel.Name = "aboutPanel";
            this.aboutPanel.Size = new System.Drawing.Size(360, 116);
            this.aboutPanel.TabIndex = 0;
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.Transparent;
            this.topPanel.Dock = Wisej.Web.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(524, 84);
            this.topPanel.TabIndex = 1;
            // 
            // line2
            // 
            this.line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.line2.Location = new System.Drawing.Point(0, 0);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(300, 1);
            // 
            // FCMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 503);
            this.Controls.Add(this.homePagePanel);
            this.Controls.Add(this.navigationPanel);
            this.Controls.Add(this.favouritesPanel);
            this.FillColor = 16777215;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.Name = "FCMainForm";
            this.Text = "FCMainForm";
            this.WindowState = Wisej.Web.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FCMainForm_Load);
            this.navigationPanel.ResumeLayout(false);
            this.navigationBottomPanel.ResumeLayout(false);
            this.statusBarPanel.ResumeLayout(false);
            this.statusBarPanel.PerformLayout();
            this.favouritesPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.homePageButton)).EndInit();
            this.homePagePanel.ResumeLayout(false);
            this.centerPanel.ResumeLayout(false);
            this.modulesPanel.ResumeLayout(false);
            this.modulesPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public Wisej.Web.Panel navigationPanel;
        public Wisej.Web.TreeView menuTree;
        public Wisej.Web.Panel favouritesPanel;
        public fecherFoundation.FCButton homePageButton;
        public Wisej.Web.Panel homePagePanel;
        public Wisej.Web.Panel modulesPanel;
        public Wisej.Web.Panel topPanel;
        public Wisej.Web.Panel bottomPanel;
        public Wisej.Web.Panel centerPanel;
        public Wisej.Web.Label pinnedModulesLabel;
        public Wisej.Web.Label inactiveModulesLabel;
        public Wisej.Web.Label currentModulesLabel;
        public Wisej.Web.Panel aboutPanel;
        public fecherFoundation.FCCommonDialog CommonDialog1;
        private Wisej.Web.Line line1;
        private Wisej.Web.Panel navigationBottomPanel;
        private Wisej.Web.Panel statusBarPanel;
        private Wisej.Web.Label statusLabel3;
        private Wisej.Web.Label statusLabel2;
        private Wisej.Web.Label statusLabel1;
        public Wisej.Web.Panel currentUserPanel;
        private Wisej.Web.Line line2;
    }
}