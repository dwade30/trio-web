﻿using System;
using System.CodeDom;
using System.Drawing;
using Wisej.Web;
using System.Threading;

namespace fecherFoundation
{
    public partial class FCModuleButton : Wisej.Web.UserControl
    {
        private FCMainFormModule module;
        private bool selected = false;
        private Wisej.Web.Timer clickTimer = new Wisej.Web.Timer();
        public FCModuleButton()
        {
            InitializeComponent();
            this.Click += module_Click;
            clickTimer.Tick += ClickTimer_Tick;
        }

        private void ClickTimer_Tick(object sender, EventArgs e)
        {
            clickTimer.Stop();
            this.Enabled = true;
        }

        public override string Text
        {
            get
            {
                return this.moduleLabel.Text;
            }

            set
            {
                this.moduleLabel.Text = value;
            }
        }

        internal string Image
        {
            get
            {
                return this.button.BackgroundImageSource;
            }

            set
            {
                this.button.BackgroundImageSource = value;
            }
        }

        internal Color BackColor
        {
            get
            {
                return this.button.BackColor;
            }
            set
            {
                this.button.BackColor = value;
            }
        }

        internal FCMainFormModule Module
        {
            get
            {
                return this.module;
            }

            set
            {
                this.module = value;
            }
        }

        private void module_Click(object sender, EventArgs e)
        {
            if (this.module != null && this.module.Collection != null && this.module.Collection.MainForm != null && this.Enabled)
            {
                this.Enabled = false;
                clickTimer.Interval = 1000;
                clickTimer.Start();
                this.module.Collection.MainForm.OnModuleClick(this.module);
            }
        }

        private void pinPicture_Click(object sender, EventArgs e)
        {
            if (this.module != null)
            {
                if (this.module.Collection != null)
                {
                    if (this.module.Collection.MaxPinnedModules())
                    {
                        FCMessageBox.Show("You have reached the maximum pinned modules");
                        return;
                    }
                }

                this.module.IsPinned = true;

                if (this.module.Collection != null && this.module.Collection.MainForm != null)
                {
                    this.module.Collection.MainForm.PinModuleClick();
                }
            }
        }
    }
}
