﻿using System;
using System.Drawing;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCInactiveButton : Wisej.Web.UserControl
    {
        private FCMainFormModule module;
        private bool selected = false;

        public FCInactiveButton()
        {
            InitializeComponent();
        }

        public override string Text
        {
            get
            {
                return this.moduleLabel.Text;
            }

            set
            {
                this.moduleLabel.Text = value;
            }
        }

        internal string Image
        {
            get
            {
                return this.button.BackgroundImageSource;
            }

            set
            {
                this.button.BackgroundImageSource = value;
            }
        }

        internal Color BackColor
        {
            get
            {
                return this.button.BackColor;
            }
            set
            {
                this.button.BackColor = value;
            }
        }

        internal FCMainFormModule Module
        {
            get
            {
                return this.module;
            }

            set
            {
                this.module = value;
            }
        }
    }
}
