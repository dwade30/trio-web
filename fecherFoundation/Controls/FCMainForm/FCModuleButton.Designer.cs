﻿namespace fecherFoundation
{
    partial class FCModuleButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FCModuleButton));
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            Wisej.Web.JavaScript.ClientEvent clientEvent2 = new Wisej.Web.JavaScript.ClientEvent();
            Wisej.Web.JavaScript.ClientEvent clientEvent3 = new Wisej.Web.JavaScript.ClientEvent();
            this.button = new Wisej.Web.Button();
            this.moduleLabel = new Wisej.Web.Label();
            this.pinPicture = new Wisej.Web.PictureBox();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pinPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // button
            // 
            this.button.AppearanceKey = "flatButton";
            this.button.BackgroundImageLayout = Wisej.Web.ImageLayout.Zoom;
            this.button.Display = Wisej.Web.Display.Icon;
            this.button.ForeColor = System.Drawing.Color.White;
            this.button.Location = new System.Drawing.Point(8, 8);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(42, 42);
            this.button.TabIndex = 0;
            this.button.TextImageRelation = Wisej.Web.TextImageRelation.ImageAboveText;
            this.button.Click += new System.EventHandler(this.module_Click);
            // 
            // moduleLabel
            // 
            this.moduleLabel.AutoSize = true;
            this.moduleLabel.Cursor = Wisej.Web.Cursors.Hand;
            this.moduleLabel.Font = new System.Drawing.Font("default", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.moduleLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            this.moduleLabel.Location = new System.Drawing.Point(66, 17);
            this.moduleLabel.Name = "moduleLabel";
            this.moduleLabel.Size = new System.Drawing.Size(4, 23);
            this.moduleLabel.TabIndex = 1;
            this.moduleLabel.Click += new System.EventHandler(this.module_Click);
            // 
            // pinPicture
            // 
            this.pinPicture.BackgroundImageSource = "icon-pin?color=#1769FF";
            this.pinPicture.BackgroundImageLayout = Wisej.Web.ImageLayout.Stretch;
            this.pinPicture.Cursor = Wisej.Web.Cursors.Hand;
            this.pinPicture.Location = new System.Drawing.Point(310, 16);
            this.pinPicture.Name = "pinPicture";
            this.pinPicture.Size = new System.Drawing.Size(0, 25);
            this.pinPicture.TabIndex = 2;
            this.pinPicture.Click += new System.EventHandler(this.pinPicture_Click);
            // 
            // FCModuleButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.Controls.Add(this.pinPicture);
            this.Controls.Add(this.moduleLabel);
            this.Controls.Add(this.button);
            clientEvent1.Event = "appear";
            clientEvent1.JavaScript = "//alert(\'appear\');\r\n//this.pinPicture.__contentElement.__element.style.visibil" +
    "ity = \"hidden\";";
            clientEvent2.Event = "pointerover";
            clientEvent2.JavaScript = resources.GetString("clientEvent2.JavaScript");
            clientEvent3.Event = "pointerout";
            clientEvent3.JavaScript = resources.GetString("clientEvent3.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this).Add(clientEvent1);
            this.javaScript1.GetJavaScriptEvents(this).Add(clientEvent2);
            this.javaScript1.GetJavaScriptEvents(this).Add(clientEvent3);
            this.Name = "FCModuleButton";
            this.Size = new System.Drawing.Size(350, 58);
            ((System.ComponentModel.ISupportInitialize)(this.pinPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Wisej.Web.Button button;
        private Wisej.Web.Label moduleLabel;
        private Wisej.Web.PictureBox pinPicture;
        private Wisej.Web.JavaScript javaScript1;
    }
}
