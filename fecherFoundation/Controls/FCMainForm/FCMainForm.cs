﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using Wisej.Web;


namespace fecherFoundation
{
    public partial class FCMainForm : FCForm
    {
        private FCNavigationMenu navigationMenu;
        private FCMainFormModules modules;
        private int lockModuleSwitch = 0;
        //private string statusBarText1 = "";
        //private string statusBarText2 = "";
		//FC:FINAL:DSE:#i2391 Prevent recursive method calling
		private bool nodeMouseClickFiredFromExpand = false;
        public FCNavigationMenu NavigationMenu
        {
            get
            {
                return navigationMenu;
            }
        }

        public string ApplicationIcon { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Dictionary<string, string> ApplicationIcons { get; set; }

        [DefaultValue("")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string StatusBarText1
        {
            get
            {
                return statusLabel1.Text;
            }
            set
            {
                statusLabel1.Text = value;
                UpdateStatusBarSize();
            }
        }

        [DefaultValue("")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string StatusBarText2
        {
            get
            {
                return statusLabel2.Text;
            }
            set
            {
                statusLabel2.Text = value;
                UpdateStatusBarSize();
            }
        }

        [DefaultValue("")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string StatusBarText3
        {
            get
            {
                return statusLabel3.Text;
            }
            set
            {
                statusLabel3.Text = value;
                UpdateStatusBarSize();
            }
        }

        public FCMainForm()
        {
            InitializeComponent();
            this.ApplicationIcons = new Dictionary<string, string>();
            this.navigationMenu = new FCNavigationMenu();
            this.navigationMenu.CollectionChanged += NavigationMenu_CollectionChanged;
            this.menuTree.NodeMouseClick += MenuTree_NodeMouseClick;
            this.menuTree.AfterExpand += MenuTree_AfterExpand;
            this.modules = new FCMainFormModules(this);
            this.MdiClient.TabControl.SelectedIndexChanged += new EventHandler(TabControl_SelectedIndexChanged);
            this.MdiClient.TabControl.ControlRemoved += new ControlEventHandler(TabControl_ControlRemoved);
        }
        
        public static FCMainForm InstancePtr
        {
            get
            {
                return (FCMainForm)Sys.GetInstance(typeof(FCMainForm));
            }
            set
            {
                Sys.SaveInstance("fecherFoundation.fecherFoundation.FCMainForm", value);
            }
        }

        protected FCMainForm _InstancePtr = null;

        public string Caption
        {
            get
            {
                return this.navigationPanel.Text;
            }
            set
            {
                this.navigationPanel.Text = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FCMainFormModules Modules
        {
            get
            {
                return this.modules;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HomePageVisible
        {
            get
            {
                return this.homePagePanel.Visible;
            }
            set
            {
                this.homePagePanel.Visible = value;
                this.favouritesPanel.Visible = !value;
                this.navigationPanel.Visible = !value;
                this.MdiClient.Visible = !value;
            }
        }
        

        private void MenuTree_AfterExpand(object sender, TreeViewEventArgs e)
        {
            FCMenuItem item = e.Node.UserData.Info;
            //FC:FINAL:DSE:#i2391 If event is fired from mouse click on the '+' button, NodeMouseClick event is not fired at all
            nodeMouseClickFiredFromExpand = true;
            if (!item.actionExecuted)
            {
                MenuTree_NodeMouseClick(sender, new TreeNodeMouseClickEventArgs(e.Node, MouseButtons.Left, 1, 0, 0));
                if (!item.AllowExpand)
                {
                    e.Node.Collapse();
                }
            }
			nodeMouseClickFiredFromExpand = false;
            int index = e.Node.Index;
            TreeNode parent = e.Node.Parent;
            while(parent != null)
            {
                index = parent.Index;
                parent = parent.Parent;
            }
            for (int i = 0; i < this.menuTree.Nodes.Count; i++)
            {
                //Collapse all first level nodes except the parent of the expanded node
                if (i != index)
                {
                    this.menuTree.Nodes[i].CollapseAll();
                }
                else
                {
                    parent = e.Node.Parent;
                    if (parent != null)
                    {
                        //Collapse all siblings of the expanded node
                        for (int j = 0; j < parent.Nodes.Count; j++)
                        {
                            if (j != e.Node.Index)
                            {
                                parent.Nodes[j].CollapseAll();
                            }
                        }
                    }
                }
            }
        }


        private void MenuTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            FCMenuItem item = e.Node.UserData.Info;
            this.NavigationMenu.CurrentItem = item;
            //FC:FINAL:SBE - #i2141 - execute action only if item does not have subnodes
            if (e.Node.Nodes == null || e.Node.Nodes.Count == 0)
            {
                this.NavigationMenu.Execute(item);
            }
            if (e.Node.Nodes != null && e.Node.Nodes.Count > 0)
            {
				//FC:FINAL:DSE:#i2391 Prevent automaticaly collapsing the menu  nodes when calling NodeMouseClick event from AfterExpand event handler
                if (e.Node.IsExpanded && !nodeMouseClickFiredFromExpand)
                {
                    e.Node.Collapse();
                }
                else
                {
                    //FC:FINAL:SBE - #i2141 - execute action if item has subnodes, and the item is expanded. When it is collapsed, we don't have to execute the action
                    this.NavigationMenu.Execute(item);
                    //FC:FINAL:SBE - do not execute action twice, when expand icon is clicked
                    item.actionExecuted = true;
                    if (item.AllowExpand)
                    {
                        e.Node.Expand();
                    }
                    item.actionExecuted = false;
                }
            }
            if (this.MdiClient.TabControl.SelectedTab != null)
            {
                this.MdiClient.TabControl.SelectedTab.UserData.Node = e.Node;
            }
        }

        private void TabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.MdiClient.TabControl.SelectedTab != null)
            {
                TreeNode node = this.MdiClient.TabControl.SelectedTab.UserData.Node as TreeNode;
                if (node != null)
                {
                    this.menuTree.SelectedNode = node;
                }
            }
        }

        private void TabControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            TabPage tabPage = e.Control as TabPage;
            if (tabPage != null)
            {
                TreeNode node = tabPage.UserData.Node as TreeNode;
                if (node != null && this.menuTree.SelectedNode != null && node == this.menuTree.SelectedNode)
                {
                    this.menuTree.SelectedNode = null;
                }
            }
        }

        private void NavigationMenu_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (FCMenuItem item in e.NewItems)
                {
                    FCRigthTreeNode node = item.CreateTreeNode();
                    switch (item.Level)
                    {
                        case 1: this.menuTree.Nodes.Add(node); break;
                        case 2: this.menuTree.Nodes.Last().Nodes.Add(node); break;
                        case 3: this.menuTree.Nodes.Last().Nodes.Last().Nodes.Add(node); break;
                        case 4: this.menuTree.Nodes.Last().Nodes.Last().Nodes.Last().Nodes.Add(node); break;
                    }
                    if (item.Level > 1)
                    {
                        node.NodeFont = new System.Drawing.Font("@treeChildNode", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
                    }

                    item.SubItems.CollectionChanged += NavigationMenu_CollectionChanged;
                }
            }
            else if(e.Action == NotifyCollectionChangedAction.Reset)
            {
                this.menuTree.Nodes.Clear();
            }
        }

        private void FCMainForm_Load(object sender, System.EventArgs e)
        {
            this.CenterControls();
            this.UpdateStatusBarSize();            
        }

        public void RefreshModules()
        {
            int pinnedCurrent = 1;
            int moduleCurrent = 1;
            int inactiveCurrent = 1;
            int pinnedModulesCount = this.modules.Where(module => module.IsPinned && module.IsActive).Count();
            int inactiveModulesCount = this.modules.Where(module => !module.IsActive).Count();
            int notPinnedModulesCount = this.modules.Count - pinnedModulesCount - inactiveModulesCount;
            int pinnedRows = 0;
            int notPinnedRows = 0;
            if (pinnedModulesCount > 0)
            {
                pinnedRows = ((pinnedModulesCount - 1) / 6) + 1;
                pinnedModulesLabel.Visible = true;
            }
            else
            {
                pinnedModulesLabel.Visible = false;
            }
            this.currentModulesLabel.Visible = notPinnedModulesCount > 0;
            this.inactiveModulesLabel.Visible = inactiveModulesCount > 0;

            int colNotPinnedCount = Convert.ToInt32(Math.Ceiling((decimal)(notPinnedModulesCount) / 3));
            notPinnedRows = ((notPinnedModulesCount - 1) / 2) + 1;

            int colInactiveCount = Convert.ToInt32(Math.Ceiling((decimal)(inactiveModulesCount) / 2));

            int PINNED_HEIGHT = 204;
            int PINNED_WIDTH = 160;
            int MODULE_HEIGHT = 58;
            int MODULE_WIDTH = 350;
            int TOP_PADDING = 25;
            int MODULES_GAP = 34;
            int FAVOURITES_TOP = 60;
            int FAVOURITES_HEIGHT = 52;
            int ALIGN_LEFT = 12;
            
            int modulePanelHeight = 0;
            if (pinnedRows > 0)
            {
                modulePanelHeight = this.currentModulesLabel.Top = this.pinnedModulesLabel.Top + pinnedRows * PINNED_HEIGHT + TOP_PADDING + MODULES_GAP;
                modulePanelHeight += this.pinnedModulesLabel.Height; 
            }
            else
            {
                this.currentModulesLabel.Top = 0;
            }

            modulePanelHeight += colNotPinnedCount * MODULE_HEIGHT + (notPinnedModulesCount == 0 ? 0 : TOP_PADDING + MODULES_GAP);

            if (inactiveModulesCount > 0)
            {
                this.inactiveModulesLabel.Top = modulePanelHeight;
                modulePanelHeight = this.inactiveModulesLabel.Top + this.inactiveModulesLabel.Height + colInactiveCount * MODULE_HEIGHT + TOP_PADDING + MODULES_GAP;
            }            

            foreach (var module in this.modules.OrderByDescending(module=>module.IsActive).ThenBy(module => module.Text))
            {
                module.FavouritesButton.Parent = null;

                if (module.IsActive)
                {
                    //module.FavouritesButton.Parent = null;
                    //add pinned button to form if it was not added before
                    if (module.IsPinned && !this.modulesPanel.Contains(module.PinnedModuleButton))
                    {
                        this.modulesPanel.Controls.Add(module.PinnedModuleButton);
                    }
                    //add favourites button to form if it was not added before
                    if (module.IsPinned && !this.favouritesPanel.Controls.Contains(module.FavouritesButton))
                    {
                        this.favouritesPanel.Controls.Add(module.FavouritesButton);
                    }
                    //add module button to form if it was not added before
                    if (!module.IsPinned && !this.modulesPanel.Contains(module.ModuleButton))
                    {
                        this.modulesPanel.Controls.Add(module.ModuleButton);
                    }

                    if (module.IsPinned)
                    {
                        //pinned button
                        int left = ((pinnedCurrent - 1) % 6) * PINNED_WIDTH;
                        int top = ((pinnedCurrent - 1) / 6) * PINNED_HEIGHT;
                        module.PinnedModuleButton.Left = left;
                        module.PinnedModuleButton.Top = top + MODULES_GAP;

                        //favourites button
                        int favouritesTop = ((pinnedCurrent - 1) * FAVOURITES_HEIGHT) + FAVOURITES_TOP;
                        module.FavouritesButton.Top = favouritesTop;
                        module.FavouritesButton.Left = 0;

                        if (pinnedCurrent == 2)
                        {
                            module.FavouritesButton.Active = true;
                        }
                        else
                        {
                            module.FavouritesButton.Active = false;
                        }

                        pinnedCurrent++;
                    }
                    else
                    {
                        // add current modules
                        int left = (moduleCurrent - 1) % 3 * MODULE_WIDTH + ALIGN_LEFT;
                        int top = (moduleCurrent - 1) / 3 * MODULE_HEIGHT + this.currentModulesLabel.Top + MODULES_GAP;                       

                        module.ModuleButton.Top = top;
                        module.ModuleButton.Left = left;

                        moduleCurrent++;
                    }
                }
                else
                {
                    if (!this.modulesPanel.Contains(module.InactiveButton))
                    {
                        this.modulesPanel.Controls.Add(module.InactiveButton);
                    }
                    // add inactive modules
                    int left = (inactiveCurrent - 1) % 3 * MODULE_WIDTH + ALIGN_LEFT;
                    int top = (inactiveCurrent - 1) / 3 * MODULE_HEIGHT + this.inactiveModulesLabel.Top + MODULES_GAP;

                    module.InactiveButton.Top = top;
                    module.InactiveButton.Left = left;

                    inactiveCurrent++;
                }
            }

            this.modulesPanel.Height = modulePanelHeight;
            this.bottomPanel.Top = this.modulesPanel.Top + this.modulesPanel.Height;
        }

        private void homePageButton_Click(object sender, EventArgs e)
        {
            this.HomePageVisible = true;
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.CenterControls();
        }

        private void CenterControls()
        {
            if (this.modulesPanel != null)
            {
                //center modules list
                this.CenterControl(this.modulesPanel, this);
            }
            if (this.bottomPanel != null)
            {
                this.CenterControl(this.bottomPanel, this);
            }
            if (this.aboutPanel != null)
            {
                //center about panel
                this.CenterControl(this.aboutPanel, this.bottomPanel);
            }
        }

        private void CenterControl(Control child, ScrollableControl parent)
        {
            if (parent.Width < child.Width)
            {
                child.Left = 0;
            }
            else
            {
                child.Left = (parent.Width - child.Width) / 2;
            }
        }

        public virtual void OnModuleClick(FCMainFormModule module, bool hideHomePage = true)
        {
            foreach(var button in this.favouritesPanel.Controls)
            {
                if (button is FCFavouritesModuleButton)
                {
                    FCFavouritesModuleButton favouriteButton = button as FCFavouritesModuleButton;
                    if (favouriteButton.Module == module)
                    {
                        favouriteButton.Active = true;
                    }
                    else
                    {
                        favouriteButton.Active = false;
                    }
                }
            }
            if (hideHomePage)
            {
                this.HomePageVisible = false;
            }
        }

        public virtual void LoadColorScheme(bool archiveMode = false)
        {

        }

		public virtual void PinModuleClick()
        {
        }

        public virtual void ConfigureSettingsContextMenu(bool permissionSystemMaintenance, bool permissionEliminate, bool permissionDelete)
        {
        }

        private void UpdateStatusBarSize()
        {
            int statusBarHeight = 0;
            if (!string.IsNullOrEmpty(statusLabel1.Text) || !string.IsNullOrEmpty(statusLabel2.Text))
            {
                statusBarHeight += 20;
                statusLabel3.Top = 20;
            }
            else
            {
                statusLabel3.Top = 5;
            }

            if (!string.IsNullOrEmpty(statusLabel3.Text))
            {
                statusBarHeight += 20;
            }

            this.statusBarPanel.Height = statusBarHeight;
            this.navigationBottomPanel.Height = this.currentUserPanel.Height + this.statusBarPanel.Height;
            //55px represents panel header
            this.menuTree.Height = this.navigationPanel.Height - (this.navigationBottomPanel.Height + 55);
        }

        private void StatusLabel2_Click(object sender, System.EventArgs e)
        {
            OnStatusBarText2Click();
        }

        protected virtual void OnStatusBarText2Click()
        {

        }

        private void StatusLabel1_Click(object sender, System.EventArgs e)
        {
            OnStatusBarText1Click();
        }

        protected virtual void OnStatusBarText1Click()
        {

        }

        public virtual int OpenModule(string module, bool addExitMenu = true, bool checkIfModuleIsAlreadyOpened = false)
        {
            return 0;
        }

        public virtual void ReloadNavigationMenu()
        {

        }

        public virtual void WaitForMVModule()
        {
            //MessageBox.Show("Motor Vehicle was started, and the rest of the application is locked. \r\n Use \"Exit Motor Vehicle\" menu item to unlock the application");
            FCUtils.UnlockUserInterface();
        }

        public virtual void EndWaitRBModule()
        {

        }

        public virtual void WaitForRBModule()
        {
            //MessageBox.Show("Blue Book was started, and the rest of the application is locked. \r\n Use \"Exit Blue Book\" menu item to unlock the application");
            FCUtils.UnlockUserInterface();
        }

        public virtual void EndWaitMVModule()
        {

        }

        public virtual void WaitForCKModule()
        {
            //MessageBox.Show("Clerk was started, and the rest of the application is locked. \r\n Use \"Exit Clerk\" menu item to unlock the application");
            FCUtils.UnlockUserInterface();
        }

        public virtual void EndWaitCKModule()
        {

        }

        public void LockModuleSwitch()
        {
            //FC:FINAL:SBE - no need to lok/unlock the module switch
            //this.lockModuleSwitch++;
            //this.favouritesPanel.Enabled = false;
        }

        public void UnlockModuleSwitch()
        {
            //FC:FINAL:SBE - no need to lok/unlock the module switch
            //this.lockModuleSwitch--;
            //if (this.lockModuleSwitch == 0)
            //{
            //    this.favouritesPanel.Enabled = true;
            //}
        }

        public void ResetLock()
        {
            this.lockModuleSwitch = 0;
            this.favouritesPanel.Enabled = true;
        }
    }
}
