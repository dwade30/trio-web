﻿namespace fecherFoundation
{
    partial class FCFavouritesModuleButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button = new fecherFoundation.FCButton();
            this.line1 = new Wisej.Web.Line();
            this.SuspendLayout();
            // 
            // button
            // 
            this.button.AppearanceKey = "disabledFlatButton";
            this.button.BackgroundImageLayout = Wisej.Web.ImageLayout.Zoom;
            this.button.Display = Wisej.Web.Display.Icon;
            this.button.ForeColor = System.Drawing.Color.White;
            this.button.Location = new System.Drawing.Point(10, 10);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(40, 40);
            this.button.TabIndex = 0;
            this.button.TextImageRelation = Wisej.Web.TextImageRelation.ImageAboveText;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // line1
            // 
            this.line1.LineColor = System.Drawing.Color.White;
            this.line1.LineSize = 2;
            this.line1.Location = new System.Drawing.Point(0, 10);
            this.line1.Name = "line1";
            this.line1.Orientation = Wisej.Web.Orientation.Vertical;
            this.line1.Size = new System.Drawing.Size(2, 40);
            // 
            // FCFavouritesModuleButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.Controls.Add(this.line1);
            this.Controls.Add(this.button);
            this.Name = "FCFavouritesModuleButton";
            this.Size = new System.Drawing.Size(60, 52);
            this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCButton button;
        private Wisej.Web.Line line1;
    }
}
