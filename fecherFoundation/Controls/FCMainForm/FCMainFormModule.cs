﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public class FCMainFormModule
    {
        private string text = string.Empty;
        private string pinnedImage = null;
        private string inactiveImage = "";
        private Image image = null;
        private bool isPinned = false;
        private Color backColor = Color.Empty;
        private FCMainFormModules collection;
        private FCPinnedModuleButton pinnedModuleButton;
        private FCModuleButton moduleButton;
        private FCFavouritesModuleButton favouritesButton;
        private FCInactiveButton inactiveButton;
        private string name = string.Empty;
        private bool isActive = true;

        public FCMainFormModule()
        {
            this.pinnedModuleButton = new FCPinnedModuleButton();
            this.moduleButton = new FCModuleButton();
            this.favouritesButton = new FCFavouritesModuleButton();
            this.inactiveButton = new FCInactiveButton();
            this.pinnedModuleButton.Module = this;
            this.moduleButton.Module = this;
            this.favouritesButton.Module = this;
            this.inactiveButton.Module = this;
        }

        public FCMainFormModule(string text) : this()
        {
            this.Text = text;
        }

        public FCMainFormModule(string text, string pinnedImage) : this(text)
        {
            this.PinnedImage = pinnedImage;
        }

        public FCMainFormModule(string text, string pinnedImage, Image image) : this(text, pinnedImage)
        {
            this.Image = image;
        }

        public FCMainFormModule(string text, string pinnedImage, Image image, bool isPinned) : this(text, pinnedImage, image)
        {
            this.IsPinned = isPinned;
        }

        public FCMainFormModule(string text, string pinnedImage, Image image, bool isPinned, Color backColor) : this(text, pinnedImage, image, isPinned)
        {
            this.BackColor = backColor;
        }


        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.text = value;
                this.pinnedModuleButton.Text = value;
                this.moduleButton.Text = value;
                this.inactiveButton.Text = value;
                this.favouritesButton.ToolTipText = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        
        public string PinnedImage
        {
            get
            {
                return this.pinnedImage;
            }

            set
            {
                this.pinnedImage = value;
                this.pinnedModuleButton.Image = value;
                this.inactiveButton.Image = value;
                this.moduleButton.Image = value;
                this.favouritesButton.Image = value;
            }
        }

        public string InactiveImage
        {
            get
            {
                return this.inactiveImage;
            }

            set
            {
                this.inactiveImage = value;
                this.inactiveButton.Image = value;                
            }
        }

        public Image Image
        {
            get
            {
                return this.image;
            }

            set
            {
                this.image = value;
                //this.moduleButton.Image = value;
            }
        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public bool IsPinned
        {
            get
            {
                return this.isPinned;
            }

            set
            {   
                this.isPinned = value;
                
                this.pinnedModuleButton.Visible = this.isActive && value;
                this.favouritesButton.Visible = this.isActive && value;
                this.moduleButton.Visible = this.isActive && !value;
            }
        }

        public Color BackColor
        {
            get
            {
                return this.backColor;
            }
            set
            {
                this.backColor = value;
                this.pinnedModuleButton.BackColor = value;
                this.inactiveButton.BackColor = value;
                this.moduleButton.BackColor = value;
                this.favouritesButton.BackColor = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
                this.inactiveButton.Visible = !isActive;
                
                this.pinnedModuleButton.Visible = isActive && this.isPinned;
                this.favouritesButton.Visible = isActive && this.isPinned;
                this.moduleButton.Visible = isActive && !this.isPinned;
            }
        }

        internal FCMainFormModules Collection
        {
            get
            {
                return this.collection;
            }

            set
            {
                this.collection = value;
            }
        }

        internal FCPinnedModuleButton PinnedModuleButton
        {
            get
            {
                return this.pinnedModuleButton;
            }
        }

        internal FCModuleButton ModuleButton
        {
            get
            {
                return this.moduleButton;
            }
        }

        internal FCInactiveButton InactiveButton
        {
            get
            {
                return this.inactiveButton;
            }
        }

        internal FCFavouritesModuleButton FavouritesButton
        {
            get
            {
                return this.favouritesButton;
            }
        }
    }

    public class FCMainFormModules : ObservableCollection<FCMainFormModule>
    {
        private FCMainForm mainForm;

        private const int MAX_PINNED_MODULES = 12;

        public FCMainFormModules(FCMainForm mainForm)
        {
            this.mainForm = mainForm;
        }

        internal FCMainForm MainForm
        {
            get
            {
                return this.mainForm;
            }
        }

        public FCMainFormModule this[string key]
        {
            get
            {
                key = key.Trim().ToUpper();
                IEnumerable<FCMainFormModule> modules = this.Where(moduleName => moduleName.Name.Trim().ToUpper() == key);
                if (modules != null && modules.Count() > 0)
                {
                    return modules.ElementAt(0);
                }

                return null;
            }
        }

        public bool MaxPinnedModules()
        {
            int nrOfPinnedModules = this.Count(module => module.IsActive && module.IsPinned);
            return nrOfPinnedModules == MAX_PINNED_MODULES;
        }


        protected override void InsertItem(int index, FCMainFormModule item)
        {
            base.InsertItem(index, item);
            item.Collection = this;
        }

        protected override void RemoveItem(int index)
        {
            FCMainFormModule item = base.Items[index];
            item.Collection = null;
            base.RemoveItem(index);
        }

        protected override void SetItem(int index, FCMainFormModule item)
        {
            FCMainFormModule oldItem = base.Items[index];
            item.Collection = null;
            base.SetItem(index, item);
            item.Collection = this;
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            base.MoveItem(oldIndex, newIndex);
        }

        protected override void ClearItems()
        {
            foreach (var item in this)
            {
                item.Collection = null;
            }
            base.ClearItems();
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnCollectionChanged(e);
        }
    }
}
