﻿namespace fecherFoundation
{
    partial class FCInactiveButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FCModuleButton));
            this.button = new Wisej.Web.Button();
            this.moduleLabel = new Wisej.Web.Label();
            this.SuspendLayout();
            // 
            // button
            // 
            this.button.AppearanceKey = "flatButton";
            this.button.BackgroundImageLayout = Wisej.Web.ImageLayout.Zoom;
            this.button.Display = Wisej.Web.Display.Icon;
            this.button.ForeColor = System.Drawing.Color.White;
            this.button.Location = new System.Drawing.Point(8, 8);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(42, 42);
            this.button.TabIndex = 0;
            this.button.TextImageRelation = Wisej.Web.TextImageRelation.ImageAboveText;
            this.button.Enabled = false;
            // 
            // moduleLabel
            // 
            this.moduleLabel.AutoSize = true;
            this.moduleLabel.Cursor = Wisej.Web.Cursors.Hand;
            this.moduleLabel.Font = new System.Drawing.Font("default", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.moduleLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            this.moduleLabel.Location = new System.Drawing.Point(66, 17);
            this.moduleLabel.Name = "moduleLabel";
            this.moduleLabel.Size = new System.Drawing.Size(4, 23);
            this.moduleLabel.TabIndex = 1;
            this.moduleLabel.Enabled = false;
            // 
            // FCModuleButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.Controls.Add(this.moduleLabel);
            this.Controls.Add(this.button);
            this.Name = "FCInactiveButton";
            this.Size = new System.Drawing.Size(400, 58);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Wisej.Web.Button button;
        private Wisej.Web.Label moduleLabel;       
    }
}
