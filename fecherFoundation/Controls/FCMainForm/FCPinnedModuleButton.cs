﻿using System;
using System.Drawing;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCPinnedModuleButton : Wisej.Web.UserControl
    {
        private FCMainFormModule module;
        private Color backColor = Color.Empty;
        private Wisej.Web.Timer clickTimer = new Wisej.Web.Timer();
        public FCPinnedModuleButton()
        {
            InitializeComponent();
            this.Click += module_Click;
            clickTimer.Tick += ClickTimer_Tick;
        }
        private void ClickTimer_Tick(object sender, EventArgs e)
        {
            clickTimer.Stop();
            this.Enabled = true;
        }
        public override string Text
        {
            get
            {
                return this.moduleLabel.Text;
            }

            set
            {
                this.moduleLabel.Text = value;
            }
        }

        internal string Image
        {
            get
            {
                return this.button1.BackgroundImageSource;
            }
            set
            {
                this.button1.BackgroundImageSource = value;
            }
        }

        internal FCMainFormModule Module
        {
            get
            {
                return this.module;
            }

            set
            {
                this.module = value;
            }
        }

        public override Color BackColor
        {
            get
            {
                return backColor;
            }

            set
            {
                backColor = value;
                this.button1.BackColor = value;
            }
        }
        
        private void module_Click(object sender, EventArgs e)
        {
            if (this.module != null && this.module.Collection != null && this.module.Collection.MainForm != null && this.Enabled)
            {
                this.Enabled = false;
                clickTimer.Interval = 1000;
                clickTimer.Start();
                this.module.Collection.MainForm.OnModuleClick(this.module);
            }
        }

        private void unpinButton_Click(object sender, EventArgs e)
        {
            if (this.module != null)
            {
                this.module.IsPinned = false;

                if (this.module.Collection != null && this.module.Collection.MainForm != null)
                {
                    this.module.Collection.MainForm.PinModuleClick();
                }                
            }
        }
    }
}
