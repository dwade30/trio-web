﻿namespace fecherFoundation
{
    public interface IMenuLogger
    {
        void LogMenuItemChoice(string itemName, string origin, string caption, string parentMenuName);
    }
}