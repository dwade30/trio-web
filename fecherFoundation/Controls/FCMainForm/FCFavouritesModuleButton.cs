﻿using System;
using System.Drawing;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCFavouritesModuleButton : Wisej.Web.UserControl
    {
        private bool active;
        private Color backColor;

        public FCFavouritesModuleButton()
        {
            InitializeComponent();
            this.Active = false;
            this.backColor = this.button.BackColor;
        }

        internal string Image
        {
            get
            {
                return this.button.BackgroundImageSource;
            }
            set
            {
                this.button.BackgroundImageSource = value;
            }
        }

        public override Color BackColor
        {
            get
            {
                return this.backColor;
            }

            set
            {
                this.backColor = value;
                this.button.BackColor = value;
            }
        }

        internal FCMainFormModule Module { get; set; }

        internal bool Active
        {
            get
            {
                return this.active;
            }
            set
            {
                this.active = value;
                this.button.AppearanceKey = value ? "flatButton" : "disabledFlatButton";
                this.line1.Parent = value ? this : null;
            }
        }

        internal string ToolTipText
        {
            set
            {
                this.button.ToolTipText = value;
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (this.Module != null && this.Module.Collection != null && this.Module.Collection.MainForm != null)
            {
                this.Module.Collection.MainForm.OnModuleClick(this.Module, false);
            }
        }
    }
}
