﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCNavigationMenu : ObservableCollection<FCMenuItem>
    {
        private dynamic owner;

        public delegate void MenuItemExecutedHandler(object sender, FCMenuItemExecutedArgs e);

        public event MenuItemExecutedHandler MenuItemExecuted;

        public IMenuLogger menuLogger = null;

        public IMenuLogger MenuLogger
        {
            set
            {
                menuLogger = value;
            }
        }

        public FCNavigationMenu()
        {
        }

        public FCNavigationMenu(dynamic owner)
        {
            this.owner = owner;
        }

        protected virtual void RaiseMenuItemExecuted(string itemName, string origin,string caption, string parentMenu)
        {
            MenuItemExecuted?.Invoke(this, new FCMenuItemExecutedArgs(itemName,origin,caption,parentMenu));
        }

        //public string Name { get; set; }

        public FCMenuItem CurrentItem { get; set; }

        public dynamic Owner
        {
            get
            {
                return this.owner;
            }
            set
            {
                this.owner = value;
            }
        }

        public string OriginName { get; set; } = "";
        public FCMenuItem Add(string text, string action, string menu = "", bool enabled = true, int level = 1, string imageKey = "")
        {
            FCMenuItem item = new FCMenuItem(text, action, menu, enabled, level, imageKey);
            base.Add(item);
            return item;
        }

        public void Execute(FCMenuItem item)
        {
            string action = item.Action;
            if (item.Action != "")
            {
               // menuLogger?.LogMenuItemChoice(item.Action, this.OriginName, item.Text, item.Menu);
                RaiseMenuItemExecuted(item.Action, this.OriginName, item.Text, item.Menu);
            }

           
            if (action.Contains(":"))
            {
                int parameter = Convert.ToInt32(action.Substring(action.IndexOf(":") + 1));
                action = action.Substring(0, action.IndexOf(":"));
                FCUtils.CallByName(owner, action, CallType.Method, parameter);
            }
            else
            {
                if (!string.IsNullOrEmpty(action))
                {
                    FCUtils.CallByName(owner, action, CallType.Method);
                }
            }
        }
    }

    public class FCMenuItem
    {
        private FCNavigationMenu subItems;
        internal bool actionExecuted = false;
        public string Text { get; set; }
        public string Action { get; set; }
        public bool Enabled { get; set; }

        public string Menu { get; set; }

        public int Level { get; set; }

        public string ImageKey { get; set; }

        public bool AllowExpand { get; set; }

        public FCRigthTreeNode Node { get; set; }

        public FCNavigationMenu SubItems
        {
            get
            {
                if (subItems == null)
                {
                    subItems = new FCNavigationMenu();
                }
                return subItems;
            }
        }

        public FCMenuItem()
        {
        }

        public FCMenuItem(string text, string action, string menu, bool enabled = true, int level = 1, string imageKey = "", bool allowExpand = true)
        {
            this.Text = text;
            this.Action = action;
            this.Menu = menu;
            this.Enabled = enabled;
            this.Level = level;
            this.ImageKey = imageKey;
            this.AllowExpand = allowExpand;
        }

        public FCRigthTreeNode CreateTreeNode()
        {
            FCRigthTreeNode node = new FCRigthTreeNode();
            node.Text = this.Text;
            node.UserData.Info = this;
            //PPJ:FINAL:MHO:#i108 - set enabled property
            node.Enabled = this.Enabled;
            if (String.IsNullOrEmpty(this.ImageKey))
            {
                node.HideNodeImage = true;
            }
            else
            {
                node.HideNodeImage = false;
                node.ImageKey = this.ImageKey;
                node.SelectedImageKey = this.ImageKey + "-active";
            }
            this.Node = node;
            return node;
        }

    }

}
