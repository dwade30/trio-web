﻿namespace fecherFoundation
{
    partial class FCPinnedModuleButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            Wisej.Web.JavaScript.ClientEvent clientEvent2 = new Wisej.Web.JavaScript.ClientEvent();
            Wisej.Web.JavaScript.ClientEvent clientEvent3 = new Wisej.Web.JavaScript.ClientEvent();
            this.moduleLabel = new Wisej.Web.Label();
            this.button1 = new Wisej.Web.Button();
            this.unpinButton = new Wisej.Web.PictureBox();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.unpinButton)).BeginInit();
            this.SuspendLayout();
            // 
            // moduleLabel
            // 
            this.moduleLabel.Cursor = Wisej.Web.Cursors.Hand;
            this.moduleLabel.Font = new System.Drawing.Font("semibold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.moduleLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            this.moduleLabel.Location = new System.Drawing.Point(0, 157);
            this.moduleLabel.Name = "moduleLabel";
            this.moduleLabel.Size = new System.Drawing.Size(160, 47);
            this.moduleLabel.TabIndex = 1;
            this.moduleLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.moduleLabel.Click += new System.EventHandler(this.module_Click);
            // 
            // button1
            // 
            this.button1.AppearanceKey = "flatButton";
            this.button1.BackgroundImageLayout = Wisej.Web.ImageLayout.OriginalSize;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(20, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 120);
            this.button1.TabIndex = 2;
            this.button1.Click += new System.EventHandler(this.module_Click);
            // 
            // unpinButton
            // 
            this.unpinButton.BackgroundImageSource = "icon-unpin?color=#DC5034";
            this.unpinButton.BackgroundImageLayout = Wisej.Web.ImageLayout.Stretch;
            this.unpinButton.Cursor = Wisej.Web.Cursors.Hand;
            this.unpinButton.Location = new System.Drawing.Point(127, 5);
            this.unpinButton.Name = "unpinButton";
            this.unpinButton.Size = new System.Drawing.Size(0, 26);
            this.unpinButton.TabIndex = 3;
            this.unpinButton.Click += new System.EventHandler(this.unpinButton_Click);
            // 
            // FCPinnedModuleButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.Controls.Add(this.unpinButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.moduleLabel);
            clientEvent1.Event = "pointerover";
            clientEvent1.JavaScript = "//this.unpinButton.__contentElement.__element.style.visibility = \"visible\";\r\nth" +
    "is.unpinButton.__contentElement.__element.style.width = \"26px\";";
            clientEvent2.Event = "pointerout";
            clientEvent2.JavaScript = "//this.unpinButton.__contentElement.__element.style.visibility = \"hidden\";\r\nthi" +
    "s.unpinButton.__contentElement.__element.style.width = \"0px\";";
            clientEvent3.Event = "appear";
            clientEvent3.JavaScript = "//this.unpinButton.__contentElement.__element.style.visibility = \"hidden\";";
            this.javaScript1.GetJavaScriptEvents(this).Add(clientEvent1);
            this.javaScript1.GetJavaScriptEvents(this).Add(clientEvent2);
            this.javaScript1.GetJavaScriptEvents(this).Add(clientEvent3);
            this.Name = "FCPinnedModuleButton";
            this.Size = new System.Drawing.Size(160, 204);
            ((System.ComponentModel.ISupportInitialize)(this.unpinButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Wisej.Web.Label moduleLabel;
        private Wisej.Web.Button button1;
        private Wisej.Web.PictureBox unpinButton;
        private Wisej.Web.JavaScript javaScript1;
    }
}
