﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using System.Collections.Generic;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// Threed.SSFrame
    /// VB.Frame
    /// </summary>
    public partial class FCFrame : GroupBox, ISupportInitialize
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private Color borderColor;
        private int borderStyle = 1;
        private bool formatCaption = true;
        #endregion

        #region Constructors

        public FCFrame()
        {
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            this.borderColor = Color.Black;
            this.Text = "";

            //CHE: set default value
            this.ShadowStyle = ShadowStyleConstants._Inset;
            this.Font3D = Font3DConstants._None;
            this.Alignment = AlignFrameTextConstants._LeftJustify;
            this.BorderStyle = 1;
            //this.ClipControls = true;
            //this.Paint += FCFrame_Paint; 
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        /// <summary>
        /// Threed.SSFrame.ShadowStyle
        /// </summary>
        public enum ShadowStyleConstants
        {
            _Inset = 0,
            _Raised = 1
        }

        /// <summary>
        /// Threed.SSFrame.Alignment
        /// </summary>
        public enum AlignFrameTextConstants
        {
            _LeftJustify = 0,
            _RightJustify = 1,
            _Center = 2
        }

        #endregion

        #region Properties

        //[DefaultValue(true)]
        //public bool ClipControls
        //{
        //    // TODO:CHE
        //    get;
        //    set;
        //}

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Caption
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        [DefaultValue(true)]
        public bool FormatCaption
        {
            get
            {
                return this.formatCaption;
            }
            set
            {
                this.formatCaption = value;
            }
        }

        public new string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                string text = value;
                if (this.formatCaption)
                {
                    text = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower());
                }

                if (base.Text != text)
                {
                    base.Text = text;
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentX { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentY { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(ShadowStyleConstants._Inset)]
        public ShadowStyleConstants ShadowStyle
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(Font3DConstants._None)]
        public Font3DConstants Font3D
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(AlignFrameTextConstants._LeftJustify)]
        public AlignFrameTextConstants Alignment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/sets the style of the border
        /// </summary>
        [DefaultValue(1)]
        public int BorderStyle
        {
            get
            {
                return borderStyle;
            }
            set
            {
                borderStyle = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Gets/set the color of the border
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Color BorderColor
        {
            get { return this.borderColor; }
            set { this.borderColor = value; }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        public void BeginInit()
        {

        }

        public void EndInit()
        {

        }

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods

        // FC:FINAL:SGA Harris #i2154 - in original, when Visible is true, BringToFront() is called
        protected override void SetVisibleCore(bool value)
        {
            base.SetVisibleCore(value);

            if (this.Visible)
            {
                this.BringToFront();
            }
        }
        #endregion

        #region Private Methods

        private void FCFrame_Paint(object sender, PaintEventArgs e)
        {
            if (this.BorderStyle != 0)
            {
                return;
            }

            //remove border
            GroupBox box = (GroupBox)sender;
            e.Graphics.Clear(this.BackColor);
        }

        #endregion
    }
}
