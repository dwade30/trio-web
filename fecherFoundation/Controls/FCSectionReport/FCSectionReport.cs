﻿using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.SectionReportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fecherFoundation
{
    public  class FCSectionReport : SectionReport
    {
        /// <summary>
        /// Raised every time a new record is processed.
        /// </summary>
        public event FetchEventHandler FetchData;
        public event EventHandler ReportEndedAndCanceled;
        protected bool stopFetch = false;
        protected bool reportCanceled = false;
        protected bool reportDisposed = false;
        protected bool disposingReport = false;
        protected bool fetchDataExecuted = false;
        protected PageSettings currentPageSettings;
        protected int paperSizeIndex = -1;

		//FC:FINAL:DSE:#1392 Report is visible if a viewer is displaying it
		/// <summary>
		/// Is there a viewer displaying the report?
		/// </summary>
		public bool Visible { set; get; }

        /// <summary>
        /// Set this flag to true if the report is created as a subreport
        /// </summary>
        public bool IsSubReport { get; set; }

        /// <summary>
        /// Flag for terminating the report processing
        /// </summary>
        public bool FetchDataStopped
        {
            get
            {
                if (ParentReport != null && ParentReport is FCSectionReport parent)
                {
                    return parent.FetchDataStopped;
                }
                return stopFetch;
            }
        }

        public bool Disposing
        {
            get
            {
                return this.disposingReport;
            }
        }

        public FCSectionReport()
        {
            this.OnCancel += new Action(OnReportCanceled);
            this.PageSettings.Changed += new EventHandler(PageSettings_Changed);
            base.FetchData += new FetchEventHandler(FCSectionReport_FetchData);
			//FC:FINAL:DSE:#1392 Report is visible if viewer is displaying it
			this.Visible = false;
            this.IsSubReport = false;
            this.currentPageSettings = new PageSettings();
            this.currentPageSettings.PaperWidth = this.PageSettings.PaperWidth;
            this.currentPageSettings.PaperHeight = this.PageSettings.PaperHeight;
            //FC:FINAL:SBE - CacheToDisk throws exception when ApplicationPoolIdentity is used on IIS application pool
           // this.Document.CacheToDisk = true;
           this.Document.CacheToDisk = false;
        }

        private void FCSectionReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            fetchDataExecuted = true;
            this.OnFetchData(eArgs);
            if (FetchDataStopped) { this.Stop(); eArgs.EOF = true; reportCanceled = true; return; }
        }

        protected virtual void OnFetchData(FetchEventArgs eArgs)
        {
            if (this.FetchData != null && !reportCanceled)
            {
                this.FetchData(this, eArgs);
            }
        }

        //FC:FINAL:MSH - i.issue #1753: PaperWidth and PaperHeight values should be swapped after changing Orientation (as in original app)
        private void PageSettings_Changed(object sender, EventArgs e)
		{
            //P2218:AM:#4163 - performance improvement: for subreports changing the page settings can be time consuming and it is not needed
            if (!this.IsSubReport)
            {
                if (PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape)
                {
                    if (PageSettings.PaperWidth < PageSettings.PaperHeight)
                    {
                        var temp = PageSettings.PaperWidth;
                        PageSettings.PaperWidth = PageSettings.PaperHeight;
                        PageSettings.PaperHeight = temp;
                    }
                }
                else if (PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait)
                {
                    if (PageSettings.PaperWidth > PageSettings.PaperHeight)
                    {
                        var temp = PageSettings.PaperWidth;
                        PageSettings.PaperWidth = PageSettings.PaperHeight;
                        PageSettings.PaperHeight = temp;
                    }
                }

                //AM:HARRIS:#3310 - set the PaperSize too when the PaperWidth/PaperHeight is changed to have the paper correctly sized
                if (currentPageSettings.PaperWidth != this.PageSettings.PaperWidth || currentPageSettings.PaperHeight != this.PageSettings.PaperHeight)
                {
                    int width = FCConvert.ToInt32((PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape ? PageSettings.PaperHeight : PageSettings.PaperWidth) * 100);
                    int height = FCConvert.ToInt32((PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape ? PageSettings.PaperWidth : PageSettings.PaperHeight) * 100);
                    if (this.Document.Printer.PaperSize.PaperName != "FCCustom")
                    {
                        this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("FCCustom", width, height);
                    }
                    else
                    {
                        this.Document.Printer.PaperSize.Width = width;
                        this.Document.Printer.PaperSize.Height = height;
                    }
                    this.currentPageSettings.PaperWidth = this.PageSettings.PaperWidth;
                    this.currentPageSettings.PaperHeight = this.PageSettings.PaperHeight;

                    PrintWidth = PageSettings.PaperWidth - PageSettings.Margins.Left - PageSettings.Margins.Right;
                }
            }
		}

		public virtual void RunReport()
        {
            //FC:FINAL:AM:#2704 - check if the report was already disposed
            if (!this.reportDisposed)
            {
                reportCanceled = false;
                //FC:FINAL:SBE - CacheToDisk throws exception when ApplicationPoolIdentity is used on IIS application pool
                //this.Document.CacheToDisk = true;
                stopFetch = false;
                base.Run();
                if (reportCanceled && !this.reportDisposed && this.ReportEndedAndCanceled != null)
                {
                    this.ReportEndedAndCanceled(this, EventArgs.Empty);
                }
            }
        }

        public virtual void StopFetchingData()
        {
            stopFetch = true;
            this.reportCanceled = true;
            if (!fetchDataExecuted)
            {
                this.Cancel();
            }
            while (this.State == ReportState.InProgress)
            {
                //wait for report to be completed
                Task.Delay(200);
                //fecherFoundation.App.DoEvents();
            }
        }

        protected override void Dispose(bool disposing)
        {
            disposingReport = true;
            this.OnCancel -= new Action(this.OnReportCanceled);
            base.FetchData -= new FetchEventHandler(FCSectionReport_FetchData);
            if (this.PageSettings != null)
            {
                this.PageSettings.Changed -= new EventHandler(PageSettings_Changed);
            }
            base.Dispose(disposing);
            this.reportDisposed = true;
            disposingReport = false;
        }

        private void OnReportCanceled()
        {
            this.reportCanceled = true;
        }
    }

    /// <summary>
    /// SectionReport extensions class
    /// </summary>
    public static class SectionReportExtensions
    {
        /// <summary>
        /// Extension method to add or get a Label control from a report section
        /// </summary>
        /// <param name="section">Report section</param>
        /// <param name="name">control name</param>
        /// <returns></returns>
        public static T AddControlWithName<T>(this Section section, string name) where T : ARControl
        {
            ARControl existingControl = section.Controls.OfType<ARControl>().Where(control => control.Name == name).FirstOrDefault();
            if (existingControl != null && existingControl is T)
            {
                return (T)Convert.ChangeType(existingControl, typeof(T));
            }
            else
            {
                T newControl = (T)Activator.CreateInstance(typeof(T));
                newControl.Name = name;
                section.Controls.Add(newControl);
                return newControl;
            }
        }
    }
}
