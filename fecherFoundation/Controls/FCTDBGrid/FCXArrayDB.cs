﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace fecherFoundation
{
    public class FCXArrayDB
    {
        public object[,] Value = (object[,])Array.CreateInstance(typeof(object), new int[] { 0, 0 }, new int[] { 0, 0 });
        public void ReDim(int intlBound1, int intuBound1, int intlBound2, int intuBound2)
        {
            //Sets the dimensions of an array object while preserving its data

            object[,] oldValue = Value;

            Value = (object[,])Array.CreateInstance(typeof(object), new int[] { intuBound1 + 1, intuBound2 + 1 }, new int[] { intlBound1, intlBound2 });

            //preserve data
            if (oldValue != null)
            {
                int iMax = Math.Min(oldValue.GetUpperBound(0) - oldValue.GetLowerBound(0) + 1, intuBound1 - intlBound1 + 1);
                int jMax = Math.Min(oldValue.GetUpperBound(1) - oldValue.GetLowerBound(1) + 1, intuBound2 - intlBound2 + 1);

                //for (int i = 0; i < oldValue.GetUpperBound(0) - oldValue.GetLowerBound(0) + 1; i++)
                for (int i = 0; i < iMax; i++)
                {
                    //for (int j = 0; j < oldValue.GetUpperBound(1) - oldValue.GetLowerBound(1) + 1; j++)
                    for (int j = 0; j < jMax ; j++)
                    {
                        Value[i, j] = oldValue[i, j];
                    }
                }
            }
        }

        public void Clear()
        {
            Value = null;
        }

        public int Count(int nDim)
        {
            return this.Value.GetUpperBound(nDim) - this.Value.GetLowerBound(nDim) + 1;
        }
    }
}