﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    [Serializable]
    public class FCFont : IDisposable
    {
        #region Private Members
        private Font font;
        private const float defaultFontHeight = 10.0f;
        #endregion

        #region Properties
        public bool Bold
        {
            get
            {
                return this.font.Bold;
            }
            set
            {
                if (value)
                {
                    this.font = new Font(this.font, this.font.Style | FontStyle.Bold);
                }
                else
                {
                    this.font = new Font(this.font, this.font.Style | ~FontStyle.Bold);
                }                
            }
        }

        public float Size
        {
            get
            {
                return this.font.Size;
            }
            set
            {
                this.font = new Font(this.font.FontFamily, value, this.font.Style);
            }
        }

        public bool Italic
        {
            get
            {
                return this.font.Italic;
            }
            set
            {
               if (value)
                {
                    this.font = new Font(this.font, this.font.Style | FontStyle.Italic);
                }
                else
                {
                    this.font = new Font(this.font, this.font.Style | ~FontStyle.Italic);
                }
            }
        }

        public FontFamily FontFamily
        {
            get
            {
                return this.font.FontFamily;
            }
            set
            {
                this.font = new Font(value, this.font.Size, this.font.Style);
            }
        }
        #endregion

        #region Constructors
        public FCFont()
        {
            this.font = new Font(FontFamily.GenericSansSerif, defaultFontHeight, FontStyle.Regular);
        }

		public FCFont(string familyName, float emSize)
		{
			this.font = new Font(familyName, emSize);
		}
        #endregion

        #region Public Methods
        public void Dispose()
        {
            if (this.font != null)
            {
                this.font.Dispose();
                this.font = null;
            }
        }
        #endregion
    }
}
