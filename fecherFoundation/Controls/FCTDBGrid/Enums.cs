﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
	public enum Error
	{
		dbgBINDERROR = 4097,
		dbgINVPROPVAL = 4098,
		dbgCOLINDEX = 6145,
		dbgNOTINIT = 6146,
		dbgCNOTFOUND = 6147,
		dbgINVROWNUM = 6148,
		dbgINVBOOKMARK = 6149,
		dbgBADSELRIDX = 6150,
		dbgSCROLLRANGE = 6151,
		dbgINVSBSTYLE = 6152,
		dbgUPDERROR = 6153,
		dbgADDERROR = 6154,
		dbgDELERROR = 6155,
		dbgCOLDATA = 6156,
		dbgINCOMPAT = 6157,
		dbgFIELDERR = 6158,
		dbgDELMULTROWS = 6159,
		dbgDATAACCESS = 6160,
		dbgBADEVENT = 6161,
		dbgNOPROPNOW = 6162,
		dbgNOCURREC = 6163,
		dbgCAPTOOLONG = 6164,
		dbgSPLITINDEX = 6244,
		dbgVLINDEX = 6245,
		dbgVITEMERR = 6246,
		dbgSTYLEINDEX = 6247,
		dbgDUPSTYLE = 6248,
		dbgSTYLEERR = 6249,
		dbgUPDSTYLE = 6250,
		dbgREMSTYLE = 6251,
		dbgADDCELLCOND = 6252,
		dbgSTYLENAME = 6253,
		dbgAPPLYSTYLE = 6254,
		dbgBMPTOOLARGE = 6255
	}

	public enum ExposeCellMode
	{
		ScrollOnSelect,
		ScrollOnEdit,
		NeverScroll
	}

	public enum DataMode
	{
		Bound,
		Unbound,
		UnboundExtended,
		Application,
		Storage
	}

	public enum MultiSelect
	{
		None,
		Simple,
		Extended
	}

	public enum OLEDragMode
	{
		Manual,
		Automatic
	}

	public enum OLEDropMode
	{
		None,
		Manual,
		Automatic
	}

	public enum ForegroundPicturePosition
	{
		Left,
		Right,
		LeftOfText,
		RightOfText,
		TopOfText,
		BottomOfText,
		PictureOnly,
		TextOnly
	}

	public enum BackgroundPictureDrawMode
	{
		Center,
		Tile,
		Stretch
	}

	public enum DataViewType
	{
		dbgNormalView = 0,
		dbgHierarchicalView = 1,
		dbgGroupView = 2,
		dbgFormView = 3,
		dbgInvertedView = 4
	}

	public enum ConvertEmptyCell
	{
		NoConversion,
		AsNull
	}

	public enum CellTip
	{
		dbgOnRecordSelector = 1,
		dbgOnEmptyColumn = 2,
		dbgOnColumnHeader = 3,
		dbgOnSplitHeader = 4,
		dbgOnEmptyRow = 5,
		dbgOnCaption = 6,
		dbgOnAddNew = 7,
		dbgOnColumnFooter
	}

	public enum CellTipPresentation
	{
		dbgNoCellTips = 0, // 0 - None (default)
		dbgAnchored,       // 1 - Anchored   
		dbgFloating        // 2 - Floating
	}

	public enum SizeMode
	{
		Scalable,
		Exact,
		NumberOfColumns
	}

	public enum DividerStyle
	{
		NoDividers,
		BlackLine,
		DarkGrayLine,
		Raised,
		Inset,
		ForeColor,
		LightGrayLine,
		CustomColor,
		DoubleLine
	}

	public enum MultipleLines
	{
		Disabled,
		Enabled
	}

	public enum FetchCellType
	{
		dbgFetchCellStyleNone = 0, //None (default)
		dbgFetchCellStyleColumn = 1, //Column
		dbgFetchCellStyleAddNewColumn = 2 //Column and AddNew
	}

	public enum AddNewMode
	{
		dbgNoAddNew = 0, //No AddNew pending
		dbgAddNewCurrent = 1, //Current cell in AddNew row
		dbgAddNewPending = 2 //AddNew pending
	}

	public enum MarqueeStyle
	{
		DottedCellBorder,
		SolidCellBorder,
		HighlightCell,
		HighlightRow,
		HighlightRowRaiseCell,
		NoMarquee,
		FloatingEditor,
		DottedRowBorder
	}

	public enum TabAction
	{
		ControlNavigation,
		ColumnNavigation,
		GridNavigation
	}

	public enum RowDividerStyle
	{
		NoDividers,
		BlackLine,
		DarkGrayLine,
		Raised,
		Inset,
		ForeColor,
		LightGrayLine
	}

	public enum BOFAction
	{
		MoveFirst,
		BOF
	}

	public enum EOFAction
	{
		MoveLast,
		EOF,
		AddNew
	}

	public enum DefaultType
	{
		ODBC = 1,
		Jet = 2
	}

	public enum DefaultCursorType
	{
		DefaultCursor,
		ODBCCursor,
		ServerSideCursor
	}

	public enum RecordsetType
	{
		Table,
		Dynaset,
		Snapshot
	}

	public enum Appearance
	{
		dbgFlat,
		dbg3D
	}

	public enum AnimateWindow
	{
		NoAnimate,
		Roll,
		Slide,
		Blend
	}

	public enum AnimateWindowClose
	{
		NoAnimateClose,
		OppositeDirection,
		SameDirection
	}

	public enum AnimateWindowDirection
	{
		Default,
		TopToBottom,
		BottomToTop,
		LeftToRight,
		RightToLeft,
		TopLeftToBottomRight,
		TopRightToBottomLeft,
		BottomLeftToTopRight,
		BottomRightToTopLeft,
		Center
	}

	//public enum BorderStyle
	//{
	//	None,
	//	FixedSingle
	//}

	public enum CellTips
	{
		None,
		Anchored,
		Floating
	}

	public enum ScrollBars
    {
        None,
        Horizontal,
        Vertical,
        Both,
        Automatic
    }

	public enum Align
	{
		None,
		Top,
		Bottom,
		Left,
		Right
	}

    public enum Alignment
    {
        Left,
        Right,
        Center,
        General
    }

	public enum VerticalAlignment
	{
		Top,
		Bottom,
		VerticalCenter
	}

	public enum PrintInfo_Menu
    {
        dbgpMenuFile = 0,
        dbgpMenuPrint = 1,
        dbgpMenuExit = 2,
        dbgpMenuView = 3,
        dbgpMenuZoomIn = 4,
        dbgpMenuZoomOut = 5,
        dbgpMenuFit = 6,
        dbgpMenuPgFirst = 7,
        dbgpMenuPgPrev = 8,
        dbgpMenuPgNext = 9,
        dbgpMenuPgLast = 10,
        dbgpMenuPrintSomePages = 11,
        dbgpMenuPrintCurrPage = 12,
        dbgpDlgPagesCaption = 13,
        dbgpDlgPagesPrompt = 14,
        dbgpDlgPagesOk = 15,
        dbgpDlgPagesCancel = 16,
        dbgpTipPrint = 17,
        dbgpTipZoomIn = 18,
        dbgpTipZoomOut = 19,
        dbgpTipFit = 20,
        dbgpTipZoom = 21,
        dbgpTipPgFirst = 22,
        dbgpTipPgPrev = 23,
        dbgpTipPgNext = 24,
        dbgpTipPgLast = 25,
        dbgpTipPageOf = 26,
        dbgpTipStop = 27,
        dbgpMenuOpen = 28,
        dbgpMenuSaveAs = 29,
        dbgpMenuPageSetup = 30,
        dbgpTipOpen = 31,
        dbgpTipSaveAs = 32,
        dbgpTipPageSetup = 33,
        dbgpFilemaskArx = 34,
        dbgpFilemaskAll = 35,
        dbgpErrFileOpen = 36,
        dbgpErrFileSave = 37,
        dbgpErrFileFormat = 38,
        dbgpApsCaption = 39,
        dbgpApsText = 40,
        dbgpApsMargins = 41,
        dbgpApsOrientation = 42,
        dbgpApsHorzText = 43,
        dbgpApsHorzLeft = 44,
        dbgpApsHorzCenter = 45,
        dbgpApsHorzRight = 46,
        dbgpApsVerText = 47,
        dbgpApsVerTop = 48,
        dbgpApsVertCenter = 49,
        dbgpApsVertBottom = 50,
        dbgpApsPageSetup = 51,
        dbgpMenuFileClose = 52,
        dbgpDlgExporting = 53,
        dbgpDlgExportPage = 54,
        dbgpTipFitWidth = 55,
        dbgpTipActualSize = 56,
        dbgpMenuFitWidth = 57,
        dbgpMenuActualSize = 58,
        dbgpTipRefresh = 59
    }

    public enum Presentation
    {
        dbgNormal = 0,
        dbgRadioButton = 1,
        dbgComboBox = 2,
        dbgSortedComboBox = 3,
        dbgCheckBox = 4
    }

    public enum MoveDirection
    {
        dbgMoveNone = 0,
        dbgMoveRight = 1,
        dbgMoveDown = 2,
        dbgMoveLeft = 3,
        dbgMoveUp = 4
    }
        
    public enum PointAt
    {
        dbgNotInGrid = 0, //Not in Grid
        dbgAtCaption = 1, //At Grid Caption
        dbgAtSplitHeader = 2, //At Split Header
        dbgAtSplitSizeBox = 3, //At Split Size Box
        dbgAtRowSelect = 4, //At Row Select
        dbgAtRowSize = 5, //At Row Size
        dbgAtColumnHeader = 6, //At Column Header
        dbgAtColumnFooter = 7, //At Column Footer
        dbgAtColumnSize = 8, //At Column Size
        dbgAtDataArea = 9, //At Data Area
        dbgAtGroupArea = 10, //At Group Area
        dbgAtGroupHeader = 11 //At Group Header 
    }

    public enum DataType
    {
        Date,
        Decimal,
        PositiveInteger,
        Integer,
        EqualOnly
    }
}
