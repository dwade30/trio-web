﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    [Serializable]
    public class FCValueItems : IEnumerable<FCValueItem>
    {
        private List<FCValueItem> valueItems;
        private bool translate = false;
        private Presentation presentation;
        private DataGridViewColumn ownerColumn;

		#region Properties

		public bool Translate
        {
            get { return translate; }
            set
            {
                translate = value;
                if (this.ownerColumn != null && this.ownerColumn.GetType() == typeof(DataGridViewComboBoxColumn))
                {
                    DataGridViewComboBoxColumn col = ((DataGridViewComboBoxColumn)this.ownerColumn);
                    if (translate && col.DataGridView.GetType() == typeof(FCTDBGrid))
                    {
                        FCTDBGrid gridCol = col.DataGridView as FCTDBGrid;
                        gridCol.RedrawInverted(true);
                    }
                }
            }
        }
        

        public int Count
        {
            get
            {
                return this.valueItems.Count;
            }
        }

        public FCValueItem this[int counter]
        {
            get
            {
                return this.valueItems[counter];
            }
        }

        public object this[object value]
        {
            get
            {
                foreach (FCValueItem vi in valueItems)
                {
                    if (vi.Value.ToString() == value.ToString())
                    {
                        return vi.DisplayValue;
                    }
                }
                return null;
            }
        }

        #endregion

        #region Constructors

        public FCValueItems(DataGridViewColumn col)
        {
            this.valueItems = new List<FCValueItem>();
            this.ownerColumn = col;
        }

        #endregion

        #region Methods

        public Presentation Presentation()
        {
            return presentation;
        }

        //APE: change the presentation of the column and return the new column because the column from which the method was called is not changed
        //to the new generated column
        public DataGridViewColumn Presentation(Presentation presentation)
        {
            this.presentation = presentation;

            if (this.ownerColumn != null)
            {
                if (this.ownerColumn.DataGridView == null)
                    return this.ownerColumn;

                FCTDBGrid grid = this.ownerColumn.DataGridView as FCTDBGrid;                

                if (!grid.IsInvertedOrFormView())
                {
                    //CHE: modify type of column according to its presentation
                    DataGridViewColumn col = this.ownerColumn;
                    Type colType = col.GetType();
                    if (col != null)
                    {
                        switch (presentation)
                        {
                            case fecherFoundation.Presentation.dbgNormal:
                                {
                                    if (colType != typeof(DataGridViewTextBoxColumn))
                                    {
                                        DataGridViewColumnExtension.CloneColumn(ref col, typeof(DataGridViewTextBoxColumn));
                                    }
                                    break;
                                }
                            case fecherFoundation.Presentation.dbgComboBox:
                            case fecherFoundation.Presentation.dbgSortedComboBox:
                                {
                                    //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
                                    //for this case use CustomComboBoxColumn with ListBox as DropDown
                                    DataGridViewColumnExtension.Button(ref col, true);
                                    break;
                                }
                            case fecherFoundation.Presentation.dbgCheckBox:
                                {
                                    if (colType != typeof(DataGridViewCheckBoxColumn))
                                    {
                                        DataGridViewColumnExtension.CloneColumn(ref col, typeof(DataGridViewCheckBoxColumn));
                                    }
                                    break;
                                }
                            case fecherFoundation.Presentation.dbgRadioButton:
                                {
                                    if (colType != typeof(DataGridViewRadioColumn))
                                    {
                                        DataGridViewColumnExtension.CloneColumn(ref col, typeof(DataGridViewRadioColumn));
                                    }
                                    break;
                                }
                        }
                        this.ownerColumn = col;
                    }

                }
                else
                {
                   grid.RedrawInvertedColumn(this.ownerColumn);
                }
                return this.ownerColumn;
            }
            return null;
        }

        public void Clear()
        {
            this.valueItems.Clear();
            DataGridViewComboBoxColumn col = this.ownerColumn as DataGridViewComboBoxColumn;
            if (col != null)
            {
                col.Items.Clear();
            }
        }

        public int Add(object displayValue, object value)
        {
            FCValueItem vi = new FCValueItem();
            vi.DisplayValue = displayValue;
            vi.Value = value;
            return Add(vi);
        }

        public int Add(FCValueItem vi)
        {
            // verify if valueitem already exists and modify the display value because in FCTrueDBGrid_DataSourceChanged the valueitem may be added with the display value empty.
            int itemIndex = -1;
            FCValueItem foundItem = null;

            string convertedVi =  Convert.ToString(vi.Value);

            foreach (FCValueItem item in this.valueItems)
            {
                ++itemIndex;
                if (Convert.ToString(item.Value) == convertedVi)
                {
                    foundItem = item;
                    break;
                }                
            }
            if (foundItem == null)
            {
				//In VB6 there is a new item created, if you change the DisplayValue and Value, you can add same item again
				FCValueItem item = new FCValueItem(vi.DisplayValue, vi.Value);
                this.valueItems.Add(item);
            }
            else
            {
                foundItem.DisplayValue = vi.DisplayValue;
            }

            if (this.ownerColumn.DataGridView == null)
            {
                return -1;
            }
            //CHE: automatically set translate if DisplayValue was set
            if (vi.DisplayValue != null)
            {
                this.translate = true;
            }

            DataGridViewComboBoxColumn cmbCol = this.ownerColumn as DataGridViewComboBoxColumn;
            if (cmbCol != null && this.Translate)
            {
                
                // verify if valueitem already exists and modify the display value because in FCTrueDBGrid_DataSourceChanged the valueitem may be added with the display value empty.
                if (foundItem == null)
                {
					//In VB6 there is a new item created, if you change the DisplayValue and Value, you can add same item again
					FCValueItem item = new FCValueItem(vi.DisplayValue, vi.Value);
					itemIndex = cmbCol.Items.Add(item);
                }
                else
                {
                    FCValueItem item = cmbCol.Items[itemIndex] as FCValueItem;
                    if (item != null)
                    {
                        item.DisplayValue = vi.DisplayValue;
                    }
                    //CNA: after item.DisplayMember has been set to vi.DisplayMember, vi must reference the cmb.Items ValueItem in order to correctly set below the ValueMember's type
                    vi = item;
                }

                if (vi.Value != null)
                {
                    //HACK:IPI: for VWG 7.1.1 CustomComBox value is not displayed if ValueMember is not the same type as the column type
                    if (cmbCol.ValueType == typeof(string) && vi.Value.GetType() != typeof(string))
                    {
                        vi.Value = Convert.ToString(vi.Value);
                    }
                    else if (cmbCol.ValueType == typeof(int) && vi.Value.GetType() != typeof(int))
                    {
                        vi.Value = Convert.ToInt32(vi.Value);
                    }
                }


                //CHE: set DisplayMember and ValueMember only if the case, otherwise is time consumer
                if (cmbCol.DisplayMember != "DisplayValue" || cmbCol.ValueMember != "Value")
                {
                    if (cmbCol.DataGridView.DataSource != null)
                    {
                        DataTable dt = cmbCol.DataGridView.DataSource as DataTable;
                        DataColumn dataColumn = dt.Columns[cmbCol.DataPropertyName];
                        if (dataColumn != null && dataColumn.DefaultValue != null)
                        {
                            dataColumn.DefaultValue = null;
                        }
                    }
                    cmbCol.DisplayMember = "DisplayValue";
                    cmbCol.ValueMember = "Value";                    
                }
                //IPI: when new value items are added, must synchronize also the corresponding ComboBox
                FCTDBGrid grid = cmbCol.DataGridView as FCTDBGrid;
                if (grid != null)
                {
                    grid.UpdateInvertedComboBox(cmbCol);
                }
                return itemIndex;
            }
            return -1;
        }

        public bool ContainsValue(object value)
        {
            string convertedValue = Convert.ToString(value);

            foreach (FCValueItem item in this.valueItems)
            {
                if (Convert.ToString(item.Value) == convertedValue)
                {
                    return true;
                }
            }
            return false;
        }

        public void Remove(int position)
        {
            if (this.valueItems == null || position < 0 || position >= this.valueItems.Count)
            {
                return;
            }

            this.valueItems.RemoveAt(position);
        }

        public IEnumerator<FCValueItem> GetEnumerator()
        {
            return this.valueItems.GetEnumerator();
        }

        internal void SetPresentation(Presentation presentation)
        {
            this.presentation = presentation;
        }

        #endregion

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }    
}
