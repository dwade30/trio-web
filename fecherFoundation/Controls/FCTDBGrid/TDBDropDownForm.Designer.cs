using System.Windows.Forms;

namespace fecherFoundation
{
    partial class TDBDropDownForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visual WebGui Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dropDownGrid = new TDBDropDown();
            //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
            //for this case use CustomComboBoxColumn with ListBox as DropDown
            this.listBox = new ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // dropDownGrid
            // 
            this.dropDownGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dropDownGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dropDownGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dropDownGrid.Location = new System.Drawing.Point(0, 0);
            this.dropDownGrid.Name = "dataGridView1";
            this.dropDownGrid.RowTemplate.DefaultCellStyle.FormatProvider = new System.Globalization.CultureInfo("de-DE");
            this.dropDownGrid.TabIndex = 0;
            this.dropDownGrid.Visible = true;
            this.dropDownGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dropDownGrid.SelectionChanged += new System.EventHandler(dropDownGrid_SelectionChanged);
            //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
            //for this case use CustomComboBoxColumn with ListBox as DropDown
            // 
            // listBox
            // 
            this.listBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox.Location = new System.Drawing.Point(0, 0);
            this.listBox.Name = "listBox1";
            this.listBox.TabIndex = 0;
            this.listBox.Visible = false;
            this.listBox.SelectedIndexChanged += new System.EventHandler(listBox_SelectedIndexChanged);
            // 
            // TDBDropDownForm
            // 
            this.Size = new System.Drawing.Size(419, 218);
            this.Text = "TDBDropDownForm";
            this.Controls.Add(this.dropDownGrid);
            this.Controls.Add(this.listBox);
            this.Load += new System.EventHandler(this.TDBDropDownForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dropDownGrid)).EndInit();
            this.ResumeLayout(false);
        }
        #endregion

        internal TDBDropDown dropDownGrid;
        //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
        //for this case use CustomComboBoxColumn with ListBox as DropDown
        internal ListBox listBox;
    }
}