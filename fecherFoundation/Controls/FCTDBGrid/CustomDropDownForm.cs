﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    internal partial class CustomDropDownForm : Form
    {
        #region Internal Members

        internal DataGridViewCustomComboBoxColumn CellTemplate;

        #endregion

        #region Private Members

        private FCCustomComboBox Template;
        private bool blnLoading = false;

        #endregion

        #region Public Properties

        public TDBDropDown TDBDropDown
        {
            get { return this.dropDownGrid; }
        }

        #endregion

        #region Constructors

        internal CustomDropDownForm(FCCustomComboBox template, DataGridViewCustomComboBoxColumn cellTemplate)
        {
            InitializeComponent();

            this.Template = template;
            this.CellTemplate = cellTemplate;
        }

        #endregion

        #region Private Event Handlers

        private void dropDownGrid_SelectionChanged(object sender, System.EventArgs e)
        {
            if (blnLoading)
                return;
            //Sets successful results for form.
            this.DialogResult = DialogResult.OK;
            //Close form.
            this.Close();
        }

        private void TDBDropDownForm_Load(object sender, EventArgs e)
        {
            //FCForm f = this.Template.Form as FCForm;
            //if (f != null)
            //{
            //    f.IsDropDownDisplayed = true;
            //}

            if (this.dropDownGrid.Parent == null)
            {
                this.Controls.Add(this.dropDownGrid);
            }

            DataGridViewCustomComboBoxColumn currentCol = CellTemplate.DataGridView.Columns[CellTemplate.Index] as DataGridViewCustomComboBoxColumn;
            TDBDropDown currentDropDown = currentCol.TDBDropDown;
            if (currentDropDown != null)
            {
                // Prevent SelectionChanged event to close the form while databinding and setting initial cell
                blnLoading = true;
                FCTDBGrid grid = CellTemplate.DataGridView as FCTDBGrid;

                grid.DisableEvents = true;
                grid.IsDropDownOpened = true;
                grid.Col = grid.Columns[CellTemplate.Index].DisplayIndex;

                currentDropDown.CallDropDownOpen(sender, e);
                int columnIndex = 0;
                this.dropDownGrid.Columns.Clear();

                this.dropDownGrid.DataSource = null;
                foreach (DataGridViewColumn originalCol in currentDropDown.Columns)
                {
                    this.dropDownGrid.Columns.Add(originalCol.Clone() as DataGridViewColumn);
                    this.dropDownGrid.Columns[columnIndex].SetColumnWidth(originalCol.Width);
                    this.dropDownGrid.Columns[columnIndex].Visible = originalCol.Visible;
                    this.dropDownGrid.Columns[columnIndex].HeaderText = originalCol.HeaderText;
                    this.dropDownGrid.Columns[columnIndex].DataPropertyName = originalCol.DataPropertyName;
                    this.dropDownGrid.Columns[columnIndex].ToolTipText = originalCol.ToolTipText;
                    this.dropDownGrid.Columns[columnIndex].ReadOnly = originalCol.ReadOnly;
                    this.dropDownGrid.Columns[columnIndex].NumberFormat(originalCol.NumberFormat());
                    columnIndex++;
                }

                //CHE: columns are added manually, do not copy AutoGenerateColumns from currentDropDown
                this.dropDownGrid.AutoGenerateColumns = false;
                this.dropDownGrid.DataSource = currentDropDown.DataSource;
                this.dropDownGrid.ColumnHeadersVisible = currentDropDown.ColumnHeadersVisible;
                this.dropDownGrid.RowHeadersVisible = currentDropDown.RowHeadersVisible;

                this.dropDownGrid.DataField = currentDropDown.DataField;
                this.dropDownGrid.ListField = currentDropDown.ListField;

                this.Size = new Size(currentDropDown.Width, currentDropDown.Height);

                if (this.dropDownGrid.ColumnCount > 0)
                {
                    this.dropDownGrid.Columns[this.dropDownGrid.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                //CHE: when columns are set to automatically resize, the user cannot adjust the column widths with the mouse
                this.dropDownGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                if (currentDropDown.AllowUserToResizeColumns)
                {
                    this.dropDownGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                }

                // Clear the selection
                //SBE: keep selected item
                int selectedindex = this.Template.SelectedIndex;
                this.dropDownGrid.ClearSelection();

                this.Template.DataSource = null;
                this.Template.Items.Clear();
                //this.Template.Items.AddRange(currentCol.ValueItems().ToList());

                //SBE: restore selected item
                this.Template.SelectedIndex = selectedindex;

                // Now, SelectionChanged should be effective.
                blnLoading = false;

                grid.DisableEvents = false;
                InvertedData data = this.Tag as InvertedData;
                if (data != null)
                {
                    grid.CurrentCell = grid.Rows[data.Row].Cells[data.Col.Index];
                }
            }
        }

        private void TDBDropDownForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            FCTDBGrid grid = CellTemplate.DataGridView as FCTDBGrid;
            grid.IsDropDownOpened = false;

            DataGridViewCustomComboBoxColumn currentCol = CellTemplate.DataGridView.Columns[CellTemplate.Index] as DataGridViewCustomComboBoxColumn;
            TDBDropDown currentDropDown = currentCol.TDBDropDown;
            if (currentDropDown != null)
            {
                currentDropDown.CallDropDownClose(sender, e);
            }
        }

        #endregion
    }
}
