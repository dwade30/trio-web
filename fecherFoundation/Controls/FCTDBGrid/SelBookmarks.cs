﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace fecherFoundation
{
    public class SelBookmarks
    {
        /// <summary>
        /// list items are 1-indexed
        /// </summary>
        List<int?> selBookmarks;
        private FCTDBGrid parentGrid;

        public SelBookmarks(FCTDBGrid grid)
        {
            selBookmarks = new List<int?>();
            parentGrid = grid;
        }

        public void Add(int? bookmark)
        {
            if (!selBookmarks.Contains(bookmark))
            {
                this.selBookmarks.Add(bookmark);
            }
            
            int indexOnGrid = GetIndexOnGrid(bookmark);
            if (indexOnGrid >= 0)
            {
                this.parentGrid.Rows[indexOnGrid].Selected = true;
            }
        }

        /// <summary>
        /// This function is called only from FCTDBGrid when bookmark represents the index of the grid
        /// </summary>
        /// <param name="indexOnGrid">1-indexed value</param>
        /// <param name="isGridIndex"></param>
		internal void Add(int? indexOnGrid, bool isGridIndex)
        {
            if (indexOnGrid == null || indexOnGrid.Value < 1 || indexOnGrid.Value > this.parentGrid.Rows.Count)
            {
                return;
            }

            int indexOnDataSource = -1;
            
            DataRowView currentRow = null;
            if (parentGrid.DataTable != null)
            {
                if (indexOnGrid >= 1 && indexOnGrid <= parentGrid.DataTable.DefaultView.Count)
                {
                    currentRow = parentGrid.DataTable.DefaultView[indexOnGrid.Value - 1];
                }

                if (currentRow != null)
                {
                    if (currentRow.IsNew && parentGrid.DataTable.DefaultView.Count == indexOnGrid)
                    {
                        indexOnDataSource = indexOnGrid.Value;
                    }
                    else
                    {
                        indexOnDataSource = parentGrid.DataTable.Rows.IndexOf(currentRow.Row) + 1;
                    }
                }
            }
            else
            {
                indexOnDataSource = indexOnGrid.Value;
            }

            if (indexOnDataSource <= 0)
            {
                return;
            }

            if (!selBookmarks.Contains(indexOnDataSource))
            {
                this.selBookmarks.Add(indexOnDataSource);
            }

            this.parentGrid.Rows[indexOnGrid.Value - 1].Selected = true;
            
        }
		

        public int Count
        {
            get
            {
                return this.selBookmarks.Count;
            }
        }

        public int? this[int index]
        {
            get
            {
                return selBookmarks[index];
            }
            set
            {
                selBookmarks[index] = value;
            }
        }

        public int IndexOf(int? value)
        {
            return selBookmarks.IndexOf(value);
        }

        public void RemoveAt(int index)
        {
            if (this.selBookmarks == null || index < 0 || index >= this.selBookmarks.Count)
            {
                return;
            }

            int indexOnGrid = GetIndexOnGrid(this.selBookmarks[index]);
            if (indexOnGrid >= 0)
            {
                DataGridViewRow row = this.parentGrid.Rows[indexOnGrid];
                if (row != null && row.Selected)
                {
                    row.Selected = false;
                }
            }
            if (this.selBookmarks.Count > index)
            {
                this.selBookmarks.RemoveAt(index);
            }
        }

        //SBE: use clearGridSelection flag to check if selection needs to be cleared. 
        //Use this flag with 'false' value when SelBookmarks needs to be synchronized with SelectedRows
        public void Clear(bool clearGridSelection = true)
        {
            if (clearGridSelection)
            {
                int indexOnGrid = -1;
                for (int i=0; i < this.selBookmarks.Count; i++)
                {
                    int? val = this.selBookmarks[i];
                    indexOnGrid = GetIndexOnGrid(val);
                    if (indexOnGrid >= 0)
                    {
                        DataGridViewRow row = this.parentGrid.Rows[indexOnGrid];
                        if (row != null && row.Selected)
                        {
                            row.Selected = false;
                        }
                    }
                }
            }
            this.selBookmarks.Clear();
        }

        /// <summary>
        /// Returns a zero-based index on FCTDBGrid matching the row with the index in DataTable.
        /// If no rows have been found the function returns -1
        /// </summary>
        /// <param name="index">The 1-based index of row on DataTable attached (index of SelBookmark row)</param>
        /// <returns></returns>
        private int GetIndexOnGrid(int? index)
        {
            if (index == null)
            {
                return -1;
            }            
            
            if (this.parentGrid == null)
            {
                return -1;
            }
            if (parentGrid.DataTable == null)
            {
                return -1;
            }

            DataRow dataRow = null;

            //CHE: GetIndexOnGrid expects 1-based parameter
            --index;
            if (index >= 0 && index < parentGrid.DataTable.Rows.Count)
            {
                dataRow = parentGrid.DataTable.Rows[index.Value];
            }

            if (dataRow != null)
            {
                for (int i = 0; i < parentGrid.Rows.Count; i++)
                {
                    if (parentGrid.Rows[i].DataBoundItem != null)
                    {
                        if ((parentGrid.Rows[i].DataBoundItem as DataRowView).Row.Equals(dataRow))
                        {
                            return i;
                        }
                    }
                }
            }

            return -1;            
        }
    }
}
