﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    internal class FCCustomComboBox : FCComboBox
    {
        #region Private Members

        /// <summary>
        /// The form is used as custom DropDown control.
        /// </summary>
        private CustomDropDownForm customDropDownForm = null;
        private TDBDropDown dropDownGrid;

        #endregion

        #region Internal Properties
        
        internal TDBDropDown TDBDropDown
        {
            get { return dropDownGrid; }
            set
            {
                dropDownGrid = value;
            }
        }

        #endregion

        #region Protected Properties
        /// <summary>
        /// Gets a value indicating whether this instance has a custom drop down.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has a custom drop down; otherwise, <c>false</c>.
        /// </value>
        //protected override bool IsCustomDropDown
        //{
        //    get
        //    {
        //        if (this.Column.Button() && this.Column.DropDownList())
        //        {
        //            return false;
        //        }
        //        return true;
        //    }
        //}

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewComboBox"/> class.
        /// </summary>
        internal FCCustomComboBox(DataGridViewCustomComboBoxColumn col) //: base(col)
        {
            customDropDownForm = new CustomDropDownForm(this, col);
            //customDropDownForm.Closed += new EventHandler(OnFormClosed);
        }

        internal FCCustomComboBox()
        {
            // Set a custom template for a cell of DataGridView.
            //this.CellTemplate = new DataGridViewCustomComboBoxCell(); 

            // Set default column property values
            this.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.DropDownStyle = ComboBoxStyle.DropDown;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.Name = "customComboBox";
            this.Width = 100;
        }
        #endregion

        #region Internal Events

        /// <summary>
        /// Occurs when selected item changed.
        /// </summary>
        internal event EventHandler ValueChanged;

        #endregion

        #region Protected Methods

        //protected override void FireEvent(IEvent objEvent)
        //{
        //    // Select event type
        //    switch (objEvent.Type)
        //    {
        //        case "DropDownWindow":
        //            {
        //                FCTDBGrid grid = this.Column.DataGridView as FCTDBGrid;
        //                grid.DisablePreRenderRegenerate = false;

        //                //do not show DropDown is CellBeginEdit was canceled
        //                if (grid.IsCancelEdit)
        //                {
        //                    return;
        //                }

        //                break;
        //            }				
        //    }
            
        //    base.FireEvent(objEvent);            
        //}

        /// <summary>
        /// Gets the custom form to display as drop down
        /// </summary>
        /// <returns></returns>
        //protected override System.Windows.Forms.Form GetCustomDropDown()
        //{
        //    FCTDBGrid grid = this.Column.DataGridView as FCTDBGrid;
        //    grid.DisablePreRenderRegenerate = true;
        //    grid.DisableEvents = true;
        //    if (grid.Col != Column.DisplayIndex)
        //    {
        //        grid.Col = Column.DisplayIndex;
        //    }
        //    grid.DisableEvents = false;
        //    DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(this.Column);

        //    if (objColDefinitions.Button && this.Column.DropDown() == null)
        //    {
        //        grid.RaiseButtonClick(this.Column, EventArgs.Empty);
        //        return null;
        //    }
        //    else
        //    {
        //        if (customDropDownForm.CellTemplate.DataGridView == null)
        //            return null;
        //        customDropDownForm.DialogResult = DialogResult.None;
        //        customDropDownForm.Width = Math.Max(this.Width, customDropDownForm.Width);
        //        return customDropDownForm;
        //    }
        //}

		/// <summary>
        /// open drop down on pressing alt + down arrow
		/// </summary>
		/// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    {
                        if ((Control.ModifierKeys & Keys.Alt) == Keys.Alt)
                        {
                            //Form objDropDown = this.GetCustomDropDown();
                            //if (objDropDown != null)
                            //{
                            //    objDropDown.ShowPopup(this, DialogAlignment.Below);
                            //}
                            return;
                        }
                        break;
                    }
            }

            base.OnKeyDown(e);
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Called when form is closed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //private void OnFormClosed(object sender, EventArgs e)
        //{
        //    //((FCTDBGrid)this.Column.DataGridView).DisablePreRenderRegenerate = false;
        //    if (((global::System.Windows.Forms.Form)sender).DialogResult == DialogResult.OK)
        //    {
        //        if (this.customDropDownForm.TDBDropDown != null && this.customDropDownForm.TDBDropDown.CurrentRow != null)
        //        {
        //            FCTDBGrid grid = ((FCTDBGrid)this.Column.DataGridView);
        //            grid.DisableEvents = true;
        //            //SBE: if DatatField is not set, then use the first column form TDBDropDown
        //            int colIndex = 0;
        //            if (this.customDropDownForm.TDBDropDown.Columns[this.customDropDownForm.TDBDropDown.DataField] != null)
        //            {
        //                colIndex = this.customDropDownForm.TDBDropDown.Columns[this.customDropDownForm.TDBDropDown.DataField].Index;
        //            }
        //            object comboValue = this.customDropDownForm.TDBDropDown[colIndex, this.customDropDownForm.TDBDropDown.CurrentRow.Index].Value.ToString();
        //            string valueText = "";
        //            if (comboValue != null)
        //            {
        //                valueText = comboValue.ToString();
        //            }
        //            bool containsItem = false;
        //            //BAN: get the selected item
        //            object selectedItem = null;
        //            foreach (object item in this.Column.ValueItems())
        //            {
        //                string displayValue;
        //                if (item.GetType() == typeof(FCValueItem))
        //                {
        //                    displayValue = ((FCValueItem)item).DisplayMember as string;
        //                }
        //                else
        //                {
        //                    displayValue = item as string;
        //                }
        //                if (displayValue != null && (displayValue == valueText || (comboValue != null && comboValue.GetType() == typeof(string) && comboValue.ToString() == displayValue)))
        //                {
        //                    containsItem = true;
        //                    selectedItem = item;
        //                    if (item.GetType() == typeof(FCValueItem))
        //                    {
        //                        comboValue = ((FCValueItem)item).ValueMember;
        //                    }
        //                    break;
        //                }
        //            }
        //            if (!containsItem)
        //            {
        //                FCValueItem item = new FCValueItem(comboValue, comboValue);
        //                selectedItem = item;
        //                this.SelectedItem = item;
        //                //this.Column.ValueItems().Add(item);
        //                this.ValueMember = "ValueMember";
        //                this.DisplayMember = "DisplayMember";
        //                //this.DataSource = this.Column.ValueItems().ToList();

        //            }
        //            //BAN: Enable OnRowValidated to be execute
        //            grid.DataChanged = true;
        //            //grid.SetDataCellValue(this.Column, comboValue);
        //            //this.Column.Value(comboValue);
        //            //BAN: If the item which corresponds to an item in the items list is not set as selected, select it
        //            if (this.SelectedItem == null)
        //            {
        //                this.SelectedItem = selectedItem;
        //            }
        //            else
        //            {
        //                this.Text = valueText;
        //            }
        //            grid.DisableEvents = false;
        //            FCDataGridViewCellValidatingEventArgs eventArgs = new FCDataGridViewCellValidatingEventArgs(grid.Col, grid.Row, grid[grid.Col, grid.Row].FormattedValue, grid.CurrentCellOldValue);
        //            grid.RaiseCellValidating(grid, eventArgs);                    
        //        }
        //        if (ValueChanged != null)
        //        {
        //            ValueChanged(this, EventArgs.Empty);
        //        }
        //    }
        //}

        #endregion
    }
}
