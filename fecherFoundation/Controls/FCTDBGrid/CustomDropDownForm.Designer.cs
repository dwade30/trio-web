﻿using System.Windows.Forms;

namespace fecherFoundation
{
    partial class CustomDropDownForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dropDownGrid = new TDBDropDown();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // dropDownGrid
            // 
            this.dropDownGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dropDownGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dropDownGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dropDownGrid.Location = new System.Drawing.Point(0, 0);
            this.dropDownGrid.Name = "dataGridView1";
            this.dropDownGrid.RowTemplate.DefaultCellStyle.FormatProvider = new System.Globalization.CultureInfo("de-DE");
            this.dropDownGrid.TabIndex = 0;
            this.dropDownGrid.Visible = true;
            this.dropDownGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dropDownGrid.SelectionChanged += new System.EventHandler(dropDownGrid_SelectionChanged);
            // 
            // TDBDropDownForm
            // 
            this.Controls.Add(this.dropDownGrid);
            this.Size = new System.Drawing.Size(419, 218);
            this.Text = "CBDropDownForm";
            this.Load += new System.EventHandler(this.TDBDropDownForm_Load);
            this.FormClosed += new FormClosedEventHandler(TDBDropDownForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dropDownGrid)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private TDBDropDown dropDownGrid;
    }
}