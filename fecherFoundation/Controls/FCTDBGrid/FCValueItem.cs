﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public class FCValueItem
    {
        #region Properties

        public object DisplayValue { get; set; }
        public object Value { get; set; }

        #endregion

        #region Constructors

        public FCValueItem()
        {

        }

        public FCValueItem(object displayMember, object valueMember)
        {
            this.DisplayValue = displayMember;
            this.Value = valueMember;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return this.DisplayValue.ToString();
        }

        #endregion
    }
}
