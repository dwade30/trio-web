﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace fecherFoundation
{
    public class FCDataGridViewCellValidatingEventArgs : CancelEventArgs
    {
        private int rowIndex, columnIndex;
        private object formattedValue;
        private object oldValue;

        public FCDataGridViewCellValidatingEventArgs(int ColIndex, int RowIndex, object FormattedValue, object OldValue)
        {
            this.rowIndex = RowIndex;
            this.columnIndex = ColIndex;
            this.formattedValue = FormattedValue;
            this.oldValue = OldValue;
        }

        public int ColumnIndex
        {
            get
            {
                return this.columnIndex;
            }
        }

        /// <include file="doc\DataGridViewCellValidatingEventArgs.uex" path="docs/doc[@for="DataGridViewCellValidatingEventArgs.FormattedValue"]/*">
        public object FormattedValue
        {
            get
            {
                return this.formattedValue;
            }
        }

        /// <include file="doc\DataGridViewCellValidatingEventArgs.uex" path="docs/doc[@for="DataGridViewCellValidatingEventArgs.RowIndex"]/*">
        public int RowIndex
        {
            get
            {
                return this.rowIndex;
            }
        }

        /// <summary>
        /// The data specified by the OldValue argument moves from the cell to the grid's copy buffer when the user completes editing within a cell, 
        /// as when tabbing to another column in the same row, pressing the Enter key, or clicking on another cell. 
        /// Before the data has been moved from the cell into the grid's copy buffer, the BeforeColUpdate event is triggered. 
        /// This event gives your application an opportunity to check the individual grid cells before they are committed to the grid's copy buffer. 
        /// </summary>
        public object OldValue
        {
            get
            {
                return this.oldValue;
            }

            set
            {
                this.oldValue = value;
            }
        }
    }
}
