﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Concurrent;
using fecherFoundation.DataBaseLayer.ADO;

namespace fecherFoundation
{
    [Serializable]
    internal class DataGridViewColumnDefinitions : IDisposable
    {
        #region Private Members
        private DataGridViewColumn mobjColumn = null;
        private int dataWidth = 0;
        private ConvertEmptyCell convertEmptyCell = ConvertEmptyCell.NoConversion;
        private string numberFormat = "";
        #endregion

        #region Internal Members

        internal KeyValuePair<Label, List<Control>> invertedColControls;

        internal int Split = -1;

        internal FCValueItems ValueItems;

        internal bool dropDownList;

        internal bool autoDropDown;

        internal FCFont headFont;

        internal FCFont font;

        //FetchStyle Property
        //Controls whether the FetchCellStyle event fires for a column.
        //Syntax
        //FetchStyle As FetchCellStyle
        //Values
        //Possible values for the FetchCellStyle are:
        //DesignTime
        //RunTime
        //0 – None (default)
        //dbgFetchCellStyleNone
        //1 – Column
        //dbgFetchCellStyleColumn
        //2 – Column and AddNew
        //dbgFetchCellStyleAddNewColumn
        //Remarks
        //Read/Write at run time and design time. 
        //If set to 0 - None (the default), no events will fire to retrieve cell styles.
        //If set to 1 - Column, events will be fired for rows not including the AddNewRow.
        //If set to 2 – Column and AddNew, events will fire for rows including the AddNewRow. The bookmark into the event will be null for the AddNew row.
        //Note
        //If you want to apply the same formatting to all cells within a row, then you should set the FetchRowStyle property instead of FetchStyle, 
        //and write code for the FetchRowStyle event instead of FetchCellStyle. This is much more efficient because events are fired on a per-row rather than on a per-cell basis.
        internal FetchCellType fetchStyle;

        internal bool merge = false;

        internal int DataWidth
        {
            get { return dataWidth; }
            set { dataWidth = value; }
        }

        internal bool Locked = false;

        #endregion

        #region Internal Properties

        internal string NumberFormat
        {
            get { return numberFormat; }
            set { numberFormat = value; }
        }

        internal Alignment Alignment { get; set; }

        internal int Order { get; set; }

        internal bool Button { get; set; }

        internal string EditMask { get; set; }

        internal ConvertEmptyCell ConvertEmptyCell 
        { 
            get 
            {
                return convertEmptyCell;
            } 
            set 
            {
                convertEmptyCell = value;
            }
        }

        #endregion

        #region Constructors

        internal DataGridViewColumnDefinitions(DataGridViewColumn objColumn)
        {
            mobjColumn = objColumn;
            ValueItems = new FCValueItems(objColumn);
            invertedColControls = new KeyValuePair<Label, List<Control>>();
            //CNA: Initialize order
            Order = objColumn.DisplayIndex;

            headFont = new FCFont();
            font = new FCFont();

            // set alignment based on col DefaultCellStyle
            switch (objColumn.DefaultCellStyle.Alignment)
            {
                case DataGridViewContentAlignment.BottomLeft:
                case DataGridViewContentAlignment.MiddleLeft:
                case DataGridViewContentAlignment.TopLeft:
                    this.Alignment = fecherFoundation.Alignment.Left;
                    break;

                case DataGridViewContentAlignment.BottomCenter:
                case DataGridViewContentAlignment.MiddleCenter:
                case DataGridViewContentAlignment.TopCenter:
                    this.Alignment = fecherFoundation.Alignment.Center;
                    break;

                case DataGridViewContentAlignment.BottomRight:
                case DataGridViewContentAlignment.MiddleRight:
                case DataGridViewContentAlignment.TopRight:
                    this.Alignment = fecherFoundation.Alignment.Right;
                    break;

                case DataGridViewContentAlignment.NotSet:
                    this.Alignment = fecherFoundation.Alignment.General;
                    break;

                default:
                    this.Alignment = fecherFoundation.Alignment.General;
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the definitions of the current DataGridViewColumn
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static DataGridViewColumnDefinitions GetDataTableDefinitions(DataGridViewColumn objColumn)
        {
            DataGridViewColumnDefinitions objColDefinitions = null;

            if (DataGridViewColumnExtension.mobjDefinitions.ContainsKey(objColumn))
            {
                objColDefinitions = DataGridViewColumnExtension.mobjDefinitions[objColumn];
            }
            else
            {
                objColDefinitions = new DataGridViewColumnDefinitions(objColumn);
                DataGridViewColumnExtension.mobjDefinitions.TryAdd(objColumn, objColDefinitions);
            }

            return objColDefinitions;
        }


        /// <summary>
        /// Remove a Column from the DataGridViewColumnExtension.mobjDefinitions-Collection
        /// </summary>
        /// <param name="col"></param>
        public static void RemoveColumn(DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objRowDefinitions = null;
            DataGridViewColumnExtension.mobjDefinitions.TryRemove(col, out objRowDefinitions);
        }

        /// <summary>
        /// Remove all Columns from the DataGridViewColumnExtension.mobjDefinitions-Collection
        /// </summary>
        /// <param name="dataGridView"></param>
        public static void RemoveAllColumns(DataGridView dataGridView)
        {
            if (dataGridView != null && dataGridView.IsHandleCreated)
            {
                foreach (DataGridViewColumn col in dataGridView.Columns)
                {
                    RemoveColumn(col);
                }
            }
        }

        public void Dispose()
        {
            if (this.headFont != null)
            {
                this.headFont.Dispose();
                this.headFont = null;
            }
            
            if (this.font != null)
            {
                this.font.Dispose();
                this.font = null;
            }
            
            if (this.ValueItems != null)
            {
                this.ValueItems.Clear();
                this.ValueItems = null;
            }

            if (this.invertedColControls.Value != null)
            {
                this.invertedColControls.Value.Clear();                
            }            
        }

        #endregion

    }

    public static class DataGridViewColumnExtension
    {
        /// <summary>
        /// A dictionary used to save the definition object 
        /// </summary>
        internal static ConcurrentDictionary<DataGridViewColumn, DataGridViewColumnDefinitions> mobjDefinitions = new ConcurrentDictionary<DataGridViewColumn, DataGridViewColumnDefinitions>();

        public static void NumberFormat(this DataGridViewColumn col, string format)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.NumberFormat = format;

            switch (format)
            {
                //Display at least one digit to the left and two digits to the right of the decimal separator.
                case "Fixed":
                    {
                        col.DefaultCellStyle.Format = "N";
                        break;
                    }
                //Display number as is, with no thousand separators. 
                case "General Number":
                    {
                        break;
                    }
                //Display number with thousands separator, at least one digit to the left and two digits to the right of the decimal separator. 
                case "Standard":
                    {
                        col.DefaultCellStyle.Format = "N";
                        break;
                    }
                //Display number multiplied by 100 with a percent sign (%) appended to the right; always display two digits to the right of the decimal separator. 
                case "Percent":
                    {
                        col.DefaultCellStyle.Format = "P";
                        break;
                    }
                //Display a date using your system's short date format. 
                case "Short Date":
                    {
                        col.DefaultCellStyle.Format = "d";
                        break;
                    }
                //Display a date using your system's short date and long time format. 
                case "General Date":
                    {
                        col.DefaultCellStyle.Format = "G";
                        break;
                    }
                //Display No if number is 0; otherwise, display Yes.
                case "Yes/No":
                    {
                        break;
                    }
                //Display Off if number is 0; otherwise, display On. 
                case "On/Off":
                    {
                        break;
                    }
                case "Currency":
                    {
                        col.DefaultCellStyle.Format = "C";
                        break;
                    }
                //cell format will be handled by FormatText event attached to the grid
                case "FormatText Event":
                    {
                        break;
                    }
                default:
                    {
                        col.DefaultCellStyle.Format = format;
                        break;
                    }
            }
        }

        public static string NumberFormat(this DataGridViewColumn col)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.NumberFormat;
        }

        public static void Alignment(this DataGridViewColumn col, Alignment aligment)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.Alignment = aligment;

            DataGridViewContentAlignment colAlignment = DataGridViewContentAlignment.NotSet;
            switch (aligment)
            {
                case fecherFoundation.Alignment.Center:
                    colAlignment = DataGridViewContentAlignment.MiddleCenter;
                    break;

                case fecherFoundation.Alignment.Left:
                    colAlignment = DataGridViewContentAlignment.MiddleLeft;
                    break;

                case fecherFoundation.Alignment.Right:
                    colAlignment = DataGridViewContentAlignment.MiddleRight;
                    break;

                default:
                    // leave Alignment = NotSet => the column will be correctly aligned in CellFormatting event handler
                    break;
            }

            col.DefaultCellStyle.Alignment = colAlignment;
            col.HeaderCell.Style.Alignment = colAlignment;
        }

        /// <summary>
        /// keep the inverted grid column controls in order to access them later when column properies have changed
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns> 
        public static KeyValuePair<Label, List<Control>> ColumnControls(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.invertedColControls;
        }
        
       /// <summary>
       /// keep the inverted grid column controls in order to access them later when column properies have changed
       /// </summary>
       /// <param name="col"></param>
       /// <param name="objControls"></param>
        public static void ColumnControls(this DataGridViewColumn col, KeyValuePair<Label, List<Control>> objControls)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.invertedColControls = objControls;
        }

        public static Alignment Alignment(this DataGridViewColumn col)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.Alignment;
        }

        /// <summary>
        /// Column header alignment
        /// </summary>
        /// <param name="col"></param>
        /// <param name="aligment"></param>
        public static void HeadAlignment(this DataGridViewColumn col, Alignment aligment)
        {
            switch (aligment)
            {
                case fecherFoundation.Alignment.Center:
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    break;
                case fecherFoundation.Alignment.General:
                case fecherFoundation.Alignment.Left:
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    break;
                case fecherFoundation.Alignment.Right:
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    break;
            }
        }

        public static void Locked(this DataGridViewColumn col, bool locked)
        {
            FCTDBGrid grid = col.DataGridView as FCTDBGrid;

            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            objColDefinitions.Locked = locked;

            //IPI: in case of CustomDropDownColumn, setting ReadOnly = true will no longer generate "DropDownWindow" event when user clicks the drop down arrow 
            // (instead will generate "Click" event and cell will enter edit mode) 
            if (col is DataGridViewCustomComboBoxColumn || col is DataGridViewComboBoxColumn)
            {
                col.DropDownList(locked);
                if (locked)
                {
                    return;
                }
            }

            if (grid != null)
            {
                //CHE: when autoincrement the readonly property cannot be set
                try
                {
                    if (grid.AllowUpdate)
                    {
                        col.ReadOnly = locked;
                    }
                    else
                    {
                        col.ReadOnly = true;
                    }
                }
                catch 
                { 
                }
            }
        }

        public static bool Locked(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            return objColDefinitions.Locked;
        }

        public static void AllowSizing(this DataGridViewColumn col, bool allowSizing)
        {
            col.Resizable = allowSizing ? DataGridViewTriState.True : DataGridViewTriState.False;            
        }

        public static bool AllowFocus(this DataGridViewColumn col)
        {
            return !col.ReadOnly;
        }

        public static void AllowFocus(this DataGridViewColumn col, bool allowFocus)
        {
            col.ReadOnly = !allowFocus;
            FCTDBGrid grid = col.DataGridView as FCTDBGrid;
            if (grid != null)
            {
                grid.ChangeColAllowFocus(col);
            }
        }

		//TODO: Not implemented
		public static void WrapText(this DataGridViewColumn col, bool wrapText)
		{
			
		}

        public static string DataField(this DataGridViewColumn col, string dataField)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            
            col.DataPropertyName = dataField;

            return col.DataPropertyName;
        }

        public static string DataField(this DataGridViewColumn col)
        {
            return col.DataPropertyName;
        }

        /// <summary>
        /// merge cells in column when grid.AllowMerging is set
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public static bool Merge(this DataGridViewColumn col)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            return objColDefinitions.merge;
        }

        /// <summary>
        /// merge cells in column when grid.AllowMerging is set
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public static void Merge(this DataGridViewColumn col, bool merge)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            objColDefinitions.merge = merge;
        }

        private static Dictionary<int, int> GetSplitColumns(DataGridView grid)
        {
            Dictionary<int, int> splitColCount = new Dictionary<int, int>( ((FCTDBGrid)grid).Splits.Count);
            int splitNr = -1;
            foreach (DataGridViewColumn tmp in grid.Columns)
            {
                splitNr = tmp.GetAttachedSplit();
                if (splitNr >= 0)
                {
                    if (!splitColCount.ContainsKey(splitNr))
                    {
                        splitColCount.Add(splitNr, 0);
                    }
                    ++splitColCount[splitNr];
                }
            }

            return splitColCount;
        }

        public static int Order(this DataGridViewColumn col, int order)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.Order = order;

            if (col.DisplayIndex != order)
            {
                FCTDBGrid grid = col.DataGridView as FCTDBGrid;
                if (grid != null)
                {
                    Dictionary<int, int> splitColCount = GetSplitColumns(col.DataGridView);

                    int offset = 0;
                    int currentSplitNr = col.GetAttachedSplit();
                    for (int i = 0; i < currentSplitNr; i++)
                    {
                        if (i >= splitColCount.Count)
                            break;

                        //CNA: check if split is used
                        if (splitColCount.ContainsKey(i))
                        {
                            offset += splitColCount[i];
                        }
                    }

                    int targetPos = offset + order;
                    if (col.DisplayIndex != targetPos)
                    {
                        if (targetPos >= grid.ColumnCount)
                        {
                            targetPos = grid.ColumnCount - 1;
                        }

                        if (targetPos != -1)
                        {
                            col.DisplayIndex = targetPos;
                        }
                        return targetPos;
                    }
                }
            }

            return order;
        }        

        public static int Order(this DataGridViewColumn col)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.Order;
        }

        public static void AttachToSplit(this DataGridViewColumn col, int splitNr)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.Split = splitNr;

            //unfreeze column if previously frozen
            if (splitNr != 0)
            {
                col.Frozen = false;
            }

            //do not set as frozen if columns width exceeds grid width, horizontal scrollbar need to be displayed
            int frozenColumnsWidth = 0;
            FCTDBGrid grid = col.DataGridView as FCTDBGrid;
            if (grid != null)
            {
                foreach (DataGridViewColumn c in grid.Columns)
                {
                    if (c.DisplayIndex <= col.DisplayIndex)
                    {
                        frozenColumnsWidth += c.Width;
                    }
                }

                if (splitNr == 0 && frozenColumnsWidth <= grid.Width && grid.Splits.Count > 1)
                {
                    col.Frozen = true;
                }
            }
        }

        public static int GetAttachedSplit(this DataGridViewColumn col)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.Split;
        }

        public static void DataWidth(this DataGridViewColumn col, int dataWidth)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.DataWidth = dataWidth;
            DataTable dataTable = null;
            FCTDBGrid grid = col.DataGridView as FCTDBGrid;
            if (grid != null)
            {
                dataTable = grid.DataSource as DataTable;
            }
            if (dataTable != null)
            {
                DataColumn dataCol = dataTable.Columns[col.DataPropertyName];
                if (dataCol != null && dataCol.DataType == typeof(System.String))
                {
                    DataRow dr = null;
                    int cellContentWidth = 0;
                    int internalDataWidth = dataWidth;
                    for (int count = 0; count < dataTable.Rows.Count; count++)
                    {
                        dr = dataTable.Rows[count];
                        cellContentWidth = dr[col.Index].ToString().Length;
                        if (cellContentWidth > internalDataWidth)
                        {
                            internalDataWidth = cellContentWidth;
                        }
                    }

                    if (internalDataWidth > dataWidth)
                    {
                        dataWidth = internalDataWidth;
                    }
                    if (dataWidth > dataCol.MaxLength)
                    {
                        dataCol.MaxLength = dataWidth;
                    }
                }
            }

            //CHE: for unbounded grid or column without DataPropertyName, set MaxInputLength on grid column
            if (dataTable == null || string.IsNullOrEmpty(col.DataPropertyName))
            {
                DataGridViewTextBoxColumn textBoxColumn = col as DataGridViewTextBoxColumn;
                if (textBoxColumn != null)
                {
                    textBoxColumn.MaxInputLength = dataWidth;
                }

                //CHE: reload inverted to set MaxLength for controls
                if (grid != null)
                {
                    //CHE: set max length
                    if (textBoxColumn != null)
                    {
                        grid.SetMaxLength(textBoxColumn);
                    }
                }
            }

        }

        public static int DataWidth(this DataGridViewColumn col)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.DataWidth;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static bool AutoCompletion(this DataGridViewColumn col, bool autoCompletion)
        {
            return autoCompletion;
        }

        //public static void DropDown(ref DataGridViewColumn col, object dropDown)
        //{
        //    //CHE: reset column to DataGridViewTextBoxColumn when calling DropDown with null
        //    if (dropDown == null)
        //    {
        //        CloneColumn(ref col, typeof(DataGridViewTextBoxColumn));
        //        return;
        //    }

        //    DataGridView grid = col.DataGridView;
        //    DataGridViewColumn initialCol = col;
        //    int initialPosition = initialCol.Index;
        //    TDBDropDown dropDownGrid = null;

        //    if (dropDown is string)
        //    {
        //        List<Control> formControls = col.DataGridView.Form.GetAllControls();
        //        string dropDownString = Convert.ToString(dropDown); 
        //        foreach (Control ctrl in formControls)
        //        {
        //            if (ctrl.Name == dropDownString)
        //            {
        //                dropDownGrid = ctrl as TDBDropDown;
        //                break;
        //            }
        //        }
        //    }
        //    else if (dropDown is TDBDropDown)
        //    {
        //        dropDownGrid = dropDown as TDBDropDown;
        //    }

        //    CloneColumn(ref col, typeof(DataGridViewCustomComboBoxColumn));
        //    DataGridViewCustomComboBoxColumn dropDownCol = col as DataGridViewCustomComboBoxColumn;
        //    dropDownCol.TDBDropDown = dropDownGrid;
            
        //    FCTDBGrid tdbGrid = grid as FCTDBGrid;
        //    if (tdbGrid != null)
        //    {
        //        tdbGrid.RedrawInvertedColumn(col);
        //    }
        //}

        //public static TDBDropDown DropDown(this DataGridViewColumn col)
        //{
        //    DataGridViewCustomComboBoxColumn customColumn = col as DataGridViewCustomComboBoxColumn;
        //    if (customColumn == null)
        //    {
        //        return null;
        //    }

        //    return customColumn.TDBDropDown;
        //}

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static int DefColWidth(this DataGridViewColumn col, int defColWidth)
        {
            return defColWidth;
        }

        public static void FetchStyle(this DataGridViewColumn col, FetchCellType fetchStyle)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            objColDefinitions.fetchStyle = fetchStyle;
        }

        public static void FetchStyle(this DataGridViewColumn col, bool fetchStyleFlag)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            objColDefinitions.fetchStyle = fetchStyleFlag ? FetchCellType.dbgFetchCellStyleAddNewColumn : FetchCellType.dbgFetchCellStyleNone;
        }

        public static FetchCellType FetchStyle(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            return objColDefinitions.fetchStyle;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static DividerStyle DividerStyle(this DataGridViewColumn col, DividerStyle dividerStyle)
        {
            return dividerStyle;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static System.Drawing.Color DividerColor(this DataGridViewColumn col, System.Drawing.Color color)
        {
            return color;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static System.Drawing.Color DividerColor(this DataGridViewColumn col)
        {
            return System.Drawing.Color.Gray;
        }

        public static object CellValue(this DataGridViewColumn col, int? row)
        {
            if (col.DataGridView != null)
            {
                if (row.HasValue)
                {
                    return col.DataGridView.Rows[row.Value].Cells[col.Index].Value;
                }
                else if (col.DataGridView.CurrentRow != null)
                {
                    return col.DataGridView.CurrentRow.Cells[col.Index].Value;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static string CellText(this DataGridViewColumn col, int? row)
        {
            if (col.DataGridView != null)
            {
                if (row.HasValue)
                {
                    return Convert.ToString(col.DataGridView.Rows[row.Value].Cells[col.Index].FormattedValue);
                }
                else if (col.DataGridView.CurrentRow != null)
                {
                    return Convert.ToString(col.DataGridView.CurrentRow.Cells[col.Index].FormattedValue);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// get displayed column text for current row
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public static string Text(this DataGridViewColumn col)
        {
            DataGridViewRow dr = col.DataGridView.CurrentRow;
            if (dr != null)
            {
                Type colType = col.GetType();
                //CNA: For ComboxBox and CustomComboBox columns Text should be a Displayvalue from ValueItems
                if (col.ValueItems() != null && 
                    (colType == typeof(DataGridViewComboBoxColumn) || colType == typeof(DataGridViewCustomComboBoxColumn)) && 
                    col.ValueItems().Count > 0)
                {
                    string cellValueString = dr.Cells[col.Index].Value.ToString();
                    foreach (FCValueItem vi in col.ValueItems())
                    {
                        if (vi.Value.ToString() == cellValueString)
                        {
                            return vi.DisplayValue.ToString();
                        }
                    }
                }
                else
                {
                    //CHE: if FormattedValue is empty, return from Value
                    if (dr.Cells[col.Index].FormattedValue != null && !string.IsNullOrEmpty(dr.Cells[col.Index].FormattedValue.ToString().Trim()))
                    {
                        return dr.Cells[col.Index].FormattedValue.ToString();
                    }
                    else
                    {
                        if (dr.Cells[col.Index].Value != null)
                        {
                            return dr.Cells[col.Index].Value.ToString();
                        }
                    }
                }
            }

            return "";
        }

        /// <summary>
        /// set displayed column text for current row
        /// </summary>
        /// <param name="col"></param>
        /// <param name="text"></param>
        public static void Text(this DataGridViewColumn col, string text)
        {
            DataGridViewRow dr = col.DataGridView.CurrentRow;
            if (dr != null)
            {
                dr.Cells[col.Index].Value = text;
            }
        }

        public static ConvertEmptyCell ConvertEmptyCell(this DataGridViewColumn col, ConvertEmptyCell convert)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            objColDefinitions.ConvertEmptyCell = convert;

            return convert;
        }

        public static ConvertEmptyCell ConvertEmptyCell(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.ConvertEmptyCell;
        }

        public static string EditMask(this DataGridViewColumn col)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.EditMask;
        }

        public static string EditMask(this DataGridViewColumn col, string editMask)
        {
            //get the instance of the definitions class
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.EditMask = editMask;

            return editMask;
        }

        public static void Button(this DataGridViewColumn col, bool button)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.Button = button;
        }

        public static void Button(ref DataGridViewColumn col, bool button)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.Button = button;

            if (col.GetType() == typeof(DataGridViewTextBoxColumn) && button)
            {
                CloneColumn(ref col, typeof(DataGridViewCustomComboBoxColumn));
            }
            //if (!button && col.DropDown() == null)
            //{
            //    CloneColumn(ref col, typeof(DataGridViewTextBoxColumn));
            //}

            objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.Button = button;
        }

        public static void CloneColumn(ref DataGridViewColumn col, Type colType)
        {
            //SBE: if DataGridViewColumn.DropDown() is called from RowColChange exception is thrown when Column is replaced
            if ((col.GetType() != colType))
            {
                DataGridView grid = col.DataGridView;
                DataGridViewColumn initialCol = col;
                int initialPosition = initialCol.Index;

                DataGridViewColumn newCol = default(DataGridViewColumn);
                if (colType == typeof(DataGridViewTextBoxColumn))
                {
                    newCol = new DataGridViewTextBoxColumn();

                }
                else if (colType == typeof(DataGridViewCustomComboBoxColumn))
                {
                    newCol = new DataGridViewCustomComboBoxColumn();
                }
                else if (colType == typeof(DataGridViewComboBoxColumn))
                {
                    newCol = new DataGridViewComboBoxColumn();
                }
                else if (colType == typeof(DataGridViewRadioColumn))
                {
                    newCol = new DataGridViewRadioColumn();
                }
                else if (colType == typeof(DataGridViewCheckBoxColumn))
                {
                    newCol = new DataGridViewCheckBoxColumn();
                }
                //else if (colType == typeof(DataGridViewCharacterCasingTextBoxColumn))
                //{
                //    newCol = new DataGridViewCharacterCasingTextBoxColumn();
                //}
                else if (colType == typeof(FCDataGridViewTextBoxColumn))
                {
                    newCol = new FCDataGridViewTextBoxColumn();
                }

                DataGridViewTextBoxColumn textCol = newCol as DataGridViewTextBoxColumn;
                if (textCol != null)
                {
                    DataGridViewTextBoxColumn textBoxColumn = initialCol as DataGridViewTextBoxColumn;
                    if (textBoxColumn != null)
                    {
                        textCol.MaxInputLength = textBoxColumn.MaxInputLength;
                    }
                }

                newCol.HeaderText = initialCol.HeaderText;
                newCol.Name = initialCol.Name;
                newCol.Visible = initialCol.Visible;
                newCol.Frozen = initialCol.Frozen;
                newCol.DataPropertyName = initialCol.DataPropertyName;
                newCol.SetColumnWidth(initialCol.Width);
                newCol.Resizable = col.Resizable;
                newCol.DefaultCellStyle = initialCol.DefaultCellStyle;
                newCol.ReadOnly = initialCol.ReadOnly;
                
                //IPI: when initialCol is removed from FCTDBGrid's column collection, it's ColumnDefinitions are disposed
                Alignment alignment = initialCol.Alignment();
                Color headBackColor = initialCol.HeadBackColor();

                FCTDBGrid fcGrid = grid as FCTDBGrid;
                fcGrid.DisableEvents = true;
                //IPI: in case of CustomDropDownColumn, setting ReadOnly = true will no longer generate "DropDownWindow" event when user clicks the drop down arrow 
                // (instead will generate "Click" event and cell will enter edit mode) 
                
                bool dropDownList = col.DropDownList() || initialCol.ReadOnly;
                if (colType == typeof(DataGridViewCustomComboBoxColumn) && initialCol.ReadOnly)
                {
                    newCol.ReadOnly = !initialCol.ReadOnly;
                }
                
                Presentation presentation = col.ValueItems().Presentation();
                newCol.ValueItems().Translate = col.ValueItems().Translate;
                foreach (FCValueItem vi in initialCol.ValueItems())
                {
                    newCol.ValueItems().Add(vi);
                }
                
                int dataWidth = initialCol.DataWidth();
                string editMask = initialCol.EditMask();
                int splitNr = initialCol.GetAttachedSplit();
                int order = initialCol.Order();
                bool merge = initialCol.Merge();
                bool button = initialCol.Button();

                fcGrid.Columns.RemoveAt(initialPosition);
                fcGrid.Columns.Insert(initialPosition, newCol);

                //IPI: set back ColumnDefinitions which were saved from initialCol
                newCol.Alignment(alignment);
                newCol.HeadBackColor(headBackColor);
                newCol.DropDownList(dropDownList);
                //CHE: only set values for definitions
                newCol.ValueItems().SetPresentation(presentation);
                newCol.Button(button);

                newCol.AttachToSplit(splitNr);

                newCol.EditMask(editMask);
                newCol.DataWidth(dataWidth);
                newCol.Order(order);
                newCol.Merge(merge);

                fcGrid.DisableEvents = false;
                col = newCol;
            }
        }

        public static bool Button(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.Button;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static bool ButtonAlways(this DataGridViewColumn col, bool buttonAlways)
        {
            return buttonAlways;
        }

        //BAN: remove column at position <col>
        public static void Remove(this DataGridViewColumnCollection columns, int col)
        {
            columns.RemoveAt(col);
        }

        public static FCValueItems ValueItems(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.ValueItems;
        }

        /// <summary>
        /// get value in specific column for current row
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public static object Value(this DataGridViewColumn col)
        {
            if (col == null || col.DataGridView == null)
            {
                return null;
            }
            //CHE: if grid is empty return null 
            //(e.g. checking value in BeforeRowColChange for "*" row will throw exception when return ((DataTable)col.DataGridView.DataSource).CurrentRow()[col.DataPropertyName];)
            DataTable dataTable = col.DataGridView.DataSource as DataTable;
            if (dataTable != null && dataTable.RecordCount() == 0)
            {
                return null;
            }

            FCTDBGrid grid = col.DataGridView as FCTDBGrid;
            DataGridViewRow currentRow = null;
            if (grid != null)
            {
                currentRow = grid.CurrentRow;
                //If the current row is the row which allows users to add new rows to the data grid view, the current cell is not dirty
                //this means that the last row was edited and committed to the grid and now the selection on the grid is on this new row
                //In this case we must return the penultimate row in the grid
                //SBE: check if Index > 0 to avoid IndexOutOfRange exception
                //CHE: if value is retrieved from CellBeginEdit do not move to penultimate row, should stay on current new added
                if (currentRow != null && currentRow.IsNewRow && !grid.IsCurrentCellInEditMode && currentRow.Index == grid.Rows.Count - 1 && currentRow.Index > 0
                    && !grid.IsCurrentCellDirty && !grid.IsCellBeginEdit)
                {
                    currentRow = grid.Rows[currentRow.Index - 1];
                }
                if (currentRow != null && col.DataGridView.GetType() == typeof(TDBDropDown))
                {
                    TDBDropDown dropDown = col.DataGridView as TDBDropDown;
                    if (dropDown.Rows.Count > dropDown.SelectedRow)
                    {
                        currentRow = dropDown.Rows[dropDown.SelectedRow];
                    }
                }
            }
            if (currentRow != null)
            {
                //for comboboxcolumn get corresponding valuemember
                DataGridViewComboBoxColumn comboCol = col as DataGridViewComboBoxColumn;
                string currentCellString = Convert.ToString(currentRow.Cells[col.Index].Value);
                if (comboCol != null)
                {
                    foreach (FCValueItem item in comboCol.Items)
                    {
                        if (Convert.ToString(item.Value) == currentCellString)
                        {
                            return item.Value; 
                        }
                    }
                }

                return currentRow.Cells[col.Index].Value;
            }
            else
            {
                if (col.DataGridView.GetType() == typeof(FCTDBGrid))
                {
					//SBE: add check for grid.Row == -1 to avoid exception
                    if (grid.Rows.Count > grid.Row && grid.Row >=0 && grid.Columns.Count > col.Index)
                    {
                        return grid.Rows[grid.Row].Cells[col.Index].Value;
                    }
                    else if (dataTable != null)
                    {
                        return dataTable.CurrentRow()[col.DataPropertyName];
                    }
                }
                return null;
            }
        }

        //set value
        public static void Value(this DataGridViewColumn col, object value)
        {
            DataGridViewRow dr = col.DataGridView.CurrentRow;
            if (dr != null)
            {
                //IPI: setting cell value will generate RowValidated event -> DisableEvents
                FCTDBGrid grid = col.DataGridView as FCTDBGrid;
                if (grid != null)
                {
                    grid.DisableEvents = true;
                }
                dr.Cells[col.Index].Value = value;
                
                DataGridViewComboBoxColumn cmbCol = col as DataGridViewComboBoxColumn;
                
                grid.UpdateInvertedComboBox(col);
                
                if (grid != null)
                {
                    if (grid.IsInvertedOrFormView())
                    {
                        if (grid.InvertedView != null)
                        {
                            //CHE: if Value is changed in BeforeColUpdate should locate again the item
                            //IPI: must locate the inverted control
                            ComboBox cCmb = null;
                            InvertedData data = null;
                            foreach (Control ctrl in grid.InvertedView.Controls)
                            {
                                data = ctrl.Tag as InvertedData;
                                if (data != null && data.Col.Index == col.Index)
                                {
                                    #region Set ComboBox item
                                    if (cmbCol != null && cmbCol.Items.Count > 0)
                                    {
                                        cCmb = ctrl as ComboBox;
                                        foreach (object item in cmbCol.Items)
                                        {
                                            string memberValue = null, displayValue = null;
                                            if (item.GetType() == typeof(FCValueItem))
                                            {
                                                FCValueItem valueItem = item as FCValueItem;
                                                memberValue = valueItem.Value as string;
                                                displayValue = valueItem.DisplayValue as string;
                                            }
                                            else
                                            {
                                                memberValue = item as string;
                                                displayValue = item as string;
                                            }
                                            if (memberValue != null && (Convert.ToString(memberValue) == Convert.ToString(value) || (col.ValueItems().Translate && displayValue == value)))
                                            {
                                                cCmb.Text = displayValue;
                                                cCmb.SelectedItem = item;
                                                break;
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }

                        grid.DisableEvents = false;
                        grid.RefreshInvertedData();
                    }
                    else
                    {
                        grid.DisableEvents = false;
                    }
                }
            }
        }

        /// <summary>
        /// in VB6 DropDownList is a property for any type of column, cannot be mapped directly to DropDownStyle which only applies for ComboBoxColumn
        /// </summary>
        /// <param name="col"></param>
        /// <param name="dropDownList"></param>
        public static void DropDownList(this DataGridViewColumn col, bool dropDownList)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.dropDownList = dropDownList;

            //CHE: set DropDownStyle
            DataGridViewComboBoxColumn comboCol = col as DataGridViewComboBoxColumn;
            if (comboCol != null)
            {
                //comboCol.DropDownStyle = dropDownList ? ComboBoxStyle.DropDownList : ComboBoxStyle.DropDown;
            }
        }

        /// <summary>
        /// in VB6 DropDownList is a property for any type of column, cannot be mapped directly to DropDownStyle which only applies for ComboBoxColumn
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public static bool DropDownList(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.dropDownList;
        }

        public static void AutoDropDown(this DataGridViewColumn col, bool autoDropDown)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            objColDefinitions.autoDropDown = autoDropDown;
        }

        public static bool AutoDropDown(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
            return objColDefinitions.autoDropDown;
        }

        //BAN
        public static void DataChanged(this DataGridViewColumn col, bool dataChanged)
        {
            FCTDBGrid grid = col.DataGridView as FCTDBGrid;
            grid.SetModified(col, dataChanged);
        }

        //BAN
        public static bool DataChanged(this DataGridViewColumn gridCol)
        {
            FCTDBGrid grid = gridCol.DataGridView as FCTDBGrid;

            if (grid.CurrentRow == null)
            {
                return false;
            }

            DataRowView boundRow = grid.CurrentRow.DataBoundItem as DataRowView;
            if (boundRow != null)
            {
                if (boundRow.IsNew)
                {
                    object cellValue = boundRow[gridCol.DataPropertyName];
                    if (cellValue != null && !string.IsNullOrEmpty(cellValue.ToString()))
                    {
                        return true;
                    }
                    return false;
                }
            }

            DataGridViewRow mobjRow = gridCol.DataGridView.CurrentRow;
            if (mobjRow == null)
            {
                return false;
            }

            DataGridViewRowDefinitions objRowDefinitions = DataGridViewRowDefinitions.GetDataTableDefinitions(mobjRow);

            if (objRowDefinitions.m_modified == null)
                return false;

            if (gridCol.Index >= objRowDefinitions.m_modified.Length)
                return false;

            return objRowDefinitions.m_modified[gridCol.Index];
        }

        //BAN
        public static void HeadBackColor(this DataGridViewColumn col, System.Drawing.Color color)
        {
            col.HeaderCell.Style.BackColor = color;

            FCTDBGrid grid = col.DataGridView as FCTDBGrid;
            if (grid != null)
            {
                grid.ChangeColHeaderBackColor(col, color);
            }
        }

        //BAN
        public static void HeadBackColor(this DataGridViewColumn col, int color)
        {
            HeadBackColor(col, ColorTranslator.FromOle(color));
        }

        //BAN
        public static System.Drawing.Color HeadBackColor(this DataGridViewColumn col)
        {
            if (col.HeaderCell.Style.BackColor == System.Drawing.Color.Empty)
            {
                return System.Drawing.SystemColors.Control;
            }
            return col.HeaderCell.Style.BackColor;
        }

        public static System.Drawing.Color HeadForeColor(this DataGridViewColumn col, System.Drawing.Color color)
        {
            col.HeaderCell.Style.ForeColor = color;

            FCTDBGrid grid = col.DataGridView as FCTDBGrid;
            if (grid != null)
            {
                grid.ChangeColHeaderForeColor(col, color);
            }
            return color;
        }

        public static System.Drawing.Color HeadForeColor(this DataGridViewColumn col, int color)
        {
            return HeadForeColor(col, ColorTranslator.FromOle(color));
        }

        public static System.Drawing.Color HeadForeColor(this DataGridViewColumn col)
        {
            if (col.HeaderCell.Style.ForeColor == System.Drawing.Color.Empty)
            {
                return System.Drawing.Color.Gray;
            }
            return col.HeaderCell.Style.BackColor;
        }

        public static FCFont HeadFont(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.headFont;
        }

        public static void HeadFont(this DataGridViewColumn col, FCFont headFont)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.headFont = headFont;
        }

        public static FCFont Font(this DataGridViewColumn col)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            return objColDefinitions.font;
        }

        public static void Font(this DataGridViewColumn col, FCFont font)
        {
            DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);

            objColDefinitions.font = font;
        }

        public static FCStyle HeadingStyle(this DataGridViewColumn col)
        {
            return new FCStyle(col.HeaderCell.Style, fecherFoundation.Alignment.General, SystemColors.WindowText, SystemColors.WindowText, VerticalAlignment.Top);
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static int CellTop(this DataGridViewColumn col)
        {
            return 0;
        }

        public static int Left(this DataGridViewColumn col)
        {
            int left = 0;
            if (col.DataGridView != null)
            {

                for (int colIndex = 0; colIndex < col.Index; colIndex++)
                {
                    DataGridViewColumn column = col.DataGridView.Columns[colIndex];
                    left += column.Visible ? column.Width : 0;
                }
            }

            return left;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static System.Drawing.Color EditBackColor(this DataGridViewColumn col, System.Drawing.Color color)
        {
            return color;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static System.Drawing.Color EditBackColor(this DataGridViewColumn col)
        {
            return System.Drawing.Color.Gray;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public static void RefreshCell(this DataGridViewColumn col)
        {
        }

        /// <summary>
        /// use extension to set column with, ColResize should not be raised when changing width programatically
        /// </summary>
        /// <param name="col"></param>
        /// <param name="width"></param>
        public static void SetColumnWidth(this DataGridViewColumn col, int width)
        { 
            FCTDBGrid grid = col.DataGridView as FCTDBGrid;
            if (grid != null)
            {
                grid.SkipColResize = true;
                using (Graphics g = grid.CreateGraphics())
                {
                    grid.FCGraphics.InitializeCoordinateSpace(g, grid.Bounds, 0, 0);
                    width = Convert.ToInt32(grid.FCGraphics.ScaleX(width, grid.FCGraphics.GetParentScaleMode(grid), ScaleModeConstants.vbPixels));
                }
                col.Width = width;
                grid.SkipColResize = false;
            }
            else
            {
                col.Width = width;
            }
        }

        public static void ReleaseColumn(this DataGridViewColumn col)
        {
            if (col != null && DataGridViewColumnExtension.mobjDefinitions.ContainsKey(col))
            {
                DataGridViewColumnDefinitions objDefinitions = DataGridViewColumnExtension.mobjDefinitions[col];

                objDefinitions.Dispose();
                objDefinitions = null;
                
                DataGridViewColumnExtension.mobjDefinitions.TryRemove(col, out objDefinitions);
            }
        }
    }
}
