﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;

namespace fecherFoundation
{
    /// <summary>
    /// The TDBDropDown control, which is a subset of the TDBGrid control, is used as a multicolumn drop-down list box for a grid column. 
    /// You cannot use it as a standalone control.
    /// </summary>
    public class TDBDropDown : FCTDBGrid
    {
        #region Private Members

        private int selectedRow = 0;

        #endregion

        #region Public Delegates

        // delegate declaration 
        public delegate void OnDropDownOpen(object sender, EventArgs ca);

        // delegate declaration 
        public delegate void OnDropDownClose(object sender, EventArgs ca);

        #endregion

        #region Public Events

        // event declaration 
        public event OnDropDownOpen DropDownOpen;        

        // event declaration 
        public event OnDropDownClose DropDownClose;

        #endregion

        #region Public Properties
        [DefaultValue("")]
        public string DataField
        {
            get;
            set;
        }

        [DefaultValue("")]
        public string ListField
        {
            get;
            set;
        }

        [DefaultValue(false)]
        public bool ValueTranslate
        {
            get;
            set;
        }
        #endregion

        #region Private Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelectedRow
        {
            get { return selectedRow; }
            set { selectedRow = value; }
        }
        
        #endregion

        #region Public Methods

        public void CallDropDownOpen(object sender, EventArgs e)
        {
            if (DropDownOpen != null)
            {
                DropDownOpen(sender, e);
            }
        }

        public void CallDropDownClose(object sender, EventArgs e)
        {
            if (DropDownClose != null)
            {
                DropDownClose(sender, e);
            }
        }

        #endregion
    }
}