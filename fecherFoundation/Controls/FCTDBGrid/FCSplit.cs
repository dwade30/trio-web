﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace fecherFoundation
{
    [Serializable]
    public class FCSplit
    {
        #region Public Members

        public FCDataGridViewColumnCollection Columns;
        #endregion

        #region Private Members

        private FCTDBGrid grid;

        #endregion

        #region Properties
        /// <summary>
        /// ExtendRightColumn Property
        ///This property allows the rightmost column of a grid or split to extend to the object's right edge, provided that the object can accommodate all of the visible columns.
        ///Syntax
        ///object.ExtendRightColumn = boolean
        ///Remarks
        ///Read/Write at run time and design time.
        ///If True, the last column will extend to the end of the grid or split.
        ///If False (the default), the area between the last column and the end of the grid or split will be filled using the system 3D Objects color (or the system Button Face color) as determined by your Control Panel settings.
        ///If a grid contains multiple splits, then setting its ExtendRightColumn property has the same effect as setting the ExtendRightColumn property of each split individually.
        ///Note
        ///This property now works even when the horizontal scroll bar is present. Prior to version 5.0, if a grid or split could not accommodate all of the visible columns, then setting this property to True had no effect.
        /// </summary>
        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool ExtendRightColumn { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public int Size { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool AllowSizing { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public bool AllowRowSizing { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
        public string Text { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public DividerStyle DividerStyle { get; set; }

		public FCStyle CaptionStyle { get; set; } = new FCStyle(Alignment.Center, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);
		public FCStyle EditorStyle { get; set; } = new FCStyle(Alignment.Left, SystemColors.Window, SystemColors.WindowText, VerticalAlignment.Top);
		public FCStyle EvenRowStyle { get; set; } = new FCStyle(Alignment.General, Color.Yellow, SystemColors.WindowText, VerticalAlignment.Top);
		public FCStyle FooterStyle { get; set; } = new FCStyle(Alignment.General, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);
		public FCStyle HeadingStyle { get; set; } = new FCStyle(Alignment.Left, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);
		public FCStyle HighlightRowStyle { get; set; } = new FCStyle(Alignment.General, SystemColors.WindowText, SystemColors.Window, VerticalAlignment.Top);
		public FCStyle InactiveStyle { get; set; } = new FCStyle(Alignment.Left, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);
		public FCStyle OddRowStyle { get; set; } = new FCStyle(Alignment.General, SystemColors.Window, SystemColors.WindowText, VerticalAlignment.Top);
		public FCStyle SelectedStyle { get; set; } = new FCStyle(Alignment.Left, SystemColors.Highlight, SystemColors.HighlightText, VerticalAlignment.Top);
		public FCStyle Style { get; set; } = new FCStyle(Alignment.Left, SystemColors.Window, SystemColors.WindowText, VerticalAlignment.Top);

		public FCFont Font { get; set; } = new FCFont();

		public FCFont FooterFont { get; set; } = new FCFont();

		public FCFont HeadFont { get; set; } = new FCFont();

		[Obsolete("Not implemented. Added for migration compatibility")]
        public SizeMode SizeMode { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public int ScrollGroup { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public double HScrollHeight { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public double VScrollWidth { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
        [DefaultValue(ScrollBars.Automatic)]
        public ScrollBars ScrollBars { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool RecordSelectors { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public bool AllowFocus { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public bool AllowRowSelect { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public bool FetchRowStyle { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
        public int RecordSelectorWidth { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public int LeftCol { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public System.Drawing.Color InactiveBackColor { get; set; } = SystemColors.Control;

		[Obsolete("Not implemented. Added for migration compatibility")]
		public System.Drawing.Color FooterBackColor { get; set; } = SystemColors.Control;

		[Obsolete("Not implemented. Added for migration compatibility")]
        public System.Drawing.Color BackColor { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool AllowUserToOrderColumns { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool AllowColSelect { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public string HeaderText { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public MarqueeStyle MarqueeStyle { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public bool CurrentCellVisible { get; set; }

		[Obsolete("Not implemented. Added for migration compatibility")]
		public Color EditBackColor { get; set; } = SystemColors.Window;

		[Obsolete("Not implemented. Added for migration compatibility")]
		public Color EditForeColor { get; set; } = SystemColors.WindowText;

		[Obsolete("Not implemented. Added for migration compatibility")]
		public Color ForeColor { get; set; } = SystemColors.WindowText;

		[Obsolete("Not implemented. Added for migration compatibility")]
		public Color SelectedBackColor { get; set; } = SystemColors.Highlight;

		[Obsolete("Not implemented. Added for migration compatibility")]
		public Color SelectedForeColor { get; set; } = SystemColors.HighlightText;
		#endregion

		#region Constructors
		public FCSplit(FCTDBGrid grid)
        {
            this.grid = grid;
            Columns = new FCDataGridViewColumnCollection(grid);
        }

		#endregion

	}
}