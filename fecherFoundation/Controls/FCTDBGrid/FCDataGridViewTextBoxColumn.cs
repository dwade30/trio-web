﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Drawing;

namespace fecherFoundation
{
    [Serializable]
    public class FCDataGridViewTextBoxColumn : DataGridViewTextBoxColumn
    {
        #region Private fields
        #endregion

        #region Constructors
        public FCDataGridViewTextBoxColumn() : base()   
        {
        }
		#endregion

		#region Properties
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public FCStyle Style { get; set; } = new FCStyle(Alignment.Left, SystemColors.Window, SystemColors.WindowText, VerticalAlignment.Top);

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public FCStyle EditorStyle { get; set; } = new FCStyle(Alignment.Left, SystemColors.Window, SystemColors.WindowText, VerticalAlignment.Top);

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public FCStyle FooterStyle { get; set; } = new FCStyle(Alignment.General, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public FCStyle HeadingStyle { get; set; } = new FCStyle(Alignment.Left, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);

		public FCFont FooterFont { get; set; } = new FCFont();

		public string TableName { get; set; }

		[DefaultValue(Alignment.General)]
		public Alignment FooterAlignment { get; set; } = Alignment.General;

		public Color EditForeColor { get; set; } = SystemColors.WindowText;

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new DataGridViewColumnSortMode SortMode
		{
			get
			{
				return base.SortMode;
			}
			set
			{
				base.SortMode = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ReadOnly
		{
			get
			{
				return base.ReadOnly;
			}
			set
			{
				base.ReadOnly = value;
			}
		}
		#endregion

		#region Methods

		#endregion
	}

}
