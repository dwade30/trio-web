﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace fecherFoundation
{
    [Serializable]
    public class FCSplitCollection : CollectionBase
    {
        #region Private Members

        private FCTDBGrid grid;

        #endregion

        #region Constructors

        public FCSplitCollection(FCTDBGrid grid)
        {
            this.grid = grid;
            this.List.Add(new FCSplit(grid));
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets value for the item by that index
        /// </summary>
        public FCSplit this[int index]
        {
            get
            {
                return (FCSplit)this.List[index];
            }
            set
            {
                this.List[index] = value;
            }
        }

        #endregion

        #region Public Methods

        public FCSplit Add(int index)
        {
            FCSplit split = new FCSplit(this.grid);
            this.List.Insert(index, split);
            return split;
        }

        public void Remove(int index)
        {
            this.List.RemoveAt(index);
        }

        #endregion 
    }
}
