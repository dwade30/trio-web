﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    internal class InvertedData
    {
        #region Private Members

        private DataGridViewColumn column;
        private int row;
        private int rowCount;
        private string value;
        private bool modified;

        #endregion

        #region Properties

        internal bool Modified
        {
            get
            {
                return modified;
            }
            set
            {
                modified = value;
            }
        }

        internal DataGridViewColumn Col
        {
            get
            {
                return this.column;
            }
            set
            {
                this.column = value;
            }
        }

        internal int Row
        {
            get
            {
                return this.row;
            }
            set
            {
                this.row = value;
            }
        }

        internal int RowCount
        {
            get
            {
                return rowCount;
            }
            set
            {
                rowCount = value;
            }
        }

        /// <summary>
        /// used to transmit value member for generated radio buttons
        /// </summary>
        internal string Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        #endregion

        #region Constructors

        public InvertedData(DataGridViewColumn col, int row, int rowCount, string value = "")
        {
            this.column = col;
            this.row = row;
            this.rowCount = rowCount;
            this.value = value;
        }

        #endregion
        
    }
}
