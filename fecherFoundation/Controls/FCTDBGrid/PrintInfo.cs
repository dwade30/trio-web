﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public class PrintInfo
    {
        #region Public Properties

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool PreviewMaximize
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool NoClipping
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public string PageFooter
        {
            set;
            get;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public string PageHeader
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public string PreviewPageOf
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool RepeatColumnFooters
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool RepeatColumnHeaders
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool RepeatGridHeader
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public int SettingsMarginLeft
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public int SettingsMarginRight
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public int SettingsMarginTop
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool VariableRowHeight
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public bool PreviewAllowFileOps
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public int SettingsOrientation
        {
            get;
            set;
        }

        #endregion

        #region Public Methods

        [Obsolete("Not implemented. Added for migration compatibility")]
        public void SetMenuText(PrintInfo_Menu constant, string text)
        {
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public void PrintPreview(int rows = -1)
        {
        }

        #endregion
    }
}
