﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace fecherFoundation
{
    public class FCDataGridViewColumnCollection : DataGridViewColumnCollection, IEnumerable
    {
        private FCTDBGrid grid;

        #region Constructors

        public FCDataGridViewColumnCollection(DataGridView grid, DataGridViewColumnCollection cols)
            : base(grid)
        {
        }

        public FCDataGridViewColumnCollection(DataGridView grid) : base(grid) 
        {
            this.grid = grid as FCTDBGrid;
        }

        #endregion

        #region Public Methods

        public override void AddRange(params DataGridViewColumn[] arrDataGridViewColumns)
        {
            this.grid.DisableEvents = true;
			foreach (DataGridViewColumn column in arrDataGridViewColumns)
			{
				column.SortMode = DataGridViewColumnSortMode.NotSortable;
			}
            base.DataGridView.Columns.AddRange(arrDataGridViewColumns);
            this.grid.RedrawInverted();
            this.grid.DisableEvents = false;
        }

        public override void Clear()
        {
            this.grid.DisableEvents = true;
            base.DataGridView.Columns.Clear();
            //CHE: remove all controls for inverted
            this.grid.ClearInvertedPanel();
            this.grid.DisableEvents = false;
        }
        
        public override int Add(string strColumnName, string strHeaderText)
        {
            this.grid.DisableEvents = true;
            int returnValue = base.DataGridView.Columns.Add(strColumnName, strHeaderText);
			this[returnValue].SortMode = DataGridViewColumnSortMode.NotSortable;
            this.grid.RedrawInverted();
            this.grid.DisableEvents = false;
            return returnValue;
        }

        public override bool Contains(DataGridViewColumn objDataGridViewColumn)
        {
            return base.DataGridView.Columns.Contains(objDataGridViewColumn);
        }

        public override bool Contains(string strColumnName)
        {
            return base.DataGridView.Columns.Contains(strColumnName);
        }

        public override void Insert(int intColumnIndex, DataGridViewColumn objDataGridViewColumn)
        {
            this.grid.DisableEvents = true;
			objDataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
			base.DataGridView.Columns.Insert(intColumnIndex, objDataGridViewColumn);
            this.grid.RedrawInverted();
            this.grid.DisableEvents = false;
        }

        public override void Remove(DataGridViewColumn objDataGridViewColumn)
        {
            this.grid.DisableEvents = true;
            base.DataGridView.Columns.Remove(objDataGridViewColumn);
            this.grid.RedrawInverted();
            this.grid.DisableEvents = false;
        }

        public override void Remove(string strColumnName)
        {
            this.grid.DisableEvents = true;
            base.DataGridView.Columns.Remove(strColumnName);
            this.grid.RedrawInverted();
            this.grid.DisableEvents = false;
        }

		public override void RemoveAt(int index)
		{
			this.grid.DisableEvents = true;
			base.DataGridView.Columns.RemoveAt(index);
			this.grid.RedrawInverted();
			this.grid.DisableEvents = false;
		}

        public override int Count
        {
            get
            {
                return base.DataGridView.Columns.Count;
            }
        }

        public override bool Equals(object obj)
        {
            return base.DataGridView.Columns.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.DataGridView.Columns.GetHashCode();
        }

        public override string ToString()
        {
            return base.DataGridView.Columns.ToString();
        }

        protected new FCTDBGrid DataGridView
        {
            get
            {
                return grid as FCTDBGrid;
            }
        }

        public new void Add(DataGridViewColumn column) 
        {
            this.grid.DisableEvents = true;
			column.SortMode = DataGridViewColumnSortMode.NotSortable;
            base.DataGridView.Columns.Add(column);
            this.grid.RedrawInverted();
            this.grid.DisableEvents = false;
        }

        public DataGridViewColumn Add(int position)
        {
            DataGridViewColumn newColumn = new DataGridViewTextBoxColumn();
			newColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.Insert(position, newColumn);
            return newColumn;
        }

        public new DataGridViewColumn this[int index] 
        { 
            get 
            {
                if (index >= 0 && index < base.DataGridView.Columns.Count)
                {
                    return base.DataGridView.Columns[index];
                }

                return null;
            } 
        }

        public new DataGridViewColumn this[string name]
        {
            get
            {
                if (base.DataGridView.Columns[name] == null)
                {
                    string upperName = name.ToUpper();
                    foreach (DataGridViewColumn col in base.DataGridView.Columns)
                    {
                        if (col.DataField().ToUpper() == upperName)
                        {
                            return col;
                        }
                    }

                    foreach (DataGridViewColumn col in base.DataGridView.Columns)
                    {
                        if (col.HeaderText.ToUpper() == upperName)
                        {
                            return col;
                        }
                    }
                }
                return base.DataGridView.Columns[name];
            }
        }

        //implement simple iteration
        public IEnumerator GetEnumerator()
        {
            return base.DataGridView.Columns.GetEnumerator();
        }

        #endregion

    }
}
