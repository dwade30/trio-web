#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;

#endregion

namespace fecherFoundation
{
    internal partial class TDBDropDownForm : Form
    {
        #region Private Members

        private DataGridViewCustomComboBoxCell CellTemplate;
        private bool blnLoading = false;
        private DataGridViewCustomComboBoxColumn currentCol;
        internal int rowValidating = -1;
        internal int colValidating = -1;

        #endregion

        #region Internal Properties

        internal TDBDropDown TDBDropDown
        {
            get { return this.dropDownGrid; }
        }

        internal DataGridViewCustomComboBoxColumn CurrentCol
        {
            get
            {
                return currentCol;
            }
        }

        //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
        //for this case use CustomComboBoxColumn with ListBox as DropDown
        internal FCValueItem SelectedItem = null;

        #endregion

        #region Constructors

        internal TDBDropDownForm(DataGridViewCustomComboBoxCell cellTemplate)
        {
            InitializeComponent();

            this.CellTemplate = cellTemplate;
        }

        #endregion

        #region Private Event Handlers

        private void dropDownGrid_SelectionChanged(object sender, System.EventArgs e)
        {
            if (blnLoading)
            {
                return;
            }

            //Sets successful results for form.
            this.DialogResult = DialogResult.OK;
            FCTDBGrid parentGrid = currentCol.DataGridView as FCTDBGrid;
            if (parentGrid != null)
            {
                parentGrid.DisableEvents = true;
                //CNA: check CurrentCell
                if (this.dropDownGrid.CurrentCell != null)
                {
                    currentCol.TDBDropDown.Rows[this.dropDownGrid.CurrentCell.RowIndex].Selected = true;
                    currentCol.TDBDropDown.SelectedRow = this.dropDownGrid.CurrentCell.RowIndex;
                }
                parentGrid.DisableEvents = false;
            }
            //Close form.
            this.Close();
        }
        //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
        //for this case use CustomComboBoxColumn with ListBox as DropDown
        private void listBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (blnLoading)
            {
                return;
            }

            //Sets successful results for form.
            this.DialogResult = DialogResult.OK;
            FCTDBGrid parentGrid = currentCol.DataGridView as FCTDBGrid;
            if (parentGrid != null)
            {
                parentGrid.DisableEvents = true;
                this.SelectedItem = listBox.SelectedItem as FCValueItem;
                parentGrid.DisableEvents = false;
            }
            //Close form.
            this.Close();
        }

        private void TDBDropDownForm_Load(object sender, EventArgs e)
        {
            if (CellTemplate.DataGridView == null)
            {
                return;
            }

            if (this.dropDownGrid.Parent == null)
            {
                this.Controls.Add(this.dropDownGrid);
            }

            currentCol = CellTemplate.DataGridView.Columns[CellTemplate.ColumnIndex] as DataGridViewCustomComboBoxColumn;
            TDBDropDown currentDropDown = currentCol.TDBDropDown;


            // Prevent SelectionChanged event to close the form while databinding and setting initial cell
            blnLoading = true;

            FCTDBGrid grid = CellTemplate.DataGridView as FCTDBGrid;

            //IPI: need to reset the cancel edit flag; when selecting the DropDownForm, CellBeginEdit is not raised and the cancel edit flag is set for the previous cell
            grid.IsCancelEdit = false;

            rowValidating = -1;
            colValidating = -1;
            if (grid.CurrentCell != null)
            {
                //BAN: if the cell template row index is different from the current cell, then a row validating should occur
                if (grid.CurrentCell.RowIndex >= 0 && CellTemplate.RowIndex >= 0 && grid.CurrentCell.RowIndex != CellTemplate.RowIndex)
                {
                    rowValidating = grid.CurrentCell.RowIndex;
                    colValidating = grid.CurrentCell.ColumnIndex;
                }

            }
            // Disable events in the parent grid, also set the DropDownOpened flag so the grid will "know" not to move focus
            // or current cell while the dropdown is opened
            grid.DisableEvents = true;
            grid.IsDropDownOpened = true;

            if (rowValidating >= 0)
            {
                grid.RaiseBeforeRowColChange();
            }

            //IPI: even if the current cell is not yet set on CellTemplate, Row/Col must reflect the CellTemplate
            // because grid[Row,Col] == CellTemplate values will be retrieved in DropDownOpen
            grid.Row = CellTemplate.RowIndex;
            grid.Col = CellTemplate.ColumnIndex;

            if (currentDropDown != null)
            {
                currentDropDown.CallDropDownOpen(sender, e);

                int columnIndex = 0;
                this.dropDownGrid.Columns.Clear();

                this.dropDownGrid.DataSource = null;
                foreach (DataGridViewColumn originalCol in currentDropDown.Columns)
                {
                    this.dropDownGrid.Columns.Add(originalCol.Clone() as DataGridViewColumn);
                    this.dropDownGrid.Columns[columnIndex].SetColumnWidth(originalCol.Width);
                    this.dropDownGrid.Columns[columnIndex].HeaderText = originalCol.HeaderText;
                    this.dropDownGrid.Columns[columnIndex].DataPropertyName = originalCol.DataPropertyName;
                    this.dropDownGrid.Columns[columnIndex].ToolTipText = originalCol.ToolTipText;
                    this.dropDownGrid.Columns[columnIndex].ReadOnly = originalCol.ReadOnly;
                    this.dropDownGrid.Columns[columnIndex].NumberFormat(originalCol.NumberFormat());
                    columnIndex++;
                }
                //CHE: columns are added manually, do not copy AutoGenerateColumns from currentDropDown
                this.dropDownGrid.AutoGenerateColumns = false;
                this.dropDownGrid.DataSource = currentDropDown.DataSource;
                columnIndex = 0;
                //Set the column visibility after setting the DataSource
                foreach (DataGridViewColumn originalCol in currentDropDown.Columns)
                {
                    this.dropDownGrid.Columns[columnIndex].Visible = originalCol.Visible;
                    columnIndex++;
                }
                this.dropDownGrid.ColumnHeadersVisible = currentDropDown.ColumnHeadersVisible;
                this.dropDownGrid.RowHeadersVisible = currentDropDown.RowHeadersVisible;

                this.dropDownGrid.DataField = currentDropDown.DataField;
                this.dropDownGrid.ListField = currentDropDown.ListField;

                this.Size = new Size(currentDropDown.Width, currentDropDown.Height);

                if (this.dropDownGrid.ColumnCount > 0)
                {
                    this.dropDownGrid.Columns[this.dropDownGrid.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                //CHE: when columns are set to automatically resize, the user cannot adjust the column widths with the mouse
                this.dropDownGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                if (currentDropDown.AllowUserToResizeColumns)
                {
                    this.dropDownGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                }

                //IPI: ClearSelection raises "sometimes" the RowValidated event which will cause the closing of the DropDownForm
                // must disable events
                this.dropDownGrid.DisableEvents = true;
                // Clear the selection
                this.dropDownGrid.ClearSelection();
                this.dropDownGrid.DisableEvents = false;
            }
            else
            {
                dropDownGrid.Visible = false;
                listBox.Visible = true;
                this.SelectedItem = null;
                listBox.Items.Clear();
                if (currentCol.ValueItems().Translate)
                {
                    listBox.ValueMember = currentCol.ValueMember;
                    listBox.DisplayMember = currentCol.DisplayMember;
                }
                foreach (FCValueItem vi in currentCol.ValueItems())
                {
                    listBox.Items.Add(new FCValueItem(vi.DisplayValue, vi.Value));
                }
            }

            // Now, SelectionChanged should be effective.
            blnLoading = false;
            grid.DisableEvents = false;
        }

        #endregion
    }
}