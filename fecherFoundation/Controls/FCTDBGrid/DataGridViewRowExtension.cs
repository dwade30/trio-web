﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Concurrent;

namespace fecherFoundation
{
    [Serializable]
    internal class DataGridViewRowDefinitions
    {
        DataGridViewRow mobjRow = null;

        internal bool Modified { get; set; }

        internal BitArray m_modified;

        internal DataGridViewRowDefinitions(DataGridViewRow objRow)
        {
            mobjRow = objRow;
        }

        /// <summary>
        /// Gets the definitions of the current DataGridViewColumn
        /// </summary>
        /// <param name="objDataTable"></param>
        /// <returns></returns>
        public static DataGridViewRowDefinitions GetDataTableDefinitions(DataGridViewRow objRow)
        {
            DataGridViewRowDefinitions objRowDefinitions = null;

            if (DataGridViewRowExtension.mobjDefinitions.ContainsKey(objRow))
            {
                objRowDefinitions = DataGridViewRowExtension.mobjDefinitions[objRow];
            }
            else
            {
                objRowDefinitions = new DataGridViewRowDefinitions(objRow);
                objRow.DataGridView.RowsRemoved += new DataGridViewRowsRemovedEventHandler(DataGridView_RowsRemoved);
                DataGridViewRowExtension.mobjDefinitions.TryAdd(objRow, objRowDefinitions);
            }

            return objRowDefinitions;
        }

        private static void DataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid == null)
            {
                return;
            }
            try
            {
                if (e.RowIndex < 0 || e.RowIndex >= grid.Rows.Count)
                {
                    return;
                }
                DataGridViewRow row = grid.Rows[e.RowIndex];
                if (row == null)
                {
                    return;
                }

                DataGridViewRowDefinitions objRowDefinitions = DataGridViewRowDefinitions.GetDataTableDefinitions(row);
                objRowDefinitions.m_modified = null;
                objRowDefinitions = null;
                DataGridViewRowExtension.mobjDefinitions.TryRemove(row, out objRowDefinitions);
            }
            catch { }
        }

        /// <summary>
        ///  Remove a Row from the DataGridViewRowExtension.mobjDefinitions-Collection
        /// </summary>
        /// <param name="row"></param>
        public static void RemoveRow(DataGridViewRow row)
        {
            DataGridViewRowDefinitions objRowDefinitions = DataGridViewRowDefinitions.GetDataTableDefinitions(row);
            objRowDefinitions.m_modified = null;
            objRowDefinitions = null;
            DataGridViewRowExtension.mobjDefinitions.TryRemove(row, out objRowDefinitions);
        }

        /// <summary>
        /// Remove all Rows from the DataGridViewRowExtension.mobjDefinitions-Collection
        /// </summary>
        /// <param name="dataGridView"></param>
        public static void RemoveAllRows(DataGridView dataGridView)
        {
            if (dataGridView != null && dataGridView.IsHandleCreated)
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    RemoveRow(row);
                }
            }
        }
    }

    public static class DataGridViewRowExtension
    {
        /// <summary>
        /// A dictionary used to save the definition object 
        /// </summary>
        internal static ConcurrentDictionary<DataGridViewRow, DataGridViewRowDefinitions> mobjDefinitions = new ConcurrentDictionary<DataGridViewRow, DataGridViewRowDefinitions>();

        public static void SetModified(this DataGridViewRow mobjRow, DataGridViewColumn col, bool on)
        {
            if (col.DataGridView == null)
            {
                return;
            }

            DataGridViewRowDefinitions objRowDefinitions = DataGridViewRowDefinitions.GetDataTableDefinitions(mobjRow);
            int colsCount = col.DataGridView.Columns.Count;
            if (objRowDefinitions.m_modified == null)
                objRowDefinitions.m_modified = new BitArray(colsCount);

            if (objRowDefinitions.m_modified.Length != colsCount)
                objRowDefinitions.m_modified.Length = colsCount;

            if (objRowDefinitions.m_modified[col.Index] != on)
            {
                objRowDefinitions.m_modified[col.Index] = on;
            }

            objRowDefinitions.Modified = on;
        }

        public static bool GetModified(this DataGridViewRow mobjRow)
        {
            DataGridViewRowDefinitions objRowDefinitions = DataGridViewRowDefinitions.GetDataTableDefinitions(mobjRow);
            return objRowDefinitions.Modified;
        }
    }
}
