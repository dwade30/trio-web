﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    public class FCStyle
    {
        #region Private Members
        private DataGridViewCellStyle cellStyle;
        private FCFont font;
		Alignment defaultAlignment;
		Color defaultBackColor;
		Color defaultForeColor;
		VerticalAlignment defaultVerticalAlignment;
		#endregion

		#region Public Properties
		public Alignment Alignment
		{
			get;
			set;
		}

        public System.Drawing.Color BackColor
        {
            get
            {
                return cellStyle.BackColor;
            }
            set
            {
                if (cellStyle == null)
                {
                    cellStyle = new DataGridViewCellStyle();
                }
                cellStyle.BackColor = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public System.Drawing.Color ForeColor
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DefaultValue(null)]
		public System.Drawing.Image ForegroundPicture
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DefaultValue(null)]
        public System.Drawing.Image BackgroundPicture
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DefaultValue(ForegroundPicturePosition.Left)]
        public ForegroundPicturePosition ForegroundPicturePosition
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DefaultValue(false)]
        public bool TransparentForegroundPicture
        {
            get;
            set;
        }

        public FCFont Font
        {
            get
            {
                return font;
            }

            set
            {
                font = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DefaultValue(false)]
        public bool WrapText
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Value
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DefaultValue(false)]
        public bool Locked
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsSplitterFixed
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
		[DefaultValue(BackgroundPictureDrawMode.Center)]
        public BackgroundPictureDrawMode BackgroundPictureDrawMode
        {
            get;
            set;
        }

		public VerticalAlignment VerticalAlignment
		{
			get;
			set;
		}
        #endregion

        #region Constructors
        public FCStyle(Alignment defaultAlignment, Color defaultBackColor, Color defaultForeColor, VerticalAlignment defaultVerticalAlignment)
        {
            this.cellStyle = new DataGridViewCellStyle();
            this.font = new FCFont();
			InitializeDefaultValues(defaultAlignment, defaultBackColor, defaultForeColor, defaultVerticalAlignment);

		}

		public FCStyle(DataGridViewCellStyle cellStyle, Alignment defaultAlignment, Color defaultBackColor, Color defaultForeColor, VerticalAlignment defaultVerticalAlignment)
        {
            this.cellStyle = cellStyle;
			this.font = new FCFont();
			InitializeDefaultValues(defaultAlignment, defaultBackColor, defaultForeColor, defaultVerticalAlignment);
		}
		#endregion

		#region Public Methods
		public bool ShouldSerializeAlignment()
		{
			return Alignment != defaultAlignment;
		}

		public void ResetAlignment()
		{
			Alignment = defaultAlignment;
		}

		public bool ShouldSerializeBackColor()
		{
			return BackColor != defaultBackColor;
		}

		public void ResetBackColor()
		{
			BackColor = defaultBackColor;
		}

		public bool ShouldSerializeForeColor()
		{
			return ForeColor != defaultForeColor;
		}

		public void ResetForeColor()
		{
			ForeColor = defaultForeColor;
		}

		public bool ShouldSerializeVerticalAlignment()
		{
			return VerticalAlignment != defaultVerticalAlignment;
		}

		public void ResetVerticalAlignment()
		{
			VerticalAlignment = defaultVerticalAlignment;
		}

		/// <summary>
		/// This method causes a style to inherit all of its properties from its parent style.
		/// </summary>
		[Obsolete("Not implemented. Added for migration compatibility")]
        public void Reset()
        {
        }

		#endregion

		#region Private Methods
		private void InitializeDefaultValues(Alignment defaultAlignment, Color defaultBackColor, Color defaultForeColor, VerticalAlignment defaultVerticalAlignment)
		{
			this.Alignment = this.defaultAlignment = defaultAlignment;
			this.BackColor = this.defaultBackColor = defaultBackColor;
			this.ForeColor = this.defaultForeColor = defaultForeColor;
			this.VerticalAlignment = this.defaultVerticalAlignment = defaultVerticalAlignment;
		}
		#endregion
	}
}
