﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.ComponentModel;


namespace fecherFoundation
{
    internal class DataGridViewCustomComboBoxCell : DataGridViewComboBoxCell
    {
        /// <summary>
        /// The form is used as custom DropDown control.
        /// </summary>
        private TDBDropDownForm _TDBDropDownForm = null;

        /// <summary>
        /// Occurs when selected item changed.
        /// </summary>
        public event EventHandler ValueChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewComboBox"/> class.
        /// </summary>
        public DataGridViewCustomComboBoxCell()
        {
            _TDBDropDownForm = new TDBDropDownForm(this);
            _TDBDropDownForm.Closed += new EventHandler(OnFormClosed);
        }

        /// <summary>
        /// Called when form is closed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnFormClosed(object sender, EventArgs e)
        {
            FCTDBGrid grid = this.DataGridView as FCTDBGrid;
            if (grid != null)
            {
                grid.IsDropDownClosing = true;

                if (grid.IsCancelEdit)
                {
                    grid.IsDropDownOpened = false;
                    return;
                }
                //CHE: return if drop down was already closed
                if (!grid.IsDropDownOpened)
                {
                    return;
                }
            }
            else
            {
                if (this._TDBDropDownForm.CurrentCol != null && this._TDBDropDownForm.CurrentCol.DataGridView != null)
                {
                    grid = this._TDBDropDownForm.CurrentCol.DataGridView as FCTDBGrid;
                }

                if (grid != null)
                {
                    grid.IsDropDownOpened = false;
                    return;
                }
            }

            grid.IsDropDownOpened = false;
            if(this.RowIndex >= 0)
            {                
                if (this._TDBDropDownForm.TDBDropDown != null && this._TDBDropDownForm.TDBDropDown.CurrentRow != null)
                {
                    //IPI: must save the row index of the CellTemplate: after a RowValidate, CurrentCell might be changed 
                    // which will cause losing the CellTemplate
                    int currentRow = this.RowIndex;                    

                    DataGridViewRow row = grid.Rows[currentRow];
                    
                    int invalidatedRows = grid.Rows.Count;
                                 
                    //IPI: this.Value cannot be set directly because it will get null when another row is validated
                    // and the current cell is set to the on the validated row
                    object cellValue = null;
                    object originalCellValue = this.Value;
					//SBE: check if grid DataField is setup as a DataProperty for one column
                    //APE: if the dropdown dialog was closed by selecting a value from the dropdown, get the selected value
                    //If the dropdown dialog was closed by pressing the dropdown arrow, set the cell value to the last value from the dropdown grid
                    //If a previous value was selected, then the dropdown grid was opened and closed by pressing the dropdown arrow, leave the previously selected value in the cell
                    if (((global::System.Windows.Forms.Form)sender).DialogResult == DialogResult.OK)
                    {
                        if (!string.IsNullOrEmpty(this._TDBDropDownForm.TDBDropDown.DataField) && this._TDBDropDownForm.TDBDropDown.Columns[this._TDBDropDownForm.TDBDropDown.DataField] != null)
                        {
                            cellValue = this._TDBDropDownForm.TDBDropDown[this._TDBDropDownForm.TDBDropDown.Columns[this._TDBDropDownForm.TDBDropDown.DataField].Index, this._TDBDropDownForm.TDBDropDown.CurrentRow.Index].Value;
                        }
                        else
                        {
                            cellValue = this._TDBDropDownForm.TDBDropDown[this._TDBDropDownForm.TDBDropDown.Columns[0].Index, this._TDBDropDownForm.TDBDropDown.CurrentRow.Index].Value;
                        }
                    }
                    else if (((global::System.Windows.Forms.Form)sender).DialogResult == DialogResult.None)
                    {
                        if (grid[this.ColumnIndex, currentRow].Value != null && !string.IsNullOrEmpty(grid[this.ColumnIndex, currentRow].Value.ToString()))
                        {
                            cellValue = grid[this.ColumnIndex, currentRow].Value;
                        }
                        else
                        {
                            if (this._TDBDropDownForm.TDBDropDown.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(this._TDBDropDownForm.TDBDropDown.DataField) && this._TDBDropDownForm.TDBDropDown.Columns[this._TDBDropDownForm.TDBDropDown.DataField] != null)
                                {
                                    cellValue = this._TDBDropDownForm.TDBDropDown[this._TDBDropDownForm.TDBDropDown.Columns[this._TDBDropDownForm.TDBDropDown.DataField].Index, this._TDBDropDownForm.TDBDropDown.Rows.Count - 1].Value;
                                }
                                else
                                {
                                    cellValue = this._TDBDropDownForm.TDBDropDown[this._TDBDropDownForm.TDBDropDown.Columns[0].Index, this._TDBDropDownForm.TDBDropDown.Rows.Count - 1].Value;
                                }
                            }
                        }
                    }
                    
                    //IPI: set DataChanged to be able to execute OnRowValidated
                    grid.DataChanged = true;

                    if (this._TDBDropDownForm.rowValidating >= 0 && this._TDBDropDownForm.colValidating >= 0)
                    {
                        //IPI: in VB6, if the current row was not editted, BeforeUpdate, RowValidated & AfterUpdate are not raised
                        if (grid.IsCurrentCellInEditMode || grid.IsCurrentRowDirty)
                        {
                            //Raise the row validating event if needed
                            //CHE: raise RowValidating to execute AfterInsert, otherwise first row is removed with filter if clicking directly on combo button on second row 
                            DataGridViewCellCancelEventArgs dataGridViewCellCancelEventArg = new DataGridViewCellCancelEventArgs(this._TDBDropDownForm.colValidating, this._TDBDropDownForm.rowValidating);
                            grid.RaiseRowValidating(grid, dataGridViewCellCancelEventArg);
                            if (!dataGridViewCellCancelEventArg.Cancel)
                            {
                                grid.RaiseRowValidated(grid, new DataGridViewCellEventArgs(this._TDBDropDownForm.colValidating, this._TDBDropDownForm.rowValidating));
                            }
                        }
                        else
                        {
                            grid.CurrentCellOldValue = grid.CurrentCell.Value;
                        }
                        //IPI: in row validation e.g.AfterUpdate, the CurrentCell/CurrentRow might be changed
                        // => CellTemplate.DataGridView is also becoming null 
                        //IPI: if a row is cancelled in BeforeUpdate, it will be removed from the grid
                        invalidatedRows -= grid.Rows.Count;       
                    }

                    if (invalidatedRows == 0 && currentRow >= grid.Rows.Count)
                    {
                        grid.BeginEdit(false);
                        grid.NotifyCurrentCellDirty(true);
                        grid.NotifyCurrentCellDirty(false);
                    }

                    if (currentRow - invalidatedRows >= 0)
                    {
                        currentRow -= invalidatedRows;
                    }
                    grid[this.ColumnIndex, currentRow].Value = cellValue;
                    //IPI: setting the current cell will create a new row in the DGV which is reflected in the DefaultView of the DataSource
                    grid.CurrentCell = grid[this.ColumnIndex, currentRow];
                    ////IPI: if BeforeUpdate returns false, the invalid row will be removed from the grid 
                    //// and grid.Rows.Count is also updated after the previous setting of the CurrentCell
                    //if (currentRow >= grid.Rows.Count)
                    //{
                    //    currentRow = grid.Rows.Count - 1;                        
                    //}
                    //IPI: when a row validation was done because CellTemplate is lost, after setting the current cell, must reset the cell value                    
                    grid.CurrentCellOldValue = originalCellValue;
                    grid[this.ColumnIndex, currentRow].Value = cellValue;
                    //IPI: when setting CurrentCell, SelectionChanged event will be raised; if a grid.Focus is invoked in a SelChange event handler,
                    // this call will generate GotFocus on the grid, which will cause isDropDownClosing to be reset
                    grid.IsDropDownClosing = true;
                    if (currentRow + 1 == grid.Rows.Count)
                    {
                        grid.BeginEdit(false);                            
                    }
                    //IPI: NotifyCurrentCellDirty will add a new row in the DGV
                    grid.NotifyCurrentCellDirty(true);


                    DataGridViewCustomComboBoxColumn currentCol = grid.Columns[this.ColumnIndex] as DataGridViewCustomComboBoxColumn;

                    TDBDropDown currentDropDown = currentCol.TDBDropDown;
                    if (currentDropDown != null)
                    {
                        currentDropDown.CallDropDownClose(currentDropDown, new EventArgs());
                    }
                    
                    //When closing the drop down, the CellValidating and CellValidated events are not raised
                    
                    //IPI: if MoveFirst is called in BeforeColUpdate for a DropDownGrid, setting CurrentCell will generate SelectionChanged causing an additional CellValidating
                    if (!grid.CellValidatingInProgress)
                    {
                        FCDataGridViewCellValidatingEventArgs eventArgs = new FCDataGridViewCellValidatingEventArgs(grid.Col, grid.Row, grid.CurrentCell.FormattedValue, grid.CurrentCellOldValue);
                        grid.RaiseCellValidating(grid, eventArgs);
                    }
                }
                //APE: if the dropdown grid contains no rows, the current cell will not be changed and the dropdownclose event will not be raised
                //in this case, change the current cell and raise the dropdownclose event
                else if (((global::System.Windows.Forms.Form)sender).DialogResult == DialogResult.None && this._TDBDropDownForm.TDBDropDown != null && this._TDBDropDownForm.TDBDropDown.CurrentRow == null)
                {
                    //IPI: must save the row index of the CellTemplate: after a RowValidate, CurrentCell might be changed 
                    // which will cause losing the CellTemplate
                    int currentRow = this.RowIndex;

                    grid.CurrentCell = grid[this.ColumnIndex, currentRow];

                    DataGridViewCustomComboBoxColumn currentCol = grid.Columns[this.ColumnIndex] as DataGridViewCustomComboBoxColumn;

                    TDBDropDown currentDropDown = currentCol.TDBDropDown;
                    if (currentDropDown != null)
                    {
                        currentDropDown.CallDropDownClose(currentDropDown, new EventArgs());
                    }
                }
                //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
                //for this case use CustomComboBoxColumn with ListBox as DropDown
                else if (this._TDBDropDownForm.SelectedItem != null)
                {
                    //IPI: must save the row index of the CellTemplate: after a RowValidate, CurrentCell might be changed 
                    // which will cause losing the CellTemplate
                    int currentRow = this.RowIndex;

                    DataGridViewRow row = grid.Rows[currentRow];

                    int invalidatedRows = grid.Rows.Count;

                    //IPI: this.Value cannot be set directly because it will get null when another row is validated
                    // and the current cell is set to the on the validated row
                    object cellValue = null;
                    object originalCellValue = this.Value;
                    if (((global::System.Windows.Forms.Form)sender).DialogResult == DialogResult.OK)
                    {
                        cellValue = this._TDBDropDownForm.SelectedItem.Value;
                    }
                    else if (((global::System.Windows.Forms.Form)sender).DialogResult == DialogResult.None)
                    {
                        if (grid[this.ColumnIndex, currentRow].Value != null && !string.IsNullOrEmpty(grid[this.ColumnIndex, currentRow].Value.ToString()))
                        {
                            cellValue = grid[this.ColumnIndex, currentRow].Value;
                        }
                    }

                    grid.DataChanged = true;

                    grid[this.ColumnIndex, currentRow].Value = cellValue;
                    grid.CurrentCell = grid[this.ColumnIndex, currentRow];
                    grid.CurrentCellOldValue = originalCellValue;
                    grid[this.ColumnIndex, currentRow].Value = cellValue;
                    grid.IsDropDownClosing = true;
                }

                if (ValueChanged != null)
                {
                    ValueChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has a custom drop down.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has a custom drop down; otherwise, <c>false</c>.
        /// </value>
        //protected override bool IsCustomDropDown
        //{
        //    get
        //    {
        //        return true;
        //    }
        //}

        /// <summary>
        /// Gets the custom form to display as drop down
        /// </summary>
        /// <returns></returns>
     //   protected override System.Windows.Forms.Form GetCustomDropDown()
     //   {
     //       DataGridViewColumn column = this.DataGridView.Columns[this.ColumnIndex];
     //       FCTDBGrid grid = column.DataGridView as FCTDBGrid;

     //       if (grid != null)
     //       {
     //           grid.DisableEvents = true;

     //           DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(column);

     //           if (objColDefinitions.Button && column.DropDown() == null)
     //           {
     //               //APE: set disable events to false beacause the current cell will be set and the events need to be raised
     //               grid.DisableEvents = false;
     //               grid.Col = grid.Columns[this.ColumnIndex].DisplayIndex;
     //               grid.Row = this.RowIndex;
     //               //APE: set disable events back after current cell is set and events raised
     //               grid.DisableEvents = true;
					////CHE: do not call button click if ReadOnly
     //               if (!grid.Columns[this.ColumnIndex].ReadOnly)
     //               {
     //                   grid.RaiseButtonClick(column, EventArgs.Empty);
     //               }

     //               //CHE: need to call notify to generate new row with * at the end
     //               grid.NotifyCurrentCellDirty(true);
     //               grid.NotifyCurrentCellDirty(false);

     //               //IPI: by ending edit mode, we avoid an additional CellValidating for the CustomComboBox column
     //               grid.EndEdit();

     //               grid.DisableEvents = false;
     //               //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
     //               //for this case use CustomComboBoxColumn with ListBox as DropDown
     //               if (column.ValueItems().Presentation() == Presentation.dbgComboBox && 
     //                   (column.ValueItems().Count > 1 || (column.ValueItems().Count == 1 && !(Convert.ToString(column.ValueItems()[0].ValueMember) == "0" && Convert.ToString(column.ValueItems()[0].DisplayMember) == ""))))
     //               {
     //                   _TDBDropDownForm.dropDownGrid.Visible = false;
     //                   _TDBDropDownForm.listBox.Visible = true;
     //                   _TDBDropDownForm.DialogResult = DialogResult.None;
     //                   return _TDBDropDownForm;
     //               }
     //               return null;
     //           }
     //           else
     //           {
     //               //CHE: in VB6 when setting presentation to ComboBox the column Button property becomes true and ButtonClick event is raised beside displaying ComboBoxColumn items; 
     //               //for this case use CustomComboBoxColumn with ListBox as DropDown
     //               _TDBDropDownForm.dropDownGrid.Visible = true;
     //               _TDBDropDownForm.listBox.Visible = false;
     //               _TDBDropDownForm.DialogResult = DialogResult.None;
     //               grid.DisableEvents = false;
     //               return _TDBDropDownForm;
     //           }
     //       }

     //       return null;
     //   }

        /// <summary>
        /// Render the control Attributes
        /// </summary>
        /// <param name="objContext"></param>
        /// <param name="objWriter"></param>
        /// <param name="blnRenderOwner"></param>
        //protected override void RenderAttributes(IContext objContext, IAttributeWriter objWriter, bool blnRenderOwner)
        //{
        //    base.RenderAttributes(objContext, objWriter, blnRenderOwner);
        //}

        //protected override void RenderCustomComboboxTextValue(IAttributeWriter objWriter)
        //{
        //    if (this.Value == null)
        //    {
        //        return;
        //    }
            
        //    var comboItems = ((DataGridViewComboBoxColumn)this.OwningColumn).Items;
        //    if (comboItems != null && comboItems.Count > 0 && comboItems[0].GetType() == typeof(FCValueItem))
        //    {
        //        //IPI: save the current value because if this.Value = "", must pass 0 value to Convert.ToDecimal, but the value rendered should still be "this.Value" (and not 0)
        //        object value = this.Value;
        //        string strValue = null;
        //        if (value != DBNull.Value)
        //        {
        //            strValue = Convert.ToString(value);
        //        }
        //        foreach (FCValueItem v in comboItems)
        //        {
        //            if (Convert.ToString(v.ValueMember) == strValue)
        //            {
        //                objWriter.WriteAttributeString(WGAttributes.Text, Convert.ToString(v.DisplayMember));
        //                return;
        //            }
        //            else if (v.ValueMember != null && v.ValueMember != DBNull.Value && this.Value != null && this.Value != DBNull.Value && (v.ValueMember.GetType() == typeof(decimal) || this.Value.GetType() == typeof(decimal)))
        //            {
        //                if (string.IsNullOrEmpty(Convert.ToString(v.ValueMember)))
        //                {
        //                    v.ValueMember = 0;
        //                }
        //                if (string.IsNullOrEmpty(strValue))
        //                {
        //                    value = 0;
        //                }
        //                if (Convert.ToDecimal(string.Format("{0:0.00,##}", v.ValueMember)) == Convert.ToDecimal(string.Format("{0:0.00,##}", value)))
        //                {
        //                    objWriter.WriteAttributeString(WGAttributes.Text, Convert.ToString(v.DisplayMember));
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //    //IPI: OnCellFormatting is not raised for CustomComboBoxColumn, should always display FormattedValue
        //    //base.RenderCustomComboboxTextValue(objWriter);   
        //    objWriter.WriteAttributeString(WGAttributes.Text, Convert.ToString(this.FormattedValue));                        
        //}

        /// <summary>
        /// Gets the formatted value of the cell's data.
        /// </summary>
        /// <param name="value">The value to be formatted.</param>
        /// <param name="rowIndex">The index of the cell's parent row.</param>
        /// <param name="cellStyle">The <see cref="T:System.Windows.Forms.DataGridViewCellStyle"></see> in effect for the cell.</param>
        /// <param name="valueTypeConverter">A <see cref="T:System.ComponentModel.TypeConverter"></see> associated with the value type that provides custom conversion to the formatted value type, or null if no such custom conversion is needed.</param>
        /// <param name="formattedValueTypeConverter">A <see cref="T:System.ComponentModel.TypeConverter"></see> associated with the formatted value type that provides custom conversion from the value type, or null if no such custom conversion is needed.</param>
        /// <param name="context">A bitwise combination of <see cref="T:System.Windows.Forms.DataGridViewDataErrorContexts"></see> values describing the context in which the formatted value is needed.</param>
        /// <returns>
        /// The value of the cell's data after formatting has been applied or null if the cell is not part of a <see cref="T:System.Windows.Forms.DataGridView"></see> control.
        /// </returns>
        /// <exception cref="T:System.Exception">Formatting failed and either there is no handler for the <see cref="E:System.Windows.Forms.DataGridView.DataError"></see> event of the <see cref="T:System.Windows.Forms.DataGridView"></see> control or the handler set the <see cref="P:System.Windows.Forms.DataGridViewDataErrorEventArgs.ThrowException"></see> property to true. The exception object can typically be cast to type <see cref="T:System.FormatException"></see> for type conversion errors or to type <see cref="T:System.ArgumentException"></see> if value cannot be found in the <see cref="P:System.Windows.Forms.DataGridViewComboBoxCell.DataSource"></see> or the <see cref="P:System.Windows.Forms.DataGridViewComboBoxCell.Items"></see> collection. </exception>
        protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
        {
            //IPI: OnCellFormatting is not raised for CustomComboBoxColumn which will not raise FormatText Event
            FCTDBGrid grid = this.DataGridView as FCTDBGrid;
            if (grid != null && rowIndex >= 0)
            {
                object cellValue = grid.Rows[rowIndex].Cells[this.ColumnIndex].Value;
                if ((cellValue != DBNull.Value && value != DBNull.Value && Convert.ToString(cellValue) != Convert.ToString(value)) ||
                    (cellValue != DBNull.Value && value == DBNull.Value) ||
                    (cellValue == DBNull.Value && value != DBNull.Value))
                {
                    grid.Rows[rowIndex].Cells[this.ColumnIndex].Value = value;
                }
                DataGridViewCellFormattingEventArgs ev = new DataGridViewCellFormattingEventArgs(this.ColumnIndex, rowIndex, value, ValueType, cellStyle);
                grid.RaiseCellFormatting(ev);

                //HACK:IPI: for VWG 7.1.1 CustomComBox value is not displayed if it is not contained in this.Items collection
                #region Add value to column Items

                if (grid.Columns[this.ColumnIndex].ValueItems().Count == 0 && value != null)
                {
                    string strValue = null;
                    if (value != DBNull.Value)
                    {
                        strValue = Convert.ToString(value);
                    }

                    bool itemFound = false;
                    object item = null;
                    for (int i = 0; i < this.Items.Count; i++)
                    {
                        item = this.Items[i];

                        if (item == DBNull.Value)
                        {
                            if (value == DBNull.Value)
                            {
                                itemFound = true;
                                break;
                            }
                        }
                        else
                        {
                            if (Convert.ToString(item) == strValue)
                            {
                                itemFound = true;
                                break;
                            }
                        }
                    }

                    if (!itemFound)
                    {
                        this.Items.Add(value);
                    }
                }
                #endregion

                //IPI: if a format has been applied and the value was not changed by CellFormatting event, return the base formatted value
                //if (this.FormattedCellStyle != null && !string.IsNullOrEmpty(this.FormattedCellStyle.Format) && 
                //    value != DBNull.Value && ev.Value != DBNull.Value && Convert.ToString(value) == Convert.ToString(ev.Value))
                //{
                //    return base.GetFormattedValue(ev.Value, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);                    
                //}

                return ev.Value;
            }

            //IPI: return the base formatted value
            return base.GetFormattedValue(value, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context);           
        }
    }
}
