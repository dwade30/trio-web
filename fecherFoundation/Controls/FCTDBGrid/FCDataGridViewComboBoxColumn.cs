﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fecherFoundation
{
	public class FCDataGridViewComboBoxColumn : DataGridViewComboBoxColumn
	{
		public FCStyle EditorStyle { get; set; } = new FCStyle(Alignment.Left, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, VerticalAlignment.Top);
		public FCStyle HeadingStyle { get; set; } = new FCStyle(Alignment.Left, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, VerticalAlignment.VerticalCenter);
		public FCStyle Style { get; set; } = new FCStyle(Alignment.Left, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, VerticalAlignment.Top);
	}

	
}
