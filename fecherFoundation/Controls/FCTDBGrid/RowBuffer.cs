﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    [Obsolete("Not implemented. Added for migration compatibility")]
    public class RowBuffer
    {
        private int[] bookmark;

        [Obsolete("Not implemented. Added for migration compatibility")]
        public new int[] Bookmark
        {
            get { return bookmark; }
            set { bookmark = value; }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public object[,] Value
        {
            get;
            set;
        }

		public int RowCount { get; set; }

		public int ColumnCount { get; set; }
    }
}
